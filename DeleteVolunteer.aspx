<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="DeleteVolunteer.aspx.vb" Inherits="DeleteVolunteer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   			
   			<table border=0  width=100%>
   			<tr><td>
						<b><asp:Label  ForeColor=green ID="lblLoginName" runat=server></asp:Label></b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<b><asp:Label forecolor=green ID="lblLoginRole" runat=server></asp:Label></b>												
						
					</td>					
				</tr>	
				<tr><td><asp:Label ID="lblSessionTimeout" Visible=false ForeColor=red runat=server></asp:Label></td></tr>
				<tr><td>			
				<table  border=1 runat="server" id="tblAssignRoles" bgcolor=silver width =30%>				
				<TBODY>					
				<tr><td  colspan="2" bgcolor=white  >
						<b>1. Select Role to get Volunteer List</b>						
					</td>
				</tr>				
				<tr>				
						<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 50px; height: 25px;">
                            &nbsp;Role Category:
						</td>
						<td style="width: 347px; height: 25px;">
						<asp:dropdownlist id="ddlRoleCatSch" tabIndex="7" runat="server" CssClass="SmallFont" autopostback=true OnSelectedIndexChanged="ddlRoleCat_selectedIndexChanged">
						<asp:ListItem Text="Select a Category" Value="0" Selected=true></asp:ListItem>
						</asp:dropdownlist>							
						<asp:label ID="lblRoleCat" runat=server Visible=false></asp:label>
						</td>
						</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 50px">Role:
						</td>
						<td style="width: 347px">
						<asp:dropdownlist id="ddlRoleSch" tabIndex="7"  DataTextField="selection" DataValueField="roleid" runat="server" CssClass="SmallFont">						
						</asp:dropdownlist>						
						<asp:label ID="lblRole" runat=server Visible=false></asp:label>
						</td>
				</tr>	
				<tr>
				<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 50px; height: 17px;">
							
						</td>
						<td style="width: 347px; height: 17px;">
						     <asp:dropdownlist id="ddlList"  visible = false tabIndex="7" runat="server" CssClass="SmallFont">	    
						    </asp:dropdownlist><br />	</td>		
						</tr>
				<tr>
				<td style="height: 4px">
				
				</td><td style="width: 347px; height: 4px;">
                    <asp:Button ID="Find" runat="server" Text="Find" /></td>				
				</tr>										
			</table>			
			</td>
			</tr>
			<tr><td>				
				<table id="TblSearchString" visible=false runat="server" width=100%>
				<tr>
					<td >
						<asp:label id="txtSearchCriteria"  runat="server" Visible="False" ForeColor="blue">
						</asp:label>							
					</td>
				</tr>				
			</table>			
			<table id="tblMessage"  visible=false runat="server" width=65%>
				<tr>
					<td>					  	
						<asp:Label id="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>						
						<asp:Label id="lblMsg" runat="server" Visible="False" ForeColor="blue"></asp:Label>						<br />
						<br /><asp:LinkButton ID="hLinkAddNewVolunteer" visible=false Text="Add New Volunteer"   Font-Bold="true" OnClick="hLinkAddNew_Onclick" runat="server"></asp:LinkButton>
						<p></p>
					</td>
				</tr>					       		
			</table>	
			</td></tr>		
			<tr><td>
			
			<table id="tblVolRoleResults" visible=false runat="server" width="100%">
				<tr>
					<td align="left" >
					<strong><asp:Label ID="lblGrdVolResultsHdg" Visible=true runat=server></asp:Label><br />
					<asp:Label id="lblUpdateError" runat="server" Visible="False" ForeColor="Red"></asp:Label>	
					</strong>						
					</td>
				</tr>
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" style="height: 103px" >
				   <asp:Panel ID="PVolResults" runat=server>
	               <asp:DataGrid ID="grdVolResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="volunteerid"
					Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" AllowPaging=true PageSize=10  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
					<ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>  
					<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor="Gainsboro" ></FooterStyle>
					<%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                            <Columns>
                                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                                <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ></asp:buttoncolumn>
     
                                <asp:BoundColumn DataField="volunteerID"   HeaderText="Volunteer Id" readonly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="memberID" HeaderText="Member Id" readonly=true  Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="name" headerText="Member Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                
                                <asp:TemplateColumn HeaderText="RoleCode" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Selection")%>'></asp:Label>
                                        <asp:HiddenField ID="hfRoleId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RoleId")%>' />
                                        
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlRoleCode" runat="server" DataTextField="Selection" DataValueField="RoleId" 
                                        OnPreRender="Role_PreRender" OnInit="Role_onInit" OnSelectedIndexChanged="Role_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList> 
                                      </EditItemTemplate>                                 
                                </asp:TemplateColumn > 
                                <asp:TemplateColumn HeaderText="TeamLead" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblTeamLead" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeamLead")%>'></asp:Label>
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlTeamLead" runat="server" DataTextField="TeamLead" DataValueField="TeamLead" 
                                        OnPreRender="TeamLead_PreRender" AutoPostBack="false">
                                        <asp:ListItem Value='Y' Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value='N' Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>                                 
                                </asp:TemplateColumn >  
                                  <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                      </ItemTemplate>           
                                      <EditItemTemplate>
                                       <asp:DropDownList id="ddlEventCode" runat="server" DataTextField="EventCode" DataValueField="EventId" 
                                        OnPreRender="EventCode_PreRender" AutoPostBack="false">                                                                                                                 
                                       </asp:DropDownList>
                                        <asp:Label ID="lblEventCode2" runat="server" text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'
                                        OnPreRender="lblEventCode2_PreRender"></asp:Label>                                                                                  
                                      </EditItemTemplate>                                      
                                </asp:TemplateColumn >   
                                 <asp:TemplateColumn HeaderText="EventYear" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlEventYear" runat="server" DataTextField="EventYear" DataValueField="EventYear" 
                                        OnPreRender="EventYear_PreRender" AutoPostBack="false">
                                        <asp:ListItem Value="2007">2007</asp:ListItem></asp:DropDownList>                                        
                                      </EditItemTemplate>                                 
                                </asp:TemplateColumn > 
                                <asp:TemplateColumn HeaderText="Product Group" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                     <asp:Label ID="lblProductGroupId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label> 
                                     <asp:Label ID="lblProductGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlProductGroup" runat="server"  OnInit="ProductGroup_onInit"
                                        OnPreRender="ProductGroup_PreRender" OnSelectedIndexChanged=ProductGroup_SelectedIndexChanged AutoPostBack="true">
                                        </asp:DropDownList>                                        
                                      </EditItemTemplate>                                 
                                </asp:TemplateColumn > 
                                <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                      <asp:Label ID="lblProductId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>'></asp:Label>
                                        <asp:Label ID="lblProduct" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlProduct" runat="server" OnPreRender="Product_PreRender" AutoPostBack="false">
                                        </asp:DropDownList>                                        
                                      </EditItemTemplate>                                 
                                </asp:TemplateColumn >      
                                <asp:TemplateColumn HeaderText="Chapter" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                      </ItemTemplate>                                                                        
                                </asp:TemplateColumn >   
                                  <asp:TemplateColumn HeaderText="YahooGroup" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>
                                      <asp:TextBox ID="txtYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:TextBox>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn > 
                                                    
                                                        
                                  
                                <asp:TemplateColumn HeaderText="Agent Flag" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblAgentFlag" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AgentFlag")%>'></asp:Label>
                                      </ItemTemplate>         
                                       <EditItemTemplate>
                                      <asp:DropDownList id="ddlAgentFlag" runat="server" DataTextField="AgentFlag" DataValueField="AgentFlag" 
                                        OnPreRender="AgentFlag_PreRender" AutoPostBack="false">
                                        <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>  
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn >    
                                <asp:TemplateColumn HeaderText="Write Access" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblWriteAccess" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.WriteAccess")%>'></asp:Label>
                                      </ItemTemplate> 
                                      <EditItemTemplate>
                                      <asp:DropDownList id="ddlWriteAccess" runat="server" DataTextField="WriteAccess" DataValueField="WriteAccess" 
                                        OnPreRender="WriteAccess_PreRender" AutoPostBack="false">
                                        <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>  
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>                                                                        
                                </asp:TemplateColumn >     
                                <asp:TemplateColumn HeaderText="Authorization" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblAuthorization" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Authorization")%>'></asp:Label>
                                      </ItemTemplate>                                                                        
                                      <EditItemTemplate>
                                      <asp:DropDownList id="ddlAuthorization" runat="server" DataTextField="Authorization" DataValueField="Authorization" 
                                        OnPreRender="Authorization_PreRender" AutoPostBack="false">                                        
                                       <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>  
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>  
                                </asp:TemplateColumn >
                                 <asp:TemplateColumn HeaderText="IndiaChapterName" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblIndiaChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.IndiaChapterName")%>'></asp:Label>
                                      </ItemTemplate>                                                                        
                                      <EditItemTemplate>
                                      <asp:DropDownList id="ddlIndiaChapter" runat="server" DataTextField="IndiaChapterName" DataValueField="IndiaChapter" 
                                        OnPreRender="IndiaChapter_PreRender" AutoPostBack="false">
                                       <asp:ListItem Selected="True" Value="0">-Select Chapter-</asp:ListItem>
							            <asp:ListItem Value="Ahmedabad">Ahmedabad</asp:ListItem>
							            <asp:ListItem Value="Bangalore">Bangalore</asp:ListItem>
							            <asp:ListItem Value="Bhubaneswar">Bhubaneswar</asp:ListItem>
							            <asp:ListItem Value="Chennai">Chennai</asp:ListItem>
							            <asp:ListItem Value="Hyderabad">Hyderabad</asp:ListItem>
							            <asp:ListItem Value="Jamshedpur">Jamshedpur</asp:ListItem>
							            <asp:ListItem Value="Jodhpur">Jodhpur</asp:ListItem>
							            <asp:ListItem Value="Kanpur">Kanpur</asp:ListItem>
							            <asp:ListItem Value="Katihar">Katihar</asp:ListItem>
							            <asp:ListItem Value="Kochi">Kochi</asp:ListItem>
							            <asp:ListItem Value="Kolkata">Kolkata</asp:ListItem>
							            <asp:ListItem Value="Madurai">Madurai</asp:ListItem>
							            <asp:ListItem Value="Pune">Pune</asp:ListItem>
							            <asp:ListItem Value="Sibsagar">Sibsagar</asp:ListItem>
							            <asp:ListItem Value="Trivandrum">Trivandrum</asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>  
                                </asp:TemplateColumn > 
                                <asp:BoundColumn DataField="national"  HeaderText="National" readonly=true Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="finals" HeaderText="finals" readonly=true  Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="zoneId" headerText="zoneId" ReadOnly=true Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="clusterId"  HeaderText="clusterId" readonly=true Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="chapterId" HeaderText="Chapter Id" readonly=true  Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="eventId" headerText="eventId" ReadOnly=true Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="indiaChapter" headerText="India Chapter" ReadOnly=true Visible="false" ></asp:BoundColumn  >                                                            
                              
                         </Columns>
  				</asp:DataGrid>&nbsp;
  				</asp:Panel>
  				</td></tr>
  				</table> 
  				</td></tr>
  				<tr><td>
  				<asp:Panel ID="pIndSearch" runat=server>
  			            
			<table  border=1 runat="server" id="tblIndSearch"  width=50% visible=false bgcolor=silver >				
				<TBODY>
				<tr><td colspan="4" bgcolor=white><b>3. Search NSF Member Records <font color="fuchsia">(Please enter name of volunteer or other attributes)</font></b></td></tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Email:
						</td>
						<td >
						<asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;First Name/Organization:
						</td>
						<td >
						<asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
						</td>
				</tr>
				
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Last Name:
						</td>
						<td >
						<asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Street:
						</td>
						<td >
						<asp:TextBox ID="txtStreet" runat="server"></asp:TextBox>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;City:
						</td>
						<td >
						<asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;State:
						</td>
						<td >
						<asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Zip:
						</td>
						<td >
						<asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Home Phone:
						</td>
						<td >
						<asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Referred by:
						</td>
						<td >
						<asp:TextBox ID="txtReferredBy" runat="server"></asp:TextBox>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Liaison Person:
						</td>
						<td >
						<asp:TextBox ID="txtLiaison" runat="server"></asp:TextBox>
				</td> </tr>
						
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;NSF Chapter:
						</td>
						<td >
						<asp:dropdownlist ID="ddlIndSearchChapter" runat="server" ></asp:DropDownList>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;
						</td>
						<td >
						  <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" />	&nbsp;&nbsp;&nbsp;					
						   <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick"/>						
						</td>
				</tr>		
					
				</TBODY>					
			</table>
			</asp:Panel>
			</td></tr>
			
			<tr><td>
			
			<table id="tblIndMessage" runat="server" width=65%>
				<tr>
					<td style="width: 496px; height: 9px">
						<asp:Label id="lblIndError" runat="server" Visible="False" ForeColor="Red"></asp:Label>						
						<asp:Label id="lblIndMsg" runat="server" Visible="False" ForeColor="blue"></asp:Label>						
					</td>
				</tr>				
			</table>
			</td></tr>
			<tr><td>
			<asp:Panel ID="pIndResults" runat=server>
			<table id="tblGrdIndResults" visible=false runat="server" width="100%">
				<tr>
					<td align="left" style="height: 2px" >
					<strong><asp:Label ID="lblGrdIndResultsHdg" runat=server></asp:Label></strong>						
					</td>
				</tr>
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" style="height: 103px" >	               		
			    <asp:DataGrid ID="grdIndResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="autoMemberid"
					Height="14px" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" AllowPaging=true PageSize=5   PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small ></AlternatingItemStyle>
					<ItemStyle Font-Size="Small" Font-Names="Verdana" backcolor=white Wrap="False" ></ItemStyle>
					<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor=Gainsboro></FooterStyle>					
                            <Columns>   
                                <asp:buttoncolumn ButtonType=LinkButton  CommandName="AddRole" Text="Add Role"></asp:buttoncolumn>
                                <asp:buttoncolumn ButtonType=LinkButton  CommandName="ViewRoles" Text="View Roles"></asp:buttoncolumn>
                                <asp:BoundColumn  DataField="AutomemberId" headerText="Member Id" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="FirstName" headerText="FirstName" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="LastName" headerText="Last Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="email" headerText="email" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="HPhone" headerText="Home Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="address1" headerText="address" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="city" headerText="city" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="state" headerText="state" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="zip" headerText="zip" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="chapterCode" headerText="chapter" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReferredBy" headerText="ReferredBy" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="LiasonPerson" headerText="Liason Person" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                               
                            </Columns>
                            </asp:DataGrid>
                             <asp:Button ID="btnGrdIndResultsClose" runat="server"  Text="Close" onclick="btnGrdIndResultsClose_onclick"/>						
                            </td></tr></table>  
                            </asp:Panel>					  
                            </td></tr>
			<tr><td>                          
         			
			<table id="tblRoles"  visible=false runat="server" width=65%>
				<tr>
					<td style="width: 496px; height: 8px">
						<asp:Label id="Label2" runat="server" Visible="False" ForeColor="Red"></asp:Label>						
						<asp:Label id="Label3" runat="server" Visible="False" ForeColor="blue"></asp:Label>						
					</td>
				</tr>				
			</table>
			</td></tr>
			<tr><td>
			<asp:Panel ID="pVolRoles" runat=server>					
			<table id="tblGrdVolRoles" visible=false runat=server>
			
			<tr><td align="left" style="height: 2px" >
					<strong><asp:Label ID="lblGrdVolRolesHdg" Visible=true runat=server></asp:Label></strong>						
					</td></tr>			
			<tr><td>
			<asp:DataGrid ID="grdVolRoles" runat="server"   CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="Memberid"
					Height="14px" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" AllowPaging=true PageSize=5  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
					<ItemStyle Font-Size="Small" Font-Names="Verdana" backcolor=white Wrap="False"></ItemStyle>
					<HeaderStyle BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor=Gainsboro></FooterStyle>					
                            <Columns>  
                                <asp:BoundColumn DataField="volunteerId" headerText="Volunteer Id" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="memberId" headerText="Member Id" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="FirstName" headerText="FirstName" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="LastName" headerText="Last Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="Selection" headerText="Role" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="EventCode" headerText="Event" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="chapterCode" headerText="Chapter" ReadOnly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="ZoneCode" headerText="Zone" ReadOnly=true Visible="true" ></asp:BoundColumn>                                
                                <asp:BoundColumn DataField="ClusterCode" headerText="Cluster" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                <asp:BoundColumn DataField="TeamLead" headerText="TeamLead" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                <asp:BoundColumn DataField="National" headerText="National" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                <asp:BoundColumn DataField="ProductGroup" headerText="Product Group" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                <asp:BoundColumn DataField="Product" headerText="Product" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                <asp:BoundColumn DataField="CreateDate" headerText="CreateDate" ReadOnly=true Visible="true" ></asp:BoundColumn>                                                              
                                
                            </Columns>
            </asp:DataGrid>
            <asp:Button ID="btnGrdVolRolesClose" runat="server"  Text="Close" onclick="btnGrdVolRolesClose_onclick"/>										
						   
            <asp:Label ID="lblGrdVolRolesMsg" Visible=false runat=server></asp:Label>
            </td></tr></table>
            </asp:Panel>					
            </td></tr>
			<tr><td>
			<asp:Panel ID="pAddRole" runat=server>
            
             <table  border=1 runat="server" id="tblAddRole"  width=50% visible=false bgcolor=silver >				
				<TBODY>
				<tr><td align="left" colspan="4" bgcolor=white style="height: 2px" >
					<strong><asp:Label ID="lblAddRolesHdg" Visible=true runat=server></asp:Label></strong>						
					</td></tr>		
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;First Name:
						</td>
						<td >
						<asp:label ID="lblARFirst" runat="server"></asp:label>
						<asp:Label Visible=false ID="lblARMemberId" runat=server></asp:Label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Last Name:
						</td>
						<td >
						<asp:label ID="lblARLast" runat="server"></asp:label>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Zone:
						</td>
						<td >
						<asp:label ID="lblARZone" runat="server"></asp:label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Cluster:
						</td>
						<td >
						<asp:label ID="lblARCluster" runat="server"></asp:label>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Chapter:
						</td>
						<td >
						<asp:label ID="lblARChapter" runat="server"></asp:label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;National:
						</td>
						<td >
						<asp:label ID="lblARNational" runat="server"></asp:label>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Finals:
						</td>
						<td >
						<asp:label ID="lblARFinals" runat="server"></asp:label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;IndiaChapter:
						</td>
						<td >
						<asp:label ID="lblARIndiaChapter" runat="server"></asp:label>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Role:
						</td>
						<td >
						<asp:dropdownList ID="ddlARRole" visible=false datatextfield="selection" DataValueField="roleid" runat=server autopostback=true OnSelectedIndexChanged="ddlARRole_selectedIndexChanged">
						<asp:ListItem Value="0" Text="Select a Role"></asp:ListItem></asp:dropdownList>
						<asp:label ID="lblARRole" visible=false runat="server"></asp:label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Event:
						</td>
						<td >
						<asp:Label ID="lblAREvent" runat=server></asp:Label>
						<asp:dropDownList ID="ddlAREvent"  DataTextField="eventCode" DataValueField="eventId" autopostback=true  runat="server" OnSelectedIndexChanged="ddlAREvent_selectedIndexChanged">
						<asp:ListItem Value="0" Text="Select a Event"></asp:ListItem></asp:dropdownlist>
						</td>
				</tr>
					<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;TeamLead:
						</td>
						<td >
						<asp:dropdownList ID="ddlARTeamLead" runat=server>
						<asp:ListItem Value='Y' Text="Yes"></asp:ListItem>
                        <asp:ListItem Value='N' Text="No" Selected=true></asp:ListItem>
						</asp:dropdownList>
						<asp:Label ID="lblARTeamLead" Visible=false runat=server></asp:Label>
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;YahooGroup:
						</td>
						<td >
						<asp:TexTBox ID="txtARYahooGroup" runat="server"></asp:TextBox>
						</td>
				</tr>
				<tr  id="rowEventYear"><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Event Year:
						</td>
						<td >
						<asp:dropdownList ID="ddlAREventYear" runat=server>
						<asp:ListItem Value="2007" Text="2007" selected=true></asp:ListItem>
						</asp:dropdownList>						
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">                           
						</td>
						<td >						
						</td>
				</tr>
				<tr  id="rowProduct"><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Product Group:
						</td>
						<td >
						<asp:dropdownList ID="ddlARProdGroup" runat=server autopostback=true  OnSelectedIndexChanged="ddlARProdGroup_selectedIndexChanged">
						</asp:dropdownList>
						
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Product:
						</td>
						<td >
						<asp:dropdownList ID="ddlARProd" runat=server>
						</asp:dropdownList>
						</td>
				</tr>
				<tr  id="rowAdmin"><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;WriteAccess:
						</td>
						<td >
						<asp:dropdownList ID="ddlARWriteAccess" runat=server>
						<asp:ListItem Value='Y' Text="Yes"></asp:ListItem>
                        <asp:ListItem Value='N' Text="No" Selected=true></asp:ListItem>
						</asp:dropdownList>						
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;AgentFlag:
						</td>
						<td >
						<asp:dropdownList ID="ddlARAgentFlag" runat=server>
						<asp:ListItem Value='Y' Text="Yes"></asp:ListItem>
                        <asp:ListItem Value='N' Text="No" selected=true></asp:ListItem>
						</asp:dropdownList>
						</td>
				</tr>
				<tr  id="rowAdmin2"><td class="ItemLabel" vAlign="top" noWrap align="right">
                            &nbsp;Authorization:
						</td>
						<td >
						<asp:dropdownList ID="ddlARAuthorization" runat=server>
						<asp:ListItem Value='Y' Text="Yes"></asp:ListItem>
                        <asp:ListItem Value='N' Text="No" selected=true></asp:ListItem>
						</asp:dropdownList>						
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">                           
						</td>
						<td >						
						</td>
				</tr>
					<tr><td class="ItemLabel" vAlign="top" noWrap align="right">
                           
						</td>
						<td >
						
						</td>
						<td class="ItemLabel" vAlign="top" noWrap align="right">
                          
						</td>
						<td >
						<asp:button ID="btnAddRole" runat=server Text="Add Role" onclick="btnAddRole_onClick"/>
						&nbsp;&nbsp;<asp:button ID="btnCancel" runat=server Text="Cancel" onclick="btnCancel_onClick"/>
						</td>
				</tr>
				<tr><td colspan="4">
				<asp:Label ID="lblAddRoleError" Visible=false  ForeColor="red"  runat=server></asp:Label>
				</td></tr>
				</table>
				</asp:Panel>
				</td></tr>
			</table>    
                               
           
			
		
</asp:Content>






 

 
 