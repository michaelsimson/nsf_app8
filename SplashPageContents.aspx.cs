﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
 
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;

public partial class SplashPageContents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["SplashPage"] = "False";
    }
    public class CoachingContentsCalss
    {
        public string ContentID { get; set; }
        public string ContentType { get; set; }
        public string ContentText { get; set; }
        public string Year { get; set; }
        public string EventID { get; set; }

    }

    [WebMethod]
    public static List<CoachingContentsCalss> GetSplashPageContents(int ContentID)
    {

        int retVal = -1;
        List<CoachingContentsCalss> objListSurvey = new List<CoachingContentsCalss>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string ContentType = "";


            if (HttpContext.Current.Session["entryToken"] != null)
            {
                if (HttpContext.Current.Session["entryToken"].ToString().Equals("Volunteer"))
                {
                    ContentType = "Coaches";
                }
                else if (HttpContext.Current.Session["entryToken"].ToString().Equals("Parent") || HttpContext.Current.Session["entryToken"].ToString().Equals("Student"))
                {
                    ContentType = "Parents/Students";
                }
            }
            string cmdText = "select cc.ContentID,E.EventID,E.name as EventName, CC.EventYear, CC.ContentType, CC.ContentText from SplashPageContents CC inner join Event E on (CC.EventID=E.EventID) where CC.EventYear in (select max(EventYear) from EventFees where EventID=13) and CC.ContentType='" + ContentType + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachingContentsCalss sur = new CoachingContentsCalss();
                        sur.Year = dr["EventYear"].ToString();
                        sur.EventID = dr["EventID"].ToString();
                        sur.ContentType = dr["ContentType"].ToString();
                        sur.ContentText = dr["ContentText"].ToString();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }
    
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (Session["entryToken"].ToString().Equals("Volunteer"))
        {
            Response.Redirect("VolunteerFunctions.aspx");
        }
        else if (Session["entryToken"].ToString().Equals("Parent") || Session["entryToken"].ToString().Equals("Student"))
        {
            Response.Redirect("UserFunctions.aspx");
        }
      
        
    }
}