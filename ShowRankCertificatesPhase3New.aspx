﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ShowRankCertificatesPhase3New.aspx.vb" Inherits="ShowRankCertificatesPhase3New" title="Untitled Page" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
  <asp:HyperLink runat="server" Text="Back to ScoreSheet" ID="hlnScoreSheet" NavigateUrl="~/ManageScoresheet.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<body>
<div class="Section1">
            <asp:Repeater runat="server" ID="rptCertificate">
                <ItemTemplate>
             <div class="Section1" style="page-break-before:always">     
               <table cellspacing="0" cellpadding="0" width="90%"  align="center" border="0" >                
                    <tr>
                  <td colspan="8" width="100%">
                            <table cellspacing="8" cellpadding="2" width="100%"  align="center" border="0">
                  <tr>
                  <td rowspan="3" align="left" width="20%">
                  
                  <asp:Image runat="server" ID="imgThinkingMan" ImageUrl="http://www.northsouth.org/app8/Images/nsfw.jpg"/>                            
                  </td>
                  
                  <td  rowspan="5" align="left" width="80%">
                  <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg1B.jpg"/><br />
                     <% If Session("SelChapterID") = 1 Then%>
                            <asp:Image runat="server" ID="imgTitleNational"  ImageUrl="http://www.northsouth.org/app8/Images/image007_National.gif"/>
                        <%else %>
                            <asp:Image runat="server" ID="imgTitle" ImageUrl="http://www.northsouth.org/app8/Images/CertImg2C.jpg"/>
                            <%end if %>
                  </td>
                  </tr>

                  </table>
                      
                  </td>
                  </tr>       
                      
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle" ForeColor="#0033cc" Text="Certificate of Excellence" Font-Names="Script MT Bold" Font-Bold="true" Font-Size="36"></asp:Label>
                        </td>
                    </tr>  
                          
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="Label1" ForeColor="#0033cc" Text="awarded to" Font-Names="Script MT Bold" Font-Bold="true" Font-Size="26"></asp:Label>
                        </td>
                    </tr>
                 
                 
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" >
                            <table width="100%">
                        <tr>
                        <td align="center">
                            <asp:Label runat="server" ID="Label2"  ForeColor="#b04e28" Font-Names="Arial" Font-Italic="true" Font-Bold="true" Font-Size="22" Text='<%# DataBinder.Eval(Container,"DataItem.Participant_Name") %>'></asp:Label>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="ItemCenter" colspan="8" align="left" style="text-align:justify;">
                <asp:Label runat="server" ID="lblcomm" Font-Names="Comic Sans MS" Font-Size="14"  Font-Bold="true">for achieving 
                            <asp:Label runat="server" ID="ldlRank"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Rank_Alpha") %>'></asp:Label>
                             rank in 
                           <%-- <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.contest_year") %>'></asp:Label>--%>
                            
                             <% If Session("SelChapterID") = 1 Then%>
                            <%--National--%> the <asp:Label ID="lblYearNat" runat="server" Text ='<%# Now.Year() %>'></asp:Label> <asp:Label runat="server" ID="Label5"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.name") %>'></asp:Label> held on  <asp:Label runat="server" ID="Label3"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ContestDate") %>'></asp:Label> at the 
                            <%Else%>
                            
                            the <asp:Label ID="lblYearReg" runat="server" Text ='<%# Now.Year() %>' ></asp:Label> Regional <asp:Label runat="server" ID="lblProduct" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.name") %>'/> held at the
                            <% end if %>
                            
                            <% If Session("SelChapterID") = 1 Then%>
                               <asp:Label runat="server" ID="Label4" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label>.
                                 <%Else%>
                                 <asp:Label runat="server" ID="lblLocation" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label> Chapter.
                                <% end if %>  
                             </asp:Label>
                        </td>
                    </tr>
                   
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                              <br />                     
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            
                           <asp:Label ID="Label6" runat="server" Font-Names="Arial Narrow" Font-Italic="true" Font-Size="12">North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.  </asp:Label>
                        
                        </td>
                    </tr>
                   
                    
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                                  <br />                       
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" >
                            <%-- <table cellspacing="0" cellpadding="0" width="98%"  align="left" border="0" >                
                                <tr >
                                    <td colspan="3" align="left" >
                                      <% If Session("SelChapterID") = 1 Then%>
                                         <img name="leftsign1"  runat="server" id="leftsign1" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.LeftSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.LeftSignatureImagePath")) %>' alt="LeftSign" width="150" height="60" />
                                     <%Else%>
                                     <img name="leftsign2" id="leftsign2" runat="server" src='<%# GetLeftSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%end if%>
                                        <hr  />
                                    </td> 
                                
                                    <td colspan="2" align="center">
                                        &nbsp;
                                    </td> 
                                
                                    <td colspan="3" align="center">
                                     <% If Session("SelChapterID") = 1 Then%>
                                      <img name="rightsign" id="rightsing1" runat="server" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.RightSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.RightSignatureImagePath")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%Else%>
                                     <img name="rightsing2" id="rightsing2" runat="server" src='<%# GetRightSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="RightSign" width="150" height="60"/>
                                     <%end if%>
                                        <hr />
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="3" align="left">                                        
                                        <asp:Label runat="server" ID="lblLeftSignature" Font-Names="Lucida Calligraphy" Font-Size="14" Font-Bold="true" Text='<%# GetLeftSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode")) %>'></asp:Label>                                                                                
                                    </td>
                                    <td colspan="2" align="center" >
                                        <asp:Label runat="server" ID="lblFooter" ForeColor="brown"  Text="www.northsouth.org" Font-Bold="true" Font-Size="12" ></asp:Label>
                                    </td>
                                    <td  colspan="3" align="left">
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="14"  Font-Names="Lucida Calligraphy" Font-Bold="true" Text='<%# GetRightSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode"))%>'></asp:Label>&nbsp;
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="3" align="left"><asp:Label runat="server" Font-Size="12" Font-Names="Lucida Calligraphy"  ID="lblSigTitle" Text='<%# GetLeftSignatureTitle (DataBinder.Eval(Container,"DataItem.ProductCode"))  %>'></asp:Label></td>
                                    <td colspan="2" align="left">&nbsp;</td>
                                    <td colspan="3" align="left"><asp:Label runat="server" Font-Size="12" ID="lblRightSigTitle" Font-Names="Lucida Calligraphy"  Text='<%# GetRightSignatureTitle(DataBinder.Eval(Container,"DataItem.ProductCode")) %>'></asp:Label></td>
                                </tr>
                            </table>--%>
                                      <table cellspacing="0" cellpadding="0" width="98%" border="0" >                
                                <tr >
                                <td width="40%"></td>
                                
                                    <td rowspan="7" width="30%" align="center">
                        <br />
                                <asp:Image runat="server" ID="Image1" ImageUrl="http://www.northsouth.org/app8/Images/CertImg3C.jpg" />
                   

                                    </td> 
                                
                                     <td width="30%"></td>
                                </tr>
             
                                     <tr >
                                    <td align="left" >
           <% If Session("SelChapterID") = 1 Then%>
                                         <img name="leftsign1"  runat="server" id="leftsign1" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.LeftSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.LeftSignatureImagePath")) %>' alt="LeftSign" width="150" height="60" />
                                     <%Else%>
                                     <img name="leftsign2" id="leftsign2" runat="server" src='<%# GetLeftSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%end if%><br />
                                     <asp:Image runat="server" ID="Image2"  ImageUrl="http://www.northsouth.org/app8/Images/SignlineA.jpg" />
                                    </td> 
                                
                             
                                
                                    <td align="left">
    <% If Session("SelChapterID") = 1 Then%>
                                      <img name="rightsign" id="rightsing1" runat="server" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.RightSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.RightSignatureImagePath")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%Else%>
                                     <img name="rightsing2" id="rightsing2" runat="server" src='<%# GetRightSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="RightSign" width="150" height="60"/>
                                     <%end if%><br />
                                      <asp:Image runat="server" ID="Image3"  ImageUrl="http://www.northsouth.org/app8/Images/SignlineA.jpg"/>
                                    </td> 
                                </tr>
                     
                          <%-- <tr>
                                    <td ><br /></td>
                                    <td ><br /></td>
                                </tr>--%>
                               <tr>
                                    <td align="left"  valign="top"> 
                                   <asp:Label runat="server" ID="lblLeftSignature" Font-Names="Comic Sans MS"   Font-Size="14" Font-Bold="true" Text='<%# GetLeftSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode")) %>'></asp:Label>    

       
                                    </td>
                          
                                    <td align="left" valign="top" >

                         <asp:Label runat="server" ID="lblRightTitle" Font-Names="Comic Sans MS"   Font-Size="14" Font-Bold="true" Text='<%# GetRightSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode"))%>'></asp:Label>
                                        </td>
                               
                                </tr> 
                                <tr>
                                    <td align="left"  valign="top" >
                                    <asp:Label runat="server" Font-Names="Comic Sans MS"   Font-Size="14" ID="lblSigTitle" Text='<%# GetLeftSignatureTitle (DataBinder.Eval(Container,"DataItem.ProductCode"))  %>'></asp:Label>

                                    </td>
                                    <td align="left" valign="top" >
                                   <asp:Label runat="server" ID="lblRightSigTitle" Font-Names="Comic Sans MS"   Font-Size="14"  Text='<%# GetRightSignatureTitle(DataBinder.Eval(Container,"DataItem.ProductCode")) %>'></asp:Label>

                                    </td>
                                </tr>
                                
                                
                            </table>
                        </td> 
                    </tr>                   
		       </table> 
		       </div> 
		       </ItemTemplate>	   
		       </asp:Repeater>
		
		<asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" class="tblMain" cellpadding="0" width="100%"  align="left" border="0" >
                <tr >
                    <td class="Heading" colspan="4">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
    </div>
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="HyperLink1" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
 </body>
</html>


 
 
 
</asp:Content>

