﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

'usp_GetContestantsforScoreShee is used in this page
Partial Class ManageScoresheet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        lbldwError.Text = ""
        lblErr.Text = ""
        LblMasterErr.Text = ""
     
        If Not IsPostBack() Then
            If Session("RoleID") = 9 Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from Volunteer where RoleId=9 and [National]='Y' and MemberID = " & Session("LoginID")) > 0 Then
                    ExamRecNational.Value = "Y"
                Else
                    ExamRecNational.Value = "N"
                End If
            Else
                ExamRecNational.Value = "N"
            End If
            If ExamRecNational.Value = "N" And Not Request.QueryString("id") Is Nothing Then
                Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select V.ChapterID, Ch.ChapterCode from Volunteer V inner Join Chapter Ch ON V.ChapterID= Ch.ChapterID  where  V. MemberID = " & Session("LoginID") & " AND V.VolunteerId =" & Request.QueryString("id") & "")
                While Reader.Read()
                    lblChapter.Text = Reader("ChapterCode")
                    lblChapter.Visible = True
                    hdnChapterID.Value = " AND C.NSFChapterID = " & Reader("ChapterID")
                End While
            End If

            ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
            ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
            ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
            ddlYear.Items(1).Selected = True

            ddlMYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
            ddlMYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
            ddlMYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
            ddlMYear.Items(1).Selected = True
            Try
                getevent()
            Catch ex As Exception
                'Response.write(ex.ToString())
            End Try
        End If
    End Sub
    Private Sub getMPhase()
        ddlMPhase.Items.Clear()
        If ddlMContest.SelectedValue = 1 Then
            ddlMPhase.Items.Insert(0, New ListItem("Global", "0"))
            'ddlMPhase.Items.Insert(1, New ListItem("Phase 1", "1"))
            ddlMPhase.Items.Insert(1, New ListItem("Phase 2", "2"))
            ddlMPhase.Items.Insert(2, New ListItem("Phase 3", "3"))
            ddlMPhase.Items.Insert(3, New ListItem("p3List", "4"))
            ddlMPhase.Items.Insert(4, New ListItem("Toppers", "5"))
        Else
            ddlMPhase.Items.Insert(0, New ListItem("Global", "0"))
            'ddlMPhase.Items.Insert(1, New ListItem("Phase 1", "1"))
            ddlMPhase.Items.Insert(1, New ListItem("Phase 2", "2"))
        End If
    End Sub

    Private Sub getevent()
        Dim strSQl As String
        If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
            strSQl = "   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & hdnChapterID.Value '" and Cn.BadgeNumber is Not NUll" &
        ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
            loadMproductGroup()
            strSQl = "   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll"
        End If
        Dim Evntcnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(Distinct C.EventId) " & strSQl)
        If Evntcnt = 1 Then
            Dim eventid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select Distinct C.EventId " & strSQl)
            ddlContest.SelectedIndex = ddlContest.Items.IndexOf(ddlContest.Items.FindByValue(eventid))
            ddlContest.Enabled = False
        ElseIf Evntcnt = 0 And (Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N")) Then
            ddlContest.Enabled = False
            lbldwError.Text = "You were not yet scheduled for any contest."
            Exit Sub
        End If

        LoadContests()
    End Sub

    Private Sub LoadContests()
        lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
        Dim strSQl As String
        If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
            ddlYear.Enabled = False
            strSQl = "select  Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll  and C.EventId = " & ddlContest.SelectedValue & hdnChapterID.Value & " order by Ch.State,Ch.Name"
        ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
            tblMaster.Visible = True
            strSQl = "select Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.EventId = " & ddlContest.SelectedValue & " order by Ch.State,Ch.Name"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)

        If ds.Tables(0).Rows.Count = 1 Then
            ddlChapter.DataSource = ds
            ddlChapter.DataBind()
            ddlChapter.Enabled = False
            BtnDownload.Enabled = True
            loadproductGroup()
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            BtnDownload.Enabled = True
            ddlChapter.Enabled = True
            'ddlChapter.DataTextField = "ChapterCode"
            'ddlChapter.DataValueField = "ChapterID"
            Dim li As ListItem
            If Session("RoleID") = 1 Or Session("RoleID") = 2 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    li = New ListItem()
                    li.Text = ds.Tables(0).Rows(i)(1).ToString()
                    li.Value = ds.Tables(0).Rows(i)(0).ToString()
                    ddlChapter.Items.Add(li)
                Next i
                ddlChapter.Items.Insert(0, New ListItem("Select", "0"))
                ddlChapter.SelectedIndex = 0
            Else
                ddlChapter.DataSource = ds
                ddlChapter.DataBind()
            End If
            loadproductGroup()
        Else
            BtnDownload.Enabled = False
            lbldwError.Text = "No badge numbers are available"
        End If
    End Sub

    Protected Sub ddlContest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContest.SelectedIndexChanged
        LoadContests()
        Dim i As Integer
        If ddlContest.SelectedValue = 1 Then
            For i = 4 To 7
                ddlPhase.Items(i).Enabled = True
            Next
            ddlTypeofData1.Items(5).Enabled = True
            ddlTypeofData1.Items(6).Enabled = True
        ElseIf ddlContest.SelectedValue = 2 Then
            For i = 4 To 7
                ddlPhase.Items(i).Enabled = False
            Next
            ddlTypeofData1.Items(5).Enabled = False
            ddlTypeofData1.Items(6).Enabled = False
        End If
    End Sub

    Protected Sub ddlMContest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadMproductGroup()
    End Sub

    Private Sub loadproductGroup()
        lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
        Dim strSQl As String
        Dim ds As DataSet
        If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
            strSQl = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join ProductGroup P On C.ProductGroupId = p.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & hdnChapterID.Value & " Order BY P.ProductGroupID "
            ddlYear.Enabled = False
        ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
            strSQl = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join ProductGroup P On C.ProductGroupId = p.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & " Order BY P.ProductGroupID "
        End If

        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        If ds.Tables(0).Rows.Count = 1 Then
            ddlProductGroup.DataSource = ds
            ddlProductGroup.DataBind()
            LoadProduct()
            ddlProductGroup.Enabled = False
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            ddlProductGroup.DataSource = ds
            ddlProductGroup.DataBind()
            ddlProductGroup.Enabled = True
            LoadProduct()
        End If
    End Sub

    Private Sub loadMproductGroup()
        LblMasterErr.Text = ""
        Dim strSQl As String
        Dim ds As DataSet
        If Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
            strSQl = "select Distinct ProductGroupCode,Name,ProductGroupID  from ProductGroup WHERE EventID=" & ddlMContest.SelectedValue & " Order BY ProductGroupID" 'C.examrecid=" & Session("loginid") & " and
            tblMaster.Visible = True
        End If
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlMProductGroup.DataSource = ds
            ddlMProductGroup.DataBind()
            ddlMProductGroup.Enabled = True
            LoadMProduct()
        Else
            ddlMProductGroup.DataSource = Nothing
            ddlMProductGroup.DataBind()
            ddlMProduct.DataSource = Nothing
            ddlMProduct.DataBind()

        End If
    End Sub

    Private Sub LoadProduct()
        lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
        Dim strSQl As String
        If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
            strSQl = "select  Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "'  And P.EventID = " & ddlContest.SelectedValue & hdnChapterID.Value & " Order By P.ProductId "
            ddlYear.Enabled = False
        ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
            strSQl = "select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' AND P.EventID=" & ddlContest.SelectedValue & " Order By P.ProductId"
        Else
            lbldwError.Text = "Sorry, You have no access"
            Exit Sub
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        If ds.Tables(0).Rows.Count = 1 Then
            ddlProduct.DataSource = ds
            ddlProduct.DataBind()
            checkscorehsheetdown()
            ddlProduct.Enabled = False
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            ddlProduct.Enabled = True
            ddlProduct.DataSource = ds
            ddlProduct.DataBind()
            checkscorehsheetdown()
        End If
    End Sub

    Private Sub LoadMProduct()
        LblMasterErr.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct ProductCode,Name   from product where Eventid in (" & ddlMContest.SelectedValue & ") and Status='O' and ProductGroupCode='" & ddlMProductGroup.SelectedItem.Value & "'")
        If (ddlMProductGroup.SelectedValue = "SB" Or ddlMProductGroup.SelectedValue = "VB" Or ddlMProductGroup.SelectedValue = "GB") Then
            ddlMPhase.SelectedIndex = ddlMPhase.Items.IndexOf(ddlMPhase.Items.FindByValue("1"))
            ddlMPhase.Enabled = True
        Else
            ddlMPhase.SelectedIndex = ddlMPhase.Items.IndexOf(ddlMPhase.Items.FindByValue("1"))
            ddlMPhase.Enabled = False
        End If
        getMPhase()
        If ds.Tables(0).Rows.Count = 1 Then
            ddlMProduct.DataSource = ds
            ddlMProduct.DataBind()
            ddlMProduct.Enabled = False
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            ddlMProduct.Enabled = True
            ddlMProduct.DataSource = ds
            ddlMProduct.DataBind()
        Else
            ddlMProduct.Items.Clear()
            ddlMProduct.Enabled = False
        End If
    End Sub
    Protected Sub BtnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDownload.Click
        If ddlProduct.Items.Count > 0 Then
            lblUploadCondn.Text = ""
            filldata()
        Else
            lbldwError.Text = "You have no valid scoresheet to download"
        End If
    End Sub

    Private Sub filldata()
        lbldwError.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheet", New SqlParameter("@ContestID", ddlProduct.SelectedValue))
        If ds.Tables(0).Rows.Count > 0 Then
            Dim SrcFilename, FileName, ProductCode, ChapterCode As String
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
            Dim SQLInsertPhase1 As String = ""
            Dim SQLScoreSheetLog As String = ""
            Dim SQLScoreSheetLogVal As String = ""

            Dim PhaseDFlag As String = ""
            Dim PhaseDValue As String = ""
            While readr.Read()
                SrcFilename = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet" '"NSFSS" & Now.Year.ToString() & "_" & readr("ProductCode") & ".xls"
                FileName = SrcFilename & "_" & readr("Chaptercode")
                If ddlPhase.SelectedValue = 2 Or ddlPhase.SelectedValue = 3 Then
                    SrcFilename = SrcFilename & "_p2"
                    FileName = FileName & "_p2"
                    If Not ddlRoom.SelectedValue = "ALL" Then
                        FileName = FileName & "_" & ddlRoom.SelectedValue
                    End If
                ElseIf ddlPhase.SelectedValue = 4 Then
                    FileName = FileName & "_p12"
                End If
                ProductCode = readr("ProductCode")
                ChapterCode = readr("Chaptercode")
            End While
            readr.Close()

            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFilename & ".xls")) '-- if the file exists on the server
            If Not file.Exists Then
                lbldwError.Text = "Sorry Master file doesnot exist"
                Exit Sub
            End If
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\Master\" & SrcFilename & ".xls"))
            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)
            Dim x As Integer
            Dim i As Integer = 7
            Dim str As IRange = Sheet1.Range(5, 28)
            Dim PWD As String = str.Value.ToString()
            Dim InsertFlag As Boolean = False
            Dim Score1, score2, score3, rank, PhaseValue, Attendance As Integer

            If Sheet1.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                lbldwError.Text = "Level Mismatch in Sheet"
                Exit Sub
            End If

            Score1 = Sheet1.Range(6, 28).Value
            score2 = Sheet1.Range(7, 28).Value
            rank = Sheet1.Range(9, 28).Value
            Attendance = Sheet1.Range(11, 28).Value
            Dim StartCount, EndCount, MaxIndexPhase1, MaxIndexPhase2, MaxIndexPhase3 As Integer
            Dim Phase1_TB(10), Ph2_Round(20), Ph3_Round(30), index

            StartCount = Sheet1.Range(12, 28).Value
            EndCount = Sheet1.Range(12, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Phase1_TB(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase1 = index
                Next
            End If

            StartCount = Sheet1.Range(13, 28).Value
            EndCount = Sheet1.Range(13, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Ph2_Round(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase2 = index
                Next
            End If

            SQLScoreSheetLog = "Insert into ScoreSheetLog (ContestID,ContestYear,FileName,EventID,Event,ChapterID,Chapter,ProductGroupID,ProductID,ProductGroupCode ,ProductCode,PhaseID,Phase,CreatedBy,CreatedDate,"
            SQLScoreSheetLogVal = ") Values(" & ddlProduct.SelectedValue & "," & ddlYear.SelectedValue & ",'" & FileName & "'," & ddlContest.SelectedValue & ",'" & ddlContest.SelectedItem.Text & "'," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "',(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductGroupCode from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductCode from Contest Where ContestID=" & ddlProduct.SelectedValue & ")," & ddlPhase.SelectedValue & ",'" & ddlPhase.SelectedItem.Text & "'," & Session("LoginID") & ",GETDATE(),"

            If ddlPhase.SelectedValue = "2" Or ddlPhase.SelectedValue = "3" Then
                ''Copy from one sheet
                lbldwError.Text = ""
                Dim NRooms As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Ph2Rooms FROM Contest WHERE ContestId=" & ddlProduct.SelectedValue)
                Dim NContestants As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM Contestant WHERE ContestID =" & ddlProduct.SelectedValue & " AND BadgeNumber IS NOT NULL AND PaymentReference IS NOT NULL ")
                Dim RoomCount, StartRoomCount, EndRoomCount, RoomCountRest As Integer
                Dim MaxValue As Integer
                Dim RoomNumber As Integer
                Dim file2 As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFilename & ".xls")) '"_P2" & '-- if the file exists on the server

                If file2.Exists Then
                    Dim xlWorkBook_Phase2 As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\Master\" & SrcFilename & ".xls")) '"_P2" & 


                    Dim SheetValue As IWorksheet
                    Dim SheetCount As Integer = xlWorkBook_Phase2.Worksheets.Count

                    For rvalue As Integer = NRooms + 1 To SheetCount
                        xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                    Next
                    MaxValue = NRooms
                    For RoomNumberCount As Integer = 1 To NRooms
                        i = 7
                        RoomNumber = RoomNumberCount
                        If NRooms > 1 Then
                            ' To split the contestants equally between the number of rooms present
                            RoomCount = Math.Floor(NContestants / NRooms)
                            RoomCountRest = NContestants Mod NRooms
                            StartRoomCount = (RoomNumber - 1) * RoomCount
                            EndRoomCount = (RoomNumber * RoomCount)

                            If RoomNumber = MaxValue Then
                                If Not RoomCountRest = 0 Then '(RoomCount / 2) 
                                    EndRoomCount = EndRoomCount + RoomCountRest
                                End If
                            End If
                        Else
                            lbldwError.Text = "Room Data Is Not available"
                            StartRoomCount = 0
                            EndRoomCount = ds.Tables(0).Rows.Count
                        End If

                        SheetValue = xlWorkBook_Phase2.Worksheets(RoomNumberCount)
                        If SheetValue.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                            lbldwError.Text = "Level Mismatch in Sheet"
                            Exit Sub
                        End If

                        str = SheetValue.Range(5, 28)
                        PWD = str.Value.ToString()
                        SheetValue.Unprotect(PWD)

                        If Not SheetValue.Range(4, 28).Value = "Y" Then
                            Exit Sub
                        End If
                        SheetValue.Range(4, 29).Value = RoomNumberCount

                        For x = StartRoomCount To EndRoomCount - 1 'ds.Tables(0).Rows.Count - 1
                            If x = StartRoomCount Then
                                SheetValue.Range(2, 1).Value = ds.Tables(0).Rows(x)("center")
                            End If
                            SheetValue.Range(i, 1).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                            SheetValue.Range(i, 2).Value = ds.Tables(0).Rows(x)("ParticipantName")
                            SheetValue.Range(i, 3).Value = ds.Tables(0).Rows(x)("DOB")
                            SheetValue.Range(i, 4).Value = ds.Tables(0).Rows(x)("Grade")
                            'store the contestant id in column AA
                            SheetValue.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")
                            If ddlPhase.SelectedValue = "3" Then
                                SheetValue.Range(i, 5).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag From ScoreDetail Where ContestantID= " & SheetValue.Range(i, 27).Value)
                            End If
                            i = i + 1
                        Next

                        If Not ddlRoom.SelectedValue = "ALL" Then
                            For rvalue As Integer = 1 To NRooms
                                If Not ddlRoom.SelectedValue = rvalue Then
                                    xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                                End If
                            Next
                        End If

                        'For SSB
                        str = SheetValue.Cells(10, 28)
                        Dim Phase2printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString  '
                        SheetValue.PageSetup.PrintArea = Phase2printarea
                        SheetValue.Protect(PWD)
                    Next

                    ''Flag set during Phase2 download
                    PhaseDFlag = "P2DList,RoomNumberD,RoomAllD"
                    PhaseDValue = "'Y'," & IIf(ddlRoom.SelectedValue = "ALL", -1, ddlRoom.SelectedValue) & "," & IIf(ddlRoom.SelectedValue = "ALL", "'Y'", "NULL")

                    SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag
                    SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                    Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)

                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & ".xls") '_P2
                    xlWorkBook_Phase2.SaveAs(Response.OutputStream)
                    Response.End()
                Else
                    lbldwError.Text = "Sorry Phase2 Master file doesnot exist "
                    Exit Sub
                End If
            Else
                Sheet1.Unprotect(PWD)
                Dim wRange As IRange
                If ddlProductGroup.SelectedValue = "MB" Then
                    wRange = Sheet1.Range(7, 18)
                    wRange.ColumnWidth() = 10
                ElseIf ddlProductGroup.SelectedValue = "SC" Then
                    wRange = Sheet1.Range(7, 15)
                    wRange.ColumnWidth() = 10
                End If
                For x = 0 To ds.Tables(0).Rows.Count - 1
                    If x = 0 Then
                        Sheet1.Range(2, 1).Value = ds.Tables(0).Rows(x)("center")
                    End If
                    Sheet1.Range(i, 1).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                    Sheet1.Range(i, 2).Value = ds.Tables(0).Rows(x)("ParticipantName")
                    Sheet1.Range(i, 3).Value = ds.Tables(0).Rows(x)("DOB")
                    Sheet1.Range(i, 4).Value = ds.Tables(0).Rows(x)("Grade")
                    'store the contestant id in column AA
                    Sheet1.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")

                    If ddlPhase.SelectedValue = 1 Then
                        ' Storing data to ScoreDetail Table during Download
                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & Sheet1.Cells(i, 27).Value() & "")

                        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ScoreDetail Where ContestantID=" & Sheet1.Range(i, 27).Value) = 0 Then
                            InsertFlag = True
                            SQLInsertPhase1 = SQLInsertPhase1 + "Insert into ScoreDetail (ContestantID,ContestID,BadgeNumber, DOB,Grade,Createdby,CreatedDate,ChapterID,ContestYear,ProductCode,ProductGroupCode,ProductID,ProductGroupID,ChildNumber,MemberID) Values (" & Sheet1.Range(i, 27).Value & "," & ddlProduct.SelectedValue & ",'" & Sheet1.Range(i, 1).Value & "','" & Sheet1.Range(i, 3).Value & "'," & Sheet1.Range(i, 4).Value & "," & Session("LoginID") & ",GETDATE()," & ddlChapter.SelectedValue & "," & ddlYear.SelectedValue & ",'" & getProductcode(ddlProduct) & "','" & ddlProductGroup.SelectedValue & "'," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ")"
                        End If
                    ElseIf ddlPhase.SelectedValue = 4 Then
                        Sheet1.Range(i, Attendance).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                        Sheet1.Range(i, Score1).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                        For index = 1 To MaxIndexPhase1 - 1
                            Sheet1.Range(i, Phase1_TB(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1_TB" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Phase1_TB" & index & " Is Not Null")
                        Next
                        For index = 1 To MaxIndexPhase2 - 1
                            Sheet1.Range(i, Ph2_Round(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Ph2_Round" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Ph2_Round" & index & " Is Not Null")
                        Next
                    End If
                    i = i + 1
                Next
                'For SSB
                str = Sheet1.Cells(10, 28)
                Dim printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString()
                Sheet1.PageSetup.PrintArea = printarea
                Sheet1.Range(7, 31).Value = ""
                Sheet1.Protect(PWD)
                Try
                    lbldwError.Text = "Downloaded Successfully"

                    'Stroing of data into ScoreDetail Table
                    If InsertFlag = True Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLInsertPhase1)
                    End If

                    If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) from scoresheet where contestId=" & ddlProduct.SelectedValue) = 0 Then ' "FileName='" & FileName & ".xls" & "'"
                        Dim SQLstr As String = "Insert into ScoreSheet(ProductCode, ChapterCode, ContestID, FileName, DownloadedBy, downloadedDate) VALUES ('" ',ChapterID,EventID,ContestYear)
                        SQLstr = SQLstr & getProductcode(ddlProduct) & "','" & ddlChapter.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & FileName & ".xls" & "'," & Session("LoginID") & ",Getdate())"  ' ," & ddlChapter.SelectedValue & "," & ddlContest.SelectedValue & "," & ddlYear.SelectedValue & "')"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLstr)
                        checkscorehsheetdown()
                    End If

                    If ddlPhase.SelectedValue = "1" Then
                        PhaseDFlag = "P1DList"
                    ElseIf ddlPhase.SelectedValue = "4" Then
                        PhaseDFlag = "P12Dlist"
                    End If
                    PhaseDValue = "'Y'"

                    SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag & ""
                    SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                    Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)

                    'Stream workbook 
                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & ".xls")
                    xlWorkBook.SaveAs(Response.OutputStream)
                    Response.End()

                Catch ex As Exception
                    'Response.Write(ex.ToString()) 
                End Try
            End If 'Phases Loop Ending
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If FileUpload.HasFile Then
            If Not System.IO.Path.GetExtension(FileUpload.FileName).ToLower = ".xls" Then
                lblErr.Text = "Only Excel file with .xls is allowed."
                Exit Sub
            End If
            Dim ContestantIDValue As String = "0"
            Dim srcfilename As String = FileUpload.FileName
            Dim checkfile As String() = srcfilename.Split("_")
            Dim Length As Integer = checkfile.Length
            If checkfile(0).Trim() <> ddlYear.SelectedValue Then
                lbldwError.Text = "Invalid Year Selection"
                Exit Sub
            ElseIf checkfile(1).Trim() <> IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") Then
                lbldwError.Text = "Invalid Event Selection"
                Exit Sub
            ElseIf checkfile(2).Trim() <> ddlProductGroup.SelectedValue Then
                lbldwError.Text = "Invalid Contest Selection"
                Exit Sub
            ElseIf checkfile(3).Trim() <> getProductcode(ddlProduct) Then
                lbldwError.Text = "Invalid Level Selection"
                Exit Sub
            ElseIf checkfile(4).Trim() <> "ScoreSheet" Then
                lbldwError.Text = "ScoreSheet missing in the File name"
                Exit Sub
            ElseIf (checkfile(5).Trim() & "," & checkfile(6).Trim().Substring(0, 2) <> ddlChapter.SelectedItem.Text.Replace(" ", "")) And (Not ddlContest.SelectedValue = 1) Then
                lbldwError.Text = "Invalid Chapter name"
                Exit Sub
            End If
            FileUpload.PostedFile.SaveAs(Server.MapPath("ScoreSheets/" & FileUpload.FileName))
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\" & srcfilename))
            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)
            Dim x As Integer
            Dim i As Integer = 7
            Dim str As IRange = Sheet1.Range(5, 28)
            Dim PWD As String = str.Value.ToString()
            Sheet1.Unprotect(PWD)
            Dim chapterCode As String = Sheet1.Range(2, 1).Value.ToString().Replace(" ", "") '& ".xls"
            ''Checking with Sheet Data
            If ((checkfile(5).Trim() & "," & checkfile(6).Trim().Substring(0, 2)) <> chapterCode) Then 'And (ddlContest.SelectedValue = 1)) '.Substring(0, (checkfile(5).Length - 4)
                lbldwError.Text = "Invalid Chapter Selection"
                Exit Sub
            ElseIf Sheet1.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                lbldwError.Text = "Level mismatch in Excel Data"
                Exit Sub
            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from contestant where ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "") = 0 Then
                lbldwError.Text = "No Contestant Data found in server"
                Exit Sub
            End If

            If (ddlTypeofData1.SelectedValue = "0" Or ddlTypeofData1.SelectedValue = 1) And Length > 7 Then
                lbldwError.Text = "File Name Mismatch"
                Exit Sub
            ElseIf ddlTypeofData1.SelectedValue = "2" Then
                If Length > 7 Then
                    If checkfile(7).Trim().Substring(0, 2) <> "p" & ddlTypeofData1.SelectedValue Then
                        lbldwError.Text = " Missing P" & ddlTypeofData1.SelectedValue & " in the filename "
                        Exit Sub
                    End If
                    If Length > 8 Then
                        If checkfile(8).Trim().Substring(0, 1) <> ddlRoom1.SelectedValue Then
                            lbldwError.Text = "RoomNumber Mismatch in the filename "
                            Exit Sub
                        End If
                    Else
                        If Not xlWorkBook.ActiveSheet.Index = ddlRoom1.SelectedValue Then
                            xlWorkBook.ActiveSheet.EnableSelection = ddlRoom1.SelectedValue
                        End If
                    End If
                Else
                    lbldwError.Text = "File Name Mismatch"
                    Exit Sub
                End If
            ElseIf (ddlTypeofData1.SelectedValue = "3") Then
                If Length > 8 Then
                    lbldwError.Text = "Invalid File"
                    Exit Sub
                ElseIf Length > 7 Then 'Or ddlTypeofData1.SelectedValue = "4") Then
                    If checkfile(7).Trim().Substring(0, 3) <> "p12" Then
                        lbldwError.Text = "File Missing P12 in the File Name"
                        Exit Sub
                    End If
                Else
                    lbldwError.Text = "File Name Mismatch"
                    Exit Sub
                End If
            ElseIf ddlTypeofData1.SelectedValue = "4" Then
                If Length > 8 Then
                    lbldwError.Text = "Invalid File"
                    Exit Sub
                ElseIf Length > 7 Then 'Or ddlTypeofData1.SelectedValue = "4") Then
                    If checkfile(7).Trim().Substring(0, 3) <> "p12" Then
                        lbldwError.Text = "File Missing P12 in the File Name"
                        Exit Sub
                    End If
                End If
            End If

            Dim Score1, score2, score3, rank, PhaseValue, Attendance As Integer
            Dim Phase_TB(10) As Integer
            Dim rank_alpha As Char
            Dim Phase1_TB(10), Ph2_Round(10), Ph3_Round(10) As Integer
            Score1 = Sheet1.Range(6, 28).Value
            score2 = Sheet1.Range(7, 28).Value
            score3 = Sheet1.Range(8, 28).Value
            rank = Sheet1.Range(9, 28).Value
            rank_alpha = Sheet1.Range(10, 28).Value
            Attendance = Sheet1.Range(11, 28).Value

            Dim SQLstr As String = ""
            Dim SQLScoreStr As String = ""
            Dim SQLScoreStrVal As String = ""
            Dim SQLScoreStrExe As String = ""
            Dim SQLScoreUpdateStr As String = ""
            Dim SQLScoreUpdateStrVal As String = ""
            Dim SQLScoreSheetLog As String = ""
            Dim SQLScoreSheetLogVal As String = ""
            Dim PhaseUFlag As String = ""
            Dim PhaseUVal As String = ""

            Dim flagcontestid As String = ""
            Dim printarea As String = Sheet1.PageSetup.PrintArea
            Dim Rowcnt As Integer = Val(printarea.Substring(printarea.LastIndexOf("$") + 1))
            Dim ValueExistFlag As Boolean = False
            Dim ScoreExistFlag As Boolean = False
            Dim StartCount, EndCount, MaxIndexPhase1, MaxIndexPhase2, MaxIndexPhase3 As Integer
            Dim index As Integer
            MaxIndexPhase1 = 1
            MaxIndexPhase2 = 1
            MaxIndexPhase3 = 1

            StartCount = Sheet1.Range(12, 28).Value
            EndCount = Sheet1.Range(12, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Phase1_TB(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase1 = index
                Next
            End If
            StartCount = Sheet1.Range(13, 28).Value
            EndCount = Sheet1.Range(13, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Ph2_Round(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase2 = index
                Next
            End If

            StartCount = Sheet1.Range(14, 28).Value
            EndCount = Sheet1.Range(14, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Ph3_Round(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase3 = index
                Next
            End If
            Dim Ph2Flag As Boolean = False
            If ddlTypeofData1.SelectedValue = "2" Then  'Uploading and Updation of ScoreSheet For  Phase2Scores
                Dim SheetName As IWorksheet
                Dim SheetCount As Integer

                SheetCount = ddlRoom1.SelectedValue
                SheetName = xlWorkBook.Worksheets(SheetCount)

                score2 = SheetName.Range(7, 28).Value
                rank_alpha = SheetName.Range(10, 28).Value

                printarea = SheetName.PageSetup.PrintArea
                Rowcnt = Val(printarea.Substring(printarea.LastIndexOf("$") + 1))
                Dim Ph2cnt As Integer

                For i = 7 To Rowcnt - 1
                    Try
                        If SheetName.Cells(i, 27).Value().ToString().Trim() = "" Then
                            Exit For
                        End If
                        If Not IsNumeric(SheetName.Cells(i, 27).Value()) = True Then
                            lblErr.Text = "This spreadsheet has insufficient data to upload automatically" & SheetName.Cells(i, 27).Value()
                            Exit Sub
                        End If
                        'Check for Badge Number & Grade
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ChildNumber) from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where Ch.FIRST_NAME +' '+ Ch.LAST_NAME ='" & SheetName.Cells(i, 2).Value() & "' AND DateDiff(d,ch.Date_OF_Birth,'" & SheetName.Cells(i, 3).Value() & "')=0 AND C.contestant_id =" & SheetName.Cells(i, 27).Value() & " and C.BadgeNumber='" & SheetName.Cells(i, 1).Value() & "' and C.Grade=" & SheetName.Cells(i, 4).Value() & " AND C.ProductCode = '" & getProductcode(ddlProduct) & "' and C.ChapterID=" & ddlChapter.SelectedItem.Value & "") = 0 Then
                            lblErr.Text = "ScoreSheet was tampered on row# " & i
                            Exit Sub
                        End If

                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & SheetName.Cells(i, 27).Value() & "")
                        SQLScoreStr = SQLScoreStr & " ContestantID ,ContestID, BadgeNumber,DOB,Grade,ChildNumber,MemberID,AttendanceFlag"
                        SQLScoreStrVal = SQLScoreStrVal & "" & SheetName.Cells(i, 27).Value() & "," & ddlProduct.SelectedValue & ",'" & SheetName.Cells(i, 1).Value() & "','" & SheetName.Cells(i, 3).Value() & "'," & SheetName.Cells(i, 4).Value() & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ",'" & SheetName.Cells(i, Attendance).Value() & "'"
                        SQLScoreUpdateStr = SQLScoreUpdateStr & " Update ScoreDetail Set ContestantID = " & SheetName.Cells(i, 27).Value() & ",ContestID =" & ddlProduct.SelectedValue & ",BadgeNumber= '" & SheetName.Cells(i, 1).Value() & "',DOB= '" & SheetName.Cells(i, 3).Value() & "', Grade=" & SheetName.Cells(i, 4).Value() & ",ChildNumber=" & dsChildNm.Tables(0).Rows(0)(0) & ",MemberID=" & dsChildNm.Tables(0).Rows(0)(1) & ",AttendanceFlag='" & SheetName.Cells(i, Attendance).Value() & "'"
                        SQLstr = SQLstr & "Update Contestant set "

                        If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & SheetName.Cells(i, 27).Value() & " and BadgeNumber='" & SheetName.Cells(i, 1).Value() & "' and Grade=" & SheetName.Cells(i, 4).Value() & " AND  Phase1Score is Null " & "") > 0 Then
                                lblErr.Text = lblErr.Text & "No Phase1 Scores Present for the Contestants in Row " & i
                                Ph2Flag = True
                            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*)From ScoreDetail Where ContestantID=" & SheetName.Cells(i, 27).Value() & "") = 0 Then
                                lblErr.Text = "No Contestant Details found for Phase1"
                                Exit Sub
                            End If
                        End If

                        If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                            If Not IsNumeric(SheetName.Cells(i, score2).Value()) = True Then
                                Ph2cnt = 1
                                Ph2Flag = True
                                lblErr.Text = lblErr.Text & "Phase2 TotalScore,"
                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                            Else
                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                SQLScoreStrVal = SQLScoreStrVal & "," & SheetName.Cells(i, score2).Value() & ""
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & SheetName.Cells(i, score2).Value()
                                ScoreExistFlag = True
                            End If
                            For index = 1 To MaxIndexPhase2 - 1
                                If Not IsNumeric(SheetName.Cells(i, Ph2_Round(index)).Value()) = True Then
                                    Ph2cnt = 1
                                    Ph2Flag = True
                                    lblErr.Text = lblErr.Text & "Ph2_Round" & index & ","
                                    SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "0,"
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=0"
                                Else
                                    SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "," & SheetName.Cells(i, Ph2_Round(index)).Value() & "" ' where ContestantID =" & SheetName.Cells(i, 27).Value() & ""
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=" & SheetName.Cells(i, Ph2_Round(index)).Value() & ""
                                    ScoreExistFlag = True
                                End If
                            Next
                            If score2 > 0 Then
                                SQLstr = SQLstr & " Score2=" & CDbl(0 & SheetName.Cells(i, score2).Value()) & ","
                                If CDbl(0 & SheetName.Cells(i, score2).Value()) > 0 Then
                                    ScoreExistFlag = True
                                End If
                            Else
                                SQLstr = SQLstr & "Score2=0 " & ","
                            End If
                            If Ph2cnt = 1 Then
                                lblErr.Text = lblErr.Text.Trim.Trim(",") & " on Row#" & i & "<br>"
                                Ph2cnt = 0
                            End If

                        ElseIf SheetName.Cells(i, Attendance).Value() = "N" Then
                            GoTo EscapValid1
                        End If

EscapValid1:
                        If flagcontestid = "" Then
                            flagcontestid = SheetName.Cells(i, 27).Value()
                        End If

                    Catch ex As Exception
                        lblErr.Text = "Score Sheet Missing Value(s)on Row #" & i
                        'Response.Write(ex.ToString())
                    End Try

                    SQLstr = SQLstr & "ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() Where contestant_id=" & SheetName.Cells(i, 27).Value() & ";"
                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() WHERE ContestantID=" & SheetName.Cells(i, 27).Value() & ";" '" Update ScoreDetail Set " 

                    Try
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & SheetName.Cells(i, 27).Value() & "") > 0 Then
                            SQLScoreStrExe = SQLScoreStrExe & SQLScoreUpdateStr
                        Else
                            SQLScoreStrExe = SQLScoreStrExe & "Insert into ScoreDetail(" & SQLScoreStr & ",Createdby,CreatedDate) Values(" & SQLScoreStrVal & "," & Session("LoginID") & ",GETDATE());"
                        End If
                        SQLScoreStr = ""
                        SQLScoreStrVal = ""

                    Catch ex As Exception
                        'Response.Write(ex.ToString())
                        Exit Sub
                    End Try

                    If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                        ContestantIDValue = ContestantIDValue & "," & SheetName.Cells(i, 27).Value()
                    End If
                Next

                ''Flag set during Phase2 Upload
                PhaseUFlag = "P2UList,RoomNumberU"
                PhaseUVal = "'Y'," & ddlRoom1.SelectedValue & ""

            Else  ' Uploading and Updation of ScoreSheet For Global and Phase1 Scores
                Dim Phasecnt As Integer
                For i = 7 To Rowcnt - 1
                    Try
                        If Sheet1.Cells(i, 27).Value.ToString() = "" Then
                            Exit For
                        End If
                        If Not IsNumeric(Sheet1.Cells(i, 27).Value()) = True Then
                            lblErr.Text = "This spreadsheet has insufficient data to upload automatically" & Sheet1.Cells(i, 27).Value()
                            Exit Sub
                        End If
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ChildNumber) from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where Ch.FIRST_NAME +' '+ Ch.LAST_NAME ='" & Sheet1.Cells(i, 2).Value() & "' AND DateDiff(d,ch.Date_OF_Birth,'" & Sheet1.Cells(i, 3).Value() & "')=0 AND C.contestant_id =" & Sheet1.Cells(i, 27).Value() & " and C.BadgeNumber='" & Sheet1.Cells(i, 1).Value() & "' and C.Grade=" & Sheet1.Cells(i, 4).Value() & " AND C.ProductCode = '" & getProductcode(ddlProduct) & "' and C.ChapterID=" & ddlChapter.SelectedItem.Value & "") = 0 Then
                            lblErr.Text = "ScoreSheet was tampered on row# " & i
                            Exit Sub
                        End If

                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & Sheet1.Cells(i, 27).Value() & "")

                        SQLScoreStr = SQLScoreStr & " ContestantID ,ContestID, BadgeNumber,DOB,Grade,ChildNumber,MemberID"
                        SQLScoreStrVal = SQLScoreStrVal & "" & Sheet1.Cells(i, 27).Value() & "," & ddlProduct.SelectedValue & ",'" & Sheet1.Cells(i, 1).Value() & "','" & Sheet1.Cells(i, 3).Value() & "'," & Sheet1.Cells(i, 4).Value() & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ""
                        SQLScoreUpdateStr = SQLScoreUpdateStr & " Update ScoreDetail Set ContestantID = " & Sheet1.Cells(i, 27).Value() & ",ContestID =" & ddlProduct.SelectedValue & ",BadgeNumber= '" & Sheet1.Cells(i, 1).Value() & "',DOB= '" & Sheet1.Cells(i, 3).Value() & "', Grade=" & Sheet1.Cells(i, 4).Value() & ",ChildNumber=" & dsChildNm.Tables(0).Rows(0)(0) & ",MemberID=" & dsChildNm.Tables(0).Rows(0)(1) & ""
                        SQLstr = SQLstr & "Update Contestant set "

                        If Attendance > 0 Then
                            SQLScoreStr = SQLScoreStr & ",AttendanceFlag"
                            SQLScoreStrVal = SQLScoreStrVal & ",'" & Sheet1.Cells(i, Attendance).Value() & "'"
                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",AttendanceFlag='" & Sheet1.Cells(i, Attendance).Value() & "'"
                            If Sheet1.Cells(i, Attendance).Value() = "N" Then
                                SQLstr = SQLstr & " Score1=Null, Score2=Null, Score3=Null, Rank= Null,"
                                GoTo EscapValid
                            End If
                        End If

                        If Not ddlTypeofData1.SelectedValue = "0" Then
                            If Attendance > 0 Then
                                If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                                    Try
                                        If Score1 > 0 Then
                                            SQLstr = SQLstr & " Score1=" & CDbl(0 & Sheet1.Cells(i, Score1).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, Score1).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, Score1).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase1Score,"
                                                SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =0"
                                            ElseIf Sheet1.Cells(i, Score1).Value() > 0 Then
                                                SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Score1).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =" & Sheet1.Cells(i, Score1).Value()
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score1=0 " & ","
                                        End If

                                        For index = 1 To MaxIndexPhase1 - 1
                                            If Not IsNumeric(Sheet1.Cells(i, Phase1_TB(index)).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase1_TB" & index & ","
                                                SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Phase1_TB(index)).Value() & "" ' where ContestantID =" & Sheet1.Cells(i, 27).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=" & Sheet1.Cells(i, Phase1_TB(index)).Value() & ""
                                                ScoreExistFlag = True
                                            End If
                                        Next
                                    Catch ex As Exception
                                        lblErr.Text = "ScoreSheet Missing Phase1 Value(s) on Row# " & i
                                        Response.Write(ex.ToString())
                                        Exit Sub
                                    End Try
                                End If
                            Else '' Added for Essay Writing and Public Speaking---Attendance not present
                                If Score1 > 0 Then
                                    SQLstr = SQLstr & " Score1=" & CDbl(0 & Sheet1.Cells(i, Score1).Value()) & ","
                                    If CDbl(0 & Sheet1.Cells(i, Score1).Value()) > 0 Then
                                        ScoreExistFlag = True
                                    End If
                                    If Not IsNumeric(Sheet1.Cells(i, Score1).Value()) = True Then
                                        Phasecnt = 1
                                        Ph2Flag = True
                                        lblErr.Text = lblErr.Text & "Phase1Score,"
                                        SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                        SQLScoreStrVal = SQLScoreStrVal & ",0"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =0"
                                    ElseIf Sheet1.Cells(i, Score1).Value() > 0 Then
                                        SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                        SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Score1).Value() & ""
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =" & Sheet1.Cells(i, Score1).Value()
                                    End If
                                Else
                                    SQLstr = SQLstr & "Score1=0 " & ","
                                End If

                            End If
                        End If
                        If (ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") Then
                            If Attendance > 0 Then
                                Try
                                    If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                                        If score2 > 0 Then
                                            SQLstr = SQLstr & " Score2=" & CDbl(0 & Sheet1.Cells(i, score2).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, score2).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, score2).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase2Score," & i
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score2).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & Sheet1.Cells(i, score2).Value() & ""
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score2=0 " & ","
                                        End If
                                        If rank > 0 Then
                                            SQLstr = SQLstr & " Rank=" & CInt(0 & Sheet1.Cells(i, rank).Value()) & ","
                                            If Not IsNumeric(Sheet1.Cells(i, rank).Value()) = True Then
                                                Phasecnt = 1
                                                lblErr.Text = lblErr.Text & "Rank,"
                                                SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                                SQLScoreStrVal = SQLScoreStrVal & ",0,''"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0, Rank_Alpha = ''"
                                            Else
                                                If Sheet1.Cells(i, rank).Value() > 0 Then
                                                    SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                                    SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, rank).Value() & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & Sheet1.Cells(i, rank).Value() & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    ValueExistFlag = True
                                                End If
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Rank= 0 " & ","
                                        End If
                                        For index = 1 To MaxIndexPhase2 - 1
                                            If Not IsNumeric(Sheet1.Cells(i, Ph2_Round(index)).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Ph2_Round" & index & ","
                                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=0"

                                            Else
                                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Ph2_Round(index)).Value() & "" ' where ContestantID =" & Sheet1.Cells(i, 27).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=" & Sheet1.Cells(i, Ph2_Round(index)).Value() & ""
                                                ScoreExistFlag = True
                                            End If
                                        Next

                                    End If
                                Catch ex As Exception
                                    lblErr.Text = "Score Sheet Missing Phase2 Value(s) on Row#" & i
                                End Try
                            Else ''Added For EW and PS-------Attendance not Present
                                If rank > 0 Then
                                    SQLstr = SQLstr & " Rank=" & CInt(0 & Sheet1.Cells(i, rank).Value()) & ","
                                    If Not IsNumeric(Sheet1.Cells(i, rank).Value()) = True Then
                                        Phasecnt = 1
                                        lblErr.Text = lblErr.Text & "Rank,"
                                        SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                        SQLScoreStrVal = SQLScoreStrVal & ",0,''"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0, Rank_Alpha = ''"
                                    Else
                                        If Sheet1.Cells(i, rank).Value() > 0 Then
                                            SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                            SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, rank).Value() & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & Sheet1.Cells(i, rank).Value() & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            ValueExistFlag = True
                                        End If
                                    End If
                                Else
                                    SQLstr = SQLstr & "Rank= 0 " & ","
                                End If

                            End If
                        End If

                        If Phasecnt = 1 Then
                            lblErr.Text = lblErr.Text.Trim.Trim(",")
                            lblErr.Text = lblErr.Text & " on Row#" & i & "<br>"
                            Phasecnt = 0
                        End If

                        ' Phase2 done Seperately
                        If ddlTypeofData1.SelectedValue = "5" Then
                            If score3 > 0 Then
                                If Not IsNumeric(Sheet1.Cells(i, score3).Value()) = True And Sheet1.Cells(i, score3).Value().Equals("") = False Then
                                    lblErr.Text = "Score 3 must be a number on row# " & i
                                    Exit Sub
                                End If
                            End If
                            If score3 > 0 Then
                                If CDbl(0 & Sheet1.Cells(i, score3).Value()) > 0 Then
                                    ScoreExistFlag = True
                                End If
                                SQLstr = SQLstr & " Score3=" & CDbl(0 & Sheet1.Cells(i, score3).Value()) & ","
                            Else
                                SQLstr = SQLstr & " Score3=0 " & ","
                            End If
                        End If

EscapValid:
                       
                    Catch ex As Exception
                        Response.Write(ex.ToString())
                    End Try

                    SQLstr = SQLstr & "ModifiedBy=" & Session("LoginID") & ",ModifiedDate = GETDATE() Where contestant_id=" & Sheet1.Cells(i, 27).Value() & ";"
                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() ,ChapterID= " & ddlChapter.SelectedValue & ",ContestYear=" & ddlYear.SelectedValue & ",ProductCode='" & getProductcode(ddlProduct) & "',ProductGroupCode= '" & ddlProductGroup.SelectedValue & "',ProductID=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & " ,ProductGroupID=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & " WHERE ContestantID=" & Sheet1.Cells(i, 27).Value() & ";" '" Update ScoreDetail Set " 

                    Try
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & Sheet1.Cells(i, 27).Value() & "") > 0 Then
                            SQLScoreStrExe = SQLScoreStrExe & SQLScoreUpdateStr '
                        Else
                            SQLScoreStrExe = SQLScoreStrExe & "Insert into ScoreDetail(" & SQLScoreStr & ",Createdby,CreatedDate,ChapterID,ContestYear,ProductCode,ProductGroupCode,ProductID,ProductGroupID ) Values(" & SQLScoreStrVal & "," & Session("LoginID") & ",GETDATE()," & ddlChapter.SelectedValue & "," & ddlYear.SelectedValue & ",'" & getProductcode(ddlProduct) & "','" & ddlProductGroup.SelectedValue & "'," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & ");" 'Where ContestantID= " & Sheet1.Cells(i, 27).Value()  '
                        End If
                        SQLScoreStr = ""
                        SQLScoreStrVal = ""

                    Catch ex As Exception
                        ' Response.Write(ex.ToString())
                        Exit Sub
                    End Try

                   
                    If Not Attendance = 0 Then
                        If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                            ContestantIDValue = ContestantIDValue & "," & Sheet1.Cells(i, 27).Value()
                        End If
                    End If
                Next
                


                ''Flag set during Phase1 upload
                If ddlTypeofData1.SelectedValue = "0" Then
                    PhaseUFlag = "P1UAttend"
                ElseIf ddlTypeofData1.SelectedValue = "1" Then
                    PhaseUFlag = "P1UScore"
                ElseIf ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4" Then
                    PhaseUFlag = "P12UList"
                End If
                PhaseUVal = "'Y'"
            End If

            ''ScoreSheetLog file.. insertion of data..
            SQLScoreSheetLog = "Insert into ScoreSheetLog (ContestID, ContestYear,FileName,EventID,Event,ChapterID,Chapter,ProductGroupID,ProductID,ProductGroupCode ,ProductCode,TypeofDataID,TypeofData," & PhaseUFlag & ",CreatedBy,CreatedDate"
            SQLScoreSheetLogVal = SQLScoreSheetLogVal & ") Values(" & ddlProduct.SelectedValue & "," & ddlYear.SelectedValue & ",'" & srcfilename & "'," & ddlContest.SelectedValue & ",'" & ddlContest.SelectedItem.Text & "'," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "',(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductGroupCode from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select ProductCode from Contest Where ContestID=" & ddlProduct.SelectedValue & ")," & ddlTypeofData1.SelectedValue & ",'" & ddlTypeofData1.SelectedItem.Text & "'," & PhaseUVal & "," & Session("LoginID") & ",GETDATE())"

            SQLScoreStrExe = SQLScoreStrExe & SQLScoreSheetLog & SQLScoreSheetLogVal

            Sheet1.Protect(PWD)
            If ScoreExistFlag = False And (ddlTypeofData1.SelectedValue = "1" Or ddlTypeofData1.SelectedValue = "2") Then
                lblErr.Text = "No score found in sheet"
                Exit Sub
            End If

            If Ph2Flag = True Then
                lblWarngMsg.Text = "Score Sheet Missing the following Values<br>" & lblErr.Text
            Else
                lblWarngMsg.Text = ""
            End If

            ' Response.Write("**SQLstr**" & SQLstr)

            lblConfirm.Text = "Warning: Scores already exist, do you want to replace them?"
            If ddlTypeofData1.SelectedValue = "4" Then
                If SQLstr.Length > 10 Then 'And ValueExistFlag = True 
                    Try
                        'SQLstr = SQLstr & "; Update sht set sht.UploadedDate=GETDATE(),sht.UploadedBy=" & Session("LoginID") & " from ScoreSheet sht Inner Join Contestant C On C.ContestID = sht.ContestID Where C.contestant_id = " & flagcontestid
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from contestant where (score1 is not null OR score2 is not null OR score3 is not null Or Rank is not null ) AND ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "") > 0 Then
                            HdnScoreDetailSQL.Value = SQLScoreStrExe
                            HdnexecQuery.Value = SQLstr
                            lblConfirm.Visible = True
                            trconfirm.Visible = True
                            trAll.Visible = False
                        Else
                            'Commented for Testing 13/03/2012 commented taken on 31/03/2012
                            Try
                                lblConfirm.Visible = False
                                trconfirm.Visible = False
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLstr)
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLScoreStrExe)
                                lblErr.Text = "Successfully Uploaded"
                            Catch ex As Exception
                                Response.Write(ex.ToString)
                            End Try
                        End If
                    Catch ex As Exception
                        Response.Write(ex.ToString)
                    End Try
                Else
                    lblErr.Text = "No rank found, Please correct the scores in Scoresheet"
                End If
            Else
                SQLstr = ""
                If ddlTypeofData1.SelectedValue = "0" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where AttendanceFlag Is Not Null and  ContestantID  in (" & ContestantIDValue & ")") > 0 Then
                    lblConfirm.Text = "Warning: Attendance already exist, do you want to replace them?"
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "1" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where Phase1Score Is Not Null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "2" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where  Phase2Score Is Not Null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then ''and AttendanceFlag Is Null
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf (ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where Phase1Score Is Not Null and Phase2Score is Not Null  and  ContestantID in (" & ContestantIDValue & ")") > 0 Then ' and Rank Is Not Null
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                Else
                    lblConfirm.Visible = False
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLScoreStrExe)
                    lblErr.Text = "Successfully Uploaded"
                End If
            End If
        Else
            lblErr.Text = "Please Click Browse to Select Filled Score Sheet."
        End If
    End Sub
    Protected Sub btnUploadMaster_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadMaster_.Click
        LblMasterErr.Text = ""
        If FileUploadMaster.HasFile Then
            Dim srcfilename As String = FileUploadMaster.FileName
            If Not System.IO.Path.GetExtension(FileUploadMaster.FileName).ToLower = ".xls" Then
                LblMasterErr.Text = "Only Excel file with .xls is allowed."
                Exit Sub
            End If
            Dim checkfile As String() = srcfilename.Split("_")
            Try
                If checkfile(0).Trim() <> ddlMYear.SelectedValue Then
                    LblMasterErr.Text = "Year Mismatch"
                    Exit Sub
                ElseIf checkfile(1).Trim() <> IIf(ddlMContest.SelectedValue = 1, "Fin", "Reg") Then
                    LblMasterErr.Text = "Event Mismatch"
                    Exit Sub
                ElseIf checkfile(2).Trim() <> ddlMProductGroup.SelectedValue Then
                    LblMasterErr.Text = "Contest Mismatch"
                    Exit Sub
                ElseIf checkfile(3).Trim() <> getProductcode(ddlMProduct) Then
                    LblMasterErr.Text = "Level Mismatch"
                    Exit Sub
                ElseIf checkfile.Length > 5 And (Not ddlMPhase.SelectedValue = "1") And (Not ddlMPhase.SelectedValue = "2") And (Not ddlMPhase.SelectedValue = "3") And (Not ddlMPhase.SelectedValue = "4") And (Not ddlMPhase.SelectedValue = "5") Then
                    LblMasterErr.Text = "Not a valid Master Scoresheet"
                    Exit Sub
                ElseIf checkfile(4).Trim().ToLower().Substring(0, 10) <> "scoresheet" Then
                    LblMasterErr.Text = "Missing 'ScoreSheet' in Filename or Not valid"
                    Exit Sub
                End If
                If Not ddlMPhase.SelectedValue = "0" And Not ddlMPhase.SelectedValue = "1" Then
                    If ddlMPhase.SelectedValue = "2" Then
                        If checkfile(5).Trim().ToLower().Substring(0, 2) <> "p2" Then
                            LblMasterErr.Text = "Missing 'P2' in filename"
                            Exit Sub
                        End If
                    ElseIf ddlMPhase.SelectedValue = "3" Then
                        If checkfile(5).Trim().ToLower().Substring(0, 2) <> "p3" Then
                            LblMasterErr.Text = "Missing 'P3' in filename"
                            Exit Sub
                        End If
                    ElseIf ddlMPhase.SelectedValue = "4" Then
                        Response.Write("*******" & checkfile(5).Trim().ToLower().Substring(0, 6).Trim())
                        If checkfile(5).Trim().Substring(0, 6) <> "p3List" Then
                            LblMasterErr.Text = "Missing 'p3List' in filename"
                            Exit Sub
                        End If
                    ElseIf ddlMPhase.SelectedValue = "5" Then
                        If checkfile(5).Trim().Substring(0, 7) <> "Toppers" Then
                            LblMasterErr.Text = "Missing 'Toppers' in filename"
                            Exit Sub
                        End If
                    End If
                End If
            Catch ex As Exception
                LblMasterErr.Text = "File name is Not in desired format with '_'"
                Exit Sub
            End Try
            FileUploadMaster.PostedFile.SaveAs(Server.MapPath("ScoreSheets/Master/" & FileUploadMaster.FileName))
            LblMasterErr.Text = "Uploaded Successfully"
        Else
            LblMasterErr.Text = "Please select Master scoresheet"
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        LoadProduct()
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        If ddlChapter.SelectedItem.Value = 0 Then
            BtnDownload.Enabled = False
            btnUpload.Enabled = False
        Else
            loadproductGroup()
        End If

    End Sub

    Protected Sub ddlMProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMProductGroup.SelectedIndexChanged
        LoadMProduct()
    End Sub

    Function getProductcode(ByVal ddl As DropDownList) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 ProductCode from Product where Name='" & ddl.SelectedItem.Text & "'")
    End Function

    Protected Sub BtnMDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LblMasterErr.Text = ""
        Dim SrcFileName As String
        Dim FileExistFlag As Boolean = False

        SrcFileName = ddlMYear.SelectedValue & "_" & IIf(ddlMContest.SelectedValue = 1, "Fin", "Reg") & "_" & ddlMProductGroup.SelectedValue & "_" & getProductcode(ddlMProduct) & "_ScoreSheet" '-- if the file exists on the server

        If ddlMPhase.Enabled = True Then
            If Not ddlMPhase.SelectedValue = 0 Then
                If (ddlMPhase.SelectedValue = 2) Or (ddlMPhase.SelectedValue = 3) Then
                    SrcFileName = SrcFileName & "_p" & ddlMPhase.SelectedValue
                ElseIf ddlMPhase.SelectedValue = 4 Then
                    SrcFileName = SrcFileName & "_p3List"
                ElseIf ddlMPhase.SelectedValue = 5 Then
                    SrcFileName = SrcFileName & "_Toppers"
                End If
            End If
        End If

        Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls"))
        If file.Exists Then 'set appropriate headers
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
        End If
        LblMasterErr.Text = ""
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        LoadContests()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        'check whether downloaded
        checkscorehsheetdown()
    End Sub

    Private Sub checkscorehsheetdown()
        lblUploadCondn.Text = "Upload button will show up after score sheet is downloaded, exit the application and come back again."
        lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
        If ddlProduct.Items.Count > 0 Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoresheetLog where ContestID=" & ddlProduct.SelectedValue) > 0 Then
                FileUpload.Visible = True
                btnUpload.Visible = True
                trupload.Visible = True
                lblUploadCondn.Text = ""
                LoadUpPhase()
                CheckEnableUFlags()
            Else
                trupload.Visible = False
                FileUpload.Visible = False
                btnUpload.Visible = False
                TrTypeofData1.Visible = False
                TrRoom1.Visible = False
            End If
        Else
            trupload.Visible = False
            FileUpload.Visible = False
            btnUpload.Visible = False
            TrTypeofData1.Visible = False
            TrRoom1.Visible = False
            lblErr.Text = "No files Downloaded"
        End If
        LoadPhase()
        'LoadUpPhase()
    End Sub

    Protected Sub BtnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConfirm.Click
        If HdnexecQuery.Value.Length > 0 Then
            If ddlTypeofData1.SelectedValue = "4" Then
                ''Commented for Testing 13/03/2012 - Comment removed on 31/03/2012
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set Score1=Null, Score2=Null , Score3=Null , RANK=Null  where ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "")  'Contestant_id in(" & hdncontestantIds.Value & ")")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, HdnexecQuery.Value)
                lblErr.Text = "Successfully Uploaded"
            End If
        End If
        If HdnScoreDetailSQL.Value.Length > 0 Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, HdnScoreDetailSQL.Value)
            lblErr.Text = "Successfully Uploaded"
        End If
        lblWarngMsg.Text = ""
        trconfirm.Visible = False
        trAll.Visible = True
        HdnexecQuery.Value = ""
        HdnScoreDetailSQL.Value = ""
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        lblWarngMsg.Text = ""
        HdnexecQuery.Value = ""
        HdnScoreDetailSQL.Value = ""
        trconfirm.Visible = False
        trAll.Visible = True
    End Sub
    Private Sub LoadRoom(ByVal NRooms As Integer)
        Dim i As Integer
        ddlRoom.Items.Clear()
        For i = 0 To NRooms - 1
            ddlRoom.Items.Insert(i, New ListItem(i + 1, i + 1))
        Next
        ddlRoom.Items.Insert(i, New ListItem("ALL", "ALL"))
    End Sub
    Private Sub LoadUpRoom(ByVal NRooms As Integer)
        ddlRoom1.Items.Clear()
        For i As Integer = 0 To NRooms - 1
            ddlRoom1.Items.Insert(i, New ListItem(i + 1, i + 1))
        Next
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        LoadPhase()
        'checkscorehsheetdown()
    End Sub

    Protected Sub ddlTypeofData1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeofData1.SelectedIndexChanged
        LoadUpPhase()
        CheckEnableUFlags()
    End Sub
    Private Sub LoadPhase()
        lbldwError.Text = ""
        'lblErr.Text = ""
        lblWarngMsg.Text = ""
        BtnDownload.Enabled = True

        Dim NRooms As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Ph2Rooms from Contest where contestId=" & ddlProduct.SelectedValue)
        If NRooms > 1 Then
            TrPhase.Visible = True
            LoadRoom(NRooms)
            If (ddlPhase.SelectedValue = "2" Or ddlPhase.SelectedValue = "3") Then  'And
                TrRoom.Visible = True
            Else
                TrRoom.Visible = False
            End If
            CheckEnableDFlags()
        ElseIf NRooms = 1 Then
            TrPhase.Visible = False
            TrRoom.Visible = False
            ddlPhase.SelectedValue = "1"
            ddlRoom.DataSource = Nothing
            ddlRoom.DataBind()
        End If
    End Sub
    Private Sub LoadUpPhase()
        lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
        FileUpload.Enabled = True
        btnUpload.Enabled = True
        TrTypeofData1.Visible = False
        TrRoom1.Visible = False
        Dim NRooms As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select Ph2Rooms from Contest where contestId=" & ddlProduct.SelectedValue)
        If NRooms > 1 Then
            TrTypeofData1.Visible = True
            LoadUpRoom(NRooms)
            If ddlTypeofData1.SelectedValue = "2" Then
                TrRoom1.Visible = True
            Else
                TrRoom1.Visible = False
            End If
        ElseIf NRooms = 1 Then
            TrTypeofData1.Visible = False
            TrRoom1.Visible = False
            ddlTypeofData1.SelectedValue = "4"
            ddlRoom1.DataSource = Nothing
            ddlRoom1.DataBind()
        End If
    End Sub
    Private Sub CheckEnableDFlags()
        If ddlPhase.SelectedValue = "3" Or ddlPhase.SelectedValue = "4" Then
            Dim readrddl As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Distinct P1UAttend,P1UScore,P2UList,isNull(RoomNumberU,-1) as RoomNumberU From ScoreSheetLog Where ContestID= " & ddlProduct.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & "And ContestYear=" & ddlYear.SelectedValue & "")
            Dim P1AttendFlag As Boolean = False
            Dim P1ScoreFlag As Boolean = False
            Dim P2ScoreFlag As Boolean = False
            Dim RoomUp As Integer = 0
            While readrddl.Read()
                If readrddl("P1UAttend").ToString = "Y" Then
                    P1AttendFlag = True
                End If
                If readrddl("P1UScore").ToString = "Y" Then
                    P1ScoreFlag = True
                End If
                For j As Integer = 0 To ddlRoom.Items.Count - 1
                    If readrddl("RoomNumberU") = j Then
                        RoomUp = RoomUp + 1
                        Exit For
                    Else
                        RoomUp = RoomUp
                    End If
                Next
            End While
            readrddl.Close()

            If P1AttendFlag = False And ddlPhase.SelectedValue = 3 Then
                lbldwError.Text = "Phase1 Attendance not yet Uploaded"
                TrRoom.Visible = False
                BtnDownload.Enabled = False
                Exit Sub
            Else
                BtnDownload.Enabled = True
            End If

            If RoomUp = ddlRoom.Items.Count - 1 Then '' All occurs in download part
                P2ScoreFlag = True
            Else
                P2ScoreFlag = False
            End If

            If ddlPhase.SelectedValue = 4 Then
                If (P1ScoreFlag = False Or P2ScoreFlag = False) Then
                    lbldwError.Text = "Phase1score and Phase2 score not yet uploaded"
                    BtnDownload.Enabled = False
                    Exit Sub
                Else
                    BtnDownload.Enabled = True
                End If
            End If
        End If
    End Sub

    Private Sub CheckEnableUFlags()
        Dim readrddl As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Distinct P1DList,P2DList,P12DList,isNull(RoomNumberU,-1) as RoomNumberU,isNull(RoomNumberD,-1) as RoomNumberD,RoomAllD From ScoreSheetLog Where ContestID= " & ddlProduct.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & "And ContestYear=" & ddlYear.SelectedValue & "")
        Dim P1DFlag As Boolean = False
        Dim P2DFlag As Boolean = False
        Dim P12DFlag As Boolean = False
        Dim P2AllDFlag As Boolean = False
        Dim RoomD As Integer
        Dim NRooms As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select Ph2Rooms from Contest where contestId=" & ddlProduct.SelectedValue)
        Dim RoomNumberIndex As Integer
        Dim RoomDL As Integer = 0
        For RoomD = 0 To ddlRoom1.Items.Count - 1
            ddlRoom1.Items(RoomD).Enabled = False
        Next

        While readrddl.Read()
            RoomNumberIndex = readrddl("RoomNumberD") - 1
            If readrddl("P1DList").ToString = "Y" Then
                P1DFlag = True
            End If
            If readrddl("P2DList").ToString = "Y" Then
                P2DFlag = True
            End If
            If readrddl("P12DList").ToString = "Y" Then
                P12DFlag = True
            End If
            If readrddl("RoomAllD").ToString = "Y" Then
                For RoomD = 0 To ddlRoom1.Items.Count - 1
                    ddlRoom1.Items(RoomD).Enabled = True
                Next
                P2AllDFlag = True
            End If
            For RoomD = 0 To ddlRoom1.Items.Count - 1
                If RoomNumberIndex = RoomD Then
                    ddlRoom1.Items(RoomD).Enabled = True
                    RoomDL = RoomDL + 1
                    Exit For
                Else
                    RoomDL = RoomDL
                End If
            Next
            If RoomDL = ddlRoom1.Items.Count Then
                P2AllDFlag = True
            End If
            
        End While


        If ddlTypeofData1.SelectedValue = "0" And P1DFlag = False Then
            lblErr.Text = "Phase1 Contestant List Not yet Downloaded"
            btnUpload.Enabled = False
            FileUpload.Enabled = False
            Exit Sub
        Else
            btnUpload.Enabled = True
            FileUpload.Enabled = True
        End If

        If ddlTypeofData1.SelectedValue = "1" And P1DFlag = False Then
            lblErr.Text = "Phase1 Contestant List Not yet Downloaded"
            btnUpload.Enabled = False
            FileUpload.Enabled = False
            Exit Sub
        Else
            btnUpload.Enabled = True
            FileUpload.Enabled = True

        End If
        If ddlTypeofData1.SelectedValue = "2" And P2DFlag = False Then
            lblErr.Text = "Phase2 Contestant List Not yet Downloaded"
            TrRoom1.Visible = False
            btnUpload.Enabled = False
            FileUpload.Enabled = False
            Exit Sub
        Else
            btnUpload.Enabled = True
            FileUpload.Enabled = True
        End If
        If (ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And NRooms > 1 Then
            If (P12DFlag = True) Or (P1DFlag = True And P2AllDFlag = True) Then
                btnUpload.Enabled = True
                FileUpload.Enabled = True
            Else
                lblErr.Text = "Phase1 and Phase2 Contestant List Not yet Downloaded"
                btnUpload.Enabled = False
                FileUpload.Enabled = False
                Exit Sub

            End If
        End If

    End Sub

    Protected Sub ddlMPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMPhase.SelectedIndexChanged
        LblMasterErr.Text = ""
        'lblMdwError.Text = ""
    End Sub

    Protected Sub ddlMProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMProduct.SelectedIndexChanged
        LblMasterErr.Text = ""
        'lblMdwError.Text = ""
    End Sub

    Protected Sub ddlMYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMYear.SelectedIndexChanged
        LblMasterErr.Text = ""
        'lblMdwError.Text = ""
    End Sub

    Protected Sub ddlRoom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoom.SelectedIndexChanged
        'lbldwError.Text = ""
        lblErr.Text = ""
        lblWarngMsg.Text = ""
    End Sub

    Protected Sub ddlMContest_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMContest.SelectedIndexChanged
        getMPhase()
    End Sub
End Class