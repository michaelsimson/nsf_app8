<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="PrepClubCalendar.aspx.vb" Inherits="ChapterCoordinator_PrepClubCalendar"     %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<script type="text/javascript" language="javascript">
			window.history.forward(1);

			function PopupPicker(ctl, w, h) {
			    var PopupWindow = null;
			    settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
			    PopupWindow = window.open('../DatePicker.aspx?ID=1,Ctl=_ctl0_Content_main_' + ctl, 'DatePicker', settings);
			    PopupWindow.focus();
			}

		</script>
	    <table width="100%" runat="server" >
	   
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
					<td colspan="2" class="Heading" align="center" style="width: 991px" >
						<strong><asp:Label ID="lblHeading1" runat="server" ></asp:Label></strong><br />
						<strong><asp:Label ID="lblHeading2" runat="server" ></asp:Label></strong>
					</td>
				</tr>
				<tr><td align="left" colspan="2"><strong>
				        <asp:Label ID="lblYear" runat="server" Text="PrepClub Year"></asp:Label>
				        <asp:DropDownList ID="drpYear" runat="server" AutoPostBack="true"></asp:DropDownList> </strong>
                    </td>
				</tr>
				<tr>
  				        <td colspan="2" style="width: 991px"><asp:label ID="lblGridError" runat="server" visible="False" width="100%" ForeColor="Red" Font-Bold="True" Font-Size="Smaller"></asp:label>
  				        </td>
  				</tr>				
  				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" style="width: 991px" >
				        <asp:DataGrid ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="PrepClubCalID"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" Visible="False">
					 <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C" ></SelectedItemStyle>
				    <ItemStyle ForeColor="Black" BackColor="#EEEEEE" ></ItemStyle>
				    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false"></HeaderStyle>
				    <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			      
				<%--	<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="Black" BackColor="Gainsboro" ></HeaderStyle>
				--%> 	<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                           <Columns>
                                 <asp:BoundColumn  DataField="PrepClubCalID" Visible="False"  ></asp:BoundColumn>
                                <asp:BoundColumn DataField="EventFeesID" Visible="False" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PrepClub">
                                      <ItemTemplate >
                                        <asp:HyperLink ID="hlinkProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'> </asp:HyperLink>
                                      </ItemTemplate>
                                    <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                </asp:TemplateColumn >
                                 <asp:TemplateColumn HeaderText="Sponsor" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblContestSponsor" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorName")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorID")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorType" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorType")%>'></asp:Label>
                                     </ItemTemplate>
                                      <EditItemTemplate>
                                        <asp:DropDownList id="ddlContestSponsor" runat="server" DataTextField="SponsorName" DataValueField="SponsorID" OnPreRender="SetDropDown_Sponsor" AutoPostBack="false"></asp:DropDownList>
                                        <asp:DropDownList id="ddlContestSponsorType" runat="server" Visible="false" DataTextField="SponsorType" DataValueField="SponsorID" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="Venue">
                                      <ItemTemplate>
                                         <asp:Label ID="lblPrepClubVenue" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Venue")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                         <asp:DropDownList ID="ddlPrepClubVenue" runat="server" DataTextField="VenueName" DataValueField="VenueID"  OnPreRender="SetDropDown_Venue" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="15%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Event Day">
                                      <ItemTemplate><asp:Label ID="lblEventDay" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.EventDay", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                       <EditItemTemplate>
                                         <asp:DropDownList ID="ddlPrepClubDay" runat="server" OnPreRender="SetDropDown_EventDay" AutoPostBack="false">
                                        			<asp:ListItem Value="Saturday">Saturday</asp:ListItem>
					                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                      </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="14%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Registration Deadline">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                                          <asp:TextBox ID="txtEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                 </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="Start Time">
                                      <ItemTemplate>
                                          <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StrtTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList ID="ddlStartTime" runat="server" OnPreRender="SetDropDown_StartTime" >
					                      </asp:DropDownList>                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" wrap="True" />
                                    <ItemStyle Width="5%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="End Time">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EnTime")%>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlEndTime" runat="server" OnPreRender="SetDropDown_EndTime">
					                        </asp:DropDownList>                                   
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" Wrap="True" />
                                    <ItemStyle Width="5%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Building">
                                      <ItemTemplate>
                                          <asp:Label ID="lblBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:Label>
                                      </ItemTemplate>
                                        <EditItemTemplate>
                                         <asp:DropDownList ID="ddlPrepClubBldg" runat="server" DataTextField="Building" DataValueField="Building"  OnPreRender="SetDropDown_Building" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>                                
                                <asp:TemplateColumn HeaderText="Room">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:Label>
                                      </ItemTemplate>
                                       <EditItemTemplate>
                                         <asp:DropDownList ID="ddlPrepClubRoom" runat="server" DataTextField="Room" DataValueField="Room"  OnPreRender="SetDropDown_Room" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>  
                                <asp:TemplateColumn HeaderText="EventDate">
                                      <ItemTemplate>
                                          <asp:Label ID="lblEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventDate", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                                          <asp:TextBox ID="txtEditEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventDate", "{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                 </asp:TemplateColumn> 
                                 <asp:TemplateColumn HeaderText="LateFee Deadline">
                                      <ItemTemplate>
                                          <asp:Label ID="lblLateFeeDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                                          <asp:TextBox ID="txtEditLateFeeDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                 </asp:TemplateColumn> 
                                                        
                                 <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                              <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
                                <asp:TemplateColumn>
                                        <ItemTemplate>
                                        </ItemTemplate>
                                </asp:TemplateColumn>
                         </Columns>
  				</asp:DataGrid></td></tr></table>
<table ID="tblduplicate" runat="server" visible="false" border="2">
<tr>
<td align="center" style="width: 942px; height: 79px;">
    <br />
<asp:Label runat="server" Text="This PrepClub is a repeat this year.  Please confirm" ForeColor="Green" Font-Bold="true"></asp:Label><br />
<asp:Button ID="btnYes" runat="server" Text="Yes"/>&nbsp;
<asp:Button ID="btnNo" runat="server" Text="No"/>

</td>
</tr>
</table>
   <table width="100%" border="2" style="border-left-color: black; border-bottom-color: black; border-top-style: solid; border-top-color: black; border-right-style: solid; border-left-style: solid; border-right-color: black; border-bottom-style: solid;">
<tr align="center" >
    <td style="width:25%"><strong>Select PrepClub Details:</strong></td>
    <td align="left" colspan="2" style="width: 50%" >
        <asp:Button ID="btnAdd" runat="server" Text="Add Selected PrepClubs" CssClass="btn_01" />
        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn_01" />
        <asp:Label ID="lblDataErr" runat="server" CssClass="SmallFont" Visible="false" ForeColor="Red" Width="40%"></asp:Label>
        <asp:Label ID="lblError" runat="server" CssClass="SmallFont" Visible="false" ForeColor="Red"></asp:Label>
    </td>
</tr>
<tr  runat="server" style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
    <td align="left" rowspan="1" style="width: 35%" >
        PrepClubs:<br />
		<asp:CheckBoxList id="lstPrepClubs" runat="server" width="100%" height="300%"  AutoPostBack="true" CssClass="SmallFont" EnableViewState="true" DataTextField="Name" DataValueField="EventFeesID" ></asp:CheckBoxList>
	</td>
	<td style="width: 65100%" runat="server" >
	<table border="1" runat="server">
	        <tr runat="server">
                    <td style="width:271px">Select Venue:</td>
                    <td style="width: 101%" runat="server">
                        <asp:dropdownlist ID="ddlVenue" runat="server" Width="95%"  AutoPostBack ="true" CssClass="SmallFont" DataTextField="VenueName" DataValueField="VenueID" >
                        </asp:dropdownlist>
                    </td>
            </tr>
            <tr>
	                <td style="width: 271px">
                        Event Day:</td>
	                <td style="width: 101%">
                    <asp:DropDownList ID="ddlWeekdays" runat="server">
	                        <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                            <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                   </asp:DropDownList>
        	                        <%--<asp:textbox id="txtPrepClubDt1" runat="server" CssClass="SmallFont"></asp:textbox> <A href="javascript:PopupPicker('txtPrepClubDt1', 200, 200);"><IMG src="../Images/Calendar.gif" border="0"> </A>--%>
                </td>
            </tr>
            <tr>
                    <td style="width: 271px">Start Date
                    </td>
                    <td style="width: 101%">
                    <asp:textbox id="txtStartDate" runat="server"  AutoPostBack ="true" CssClass="SmallFont"></asp:textbox>
                    <asp:Button ID="btnDate" runat="server" Text="Select Date" Height="25px"/>
                   <asp:Calendar ID="Calendar1" runat="server"  Visible="False" ondayrender="Calendar1_DayRender">
        </asp:Calendar>
               
<%--                    <a href="javascript:PopupPicker('txtStartDate', 200, 200);"><img src="../Images/Calendar.gif" border="0"> </a>
--%>
                    </td>
            </tr>
            <tr>
                    <td style="width: 271px">RegistrationDeadline
                    </td>
                    <td style="width: 101%">
                    <asp:textbox id="txtRegDeadline" runat="server" CssClass="SmallFont"></asp:textbox> 
                    <asp:Button ID="btnRegDate" runat="server" Text="Select Date" Height="25px"/>
                   <asp:Calendar ID="Calendar2" runat="server"  Visible="False">
        </asp:Calendar>
                   <%-- <a href="javascript:PopupPicker('txtRegDeadline', 200, 200);"><img src="../Images/Calendar.gif" border="0"> </a>
               --%>     </td>
            </tr>
            <tr>
            <td style="width: 271px">LateFeeReg.Deadline </td>
            <td style="width: 101%"><asp:TextBox ID="txtLateRegDeadline" runat="server"  CssClass="SmallFont"></asp:TextBox>
            <asp:Button ID="BtnLateRegDate" runat="server" Text="Select Date" Height="25px"/>
                   <asp:Calendar ID="Calendar3" runat="server"  Visible="False">
        </asp:Calendar>
            </td>
            </tr>
            <tr>
	                <td style="width:  271px">Start Time:</td>
	                <td style="width: 101%">
	                        <%--<asp:textbox id="txtStartTime" runat="server" CssClass="SmallFont" ></asp:textbox>--%>
	                <asp:DropDownList ID="ddlStartTime1" runat="server"></asp:DropDownList> <%-- OnPreRender="SetDropDown_StartTime" --%>                  
                    </td>
            </tr>
            <tr>
	                <td style="width:  271px">End Time:</td>
	                <td style="width: 101%">
	                        <%--<asp:textbox id="txtEndTime" runat="server" CssClass="SmallFont" ></asp:textbox>--%>
	                      <asp:DropDownList ID="ddlEndTime1" runat="server" ><%--OnPreRender="SetDropDown_EndTime"--%>
                					                              
                                                          </asp:DropDownList>                                     
                    </td>
            </tr>
            <tr>
	                <td style="width:  271px">Building:</td>
	                <td style="width: 101%">
	                <asp:DropDownList ID="ddlBldg" runat="server"></asp:DropDownList>
	                <%--<asp:textbox id="txtBldg" runat="server" CssClass="SmallFont"></asp:textbox>--%>
	                </td>
            </tr>
            <tr>
                    <td style="width: 271px">Room:</td>
                    <td style="width: 101%">
                    <asp:DropDownList ID="ddlRoom" runat="server"></asp:DropDownList>
                   <%-- <asp:textbox id="txtRoom" runat="server" CssClass="SmallFont"></asp:textbox>--%></td>
            </tr>
            <tr>
                    <td style="width:  271px">Select Sponsor Type:</td>
                    <td style="width: 101%">
                        <asp:RadioButtonlist ID="rbtnMemberType" runat="server" AutoPostBack="true" 
                            RepeatDirection="Horizontal"  Width="298px" Height="16px">
                            <asp:ListItem  Value="O">Organization</asp:ListItem>
                            <asp:ListItem Value="I">Individual</asp:ListItem>
                             <asp:ListItem Selected="True" Value="">None</asp:ListItem>
                        </asp:RadioButtonlist></td>
            </tr>
            <tr>
                    <td style="width:  271px">Select Sponsor:</td>
                    <td style="width: 101%">
                        <asp:dropdownlist ID="ddlSponsor" runat="server" Width="95%" DataTextField="SponsorName" DataValueField="SponsorID" >
                        </asp:dropdownlist>
                    </td>
            </tr>
    </table></td>
</tr>

<tr>
<td colspan="3"><asp:Label ID="lblErr" runat="server" width="100%"></asp:Label></td>
</tr>
<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="3" class="ContentSubTitle" align="center" style="width: 991px" >
				        <asp:DataGrid ID="grdTargetOld" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="PrepClubCalID"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" Visible="False">
				   <ItemStyle ForeColor="Black" BackColor="#EEEEEE" ></ItemStyle>
				    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false"></HeaderStyle>
				    <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
		<%--
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="White" BackColor="#330099" ></HeaderStyle>
			--%>		<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                 <asp:BoundColumn  DataField="PrepClubCalID" Visible="False"  ></asp:BoundColumn>
                                <asp:BoundColumn DataField="EventFeesID" Visible="False" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="WorkShop">
                                      <ItemTemplate >
                                        <asp:HyperLink ID="hlinkProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'> </asp:HyperLink>
                                      </ItemTemplate>
                                    <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                </asp:TemplateColumn >
                                 <asp:TemplateColumn HeaderText="Sponsor" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblContestSponsor" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorName")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorID")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorType" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorType")%>'></asp:Label>
                                     </ItemTemplate>
                                    
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="Venue">
                                      <ItemTemplate>
                                         <asp:Label ID="lblPrepClubVenue" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Venue")%>'></asp:Label>
                                      </ItemTemplate>
                                    
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="15%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PrepClub Date">
                                      <ItemTemplate><asp:Label ID="lblPrepClubDate" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.EventDate", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                    
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="14%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Registration Deadline">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="10%" />
                                 </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Start Time">
                                      <ItemTemplate>
                                          <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" wrap="True" />
                                    <ItemStyle Width="5%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="End Time">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndTime")%>' ></asp:Label>
                                    </ItemTemplate>
                                      <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" wrap="True" />
                                    <ItemStyle Width="5%" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Building">
                                      <ItemTemplate>
                                          <asp:Label ID="lblBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:Label>
                                      </ItemTemplate>
                                     
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>                                
                                <asp:TemplateColumn HeaderText="Room">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:Label>
                                      </ItemTemplate>
                                     
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>  
                                    <asp:TemplateColumn HeaderText="EventDay">
                                      <ItemTemplate>
                                          <asp:Label ID="lblEventDay" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventDay")%>'></asp:Label>
                                      </ItemTemplate>
                                     
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>  
                                <asp:TemplateColumn HeaderText="LateRegDeadlineDate">
                                      <ItemTemplate>
                                          <asp:Label ID="lblLateRegDLDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                     
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="True" />
                                    <ItemStyle Width="2%" />
                                </asp:TemplateColumn>                         
                               
                         </Columns>
  				</asp:DataGrid></td></tr>
   </table>
    
    <div><asp:HyperLink runat="server" NavigateUrl="~/VolunteerFunctions.aspx" ID="hlinkChapterFunctions">Go back to Menu</asp:HyperLink></div>
</asp:Content>

