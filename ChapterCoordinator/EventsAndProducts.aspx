<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master"  CodeFile="EventsAndProducts.aspx.vb" Inherits="ChapterCoordinator_EventsAndProducts" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
		<script type="text/javascript" language="javascript">
			window.history.forward(1);

			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('../DatePicker.aspx?Ctl=' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}

		</script>
	    <table width="100%">
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
					<td colspan="2" class="Heading" align="center" >
						<strong><asp:Label ID="lblHeading1" runat="server" ></asp:Label></strong><br />
						<strong><asp:Label ID="lblHeading2" runat="server" ></asp:Label></strong>
					</td>
				</tr>
  				  <tr>
  				        <td colspan="2"><asp:label ID="lblGridError" runat="server" visible="False" width="100%" ForeColor="Red" Font-Bold="True" Font-Size="Smaller"></asp:label>
  				        </td>
  				  </tr>				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" >
				        <asp:DataGrid ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="ContestID"
					 GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="White" BackColor="#330099" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                 <asp:BoundColumn DataField="ContestID" Visible="False" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="ContestCategoryID" Visible="False" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Contests" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:HyperLink ID="hlinkContestCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>' ToolTip='<%#DataBinder.Eval(Container, "DataItem.ProductDescription")%>'></asp:HyperLink>
                                      </ItemTemplate>
                                    <HeaderStyle Width="10%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn >
                                <asp:TemplateColumn HeaderText="Sponsor" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblContestSponsor" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorName")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorID")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorType" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorType")%>'></asp:Label>
                                     </ItemTemplate>
                                      <EditItemTemplate>
                                        <asp:DropDownList id="ddlContestSponsor" runat="server" DataTextField="SponsorName" DataValueField="SponsorID" OnPreRender="SetDropDown_Sponsor" AutoPostBack="false"></asp:DropDownList>
                                        <asp:DropDownList id="ddlContestSponsorType" runat="server" Visible="false" DataTextField="SponsorType" DataValueField="SponsorID" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="Venue"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblContestVenue" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Venue")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                         <asp:DropDownList ID="ddlContestVenue" runat="server" DataTextField="VenueName" DataValueField="VenueID"  OnPreRender="SetDropDown_Venue" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Registration Deadline"  ItemStyle-Width="10%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:Label>
                                          <asp:TextBox ID="txtEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="true" />
                                 </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Check in Time" ItemStyle-Width="5%" >
                                      <ItemTemplate>
                                          <asp:Label ID="lblCheckInTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckInTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList ID="ddlCheckInTime" runat="server" OnPreRender="SetDropDown_CheckInTime">
                              				        <asp:ListItem Value="6:00">6:00 AM</asp:ListItem>
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>                                          </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Contest Date"  ItemStyle-Width="14%" HeaderStyle-Width="14%">
                                      <ItemTemplate><asp:Label ID="lblContestDate" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.ContestDate", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                     <EditItemTemplate>
                                        <asp:DropDownList ID="ddlGridContestDt" runat="server" DataTextField="WeekendDt" OnPreRender="SetDropDown_ContestDt" AutoPostBack="false">
	                                    </asp:DropDownList><asp:Label ID="lblGridContestDate" runat="Server" Width="100%" visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.ContestDate", "{0:d}")%>'></asp:Label>
                                   </EditItemTemplate>
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Start Time"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList ID="ddlStartTime" runat="server" OnPreRender="SetDropDown_StartTime" >
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							                        <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							                        <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							                        <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							                        <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							                        <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							                        <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							                        <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							                        <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							                        <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							                        <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							                        <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="End Time"  ItemStyle-Width="5%">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndTime")%>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlEndTime" runat="server" OnPreRender="SetDropDown_EndTime">
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							                        <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							                        <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							                        <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							                        <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							                        <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							                        <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							                        <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							                        <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							                        <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							                        <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							                        <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                                   
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Building"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>                                
                                <asp:TemplateColumn HeaderText="Room"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>                             
                                <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
       <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
                                <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlContest" runat="server" Width="100%" CssClass="SmallFont" DataTextField="ProductCode" DataValueField="ContestId" AutoPostBack="false" OnPreRender="SetDropDown_CopyFromContest" >
                                            </asp:DropDownList>
                                            <asp:Button id="btnCopy" runat="server" Text="Copy From" CssClass="SmallFont" OnClientClick="return(ddlcontest.selecteditem.value)" OnClick="GridCopy" />
                                        </ItemTemplate>
                                </asp:TemplateColumn>
                              
                         </Columns>
  				</asp:DataGrid></td></tr></table>
  				  <br /><center><asp:Button id="btnHide" runat="server" text="Show me How to add contests" Width="25%"/></center>
<asp:Panel runat="server" ID="pnlMsg" Width="100%"><b>Two Day Event:</b>
<ol START="1">
<li>Select a) all contests for the first day of contest, b) the contest date, c) building, d) venue, e) sponsor type and f) sponsor.</LI><li>Repeat the same for the second day of the contest.</li><li>Click "Add Selected Contests" Button</li></ol>
                                         <b>Single Day Event:</b>
<ol  START="1"><li>Select all contests in one step as described in 1) above.</li><li>Click "Add Selected Contests" Button</li></ol>
</asp:Panel><br />
   <table width="100%" border="2" style="border-left-color: black; border-bottom-color: black; border-top-style: solid; border-top-color: black; border-right-style: solid; border-left-style: solid; border-right-color: black; border-bottom-style: solid;">
<tr align="center" >
    <td colspan="2"><strong>Select Contest Details:</strong></td>
    <td align="left" colspan="2" style="width: 50%" >
        <asp:Button ID="btnAdd" runat="server" Text="Add Selected Contests" CssClass="SmallFont" />
        <asp:Label ID="lblDataErr" runat="server" CssClass="SmallFont" Visible="false" ForeColor="Red" Width="40%"></asp:Label>
    </td>
</tr>
<tr style="height: 20%; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
    <td align="left"  colspan="2" rowspan="5" style="width: 30%" >
        Contests:<br />
		<asp:CheckBoxList id="lstContests" runat="server" autopostback="false" width="100%" CssClass="SmallFont" EnableViewState="true" DataTextField="Name" DataValueField="ProductID"></asp:CheckBoxList>
	</td>
	<td style="width: 15%">Contest Date:</td>
	<td style="width: 55%">
	        <asp:DropDownList ID="ddlContestDt" runat="server" Width="95%"  DataTextField="WeekendDt" >
	        </asp:DropDownList>
    </td>
  
</tr>
<tr>
	<td style="width: 15%">Building:</td>
	<td style="width: 55%">
	        <asp:textbox id="txtBldg" runat="server" CssClass="SmallFont"></asp:textbox>
    </td>
</tr>
<tr>
    <td style="width: 15%">Select Venue:</td>
    <td style="width: 55%">
        <asp:dropdownlist ID="ddlVenue" runat="server" Width="95%" CssClass="SmallFont"  DataTextField="VenueName" DataValueField="VenueID" >
        </asp:dropdownlist>
    </td>
</tr>
<tr>
    <td style="width: 15%">Select Sponsor Type:</td>
    <td style="width: 55%">
        <asp:RadioButtonlist ID="rbtnMemberType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Width="412px">
            <asp:ListItem Selected="True" Value="O">Organization</asp:ListItem>
            <asp:ListItem Value="I">Individual</asp:ListItem>
        </asp:RadioButtonlist></td>
</tr>
<tr>
    <td style="width: 15%">Select Sponsor:</td>
    <td style="width: 55%">
        <asp:dropdownlist ID="ddlSponsor" runat="server" Width="95%"  DataTextField="SponsorName" DataValueField="SponsorID" >
        </asp:dropdownlist>
    </td>
</tr>
<tr>
<td colspan="4"><asp:Label ID="lblErr" runat="server" width="100%"></asp:Label></td>
</tr>
   </table>
    
    <div><asp:HyperLink runat="server" NavigateUrl="~/VolunteerFunctions.aspx" ID="hlinkChapterFunctions">Go back to Menu</asp:HyperLink></div>
</asp:Content>

 

 
 
 