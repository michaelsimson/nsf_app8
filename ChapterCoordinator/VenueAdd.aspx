<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VenueAdd.aspx.vb" Inherits="ChapterCoordinator_VenueAdd" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<script type="text/javascript" language="javascript">
			window.history.forward(1);

			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('../DatePicker.aspx?Ctl=' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}

		</script>
    &nbsp;&nbsp;
    <div>
    	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
		    <tr>
		        <th colspan=2 align=center> Venue Info
		        </th>
		    </tr>
							
			<tr>
				<td class="literal">ORGANIZATIONNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataORGANIZATIONNAME" Text='<%# Bind("ORGANIZATIONNAME") %>' MaxLength="75"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">TITLE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataTITLE" Text='<%# Bind("TITLE") %>' MaxLength="6"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FIRSTNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFIRSTNAME" Text='<%# Bind("FIRSTNAME") %>' MaxLength="35"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">LASTNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLASTNAME" Text='<%# Bind("LASTNAME") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ADDRESS1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataADDRESS1" Text='<%# Bind("ADDRESS1") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ADDRESS2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataADDRESS2" Text='<%# Bind("ADDRESS2") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CITY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCITY" Text='<%# Bind("CITY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">STATE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSTATE" Text='<%# Bind("STATE") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZIP:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZIP" Text='<%# Bind("ZIP") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">COUNTRY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCOUNTRY" Text='<%# Bind("COUNTRY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PHONE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPHONE" Text='<%# Bind("PHONE") %>' MaxLength="15"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FAX:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFAX" Text='<%# Bind("FAX") %>' MaxLength="15"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EMAIL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEMAIL" Text='<%# Bind("EMAIL") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SecondaryEMAIL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSecondaryEMAIL" Text='<%# Bind("SecondaryEMAIL") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MATCHINGGIFT:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMATCHINGGIFT" Text='<%# Bind("MATCHINGGIFT") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
				
			<tr>
				<td class="literal">REFERREDBY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataREFERREDBY" Text='<%# Bind("REFERREDBY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				

			<tr>
				<td class="literal">LIAISONPERSON:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLIAISONPERSON" Text='<%# Bind("LIAISONPERSON") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeleteReason:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeleteReason" Text='<%# Bind("DeleteReason") %>' MaxLength="200"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">BusType:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBusType" Text='<%# Bind("BusType") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Venue:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVenue" Text='<%# Bind("Venue") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Sponsor:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSponsor" Text='<%# Bind("Sponsor") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Website:</td>
				<td>
					<asp:TextBox runat="server" ID="dataWebsite" Text='<%# Bind("Website") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
					
			<tr>
			    <td colspan="2" align="center">
			        <asp:Button ID="btnUpdate" Text="Save" runat="server"   />
			    </td>
			</tr>				

   </table>
   </div>
    <div>
        <asp:HyperLink runat="server" NavigateUrl="ChapterFunctions.aspx" ID="hlinkChapterFunctions">Back to Chapter Functions</asp:HyperLink>
    </div>
    
</asp:Content>


 

 
 
 