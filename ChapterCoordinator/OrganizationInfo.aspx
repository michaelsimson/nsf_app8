<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="OrganizationInfo.aspx.vb" Inherits="ChapterCoordinator_OrganizationInfo" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="AutoMemberID" DataSourceID="ObjectDataSource1" ShowFooter=true>
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True"  />
            <asp:BoundField DataField="TableName" HeaderText="TableName" ReadOnly="True" SortExpression="TableName" />
            <asp:BoundField DataField="COUNTRY" HeaderText="COUNTRY" SortExpression="COUNTRY" />
            <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />
            <asp:BoundField DataField="REFERREDBY" HeaderText="REFERREDBY" SortExpression="REFERREDBY" />
            <asp:BoundField DataField="SecondaryEMAIL" HeaderText="SecondaryEMAIL" SortExpression="SecondaryEMAIL" />
            <asp:BoundField DataField="MAILINGLABEL" HeaderText="MAILINGLABEL" SortExpression="MAILINGLABEL" />
            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />
            <asp:BoundField DataField="EntityTrackingKey" HeaderText="EntityTrackingKey" SortExpression="EntityTrackingKey" />
            <asp:BoundField DataField="MEMBERID" HeaderText="MEMBERID" SortExpression="MEMBERID" />
            <asp:BoundField DataField="EMAIL" HeaderText="EMAIL" SortExpression="EMAIL" />
            <asp:BoundField DataField="SENDEMAIL" HeaderText="SENDEMAIL" SortExpression="SENDEMAIL" />
            <asp:BoundField DataField="CITY" HeaderText="CITY" SortExpression="CITY" />
            <asp:CheckBoxField DataField="IsValid" HeaderText="IsValid" ReadOnly="True" SortExpression="IsValid" />
            <asp:BoundField DataField="SENDNEWSLETTER" HeaderText="SENDNEWSLETTER" SortExpression="SENDNEWSLETTER" />
            <asp:CheckBoxField DataField="IsNew" HeaderText="IsNew" SortExpression="IsNew" />
            <asp:BoundField DataField="STATE" HeaderText="STATE" SortExpression="STATE" />
            <asp:BoundField DataField="SENDRECEIPT" HeaderText="SENDRECEIPT" SortExpression="SENDRECEIPT" />
            <asp:BoundField DataField="Sponsor" HeaderText="Sponsor" SortExpression="Sponsor" />
            <asp:BoundField DataField="Error" HeaderText="Error" ReadOnly="True" SortExpression="Error" />
            <asp:BoundField DataField="TITLE" HeaderText="TITLE" SortExpression="TITLE" />
            <asp:BoundField DataField="FIRSTNAME" HeaderText="FIRSTNAME" SortExpression="FIRSTNAME" />
            <asp:CheckBoxField DataField="IsDirty" HeaderText="IsDirty" ReadOnly="True" SortExpression="IsDirty" />
            <asp:BoundField DataField="Venue" HeaderText="Venue" SortExpression="Venue" />
            <asp:BoundField DataField="MemberSince" HeaderText="MemberSince" SortExpression="MemberSince" />
            <asp:BoundField DataField="LIAISONPERSON" HeaderText="LIAISONPERSON" SortExpression="LIAISONPERSON" />
            <asp:BoundField DataField="PHONE" HeaderText="PHONE" SortExpression="PHONE" />
            <asp:BoundField DataField="MATCHINGGIFT" HeaderText="MATCHINGGIFT" SortExpression="MATCHINGGIFT" />
            <asp:BoundField DataField="AutoMemberID" HeaderText="AutoMemberID" InsertVisible="False"
                ReadOnly="True" SortExpression="AutoMemberID" />
            <asp:BoundField DataField="FAX" HeaderText="FAX" SortExpression="FAX" />
            <asp:CheckBoxField DataField="IsDeleted" HeaderText="IsDeleted" ReadOnly="True" SortExpression="IsDeleted" />
            <asp:BoundField DataField="MIDDLEINITIAL" HeaderText="MIDDLEINITIAL" SortExpression="MIDDLEINITIAL" />
            <asp:BoundField DataField="ZIP" HeaderText="ZIP" SortExpression="ZIP" />
            <asp:BoundField DataField="Website" HeaderText="Website" SortExpression="Website" />
            <asp:BoundField DataField="ORGANIZATIONNAME" HeaderText="ORGANIZATIONNAME" SortExpression="ORGANIZATIONNAME" />
            <asp:BoundField DataField="LASTNAME" HeaderText="LASTNAME" SortExpression="LASTNAME" />
            <asp:BoundField DataField="DeletedFlag" HeaderText="DeletedFlag" SortExpression="DeletedFlag" />
            <asp:BoundField DataField="NSFCHAPTER" HeaderText="NSFCHAPTER" SortExpression="NSFCHAPTER" />
            <asp:BoundField DataField="BusType" HeaderText="BusType" SortExpression="BusType" />
            <asp:CheckBoxField DataField="IsEntityTracked" HeaderText="IsEntityTracked" SortExpression="IsEntityTracked" />
            <asp:CheckBoxField DataField="SuppressEntityEvents" HeaderText="SuppressEntityEvents"
                SortExpression="SuppressEntityEvents" />
            <asp:BoundField DataField="DeleteReason" HeaderText="DeleteReason" SortExpression="DeleteReason" />
            <asp:BoundField DataField="IRScat" HeaderText="IRScat" SortExpression="IRScat" />
            <asp:BoundField DataField="ADDRESS1" HeaderText="ADDRESS1" SortExpression="ADDRESS1" />
            <asp:BoundField DataField="ADDRESS2" HeaderText="ADDRESS2" SortExpression="ADDRESS2" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="nsf.Entities.OrganizationInfo"
        DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetAll" TypeName="nsf.Data.SqlClient.SqlOrganizationInfoProvider"
        UpdateMethod="Update"></asp:ObjectDataSource>

</asp:Content>


 

 
 
 