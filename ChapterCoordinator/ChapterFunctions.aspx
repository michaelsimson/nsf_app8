<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChapterFunctions.aspx.vb" Inherits="ChapterCoordinator_ChapterFunctions" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
			<div class="Heading" style="text-align: center">
				<h1>Chapter Functions</h1>
			</div>
			<table id="Table1" width="100%" border="2" cellpadding="1" cellspacing="1" style="background-color: #ff9966; border-right: #ffff66 thin solid; border-top: #ffff66 thin solid; border-left: #ffff66 thin solid; border-bottom: #ffff66 thin solid;">
				<tr id="trChapterCoordinator" runat="server" align="left" >
					<td style="border-right: #ffff99 thin solid; border-top: #ffff99 thin solid; border-left: #ffff99 thin solid; border-bottom: #ffff99 thin solid; background-color: #ffffcc;">
						<p><asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx"> Add/Update Venue Information</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink2" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx"> Add/Update Sponsor Information</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink3" runat="server" NavigateUrl="~/ChapterCoordinator/EventsAndProducts.aspx">Prepare Contest Calendar</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink5" runat="server" NavigateUrl="~/VolunteerRole.aspx">Assign Roles to Volunteers</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink6" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">View Registration Data</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink7" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Generate Badge Numbers</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink8" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Download Registration Data</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink9" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Make a Donation</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink10" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">View Donation History</asp:hyperlink></p>
						<p><asp:hyperlink id="Hyperlink11" runat="server" NavigateUrl="~/SendLogonInfo.aspx">Send Emails</asp:hyperlink></p>
						<p><asp:Button id="btnCCoLogoff" Text="Log Out" runat="server" CssClass="FormButton"></asp:Button></p>
					</td>
				</tr>
			</table>
</asp:Content>

 

 
 
 