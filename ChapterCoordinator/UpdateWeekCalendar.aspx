<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="UpdateWeekCalendar.aspx.vb" Inherits="UpdateWeekCalendar" title="Update WeekCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style="text-align:left">
<script language="javascript" type="text/javascript">
    function PopupPicker(ctl, w, h) {
        var PopupWindow = null;
        settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
        PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
        PopupWindow.focus();
    }
		</script>
           <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
</div>
<table  cellpadding = "2" cellspacing = "0" border="0" width="700px">
<tr><td align="center" ></td></tr><tr><td align="center" >
<table  cellpadding = "5" cellspacing = "0" border="0">
<tr><td align="left"> Event</td><td align="left" >
    <asp:DropDownList ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged ="ddlEvent_SelectedIndexChanged" DataTextField="Name" DataValueField ="EventID"  runat="server">
        <asp:ListItem Value="1">Finals</asp:ListItem>
        <asp:ListItem Value="2">Chapter</asp:ListItem>
      <asp:ListItem Value="0" Selected="True" >Select Event</asp:ListItem>
    </asp:DropDownList>
</td><%--<td align="left"> Event Year</td><td align="left" >
 <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" Width="75px" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server" Height="24px">
           </asp:DropDownList>
</td>--%></tr>
<tr><td align="center" colspan="2">
    <asp:Button ID="btnAddNew" runat="server" Text="Add New" /> &nbsp;
       
   </tr></table>
    </td></tr>
<tr><td align="center" >
<table id="traddUpdate" runat="server" visible = "false" cellpadding = "2" cellspacing = "0" border="0">
 
<tr><td align="left" width="100px"> Day 1 </td><td align="left"  width="125px">
    <asp:TextBox ID="txtDay1Date" runat="server"></asp:TextBox>
    </td><td width="100px">
    <a href="javascript:PopupPicker('<%=txtDay1Date.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
    <asp:RequiredFieldValidator ControlToValidate="txtDay1Date" ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    </td></tr>
<tr><td align="left"> Day 2 </td><td align="left" >  <asp:TextBox ID="txtDay2Date" runat="server"></asp:TextBox>
</td><td>
<a href="javascript:PopupPicker('<%=txtDay2Date.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
    <asp:RequiredFieldValidator ControlToValidate="txtDay2Date" ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
</td></tr>
<tr runat="server" id="trfinals" visible = "false" ><td align = "left" colspan = "3" >
<table cellpadding = "2" cellspacing = "0" border="0">
<tr><td align="left" width="100px">  Day 0 Dinner</td><td align="left" >
    <asp:TextBox ID="txtDay0Dinner" runat="server" Text="0.00"></asp:TextBox>
    </td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay0Dinner" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay0Dinner" ID="RangeValidator1" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 1 Breakfast</td><td align="left" >
 <asp:TextBox ID="txtDay1Breakfast" runat="server" Text="0.00"></asp:TextBox>
  </td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay1Breakfast" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay1Breakfast" ID="RangeValidator2" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 1 Lunch</td><td align="left" >
 <asp:TextBox ID="txtDay1Lunch" runat="server" Text="0.00"></asp:TextBox>
 </td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay1Lunch" ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay1Lunch" ID="RangeValidator3" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 1 Dinner</td><td align="left" >
 <asp:TextBox ID="txtDay1Dinner" runat="server" Text="0.00"></asp:TextBox>
</td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay1Dinner" ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay1Dinner" ID="RangeValidator4" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 2 Breakfast</td><td align="left" >
 <asp:TextBox ID="txtDay2Breakfast" runat="server" Text="0.00"></asp:TextBox>
</td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay2Breakfast" ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay2Breakfast" ID="RangeValidator5" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 2 Lunch</td><td align="left" >
 <asp:TextBox ID="txtDay2Lunch" runat="server" Text="0.00"></asp:TextBox>
</td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay2Lunch" ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay2Lunch" ID="RangeValidator6" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
<tr><td align="left"> Day 2 Dinner</td><td align="left" >
 <asp:TextBox ID="txtDay2Dinner" runat="server" Text="0.00"></asp:TextBox>
</td><td>
    <asp:RequiredFieldValidator ControlToValidate="txtDay2Dinner" ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>
    <asp:RangeValidator ControlToValidate="txtDay2Dinner" ID="RangeValidator7" runat="server" ErrorMessage="only numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
</td></tr>
</table>
</td></tr>
<tr><td  align="center" colspan="2" >
    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add" /> &nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnCancel" runat="server" CausesValidation="false" OnClick ="btnCancel_Click" Text="Clear" />
    </td></tr>
<tr><td  align="center" colspan="3" >
    <asp:Label ID="lblErr" ForeColor ="Red" runat="server"></asp:Label>&nbsp;<asp:Label Visible="false"  ID="lblWeekId" 
        runat="server" ></asp:Label></td></tr>
</table>
</td></tr><tr><td align="center" >
    <asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>
    <br />Note :  * You cannot update Event.
</td></tr><tr><td align="center" >
    <asp:DataGrid ID="DGWeekCalendar"  runat="server" DataKeyField="WeekId" 
        AutoGenerateColumns="False"   OnItemCommand="DGWeekCalendar_ItemCommand" CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>

           <ASP:TemplateColumn>
           <ItemTemplate>
		   <asp:LinkButton  CausesValidation="false"  id="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
		   </ItemTemplate>
		   </ASP:TemplateColumn>
<ASP:TemplateColumn>
           <ItemTemplate>
		   <asp:LinkButton  CausesValidation="false" id="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
		   </ItemTemplate>
			</ASP:TemplateColumn><asp:Boundcolumn DataField="WeekId"  HeaderText="WeekId" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="SatDay1"  HeaderText="Day1" DataFormatString="{0:d}" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="SunDay2" HeaderText="Day2" DataFormatString="{0:d}" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="DinnerDay0" HeaderText="Day0_Dinner" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BreakfastDay1" HeaderText="Day1_Breakfast" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="MealDay1" HeaderText="Day1_Lunch" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="DinnerDay1" HeaderText="Day1_Dinner" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BreakfastDay2" HeaderText="Day2_Breakfast" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="MealDay2" HeaderText="Day2_Lunch" DataFormatString="{0:c}"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="DinnerDay2" HeaderText="Day2_Dinner" DataFormatString="{0:c}"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Name" HeaderText="Eventname"/>
            </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>
</td></tr></table> 
</asp:Content>

