<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EventsAndProductsDetail.aspx.vb" Inherits="ChapterCoordinator_EventsAndProductsDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="frmEventsAndProductsDetail" runat="server">
    <table width="100%"  border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ContestID:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestID" Text='<%# Bind("ContestID") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataContestID" runat="server" Display="Dynamic" ControlToValidate="dataContestID" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataContestID" runat="server" Display="Dynamic" ControlToValidate="dataContestID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestCode:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestCode" Text='<%# Bind("ContestCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataContestCode" runat="server" Display="Dynamic" ControlToValidate="dataContestCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestTypeID:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestTypeID" Text='<%# Bind("ContestTypeID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestTypeID" runat="server" Display="Dynamic" ControlToValidate="dataContestTypeID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">NSFChapterID:</td>
				<td>
					<asp:TextBox runat="server" ID="txtNSFChapterID" Text='<%# Bind("NSFChapterID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataNSFChapterID" runat="server" Display="Dynamic" ControlToValidate="dataNSFChapterID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestCategoryID:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestCategoryID" Text='<%# Bind("ContestCategoryID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestCategoryID" runat="server" Display="Dynamic" ControlToValidate="dataContestCategoryID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Phase:</td>
				<td>
					<asp:TextBox runat="server" ID="txtPhase" Text='<%# Bind("Phase") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataPhase" runat="server" Display="Dynamic" ControlToValidate="dataPhase" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">GroupNo:</td>
				<td>
					<asp:TextBox runat="server" ID="txtGroupNo" Text='<%# Bind("GroupNo") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataGroupNo" runat="server" Display="Dynamic" ControlToValidate="dataGroupNo" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">BadgeNoBeg:</td>
				<td>
					<asp:TextBox runat="server" ID="txtBadgeNoBeg" Text='<%# Bind("BadgeNoBeg") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataBadgeNoBeg" runat="server" Display="Dynamic" ControlToValidate="dataBadgeNoBeg" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">BadgeNoEnd:</td>
				<td>
					<asp:TextBox runat="server" ID="txtBadgeNoEnd" Text='<%# Bind("BadgeNoEnd") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataBadgeNoEnd" runat="server" Display="Dynamic" ControlToValidate="dataBadgeNoEnd" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestDate:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestDate" Text='<%# Bind("ContestDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataContestDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">StartTime:</td>
				<td>
					<asp:TextBox runat="server" ID="txtStartTime" Text='<%# Bind("StartTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EndTime:</td>
				<td>
					<asp:TextBox runat="server" ID="txtEndTime" Text='<%# Bind("EndTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Building:</td>
				<td>
					<asp:TextBox runat="server" ID="txtBuilding" Text='<%# Bind("Building") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestYear:</td>
				<td>
					<asp:TextBox runat="server" ID="txtContestYear" Text='<%# Bind("ContestYear") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Room:</td>
				<td>
					<asp:TextBox runat="server" ID="txtRoom" Text='<%# Bind("Room") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RegistrationDeadline:</td>
				<td>
					<asp:TextBox runat="server" ID="txtRegistrationDeadline" Text='<%# Bind("RegistrationDeadline", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataRegistrationDeadline" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">CheckinTime:</td>
				<td>
					<asp:TextBox runat="server" ID="txtCheckinTime" Text='<%# Bind("CheckinTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">VenueId:</td>
				<td>
					<asp:TextBox runat="server" ID="txtVenueId" Text='<%# Bind("VenueId") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataVenueId" runat="server" Display="Dynamic" ControlToValidate="dataVenueId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">SponsorId:</td>
				<td>
					<asp:TextBox runat="server" ID="txtSponsorId" Text='<%# Bind("SponsorId") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataSponsorId" runat="server" Display="Dynamic" ControlToValidate="dataSponsorId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">SponsorType:</td>
				<td>
					<asp:TextBox runat="server" ID="txtSponsorType" Text='<%# Bind("SponsorType") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:TextBox runat="server" ID="txtEventId" Text='<%# Bind("EventId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:TextBox runat="server" ID="txtEventCode" Text='<%# Bind("EventCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventCode" runat="server" Display="Dynamic" ControlToValidate="dataEventCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:TextBox runat="server" ID="txtProductGroupId" Text='<%# Bind("ProductGroupId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="txtProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductId:</td>
				<td>
					<asp:TextBox runat="server" ID="txtProductId" Text='<%# Bind("ProductId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="txtProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
			    <td colspan="2" align="center">
			        <asp:Button ID="btnUpdate" Text="Save" runat="server"   />
			    </td>
			</tr>				

   </table>
    
    <div><asp:HyperLink runat="server" NavigateUrl="ChapterFunctions.aspx" ID="hlinkChapterFunctions">Back to Chapter Functions</asp:HyperLink></div>
		</form>
</body>
</html>

 

 
 
 