Imports nsf.Data.SqlClient

Partial Class ChapterCoordinator_EventsAndProductsDetail
    Inherits System.Web.UI.Page
    Dim objEntContest As New nsf.Entities.Contest
    Dim objSqlContestProviderBase As New SqlContestProviderBase

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim objInsContest As New nsf.Entities.Contest
        With objInsContest
            .ContestID = 1
            .BadgeNoBeg = 1
            ' .ContestDate = Convert.ToDateTime(Server.HtmlEncode(txtAppContestDate.Text))
            .ContestCode = ""
            .ContestCategoryID = 1
        End With
        objSqlContestProviderBase.Insert(objInsContest)
    End Sub

    Protected Sub frmEventsAndProductsDetail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles frmEventsAndProductsDetail.Load
        If Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("../login.aspx")
        End If

        If Not Page.IsPostBack Then
            objEntContest = objSqlContestProviderBase.GetByContestID(Request.QueryString("ID"))
            txtContestCode.Text = objEntContest.ContestCode
        End If
    End Sub
End Class

