Imports nsf.Data.SqlClient
Imports nsf.Data
Partial Class ChapterCoordinator_VenueAdd
    Inherits System.Web.UI.Page
    Dim objOgInfo As New nsf.Entities.OrganizationInfo
    Dim objSqlOrganizationInfoProviderBase As New SqlOrganizationInfoProviderBase

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim objInsOrgInfo As New nsf.Entities.OrganizationInfo
        Dim connString As String = System.Configuration.ConfigurationManager.AppSettings("DBConnection")
        Dim transactionManager As New TransactionManager(connString)
        With objInsOrgInfo
            .CreateDate = Now()
            .ModifyDate = Now()
            ' .ContestDate = Convert.ToDateTime(Server.HtmlEncode(txtAppContestDate.Text))
            .MEMBERID = String.Empty
            .MemberSince = String.Empty
            .ModifyDate = Date.Now
            .DeleteReason = String.Empty
            .LIAISONPERSON = String.Empty
            .MAILINGLABEL = String.Empty
            .SENDEMAIL = String.Empty
            .SENDNEWSLETTER = String.Empty
            .SENDRECEIPT = String.Empty
        End With
        With transactionManager
            objSqlOrganizationInfoProviderBase.Insert(transactionManager, objInsOrgInfo)
        End With
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("../login.aspx?entry=" & Session("entryToken"))
        End If
    End Sub
End Class

