Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL

Imports nsf.Entities
Imports nsf.Data.SqlClient

Partial Class ChapterCoordinator_SponsorAdd
    Inherits System.Web.UI.Page

    Private Sub GetSpouseData()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim indId As Integer

        'If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
        '    setSpouseValidators(True)
        'End If

        If Page.IsValid Then
            
            indId = UpdateIndividualInfo()

            lblMessage.Text = "Sponsor Added Successfully"
            lblMessage.Visible = True
            If Session("ParentsChapterID") Is Nothing Then
                Session("ParentsChapterID") = ddlChapterInd.SelectedItem.Value
            End If
        End If

    End Sub
    Private Function UpdateIndividualInfo() As Integer
        Dim objIndividual As New IndSpouse10
        Dim indID As Integer

        If Not Session("IndRegInfo") Is Nothing Then
            objIndividual = Session("IndRegInfo")
        End If

        With objIndividual
            .Title = IIf(ddlTitleInd.SelectedItem.Value = "", DBNull.Value, ddlTitleInd.SelectedItem.Value)
            .FirstName = txtFirstNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtFirstNameInd.Text.Substring(1))
            .LastName = txtLastNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtLastNameInd.Text.Substring(1))
            .DonorType = "IND"
            .Address1 = Server.HtmlEncode(txtAddress1Ind.Text)
            .Address2 = Server.HtmlEncode(txtAddress2Ind.Text)
            .City = Server.HtmlEncode(txtCityInd.Text)
            .State = ddlStateInd.SelectedValue
            .Zip = Server.HtmlEncode(txtZipInd.Text)
            .Email = Session("LoginEmail")
            .HPhone = Server.HtmlEncode(txtHomePhoneInd.Text)
            .CPhone = Server.HtmlEncode(txtCellPhoneInd.Text)
            .Country = ddlCountryInd.SelectedValue
            .Gender = ddlGenderInd.SelectedValue
            .WPhone = Server.HtmlEncode(txtWorkPhoneInd.Text)
            .WFax = Server.HtmlEncode(txtWorkFaxInd.Text)
            .SecondaryEmail = Server.HtmlEncode(txtSecondaryEmailInd.Text)
            .Education = Server.HtmlEncode(ddlEducationalInd.SelectedValue)
            .Career = Server.HtmlEncode(ddlCareerInd.SelectedValue)
            .Employer = Server.HtmlEncode(txtEmployerInd.Text)
            .CountryOfOrigin = ddlCountryOfOriginInd.SelectedValue
            If .CountryOfOrigin = "IN" Or .CountryOfOrigin = "US" Then
                .StateOfOrigin = Me.ddlStateOfOriginInd.SelectedValue
            Else
                .StateOfOrigin = Server.HtmlEncode(txtStateOfOriginInd.Text)
            End If

            .VolunteerFlag = rbVolunteerInd.SelectedValue
            .ReferredBy = Server.HtmlEncode(ddlReferredByInd.SelectedValue)
            .Chapter = ddlChapterInd.SelectedValue
            .MaritalStatus = ddlMaritalStatus.SelectedValue
            .Liaison = rbLiaisonInd.SelectedValue
            .Sponsor = rbSponsorInd.SelectedValue

            '.NewsLetter = rblSendNewsletterInd.SelectedValue
            '.MailingLabel = rblMailingLabelInd.SelectedValue
            '.SendReceipt = rblSendReceiptInd.SelectedValue
            '.LiasonPerson = Server.HtmlEncode(txtLiaisonInd.Text)
            If .CreateDate = Nothing Then
                .CreateDate = Now
            End If
            If .MemberSince = Nothing Then
                .MemberSince = Now
            End If
            .ModifyDate = Now
            Session("ParentLName") = .LastName
            Session("ParentFName") = .FirstName
            .Address1_Old = String.Empty
            .Address2_Old = String.Empty
            .City_old = String.Empty
            .State_Old = String.Empty
            .ZipCode_Old = String.Empty
            .PrimaryEmail_Old = String.Empty
            .HomePhone_Old = String.Empty
            .CellPhone_Old = String.Empty
            .CreateDate = Now
            .MemberSince = Now

        End With

        
        Dim objInd As New IndSpouseExt
        indID = objIndividual.AddIndSpouse(Application("ConnectionString"))



        'If objInd.CheckIndSpouseDuplicateEmail(Application("ConnectionString"), objIndividual.Email, objIndividual.LastName, objIndividual.Address1) = 0 Then
        'Else
        'Dim item
        'Dim tmpString As String
        'Dim prmArray(5) As SqlParameter
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        'Dim cmd As New SqlCommand
        'tmpString = ""
        'For Each item In Request.Form
        '    If item <> "__EVENTTARGET" And item <> "__EVENTARGUMENT" And item <> "__VIEWSTATE" Then
        '        tmpString = tmpString.ToString & item & "=" & Request.Form(item) & ", "
        '    End If
        'Next

        'cmd.Connection = conn
        'cmd.CommandType = CommandType.StoredProcedure
        'cmd.CommandText = "usp_InsertDuplicateAppExceptions"
        'conn.Open()

        'cmd.Parameters.AddWithValue("@ExceptionText", DbType.String).Value = tmpString
        'cmd.Parameters.AddWithValue("@LoginID", DbType.String).Value = Session("LoginEmail")
        'cmd.Parameters.AddWithValue("@IndId", DbType.UInt32).Value = IIf(Session("CustIndID") Is Nothing, 0, Session("CustIndID"))
        'cmd.Parameters.AddWithValue("@SpouseID", DbType.UInt32).Value = IIf(Session("SpouseID") Is Nothing, 0, Session("SpouseID"))
        'cmd.ExecuteNonQuery()

        'conn.Close()

        'Response.Redirect("DuplicateReg.aspx?dup=Primary")
        'End If
        'End If
        Session("IndRegInfo") = objIndividual
        Return indID
    End Function
    
    Private Sub ddlCountryOfOriginInd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountryOfOriginInd.SelectedIndexChanged
        If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
            txtStateOfOriginInd.Visible = False
        Else
            Me.ddlStateOfOriginInd.Visible = False
            Me.txtStateOfOriginInd.Visible = True
        End If
    End Sub
    Private Function loadStates(ByRef ddlControl As DropDownList, ByVal p_country As String) As Boolean
        Dim rtnValue As Boolean
        If p_country = "IN" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet

            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
            If dsIndiaStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsIndiaStates.Tables(0)
                ddlControl.DataTextField = dsIndiaStates.Tables(0).Columns("StateName").ToString
                ddlControl.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        ElseIf p_country = "US" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsStates As DataSet

            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsStates.Tables(0)
                ddlControl.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlControl.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        End If
        Return rtnValue
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
            Server.Transfer("login.aspx")
        End If

        Dim objIndSpouse As New IndSpouse10


        If Page.IsPostBack = False Then
            Page.MaintainScrollPositionOnPostBack = True
            '*** Populate State DropDown
            Dim dsStates As DataSet

            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlStateInd.DataSource = dsStates.Tables(0)
                ddlStateInd.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlStateInd.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlStateInd.DataBind()
                ddlStateInd.Items.Insert(0, New ListItem("Select State", String.Empty))

            End If

            '*** Populate Chapter DropDown
            '*** Populate Chapter Names List
            'Dim objChapters As New NorthSouth.BAL.Chapter
            'Dim dsChapters As New DataSet
            'objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            'If dsChapters.Tables.Count > 0 Then
            '    ddlChapterInd.DataSource = dsChapters.Tables(0)
            '    ddlChapterInd.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
            '    ddlChapterInd.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
            '    ddlChapterInd.DataBind()
            '    ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            'End If
                ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            txtPrimaryEmailInd.ReadOnly = False
            ddlStateOfOriginInd.Visible = True
            txtStateOfOriginInd.Visible = False

            loadStates(Me.ddlStateOfOriginInd, "IN")
            lblMessage.Visible = False

        End If
    End Sub

End Class


