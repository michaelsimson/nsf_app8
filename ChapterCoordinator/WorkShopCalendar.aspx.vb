Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ChapterCoordinator_WorkShopCalendar
    Inherits System.Web.UI.Page

    Private mChapterCity As String
    Private mChapterState As String
    Private mChapterCode As String

    Private mdtWkShopDate1 As Date
    Private mdtWkShopDate2 As Date
    Private mdtRegDeadLine As Date
    Private mintSponsorID As Integer
    Private mstrSponsorType As String
    Private mintEventID As Integer
    Private mstrEventCode As String
    Private mintProductGroupID As Integer
    Private mstrProductGroupcode As String
    Private mintProductID As Integer
    Private mstrProductCode As String
    Private mRegFee As Double
    Private mLateFee As Double

    Private mstrStartTime As String
    Private mstrEndTime As String
    Private mstrCheckInTime As String
    Private mstrBuilding As String
    Private mstrRoom As String
    Private mintVenueID As Integer

    Private mintEventFeesID As Integer
    Private strRegDeadLine As String
    Private strWkShopDate As String
    Private strBuilding As String
    Private strRoom As String

    Private Shared mintEventCalendarID As Integer

    Protected strVenue As String
    Protected strCheckInTime As String
    Protected strStartTime As String
    Protected strEndTime As String
    Protected strCopyFromContest As String
    Protected strWorkShopDt As String
    Protected strSponsor As String
    Protected strSponsorID As String
    Protected strSponsorType As String

    Dim cnTemp As SqlConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Debug()
        'Session("LoggedIn") = "True"
        'Session("LoginChapterID") = 48
        'Session("EventID") = 3

        ''admin
        'Session("LoginID") = 9210
        'Session("LoginRole") = "Admin"
        'chapter

        'Session("LoginID") = 2379
        'Session("LoginRole") = "ChapterC"


        ' ''Debug for National Contest
        ''Session("EventID") = 1
        ''Session("LoginID") = 4009
        ''Session("LoginRole") = "NationalC"
        ''If Session("EventID") = 1 Then Session("LoginChapterID") = 1

        cnTemp = New SqlConnection(Application("ConnectionString"))

        If Not Page.IsPostBack Then
            lstWorkShops.Attributes.Add("onchange", "EnableDisableEvents")

            If LCase(Session("LoggedIn")) <> "true" Then
                Response.Redirect("..\login.aspx?entry=" & Session("entryToken"))
            End If

            drpYear.Items.Add(Year(DateTime.Now).ToString())
            drpYear.Items.Add(Integer.Parse(Year(DateTime.Now).ToString()) + 1)


            DisplayHeadings()
            LoadDropDowns()
            LoadlstWorkShops(Integer.Parse(drpYear.SelectedItem.Text))
            LoadGrid()

            lblErr.Text = Session("LoginChapterCode")
            lblErr.Visible = True
        End If

    End Sub

    Private Sub DisplayHeadings()
        If Not Session("LoginChapterID") Is Nothing Then
            Dim drChapter As SqlDataReader
            If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
            drChapter = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_Chapter_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If (drChapter.HasRows()) Then
                Do While (drChapter.Read())
                    mChapterCity = drChapter("City")
                    mChapterState = drChapter("State")
                    mChapterCode = drChapter("ChapterCode")
                    Session("LoginChapterCode") = mChapterCode
                Loop
            End If
            drChapter.Close()
        End If

        If Session("EventID") = 1 Then ' national
            lblHeading1.Text = "National Contests " & Application("ContestYear")
            If Not Application("NationalFinalsCity") Is Nothing Then
                lblHeading2.Text = Application("NationalFinalsCity")
                lblHeading2.Visible = True
            Else
                lblHeading2.Visible = False
            End If
        Else '2, 3 for regional, workshop
            lblHeading1.Text = "WorkShop "  '& " EventID=" & Session("EventID") & " ChapterID=" & Session("LoginChapterID") & " EventLoginID=" & Session("EventLoginID")
            lblHeading2.Text = mChapterCity & ", " & mChapterState
            lblHeading2.Visible = True
        End If
    End Sub

    Private Sub LoadlstWorkShops(ByVal selYear As Integer)
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from EventCalendar where  ChapterId=" & Session("LoginChapterID") & " AND EventId=3 and EventYear=" & selYear & " and EventDate < GETDATE()") > 0 Then
            btnAdd.Attributes.Add("onclick", "return confirm('This workshop is a repeat this year.  Please confirm.');")
        Else
            btnAdd.Attributes.Remove("onclick")
        End If
        Dim dsWorkshops As DataSet
        Dim param(3) As SqlParameter
        param(1) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(2) = New SqlParameter("@EventID", Session("EventID"))
        param(3) = New SqlParameter("@selYear", selYear)
        dsWorkshops = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_GetWorkshopsByYear", param)
        'Dim dvTemp As DataView = dsWorkshops.Tables(0).DefaultView
        'dvTemp.RowFilter = "Status = 'O'"
        lstWorkShops.DataSource = dsWorkshops
        lstWorkShops.DataBind()
        If lstWorkShops.Items.Count > 0 Then
            btnAdd.Visible = True
        Else
            btnAdd.Visible = False
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim intCtr As Integer

        ' If txtWkShopDt1.Text = "" Then Exit Sub
        'If IsDate(txtWkShopDt1.Text) = False Then Exit Sub
        lblDataErr.Visible = False
        lblDataErr.Text = ""
        lblError.Visible = False
        If IsDate(txtWkShopDt1.Text) Then
            If Not CDate(txtWkShopDt1.Text) > Today() Then
                lblDataErr.Text = "Event date should be greater than today"
                lblDataErr.Visible = True
                Exit Sub
            End If
        Else
            lblDataErr.Text = "Please enter a valid Event Date"
            lblDataErr.Visible = True
            Exit Sub
        End If

        If ddlVenue.SelectedIndex < 0 Then
            lblDataErr.Text = "Please select a Venue from the dropdown"
            lblDataErr.Visible = True

            Exit Sub
        End If

        lblError.Text = " <br>Following events are rejected as they were scheduled or less than one month from last event "
        lblError.Visible = False
        For intCtr = 0 To lstWorkShops.Items.Count - 1
            lblDataErr.Text = ""
            If lstWorkShops.Items(intCtr).Selected = True Then
                GetWorkShopInfo(lstWorkShops.Items(intCtr).Value)
                mdtWkShopDate1 = txtWkShopDt1.Text
                mdtWkShopDate2 = mdtWkShopDate1.AddDays(+1).Date
                mdtRegDeadLine = mdtWkShopDate1.AddDays(-8).Date
                'Validate the Input Fields Before inserting into the Databse

                Dim intSelCheckInTime, intSelStartTime, intSelEndTime As Integer

                'If txtCheckinTime.Text().Trim().Length > 0 Then
                '    If DateTime.TryParseExact(txtCheckinTime.Text, "hh:mm", Nothing, Nothing, SelCheckInTime) Then
                '        'nothing
                '    Else
                '        lblDataErr.Text = "Please Enter a valid Check In Time in 12 hour format"
                '        lblDataErr.Visible = True
                '        Exit Sub
                '    End If
                'Else
                '    lblDataErr.Text = "Please Enter a valid Check In Time in 12 hour format"
                '    lblDataErr.Visible = True
                '    Exit Sub

                'End If

                'If txtStartTime.Text().Trim().Length > 0 Then
                '    If DateTime.TryParseExact(txtStartTime.Text, "hh:mm", Nothing, Nothing, SelStartTime) Then
                '        'nothing
                '    Else
                '        lblDataErr.Text = "Please Enter a valid Start Time in 12 hour format"
                '        lblDataErr.Visible = True
                '        Exit Sub
                '    End If
                'Else
                '    lblDataErr.Text = "Please Enter a valid Check In Time in 12 hour format"
                '    lblDataErr.Visible = True
                '    Exit Sub

                'End If



                'If txtEndTime.Text().Trim().Length > 0 Then
                '    If DateTime.TryParseExact(txtEndTime.Text, "hh:mm", Nothing, Nothing, SelEndTime) Then
                '        'nothing
                '    Else
                '        lblDataErr.Text = "Please Enter a valid End Time in 12 hour format"
                '        lblDataErr.Visible = True
                '        Exit Sub
                '    End If
                'Else
                '    lblDataErr.Text = "Please Enter a valid Check In Time in 12 hour format"
                '    lblDataErr.Visible = True
                '    Exit Sub

                'End If


                'If SelCheckInTime >= SelStartTime Then
                '    lblDataErr.Text = " Check In Time should be before Start Time."
                '    lblDataErr.Visible = True
                '    Exit Sub
                'End If


                'If SelCheckInTime >= SelEndTime Then
                '    lblDataErr.Text = " Check In Time should be before End Time."
                '    lblDataErr.Visible = True
                '    Exit Sub
                'End If

                'If SelEndTime <= SelStartTime Then
                '    lblDataErr.Text = " Start Time should be before End Time."
                '    lblDataErr.Visible = True
                '    Exit Sub
                'End If

                'If SelEndTime <= SelCheckInTime Then
                '    lblDataErr.Text = " Check In Time should be before End Time."
                '    lblDataErr.Visible = True
                '    Exit Sub
                'End If
                intSelCheckInTime = ddlCheckInTime1.SelectedIndex
                intSelStartTime = 1 + ddlStartTime1.SelectedIndex
                intSelEndTime = 3 + ddlEndTime1.SelectedIndex

                If intSelCheckInTime >= intSelStartTime Then lblDataErr.Text = " Check In Time should be before Start Time."
                If intSelCheckInTime >= intSelEndTime Then lblDataErr.Text = " Check In Time should be before End Time."
                If intSelEndTime <= intSelStartTime Then lblDataErr.Text = " Start Time should be before End Time."
                If intSelEndTime <= intSelCheckInTime Then lblDataErr.Text = " Check In Time should be before End Time."
                If lblDataErr.Text <> "" Then
                    lblDataErr.Visible = True
                    Exit Sub
                End If
                If intSelStartTime > intSelCheckInTime And intSelStartTime < intSelEndTime Then
                    'nothing
                Else
                    lblDataErr.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                    lblDataErr.Visible = True
                    Exit Sub
                End If
                If (intSelEndTime - intSelStartTime) >= 2 Then
                    'nothing
                Else
                    lblDataErr.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                    lblDataErr.Visible = True
                    Exit Sub
                End If

                'If ddlSponsor.SelectedIndex < 0 Then
                '    lblDataErr.Text = "Please Select a Sponser"
                '    lblDataErr.Visible = True
                '    Exit Sub

                'End If
                If Right(lstWorkShops.Items(intCtr).Text, 4) <> Year(DateTime.Parse(txtWkShopDt1.Text)).ToString() Then
                    lblDataErr.Text = " Year of the Event Day should be  same as Workshop year"
                    lblDataErr.Visible = True
                    Exit Sub
                End If
                InsertWorkShopCalendar(lstWorkShops.Items(intCtr).Value)
            End If
        Next
        txtWkShopDt1.Text = ""
        txtEDDesc.Text = ""
        txtBldg.Text = ""
        LoadGrid()
        LoadlstWorkShops(Integer.Parse(drpYear.SelectedItem.Text))

    End Sub

    Private Sub GetWorkShopInfo(ByVal EventFeesID As Integer)
        Dim drWkShop As SqlDataReader
        drWkShop = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_EventFees_GetByEventFeesId", New SqlParameter("@EventFeesID", EventFeesID))
        If (drWkShop.HasRows()) Then
            Do While (drWkShop.Read())
                mintEventID = drWkShop("EventID")
                mstrEventCode = drWkShop("EventCode")
                mintProductGroupID = drWkShop("ProductGroupID")
                mstrProductGroupcode = drWkShop("ProductGroupCode")
                mintProductID = drWkShop("ProductID")
                mstrProductCode = drWkShop("ProductCode")
                mRegFee = drWkShop("RegFee")
                mLateFee = drWkShop("LateFee")
            Loop
        End If
        drWkShop.Close()
    End Sub

    Private Sub LoadDropDowns()
        ddlVenue.Items.Clear()
        Dim dsVenue As DataSet
        dsVenue = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsVenue.Tables(0).Rows.Count > 0 Then
            ddlVenue.DataSource = dsVenue
            ddlVenue.DataBind()
            'If ddlVenue.Items.Count > 0 Then
            '    ddlVenue.Items.Insert(0, "")
            'End If
        Else
            ' ddlVenue.Items.Insert(0, "")
        End If

        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
            ddlSponsor.DataSource = dvSponsor
            ddlSponsor.DataBind()
            If ddlSponsor.Items.Count > 0 Then
                ddlSponsor.Items.Insert(0, "")
            End If
        End If

        'If Session("EventID") = 3 Then 'workshops
        '    Dim dsContestDt As DataSet
        '    dsContestDt = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_WeekCalendar_Get_List", New SqlParameter("@ContestDt", FormatDateTime("1/1/" & Application("ContestYear"), DateFormat.ShortDate)))
        '    ddlContestDt.DataSource = dsContestDt
        '    ddlContestDt.DataBind()
        '    ddlContestDt.Items.Insert(0, "")
        '    If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
        '    Else
        '        If ddlContestDt.Items.Count > 13 Then
        '            Do
        '                ddlContestDt.Items.RemoveAt(13)
        '            Loop Until ddlContestDt.Items.Count = 13
        '        End If
        '    End If
        'Else
        '    Dim dtCheckDates As Date
        '    dtCheckDates = "8/1/" & Application("ContestYear")
        '    Do
        '        If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
        '            ddlContestDt.Items.Add(dtCheckDates)
        '        End If
        '        dtCheckDates = dtCheckDates.AddDays(1)
        '    Loop Until Day(dtCheckDates) = 31
        'End If

    End Sub
    Private Sub FillDropDown()
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
            ddlSponsor.DataSource = dvSponsor
            ddlSponsor.DataBind()
            If ddlSponsor.Items.Count > 0 Then
                ddlSponsor.Items.Insert(0, "")
            End If
        End If
    End Sub

    Private Function InsertWorkShopCalendar(ByVal EventFeesID As Integer) As Boolean
        lblErr.Text = Session("LoginChapterCode")
        lblErr.Visible = True
        Try
            Dim param(25) As SqlParameter
            param(0) = New SqlParameter("@EventID", mintEventID)
            param(1) = New SqlParameter("@EventCode", mstrEventCode)
            param(2) = New SqlParameter("@ProductGroupID", mintProductGroupID)
            param(3) = New SqlParameter("@ProductGroupCode", mstrProductGroupcode)
            param(4) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
            param(5) = New SqlParameter("@ChapterCode", Session("LoginChapterCode"))
            param(6) = New SqlParameter("@ProductID", mintProductID)
            param(7) = New SqlParameter("@ProductCode", mstrProductCode)
            param(8) = New SqlParameter("@EventYear", drpYear.SelectedValue)
            param(9) = New SqlParameter("@EventDate", mdtWkShopDate1)
            param(10) = New SqlParameter("@EventDesc", txtEDDesc.Text)
            'param(11) = New SqlParameter("@EventDay2", mdtWkShopDate2)
            param(11) = New SqlParameter("@EventFee", mRegFee)

            param(12) = New SqlParameter("@CheckinTime", ddlCheckInTime1.SelectedItem.Text)
            param(13) = New SqlParameter("@StartTime", ddlStartTime1.SelectedItem.Text)
            param(14) = New SqlParameter("@EndTime", ddlEndTime1.SelectedItem.Text)
            param(15) = New SqlParameter("@Building", txtBldg.Text)
            param(16) = New SqlParameter("@LateFee", mLateFee)

            If ddlVenue.SelectedIndex < 0 Then
                param(17) = New SqlParameter("@VenueID", DBNull.Value)
            Else
                param(17) = New SqlParameter("@VenueID", ddlVenue.SelectedValue)
            End If


            param(18) = New SqlParameter("@CreateDate", Now)
            param(19) = New SqlParameter("@CreatedBy", Session("LoginID"))
            param(20) = New SqlParameter("@EventFeesID", EventFeesID)
            param(21) = New SqlParameter("@Room", DBNull.Value)
            param(22) = New SqlParameter("@RegDeadline", mdtRegDeadLine)

            param(23) = New SqlParameter("@SponsorId", ddlSponsor.SelectedValue)
            param(24) = New SqlParameter("@SponsorType", rbtnMemberType.SelectedValue)

            param(25) = New SqlParameter("@ECalendarID", SqlDbType.Int)
            param(25).Direction = ParameterDirection.Output
            Dim i As Integer = 0
            i = SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertWorkShop_EventCalendar", param)
            If i > 0 Then
                lblDataErr.Text = "Added successfully"
                lblDataErr.Visible = True
            Else
                lblError.Visible = True
                lblError.Text = lblError.Text + "<br> Product Code :" & mstrProductCode & " Event Date : " & mdtWkShopDate1
            End If
           
            Return True
        Catch ex As SqlException
            lblDataErr.Text = ex.Message
            lblDataErr.Visible = True
            Return False
        End Try
    End Function

    Protected Sub grdTarget_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.CancelCommand
        grdTarget.EditItemIndex = -1
        LoadGrid()
        lblGridError.Visible = False
    End Sub

    Protected Sub grdTarget_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.DeleteCommand
        Dim param(1) As SqlParameter

        Try
            lblDataErr.Text = ""
            If e.CommandName = "Delete" Then

                grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
                mintEventCalendarID = CInt(e.Item.Cells(0).Text)
                mintEventFeesID = CInt(e.Item.Cells(1).Text)

                param(0) = New SqlParameter("@ECalendarID", mintEventCalendarID)
                'delete routine
                SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "usp_DeleteWorkShop_EventCalendar", param)
                lblDataErr.Text = "Deleted Successfully"
                lblDataErr.Visible = True
                LoadlstWorkShops(Integer.Parse(drpYear.SelectedItem.Text))
                LoadGrid("Del")

            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub grdTarget_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.EditCommand
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        mintEventCalendarID = CInt(e.Item.Cells(0).Text)
        mintEventFeesID = CInt(e.Item.Cells(1).Text)
        strSponsor = CType(e.Item.Cells(3).FindControl("lblContestSponsor"), Label).Text
        strSponsorID = CType(e.Item.Cells(3).FindControl("lblContestSponsorID"), Label).Text
        strSponsorType = CType(e.Item.Cells(3).FindControl("lblContestSponsorType"), Label).Text
        strVenue = CType(e.Item.Cells(3).FindControl("lblWkShopVenue"), Label).Text
        strRegDeadLine = CType(e.Item.Cells(4).FindControl("lblRegDeadLine"), Label).Text
        strCheckInTime = CType(e.Item.Cells(5).FindControl("lblCheckInTime"), Label).Text
        strWkShopDate = CType(e.Item.Cells(6).FindControl("lblWkShopDate"), Label).Text
        strStartTime = CType(e.Item.Cells(7).FindControl("lblStartTime"), Label).Text
        strEndTime = CType(e.Item.Cells(8).FindControl("lblEndTime"), Label).Text
        strBuilding = CType(e.Item.Cells(9).FindControl("lblBuilding"), Label).Text
        strRoom = CType(e.Item.Cells(10).FindControl("lblRoom"), Label).Text
        'disenable delete and copy buttons
        Dim intcell As Integer = e.Item.Cells.Count - 1
        Dim myTableCell As TableCell
        myTableCell = e.Item.Cells(intcell)
        If Not CType(e.Item.Cells(intcell).FindControl("btnDelete"), Button) Is Nothing Then
            CType(e.Item.Cells(intcell).FindControl("btnDelete"), Button).Enabled = False
        End If
        LoadGrid()
    End Sub

    Protected Sub grdTarget_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTarget.ItemDataBound
        Dim dsTarget As New DataSet

        ' First, make sure we're NOT dealing with a Header or Footer row
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            'Now, reference the PushButton control that the Delete ButtonColumn has been rendered to
            Dim rownum As Integer
            rownum = e.Item.ItemIndex + 1

            Dim intEventCalendarID As String = e.Item.Cells(0).Text

            Dim ddlVenueTemp As DropDownList
            ddlVenueTemp = e.Item.Cells(4).FindControl("ddlWkShopVenue")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If Not ddlVenueTemp Is Nothing Then
                ddlVenueTemp.DataSource = dsTarget
                ddlVenueTemp.DataBind()
            End If

            Dim ddlSponsorTemp As DropDownList, ddlSponsorTypeTemp As DropDownList
            ddlSponsorTemp = e.Item.Cells(3).FindControl("ddlContestSponsor")
            ddlSponsorTypeTemp = e.Item.Cells(3).FindControl("ddlContestSponsorType")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If dsTarget.Tables(0).Rows.Count > 0 Then
                Dim dvSponsor As New DataView(dsTarget.Tables(0), "", "SponsorName", DataViewRowState.CurrentRows)
                If Not ddlSponsorTemp Is Nothing Then
                    ddlSponsorTemp.DataSource = dvSponsor
                    ddlSponsorTemp.DataBind()
                    If ddlSponsorTemp.Items.Count > 0 Then
                        ddlSponsorTemp.Items.Insert(0, "")
                    End If
                End If
                If Not ddlSponsorTypeTemp Is Nothing Then
                    ddlSponsorTypeTemp.DataSource = dvSponsor
                    ddlSponsorTypeTemp.DataBind()
                    If ddlSponsorTypeTemp.Items.Count > 0 Then
                        ddlSponsorTypeTemp.Items.Insert(0, "")
                    End If
                End If
            End If

            Dim delcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(delcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")

            Dim intEventCalendarIDCell As Integer = 1
            Dim EventCalendarIDCell As TableCell = e.Item.Cells(intEventCalendarIDCell)
            Dim strValue As String = EventCalendarIDCell.Text
        End If

    End Sub

    Private Sub LoadGrid()
        Dim dsWorkshops As New DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(1) = New SqlParameter("@EventID", Session("EventID"))
        dsWorkshops = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_EventCalendar_GetEventsByChapterID", param)
        grdTarget.Visible = False
        If Not dsWorkshops Is Nothing Then
            If dsWorkshops.Tables(0).Rows.Count > 0 Then
                grdTarget.DataSource = dsWorkshops
                grdTarget.DataBind()
                grdTarget.Visible = True
            End If
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_EventCalendar_GetEventsByChapterIDPast", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdTargetOld.DataSource = ds
            grdTargetOld.DataBind()
            grdTargetOld.Visible = True
        Else
            grdTargetOld.Visible = False
        End If
    End Sub

    Private Sub LoadGrid(ByVal action As String)
        Dim dsWorkshops As New DataSet
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(1) = New SqlParameter("@EventID", Session("EventID"))
        dsWorkshops = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_EventCalendar_GetEventsByChapterID", param)

        grdTarget.Visible = False
        If Not dsWorkshops Is Nothing Then
            If dsWorkshops.Tables(0).Rows.Count > 0 Then
                grdTarget.DataSource = dsWorkshops
                grdTarget.DataBind()
                grdTarget.Visible = True
                grdTarget.EditItemIndex = -1
                If grdTarget.EditItemIndex <> -1 Then
                    Dim myTableCell As TableCell
                    myTableCell = grdTarget.Items(grdTarget.EditItemIndex).Cells(grdTarget.Items(0).Cells.Count - 1)
                    If Not CType(myTableCell.FindControl("btnCopy"), Button) Is Nothing Then
                        CType(myTableCell.FindControl("btnCopy"), Button).Enabled = False
                    End If
                End If
                'Else
                '    'If action = "Del" Then
                '    Server.Transfer("WorkShopCalendar.aspx")
                'End If



            End If
            If action = "Del" Then
                Server.Transfer("WorkShopCalendar.aspx")
            End If
        End If

    End Sub


    Public Sub SetDropDown_Venue(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strVenue))
    End Sub

    Public Sub SetDropDown_WorkShopDt(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If Trim(strWorkShopDt) <> "" Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strWorkShopDt))
        Else
            ddlTemp.Items(0).Selected = True
        End If
    End Sub

    Public Sub SetDropDown_CheckInTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strCheckInTime))
    End Sub

    Public Sub SetDropDown_StartTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strStartTime))
    End Sub

    Public Sub SetDropDown_EndTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strEndTime))
    End Sub

    Protected Sub grdTarget_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.UpdateCommand
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim txtEditText As TextBox, ddlTemp As DropDownList, blnValidData As Boolean, lblText As Label
        Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer
        'Venue
        lblDataErr.Text = ""
        lblGridError.Text = ""
        Dim blnVenueSelected As Boolean
        ddlTemp = e.Item.FindControl("ddlWkShopVenue")
        If ddlTemp.SelectedIndex < 0 Then
            blnVenueSelected = False
        Else
            blnVenueSelected = True
            mintVenueID = ddlTemp.SelectedValue

        End If

        'WorkShopDate
        txtEditText = e.Item.FindControl("txtGridEditWSDate")
        If txtEditText.Text <> "" Then
            mdtWkShopDate1 = txtEditText.Text
        Else
            lblGridError.Text = "Please Enter Registration Workshop date"
            lblGridError.Visible = True
            Exit Sub
            'mdtWkShopDate1 = strWkShopDate
        End If


        'validate wkshopdate
        lblText = e.Item.FindControl("lblGridEditWSDate")
        If lblText.Text <> "" And txtEditText.Text <> "" Then
            If Year(DateTime.Parse(lblText.Text)) <> Year(DateTime.Parse(txtEditText.Text)) Then
                lblGridError.Text = "Year of the WorkShop Date should be  same as Workshop year"
                lblGridError.Visible = True
                Exit Sub
            End If
        End If

        'RegDeadLine
        txtEditText = e.Item.FindControl("txtEditRegDeadLine")
        If txtEditText.Text <> "" Then
            mdtRegDeadLine = txtEditText.Text
        Else
            lblGridError.Text = "Please Enter Registration DeadLine date"
            lblGridError.Visible = True
            Exit Sub
        End If

        ddlTemp = e.Item.FindControl("ddlCheckInTime")
        mstrCheckInTime = ddlTemp.SelectedItem.Text
        intSelCheckInTime = ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlStartTime")
        mstrStartTime = ddlTemp.SelectedItem.Text
        intSelStartTime = 1 + ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlEndTime")
        mstrEndTime = ddlTemp.SelectedItem.Text
        intSelEndTime = 3 + ddlTemp.SelectedIndex

        txtEditText = e.Item.FindControl("txtBuilding")
        mstrBuilding = txtEditText.Text

        txtEditText = e.Item.FindControl("txtRoom")
        mstrRoom = txtEditText.Text

        blnValidData = False
        lblGridError.Visible = False
        lblGridError.Text = ""
        If mstrStartTime <> "" Then
            If intSelCheckInTime >= intSelStartTime Then lblGridError.Text = " Check In Time should be before Start Time."
            If intSelCheckInTime >= intSelEndTime Then lblGridError.Text = " Check In Time should be before End Time."
            If intSelEndTime <= intSelStartTime Then lblGridError.Text = " Start Time should be before End Time."
            If intSelEndTime <= intSelCheckInTime Then lblGridError.Text = " Check In Time should be before End Time."
            If lblGridError.Text <> "" Then
                lblGridError.Visible = True
                blnValidData = False
                GoTo FinalProcess
            End If
            If intSelStartTime > intSelCheckInTime And intSelStartTime < intSelEndTime Then
                blnValidData = True
            Else
                lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
            If (intSelEndTime - intSelStartTime) >= 2 Then
                blnValidData = True
            Else
                lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
        End If

        Dim blnSponsorSelected As Boolean, ddlTempSponsorType As DropDownList
        ddlTemp = e.Item.FindControl("ddlContestSponsor")
        ddlTempSponsorType = e.Item.FindControl("ddlContestSponsorType")
        If ddlTemp.SelectedIndex < 1 Then
            blnSponsorSelected = False
        Else
            blnSponsorSelected = True
            mintSponsorID = ddlTemp.SelectedValue
            mstrSponsorType = ddlTempSponsorType.Items(ddlTemp.SelectedIndex).Text
        End If

        'update routine
        UpdateWorkShops(blnVenueSelected, blnSponsorSelected)
        grdTarget.EditItemIndex = -1

        'disenable delete buttons
        Dim intcell As Integer = 12 ' e.Item.Cells.Count - 1
        Dim myTableCell As TableCell
        myTableCell = e.Item.Cells(intcell)
        Dim btnDelete As Button = myTableCell.Controls(0)
        btnDelete.Enabled = True

        LoadGrid()
        Exit Sub

FinalProcess:
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        LoadGrid()
    End Sub

    Private Function UpdateWorkShops(ByVal VenueSelected As Boolean, ByVal SponsorSelected As Boolean) As Boolean
        Try
            lblDataErr.Text = ""
            Dim param(12) As SqlParameter
            If VenueSelected = True Then
                param(0) = New SqlParameter("@VenueId", mintVenueID)
                If mintVenueID <> 0 Then
                    param(0) = New SqlParameter("@VenueId", mintVenueID)
                Else
                    param(0) = New SqlParameter("@VenueId", DBNull.Value)
                End If
            Else
                param(0) = New SqlParameter("@VenueId", DBNull.Value)
            End If

            param(1) = New SqlParameter("@EventDate", mdtWkShopDate1)
            param(2) = New SqlParameter("@RegDeadline", mdtRegDeadLine)
            param(3) = New SqlParameter("@CheckinTime", mstrCheckInTime)
            param(4) = New SqlParameter("@StartTime", mstrStartTime)
            param(5) = New SqlParameter("@EndTime", mstrEndTime)
            param(6) = New SqlParameter("@Building", mstrBuilding)
            param(7) = New SqlParameter("@Room", mstrRoom)
            param(8) = New SqlParameter("@ModifiedDate", Now)
            param(9) = New SqlParameter("@ModifiedBy", Session("LoginID"))
            'param(10) = New SqlParameter("@EventFeesID", mintEventFeesID)
            param(10) = New SqlParameter("@EcalendarID", mintEventCalendarID)
            If SponsorSelected = True Then
                If mintSponsorID <> 0 Then
                    param(11) = New SqlParameter("@SponsorId", mintSponsorID)
                    param(12) = New SqlParameter("@SponsorType", mstrSponsorType)
                Else
                    param(11) = New SqlParameter("@SponsorId", DBNull.Value)
                    param(12) = New SqlParameter("@SponsorType", DBNull.Value)
                End If
            Else
                param(11) = New SqlParameter("@SponsorId", DBNull.Value)
                param(12) = New SqlParameter("@SponsorType", DBNull.Value)
            End If

            SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_UpdateWorkShop_EventCalendar", param)
            lblDataErr.Text = "Updated successfully"
            lblDataErr.Visible = True
            Return True
        Catch ex As SqlException
            lblDataErr.Text = ex.Message
            lblDataErr.Visible = True
            Return False
        End Try

    End Function
    Public Sub SetDropDown_Sponsor(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor)))
        If ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor))) <= 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Trim(strSponsorID)))
        End If
    End Sub
    Protected Sub rbtnMemberType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnMemberType.SelectedIndexChanged
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Select Case rbtnMemberType.SelectedIndex
                Case 0
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()

                Case 1
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'I'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()

            End Select
        End If
    End Sub

    Protected Sub lstWorkShops_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstWorkShops.SelectedIndexChanged

        Dim strSelected As String
        Dim strSelecteYear As String
        strSelecteYear = ""
        For Each lst As ListItem In lstWorkShops.Items
            If lst.Selected = True Then
                strSelected = lst.Text
                strSelecteYear = strSelected.Substring(strSelected.Length - 4, 4)
                Exit For
            End If

        Next
        'disableCheckboxes(strSelecteYear)
    End Sub

    Private Sub disableCheckboxes(ByVal stryear As String)


        For Each lst As ListItem In lstWorkShops.Items
            If lst.Text.Substring(lst.Text.Length - 4, 4) <> stryear And lst.Selected = False Then
                lst.Enabled = False
            Else
                lst.Enabled = True
            End If

        Next

    End Sub

    Protected Sub drpYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpYear.SelectedIndexChanged
        LoadlstWorkShops(Integer.Parse(drpYear.SelectedItem.Text))
    End Sub
End Class
