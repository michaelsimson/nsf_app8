
Partial Class ChapterCoordinator_ChapterFunctions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("../login.aspx?entry=" & Session("entryToken"))
            End If
        End If
        Session("LoginEventID") = 2 'for chapter/regionals
    End Sub
End Class

