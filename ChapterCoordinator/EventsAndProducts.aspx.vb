Imports System.Data.SqlClient
'Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data

Imports nsf.Data.SqlClient

Partial Class ChapterCoordinator_EventsAndProducts
    Inherits System.Web.UI.Page
    Private mChapterCity As String
    Private mChapterState As String

    Private Shared mintContestID As Integer
    Private Shared mintContestCategoryID As Integer
    Private mdtContestDate As Date
    Private mstrStartTime As String
    Private mstrEndTime As String
    Private mstrCheckInTime As String
    Private mstrContestTime As String
    Private mdtRegDeadLine As Date
    Private mstrBuilding As String
    Private mstrRoom As String
    Private mintVenueID As Integer
    Private mintSponsorID As Integer
    Private mstrSponsorType As String
    Private mintProductID As Integer
    Private mstrProductCode As String
    Private mintProductGroupID As Integer
    Private mstrProductGroupcode As String
    Private mintEventID As Integer
    Private mstrEventCode As String

    Protected strSponsor As String
    Protected strSponsorID As String
    Protected strSponsorType As String
    Protected strVenue As String
    Protected strCheckInTime As String
    Protected strStartTime As String
    Protected strEndTime As String
    Protected strCopyFromContest As String
    Protected strContestDt As String

    Dim cnTemp As SqlConnection
    'Dim objNSFChapter As New Chapter
    'Dim objNSFContest As New Contest

    Dim objEntChapter As New nsf.Entities.Chapter
    Dim objEntContest As New nsf.Entities.Contest
    Dim objSqlChapterProviderBase As New SqlChapterProviderBase
    Dim objSqlContestProviderBase As New SqlContestProviderBase
    Dim objSqlIndSpouseProviderBase As New SqlIndSpouseProviderBase
    Dim objSqlOrgInfoProviderBase As New SqlOrganizationInfoProviderBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Debug()
        ''Session("LoggedIn") = "True"
        ''Session("LoginChapterID") = 48
        ''Session("EventID") = 2
        ''admin
        'Session("LoginID") = 9210
        'Session("LoginRole") = "Admin"
        ' ''chapter
        ''Session("LoginID") = 2379
        ''Session("LoginRole") = "ChapterC"

        ' ''Debug for National Contest
        ''Session("EventID") = 1
        ''Session("LoginID") = 4009
        ''Session("LoginRole") = "NationalC"
        ''If Session("EventID") = 1 Then Session("LoginChapterID") = 1

        cnTemp = New SqlConnection(Application("ConnectionString"))

        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("../login.aspx?entry=" & Session("entryToken"))
            End If

            DisplayHeadings()
            LoadDropDowns()
            LoadGrid()
            LoadlstContests()
        End If
        lblErr.Visible = True


        If grdTarget.Items.Count > 0 Then
            btnHide.Text = "Show me How to do add Contests"
            pnlMsg.Visible = False
        Else
            btnHide.Text = "Hide the description"
            pnlMsg.Visible = True
        End If
    End Sub

    Private Sub DisplayHeadings()
        'If Not Session("ChapterID") Is Nothing Then
        '    objSqlChapterProviderBase.ConnectionString = Application("ConnectionString")
        '    objSqlChapterProviderBase.UseStoredProcedure = True
        '    objEntChapter = objSqlChapterProviderBase.GetByChapterID(Session("ChapterID"))
        '    mChapterCity = objEntChapter.City
        '    mChapterState = objEntChapter.State
        'End If

        'If Session("LoginChapterID") Is Nothing Then
        '    Dim drVolunteer As SqlDataReader
        '    If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        '    drVolunteer = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "Volunteer_GetByVolunteerID", New SqlParameter("@VolunteerId", Session("LoginID")))
        '    If (drVolunteer.HasRows()) Then
        '        Do While (drVolunteer.Read())
        '            Session("ChapterID") = drVolunteer("ChapterID")
        '        Loop
        '    End If
        '    drVolunteer.Close()
        'End If

        If Not Session("LoginChapterID") Is Nothing Then
            Dim drChapter As SqlDataReader
            If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
            drChapter = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_Chapter_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If (drChapter.HasRows()) Then
                Do While (drChapter.Read())
                    mChapterCity = drChapter("City")
                    mChapterState = drChapter("State")
                Loop
            End If
            drChapter.Close()
        End If

        If Session("EventID") = 1 Then ' national
            lblHeading1.Text = "National Contests " & Session("EventYear")
            If Not Application("NationalFinalsCity") Is Nothing Then
                lblHeading2.Text = Application("NationalFinalsCity")
                lblHeading2.Visible = True
            Else
                lblHeading2.Visible = False
            End If
        Else '2 for regional
            lblHeading1.Text = "Regional Contests " & Session("EventYear") '& " EventID=" & Session("EventID") & " ChapterID=" & Session("LoginChapterID") & " EventLoginID=" & Session("EventLoginID")
            lblHeading2.Text = mChapterCity & ", " & mChapterState
            lblHeading2.Visible = True
        End If
    End Sub

    Private Sub LoadGrid()
        'Dim lstEntContests As nsf.Entities.TList(Of nsf.Entities.Contest)
        'objSqlContestProviderBase.ConnectionString = Application("ConnectionString")
        'objSqlContestProviderBase.UseStoredProcedure = True
        'lstEntContests = objSqlContestProviderBase.GetAll()
        'Dim dsTarget As New DataSet

        'If Not Session("ChapterID") Is Nothing Then
        '    lstEntContests.Filter = "ChapterID = 49"
        'End If
        'grdTarget.DataSource = lstEntContests
        'grdTarget.DataBind()

        'If grdTarget.Items.Count < 1 Then
        '    grdTarget.Visible = False
        'Else
        '    grdTarget.Visible = True
        'End If


        Dim dsContests As New DataSet
        Dim param(2) As SqlParameter

        If Session("EventID") = 1 Then
            'Nationals
            param(0) = New SqlParameter("@EventID", Session("EventID"))
            param(1) = New SqlParameter("@ContestYear", Session("EventYear"))
            dsContests = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Contest_Get_List", param)
        Else
            'Regionals
            param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
            param(1) = New SqlParameter("@ContestYear", Session("EventYear"))
            param(2) = New SqlParameter("@EventID", Session("EventID"))
            'MsgBox(Session("EventYear"))
            dsContests = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Contest_GetByChapterID", param)
        End If

        pnlMsg.Visible = True
        grdTarget.Visible = False
        If Not dsContests Is Nothing Then
            If dsContests.Tables(0).Rows.Count > 0 Then
                grdTarget.DataSource = dsContests
                grdTarget.DataBind()
                grdTarget.Visible = True
                pnlMsg.Visible = False

                If grdTarget.EditItemIndex <> -1 Then
                    Dim myTableCell As TableCell
                    myTableCell = grdTarget.Items(grdTarget.EditItemIndex).Cells(grdTarget.Items(0).Cells.Count - 1)
                    If Not CType(myTableCell.FindControl("btnCopy"), Button) Is Nothing Then
                        CType(myTableCell.FindControl("btnCopy"), Button).Enabled = False
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub LoadlstContests()
        Dim dsContests As DataSet
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@EventID", Session("EventID"))
        param(1) = New SqlParameter("@ContestYear", Session("EventYear"))
        param(2) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        dsContests = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Product_GetEvents", param)
        Dim dvTemp As DataView = dsContests.Tables(0).DefaultView
        dvTemp.RowFilter = "Status = 'O'"
        lstContests.DataSource = dvTemp
        lstContests.DataBind()
        MsgBox(Session("EventID") & " " & Session("EventYear") & " " & Session("LoginChapterID") & " " & lstContests.Items.Count)
        If lstContests.Items.Count > 0 Then
            btnAdd.Visible = True
        Else
            btnAdd.Visible = False
        End If
    End Sub

    Private Sub LoadDropDowns()
        'Dim lstEntIndSpouse As nsf.Entities.TList(Of nsf.Entities.IndSpouse)
        'lstEntIndSpouse = objSqlIndSpouseProviderBase.GetAll()

        'If Not Session("ChapterID") Is Nothing Then
        '    lstEntIndSpouse.Filter = "ChapterID = 49"
        'End If
        'ddlSponsor.DataSource = lstEntIndSpouse
        ''ddlSponsor.DataSourceID = lstEntIndSpouse().AutoMemberID
        'ddlSponsor.DataBind()


        'Dim lstEntVenue As nsf.Entities.TList(Of nsf.Entities.OrganizationInfo)
        'lstEntVenue = objSqlOrgInfoProviderBase.GetAll()

        'If Not Session("ChapterID") Is Nothing Then
        '    lstEntVenue.Filter = "ChapterID = 49"
        'End If
        'ddlVenue.DataSource = lstEntVenue
        'ddlVenue.DataBind()


        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
            ddlSponsor.DataSource = dvSponsor
            ddlSponsor.DataBind()
            If ddlSponsor.Items.Count > 0 Then
                ddlSponsor.Items.Insert(0, "")
            End If
        End If

        Dim dsVenue As DataSet
        dsVenue = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        ddlVenue.DataSource = dsVenue
        ddlVenue.DataBind()
        If ddlVenue.Items.Count > 0 Then
            ddlVenue.Items.Insert(0, "")
        End If

        If Session("EventID") = 2 Then 'regionals
            Dim dsContestDt As DataSet
            '            dsContestDt = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_WeekCalendar_Get_List", New SqlParameter("@ContestDt", FormatDateTime("1/1/" & Application("ContestYear"), DateFormat.ShortDate)))
            dsContestDt = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_WeekCalendar_Get_List", New SqlParameter("@EventID", Session("EventID")))
            ddlContestDt.DataSource = dsContestDt
            ddlContestDt.DataBind()
            ddlContestDt.Items.Insert(0, "")
            If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
            Else
                If ddlContestDt.Items.Count > 15 Then
                    Do
                        ddlContestDt.Items.RemoveAt(15)
                    Loop Until ddlContestDt.Items.Count = 15
                End If
            End If
        Else
            'Ratnam uncle wanted to change this procedure to retrieve the data from WeekCalendar - sandhya 6/72009
            'Dim dtCheckDates As Date
            'dtCheckDates = "8/1/" & Session("EventYear")
            'Do
            '    ' If IsDate(dtCheckDates) = False Then Exit Do
            '    If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
            '        ddlContestDt.Items.Add(dtCheckDates)
            '    End If
            '    dtCheckDates = dtCheckDates.AddDays(1)
            '    If Day(dtCheckDates) = 31 Then
            '        If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
            '            ddlContestDt.Items.Add(dtCheckDates)
            '        End If
            '        dtCheckDates = "9/1/" & Session("EventYear")
            '        If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
            '            ddlContestDt.Items.Add(dtCheckDates)
            '        End If
            '        Exit Do
            '    End If
            'Loop
            Dim dsContestDt As DataSet
            dsContestDt = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_WeekCalendar_Get_List", New SqlParameter("@EventID", Session("EventID")))
            ddlContestDt.DataSource = dsContestDt
            ddlContestDt.DataBind()
            ddlContestDt.Items.Insert(0, "")
            If ddlContestDt.Items.Count > 5 Then
                Do
                    ddlContestDt.Items.RemoveAt(5)
                Loop Until ddlContestDt.Items.Count = 5
            End If

        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Dim dtContestDate As Date, strMinDt As String, strMaxDt As String, blnValidDate As Boolean, dtRegDeadLine As Date

        'strMinDt = "1/1/" & Application("ContestYear")
        'strMaxDt = "10/31/" & Application("ContestYear")
        'lblDataErr.Visible = False

        'blnValidDate = False
        'If Not Server.HtmlEncode(txtAppContestDate.Text) = "" Then
        '    dtContestDate = Convert.ToDateTime(Server.HtmlEncode(txtAppContestDate.Text))
        '    If dtContestDate >= Convert.ToDateTime(strMinDt) And dtContestDate <= Convert.ToDateTime(strMaxDt) Then
        '        blnValidDate = True
        '    End If
        '    If blnValidDate = False Then
        '        lblDataErr.Text = "Invalid ContestDate."
        '        lblDataErr.Visible = True
        '        Exit Sub
        '    End If
        'End If

        'blnValidDate = False
        'If Not Server.HtmlEncode(txtRegDeadLine.Text) = "" Then
        '    dtRegDeadLine = Convert.ToDateTime(Server.HtmlEncode(txtRegDeadLine.Text))
        '    If Not Server.HtmlEncode(txtAppContestDate.Text) = "" Then
        '        If dtRegDeadLine >= Convert.ToDateTime(strMinDt) And dtRegDeadLine <= dtContestDate Then
        '            blnValidDate = True
        '        End If
        '    Else
        '        If dtRegDeadLine >= Convert.ToDateTime(strMinDt) And dtContestDate <= Convert.ToDateTime(strMaxDt) Then
        '            blnValidDate = True
        '        End If
        '    End If
        '    If blnValidDate = False Then
        '        lblDataErr.Text = "Invalid Registration Deadline Date."
        '        lblDataErr.Visible = True
        '        Exit Sub
        '    End If
        'End If

        Dim intCtr As Integer

        For intCtr = 0 To lstContests.Items.Count - 1
            If lstContests.Items(intCtr).Selected = True Then
                GetContestInfo(lstContests.Items(intCtr).Value)
                GetContestCategoryInfo(lstContests.Items(intCtr).Value)
                If ddlContestDt.SelectedItem.Text <> "" Then
                    mdtContestDate = ddlContestDt.SelectedItem.Text
                    mdtContestDate = mdtContestDate.Date
                    mdtRegDeadLine = mdtContestDate.AddDays(-14).Date
                End If
                InsertContest(lstContests.Items(intCtr).Value)
            End If
        Next
        LoadGrid()
        LoadlstContests()
    End Sub

    Private Sub GetContestInfo(ByVal ProductID As Integer)
        Dim drContest As SqlDataReader
        drContest = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_Product_GetByProductId", New SqlParameter("@ProductID", ProductID))
        If (drContest.HasRows()) Then
            Do While (drContest.Read())
                mintProductID = drContest("ProductID")
                mstrProductCode = drContest("ProductCode")
                mintProductGroupID = drContest("ProductGroupID")
                mstrProductGroupcode = drContest("ProductGroupCode")
                mintEventID = drContest("EventID")
                mstrEventCode = drContest("EventCode")
            Loop
        End If
        drContest.Close()
    End Sub

    Private Sub GetContestCategoryInfo(ByVal ProductID As Integer)
        Dim drProduct As SqlDataReader, strProductCode As String
        strProductCode = ""
        drProduct = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_Product_GetByProductId", New SqlParameter("@ProductID", ProductID))
        If (drProduct.HasRows()) Then
            Do While (drProduct.Read())
                strProductCode = drProduct("ProductCode")
            Loop
        End If
        drProduct.Close()

        mintContestCategoryID = 0
        If strProductCode <> "" Then
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_ContestCategory_GetByProductCode", New SqlParameter("@ProductCode", strProductCode))
            If (drContestCategory.HasRows()) Then
                Do While (drContestCategory.Read())
                    If drContestCategory.Item("ContestYear") = Session("EventYear") Then
                        mintContestCategoryID = drContestCategory("ContestCategoryID")
                        Exit Do
                    End If
                Loop
            End If
            drContestCategory.Close()
        End If
    End Sub

    Private Function InsertContest(ByVal EventID As Integer) As Boolean
        Try
            Dim param(30) As SqlParameter
            param(0) = New SqlParameter("@ContestTypeID", mintEventID)
            param(1) = New SqlParameter("@NSFChapterID", Session("LoginChapterID"))
            param(2) = New SqlParameter("@ContestCategoryID", mintContestCategoryID)
            param(3) = New SqlParameter("@Phase", DBNull.Value)
            param(4) = New SqlParameter("@GroupNo", DBNull.Value)
            param(5) = New SqlParameter("@BadgeNoBeg", DBNull.Value)
            param(6) = New SqlParameter("@BadgeNoEnd", DBNull.Value)
            If ddlContestDt.SelectedItem.Text <> "" Then
                param(7) = New SqlParameter("@ContestDate", mdtContestDate.Date)
            Else
                param(7) = New SqlParameter("@ContestDate", DBNull.Value)
            End If
            param(8) = New SqlParameter("@StartTime", DBNull.Value)
            param(9) = New SqlParameter("@EndTime", DBNull.Value)
            param(10) = New SqlParameter("@Building", txtBldg.Text)
            param(11) = New SqlParameter("@ContestYear", Session("EventYear"))
            param(12) = New SqlParameter("@Room", DBNull.Value)
            If ddlContestDt.SelectedItem.Text <> "" Then
                param(13) = New SqlParameter("@RegistrationDeadline", mdtRegDeadLine.Date)
            Else
                param(13) = New SqlParameter("@RegistrationDeadline", DBNull.Value)
            End If
            param(14) = New SqlParameter("@CheckinTime", DBNull.Value)
            If ddlVenue.SelectedIndex < 0 Then
                param(15) = New SqlParameter("@VenueId", DBNull.Value)
            Else
                param(15) = New SqlParameter("@VenueId", ddlVenue.SelectedValue)
            End If
            If ddlSponsor.SelectedIndex < 0 Then
                param(16) = New SqlParameter("@SponsorId", DBNull.Value)
                param(17) = New SqlParameter("@SponsorType", DBNull.Value)
            Else
                param(16) = New SqlParameter("@SponsorId", ddlSponsor.SelectedValue)
                param(17) = New SqlParameter("@SponsorType", rbtnMemberType.SelectedValue)
            End If
            param(18) = New SqlParameter("@EventId", mintEventID)
            param(19) = New SqlParameter("@EventCode", mstrEventCode)
            param(20) = New SqlParameter("@ProductGroupId", mintProductGroupID)
            param(21) = New SqlParameter("@ProductGroupCode", mstrProductGroupcode)
            param(22) = New SqlParameter("@ProductId", mintProductID)
            param(23) = New SqlParameter("@ProductCode", mstrProductCode)
            param(24) = New SqlParameter("@CreateDate", Now)
            param(25) = New SqlParameter("@CreatedBy", Session("LoginID"))
            param(26) = New SqlParameter("@ModifiedDate", DBNull.Value)
            param(27) = New SqlParameter("@ModifiedBy", DBNull.Value)
            param(28) = New SqlParameter("@ContestCode", DBNull.Value)
            param(29) = New SqlParameter("@ContestID", SqlDbType.Int)
            param(29).Direction = ParameterDirection.Output
            SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_Contest_Insert", param)
        Catch ex As SqlException
            'ex.message
        End Try
    End Function

    'Private Function InsertContestsOld() As Boolean
    '    Dim strContests As New StringBuilder
    '    Dim intCtr As Integer
    '    Dim cnTemp As New SqlConnection(Application("ConnectionString"))
    '    Dim cmd As New SqlCommand

    '    For intCtr = 0 To lstContests.Items.Count - 1
    '        If lstContests.Items(intCtr).Selected = True Then
    '            strContests.Append(lstContests.Items(intCtr).Value & "~")
    '        End If
    '    Next

    '    Try
    '        Dim prmArray(5) As SqlParameter

    '        cmd.Connection = cnTemp
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandText = "usp_InsertNewEvent"
    '        cnTemp.Open()

    '        cmd.Parameters.AddWithValue("@Contest_Type", DbType.Int32).Value = Server.HtmlEncode(Application("ContestType"))
    '        cmd.Parameters.AddWithValue("@Contest_Year", DbType.String).Value = Server.HtmlEncode(Application("ContestYear"))
    '        If Not Server.HtmlEncode(txtAppContestDate.Text) = "" Then
    '            cmd.Parameters.AddWithValue("@Contest_Date", DbType.DateTime).Value = Convert.ToDateTime(Server.HtmlEncode(txtAppContestDate.Text))
    '        Else
    '            cmd.Parameters.AddWithValue("@Contest_Date", DbType.DateTime).Value = DBNull.Value
    '        End If
    '        cmd.Parameters.AddWithValue("@ChapterID", DbType.String).Value = Session("ChapterID")
    '        cmd.Parameters.AddWithValue("@Contests", DbType.String).Value = Server.HtmlEncode(strContests.ToString)

    '        cmd.ExecuteScalar()
    '        cnTemp.Close()

    '    Catch ex As SqlException
    '        Return False

    '    Finally
    '        cmd = Nothing
    '        cnTemp = Nothing
    '    End Try
    '    Return True

    'End Function

    Private Function UpdateContests(ByVal SponsorSelected As Boolean, ByVal VenueSelected As Boolean) As Boolean
        'objEntContest.ContestID = mintContestID
        'objEntContest.ContestDate = mdtContestDate
        'objEntContest.StartTime = mstrStartTime
        'objEntContest.EndTime = mstrEndTime
        'objEntContest.CheckinTime = mstrCheckInTime
        'objEntContest.Building = mstrBuilding
        'objEntContest.Room = mstrRoom
        'objEntContest.RegistrationDeadline = mdtRegDeadLine
        'objEntContest.ContestCategoryID = mintContestCategoryID
        'objEntContest.NSFChapterID = Session("ChapterID")
        'objEntContest.ContestTypeID = Application("ContestType")
        'objEntContest.ContestYear = Application("ContestYear")
        'objSqlContestProviderBase.Update(objEntContest)

        Try
            Dim param(17) As SqlParameter
            param(0) = New SqlParameter("@Phase", DBNull.Value)
            param(1) = New SqlParameter("@GroupNo", DBNull.Value)
            param(2) = New SqlParameter("@BadgeNoBeg", DBNull.Value)
            param(3) = New SqlParameter("@BadgeNoEnd", DBNull.Value)
            If mdtContestDate.Year > 1950 Then
                param(4) = New SqlParameter("@ContestDate", mdtContestDate)
                If mdtContestDate.Year > 1950 Then
                    lblGridError.Text = DateDiff(DateInterval.Day, mdtRegDeadLine, mdtContestDate)
                    lblGridError.Visible = True
                    If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
                        param(5) = New SqlParameter("@RegistrationDeadline", mdtRegDeadLine)
                    Else
                        If ((DateDiff(DateInterval.Day, mdtRegDeadLine, mdtContestDate) > 14) Or (DateDiff(DateInterval.Day, mdtRegDeadLine, mdtContestDate) < 10)) Then
                            param(5) = New SqlParameter("@RegistrationDeadline", mdtContestDate.AddDays(-14))
                        Else
                            param(5) = New SqlParameter("@RegistrationDeadline", mdtRegDeadLine)
                        End If
                    End If
                Else
                    param(5) = New SqlParameter("@RegistrationDeadline", DBNull.Value)
                End If
            Else
                param(4) = New SqlParameter("@ContestDate", DBNull.Value)
                param(5) = New SqlParameter("@RegistrationDeadline", DBNull.Value)
            End If
            param(6) = New SqlParameter("@StartTime", mstrStartTime)
            param(7) = New SqlParameter("@EndTime", mstrEndTime)
            param(8) = New SqlParameter("@Building", mstrBuilding)
            param(9) = New SqlParameter("@Room", mstrRoom)
            param(10) = New SqlParameter("@CheckinTime", mstrCheckInTime)
            If VenueSelected = True Then
                param(11) = New SqlParameter("@VenueId", mintVenueID)
                If mintVenueID <> 0 Then
                    param(11) = New SqlParameter("@VenueId", mintVenueID)
                Else
                    param(11) = New SqlParameter("@VenueId", DBNull.Value)
                End If
            Else
                param(11) = New SqlParameter("@VenueId", DBNull.Value)
            End If

            If SponsorSelected = True Then
                If mintSponsorID <> 0 Then
                    param(12) = New SqlParameter("@SponsorId", mintSponsorID)
                    param(13) = New SqlParameter("@SponsorType", mstrSponsorType)
                Else
                    param(12) = New SqlParameter("@SponsorId", DBNull.Value)
                    param(13) = New SqlParameter("@SponsorType", DBNull.Value)
                End If
            Else
                param(12) = New SqlParameter("@SponsorId", DBNull.Value)
                param(13) = New SqlParameter("@SponsorType", DBNull.Value)
            End If

            param(14) = New SqlParameter("@ModifiedDate", Now)
            param(15) = New SqlParameter("@ModifiedBy", Session("LoginID"))
            param(16) = New SqlParameter("@ContestID", mintContestID)
            'lblErr.Text = param(4).Value.ToString & param(5).Value.ToString & param(6).Value.ToString & param(7).Value.ToString & param(8).Value.ToString & param(9).Value.ToString & _
            'param(10).Value.ToString & param(11).Value.ToString & param(12).Value.ToString & param(13).Value.ToString
            '            lblErr.Visible = True
            SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_Contest_Update", param)
        Catch ex As SqlException
            'ex.message
        End Try

    End Function

    Protected Sub grdTarget_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.CancelCommand
        grdTarget.EditItemIndex = -1
        LoadGrid()
        lblGridError.Visible = False
    End Sub

    Protected Sub grdTarget_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.DeleteCommand
        'Try
        '    If e.CommandName = "Delete" Then
        '        mintContestID = CInt(e.Item.Cells(1).Text)
        '        'delete routine
        '        objSqlContestProviderBase.Delete(mintContestID)
        '        LoadGrid()
        '    End If
        'Catch ex As Exception
        'End Try

        Try
            If e.CommandName = "Delete" Then
                mintContestID = CInt(e.Item.Cells(0).Text)
                'delete routine
                SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "usp_Contest_Delete", New SqlParameter("@ContestID", mintContestID))
                LoadGrid()
                LoadlstContests()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub grdTarget_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.EditCommand
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        mintContestID = CInt(e.Item.Cells(0).Text)
        mintContestCategoryID = CInt(e.Item.Cells(1).Text)

        strSponsor = CType(e.Item.Cells(3).FindControl("lblContestSponsor"), Label).Text
        strSponsorID = CType(e.Item.Cells(3).FindControl("lblContestSponsorID"), Label).Text
        strSponsorType = CType(e.Item.Cells(3).FindControl("lblContestSponsorType"), Label).Text
        strVenue = CType(e.Item.Cells(4).FindControl("lblContestVenue"), Label).Text
        strContestDt = CType(e.Item.Cells(7).FindControl("lblContestDate"), Label).Text
        ' If Not (e.Item.Cells(5).FindControl("txtEditRegDeadLine")) Is Nothing Then
        If Not e.Item.Cells(13).FindControl("ddlContest") Is Nothing Then
            If CType(e.Item.Cells(13).FindControl("ddlContest"), DropDownList).Items.Count > 0 Then
                strCopyFromContest = CType(e.Item.Cells(13).FindControl("ddlContest"), DropDownList).SelectedItem.Text
            Else
                strCopyFromContest = ""
            End If
        Else
            strCopyFromContest = ""
        End If
            strCheckInTime = CType(e.Item.Cells(6).FindControl("lblCheckInTime"), Label).Text
            strStartTime = CType(e.Item.Cells(8).FindControl("lblStartTime"), Label).Text
            strEndTime = CType(e.Item.Cells(9).FindControl("lblEndTime"), Label).Text

            'disenable delete and copy buttons
            Dim intcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(intcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Enabled = False

            intcell = e.Item.Cells.Count - 1
            myTableCell = e.Item.Cells(intcell)
            If Not CType(e.Item.Cells(intcell).FindControl("btnCopy"), Button) Is Nothing Then
                CType(e.Item.Cells(intcell).FindControl("btnCopy"), Button).Enabled = False
            End If

            LoadGrid()

    End Sub

    Private Function ContestantsAreAttached(ByVal ContestID As String) As Boolean
        Dim dsResult As New DataSet
        dsResult = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Contestant_GetByContestID", New SqlParameter("@ContestID", Trim(ContestID)))
        If dsResult.Tables(0).Rows.Count > 0 Then
            ContestantsAreAttached = True
        Else
            ContestantsAreAttached = False
        End If
    End Function

    Protected Sub grdTarget_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTarget.ItemDataBound
        Dim dsTarget As New DataSet, ddlTemp As DropDownList, btnTemp As Button

        ' First, make sure we're NOT dealing with a Header or Footer row
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            'Now, reference the PushButton control that the Delete ButtonColumn has been rendered to
            Dim rownum As Integer
            rownum = e.Item.ItemIndex + 1

            Dim strContestID As String = e.Item.Cells(0).Text

            Dim ddlSponsorTemp As DropDownList, ddlSponsorTypeTemp As DropDownList
            ddlSponsorTemp = e.Item.Cells(3).FindControl("ddlContestSponsor")
            ddlSponsorTypeTemp = e.Item.Cells(3).FindControl("ddlContestSponsorType")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If dsTarget.Tables(0).Rows.Count > 0 Then
                Dim dvSponsor As New DataView(dsTarget.Tables(0), "", "SponsorName", DataViewRowState.CurrentRows)
                If Not ddlSponsorTemp Is Nothing Then
                    ddlSponsorTemp.DataSource = dvSponsor
                    ddlSponsorTemp.DataBind()
                    If ddlSponsorTemp.Items.Count > 0 Then
                        ddlSponsorTemp.Items.Insert(0, "")
                    End If
                End If
                If Not ddlSponsorTypeTemp Is Nothing Then
                    ddlSponsorTypeTemp.DataSource = dvSponsor
                    ddlSponsorTypeTemp.DataBind()
                    If ddlSponsorTypeTemp.Items.Count > 0 Then
                        ddlSponsorTypeTemp.Items.Insert(0, "")
                    End If
                End If
            End If

            Dim ddlVenueTemp As DropDownList
            ddlVenueTemp = e.Item.Cells(4).FindControl("ddlContestVenue")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If Not ddlVenueTemp Is Nothing Then
                ddlVenueTemp.DataSource = dsTarget
                ddlVenueTemp.DataBind()
                If ddlVenueTemp.Items.Count > 0 Then
                    ddlVenueTemp.Items.Insert(0, "")
                End If
            End If

            Dim ddlContestDtTemp As DropDownList
            ddlContestDtTemp = e.Item.Cells(7).FindControl("ddlGridContestDt")
            If Session("EventID") = 1 Then  'nationals
                If Not ddlContestDtTemp Is Nothing Then
                    Dim dtCheckDates As Date
                    dtCheckDates = "8/1/" & Session("EventYear")
                    Do
                        'If IsDate(dtCheckDates) = False Then Exit Do
                        If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
                            ddlContestDtTemp.Items.Add(dtCheckDates)
                        End If
                        dtCheckDates = dtCheckDates.AddDays(1)
                        If Day(dtCheckDates) = 31 Then
                            If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
                                ddlContestDtTemp.Items.Add(dtCheckDates)
                            End If
                            dtCheckDates = "9/1/" & Session("EventYear")
                            If (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 6) Or (Weekday(dtCheckDates, Microsoft.VisualBasic.FirstDayOfWeek.Monday) = 7) Then
                                ddlContestDtTemp.Items.Add(dtCheckDates)
                            End If
                            Exit Do
                        End If
                    Loop
                End If
            Else 'regionals
                dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_WeekCalendar_Get_List", New SqlParameter("@EventID", Session("EventID")))
                If Not ddlContestDtTemp Is Nothing Then
                    ddlContestDtTemp.DataSource = dsTarget
                    ddlContestDtTemp.DataBind()
                    ddlContestDtTemp.Items.Insert(0, "")
                    If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
                    Else
                        If ddlContestDtTemp.Items.Count > 15 Then
                            Do
                                ddlContestDtTemp.Items.RemoveAt(15)
                            Loop Until ddlContestDtTemp.Items.Count = 15
                        End If
                    End If
                End If
            End If


            Dim delcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(delcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")

            Dim intContestCategoryIDCell As Integer = 1
            Dim ContestCategoryIDCell As TableCell = e.Item.Cells(intContestCategoryIDCell)
            Dim strValue As String = ContestCategoryIDCell.Text

            Dim copycell As Integer = e.Item.Cells.Count - 1
            myTableCell = e.Item.Cells(copycell)
            ddlTemp = myTableCell.Controls(1)
            Dim param(3) As SqlParameter, dsResult As New DataSet
            param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
            param(1) = New SqlParameter("@ContestYear", Session("EventYear"))
            param(2) = New SqlParameter("@EventID", Session("LoginEventD"))
            dsResult = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Contest_GetByChapterID", param)
            ddlTemp.DataSource = dsResult.Tables(0)
            ddlTemp.DataBind()
            If ddlTemp.Items.Count > 0 Then
                Dim lstItem As ListItem = ddlTemp.Items.FindByValue(e.Item.Cells(0).Text)
                If Not lstItem Is Nothing Then ddlTemp.Items.Remove(lstItem)
            End If
            btnTemp = myTableCell.Controls(3)
            If ddlTemp.Items.Count > 0 Then
                strCopyFromContest = ddlTemp.Items(0).Text
                btnTemp.CommandName = CStr(rownum)
                btnTemp.Attributes.Add("OnClick", "return CStr(rownum)")
            Else
                btnTemp.Visible = False
                ddlTemp.Visible = False
            End If


            Dim item As ListItem
            Dim itemToDelete As ListItem
            For Each item In ddlTemp.Items
                If item.Text = strValue Then
                    itemToDelete = item
                    ddlTemp.Items.Remove(itemToDelete)
                    Exit For
                End If
            Next

            'disenable delete
            Dim intcell As Integer = e.Item.Cells.Count - 2
            myTableCell = e.Item.Cells(intcell)
            btnDelete = myTableCell.Controls(0)
            If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
                If ContestantsAreAttached(strContestID) = True Then
                    btnDelete.Enabled = False
                Else
                    btnDelete.Enabled = True
                End If
            Else
                btnDelete.Enabled = False
            End If

            If Session("EventID") = 2 Then 'Regionals
                If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
                    'Only National and Admin volunteers are allowed to edit the date field
                    If Not (e.Item.Cells(5).FindControl("lblEditRegDeadLine")) Is Nothing Then
                        CType(e.Item.Cells(5).FindControl("lblEditRegDeadLine"), Label).Visible = False
                    End If
                    If Not (e.Item.Cells(5).FindControl("txtEditRegDeadLine")) Is Nothing Then
                        CType(e.Item.Cells(5).FindControl("txtEditRegDeadLine"), TextBox).Visible = True
                    End If
                    If Not (e.Item.Cells(7).FindControl("lblGridContestDate")) Is Nothing Then
                        CType(e.Item.Cells(7).FindControl("lblGridContestDate"), Label).Visible = False
                    End If
                    If Not (e.Item.Cells(7).FindControl("ddlGridContestDt")) Is Nothing Then
                        CType(e.Item.Cells(7).FindControl("ddlGridContestDt"), DropDownList).Visible = True
                    End If
                Else
                    'make dates non editable for chapter coordinators
                    If Not (e.Item.Cells(5).FindControl("lblEditRegDeadLine")) Is Nothing Then
                        CType(e.Item.Cells(5).FindControl("lblEditRegDeadLine"), Label).Visible = True
                    End If
                    If Not (e.Item.Cells(5).FindControl("txtEditRegDeadLine")) Is Nothing Then
                        CType(e.Item.Cells(5).FindControl("txtEditRegDeadLine"), TextBox).Visible = False
                    End If

                    If Not (e.Item.Cells(7).FindControl("lblGridContestDate")) Is Nothing Then
                        CType(e.Item.Cells(7).FindControl("lblGridContestDate"), Label).Visible = False
                    End If
                    If Not (e.Item.Cells(7).FindControl("ddlGridContestDt")) Is Nothing Then
                        CType(e.Item.Cells(7).FindControl("ddlGridContestDt"), DropDownList).Visible = True
                    End If
                End If
            Else
                'make dates non editable for chapter coordinators
                If Not (e.Item.Cells(5).FindControl("lblEditRegDeadLine")) Is Nothing Then
                    CType(e.Item.Cells(5).FindControl("lblEditRegDeadLine"), Label).Visible = False
                End If
                If Not (e.Item.Cells(5).FindControl("txtEditRegDeadLine")) Is Nothing Then
                    CType(e.Item.Cells(5).FindControl("txtEditRegDeadLine"), TextBox).Visible = True
                End If
                If Not (e.Item.Cells(7).FindControl("lblGridContestDate")) Is Nothing Then
                    CType(e.Item.Cells(7).FindControl("lblGridContestDate"), Label).Visible = False
                End If
                If Not (e.Item.Cells(7).FindControl("ddlGridContestDt")) Is Nothing Then
                    CType(e.Item.Cells(7).FindControl("ddlGridContestDt"), DropDownList).Visible = True
                End If
            End If


        End If

    End Sub

    Protected Sub grdTarget_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.UpdateCommand
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim txtEditText As TextBox, ddlTemp As DropDownList, blnValidData As Boolean, lblText As Label
        Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer

        'ContestDate
        'If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
        '    ddlTemp = e.Item.FindControl("ddlGridContestDt")
        '    mdtContestDate = ddlTemp.SelectedItem.Text
        '    mdtContestDate = mdtContestDate.Date
        'Else
        '    lblText = e.Item.Cells(5).FindControl("lblGridContestDate")
        '    mdtContestDate = lblText.Text
        'End If
        ddlTemp = e.Item.FindControl("ddlGridContestDt")
        If ddlTemp.SelectedItem.Text <> "" Then
            mdtContestDate = ddlTemp.SelectedItem.Text
            mdtContestDate = mdtContestDate.Date
        End If

        'RegDeadLine
        If Session("LoginRole") = "NationalC" Or Session("LoginRole") = "Admin" Then
            txtEditText = e.Item.FindControl("txtEditRegDeadLine")
            If txtEditText.Text = "" Then
                lblGridError.Text = "Registration dead line cannot be empty."
                lblGridError.Visible = True
                GoTo FinalProcess
            Else
                If IsDate(txtEditText.Text) = True Then
                    mdtRegDeadLine = txtEditText.Text
                    mdtRegDeadLine = mdtRegDeadLine.Date
                Else
                    lblGridError.Text = "Invalid date."
                    lblGridError.Visible = True
                    GoTo FinalProcess
                End If
            End If

            If mdtContestDate.Year > 1950 And mdtRegDeadLine.Year > 1950 Then
                If DateDiff(DateInterval.Day, mdtRegDeadLine, mdtContestDate) < 10 Then
                    lblGridError.Text = "There should be atleast 10 days between Registration deadline and Contest date."
                    lblGridError.Visible = True
                    '   GoTo FinalProcess
                End If
            End If
        Else
            lblText = e.Item.FindControl("lblEditRegDeadLine")
            If lblText.Text <> "" Then
                mdtRegDeadLine = lblText.Text
            Else
                If ddlTemp.SelectedItem.Text <> "" Then
                    mdtRegDeadLine = mdtContestDate.AddDays(-14)
                End If
            End If
        End If

        ddlTemp = e.Item.FindControl("ddlStartTime")
        mstrStartTime = ddlTemp.SelectedItem.Text
        intSelStartTime = 1 + ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlEndTime")
        mstrEndTime = ddlTemp.SelectedItem.Text
        intSelEndTime = 3 + ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlCheckInTime")
        mstrCheckInTime = ddlTemp.SelectedItem.Text
        intSelCheckInTime = ddlTemp.SelectedIndex

        txtEditText = e.Item.FindControl("txtBuilding")
        mstrBuilding = txtEditText.Text

        txtEditText = e.Item.FindControl("txtRoom")
        mstrRoom = txtEditText.Text

        blnValidData = False
        lblGridError.Visible = False
        lblGridError.Text = ""
        If mstrStartTime <> "" Then
            If intSelCheckInTime >= intSelStartTime Then lblGridError.Text = " Check In Time should be before Start Time."
            If intSelCheckInTime >= intSelEndTime Then lblGridError.Text = " Check In Time should be before End Time."
            If intSelEndTime <= intSelStartTime Then lblGridError.Text = " Start Time should be before End Time."
            If intSelEndTime <= intSelCheckInTime Then lblGridError.Text = " Check In Time should be before End Time."
            If lblGridError.Text <> "" Then
                lblGridError.Visible = True
                blnValidData = False
                GoTo FinalProcess
            End If
            If intSelStartTime > intSelCheckInTime And intSelStartTime < intSelEndTime Then
                blnValidData = True
            Else
                lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
            If (intSelEndTime - intSelStartTime) >= 2 Then
                blnValidData = True
            Else
                lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
        End If


        'Sponsor cell
        Dim blnSponsorSelected As Boolean, ddlTempSponsorType As DropDownList
        ddlTemp = e.Item.FindControl("ddlContestSponsor")
        ddlTempSponsorType = e.Item.FindControl("ddlContestSponsorType")
        If ddlTemp.SelectedIndex < 1 Then
            blnSponsorSelected = False
        Else
            blnSponsorSelected = True
            mintSponsorID = ddlTemp.SelectedValue
            mstrSponsorType = ddlTempSponsorType.Items(ddlTemp.SelectedIndex).Text
        End If

        'Venue
        Dim blnVenueSelected As Boolean
        ddlTemp = e.Item.FindControl("ddlContestVenue")
        If ddlTemp.SelectedIndex < 1 Then
            blnVenueSelected = False
        Else
            blnVenueSelected = True
            mintVenueID = ddlTemp.SelectedValue
        End If

        'update routine
        UpdateContests(blnSponsorSelected, blnVenueSelected)
        grdTarget.EditItemIndex = -1

        'disenable delete and copy buttons
        Dim intcell As Integer = e.Item.Cells.Count - 2
        Dim myTableCell As TableCell
        myTableCell = e.Item.Cells(intcell)
        Dim btnDelete As Button = myTableCell.Controls(0)
        btnDelete.Enabled = True

        intcell = e.Item.Cells.Count - 1
        myTableCell = e.Item.Cells(intcell)
        Dim btnCopy As Button = myTableCell.Controls(3)
        btnCopy.Enabled = True

        LoadGrid()
        Exit Sub

FinalProcess:
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        LoadGrid()
    End Sub

    Sub GridCopy(ByVal sender As Object, ByVal e As EventArgs)
        If grdTarget.EditItemIndex <> -1 Then
            lblGridError.Text = "Cannot copy when you are in edit mode."
            Exit Sub
        End If
        Dim intCopyingContestID As Integer, intContestID As Integer, intcell As Integer
        Dim mybtn As Button = sender
        Dim ddlTemp As DropDownList
        intcell = grdTarget.Columns.Count
        Dim mycell As TableCell = grdTarget.Items.Item(CInt(mybtn.CommandName) - 1).Cells(intcell - 1)
        ddlTemp = mycell.Controls(1)
        intCopyingContestID = ddlTemp.SelectedItem.Value
        intContestID = grdTarget.Items.Item(CInt(mybtn.CommandName) - 1).Cells(0).Text

        Dim dsContest As New DataSet
        dsContest = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Contest_GetByContestID", New SqlParameter("@ContestID", intCopyingContestID))

        Try
            If Not dsContest Is Nothing Then
                Dim dr As DataRow
                For Each dr In dsContest.Tables(0).Rows
                    Dim strSQL As String
                    strSQL = " Update Contest Set "

                    If Not IsDBNull(dr.Item("ContestDate")) Then
                        strSQL = strSQL & " ContestDate = '" & dr.Item("ContestDate") & "', "
                    Else
                        strSQL = strSQL & " ContestDate = '', "
                    End If

                    If Not IsDBNull(dr.Item("StartTime")) Then
                        strSQL = strSQL & " StartTime = '" & dr.Item("StartTime") & "', "
                    Else
                        strSQL = strSQL & " StartTime = '', "
                    End If

                    If Not IsDBNull(dr.Item("EndTime")) Then
                        strSQL = strSQL & " EndTime = '" & dr.Item("EndTime") & "', "
                    Else
                        strSQL = strSQL & " EndTime = '', "
                    End If

                    If Not IsDBNull(dr.Item("Building")) Then
                        strSQL = strSQL & " Building = '" & dr.Item("Building") & "', "
                    Else
                        strSQL = strSQL & " Building = '', "
                    End If

                    If Not IsDBNull(dr.Item("Room")) Then
                        strSQL = strSQL & " Room = '" & dr.Item("Room") & "', "
                    Else
                        strSQL = strSQL & " Room = '', "
                    End If

                    If Not IsDBNull(dr.Item("RegistrationDeadline")) Then
                        strSQL = strSQL & " RegistrationDeadline = '" & dr.Item("RegistrationDeadline") & "', "
                    Else
                        strSQL = strSQL & " RegistrationDeadline = '', "
                    End If

                    If Not IsDBNull(dr.Item("CheckinTime")) Then
                        strSQL = strSQL & " CheckinTime = '" & dr.Item("CheckinTime") & "', "
                    Else
                        strSQL = strSQL & " CheckinTime = '', "
                    End If

                    If Not IsDBNull(dr.Item("VenueId")) Then
                        strSQL = strSQL & " VenueId = " & dr.Item("VenueId") & ", "
                    End If

                    If Not IsDBNull(dr.Item("SponsorType")) Then
                        If Trim(dr.Item("SponsorType")) <> "" Then
                            strSQL = strSQL & " SponsorType = '" & dr.Item("SponsorType") & "', "
                            If Not IsDBNull(dr.Item("SponsorId")) Then
                                strSQL = strSQL & " SponsorId = " & dr.Item("SponsorId") & ", "
                            End If
                        End If
                    End If

                    strSQL = strSQL & " ModifiedDate = '" & Now & "', " & _
                             " ModifiedBy = " & Session("LoginID") & _
                             " WHERE ContestID = " & intContestID
                    'lblErr.Text = strSQL
                    'lblErr.Visible = True
                    SqlHelper.ExecuteScalar(cnTemp, CommandType.Text, strSQL)
                Next
            End If
        Catch ex As Exception

        End Try
        LoadGrid()
    End Sub

    Public Sub SetDropDown_Sponsor(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor)))
        If ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor))) <= 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Trim(strSponsorID)))
        End If
    End Sub

    Public Sub SetDropDown_Venue(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strVenue))
    End Sub

    Public Sub SetDropDown_ContestDt(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If Trim(strContestDt) <> "" Then
            If Session("EventID") = 2 Then 'Regionals
                strContestDt = strContestDt & " 12:00:00 AM"
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strContestDt))
        Else
            ddlTemp.Items(0).Selected = True
        End If
    End Sub

    Public Sub SetDropDown_CheckInTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strCheckInTime))
    End Sub

    Public Sub SetDropDown_StartTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strStartTime))
    End Sub

    Public Sub SetDropDown_EndTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strEndTime))
    End Sub

    Public Sub SetDropDown_CopyFromContest(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If strCopyFromContest <> "" Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strCopyFromContest))
        End If
    End Sub

    Protected Sub rbtnMemberType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnMemberType.SelectedIndexChanged
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Select Case rbtnMemberType.SelectedIndex
                Case 0
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()

                Case 1
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'I'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()

            End Select
        End If
    End Sub

    Protected Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHide.Click
        If Left(btnHide.Text, 1) = "H" Then
            btnHide.Text = "Show me How to do add Contests"
            pnlMsg.Visible = False
        Else
            btnHide.Text = "Hide the description"
            pnlMsg.Visible = True
        End If
    End Sub
End Class
