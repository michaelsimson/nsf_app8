Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ChapterCoordinator_PrepClubCalendar
    Inherits System.Web.UI.Page

    Private mChapterCity As String
    Private mChapterState As String
    Private mChapterCode As String

    Private mdtPrepClubDate1 As Date
    Private mdtPrepClubDate2 As Date
    Private mdtRegDeadLine As Date
    Private mdtLateRegDLDate As String 'Date
    Private mintSponsorID As Integer
    Private mstrSponsorType As String
    Private mintEventID As Integer
    Private mstrEventCode As String
    Private mintProductGroupID As Integer
    Private mstrProductGroupcode As String
    Private mintProductID As Integer
    Private mstrProductCode As String
    Private mRegFee As Double
    Private mLateFee As Double

    Private mDeadLineDate As Date
    Private mLateRegDeadlineDate As Date

    Private mstrStartTime As String
    Private mstrEndTime As String
    Private mstrCheckInTime As String
    Private mstrBuilding As String
    Private mstrDay As String
    Private mstrRoom As String
    Private mintVenueID As Integer

    Private mintEventFeesID As Integer
    Private strRegDeadLine As String
    Private strEventDay As String
    Private strBuilding As String
    Private strRoom As String

    Private Shared mintPrepClubID As Integer

    Protected strVenue As String
    Protected strCheckInTime As String
    Protected strStartTime As String
    Protected strEndTime As String
    Protected strCopyFromContest As String
    Protected strPrepClubDt As String
    Protected strSponsor As String
    Protected strSponsorID As String
    Protected strSponsorType As String

    Dim cnTemp As SqlConnection
    Public ChapterID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack = True Then
            'lstPrepClubs.Attributes.Add("onchange", "EnableDisableEvents")

            If LCase(Session("LoggedIn")) <> "true" Then
                Response.Redirect("..\login.aspx?entry=" & Session("entryToken"))
            End If
    
            LoadYear()
            Session("EventID") = 19
            ChapterID = Session("LoginChapterID")
            ' Session("VenueID") = 0
            DisplayHeadings()
            LoadVenue(ddlVenue)
                    LoadDropDowns()
            LoadlstPrepClubs(Integer.Parse(drpYear.SelectedItem.Text))
            LoadGrid()

            lblErr.Text = Session("LoginChapterCode")
            lblErr.Visible = True
        End If
    End Sub
    Private Sub LoadYear()
        drpYear.Items.Add(Year(DateTime.Now) - 1.ToString())
        drpYear.Items.Add(Year(DateTime.Now).ToString())
        drpYear.Items.Add(Integer.Parse(Year(DateTime.Now).ToString()) + 1)

        drpYear.SelectedIndex = 1
    End Sub
    Private Sub DisplayHeadings()
        If Not Session("LoginChapterID") Is Nothing Then
            Dim drChapter As SqlDataReader
            If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
            drChapter = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_Chapter_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If (drChapter.HasRows()) Then
                Do While (drChapter.Read())
                    mChapterCity = drChapter("City")
                    mChapterState = drChapter("State")
                    mChapterCode = drChapter("ChapterCode")
                    Session("LoginChapterCode") = mChapterCode
                Loop
            End If
            drChapter.Close()
        End If

        If Session("EventID") = 1 Then ' national
            lblHeading1.Text = "National Contests " & Application("ContestYear")
            If Not Application("NationalFinalsCity") Is Nothing Then
                lblHeading2.Text = Application("NationalFinalsCity")
                lblHeading2.Visible = True
            Else
                lblHeading2.Visible = False
            End If
        Else '2, 3 for regional, workshop
            lblHeading1.Text = "PrepClub "  '& " EventID=" & Session("EventID") & " ChapterID=" & Session("LoginChapterID") & " EventLoginID=" & Session("EventLoginID")
            lblHeading2.Text = mChapterCity & ", " & mChapterState
            lblHeading2.Visible = True
        End If
    End Sub

    Private Sub LoadlstPrepClubs(ByVal selYear As Integer)
        Try
            Dim dsPrepClubs As DataSet
            Dim param(3) As SqlParameter
            param(1) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
            param(2) = New SqlParameter("@EventID", Session("EventID"))
            param(3) = New SqlParameter("@selYear", selYear)
            dsPrepClubs = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "[usp_GetPrepClubsByYear]", param)
            'Dim dvTemp As DataView = dsPrepClubs.Tables(0).DefaultView
            'dvTemp.RowFilter = "Status = 'O'"

            lstPrepClubs.DataSource = dsPrepClubs
            lstPrepClubs.DataBind()

            If lstPrepClubs.Items.Count > 0 Then
                btnAdd.Visible = True
            Else
                btnAdd.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Dim intCtr As Integer
        Try
            lblDataErr.Visible = False
            lblDataErr.Text = ""
            lblError.Visible = False

            Dim SelCount As Integer = 0
            For Each lst As ListItem In lstPrepClubs.Items
                If lst.Selected = True Then
                    SelCount = SelCount + 1
                End If
            Next
            If SelCount = 0 Then
                lblDataErr.Text = "Please Select Prep clubs"
            ElseIf ddlVenue.Items.Count > 0 And ddlVenue.SelectedIndex <= 0 Then
                lblDataErr.Text = "Please select a Venue from the dropdown"
            ElseIf txtStartDate.Text = "" Then
                lblDataErr.Text = "Please select EventDate "
            ElseIf txtRegDeadline.Text = "" Then
                lblDataErr.Text = "Please select Deadline Date "
                'ElseIf txtLateRegDeadline.Text = "" Then
                '    lblDataErr.Text = "Please select Late Registration Deadline Date "
            ElseIf ddlStartTime1.SelectedIndex = 0 Then
                lblDataErr.Text = "Please select Start Time "
            ElseIf ddlEndTime1.SelectedIndex <= 0 Then
                lblDataErr.Text = "Please select End Time "
                'ElseIf ddlBldg.Items.Count > 1 And ddlBldg.SelectedIndex <= 0 Then
                '    lblDataErr.Text = "Please select a Building from the dropdown"
                'ElseIf ddlRoom.Items.Count > 1 And ddlRoom.SelectedIndex <= 0 Then
                '    lblDataErr.Text = "Please select a Room from the dropdown"
                'ElseIf ddlSponsor.Items.Count > 1 And ddlSponsor.SelectedIndex <= 0 Then
                '    lblDataErr.Text = "Please select a Sponsor from the dropdown"
            ElseIf Convert.ToDateTime(txtStartDate.Text) < Convert.ToDateTime(txtRegDeadline.Text) Then
                lblDataErr.Text = "Start Date must be greater than Deadline date"
            ElseIf txtLateRegDeadline.Text <> "" Then
                If Convert.ToDateTime(txtLateRegDeadline.Text) < Convert.ToDateTime(txtRegDeadline.Text) Then
                    lblDataErr.Text = "Late Reg Deadline Date must be greater than Deadline date"
                ElseIf txtLateRegDeadline.Text <> "" And Convert.ToDateTime(txtStartDate.Text) < Convert.ToDateTime(txtLateRegDeadline.Text) Then
                    lblDataErr.Text = "Start Date must be greater than Late Reg Deadline Date"
                End If
            Else
                Dim intSelStartTime, intSelEndTime As Integer
                intSelStartTime = 1 + ddlStartTime1.SelectedIndex
                intSelEndTime = 3 + ddlEndTime1.SelectedIndex

                If intSelEndTime <= intSelStartTime Then
                    lblDataErr.Text = " Start Time should be before End Time."
                End If
            End If
            If lblDataErr.Text <> "" Then
                lblDataErr.Visible = True
                Exit Sub
            Else
                lblError.Visible = False
            End If

            'Session("VenueID") = ddlVenue.SelectedItem.Value

            Dim StrSQL As String = "Select * from PrepClubCal where ChapterId=" & Session("LoginChapterID") & " AND EventId=" & Session("EventID") & " and EventYear=" & drpYear.SelectedValue & IIf(Session("VenueID") = "", "", " and VenueID=" & Session("VenueID")) & IIf(Session("Products") = "", "", " and ProductID in (" & Session("Products") & ")") & " and EventDate > GETDATE()"
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from PrepClubCal where ChapterId=" & Session("LoginChapterID") & " AND EventId=" & Session("EventID") & " and EventYear=" & drpYear.SelectedValue & IIf(Session("VenueID") = "", "", " and VenueID=" & Session("VenueID")) & IIf(Session("Products") = "", "", " and ProductID in (" & Session("Products") & ")") & " and EventDate > GETDATE()")) > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                tblduplicate.Visible = True
                'btnAdd.Attributes.Add("onclick", "return confirm('Select COUNT(*) from PrepClubCal where ChapterId=" & Session("LoginChapterID") & " AND EventId=" & Session("EventID") & " and EventYear=" & drpYear.SelectedValue & IIf(Session("VenueID") = "", "", " and VenueID=" & Session("VenueID")) & IIf(Session("Products") = "", "", " and ProductID in (" & Session("Products") & ")") & " and EventDate > GETDATE().This PrepClub is a repeat this year.  Please confirm');")
            Else
                tblduplicate.Visible = False
                'Dim param(26) As SqlParameter
                'param = Session("Param")
                lblDataErr.Text = ""
                For intCtr = 0 To lstPrepClubs.Items.Count - 1

                    If lstPrepClubs.Items(intCtr).Selected = True Then
                        GetPrepClubInfo(lstPrepClubs.Items(intCtr).Value)
                        InsertPrepClubCalendar(lstPrepClubs.Items(intCtr).Value)
                    End If
                Next
                LoadGrid()
                LoadlstPrepClubs(Integer.Parse(drpYear.SelectedItem.Text))

                'Dim i As Integer = SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertPrepClub_EventCalendar", param) ' Session("Param"))
                'If i > 0 Then
                '    lblDataErr.Text = "Added successfully"
                '    lblDataErr.Visible = True
                '    LoadGrid()
                'Else
                '    lblError.Visible = True
                '    lblError.Text = lblError.Text + "<br> Product Code :" & mstrProductCode & " Event Date : " & mdtPrepClubDate1
                'End If
                '  btnAdd.Attributes.Remove("onclick")
            End If

            ' LoadGrid()
        Catch ex As Exception
            'Response.Write(ex.ToString()) '& "<br />" & "Select COUNT(*) from PrepClubCal where ChapterId=" & Session("LoginChapterID") & " AND EventId=" & Session("EventID") & " and EventYear=" & drpYear.SelectedValue & " and VenueID'=" & Session("VenueID") & "'" & IIf(Session("Products") = "", "", " and ProductID in (" & Session("Products") & ")") & " and EventDate > GETDATE()")
        End Try

    End Sub

    Private Sub GetPrepClubInfo(ByVal EventFeesID As Integer)
        Try
            Dim drPrepClub As SqlDataReader
            drPrepClub = SqlHelper.ExecuteReader(cnTemp, CommandType.StoredProcedure, "usp_EventFees_GetByEventFeesId", New SqlParameter("@EventFeesID", EventFeesID))
            If (drPrepClub.HasRows()) Then
                Do While (drPrepClub.Read())
                    mintEventID = drPrepClub("EventID")
                    mstrEventCode = drPrepClub("EventCode")
                    mintProductGroupID = drPrepClub("ProductGroupID")
                    mstrProductGroupcode = drPrepClub("ProductGroupCode")
                    mintProductID = drPrepClub("ProductID")
                    mstrProductCode = drPrepClub("ProductCode")
                    mRegFee = drPrepClub("RegFee")
                    mLateFee = drPrepClub("LateFee")
                    'mDeadLineDate = IIf(drPrepClub("DeadlineDate") Is DBNull.Value, DBNull.Value, drPrepClub("DeadlineDate"))
                    'mLateRegDeadlineDate = IIf(drPrepClub("LateRegDLDate") Is DBNull.Value, DBNull.Value, drPrepClub("LateRegDLDate"))
                Loop
            End If
            drPrepClub.Close()
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadVenue(ByVal ddlVenue As DropDownList)

        ddlVenue.Items.Clear()
        Dim dsVenue As DataSet
        dsVenue = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsVenue.Tables(0).Rows.Count > 0 Then
            ddlVenue.DataSource = dsVenue
            ddlVenue.DataBind()
        Else
            ' ddlVenue.Items.Insert(0, "")
        End If
        If ddlVenue.Items.Count > 1 Then
            ddlVenue.Items.Insert(0, "Select Venue")
        End If
    End Sub
    Private Sub LoadDropDowns()
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = ''", "SponsorName", DataViewRowState.CurrentRows)
            ddlSponsor.DataSource = dvSponsor
            ddlSponsor.DataBind()
            If ddlSponsor.Items.Count > 1 Then
                ddlSponsor.Items.Insert(0, "Select Sponsor")
            End If
        End If
        ' LoadWeekdays(ddlWeekdays)
        LoadDisplayTime(ddlStartTime1)
        LoadDisplayTime(ddlEndTime1)
        ddlEndTime1.Items(1).Enabled = False
        LoadBldg(ddlBldg)
        LoadRoom(ddlRoom)
    End Sub
    'Private Sub LoadWeekdays(ByVal ddlObject As DropDownList)
    ''  items.Add(new ListItem("Item 2", "Value 2"));
    'ddlObject.Items.Add(New ListItem("Monday", "0"))
    'ddlObject.Items.Add(New ListItem("Tuesday", "1"))
    'ddlObject.Items.Add(New ListItem("Wednesday", "2"))
    'ddlObject.Items.Add(New ListItem("Thursday", "3"))
    'ddlObject.Items.Add(New ListItem("Friday", "4"))
    'ddlObject.Items.Add(New ListItem("Saturday", "5"))
    'ddlObject.Items.Add(New ListItem("Sunday", "6"))

    ' For i As Integer = 0 To 6
    '   ddlObject.Items.Add(New ListItem(WeekdayName(i + 1)))
    ' ddlWeekdays.Items.Add(WeekdayName(i))
    '  Next
    'End Sub
    Private Sub LoadDisplayTime(ByVal ddlObject As DropDownList)
        Try

            Dim dt As DataTable
            dt = New DataTable()
            Dim dr As DataRow
            Dim StartTime As DateTime = "6:30 AM " 'Now()
            dt.Columns.Add("ddlText", Type.GetType("System.String"))
            dt.Columns.Add("ddlValue", Type.GetType("System.String"))
            ' "8:00:00 AM " To "12:00:00 AM "
            While StartTime <= "10:30:00 PM "
                dr = dt.NewRow()

                dr("ddlText") = StartTime.ToString("h:mmtt")
                dr("ddlValue") = StartTime.ToString("h:mmtt")

                dt.Rows.Add(dr)
                StartTime = StartTime.AddHours(0.5) '.AddMinutes(60)
            End While

            ddlObject.DataSource = dt
            ddlObject.DataTextField = "ddlText"
            ddlObject.DataValueField = "ddlValue"
            ddlObject.DataBind()

            'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
            'ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
            ddlObject.Items.Insert(0, "Select time")

        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub LoadBldg(ByVal ddlObject As DropDownList)
        Try
            Dim dsBldg As DataSet = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select Distinct IsNull(Building,'') as Building From Contest Where NsfChapterId=" & Session("LoginChapterID"))
            If dsBldg.Tables(0).Rows.Count > 0 Then
                ddlObject.DataSource = dsBldg
                ddlObject.DataTextField = "Building"
                ddlObject.DataValueField = "Building"
                ddlObject.DataBind()

                If dsBldg.Tables(0).Rows.Count > 1 Then
                    ddlObject.Items.Insert(0, New ListItem("Select Building", ""))
                    'ddlObject.Items.Insert((0), "Select Building")
                    ddlObject.SelectedIndex = 0
                ElseIf dsBldg.Tables(0).Rows.Count = 1 Then
                    ddlObject.Enabled = False
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Private Sub LoadRoom(ByVal ddlObject As DropDownList)
        Try
            Dim dsRoom As DataSet = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select Distinct IsNull(Room,'') as Room From Contest Where NsfChapterId=" & Session("LoginChapterID"))
            If dsRoom.Tables(0).Rows.Count > 0 Then
                ddlObject.DataSource = dsRoom
                ddlObject.DataTextField = "Room"
                ddlObject.DataValueField = "Room"
                ddlObject.DataBind()
                If dsRoom.Tables(0).Rows.Count > 1 Then
                    ddlObject.Items.Insert(0, New ListItem("Select Room", ""))
                    'ddlObject.Items.Insert(0, "Select Room")
                    ddlObject.SelectedIndex = 0
                ElseIf dsRoom.Tables(0).Rows.Count = 1 Then
                    ddlObject.Enabled = False
                End If
                'txtRoom.Text = dsRoom.Tables(0).Rows(0)("Room")
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub FillDropDown()
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
            ddlSponsor.DataSource = dvSponsor
            ddlSponsor.DataBind()
            If ddlSponsor.Items.Count > 0 Then
                ddlSponsor.Items.Insert(0, "")
            End If
        End If
    End Sub

    Private Function InsertPrepClubCalendar(ByVal EventFeesID As Integer) As Boolean
        lblErr.Text = Session("LoginChapterCode")
        lblErr.Visible = True
        Try

            Dim param(26) As SqlParameter
            param(0) = New SqlParameter("@EventID", mintEventID)
            param(1) = New SqlParameter("@EventCode", mstrEventCode)
            param(2) = New SqlParameter("@ProductGroupID", mintProductGroupID)
            param(3) = New SqlParameter("@ProductGroupCode", mstrProductGroupcode)
            param(4) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
            param(5) = New SqlParameter("@ChapterCode", Session("LoginChapterCode"))
            param(6) = New SqlParameter("@ProductID", mintProductID)
            param(7) = New SqlParameter("@ProductCode", mstrProductCode)
            param(8) = New SqlParameter("@EventYear", drpYear.SelectedValue)
            param(9) = New SqlParameter("@EventDay", ddlWeekdays.SelectedItem.Text) ' mdtPrepClubDate1)
            param(10) = New SqlParameter("@EventDate", Convert.ToDateTime(txtStartDate.Text))
            param(11) = New SqlParameter("@EventFee", mRegFee)
            param(12) = New SqlParameter("@StartTime", Convert.ToDateTime(ddlStartTime1.SelectedItem.Value))
            param(13) = New SqlParameter("@EndTime", Convert.ToDateTime(ddlEndTime1.SelectedItem.Value))
            param(14) = New SqlParameter("@Building", IIf(ddlBldg.SelectedItem.Text = "", DBNull.Value, ddlBldg.SelectedItem.Text)) '  txtBldg.Text)
            param(15) = New SqlParameter("@LateFee", mLateFee)

            If ddlVenue.SelectedIndex < 0 Then
                param(16) = New SqlParameter("@VenueID", DBNull.Value)
            Else
                param(16) = New SqlParameter("@VenueID", ddlVenue.SelectedValue)
            End If

            param(17) = New SqlParameter("@CreateDate", Now)
            param(18) = New SqlParameter("@CreatedBy", Session("LoginID"))
            param(19) = New SqlParameter("@EventFeesID", EventFeesID)
            param(20) = New SqlParameter("@Room", IIf(ddlRoom.SelectedItem.Text = "", DBNull.Value, ddlRoom.SelectedItem.Text)) 'DBNull.Value)
            param(21) = New SqlParameter("@RegDeadline", Convert.ToDateTime(txtRegDeadline.Text))

            If txtLateRegDeadline.Text = "" Then
                param(22) = New SqlParameter("@LateRegDLDate", DBNull.Value)
            Else
                param(22) = New SqlParameter("@LateRegDLDate", Convert.ToDateTime(txtLateRegDeadline.Text))
            End If
            ' param(22) = New SqlParameter("@LateRegDLDate", IIf(txtLateRegDeadline.Text = "", "NULL", Convert.ToDateTime(txtLateRegDeadline.Text)))

            param(23) = New SqlParameter("@SponsorId", ddlSponsor.SelectedValue)
            param(24) = New SqlParameter("@SponsorType", rbtnMemberType.SelectedValue)
            param(25) = New SqlParameter("@PrepClubCalID", SqlDbType.Int)
            'param(25).Direction = ParameterDirection.Output
            Dim i As Integer = 0
            'Session("Param") = param
            i = SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertPrepClub_EventCalendar", param)
            If i > 0 Then
                lblDataErr.Text = "Added successfully"
                lblDataErr.Visible = True
            Else
                lblError.Visible = True
                lblError.Text = lblError.Text + "<br> Product Code :" & mstrProductCode & " Event Date : " & mdtPrepClubDate1
            End If

            Return True
        Catch ex As SqlException
            'Response.Write(ex.ToString())
            lblDataErr.Text = ex.Message
            lblDataErr.Visible = True
            Return False
        End Try
    End Function

    Protected Sub grdTarget_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.CancelCommand
        grdTarget.EditItemIndex = -1
        LoadGrid()
        lblGridError.Visible = False
    End Sub

    Protected Sub grdTarget_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.DeleteCommand
        Dim param(1) As SqlParameter

        Try
            lblDataErr.Text = ""
            If e.CommandName = "Delete" Then

                grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
                mintPrepClubID = CInt(e.Item.Cells(0).Text)
                mintEventFeesID = CInt(e.Item.Cells(1).Text)

                param(0) = New SqlParameter("@PrepClubCalID", mintPrepClubID)
                'delete routine
                SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "[usp_DeletePrepClubCalendar]", param)
                lblDataErr.Text = "Deleted Successfully"
                lblDataErr.Visible = True
                ' LoadlstPrepClubs(Integer.Parse(drpYear.SelectedItem.Text))
                LoadGrid("Del")

            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub grdTarget_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.EditCommand
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        mintPrepClubID = CInt(e.Item.Cells(0).Text)
        mintEventFeesID = CInt(e.Item.Cells(1).Text)
        strSponsor = CType(e.Item.Cells(3).FindControl("lblContestSponsor"), Label).Text
        strSponsorID = CType(e.Item.Cells(3).FindControl("lblContestSponsorID"), Label).Text
        strSponsorType = CType(e.Item.Cells(3).FindControl("lblContestSponsorType"), Label).Text
        strVenue = CType(e.Item.Cells(4).FindControl("lblPrepClubVenue"), Label).Text
        strEventDay = CType(e.Item.Cells(5).FindControl("lblEventDay"), Label).Text

        strRegDeadLine = CType(e.Item.Cells(6).FindControl("lblRegDeadLine"), Label).Text
        strStartTime = CType(e.Item.Cells(7).FindControl("lblStartTime"), Label).Text
        strEndTime = CType(e.Item.Cells(8).FindControl("lblEndTime"), Label).Text
        strBuilding = CType(e.Item.Cells(9).FindControl("lblBuilding"), Label).Text
        strRoom = CType(e.Item.Cells(10).FindControl("lblRoom"), Label).Text
        'disenable delete and copy buttons
        Dim intcell As Integer = e.Item.Cells.Count - 1
        Dim myTableCell As TableCell
        myTableCell = e.Item.Cells(intcell)
        If Not CType(e.Item.Cells(intcell).FindControl("btnDelete"), Button) Is Nothing Then
            CType(e.Item.Cells(intcell).FindControl("btnDelete"), Button).Enabled = False
        End If
        LoadGrid()
    End Sub

    Protected Sub grdTarget_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTarget.ItemDataBound
        Dim dsTarget As New DataSet

        ' First, make sure we're NOT dealing with a Header or Footer row
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            'Now, reference the PushButton control that the Delete ButtonColumn has been rendered to
            Dim rownum As Integer
            rownum = e.Item.ItemIndex + 1

            Dim intPrepClubID As String = e.Item.Cells(0).Text

            Dim ddlVenueTemp As DropDownList
            ddlVenueTemp = e.Item.Cells(4).FindControl("ddlPrepClubVenue")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If Not ddlVenueTemp Is Nothing Then
                ddlVenueTemp.DataSource = dsTarget
                ddlVenueTemp.DataBind()
            End If

            Dim ddlSponsorTemp As DropDownList, ddlSponsorTypeTemp As DropDownList
            ddlSponsorTemp = e.Item.Cells(3).FindControl("ddlContestSponsor")
            ddlSponsorTypeTemp = e.Item.Cells(3).FindControl("ddlContestSponsorType")
            dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            If dsTarget.Tables(0).Rows.Count > 0 Then
                Dim dvSponsor As New DataView(dsTarget.Tables(0), "", "SponsorName", DataViewRowState.CurrentRows)
                If Not ddlSponsorTemp Is Nothing Then
                    ddlSponsorTemp.DataSource = dvSponsor
                    ddlSponsorTemp.DataBind()
                    If ddlSponsorTemp.Items.Count > 0 Then
                        ddlSponsorTemp.Items.Insert(0, "")
                    End If
                End If
                If Not ddlSponsorTypeTemp Is Nothing Then
                    ddlSponsorTypeTemp.DataSource = dvSponsor
                    ddlSponsorTypeTemp.DataBind()
                    If ddlSponsorTypeTemp.Items.Count > 0 Then
                        ddlSponsorTypeTemp.Items.Insert(0, "")
                    End If
                End If
            End If

            'Dim ddlPrepClubDay As DropDownList
            'ddlPrepClubDay = e.Item.Cells(4).FindControl("ddlPrepClubDay")
            'LoadWeekdays(ddlPrepClubDay)
            'dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            'If Not ddlPrepClubDay Is Nothing Then
            '    ddlPrepClubDay.DataSource = dsTarget
            '    ddlPrepClubDay.DataBind()
            'End If

            Dim ddlStartTimeTemp As DropDownList
            ddlStartTimeTemp = e.Item.Cells(7).FindControl("ddlStartTime")
            LoadDisplayTime(ddlStartTimeTemp)

            Dim ddlEndTimeTemp As DropDownList
            ddlEndTimeTemp = e.Item.Cells(8).FindControl("ddlEndTime")
            LoadDisplayTime(ddlEndTimeTemp)

            Dim ddlPrepClubBldgTemp As DropDownList
            ddlPrepClubBldgTemp = e.Item.Cells(9).FindControl("ddlPrepClubBldg")
            LoadBldg(ddlPrepClubBldgTemp)
            'dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            'If Not ddlVenueTemp Is Nothing Then
            '    ddlPrepClubBldg.DataSource = dsTarget
            '    ddlPrepClubBldg.DataBind()
            'End If

            Dim ddlPrepClubRoomTemp As DropDownList
            ddlPrepClubRoomTemp = e.Item.Cells(10).FindControl("ddlPrepClubRoom")
            LoadRoom(ddlPrepClubRoomTemp)
            'dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("LoginChapterID")))
            'If Not ddlVenueTemp Is Nothing Then
            '    ddlPrepClubRoom.DataSource = dsTarget
            '    ddlPrepClubRoom.DataBind()
            'End If

            Dim delcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(delcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")

            Dim intPrepClubIDCell As Integer = 1
            Dim PrepClubIDCell As TableCell = e.Item.Cells(intPrepClubIDCell)
            Dim strValue As String = PrepClubIDCell.Text
        End If

    End Sub

    Private Sub LoadGrid()
        Dim dsPrepClubs As New DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(1) = New SqlParameter("@EventID", Session("EventID"))
        dsPrepClubs = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_PrepClubCalendar_GetEventsByChapterID", param)
        ' grdTarget.Visible = False

        If Not dsPrepClubs Is Nothing Then
            If dsPrepClubs.Tables(0).Rows.Count > 0 Then
                grdTarget.DataSource = dsPrepClubs
                grdTarget.DataBind()
                grdTarget.Visible = True
            End If
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_PrepClubCalendar_GetEventsByChapterIDPast", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdTargetOld.DataSource = ds
            grdTargetOld.DataBind()
            grdTargetOld.Visible = True
        Else
            grdTargetOld.Visible = False
        End If
    End Sub

    Private Sub LoadGrid(ByVal action As String)
        Dim dsPrepClubs As New DataSet
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(1) = New SqlParameter("@EventID", Session("EventID"))
        dsPrepClubs = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_PrepClubCalendar_GetEventsByChapterID", param)

        grdTarget.Visible = False
        If Not dsPrepClubs Is Nothing Then
            If dsPrepClubs.Tables(0).Rows.Count > 0 Then
                grdTarget.DataSource = dsPrepClubs
                grdTarget.DataBind()
                grdTarget.Visible = True
                grdTarget.EditItemIndex = -1
                If grdTarget.EditItemIndex <> -1 Then
                    Dim myTableCell As TableCell
                    myTableCell = grdTarget.Items(grdTarget.EditItemIndex).Cells(grdTarget.Items(0).Cells.Count - 1)
                    If Not CType(myTableCell.FindControl("btnCopy"), Button) Is Nothing Then
                        CType(myTableCell.FindControl("btnCopy"), Button).Enabled = False
                    End If
                End If
                'Else
                '    'If action = "Del" Then
                '    Server.Transfer("PrepClubCalendar.aspx")
                'End If



            End If
            If action = "Del" Then
                Server.Transfer("PrepClubCalendar.aspx")
            End If
        End If

    End Sub
    Public Sub SetDropDown_Venue(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strVenue))
    End Sub
    Public Sub SetDropDown_EventDay(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strEventDay))
    End Sub
    Public Sub SetDropDown_Building(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(strBuilding))
    End Sub
    Public Sub SetDropDown_Room(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(strRoom))
    End Sub
    Public Sub SetDropDown_PrepClubDt(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If Trim(strPrepClubDt) <> "" Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strPrepClubDt))
        Else
            ddlTemp.Items(0).Selected = True
        End If
    End Sub

    Public Sub SetDropDown_StartTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strStartTime))
    End Sub

    Public Sub SetDropDown_EndTime(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strEndTime))
    End Sub

    Protected Sub grdTarget_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.UpdateCommand
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim txtEditText As TextBox, txtEditEventDate As TextBox, txtLateDLDate As TextBox, ddlTemp As DropDownList, blnValidData As Boolean, lblText As Label
        Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer
        'Venue
        lblDataErr.Text = ""
        lblGridError.Text = ""
        Dim blnVenueSelected As Boolean
        ddlTemp = e.Item.FindControl("ddlPrepClubVenue")
        If ddlTemp.SelectedIndex < 0 Then
            blnVenueSelected = False
        Else
            blnVenueSelected = True
            mintVenueID = ddlTemp.SelectedValue
        End If

        'validate PrepClubdate
        lblText = e.Item.FindControl("lblGridEditWSDate")
       

        txtEditEventDate = e.Item.FindControl("txtEditEventDate")
        If txtEditEventDate.Text <> "" Then
            mdtPrepClubDate1 = Convert.ToDateTime(txtEditEventDate.Text)
        Else
            lblGridError.Text = "Please Enter EventDate"
            lblGridError.Visible = True
            Exit Sub
        End If

        'RegDeadLine
        txtEditText = e.Item.FindControl("txtEditRegDeadLine")
        If txtEditText.Text <> "" Then
            mdtRegDeadLine = Convert.ToDateTime(txtEditText.Text)
        Else
            lblGridError.Text = "Please Enter Registration DeadLine date"
            lblGridError.Visible = True
            Exit Sub
        End If

        txtLateDLDate = e.Item.FindControl("txtEditLateFeeDeadLine")
        If txtLateDLDate.Text <> "" Then
            mdtLateRegDLDate = Convert.ToDateTime(txtLateDLDate.Text)
        Else
            ' mdtLateRegDLDate = 
            'lblGridError.Text = "Please Enter Registration DeadLine date"
            'lblGridError.Visible = True
            'Exit Sub
        End If

        ddlTemp = e.Item.FindControl("ddlStartTime")
        mstrStartTime = ddlTemp.SelectedItem.Text
        intSelStartTime = 1 + ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlEndTime")
        mstrEndTime = ddlTemp.SelectedItem.Text
        intSelEndTime = 3 + ddlTemp.SelectedIndex

        ddlTemp = e.Item.FindControl("ddlPrepClubDay")
        mstrDay = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlPrepClubBldg")
        mstrBuilding = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)

        ddlTemp = e.Item.FindControl("ddlPrepClubRoom")
        mstrRoom = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)

        'txtEditText = e.Item.FindControl("txtBuilding")
        'mstrBuilding = txtEditText.Text

        'txtEditText = e.Item.FindControl("txtRoom")
        'mstrRoom = txtEditText.Text

        blnValidData = False
        lblGridError.Visible = False
        lblGridError.Text = ""

        If mstrStartTime.Contains("Select") Then
            lblGridError.Text = " Select Start Time"
            lblGridError.Visible = True
            GoTo FinalProcess
        ElseIf mstrEndTime.Contains("Select") Then
            lblGridError.Text = " Select End Time"
            lblGridError.Visible = True
            GoTo FinalProcess
            'ElseIf mstrBuilding.Contains("Select") Then
            '    lblGridError.Text = " Select Building"
            '    lblGridError.Visible = True
            '    GoTo FinalProcess
            'ElseIf mstrRoom.Contains("Select") Then
            '    lblGridError.Text = " Select Room"
            '    lblGridError.Visible = True
            '    GoTo FinalProcess
        End If

        If mstrStartTime <> "" Then
            If intSelEndTime <= intSelStartTime Then lblGridError.Text = " Start Time should be before End Time."
            'If intSelEndTime <= intSelCheckInTime Then lblGridError.Text = " Check In Time should be before End Time."
            If lblGridError.Text <> "" Then
                lblGridError.Visible = True
                blnValidData = False
                GoTo FinalProcess
            End If
            If intSelStartTime < intSelEndTime Then
                blnValidData = True
            Else
                'lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
            If (intSelEndTime - intSelStartTime) >= 2 Then
                blnValidData = True
            Else
                ' lblGridError.Text = " Check In Time should be at least 30min. Before Start Time and End Time should be at least 30min. after the Start Time"
                lblGridError.Visible = True
                GoTo FinalProcess
            End If
        End If

        Dim blnSponsorSelected As Boolean, ddlTempSponsorType As DropDownList
        ddlTemp = e.Item.FindControl("ddlContestSponsor")
        ddlTempSponsorType = e.Item.FindControl("ddlContestSponsorType")
        If ddlTemp.SelectedIndex < 1 Then
            blnSponsorSelected = False
        Else
            blnSponsorSelected = True
            mintSponsorID = ddlTemp.SelectedValue
            mstrSponsorType = ddlTempSponsorType.Items(ddlTemp.SelectedIndex).Text
        End If

        'update routine
        UpdatePrepClubs(blnVenueSelected, blnSponsorSelected)
        grdTarget.EditItemIndex = -1

        'disenable delete buttons
        Dim intcell As Integer = 13 ' e.Item.Cells.Count - 1
        Dim myTableCell As TableCell
        myTableCell = e.Item.Cells(intcell)
        Dim btnDelete As Button = myTableCell.Controls(0)
        btnDelete.Enabled = True

        LoadGrid()
        Exit Sub

FinalProcess:
        grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
        LoadGrid()
    End Sub

    Private Function UpdatePrepClubs(ByVal VenueSelected As Boolean, ByVal SponsorSelected As Boolean) As Boolean
        Try
            lblDataErr.Text = ""
            Dim param(13) As SqlParameter
            If VenueSelected = True Then
                param(0) = New SqlParameter("@VenueId", mintVenueID)
                If mintVenueID <> 0 Then
                    param(0) = New SqlParameter("@VenueId", mintVenueID)
                Else
                    param(0) = New SqlParameter("@VenueId", DBNull.Value)
                End If
            Else
                param(0) = New SqlParameter("@VenueId", DBNull.Value)
            End If

            param(1) = New SqlParameter("@EventDate", mdtPrepClubDate1)
            param(2) = New SqlParameter("@RegDeadline", mdtRegDeadLine)
            param(3) = New SqlParameter("@EventDay", mstrDay)
            param(4) = New SqlParameter("@StartTime", mstrStartTime)
            param(5) = New SqlParameter("@EndTime", mstrEndTime)
            param(6) = New SqlParameter("@Building", IIf(mstrBuilding = "", DBNull.Value, mstrBuilding))
            param(7) = New SqlParameter("@Room", IIf(mstrRoom = "", DBNull.Value, mstrRoom))
            param(8) = New SqlParameter("@ModifiedDate", Now)
            param(9) = New SqlParameter("@ModifiedBy", Session("LoginID"))
            'param(10) = New SqlParameter("@EventFeesID", mintEventFeesID)
            param(10) = New SqlParameter("@PrepClubID", mintPrepClubID)
            If SponsorSelected = True Then
                If mintSponsorID <> 0 Then
                    param(11) = New SqlParameter("@SponsorId", mintSponsorID)
                    param(12) = New SqlParameter("@SponsorType", mstrSponsorType)
                Else
                    param(11) = New SqlParameter("@SponsorId", DBNull.Value)
                    param(12) = New SqlParameter("@SponsorType", DBNull.Value)
                End If
            Else
                param(11) = New SqlParameter("@SponsorId", DBNull.Value)
                param(12) = New SqlParameter("@SponsorType", DBNull.Value)
            End If

            If Not IsDate(mdtLateRegDLDate) Then
                param(13) = New SqlParameter("@LateRegDLDate", DBNull.Value)
            Else
                param(13) = New SqlParameter("@LateRegDLDate", Convert.ToDateTime(mdtLateRegDLDate))
            End If

            SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_UpdatePrepClubCalendar", param)
            lblDataErr.Text = "Updated successfully"
            lblDataErr.Visible = True
            Return True
        Catch ex As SqlException
            'Response.Write(ex.ToString())
            lblDataErr.Text = ex.Message
            lblDataErr.Visible = True
            Return False
        End Try

    End Function
    Public Sub SetDropDown_Sponsor(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor)))
        If ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Trim(strSponsor))) <= 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Trim(strSponsorID)))
        End If
    End Sub
    Protected Sub rbtnMemberType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnMemberType.SelectedIndexChanged
        Dim dsSponsor As DataSet
        dsSponsor = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_SponsorForContests", New SqlParameter("@ChapterID", Session("LoginChapterID")))
        If dsSponsor.Tables(0).Rows.Count > 0 Then
            Select Case rbtnMemberType.SelectedIndex
                Case 0
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'O'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()

                Case 1
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = 'I'", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()
                Case 2
                    Dim dvSponsor As New DataView(dsSponsor.Tables(0), "SponsorType = NULL", "SponsorName", DataViewRowState.CurrentRows)
                    ddlSponsor.DataSource = dvSponsor
                    ddlSponsor.DataBind()
            End Select
        End If
    End Sub

    Protected Sub lstPrepClubs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstPrepClubs.SelectedIndexChanged
        'Dim strSelected As String
        ' Dim strSelecteYear As String
        'strSelecteYear = ""
        Session("Products") = ""
        Dim ProductID As String = ""
        For Each lst As ListItem In lstPrepClubs.Items
            If lst.Selected = True Then
                ' strSelected = lst.Text
                'strSelecteYear = strSelected.Substring(strSelected.Length - 4, 4)
                ProductID = ProductID & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductID from EventFees where EventFeesID =" & lst.Value) & "," 'dsPrepClubs.Tables(0).Rows(lst)("ProductID") & ","
                'Exit For
            End If
        Next
        Session("Products") = ProductID.TrimEnd(",")
        'disableCheckboxes(strSelecteYear)
    End Sub

    Private Sub disableCheckboxes(ByVal stryear As String)
        For Each lst As ListItem In lstPrepClubs.Items
            If lst.Text.Substring(lst.Text.Length - 4, 4) <> stryear And lst.Selected = False Then
                lst.Enabled = False
            Else
                lst.Enabled = True
            End If
        Next
    End Sub

    Protected Sub drpYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpYear.SelectedIndexChanged
        LoadlstPrepClubs(Integer.Parse(drpYear.SelectedItem.Text))
    End Sub

    Protected Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As DayRenderEventArgs)
        If Not e.Day.IsWeekend Then
            e.Day.IsSelectable = False
        End If
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        txtStartDate.Text = Calendar1.SelectedDate
        txtRegDeadline.Text = (Convert.ToDateTime(txtStartDate.Text)).AddDays(-14)
        ddlWeekdays.SelectedIndex = ddlWeekdays.Items.IndexOf(ddlWeekdays.Items.FindByText(Calendar1.SelectedDate.DayOfWeek.ToString()))
        Calendar1.Visible = False
    End Sub

    Protected Sub btnDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDate.Click
        Calendar1.Visible = True
    End Sub

    Protected Sub btnRegDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegDate.Click
        Calendar2.Visible = True
    End Sub

    Protected Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        txtRegDeadline.Text = Calendar2.SelectedDate
        Calendar2.Visible = False
    End Sub

    Protected Sub Calendar3_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar3.SelectionChanged
        txtLateRegDeadline.Text = Calendar3.SelectedDate
        Calendar3.Visible = False
    End Sub

    Protected Sub BtnLateRegDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLateRegDate.Click
        Calendar3.Visible = True
    End Sub

    Protected Sub ddlVenue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVenue.SelectedIndexChanged
        Session("VenueID") = ""
        Dim selectedValue As Integer = ddlVenue.SelectedValue
        ddlVenue.SelectedValue = selectedValue
        Session("VenueID") = ddlVenue.SelectedValue

        'LoadlstPrepClubs(drpYear.SelectedValue)
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        'lblDataErr.Text = ""
        Dim intCtr As Integer
        For intCtr = 0 To lstPrepClubs.Items.Count - 1
            If lstPrepClubs.Items(intCtr).Selected = True Then
                GetPrepClubInfo(lstPrepClubs.Items(intCtr).Value)
                InsertPrepClubCalendar(lstPrepClubs.Items(intCtr).Value)
            End If
        Next
        LoadGrid()
        LoadlstPrepClubs(Integer.Parse(drpYear.SelectedItem.Text))
        tblduplicate.Visible = False
        'Dim param(26) As SqlParameter
        'param = Session("Param")
        'Dim i As Integer = SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertPrepClub_EventCalendar", param) 'Session("Param"))
        'If i > 0 Then
        '    lblDataErr.Text = "Added successfully"
        '    lblDataErr.Visible = True
        '    LoadGrid()
        'Else
        '    lblError.Visible = True
        '    lblError.Text = lblError.Text + "<br> Product Code :" & mstrProductCode & " Event Date : " & mdtPrepClubDate1
        'End If
        ''
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        tblduplicate.Visible = False
    End Sub

    Protected Sub txtStartDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStartDate.TextChanged
        txtRegDeadline.Text = (Convert.ToDateTime(txtStartDate.Text)).AddDays(-14)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblDataErr.Text = ""
        lblErr.Text = ""
        lblError.Text = ""
        ddlVenue.SelectedIndex = 0
        ddlWeekdays.SelectedIndex = 0
        txtStartDate.Text = ""
        txtRegDeadline.Text = ""
        txtLateRegDeadline.Text = ""
        ddlStartTime1.SelectedIndex = 0
        ddlEndTime1.SelectedIndex = 0
        ddlSponsor.SelectedIndex = 0
        ddlBldg.SelectedIndex = 0
        ddlRoom.SelectedIndex = 0
    End Sub
End Class
