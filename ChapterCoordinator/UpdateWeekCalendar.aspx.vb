﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class UpdateWeekCalendar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            'Dim year As Integer = 0
            'year = Convert.ToInt32(DateTime.Now.Year)
            'ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            'ddlEventYear.Items.Insert(1, DateTime.Now.Year.ToString())
            'ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            'ddlEventYear.Items(1).Selected = True
        End If
    End Sub

    Function getEventcode(ByVal EventID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Eventcode from Event where EventID=" & EventID & "")
    End Function
    
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Add new Event
        If ddlEvent.SelectedValue = "0" Then
            lblErr.Text = "Please Select an Event"
            Exit Sub
            'ElseIf ddlProduct.SelectedValue = "0" Then
            '    lblErr.Text = "Please Select a Product"
            '    Exit Sub
            'ElseIf ddlGradeTo.SelectedValue = "0" Then
            '    lblErr.Text = "Please Select Valid Grade for Grade to"
            '    Exit Sub
            'ElseIf Val(txtRegFee.Text) < 1 Then
            '    lblErr.Text = "Please Enter Valid Reg. Fee"
            '    Exit Sub
            'ElseIf Val(txtTax.Text) < 1 Then
            '    lblErr.Text = "Please Enter Tax Deduction Amount"
            '    Exit Sub
        ElseIf Page.IsValid = False Then
            lblErr.Text = "Please enter all values"
            Exit Sub

        ElseIf IsDate(txtDay1Date.Text) = False Or IsDate(txtDay2Date.Text) = False Then
            lblErr.Text = "Invalid Date."
            Exit Sub
        ElseIf Convert.ToDateTime(txtDay1Date.Text) < Now.Date Or Convert.ToDateTime(txtDay2Date.Text) < Now.Date Then
            lblErr.Text = "Cannot be a Past Date."
            Exit Sub

            lblErr.Text = ""
        End If
        Dim sqlstr As String = ""
        If btnAdd.Text = "Add" Then
            'Add New
            ' Response.Write("select COUNT(*) from WeekCalendar where DateDiff(d,SatDay1,'" & txtDay1Date.Text & "') = 0  AND DateDiff(d,SunDay2,'" & txtDay1Date.Text & "') = 0")
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from WeekCalendar where DateDiff(d,SatDay1,'" & txtDay1Date.Text & "') = 0  AND DateDiff(d,SunDay2,'" & txtDay2Date.Text & "') = 0") < 1 Then
                If ddlEvent.SelectedValue = "1" Then
                    sqlstr = "INSERT INTO   WeekCalendar(SatDay1, SunDay2, DinnerDay0, BreakfastDay1, MealDay1, DinnerDay1, BreakfastDay2, MealDay2, DinnerDay2, EventID, EventCode, CreateDate, CreatedBy) VALUES('"
                    sqlstr = sqlstr & txtDay1Date.Text & "','" & txtDay2Date.Text & "'," & txtDay0Dinner.Text & "," & txtDay1Breakfast.Text & "," & txtDay1Lunch.Text & "," & txtDay1Dinner.Text & "," & txtDay2Breakfast.Text & "," & txtDay2Lunch.Text & "," & txtDay2Dinner.Text & "," & ddlEvent.SelectedValue & ",'" & getEventcode(ddlEvent.SelectedValue) & "',GetDate()," & Session("LoginID") & ")"
                ElseIf ddlEvent.SelectedValue = "2" Then
                    sqlstr = "INSERT INTO   WeekCalendar(SatDay1, SunDay2, EventID, EventCode, CreateDate, CreatedBy) VALUES('"
                    sqlstr = sqlstr & txtDay1Date.Text & "','" & txtDay2Date.Text & "'," & ddlEvent.SelectedValue & ",'" & getEventcode(ddlEvent.SelectedValue) & "',GetDate()," & Session("LoginID") & ")"
                Else
                    Exit Sub
                End If
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                    clear()
                    traddUpdate.Visible = False
                    lblError.Text = "Added Sucessfully"
                    loadgrid()
                Catch ex As Exception
                    Response.Write(sqlstr)
                End Try

            Else
                lblErr.Text = "This record already Exist"
            End If
            ElseIf lblWeekId.Text.Length > 0 Then
            'Update Event
            Try
                'Response.Write("select COUNT(*) from WeekCalendar where DateDiff(d,SatDay1,'" & txtDay1Date.Text & "')=0 AND DateDiff(d,SunDay2,'" & txtDay2Date.Text & "')=0 AND WeekID not in (" & lblWeekId.Text & ")")
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from WeekCalendar where DateDiff(d,SatDay1,'" & txtDay1Date.Text & "')=0 AND DateDiff(d,SunDay2,'" & txtDay2Date.Text & "')=0 AND WeekID not in (" & lblWeekId.Text & ")") < 1 Then
                    If ddlEvent.SelectedValue = "1" Then
                        sqlstr = "Update WeekCalendar SET SatDay1 ='" & txtDay1Date.Text & "', SunDay2='" & txtDay2Date.Text & "',  DinnerDay0=" & txtDay0Dinner.Text & ", BreakfastDay1=" & txtDay1Breakfast.Text & ", MealDay1=" & txtDay1Lunch.Text & ", DinnerDay1=" & txtDay1Dinner.Text & ", BreakfastDay2=" & txtDay2Breakfast.Text & ""
                        sqlstr = sqlstr & ",  MealDay2=" & txtDay2Lunch.Text & ", DinnerDay2=" & txtDay2Dinner.Text & ", ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & "  WHERE WeekID = " & lblWeekId.Text
                    ElseIf ddlEvent.SelectedValue = "2" Then
                        sqlstr = "Update WeekCalendar SET SatDay1 ='" & txtDay1Date.Text & "', SunDay2='" & txtDay2Date.Text & "', ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & "  WHERE WeekID = " & lblWeekId.Text
                    Else
                        Exit Sub
                    End If
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                    clear()
                    traddUpdate.Visible = False
                    lblError.Text = "Updated Sucessfully"
                    loadgrid()

            Else
                lblErr.Text = "This record already Exist, So you can't update"
                End If
            Catch ex As Exception
                Response.Write(sqlstr)
            End Try
            End If

    End Sub


    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlEvent.SelectedValue = 0 Then
            lblError.Text = ""
            traddUpdate.Visible = False
            loadgrid()
        End If
        If ddlEvent.SelectedValue = "1" Then
            trfinals.Visible = True
            RequiredFieldValidator1.EnableClientScript = True
            RequiredFieldValidator2.EnableClientScript = True
            RequiredFieldValidator3.EnableClientScript = True
            RequiredFieldValidator4.EnableClientScript = True
            RequiredFieldValidator5.EnableClientScript = True
            RequiredFieldValidator6.EnableClientScript = True
            RequiredFieldValidator7.EnableClientScript = True
        ElseIf ddlEvent.SelectedValue = "2" Then
            trfinals.Visible = False
            RequiredFieldValidator1.EnableClientScript = False
            RequiredFieldValidator2.EnableClientScript = False
            RequiredFieldValidator3.EnableClientScript = False
            RequiredFieldValidator4.EnableClientScript = False
            RequiredFieldValidator5.EnableClientScript = False
            RequiredFieldValidator6.EnableClientScript = False
            RequiredFieldValidator7.EnableClientScript = False
        End If
    End Sub

    'Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    loadgrid()
    'End Sub

    Private Sub DGWeekCalendar_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGWeekCalendar.ItemCreated
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.FindControl("lbtnRemove"), LinkButton) '(e.Item.Cells(1).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub DGWeekCalendar_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim WeekID As Integer = CInt(e.Item.Cells(2).Text)
        lblErr.Text = ""
        lblError.Text = ""
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from WeekCalendar Where WeekID=" & WeekID & "")
                loadgrid()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            btnAdd.Text = "Update"
            lblWeekId.Text = WeekID.ToString()
            loadforUpdate(WeekID)
        End If
    End Sub

    Private Sub loadforUpdate(ByVal WeekID As Integer)
        ' For update 
        traddUpdate.Visible = True
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from WeekCalendar Where WeekID=" & WeekID & "")
        'txtRegFee.Text = ds.Tables(0).Rows(0)("RegFee")
        'SatDay1, SunDay2, DinnerDay0, BreakfastDay1, MealDay1, DinnerDay1, BreakfastDay2, MealDay2, DinnerDay2, EventID,
        txtDay1Date.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("SatDay1")), String.Empty, ds.Tables(0).Rows(0)("SatDay1"))
        txtDay2Date.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("SunDay2")), String.Empty, ds.Tables(0).Rows(0)("SunDay2"))
        If ddlEvent.SelectedValue = "1" Then
            txtDay0Dinner.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("DinnerDay0")), String.Empty, Format$(ds.Tables(0).Rows(0)("DinnerDay0"), "Currency").ToString.Replace("$", ""))
            txtDay1Breakfast.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("BreakfastDay1")), String.Empty, Format$(ds.Tables(0).Rows(0)("BreakfastDay1"), "Currency").ToString.Replace("$", ""))
            txtDay1Lunch.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("MealDay1")), String.Empty, Format$(ds.Tables(0).Rows(0)("MealDay1"), "Currency").ToString.Replace("$", ""))
            txtDay1Dinner.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("DinnerDay1")), String.Empty, Format$(ds.Tables(0).Rows(0)("DinnerDay1"), "Currency").ToString.Replace("$", ""))
            txtDay2Breakfast.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("BreakfastDay2")), String.Empty, Format$(ds.Tables(0).Rows(0)("BreakfastDay2"), "Currency").ToString.Replace("$", ""))
            txtDay2Lunch.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("MealDay2")), String.Empty, Format$(ds.Tables(0).Rows(0)("MealDay2"), "Currency").ToString.Replace("$", ""))
            txtDay2Dinner.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("DinnerDay2")), String.Empty, Format$(ds.Tables(0).Rows(0)("DinnerDay2"), "Currency").ToString.Replace("$", ""))
        End If
    End Sub


    Private Sub loadgrid()
        Dim strSQL As String = "Select W.WeekId, W.SatDay1, W.SunDay2, W.DinnerDay0, W.BreakfastDay1, W.MealDay1, W.DinnerDay1, W.BreakfastDay2, W.MealDay2, W.DinnerDay2, W.CreateDate, W.CreatedBy,W.EventID, W.EventCode,E.Name  From WeekCalendar W Inner Join Event E On E.EventId = W.EventID WHERE W.EventID = " & ddlEvent.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        DGWeekCalendar.DataSource = dt
        DGWeekCalendar.DataBind()
        If (Count < 1) Then
            lblErr.Text = "No record available."
        Else
            lblErr.Text = ""
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        'add new button
        clear()
        If ddlEvent.SelectedValue = "0" Then
            traddUpdate.Visible = False
            lblError.Text = "Please select Event"
        Else
            lblError.Text = ""
            traddUpdate.Visible = True
        End If
    End Sub

    Private Sub clear()
        btnAdd.Text = "Add"
        lblWeekId.Text = ""
        txtDay1Date.Text = String.Empty
        txtDay2Date.Text = String.Empty
        txtDay0Dinner.Text = "0.00"
        txtDay1Breakfast.Text = "0.00"
        txtDay1Lunch.Text = "0.00"
        txtDay1Dinner.Text = "0.00"
        txtDay2Breakfast.Text = "0.00"
        txtDay2Lunch.Text = "0.00"
        txtDay2Dinner.Text = "0.00"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub
End Class

