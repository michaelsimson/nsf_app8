﻿<%@ Page Language="VB" MaintainScrollPositionOnPostback="true" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRReg.aspx.vb" Inherits="FundRReg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <%--<script type="text/javascript"  src="../../js/jquery-1.7.2.min.js"></script>--%>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <style type="text/css">
         
        input[type=checkbox]  {
 -webkit-appearance: inherit;
  border: 1px solid #000;
  line-height:12px;
  height:12px;
  width:12px;
  vertical-align:middle;
  background-color:white;
  font-weight:bold;
}

input[type=checkbox]:focus {
  outline:none;
}

input[type=checkbox]:checked  {
 padding:0;
}

input[type=checkbox]:checked:after  {
 content:"✔";
}

    a.kinda-link:hover {
            cursor: pointer;
        }
        .ac-wrapper {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.6);
            z-index: 1001;
        }
        .popup {
            width: 820px;
            height: 310px;
            background: #FFFFFF;
            border: 2px solid #8CC403;
            border-radius: 25px;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            box-shadow: #8CC403 0px 0px 3px 3px;
            -moz-box-shadow: #8CC403 0px 0px 3px 3px;
            -webkit-box-shadow: #8CC403 0px 0px 3px 3px;
            position: relative;
            top: 200px;
            left: 350px;
        }
               .popupTicket {
            width: 360px;
            height: 165px;
            background: #FFFFFF;
            border: 2px solid #8CC403;
            /*border-radius: 25px;*/
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            box-shadow: #036BC4 0px 0px 3px 3px;
            -moz-box-shadow: #8CC403 0px 0px 3px 3px;
            -webkit-box-shadow: #8CC403 0px 0px 3px 3px;
            position: relative;
            top: 200px;
            left: 550px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        window.history.forward(1);
        function PopupPicker(ctl, w, h) {
            var PopupWindow = null;
            settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl, 'DatePicker', settings);
            PopupWindow.focus();
        }
        function PopUpTicket(hideOrshow,msg) {
            if (hideOrshow == 'hide') document.getElementById('divTicket').style.display = "none";
            else document.getElementById('divTicket').removeAttribute('style');
            var err = document.getElementById('spErrorMsg');
            err.innerHTML = msg;
        }
        function PopUpNoOfConts(hideOrshow, Qty) {          
            if (hideOrshow == 'hide') document.getElementById('wrapNoOfConts').style.display = "none";
            else document.getElementById('wrapNoOfConts').removeAttribute('style');
            if (Qty > 0) {
                var txtHFBeeQty = document.getElementById('<%=txtHFBeeQty.ClientID%>');
	   	        var txtBeeTotQty = document.getElementById(txtHFBeeQty.value);
                // txtBeeTotQty.value = totQty;
	   	       // alert('>>:'+txtHFBeeQty.value);
	   	        txtBeeTotQty.value = Qty;
	   	        try {
	   	            var txtHFBeeAmount = document.getElementById('<%=txtHFBeeAmount.ClientID%>');
	   	            var txtBeeTotAmount = document.getElementById(txtHFBeeAmount.value);
	   	            txtBeeTotAmount.innerHTML = Qty * 10;
	   	        } catch (ex) { }
                   document.getElementById('<%=btnCalc.ClientID%>').click();
	   	    }
           }
           function btnSubmitTotal_Click() {
               var txtHFBeeAmount = document.getElementById('<%=txtHFBeeAmount.ClientID%>');
                var txtBeeTotAmount = document.getElementById(txtHFBeeAmount.value);
                txtBeeTotAmount.innerHTML = tot;

                var txtHFBeeQty = document.getElementById('<%=txtHFBeeQty.ClientID%>');
                var txtBeeTotQty = document.getElementById(txtHFBeeQty.value);
                // txtBeeTotQty.value = totQty;
                txtBeeTotQty.value = result;
                PopUpNoOfConts('hide');
            }
            function ConfMissedToSelBee() {
                if (confirm("No child in the family is going to participate in contests. Do you want to Continue?")) {
                    document.getElementById('<%=hfMissedToSelBee.ClientID%>').value = "false";
                    document.getElementById('<%=btnContinue.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=hfMissedToSelBee.ClientID%>').value = "true";
                }
                //return false;
            }           
            function checkDate(field) {
                try {
                    var allowBlank = true;
                    var minYear = 1902;
                    var maxYear = (new Date()).getFullYear();
                    var errorMsg = "";
                    // regular expression to match required date format
                    re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

                    if (field.value != '') {
                        if (regs = field.value.match(re)) {
                            if (regs[2] < 1 || regs[2] > 31) {
                                errorMsg = "Invalid value for day: " + regs[1];
                            } else if (regs[1] < 1 || regs[1] > 12) {
                                errorMsg = "Invalid value for month: " + regs[2];
                            } else if (regs[3] < minYear || regs[3] > maxYear) {
                                errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear + " and " + maxYear;
                            }
                        } else {
                            errorMsg = "Invalid date format: " + field.value;
                        }
                    } else if (!allowBlank) {
                        errorMsg = "Empty date not allowed!";
                    }
                    //  alert(field & ':' & errorMsg);
                    if (errorMsg != "") {
                        field.focus();
                        return false;
                    }
                } catch (ex) { //alert(ex.toString()); 
                }
                return true;
            }
            function IsValidChild() {
                try {
                    var err = document.getElementById('<%=err.ClientID%>');
                    err.innerHTML = '';
                    var grid = document.getElementById('_ctl0_Content_main_dgChild');
                    var grdId = '<%=dgChild.ClientID%>';// '_ctl0_Content_main_dgChild';
                    var rowCnt = grid.rows.length;
                    var memberId = document.getElementById('<%=hfMemberId.ClientId%>');                   
                    var i = 2;
                    var txtFName = "";
                    var txtLName = "";
                    var curDate = new Date();
                    curDate = curDate.getYear() - 3;          
                    var chkSB, chkMB, chkSC, chkGB;
                    var chkSelected = false;
                    var dob, ddlGender, ddlGrade;
                    for (var i = 2; i <= 4; i++) {
                        chkSB = document.getElementById(grdId + '__ctl' + i + '_chkSpelling');
                        chkMB = document.getElementById(grdId + '__ctl' + i + '_chkMath');
                        chkSC = document.getElementById(grdId + '__ctl' + i + '_chkScience');
                        chkGB = document.getElementById(grdId + '__ctl' + i + '_chkGeography');
                        var exe_insert = false;
                        if (chkSB != null && chkSB.type == "checkbox" && chkSB.checked == true && chkSB.disabled == false) {
                            exe_insert = true;
                            chkSelected = true;
                        }
                        if (chkMB != null && chkMB.type == "checkbox" && chkMB.checked == true && chkMB.disabled == false) {
                            exe_insert = true;
                            chkSelected = true;
                        }
                        if (chkSC != null && chkSC.type == "checkbox" && chkSC.checked == true && chkSC.disabled == false) {
                            exe_insert = true;
                            chkSelected = true;
                        }
                        if (chkGB != null && chkGB.type == "checkbox" && chkGB.checked == true && chkGB.disabled == false) {
                            exe_insert = true;
                            chkSelected = true;
                        }
                        if (exe_insert == true) {
                            txtFName = document.getElementById(grdId + '__ctl' + i + '_txtFirstName');
                            txtLName = document.getElementById(grdId + '__ctl' + i + '_txtLastName');
                            dob = document.getElementById(grdId + '__ctl' + i + '_txtDOB');
                            ddlGender = document.getElementById(grdId + '__ctl' + i + '_ddlGender');
                            ddlGrade = document.getElementById(grdId + '__ctl' + i + '_ddlGrade');
                         //  alert('tt'+txtFName.value);
                            if (txtFName == null) {
                                continue;
                            }
                            if (txtFName.value.length == 0) {
                                err.innerHTML = 'Plase enter Child First Name';
                                return false;
                            }
                            if (txtLName.value.length == 0) {
                                err.innerHTML = 'Plase enter Child Last Name';
                                return false;
                            }
                            else if (dob.value.length == 0) {
                                err.innerHTML = 'Date of Birth Should be Valid Date in MM/DD/YYYY form';
                                return false;
                            }
                            else if (checkDate(dob) == false) {
                                err.innerHTML = 'Date of Birth Should be Valid Date in MM/DD/YYYY form';
                                return false;
                            }
                            else if (dob.value > curDate) {
                                err.innerHTML = 'Child Should be atleast 3 Years old';
                                return false;
                            }
                            //else {
                            //   // alert(1);
                            //    var url = "http://www.northsouth.org/app8/childinfo.asmx";
                            //    $.ajax({
                            //        type: "POST",
                            //        url: url + "/IsDupliateChild",
                            //        data: "{'MemberId': '" + memberId + "','FName': '" + txtFName.value + "','LName': '" + txtLName.value + "', 'DOB': '" + dob.value + "', 'Gender': '" + ddlGender.value + "', 'Grade': " + ddlGrade.value + " } ",
                            //        async: false,
                            //        contentType: "application/json; charset=utf-8",
                            //        dataType: "json",
                            //        processdata: true,
                            //        success: function (json) {
                            //            //alert(2);
                            //            if (JSON.stringify(json) == "True") { alert("Duplicate Exists!"); return false; }
                            //        },
                            //        error: function (e) {
                            //         //  alert('a = '+e.responseText);
                            //            return false;
                            //        }
                            //    });
                           // }
                        }

                    }//end for
                    if (chkSelected == false) {
                        alert("Please select a checkbox or press Cancel to quit.");
                        return false;
                    }
                    
                    return true;               
                }
    catch (ex) {    // alert('Err :'+ ex); 
        return false;
    }   
}

        function ConfClear() {
            if (confirm('Are you sure to clear/delete unpaid item?')) {
                return true;
            }
            return false;
        }

    </script>
    <asp:HiddenField ID="hfMissedToSelBee" runat="server" Value="true" />
    <asp:HiddenField ID="hfMemberId" runat="server" Value="False" />

    <span style="display: none">
        <asp:Button ID="btnFillChild" runat="server" Text="Button" />
        <asp:Button ID="btnCalc" runat="server" Text="Calc" OnClick="btnCalc_Click" />
    </span>
    <asp:hiddenfield runat="server" id="hfTicketContinue" value="False"></asp:hiddenfield>


    <div id="divTicket" class="ac-wrapper" style="display:none;" >
        <div class="popupTicket">           
           
               <table  cellpadding="2" cellspacing="0" width="100%">
                    <tr style="background-color: #036BC4;">
                        <td colspan="2" style="color: white; font-weight: bold">Dinner Ticket<span style="float: right">                                           
                            <asp:Button Width="31px" ID="btnTicketClose" runat="server" CssClass="SmallFont" Text="X" OnClick="btnTicketCancel_Click"></asp:Button>
                        </span></td>
                    </tr>  
                   <tr>
                       
                       <td colspan="2"><br /><center><span id="spErrorMsg" style="color:red"></span></center> <br /></td>
                      
                   </tr>             
                   <tr>
                       <td><span  style="float:right"><asp:button runat="server" text="Buy Extra Tickets / Modify Attendees" id="btnTicketCancel"/></span>&nbsp;&nbsp;</td>
                       <td> <asp:button runat="server" text="Ok, Continue" id="btnOkContinue" /></td>
                   </tr>
                   </table>
            </div>

    </div>


    <div id="wrapNoOfConts" class="ac-wrapper" style="display: none">
        <div class="popup">
            <div style="margin: 10px">
                <asp:TextBox ID="txtHFBeeAmount" runat="server" Style="display: none;" />
                <asp:TextBox ID="txtHFBeeQty" runat="server" Style="display: none;" />


                <table id="tblNoOfConts" runat="server" cellpadding="4" cellspacing="0" width="100%">
                    <tr style="background-color: #ffffcc;">
                        <td colspan="2" style="color: green; font-weight: bold">Contest Selection by Child <span style="float: right">                                           
                            <asp:Button Width="70px" ID="btnClosePopup" runat="server" CssClass="SmallFont" Text="X" OnClick="btnCancelTotal_Click"></asp:Button>
                        </span></td>
                    </tr>               
                    <tr id="trContest">
                        <td colspan="2" align="center">
                      <center> <font style="color:red;">   <asp:Label ID="err" runat="server" ></asp:Label></font></center>
                           
                            <asp:DataGrid ID="dgChild" runat="server" CssClass="GridStyle" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="2px" CellPadding="6"
                                DataKeyField="ChildNumber" BackColor="White" BorderColor="#464365" BorderStyle="Solid" GridLines="Horizontal" ShowFooter="true" HeaderStyle-HorizontalAlign="Center" 
                                 ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"  >
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Child"  ItemStyle-HorizontalAlign="Left" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerNo" runat="server" Text='<%# Container.ItemIndex + 1%>' />
                                           
                                            <span style="display: none">                                             
                                                <asp:Label ID="lblChildNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildNumber")%>' />
                                                <asp:Label ID="lblGrade" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Grade")%>' /> 
                                                <asp:Label ID="lblGender" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gender")%>' /> 
                                                <asp:Label ID="lblSB" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "SB")%>' /> 
                                                <asp:Label ID="lblMB" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "MB")%>' /> 
                                                <asp:Label ID="lblSC" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "SC")%>' /> 
                                                <asp:Label ID="lblGB" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "GB")%>' /> 
                                            </span>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                           <b> Total</b>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>

                                     <asp:TemplateColumn HeaderText="First Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FIRST_NAME")%>'  Width="90" />
                                            <asp:TextBox ID="txtFirstName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FIRST_NAME")%>' Visible="false"  Width="90"  />

                                            </ItemTemplate>
                                     </asp:TemplateColumn>
                                     
                                     <asp:TemplateColumn HeaderText="Last Name" ItemStyle-HorizontalAlign="Center" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LAST_NAME")%>'  Width="90" />
                                            <asp:TextBox ID="txtLastName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LAST_NAME")%>' Visible="false"   Width="90"  />
                                            </ItemTemplate>
                                     </asp:TemplateColumn>
                                      
                                     <asp:TemplateColumn HeaderText="Gender" ItemStyle-Width="90">
                                        <ItemTemplate> 
                                              <asp:DropDownList ID="ddlGender" runat="server" >
                                                <asp:ListItem Value="Male">Male</asp:ListItem>
                                                <asp:ListItem Value="Female">Female</asp:ListItem>
                                                  </asp:DropDownList> 
                                            </ItemTemplate>
                                     </asp:TemplateColumn>
                                 <asp:TemplateColumn HeaderText="Date Of Birth">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOB" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOB")%>' DataFormatString="{0:d}" Width="80" />
                                            <asp:TextBox ID="txtDOB" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOB")%>' DataFormatString="{0:d}" Width="80" Visible="false"  />
                                            </ItemTemplate>
                                     </asp:TemplateColumn>                                     
                                    <asp:TemplateColumn HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlGrade" runat="server" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged" AutoPostBack="true"  >
                                                <asp:ListItem>0</asp:ListItem>
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Spelling">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSpelling" runat="server" OnClick="" OnCheckedChanged="chkSelectProduct_CheckedChanged" AutoPostBack="true" Checked='<%# IIf(Eval("SB") > 0, True, False)%>'
                                                Enabled='<%# IIf(Eval("SB") > 0, False, True)%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Math">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkMath" runat="server" OnCheckedChanged="chkSelectProduct_CheckedChanged" AutoPostBack="true" Checked='<%# IIf(Eval("MB") > 0, True, False)%>'
                                                Enabled='<%# IIf(Eval("MB") > 0, False, True)%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Science">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkScience" runat="server" OnCheckedChanged="chkSelectProduct_CheckedChanged" AutoPostBack="true" Checked='<%# IIf(Eval("SC") > 0, True, False)%>'
                                                Enabled='<%# IIf(Eval("SC") > 0, False, True)%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Geography">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkGeography" runat="server" OnCheckedChanged="chkSelectProduct_CheckedChanged" AutoPostBack="true" Checked='<%# IIf(Eval("GB") > 0, True, False)%>'
                                                Enabled='<%# IIf(Eval("GB") > 0, False, True)%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotal" runat="server" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>

                            </asp:DataGrid>
                           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Note : Grade is as of the <asp:Label ID="lblEventDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trSubmit">
                        <td colspan="2" align="center">
                            <asp:Button Width="70px" ID="btnContestRegSubmit" runat="server" CssClass="SmallFont" Text="Submit" OnClientClick="return IsValidChild();" OnClick="btnContestRegSubmit_Click"></asp:Button>

                            <asp:Button Width="70px" ID="btnCancelTotal" runat="server" CssClass="SmallFont" Text="Cancel" OnClick="btnCancelTotal_Click"></asp:Button>
                        </td>
                    </tr>

 
                     
                </table>

            </div>
        </div>
    </div>
    <div align="left">
        &nbsp;&nbsp;&nbsp;<asp:HyperLink ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx" CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
    </div>
    <div align="center" runat="server" id="div1">
        <table border="0" cellpadding="2" cellspacing="0" width="1000">
            <tr>
                <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSrchParent" OnClick="btnSrchParent_Click" runat="server" Text="Search Patron" Visible="false" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="ltl1" runat="server"></asp:Literal></td>
            </tr>

            <tr>
                <td align="center">
                    <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
                        <b>Search NSF Patron</b>
                        <div align="center">
                            <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%" visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name/ Organization 
                Name :</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                                </tr>
                                <%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
                        </div>
                        <br />
                        <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Search Result</b>
                            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                                <Columns>
                                    <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
            <tr id="trH1" visible="false" runat="server">
                <td align="center">
                    <div style="font-size: 14px; color: #008000; font-weight: bold">
                        <asp:Label ID="lblEventDesc" runat="server"></asp:Label>
                    </div>
                    Make Your Selections
     <br />
                    <asp:GridView Visible="false" DataKeyNames="FundRCalID" GridLines="Both" ID="gvEvents" runat="server" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" CssClass="GridStyle" CaptionAlign="Top" BackColor="White" BorderColor="#CCCCCC" BorderWidth="2px" CellPadding="5" EnableSortingAndPagingCallbacks="false">
                        <Columns>
                            <asp:ButtonField CommandName="Select" Text="Select Event" HeaderText="Select"></asp:ButtonField>
                            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />
                            <asp:BoundField DataField="EventDate" HeaderText="EventDate" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="Timings" HeaderText="Timings" />
                            <asp:BoundField DataField="VenueName" HeaderText="Venue" />
                            <asp:BoundField DataField="EventDescription" HeaderText="EventDescription" />
                        </Columns>
                        <HeaderStyle BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" ForeColor="White" />
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    </asp:GridView>
                </td>
            </tr>
            <tr id="trH2" visible="false" runat="server">
                <td align="center">

                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="center">
                                <table><tr>
                                    <td>

                                    
                                <asp:DataGrid ID="dgCatalog" OnItemDataBound="dgCatalog_ItemDataBound" runat="server" CssClass="GridStyle" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderWidth="2px" CellPadding="3"
                                    DataKeyField="FundRFeesID" BackColor="White" BorderColor="#464365"
                                    BorderStyle="Solid" GridLines="Horizontal">
                                    <FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
                                    <SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
                                    <AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
                                    <ItemStyle HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
                                    <HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="White" Font-Bold="true" Wrap="False"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label>
                                                <asp:Label ID="lblProductGroupId" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupId") %>'>		</asp:Label>
                                                <asp:Label ID="lblProductGroupCode" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupCode") %>'>		</asp:Label>
                                                <asp:Label ID="lblFeeFrom" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeFrom") %>'>		</asp:Label>
                                                <asp:Label ID="lblFeeTo" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeTo") %>'>		</asp:Label>
                                                <asp:Label ID="lblFeeEnabled" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeEnabled") %>'>		</asp:Label>
                                                <asp:Label ID="lblQuantityEnabled" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityEnabled") %>'>		</asp:Label>
                                                <asp:Label ID="lblPaymentMethod" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label>
                                                <asp:Label runat="server" ID="lblAmount" Visible="false" CssClass="SmallFont"></asp:Label>
                                                <asp:Label ID="lblFundRFeesID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label>
                                                <asp:Label ID="lblProductCode" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'>	</asp:Label>
                                                <asp:Label ID="lblShare" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Share") %>'></asp:Label>
                                                <asp:Label ID="lblSelection" Visible="false" runat="server" CssClass="SmallFont">	</asp:Label>
                                                <asp:Label ID="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Fee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmt" runat="server" CssClass="SmallFont">													
                                                </asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Amount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:TextBox Width="50px" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" ID="txtAmount" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F0}") %>'></asp:TextBox>

                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlPaymntMthd" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymntMthd_SelectedIndexChanged" Enabled="false" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:TextBox Width="50px" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" ID="txtQuantity" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:TextBox>
                                                <asp:DropDownList ID="ddlQty" runat="server" OnSelectedIndexChanged="ddlQty_SelectedIndexChanged" AutoPostBack="true" visible="false"></asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Check" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCheckAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Credit_Card" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditCardAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="In Kind" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInKindAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Tax Deductibile %" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltaxDeduction" Visible="true" runat="server" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"TaxDeduction") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid>
<table cellpadding="5" cellspacing="0" border="0" style="float:right;">
                                    <tr>
                                        <td style="display:none;">Check </td>
                                        <td style="display:none;">:
                                            <asp:Label ID="lblCheck" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="display:none;">CreditCard </td>
                                        <td style="display:none;">:
                                            <asp:Label ID="lblCredit" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="display:none;">In Kind </td>
                                        <td style="display:none;">:
                                            <asp:Label ID="lblInKind" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                            <asp:Label ID="lblTotal" runat="server"></asp:Label></td>
                                    </tr>
                                </table>

</td>
                                    <td>
                                        <table cellpadding="3px">
                                            <tr><td> &nbsp;</td></tr>
                                               <tr> <td><b>Up to 2 adults & 2 children are included</b></td></tr>
                                               <tr> <td> &nbsp;</td></tr>
                                                <tr><td><b>For more than 4, buy extra tickets</b></td></tr>
                                               <tr> <td style="text-wrap:none">&nbsp;</td></tr>
                                        </table>
                                    </td>
                                       </tr></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="center">

                                <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" OnClientClick="return ConfClear()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblerr" ForeColor="Red" runat="server"></asp:Label>
                    <asp:Label ID="lblCustIndID" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblFundRCalID" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblFundRFeesID" ForeColor="White" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trH4" visible="false" runat="server">
                <td align="center" style="padding: 2px 2px 2px 2px" class="title04">Table 2: Previously Paid Selections
  <asp:DataGrid ID="dgPaidProducts" CellPadding="4"
      OnItemDataBound="dgPaidProducts_ItemDataBound" runat="server" CssClass="GridStyle" AllowSorting="True"
      AutoGenerateColumns="False" BorderWidth="2px"
      BackColor="White" BorderColor="#CC9966"
      BorderStyle="Solid">
      <FooterStyle CssClass="GridFooter" BackColor="#FFFFCC" ForeColor="#330099"></FooterStyle>
      <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399"></SelectedItemStyle>
      <AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
      <ItemStyle HorizontalAlign="Left" Wrap="true" BackColor="White"
          ForeColor="#330099"></ItemStyle>
      <HeaderStyle Wrap="true" BackColor="#990000" Font-Bold="True"
          ForeColor="#FFFFCC"></HeaderStyle>
      <Columns>
          <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label ID="lblProductID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label>
                  <asp:Label runat="server" ID="lblAmount" Visible="false" CssClass="SmallFont"></asp:Label>
                  <asp:Label ID="lblFundRFeesID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label>
                  <asp:Label ID="lblProductCode" runat="server" Visible="false" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
                  <asp:Label ID="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label runat="server" ID="lblSelfees" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label runat="server" ID="lblSelPaymentMode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentMode") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label runat="server" ID="lblQuantity" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Check" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label ID="lblCheckAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Credit_Card" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label ID="lblCreditCardAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="In Kind" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label ID="lblInKindAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Payment_Info" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
              <ItemTemplate>
                  <asp:Label ID="lblPaymentMode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'></asp:Label>
                  &nbsp;&nbsp;
        <asp:Label ID="lblPaymentDate" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentDate","{0:d}") %>'></asp:Label>
                  <br />
                  <asp:Label ID="lblPaymentReference" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>'></asp:Label>
              </ItemTemplate>
              <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
              <ItemStyle Wrap="False"></ItemStyle>
          </asp:TemplateColumn>
      </Columns>
      <PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
  </asp:DataGrid>

                </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="div2" align="center" visible="false">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="left">Select Contest</td>
                <td align="left">
                    <asp:ListBox ID="lstProducts" DataTextField="ProductName" DataValueField="ProductID" SelectionMode="Multiple" runat="server"></asp:ListBox></td>
            </tr>
            <tr>
                <td align="left">Payment Method</td>
                <td align="left">
                    <asp:DropDownList ID="ddlProductPaymentmthd" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnPrdContinue" runat="server" OnClick="btnPrdContinue_Click" Text="Continue" />&nbsp;&nbsp;
		   <asp:Button ID="btnPrdCancel" runat="server" OnClick="btnPrdCancel_Click" Text="Cancel" /><br />
                    <asp:Label ID="lblPrdError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>



    </div>

    </div>
    
</asp:Content>
