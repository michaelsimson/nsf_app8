<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="FailComplaint.aspx.cs" Inherits="FailComplaint" Title="FailComplaint" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" Runat="Server">

</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">

    <asp:HyperLink ID="hlink" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Page</asp:HyperLink>&nbsp;&nbsp;&nbsp;					    
			   
    <center>
        <table style="margin-left: 150px;">
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Select Type"></asp:Label></td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlOption" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlOption_SelectedIndexChanged" Width="150">
                        <asp:ListItem Value="messagesFailed">Failed</asp:ListItem>
                        <asp:ListItem Value="messagesFblReported">Complaint</asp:ListItem>
                        <asp:ListItem Value="messagesAbuse">Complaint_Abuse</asp:ListItem>
                        <asp:ListItem Value="AbuseActive">Abuse_Active</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Select Complaint/Failure Type</td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlType_SelectedIndexChanged">

                        <%--<asp:ListItem Value="All">All</asp:ListItem>
        <asp:ListItem Value="Abuse">Abuse</asp:ListItem>
        <asp:ListItem Value="Bounce">Bounce</asp:ListItem>
        <asp:ListItem Value="Fail">Fail</asp:ListItem>--%>
                    </asp:DropDownList></td>
                <td>&nbsp;</td>
                <td>Email :
    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>

                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="btnRetrieve" runat="server" Text="Generate"
                        OnClick="btnRetrieve_Click" Visible="false" />

                    <asp:Button ID="btnGetReport" runat="server" Text="Get Report"
                        OnClick="btnGetReport_Click" Visible="false" />
                    &nbsp;&nbsp;&nbsp; 
                    <asp:Button ID="BtnExport" runat="server" Text="Export Report"
                        OnClick="btnSave_Click" />

                </td>
                <td align="center">&nbsp;</td>
                <td align="center">
                    <asp:HiddenField ID="btnHidSearchFlag" runat="server" />
                </td>
                <td align="center">&nbsp;</td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </center>

    <asp:LinkButton ID="lnkBtnBack" runat="server" OnClick="lnkBtnBack_Click">Back to Previous screen</asp:LinkButton>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
    <asp:Button ID="btnBulkUnsubscribe" runat="server" Text="Bulk Unsubscribe" OnClick="btnBulkUnsubscribe_Click" />

    <br />
    <center>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblerr" runat="server" Text=""></asp:Label>
        <center>
            <asp:Label ID="lblNoRecord" runat="server" ForeColor="Green" Font-Bold="true"></asp:Label>
        </center>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <asp:GridView ID="GVFailComplaints" AutoGenerateColumns="False" runat="server"
            AllowPaging="True" PageSize="100" PagerSettings-PageButtonCount="20"
            OnPageIndexChanging="GVFailComplaints_PageIndexChanging">
            <Columns>
                <asp:BoundField DataField="FailCompID" HeaderText="FailCompID" />
                <asp:BoundField DataField="MailFrom" HeaderText="MailFrom" />
                <asp:BoundField DataField="MailTo" HeaderText="MailTo" />

                <asp:TemplateField HeaderText="Details">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkViewDetails" Text='View' NavigateUrl='<%# GetUrl(Eval("MailTo"))%>' runat="server">
                            
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:g}" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="ServerID" HeaderText="ServerID" />
                <asp:BoundField DataField="ComplaintType" HeaderText="ComplaintType" />
                <asp:BoundField DataField="UserAgent" HeaderText="UserAgent" />
                <asp:BoundField DataField="NewsLetter" HeaderText="NewsLetter" />
                <asp:BoundField DataField="FailureCode" HeaderText="FailureCode" />
                <asp:BoundField DataField="FailureType" HeaderText="FailureType" />
                <asp:BoundField DataField="Reason" HeaderText="Reason" />

            </Columns>
            <EmptyDataTemplate>
                FailCompID
            </EmptyDataTemplate>
        </asp:GridView>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:GridView ID="GrdAbuseActive" AutoGenerateColumns="False" runat="server"
            AllowPaging="True" PageSize="100" PagerSettings-PageButtonCount="20"
            OnPageIndexChanging="GrdAbuseActive_PageIndexChanging">
            <Columns>
                <asp:BoundField DataField="FailCompID" HeaderText="FailCompID" />
                <asp:BoundField DataField="MailFrom" HeaderText="MailFrom" />
                <asp:BoundField DataField="MailTo" HeaderText="MailTo" />

                <asp:TemplateField HeaderText="Details">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkViewDetails" Text='View' NavigateUrl='<%# GetUrl(Eval("MailTo"))%>' runat="server">
                            
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:g}" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="ServerID" HeaderText="ServerID" />
                <asp:BoundField DataField="ComplaintType" HeaderText="ComplaintType" />
                <asp:BoundField DataField="UserAgent" HeaderText="UserAgent" />
                <asp:BoundField DataField="NewsLetter" HeaderText="NewsLetter" />
                <asp:BoundField DataField="FailureCode" HeaderText="FailureCode" />
                <asp:BoundField DataField="FailureType" HeaderText="FailureType" />
                <asp:BoundField DataField="Reason" HeaderText="Reason" />

            </Columns>
            <EmptyDataTemplate>
                FailCompID
            </EmptyDataTemplate>
        </asp:GridView>
    </center>
</asp:Content>

