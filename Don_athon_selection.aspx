﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Don_athon_selection.aspx.vb" Inherits="Don_athon_selection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	text-align:center;
	width:100%;
	background-color:#FFF;
}
.style2 {
	font-size: 12px;
	font-family: Calibri;
	color: #FFFFFF;
}
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center" style="width :100%" >
    <table  border="1"  cellpadding = "0" cellspacing = "0" bordercolor="#189A2C" style="width:1000px; text-align:center">
    <tr>
    <td>
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:1000px; text-align:center">
    <tr><td> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="right"  >
                      <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                        <map name="Map" id="Map">
                          <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                        </map></td>
                      <td width="76%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="51%"  background="images_new/topbg_1.jpg"><img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                              <td width="49%" valign="top" background="images_new/img_03.jpg">
                              <table width="89%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="30%">&nbsp;</td>         
                                  <td width="70%" valign="top" >
                                  <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td valign="middle" style="padding-left:10px; padding-top:5px">
                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                        <Items>                    
                                       
                                        </Items>     
                                        </asp:Menu>
                                        </td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr><td colspan ="2" align="center" ><br />
                                    <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server" ></asp:Label>
                                    </td></tr>
                              </table>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="35" valign="middle" background="images_new/menubg.jpg">
<table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    <tr><td style="text-align:center; width :10px" >  </td><td style="vertical-align:middle; text-align:center;">
        <asp:Label ID="lblPageCaption" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" runat="server" ></asp:Label>
        <asp:Literal ID="ltlEvents" runat="server"></asp:Literal> </td></tr> 
    </table>
</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
    </td>
    </tr>
  
      
        <tr><td align="left" style="height :1px">
        
           &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" Visible="false" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink><%--</td></tr>--%>
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink><%--</td></tr>--%>
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink> 
            </td></tr>
            
        <tr><td runat="server" id="trSelection" align="center">        
            Select Event : <asp:DropDownList ID="ddlWalkaThon" AutoPostBack="true" OnSelectedIndexChanged ="ddlWalkaThon_SelectedIndexChanged" runat="server">
            </asp:DropDownList>
    
    
        </td> </tr>
          <tr runat="server" id="trExcelethon" visible="true" >
          <td align="center" style="padding-left:5px; padding-right:5px;">
           

              <asp:Literal ID="Literal1" runat="server"></asp:Literal>
           

</td> </tr>
         
                    <tr><td height="5px" align="center"></td> </tr> 
                     <tr><td  align="center" valign="top" style="height:350px">
                         <asp:Button ID="BtnRegister" runat="server" Text="Register" />
                     </td> </tr> 
            <tr><td align="right"  bgcolor="#99CC33" style="height:25px; vertical-align:middle ;" class="style2">
                © North South Foundation. All worldwide rights reserved.
<a class="btn_01" href="/public/main/privacy.aspx">Copyright</a>&nbsp;&nbsp;&nbsp;
</td> 
            </tr>
    </table>
   </td></tr> </table> 
        </div>
    </form>
</body>
</html>
