<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.VolunteerSignUp" CodeFile="VolunteerSignUp.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EventSelection</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
		<script language="javascript">
		//-------------------------------------------------------------
		// Select all the checkboxes (Hotmail style)
		//-------------------------------------------------------------
		function SelectAllCheckboxes(spanChk){
	    
		// Added as ASPX uses SPAN for checkbox 
		var oItem = spanChk.children;
		var theBox=(spanChk.type=="checkbox")?spanChk:spanChk.children.item[0];
		xState=theBox.checked;    

			elm=theBox.form.elements;
			for(i=0;i<elm.length;i++)
			if(elm[i].type=="checkbox" && elm[i].id!=theBox.id)
				{
				//elm[i].click();
				if(elm[i].checked!=xState)
				elm[i].click();
				//elm[i].checked=xState;
				}
		}

		//-------------------------------------------------------------
		//----Select highlish rows when the checkboxes are selected
		//
		// Note: The colors are hardcoded, however you can use 
		//       RegisterClientScript blocks methods to use Grid's
		//       ItemTemplates and SelectTemplates colors.
		//-------------------------------------------------------------
		function HighlightRow(chkB)    {
		var oItem = chkB.children;
		xState=oItem.item(0).checked;    
		if(xState)
			{chkB.parentElement.parentElement.style.backgroundColor='lightcoral';
			// grdEmployees.SelectedItemStyle.BackColor
			chkB.parentElement.parentElement.style.color='white'; 
			// grdEmployees.SelectedItemStyle.ForeColor
			}else 
			{chkB.parentElement.parentElement.style.backgroundColor='white'; 
				//grdEmployees.ItemStyle.BackColor
			chkB.parentElement.parentElement.style.color='black'; 
				//grdEmployees.ItemStyle.ForeColor
			}
		}		
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE width="100%" align="center">
				<TBODY align="center">
					<TR>
						<TD class="Heading" vAlign="top" align="center">Volunteer SignUp
						</TD>
					</TR>
					<TR>
						<TD><asp:datagrid id="dgSigningAuth" runat="server" Width="100%" DataKeyField="VolunteerTaskID" AutoGenerateColumns="False"
								CellPadding="4" CssClass="GridStyle">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="ItemSelect"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem"></ItemStyle>
								<HeaderStyle CssClass="GridHeader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Check All">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderTemplate>
											 <asp:CheckBox id="chkAll" runat="server" AutoPostBack="false"
												ToolTip="Select/Deselect All" />
										</HeaderTemplate>
										<ItemTemplate>
											<span id="spanChk">
												<asp:CheckBox id="chkEvent"  runat="server" AutoPostBack="false" ToolTip="Select/Deselect All"></asp:CheckBox></span> 
													
										</ItemTemplate>
										<FooterStyle VerticalAlign="Top"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Event Name">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=lblActivityName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TaskDescription") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Date">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<asp:Label id="lblEventDate" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Time">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<asp:Label id="lblEventTime" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Venue">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></TD>
					</TR>
					<tr>
						<td>
							<br>
						</td>
					</tr>
					<TR>
						<td><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="SignUp">
						     </asp:button>
						 </td>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>

 

 
 
 