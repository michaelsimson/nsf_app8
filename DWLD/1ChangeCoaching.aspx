<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="1ChangeCoaching.aspx.vb" Inherits="Admin_ChangeCoaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
			<table id="tblLogin" border="0" cellpadding = "3" cellspacing = "0" width="900px" runat="server">
				<tr>
					<td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h1>Change Coach</h1>
					</td>
				</tr>
				 <tr>
			        <td colspan="2">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td>&nbsp;</td>
			    </tr>
				<tr id="trvol" runat = "server" >
					<td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
					<td><asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox><asp:button id="btnLoadChild" runat="server" CssClass="FormButtonCenter" Text="Load Child(ren)"></asp:button><br>
						&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
							Display="Dynamic"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr>
				<tr id = "trchild" runat="server" visible="false"><td align = "right"> Child </td><td align = "left"><asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField ="ChildNumber" Width="150px" runat="server"></asp:DropDownList></td></tr>
				<tr><td align="right">Event Year</td>
       <td align="left">
           <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="24px" Width="150px">
           </asp:DropDownList>
       </td><td></td></tr>
				<tr>
					<td colSpan="2" align="center">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label></td></tr> 
                        <tr><td> <asp:Label ID="lblPrd" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label> 
                        <asp:Label ID="lblPrdGrp" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label></td></tr>
					<tr>
					<td colSpan="2">
														
						<ASP:DATAGRID id="dgselected" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="CoachRegID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="childnumber" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="SignUpID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Grade" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn HeaderText="Change Coach" ItemStyle-Width="12%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						<ItemTemplate>
						<asp:LinkButton id="lbtnChange" runat="server" CausesValidation="false" CommandName="Select" Text="Change Coach"></asp:LinkButton>
						</ItemTemplate></ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						      <asp:Label ID="lblChildName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                            </ItemTemplate>
						</ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="12%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblCoachName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                              </ItemTemplate>
                         </ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblLevel" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Capacity"  ItemStyle-HorizontalAlign="Center"  >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblCapacity" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
					<ASP:BOUNDCOLUMN ItemStyle-HorizontalAlign = "Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor = "White" HeaderStyle-Font-Bold = "true" DataField="ApprovedCount" Visible="true"></ASP:BOUNDCOLUMN>

						<ASP:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:BOUNDCOLUMN DataField="SessionNo" HeaderText="Session#" HeaderStyle-ForeColor="white"  ></ASP:BOUNDCOLUMN>
                         <ASP:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
						</COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>	     
									
									</td>
							</tr>
							<tr  bgcolor="#FFFFFF">
				    <td colspan="2">
				        <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
				    </td>
				</tr>
							<tr  bgcolor="#FFFFFF">
				    <td colspan="2">
				        
				        <ASP:DATAGRID id="dgCoachSelection" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="SignUpID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn HeaderText="Replace with" ItemStyle-Width="12%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						<ItemTemplate>
						<asp:LinkButton id="lbtnChange" runat="server" CausesValidation="false" CommandName="Select" Text="Replace with"></asp:LinkButton>
						</ItemTemplate>

<ItemStyle Width="12%"></ItemStyle>
                        </ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" HeaderStyle-Width="20%" ItemStyle-Width="20%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>

<ItemStyle Width="20%"></ItemStyle>
						</ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Coach Name"  HeaderStyle-Width="15%" ItemStyle-Width="15%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblCoachName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                              </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                         </ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblLevel" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
						    </ItemTemplate>

<ItemStyle Width="10%"></ItemStyle>
						</ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Capacity" ItemStyle-HorizontalAlign="Center"  >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblCapacity" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
						    </ItemTemplate>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</ASP:TemplateColumn>
						<ASP:BOUNDCOLUMN ItemStyle-HorizontalAlign = "Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor = "White" HeaderStyle-Font-Bold = "true" DataField="ApprovedCount" Visible="true">
<HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </ASP:BOUNDCOLUMN>
                        <ASP:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDayTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblTime1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                         </ASP:TemplateColumn>
                         <ASP:BOUNDCOLUMN DataField="SessionNo" HeaderText="Session#" HeaderStyle-ForeColor="white"  >
<HeaderStyle ForeColor="White"></HeaderStyle>
                        </ASP:BOUNDCOLUMN>
                         <ASP:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" 
                            ItemStyle-Width="15%" Visible="False" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'  Visible="false"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Donation" HeaderStyle-Width="15%" 
                            ItemStyle-Width="15%" Visible="False" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDonation" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem, "Regfee","{0:c}") %>'  
                                        Visible="False"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>

<ItemStyle Width="15%"></ItemStyle>
                        </ASP:TemplateColumn>
						<ASP:TemplateColumn><ItemTemplate>
<%--						<asp:LinkButton id="lblRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
--%>						</ItemTemplate>
						</ASP:TemplateColumn>
					    </COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>
				      <br /> 
                        <asp:Label ID="lblCoachRegID" runat="server" ForeColor="White" ></asp:Label> 
                         <asp:Label ID="lblSignUPID" runat="server" ForeColor = "White" ></asp:Label>
                         </td>
				</tr>
						</table>
					
		</div>
</asp:Content>

