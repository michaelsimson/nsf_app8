<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="ManageCoachPapers.aspx.cs" Inherits="ManageTestPapers" Title="Manage Coach Papers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script type = "text/javascript">
    function Confirm() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Are you sure there is no Question Paper for the students this period?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
       
        document.forms[0].appendChild(confirm_value);
    }
    </script>
     <script language="javascript" type="text/javascript">
         function PopupPicker(ctl) {
             var PopupWindow = null;
             settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
             PopupWindow = window.open('CoachPapHelp.aspx?ID=' + ctl, 'Coach Papers Help', settings);
             PopupWindow.focus();
         }
	</script>
    <style>
.dwnlodcenter
{
	text-align: center;
}
    .style1
    {
            width: 386px;
        }
    .style4
    {
        height: 21px;
        width: 442px;
    }
    .style5
    {
        text-align: left;
    }
    .style11
    {
            width: 69px;
        }
    .style12
    {
        text-align: left;
        width: 442px;
    }
    .style13
    {
        width: 220px;
    }
    .style15
    {
            width: 191px;
        }
    .style17
    {
        height: 21px;
        width: 139px;
    }
    .style18
    {
        height: 42px;
        width: 139px;
    }
        .style19
        {
            width: 10px;
        }
        .style20
        {
            width: 25px;
        }
        .style21
        {
            width: 70px;
        }
        .style22
        {
            width: 37px;
        }
        .style23
        {
            width: 30px;
        }
        .style24
        {
            width: 57px;
        }
        .style25
        {
            width: 59px;
        }
        .style26
        {
            width: 139px;
        }
        .style30
        {
            height: 2px;
            width: 717px;
        }
        .style31
        {
            height: 2px;
            width: 104px;
        }
        .style32
        {
            width: 41px;
        }
        .style33
        {
            width: 526px;
        }
        .style35
        {
            width: 717px;
        }
        .stynew
        {
        	width:114px;
        }
        .marginL0
        {
        	margin-left:0px;
        }
        .style36
        {
            width: 104px;
        }
        .style37
        {
            width: 442px;
        }
        </style>
    <div align="left" >
     	<asp:HyperLink ID="backToVolunteerFunctions"  CssClass="btn_02"  NavigateUrl="VolunteerFunctions.aspx" runat="server" >  Back to Volunteer Functions</asp:HyperLink>

   </div>
   
   <div align="center">
      
    <asp:DropDownList ID="dllfileChoice"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllfileChoice_SelectedIndexChanged"
         Visible="False">
    </asp:DropDownList>
       
   </div>
   
    <asp:Label ID="lblNoPermission" runat="server" ForeColor="Red" Visible="false"></asp:Label>
    <br />
   
    <asp:HiddenField ID="hdnCPID" runat="server" />
   
   <asp:Panel ID="Panel1" runat="server" Visible="False" style="text-align: center" 
        BackColor="White">
        <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Black" style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: large;" Text="Upload Papers"></asp:Label>
         <div align="center">       
                    <table style="margin: 0 auto 0 125px; width: 87%;">
                        <tr>
                            <td class="style15" style="text-align: left;">
                                &nbsp;</td>
                            <td style="text-align: left;" class="style1">
                                &nbsp;</td>
                            <td style="text-align: left;" class="style13">
                                &nbsp;</td>
                            <td style="width: 41px; text-align: left;">
                                &nbsp;</td>
                            <td style="width: 402px; ">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style15" style="text-align: left;">
                                <asp:Label ID="Label12" runat="server" Text="Year"></asp:Label>
                            </td>
                            <td style="text-align: left;" class="style1">
                                <asp:DropDownList ID="ddlContestYear" runat="server" Height="22px" Width="75px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                    ControlToValidate="ddlContestYear" ErrorMessage="  * Required ContestYear#" 
                                    InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td style="text-align: left;" class="style13">
                                Paper Type</td>
                            <td style="width: 41px; text-align: left;">
                                <asp:DropDownList ID="ddlNoOfContestants" runat="server" Height="20px" 
                                    style="margin-left: 0px" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 402px; text-align: left;">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                    ControlToValidate="ddlNoOfContestants" ErrorMessage="*Required Paper Type" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style15" style="text-align: left;">
                                Event</td>
                            <td style="text-align: left;" class="style1">
                                <asp:DropDownList ID="ddlEvent" runat="server" Height="22px" Width="145px" >
                                  
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                    ControlToValidate="ddlEvent" ErrorMessage="* Required Event" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td style="text-align: left;" class="style13">
                                Doc. Type</td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="ddlDocType" runat="server" Height="20px" Width="140px"  AutoPostBack="true" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                    <asp:ListItem Text="Questions" Value="Q"></asp:ListItem>
                                    <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                                    <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: left;">
                            <a href="javascript:PopupPicker('DocType');">Help</a>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                    ControlToValidate="ddlDocType" ErrorMessage="  * Required Doc. Type" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style15" style="text-align: left;">
                                Procuct Group</td>
                            <td style="text-align: left;" class="style1">
                                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="true" 
                                    OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" 
                                    Width="145px" Height="22px" ontextchanged="ddlProductGroup_TextChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="ddlProductGroup" ErrorMessage="  * Required Product Group" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td style="text-align: left;" class="style13">
                                Week#</td>
                            <td style="text-align: left;" class="style32">
                                <asp:DropDownList ID="ddlWeek" runat="server" Height="22px" Width="140px" AutoPostBack="true" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: left;" class="style33">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="ddlWeek" ErrorMessage="  * Required Week Of" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td> 
                        </tr>
                        <tr>
                            <td class="style15" style="text-align: left;">
                                Product Code</td>
                            <td style="text-align: left;" class="style1">
                                <asp:DropDownList ID="ddlProduct" runat="server" 
                                    onselectedindexchanged="ddlProduct_SelectedIndexChanged" Width="145px" 
                                    Height="22px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="ddlProduct" ErrorMessage="  * Required Product" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td style="text-align: left;" class="style13">
                                Set#</td>
                            <td style="width: 41px; text-align: left;">
                                <asp:DropDownList ID="ddlSet" runat="server" Height="22px" Width="140px" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 526px; text-align: left;">
                                <a href="javascript:PopupPicker('SetNo');">Help</a>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="ddlSet" ErrorMessage="  * Required Set#" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style15" style="text-align: left;">
                                Level</td>
                            <td style="text-align: left;" class="style1">
                                <asp:DropDownList ID="ddlLevel" runat="server"   
                                    Height="22px" Width="145px" >
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                    ControlToValidate="ddlLevel" ErrorMessage="*Required Level" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                 <td style="text-align: left;" class="style13">
                                Sections</td>
                            <td style="width: 41px; text-align: left;">
                                <asp:DropDownList ID="ddlSections" runat="server" Height="20px" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 526px; text-align: left;">
                                <a href="javascript:PopupPicker('Sections');">Help</a>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                    ControlToValidate="ddlSections" ErrorMessage="* Required Sections" 
                                    InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                           
                        </tr>
                    </table>  
                    </div>           
           </asp:Panel>
    
   <asp:Panel ID="PanelAdd" runat ="server" Visible="False" >
    <div align="center" > 
        <table style="margin: 0 auto 0 125px; width: 87%;">
        
            <tr>
                <td class="style17" style="text-align: left;">
                    File Name</td>
                <td style="text-align: left;" colspan="2" class="style4">
                    <asp:FileUpload ID="FileUpLoad1" runat="server" Height="22px" Width="480px" 
                                    style="text-align: left" />
                </td>
                <td colspan="2" style="height: 21px; text-align: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="FileUpLoad1" 
                        ErrorMessage="  *Required File ">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style26">
                </td>
                <td colspan="2" class="style12">
                    Ex:2013_Coaching_HW_MB_MB2_INT_Set2_Wk2_Sec1_Q.zip</td>
                <td class="style5" colspan="2">
                    <asp:HyperLink ID="Help" runat="server" NavigateUrl="~/NCHelp.aspx" 
                                    onclick="window.open (this.href, 'popupwindow','width=700,height=500,scrollbars,resizable');return false;" 
                                    Width="29px">Help</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="style18" style="text-align: left;">
                    <asp:Label ID="Label6" runat="server" style="text-align: right" 
                                    Text="Description"></asp:Label>
                </td>
                <td colspan="4" style="width: 526px; height: 42px; text-align: left;">
                    <asp:TextBox ID="txtDescription" runat="server" Height="45px" Text="" 
                                    TextMode="MultiLine" Width="513px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style26" style="text-align: left;">
                    <asp:Label ID="Label9" runat="server" style="text-align: right" Text="Password"></asp:Label>
                </td>
                <td colspan="4" style="width: 526px; text-align: left;">
                    <asp:TextBox ID="TxtPassword" runat="server" Text=""></asp:TextBox>
                    <asp:Label ID="Label10" runat="server" EnableViewState="false" 
                                    ForeColor="Green"> (Optional)</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style26" style="text-align: left;">
                </td>
                <td style="width: 277px; text-align: left;">
                    <asp:CheckBox ID="ckFname" runat="server" Font-Bold="True" 
                                    style="text-align: left" 
                        Text="Upload File without Naming Convention" />
                    &nbsp;
                </td>
                <td colspan="2">
                    <asp:Button ID="UploadBtn" runat="server" OnClick="UploadBtn_Click" 
                                    Text="Upload File" Width="105px" style="height: 26px" />
                </td>
                <td style="width: 526px; text-align: left;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style26" style="text-align: left;">
                    &nbsp;</td>
                <td style="text-align: left;" colspan="2" class="style37">
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false" 
                                    ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2" style="text-align: right;">
                
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                        onclick="LinkButton1_Click">Show Uploaded files</asp:LinkButton>
                        <br />
                    <asp:Label ID="lblstatus" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </div> 
    </asp:Panel>
    
      
    <asp:Panel ID="PanelModify" runat="server" Visible="False">
    <div >
         <table style="margin: 0 auto 0 125px; width: 87%;">
            <tr>
                <td class="style36" style="text-align: left;">
                    <asp:Label ID="Label15" runat="server" style="text-align: right" 
                                    Text="Description"></asp:Label>
                </td>
                <td style="text-align: left;" class="style35">
                    <asp:TextBox ID="txtDescriptionM" runat="server" Height="45px" Text="" 
                                    TextMode="MultiLine" Width="398px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style31" style="text-align: left;">
                    <asp:Label ID="Label16" runat="server" style="text-align: right" 
                        Text="Password"></asp:Label>
                </td>
                <td style="text-align: left; margin-left:0px;" class="style30">
                    <asp:TextBox ID="TxtPasswordM" runat="server" Text="" CssClass="marginL0" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>            
            <tr>
                <td class="style31" style="text-align: left;">
                    &nbsp;</td>
                <td class="style30" style="text-align: left; margin-left:0px;">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" 
                        onclick="btnUpdate_Click" Enabled="False" />
                    <asp:Button ID="btncancel" runat="server" onclick="btncancel_Click" 
                        Text="Cancel" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td class="style31" style="text-align: left;">
                    &nbsp;</td>
                <td class="style30" style="text-align: left; margin-left:0px;">
                    <asp:Label ID="lblMerror" runat="server" ForeColor="#FF3300"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </asp:Panel>
    
   <asp:Panel ID="PanelSections"  runat="server" BorderWidth="1px" BorderColor="black"  Width="100%" Visible="False">
       <div align="center" >
        <asp:Label ID="lblReplace" ForeColor="Red"  Text="Are you sure there is no Question Paper for the students this period?" runat="server"></asp:Label><br />
        <asp:Button ID="bttnYes" runat="server" Text="Yes" onclick="bttnYes_Click" /> &nbsp;&nbsp;<asp:Button 
               ID="bttnNo" runat="server" Text="No" onclick="bttnNo_Click" />
        </div>
    </asp:Panel>
    
    <asp:Panel ID="Panel5" runat ="server" BorderWidth="1px" BorderColor="black"  Width="100%" Visible="False">
     <div align="center" >
        <asp:Label ID="Label1" ForeColor="Red"  Text="This is a duplicate.  Do you want to replace the current file?" runat="server"></asp:Label><br />
        <asp:Button ID="Button1" runat="server" Text="Yes" onclick="btnYes_Click" /> &nbsp;&nbsp;<asp:Button 
               ID="Button2" runat="server" Text="No" onclick="btnNo_Click" />
        </div>
    </asp:Panel>
    <br />
    
      
    <asp:Panel ID="Panel6" runat ="server"  Width="100%" Visible="False">
     <div align="center" >
       <br />
       
         <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Size="Small" 
             Text="Existing Record"></asp:Label>
       
         <asp:GridView ID="gvDuplicate" runat="server" AutoGenerateColumns="False" 
             style="text-align: center" Width="907px">
             <Columns>
                 <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId" 
                     SortExpression="CoachPaperId">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" />
                 </asp:BoundField>
                 <asp:BoundField DataField="QPaperId" HeaderText="QPaperId" 
                     SortExpression="QPaperId">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                 </asp:BoundField>
                 <asp:BoundField DataField="EventYear" HeaderText="EventYear" 
                     SortExpression="EventYear">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" />
                 </asp:BoundField>
                 <asp:BoundField DataField="ProductId" HeaderText="ProductId" 
                     SortExpression="ProductId">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="EventId" HeaderText="EventId" 
                     SortExpression="EventId">
                     <ControlStyle BackColor="#00CC00" />
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="PaperType" HeaderText="PaperType" 
                     SortExpression="PaperType">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="DocType" HeaderText="DocType" 
                     SortExpression="DocType">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" 
                     SortExpression="ProductGroupCode">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" 
                     SortExpression="ProductCode">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" />
                 </asp:BoundField>
                 <asp:BoundField DataField="Sections" HeaderText="SecNum" 
                     SortExpression="Sections">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                         HorizontalAlign="Center" />
                 </asp:BoundField>
                 <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" 
                     SortExpression="TestFileName">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                 </asp:BoundField>
                 <asp:BoundField DataField="Description" HeaderText="Description" 
                     SortExpression="Description">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                 </asp:BoundField>
                 <asp:BoundField DataField="Password" HeaderText="Password" 
                     SortExpression="Password">
                     <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                     <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                 </asp:BoundField>
                 <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" 
                     SortExpression="ProductGroupId" Visible="False" />
             </Columns>
             <SelectedRowStyle BackColor="#99FFCC" />
         </asp:GridView>
       <br />
         <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Size="Small" 
             Text="Modified Input"></asp:Label>
         <table class="ItemLabelCenter" frame="border" rules="all">
             <tr bgcolor="#FFFF80" >
                 <td>
                     Year</td>
                 <td>
                     PaperType</td>
                 <td>
                     DocType</td>
                 <td>
                     P.Group</td>
                 <td>
                     Product</td>
                 <td>
                     Level</td>
                 <td>
                     Sec#</td>
                 <td>
                     Week#</td>
                 <td>
                     Set#</td>
                 <td>
                     File Name</td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="lbluiyear" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lblPtype" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lbldoctype" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lblpgroup" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lblProduct" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lbllevel" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lblsec" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lblweek" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lbluiset" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td>
                     <asp:Label ID="lbluifname" runat="server" Text="Label"></asp:Label>
                 </td>
             </tr>
         </table>
        </div>
    </asp:Panel>
    <br />
    
   <asp:Panel ID="Panel2" runat="server" Visible="False">
        <asp:Label ID="Label7" runat="server" Text="Filter/Download Coach Papers" 
            Width="317px" Font-Bold="True"
            Font-Size="Large"></asp:Label>
        <br />
        <center >
        <table style="width: 100%" border="0" cellpadding="4" cellspacing="0" bordercolor="black">
            <tr>
                <td nowrap="nowrap" bgcolor="honeydew" class="style11">
                    Year</td>
                <td bgcolor="honeydew" nowrap="nowrap" class="style20">
                    Event</td>
                <td nowrap="nowrap" bgcolor="honeydew" class="style21">
                    Product Group Code</td>
                <td nowrap="nowrap" bgcolor="honeydew" class="style22">
                    Product Code</td>
                <td bgcolor="honeydew" nowrap="nowrap" class="style23">
                    Level</td>
                <td nowrap="nowrap" bgcolor="honeydew" class="style25">
                    Paper Type</td>
                <td nowrap="nowrap" bgcolor="honeydew" class="style19">
                    Week#</td>
                      <td nowrap="nowrap" bgcolor="honeydew" class="style24">
                          Set#</td>
                <td nowrap="nowrap">
                    &nbsp;</td>
                <td nowrap="nowrap">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style11">
                    <asp:DropDownList ID="ddlFlrYear" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="style20">
                    <asp:DropDownList ID="ddlFlrEvent" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="style21">
                    <asp:DropDownList ID="ddlFlrProductGroup" runat="server" AutoPostBack="true" 
                        OnSelectedIndexChanged="ddlFlrProductGroup_SelectedIndexChanged" 
                        Height="19px" Width="119px">
                    </asp:DropDownList>
                </td>
                <td class="style22">
                    <asp:DropDownList ID="ddlFlrProduct" runat="server" 
                        onselectedindexchanged="ddlFlrProduct_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="style23">
                    <asp:DropDownList ID="ddlFlrLevel" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
                <td class="style25">
                    <asp:DropDownList ID="ddlFlrNoOfContestants" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="style19">
                    <asp:DropDownList ID="ddlFlrWeek" runat="server" AutoPostBack="true"
                        onselectedindexchanged="ddlFlrWeek_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                    <td class="style24">
                        
                        <asp:DropDownList ID="ddlFlrSet" runat="server">
                        </asp:DropDownList>
                        
                </td>
                <td style="width: 115px">
                    
                </td>
                <td style="width: 115px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style11">
                    <asp:Button ID="btnSearch" runat="server" Height="26px" 
                        OnClick="btnSearch_Click" style="width: 61px" Text="Search" Width="90px" />
                </td>
                <td class="style20">
                    <asp:Button ID="btnReset" runat="server" Height="26px" OnClick="btnReset_Click" 
                        Text="Reset" Width="60px" />
                </td>
                <td colspan="5">
                    <asp:Label ID="lblSearchErr" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td class="style24">
                    &nbsp;</td>
                <td style="width: 115px">
                    &nbsp;</td>
                <td style="width: 115px">
                    &nbsp;</td>
            </tr>
        </table>
        </center>
        <br />

    </asp:Panel>
    
   <asp:Panel ID="Panel4" runat="server" Visible="False">
        <asp:Label ID="Label13" runat="server" Text="Download Coach Papers" 
            Width="249px" Font-Bold="True"
            Font-Size="Large" Height="20px"></asp:Label> &nbsp;&nbsp;&nbsp; 
        
            <br />
            <br />
        <asp:Label ID="Label14" runat="server" Text="Select Week range"> </asp:Label>
            &nbsp;
        <asp:DropDownList ID="ddlFlrWeekForExamReceiver" runat="server" OnSelectedIndexChanged="ddlFlrWeekForExamReceiver_SelectedIndexChanged">
        </asp:DropDownList>
            <asp:Button ID="ButtonForExamReceiver" runat="server" OnClick="ButtonForExamReceiver_Click" Text="Search" /><br />
        <br />
        <br />
    </asp:Panel>
    
   <asp:Panel ID="Panel3" runat="server" Visible="False">
            <center>
                <asp:Label ID="lblChapter" runat="server" CssClass="title02" Visible="false"></asp:Label>
                <br />
                <asp:Label ID="LblexamRecErr" runat="server" ForeColor="Red"></asp:Label>
            </center>
            <center>
                <asp:GridView ID="gvTestPapers" runat="server" 
                    AutoGenerateColumns="False" o="gvTestPapers_RowDataBound" 
                    OnRowCommand="gvTestPapers_RowCommand" 
                    OnSelectedIndexChanged="gvTestPapers_SelectedIndexChanged" 
                    OnSorting="gvTestPapers_Sorting" style="text-align: center" Width="907px" 
                    DataKeyNames="CoachPaperId,TestFileName">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            HeaderText="Download" ImageUrl="~/Images/download.png" ShowHeader="True">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" HorizontalAlign="Center" 
                                BorderColor="Black" />
                            <ItemStyle CssClass="dwnlodcenter" HorizontalAlign="Center" Wrap="True" 
                                BorderColor="Black" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId" 
                            SortExpression="CoachPaperId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="QPaperId" HeaderText="QPaperId" 
                            SortExpression="QPaperId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventYear" HeaderText="EventYear" 
                            SortExpression="EventYear">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductId" HeaderText="ProductId" 
                            SortExpression="ProductId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventId" HeaderText="EventId" 
                            SortExpression="EventId">
                            <ControlStyle BackColor="#00CC00" />
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PaperType" HeaderText="PaperType" 
                            SortExpression="PaperType">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DocType" HeaderText="DocType" 
                            SortExpression="DocType">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" 
                            SortExpression="ProductGroupCode">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" 
                            SortExpression="ProductCode">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Sections" HeaderText="SecNum" 
                            SortExpression="Sections">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" 
                            SortExpression="TestFileName">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="Description" 
                            SortExpression="Description">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Password" HeaderText="Password" 
                            SortExpression="Password">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:Label ID="lblPrdError" runat="server" ForeColor="Red"></asp:Label>
            </center>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
            <asp:HiddenField ID="hdnTechNational" runat="server" />
        </asp:Panel>
   
   <asp:Panel ID="ModifyGrid" runat="server">
       <asp:HiddenField ID="hdntlead" runat="server" />
   
   </asp:Panel>
    <asp:HiddenField ID="hdnChapterID" runat="server" />
   
    <asp:HiddenField ID="hdnTempFileName" runat="server" />
                <asp:HiddenField ID="ULFile" runat="server" />
    <asp:HiddenField ID="hdnsection" runat="server" />
                <asp:HiddenField ID="hdnuploaded" runat="server" />
                <br />
    <br />
                <asp:GridView ID="gvModify" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="ProductGroupId" 
                    onselectedindexchanged="gvModify_SelectedIndexChanged" 
                    style="text-align: center" Width="907px">
                    <Columns>
                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Modify">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId" 
                            SortExpression="CoachPaperId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="QPaperId" HeaderText="QPaperId" 
                            SortExpression="QPaperId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventYear" HeaderText="EventYear" 
                            SortExpression="EventYear">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductId" HeaderText="ProductId" 
                            SortExpression="ProductId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventId" HeaderText="EventId" 
                            SortExpression="EventId">
                            <ControlStyle BackColor="#00CC00" />
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PaperType" HeaderText="PaperType" 
                            SortExpression="PaperType">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DocType" HeaderText="DocType" 
                            SortExpression="DocType">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" 
                            SortExpression="ProductGroupCode">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" 
                            SortExpression="ProductCode">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Sections" HeaderText="SecNum" 
                            SortExpression="Sections">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" 
                                HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" 
                            SortExpression="TestFileName">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="Description" 
                            SortExpression="Description">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Password" HeaderText="Password" 
                            SortExpression="Password">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                            <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" 
                            SortExpression="ProductGroupId" Visible="False" />
                    </Columns>
                    <SelectedRowStyle BackColor="#99FFCC" />
                </asp:GridView>
    <asp:HiddenField ID="hdnupfile" runat="server" />
    <asp:Label ID="lblhiddenCpId" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblhiddenFname" runat="server" Visible="False"></asp:Label>
                </asp:Content>

 

 
 
 