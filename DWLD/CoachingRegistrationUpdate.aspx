﻿

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingRegistrationUpdate.aspx.vb" Inherits="CoachingRegistrationUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <div align="left" >
      &nbsp;&nbsp;&nbsp;
      <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
   </div>
   <div style ="text-align:center">
      <table border="0" cellpadding = "3" cellspacing = "0" style ="width :800px; text-align:center" >
         <tr>
            <td align = "center" style="color: #000000;font-family: Arial Rounded MT Bold; font-size: 14px;" >Update Child Registration Data</td>
         </tr>
         <tr>
            <td align="center">
               <table border="0" cellpadding = "3" cellspacing = "0" >
                  <tr id="trvol" runat = "server" >
                     <td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
                     <td>
                        <asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox>
                        <asp:button id="btnLoadChild" OnClick ="btnLoadChild_Click" runat="server" CssClass="FormButtonCenter" Text="Load Child(ren)"></asp:button>
                        <br>
                        &nbsp;
                        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
                           Display="Dynamic"></asp:requiredfieldvalidator>
                        <asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
                           ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" colspan="2">
                        <asp:Button ID="btnContinue" runat="server" CausesValidation="false" Text="Load All" OnClick="btnContinue_Click" />
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr id="trEdit" visible = "false" runat = "server" >
            <td align = "center"  >
               <table border="0" cellpadding = "3" cellspacing = "0" >
                  <tr>
                     <td align = "left" >Approved </td>
                     <td align = "left" >
                        <asp:DropDownList ID="ddlApproved" runat="server">
                           <asp:ListItem Value="Y">Yes</asp:ListItem>
                           <asp:ListItem Value="N">No</asp:ListItem>
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align = "left" >Product </td>
                     <td align = "left" >
                        <asp:DropDownList  Width="150px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" ID="ddlProduct" DataTextField="ProductName" DataValueField="ProductID" runat="server"></asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align = "left" >Coach </td>
                     <td align = "left" >
                        <asp:DropDownList ID="ddlCoachName" Enabled="false" DataTextField="CoachName" DataValueField="SignUpID"   runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align = "center" colspan="2" >
                        <asp:Button ID="btnUpdate" runat="server" CausesValidation="false" Text="Update" OnClick ="btnUpdate_Click" />
                        &nbsp;&nbsp;&nbsp;   
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel" OnClick ="btnCancel_Click" />
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align = "center" >
               <asp:Label ID="lblErr" runat="server" ForeColor = "Red" ></asp:Label>
            </td>
         </tr>
         <tr>
            <td>
               <asp:Label ID="lblPrdGrp" runat="server" Visible ="false"></asp:Label>
               <asp:Label ID="lblPrd" runat="server" Visible ="false"></asp:Label>
               <asp:Label ID="lblApproved" runat="server" Visible="false"></asp:Label>
            </td>
         </tr>
         <tr ID="trExcCapacity" runat="server" visible="false">
            <td>
               <table align="center">
                  <tr>
                     <td align="center">
                        <asp:Label ID="lblExcCap"  ForeColor="Red" Font-Bold="true" runat="server" Text="Capacity will be exceeded, do you still want to approve this case?"></asp:Label>
                     </td>
                  </tr>
                  <tr>
                     <td><asp:Button ID="BtnYes" runat="server" Text="Yes" style="width: 37px" /> &nbsp;
                        <asp:Button ID="BtnNo" runat="server" Text="No" /></td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align = "center" >
               <ASP:DATAGRID id="dgselected" runat="server" OnItemCommand="dgselected_ItemCommand"  OnPageIndexChanged ="dgselected_PageIndexChanged" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                  Height="14px" GridLines="Both" DataKeyField="CoachRegID" CellPadding="4" BackColor="#755200" BorderWidth="3px" BorderStyle="Double"
                  BorderColor="#336666" PageSize="10"  AllowPaging="true" ForeColor="White" Font-Bold="True">
                  <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                  <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
                  <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                  <COLUMNS>
                     <ASP:BOUNDCOLUMN DataField="CoachRegID" Visible="false"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="childnumber" Visible="false"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="ProductCode" Visible="false"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="SignUpID" Visible="false"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="ProductID" Visible="false"></ASP:BOUNDCOLUMN>
                     <ASP:TemplateColumn HeaderText="Edit">
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:LinkButton id="lbtnEdit" runat="server" CausesValidation="false" CommandName="Select" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Delete" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:LinkButton id="lbtnDelete" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblChildName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TEMPLATECOLUMN HeaderText="Product" ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>
                     </ASP:TEMPLATECOLUMN>
                     <ASP:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="12%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblCoachName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblLevel" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblCapacity" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:BOUNDCOLUMN DataField="ApprovedCount" ItemStyle-HorizontalAlign="Center" HeaderText ="Approved Count" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="Status" HeaderText ="Status" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                     <ASP:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" Visible="true"  ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" Visible="true"  ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblCoachTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:BOUNDCOLUMN DataField="SessionNo" HeaderText ="Session#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                     <ASP:TemplateColumn HeaderText="Start Date" Visible = "false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="End Date" Visible="false"  HeaderStyle-Width="15%" ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:BOUNDCOLUMN DataField="FatherName" HeaderText ="Father Name" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                     <ASP:BOUNDCOLUMN DataField="EMail" HeaderText ="Email" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                     <ASP:TemplateColumn HeaderText="City" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                     <ASP:TemplateColumn HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
                        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                        <ItemTemplate>
                           <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>'  Visible="True"></asp:Label>
                        </ItemTemplate>
                     </ASP:TemplateColumn>
                  </COLUMNS>
                  <HeaderStyle HorizontalAlign="Left" />
                  <ItemStyle HorizontalAlign ="Left" />
                  <PAGERSTYLE ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PAGERSTYLE>
                  <AlternatingItemStyle BackColor="LightBlue" />
               </ASP:DATAGRID>
            </td>
         </tr>
         <tr>
            <td align = "center" >
               <asp:Label ID="lblCoachRegID" runat="server" ForeColor = "White" ></asp:Label>
               <asp:Label ID="lblSignUpID" runat="server" ForeColor = "White" ></asp:Label>
               <asp:Label ID="lblChildNumber" runat="server" ForeColor = "White" ></asp:Label>
            </td>
         </tr>
      </table>
   </div>
</asp:Content>

