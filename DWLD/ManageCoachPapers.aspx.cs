using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

#region " Class ManageTestPapers "

public partial class ManageTestPapers : System.Web.UI.Page
{
    #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion

    #region " Event Handlers "
    string upFile;
    int MemberID = -1;
    int RoleCode = -1;
    int AccLevel = -1;
    bool SearchFlag = false;
    int SectionFlag = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

           Session["RoleId"] =88;
        Session["EntryToken"] = "VOLUNTEER";
        Session["LoginID"] = 36876;

        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}

        //if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
        //    Response.Redirect("login.aspx?entry=v");

        if (!IsPostBack)
        {
            ddlDocType.Items.Insert(0, new ListItem("[Select Doc. Type]", "-1"));
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "30" || Session["RoleId"].ToString() == "89")
                {
                    hdntlead.Value = "Y";
                    GetDropDownChoice(dllfileChoice, true);
                    dllfileChoice.Visible = true;
                    lblNoPermission.Visible = false;
                }

                else if (Session["RoleId"].ToString() == "88")
                {
                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                    string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead='Y' and MemberID=" + Session["LoginID"].ToString();
                    int mcount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql));

                    if (mcount > 0)
                    {
                        GetDropDownChoice(dllfileChoice, true);
                        dllfileChoice.Visible = true;
                        lblNoPermission.Visible = false;
                        hdntlead.Value = "Y";
                    }
                    else
                    {
                        GetDropDownChoice(dllfileChoice, true);
                        dllfileChoice.Visible = true;
                        dllfileChoice.Items.RemoveAt(1);
                        dllfileChoice.Items.RemoveAt(2);
                        lblNoPermission.Visible = false;
                        hdntlead.Value = "N";
                    }
                }
                else
                {
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
                }
            }
        }
    }
    //private void GetTestPapers_NatTechT()
    //{
    //        LblexamRecErr.Text = "";
    //        //String StrSQL = " SELECT DISTINCT TP.TestPaperId, TP.ContestYear, TP.ProductId, TP.ProductCode, TP.ProductGroupId, TP.ProductGroupCode, TP.EventID, TP.WeekId, TP.SetNum, TP.NoOfContestants," ;
    //        //StrSQL = StrSQL  + " TP.TestFileName,TP.DocType, TP.Description, TP.Password, TP.CreateDate, TP.CreatedBy, TP.ModifyDate, TP.ModifiedBy FROM ";
    //        //StrSQL = StrSQL + " TestPapers TP INNER JOIn NatTechTeam N on N.ProductGroupCode = TP.ProductGroupCode Where TP.ContestYear = Year(GETDATE())  and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]);
    //        //StrSQL = StrSQL + " order by TP.WeekId,TP.ProductID, TP.DocType, TP.NoOfContestants";  

    //        String StrSQL = " SELECT DISTINCT TP.CoachPaperId, TP.EventYear, TP.ProductId, TP.ProductCode, TP.ProductGroupId, TP.ProductGroupCode, TP.EventID, TP.WeekId, TP.SetNum, TP.PaperType,";
    //        StrSQL = StrSQL + " TP.TestFileName,TP.DocType, TP.Description, TP.Password, TP.CreateDate, TP.CreatedBy, TP.ModifyDate, TP.ModifiedBy FROM ";
    //        StrSQL = StrSQL + " CoachPapers TP INNER JOIn NatTechTeam N on N.ProductGroupCode = TP.ProductGroupCode Where TP.EventYear = Year(GETDATE())  and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]);
    //        StrSQL = StrSQL + " order by TP.WeekId,TP.ProductID, TP.DocType, TP.PaperType";  

    //        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            Panel3.Visible = true;
    //            gvTestPapers.DataSource = ds;
    //            gvTestPapers.DataBind();
    //        }
    //        else 
    //        {
    //            LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
    //        }
    //}   

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        GetProductCodes(PGId(ddlProductGroup), ddlProduct, true,ddlContestYear);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup),ddlContestYear);
        }
        else
        {
            PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true);
        }
        if (PGCode(ddlProductGroup) == "UV")
        {
            ddlSections.SelectedItem.Text = "0";
            ddlSections.Enabled = false;
        }
        else
        {
            PopulateSection(ddlSections, true);
            ddlSections.Enabled = true;
        }
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblstatus.Text = "";

        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadPapers.ToString())
        {
            LoadSearchAndDownloadPanels(true);
        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "30"))
            {
                Label8.Text = "Upload Papers";

                LoadUploadPanel();
            }
        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "30"))
            {
                Label8.Text = "Modify Papers";
                LoadModifyPanel();
            }

        }
    }

    protected void UpdateData()
    {
        int value = 0;
        if (ddlLevel.Enabled != true)
        {
            RequiredFieldValidator11.Enabled = false;
        }
        else
        {
            RequiredFieldValidator11.Enabled = true;
            if (ddlLevel.SelectedIndex == 0)
            {
                RequiredFieldValidator11.IsValid = false;
            }
        }

        if (!ddlSections.Enabled)
        {
            RequiredFieldValidator10.Enabled = false;
        }
        else
        {
            if (ddlSections.SelectedIndex == 0)
            {
                RequiredFieldValidator10.Enabled = true;
                RequiredFieldValidator10.Visible = true;
                RequiredFieldValidator10.IsValid = false;
            }

        }
        if (this.IsValid)
        {

            if (FileUpLoad1.HasFile | ULFile.Value != "")
            {

                EntityTestPaper objETP = new EntityTestPaper();
                objETP.ProductId = PGId(ddlProduct);
                objETP.ProductCode = PGCode(ddlProduct);
                objETP.ProductGroupCode = PGCode(ddlProductGroup);
                objETP.ProductGroupId = PGId(ddlProductGroup);
                objETP.EventCode = ddlEvent.SelectedItem.Text;
                objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
                objETP.SetNum = int.Parse(ddlSet.SelectedValue);
                objETP.Sections = int.Parse(ddlSections.SelectedItem.Text);
                objETP.PaperType = ddlNoOfContestants.SelectedItem.Value.ToString(); // Modified 22/07/2013
                //objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
                objETP.ContestYear = ddlContestYear.SelectedItem.Text;
                objETP.DocType = ddlDocType.SelectedItem.Value.ToString();
                objETP.Description = txtDescription.Text;
                objETP.Password = TxtPassword.Text;
                objETP.CreateDate = System.DateTime.Now;
                objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
                objETP.Event = ddlEvent.SelectedItem.Text;

                if (ddlLevel.SelectedItem.Value.ToString() != "-1")
                {
                    objETP.Level = ddlLevel.SelectedItem.Text;
                }
                else
                {
                    objETP.Level = "NA";
                }

                if (ckFname.Checked == true)
                {
                    objETP.TestFileName = ModifiedFileName(objETP, false);
                }
                else
                {
                    if (SectionFlag == 1 | SectionFlag == 2)
                    {
                        objETP.TestFileName = ULFile.Value;
                    }
                    else
                    {
                        objETP.TestFileName = FileUpLoad1.FileName;
                    }
                }

                if (ValidateFileName(objETP))
                {
                    try
                    {
                        value = TestPapers_Insert(objETP);
                        if (value != 0)
                        {
                            if (ckFname.Checked == true)
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                    File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value));
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                }
                                else
                                {
                                    SaveFileNewName(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], upFile);
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                }
                            }
                            else
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                    File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded: " + ULFile.Value.Replace("cpy_", "").ToString();

                                }
                                else
                                {
                                    SaveFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], FileUpLoad1);
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                                }
                            }
                        }
                        else
                        {
                            string cpID = Convert.ToString(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT CoachPaperId From CoachPapers where ProductGroupId=" + objETP.ProductGroupId + " and ProductId=" + objETP.ProductId + " and EventId=13 and EventYear=" + ddlContestYear.SelectedItem.Text + "and EventCode='Coaching' and PaperType='" + objETP.PaperType + "' and DocType='" + objETP.DocType + "' and WeekId =" + objETP.WeekId + " and SetNum=" + objETP.SetNum + " and (Level='" + objETP.Level + "' OR Level is null)"));
                            hdnCPID.Value = cpID;
                            Session["CoachPaperID"] = cpID;
                            DataSet dsDuplicate = (SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT * From CoachPapers where ProductGroupId=" + objETP.ProductGroupId + " and ProductId=" + objETP.ProductId + " and EventId=13 and EventYear=" + ddlContestYear.SelectedItem.Text + "and EventCode='Coaching' and PaperType='" + objETP.PaperType + "' and DocType='" + objETP.DocType + "' and WeekId =" + objETP.WeekId + " and SetNum=" + objETP.SetNum + " and Level='" + objETP.Level + "'"));
                            lbluiyear.Text = ddlContestYear.Text;
                            lblPtype.Text = objETP.PaperType;
                            lbldoctype.Text = objETP.DocType;
                            lblpgroup.Text = objETP.ProductGroupCode;
                            lblProduct.Text = objETP.ProductCode;
                            lbllevel.Text = objETP.Level;
                            lblsec.Text = objETP.Sections.ToString();
                            lblweek.Text = objETP.WeekId.ToString();
                            lbluiset.Text = objETP.SetNum.ToString();
                            lbluifname.Text = objETP.TestFileName;
                            if (hdnuploaded.Value == "Y") { gvModify.Visible = false; }
                            string Uinput = ddlContestYear.Text + "&nbsp;&nbsp;" + objETP.PaperType + "&nbsp;&nbsp;" + objETP.DocType + "&nbsp;&nbsp;" + objETP.ProductGroupCode + "&nbsp;&nbsp;" + objETP.ProductCode + "&nbsp;&nbsp;" + objETP.Level + "&nbsp;&nbsp;" + objETP.Sections + "&nbsp;&nbsp;" + objETP.SetNum + "&nbsp;&nbsp;" + objETP.WeekId + "&nbsp;&nbsp;" + objETP.TestFileName;
                            if (dsDuplicate.Tables[0].Rows.Count > 0)
                            {
                                gvDuplicate.DataSource = dsDuplicate;
                                gvDuplicate.DataBind();
                                // lblUserInput.Text = Uinput;
                            }

                            if (ckFname.Checked == true)
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + upFile) | cpID != string.Empty)
                                    {
                                        //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                        Panel1.Visible = false;
                                        PanelAdd.Visible = false;
                                        Panel5.Visible = true;
                                        Panel6.Visible = true;
                                        dllfileChoice.Enabled = false;
                                    }
                                    else
                                    {
                                        File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                        File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                                        lblMessage.ForeColor = System.Drawing.Color.Blue;
                                        lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                    }
                                }
                                else
                                {

                                    if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + upFile) | cpID != string.Empty)
                                    {
                                        FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "Temp_" + upFile));
                                        hdnTempFileName.Value = "Temp_" + upFile;
                                        Panel1.Visible = false;
                                        PanelAdd.Visible = false;
                                        Panel5.Visible = true;
                                        Panel6.Visible = true;
                                        dllfileChoice.Enabled = false;
                                    }
                                    else
                                    {

                                        SaveFileNewName(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], upFile);
                                        lblMessage.ForeColor = System.Drawing.Color.Blue;
                                        lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                    }
                                }


                            }
                            else
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                    Panel1.Visible = false;
                                    PanelAdd.Visible = false;
                                    Panel5.Visible = true;
                                    Panel6.Visible = true;
                                    dllfileChoice.Enabled = false;
                                }
                                else
                                {

                                    FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "Temp_" + FileUpLoad1.FileName.ToString()));
                                    hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                                    Panel1.Visible = false;
                                    PanelAdd.Visible = false;
                                    Panel5.Visible = true;
                                    Panel6.Visible = true;
                                    dllfileChoice.Enabled = false;
                                }
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        lblMessage.Text = err.Message;
                    }
                }

            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "No File Uploaded.";
            }
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "Validation Error.";
        }
    }
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        lblstatus.Text = "";
        if (ddlLevel.Enabled != true)
        {
            RequiredFieldValidator11.Enabled = false;

        }
        else
        {
            RequiredFieldValidator11.Enabled = true;
            if (ddlLevel.SelectedIndex == 0)
            {
                RequiredFieldValidator11.IsValid = false;
            }

        }
        if (!ddlSections.Enabled)
        {
            RequiredFieldValidator10.Enabled = false;
        }
        else
        {
            if (ddlSections.SelectedIndex == 0)
            {
                RequiredFieldValidator10.Enabled = true;
                RequiredFieldValidator10.Visible = true;
                RequiredFieldValidator10.IsValid = false;
            }
        }
        if (this.IsValid)
        {
            lblMessage.Text = "";
            SectionFlag = 0;
            hdnsection.Value = "0";
            if (ddlSections.SelectedItem.Text == "0" & PGCode(ddlProductGroup) != "UV")
            {
                FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + FileUpLoad1.FileName));
                ULFile.Value = FileUpLoad1.PostedFile.FileName;
                PanelSections.Visible = true;
                Panel1.Visible = false;
                PanelAdd.Visible = false;
                dllfileChoice.Enabled = false;
            }
            else
            {
                UpdateData();
            }
        }

    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        //if (hdnTempFileName.Value.Length > 0)
        //{
        if (hdnsection.Value == "1")
        {
            if (ckFname.Checked == true)
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnupfile.Value.ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + hdnupfile.Value.ToString();
            }
            else
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), ULFile.Value.Replace("cpy_", "").ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + ULFile.Value.Replace("cpy_", "").ToString();
            }
        }
        else if (hdnTempFileName.Value.Length > 0)
        {
            try
            {
                //Response.Write("Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections="+ ddlSections.SelectedItem.Value.ToString() +",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
                //File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
                //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), true);
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + hdnTempFileName.Value.Replace("Temp_", "").ToString();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString()); 
            }

        }

        else
        {
            Panel5.Visible = false;
            PanelAdd.Visible = false;
            Panel1.Visible = true;
        }

    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + "cpy_" + ULFile.Value.ToString()))
        {
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));

        }
        //if (hdnTempFileName.Value.Length > 0)
        //   // File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value));
        if (hdnTempFileName.Value.Length > 0)
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.ToString()));
        Panel6.Visible = false;
        Panel5.Visible = false;
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
    }

    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                objETP.TestPaperId = int.Parse(gvTestPapers.Rows[index].Cells[1].Text);
                objETP.TestFileName = gvTestPapers.Rows[index].Cells[14].Text;

                if (e.CommandName == "Download")
                {
                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], objETP);
                }
            }
        }
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {

    }

    private int PGId(DropDownList ddlObject)
    {
        int index;
        int PGI = -1;
        index = ddlObject.SelectedValue.ToString().IndexOf("-");
        if (index > 0)
        {
            PGI = Int32.Parse(ddlObject.SelectedValue.ToString().Trim().Substring(0, index));
        }
        return PGI;
    }

    private string PGCode(DropDownList ddlObject)
    {
        int index = -1;
        string PGC = string.Empty;
        index = ddlObject.SelectedValue.ToString().Trim().LastIndexOf("-");
        if (index > 0)
        {
            index++;
            PGC = ddlObject.SelectedValue.ToString().Substring(index);
        }
        return PGC;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            m_objETP.ProductId = PGId(ddlFlrProduct);
            m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
            m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedValue);
            m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
            m_objETP.RoleId = Int32.Parse(Session["RoleId"].ToString());
            m_objETP.MemberId = Int32.Parse(Session["LoginId"].ToString());
            //  m_objETP.Level = ddlFlrLevel.SelectedItem.Text;

            if (ddlFlrNoOfContestants.SelectedValue != "-1")
            {
                m_objETP.PaperType = ddlFlrNoOfContestants.SelectedValue; //Modified 27/07/2013
            }
            if (ddlFlrLevel.SelectedValue != "-1")
            {
                if (m_objETP.ProductGroupId == 42)
                {
                    m_objETP.Level = "NA";
                }
                else
                {
                    m_objETP.Level = ddlFlrLevel.SelectedItem.Text;
                }
            }
            else
            {
                if (m_objETP.ProductGroupId == 42)
                {
                    m_objETP.Level = "NA";
                }
                else
                {
                    m_objETP.Level = string.Empty;// = ddlFlrLevel.SelectedItem.Text;
                }
            }
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup),ddlFlrYear);
            }
            m_objETP.Description = string.Empty;// tbxFlrDescription.Text;
            m_objETP.TestFileName = string.Empty;// tbxFlrTestFileName.Text;

            GetTestPapers(m_objETP);

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            LblexamRecErr.Text = "";
            ddlFlrProduct.SelectedIndex = 0;
            ddlFlrProductGroup.SelectedIndex = 0;
            ddlFlrWeek.SelectedIndex = 0;
            ddlFlrSet.SelectedIndex = 0;
            ddlFlrNoOfContestants.SelectedIndex = 0;
            ddlFlrEvent.SelectedIndex = 0;
            ddlFlrLevel.SelectedIndex = 0;
            m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
            GetCoachPapers("Search");

        }
        catch (Exception ex)
        {
        }
    }

    protected void GetCoachPapers(string input)
    {
        DataSet ds;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "30")
        {
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from CoachPapers ORDER BY CoachPaperId");// where ProductGroupId=" + m_objETP.ProductGroupId + " and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + m_objETP.ProductGroupId + " and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ")");

        }
        else if (Session["RoleId"].ToString() == "89")
        {
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from CoachPapers where ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY CoachPaperId"); //ddlFlrProductGroup

        }
        else
        {
            string sqlstring1 = string.Empty;
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value == "Y")
            {
                sqlstring1 = "select * from CoachPapers where ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY CoachPaperId";//ddlFlrProductGroup
            }
            else
            {
                sqlstring1 = "select * from CoachPapers where ProductGroupId=" + PGId(ddlFlrProductGroup).ToString() + " and ProductCode in (select distinct C.ProductCode from CalSignUp C where C.ProductGroupId=" + PGId(ddlFlrProductGroup).ToString() + " and C.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") and [Level]  in (select distinct dbo.[CalSignUp].[Level] from dbo.[CalSignUp] where dbo.[CalSignUp].[Level] = dbo.[CalSignUp].[Level] and dbo.[CalSignUp].MemberID =" + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY CoachPaperId";

            }

            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstring1);

        }

        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
            lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
        else
            lblSearchErr.Text = string.Empty;
        DataView dv = new DataView(dt);
        if (input == "Search")
        {
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
        }
        else
        {
            gvModify.DataSource = dv;
            gvModify.DataBind();
            lblSearchErr.Text = "";
        }
    }
    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true,ddlFlrYear);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup),ddlFlrYear);
        }
        else
        {
            PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
        }
    }

    #endregion

    #region " Private Methods - Data Access Layer "
    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_Insert";
        SqlParameter[] param = new SqlParameter[20];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@EventCode", objETP.EventCode);
        param[5] = new SqlParameter("@WeekId", objETP.WeekId);
        param[6] = new SqlParameter("@SetNum", objETP.SetNum);
        param[7] = new SqlParameter("@PaperType", objETP.PaperType); //Modified 22/07/2013
        param[8] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[9] = new SqlParameter("@Description", objETP.Description);
        param[10] = new SqlParameter("@Password", objETP.Password);
        param[11] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[12] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[13] = new SqlParameter("@ModifyDate", objETP.ModifyDate);
        param[14] = new SqlParameter("@ModifiedBy", objETP.ModifiedBy);
        param[15] = new SqlParameter("@DocType", objETP.DocType);
        param[16] = new SqlParameter("@EventYear", objETP.ContestYear);
        param[17] = new SqlParameter("@Sections", objETP.Sections);
        if (objETP.Level == "NA") { param[18] = new SqlParameter("@Level", DBNull.Value); } else { param[18] = new SqlParameter("@Level", objETP.Level); }
        value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
        hdnupfile.Value = objETP.TestFileName;
        if (value == null)
        {
            return -1;
            // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }

    private void GetProductGroupCodeByRoleId(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            string conn = Application["ConnectionString"].ToString();
            DataSet dsproductgroup;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleId"].ToString()));
            param[1] = new SqlParameter("@MemberID", Int32.Parse(Session["LoginId"].ToString()));
            param[2] = new SqlParameter("@TLead", hdntlead.Value.ToString());
            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductGroupByRoleID", param);
            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "IDandCode";
            ddlObject.DataBind();
            if (dsproductgroup.Tables[0].Rows.Count == 1)
            {
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
                ddlObject.SelectedIndex = 0;
            }
            else if (dsproductgroup.Tables[0].Rows.Count < 1)
            {
                ddlObject.SelectedIndex = -1;
            }
            else
            {
                ddlObject.SelectedIndex = -1;
            }


            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }

        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }

    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsproductgroup;
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88"))
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.[ProductGroupId], V.[ProductGroupCode] AS EventCodeAndProductGroupCode FROM dbo.[Volunteer] V where V.[RoleId]=" + Int32.Parse(Session["RoleId"].ToString()) + " and V.[MemberId]=" + Int32.Parse(Session["LoginID"].ToString()));
                if (dsproductgroup.Tables[0].Rows.Count < 2)
                {
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;
                }
            }
            else
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(13) Order by A.EventCode");

            }

            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetFlrContextYear()
    {
        ddlFlrYear.Items.Clear();
        int Minyear,Maxyear;
        try
        {
            Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from testpapers"));
            Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from testpapers"));
            
            int year = DateTime.Now.Year;
            int j = 0;
            for (int i = Minyear; i <= year; i++)
            {
                ddlFlrYear.Items.Insert(j, new ListItem(i.ToString()));
                j = j + 1;
            }
            ddlFlrYear.SelectedIndex = ddlFlrYear.Items.IndexOf(ddlFlrYear.Items.FindByText(Maxyear.ToString()));
        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }


    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {

        try
        {
            ddlContestYear.Items.Insert(0, new ListItem(DateTime.Now.AddYears(-1).Year.ToString()));
            ddlContestYear.Items.Insert(1, new ListItem(DateTime.Now.Year.ToString()));
            ddlContestYear.Items.Insert(2, new ListItem(DateTime.Now.AddYears(1).Year.ToString()));

            ddlContestYear.SelectedIndex = 0;
        }

        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;

        }
        //ddlContestYear.Items.Clear();
        //int Minyear,Maxyear;
        //try
        //{
        //    Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from testpapers"));
        //    Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from testpapers"));

        //    int year = DateTime.Now.Year;
        //    int j = 0;
        //    for (int i = Minyear; i <= year; i++)
        //    {
        //        ddlContestYear.Items.Insert(j, new ListItem(i.ToString()));
        //        j = j + 1;
        //    }
        //    ddlContestYear.SelectedIndex = ddlContestYear.Items.IndexOf(ddlContestYear.Items.FindByText(Maxyear.ToString()));
        //}
        //catch (SqlException se)
        //{
        //    lblMessage.ForeColor = System.Drawing.Color.Red;
        //    lblMessage.Text = se.Message.ToString();
        //    return;
        //}

    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem,DropDownList ddlYear)
    {
        DataSet dsproduct;
        try
        {
            if (ProductGroupId != -1)
            {

                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleID"].ToString()));
                param[1] = new SqlParameter("@MemberId", Int32.Parse(Session["LoginID"].ToString()));
                param[2] = new SqlParameter("@ProductGroupId", ProductGroupId);
                param[3] = new SqlParameter("@EventYear", Int32.Parse(ddlYear.SelectedValue));//DateTime.Now.Year.ToString()));
                param[4] = new SqlParameter("@TLead", hdntlead.Value.ToString());
                // Response.Write(Session["RoleID"].ToString() + "--" + Session["LoginID"].ToString());
                dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductCodeByRoleID", param);
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "IDandCode";
                ddlObject.DataBind();
                if (dsproduct.Tables[0].Rows.Count < 2)
                {
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;
                }
                //else
                //{
                //    ddlObject.SelectedIndex = 0;
                //}

            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
                ddlObject.SelectedIndex = 0;
            }

        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }
    }

    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }
    }

    private void GetWeek(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Week#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void PopulateEvents(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Coaching", "13"));

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Event]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Homework", "HW"));
        ddlObject.Items.Add(new ListItem("PreTest", "PT"));
        ddlObject.Items.Add(new ListItem("RegTest", "RT"));
        ddlObject.Items.Add(new ListItem("FinalTest", "FT"));
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Paper Type]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulatePapertype(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Homework", "HW"));
        ddlObject.Items.Add(new ListItem("PreTest", "PT"));
        ddlObject.Items.Add(new ListItem("RegTest", "RT"));
        ddlObject.Items.Add(new ListItem("FinalTest", "FT"));
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Paper Type]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void PopulateSection(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Section#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void PopulateLevelNew(DropDownList ddlObject, bool blnCreateEmptyItem, int ProductGroupID,DropDownList ddlYear)
    {
        ddlObject.Enabled = true;
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT DISTINCT Level FROM CalSignUp WHERE Accepted='Y' and  MemberID =" + Session["LoginId"].ToString() + " AND EventYear =" + Int32.Parse(ddlYear.SelectedValue) + " and ProductGroupId=" + ProductGroupID); //DateTime.Now.Year.ToString()
        //"SELECT DISTINCT Level FROM CalSignUp WHERE Level is not null and MemberID =" + Session["LoginId"].ToString() + "and Accepted='Y'"); //" and ProductGroupID in (" + ProductGroupID + ") 
        // + " AND EventYear =" + Int32.Parse(DateTime.Now.Year.ToString()));
        ddlObject.DataSource = ds;
        ddlObject.DataTextField = "Level";
        ddlObject.DataValueField = "Level";
        ddlObject.DataBind();

        if (ds.Tables[0].Rows.Count < 2)
        {
            //if(ds.Tables[0].Rows[0]["Level"] == DBNull.Value)
            // {
            //     blnCreateEmptyItem = true;
            //     ddlObject.Enabled = false;
            // }

            // else
            // {
            blnCreateEmptyItem = false;
            ddlObject.Enabled = false;
            //}
        }
        if (ds.Tables[0].Rows.Count == 0)
        {
            blnCreateEmptyItem = true;
            ddlObject.Enabled = false;
        }

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
        }

    }

    private void PopulateLevel(DropDownList ddlObject, string iCondition, bool blnCreateEmptyItem)
    {
        ArrayList list = new ArrayList();
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        if (iCondition.ToLower() == "math")
        {
            list.Add(new ListItem("Beginner", "BEG"));
            list.Add(new ListItem("Intermediate", "INT"));
            list.Add(new ListItem("Advanced", "ADV"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition.ToLower() == "sat")
        {
            list.Add(new ListItem("Junior", "JR"));
            list.Add(new ListItem("Senior", "SR"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition == "NA")
        {
            //ddlObject.Enabled = false;
        }
        else
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = false;
            blnCreateEmptyItem = false;
        }



        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
        }

        //ddlObject.SelectedIndex = 0;


    }
    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "UploadPapers", "DownloadPapers", "ModifyPapers" };
        // string[] Choice = { "UploadPapers", "DownloadPapers" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void GetTestPapers(EntityTestPaper objETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[20];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        if (ddlFlrYear.SelectedValue == DateTime.Now.Year.ToString())
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        else
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@PaperType", objETP.PaperType);// Modified 22/07/2013
        //param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@EventYear", ddlFlrYear.SelectedValue);
        param[12] = new SqlParameter("@DocType", objETP.DocType);
        param[13] = new SqlParameter("@Sections", objETP.Sections);
        param[14] = new SqlParameter("EventId", objETP.EventId);
        param[15] = new SqlParameter("@Level", objETP.Level);
        param[16] = new SqlParameter("@RoleId", objETP.RoleId);
        param[17] = new SqlParameter("@MemberId", objETP.MemberId);
        param[18] = new SqlParameter("@Tlead", hdntlead.Value);

        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
                lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
            else
                lblSearchErr.Text = string.Empty;
            DataView dv = new DataView(dt);
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
        }
        catch (Exception ex)
        {
        }

    }

    private int GetTestPapers(int memberid, int roleid, string contestYear, int weekID)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetTestPapersforDownload";
        string SQLContest = "";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@memberid", memberid);
        param[1] = new SqlParameter("@contestyear", contestYear);
        param[2] = new SqlParameter("@WeekId", weekID);
        param[3] = new SqlParameter("@NoOfDaysBeforeContestDate", 10);
        param[4] = new SqlParameter("@Chapter", hdnChapterID.Value);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            DataTable dtNew = dt.Clone();
            String CurrTestFileName;
            String CurrTestFilePrefix;
            String PrevTestFilePrefix = "";
            try
            {
                //Following loop is to filter out the Test Paprers, which belong to the same product code and same set but with 
                //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
                //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                    CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                    if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                    {
                        PrevTestFilePrefix = CurrTestFilePrefix;
                        DataRow dr = dt.Rows[i];
                        dtNew.ImportRow(dr);
                    }
                }


                SQLContest = " Select COUNT(*) from CoachPapers where ProductCode in (Select Distinct c.ProductCode From Contestant c Inner Join Contest cn on cn.Contest_year=c.ContestYear and Cn.ContestId=c.Contestcode ";
                SQLContest = SQLContest + " where c.parentid=" + memberid + " and c.ContestYear= " + contestYear + " and cn.contestdate - 10 <= Convert(datetime, Convert(int, GetDate())) and ExamRecID=" + memberid + ") and ContestYear=" + contestYear;
                if (weekID > 0)
                {
                    SQLContest = SQLContest + " and WeekId=" + weekID;
                }
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, SQLContest)) > 0)
                {
                    lblPrdError.Text = "Your children are in the same contest(s).  So you cannot receive the other test papers.";
                }
            }

            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            DataView dv = new DataView(dtNew);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();

            return dtNew.Rows.Count;
        }
        else
        {
            return 0;
        }
    }

    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CoachPaperId", objDelETP.TestPaperId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }

    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion

    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "No uploaded file";
        }
    }


    private void SaveFileNewName(string sVirtualPath, string uploadFileName)
    {
        if (FileUpLoad1.HasFile)
        {
            FileUpLoad1.SaveAs(string.Concat(Server.MapPath(sVirtualPath), uploadFileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "No uploaded file";
        }
    }
    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {

            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");

            //The following code calls Response.End which results in bad thread errors
            //so, trying to avoid it with above code
            /*
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                FileInfo finfo = new FileInfo(filePath);
                long FileInBytes = finfo.Length;
                Response.AddHeader("Content-Length", FileInBytes.ToString());
            }
            Response.Clear();
            //Response.Write("File Path: " + filePath);
            Response.WriteFile(@filePath);
            Response.Flush();
            Response.End();
            */
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }
    }
    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }
    }

    private string ModifyFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode;
        if (objETP.ProductGroupCode != string.Empty)
        {
            ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        }
        string extension;
        string l_value = string.Empty;
        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                l_value = "JR";
                break;
            case "Senior":
                l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }

        TFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
        // System.DateTime.Now.Year
        extension = Path.GetExtension(txtDescriptionM.Text);
        upFile = string.Concat(TFileName.ToString(), extension);
        return upFile;
    }

    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string ModifiedFileName(EntityTestPaper objETP, bool option)
    {
        StringBuilder CFileName = new StringBuilder();
        string exten;
        string l_value = string.Empty;
        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                l_value = "JR";
                break;
            case "Senior":
                l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }
        //if (ddlLevel.SelectedIndex != 0)
        //{
        CFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
        //System.DateTime.Now.Year
        //}
        //else
        //{
        //CFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", System.DateTime.Now.Year, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, "NA", objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
        //}

        if (option == false)
        {
            if (SectionFlag == 1 | SectionFlag == 2)
            {
                exten = Path.GetExtension(ULFile.Value);
                upFile = string.Concat(CFileName.ToString(), exten);
            }
            else
            {
                exten = Path.GetExtension(FileUpLoad1.PostedFile.FileName);
                upFile = string.Concat(CFileName.ToString(), exten);
            }
        }
        else
        {
            upFile = CFileName.ToString();
        }

        return upFile;
    }

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {

        string ProductGroupCode = EventCodeAndProductGroupCode.Replace("Coaching - ", "");
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = EventCodeAndProductGroupCode.Substring(0, EventCodeAndProductGroupCode.Length - 5);
        return EventCode;
    }

    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {
        try
        {
            lblSearchErr.Text = "";
            Panel1.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            gvModify.Visible = false;
            Panel2.Visible = searchPanel;
            Panel3.Visible = searchPanel;
            if (searchPanel == false)
            {
                Panel4.Visible = true;
                GetWeek(ddlFlrWeekForExamReceiver, true);
            }
            GetFlrContextYear();
            GetProductGroupCodeByRoleId(ddlFlrProductGroup, true);

            GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true,ddlFlrYear);
            GetWeek(ddlFlrWeek, true);
           // GetFlrContextYear();
            PopulateContestants(ddlFlrNoOfContestants, true);
            PopulateEvents(ddlFlrEvent, false);
            PopulateSets(ddlFlrSet, true);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup),ddlFlrYear);
            }
            else
            {
                PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    private void LoadModifyPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = true;
            ModifyGrid.Visible = true;
            if (gvModify.Columns[0].Visible == false)
                gvModify.Columns[0].Visible = true;
            gvModify.Visible = true;
            GetContextYear(ddlContestYear, true);
            GetProductGroupCodeByRoleId(ddlProductGroup, true);
            //GetContextYear(ddlContestYear, true);

            //ddlContestYear.SelectedIndex = 1;
            GetWeek(ddlWeek, true);
            GetProductCodes(PGId(ddlProductGroup), ddlProduct, true,ddlContestYear);
            PopulateContestants(ddlNoOfContestants, true);
            PopulateEvents(ddlEvent, false);
            PopulateSets(ddlSet, true);
            PopulateSection(ddlSections, true);
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup),ddlContestYear);
            }
            else
            {
                PopulateLevel(ddlLevel, "NA", true); // Hard Coded - Not retrieving from database 
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true);
            }
            ddlDocType.SelectedIndex = 0;
            m_objETP.ProductGroupId = PGId(ddlProductGroup);
            GetCoachPapers("Modify");
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }

    }
    private void LoadUploadPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
            PanelAdd.Visible = true;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            gvModify.Visible = false;
            ddlDocType.Enabled = true;
            GetProductGroupCodeByRoleId(ddlProductGroup, true);
            GetContextYear(ddlContestYear, true);
            //ddlContestYear.SelectedIndex = 1;
            GetWeek(ddlWeek, true);
            GetProductCodes(PGId(ddlProductGroup), ddlProduct, true,ddlContestYear);
            PopulateContestants(ddlNoOfContestants, true);
            PopulateEvents(ddlEvent, false);
            PopulateSets(ddlSet, true);
            PopulateSection(ddlSections, true);
            LinkButton1.Text = "Show Uploaded files";
            string rid = Session["RoleId"].ToString();
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup),ddlContestYear);
                ddlDocType.SelectedIndex = 0;
            }
            else
            {
                PopulateLevel(ddlLevel, "NA", true); // Hard Coded - Not retrieving from database 
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true);
                ddlDocType.SelectedIndex = 0;
            }

        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region " Private Methods - Validations "

    private bool ValidateFileName(EntityTestPaper objETP)
    {

        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode;
        if (objETP.ProductGroupCode != string.Empty)
        {
            ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        }
        bool IsValidFileName = false;
        // Get the initial part of the file name
        string extension;

        string l_value = string.Empty;
        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                l_value = "JR";
                break;
            case "Senior":
                l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }

        TFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear , objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
            //System.DateTime.Now.Year
        if (SectionFlag == 1 | SectionFlag == 2)
        {
            extension = Path.GetExtension(ULFile.Value);
            upFile = string.Concat(TFileName.ToString(), extension);
        }
        else
        {
            extension = Path.GetExtension(FileUpLoad1.PostedFile.FileName);
            upFile = string.Concat(TFileName.ToString(), extension);
        }

        if (SectionFlag == 1 | SectionFlag == 2)
        {

            if (ULFile.Value.ToLower() == string.Concat(TFileName.ToString().ToLower(), extension))
            {
                IsValidFileName = true;
            }

        }
        else
        {
            if (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), extension))
            {
                IsValidFileName = true;
            }
        }
        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            if (ckFname.Checked == true)
            {
                m_objETP.TestFileName = upFile;
                return true;
            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString() + extension);
                return false;
            }
        }
        else
        {
            return true;
        }

    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion

    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (dllfileChoice.SelectedItem.Text == "UploadPapers")
        //{
        if (ddlWeek.SelectedValue != "-1")
        {
            ddlSet.SelectedIndex = ddlSet.Items.IndexOf(ddlSet.Items.FindByValue(ddlWeek.SelectedValue));
            ddlSet.Enabled = false;

            if (dllfileChoice.SelectedItem.Text == "UploadPapers")

                if (ddlDocType.SelectedValue == "Q")
                    ddlSections.Enabled = true;
                else
                {
                    String StrSQL = "";
                    StrSQL = "Select Distinct Sections from CoachPapers where [ProductId]=" + PGId(ddlProduct) + " and [ProductCode]='" + PGCode(ddlProduct) + "' and [ProductGroupId]=" + PGId(ddlProductGroup) + " and [ProductGroupCode]='" + PGCode(ddlProductGroup) + "' and [Level]='" + ddlLevel.SelectedItem.Text + "' and [EventYear]=" + ddlContestYear.SelectedItem.Text;
                    StrSQL = StrSQL + " and [EventId]= " + ddlEvent.SelectedValue + " and [EventCode]='" + ddlEvent.SelectedItem.Text + "' and [WeekId]=" + int.Parse(ddlWeek.SelectedItem.Value) + " and [SetNum]=" + int.Parse(ddlSet.SelectedValue);
                    StrSQL = StrSQL + " and [PaperType]='" + ddlNoOfContestants.SelectedItem.Value.ToString() + "' and DocType ='Q'";//" + ddlDocType.SelectedItem.Value.ToString()+ "
                    int sections = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL));
                    if (sections >= 0)
                        ddlSections.SelectedIndex = ddlSections.Items.IndexOf(ddlSections.Items.FindByValue(sections.ToString()));
                    ddlSections.Enabled = false;
                }
        }
        //}
        //else
        //ddlSet.Enabled =true;

    }
    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dllfileChoice.SelectedItem.Text == "UploadPapers")

            if (ddlDocType.SelectedValue == "Q")
                ddlSections.Enabled = true;
            else
            {
                String StrSQL = "";
                StrSQL = "Select Distinct Sections from CoachPapers where [ProductId]=" + PGId(ddlProduct) + " and [ProductCode]='" + PGCode(ddlProduct) + "' and [ProductGroupId]=" + PGId(ddlProductGroup) + " and [ProductGroupCode]='" + PGCode(ddlProductGroup) + "' and [Level]='" + ddlLevel.SelectedItem.Text + "' and [EventYear]=" + ddlContestYear.SelectedItem.Text;
                StrSQL = StrSQL + " and [EventId]= " + ddlEvent.SelectedValue + " and [EventCode]='" + ddlEvent.SelectedItem.Text + "' and [WeekId]=" + int.Parse(ddlWeek.SelectedItem.Value) + " and [SetNum]=" + int.Parse(ddlSet.SelectedValue);
                StrSQL = StrSQL + " and [PaperType]='" + ddlNoOfContestants.SelectedItem.Value.ToString() + "' and DocType ='Q'";//" + ddlDocType.SelectedItem.Value.ToString()+ "
                int sections = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL));
                if (sections >= 0)
                    ddlSections.SelectedIndex = ddlSections.Items.IndexOf(ddlSections.Items.FindByValue(sections.ToString()));
                ddlSections.Enabled = false;
            }
    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }

    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
        int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), ddlFlrYear.SelectedValue  , selectedWeekId); //DateTime.Now.Year.ToString()
        if (Count == 0)
        {
            lblNoPermission.Visible = true;
            lblNoPermission.Text = "There are no records";
        }
    }

    protected void ddlFlrProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {

    }
    protected void gvModify_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMerror.Text = "";
        try
        {
            GridViewRow row = gvModify.SelectedRow;
            ddlNoOfContestants.SelectedValue = row.Cells[6].Text;
            ddlDocType.SelectedValue = row.Cells[7].Text;
            ddlSections.SelectedValue = row.Cells[11].Text;
            ddlSet.SelectedValue = row.Cells[13].Text;
            ddlWeek.SelectedValue = row.Cells[12].Text;
            lblhiddenCpId.Text = row.Cells[1].Text;
            ddlProductGroup.SelectedValue = gvModify.DataKeys[row.RowIndex].Values["ProductGroupId"].ToString() + "-" + row.Cells[8].Text;
            GetProductCodes(PGId(ddlProductGroup), ddlProduct, true,ddlContestYear);
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup),ddlContestYear);
            }
            else
            {
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true);
            }
            ddlProduct.SelectedValue = row.Cells[4].Text + "-" + row.Cells[9].Text;

            if (ddlLevel.Enabled)
            {
                ddlLevel.SelectedValue = ddlLevel.Items.FindByText(row.Cells[10].Text).Value;
            }

            ddlDocType.Enabled = false;
            lblhiddenFname.Text = row.Cells[14].Text.Trim();
            txtDescriptionM.Text = row.Cells[15].Text.Replace("&nbsp;", "");
            TxtPasswordM.Text = row.Cells[16].Text.Replace("&nbsp;", "");
            btnUpdate.Enabled = true;
            btncancel.Enabled = true;
        }
        catch (Exception ex)
        {
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        RequiredFieldValidator11.Enabled = false;
        lblMerror.Text = "";
        SearchFlag = true;
        gvModify_SelectedIndexChanged(null, null);
        btnUpdate.Enabled = false;
        btncancel.Enabled = false;

    }
    protected void ddlProductGroup_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblMerror.Text = "";
        if (ddlLevel.Enabled != true)
        {
            RequiredFieldValidator11.Enabled = false;

        }
        else
        {
            RequiredFieldValidator11.Enabled = true;
            if (ddlLevel.SelectedIndex == 0)
            {
                RequiredFieldValidator11.IsValid = false;
            }

        }
        if (!ddlSections.Enabled)
        {
            RequiredFieldValidator10.Enabled = false;
        }
        else
        {
            if (ddlSections.SelectedIndex == 0)
            {
                RequiredFieldValidator10.Enabled = true;
                RequiredFieldValidator10.Visible = true;
                RequiredFieldValidator10.IsValid = false;
            }
        }
        if (Page.IsValid)
        {
            EntityTestPaper objETP = new EntityTestPaper();
            lblMerror.Text = "";
            RequiredFieldValidator11.Enabled = false;
            //ddlLevel.Enabled = true;
            objETP.ProductId = PGId(ddlProduct);
            objETP.ProductCode = PGCode(ddlProduct);
            objETP.ProductGroupCode = PGCode(ddlProductGroup);
            objETP.ProductGroupId = PGId(ddlProductGroup);
            objETP.EventCode = ddlEvent.SelectedItem.Text;
            objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
            objETP.SetNum = int.Parse(ddlSet.SelectedValue);
            objETP.Sections = int.Parse(ddlSections.SelectedItem.Text);
            objETP.PaperType = ddlNoOfContestants.SelectedItem.Value.ToString(); // Modified 22/07/2013
            //objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
            objETP.ContestYear = ddlContestYear.SelectedItem.Text;
            objETP.DocType = ddlDocType.SelectedItem.Value.ToString();
            objETP.Description = txtDescriptionM.Text;
            objETP.Password = TxtPasswordM.Text;
            objETP.CreateDate = System.DateTime.Now;
            objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
            objETP.Event = ddlEvent.SelectedItem.Text;
            if (ddlLevel.Enabled == true)
            {
                objETP.Level = ddlLevel.SelectedItem.Text;
            }
            else
            {
                objETP.Level = "NA";
            }
            string mFile = ModifiedFileName(objETP, true);
            string extension;
            extension = Path.GetExtension(lblhiddenFname.Text);
            mFile += extension;
            objETP.TestFileName = mFile;
            string strsql = "UPDATE Coachpapers Set [ProductId]=" + objETP.ProductId + ",[ProductCode]='" + objETP.ProductCode;
            strsql += "',[ProductGroupId]=" + objETP.ProductGroupId + ",[ProductGroupCode]='" + objETP.ProductGroupCode;
            if (objETP.Level == "NA")
                strsql += "',[Level]=" + DBNull.Value;
            else
                strsql += "',[Level]='" + objETP.Level + "'";

            strsql += ",[WeekId]=" + objETP.WeekId + ",[SetNum]=" + objETP.SetNum;
            strsql += ",[PaperType]='" + objETP.PaperType + "',[Sections]=" + objETP.Sections + ",[TestFileName]='" + objETP.TestFileName + "',[Description]='" + objETP.Description;
            strsql += "',[Password]='" + objETP.Password + "',[ModifyDate]='" + objETP.CreateDate + "',[ModifiedBy]=" + objETP.CreatedBy + " WHERE [CoachPaperId]=" + lblhiddenCpId.Text;

            try
            {
                string oldFileName = lblhiddenFname.Text;
                string newFileName = mFile;
                File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), oldFileName), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), newFileName));
                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsql);
                GetCoachPapers("Modify");
                btnUpdate.Enabled = false;
                btncancel.Enabled = false;

            }
            catch (Exception ex)
            {
                lblMerror.Text = ex.Message.ToString();
            }
        }
    }

    protected void bttnYes_Click(object sender, EventArgs e)
    {
        SectionFlag = 1;
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        PanelSections.Visible = false;
        dllfileChoice.Enabled = true;
        hdnsection.Value = "1";
        UpdateData();
    }
    protected void bttnNo_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + "cpy_" + ULFile.Value.ToString()))
        {
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));

        }
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        PanelSections.Visible = false;
        hdnsection.Value = "0";
        SectionFlag = 0;
        RequiredFieldValidator5.Enabled = true;
        RequiredFieldValidator5.Visible = true;
        RequiredFieldValidator5.IsValid = false;
        lblMessage.Text = "Please select a section value other than 0.";
    }

    protected void UploadedData()
    {
        try
        {

            lblstatus.Text = "";
            lblstatus.Visible = false;
            string StrSql = string.Empty;
            Boolean VolFlag = false;
            String WhereCndn = "";
            if (Session["RoleId"].ToString() == "1" | Session["RoleId"].ToString() == "2" | Session["RoleId"].ToString() == "30")
            {
                StrSql = "SELECT * FROM CoachPapers ";
                VolFlag = true;
            }
            else
            {
                StrSql = "select * from CoachPapers C where C.ProductID in(select ProductID  from volunteer where MemberID =" + Session["LoginID"].ToString() + ")";
                VolFlag = false;
                //Response.Write(StrSql);
            }
            if (VolFlag == true)
            {
                if (ddlProductGroup.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " Where ProductGroupId=" + PGId(ddlProductGroup);

                if (ddlProduct.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and ProductId=" + PGId(ddlProduct);
                    else
                        WhereCndn = WhereCndn + " Where ProductId=" + PGId(ddlProduct);

                if (ddlLevel.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and Level='" + ddlLevel.SelectedItem.Text + "'";
                    else
                        WhereCndn = WhereCndn + " Where Level='" + ddlLevel.SelectedItem.Text + "'";

                if (ddlNoOfContestants.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and PaperType='" + ddlNoOfContestants.SelectedValue + "'";
                    else
                        WhereCndn = WhereCndn + " Where PaperType='" + ddlNoOfContestants.SelectedValue + "'";

                if (ddlDocType.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and DocType='" + ddlDocType.SelectedValue + "'";
                    else
                        WhereCndn = WhereCndn + " Where DocType='" + ddlDocType.SelectedValue + "'";

                if (ddlWeek.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and WeekID=" + ddlWeek.SelectedValue;
                    else
                        WhereCndn = WhereCndn + " Where WeekID=" + ddlWeek.SelectedValue;

                //if (ddlSections.SelectedValue != "-1")
                //    if(WhereCndn != "")
                //        WhereCndn = WhereCndn + " and Sections=" + ddlSections.SelectedValue ;
                //    else
                //        WhereCndn = WhereCndn + " Where Sections=" + ddlSections.SelectedValue ;

            }
            else
            {
                if (ddlProductGroup.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and ProductGroupId=" + PGId(ddlProductGroup);
                if (ddlProduct.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and ProductId=" + PGId(ddlProduct);
                if (ddlLevel.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and Level='" + ddlLevel.SelectedItem.Text + "'";
                if (ddlNoOfContestants.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and PaperType='" + ddlNoOfContestants.SelectedValue + "'";
                if (ddlDocType.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and DocType='" + ddlDocType.SelectedValue + "'";
                if (ddlWeek.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and WeekID=" + ddlWeek.SelectedValue;
                //if (ddlSections.SelectedValue != "-1")
                //   WhereCndn = WhereCndn + " and Sections=" + ddlSections.SelectedValue ;

            }
            StrSql = StrSql + WhereCndn + " ORDER BY CoachPaperID";
            //Response.Write(StrSql);


            if (LinkButton1.Text == "Show Uploaded files")
            {

                DataSet dsu = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSql);
                if (dsu.Tables[0].Rows.Count > 0)
                {
                    gvModify.DataSource = dsu;
                    gvModify.DataBind();
                    gvModify.SelectedIndex = -1;
                    gvModify.Visible = true;
                    gvModify.Columns[0].Visible = false;
                    LinkButton1.Text = "Hide Uploaded files";
                    hdnuploaded.Value = "Y";
                }
                else
                {
                    gvModify.DataSource = null;
                    gvModify.DataBind();
                    lblstatus.Text = "No files uploaded.";
                    lblstatus.Visible = true;
                    hdnuploaded.Value = "N";
                }

            }
            else if (LinkButton1.Text == "Hide Uploaded files")
            {
                gvModify.Visible = false;
                LinkButton1.Text = "Show Uploaded files";
                hdnuploaded.Value = "N";
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        UploadedData();
    }


    protected void ddlFlrWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFlrWeek.SelectedValue != "-1")
        {
            ddlFlrSet.SelectedIndex = ddlFlrSet.Items.IndexOf(ddlFlrSet.Items.FindByValue(ddlFlrSet.SelectedValue));
            ddlFlrSet.Enabled = false;
        }
    }


#endregion

    #region "Enums"
    public enum ScreenChoice
    {
        DownloadPapers,
        UploadPapers,
        ModifyPapers

    }

    #endregion

    #region " Class EntityTestPaper (ETP) "
    public class EntityTestPaper
    {
        public Int32 TestPaperId = -1;
        public Int32 ProductId = -1;
        public string ProductCode = DBNull.Value.ToString();
        public string Event = DBNull.Value.ToString();
        public Int32 EventId = -1;
        public Int32 ProductGroupId = -1;
        public string ProductGroupCode = DBNull.Value.ToString();
        public string EventCode = DBNull.Value.ToString();
        public Int32 WeekId = -1;
        public Int32 SetNum = -1;
        public string PaperType = DBNull.Value.ToString(); //Modified on 22/07/2013
        public Int32 Sections = -1;
        public string TestFileName = DBNull.Value.ToString();
        public string Description = DBNull.Value.ToString();
        public string Password = DBNull.Value.ToString();
        public DateTime CreateDate = new System.DateTime(1900, 1, 1);
        public Int32 CreatedBy = -1;
        public DateTime? ModifyDate = null;
        public Int32? ModifiedBy = null;
        public string ContestYear = DBNull.Value.ToString();
        public string DocType = DBNull.Value.ToString();
        public string WeekOf = DBNull.Value.ToString();
        public string ReceivedBy = DBNull.Value.ToString();
        public string Level = DBNull.Value.ToString();
        public Int32 RoleId = -1;
        public Int32 MemberId = -1;
        public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);

        public string getCompleteFTPFilePath(String TestFileName)
        {
            return String.Format("{0}/{1}",
                      System.Configuration.ConfigurationManager.AppSettings["FTPCoachPapersPath"], TestFileName);
        }
        public EntityTestPaper()
        {
        }

    }
}
#endregion

 