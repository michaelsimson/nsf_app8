﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Drawing;
using System.Data.SqlClient;


public partial class IRS1099KDataReconciliation : System.Web.UI.Page
{
    string StrQryCheck;
    string ConnectionString = "ConnectionString";

    protected string YearFormat(string year)
    {

        if (Session["FiscalYear"].ToString().Equals(string.Empty))
        {
            return year;
        }
        else
        {
            year = year + "-" + ((Convert.ToInt32(year) + 1).ToString()).Substring(2, 2);
            return year;
        }
    }
    protected string CurFormat(string moneyvalue)
    {
        if (moneyvalue.Equals(""))
        {
            return null;
        }
        else
        {
            decimal m = Convert.ToDecimal(moneyvalue);
            
            string html = String.Format("{0:C}", m);
            //html.Replace('$', ' ');
            if (m < 0)
            {
                
                string r=html.Replace('(',' ');
                string s=r.Replace(')',' ');
                return "-" + s.Replace('$', ' ');
            }
            return html.Replace('$', ' ');
        }
        //return moneyvalue;
    }




    protected void Page_Load(object sender, EventArgs e)
    {
       //Session["LoginID"] = "4240";
       if (Session["LoginID"] == null)
       {
           Response.Redirect("~/Maintest.aspx");
       }
        else
        {
           if (!IsPostBack)
            {
                //Session["LoginID"] = "4240";
             
                //Display();

                PopulateYear(ddlYear);
                ddlYear.SelectedIndex = 1;
                ddlReportType.SelectedIndex = 1;
                divTableView.Visible = false;
                //displayrecords();
            }
       }
    }


    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            //for (int i = 2011; i <= MaxYear; i++)
            //{
            //    list.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            for (int i = MaxYear; i >= (DateTime.Now.Year) - 4; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();

             ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Session["MonthlyReport"] = string.Empty;

        Session["AnnualReportRevenue"] = string.Empty;
        Session["AnnualReportTrans"] = string.Empty;
        Session["FiscalYear"] = string.Empty;
        Session["AnnualReportRevenueFiscal"] = string.Empty;
        if (ddlFrequency.SelectedIndex == 0)
        {
                if ((ddlYear.SelectedValue == "-1") || (ddlReportType.SelectedValue == "-1"))
                {
                    lblError.Visible = true;
                    lblError.Text = "Please select All Options";
                    divTableView.Visible = false;
                    //gvIRSReport.Visible = false;
                    //gvAnnualReportTrans.Visible = false;
                }
                else
                {
                    lblError.Visible = false;
           
                lblTableHeader.Text = "Monthly Reconciliation Report";
                SqlParameter[] spParameter = new SqlParameter[2];

                spParameter[0] = new SqlParameter("@year", SqlDbType.Int, 4);
                spParameter[0].Direction = ParameterDirection.Input;
                spParameter[0].Value = ddlYear.SelectedValue;

                if (ddlReportType.SelectedIndex == 1)
                {
                    spParameter[1] = new SqlParameter("@CalType", SqlDbType.Int, 4);
                    spParameter[1].Direction = ParameterDirection.Input;
                    spParameter[1].Value = 0;
                }
                else
                {
                    spParameter[1] = new SqlParameter("@CalType", SqlDbType.Int, 4);
                    spParameter[1].Direction = ParameterDirection.Input;
                    spParameter[1].Value = 1;
                }
                DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.StoredProcedure, "usp_GetIRSCalendarYear", spParameter);

                DataTable dt = ds.Tables[0];

                int vAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["VMDCount"]))
                        vAmount += Convert.ToInt32(dr["VMDCount"]);
                }
                int vnsfAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (!DBNull.Value.Equals(dr["VMDNSFCount"]))
                        vnsfAmount += Convert.ToInt32(dr["VMDNSFCount"]);
                }
                int vnsfRefunds = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (!DBNull.Value.Equals(dr["VMDNSFRefunds"]))
                        vnsfRefunds += Convert.ToInt32(dr["VMDNSFRefunds"]);
                }
                int aAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["AmExCount"]))
                        aAmount += Convert.ToInt32(dr["AmExCount"]);
                }
                int ansfAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["AmExNSFCount"]))
                        ansfAmount += Convert.ToInt32(dr["AmExNSFCount"]);
                }
                int ansfRefunds= 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["AmExNSFRefunds"]))
                        ansfRefunds += Convert.ToInt32(dr["AmExNSFRefunds"]);
                }
                int totalAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["TotalCount"]))
                        totalAmount += Convert.ToInt32(dr["TotalCount"]);
                }
                int totalnsfAmount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["TotalNSFCount"]))
                        totalnsfAmount += Convert.ToInt32(dr["TotalNSFCount"]);
                }
                int totalnsfRefunds = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["TotalNSFRefunds"]))
                        totalnsfRefunds += Convert.ToInt32(dr["TotalNSFRefunds"]);
                }
                int diff = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!DBNull.Value.Equals(dr["Diff"]))
                        diff += Convert.ToInt32(dr["Diff"]);
                }
                dt.Rows.Add(new object[] { "TOTAL", vAmount, vnsfAmount,vnsfRefunds, aAmount, ansfAmount,ansfRefunds, totalAmount, totalnsfAmount,totalnsfRefunds,diff });

                gvIRSReport.DataSource = ds;
                gvIRSReport.DataBind();
                excel.Enabled = true;
                Session["MonthlyReport"] = ds.Tables[0];

                divTableView.Visible = true;
                divShowTrans.Visible = true;
                lblYearTrans.Text = ddlYear.SelectedValue + " Transactions";
                string strQuery = "Select sum(Count) as Count from IRS1099KTrans where Year=" + ddlYear.SelectedValue;
                DataSet dset = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strQuery);
                if (dset.Tables[0].Rows.Count > 0)
                {
                    string cCount = dset.Tables[0].Rows[0]["Count"].ToString();
                    txtBoxTransCount.Text = cCount;
                }

                //gvIRSReport.Visible = true;
                divTableview2.Visible = false;
            }
            
        }
        else
            {
                if (ddlReportType.SelectedIndex == 1)
                {
                    lblTableHeader.Text = "Annual Reconciliation Report:Revenues";
                    DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.StoredProcedure, "usp_GetIRSAnnualYear");
                    gvIRSReport.DataSource = ds;
                    gvIRSReport.DataBind();
                    excel.Enabled = true;
                    Session["AnnualReportRevenue"] = ds.Tables[0];

                    divTableview2.Visible = true;
                    lblHeaderTable2.Text = "Annual Reconciliation Report:Trans Volume";
                    DataSet dstrans = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.StoredProcedure, "usp_GetIRSAnnualYearTrans");
                    gvAnnualReportTrans.DataSource = dstrans;
                    gvAnnualReportTrans.DataBind();
                    Session["AnnualReportTrans"] = dstrans.Tables[0];
                    divTableView.Visible = true;
                    divShowTrans.Visible = false;
                }
                else
                {
                    Session["FiscalYear"] = "FisAnnualYear";
                    divTableView.Visible = true;
                    divTableview1.Visible = true;
                    divShowTrans.Visible = false;
                    lblTableHeader.Text = "Annual Reconciliation Report:Revenues";
                    DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.StoredProcedure, "usp_GetIRSAnnualFiscalYearRev");
                    gvIRSReport.DataSource = ds;
                    gvIRSReport.DataBind();
                    excel.Enabled = true;
                    Session["AnnualReportRevenueFiscal"] = ds.Tables[0];
                    divTableview2.Visible = false;
                    //Session["AnnualReportTrans"] = string.Empty;
                }
            }
    }

    protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFrequency.SelectedIndex == 1)
        {
            ddlYear.SelectedIndex = 0;
            ddlYear.Enabled = false;
            //ddlReportType.SelectedIndex = 0;
            //ddlReportType.Enabled = false;
        }
        else {
            ddlYear.Enabled = true ;
            ddlYear.SelectedIndex = 1;
            ddlReportType.Enabled = true;
            ddlReportType.SelectedIndex = 1;
        }
    }
    protected void gvIRSReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

            e.Row.Cells[0].Width = new Unit("15%");
            e.Row.Cells[1].Width = new Unit("8%");
            e.Row.Cells[2].Width = new Unit("8.5%");
            e.Row.Cells[3].Width = new Unit("8.5%");
            e.Row.Cells[4].Width = new Unit("8%");
            e.Row.Cells[5].Width = new Unit("8.5%");
            e.Row.Cells[6].Width = new Unit("8.5%");
            e.Row.Cells[7].Width = new Unit("9%");
            e.Row.Cells[8].Width = new Unit("9%");
            e.Row.Cells[9].Width = new Unit("9%");
            e.Row.Cells[10].Width = new Unit("9%");

        }
    }
    protected void gvAnnualReportTrans_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

            e.Row.Cells[0].Width = new Unit("15%");
            e.Row.Cells[1].Width = new Unit("8%");
            e.Row.Cells[2].Width = new Unit("8.5%");
            e.Row.Cells[3].Width = new Unit("8.5%");
            e.Row.Cells[4].Width = new Unit("8%");
            e.Row.Cells[5].Width = new Unit("8.5%");
            e.Row.Cells[6].Width = new Unit("8.5%");
            e.Row.Cells[7].Width = new Unit("9%");
            e.Row.Cells[8].Width = new Unit("9%");
            e.Row.Cells[9].Width = new Unit("9%");
            e.Row.Cells[10].Width = new Unit("9%");

        }
    }
    protected void excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    protected void ExportToExcel()
    {
        try
        {
        //    Session["MonthlyReport"] = string.Empty;

        //    Session["AnnualReportRevenue"] = string.Empty;
        //    Session["AnnualReportTrans"] = string.Empty;
            //Session["AnnualReportRevenueFiscal"] = string.Empty;
            if (Session["MonthlyReport"]!= string.Empty)
            {
                DataTable dtWorkshop = (DataTable)Session["MonthlyReport"];
                //dtWorkshop.Columns["Diff"].ColumnName = "Diff";
                Response.Clear();
                if (ddlReportType.SelectedIndex == 1)
                {
                    Response.AppendHeader("content-disposition", "attachment;filename=IRS1099K_MonthlyReport_CalendarYear.xls");
                }
                else if (ddlReportType.SelectedIndex == 2)
                {
                    Response.AppendHeader("content-disposition", "attachment;filename=IRS1099K_MonthlyReport_FiscalYear.xls");
                }
                Response.Charset = "";
                if (ddlReportType.SelectedIndex == 1)
                {
                    Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtWorkshop.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Monthly Reconciliation Report (Calendar Year) </td></tr>");
                }
                else if(ddlReportType.SelectedIndex==2)
                {
                    Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtWorkshop.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Monthly Reconciliation Report (Fiscal Year) </td></tr>");
                }
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'><td></td><td colspan=3>VisaMCDisc</td><td colspan=3>AmEx</td><td colspan=4>Total</td></tr>");
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtWorkshop.Columns)
                {
                                   
                    if (dc.ColumnName.Equals("VMDCount") || dc.ColumnName.Equals("AmExCount") || dc.ColumnName.Equals("TotalCount"))
                    {
                        Response.Write("<th>1099-K</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFCount") || dc.ColumnName.Equals("AmExNSFCount") || dc.ColumnName.Equals("TotalNSFCount"))
                    {
                        Response.Write("<th>NSF Charges</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFRefunds") || dc.ColumnName.Equals("AmExNSFRefunds") || dc.ColumnName.Equals("TotalNSFRefunds"))
                    {
                        Response.Write("<th>NSF Refunds</th>");
                    }
                    else
                    {
                        Response.Write("<th>" + dc.ColumnName + "</th>");
                    }
                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtWorkshop.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtWorkshop.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            else if (Session["AnnualReportRevenue"] != string.Empty )
            {

                //first table
                DataTable dtWorkshop = (DataTable)Session["AnnualReportRevenue"];

                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment;filename=IRS1099K_AnnualReport_CalendarYear.xls");
                Response.Charset = "";
                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtWorkshop.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Annual Reconciliation Report:Revenues (Calendar Year)</td></tr>");
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'><td></td><td colspan=3>VisaMCDisc</td><td colspan=3>AmEx</td><td colspan=4>Total</td></tr>");
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtWorkshop.Columns)
                {

                     if (dc.ColumnName.Equals("VMDCount") || dc.ColumnName.Equals("AmExCount") || dc.ColumnName.Equals("TotalCount"))
                    {
                        Response.Write("<th>1099-K</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFCount") || dc.ColumnName.Equals("AmExNSFCount") || dc.ColumnName.Equals("TotalNSFCount"))
                    {
                        Response.Write("<th>NSF Charges</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFRefunds") || dc.ColumnName.Equals("AmExNSFRefunds") || dc.ColumnName.Equals("TotalNSFRefunds"))
                    {
                        Response.Write("<th>NSF Refunds</th>");
                    }
                     else if (dc.ColumnName.Equals("Month"))
                    {
                        Response.Write("<th>Year</th>");
                    }  
                    else
                    {
                        Response.Write("<th>" + dc.ColumnName + "</th>");
                    }
                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtWorkshop.Rows)
                {
                    Response.Write("<tr>");                                            
                    for (int i = 0; i < dtWorkshop.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");

                //second table
                                                     
            }
            else if (Session["AnnualReportRevenueFiscal"] != string.Empty)
            {
                DataTable dtWorkshop = (DataTable)Session["AnnualReportRevenueFiscal"];

                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment;filename=IRS1099K_AnnualReport_FiscalYear.xls");
                Response.Charset = "";
                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtWorkshop.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Annual Reconciliation Report:Revenues (Fiscal Year)</td></tr>");
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'><td></td><td colspan=3>VisaMCDisc</td><td colspan=3>AmEx</td><td colspan=4>Total</td></tr>");
                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtWorkshop.Columns)
                {
                     if (dc.ColumnName.Equals("VMDCount") || dc.ColumnName.Equals("AmExCount") || dc.ColumnName.Equals("TotalCount"))
                    {
                        Response.Write("<th>1099-K</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFCount") || dc.ColumnName.Equals("AmExNSFCount") || dc.ColumnName.Equals("TotalNSFCount"))
                    {
                        Response.Write("<th>NSF Charges</th>");
                    }
                    else if (dc.ColumnName.Equals("VMDNSFRefunds") || dc.ColumnName.Equals("AmExNSFRefunds") || dc.ColumnName.Equals("TotalNSFRefunds"))
                    {
                        Response.Write("<th>NSF Refunds</th>");
                    }
                     else if (dc.ColumnName.Equals("Month"))
                    {
                        Response.Write("<th>Year</th>");
                    }
                    else
                    {
                        Response.Write("<th>" + dc.ColumnName + "</th>");
                    }
                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtWorkshop.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtWorkshop.Columns.Count; i++)
                    {

                        if (i == 0)
                        {
                            Response.Write("<td>" + dr[i] + "-" + ((Convert.ToInt32(dr[i]) + 1).ToString()).Substring(2,2) + "</td>");
                        }
                        else
                        {
                            Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                        }
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        catch (Exception ex)
        {
        }
    }


    
}