Imports System.Data.SqlClient
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data

Namespace VRegistration

    Partial Class MainChild
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lnkUpdate As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim cnTemp As SqlConnection

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ''Debug 
            'Session("CustIndID") = 4
            'Session("LoggedIn") = "True"
            'Session("CustSpouseID") = 5

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If

            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                If Session("EventID") Is Nothing Then
                    btnYes.Visible = False
                Else
                    If Session("EventID") = 3 Then
                        btnYes.Text = "Continue for Workshop Registration"
                    End If
                    If Session("EventID") = 4 Then
                        btnYes.Text = "Continue for Game Registration"
                    End If
                    If Session("EventID") = 13 Then
                        btnYes.Text = "Choose Coaching Details"
                    End If
                    If Session("EventID") = 19 Then
                        btnYes.Text = "Continue for Prep Club Registration"
                    End If
                    If Session("EventID") = 20 Then
                        btnYes.Text = "Continue for Online Workshop Registration"
                    End If
                End If
            End If

            '4/10/2008 - please do not comment this logic.
            'this is done to avoid issues related to pulling of unwanted children records.
            If Request.QueryString("Id") Is Nothing Then
                If Not (Session("CustIndID") Is Nothing) Then
                    If Len(Str(Session("CustIndID"))) <= 1 Then
                        Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                    End If
                Else
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
            Else
                If Len(Str(Request.QueryString("Id"))) <= 1 Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                Else
                    Session("CustIndID") = Request.QueryString("Id")
                End If
            End If

            cnTemp = New SqlConnection(Application("ConnectionString"))

            If Page.IsPostBack = False Then
                If Not (Session("CustIndID") Is Nothing) Then
                    hlinkParentRegistration.NavigateUrl = "~/DonorFunctions.aspx"
                    LoadGrid()
                End If
                If Session("entryToken").ToString.ToUpper = "DONOR" Then
                    hlinkParentRegistration.NavigateUrl = "~/DonorFunctions.aspx"
                ElseIf Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                    hlinkParentRegistration.NavigateUrl = "~/VolunteerFunctions.aspx"
                Else
                    hlinkParentRegistration.NavigateUrl = "~/UserFunctions.aspx"
                End If
                If Not Request.QueryString("Id") Is Nothing Then
                    lnkAdd.NavigateUrl = "AddChild.aspx?Id=" & Request.QueryString("Id")
                    hlnkSearch.Enabled = True
                    LoadGrid()
                End If
            End If
            If Not Request.QueryString("Id") Is Nothing Then
                hlnkSearch.Visible = True
            Else
                hlnkSearch.Visible = False
            End If
            Dim strMembers As String = ""
            Try
                strMembers = " select Convert(varchar,ind.automemberid) +','+ convert(varchar,case when ind.DonorType='IND' then isnull(sp.automemberid,'') else isnull(ind.relationship,'') end) from indspouse ind"
                strMembers = strMembers & " left join indspouse sp on ind.automemberid=sp.relationship  where ind.automemberid =" & Session("LoginID")
                strMembers = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strMembers)

                Dim iBlocked As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from blockList where memberid in (" & strMembers & ") and IsBlocked='Y'")
                If iBlocked > 0 Then
                    lblAlert.Text = "You filed for charge back, which created administrative nightmare to the volunteers. We want to avoid this. Please consult your chapter coordinator."
                    btnYes.Enabled = False
                End If
            Catch ex As Exception
            End Try

            Dim iCOIValid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from IndSpouse where CountryOfOrigin='IN' and AutoMemberId in (" & strMembers & ")")
            If iCOIValid = 0 Then
                Dim cmdText As String = "Select count(*) from IndSpouse I inner join Chapter C on C.ChapterId= I.ChapterId and C.NIO is null where AutoMemberId in (" & Session("CustIndID") & ")"
                Dim iEventId As Integer = Convert.ToInt32(Session("EventId"))
                If iEventId = 13 Or iEventId = 20 Then
                    cmdText = "select count(*) from Chapter where ChapterCode ='Coaching, US' and NIO is null"
                End If
                iCOIValid = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                If iCOIValid = 1 Then
                    lblAlert.Text = "Sorry! You are not eligible to register."
                    btnYes.Enabled = False
                End If
            End If
            'Dim iCountry As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from IndSpouse where CountryOfOrigin='IN' and AutoMemberId in (" & strMembers & ")")
            'If iCountry = 0 Then
            '    lblAlert.Text = "Sorry! You are not eligible to register."
            '    btnYes.Enabled = False
            'End If
        End Sub

        Private Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    If Not Request.QueryString("Id") Is Nothing Then
                        CType(e.Item.FindControl("hlUpdateChild"), HyperLink).NavigateUrl = "UpdateChild.aspx?ChildID=" & e.Item.DataItem("ChildNumber").ToString & "&ID=" & Request.QueryString("Id")
                    Else
                        CType(e.Item.FindControl("hlUpdateChild"), HyperLink).NavigateUrl = "UpdateChild.aspx?ChildID=" & e.Item.DataItem("ChildNumber").ToString
                    End If

                    If Not IsDBNull(e.Item.DataItem("ModifyDate")) Then
                        If IsDataModified(FormatDateTime(CType(e.Item.DataItem("ModifyDate"), Date), DateFormat.ShortDate)) = False Then
                            e.Item.FindControl("alertUpdate").Visible = True
                        Else
                            e.Item.FindControl("alertUpdate").Visible = False
                        End If
                    Else

                        If Not IsDBNull(e.Item.DataItem("CreateDate")) Then
                            If IsDataModified(FormatDateTime(CType(e.Item.DataItem("CreateDate"), Date), DateFormat.ShortDate)) = False Then
                                e.Item.FindControl("alertUpdate").Visible = True
                            Else
                                e.Item.FindControl("alertUpdate").Visible = False
                            End If
                        Else
                            e.Item.FindControl("alertUpdate").Visible = False
                        End If
                    End If
                    Select Case Session("EventID")
                        Case 13
                            ' validate if this child is registered for this year
                            Dim sqlStr As String = "select count(*) from CoachReg CR where CR.EventYear=" & Now.Year & " and CR.PMemberid=" & Session("CustIndID") & " and CR.ChildNumber=" & e.Item.DataItem("ChildNumber").ToString
                            Dim iRowCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
                            If iRowCnt <> 0 Then
                                e.Item.FindControl("alertUpdate").Visible = False

                            End If
                    End Select
            End Select
        End Sub

        Private Sub LoadGrid()
            Dim objChild As New Child
            Dim dsChild As New DataSet
            Dim strWhere As String
            Dim intChildUpdateCount As Integer, intChildCount As Integer, blnSuccess As Boolean
            blnSuccess = False
            strWhere = "MemberId='" & Session("CustIndID") & "' or SpouseID='" & Session("CustIndID") & "'"

            objChild.SearchChildWhere(cnTemp.ConnectionString, dsChild, strWhere)

            lblAlert.Text = "There are no children. Do you want to add child?"
            If Not dsChild Is Nothing Then
                If dsChild.Tables.Count > 0 Then
                    If dsChild.Tables(0).Rows.Count > 0 Then
                        blnSuccess = True
                        'this statement was added to avoid listing of unwanted children
                        If dsChild.Tables(0).Rows.Count > 25 Then
                            blnSuccess = False
                            If Session("CustIndID") > 0 Then
                                lblAlert.Text = "There are " & dsChild.Tables(0).Rows.Count & " children listed for this where clause " & strWhere & ".Please Report to NSF admin."
                            Else
                                lblAlert.Text = "Session Expired.Please login again to proceed."
                            End If

                        End If
                    End If
                End If
            End If

            If blnSuccess = True Then
                lblAlert.Text = ""
                dgChildList.DataSource = dsChild.Tables(0)
                dgChildList.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
                ViewState("ChildInfo") = dsChild
                intChildCount = dsChild.Tables(0).Rows.Count
                intChildUpdateCount = ChildInfoUpdate(dsChild)
                If intChildUpdateCount = intChildCount Then
                    btnYes.Enabled = True
                Else
                    If intChildUpdateCount = 0 Then
                        lblAlert.Text = "Update Child details before proceeding to the contest details."
                        btnYes.Enabled = False
                    Else
                        lblAlert.Text = "Some of the child information needs update. Make sure grade, date of birth, hobbies and achievements are correct and upto date."
                        btnYes.Enabled = False
                    End If
                    lblAlert.Visible = True
                End If
            Else
                dgChildList.Visible = False
                lblAlert.Visible = True
                btnYes.Visible = True
            End If
            If dgChildList.Items.Count = 0 Then
                btnYes.Text = "Continue"
                btnYes.Enabled = True
            End If
            If Session("entryToken").ToString.ToUpper = "PARENT" And Not Session("EventID") Is Nothing Then
                btnYes.Visible = True
            ElseIf dsChild Is Nothing Then
                lblAlert.Text = "Please add child."
                btnYes.Enabled = False
            ElseIf dsChild.Tables(0).Rows.Count = 0 Then

            Else
                lblAlert.Text = ""
                btnYes.Enabled = True
                btnYes.Visible = True
                btnYes.Text = "Continue"
            End If
        End Sub

        Private Function ChildInfoUpdate(ByVal DSResult As DataSet) As Integer
            Dim intCtr As Integer
            Dim dtModifyDate As Date
            Dim intUpdatedChildren As Integer

            For intCtr = 0 To DSResult.Tables(0).Rows.Count - 1
                'Declaring session variables for each child of the parent
                Session("Child" & intCtr + 1) = DSResult.Tables(0).Rows.Item(intCtr).Item(0)
                If Not IsDBNull(DSResult.Tables(0).Rows.Item(intCtr).Item("ModifyDate")) Then
                    dtModifyDate = DSResult.Tables(0).Rows.Item(intCtr).Item("ModifyDate")
                    Try
                        Select Case Session("EventID")
                            Case 13
                                ' validate if this child is registered for this year
                                Dim sqlStr As String = "select count(*) from CoachReg CR where CR.EventYear=" & Now.Year & " and CR.PMemberid=" & Session("CustIndID") & " and CR.ChildNumber=" & DSResult.Tables(0).Rows.Item(intCtr).Item(0)
                                Dim iRowCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
                                If iRowCnt <> 0 Then
                                    intUpdatedChildren = intUpdatedChildren + 1
                                    Continue For
                                End If
                        End Select
                        If IsDataModified(dtModifyDate) = True Then
                            intUpdatedChildren = intUpdatedChildren + 1
                        End If
                    Catch ex As Exception
                    End Try
                End If
            Next
            Return intUpdatedChildren
        End Function

        Private Function IsDataModified(ByVal dtCheck As Date) As Boolean
            Dim CurrentYear As Integer
            Dim CurrentMonth As Integer
            Dim strStartDate As String, dtStartdate As Date
            Dim strEndDate As String, dtEndDate As Date

            CurrentYear = Year(Today())
            CurrentMonth = Month(Today())

            'Updated on 09-01-2014 to allow update in child information[Grade] from Aug 1
            If CurrentMonth < 8 Then
                strStartDate = "8/1/" & (CurrentYear - 1)
                strEndDate = "7/31/" & CurrentYear
            Else
                strStartDate = "8/1/" & CurrentYear
                strEndDate = "7/31/" & (CurrentYear + 1)
            End If
            Try
                dtStartdate = FormatDateTime(strStartDate, DateFormat.ShortDate)
                dtEndDate = FormatDateTime(strEndDate, DateFormat.ShortDate)

                If ((dtCheck >= dtStartdate) And (dtCheck <= dtEndDate)) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return True
            End Try


        End Function

        Protected Sub dgChildList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgChildList.SelectedIndexChanged

        End Sub

        Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
            If Not Request.QueryString("Id") Is Nothing And Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                Response.Redirect("AddChild.aspx?Id=" & Request.QueryString("Id"))
            ElseIf Not Request.QueryString("Id") Is Nothing And Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("dbsearchresults.aspx")
            End If
            If dgChildList.Items.Count = 0 Then
                Response.Redirect("AddChild.aspx")
            End If
            If Not Session("PanelFrom") Is Nothing Then
                Dim PanelId As String = Session("PanelFrom").ToString
                If PanelId = "RegCont" Or PanelId = "Finals" Or PanelId = "Workshop" Or PanelId = "PrepClub" Or PanelId = "Games" Or PanelId = "OnlineCoaching" Or PanelId = "General" Or PanelId = "OnlineWorkhop" Then
                    If ValidateRecords(Session("LoginId").ToString) = True Then
                        Select Case Session("EventID")
                            Case 1
                                Response.Redirect("~/Parents/NationalInvitation.aspx")
                            Case 2
                                Response.Redirect("RegionalSummary.aspx")
                            Case 3
                                Response.Redirect("~/WorkshopSummary.aspx")
                            Case 4
                                Response.Redirect("AvailableGames.aspx")
                            Case 13
                                Dim eventid As Integer = EventIDCheck.GetEventIDFromDB(13)
                                If eventid = 13 Then
                                    Response.Redirect("CoachingSummary.aspx")
                                Else
                                    Response.Redirect("UserFunctions.aspx")
                                End If
                            Case 19
                                Response.Redirect("~/PrepClubSummary.aspx")
                            Case 20
                                Response.Redirect("~/OnlineWkshopSummary.aspx")
                            Case Else
                                Exit Sub
                        End Select
                    End If
                End If
            End If
        End Sub

        Protected Function ValidateRecords(ByVal MemberId As String) As Boolean
            Dim sqlStr As String = "SELECT A.[Email],A.[AutoMemberID],A.[Relationship],A.[DonorType],A.[FirstName],A.[LastName],A.[Address1],A.[City],A.[State],A.[Zip],A.[Gender],A.[Country],A.[Title],A.[CountryOfOrigin],A.[MaritalStatus],A.[VolunteerFlag] FROM [IndSpouse] A where A.AutoMemberID = " & MemberId & " or Relationship = " & MemberId & "  UNION SELECT A.[Email],A.[AutoMemberID],A.[Relationship],A.[DonorType],A.[FirstName],A.[LastName],A.[Address1],A.[City],A.[State],A.[Zip],A.[Gender],A.[Country],A.[Title],A.[CountryOfOrigin],A.[MaritalStatus],A.[VolunteerFlag] FROM [IndSpouse] A where A.AutoMemberID in (select Relationship from IndSpouse where AutoMemberID = " & MemberId & " or Relationship = " & MemberId & ")"
            Dim conn As String = Application("Connectionstring").ToString
            Dim ind_ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlStr)
            Dim E_status As Boolean = True
            Dim I_MemberId As String = String.Empty
            Dim I_Email As String = String.Empty
            Dim I_Title As String = String.Empty
            Dim I_Fname As String = String.Empty
            Dim I_Lname As String = String.Empty
            Dim I_Address1 As String = String.Empty
            Dim I_City As String = String.Empty
            Dim I_State As String = String.Empty
            Dim I_Zip As String = String.Empty
            Dim I_Country As String = String.Empty
            Dim I_Gender As String = String.Empty
            Dim I_Orign As String = String.Empty
            Dim I_Vflag As String = String.Empty
            Dim I_Marital As String = String.Empty
            Dim I_Spouse As String = String.Empty

            Dim S_MemberId As String = String.Empty
            Dim S_Email As String = String.Empty
            Dim S_Title As String = String.Empty
            Dim S_Fname As String = String.Empty
            Dim S_Lname As String = String.Empty
            Dim S_Address1 As String = String.Empty
            Dim S_City As String = String.Empty
            Dim S_State As String = String.Empty
            Dim S_Zip As String = String.Empty
            Dim S_Country As String = String.Empty
            Dim S_Gender As String = String.Empty
            Dim S_Orign As String = String.Empty
            Dim S_Vflag As String = String.Empty
            Dim S_Marital As String = String.Empty
            Dim S_Spouse As String = String.Empty

            For Each Row As DataRow In ind_ds.Tables(0).Rows
                If Row("DonorType").ToString = "IND" Then
                    I_MemberId = Row("AutomemberId").ToString()
                    I_Email = Row("Email").ToString()
                    I_Title = Row("Title").ToString()
                    I_Fname = Row("FirstName").ToString()
                    I_Lname = Row("LastName").ToString()
                    I_Address1 = Row("Address1").ToString()
                    I_City = Row("City").ToString()
                    I_State = Row("State").ToString()
                    I_Zip = Row("Zip").ToString()
                    I_Country = Row("Country").ToString()
                    I_Gender = Row("Gender").ToString()
                    I_Orign = Row("CountryOfOrigin").ToString()
                    I_Vflag = Row("VolunteerFlag").ToString()
                    I_Marital = Row("MaritalStatus").ToString()
                ElseIf Row("DonorType").ToString = "SPOUSE" Then
                    S_MemberId = Row("AutomemberId").ToString()
                    S_Email = Row("Email").ToString()
                    S_Title = Row("Title").ToString()
                    S_Fname = Row("FirstName").ToString()
                    S_Lname = Row("LastName").ToString()
                    S_Address1 = Row("Address1").ToString()
                    S_City = Row("City").ToString()
                    S_State = Row("State").ToString()
                    S_Zip = Row("Zip").ToString()
                    S_Country = Row("Country").ToString()
                    S_Gender = Row("Gender").ToString()
                    S_Orign = Row("CountryOfOrigin").ToString()
                    S_Vflag = Row("VolunteerFlag").ToString()
                    S_Marital = Row("MaritalStatus").ToString()
                End If
            Next
            If I_Email <> String.Empty Then
                If S_Email <> String.Empty Then
                    If I_Email = S_Email Then
                        If S_Marital.Trim() = "Widowed" And I_Marital.Trim() = "Deceased" Then
                            GoTo SkipStep
                        ElseIf S_Marital.Trim() = "Deceased" And I_Marital.Trim() = "Widowed" Then
                            GoTo SkipStep
                        Else
                            If S_Marital.Trim() = "Widowed" And I_Marital.Trim() = "Widowed" Then
                                lblAlert.Text = "Your family profile is not complete.  Please update your profile."
                                btnYes.Enabled = False
                            ElseIf S_Marital.Trim() = "Deceased" And I_Marital.Trim() = "Deceased" Then
                            End If
                            lblAlert.Text = "Your family profile is not complete.  Please update your profile."
                            btnYes.Enabled = False
                        End If
                    Else
                        Dim result As Integer = CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, "if not exists (select user_email from login_master where user_email ='" & S_Email & "')select 7 else select 1"))
                        Dim result1 As Integer = CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, "if not exists (select user_email from login_master where user_email ='" & I_Email & "')select 7 else select 1"))
                        If result = 7 Or result1 = 7 Then
                            If S_Marital.Trim() = "Widowed" And I_Marital.Trim() = "Deceased" Then
                                GoTo SkipStep
                            ElseIf S_Marital.Trim() = "Deceased" And I_Marital.Trim() = "Widowed" Then
                                GoTo SkipStep
                            Else
                                lblAlert.Text = "Your family profile is not complete.  Please update your profile."
                                btnYes.Enabled = False
                                E_status = False
                            End If
                        End If
                    End If
                Else
                    If S_Marital.Trim() = "Widowed" And I_Marital.Trim() = "Deceased" Then
                        GoTo SkipStep
                    ElseIf S_Marital.Trim() = "Deceased" And I_Marital.Trim() = "Widowed" Then
                        GoTo SkipStep
                    Else
                        If I_Marital = "Married" Or I_Marital = "Re-Married" Then
                            lblAlert.Text = "Your spouse profile is not complete.  Please complete it and return."
                            btnYes.Enabled = False
                            E_status = False
                        End If
                    End If

                End If
            Else
                If S_Email <> String.Empty Then
                    If S_Marital.Trim() = "Widowed" And I_Marital.Trim() = "Deceased" Then
                        GoTo SkipStep
                    ElseIf S_Marital.Trim() = "Deceased" And I_Marital.Trim() = "Widowed" Then
                        GoTo SkipStep
                    End If
                Else
                    lblAlert.Text = "Your family profile is not complete.  Please update your profile."
                    btnYes.Enabled = False
                    E_status = False
                End If
            End If
            If I_Marital.Trim() = "Married" Or I_Marital.Trim() = "Re-Married" Then
                Dim strFieldName As String = ""
                If S_Title.Trim() = "" Or S_Fname.Trim() = "" Or S_Lname.Trim() = "" Or S_Address1.Trim() = "" Or S_City.Trim() = "" Or S_State.Trim() = "" Or S_Zip.Trim() = "" Or S_Country.Trim() = "" Or S_Gender.Trim() = "" Or S_Orign.Trim() = "" Or S_Marital.Trim() = "" Then
                    lblAlert.Text = " Your spouse profile is incomplete.  Please complete it and return."
                    If S_Title.Trim() = "" Then
                        strFieldName = strFieldName + " Title"
                    End If
                    If S_Fname.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " FirstName"
                    End If
                    If S_Lname.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " LastName"
                    End If
                    If S_Address1.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Address"
                    End If
                    If S_City.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " City"
                    End If
                    If S_State.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " State"
                    End If
                    If S_Zip.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Zip"
                    End If
                    If S_Country.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Country"
                    End If
                    If S_Gender.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Gender"
                    End If
                    If S_Orign.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Origin"
                    End If

                    If S_Marital.Trim() = "" Then
                        If strFieldName.Length > 0 Then
                            strFieldName = strFieldName + " ,"
                        End If
                        strFieldName = strFieldName + " Marital"
                    End If
                    lblAlert.Text = "In your spouse profile, " & strFieldName & " is incomplete. Please complete it and return" ' " Your spouse profile is not available.  Please provide it and return."
                    btnYes.Enabled = False
                    E_status = False
                End If
                If S_Title.Trim() = "" And S_Fname.Trim() = "" And S_Lname.Trim() = "" And S_Address1.Trim() = "" And S_City.Trim() = "" And S_State.Trim() = "" And S_Zip.Trim() = "" And S_Country.Trim() = "" And S_Gender.Trim() = "" And S_Orign.Trim() = "" And S_Marital.Trim() = "" Then
                    lblAlert.Text = " Your spouse profile is not available.  Please provide it and return."
                    btnYes.Enabled = False
                    E_status = False
                End If
            End If
SkipStep:
            If I_Orign.Trim() <> "IN" And S_Orign.Trim() <> "IN" Then

                Dim iCOIValid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from IndSpouse I inner join Chapter C on C.ChapterId= I.ChapterId and C.NIO is null where AutoMemberId in (" & Session("CustIndID") & ")")
                If iCOIValid = 1 Then
                    lblAlert.Text = "We are sorry.  You are not eligible for participation."
                    btnYes.Enabled = False
                    E_status = False
                End If

            End If
            Return E_status
        End Function
    End Class

End Namespace
