Imports System
Imports System.Web
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization

Partial Class newslettersendmail
    Inherits System.Web.UI.Page
    Dim conn As New SqlConnection("Data Source=216.250.117.23; Initial Catalog=northsouth_prd; User ID='northsouth'; Password='Nhemi$SHemi';")
    Dim newslettername As String = "Newsletter/Newsletter_" & Now.Month & "_" & Now.Year & ".html"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        ElseIf (Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "43" Or Session("RoleId").ToString() = "90") Then
            lblRemaining.Text = "Emails to be send : " & SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(Email) from NewsletterEmails where flag='N'")
        Else
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack Then
            Dim file1 As New FileInfo(Server.MapPath(newslettername))
            txtSubject.Text = "NSF Newsletter " & Now.ToString("MMMM") & " " & Now.Year.ToString()
            If file1.Exists Then
                Dim re As StreamReader
                Dim emailBody As String = ""
                Dim subMail As String = txtSubject.Text
                re = File.OpenText(Server.MapPath(newslettername))
                ltlNewsletter.Text = re.ReadToEnd
                re.Close()
                btnSend.Enabled = True
                btnCheckEmail.Enabled = True
                btnLoad.Enabled = True
            Else
                btnSend.Enabled = False
                btnCheckEmail.Enabled = False
                btnLoad.Enabled = False
                lblRemaining.Text = "File not Found :" & Server.MapPath(newslettername).ToString()
            End If

        End If
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim re As StreamReader
        Dim emailBody As String = ""
        Dim subMail As String = txtSubject.Text
        re = File.OpenText(Server.MapPath(newslettername))
        emailBody = re.ReadToEnd
        re.Close()
        Dim i As Integer = 1
        Dim j As Integer = 1
        Dim StrSQL As String
        StrSQL = "select Email,ID from NewsletterEmails where flag='N'"
        Dim dsEmails As New DataSet
        Dim tblEmails() As String = {"Emails"}
        SqlHelper.FillDataset(conn, CommandType.Text, StrSQL, dsEmails, tblEmails)
        'lblemailstatus.Text = "Email Send To :"
        If dsEmails.Tables("Emails").Rows.Count > 0 Then
            lblBounced.Text = " Email Bounced for :" & " <br> "
            Dim n As Integer
            Dim SuccessmailIDs As String = "0"
            Dim failuremailIDs As String = "0"
            Dim count As Integer = 0
            For n = 0 To dsEmails.Tables("Emails").Rows.Count - 1
                count = count + 1
                If SendEmail(subMail, emailBody.ToString().Replace("##ID##", dsEmails.Tables(0).Rows(n)("Email").ToString()), dsEmails.Tables(0).Rows(n)("Email")) Then
                    'lblemailstatus.Text = lblemailstatus.Text & " <br> " & i & ". " & dsEmails.Tables(0).Rows(n)("Email")
                    SuccessmailIDs = SuccessmailIDs & "," & dsEmails.Tables(0).Rows(n)("ID")
                    i = i + 1
                Else
                    lblBounced.Text = lblBounced.Text & j & " " & dsEmails.Tables(0).Rows(n)("Email") & " <br> "
                    failuremailIDs = failuremailIDs & "," & dsEmails.Tables(0).Rows(n)("ID")
                    j = j + 1
                End If
                If n Mod 2500 = 0 And Not n = 0 Then
                    SendEmail("Newsletter Sending in Progress", "Mail sent to : " & n.ToString(), "ferdin448@gmail.com")
                    System.Threading.Thread.Sleep(10000) ' will pause for 10 seconds
                End If
                If (count = 500) Then  ' for every 500 emails sent
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='Y',ModifiedDate=Getdate() where ID in (" & SuccessmailIDs & ")")
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='F',ModifiedDate=Getdate() where ID in (" & failuremailIDs & ")")
                    SuccessmailIDs = "0"
                    failuremailIDs = "0"
                    count = 0
                ElseIf n = dsEmails.Tables("Emails").Rows.Count - 1 Then
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='Y',ModifiedDate=Getdate() where ID in (" & SuccessmailIDs & ")")
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='F',ModifiedDate=Getdate() where ID in (" & failuremailIDs & ")")
                    'SendEmail("Newsletter Sent all Receipients", "Mail sent to all : " & n.ToString(), "ferdin448@gmail.com")
                    'Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert(' " & i & "message sent ');"), True)
                End If
            Next n
            lblRemaining.Text = "Newsletter Successfully Send to " & i & " Emails"
            If j < 1 Then
                lblBounced.Text = ""
            End If
        Else
            lblRemaining.ForeColor = Color.Red
            lblRemaining.Text = "Please load Emails"
        End If
    End Sub

    Protected Sub btnCheckEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckEmail.Click
        If txttestEmail.Text.Length < 3 Then
            lblRemaining.Text = "Please Enter Email"
        Else
            Dim re As StreamReader
            Dim emailBody As String = ""
            Dim subMail As String = txtSubject.Text
            re = File.OpenText(Server.MapPath(newslettername))
            emailBody = re.ReadToEnd
            re.Close()
            If SendEmail(subMail, emailBody.ToString().Replace("##ID##", txttestEmail.Text), txttestEmail.Text) Then
                lblRemaining.Text = "Mail send to " & txttestEmail.Text
            Else
                lblRemaining.Text = "Mail not send to " & txttestEmail.Text & ". Please Check Email"
            End If

        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
        'Build Email Message

        Dim email As New MailMessage
        Dim ok As Boolean = True
        Try
            email.From = New MailAddress("nsfnewsl@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            client.Send(email)
        Catch e As Exception
            ' lblBounced.Text = lblBounced.Text & e.Message.ToString & " : "
            ok = False
        End Try
        Return ok
    End Function

    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        '***************************************************
        '***************** FERDINE SILVAA ******************
        '***************************************************
        Dim sql As String
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        sql = "update Sub set Sub.ValidEmailFlag ='N' FROM newsletter_subscribers sub Inner Join Indspouse I ON "
        sql = sql & " I.Email=Sub.email_address where (I.newsletter in ('1','3') OR I.ValidEmailFlag is Not NULL) and Sub.ValidEmailFlag is NUll; Insert into NewsletterEmails(Email,flag,CreateDate) "
        sql = sql & " Select t1.Email as Email,'N'  as Flag,Getdate() as CreateDate  from"
        sql = sql & "((select distinct LTrim(RTrim(email_address)) as Email from newsletter_subscribers  "
        sql = sql & " where ValidEmailFlag is NUll and email_address like '%@%') UNION"
        sql = sql & " select distinct LTrim(RTrim(email)) as Email from indspouse where email not in (select email_address from newsletter_subscribers)"
        sql = sql & " and email like '%@%' AND ValidEmailFlag is Null AND (newsletter not in ('1','3') OR (Newsletter is null)) and deletedflag = 'No')  t1 Group by t1.Email"
        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql)
        Dim count As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(Email) from NewsletterEmails where flag='N'")
        lblRemaining.Text = "Emails to be send : " & count
    End Sub
End Class


