Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Imports System.Net.Mail
Imports System.IO
Imports System.Diagnostics
Imports System


Partial Class Parents_RegionalSummary
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Session("MultipleChapterC") = False
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")
                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                Try
                    lblAddress2.Text = drIndSpouse.Item("Address2")
                Catch ex As Exception

                End Try
                lblCity.Text = drIndSpouse.Item("City") & " , " & drIndSpouse.Item("State") & " " & drIndSpouse.Item("Zip")
                lblChapter.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ChapterCode From Chapter Where ChapterID=" & drIndSpouse.Item("ChapterID"))

                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")
            End If
        End If

        Dim objChild As New Child
        Dim dsChild As New DataSet

        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")

        If dsChild.Tables.Count > 0 Then
            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Rows.Count
            ViewState("ChildInfo") = dsChild
        End If

        If hdnFlag.Value = "false" Then
            Dim Cstcnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ContestID)  from Contest C Inner Join IndSpouse I On C.NSFChapterID = I.ChapterID AND C.Contest_Year = " & Session("EventYear") & " AND C.EventId = " & Session("EventID") & " AND I.AutoMemberID = " & intIndID)
            If Cstcnt = 0 Then
                lblContestInfo.Text = "<b>There are no contests yet offered in your center.  Please consult your chapter coordinator. " & _
                                      "If you proceed and register at another center, it is your responsibility to follow through and participate " & _
                                      " at that center on the date(s) at the venue available at the time. " & _
                                      "No refund will be given under any circumstances.</b> "
            Else
                lblContestInfo.Text = "<b> Your child(ren) is/are not eligible for contests.</b> "
            End If
            lblContestInfo.Visible = True
            btnRegister.Enabled = False
        Else
            lblContestInfo.Visible = False
        End If

    End Sub

    Protected Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                'Dim dsContest As DataSet

                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cmd As New SqlCommand
                Dim dsInvitees As New DataSet
                Dim da As New SqlDataAdapter

                cmd.Connection = conn
                conn.Open()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "usp_GetEligibleContests"
                cmd.Parameters.Add(New SqlParameter("@ChildNumber", e.Item.DataItem("ChildNumber")))
                cmd.Parameters.Add(New SqlParameter("@ContestYear", Session("EventYear")))
                cmd.Parameters.Add(New SqlParameter("@ContestTypeID", Session("EventID")))
                cmd.Parameters.Add(New SqlParameter("@IndID", Session("CustIndID")))

                da.SelectCommand = cmd
                da.Fill(dsInvitees)
                Dim dv As New DataView
                If dsInvitees.Tables(0).Rows.Count > 0 Then
                    hdnFlag.Value = "true"
                Else
                    Exit Sub
                End If

                'fetch number of chapters
                Dim ChapterCodes As String = ""
                Dim ChapterCodeNos As Integer = 0

                Dim ChapterC(20) As String
                Dim ContestNos As Integer = 0

                Dim Contests(20) As String
                Dim sbContest As New StringBuilder, strContestDesc As String
                strContestDesc = ""
                sbContest.Append("<table cellpadding='2px' cellspacing='0px' border='1px'><tr><td Align='center' class='SmallFont'  style='color:#990000;font-weight:bold'>Eligible Contests</td>")

                'No of Columns - Chapters
                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    If InStr(ChapterCodes, dr.Item("City")) <= 0 Then
                        ChapterCodeNos = ChapterCodeNos + 1
                        ChapterCodes = ChapterCodes & ", " & dr.Item("City")
                        ChapterC(ChapterCodeNos) = dr.Item("City")
                        sbContest.Append("<td Align='center' class='SmallFont' style='Color:#990000;font-weight:bold'>" & dr.Item("City") & "</td>")
                    End If
                Next
                sbContest.Append("</tr>")


                'No of Rows - Contest
                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    If strContestDesc <> dr.Item("ContestDesc") Then
                        ContestNos = ContestNos + 1
                        Contests(ContestNos) = dr.Item("ContestDesc")
                        strContestDesc = dr.Item("ContestDesc")
                    End If
                Next

                'Initialing the Matrix
                Dim Matrix(ContestNos + 1, ChapterCodeNos + 1) As String
                Dim i, j As Integer
                For i = 0 To ContestNos
                    For j = 0 To ChapterCodeNos
                        Matrix(i, j) = ""
                    Next
                Next


                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    Matrix(Array.IndexOf(Contests, dr.Item("ContestDesc")), 0) = dr.Item("ContestDesc")
                    'Response.Write(Array.IndexOf(Contests, dr.Item("ContestDesc")) & "*" & Array.IndexOf(ChapterC, dr.Item("City")) & " = " & dr.Item("ContestDate") & "<br>")
                    Matrix(Array.IndexOf(Contests, dr.Item("ContestDesc")), Array.IndexOf(ChapterC, dr.Item("City"))) = dr.Item("ContestDate")
                    Matrix(0, Array.IndexOf(ChapterC, dr.Item("City"))) = dr.Item("City")
                Next

                For i = 1 To ContestNos
                    sbContest.Append("<tr>")
                    For j = 0 To ChapterCodeNos
                        If j = 0 Then
                            sbContest.Append("<td width='200px'  class='SmallFont'>" & Matrix(i, j) & "</td>")
                        Else
                            sbContest.Append("<td  class='SmallFont'>" & IIf(Matrix(i, j).ToString.Length <= 1, " &nbsp; ", Matrix(i, j)) & "</td>")
                        End If
                    Next
                    sbContest.Append("</tr>")
                Next
                sbContest.Append("</table>")

                CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString
                'Dim sbCity As New StringBuilder, strCity As String
                ' strCity = ""
                'sbCity.Append("<ul>")
                Dim cnt As Integer = 0
                Dim Chapters As String = ""
                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    'If InStr(strCity, dr.Item("City")) <= 0 Then
                    '    sbCity.Append("<li>" & dr.Item("City") & "</li>")
                    '    strCity = strCity & ", " & dr.Item("City")
                    'End If
                    If InStr(Chapters, dr.Item("NSFChapterID")) <= 0 Then
                        cnt = SqlHelper.ExecuteScalar(conn, Data.CommandType.Text, "SELECT  COUNT(*)  FROM  Volunteer  where RoleID = 5  and Chapterid in(" & dr.Item("NSFChapterID") & ")GROUP BY RoleID,chapterid having Count(*)>1")
                        If cnt > 0 Then
                            Session("MultipleChapterC") = True
                            Chapters = Chapters & " " & dr.Item("NSFChapterID")
                        End If
                    End If
                Next
                'sbCity.Append("</ul>")
                'CType(e.Item.FindControl("lblLocation"), Label).Text = sbCity.ToString
                If Session("MultipleChapterC") = True Then
                    TblContestInfo.Visible = True
                    btnRegister.Text = "Click here to Exit"
                    'mail Sending Program
                    Dim SMailFrom As String
                    Dim sMailTo As String
                    Dim sSubject As String
                    Dim sBody As String
                    SMailFrom = "nsfcontests@gmail.com"
                    sMailTo = "nsfcontests@gmail.com"
                    sSubject = "More Than one ChapterC found In Chapter " & Chapters
                    Dim mm As New MailMessage(SMailFrom, sMailTo)
                    mm.Subject = sSubject


                    sBody = "More than one chapter coordinator was found in chapter " & Chapters & ". Kindly escalate this issue so that the problem is rectified and the parents are able to register. "


                    mm.Body = sBody

                    '(3) Create the SmtpClient object
                    Dim client As New SmtpClient()

                    '(4) Send the MailMessage (will use the Web.config settings)
                    'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")



                    Try
                        client.Send(mm)
                    Catch ex As Exception
                        ' MsgBox(ex.ToString)
                    End Try

                End If

        End Select
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        If Session("MultipleChapterC") = True Then
            Response.Redirect("http://www.northsouth.org")
        Else
            Response.Redirect("~/ContestantRegistration.aspx")
        End If
    End Sub
End Class
