﻿
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


    Partial Class Parents_FinalsInvitee_Decline
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                Response.Redirect("..\maintest.aspx")
            End If
        End Sub

        Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim strsql As String

            If Not ddlreason.SelectedValue = 0 Then
                Try
                    If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(*) from Contestant where NationalInvitee =1  and parentid=" & Session("CustIndID") & " and ContestYear=YEAR(getdate()) ") < 1 Then
                        lblerr.Text = "You have no record to update"
                    ElseIf SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(*) from Contestant where NationalInvitee =1  and parentid=" & Session("CustIndID") & " and invite_decline_comments is  Null and ContestYear=YEAR(getdate()) ") > 0 Then
                        strsql = "update Contestant set invite_decline_comments='" & ddlreason.SelectedItem.Text & "' where parentid=" & Session("CustIndID") & " and ContestYear=YEAR(getdate())  and invite_decline_comments is Null and NationalInvitee=1"
                        SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, strsql)
                        lblerr.Text = "Decline Reason Registered"
                    Else
                        lblerr.Text = "Already registered decline"
                    End If
                Catch ex As Exception
                    lblerr.Text = ex.ToString()
                End Try
            Else
                lblerr.Text = "Please select a Reason"
            End If
        End Sub
    End Class

End Namespace

