﻿<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="vwOnlineWrkShopCal.aspx.vb" Inherits="vwOnlineWrkShopCal" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <div style="text-align:left">
     
     <table width="100%"><tr><td><asp:LinkButton ID="lbtnUserFunctions" CssClass="btn_02" PostBackUrl="~/UserFunctions.aspx" runat="server">Back to Parent Functions</asp:LinkButton></td>
 
     </tr></table> 
      
       
       <table>
           <tr>
               <td><div style="font-size:26px; font-weight:bold ; font-family:Calibri;color: rgb(73, 177, 23);" align="center">
                   Online Workshop Calendar</div>

               </td>
           </tr>
           <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
            <td style="width:991px" align="center" class="ContentSubTitle">
 <center><asp:label id="errmsg" runat="server"></asp:label></center>
               <asp:DataGrid ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="OnlineWSCalID"
                  Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Inset"
                  BorderColor="#336666" Visible="False">
                  <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C" ></SelectedItemStyle>
                  <ItemStyle ForeColor="Black" BackColor="#EEEEEE" ></ItemStyle>
                  <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false"></HeaderStyle>
                  <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>                  
                  <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                  <Columns>
                    <asp:BoundColumn  DataField="OnlineWSCalID" Visible="false" readonly="true" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventFeesID" Visible="False" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventYear" HeaderText="EventYear" Readonly="true" HeaderStyle-ForeColor="White" Visible="true" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventID" HeaderText="Event"  Readonly="true" HeaderStyle-ForeColor="White" Visible="false" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="ProductGroupID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="ProductID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                                   <asp:TemplateColumn HeaderText="ProductCode">
                        <ItemTemplate >
                           <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                        </ItemTemplate>                        
                        <HeaderStyle ForeColor="White" Font-Bold="True" />
                        <ItemStyle />
                     </asp:TemplateColumn >
                     <asp:TemplateColumn HeaderText="Event Date">
                        <ItemTemplate>
                           <asp:Label ID="lblEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>                        
                        <HeaderStyle  ForeColor="White" Font-Bold="True" />
                        <ItemStyle />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn  HeaderText="Time   EST">
                        <ItemTemplate>
                           <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>' ></asp:Label>
                        </ItemTemplate>
                        
                        <HeaderStyle  ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn  HeaderText="Duration Hours">
                        <ItemTemplate>
                           <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>' ></asp:Label>
                        </ItemTemplate>
                        
                        <HeaderStyle  ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle   width="10%"/>
                     </asp:TemplateColumn>
                     <asp:BoundColumn DataField="TeacherID" HeaderText="TeacherID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                     <asp:TemplateColumn  HeaderText="Team Lead">
                        <ItemTemplate>
                           <asp:Label ID="lblTeacher" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Teacher")%>' ></asp:Label>
                        </ItemTemplate>
                       
                        <HeaderStyle   ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle  />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Registration Deadline">
                        <ItemTemplate >
                          <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                       
                        <HeaderStyle  ForeColor="White" Font-Bold="True" />
                        <ItemStyle width="10%"/>
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="LateFee Deadline">
                        <ItemTemplate>
                           <asp:Label ID="lblLateFeeDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                        
                        <HeaderStyle  ForeColor="White" Font-Bold="True" />
                        <ItemStyle  width="10%"/>
                     </asp:TemplateColumn>
                   
                  </Columns>
               </asp:DataGrid>
               &nbsp;
            </td>
         </tr>
      </table>


       </div> 
     </asp:Content>