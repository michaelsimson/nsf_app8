Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration
    Partial Class PrepClubRegistrationStatus
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                Server.Transfer("../login.aspx?entry=" & Session("entryToken"))
            End If
            Dim redirectURL As String
            redirectURL = Request.ApplicationPath & "/UserFunctions.aspx"
            hlinkParentRegistration.NavigateUrl = redirectURL

            '***************************************************
            '***Get IndID and SpouseID for the givn Logon Person
            '***************************************************
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim StrIndSpouse As String = ""
            Dim intIndID As Integer = 0
            Dim dsIndSpouse As New DataSet

            StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

            Dim objIndSpouse As New IndSpouse10
            objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

            If dsIndSpouse.Tables.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                        End If
                    Else
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        Else
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                        End If
                    End If
                    Session("CustIndID") = intIndID
                End If
            End If

            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Registration"}

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@MemberID"
            prmArray(0).Value = Session("CustIndID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@EventYear"
            prmArray(1).Value = Application("ContestYear")
            prmArray(1).Direction = ParameterDirection.Input
            'Future
            ' SqlHelper.ExecuteNonQuery(connContest, CommandType.StoredProcedure, "usp_DeletePendingPrepClub", prmArray)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetPrepClubStatusFuture", prmArray)
            If ds.Tables(0).Rows.Count > 0 Then
                Table3.Visible = True
                dgSelectedContestsFuture.DataSource = ds.Tables(0)
                dgSelectedContestsFuture.DataBind()
                lblCommentFuture.Text = "<br><b>Future Events</b>"
            Else
                Table3.Visible = False
            End If

            'Past Events
            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetPrepClubStatus", dsContestant, tblConestant, prmArray)

            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If
            'Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now
            If dsContestant.Tables.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                Session("ContestsSelected") = ""
                dgSelectedContests.DataBind()
                If Not dsContestant.Tables(0).Rows.Count > 0 Then
                    lblComment.Text = "No registration data is available to view."
                Else
                    lblComment.Text = "<br><br><b> Past Events </b>"
                End If
                dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
                'dgSelectedContests.Visible = True
            End If
        End Sub

        Private Sub dgSelectedContestsFuture_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContestsFuture.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info " & _
                                                                            "<BR> Fee:" & e.Item.DataItem("Fee").ToString & _
                                                                            "<BR> Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") & _
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
            End Select
        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info " & _
                                                                            "<BR> Fee:" & e.Item.DataItem("Fee").ToString & _
                                                                            "<BR> Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") & _
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
            End Select
        End Sub
    End Class
End Namespace


