Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL

Partial Class Parents_NationalInvitation
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        'Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        ''If dsIndSpouse.Tables.Count > 0 Then
        ''    If dsIndSpouse.Tables(0).Rows.Count > 0 Then
        ''        If dsIndSpouse.Tables(0).Rows.Count > 1 Then
        ''            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
        ''                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
        ''            ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
        ''                intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
        ''            End If
        ''            lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
        ''            " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

        ''        Else
        ''            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
        ''                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
        ''            Else
        ''                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
        ''            End If
        ''            lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
        ''        End If
        ''        Session("IndID") = intIndID
        ''        Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
        ''        Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

        ''        '**************************
        ''        '*** Spouse Info Capturing
        ''        '**************************
        ''        Dim StrSpouse As String = ""
        ''        Dim intSpouseID As Integer = 0
        ''        Dim dsSpouse As New DataSet
        ''        StrSpouse = "Relationship='" & Session("IndID") & "'"


        ''        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
        ''        If dsSpouse.Tables.Count > 0 Then
        ''            If dsSpouse.Tables(0).Rows.Count > 0 Then
        ''                intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
        ''            End If
        ''        End If

        ''        Session("SpouseID") = intSpouseID

        '********************************************************
        '*** Populate Parent Info on the Page
        '********************************************************
        Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

        lblParentName.Text = drIndSpouse.Item("FirstName") & " " & drIndSpouse.Item("LastName")
        lblAddress1.Text = drIndSpouse.Item("Address1")
        lblAddress2.Text = drIndSpouse.Item("Address2")
        If drIndSpouse.Item("HPhone").ToString <> "" Then
            lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
        Else
            lblHomePhone.Text = "Home Phone Not Provided"
        End If
        If drIndSpouse.Item("CPhone").ToString <> "" Then
            lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
        Else
            lblCellPhone.Text = "Cell Phone Not Provided"
        End If
        If drIndSpouse.Item("WPhone").ToString <> "" Then
            lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
        Else
            lblWorkPhone.Text = "Work Phone Not Provided"
        End If
        lblEMail.Text = drIndSpouse.Item("EMail")


        Dim objChild As New Child
        Dim dsChild As New DataSet

        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & Session("CustIndID") & "'")

        If dsChild.Tables.Count > 0 Then
            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Rows.Count
            ViewState("ChildInfo") = dsChild
        End If

        Dim PriorityDeadline As String
        PriorityDeadline = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select CONVERT(VARCHAR(10), PriorityDeadline, 101) as PriorityDeadline From WeekCalendar Where EventID=1 and GETDATE()< PriorityDeadline")
        If PriorityDeadline <> "" Then
            lblPriority.Text = "Registration is currently open only for invitees on the Priority List.  Registration for invitees on the Wait List will be open after " & PriorityDeadline & "."
        Else
            lblPriority.Text = ""
        End If
    End Sub

    Protected Sub OldPage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If

        Dim objChild As New Child
        Dim dsChild As New DataSet

        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")
        'objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & Session("CustIndID") & "'")

        If dsChild.Tables.Count > 0 Then
            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Rows.Count
            ViewState("ChildInfo") = dsChild
        End If
    End Sub

    Private Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                'Dim dsContest As DataSet

                '   Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim blnNationalInvitee As Boolean = False
                Dim cmd As New SqlCommand
                Dim dsInvitees As New DataSet, dsAdditionalContests As New DataSet
                Dim da As New SqlDataAdapter

                cmd.Connection = cnTemp
                If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
                cmd.CommandType = CommandType.Text
                cmd.CommandText = " SELECT ContestDesc FROM contestcategory WHERE " & _
                                  " contestyear = " & Session("EventYear") & _
                                  " and contestcategoryid in (	Select C2.ContestCategoryID " & _
                                  " from Contestant  C1 " & _
                                  " Inner Join	Contest C2 on C2.ContestID = C1.ContestCode " & _
                                  " where	C1.ChildNumber = " & e.Item.DataItem("ChildNumber") & _
                                  " and C1.ContestYear = " & Session("EventYear") & _
                                  " and C1.ParentID = " & Session("CustIndID") & " and C1.NationalInvitee = 1 " & _
                                  " and NationalFinalsStatus = 'ACTIVE' )"
                da.SelectCommand = cmd
                da.Fill(dsInvitees)

                Dim sbContest As New StringBuilder
                sbContest.Append("<ul>")
                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    blnNationalInvitee = True
                    sbContest.Append("<li>" & dr.Item("ContestDesc") & "</li>")
                Next


                cmd.CommandText = " SELECT ContestDesc FROM contestcategory WHERE " & _
                                      " contestyear = " & Application("ContestYear") & _
                                      " and NationalSelectionCriteria = 'O' " & _
                                      " and (Select Grade from Child  " & _
                                      " where childnumber = " & e.Item.DataItem("ChildNumber") & ") " & _
                                      " between GradeFrom and GradeTo "
                da.SelectCommand = cmd
                da.Fill(dsAdditionalContests)

                For Each dr As DataRow In dsAdditionalContests.Tables(0).Rows
                    sbContest.Append("<li>" & dr.Item("ContestDesc") & "</li>")
                Next
                sbContest.Append("</ul>")

                CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString
        End Select
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Response.Redirect("~/ContestantRegistration.aspx")
    End Sub
End Class
