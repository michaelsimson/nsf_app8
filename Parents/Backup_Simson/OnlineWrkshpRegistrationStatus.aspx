<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="VRegistration.OnlineWrkshpRegistrationStatus" CodeFile="OnlineWrkshpRegistrationStatus.aspx.vb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
			<table cellspacing="1" width="100%" cellpadding="1" border="0">				
					<tr><td width="20%"><asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" runat="server" NavigateUrl="UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink> </td>
						<td class="Heading" align="left"><span style="margin-left:160px"> Online Workshop Status</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table id="Table3" visible="false" runat = "server" cellSpacing="1" cellPadding="1" width="100%" border="0" >
								<tr>
									<td><asp:label id="lblCommentFuture" runat="server" CssClass="largewordingbold"></asp:label></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3">
									<asp:datagrid id="dgSelectedContestsFuture" runat="server" CssClass="mediumwording" Font-Size="X-Small"
											AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
											BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
											<FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
											<AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
											<HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="OnlineWSCalId"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
												<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Workshop">
												<HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Workshop Date">
													<HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "EventDate") %>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Workshop Time">
													<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Teacher">
													<HeaderStyle Width="200px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "Teacher")%>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Payment Info">
													<HeaderStyle Width="300px"></HeaderStyle>
													<ItemTemplate>
														<asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>														
                                                        <br />
                                                        <asp:Label ID="lblDW" runat="server" ForeColor="Red"  Visible="false" CssClass="SmallFont" Text="Work Shop Book is not yet available. Please try in a few days."></asp:Label>	
                                                        <asp:HyperLink id="hlDownloadLink" runat="server" Visible="False" Target="_blank" >Click here to download <%# DataBinder.Eval(Container.DataItem, "EventDesc") %> Workshop book</asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
                                              	<asp:BoundColumn DataField="Approved" Visible="false" ></asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                        <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
										</asp:datagrid></td>
								</tr>
							</table>							
							</td>
							</tr>
					<tr>
						<td colspan="2">
							<table id="Table2" cellspacing="1" cellpadding="1" width="100%" border="0" >
								<tr>
									<td><asp:label id="lblComment" runat="server" CssClass="largewordingbold"></asp:label></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3">
									<asp:datagrid id="dgSelectedContests" runat="server" CssClass="mediumwording" Font-Size="X-Small"
											AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
											BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
											<FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
											<AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
											<HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
												<%--<asp:BoundColumn Visible="False" DataField="ECalendarID"></asp:BoundColumn>--%>
												<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
												<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Workshop">
												<HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Workshop Date">
													<HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "EventDate") %>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Workshop Time">
													<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
													</ItemTemplate>
												</asp:TemplateColumn>												
												<asp:TemplateColumn HeaderText="Teacher">
													<HeaderStyle Width="200px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "Teacher")%>
													</ItemTemplate>
												</asp:TemplateColumn>
												
												<asp:TemplateColumn HeaderText="Payment Info">
													<HeaderStyle Width="300px"></HeaderStyle>
													<ItemTemplate>
														<asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>		
                                                        <br />
                                                         <asp:Label ID="lblDW" ForeColor="Red"  runat="server" Visible="false"  CssClass="SmallFont" Text="Work Shop Book is not yet available. Please try in a few days."></asp:Label>	
                                                        <asp:HyperLink id="hlDownloadLink" runat="server" Visible="False" Target="_blank" >Click here to download <%# DataBinder.Eval(Container.DataItem, "EventDesc") %> Workshop book</asp:HyperLink>
																									
													</ItemTemplate>
												</asp:TemplateColumn>
                                                	<asp:BoundColumn DataField="Approved" Visible="false" ></asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                        <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
										</asp:datagrid></td>
								</tr>
								<tr>
									<td colspan="3">
									    <b style="color:red">Important Instructions:</b>
                                        <ol style="margin-top:1px;">
<li>Work Shop Book can be downloaded 10 days before the Work Shop Date and until 10 days after.</li>
<li>The book has an expiry date. We suggest that you print a hard copy as soon as you download. After the expiry date (normally after 30 days), you will not be able to open the pdf file.</li>
<li>The book can be opened only through Copy Safe PDF Reader which can be installed free. An executable file is also attached in the download.  You only need to click on the .exe file once and install. </li>
</ol></td>
								</tr>
							</table>							
							</td>
							</tr>							 
							</table>							
							
		</div>	
		</asp:Content>					
							
							

 
 