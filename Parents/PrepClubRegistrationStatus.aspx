<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="VRegistration.PrepClubRegistrationStatus" CodeFile="PrepClubRegistrationStatus.aspx.vb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
		<table id="Table1" cellSpacing="1" width="100%" cellPadding="1" border="0">
			<tr><td class="Heading" align="center">PrepClub Status</td></tr>
			<tr><td>
				<table id="Table3" visible="false" runat = "server" cellSpacing="1" cellPadding="1" width="100%" border="0" >
					<tr>
							<td><asp:label id="lblCommentFuture" runat="server" CssClass="largewordingbold"></asp:label></td>
							<td></td>
							<td></td>
					</tr>
					<tr>
							<td colspan="3">
							<asp:datagrid id="dgSelectedContestsFuture" runat="server" CssClass="mediumwording" Font-Size="X-Small"
									AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
									BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
									<FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
									<AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
									<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
									<HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="PrepClubCalID"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
										<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="PrepClub">
										<HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
											</ItemTemplate>
										</asp:TemplateColumn>
										
										<asp:TemplateColumn HeaderText="PrepClub Date">
											<HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem, "EventDate") %>
											</ItemTemplate>
										</asp:TemplateColumn>
										
										<%--<asp:TemplateColumn HeaderText="PrepClub Time">
											<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
											</ItemTemplate>
										</asp:TemplateColumn>--%>
										
										<asp:TemplateColumn HeaderText="Contact Info">
											<HeaderStyle Width="200px"></HeaderStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem, "ContactInfo") %>
											</ItemTemplate>
										</asp:TemplateColumn>
										
										<asp:TemplateColumn HeaderText="Payment Info">
											<HeaderStyle Width="300px"></HeaderStyle>
											<ItemTemplate>
												<asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>														
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
								</asp:datagrid>
								</td>
							</tr>
				</table>
			</td>
			</tr>
			<tr>
					<td>
						<table id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0" >
								<tr>
									<td><asp:label id="lblComment" runat="server" CssClass="largewordingbold"></asp:label></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td colSpan="3">
									<asp:datagrid id="dgSelectedContests" runat="server" CssClass="mediumwording" Font-Size="X-Small"
											AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
											BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
											<FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
											<AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
											<HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="PrepClubCalID"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
												<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="PrepClub">
												<HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
													</ItemTemplate>
												</asp:TemplateColumn>
												
												<asp:TemplateColumn HeaderText="PrepClub Date">
													<HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "EventDate") %>
													</ItemTemplate>
												</asp:TemplateColumn>
												
												<%--<asp:TemplateColumn HeaderText="PrepClub Time">
													<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
													</ItemTemplate>
												</asp:TemplateColumn>--%>
												
												<asp:TemplateColumn HeaderText="Contact Info">
													<HeaderStyle Width="200px"></HeaderStyle>
													<ItemTemplate>
														<%# DataBinder.Eval(Container.DataItem, "ContactInfo") %>
													</ItemTemplate>
												</asp:TemplateColumn>
												
												<asp:TemplateColumn HeaderText="Payment Info">
													<HeaderStyle Width="300px"></HeaderStyle>
													<ItemTemplate>
														<asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>														
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                        <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
										</asp:datagrid></td>
								</tr>
						</table>
					</td>
			</tr>
			<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"><asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" runat="server" NavigateUrl="UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
			</tr>
	    </table>							
	</div>	
</asp:Content>					
							
							

 
 