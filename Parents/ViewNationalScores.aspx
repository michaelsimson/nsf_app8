<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.ViewNationalScores" Trace="false" CodeFile="ViewNationalScores.aspx.vb" MasterPageFile="~/NSFInnerMasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
			<table cellpadding="1px" border ="0px" cellspacing ="0" width="950px">
			<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"><asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
					</td>
				</tr>
				<tr>
					<td class="ContentHeading" align="center">Bee Scores
					</td>
				</tr>
				<TR>
					<TD colSpan="3" style="height:350px" align="center" >
						<asp:DataGrid id="dgSelectedContests" runat="server" CssClass="mediumwording" BackColor="#DEBA84"
							BorderWidth="1px" BorderStyle="None" BorderColor="#DEBA84" CellPadding="3" AutoGenerateColumns="False"
							Font-Size="X-Small" CellSpacing="2">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ContestID"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
								<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Contest">
									<HeaderStyle Width="300px"></HeaderStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem, "ContestCode") %>
										<BR>
										<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Date">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem, "ContestDate") %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Info">
									<HeaderStyle Width="300px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblScore" runat="server"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
						</asp:DataGrid></TD>
				</TR>
				
			</table>
</asp:content>
 

 
 
 