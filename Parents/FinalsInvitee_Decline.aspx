<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="FinalsInvitee_Decline.aspx.vb" Inherits="VRegistration.Parents_FinalsInvitee_Decline" title="Parents FinalsInvitee Decline" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    <br /><br />
    <div>
			<TABLE id="Table1" width="750px" cellSpacing="0" cellPadding="3" border="0">
				
					<tr>
						<td align="left" style="width:30%"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="../UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
					</tr>
					<tr><td class="Heading" align="center">Decline Finals Invitation</td></tr>
					<tr>
						<td align="center">
						<br /><br />
						<table border="0" cellpadding="5" cellspacing="0">
						<tr>
						<td>Declining Invitation</td><td>
                            <asp:DropDownList ID="ddlreason" runat="server" Width="200px">
                                <asp:ListItem Value="0" Selected="True">Select Reason</asp:ListItem>
                                <asp:ListItem Value="1">Going to India</asp:ListItem>
                                <asp:ListItem Value="2">Inconvenient time</asp:ListItem>
                                <asp:ListItem Value="3">Inconvenient venue</asp:ListItem>
                                <asp:ListItem Value="4">Expensive to attend</asp:ListItem>
                                <asp:ListItem Value="5">Not Interested</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                        </td></tr>
						<tr>
						<td colspan="2" align="center">
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" /></td></tr>
						<tr>
						<td colspan="2" align="center">
                            <asp:Label ID="lblerr" runat="server" Text=""></asp:Label>
                            </td></tr>
						</table>
						</td>
							</tr>
							
							</TABLE>
		</div>	
		</asp:Content>					
							
							

 
 