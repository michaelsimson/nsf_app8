<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="VRegistration.OnlineWrkshpRegistrationStatus" CodeFile="OnlineWrkshpRegistrationStatus.aspx.vb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="../Scripts/jquery-1.9.1.js"></script>
        <script language="javascript" type="text/javascript">
            function JoinMeeting() {

                var workshopDate = document.getElementById("<%=hdnWorkshopDate.ClientID%>").value;
                var url = "";
                if (workshopDate == "May 18, 2016") {

                    url = "https://zoom.us/j/363103674";
                } else if (workshopDate == "May 22, 2016") {
                    url = "https://zoom.us/j/787524546";
                }
                window.open(url, '_blank');
            }
            function StartMeeting() {
                JoinMeeting();
            }
            function showAlert() {
                alert("Workshop attendees can only join their workshop up to 30 minutes before workshop time");
            }
        </script>
    </div>
    <div>
        <table cellspacing="1" width="100%" cellpadding="1" border="0">
            <tr>
                <td width="20%">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkParentRegistration" runat="server" NavigateUrl="UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink>
                </td>
                <td class="Heading" align="left"><span style="margin-left: 160px">Online Workshop Status</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="Table3" visible="false" runat="server" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblCommentFuture" runat="server" CssClass="largewordingbold"></asp:Label></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataGrid ID="dgSelectedContestsFuture" runat="server" CssClass="mediumwording" Font-Size="X-Small"
                                    AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
                                    BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None" OnItemCommand="dgSelectedContestsFuture_ItemCommand">
                                    <FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
                                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
                                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                                    <HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="OnlineWSCalId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContestantName" HeaderText="Attendee"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Workshop">
                                            <HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Workshop Date">
                                            <HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "EventDate") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Workshop Time">
                                            <HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%> EST
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Teacher">
                                            <HeaderStyle Width="200px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "Teacher")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Payment Info">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>
                                                <br />
                                                <asp:Label ID="lblDW" runat="server" ForeColor="Red" Visible="false" CssClass="SmallFont" Text="Work Shop Book is not yet available. Please try in a few days."></asp:Label>
                                                <asp:HyperLink ID="hlDownloadLink" runat="server" Visible="False" Target="_blank">Click here to download <%# DataBinder.Eval(Container.DataItem, "EventDesc") %> Workshop book</asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Approved" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Web Conf Link">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemStyle BackColor="Yellow" />
                                            <ItemTemplate>
                                                <div style="display: none;">
                                                    <asp:Label ID="lblWebinarURL" runat="server" ForeColor="Red" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "JoinURL") %>'></asp:Label>

                                                    <asp:Label ID="lblTime" runat="server" ForeColor="Red" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "CheckInTime") %>'></asp:Label>
                                                    <asp:Label ID="lblDate" runat="server" ForeColor="Red" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "EventDate") %>'></asp:Label>
                                                </div>
                                                <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Webinar" CommandName="Join" />
                                                <span runat="server" id="divWeb" style="display: none;"><a href='<%# DataBinder.Eval(Container.DataItem, "WebinarLink") %>' target="_blank">Click here</a> and provide your NSF registered email address and full name to receive Webinar joining instructions via email
                                                </span>
                                                <%--<asp:Button ID="btnRegWebinar" CommandName="RegWebinar" runat="server" Text="Register for Webinar" Enabled="false"/>--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Recording Link">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemStyle BackColor="Yellow" />
                                            <ItemTemplate>
                                                <div style="display: none;">
                                                    <asp:Label ID="lblRecordingURL" runat="server" ForeColor="Red" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "RecordingLink") %>'></asp:Label>
                                                </div>
                                                <asp:HyperLink ID="hlRecLink" runat="server" Text="Click Here" Enabled="false" Target="_blank" NavigateUrl='<%#  DataBinder.Eval(Container.DataItem, "RecordingLink")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                    <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                </asp:DataGrid></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="Table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblComment" runat="server" CssClass="largewordingbold"></asp:Label></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataGrid ID="dgSelectedContests" runat="server" CssClass="mediumwording" Font-Size="X-Small"
                                    AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
                                    BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
                                    <FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
                                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
                                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                                    <HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
                                        <%--<asp:BoundColumn Visible="False" DataField="ECalendarID"></asp:BoundColumn>--%>
                                        <asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContestantName" HeaderText="Attendee"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Workshop">
                                            <HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Workshop Date">
                                            <HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "EventDate") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Workshop Time">
                                            <HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Teacher">
                                            <HeaderStyle Width="200px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "Teacher")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Payment Info">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>
                                                <br />
                                                <asp:Label ID="lblDW" ForeColor="Red" runat="server" Visible="false" CssClass="SmallFont" Text="Work Shop Book is not yet available. Please try in a few days."></asp:Label>
                                                <asp:HyperLink ID="hlDownloadLink" runat="server" Visible="False" Target="_blank">Click here to download <%# DataBinder.Eval(Container.DataItem, "EventDesc") %> Workshop book</asp:HyperLink>

                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Approved" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Recording Link">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemStyle BackColor="Yellow" />
                                            <ItemTemplate>
                                                <div style="display: none;">
                                                    <asp:Label ID="lblRecordingURL" runat="server" ForeColor="Red" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "RecordingLink") %>'></asp:Label>
                                                </div>
                                                <asp:HyperLink ID="hlRecLink" runat="server" Text="Click Here" Enabled="false" Target="_blank" NavigateUrl='<%#  DataBinder.Eval(Container.DataItem, "RecordingLink")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                    <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                </asp:DataGrid></td>
                        </tr>
                        <%--   <tr>
                            <td colspan="3">
                                <b style="color: red">Important Instructions:</b>
                                <ol style="margin-top: 1px;">
                                    <li>Work Shop Book can be downloaded a few days before the Work Shop Date.</li>
                                    <li>The book has an expiry date. We suggest that you print a hard copy as soon as you download. After the expiry date (normally after 30 days), you will not be able to open the pdf file.</li>
                                    <li>The book can be opened only through Copy Safe PDF Reader which can be installed free. An executable file is also attached in the download.  You only need to click on the .exe file once and install. </li>
                                </ol>
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
        </table>

    </div>
    <input type="hidden" id="hdnWorkshopDate" runat="server" value="0" />
</asp:Content>




