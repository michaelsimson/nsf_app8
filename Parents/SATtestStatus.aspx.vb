Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


    Partial Class SATtestStatus
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If
            Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center> Name</td><td align=Center>Attended</td></tr>"
            Dim tableExist As String = ""
            Dim flag As Boolean = False
            Dim reader As SqlDataReader
            Try

            
                reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select CT.ChildNumber,Ch.First_Name+' '+Ch.Last_Name as Name from ChildTestAnswers CT INNER JOIN Child ch ON Ch.ChildNumber=CT.ChildNumber INNER JOIN IndSpouse I ON I.AutoMemberID=Ch.MemberID AND I.AutoMemberID=" & Session("CustIndID") & " Group By CT.ChildNumber,CT.ChildNumber,Ch.First_Name+' '+Ch.Last_Name Having COUNT(CT.ChildTestAnswersID)>124")
                While reader.Read()
                    flag = True
                    TableStr = TableStr & "<tr><td align=Center>" & reader("Name") & "</td><td align=Center> Yes </td>"
                End While
                TableStr = TableStr & "</table>"
                reader.Close()
            Catch ex As Exception

            End Try
            If flag = False Then
                TableStr = "No record to show"
            End If
            lbl.Text = TableStr
        End Sub

    End Class

End Namespace

