Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


Partial Class RegistrationStatus
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If

            Session("EventID") = Request.QueryString("EventID")

            ' **************** Set Eventyear * *****************
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@EventID", Session("EventID"))

            If Now.Month = 12 Then 'Since Registration starts by December
                Session("EventYear") = Now.Year + 1 'SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
            Else
                Session("EventYear") = Now.Year
            End If
            Dim redirectURL As String

            redirectURL = Request.ApplicationPath & "/UserFunctions.aspx"

            hlinkParentRegistration.NavigateUrl = redirectURL


            '***************************************************
            '***Get IndID and SpouseID for the givn Logon Person
            '***************************************************
            Dim StrIndSpouse As String = ""
            Dim intIndID As Integer = 0
            Dim dsIndSpouse As New DataSet

            StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

            Dim objIndSpouse As New IndSpouse10
            objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

            If dsIndSpouse.Tables.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                        End If
                    Else
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        Else
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                        End If
                    End If
                    Session("CustIndID") = intIndID
                End If
            End If

            Dim connContest As New SqlConnection(Application("ConnectionString"))
            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ParentID"
            prmArray(0).Value = Session("CustIndID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = Session("EventYear")
            prmArray(1).Direction = ParameterDirection.Input
            Select Case Session("EventID").ToString
                Case "1"
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)
                Case "2"
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)
            End Select

            ' Sandhya - don't why it is here. this duplicates the record. SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)

            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If
            'Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now
            If dsContestant.Tables.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                Session("ContestsSelected") = ""
                dgSelectedContests.DataBind()
                If Not dsContestant.Tables(0).Rows.Count > 0 Then
                    lblComment.Text = "No Contests Registered at this time."
                End If
                dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
                CheckChapterConf()
            End If
            If Session("EventID").ToString = "1" Then
                Dim dsMeals As New DataSet
                Dim tblMeals() As String = {"Contestant"}

                Dim mealsArray(2) As SqlParameter

                mealsArray(0) = New SqlParameter
                mealsArray(0).ParameterName = "@ParentID"
                mealsArray(0).Value = Session("CustIndID")
                mealsArray(0).Direction = ParameterDirection.Input

                mealsArray(1) = New SqlParameter
                mealsArray(1).ParameterName = "@ContestYear"
                mealsArray(1).Value = Session("EventYear")
                mealsArray(1).Direction = ParameterDirection.Input
                SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetMealsChargeByParentID", dsMeals, tblMeals, mealsArray)

                If dsMeals.Tables.Count > 0 Then
                    If dsMeals.Tables(0).Rows.Count > 0 Then
                        dgMeals.DataSource = dsMeals.Tables(0)
                        dgMeals.DataBind()
                    Else
                        dgMeals.Visible = False
                    End If
                Else
                    tblMealsRegister.Visible = False
                End If
            Else
                tblMealsRegister.Visible = False
            End If
            connContest = Nothing
            If Session("EventID").ToString = "1" Then
                dgSelectedContests.Columns(7).Visible = False
            Else
                dgSelectedContests.Columns(9).Visible = False
            End If

        End Sub
        Private Sub CheckChapterConf()
            Dim dgItem As DataGridItem
            Dim dgItem1 As DataGridItem
            Dim LblChapterID As Label
            Dim LblChapterID_1 As Label
            Dim LbContestDate As Label
            Dim LbContestDate_1 As Label
            Dim DupliFlag As Boolean = False
            Dim ChapFlag As Boolean = False
            'Dim ParentChapterID As Integer = Session("ChapterID")
            If dgSelectedContests.Items.Count > 0 Then
                For Each dgItem In dgSelectedContests.Items
                    LblChapterID = dgItem.FindControl("lblChapterCode")
                    LbContestDate = dgItem.FindControl("lblContestDate")
                    For Each dgItem1 In dgSelectedContests.Items
                        If dgItem.ItemIndex <> dgItem1.ItemIndex Then
                            LblChapterID_1 = dgItem1.FindControl("lblChapterCode")
                            LbContestDate_1 = dgItem1.FindControl("lblContestDate")
                            If LbContestDate_1.Text.Equals(LbContestDate.Text) And Not LblChapterID_1.Text.Equals(LblChapterID.Text) Then
                                DupliFlag = True
                            End If
                        End If
                    Next
                    If DupliFlag = True Then
                        TrConfMsg.Visible = True
                    Else
                        TrConfMsg.Visible = False
                    End If
                Next
            End If
        End Sub
        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    If Session("EventID").ToString = "1" Then
                        '*****************************************
                        '*** Make sure the Contestant Photo is available  to enable the download link
                        '*****************************************
                        If Not IsDBNull(e.Item.DataItem("photo_image")) Or SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN DATEDIFF(d,RegistrationDeadline,Getdate()) > 10 THEN 1 Else 0 END from contest where contestid=" & e.Item.DataItem("contestID") & "") = 1 Then
                            If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                                'Dim linkURL As String = Request.ApplicationPath & "/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                                Dim linkURL As String = Request.ApplicationPath & "/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                                ' Dim linkDOCURL As String = Request.ApplicationPath & "/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                                Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                                Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                                If ((Not (hlnk) Is Nothing) _
                                            AndAlso (linkURL <> "")) Then
                                    If e.Item.DataItem("PaymentReference").ToString <> "" Then
                                        hlnk.Visible = True
                                        hlnk.NavigateUrl = linkURL
                                        hlnk.Target = "downloadmateriallink"
                                        'hDOClnk.Visible = True
                                        'hDOClnk.NavigateUrl = linkDOCURL
                                        'hDOClnk.Target = "downloadmateriallink"
                                    End If

                                    Dim date3, date4 As DateTime
                                    date3 = DateTime.Now
                                    date4 = New DateTime(date3.Year, 9, 7)

                                    'after sep 7 should be disabled
                                    If DateTime.Compare(date3, date4) > 0 And date3.Year = e.Item.DataItem("ContestYear") Then
                                        hlnk.Visible = False
                                        hDOClnk.Visible = False
                                    End If
                                    'we dont have docs ..so commenting
                                    hDOClnk.Visible = False
                                End If
                            End If
                        Else
                            Dim lbl As Label = CType(e.Item.FindControl("lblDownload"), Label)
                            lbl.Text = "Download Link will not be visible until the contestant photo is uploaded."
                            lbl.Visible = True
                        End If
                    Else
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                            Dim linkURL As String = Request.ApplicationPath & "/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                            Dim linkDOCURL As String = Request.ApplicationPath & "/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")
                            
                            Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                            Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)
                            Dim date1, date2 As DateTime
                            date1 = DateTime.Now
                            date2 = New DateTime(date1.Year, 7, 31)

                            If ((Not (hlnk) Is Nothing) _
                                        AndAlso (linkURL <> "")) Then
                                hlnk.Visible = True
                                hlnk.NavigateUrl = linkURL
                                hlnk.Target = "downloadmateriallink"
                                'hDOClnk.Visible = True
                                'hDOClnk.NavigateUrl = linkDOCURL
                                'hDOClnk.Target = "downloadmateriallink"

                                If e.Item.DataItem("PaymentReference").ToString <> "" Then
                                    Select Case Mid(e.Item.DataItem("ContestAbbr").ToString, 1, 2)
                                        Case "MB"  'math contest
                                            hlnk.Text = "Click here to download Math Bee Sample Question Paper."
                                            hlnk.Visible = True
                                        Case "SG", "JG"   'geography contest
                                            hlnk.Text = "Click here to download Geography Bee Sample Question Paper"
                                            hlnk.Visible = True
                                        Case Else  'take default value
                                            hlnk.Visible = True
                                    End Select
                                    'If Mid(e.Item.DataItem("ContestAbbr").ToString, 1, 2) = "MB" Then
                                    '    hlnk.Text = "Click here to download Math Bee Sample Question Paper."
                                    '    hDOClnk.Visible = False
                                    'Else
                                    '    hDOClnk.Text = "Click here to download practice words as Word Document."
                                    'End If
                                    'hlnk.Visible = True
                                Else
                                    hlnk.Visible = False
                                    hDOClnk.Visible = False
                                End If
                                'after May 20, download links should be disabled
                                If DateTime.Compare(date1, date2) > 0 And date1.Year = e.Item.DataItem("ContestYear") Then
                                    hlnk.Visible = False
                                    hDOClnk.Visible = False
                                End If

                                'we dont have docs ..so commenting
                                hDOClnk.Visible = False
                            End If
                        End If
                    End If
                    Dim param(2) As SqlParameter
                    param(0) = New SqlParameter("@ChapterID", e.Item.DataItem("ChapterID"))
                    param(1) = New SqlParameter("@ContestYear", Session("EventYear"))

                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.StoredProcedure, "usp_GetVolunteerForChapterID", param)
                    While ds.Tables(0).Rows.Count > 0
                        CType(e.Item.FindControl("lblContactInfo"), Label).Text = ds.Tables(0).Rows(0)(0).ToString() & " " & ds.Tables(0).Rows(0)(1).ToString() & "<br>" & ds.Tables(0).Rows(0)(2).ToString() & " <br> " & ds.Tables(0).Rows(0)(3).ToString()
                        Exit While
                    End While

                    'Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, "Select Tshirtsize From ContestantPhoto Where MemberId=" & Session("CustIndID") & "")

                    'While ds1.Tables(0).Rows.Count > 0
                    '    CType(e.Item.FindControl("lblTShirtSize"), Label).Text = ds1.Tables(0).Rows(0)(0).ToString()
                    '    Exit While
                    'End While

                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info " & _
                                                                            "<BR> Fee:" & e.Item.DataItem("Fee").ToString & _
                                                                            "<BR> Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") & _
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
                    If e.Item.DataItem("PaymentDate").ToString = "" Then
                        CType(e.Item.FindControl("lblPaymentInfo"), Label).ForeColor = Color.Red
                        'e.Item.BackColor = Color.Red
                    End If


            End Select
        End Sub

    Private Sub dgMeals_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                FormatCurrency(e.Item.DataItem("Amount"))
                If IsDate(e.Item.DataItem("ContestDate")) Then Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString()
                If IsDate(e.Item.DataItem("PaymentDate")) Then Convert.ToDateTime(e.Item.DataItem("PaymentDate")).ToShortDateString()
        End Select
    End Sub
End Class

End Namespace

