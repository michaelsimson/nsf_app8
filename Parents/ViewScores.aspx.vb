Imports System.Text
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL


Namespace VRegistration

Partial Class ViewScores
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ' Get the section.
            'Dim customErrorsSection As CustomErrorsSection = CType(Configuration.GetSection("system.web/customErrors"), CustomErrorsSection)
            'Dim currentDefaultRedirect As String = customErrorsSection.DefaultRedirect

            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                Server.Transfer("~/Login.aspx?entry=" & Session("entryToken"))
            End If

            Dim redirectURL As String
	    redirectURL = Request.ApplicationPath & "/UserFunctions.aspx"
            hlinkParentRegistration.NavigateUrl = redirectURL

            DisplayContests()
    End Sub

    Private Sub DisplayContests()


        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

            If Session("CustIndID") = 0 Then
                StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

                Dim objIndSpouse As New IndSpouse10
                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

                If dsIndSpouse.Tables.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                            End If
                        Else
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            Else
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                            End If
                        End If
                        Session("CustIndID") = intIndID
                    End If
                End If
            End If


            If Session("CustIndID") = 0 Then
                Server.Transfer("~/Login.aspx?entry=" & Session("entryToken"))
            End If



            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}

            Dim prmArrayDelete(3) As SqlParameter
            prmArrayDelete(0) = New SqlParameter
            prmArrayDelete(0).ParameterName = "@ParentID"
            prmArrayDelete(0).Value = Session("CustIndID")
            prmArrayDelete(0).Direction = ParameterDirection.Input

            prmArrayDelete(1) = New SqlParameter
            prmArrayDelete(1).ParameterName = "@ContestYear"
            prmArrayDelete(1).Value = Now.Year
            prmArrayDelete(1).Direction = ParameterDirection.Input

            prmArrayDelete(2) = New SqlParameter
            prmArrayDelete(2).ParameterName = "@LateFeeDate"
            prmArrayDelete(2).Value = IIf(Application("ContestType") = "1", Application("LateRegistrationDate"), DBNull.Value)
            prmArrayDelete(2).Direction = ParameterDirection.Input
            Dim prmArrayGet(2) As SqlParameter
            prmArrayGet(0) = New SqlParameter
            prmArrayGet(0).ParameterName = "@ParentID"
            prmArrayGet(0).Value = Session("CustIndID")
            prmArrayGet(0).Direction = ParameterDirection.Input

            prmArrayGet(1) = New SqlParameter
            prmArrayGet(1).ParameterName = "@ContestYear"
            prmArrayGet(1).Value = Now.Year
            prmArrayGet(1).Direction = ParameterDirection.Input

            Select Case ConfigurationManager.AppSettings("EventID").ToString
                Case "1"
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArrayGet)
                Case "2"
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArrayGet)
            End Select


            If dsContestant.Tables.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                Session("ContestsSelected") = ""
                dgSelectedContests.DataBind()
                dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
            End If

            connContest = Nothing
    End Sub


    Private Sub dgSelectedContests_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
                Dim pdt As Object = DataBinder.Eval(e.Item.DataItem, "PaymentDate")
                'CType(e.Item.FindControl("lblContact"), Label).Text = e.Item.DataItem("ChapterCity") & ", " & _
                '                                                        e.Item.DataItem("ChapterState") & _
                '                                                        "<BR> Coordinator: " & e.Item.DataItem("CoordinatorName")
                'if payment date is not available it is considered as unpaid
                If Not IsDBNull(pdt) Then
                    'enable the remove contest option for unpaid entries
                    Dim contestCode As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestID"), String)
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select  count(contestant_ID) as ContestCnt,Max(Score1+Score2+ISNULL(score3,0)) as  maxScore from contestant where contestID=" & contestCode & " AND (Score1+Score2+ISNULL(score3,0)) is Not Null and (Score1+Score2+ISNULL(score3,0)) > 0")
                        Dim ContestAbbr As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestAbbr"), String)
                    Dim ChildNumber As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
                    Dim contestCategoryID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ContestCategoryID"), Integer)
                    If ViewState("SelectedContests") Is Nothing Then
                        ViewState("SelectedContests") = New ArrayList
                    End If
                    CType(ViewState("SelectedContests"), ArrayList).Add(ChildNumber.ToString & contestCategoryID.ToString)
                    Dim score1 As Double = 0
                    Dim score2 As Double = 0
                    Dim score3 As Double = 0
                    Dim rank As Integer = 0
                    Dim finalscore1 As Double = 0
                    Dim finalscore2 As Double = 0
                    Dim finalpercentile As Decimal = 0
                    Dim finalrank As Integer = 0
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score1")) Then
                        score1 = CType(DataBinder.Eval(e.Item.DataItem, "Score1"), Double)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score2")) Then
                        score2 = CType(DataBinder.Eval(e.Item.DataItem, "Score2"), Double)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score3")) Then
                        score3 = CType(DataBinder.Eval(e.Item.DataItem, "Score3"), Double)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Rank")) Then
                        rank = CType(DataBinder.Eval(e.Item.DataItem, "Rank"), Integer)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1")) Then
                        finalscore1 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1"), Double)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2")) Then
                        finalscore2 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2"), Double)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Rank")) Then
                        finalrank = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Rank"), Integer)
                    End If
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile")) Then
                        finalpercentile = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile"), Decimal)
                    End If
                    Dim lblScore As Label = CType(e.Item.FindControl("lblScore"), Label)
                    If (score1 _
                                + (score2 _
                                + (score3 > 0))) Then
                        Dim sb As StringBuilder = New StringBuilder
                            sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=2>")
                        sb.Append("<tr bgcolor=lightblue forecolor=white>")
                        sb.Append("<td width=20%><b>Scores</b></td>")
                            sb.Append("<td width=20%>Phase_1</td>")
                            sb.Append("<td width=20%>Phase_2</td>")
                            sb.Append("<td width=20%>Phase_3</td>")
                        sb.Append("<td width=20%>Total</td>")
                        sb.Append("<td width=20%>Percentile</td>")
                            sb.Append("<td width=20%>Rank</td>")
                            sb.Append("<td width=20%>#_of_contestants</td>")
                            sb.Append("<td width=20%>Highest_Score</td>")
                        sb.Append("</tr>")
                        sb.Append("<tr>")
                            sb.Append("<td width=20% align=center>Regionals</td>")
                        If (score1 > 0) Then
                                sb.Append(("<td width=20% align=center>" _
                                            + (score1.ToString + "</td>")))
                        Else
                                sb.Append("<td width=20% align=center>na</td>")
                        End If
                        If (score2 > 0) Then
                                sb.Append(("<td width=20% align=center>" _
                                            + (score2.ToString + "</td>")))
                        Else
                                sb.Append("<td width=20% align=center>na</td>")
                        End If
                        If (score3 > 0) Then
                                sb.Append(("<td width=20% align=center>" _
                                            + (score3.ToString + "</td>")))
                        Else
                                sb.Append("<td width=20% align=center>na</td>")
                        End If
                        If (score1 _
                                    + (score2 _
                                    + (score3 > 0))) Then
                                sb.Append(("<td width=20% align=center>" _
                                            + ((score1 _
                                            + (score2 + score3)).ToString + "</td>")))
                        Else
                                sb.Append("<td width=20% align=center>na</td>")
                        End If
                            sb.Append("<td width=20% align=center>na</td>")
                        'no percentile for regionals
                        If (rank > 0) Then
                                sb.Append(("<td width=20% align=center>" _
                                            + (rank.ToString + "</td>")))
                        Else
                                sb.Append("<td width=20% align=center>na</td>")
                            End If
                            sb.Append("<td width=20% align=center>" & ds.Tables(0).Rows(0)(0).ToString() & "</td>")
                            sb.Append("<td width=20% align=center>" & ds.Tables(0).Rows(0)(1).ToString() & "</td>")
                        sb.Append("</tr>")
                        If (finalscore1 _
                                    + (finalscore2 > 0)) Then
                            sb.Append("<tr>")
                            sb.Append("<td width=20%>Finals</td>")
                            If (finalscore1 > 0) Then
                                sb.Append(("<td width=20%>" _
                                                + (finalscore1.ToString + "</td>")))
                            Else
                                sb.Append("<td width=20%>na</td>")
                            End If
                            If (finalscore2 > 0) Then
                                sb.Append(("<td width=20%>" _
                                                + (finalscore2.ToString + "</td>")))
                            Else
                                sb.Append("<td width=20%>na</td>")
                            End If
                            sb.Append("<td width=20%>na</td>")
                            If (finalscore1 _
                                        + (finalscore2 > 0)) Then
                                sb.Append(("<td width=20%>" _
                                                + ((finalscore1 + finalscore2).ToString + "</td>")))
                            Else
                                sb.Append("<td width=20%>na</td>")
                            End If
                            If (finalpercentile > 0) Then
                                sb.Append(("<td width=20%>" _
                                                + (finalpercentile.ToString + "</td>")))
                            Else
                                sb.Append("<td width=20%>na</td>")
                            End If
                            If (finalrank > 0) Then
                                sb.Append(("<td width=20%>" _
                                                + (finalrank.ToString + "</td>")))
                            Else
                                sb.Append("<td width=20%>na</td>")
                                End If
                            sb.Append("</tr>")
                        End If
                        sb.Append("</table>")
                        lblScore.Text = sb.ToString
                    End If
                End If
        End Select
    End Sub

End Class

End Namespace

