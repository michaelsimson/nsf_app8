Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


    Partial Class WrkshpRegistrationStatus
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                Server.Transfer("../login.aspx?entry=" & Session("entryToken"))
            End If
            Dim redirectURL As String
            redirectURL = Request.ApplicationPath & "/UserFunctions.aspx"
            hlinkParentRegistration.NavigateUrl = redirectURL

            '***************************************************
            '***Get IndID and SpouseID for the givn Logon Person
            '***************************************************
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim StrIndSpouse As String = ""
            Dim intIndID As Integer = 0
            Dim dsIndSpouse As New DataSet

            StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

            Dim objIndSpouse As New IndSpouse10
            objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

            If dsIndSpouse.Tables.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                        End If
                    Else
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        Else
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                        End If
                    End If
                    Session("CustIndID") = intIndID
                End If
            End If

            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Registration"}

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@MemberID"
            prmArray(0).Value = Session("CustIndID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@EventYear"
            prmArray(1).Value = Application("ContestYear")
            prmArray(1).Direction = ParameterDirection.Input
            'Future
           ' SqlHelper.ExecuteNonQuery(connContest, CommandType.StoredProcedure, "usp_DeletePendingWorkshop", prmArray)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetWorkShopStatusFuture", prmArray)
            If ds.Tables(0).Rows.Count > 0 Then
                Table3.Visible = True
                dgSelectedContestsFuture.DataSource = ds.Tables(0)
                dgSelectedContestsFuture.DataBind()
                lblCommentFuture.Text = "<br><b>Future Events</b>"
            Else
                Table3.Visible = False
            End If

            'Past Events
            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetWorkShopStatus", dsContestant, tblConestant, prmArray)

            ' Sandhya - don't why it is here. this duplicates the record. SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)

            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If
            'Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now
            If dsContestant.Tables.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                Session("ContestsSelected") = ""
                dgSelectedContests.DataBind()
                If Not dsContestant.Tables(0).Rows.Count > 0 Then
                    lblComment.Text = "No registration data is available to view."
                Else
                    lblComment.Text = "<br><br><b> Past Events </b>"
                End If
                '############ SILVAAAAAAAA ###########
                dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
                'dgSelectedContests.Visible = True
            End If

            
            'If Session("EventID").ToString = "1" Then
            '    Dim dsMeals As New DataSet
            '    Dim tblMeals() As String = {"Contestant"}

            '    Dim mealsArray(2) As SqlParameter

            '    mealsArray(0) = New SqlParameter
            '    mealsArray(0).ParameterName = "@ParentID"
            '    mealsArray(0).Value = Session("CustIndID")
            '    mealsArray(0).Direction = ParameterDirection.Input

            '    mealsArray(1) = New SqlParameter
            '    mealsArray(1).ParameterName = "@ContestYear"
            '    mealsArray(1).Value = Application("ContestYear")
            '    mealsArray(1).Direction = ParameterDirection.Input
            '    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetMealsChargeByParentID", dsMeals, tblMeals, mealsArray)

            '    If dsMeals.Tables.Count > 0 Then
            '        If dsMeals.Tables(0).Rows.Count > 0 Then
            '            dgMeals.DataSource = dsMeals.Tables(0)
            '            dgMeals.DataBind()
            '        Else
            '            dgMeals.Visible = False
            '        End If
            '    Else
            '        tblMealsRegister.Visible = False
            '    End If
            'Else
            '    tblMealsRegister.Visible = False
            'End If
            'connContest = Nothing


        End Sub

        Private Sub dgSelectedContestsFuture_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContestsFuture.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info " & _
                                                                            "<BR> Fee:" & e.Item.DataItem("Fee").ToString & _
                                                                            "<BR> Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") & _
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
            End Select
        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    '            If Session("EventID").ToString = "1" Then
                    '                '*****************************************
                    '                '*** Make sure the Contestant Photo is available  to enable the download link
                    '                '*****************************************
                    '                If Not IsDBNull(e.Item.DataItem("photo_image")) Then
                    '                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                    '                        Dim linkURL As String = Request.ApplicationPath & "/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                    '                        ' Dim linkDOCURL As String = Request.ApplicationPath & "/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                    '                        Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                    '                        Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                    '                        If ((Not (hlnk) Is Nothing) _
                    '                                    AndAlso (linkURL <> "")) Then
                    '                            If e.Item.DataItem("PaymentReference").ToString <> "" Then
                    '                                hlnk.Visible = True
                    '                                hlnk.NavigateUrl = linkURL
                    '                                hlnk.Target = "dParents/RegistrationStatus.aspx?ParentUpdate=True&EventID=2ownloadmateriallink"
                    '                                'hDOClnk.Visible = True
                    '                                'hDOClnk.NavigateUrl = linkDOCURL
                    '                                'hDOClnk.Target = "downloadmateriallink"
                    '                            End If

                    '                            Dim date3, date4 As DateTime
                    '                            date3 = DateTime.Now
                    '                            date4 = New DateTime(date3.Year, 9, 7)

                    '                            'after sep 7 should be disabled
                    '                            If DateTime.Compare(date3, date4) > 0 Then
                    '                                hlnk.Visible = False
                    '                                hDOClnk.Visible = False
                    '                            End If
                    '                            'we dont have docs ..so commenting
                    '                            hDOClnk.Visible = False


                    '                        End If
                    '                    End If
                    '                End If
                    '            Else
                    '                If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                    '                    Dim linkURL As String = Request.ApplicationPath & "/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                    '                    Dim linkDOCURL As String = Request.ApplicationPath & "/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                    '                    Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                    '                    Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)
                    '                    Dim date1, date2 As DateTime
                    '                    date1 = DateTime.Now
                    '                    date2 = New DateTime(date1.Year, 5, 20)


                    '                    If ((Not (hlnk) Is Nothing) _
                    '                                AndAlso (linkURL <> "")) Then
                    '                        hlnk.Visible = True
                    '                        hlnk.NavigateUrl = linkURL
                    '                        hlnk.Target = "downloadmateriallink"
                    '                        'hDOClnk.Visible = True
                    '                        'hDOClnk.NavigateUrl = linkDOCURL
                    '                        'hDOClnk.Target = "downloadmateriallink"
                    '                        If e.Item.DataItem("PaymentReference").ToString <> "" Then
                    '                            Select Case Mid(e.Item.DataItem("ContestAbbr").ToString, 1, 2)
                    '                                Case "MB"  'math contest
                    '                                    hlnk.Text = "Click here to download Math Bee Sample Question Paper."
                    '                                    hlnk.Visible = True
                    '                                Case "SG", "JG"   'geography contest
                    '                                    hlnk.Text = ""
                    '                                    hlnk.Visible = False
                    '                                Case Else  'take default value
                    '                                    hlnk.Visible = True
                    '                            End Select
                    '                            'If Mid(e.Item.DataItem("ContestAbbr").ToString, 1, 2) = "MB" Then
                    '                            '    hlnk.Text = "Click here to download Math Bee Sample Question Paper."
                    '                            '    hDOClnk.Visible = False
                    '                            'Else
                    '                            '    hDOClnk.Text = "Click here to download practice words as Word Document."
                    '                            'End If
                    '                            'hlnk.Visible = True
                    '                        Else
                    '                            hlnk.Visible = False
                    '                            hDOClnk.Visible = False
                    '                        End If
                    '                        'after May 20, download links should be disabled
                    '                        If DateTime.Compare(date1, date2) > 0 Then
                    '                            hlnk.Visible = False
                    '                            hDOClnk.Visible = False
                    '                        End If

                    '                        'we dont have docs ..so commenting
                    '                        hDOClnk.Visible = False
                    '                    End If
                    '                End If
                    '            End If
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info " & _
                                                                            "<BR> Fee:" & e.Item.DataItem("Fee").ToString & _
                                                                            "<BR> Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") & _
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
            End Select
        End Sub

        'Private Sub dgMeals_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        '    Select Case e.Item.ItemType
        '        Case ListItemType.Item, ListItemType.AlternatingItem
        '            FormatCurrency(e.Item.DataItem("Amount"))
        '            If IsDate(e.Item.DataItem("ContestDate")) Then Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString()
        '            If IsDate(e.Item.DataItem("PaymentDate")) Then Convert.ToDateTime(e.Item.DataItem("PaymentDate")).ToShortDateString()
        '    End Select
        'End Sub
    End Class
End Namespace


