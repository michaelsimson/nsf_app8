<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="VRegistration.WrkshpRegistrationStatus" CodeFile="WrkshpRegistrationStatus.aspx.vb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <table id="Table1" cellspacing="1" width="100%" cellpadding="1" border="0">

            <tr>
                <td class="Heading" align="center">Workshop Status
                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table3" visible="false" runat="server" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblCommentFuture" runat="server" CssClass="largewordingbold"></asp:Label></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataGrid ID="dgSelectedContestsFuture" runat="server" CssClass="mediumwording" Font-Size="X-Small"
                                    AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
                                    BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
                                    <FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
                                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
                                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                                    <HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="ECalendarID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Workshop">
                                            <HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Workshop Date">
                                            <HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "EventDate") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Workshop Time">
                                            <HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Contact Info">
                                            <HeaderStyle Width="200px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "ContactInfo") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Payment Info">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Join URL">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblJoinURL" runat="server" CssClass="SmallFont"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                    <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                </asp:DataGrid></td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblComment" runat="server" CssClass="largewordingbold"></asp:Label></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataGrid ID="dgSelectedContests" runat="server" CssClass="mediumwording" Font-Size="X-Small"
                                    AutoGenerateColumns="False" CellPadding="2" BorderColor="Tan" BorderWidth="1px"
                                    BackColor="LightGoldenrodYellow" ForeColor="Black" GridLines="None">
                                    <FooterStyle CssClass="GridFooter" BackColor="Tan"></FooterStyle>
                                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False" BackColor="PaleGoldenrod"></AlternatingItemStyle>
                                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                                    <HeaderStyle Width="200px" CssClass="GridHeader" Wrap="False" BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="ECalendarID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Workshop">
                                            <HeaderStyle Width="150px" Wrap="false"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "EventDesc")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Workshop Date">
                                            <HeaderStyle Wrap="False" Width="150px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "EventDate") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Workshop Time">
                                            <HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Contact Info">
                                            <HeaderStyle Width="200px"></HeaderStyle>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "ContactInfo") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:TemplateColumn HeaderText="Payment Info">
                                            <HeaderStyle Width="300px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentInfo" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" ForeColor="DarkSlateBlue" BackColor="PaleGoldenrod"></PagerStyle>
                                    <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                </asp:DataGrid></td>
                        </tr>
                    </table>

                </td>
            </tr>


            <tr>
                <td class="ContentSubTitle" align="left" colspan="2">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkParentRegistration" runat="server" NavigateUrl="UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink></td>
            </tr>
        </table>

    </div>
</asp:Content>




