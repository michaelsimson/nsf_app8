Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


    Partial Class OnlineWrkshpRegistrationStatus
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Session("LoggedIn") = "true"
            'Session("LoginEmail") = "mathew.may@gmail.com"

            'Put user code to initialize the page here

            If LCase(Session("LoggedIn")) <> "true" Then
                Response.Redirect(Request.ApplicationPath & "/maintest.aspx")
            End If

            If Not Page.IsPostBack Then


                Dim redirectURL As String
                redirectURL = Request.ApplicationPath & "/UserFunctions.aspx"
                hlinkParentRegistration.NavigateUrl = redirectURL

                '***************************************************
                '***Get IndID and SpouseID for the givn Logon Person
                '***************************************************
                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim StrIndSpouse As String = ""
                Dim intIndID As Integer = 0
                Dim dsIndSpouse As New DataSet

                StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

                Dim objIndSpouse As New IndSpouse10
                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

                If dsIndSpouse.Tables.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                            End If
                        Else
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            Else
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                            End If
                        End If
                        Session("CustIndID") = intIndID
                    End If
                End If

                Dim connContest As New SqlConnection(Application("ConnectionString"))

                Dim dsContestant As New DataSet
                Dim tblConestant() As String = {"Registration"}

                Dim prmArray(2) As SqlParameter
                prmArray(0) = New SqlParameter
                prmArray(0).ParameterName = "@MemberID"
                prmArray(0).Value = Session("CustIndID")
                prmArray(0).Direction = ParameterDirection.Input

                prmArray(1) = New SqlParameter
                prmArray(1).ParameterName = "@EventYear"
                prmArray(1).Value = Application("ContestYear")
                prmArray(1).Direction = ParameterDirection.Input
                'Future
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetOnlineWorkShopStatusFuture", prmArray)
                If ds.Tables(0).Rows.Count > 0 Then
                    Table3.Visible = True
                    dgSelectedContestsFuture.DataSource = ds.Tables(0)
                    dgSelectedContestsFuture.DataBind()
                    lblCommentFuture.Text = "<br><b>Future Events</b>"
                    For i As Integer = 0 To dgSelectedContestsFuture.Items.Count - 1
                        Dim HostUrl As Label = Nothing
                        Dim joinButton As Button = Nothing
                        'HostUrl = CType(DGCoach.Items(i).FindControl("lnkMeetingURL"), LinkButton)
                        'joinButton = CType(DGCoach.Items(i).FindControl("btnJoin"), Button)
                        HostUrl = CType(dgSelectedContestsFuture.Items(i).FindControl("lblWebinarURL"), Label)
                        joinButton = CType(dgSelectedContestsFuture.Items(i).FindControl("btnJoinMeeting"), Button)
                        If (HostUrl.Text <> "") Then
                            joinButton.Visible = True
                        Else
                            joinButton.Visible = False
                        End If
                    Next
                Else
                    Table3.Visible = False
                End If

                'Past Events
                SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetOnlineWorkShopStatus", dsContestant, tblConestant, prmArray)
                If Not ViewState("SelectedContests") Is Nothing Then
                    CType(ViewState("SelectedContests"), ArrayList).Clear()
                End If
                Dim strToday As Date = Date.Now
                If dsContestant.Tables.Count > 0 Then
                    dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                    Session("ContestsSelected") = ""
                    dgSelectedContests.DataBind()
                    If Not dsContestant.Tables(0).Rows.Count > 0 Then
                        ' lblComment.Text = "No registration data is available to view."
                        lblComment.Text = "<br><br><b> Past Events </b><br>  &nbsp;&nbsp;    No registration data is available to view.<br>"
                    Else
                        lblComment.Text = "<br><br><b> Past Events </b>"
                    End If
                    dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
                End If
            End If
        End Sub

        Private Sub dgSelectedContestsFuture_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContestsFuture.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Fee:" & e.Item.DataItem("Fee").ToString &
                                                                            "<BR>Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") &
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
                    '"Payment Info " & _
                    Dim evDT As Date = CType(DataBinder.Eval(e.Item.DataItem, "EventDate"), Date)
                    Dim curDT As Date = Now
                    Dim ts As TimeSpan = evDT.Subtract(curDT)
                    Dim noOfDays As Integer = ts.TotalDays
                    Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                    Dim divWeb As HtmlGenericControl = CType(e.Item.FindControl("divWeb"), HtmlGenericControl)
                    Dim hlRecLink As HyperLink = CType(e.Item.FindControl("hlRecLink"), HyperLink)
                    Dim lblRecordingURL As Label = CType(e.Item.FindControl("lblRecordingURL"), Label)
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Approved")) And DataBinder.Eval(e.Item.DataItem, "Approved").ToString().ToLower = "y" Then
                        If lblRecordingURL.Text.Length > 0 Then
                            hlRecLink.Enabled = True
                        End If
                        divWeb.Visible = True
                        'CType(e.Item.FindControl("btnRegWebinar"), Button).Enabled = True
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) And DataBinder.Eval(e.Item.DataItem, "FileName") <> "" Then
                            If (noOfDays <= 10 And noOfDays >= -30) Then
                                CType(e.Item.FindControl("lblDW"), Label).Visible = False
                                Dim linkURL As String = Request.ApplicationPath & "/IncludeOnlineWS/" & CType(DataBinder.Eval(e.Item.DataItem, "FileName"), String)
                                If ((Not (hlnk) Is Nothing) AndAlso (linkURL <> "")) Then
                                    hlnk.Visible = True
                                    hlnk.NavigateUrl = linkURL
                                End If
                            End If
                        End If

                        If (noOfDays > 10) Then
                            CType(e.Item.FindControl("lblDW"), Label).Visible = True
                            If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) Or DataBinder.Eval(e.Item.DataItem, "FileName") = "" Then
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Workshop Book will be available on " & evDT.AddDays(-10).ToShortDateString()
                            Else
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Workshop Book is not yet available. Please try in a few days."
                            End If
                        ElseIf noOfDays < -30 Then
                            CType(e.Item.FindControl("lblDW"), Label).Text = "Deadline has passed for downloading the workshop book."
                            CType(e.Item.FindControl("lblDW"), Label).Visible = True
                            hlnk.Visible = False
                        Else
                            If IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) Or DataBinder.Eval(e.Item.DataItem, "FileName") = "" Then
                                CType(e.Item.FindControl("lblDW"), Label).Visible = True
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Work Shop Book is not yet available. Please try in a few days."
                            End If
                        End If
                    Else
                        divWeb.Visible = False
                        CType(e.Item.FindControl("lblDW"), Label).Visible = False
                    End If
            End Select

        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Fee:" & e.Item.DataItem("Fee").ToString &
                                                                            "<BR>Payment Date:" & IIf(e.Item.DataItem("PaymentDate").ToString <> "", e.Item.DataItem("PaymentDate").ToString, "<B>UnPaid</B>") &
                                                                            "<BR>Payment Reference:" & e.Item.DataItem("PaymentReference").ToString
                    '"Payment Info " & _ "<BR>"
                    Dim evDT As Date = CType(DataBinder.Eval(e.Item.DataItem, "EventDate"), Date)
                    Dim curDT As Date = Now
                    Dim ts As TimeSpan = evDT.Subtract(curDT)
                    Dim noOfDays As Integer = ts.TotalDays
                    Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                    Dim hlRecLink As HyperLink = CType(e.Item.FindControl("hlRecLink"), HyperLink)
                    Dim lblRecordingURL As Label = CType(e.Item.FindControl("lblRecordingURL"), Label)
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Approved")) And DataBinder.Eval(e.Item.DataItem, "Approved").ToString().ToLower = "y" Then
                        If lblRecordingURL.Text.Length > 0 Then
                            hlRecLink.Enabled = True
                        End If

                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) And DataBinder.Eval(e.Item.DataItem, "FileName") <> "" Then
                            If (noOfDays <= 10 And noOfDays >= -30) Then
                                CType(e.Item.FindControl("lblDW"), Label).Visible = False
                                Dim linkURL As String = Request.ApplicationPath & "/IncludeOnlineWS/" & CType(DataBinder.Eval(e.Item.DataItem, "FileName"), String)
                                If ((Not (hlnk) Is Nothing) AndAlso (linkURL <> "")) Then
                                    hlnk.Visible = True
                                    hlnk.NavigateUrl = linkURL
                                End If
                            End If
                        End If

                        If (noOfDays > 10) Then
                            CType(e.Item.FindControl("lblDW"), Label).Visible = True
                            If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) Or DataBinder.Eval(e.Item.DataItem, "FileName") = "" Then
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Workshop Book will be available on " & evDT.AddDays(-10).ToShortDateString()
                            Else
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Workshop Book is not yet available. Please try in a few days."
                            End If
                        ElseIf noOfDays < -30 Then
                            CType(e.Item.FindControl("lblDW"), Label).Text = "Deadline has passed for downloading the workshop book."
                            CType(e.Item.FindControl("lblDW"), Label).Visible = True
                            hlnk.Visible = False
                        Else
                            If IsDBNull(DataBinder.Eval(e.Item.DataItem, "FileName")) Or DataBinder.Eval(e.Item.DataItem, "FileName") = "" Then
                                CType(e.Item.FindControl("lblDW"), Label).Visible = True
                                CType(e.Item.FindControl("lblDW"), Label).Text = "Work Shop Book is not yet available. Please try in a few days."
                            End If
                        End If
                    Else
                        CType(e.Item.FindControl("lblDW"), Label).Visible = False
                    End If
            End Select
        End Sub


        Protected Sub dgSelectedContestsFuture_ItemCommand(source As Object, e As DataGridCommandEventArgs)

            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Join") Then
                Try
                    Dim strWebinarURL As String = CType(e.Item.FindControl("lblWebinarURL"), Label).Text
                    Dim startDate As String = Convert.ToDateTime(CType(e.Item.FindControl("lblDate"), Label).Text).ToString("MM/dd/yyyy")
                    Dim startTime = CType(e.Item.FindControl("lblTime"), Label).Text

                    Dim dtFromS As New DateTime()
                    Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                    Dim mins As Double = 40.0
                    If DateTime.TryParse(startTime, dtFromS) Then
                        Dim TS As TimeSpan = dtFromS - dtEnds
                        mins = TS.TotalMinutes
                    End If
                    Dim strTodayDate = DateTime.Now.ToString("MM/dd/yyyy")
                    'Response.Redirect(strWebinarURL)
                    If (startDate = strTodayDate And mins <= 30) Then
                        Dim js As String = "window.open('" + strWebinarURL + "', '_blank');"
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open Webinar", js, True)
                    Else

                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert();", True)
                    End If



                    'Dim workshopDate As String = String.Empty
                    'Dim UserID As String = String.Empty
                    'Dim Pwd As String = String.Empty
                    'Dim HostUrl As LinkButton = Nothing
                    'workshopDate = CType(e.Item.FindControl("lblWebinarDate"), Label).Text
                    'hdnWorkshopDate.Value = workshopDate
                    'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", True)
                Catch se As SqlException

                End Try

            End If
        End Sub
    End Class
End Namespace


