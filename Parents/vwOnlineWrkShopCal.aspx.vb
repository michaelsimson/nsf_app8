﻿Imports Microsoft.ApplicationBlocks.Data

Partial Class vwOnlineWrkShopCal

    Inherits System.Web.UI.Page

    Private Sub BindGrid()
        Try
            Dim StrSql As String = " Select OnlineWSCalID,IsNull(EventFeesID,0) as EventFeesID,(Select FirstName + '' + LastName from IndSpouse where autoMemberID =TeacherID) as Teacher,TeacherID,EventYear,EventID,(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=OWS.ProductGroupId and EventId =20) as ProductGroupCode,(Select Top 1 Name ProductCode from Product where ProductGroupId=OWS.ProductGroupId and ProductId=OWS.ProductId and EventId =20) as ProductCode,ProductGroupId,ProductID,Date,ISNULL(CONVERT(VARCHAR(10),CAST(Time as Time),100),'TBD') as Time,Duration,RegistrationDeadline as RegDeadline,LateFeeDeadLine as LateRegDLDate,IsNull(MaxCap,0) as MaxCap from OnlineWSCal OWS where EventYear =(select EventYear from Event where EventId=20)"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql) ' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID" & ddlProduct.SelectedValue & "") ' and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "'" ")
            If ds.Tables(0).Rows.Count > 0 Then
                grdTarget.Visible = True
                grdTarget.DataSource = ds
                grdTarget.DataBind()

                grdTarget.Columns(1).Visible = False
                '    Session("dsOWkshop") = ds
            Else
                errmsg.Text = "No records to display"
                errmsg.ForeColor = Color.Red

                grdTarget.Visible = False
                grdTarget.DataSource = Nothing
                grdTarget.DataBind()
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect(".\login.aspx?entry=" & Session("entryToken"))
        End If
        BindGrid()
    End Sub
End Class
