<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.reg_Contestants" Trace="false" CodeFile="reg_Contestants.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Contestants</title>
		<LINK href="cssMain.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BODY {
	FONT-FAMILY: verdana, arial, helvetica
}
.calTitle {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; WIDTH: 90px; COLOR: black; BACKGROUND-COLOR: #cccccc
}
.calBody {
	BORDER-TOP-WIDTH: 10px; BORDER-LEFT-WIDTH: 10px; FONT-SIZE: 11px; BORDER-BOTTOM-WIDTH: 10px; BORDER-RIGHT-WIDTH: 10px
}
		</style>
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="1" cellPadding="1" border="0">
				<TR>
					<TD width="100%">
						<TABLE cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD width="75%"><asp:datagrid id="dgChildren" runat="server" CssClass="largewordingbold" BackColor="White" BorderWidth="1px"
										BorderStyle="None" BorderColor="#CC9966" CellPadding="4" AutoGenerateColumns="False" Font-Size="Medium"
										Font-Names="Arial Black">
										<FooterStyle CssClass="GridFooter"></FooterStyle>
										<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
										<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
										<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Name of the Child">
												<ItemTemplate>
													<%# DataBinder.Eval(Container.DataItem, "First_Name") %>
													<%# DataBinder.Eval(Container.DataItem, "Middle_Initial") %>
													<%# DataBinder.Eval(Container.DataItem, "Last_Name") %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="DATE_OF_BIRTH" HeaderText="DOB" DataFormatString="{0:d}"></asp:BoundColumn>
											<asp:BoundColumn DataField="GRADE" HeaderText="Grade"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<TABLE width="100%" border="0">
														<TR>
															<TD class="largewording" align="right">
																<ASP:LABEL id="lblDisplayContestText" runat="server" ForeColor="Red">To 
                  select a contest for this child, click </ASP:LABEL></TD>
															<TD class="largewordingbold" align="left">
																<ASP:LINKBUTTON id="lbDisplayContest" runat="server" CausesValidation="false" CommandName="Select"
																	Text="Select a Contest"></ASP:LINKBUTTON></TD>
														</TR>
														<TR>
															<TD class="largewording" align="right">
																<ASP:LABEL id="lblUpdateInfoText" runat="server" ForeColor="Red">Grade and 
                  Age are required to determine the eligibility, To provide this 
                  info, click </ASP:LABEL></TD>
															<TD class="largewordingbold" align="left">
																<ASP:HYPERLINK id="hlUpdateInfo" runat="server" NavigateUrl="UpdateChild.aspx">Update Child 
                Info</ASP:HYPERLINK></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
									</asp:datagrid></TD>
								<TD class="mediumwording" style="COLOR: red" vAlign="top" noWrap align="center">
									<P>To add another child, click<br>
										<asp:hyperlink id="hlnkAddChild" runat="server" CssClass="mediumwording" Font-Size="Medium" NavigateUrl="AddChild.aspx">Add Child</asp:hyperlink></P>
									<P><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="MainChild.aspx">Back to Childern List</asp:hyperlink></P>
								</TD>
							</TR>
						</TABLE>
						<asp:label id="lblLastDate" runat="server" CssClass="largewordingbold" ForeColor="Red"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewording" width="100%"><br>
						Please note: you need to click select contest each time you want to add a 
						contest.
						<BR>
						After making your selections, please click at the bottom of this page where it 
						says "Pay Now".
					</TD>
				</TR>
				<TR>
					<TD class="ItemCenter" width="100%"><asp:label id="lblMessage" runat="server" CssClass="largewordingbold"></asp:label></TD>
				</TR>
				<TR>
					<TD align="right" width="100%"><asp:panel id="Panel2" runat="server">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1" border="0">
								<TR>
									<TD>
										<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD>
													<asp:Label id="Label1" runat="server" CssClass="largewordingbold">----Selected Contests for all the 
                  Children----</asp:Label></TD>
												<TD></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD colSpan="3">
													<asp:DataGrid id="dgSelectedContests" runat="server" CssClass="mediumwording" BackColor="#DEBA84"
														BorderWidth="1px" BorderStyle="None" BorderColor="#DEBA84" CellPadding="3" AutoGenerateColumns="False"
														Font-Size="X-Small" CellSpacing="2">
														<FooterStyle CssClass="GridFooter"></FooterStyle>
														<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
														<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
														<Columns>
															<asp:BoundColumn Visible="False" DataField="ChildNumber"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="ContestID"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="PaymentDate"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="Contestant_ID"></asp:BoundColumn>
															<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant"></asp:BoundColumn>
															<asp:TemplateColumn HeaderText="Contest">
																<HeaderStyle Width="300px"></HeaderStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem, "ContestCode") %>
																	<BR>
																	<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Contest Date">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem, "ContestDate") %>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Contest Time">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem, "ContestTime") %>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Contact Info">
																<HeaderStyle Width="300px"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id="lblContact" runat="server"></asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Contest Info">
																<HeaderStyle Width="300px"></HeaderStyle>
																<ItemTemplate>
																	Payment Info<BR>
																	Amount:US$<%# DataBinder.Eval(Container.DataItem, "Fee") %><BR>
																	Date Paid :<%# DataBinder.Eval(Container.DataItem, "PaymentDate") %><BR>
																	Reference :<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %><BR>
																	<BR>
																	<asp:Label id="lblScore" runat="server"></asp:Label><BR>
																	<BR>
																	<asp:HyperLink id="hlDownloadLink" runat="server" Visible="False">Click here to 
                  download practice words as PDF File</asp:HyperLink><br>
																	<asp:HyperLink id="hlDOCDownloadLink" runat="server" Visible="False">Click here to 
                  download practice words as Word Document</asp:HyperLink>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<ItemTemplate>
																	<asp:LinkButton id="lbRemoveContest" runat="server" CausesValidation="false" CommandName="Select"
																		Text="Remove Contest"></asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
													</asp:DataGrid><FONT face="Verdana, Arial, Helvetica" color="#ff0000" size="1">If 
														the Contest Date is blank, the date will be announced shortly<BR>
														If the contest time is displayed as 12:00:00 AM, that means the time is not 
														determined yet for this contest.
														<br>
														Contest Time shown above are general. Please refer to the program schedule for 
														the actual time.<BR>
													</FONT>
												</TD>
											</TR>
											<TR>
												<TD></TD>
												<TD>
													<ASP:LABEL id="lblregFee" runat="server" CssClass="mediumwordingbold">Registration Fee Due (US$) : 
                  </ASP:LABEL>
													<ASP:LABEL id="RegFee" runat="server" CssClass="mediumwordingbold"></ASP:LABEL></TD>
												<TD>
													<ASP:HYPERLINK id="hlPayNow" runat="server" CssClass="mediumwordingbold" Font-Size="Medium" NavigateUrl="TermsAndConditions.aspx"
														Visible="False">Pay 
                  Now</ASP:HYPERLINK></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD class="largewording">Please note that registration is not complete until 
										payment is made. A link for practice words (spelling and vocabulary) will be 
										emailed to you after payment has been received. Please note that 2/3 of 
										registration fee goes to <A href="scholarships.asp">India Scholarships</A> and 
										is a tax-deductible contribution to NSF. Please print and use your payment 
										confirmation for tax-deduction purpose. Registration fee cannot be refunded 
										under any circumstances except in the case of a system error. If you register, 
										but do not participate, the entire registration fee becomes a tax-deductible 
										donation to NSF.
									</TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="100%"><asp:panel id="Panel3" runat="server">
							<TABLE cellSpacing="1" cellPadding="1" border="0">
								<TR>
									<TD>
										<TABLE cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD>
													<ASP:LABEL id="lblEligible" runat="server" CssClass="largewordingbold">The contests your child is 
                  eligible for are given below. Please make your 
                  selection(s).<BR>To select a Contest, click "select" link 
                  displayed against the contest.

</ASP:LABEL></TD>
												<TD>
													<ASP:LINKBUTTON id="lnkClose" runat="server" CssClass="mediumwordingbold" Font-Size="Medium" Text="Select">Close</ASP:LINKBUTTON></TD>
											</TR>
											<tr>
											   <td colspan="2"><asp:Label ID="lblGridHeading" runat="server" CssClass="mediumwording"></asp:Label></td>							
											</tr>
										</TABLE>
										<ASP:DATAGRID id="dgEligibleContests" runat="server" CssClass="mediumwording" BackColor="#DEBA84"
											BorderWidth="1px" BorderStyle="None" BorderColor="#DEBA84" CellPadding="3" AutoGenerateColumns="False"
											Font-Size="X-Small" CellSpacing="2">
											<FooterStyle CssClass="GridFooter"></FooterStyle>
											<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
											<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
											<COLUMNS>
												<ASP:TEMPLATECOLUMN HeaderText="Contest">
													<HEADERSTYLE Width="300px"></HEADERSTYLE>
													<ITEMTEMPLATE>
														<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
													</ITEMTEMPLATE>
												</ASP:TEMPLATECOLUMN>
												<ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="Fee" HeaderText="Fee" DataFormatString="${0:N2}"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestDesc" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestDate" HeaderText="ContestDate" DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestTime" HeaderText="ContestTime"></ASP:BOUNDCOLUMN>
												<ASP:TEMPLATECOLUMN HeaderText="Contact Info">
													<HEADERSTYLE Width="300px"></HEADERSTYLE>
													<ITEMTEMPLATE>
														<ASP:Label ID="lblContactInfo" Runat="server" CssClass="SmallFont"></ASP:Label>
													</ITEMTEMPLATE>
												</ASP:TEMPLATECOLUMN>
												<ASP:TEMPLATECOLUMN Visible="true">
													<ITEMTEMPLATE>
														<ASP:LINKBUTTON id="lbtnSelect" runat="server" CausesValidation="false" CommandName="Select" Text="Select"></ASP:LINKBUTTON>
													</ITEMTEMPLATE>
												</ASP:TEMPLATECOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestCategoryID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="EventID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="EventCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductGroupID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductGroupCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ChapterID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:TemplateColumn>
												    <ItemTemplate>
												        <asp:CheckBox ID="chkSelectContest" runat="server" Checked="false" />
												    </ItemTemplate>
												</ASP:TemplateColumn>
											</COLUMNS>
											<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
										</ASP:DATAGRID>

									</TD>
								</TR>
				<TR>
					<TD class="ItemCenter" width="100%">
					<asp:Button ID="btnAddContests" runat="server" Text="Add Selected Contests" />
					</TD>
				</TR>
				<TR>
					<TD class="ItemCenter" width="100%">
											<FONT face="Verdana, Arial, Helvetica" color="#ff0000" size="1">Contest Time shown 
												above are general. Please refer to the program schedule for the actual time.<BR>
												If the Contest Date is blank, the date will be announced shortly<BR>
												If the contest time is displayed as 1/1/1900 12:00:00 AM, that means the time 
												is not determined yet for this contest<BR>
												If no Contests are listed please contact your Chapter Coordinator for further 
												processing </FONT>					</TD>
				</TR>				</TABLE>
						</asp:panel></TD>
				</TR>
				<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"><asp:hyperlink id="hlinkParent" runat="server" NavigateUrl="UserFunctions.aspx">Back to Registration Page</asp:hyperlink></td>
				</tr>
			</TABLE>
			<asp:label id="lblChildNumber" runat="server" Visible="False"></asp:label></form>
	</body>
</HTML>

 

 
 
 