<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" SmartNavigation="False" ValidateRequest="false" CodeFile="Email.aspx.vb" Inherits="Email" title="Untitled Page" Debug="true" EnableViewState="true" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<asp:hyperlink id="hlinkVolunteerFunctions" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
<table  width="100%">
    <tr><td colspan="3" ><asp:Label ID="lblDebug" runat="server" ></asp:Label></td></tr>
    <tr>
    <td>
    <asp:Button ID="btnSend" runat="server" Text="Send Email" />
    </td>
    <td  style="width: 60%;" align="left">
    <asp:Button ID="btnIns" runat="server" Text="Insert Checked Contacts"  /><asp:Label ID="lblEmailError" runat="server" width="440px" Font-Bold="True"></asp:Label></td>
    <td style="width: 31%;" rowspan="6">
    <asp:Panel ID="pnlMainAddress" runat="server" GroupingText="Addresses" Width="100%"  Height="600px">
        <asp:dropdownlist ID="ddlTo" runat="server" AutoPostBack="true" EnableViewState=true>
        <asp:ListItem Text="Parents - Main Contact" Value="PP" Selected="True"></asp:ListItem>
        <asp:ListItem Text="Parents - Both" Value="PB" ></asp:ListItem>
        <asp:ListItem Text="Volunteers with Assigned Roles" Value="VR"></asp:ListItem>
        <asp:ListItem Text="Volunteers with Unassigned Roles" Value="VUR"></asp:ListItem>
        <asp:ListItem Text="Paid Registrations" Value="Paid"></asp:ListItem>
        <asp:ListItem Text="Pending Registrations" Value="Pending"></asp:ListItem>
      </asp:dropdownlist><br /><br />
       <asp:Panel ID="pnllstAddress" runat="server" ScrollBars="Vertical" Width="95%"  Height="550px">
        <asp:CheckBoxList id="lstAddress" runat="server" autopostback="false"   width="100%" height="11%" CssClass="SmallFont" EnableViewState="true" DataTextField="Email"  DataValueField="AutoMemberID" ></asp:CheckBoxList>
        </asp:Panel></asp:Panel>    </td>
    </tr>
    <tr style="height: 25px;">
    <td> From: </td>
    <td style="width: 60%;"><asp:TextBox ID="txtFrom" runat="server" Width="99%" Text="" ></asp:TextBox></td>
    </tr>
    <tr style="height: 50px;">
    <td style="height: 7px"> To: </td>
    <td style="width: 90%; height: 7px;"><asp:TextBox ID="txtTo" runat="server" Width="99%" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    
    <tr style="height: 25px;">
    <td> Subject: </td>
    <td style="width: 60%;"><asp:TextBox ID="txtSubject" runat="server" Width="99%"></asp:TextBox></td>
   </tr>
    
    <tr style="height: 25px;">
    <td> File to be Attached: </td>
    <td style="width: 60%;"><asp:FileUpload ID="AttachmentFile" runat="server"/>   </td>
    </tr>
    
    <tr style="height: 300px;">
    <td style="width: 60%;" colspan="2"><FTB:FreeTextBox id="txtEmailBodtText" Text="" runat="Server" Width="99%" /></td>
   </tr>
    <tr style="height: 25px;">
    <td style="width: 60%;" colspan="3"></td>
    </tr></table>
</asp:Content>


 

 
 
 