Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Imports Microsoft.ApplicationBlocks.Data

Imports NorthSouth.BAL


Namespace VRegistration



    Partial Class CreateUser
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtLoginId As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlRole As System.Web.UI.WebControls.DropDownList
        Protected WithEvents LblError As System.Web.UI.WebControls.Label
        Protected WithEvents lblConfirm As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


            'Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            'Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            'If (homeURL <> Nothing) Then
            'homeLink.HRef = homeURL
            'End If
            'If (loginURL <> Nothing) Then
            ' loginLink.HRef = loginURL + Session("entryToken").ToString().Substring(0, 1)
            'End If
            '*** Populate Chapter DropDown
            If Page.IsPostBack = False Then
                Dim objChapters As New Chapter
                Dim dsChapters As New DataSet
                Dim liChapter As New ListItem
                Dim entryToken As String
                Dim SecureUrl As String
                If (Not Request.IsSecureConnection And Not ((Request.Url.Host = "localhost") Or (Request.Url.Host = "ferdine"))) Then
                    SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                    Response.Redirect(SecureUrl, True)
                End If

                entryToken = Request.QueryString("entry")
                If (entryToken <> Nothing) Then
                    If (entryToken = "P") Or (entryToken = "p") Then
                        Session("entryToken") = "Parent"
                    ElseIf (entryToken = "V") Or (entryToken = "v") Then
                        Session("entryToken") = "Volunteer"
                    ElseIf (entryToken = "D") Or (entryToken = "d") Then
                        Session("entryToken") = "Donor"
                        Session("EventID") = 11
                        Response.Redirect("Don_athonDonorDetails.aspx?id=1")
                    Else
                        Session("entryToken") = ""
                    End If
                End If

                If (Session("entryToken") = "Parent") Then
                    roleLabel.Text = "Parent"
                    roleLabel.Visible = True
                ElseIf (Session("entryToken") = "Donor") Then
                    Session("EventID") = 11
                    Response.Redirect("Don_athonDonorDetails.aspx?id=1")
                    divparents.Visible = False
                    rfvNSFChapter.Enabled = False
                    roleLabel.Text = "Donor"
                    roleLabel.Visible = True
                ElseIf (Session("entryToken")) = "Volunteer" Then
                    roleLabel.Text = "Volunteer"
                    roleLabel.Visible = True
                Else
                    roleLabel.Visible = False
                    RolesList.Visible = True
                End If

                liChapter.Text = "Select Chapter"
                liChapter.Value = "0"
                ' objChapters.GetAllChapters(Application("ConnectionString"), dsChapters
                Try
                    dsChapters = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapters")
                Catch se As SqlException
                    lblErrord.Visible = True
                    lblErrord.Text = se.Message
                    Return
                End Try

                If dsChapters.Tables.Count > 0 Then
                    ddlChapter.DataSource = dsChapters.Tables(0)
                    ddlChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                    ddlChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                    ddlChapter.DataBind()
                    ddlChapter.Items.Add(liChapter)

                    ddlChapter.Items.FindByValue("0").Selected = True

                End If
            End If

          
        End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim mailobj As New MailMessage
            Dim i As Integer = 0
            Dim Password As String = ""
            Dim RoleString As String = ""
            Dim errFlag As Boolean = False
            If Trim(txtUserEmail.Text) = "" Then
                rfvUserEmail.ErrorMessage = "User EMail should not be Blank"
                rfvUserEmail.IsValid = False
            End If

            Dim objReg As New Regex("")

            If Not Regex.IsMatch(txtUserEmail.Text.Trim(), "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase) Then
                regUserEmail.ErrorMessage = "EMail id Should be a Valid Email Address"
                regUserEmail.IsValid = False
                Exit Sub
            End If
            If Trim(txtPassword.Text) = "" Then
                rfvPassword.ErrorMessage = "Password cannot be a Blank"
                rfvPassword.IsValid = False
                Exit Sub
            End If
            If ddlChapter.SelectedValue = 0 And Not (Session("entryToken") = "Donor") Then
                rfvNSFChapter.ErrorMessage = "Chapter cannot be Blank"
                rfvNSFChapter.IsValid = False
                Exit Sub
            End If

            If (Session("entryToken") = Nothing Or Session("entryToken") = "") Then
                RoleString = RolesList.SelectedValue
            Else
                RoleString = Session("entryToken")
            End If
            'Password = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "getPassword", New SqlParameter("@Email", txtUserEmail.Text.Trim)).ToString()
            Dim displayString As String = ""

            Dim ChildMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT email from child where email='" & txtUserEmail.Text.Trim() & "'"))
            Dim IndSpouseMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT email from indspouse where email='" & txtUserEmail.Text.Trim() & "'"))
            Dim LmasterMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select user_email from login_master where user_email='" & txtUserEmail.Text.Trim() & "'"))

            If ChildMail <> "" Then
                displayString = "An account with that email already exists in Child record. <a href='login.aspx?entry=s'> click here </a> to login"
                errFlag = True
            End If
            If IndSpouseMail <> "" Then
                displayString = "An account with that email already exists in Parent record please use other email id."
                errFlag = True
            End If
            If LmasterMail <> "" Then
                displayString = "An account with that email already exists in our system. <a href='maintest.aspx'> click here </a> to login"
                errFlag = True
            End If

            If IndSpouseMail <> "" And LmasterMail = "" Then
                errFlag = False
            End If

            If errFlag = True Then
                lblErrord.Visible = True
                lblConfirmation.Visible = False
                lblErrord.Text = displayString
            Else
                lblErrord.Visible = False
                'MsgBox(ddlChapter.SelectedItem.Value)
                Dim param(12) As SqlParameter
                param(0) = New SqlParameter("@ChapterID", IIf(ddlChapter.SelectedItem.Value = 0, 1, ddlChapter.SelectedItem.Value))
                param(1) = New SqlParameter("@date_expires", DateAdd("m", 1, Now))
                param(2) = New SqlParameter("@date_added", Now)
                param(3) = New SqlParameter("@last_login", Now)
                param(4) = New SqlParameter("@date_updated", Now)
                param(5) = New SqlParameter("@view_tasks", "Yes")
                param(6) = New SqlParameter("@user_name", Server.HtmlEncode(txtUserName.Text))
                param(7) = New SqlParameter("@parent_email", DBNull.Value)
                param(8) = New SqlParameter("@active", "Yes")
                param(9) = New SqlParameter("@user_email", Server.HtmlEncode(txtUserEmail.Text).Trim)
                param(10) = New SqlParameter("@user_pwd", Server.HtmlEncode(txtPassword.Text))
                param(11) = New SqlParameter("@user_role", Server.HtmlEncode(RoleString))
                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_AddNSFUser", param)

                tblCreateUser.Visible = False
                tblConfirmation.Visible = True
                trNewuser.Visible = False
                btnContinue.Visible = True
                lblConfirmation.Visible = True

                Session("LoginEmail") = txtUserEmail.Text
                Session("UserName") = txtUserName.Text
                Session("ChapterID") = ddlChapter.SelectedItem.Value
                Session("userid") = txtUserEmail.Text
                Session("LoggedIn") = "True"
                '*** Sending New Registration Confirmation Message 
                Dim re As TextReader
                Dim emailbody As String
                re = File.OpenText(Server.MapPath("NewRegistration.htm"))
                emailbody = re.ReadToEnd
                re.Close()
                emailbody = emailbody.Replace("[USERID]", param(9).Value.ToString)
                emailbody = emailbody.Replace("[PASSWORD]", param(10).Value.ToString)
                ''NOTE Remember to uncomment the following before sending the source
                SendEmail("North South Foundation Bee Registration", emailbody, Session("LoginEmail"))
            End If
        End Sub

        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            Dim sFrom As String = "nsfcontests@gmail.com"
            Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            mail.IsBodyHtml = True

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Try
                client.Send(mail)
            Catch e As Exception
                ok = False
            End Try
        End Sub
        Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
            Dim autoMemberID As String = ""
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Select Case Session("entryToken")
                Case "Parent"
                    'Response.Redirect("Login.aspx?entryToken=p")
                    autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Session("LoginEmail")))
                    If (autoMemberID <> "") Then
                       Response.Redirect("Registration.aspx?ParentUpdate=true")
                    Else
                        Response.Redirect("Registration.aspx")
                    End If
                Case "Volunteer"
                    ' ############### Checking for Volunteers Unique Email  #####################
                    Response.Redirect("VolUniqueEmail.aspx")

                Case "Donor"
                    'Response.Redirect("Login.aspx?entryToken=d")
                    Response.Redirect("Don_athonDonorDetails.aspx?id=1")
                Case "Game"
                    Response.Redirect("Login.aspx?entryToken=g")
                Case Else
                    Response.Redirect("Login.aspx")
            End Select
        End Sub

    End Class

End Namespace

