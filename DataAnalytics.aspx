﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="DataAnalytics.aspx.vb" Inherits="DataAnalytics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <script type="text/javascript">
        function getDropdownListSelectedText() {
        var DropdownList = document.getElementById('<%=ddlReportType.ClientID%>');
            var SelectedValue = DropdownList.value;            
            if (SelectedValue == "4") {           
                document.getElementById('<%=ddlProductGrp.ClientID%>').disabled = false;
                    document.getElementById('<%=ddlYearCount.ClientID%>').disabled = true;
            }
            else {
                document.getElementById('<%=ddlProductGrp.ClientID%>').disabled = true;
                document.getElementById('<%=ddlYearCount.ClientID%>').disabled = false;
            }              
        }
        function Show_Table2(t, yearCnt) {
            
            if (t == 1 || t == 2) {
            
                document.getElementById('<%=hfType.ClientID%>').value = t;
                document.getElementById('<%=hfSelValue.ClientID%>').value = yearCnt;
                document.getElementById('<%=btnShowTable2.ClientID%>').click();
               
            }
            else if (t == 3 ||t==4) {
                document.getElementById('<%=hfType.ClientID%>').value = t;
                // yearcnt is ContestYear
                document.getElementById('<%=hfSelValue.ClientID%>').value = yearCnt;
                 document.getElementById('<%=btnShowTable2.ClientID%>').click();
             }
        }
</script>
    <asp:Button ID="btnShowTable2" runat="server" style="display:none;" />
    <asp:HiddenField ID="hfSelValue" runat="server" />
        <asp:HiddenField ID="hfType" runat="server" />

      <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td colspan="2" style="height: 26px">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
             </td>
        </tr> 
        <tr>           
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
               <center><h2>Data Analytics</h2></center>   
            </td>
        </tr>       
        <tr>
            <td colspan="2">
              
                 <table  width="90%"  style="margin-left:50px;margin-right:auto;">
                     <tr class="ContentSubTitle" style="font-weight:bold;background-color:#ffffcc;">
                         <td>#of Years </td><td>
                         <asp:DropDownList ID="ddlYearCount" runat="server">
                         </asp:DropDownList>
                         </td>
                         <td>Report</td><td>
                         <asp:DropDownList ID="ddlReportType" runat="server" width="185px" onchange="getDropdownListSelectedText();">
                         </asp:DropDownList>
                         </td>
                         <td>
                            <asp:Label ID="lblProductGrp" Text="Product Group" runat="server" ></asp:Label></td>
                         <td>
                             <asp:DropDownList ID="ddlProductGrp" runat="server"  DataTextField="Name" DataValueField="ProductGroupCode"  Enabled="false" >
                             </asp:DropDownList>
                         </td>
                         <td>
                             <asp:Button ID="btnSubmit" runat="server" Text="Submit" /> <asp:Button ID="btnExport" runat="server" Text="Export to Excel"  />
                         </td>
                     </tr>
                    </table>
                <div style="text-align:center">  <asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                <div style="margin-left:240px;">
                                           <asp:Button ID="btnExportAll" runat="server" Text="Export All" visible="false"/></div>
                                           <asp:Literal ID="ltrHTML" runat="server"></asp:Literal>
                           <br />
                           <br />
                <div   id="divTable2" runat="server" visible="false" >
                    <div style="float:right">
                        <asp:Button ID="btnExportTabl2" runat="server" Text="Export to Excel" /></div>
                    <br />
                <div style="height:500px;width:900px;overflow: scroll;" >
                 <asp:Literal ID="ltrTable2" runat="server"></asp:Literal>
                    </div>
                </div>
                </td>


        </tr></table>
</asp:Content>

