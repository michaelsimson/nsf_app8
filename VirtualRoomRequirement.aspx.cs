﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using VRegistration;

public partial class VirtualRoomRequirement : System.Web.UI.Page
{
    #region Variable Declaration

    string[] daysx;
    string Preclass;
    string Postclass;
    string str1grid;
    int cycle1;
    int cycle; int MaxVRoomNumber, SignUpID, rownum;
    DateTime startime;
    // string UpdateCalSignUpByYear = "Update CalSignUp set Cycle=0,VRoom=0 where  EventYear='"+DDyear.SelectedValue+"'";

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {

        LBmain.Visible = false;
        //lblalert.Text = "";
        //Session["LoginID"] = 12345;
        //Session["RoleId"] = 1;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
        {
            Response.Redirect("~/login.aspx?entry=p");
        }
        if (Convert.ToInt32(Session["RoleId"]) == 1 || Convert.ToInt32(Session["RoleId"]) == 96)
        {
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {

            }
            else if (Convert.ToInt32(Session["RoleId"]) == 96)
            {
                string TeamLead = string.Empty;
                DataSet dsVol = new DataSet();
                string cmdTeamText = "select TeamLead from Volunteer where MemberID=" + Session["LoginID"].ToString() + " and RoleID=" + Session["RoleID"].ToString() + "";
                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTeamText);
                if (null != dsVol && dsVol.Tables != null)
                {
                    if (dsVol.Tables[0].Rows.Count > 0)
                    {
                        TeamLead = dsVol.Tables[0].Rows[0]["TeamLead"].ToString();
                    }
                }

                if (TeamLead == "Y")
                {


                }
                else
                {
                    Response.Redirect("~/VolunteerFunctions.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/VolunteerFunctions.aspx");
        }

        //if (Convert.ToInt32(Session["RoleId"]) != 1)
        //{
        //    Response.Redirect("~/VolunteerFunctions.aspx");
        //}




        if (!IsPostBack)
        {
            // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, UpdateCalSignUpByYear);
            DDpre.Items.Insert(0, "30");
            DDpre.Items.Insert(0, "25");
            DDpre.Items.Insert(0, "20");
            DDpre.Items.Insert(0, "15");
            DDpre.Items.Insert(0, "Select");

            DDpost.Items.Insert(0, "60");
            DDpost.Items.Insert(0, "45");
            DDpost.Items.Insert(0, "30");
            DDpost.Items.Insert(0, "15");
            DDpost.Items.Insert(0, "Select");
            PopulateYear(DDyear);
            loadPhase(ddlSemester);
        }
        // lblalert.Text="";
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            //for (int i = 2011; i <= MaxYear; i++)
            //{
            //    list.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            for (int i = MaxYear; i >= (DateTime.Now.Year) - 3; i--)
            {
                list.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();

            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));

            ddlObject.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        }
        catch (Exception ex) { }

    }

    public void VRoomcalculation()
    {
        DataTable dtDay = new DataTable();
        DataTable dtTemp = new DataTable();
        CalculateVRoomByDay("Sunday");
        CalculateVRoomByDay("Monday");
        CalculateVRoomByDay("Tuesday");
        CalculateVRoomByDay("Wednesday");
        CalculateVRoomByDay("Thursday");
        CalculateVRoomByDay("friday");
        CalculateVRoomByDay("Saturday");
    }

    protected void DisplayDataInGrid(bool isServer, DataSet dsServer)
    {

        try
        {
            DataSet ds;
            if (isServer == false)
            {

                VRoomcalculation();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetQueryByDay(string.Empty));
            }
            else
            {
                ds = dsServer;
            }

            ds.AcceptChanges();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                dr.BeginEdit();
                switch (dr["Day"].ToString().ToLower())
                {
                    case "saturday":
                        dr["D"] = 1;
                        break;
                    case "sunday":
                        dr["D"] = 2;
                        break;
                    case "monday":
                        dr["D"] = 3;
                        break;

                    case "tuesday":
                        dr["D"] = 4;
                        break;
                    case "wednesday":
                        dr["D"] = 5;
                        break;
                    case "thursday":
                        dr["D"] = 6;
                        break;

                    case "friday":
                        dr["D"] = 7;
                        break;

                }
                dr.EndEdit();
            }
            ds.AcceptChanges();

            DataView dv = ds.Tables[0].DefaultView;
            dv.Sort = "D Asc";
            DataTable sortedDT = dv.ToTable();
            //DataTable dsne = sortedDT.Copy();
            DataTable dtn = sortedDT.Copy();
            int i = 1; foreach (DataRow r in dtn.Rows) r["RowNum"] = i++;
            DataRow[] drs = dtn.Select("D='1'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }

                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }

            drs = dtn.Select("D='2'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        //int vr = (drs[previousRowIndex]["VRoom"].ToString() != "" ? Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString()) : 0);
                        //int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }
            drs = dtn.Select("D='3'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        //int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }

            drs = dtn.Select("D='4'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        // int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }

            drs = dtn.Select("D='5'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        //int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }
            drs = dtn.Select("D='6'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        //int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }
            drs = dtn.Select("D='7'");
            {
                if (null != drs && drs.Length > 0)
                {
                    for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                    {
                        rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                        //int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        //int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        int vr = 0;
                        if (drs[previousRowIndex]["VRoom"] != null && drs[previousRowIndex]["VRoom"].ToString() != "")
                        {
                            vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                        }
                        else
                        {
                            vr = 0;
                        }


                        int cyclen = 0;

                        if (drs[previousRowIndex]["cycle"] != null && drs[previousRowIndex]["cycle"].ToString() != "")
                        {
                            cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                        }
                        else
                        {
                            cyclen = 0;
                        }
                        for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                        {
                            if (vr.ToString() == drs[nextRowIndex]["VRoom"].ToString())
                            {
                                drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                break;
                            }
                        }
                        if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                        {
                            drs[previousRowIndex]["next"] = 0;
                        }
                        if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                        {
                            drs[previousRowIndex]["Previous"] = 0;
                        }
                    }
                }
            }


            ds.AcceptChanges();
            DataSet dsgrid = new DataSet();
            dsgrid.Tables.Add(dtn);
            gridview_VR.DataSource = dsgrid;
            gridview_VR.DataBind();
            Session["Sessionno"] = dsgrid;
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }

    }

    protected void ExportToExcel()
    {
        try
        {
            DataSet dsExport = (DataSet)Session["Sessionno"];
            DataTable dt = dsExport.Tables[0];
            dt.Columns.RemoveAt(13);
            dt.Columns.RemoveAt(14);
            dt.Columns.RemoveAt(15);
            //dt.Columns[12].
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
                 "attachment;filename=VirtualRoomData.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                sb.Append(dt.Columns[k].ColumnName + ',');
            }
            sb.Append("\r\n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                }

                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        catch (Exception ex)
        {
        }
    }

    protected void Virtual_Click(object sender, EventArgs e)
    {
        hYear.Value = DDyear.SelectedValue;
        lblsuccess.Visible = false;
        lblNoRecords.Visible = false;

        try
        {
            string UpdateCalSignUpByYear = "Update CalSignUp set Cycle=0,VRoom=0 where  EventYear='" + DDyear.SelectedValue + "' and Semester='" + ddlSemester.SelectedValue + "'";
            if ((DDpre.SelectedItem.Text != "Select") && (DDpost.SelectedItem.Text != "Select") && (DDyear.SelectedItem.Value != "Select Year"))
            {
                // string strdeletetemptable = " IF ( EXISTS (select * from TempCalSignup)) drop table TempCalSignup";
                // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strdeletetemptable);
                string strcreateTempTable = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempCalSignup')) begin drop table TempCalSignup end create table TempCalSignup (SignupID int,MemberID int,EventYear int,EventID int,EventCode nvarchar(25),Semester nvarchar(50),";
                strcreateTempTable = strcreateTempTable + "ProductGroupID int,ProductGroupCode nvarchar(50),ProductID int,ProductCode nvarchar(50),[Level] nvarchar(50),";
                strcreateTempTable = strcreateTempTable + "SessionNo int,Day nvarchar(50),Time time(7),Preference int,MaxCapacity int,MinCapacity int,StartDate smalldatetime,";
                strcreateTempTable = strcreateTempTable + "EndDate smalldatetime,CreateDate smalldatetime,CreatedBy int,ModifiedDate smalldatetime,ModifiedBy int,Accepted nvarchar(1),";
                // strcreateTempTable = strcreateTempTable + "Duration float,[Begin] time(7),[End] time(7),Cycle int,VRoom int,UserID varchar(255),PWD varchar(255),UpFlag varchar(15)) insert into TempCalSignup select * from CalSignup where EventYear=" + DDyear.SelectedValue + " ";

                strcreateTempTable = strcreateTempTable + " Duration float,[Begin] time(7),[End] time(7),Cycle int,VRoom int,UserID varchar(255),PWD varchar(255),UpFlag varchar(15)) insert into TempCalSignup select SignupID,MemberID,EventYear,EventID,EventCode,Semester,ProductGroupID,ProductGroupCode,ProductID,ProductCode,[Level],SessionNo,Day,Time,Preference,MaxCapacity,MinCapacity,StartDate,EndDate,CreateDate,CreatedBy,ModifiedDate,ModifiedBy,Accepted,Duration,[Begin],[End],Cycle,VRoom,UserID,PWD,UpFlag from CalSignup where EventYear=" + DDyear.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' and Accepted='Y'";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strcreateTempTable);

                if ((SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select * from TempCalSignup where EventYear=" + DDyear.SelectedValue)).Tables[0].Rows.Count == 0)
                {
                    //Response.Write("no records");  
                    lblNoRecords.Visible = true;
                    lblNoRecords.Text = "No Record Exists!";
                    aftersubmit.Visible = false;
                }
                else
                {
                    GetVRoomLookUpDetails();
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update TempCalSignup set Cycle=0,VRoom=0 where  EventYear='" + DDyear.SelectedValue + "' and Semester='" + ddlSemester.SelectedValue + "' and Accepted='Y'");
                    starttime1();
                    DisplayDataInGrid(false, new DataSet());

                    aftersubmit.Visible = true;
                    lblalert.Visible = false;
                    saveresultsbtn.Enabled = true;
                }
            }
            else
            {
                lblalert.Text = "Select All options";
                lblalert.Visible = true;

            }
        }
        catch (Exception ex)
        {

        }

    }

    public void CalculateVRoomByDay(string DyName)
    {
        try
        {
            MaxVRoomNumber = 0;
            cycle = 0;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetQueryByDay(DyName));
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    startime = Convert.ToDateTime(dt.Rows[i]["Begin"].ToString());
                    TimeSpan StartTime = (TimeSpan)dt.Rows[i]["Begin"];
                    rownum = Convert.ToInt16(dt.Rows[i]["RowNum"].ToString());
                    SignUpID = Convert.ToInt16(dt.Rows[i]["SignUpID"].ToString());
                    DataRow[] drs = dt.Select("RowNum<" + rownum);
                    bool isNeedToCheck = false;
                    if (null != drs && drs.Length > 0)
                    {
                        foreach (DataRow dr in drs)
                        {
                            TimeSpan endTime;
                            int hr = 24;
                            int min;
                            int sec;
                            //Add 24 hrs with end time
                            //When Start time is less than end time;
                            if ((TimeSpan)dr["Begin"] > (TimeSpan)dr["End"])
                            {
                                endTime = (TimeSpan)dr["End"];
                                hr = endTime.Hours + 24;
                                min = endTime.Minutes;
                                sec = endTime.Seconds;
                                if (hr < startime.Hour)
                                {
                                    isNeedToCheck = true;
                                    break;
                                }
                                else if (hr == startime.Hour)
                                {
                                    if (min < startime.Minute)
                                    {
                                        isNeedToCheck = true; break;
                                    }
                                    else if (min == startime.Minute)
                                    {
                                        if (sec < startime.Second)
                                        {
                                            isNeedToCheck = true; break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if ((TimeSpan)dr["End"] < StartTime)
                                {
                                    isNeedToCheck = true; break;
                                }
                            }
                        }
                    }
                    if (isNeedToCheck)
                    {
                        int k = 0;
                        MaxVRoomNumber = MaxVRoom(DyName);
                        for (int j = 1; j <= MaxVRoomNumber; j++)
                        {
                            cycle = MaxCycleByVirtual(j, DyName);
                            DataRow[] drsDuplicate = dt.Select("VRoom='" + j.ToString() + "' AND Cycle='" + cycle.ToString() + "'");
                            List<DataRow> drList = new List<DataRow>();

                            if (null != drsDuplicate && drsDuplicate.Length > 0)
                            {
                                foreach (DataRow dr in drsDuplicate)
                                {
                                    TimeSpan endTime;
                                    int hr = 24;
                                    int min;
                                    int sec;
                                    //Add 24 hrs with end time
                                    //When Start time is less than end time;
                                    if ((TimeSpan)dr["Begin"] > (TimeSpan)dr["End"])
                                    {
                                        endTime = (TimeSpan)dr["End"];
                                        hr = endTime.Hours + 24;
                                        min = endTime.Minutes;
                                        sec = endTime.Seconds;
                                        if (hr < startime.Hour)
                                            drList.Add(dr);
                                        else if (hr == startime.Hour)
                                        {
                                            if (min < startime.Minute)
                                                drList.Add(dr);
                                            else if (min == startime.Minute)
                                            {
                                                if (sec < startime.Second)
                                                    drList.Add(dr);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if ((TimeSpan)dr["End"] < StartTime)
                                            drList.Add(dr);
                                    }
                                }
                            }
                            if (drList.Count > 0)
                            {
                                TimeSpan endTime;
                                int hr = 24;
                                int min;
                                int sec;
                                //Add 24 hrs with end time
                                //When Start time is less than end time;
                                bool bCheck = false;
                                if ((TimeSpan)drList[0]["Begin"] > (TimeSpan)drList[0]["End"])
                                {
                                    endTime = (TimeSpan)drList[0]["End"];
                                    hr = endTime.Hours + 24;
                                    min = endTime.Minutes;
                                    sec = endTime.Seconds;
                                    if (hr < startime.Hour)
                                        bCheck = true;
                                    else if (hr == startime.Hour)
                                    {
                                        if (min < startime.Minute)
                                            bCheck = true;
                                        else if (min == startime.Minute)
                                        {
                                            if (sec < startime.Second)
                                                bCheck = true;

                                        }
                                    }
                                }
                                else
                                {
                                    if ((TimeSpan)drList[0]["End"] < StartTime)
                                        bCheck = true;
                                }
                                if (bCheck)
                                {
                                    cycle++;
                                    MaxVRoomNumber = j;
                                    k = 1;
                                    dt.Rows[i].BeginEdit();
                                    dt.Rows[i]["Cycle"] = cycle;
                                    dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                                    dt.Rows[i].EndEdit();
                                    UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                                    break;
                                }
                            }
                        }
                        if (k == 0)
                        {
                            cycle = 1;
                            MaxVRoomNumber = MaxVRoom(DyName);
                            MaxVRoomNumber++;
                            dt.Rows[i].BeginEdit();
                            dt.Rows[i]["Cycle"] = cycle;
                            dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                            dt.Rows[i].EndEdit();
                            UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                        }
                    }
                    else
                    {
                        MaxVRoomNumber = MaxVRoom(DyName);
                        MaxVRoomNumber++;
                        cycle = 1;
                        dt.Rows[i].BeginEdit();
                        dt.Rows[i]["Cycle"] = cycle;
                        dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                        dt.Rows[i].EndEdit();
                        UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                    }

                }
            }
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }

    public int MaxCycleByVirtual(int VRoomNumber, string DyName)
    {
        try
        {
            int s;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString()
              , CommandType.Text, "select isnull(max(Cycle),0) from TempCalSignup where VRoom=" + VRoomNumber + " and EventYear='" + DDyear.SelectedValue + "' and  Accepted = 'Y' and EventId=13 and Day='" + DyName + "'");
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                s = Convert.ToInt16(dt.Rows[0]["column1"].ToString());
                return s;
            }
            else
            {
                return 1;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void starttime1()
    {
        try
        {
            daysx = new string[7];
            daysx[0] = "Friday";
            daysx[1] = "Monday";
            daysx[2] = "Saturday";
            daysx[3] = "Sunday";
            daysx[4] = "Thursday";
            daysx[5] = "Tuesday";
            daysx[6] = "Wednesday";
            foreach (string s in daysx)
            {
                DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,
                       "select C.SignUpID,C.ProductID,E.Duration from TempCalSignup C inner join EventFees E on C.ProductID=E.ProductID where E.EventYear='" + DDyear.SelectedValue + "' and C.Accepted = 'Y' and Day='" + s + "'");
                DataTable dt1 = ds1.Tables[0];
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        int poductid = Convert.ToInt16(dt1.Rows[i]["ProductID"].ToString());
                        string Duration1 = dt1.Rows[i]["Duration"].ToString();
                        int signid = Convert.ToInt16(dt1.Rows[i]["SignUpID"].ToString());
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,
                        "update TempCalSignup set [begin]=cast([Time]as datetime)-cast('" + ViewState["Preclass"].ToString() + "' as datetime), [End]=cast([Time]as datetime)+cast('" + ViewState["Postclass"].ToString() + "' as datetime)-cast('00:02:00' as datetime)+cast(DATEADD(mi, ('" + Duration1 + "' - FLOOR('" + Duration1 + "')) * 60, DATEADD(hh, FLOOR('" + Duration1 + "'), CAST ('00:00:00' AS TIME))) as datetime) where SignUpID='" + signid + "'");
                    }
                }
            }
        }

        catch (Exception ex)
        {
            //  Response.Write(ex.ToString());
        }
    }
    public string process(object myval)
    {
        if (myval.ToString().Length == 1)
        {
            myval = "northsouth." + "0" + myval.ToString() + "@live.com";
            return myval.ToString();
        }
        else
        {
            return "northsouth." + myval.ToString() + "@live.com";
        }
    }

    public string processpwd(object myval)
    {
        if (myval.ToString().Length == 1)
        {
            myval = "nsfaccount." + "0" + myval.ToString();
            return myval.ToString();
        }
        else
        {
            return "nsfaccount" + myval.ToString();
        }
    }
    public int MaxVRoom(string DyName)
    {
        try
        {
            object oRoomNumber = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text,
                "select Isnull(max(VRoom),0) from TempCalSignup where EventYear='" + DDyear.SelectedValue + "'  and Accepted = 'Y' and  [Day]='" + DyName + "' and EventId=13 ");

            if (!DBNull.Value.Equals(oRoomNumber) && Convert.ToInt32(oRoomNumber) > 0)
            {
                return Convert.ToInt32(oRoomNumber);
            }
            else
            { return 0; }
        }
        catch
        {
        }
        return 1;
    }
    public int CheckDuplicateVRoom(int cycle, int vir, DateTime StartTime, string DyName)
    {
        try
        {
            Object oEndTime = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "sp_chkdup",
                new SqlParameter("@start", StartTime), new SqlParameter("@rownum", rownum),
                 new SqlParameter("@cycle", cycle), new SqlParameter("@vr", vir), new SqlParameter("@da", DyName));

            if (!DBNull.Value.Equals(oEndTime))
            {
                DateTime EndTime = Convert.ToDateTime(oEndTime);
                if (StartTime > EndTime)
                {
                    return 1;
                }

                else
                {
                    return 0;
                }
            }
        }
        catch
        {
        }
        return 1;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
    }

    #region UserDefined Methods
    private string GetPreviousNextQueryByDay(string DayName)
    {  //increased row count from 250 to 400 on  Sep06_2016
        return "Select   top 400 ROW_NUMBER()  over(order by C.Time) AS RowNum,C.SignUpID,  C.Cycle,C.VRoom from TempCalSignup C "
            + " inner Join  EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID  "
            + "  inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and DAY='" + DayName + "'"
            + " and  c.EventID='13'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "'  group BY c.SignUpID,c.Time ,c.MemberID,c.EventYear,c.ProductCode,C.ProductGroupCode,c.Level,c.Accepted, I.FirstName,i.LastName,E.Duration,C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom,c.Time ";
    }

    private string GetQueryByDay(string DayName)
    {
        //increased row count from 250 to 400 on  Sep06_2016
        string Query = "Select top 400 ROW_NUMBER() over(order by  C.Time,E.Duration)  AS RowNum, C.SignUpID, C.MemberID, C.EventYear, C.ProductCode, C.ProductGroupCode, C.Level, c.Accepted,I.FirstName,I.LastName,E.Duration, C.Day,CONVERT(VARCHAR(5), C.Time, 108) as Times,C.Time,convert(varchar(5),C.[Begin],108) as [Begins],C.[Begin],convert(varchar(5),C.[End],108) as [Ends],C.[End],C.Cycle,C.VRoom,0 as Previous,0 as Next,UserID, Pwd as Password, C.Semester from TempCalSignup C "
               + " inner Join  EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID    inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and c.EventID='13' and C.Semester='" + ddlSemester.SelectedValue + "'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "'";
        if (!string.IsNullOrEmpty(DayName))
        {
            Query += "and  Day in ('" + DayName + "')";
        }
        else
        {
            Query = "Select top 400  0 as RowNum, C.SignUpID, C.MemberID, C.EventYear, C.ProductCode, C.ProductGroupCode, C.Level, c.Accepted,I.FirstName,I.LastName,E.Duration,0 as D, C.Day,CONVERT(VARCHAR(5), C.Time, 108) as Times,C.Time,convert(varchar(5),C.[Begin],108) as [Begins],C.[Begin],convert(varchar(5),C.[End],108) as [Ends],C.[End],C.Cycle,C.VRoom,0 as Previous,0 as Next ,UserID,PWD as Password, C.Semester from TempCalSignup C "
              + " inner Join EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID    inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and c.EventID='13'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "' and C.Semester='" + ddlSemester.SelectedValue + "' order by C.Day, C.Time,C.VRoom";
        }
        // + "  group BY c.SignUpID,c.Time ,c.MemberID,c.EventYear,c.ProductCode,c.Level,c.Accepted, I.FirstName,i.LastName,E.Duration,C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom ";
        return Query;
    }
    private DataTable GetVRCalculateProc(DateTime StartTime, int RowNumber)
    {
        try
        {
            DataSet dsVRoom = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(),
                        CommandType.StoredProcedure, "sp_vrcalc", new SqlParameter("@start", StartTime), new SqlParameter("@rownum", RowNumber));
            if (null != dsVRoom && dsVRoom.Tables.Count > 0)
            {
                return dsVRoom.Tables[0];
            }
        }
        catch
        {
        }
        return null;
    }
    private void UpdateCalSignUp(int Cycle, int VRoom, int SignUpID, string DayName)
    {
        try
        {
            DataTable dtVRoomLookUp = (DataTable)ViewState["tblVRoom"];
            DataRow[] drResult;
            string cmdText;
            cmdText = "update TempCalSignup set Cycle=" + Cycle + " ,VRoom=" + VRoom + " where SignUpID=" + SignUpID + " and [Day]='" + DayName + "' AND EventYear='" + DDyear.SelectedValue + "'";

            if (!DBNull.Value.Equals(dtVRoomLookUp))
            {
                drResult = dtVRoomLookUp.Select("VRoom =" + VRoom);
                if (drResult.Length > 0)
                {
                    cmdText = "update TempCalSignup set Cycle=" + Cycle + " ,VRoom=" + VRoom + ", UserID='" + drResult[0]["UserId"].ToString() + "',PWD = '" + drResult[0]["Pwd"].ToString() + "' where SignUpID='" + SignUpID + "' and [Day]='" + DayName + "' AND EventYear='" + DDyear.SelectedValue + "'";

                }
            }
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    private void GetVRoomLookUpDetails()
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT VROOM,UserId,Pwd FROM VirtualRoomLookUp");
            ViewState["tblVRoom"] = ds.Tables[0];

        }
        catch
        {
        }
    }

    #endregion
    protected void excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    protected void DDpre_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["Preclass"] = "00:" + DDpre.SelectedItem.Value + ":00";

    }
    protected void DDpost_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["Postclass"] = "00:" + DDpost.SelectedItem.Value + ":00";
        if (DDpost.SelectedItem.Value == "60")
        {
            ViewState["Postclass"] = "01:00:00";
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        hlinkChapterFunctions.Visible = true;
        Btnsubmit.Visible = true;
        //lblpost.Visible = true;
        DDpost.Visible = true;
        //lblpre.Visible = true;
        DDpre.Visible = true;
        DDyear.Visible = true;
        // Label1.Visible = true;
        //excel.Visible = false;
        gridview_VR.Visible = false;

    }
    protected void saveresultsbtn_Click(object sender, EventArgs e)
    {
        try
        {
            lblNoRecords.Visible = false;
            lblsuccess.Visible = false;
            string checkscheduledornot = "select count(*) from CalSignup where UserId is not null and EventYear=" + hYear.Value + " and Semester='" + ddlSemester.SelectedValue + "' and Accepted='Y'";
            bool isValid = true;

            string strValidation = "select Max(StartDate) as StartDate ,count(*) as Cnt from CoachingDateCal where ScheduleType='Term' and EventYear=" + hYear.Value + " and EventId=13 and Semester='" + ddlSemester.SelectedValue + "'";
            DataSet dsCDC = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strValidation);
            // string sd = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strValidation).ToString();
            if (dsCDC.Tables[0].Rows[0]["Cnt"].ToString() != "0")
            {
                string sd = dsCDC.Tables[0].Rows[0]["StartDate"].ToString();
                DateTime minStartDate;
                if (sd != "")
                {
                    minStartDate = Convert.ToDateTime(sd);
                    //Commented by bindhu on Oct06_2015 to run the coach schedule ( changed the rules 14 days into 2 days                   
                    if (DateTime.Now > minStartDate.Subtract(TimeSpan.FromDays(1)))
                    {
                        //lblalert.Text = "Not allowed schedule changes after coaches start communicating with students";
                        lblalert.Text = "No Virtual Room changes are allowed starting two weeks prior to the classes start date";
                        lblalert.Visible = true;
                        isValid = false;
                    }
                }
            }
            ////if (sd != "")
            ////{
            //    minStartDate = Convert.ToDateTime(sd);
            //    //Commented by bindhu on Oct06_2015 to run the coach schedule ( changed the rules 14 days into 2 days
            //  //  if (DateTime.Now > minStartDate.Subtract(TimeSpan.FromDays(14)))
            //    if (DateTime.Now > minStartDate.Subtract(TimeSpan.FromDays(1)))
            //    {
            //        //lblalert.Text = "Not allowed schedule changes after coaches start communicating with students";
            //        lblalert.Text = "No Virtual Room changes are allowed starting two weeks prior to the classes start date";
            //        lblalert.Visible = true;
            //    }
            //    else
            //    {
            if (isValid)
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, checkscheduledornot).ToString()) > 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtosave();", true);
                }
                else
                {
                    hiddenbtn_Click(hiddenbtn, new EventArgs());
                }
            }
            // }
            //}
            //else
            //{
            //    //alert("error message should be issued");
            //    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtosavedatenull();", true);
            //}
        }
        catch (Exception ex) { Response.Write(ex); }
    }
    protected void btnclick()
    {

    }
    protected void disresults_Click(object sender, EventArgs e)
    {
        lblalert.Visible = false;
        lblsuccess.Visible = false;
        lblNoRecords.Visible = false;
        if (DDyear.SelectedItem.Text == "Select Year")
        {
            lblalert.Text = "Select Year";
            lblalert.Visible = true;
        }
        else
        {
            if ((SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select * from CalSignup where EventYear=" + DDyear.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "'")).Tables[0].Rows.Count == 0)
            {
                //Response.Write("no records"); 
                lblNoRecords.Visible = true;
                lblNoRecords.Text = "No Record Exists!";
                aftersubmit.Visible = false;

            }
            else
            {
                lblalert.Visible = false;
                //increased row count from 250 to 400 on  Sep06_2016
                string strqry = "Select top 400  0 as RowNum, C.SignUpID, C.MemberID, C.EventYear, C.ProductCode, C.ProductGroupCode, C.Level, c.Accepted,I.FirstName,I.LastName,E.Duration,Case Day When 'Saturday' then 1 when 'Sunday' then 2 when 'Monday' then 3 when 'Tuesday' then 4 when 'Wednesday' then 5 when 'Thursday' then 6 when 'Friday' then 7 end as D,C.Day,CONVERT(VARCHAR(5), C.Time, 108) as Times,C.Time,convert(varchar(5),C.[Begin],108) as [Begins],C.[Begin],convert(varchar(5),C.[End],108) as [Ends],C.[End],C.Cycle,C.VRoom,0 as Previous,0 as Next ,UserID,PWD as Password, C.Semester from CalSignup C  inner Join EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID    inner join IndSpouse I on I.AutoMemberID =C.Memberid where e.EventID='13' and C.Semester='" + ddlSemester.SelectedValue + "' and c.EventID='13' and c.EventYear=" + DDyear.SelectedValue + " and C.Accepted='Y' and E.EventYear='" + DDyear.SelectedValue + "' order by C.Day, C.Time,C.VRoom";
                DataSet dsCalSignup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry);
                DisplayDataInGrid(true, dsCalSignup);
                // gridview_VR.DataSource = dsCalSignup;

                // gridview_VR.DataBind();
                saveresultsbtn.Enabled = false;
                aftersubmit.Visible = true;
            }
        }

    }

    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Write("before update");
            string saveqry = "update Calsignup set ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + ",[Begin] = B.[Begin],[End]=B.[End],Cycle=B.Cycle,VRoom=B.VRoom,UserID=B.UserID,PWD=B.PWD from Calsignup A inner join TempCalSignup B on A.SignupId = B.SignupId";


            int i = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, saveqry);
            if (i > 0)
            {
                lblsuccess.Text = "Saved Successfully!";
                lblsuccess.Visible = true;
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(DDyear.SelectedValue);
    }
}























