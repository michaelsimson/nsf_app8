﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ShowVoucher.aspx.vb" Inherits="ShowVoucher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Donation Receipt</title>
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function printdoc() {
            try {
                document.getElementById('<%= btnPrint.ClientID%>').style.visibility = "hidden";
	            document.getElementById('<%= BtnExport.ClientID%>').style.visibility = "hidden";
	            document.getElementById('<%=lblError.ClientID%>').style.visibility = "hidden";
	            document.getElementById('<%=hlinkParentRegistration.ClientID%>').style.visibility = "hidden";
	            //document.getElementById('dgVoucher').childNodes.item(0)..style.visibility = "hidden";
	            window.print();
	            document.getElementById('<%=btnPrint.ClientID%>').style.visibility = "visible";
                 document.getElementById('<%=BtnExport.ClientID%>').style.visibility = "visible";
	            document.getElementById('<%=lblError.ClientID%>').style.visibility = "visible";
	            document.getElementById('<%=hlinkParentRegistration.ClientID%>').style.visibility = "visible";
	        }
            catch (ex) {
                //  alert('Error :' + ex.toString());
            }
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="1" cellpadding="3" style="border: 0; width: 750px; text-Align: center; background-color: White;">
                <tr>
                    <td align="left">
                        <asp:HyperLink ID="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/ManageVoucher.aspx?id=1">Back to Manage Vouchers</asp:HyperLink>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="ItemCenter" align="center"><font face="Arial" size="4">
                        <asp:Label CssClass="ItemCenter" ID="lblHeading" runat="server" Text="Label"></asp:Label>
                    </font>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="font-family: Arial; font-weight: bold; font-size: 14px">
                        <asp:Label ID="lblDetRawData" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="font-family: Arial; font-weight: bold; font-size: 14px">
                        <asp:Label Style="font-family: Arial; font-weight: bold; font-size: 14px" runat="server" ID="lblDepositNo"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="Center" style="font-family: Arial; font-weight: bold; font-size: 14px">&nbsp;<asp:Label Style="font-family: Arial; font-weight: bold; font-size: 14px" runat="server" ID="lblBankAccount"></asp:Label></td>
                </tr>
                <tr>
                    <td align="Center" style="font-family: Arial; font-weight: bold; font-size: 14px">

                        <asp:Label Style="font-family: Arial; font-weight: bold; font-size: 14px" runat="server" ID="lblChaseTransNo"></asp:Label>

                    </td>
                </tr>
                <asp:Panel runat="server" ID="pnlMessage" Visible="false">
                    <tr>
                        <td class="ItemCenter" align="center" colspan="2">
                            <asp:Label ID="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red" Font-Size="Medium"></asp:Label></td>
                    </tr>
                </asp:Panel>
                <tr>
                    <td align="center">

                        <input type="button" runat="server" onclick="return printdoc();" value="Print" id="btnPrint" class="FormButton" />
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <asp:Button ID="BtnExport" runat="server" Text="Export To Excel" OnClick="BtnExport_Click" />

                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnValidate" runat="server" Text="Validate Raw Data with NFG_Transactions" Visible="False" Width="254px" />

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr runat="server" id="TrDetailView" visible="false" bgcolor="#FFFFFF">
                    <td align="center">
                        <div align="center"><span class="title04">Panel 2</span> </div>
                        <asp:DataGrid OnUpdateCommand="grdEditVoucher_UpdateCommand" OnEditCommand="grdEditVoucher_EditCommand" ID="grdEditVoucher" runat="server" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="DonationID"
                            GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4" BorderWidth="3px" BorderStyle="Double"
                            BorderColor="#336666">
                            <HeaderStyle Font-Bold="true" ForeColor="White" />
                            <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                <asp:EditCommandColumn ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
                                <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="DonationID" ReadOnly="true" DataField="DonationID"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-ForeColor="White" ItemStyle-Wrap="false" HeaderText="Name" ReadOnly="true" DataField="DName"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="DepositSlip" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDepositSlip" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositSlip")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDepositSlip" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositSlip")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="15%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Depositdate" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositDate")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Depositdate")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="15%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="DonorType" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDonorType" runat="server" OnPreRender="SetDropDown_DonorType" AutoPostBack="false">
                                            <asp:ListItem>OWN</asp:ListItem>
                                            <asp:ListItem>IND</asp:ListItem>
                                            <asp:ListItem>SPOUSE</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="10%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BusType">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.BusType")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlBusType" OnPreRender="SetDropDown_BusType" AutoPostBack="false">
                                            <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="NRI,Business" Value="NRI,Business"></asp:ListItem>
                                            <asp:ListItem Text="NRI, Ethnic" Value="NRI, Ethnic"></asp:ListItem>
                                            <asp:ListItem Text="NRI, Professional" Value="NRI, Professional"></asp:ListItem>
                                            <asp:ListItem Text="NRI, Other" Value="NRI, Other"></asp:ListItem>
                                            <asp:ListItem Text="Press, India" Value="Press, India"></asp:ListItem>
                                            <asp:ListItem Text="Press, NRI" Value="Press, NRI"></asp:ListItem>
                                            <asp:ListItem Text="Press, US" Value="Press, US"></asp:ListItem>
                                            <asp:ListItem Text="Religious, Temples" Value="Religious, Temples"></asp:ListItem>
                                            <asp:ListItem Text="Religious, Other" Value="Religious, Other"></asp:ListItem>
                                            <asp:ListItem Text="University_College_School" Value="University_College_School"></asp:ListItem>
                                            <asp:ListItem Text="Corporation_LLC" Value="Corporation_LLC"></asp:ListItem>
                                            <asp:ListItem Text="United Way" Value="United Way"></asp:ListItem>
                                            <asp:ListItem Text="Other Non-Profit" Value="Other Non-Profit"></asp:ListItem>
                                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%" ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="IRSCat" ItemStyle-Width="14%" HeaderStyle-Width="14%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIRSCat" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.IRSCat")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlIRSCat" OnPreRender="SetDropDown_IRSCat" AutoPostBack="false">
                                            <asp:ListItem Text="Profit" Value="Profit"></asp:ListItem>
                                            <asp:ListItem Text="Non-proft,501(c)(3)" Value="Non-proft,501(c)(3)"></asp:ListItem>
                                            <asp:ListItem Text="Non-proft,PAC" Value="Non-proft,PAC"></asp:ListItem>
                                            <asp:ListItem Text="Non-proft,Other" Value="Non-proft,Other"></asp:ListItem>
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="14%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DonationType" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDonationType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonationType")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddldonationtype" runat="server" OnPreRender="SetDropDown_donationtype" AutoPostBack="false">
                                            <asp:ListItem Value="1" Text="Unrestricted" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Temp Restricted"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Perm Restricted"></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%" ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEvent" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList DataTextField="Name" DataValueField="EventID" ID="ddlEvent" runat="server" OnPreRender="SetDropDown_Event" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%" ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ChapterCode" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblChapterCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList DataTextField="WebFolderName" DataValueField="ChapterID" ID="ddlChapterCode" runat="server" OnPreRender="SetDropDown_Chapter" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%" ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" ItemStyle-Width="2%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:C}")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:F}")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="2%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Purpose" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPurpose" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Purpose")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList DataTextField="PurposeDesc" DataValueField="PurposeCode" ID="ddlPurpose" runat="server" OnPreRender="SetDropDown_Purpose" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%" ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Project" ItemStyle-Width="2%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProject" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Project")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtProject" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Project")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="2%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Check_date" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:Label>
                                    </ItemTemplate>
                                    <%--<EditItemTemplate>
                                       <asp:TextBox ID="txtDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Check_Number" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TRANSACTION_NUMBER")%>'></asp:Label>
                                    </ItemTemplate>
                                    <%-- <EditItemTemplate>
                                       <asp:TextBox ID="txtCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TRANSACTION_NUMBER")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="Memberid">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMemberid" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="15%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>

                            </Columns>
                        </asp:DataGrid>
                        <asp:Label ID="lblErr" runat="server"></asp:Label>
                        &nbsp; &nbsp;&nbsp;
       
          <asp:Button ID="btnClose" Visible="false" OnClick="btnClose_Click" runat="server" Text="Close Panel 2" />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>




                        <asp:DataGrid ID="DG_DonationChecks" runat="server" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="DonationID"
                            GridLines="Both" HeaderStyle-BackColor="#009900" CellPadding="4" BorderWidth="3px" BorderStyle="Double"
                            BorderColor="#336666">
                            <HeaderStyle Font-Bold="true" ForeColor="White" />
                            <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>

                            <Columns>
                                <asp:BoundColumn HeaderText="DonationID" ReadOnly="true" DataField="DonationID"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Name" ReadOnly="true" DataField="DName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="DepositSlip" DataField="DepositSlip"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Depositdate" DataField="DepositDate"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="DonorType" DataField="DonorType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="BusType" DataField="BusType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="IRSCat" DataField="IRSCat"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="DonationType" DataField="DonationType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Event" DataField="EventName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="WebFolderName" DataField="WebFolderName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Description" DataField="Description"></asp:BoundColumn>
                                <%--<asp:BoundColumn HeaderText="Amount" DataField="Amount"  DataFormatString ="{0:C}"></asp:BoundColumn>
                                --%>
                                <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                    <ItemTemplate>
                                        <div style="text-align: right">
                                            <asp:Label Font-Size="10" runat="server" ID="lblAmount_Raw" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderText="Purpose" DataField="Purpose"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Check_date" DataField="Donationdate"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Check_Number" HeaderStyle-Width="15%" DataField="TRANSACTION_NUMBER"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Memberid" DataField="MemberID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="chapterid" DataField="chapterid" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="AccNo" DataField="AccNo"></asp:BoundColumn>

                                <asp:BoundColumn HeaderText="RestrictionType" DataField="RestrictionType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="BankID" DataField="BankID"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="BankName" DataField="BankName"></asp:BoundColumn>

                            </Columns>
                        </asp:DataGrid>


                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td colspan="4">
                                    <asp:DataGrid Visible="true" ShowFooter="false" runat="server" ID="dgVoucher"
                                        AutoGenerateColumns="False" Width="600px" BorderWidth="1px" CellPadding="4" AllowSorting="True">

                                        <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>
                                            <asp:ButtonColumn ButtonType="LinkButton" CommandName="EditRow" Text="Detail"></asp:ButtonColumn>
                                            <asp:TemplateColumn ItemStyle-Width="80px" HeaderText="Account" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblAccount" Text='<%# DataBinder.Eval(Container, "DataItem.AccNo") %>'></asp:Label>
                                                    <asp:Label Font-Size="10" Visible="false" runat="server" ID="lblDonorType" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType") %>'></asp:Label>
                                                    <asp:Label Font-Size="10" Visible="false" runat="server" ID="lblDonationType" Text='<%# DataBinder.Eval(Container, "DataItem.DonationType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn ItemStyle-Width="30px" HeaderText="EID" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblEventID" Text='<%# DataBinder.Eval(Container, "DataItem.EventID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn ItemStyle-Width="150px" HeaderText="Description" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" HeaderStyle-Wrap="false" HeaderText="Class" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: left">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblClass" Text='<%# DataBinder.Eval(Container,"DataItem.Class") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" Visible="false" HeaderText="DocNum" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: Left">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblDocNum"></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>


                                        </Columns>
                                    </asp:DataGrid>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:DataGrid ShowFooter="false" runat="server" ID="dgVoucherRawData" Visible="false" AutoGenerateColumns="False" Width="600px" BorderWidth="1px" CellPadding="4" AllowSorting="True">
                                        <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>
                                            <asp:ButtonColumn ButtonType="LinkButton" CommandName="EditRow" Text="Detail"></asp:ButtonColumn>
                                            <asp:TemplateColumn ItemStyle-Width="80px" HeaderText="Account" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblAccount" Text='<%# DataBinder.Eval(Container, "DataItem.AccNo") %>'></asp:Label>
                                                    <asp:Label Font-Size="10" Visible="false" runat="server" ID="lblDonorType" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType") %>'></asp:Label>
                                                    <asp:Label Font-Size="10" Visible="false" runat="server" ID="lblDonationType" Text='<%# DataBinder.Eval(Container, "DataItem.DonationType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="80px" HeaderText="Unique_ID" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblUnique_ID" Text='<%# DataBinder.Eval(Container, "DataItem.Unique_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" HeaderText="Description" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" HeaderText="Customer Name" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblCustomerName" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" HeaderStyle-Wrap="false" HeaderText="Class" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: left">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblClass" Text='<%# DataBinder.Eval(Container,"DataItem.Class") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" Visible="false" HeaderText="DocNum" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: Left">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblDocNum"></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Donation" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblDonation" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Donation"),2) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Fees" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblFees" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Fees"),2) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" HeaderText="Meals" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblMeals" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Meals"),2) %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="txt01"></td>
                                <td align="center" class="txt01">
                                    <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
                                </td>
                                <td align="left" class="txt01"></td>
                                <td class="txt01" align="left"></td>
                            </tr>
                            <tr>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left" class="txt01">
                                    <asp:Label ID="lblText" Visible="false" runat="server">Total</asp:Label></td>
                                <td align="right">
                                    <asp:Label ID="lblTotal" runat="server" CssClass="txt01" Visible="false"></asp:Label>&nbsp;&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="left" class="txt01">Purpose </td>
                                <td align="left">: 
                        <asp:Label ID="lblPurpose" runat="server" Text=""></asp:Label></td>
                                <td align="left">
                                    <asp:Label ID="lblReportDate" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:Label ID="lblReport_date" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left" class="txt01">
                                    <asp:Label ID="Requestorlbl" CssClass="txt01" runat="server" Text="Requestor"></asp:Label>
                                </td>
                                <td align="left" class="txt01">
                                    <%--<%=Now.Month.ToString() & "/" & Now.Day.ToString() & "/" & Now.Year.ToString()
                                                                                                         %>: _____________________________--%>
                                    <%-- <td align="center" colspan="2">
                                  <table><tr>--%>:
                                    <asp:Label ID="lblRequester" CssClass="txt01" runat="server"></asp:Label>
                                    <%-- <asp:FormView ID="frmCheckList_BankDeposits" runat="server" Width="500px"  HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" Visible="true">
                <ItemTemplate >
                                    --%> 
                                </td>
                                <td align="left" class="txt01">
                                    <asp:Label ID="lblDatePaid" runat="server" Text="Date"></asp:Label></td>
                                <td class="txt01" align="left">:
                                    <asp:Label ID="lblDDate" runat="server"></asp:Label>
                                    <%--  </ItemTemplate>
                </asp:FormView>
                                    --%></td>
                            </tr>

                            <tr>
                                <td align="left" class="txt01">Reviewer</td>
                                <td align="left" class="txt01">: _____________________________</td>
                                <td align="left" class="txt01">Date</td>
                                <td class="txt01" align="left">:	__________</td>
                            </tr>
                            <tr>
                                <td align="left" class="txt01">Approver</td>
                                <td align="left" class="txt01">: _____________________________</td>
                                <td align="left" class="txt01">Date</td>
                                <td class="txt01" align="left">: __________</td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr style="width: 105%" runat="server">
                    <%--<asp:FormView ID="frmCheckList_ExpChecks" runat="server" Width="500px"  HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" Visible="true">
                <ItemTemplate >--%>

                    <td align="center">

                        <%-- </ItemTemplate>
                </asp:FormView>--%><table id="tblCheckListDep" title="Check List" runat="server" forecolor="Green" visible="false">
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="lblheadngDep" runat="server" ForeColor="Green" Text="Check List for Deposits"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCheckListDep" Text="Check List for Deposits" Width="200px" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblReviewer1" Text="1" runat="server" Width="75px"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblReviewer2" Text="2" runat="server" Width="75px"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblReviewer3" Text="3" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCopyChecksDep" Text="Copy of Checks" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label4" Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label5" Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label6" Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDonaCategoryDep" Text="Donation Categories*" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblChpAssignmentDep" Text="Chapter Assignment" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblBankAssignmentDep" Text="Bank Assignment" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblTotMatchDep" Text="Totals Match" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAmtMatchDep" Text="Amt matches with Bank Stmt" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblNSFServerDep" Text="Amt matches with NSF Server" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDonor_Receipts" Text="Donor Receipts" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSignatureDep" Text="Signature (initials)" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label Text="--------" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2" align="left" colspan="4">
                            <asp:Label ID="Label35" Text="*-individual, Non-Profit Org, Corp;temp restricted, perm restricted, unrestricted" runat="server"></asp:Label></td>
                    </tr>

                </table>
                    </td>
                </tr>
                <%--</td></tr></table>--%>

                <%--<asp:FormView ID="frmCheckList_ExpChecks" runat="server" Width="500px"  HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" Visible="true">
                <ItemTemplate >--%>
                <tr>
                    <td align="center" style="width: 100%">
                        <table id="tblCheckListExp" runat="server" title="Check List" visible="false" style="width: 55%">
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Label ID="lblheadngExp" runat="server" ForeColor="Green" Text="Check List for Expenses"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblCheckListExp" Text="Check List for Expenses" Width="200px" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbl1_Reviewer1" Text="1" runat="server" Width="75px"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbl2_Reviewer2" Text="2" runat="server" Width="75px"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbl3_Reviewer3" Text="3" runat="server" Width="75px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblReceiptExp" Text="Receipts" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="Label1" Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="Label2" Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="Label3" Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblCategorizationExp" Text="Categorization" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblChpAssignmentExp" Text="Chapter Assignment" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblBankAssignmentExp" Text="Bank Assignment" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblTotMatchExp" Text="Totals Match" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblAmtMatchExp" Text="Amt matches with Bank Stmt" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblNSFServerExp" Text="Amt matches with NSF Server" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblSignatureExp" Text="Signature (initials)" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label Text="--------" runat="server"></asp:Label></td>
                            </tr>

                        </table>
                        <%-- </ItemTemplate>
                </asp:FormView>--%>

                        <%--</td></tr></table>--%>
                    </td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>






