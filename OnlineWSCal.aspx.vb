Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

<Serializable()> Public Class OnlineWSCal
    Inherits System.Web.UI.Page

    Private Shared mintOWkShopID As Integer
    Private mintEventYear As Integer
    Private mintEventID As Integer
    Private mstrEventCode As String
    Private mintProductGroupID As Integer

    Private mstrProductGroupcode As String
    Private mintProductID As Integer
    Private mstrProductCode As String
    Private mRegFee As Double
    Private mLateFee As Double

    'Private mdtOWkshopDate2 As Date
    Private mstrEventDate As String 'Date
    Private mstrRegDLDate As String 'Date
    Private mstrLateDLDate As String 'Date
    Private mstrStartTime As String
    Private mstrTime As String
    Private mstrDuration As String
    Private mstrMaxCap As String
    Private strMaxCap As String

    Private mintEventFeesID As Integer
    Private mintTeacherID As Integer
    Protected strEventDate As String
    Protected strRegDLDate As String
    Protected strLateDLDate As String
    Protected strTime As String
    Private strDuration As String

    Private fUpload As FileUpload
    Dim cnTemp As SqlConnection
    Public ChapterID As Integer
    Private strWebinarLink As String, strRecordingLink As String
    Public AccessToken As String = "ZR8zO6GhdSoGiKIXXgd5Ic3z4xQA"
    Public Organizerkey As String = "5516732880916201477"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Session("LoggedIn") = "true"
            'Session("LoginID") = 4240
            'Session("EventId") = 20

            lblerr.ForeColor = Color.Red
            cnTemp = New SqlConnection(Application("ConnectionString"))
            Page.MaintainScrollPositionOnPostBack = True
            If Not Page.IsPostBack = True Then
                hdnAccessToken.Value = AccessToken
                hdnOrganizerKey.Value = Organizerkey
                'lstOWkshops.Attributes.Add("onchange", "EnableDisableEvents")
                hdnLoginId.Value = Session("LoginId").ToString()
                If LCase(Session("LoggedIn")) <> "true" Then

                    Response.Redirect(".\login.aspx?entry=" & Session("entryToken"))
                End If
                LoadYear(ddlEventYear)
                Session("EventID") = 20
                LoadProductGroup(ddlProductGroup)
                LoadProduct(ddlProduct, ddlProductGroup.SelectedValue)
                'LoadProduct(ddlProduct)
                LoadDisplayTime(ddlDisplayTime)
                LoadTeacher(ddlTeacher, ddlEventYear.SelectedValue)
                LoadGrid()
            End If
        Catch ex As Exception
            lblerr.Text = "Invalid Exception.Please login again." & ex.ToString()
        End Try
    End Sub
    Private Sub LoadYear(ByVal ddlObject As DropDownList)
        Dim Year As Integer = Now.Year - 1
        For i As Integer = 0 To 4
            ddlObject.Items.Insert(i, New ListItem(Year + i, Year + i))
        Next
        If Session("EventYear") Is Nothing Then
            ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText(Now.Year))
        Else
            ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText(Session("EventYear")))
        End If
        'If Now.Month < 5 Then
        '    ddlObject.SelectedIndex = 0 'Year = Now.Year - 1
        'Else
        '    ddlObject.SelectedIndex = 1 'Year = Now.Year
        'End If
    End Sub

    Private Sub LoadProductGroup(ByVal ddlObject As DropDownList)
        Dim StrSQL As String = ""
        StrSQL = "Select Distinct P.ProductGroupId,P.ProductGroupCode,P.Name from ProductGroup P Inner Join EventFees EF on EF.EventID=P.EventID and EF.ProductGroupID =P.ProductGroupID  where EF.EventID =" & Session("EventID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ddlObject.Items.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select ProductGroup", 0))
            ElseIf ddlObject.Items.Count = 1 Then
                LoadProduct(ddlProduct, ddlObject.SelectedValue)
            End If
        Else
            lblerr.Text = "No ProductGroups present for the Event."
        End If
    End Sub

    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ProductGroupID As Integer)
        Dim StrSQL As String = ""
        Try
            lblerr.Text = ""

            ddlObject.Items.Clear()
            ddlObject.Enabled = False

            StrSQL = "Select Distinct P.ProductId,P.ProductCode,P.Name from Product P Inner Join EventFees EF on EF.EventID=P.EventID and EF.ProductGroupID =P.ProductGroupID  and P.ProductID= EF.ProductID where EF.EventID =" & Session("EventID") & " "
            If (ProductGroupID <> "0") Then
                StrSQL = StrSQL & " and P.ProductGroupID =" & ProductGroupID & ""
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlObject.DataSource = ds
                ddlObject.DataBind()
                If ddlObject.Items.Count > 1 Then
                    ddlObject.Enabled = True
                    If (hdnisPrdEnable.Value = "False") Then
                        ddlObject.Enabled = False
                    End If
                    ddlObject.Items.Insert(0, New ListItem("Select Product", 0))
                ElseIf ddlObject.Items.Count = 1 Then
                    Dim StringSQL As String = "Select Distinct EventFeesID from EventFees where EventYear =" & ddlEventYear.SelectedValue & " and EventID=" & Session("EventID") & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID = " & ddlProduct.SelectedValue
                    hdnEventFeesID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StringSQL)

                End If
            Else
                lblerr.Text = "No Products present for the Event."
            End If
        Catch ex As Exception
            'Response.Write(StrSQL)
        End Try
    End Sub

    Private Sub LoadDisplayTime(ByVal ddlObject As DropDownList)
        Try

            Dim dt As DataTable
            dt = New DataTable()
            Dim dr As DataRow
            Dim StartTime As DateTime = "8:00 AM " 'Now()
            dt.Columns.Add("ddlText", Type.GetType("System.String"))
            dt.Columns.Add("ddlValue", Type.GetType("System.String"))
            ' "8:00:00 AM " To "12:00:00 AM "
            While StartTime <= "8:00:00 PM "
                dr = dt.NewRow()

                dr("ddlText") = StartTime.ToString("h:mmtt").ToString()
                dr("ddlValue") = StartTime.ToString("h:mmtt").ToString()

                dt.Rows.Add(dr)
                StartTime = StartTime.AddHours(0.5) '.AddMinutes(60)
            End While

            If dt.Rows.Count > 0 Then
                ddlObject.DataSource = dt
                ddlObject.DataTextField = "ddlText".ToString()
                ddlObject.DataValueField = "ddlValue".ToString()
                ddlObject.DataBind()
            End If
            'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
            'ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
            ddlObject.Items.Insert(0, New ListItem("TBD", ""))

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        lblerr.Text = ""
        lblFileErr.Text = ""
        LoadProductGroup(ddlProductGroup)
        LoadProduct(ddlProduct, ddlProductGroup.SelectedValue)
        LoadGrid()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        If ddlProductGroup.SelectedValue > 0 Then
            LoadProduct(ddlProduct, ddlProductGroup.SelectedValue)
        Else
            lblerr.Text = "Please select Product."
            ddlProduct.Enabled = False
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'LoadTeacher(ddlTeacher, ddlEventYear.SelectedValue)
        Try
            lblerr.Text = ""
            Dim StrSQL As String = "Select Distinct EventFeesID from EventFees where EventYear =" & ddlEventYear.SelectedValue & " and EventID=" & Session("EventID") & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID = " & ddlProduct.SelectedValue
            hdnEventFeesID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)

            Dim cmdText As String = " select ProductGroupId from Product where ProductId=" & ddlProduct.SelectedValue & ""
            Dim Pgid As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
            ddlProductGroup.SelectedValue = Pgid

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub btnDeleteWSCal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim WSCalId As Integer = Convert.ToInt32(mintOWkShopID)
            Dim str As String = "Select count(*) from Registration_OnlineWkshop where OnlineWSCalId=" + WSCalId.ToString()
            If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str) > 0) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg3", "alert('You cannot delete at this time since we had the registration');", True)
            Else
                str = "Delete from OnlineWSCal where OnlineWSCalId=" + WSCalId.ToString()
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, str)
                lblerr.ForeColor = Color.Green
                lblerr.Text = "Deleted Successfully."
                LoadGrid()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click
        Dim StrInsert As String = ""
        lblerr.ForeColor = Color.Red
        lblFileErr.Text = ""
        Try
            If ddlProductGroup.SelectedItem.Value = 0 Then
                lblerr.Text = "Please Select Product Group"
                Exit Sub
            ElseIf ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Product."
                Exit Sub
            ElseIf ddlProduct.Items.Count < 1 Then
                lblerr.Text = "No Products present to add."
                Exit Sub
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Time."
                Exit Sub
            ElseIf ddlDuration.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Duration."
                Exit Sub
            ElseIf ddlTeacher.SelectedIndex = 0 Then 'txtName.Text = "" Then
                lblerr.Text = "Please Select Teacher"
                Exit Sub
            ElseIf txtMaxCap.Text = "" Then
                lblerr.Text = "Please enter max. capacity."
                Exit Sub
            End If
            If (txtStartDate.Text = "" Or txtStartDate.Text.Trim = "TBD") Then
                lblerr.Text = "Please enter Date"
                Exit Sub
            End If
            If (txtStartDate.Text <> "" And txtStartDate.Text.Trim <> "TBD") Then
                If Convert.ToDateTime(txtStartDate.Text) < Now.Date() Then
                    lblerr.Text = "Start Date should be future date"
                    Exit Sub
                Else
                    lblerr.Text = ""
                End If
            End If
            If (txtRegDeadline.Text = "" Or txtRegDeadline.Text.Trim = "TBD") Then
                lblerr.Text = "Please enter Registration Deadline Date"
                Exit Sub
            End If
            If (txtLateRegDeadline.Text = "" Or txtLateRegDeadline.Text.Trim = "TBD") Then
                lblerr.Text = "Please enter Late Registration Deadline Date"
                Exit Sub
            End If
            If (txtRegDeadline.Text <> "" And txtRegDeadline.Text.Trim <> "TBD") And (txtLateRegDeadline.Text <> "" And txtLateRegDeadline.Text.Trim <> "TBD") Then 'ddlRegDeadline.SelectedIndex > 1 And ddlLateRegDL.SelectedIndex > 1 Then
                If Convert.ToDateTime(txtLateRegDeadline.Text) < Convert.ToDateTime(txtRegDeadline.Text) Then
                    lblerr.Text = "Late Reg Deadline Date must be greater than Deadline date"
                    Exit Sub
                End If
            End If

            Dim StrCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "'")
            Dim StrTeachCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "' and ( ProductGroupId<>" & ddlProductGroup.SelectedValue & " Or ProductID<>" & ddlProduct.SelectedValue & ") and TeacherID=" & ddlTeacher.SelectedValue)
            If StrCount > 0 Then
                lblerr.Text = "Online Workshop already exists for the selected date and time."
                Exit Sub
            ElseIf StrTeachCount > 0 Then
                lblerr.Text = "Online Workshop already exists with the same date and time for different product for the selected volunteer."
                Exit Sub
            Else
                If hdnEventFeesID.Value.Length = 0 Then
                    lblerr.Text = "EventFees details has not been inserted for the year " & ddlEventYear.SelectedValue & "."
                    Exit Sub
                End If

                If (FileUpLoad1.HasFile) Then
                    If IsValidFileName(ddlEventYear.SelectedValue.ToString, ddlProductGroup.SelectedValue, FileUpLoad1, txtStartDate.Text, ddlProduct.SelectedValue) = False Then
                        lblFileErr.Visible = True
                        Exit Sub
                        'error msg
                    End If
                End If
                StrInsert = StrInsert & "Insert into OnlineWSCal(TeacherID,EventYear,EventID,ProductGroupId,ProductGroupCode,ProductID,ProductCode,Date,Time,Duration,RegistrationDeadline,LateFeeDeadLine,EventFeesID,EventFee,CreatedDate,CreatedBy,MaxCap,FileName,WebinarLink, RecordingLink) Values"
                StrInsert = StrInsert & "(" & ddlTeacher.SelectedValue & "," & ddlEventYear.SelectedValue & "," & Session("EventID") & "," & ddlProductGroup.SelectedValue & ",(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & ddlProductGroup.SelectedValue & " and EventId =" & Session("EventID") & ")," & ddlProduct.SelectedValue & ",(Select Top 1 ProductCode from Product  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & "  and EventId =" & Session("EventID") & "),'" & txtStartDate.Text & "','" & ddlDisplayTime.Text & "'," & ddlDuration.SelectedValue & ",'" & txtRegDeadline.Text & "','" & txtLateRegDeadline.Text & "', " & hdnEventFeesID.Value & ",(Select RegFee from EventFees where EventFeesID=" & hdnEventFeesID.Value & "),GETDATE()," & Session("LoginID") & "," & txtMaxCap.Text & ","
                If sFileName.Length = 0 Then
                    StrInsert = StrInsert & " NULL"
                Else
                    StrInsert = StrInsert & " '" & sFileName & "'"
                End If
                StrInsert = StrInsert & ",'" & txtWebinarLink.Text & "','" & txtRecordingLink.Text & "')"

                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrInsert) > 0 Then
                    If (FileUpLoad1.HasFile) Then
                        SaveFile(FileUpLoad1)
                    End If
                    lblerr.ForeColor = Color.Green
                    lblerr.Text = "Added Successfully."
                    LoadGrid()
                End If



            End If
        Catch ex As Exception
            'Response.Write(ex.ToString() & StrInsert)
            lblerr.Text = "Not Added. " + ex.ToString()
        End Try
    End Sub
    Private Sub SaveFile(objFileUpload As FileUpload)

        If (objFileUpload.HasFile) Then
            Dim path As String = "~/IncludeOnlineWS"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            objFileUpload.SaveAs(Server.MapPath(path) + "/" + sFileName)
        Else
            lblFileErr.Text = "No uploaded file"
        End If
    End Sub
    Dim sFileName As String = ""
    Private Function IsValidFileName(yr As String, pgroupid As Integer, objfupload As FileUpload, evDT As Date, prdID As Integer) As Boolean
        Dim strPGCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & pgroupid)
        Dim strPrdCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Top 1 ProductCode from Product where ProductGroupId=" & pgroupid & " and ProductID = " & prdID & "")
        '  Dim ext As String = System.IO.Path.GetExtension(objfupload.FileName) '= FileUpLoad1.PostedFile.FileName.
        Dim strDt As String = evDT.Month & evDT.Day & evDT.Year
        Dim strDt2 As String = ""
        If evDT.Month < 10 Then
            'strDt2 = "0" & evDT.Month & evDT.Day & evDT.Year
            strDt2 = "0" & evDT.Month.ToString()
        Else
            strDt2 = evDT.Month.ToString()
        End If
        If evDT.Day < 10 Then
            strDt2 = strDt2 & "0" & evDT.Day.ToString()
        Else
            strDt2 = strDt2 & evDT.Day.ToString()
        End If
        strDt2 = strDt2 & evDT.Year.ToString()

        'Hidden by sims as per Mr. Praveen's request
        'sFileName = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt & ".zip" ' + ext
        Dim ext As String = System.IO.Path.GetExtension(objfupload.FileName)
        Dim OrgFileName = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt & "" & ext & ""
        sFileName = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt & "" & ext & ""
        'Hidden by sims as per Mr.Praveen's comments
        'Dim sFName2 As String = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt2 & ".zip"
        Dim sFName2 As String = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt2 & "" & ext & ""
        'My.Computer.FileSystem.RenameFile("C:\Test.txt", "SecondTest.txt")
        ' If objfupload.FileName.ToString() <> sFileName And objfupload.FileName.ToString() <> sFName2 Then
        If OrgFileName <> sFileName And OrgFileName <> sFName2 Then
            lblFileErr.Visible = False
            lblFileErr.Text = " Workshop book name must be like '" & sFileName & "' Or " & sFName2
            Return False
        End If
        sFileName = sFName2
        'sFileName = yr & "_" & strPrdCode & "_OWorkShopBook_" & strDt2 & ".zip"
        'If objfupload.FileName.ToString() <> sFileName Then
        '    lblFileErr.Visible = False
        '    lblFileErr.Text = " Workshop book name must be like '" & sFileName
        '    Return False
        'End If
        Return True
    End Function
    Private Sub LoadGrid()
        Try
            Dim StrSql As String = " Select OnlineWSCalID,IsNull(EventFeesID,0) as EventFeesID,(Select FirstName + '' + LastName from IndSpouse where autoMemberID =TeacherID) as Teacher, (Select Email from IndSpouse where autoMemberID =TeacherID) as TeacherEmail,TeacherID,EventYear,EventID,(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=OWS.ProductGroupId and EventId =" & Session("EventID") & ") as ProductGroupCode, (Select Top 1 Name from ProductGroup where ProductGroupId=OWS.ProductGroupId and EventId =" & Session("EventID") & ") as ProductGroupName,(Select Top 1 ProductCode from Product where ProductGroupId=OWS.ProductGroupId and ProductId=OWS.ProductId and EventId =" & Session("EventID") & ") as ProductCode,(Select Top 1 Name from Product where ProductGroupId=OWS.ProductGroupId and ProductId=OWS.ProductId and EventId =" & Session("EventID") & ") as ProductName,ProductGroupId,ProductID,Date,ISNULL(CONVERT(VARCHAR(10),CAST(Time as Time),100),'TBD') as Time,Duration,RegistrationDeadline as RegDeadline,LateFeeDeadLine as LateRegDLDate,IsNull(MaxCap,0) as MaxCap,isnull(filename,'') FileName, isnull(WebinarLink,'') WebinarLink, isnull(RecordingLink,'') RecordingLink from OnlineWSCal OWS where EventYear =" & ddlEventYear.SelectedValue & " Order by [Date] desc"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql) ' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID" & ddlProduct.SelectedValue & "") ' and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "'" ")
            If ds.Tables(0).Rows.Count > 0 Then
                grdTarget.Visible = True
                grdTarget.DataSource = ds
                grdTarget.DataBind()

                grdTarget.Columns(1).Visible = False
                Session("dsOWkshop") = ds

                For i As Integer = 0 To grdTarget.Items.Count - 1
                    Try
                        Dim txtEditEventDate As Label = grdTarget.Items(i).FindControl("lblEventDate")
                        Dim txtEditEventDate1 As TextBox
                        If (txtEditEventDate Is Nothing) Then
                            txtEditEventDate1 = grdTarget.Items(i).FindControl("txtEditEventDate")
                        End If
                        Dim strEventDate As String
                        If (txtEditEventDate Is Nothing) Then
                            strEventDate = IIf(txtEditEventDate1.Text = "" Or txtEditEventDate1.Text.Trim = "TBD", "", txtEditEventDate1.Text)
                        Else
                            strEventDate = IIf(txtEditEventDate.Text = "" Or txtEditEventDate.Text.Trim = "TBD", "", txtEditEventDate.Text)
                        End If


                        Dim strTodayDAte = DateTime.Now.ToString("MM/dd/yyyy")
                        '' Dim dtTodayDAte As Date = strTodayDAte
                        Dim EventDate As Date = strEventDate
                        If (DateTime.Now > EventDate) Then
                            grdTarget.Items(i).FindControl("lnkCreateWebinar").Visible = False
                        Else
                            grdTarget.Items(i).FindControl("lnkCreateWebinar").Visible = True

                        End If

                        If (ds.Tables(0).Rows(0)("WebinarLink").ToString() = "") Then
                            grdTarget.Items(i).FindControl("lnkCreateWebinar").Visible = True
                        Else
                            grdTarget.Items(i).FindControl("lnkCreateWebinar").Visible = False
                        End If

                        If (EventDate <= Date.Now) Then
                            txtEditEventDate.Enabled = False



                            Dim txtEditRegDeadLine As TextBox = grdTarget.Items(i).FindControl("txtEditRegDeadLine")
                            txtEditRegDeadLine.Enabled = False
                            Dim txtLateDLDate As TextBox = grdTarget.Items(i).FindControl("txtEditLateFeeDeadLine")
                            txtLateDLDate.Enabled = False

                            Dim ddlTemp As DropDownList = grdTarget.Items(i).FindControl("ddlTeacher")
                            ddlTemp.Enabled = False

                            ddlTemp = grdTarget.Items(i).FindControl("ddlTime")
                            ddlTemp.Enabled = False

                            ddlTemp = grdTarget.Items(i).FindControl("ddlDuration")
                            ddlTemp.Enabled = False

                            ddlTemp = grdTarget.Items(i).FindControl("ddlProductGroup")
                            ddlTemp.Enabled = False

                            ddlTemp = grdTarget.Items(i).FindControl("ddlProduct")
                            ddlTemp.Enabled = False

                            hdnisPrdEnable.Value = "False"

                            txtMaxCap = grdTarget.Items(i).FindControl("txtEditMaxCap")
                            txtMaxCap.Enabled = False

                            txtWebinarLink = grdTarget.Items(i).FindControl("txtWebinarLink")
                            txtWebinarLink.Enabled = False

                            Dim Btn1 As Button = grdTarget.Items(i).FindControl("Button1")
                            Btn1.Enabled = False

                            Dim Btn2 As Button = grdTarget.Items(i).FindControl("Button2")
                            Btn2.Enabled = False

                            Dim Btn3 As Button = grdTarget.Items(i).FindControl("Button3")
                            Btn3.Enabled = False

                            Exit Sub
                        End If
                    Catch ex As Exception
                        hdnisPrdEnable.Value = "True"
                    End Try
                Next


            Else
                grdTarget.Visible = False
                grdTarget.DataSource = Nothing
                grdTarget.DataBind()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetEventDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            If Trim(mstrEventDate) <> "" And Trim(mstrEventDate) <> "TBD" Then
                txtTemp.Text = mstrEventDate
            ElseIf Trim(strEventDate) <> "" And Trim(strEventDate) <> "TBD" Then
                txtTemp.Text = strEventDate
            ElseIf dr.Item("Date").ToString() <> "" Then
                txtTemp.Text = dr.Item("Date")
            Else
                txtTemp.Text = "TBD"
            End If

            If txtTemp.Text <> "TBD" Then
                dr.Item("Date") = txtTemp.Text
            End If
            'Session("editRow")= dr
            Cache("editRow") = dr
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetRegDeadline_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrRegDLDate) <> "" And Trim(mstrRegDLDate) <> "TBD" Then
                txtTemp.Text = mstrRegDLDate
            ElseIf Trim(strRegDLDate) <> "" And Trim(strRegDLDate) <> "TBD" Then
                txtTemp.Text = strRegDLDate
            ElseIf dr.Item("RegDeadLine").ToString() <> "" Then
                txtTemp.Text = dr.Item("RegDeadLine")
            Else
                txtTemp.Text = "TBD"
            End If
            If txtTemp.Text <> "TBD" Then
                dr.Item("RegDeadLine") = txtTemp.Text
            End If
            'If strRegDLDate = "" And (dr.Item("RegDeadLine") = "" Or dr.Item("RegDeadLine") = "TBD") Then
            '    If dr.Item("Date") <> "" And dr.Item("Date") <> "TBD" Then
            '        txtTemp.Text = (Convert.ToDateTime(dr.Item("Date"))).AddDays(-14)
            '    End If
            'ElseIf dr.Item("RegDeadLine") <> "" Then
            '    txtTemp.Text = dr.Item("RegDeadLine")
            'Else
            '    txtTemp.Text = strRegDLDate 'dr.Item("RegDeadLine") '
            '    dr.Item("RegDeadLine") = txtTemp.Text
            'End If
            Cache("editRow") = dr 'Session("editRow")
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub SetLateRegDLDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrLateDLDate) <> "" And Trim(mstrLateDLDate) <> "TBD" Then
                txtTemp.Text = mstrLateDLDate
            ElseIf Trim(strLateDLDate) <> "" And Trim(strLateDLDate) <> "TBD" Then
                txtTemp.Text = strLateDLDate

            ElseIf dr.Item("LateRegDLDate").ToString() <> "" Then
                txtTemp.Text = dr.Item("LateRegDLDate")
            Else
                txtTemp.Text = "TBD"
            End If
            If txtTemp.Text <> "TBD" Then
                dr.Item("LateRegDLDate") = txtTemp.Text
            End If



            'If strLateDLDate = "" And (dr.Item("LateRegDLDate") = "" Or dr.Item("LateRegDLDate") = "TBD") Then
            '    If dr.Item("Date") <> "" And dr.Item("Date") <> "TBD" Then
            '        txtTemp.Text = (Convert.ToDateTime(dr.Item("Date"))).AddDays(-7)
            '    End If
            'ElseIf dr.Item("LateRegDLDate") <> "" Then
            '    txtTemp.Text = dr.Item("LateRegDLDate")
            'Else
            '    txtTemp.Text = strLateDLDate
            'End If
            Cache("editRow") = dr 'Session("editRow")

        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub

    Public Sub Calendar1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar1"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditEventDate"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False

            strEventDate = myTextbox.Text

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            dr.Item("Date") = myTextbox.Text

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub txtWebinarLink_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            If Trim(strWebinarLink) <> "" Then
                txtTemp.Text = strWebinarLink
            ElseIf dr.Item("WebinarLink").ToString() <> "" Then
                txtTemp.Text = dr.Item("WebinarLink")
            Else
                txtTemp.Text = ""
            End If
            Cache("editRow") = dr
        Catch ex As Exception
        End Try
    End Sub

    Public Sub txtRecordingLink_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            If Trim(strRecordingLink) <> "" Then
                txtTemp.Text = strRecordingLink
            ElseIf dr.Item("RecordingLink").ToString() <> "" Then
                txtTemp.Text = dr.Item("RecordingLink")
            Else
                txtTemp.Text = ""
            End If
            Cache("editRow") = dr
        Catch ex As Exception
        End Try
    End Sub

    Public Sub SetMaxCap_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrMaxCap) <> "" Then 'And Trim(mstrLateDLDate) <> "TBD" Then
                txtTemp.Text = mstrMaxCap
            ElseIf Trim(strMaxCap) <> "" Then  'And Trim(strLateDLDate) <> "TBD" Then
                txtTemp.Text = strMaxCap

            ElseIf dr.Item("MaxCap").ToString() <> "" Then
                txtTemp.Text = dr.Item("MaxCap")
            Else
                txtTemp.Text = ""
            End If

            Cache("editRow") = dr 'Session("editRow")

        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Connect to the calendar control
            Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar1")
            'This code will change the state to opposite of what it is now.
            myCalendar.Visible = Not myCalendar.Visible
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Public Sub Calendar2_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar2"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditRegDeadLine"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False

            strRegDLDate = myTextbox.Text
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            dr.Item("RegDeadLine") = myTextbox.Text
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar2")
        myCalendar.Visible = Not myCalendar.Visible
    End Sub

    Public Sub Calendar3_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar3"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditLateFeeDeadLine"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False
            strLateDLDate = myTextbox.Text

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            dr.Item("LateRegDLDate") = myTextbox.Text
        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar3")
        myCalendar.Visible = Not myCalendar.Visible

    End Sub
    Public Sub SetDropDown_Time(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            LoadDisplayTime(ddlTemp)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            If ddlTemp.SelectedIndex <= 0 Then
                If dr.Item("Time") <> "" Then ' ddlTemp.SelectedIndex <= 0 Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
                ElseIf Trim(strTime).ToString() <> "TBD" Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strTime))
                ElseIf Trim(mstrTime).ToString() <> "TBD" Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(mstrTime))
                Else
                    ddlTemp.Items(0).Selected = True
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetDropDown_Teacher(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            LoadTeacher(ddlTemp, dr.Item("EventYear"))

            If dr.Item("TeacherID") > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("TeacherID")))
            Else
                ddlTemp.Items(0).Selected = True

            End If

            'If mintTeacherID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
            '    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(mintTeacherID))
            '   Else
            '    ddlTemp.Items(0).Selected = True

            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetDropDown_ProductGroup(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            LoadProductGroup(ddlTemp)

            'dr.Item("ProductGroupId")
            If dr.Item("ProductGroupId") > 0 Then 'mintProductGroupID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("ProductGroupId"))) 'mintProductGroupID
            Else
                ddlTemp.Items(0).Selected = True
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub ddlDGProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        'Dim rowProdGroupId As Integer = 0
        'Dim rowProdGroupCode As String = ""

        'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductGroupCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductGroupId") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr

        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductId") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGDuration_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("Duration") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("Time") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub

    Public Sub ddlDGTeacher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("TeacherID") = ddlTemp.SelectedItem.Value
        dr.Item("Teacher") = ddlTemp.SelectedItem.Text

        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub


    Public Sub SetDropDown_Product(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0
        Dim rowProductCode As String = ""
        Dim rowProductID As Integer = 0
        'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If

        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If
        If (Not dr.Item("ProductCode") Is DBNull.Value) Then
            rowProductCode = dr.Item("productCode")
        End If
        If (Not dr.Item("ProductId") Is DBNull.Value) Then
            rowProductID = dr.Item("ProductId")
        End If

        If (rowProdGroupId <= 0) Then
            LoadProduct(ddlTemp, rowProdGroupId)
        ElseIf (rowProdGroupId > 0) Then
            LoadProduct(ddlTemp, rowProdGroupId)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
        Else
            ddlTemp.Items.Clear()
        End If

        If dr.Item("ProductID") > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("ProductID")))
        ElseIf rowProductID > 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
        Else
            ddlTemp.Items(0).Selected = True
        End If


    End Sub
    'Public Sub SetDropDown_Product(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
    '        ddlTemp = sender

    '        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
    '        If (dr Is Nothing) Then
    '            Return
    '        End If
    '        LoadProduct(ddlTemp, dr.Item("ProductGroupID"))

    '        If mintProductID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
    '            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(mintProductID))
    '        Else
    '            ddlTemp.Items(0).Selected = True

    '        End If
    '    Catch ex As Exception
    '        ''Response.Write(ex.ToString())
    '    End Try
    'End Sub


    Private Sub LoadTeacher(ByVal ddlTeacher As DropDownList, ByVal EventYear As Integer)
        Dim dsTarget As DataSet
        dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select I.AutoMemberID as TeacherID,I.FirstName + ' ' + I.LastName as Teacher from IndSpouse I Inner join Volunteer V on I.AutoMemberID =V.MemberID where V.RoleID =98")
        'AutoMemberID in "'(Select Distinct TeacherID from OnlineWSCal where EventYear =" & EventYear & ")")
        If Not ddlTeacher Is Nothing Then
            ddlTeacher.DataSource = dsTarget
            ddlTeacher.DataBind()
            ddlTeacher.DataValueField = "TeacherID"
            ddlTeacher.DataTextField = "Teacher"
            ddlTeacher.DataBind()

            If ddlTeacher.Items.Count > 1 Then
                ddlTeacher.Items.Insert(0, "Select Teacher")
            End If
        End If

    End Sub
    Public Sub SetDropDown_Duration(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If

            If dr.Item("Duration").ToString() <> "TBD" Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Duration")))
            ElseIf strDuration Is Nothing Then
                ddlTemp.SelectedValue = strDuration
            ElseIf mstrDuration Is Nothing Then
                ddlTemp.SelectedValue = mstrDuration
            Else
                ddlTemp.Items(0).Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdTarget_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.EditCommand

        Try
            'tblAddOWkshops.Visible = False
            'PnlAddOWkshops.Enabled = False
            lblerr.Text = ""
            lblerr.ForeColor = Color.Red
            grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
            mintOWkShopID = CInt(e.Item.Cells(1).Text)

            'Session("mintOWkShopID") = mintOWkShopID

            mintEventFeesID = CInt(e.Item.Cells(4).Text)
            mintEventYear = CInt(e.Item.Cells(5).Text)
            mintEventID = CInt(e.Item.Cells(6).Text)

            mintProductGroupID = CInt(e.Item.Cells(7).Text)
            mintProductID = CInt(e.Item.Cells(8).Text)
            hdnPgId.Value = mintProductGroupID
            hdnPId.Value = mintProductID
            mstrProductGroupcode = CStr(e.Item.Cells(9).Text)
            mstrProductCode = CStr(e.Item.Cells(10).Text)
            strEventDate = CType(e.Item.Cells(11).FindControl("lblEventDate"), Label).Text
            mstrEventDate = strEventDate
            strTime = CType(e.Item.Cells(12).FindControl("lblTime"), Label).Text
            strDuration = CType(e.Item.Cells(13).FindControl("lblDuration"), Label).Text
            mintTeacherID = CInt(e.Item.Cells(14).Text)
            hdnTeacherId.Value = mintTeacherID

            strRegDLDate = CType(e.Item.Cells(16).FindControl("lblRegDeadLine"), Label).Text
            strLateDLDate = CType(e.Item.Cells(17).FindControl("lblLateFeeDeadLine"), Label).Text
            strMaxCap = CType(e.Item.Cells(18).FindControl("lblMaxCap"), Label).Text

            strWebinarLink = CType(e.Item.Cells(20).FindControl("lblWebinarLink"), Label).Text
            strRecordingLink = CType(e.Item.Cells(20).FindControl("lblRecordingLink"), Label).Text

            If (strWebinarLink <> "") Then
                CType(e.Item.FindControl("lnkCreateWebinar"), LinkButton).Visible = False
            Else
                CType(e.Item.FindControl("lnkCreateWebinar"), LinkButton).Visible = True
            End If



            'disenable delete and copy buttons
            Dim intcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(intcell)

            Dim OWkshop_ds As DataSet = CType(Session("dsOWkshop"), DataSet)
            Dim currentRowIndex As Integer = e.Item.ItemIndex
            Dim OWkshop_Copy As DataSet
            Dim dr As DataRow
            If Not OWkshop_ds Is Nothing Then
                OWkshop_Copy = OWkshop_ds.Copy
                dr = OWkshop_Copy.Tables(0).Rows(currentRowIndex)
            End If
            Cache("editRow") = dr

            CalRow.Value = e.Item.ItemIndex
            LoadGrid()
            'Hide the update controls after the workshop date




        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdTarget_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.UpdateCommand
        Try
            Dim row As Integer = CInt(e.Item.ItemIndex)
            Dim txtEditRegDeadLine As TextBox, txtEditEventDate As TextBox, txtLateDLDate As TextBox, txtMaxCap As TextBox, ddlTemp As DropDownList, blnValidData As Boolean, lblText As Label
            Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer

            Dim PgId As Integer
            Dim PId As Integer
            'Venue
            lblerr.Text = ""
            lblGridError.Text = ""
            lblText = e.Item.FindControl("lblGridEditWSDate")
            '
            txtEditEventDate = e.Item.FindControl("txtEditEventDate")
            mstrEventDate = IIf(txtEditEventDate.Text = "" Or txtEditEventDate.Text.Trim = "TBD", "", txtEditEventDate.Text)
            Session("EventDate") = mstrEventDate

            txtEditRegDeadLine = e.Item.FindControl("txtEditRegDeadLine")
            mstrRegDLDate = IIf(txtEditRegDeadLine.Text = "" Or txtEditRegDeadLine.Text.Trim = "TBD", "", txtEditRegDeadLine.Text)

            txtLateDLDate = e.Item.FindControl("txtEditLateFeeDeadLine")
            mstrLateDLDate = IIf(txtLateDLDate.Text = "" Or txtLateDLDate.Text = "TBD", "", txtLateDLDate.Text)

            ddlTemp = e.Item.FindControl("ddlTeacher")
            Try
                mintTeacherID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)
            Catch ex As Exception
                mintTeacherID = hdnTeacherId.Value
            End Try


            ddlTemp = e.Item.FindControl("ddlTime")
            mstrTime = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)
            intSelEndTime = ddlTemp.SelectedIndex '3 +

            ddlTemp = e.Item.FindControl("ddlDuration")
            mstrDuration = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedValue)

            ddlTemp = e.Item.FindControl("ddlProductGroup")
            mstrProductGroupcode = IIf(ddlTemp.SelectedValue <= 0, "", ddlTemp.SelectedItem.Text)

            ddlTemp = e.Item.FindControl("ddlProductGroup")
            mintProductGroupID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)

            Session("PGId") = mintProductGroupID
            '    Session("EventId") = mintEventID
            PgId = ddlTemp.SelectedValue

            ddlTemp = e.Item.FindControl("ddlProduct")
            mstrProductCode = IIf(ddlTemp.SelectedValue <= 0, "", ddlTemp.SelectedItem.Text)
            PId = ddlTemp.SelectedValue

            ddlTemp = e.Item.FindControl("ddlProduct")
            mintProductID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)
            Session("ProductId") = mintProductID
            txtMaxCap = e.Item.FindControl("txtEditMaxCap")
            mstrMaxCap = IIf(txtMaxCap.Text = "" Or txtMaxCap.Text = "TBD", "", txtMaxCap.Text)

            fUpload = e.Item.FindControl("fupload")
            sFileName = CType(e.Item.FindControl("lblFileName"), Label).Text
            Session("FileName") = sFileName
            ' Session("FileUpload") = fUpload

            txtWebinarLink = e.Item.FindControl("txtWebinarLink")
            strWebinarLink = txtWebinarLink.Text

            txtRecordingLink = e.Item.FindControl("txtRecordingLink")
            strRecordingLink = txtRecordingLink.Text
            Session("RecordingLink") = strRecordingLink

            Dim EventDate As Date = mstrEventDate
            If (EventDate <= Date.Now) Then
                UpdateWokshopAfterDate()
                Exit Sub
            End If

            'mstrEventDate = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ShowAlert()", True)

            ' Else


            blnValidData = False
            lblGridError.Visible = False
            lblGridError.Text = ""

            If mstrEventDate <> "" Then 'And mstrRegDLDate <> "" Then
                If Convert.ToDateTime(mstrEventDate) < Now.Date() Then
                    lblGridError.Text = "Event Date must be Future date"
                End If
            End If
            If mstrRegDLDate <> "" And mstrLateDLDate <> "" Then
                If Convert.ToDateTime(mstrLateDLDate) < Convert.ToDateTime(mstrRegDLDate) Then
                    lblGridError.Text = "Late Reg Deadline Date must be greater than Deadline date"
                    'ElseIf mstrRegDLDate <> "" Then
                    '    If Convert.ToDateTime(mstrEventDate) < Convert.ToDateTime(mstrLateDLDate) Then
                    '        lblGridError.Text = "Start Date must be greater than Late Reg Deadline Date"
                    '    End If
                End If
            End If
            If mstrMaxCap = "" Then 'And mstrRegDLDate <> "" Then
                lblGridError.Text = "Event Date must be Future date"
            End If



            If UpdateWkShops() = False Then
                Exit Sub
            End If
            grdTarget.EditItemIndex = -1


            If lblGridError.Text <> "" Then
                lblGridError.Visible = True
                blnValidData = False
                GoTo FinalProcess
            End If


            'update routine
            'disenable delete buttons
            'Dim intcell As Integer = e.Item.Cells.Count - 2
            'Dim myTableCell As TableCell
            'myTableCell = e.Item.Cells(intcell)
            'Dim btnDelete As Button = myTableCell.Controls(0)
            'btnDelete.Enabled = True

            LoadGrid()
            Exit Sub

FinalProcess:
            grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
            LoadGrid()

            ' End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Function UpdateWkShops() As Boolean
        Try
            lblerr.ForeColor = Color.Red
            lblFileErr.Text = ""
            'Response.Write(StrUpdate)
            Dim StrCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & mintProductGroupID & " and ProductID=" & mintProductID & " and Date ='" & mstrEventDate & "' and Time='" & mstrTime & "' and OnlineWSCalID<>" & mintOWkShopID)
            Dim StrTeachCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and Date ='" & mstrEventDate & "' and Time='" & mstrTime & "' and ( ProductGroupId<>" & mintProductGroupID & " Or ProductID<>" & mintProductID & ") and TeacherID =" & mintTeacherID & " and OnlineWSCalID<>" & mintOWkShopID)

            If (hdnPgId.Value.Trim() = mintProductGroupID.ToString().Trim() And hdnPId.Value.Trim() = mintProductID.ToString().Trim()) Then

            Else
                Dim cmdText As String = "select count(*) from Registration_OnlineWkshop where EventYear=" & ddlEventYear.SelectedValue & " and ProductGroupId=" & hdnPgId.Value & " and ProductId= " & hdnPId.Value & " "
                Dim iCount As Integer = 0
                iCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                If (iCount > 0) Then
                    lblGridError.Text = "You cannot update the product group or product at this time since we had the registration."
                    lblerr.ForeColor = Color.Red

                    Return True
                End If

            End If

            If StrCount > 0 Then
                lblGridError.Text = " Online Workshop already exists for the selected date and Time. "

                Return True
            ElseIf StrTeachCount > 0 Then
                lblGridError.Text = " Online Workshop already exists with the same date and time for different product for the selected volunteer. "
                Return True
            Else
                If fUpload.HasFile Then
                    If IsValidFileName(ddlEventYear.SelectedValue, mintProductGroupID, fUpload, mstrEventDate, mintProductID) = False Then
                        lblFileErr.Visible = True
                        Return False
                        'error msg
                    End If
                End If
                Dim iEventFeesId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull (EventFeesId,0) from EventFees where EventId=" & Session("EventID") & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & mintProductGroupID & " and ProductId=" & mintProductID)

                Dim StrUpdate As String = " Update OnlineWSCal set EventFeesId=" & iEventFeesId & " , TeacherID=" & mintTeacherID & ",ProductGroupCode=(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & mintProductGroupID & " and EventId =" & Session("EventID") & "),ProductGroupId=" & mintProductGroupID & ",ProductCode=(Select Top 1 ProductCode from Product  where ProductGroupId=" & mintProductGroupID & " and ProductId=" & mintProductID & "  and EventId =" & Session("EventID") & "),ProductID=" & mintProductID & ",Date='" & mstrEventDate & "',Time='" & mstrTime & "',Duration=" & mstrDuration & ",RegistrationDeadline='" & mstrRegDLDate & "',LateFeeDeadLine='" & mstrLateDLDate & "',ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & ",MaxCap=" & mstrMaxCap & ", FileName="
                If sFileName.Length = 0 Then
                    StrUpdate = StrUpdate & "NULL"
                Else

                    StrUpdate = StrUpdate & "'" & sFileName & "'"
                End If
                StrUpdate = StrUpdate & ", WebinarLink='" & strWebinarLink & "'" & ", RecordingLink='" & strRecordingLink & "'"

                StrUpdate = StrUpdate & " where OnlineWSCalID = " & mintOWkShopID
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate)
                If fUpload.HasFile Then
                    SaveFile(fUpload)
                End If
                lblerr.ForeColor = Color.Green
                lblerr.Text = "Updated successfully"
                lblGridError.Text = ""
                'tblAddPrepclubs.Visible = True
                PnlAddOWkshops.Enabled = True
                lblerr.Visible = True

                Dim cmdText As String = "select count(*) from Registration_OnlineWkshop where OnlineWSCalID = " & mintOWkShopID & " "
                Dim iCount As Integer = 0
                iCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                If (iCount > 0) Then
                    cmdText = " update Registration_OnlineWkshop set EventDate='" & mstrEventDate & "' where OnlineWSCalID = " & mintOWkShopID & ""
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                End If

                Return True

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString()) ' & " Update OnlineWSCal set TeacherID=" & mintTeacherID & ",ProductGroupId=" & mintProductGroupID & ",ProductID=" & mintProductID & ",Date='" & mstrEventDate & "',Time='" & mstrTime & "',Duration=" & mstrDuration & ",RegistrationDeadline='" & mstrRegDLDate & "',LateFeeDeadLine='" & mstrLateDLDate & "',ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & " where OnlineWSCalID=" & mintOWkShopID)
            Throw New Exception(ex.Message)
            lblerr.Text = ex.Message
            lblerr.Visible = True
            Return False
        End Try
    End Function

    Protected Sub grdTarget_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTarget.ItemDataBound
        Try
            Dim dsTarget As New DataSet
            ' First, make sure we're NOT dealing with a Header or Footer row
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                'Now, reference the PushButton control that the Delete ButtonColumn has been rendered to
                Dim rownum As Integer
                rownum = e.Item.ItemIndex + 1
                Dim intOWkShopID As String = CStr(e.Item.Cells(1).Text)
                Dim ddlTeacherTemp As DropDownList
                ddlTeacherTemp = e.Item.Cells(15).FindControl("ddlTeacher")
                'Try
                '    ddlTeacherTemp.SelectedValue = hdnTeacherId.Value
                'Catch ex As Exception

                'End Try
                dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select FirstName + ' ' + LastName as Teacher from IndSpouse where AutoMemberID in (Select Distinct TeacherID from OnlineWSCal where EventYear =" & e.Item.Cells(4).Text & ")")
                If Not ddlTeacherTemp Is Nothing Then
                    ddlTeacherTemp.DataSource = dsTarget
                    ddlTeacherTemp.DataBind()
                    If ddlTeacherTemp.Items.Count > 1 Then
                        ddlTeacherTemp.Items.Insert(0, "Select Teacher")
                    End If
                End If

                Dim ddlTimeTemp As DropDownList
                ddlTimeTemp = e.Item.Cells(12).FindControl("ddlTime")
                If Not ddlTimeTemp Is Nothing Then
                    LoadDisplayTime(ddlTimeTemp)
                End If

                Dim ddlDurationTemp As DropDownList
                ddlDurationTemp = e.Item.Cells(13).FindControl("ddlDuration")
                Dim intPrepClubIDCell As Integer = 1
                Dim PrepClubIDCell As TableCell = e.Item.Cells(intPrepClubIDCell)
                Dim strValue As String = PrepClubIDCell.Text

                Dim EventDate As Date = CType(e.Item.FindControl("lblEventDate"), Label).Text
                If (EventDate <= Date.Now) Then
                    'hidden by Sims as per Mr. PRaveen's comments
                    ' e.Item.Cells(0).Text = ""
                End If

                Dim strFileName As String = CType(e.Item.FindControl("lblFileName"), Label).Text
                If (strFileName.Length = 0) Then
                    CType(e.Item.FindControl("imgBtnDownload"), ImageButton).Visible = False
                End If
                Dim str As String = "Select count(*) from Registration_OnlineWkshop where OnlineWSCalId=" + intOWkShopID
                If (Convert.ToInt32(SqlHelper.ExecuteScalar(cnTemp, CommandType.Text, str)) > 0) Then
                    e.Item.Cells(2).Text = ""
                End If

            End If
        Catch ex As Exception
            '  Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdTarget_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.CancelCommand
        Try
            grdTarget.EditItemIndex = -1
            LoadGrid()
            lblGridError.Visible = False
            PnlAddOWkshops.Enabled = True
        Catch ex As Exception
            ''Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub grdTarget_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles grdTarget.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Download") Then
            Dim strFileName As String = CType(e.Item.FindControl("lblFileName"), Label).Text
            If (strFileName.Length > 0) Then
                Dim path As String = "~/IncludeOnlineWS/"
                Dim filePath As String = String.Concat(Server.MapPath(path), strFileName)
                Context.Items("DOWNLOAD_FILE_PATH") = filePath
                Server.Transfer("downloader.aspx")
            End If
        ElseIf cmdName = "DeleteLevel" Then
            Dim WSCalId As Integer = CInt(e.CommandArgument)
            mintOWkShopID = CInt(WSCalId)
            Dim str As String = "Select count(*) from Registration_OnlineWkshop where OnlineWSCalId=" + WSCalId.ToString()
            If (Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str)) > 0) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg1", "alert('You cannot delete at this time since we had the registration');", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg2", "DeleteConfirm();", True)

            End If
        ElseIf cmdName = "CreateW" Then

            Dim onlineWsCalId As String = CType(e.Item.FindControl("lblOnlineWsCalId"), Label).Text
            Dim eventId As String = CType(e.Item.FindControl("lbleventId"), Label).Text
            hdnEventId.Value = eventId
            Dim eventyear As String = CType(e.Item.FindControl("lblEventYear"), Label).Text
            hdnEventYear.Value = eventyear
            Dim productGroupid As String = CType(e.Item.FindControl("lblPgId"), Label).Text
            hdnPgId.Value = productGroupid
            Dim productId As String = CType(e.Item.FindControl("lblPrdId"), Label).Text
            hdnPId.Value = productId
            Dim productGroupName As String = CType(e.Item.FindControl("lblProductGroupName"), Label).Text

            Dim productName As String = CType(e.Item.FindControl("lblProductName"), Label).Text
            Dim teacherid As String = CType(e.Item.FindControl("lblTeacherID"), Label).Text
            hdnTeacherId.Value = teacherid
            Dim teacherName As String = CType(e.Item.FindControl("lblteacherName"), Label).Text
            hdnTeacherName.Value = teacherName
            Dim teacherEmail As String = CType(e.Item.FindControl("lblTeacherEmail"), Label).Text
            hdnTeacherEmail.Value = teacherEmail
            Dim startDate As String = CType(e.Item.FindControl("lblStartDate"), Label).Text
            startDate = Convert.ToDateTime(startDate).ToShortDateString()
            hdnStartDate.Value = startDate
            Dim startTime As String = CType(e.Item.FindControl("lblStartTime"), Label).Text
            startTime = Convert.ToDateTime(startTime).ToString("HH:mm:ss")
            hdnStartTime.Value = startTime
            Dim duration As String = CType(e.Item.FindControl("lblDurationWeb"), Label).Text
            Dim ProductGroupCode As String = CType(e.Item.FindControl("lblPGCode"), Label).Text
            Dim productCode As String = CType(e.Item.FindControl("lblPCode"), Label).Text

            Dim Title As String = String.Empty
            If (productId <> "") Then
                Title = eventyear + " Online Workshop " + " - " + productCode + " " + productName

            Else
                Title = eventyear + " Online Workshop " + " - " + ProductGroupCode + " " + productGroupName
            End If
            hdnWebinarTitle.Value = Title
            hdnDuration.Value = duration
            Dim cmdText As String = String.Empty
            cmdText = "select count(*) from WebinarSessions where Eventyear='" + eventyear + "' and productgroupid=" + productGroupid + " and ProductId=" + productId + " and StartDate='" + startDate + "' and StartTime ='" + startTime + "'"

            Dim iCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
            If (iCount > 0) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ShowDuplicateAlert();", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "checkTimeConfictsOfWebinar();", True)
            End If
            '   Response.Redirect("TestGoToWebinar.aspx?Id=" + onlineWsCalId + "")

        End If
    End Sub



    Protected Sub btnUpdateDocument_Click(sender As Object, e As EventArgs)


        If fUpload.HasFile Then
            If IsValidFileName(ddlEventYear.SelectedValue, Session("PGId").ToString(), fUpload, Session("EventDate").ToString(), Session("ProductId").ToString()) = False Then
                lblFileErr.Visible = True
                Exit Sub
                'error msg
            End If
        End If
        sFileName = Session("FileName").ToString()
        If (sFileName = "") Then
        Else
            Try
                sFileName = fUpload.FileName.ToString()
            Catch ex As Exception

            End Try
        End If
        strRecordingLink = Session("RecordingLink").ToString()
        Dim iEventFeesId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull (EventFeesId,0) from EventFees where EventId=" & Session("EventID") & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & Session("PGId").ToString() & " and ProductId=" & Session("ProductId").ToString())

        Dim StrUpdate As String = " Update OnlineWSCal set ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & ", FileName="
        If sFileName.Length = 0 Then
            StrUpdate = StrUpdate & "NULL"
        Else
            StrUpdate = StrUpdate & "'" & sFileName & "'"
        End If
        StrUpdate = StrUpdate & ", RecordingLink='" & strRecordingLink & "'"

        StrUpdate = StrUpdate & " where OnlineWSCalID = " & mintOWkShopID
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate)
        If fUpload.HasFile Then
            SaveFile(fUpload)
        End If
        lblerr.ForeColor = Color.Green
        lblerr.Text = "Updated successfully"
        lblGridError.Text = ""
        'tblAddPrepclubs.Visible = True
        PnlAddOWkshops.Enabled = True
        lblerr.Visible = True

        grdTarget.EditItemIndex = -1


        If lblGridError.Text <> "" Then
            lblGridError.Visible = True

        End If

        LoadGrid()

    End Sub
    Public Sub UpdateWokshopAfterDate()
        If fUpload.HasFile Then
            If IsValidFileName(ddlEventYear.SelectedValue, Session("PGId").ToString(), fUpload, Session("EventDate").ToString(), Session("ProductId").ToString()) = False Then
                lblFileErr.Visible = True
                Exit Sub
                'error msg
            End If
        End If
        sFileName = Session("FileName").ToString()
        If (sFileName = "") Then
            sFileName = fUpload.FileName.ToString()
        Else

        End If
        strRecordingLink = Session("RecordingLink").ToString()
        Dim iEventFeesId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull (EventFeesId,0) from EventFees where EventId=" & Session("EventID") & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & Session("PGId").ToString() & " and ProductId=" & Session("ProductId").ToString())

        Dim StrUpdate As String = " Update OnlineWSCal set ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & ", FileName="
        If sFileName.Length = 0 Then
            StrUpdate = StrUpdate & "NULL"
        Else
            StrUpdate = StrUpdate & "'" & sFileName & "'"
        End If
        StrUpdate = StrUpdate & ", RecordingLink='" & strRecordingLink & "'"

        StrUpdate = StrUpdate & " where OnlineWSCalID = " & mintOWkShopID
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate)
        If fUpload.HasFile Then
            SaveFile(fUpload)
        End If
        lblerr.ForeColor = Color.Green
        lblerr.Text = "Updated successfully"
        lblGridError.Text = ""
        'tblAddPrepclubs.Visible = True
        PnlAddOWkshops.Enabled = True
        lblerr.Visible = True

        grdTarget.EditItemIndex = -1


        If lblGridError.Text <> "" Then
            lblGridError.Visible = True

        End If

        LoadGrid()
    End Sub
End Class
