﻿<%@ Page Language="C#" CodeFile="SBVBJudgeCopyHTML.aspx.cs" Inherits="SBVBJudgeCopyHTML" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>NorthSouth Foundation</title>
         <script type="text/javascript">
             function printpage() {
                 //Get the print button and put it into a variable
                 var printButton = document.getElementById("printForm");
                 //Set the print button visibility to 'hidden' 
                 printButton.style.visibility = 'hidden';
                 //Print the page content
                 window.print()
                 //Set the print button to 'visible' again 
                 //[Delete this line if you want it to stay hidden after printing]
                 printButton.style.visibility = 'visible';
             }
</script>

 
    </head>
<body>
     <form style="text-align:right;" id="printForm" runat="server">
  <a href="#" id="lnkPrint" onclick="printpage()">Click here</a> to Print
    </form>
      <asp:Panel ID="pTableHeader_SB" runat="server" Visible="false">
             <table border="1" cellpadding="1" cellspacing="0" width="100%" style="height:100%">
                 <tr style="font-size:10px;" >
                     <th width="5%"><b>S.No</b></th>
                     <th width="7.5%"><b>Round</b></th>
                     <th width="6%"><b>Level</b></th>
                     <th width="13%"><b>Word</b></th>
                     <th width="5%"><b>POS</b></th>
                     <th width="14%"><b>Pronunciation</b></th>
                     <th width="6.5%"><b>Root</b></th>
                     <th width="24%"><b>Definitions</b></th>
                     <th width="25%"><b>Sentence</b></th>
                  </tr></table></asp:Panel>
    <table border="1" cellpadding="2" cellspacing="0" width="100%"  style="height:100%;">
    <asp:Repeater ID="rpSB_JudgeCopy" Visible="true" runat="server"  >
                     <ItemTemplate>
                        
                        </ItemTemplate>
                         </asp:Repeater> 
        </table> 
     <asp:Panel ID="pTableHeader" runat="server" Visible="false">
             <table border="1" cellpadding="1" cellspacing="0" width="100%" style="height:100%">
                 <tr style="font-size:10px;" >
                     <th width="7%"><b>S.No</b></th>
                     <th width="7%"><b>Round</b></th>
                     <th width="7%"><b>Level</b></th>
                     <th width="28%"><b>Word</b></th>
                     <th width="5%"><b>Ans</b></th>
                     <th width="53%"><b>Meaning</b></th>
                  </tr></table></asp:Panel>
     <table border="1" cellpadding="1" cellspacing="0" width="100%"  style="height:100%">
     <asp:Repeater ID="rpToPDF" Visible="true" runat="server" OnItemDataBound="rpToPDF_ItemDataBound" >
                     <ItemTemplate>
                   </ItemTemplate>
                         </asp:Repeater></table>


     <asp:DataList ID="rpStuPhase2" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Width="1000px">
            <ItemTemplate>
            
            </ItemTemplate>
         

        </asp:DataList>
  </body>
