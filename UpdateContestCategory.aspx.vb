﻿Imports System.IO
Imports System.Web.UI.Page
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class UpdateContestCategory
    Inherits System.Web.UI.Page
    'Dim Serverpath As String = "F:\NSF root\" '"d:\inetpub\wwwroot\northsouth\"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlPropYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlPropYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlPropYear.Items(0).Selected = True
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            ddlEventYear.Items(1).Selected = True
            loadgrid()
            LoadProductGroup()
        End If
    End Sub


    Function getProductGroupID(ByVal ProductGroupCode As String) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from ProductGroup where EventID=1 and ProductGroupCode='" & ProductGroupCode & "'")
    End Function
    'Function getProductcode(ByVal ProductID As Integer) As String
    '    Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    'End Function

    'Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
    '    Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    'End Function
   
    Private Sub LoadProductGroup()
        ddlProduct.Items.Clear()
        Dim strSql As String = "SELECT   ProductGroupCode, Name from ProductGroup WHERE EventId in (1,2) group By ProductGroupCode, Name order by Name"
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlProductGroup.DataSource = drproductgroup
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count < 1 Then
            lblErr.Text = "No Product is opened."
        ElseIf ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Items.Insert(0, New ListItem("Select Product Group", "0"))
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        Else
            ddlProductGroup.Enabled = False
            LoadProductID()
        End If
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try
                strSql = "Select ProductCode, Name from Product where EventID in (1,2) and ProductGroupCode ='" & ddlProductGroup.SelectedValue & "' Group by ProductCode, Name order by ProductCode, Name"
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product", "0"))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    'loadGrid(True)
                End If
            Catch ex As Exception
                lblErr.Text = strSql & ex.ToString
            End Try
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        ddlProduct.Items.Clear()
        LoadProductID()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Add new Event
        lblErr.Text = ""
        If ddlProductGroup.SelectedValue = "0" Then
            lblErr.Text = "Please Select a Product Group"
            Exit Sub
        ElseIf ddlProduct.SelectedValue = "0" Then
            lblErr.Text = "Please Select a Product"
            Exit Sub
        ElseIf ddlPh2Split.SelectedValue = "N" And (ddlPhase2_Reg.SelectedValue = "Y" Or ddlPhase3_Reg.SelectedValue = "Y") Then
            lblErr.Text = "No Phase2 split allowed for Regionals."
            Exit Sub
        ElseIf ddlGradeTo.SelectedValue = "0" Then 'ddlGradeBased.SelectedValue = "1" And 
            lblErr.Text = "Please Select Valid Grade from &  Grade to"
            Exit Sub
            'ElseIf ddlGradeBased.SelectedValue = "0" And ddlAgeTo.SelectedValue = "0" Then
            'lblErr.Text = "Please Select Valid Age from & Age to"
            'Exit Sub
        ElseIf Val(txtRegFee.Text) < 1 Then
            lblErr.Text = "Please Enter Valid Reg.RegFee"
            Exit Sub
        ElseIf Val(txtRegTax.Text) < 1 Then
            lblErr.Text = "Please Enter Tax Deduction Amount"
            Exit Sub
        ElseIf Val(txtNationalFinalsFee.Text) < 1 Then
            lblErr.Text = "Please Enter Valid Nat.RegFee"
            Exit Sub
        ElseIf ddlPh2Split.SelectedValue = "N" And (ddlPhase2_Fin.SelectedValue = "Y" Or ddlPhase3_Fin.SelectedValue = "Y") Then
            lblErr.Text = "No Phase2 split allowed for Nationals."
            Exit Sub
        ElseIf Val(txtNatTax.Text) < 1 Then
            lblErr.Text = "Please Enter Tax Deduction Amount for Nationals"
            Exit Sub
        ElseIf Val(txtRegTax.Text) > 100 Then
            lblErr.Text = "Invalid Reg. Tax Deduction Amount. Between 0 & 100"
            Exit Sub
        ElseIf Val(txtNatTax.Text) > 100 Then
            lblErr.Text = "Invalid Nat. Tax Deduction Amount. Between 0 & 100"
            Exit Sub
        Else
            lblErr.Text = ""
        End If
        Dim sqlstr As String = ""
        If btnAdd.Text = "Add" Then
            'Add New
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from ContestCategory where ContestYear=" & ddlEventYear.Text & " AND  ProductGroupCode ='" & ddlProductGroup.SelectedValue & "' AND ContestCode = '" & ddlProduct.SelectedValue & "'") < 1 Then
                sqlstr = "INSERT INTO ContestCategory(ContestCode, ContestDesc, Ph1Split,Ph2Split,Phase2_Reg,Phase3_Reg,"
                'If ddlGradeBased.SelectedValue = "1" Then
                sqlstr = sqlstr & " GradeFrom, GradeTo,"
                ' Else
                'sqlstr = sqlstr & " AgeFrom, AgeTo,"
                'End If
                sqlstr = sqlstr & " RegionalStatus, RegionalFee,Phase2_Fin,Phase3_Fin, NationalFinalsStatus,NationalFinalsFee, DownloadLink, DownloadLinkNational, ContestYear, CategoryID, NationalSelectionCriteria, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,CreateDate, CreatedBy,GradeBased) VALUES("
                sqlstr = sqlstr & "'" & ddlProduct.SelectedValue & "','" & ddlProduct.SelectedItem.Text & "','" & ddlPh1Split.SelectedValue & "','" & ddlPh2Split.SelectedValue & "','" & ddlPhase2_Reg.SelectedValue & "','" & ddlPhase3_Reg.SelectedValue & "',"
                'If ddlGradeBased.SelectedValue = "1" Then
                sqlstr = sqlstr & ddlGradeFrom.SelectedValue & "," & ddlGradeTo.SelectedValue
                'Else
                'sqlstr = sqlstr & ddlAgeFrom.SelectedValue & "," & ddlAgeTo.SelectedValue
                'End If
                sqlstr = sqlstr & ",'" & ddlRegStatus.SelectedValue & "'," & txtRegFee.Text & ",'" & ddlPhase2_Fin.SelectedValue & "','" & ddlPhase3_Fin.SelectedValue & "','" & ddlNationalStatus.SelectedValue & "'," & txtNationalFinalsFee.Text & ",'" & txtRegDownloadLink.Text & "','" & txtNationalDownloadLink.Text & "'," & ddlEventYear.SelectedValue & "," & getProductGroupID(ddlProductGroup.SelectedValue) & ",'" & ddlNatSelCriteria.SelectedValue & "'," & getProductGroupID(ddlProductGroup.SelectedValue) & ",'" & ddlProductGroup.SelectedValue & "'," & txtRegTax.Text & "," & txtNatTax.Text & ",GetDate()," & Session("LoginID") & ",1)"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                clear()
                traddUpdate.Visible = False
                lblError.Text = "Added Sucessfully"
                loadgrid()
            Else
                lblErr.Text = "This record already Exist"
            End If
        ElseIf lblEventFeesID.Text.Length > 0 Then
            'Update Event
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from ContestCategory where ContestYear=" & ddlEventYear.Text & " AND ProductGroupCode ='" & ddlProductGroup.SelectedValue & "' AND ContestCode = '" & ddlProduct.SelectedValue & "' AND ContestCategoryID not in (" & lblEventFeesID.Text & ")") < 1 Then
                sqlstr = "Update ContestCategory SET ContestCode='" & ddlProduct.SelectedValue & "', ContestDesc='" & ddlProduct.SelectedItem.Text & "', Ph1Split='" & ddlPh1Split.SelectedValue & "', Ph2Split='" & ddlPh2Split.SelectedValue & "',Phase2_Reg='" & ddlPhase2_Reg.SelectedValue & "',Phase3_Reg='" & ddlPhase3_Reg.SelectedValue & "'"
                'If ddlGradeBased.SelectedValue = "1" Then
                sqlstr = sqlstr & ", GradeFrom=" & ddlGradeFrom.SelectedValue & ", GradeTo=" & ddlGradeTo.SelectedValue
                'Else
                'sqlstr = sqlstr & ", AgeFrom=" & ddlAgeFrom.SelectedValue & ", AgeTo=" & ddlAgeTo.SelectedValue
                'End If
                sqlstr = sqlstr & ", RegionalStatus='" & ddlRegStatus.SelectedValue & "', RegionalFee=" & txtRegFee.Text & ",Phase2_Fin='" & ddlPhase2_Fin.SelectedValue & "',Phase3_Fin='" & ddlPhase3_Fin.SelectedValue & "', NationalFinalsStatus='" & ddlNationalStatus.SelectedValue & "',NationalFinalsFee=" & txtNationalFinalsFee.Text & ", DownloadLink='" & txtRegDownloadLink.Text & "',"
                sqlstr = sqlstr & " DownloadLinkNational='" & txtNationalDownloadLink.Text & "', ContestYear=" & ddlEventYear.SelectedValue & ", CategoryID=" & getProductGroupID(ddlProductGroup.SelectedValue) & ", NationalSelectionCriteria='" & ddlNatSelCriteria.SelectedValue & "', ProductGroupId=" & getProductGroupID(ddlProductGroup.SelectedValue) & ", ProductGroupCode='"
                sqlstr = sqlstr & ddlProductGroup.SelectedValue & "', TaxDedRegional=" & txtRegTax.Text & ", TaxDedNational=" & txtNatTax.Text & ", ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",GradeBased=1  WHERE ContestCategoryID = " & lblEventFeesID.Text

                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                    clear()
                    traddUpdate.Visible = False
                    lblError.Text = "Updated Sucessfully"
                    loadgrid()
                Catch ex As Exception
                    Response.Write(sqlstr)
                End Try
            Else
                lblErr.Text = "This record already Exist, So you can't update"
            End If
        End If

    End Sub


    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadgrid()
    End Sub

    Private Sub DGEventFees_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGEventFees.ItemCreated
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.FindControl("lbtnRemove"), LinkButton) '(e.Item.Cells(1).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub DGEventFees_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim EventFeesID As Integer = CInt(e.Item.Cells(3).Text)
        lblError.Text = ""
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from ContestCategory Where ContestCategoryID=" & EventFeesID & "")
                loadgrid()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            btnAdd.Text = "Update"
            lblEventFeesID.Text = EventFeesID.ToString()
            loadforUpdate(EventFeesID)
        End If
    End Sub

    Private Sub loadforUpdate(ByVal EventFeesID As Integer)
        ' For update 
        traddUpdate.Visible = True
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select  ContestCategoryID, ContestCode, ContestDesc, ISNull(Ph1Split,'') as Ph1Split, ISNull(Ph2Split,'') as Ph2Split, ISNull(Phase2_Reg,'') as Phase2_Reg, ISNull(Phase3_Reg,'') as Phase3_Reg,GradeFrom, GradeTo, RegionalStatus, RegionalFee, ISNull(Phase2_Fin,'') as Phase2_Fin, ISNull(Phase3_Fin,'') as Phase3_Fin ,NationalFinalsStatus, NationalFinalsFee, DownloadLink, DownloadLinkNational, ContestYear, CategoryID, NationalSelectionCriteria, CriteriaValue, ProductGroupId, ProductGroupCode, CreateDate, CreatedBy, ModifyDate, ModifiedBy, TaxDedRegional, TaxDedNational from ContestCategory Where ContestCategoryID=" & EventFeesID & "")
        ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(ds.Tables(0).Rows(0)("ProductGroupCode")))
        LoadProductID()
        ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByText(ds.Tables(0).Rows(0)("ContestDesc")))
        'ddlGradeBased.SelectedIndex = ddlGradeBased.Items.IndexOf(ddlGradeBased.Items.FindByValue(ds.Tables(0).Rows(0)("GradeBased")))
        ddlPh1Split.SelectedIndex = ddlPh1Split.Items.IndexOf(ddlPh1Split.Items.FindByValue(ds.Tables(0).Rows(0)("Ph1Split")))
        ddlPh2Split.SelectedIndex = ddlPh2Split.Items.IndexOf(ddlPh2Split.Items.FindByValue(ds.Tables(0).Rows(0)("Ph2Split")))

        ddlPhase2_Reg.SelectedIndex = ddlPhase2_Reg.Items.IndexOf(ddlPhase2_Reg.Items.FindByValue(ds.Tables(0).Rows(0)("Phase2_Reg")))
        ddlPhase3_Reg.SelectedIndex = ddlPhase3_Reg.Items.IndexOf(ddlPhase3_Reg.Items.FindByValue(ds.Tables(0).Rows(0)("Phase3_Reg")))

        'If ds.Tables(0).Rows(0)("GradeBased") = "True" Then
        ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByValue(ds.Tables(0).Rows(0)("GradeFrom")))
        ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByValue(ds.Tables(0).Rows(0)("GradeTo")))
        'Else
        'ddlAgeFrom.SelectedIndex = ddlAgeFrom.Items.IndexOf(ddlAgeFrom.Items.FindByValue(ds.Tables(0).Rows(0)("AgeFrom")))
        'ddlAgeTo.SelectedIndex = ddlAgeTo.Items.IndexOf(ddlAgeTo.Items.FindByValue(ds.Tables(0).Rows(0)("AgeTo")))
        'End If
        ddlRegStatus.SelectedIndex = ddlRegStatus.Items.IndexOf(ddlRegStatus.Items.FindByValue(ds.Tables(0).Rows(0)("RegionalStatus")))
        txtRegFee.Text = setamt(ds.Tables(0).Rows(0)("RegionalFee"))

        If Not ds.Tables(0).Rows(0)("DownloadLink") Is DBNull.Value Then
            txtRegDownloadLink.Text = ds.Tables(0).Rows(0)("DownloadLink")
        End If
        txtRegTax.Text = ds.Tables(0).Rows(0)("TaxDedRegional")

        ddlPhase2_Fin.SelectedIndex = ddlPhase2_Fin.Items.IndexOf(ddlPhase2_Fin.Items.FindByValue(ds.Tables(0).Rows(0)("Phase2_Fin")))
        ddlPhase3_Fin.SelectedIndex = ddlPhase3_Fin.Items.IndexOf(ddlPhase3_Fin.Items.FindByValue(ds.Tables(0).Rows(0)("Phase3_Fin")))

        If Not ds.Tables(0).Rows(0)("NationalSelectionCriteria") Is DBNull.Value Then
            ddlNatSelCriteria.SelectedIndex = ddlNatSelCriteria.Items.IndexOf(ddlNatSelCriteria.Items.FindByValue(ds.Tables(0).Rows(0)("NationalSelectionCriteria")))
        Else
            ddlNatSelCriteria.SelectedIndex = ddlNatSelCriteria.Items.IndexOf(ddlNatSelCriteria.Items.FindByValue("M"))
        End If
        ddlNationalStatus.SelectedIndex = ddlNationalStatus.Items.IndexOf(ddlNationalStatus.Items.FindByValue(ds.Tables(0).Rows(0)("NationalFinalsStatus")))
        txtNationalFinalsFee.Text = setamt(ds.Tables(0).Rows(0)("NationalFinalsFee").ToString())
        If Not ds.Tables(0).Rows(0)("DownloadLinkNational") Is DBNull.Value Then
            txtNationalDownloadLink.Text = ds.Tables(0).Rows(0)("DownloadLinkNational")
        End If
        txtNatTax.Text = ds.Tables(0).Rows(0)("TaxDedNational")
    End Sub

    Private Function setamt(ByVal amount As String) As Decimal
        Dim decim As String
        decim = FormatCurrency(amount)
        Return decim.Replace("$", "").Replace(",", "")
    End Function

    Private Sub loadgrid()
        Dim strSQL As String = "SELECT     ContestCategoryID, ContestCode, ContestDesc, Ph1Split,Ph2Split, Phase2_Reg,Phase3_Reg,GradeFrom, GradeTo, RegionalStatus, RegionalFee, Phase2_Fin,Phase3_Fin,NationalFinalsStatus, "
        strSQL = strSQL & " NationalFinalsFee, DownloadLink, ContestYear, CategoryID, NationalSelectionCriteria, CriteriaValue, ProductGroupId, ProductGroupCode, DownloadLinkNational, "
        strSQL = strSQL & " TaxDedRegional, TaxDedNational FROM ContestCategory WHERE ContestYear = " & ddlEventYear.SelectedValue
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        DGEventFees.DataSource = dt
        DGEventFees.DataBind()
        'DGEventFees.Columns(12).Visible = False

        If (Count < 1) Then
            lblErr.Text = "No record available."
        Else
            lblErr.Text = ""
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        'add new button
        clear()
        lblError.Text = ""
        traddUpdate.Visible = True
    End Sub

    Private Sub clear()
        btnAdd.Text = "Add"
        lblEventFeesID.Text = ""
        ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByValue("0"))
        ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByValue("0"))
        ddlPh1Split.SelectedIndex = ddlPh1Split.Items.IndexOf(ddlPh1Split.Items.FindByValue("Y"))
        ddlPh2Split.SelectedIndex = ddlPh2Split.Items.IndexOf(ddlPh2Split.Items.FindByValue("Y"))
        ddlPhase2_Reg.SelectedIndex = ddlPhase2_Reg.Items.IndexOf(ddlPhase2_Reg.Items.FindByValue("N"))
        ddlPhase3_Reg.SelectedIndex = ddlPhase3_Reg.Items.IndexOf(ddlPhase3_Reg.Items.FindByValue("N"))

        'ddlAgeFrom.SelectedIndex = ddlAgeFrom.Items.IndexOf(ddlAgeFrom.Items.FindByValue("0"))
        'ddlAgeTo.SelectedIndex = ddlAgeTo.Items.IndexOf(ddlAgeTo.Items.FindByValue("0"))
        'ddlGradeBased.SelectedIndex = ddlGradeBased.Items.IndexOf(ddlGradeBased.Items.FindByValue("1"))
        ddlRegStatus.SelectedIndex = ddlRegStatus.Items.IndexOf(ddlRegStatus.Items.FindByValue("Active"))
        ddlPhase2_Fin.SelectedIndex = ddlPhase2_Fin.Items.IndexOf(ddlPhase2_Fin.Items.FindByValue("N"))
        ddlPhase3_Fin.SelectedIndex = ddlPhase3_Fin.Items.IndexOf(ddlPhase3_Fin.Items.FindByValue("N"))

        ddlNationalStatus.SelectedIndex = ddlNationalStatus.Items.IndexOf(ddlNationalStatus.Items.FindByValue("Active"))
        ddlNatSelCriteria.SelectedIndex = ddlNatSelCriteria.Items.IndexOf(ddlNatSelCriteria.Items.FindByValue("M"))
        txtRegDownloadLink.Text = String.Empty
        txtNationalDownloadLink.Text = String.Empty
        txtRegFee.Text = "0.00"
        txtNationalFinalsFee.Text = "0.00"
        txtNatTax.Text = "0"
        txtRegTax.Text = "0"
        ddlProduct.Items.Clear()
        LoadProductGroup()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub

    Protected Sub btnRelpicateSel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRelpicateSel.Click
        traddUpdate.Visible = False
        If Val(ddlPropYear.SelectedValue) <= Now.Year Then
            lblError.Text = "Replication not Allowed"
            Exit Sub
        End If
        Dim item As DataGridItem
        Dim flag As Boolean = False
        Dim EventIds As String = "0"
        Dim EventFeesID As Integer
        Dim sqlstr As String
        Dim chk As CheckBox
        For Each item In DGEventFees.Items
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                EventFeesID = CInt(item.Cells(3).Text)
                EventIds = EventIds & "," & EventFeesID.ToString
                flag = True
            End If
        Next
        lblfileErr.Text = ""
        Try
            If flag = True Then
                'Insert coding
                'Response.Write("select ContestCode, ContestDesc,Ph1Split,Ph2Split, AgeFrom, AgeTo, GradeFrom, GradeTo, RegionalStatus, RegionalFee, NationalFinalsStatus, NationalFinalsFee, DownloadLink, IsNull(DownloadLinkNational,DownloadLink) as DownloadLinkNational, " & ddlPropYear.SelectedValue & "  as ContestYear, CategoryID, NationalSelectionCriteria, CriteriaValue, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,GETDATE()," & Session("LoginID") & ",GradeBased FROM ContestCategory Where ContestCategoryID in (" & EventIds & ")")
                'Exit Sub
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ContestCode, ContestDesc,Ph1Split,Ph2Split,Phase2_Reg,Phase3_Reg, AgeFrom, AgeTo, GradeFrom, GradeTo, RegionalStatus, RegionalFee, Phase2_Fin,Phase3_Fin,NationalFinalsStatus, NationalFinalsFee, DownloadLink, IsNull(DownloadLinkNational,DownloadLink) as DownloadLinkNational, " & ddlPropYear.SelectedValue & "  as ContestYear, CategoryID, NationalSelectionCriteria, CriteriaValue, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,GETDATE()," & Session("LoginID") & ",GradeBased FROM ContestCategory Where ContestCategoryID in (" & EventIds & ")") ' GradeBased
                '"SELECT EventID, EventCode, " & ddlPropYear.SelectedValue & "  as EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo, RegFee, LateFee,TaxDedRegional,GETDATE()," & Session("LoginID") & " FROM EVENTFEES WHERE EVENTFEESID in (" & EventIds & ")")
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from ContestCategory where ContestYear=" & ds.Tables(0).Rows(i)("ContestYear") & " AND ProductGroupID =" & ds.Tables(0).Rows(i)("ProductGroupID") & " AND ContestCode = '" & ds.Tables(0).Rows(i)("ContestCode") & "'") < 1 Then
                            sqlstr = "INSERT INTO ContestCategory(ContestCode, ContestDesc,"
                            'If ds.Tables(0).Rows(i)("GradeBased").ToString.Trim = "True" Then
                            sqlstr = sqlstr & " Ph1Split,Ph2Split,Phase2_Reg,Phase3_Reg,GradeFrom, GradeTo,"
                            'Else
                            'sqlstr = sqlstr & " GradeBased,AgeFrom, AgeTo,"
                            'End If
                            sqlstr = sqlstr & "RegionalStatus, RegionalFee,Phase2_Fin,Phase3_Fin, NationalFinalsStatus,NationalFinalsFee, DownloadLink, DownloadLinkNational, ContestYear, CategoryID, NationalSelectionCriteria, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,CreateDate, CreatedBy,GradeBased) VALUES("
                            sqlstr = sqlstr & "'" & ds.Tables(0).Rows(i)("ContestCode") & "','" & ds.Tables(0).Rows(i)("ContestDesc") & "',"
                            'If ds.Tables(0).Rows(i)("GradeBased").ToString.Trim = "True" Then
                            sqlstr = sqlstr & "'" & ds.Tables(0).Rows(i)("Ph1Split") & "','" & ds.Tables(0).Rows(i)("Ph2Split") & "','" & ds.Tables(0).Rows(i)("Phase2_Reg") & "','" & ds.Tables(0).Rows(i)("Phase3_Reg") & "','" & ds.Tables(0).Rows(i)("GradeFrom") & "','" & ds.Tables(0).Rows(i)("GradeTo") & "','"
                            'Else
                            'sqlstr = sqlstr & "0," & ds.Tables(0).Rows(i)("AgeFrom") & "," & ds.Tables(0).Rows(i)("AgeTo") & ",'"
                            'End If
                            sqlstr = sqlstr & ds.Tables(0).Rows(i)("RegionalStatus") & "'," & ds.Tables(0).Rows(i)("RegionalFee") & ",'" & ds.Tables(0).Rows(i)("Phase2_Fin") & "','" & ds.Tables(0).Rows(i)("Phase3_Fin") & "','" & ds.Tables(0).Rows(i)("NationalFinalsStatus") & "'," & ds.Tables(0).Rows(i)("NationalFinalsFee") & "," & IIf(ds.Tables(0).Rows(i)("DownloadLink") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("DownloadLink").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text) & "'") & ","
                            sqlstr = sqlstr & IIf(ds.Tables(0).Rows(i)("DownloadLinkNational") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("DownloadLinkNational").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text) & "'") & "," & ds.Tables(0).Rows(i)("ContestYear") & "," & ds.Tables(0).Rows(i)("CategoryID") & "," & IIf(ds.Tables(0).Rows(i)("NationalSelectionCriteria") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("NationalSelectionCriteria") & "'") & "," & ds.Tables(0).Rows(i)("ProductGroupId") & ",'" & ds.Tables(0).Rows(i)("ProductGroupCode") & "'," & ds.Tables(0).Rows(i)("TaxDedRegional") & "," & ds.Tables(0).Rows(i)("TaxDedNational") & ",GetDate()," & Session("LoginID") & ",'" & ds.Tables(0).Rows(i)("GradeBased").ToString() & "')"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                            Try
                                If Not (ds.Tables(0).Rows(i)("DownloadLink") Is DBNull.Value) Then
                                    copyfile(ds.Tables(0).Rows(i)("DownloadLink").ToString(), ds.Tables(0).Rows(i)("DownloadLink").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text))
                                End If
                                If Not (ds.Tables(0).Rows(i)("DownloadLinkNational") Is DBNull.Value) Then
                                    copyfile(ds.Tables(0).Rows(i)("DownloadLinkNational").ToString(), ds.Tables(0).Rows(i)("DownloadLinkNational").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text))
                                End If
                            Catch ex As Exception
                                lblfileErr.Text = lblfileErr.Text & "<BR>" & ex.ToString()
                                'Exit Sub
                            End Try
                        End If
                    Next
                    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(ddlPropYear.SelectedValue))
                    loadgrid()
                    lblError.Text = "selected Item replicated successfully"
                End If

            Else
                lblError.Text = "No record is selected"
            End If
        Catch ex As Exception
            Response.Write(sqlstr & ex.ToString())
        End Try
    End Sub

    Protected Sub btnRelpicateAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRelpicateAll.Click
        traddUpdate.Visible = False
        If Val(ddlPropYear.SelectedValue) <= Now.Year Then
            lblError.Text = "Replication not Allowed"
            Exit Sub
        End If
        Dim item As DataGridItem
        Dim flag As Boolean = False
        Dim EventIds As String = "0"
        Dim EventFeesID As Integer
        Dim sqlstr As String
        For Each item In DGEventFees.Items
            EventFeesID = CInt(item.Cells(3).Text)
            EventIds = EventIds & "," & EventFeesID.ToString
        Next
        'Insert coding
        lblfileErr.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ContestCode, ContestDesc, Ph1Split,Ph2Split,Phase2_Reg,Phase3_Reg,GradeFrom, GradeTo, RegionalStatus, RegionalFee, Phase2_Fin,Phase3_Fin, NationalFinalsStatus, NationalFinalsFee, DownloadLink,IsNull(DownloadLinkNational,DownloadLink) as DownloadLinkNational, " & ddlPropYear.SelectedValue & "  as ContestYear, CategoryID, NationalSelectionCriteria, CriteriaValue, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,GETDATE()," & Session("LoginID") & ",Gradebased FROM ContestCategory Where ContestCategoryID in (" & EventIds & ")")
        If ds.Tables(0).Rows.Count > 0 Then
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from ContestCategory where ContestYear=" & ds.Tables(0).Rows(i)("ContestYear") & " AND ProductGroupID =" & ds.Tables(0).Rows(i)("ProductGroupID") & " AND ContestCode = '" & ds.Tables(0).Rows(i)("ContestCode") & "'") < 1 Then
                    sqlstr = "INSERT INTO ContestCategory(ContestCode, ContestDesc, Ph1Split ,Ph2Split ,Phase2_Reg,Phase3_Reg" 'GradeBased
                    'If ds.Tables(0).Rows(i)("GradeBased").ToString.Trim = "True" Then
                    sqlstr = sqlstr & " GradeFrom, GradeTo,"
                    'Else
                    'sqlstr = sqlstr & " AgeFrom, AgeTo,"
                    ' End If
                    sqlstr = sqlstr & "RegionalStatus, RegionalFee,Phase2_Fin,Phase3_Fin, NationalFinalsStatus,NationalFinalsFee, DownloadLink, DownloadLinkNational, ContestYear, CategoryID, NationalSelectionCriteria, ProductGroupId, ProductGroupCode, TaxDedRegional, TaxDedNational,CreateDate, CreatedBy,GradeBased) VALUES("
                    sqlstr = sqlstr & "'" & ds.Tables(0).Rows(i)("ContestCode") & "','" & ds.Tables(0).Rows(i)("ContestDesc") & "','" & ds.Tables(0).Rows(i)("Ph1Split") & "','" & ds.Tables(0).Rows(i)("Ph2Split") & "','" & ds.Tables(0).Rows(i)("Phase2_Reg") & "','" & ds.Tables(0).Rows(i)("Phase3_Reg") & "',"
                    'If ds.Tables(0).Rows(i)("GradeBased").ToString.Trim = "True" Then
                    sqlstr = sqlstr & ds.Tables(0).Rows(i)("GradeFrom") & "," & ds.Tables(0).Rows(i)("GradeTo") & ",'"
                    'Else
                    ' sqlstr = sqlstr & ds.Tables(0).Rows(i)("AgeFrom") & "," & ds.Tables(0).Rows(i)("AgeTo") & ",'"
                    ' End If
                    sqlstr = sqlstr & ds.Tables(0).Rows(i)("RegionalStatus") & "'," & ds.Tables(0).Rows(i)("RegionalFee") & ",'" & ds.Tables(0).Rows(i)("Phase2_Fin") & "','" & ds.Tables(0).Rows(i)("Phase3_Fin") & "','" & ds.Tables(0).Rows(i)("NationalFinalsStatus") & "'," & ds.Tables(0).Rows(i)("NationalFinalsFee") & "," & IIf(ds.Tables(0).Rows(i)("DownloadLink") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("DownloadLink").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text) & "'") & ","
                    sqlstr = sqlstr & IIf(ds.Tables(0).Rows(i)("DownloadLinkNational") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("DownloadLinkNational").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text) & "'") & "," & ds.Tables(0).Rows(i)("ContestYear") & "," & ds.Tables(0).Rows(i)("CategoryID") & "," & IIf(ds.Tables(0).Rows(i)("NationalSelectionCriteria") Is DBNull.Value, "Null", "'" & ds.Tables(0).Rows(i)("NationalSelectionCriteria") & "'") & "," & ds.Tables(0).Rows(i)("ProductGroupId") & ",'" & ds.Tables(0).Rows(i)("ProductGroupCode") & "'," & ds.Tables(0).Rows(i)("TaxDedRegional") & "," & ds.Tables(0).Rows(i)("TaxDedNational") & ",GetDate()," & Session("LoginID") & ",'" & ds.Tables(0).Rows(i)("GradeBased") & "')"

                    Try
                        If Not (ds.Tables(0).Rows(i)("DownloadLink") Is DBNull.Value) Then
                            copyfile(ds.Tables(0).Rows(i)("DownloadLink").ToString(), ds.Tables(0).Rows(i)("DownloadLink").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text))
                        End If
                        If Not (ds.Tables(0).Rows(i)("DownloadLinkNational") Is DBNull.Value) Then
                            copyfile(ds.Tables(0).Rows(i)("DownloadLinkNational").ToString(), ds.Tables(0).Rows(i)("DownloadLinkNational").ToString().Replace(ddlEventYear.SelectedItem.Text, ddlPropYear.SelectedItem.Text))
                        End If
                    Catch ex As Exception
                        lblfileErr.Text = lblfileErr.Text & "<BR>" & ex.ToString()
                        'Exit Sub
                    End Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
                End If
            Next
            lblError.Text = "Replicated successfully"
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(ddlPropYear.SelectedValue))
            loadgrid()
        End If
    End Sub

    Private Sub copyfile(ByVal OldFileName As String, ByVal NewFileName As String)
        ' Response.Write(Request.ApplicationPath.ToString() & "<br>")
        ' Response.Write(Server.MapPath(Request.ApplicationPath.ToString() & "\include\" & OldFileName.ToString().Replace("/", "\")))
        If File.Exists(Server.MapPath(Request.ApplicationPath.ToString() & "\include\" & OldFileName.ToString().Replace("/", "\"))) Then
            File.Copy(Server.MapPath(Request.ApplicationPath.ToString() & "\include\" & OldFileName.ToString().Replace("/", "\")), (Server.MapPath(Request.ApplicationPath.ToString() & "\include\" & NewFileName.ToString().Replace("/", "\"))), True)
        Else
            lblfileErr.Text = lblfileErr.Text & "<BR>" & OldFileName & " - Does not exist."
        End If
        'File.Copy(Server.MapPath(Request.ApplicationPath.ToString() & "\include\" & OldFileName.ToString().Replace("/", "\")), (Server.MapPath(Request.ApplicationPath.ToString() & "\UploadFiles\" & NewFileName.ToString().Replace("/", "\"))), True)
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
        traddUpdate.Visible = False
    End Sub
End Class

