Imports System.IO
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports NorthSouth.BAL
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class ShowParticipantCertificatePDF
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadCertificates()
        End If
    End Sub
    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()

        generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
        Dim filenameRange As String = ""
        If Session("SelChapterID").ToString() = "1" Then
            Dim StrRange As String() = Session("PageRange").ToString.Split("_")
            filenameRange = Session("PageRange").ToString()
            dsCertificates = badge.GetParticipantsHavingCertificatesFinals(Application("ConnectionString"), Session("SelChapterID"), Session("Event_Year"), generateBadge.ContestDates, StrRange(0), StrRange(1))
        Else
            dsCertificates = badge.GetParticipantsHavingCertificates(Application("ConnectionString"), Session("SelChapterID"), Session("Event_Year"), generateBadge.ContestDates)
        End If
        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            Try
                rptCertificate.DataSource = dsCertificates
                rptCertificate.DataBind()
            Catch ex As Exception

            End Try
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.pdf"
            'FontFactory.Register(Server.MapPath("Font\\SCRIPTBL.TTF"))
            'Dim Font As New Font(iTextSharp.text.Font.FontFamily.HELVETICA, 15, iTextSharp.text.Font.BOLD, BaseColor.BLACK)


            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Participants_" & strChapterName.ToString().Replace(" ", "").Replace(",", "_") & ".pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            rptCertificate.DataSource = dsCertificates.Tables(0)
            rptCertificate.DataBind()
            rptCertificate.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            'Dim docWorkingDocument As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 1, 1, 0, 0)
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER.Rotate(), 72, 72, 50, 0)
            'Dim pgSize As New iTextSharp.text.Rectangle(600, 600)
            'Dim pdfdoc As New iTextSharp.text.Document(pgSize, 5, 5, 45, 0)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.End()

        End If
        'If generateBadge.Export = True Then
        '    Response.Clear()
        '    Response.Buffer = True
        '    'Response.Charset = ""
        '    Response.ContentType = "application/vnd.word"
        '    Dim strChapterName As String
        '    If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
        '        strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
        '    End If

        '    Response.AddHeader("content-disposition", "attachment;filename=Certificates_Participants_" & strChapterName & "_" & filenameRange & ".doc")
        '    Dim stringWrite As New System.IO.StringWriter()
        '    Dim htmlWrite As New HtmlTextWriter(stringWrite)
        '    rptCertificate.RenderControl(htmlWrite)
        '    Response.Write("<html>")
        '    Response.Write("<head>")
        '    Response.Write("<style>")
        '    '612.0  792.0
        '    Response.Write("@page Section2 {size:792.0pt 612.0pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
        '    Response.Write("@page Section1 {size:792.0pt 612.05pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        '    'Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
        '    'Response.Write("@page Section1 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        '    'Response.Write("@page Section1 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
        '    Response.Write("div.Section1 {page:Section1;}")
        '    'Response.Write("@page Section2 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
        '    Response.Write("div.Section2 {page:Section2;}")
        '    Response.Write("</style>")
        '    Response.Write("</head>")
        '    Response.Write("<body>")
        '    Response.Write("<div class='Section2'>")
        '    Response.Write(stringWrite.ToString())
        '    Response.Write("</div>")
        '    Response.Write("</body>")
        '    Response.Write("</html>")
        '    Response.End()
        'End If
    End Sub
    Protected Function GetProductName(ByVal ChildNumber As Double) As String
        Dim strProductName As String
        strProductName = ""
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsProductNames.Tables(0).Rows.Count - 1
                If Len(strProductName) > 0 Then
                    If i = dsProductNames.Tables(0).Rows.Count - 1 Then
                        strProductName = strProductName & " and " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    Else
                        strProductName = strProductName & ", " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    End If
                Else
                    strProductName = dsProductNames.Tables(0).Rows(i)(0).ToString()
                End If
            Next
        Else
            strProductName = ""
        End If
        Return strProductName
    End Function

    Protected Function GetContestDates(ByVal ChildNumber As Double, ByVal ContestDate As String, ByVal ProductCnt As Integer) As String
        Dim strcnstDate As String
        strcnstDate = ""
        If ProductCnt = 1 Then
            strcnstDate = " on " & ContestDate
        Else
            Dim dscnstDates As New DataSet
            Dim i As Integer
            dscnstDates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select convert(varchar(30),Cn.contestdate,107), DATENAME(MM, Cn.contestdate) AS CnstMonth from contestant C Inner Join Contest Cn ON C.ContestID=Cn.ContestID AND C.ChapterID = Cn.NSFChapterID and C.EventId = Cn.EventId   where C.ChildNumber=" & ChildNumber & " and Cn.ContestDate in (" & generateBadge.ContestDates & ") and C.ContestYear=" & Session("Event_Year") & " and C.ChapterID=" & Convert.ToInt32(Session("SelChapterID")) & " Group By  Cn.contestdate")  ' Convert.ToInt32(Session("SelChapterID")), , generateBadge.ContestDates, )
            If dscnstDates.Tables(0).Rows.Count > 1 Then
                For i = 0 To dscnstDates.Tables(0).Rows.Count - 1
                    If Len(strcnstDate) > 0 Then
                        If i = dscnstDates.Tables(0).Rows.Count - 1 Then
                            strcnstDate = strcnstDate & "-" & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 8)
                        Else
                            strcnstDate = strcnstDate & "-" & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 2)
                        End If
                    Else
                        strcnstDate = " during " & dscnstDates.Tables(0).Rows(i)(1).ToString() & " " & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 2)
                    End If
                Next
            Else
                strcnstDate = " on " & ContestDate
            End If
        End If
        Return strcnstDate
    End Function
    Protected Sub rptCertificate_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        FontFactory.Register(Server.MapPath("Font"))
        FontFactory.Register(Server.MapPath("Font\\ScriptMT.ttf"))
        FontFactory.Register(Server.MapPath("Font\\georgiaz.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comic.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comicbd.ttf"))
        FontFactory.Register(Server.MapPath("Font\\ARIALNI.TTF"))
        Dim bfR As iTextSharp.text.pdf.BaseFont
        bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont As New Font(bfR)
        Dim bfR1 As iTextSharp.text.pdf.BaseFont
        bfR1 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\georgiaz.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont1 As New Font(bfR1)
        Dim bfR2 As iTextSharp.text.pdf.BaseFont
        bfR2 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comic.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont2 As New Font(bfR2)
        Dim bfR3 As iTextSharp.text.pdf.BaseFont
        bfR3 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comicbd.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont3 As New Font(bfR3)
        Dim bfR4 As iTextSharp.text.pdf.BaseFont
        bfR4 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ARIALNI.TTF"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont4 As New Font(bfR4)
        Dim Str As String
        Dim Str1 As String
        Dim Str2 As String
        Dim Str3 As String
        Dim Str4 As String
        Str1 = mainFont1.Familyname.ToString()
        Str = mainFont.Familyname.ToString()
        Str2 = mainFont2.Familyname.ToString()
        Str3 = mainFont3.Familyname.ToString()
        Str4 = mainFont4.Familyname.ToString()
        Dim item As RepeaterItem
        If Not (e.Item.DataItem.Equals(DBNull.Value)) Then
            item = e.Item


            Dim drv As System.Data.DataRowView = DirectCast((e.Item.DataItem), System.Data.DataRowView)
            Dim Newlbl As Label = DirectCast(item.FindControl("lblTitle"), Label)
            Dim Newlbl1 As Label = DirectCast(item.FindControl("lblTitle2"), Label)
            Dim Newlbl2 As Label = DirectCast(item.FindControl("lblName"), Label)
            Dim Newlbl3 As Label = DirectCast(item.FindControl("lblcomm"), Label)
            Dim Newlbl4 As Label = DirectCast(item.FindControl("lblNSF"), Label)

            Dim Newlbl5 As Label
            If IsDBNull(item.FindControl("Label11")) Then
                Response.Write("c")
            Else
                Newlbl5 = DirectCast(item.FindControl("Label11"), Label)
                Newlbl5.Font.Name = mainFont3.Familyname.ToString()
                Newlbl5.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")

            End If
            Dim Newlbl6 As Label
            Newlbl6 = DirectCast(item.FindControl("Label12"), Label)
            Dim Newlbl7 As Label = DirectCast(item.FindControl("Label4"), Label)
            Dim Newlbl8 As Label = DirectCast(item.FindControl("Label5"), Label)
            Dim Newlbl9 As Label = DirectCast(item.FindControl("lblRightTitle"), Label)
            Dim Newlbl10 As Label = DirectCast(item.FindControl("lblRightSignature"), Label)
            Dim Newlbl11 As Label = DirectCast(item.FindControl("lblRightTitle1"), Label)
            Dim Newlbl12 As Label = DirectCast(item.FindControl("lblRightTitle2"), Label)
            Dim Newlbl13 As Label = DirectCast(item.FindControl("lblSigTitle"), Label)
            Dim Newlbl14 As Label = DirectCast(item.FindControl("lblSigTitle1"), Label)
            Dim Newlbl15 As Label = DirectCast(item.FindControl("lblSigTitle1"), Label)
            Dim Newlbl16 As Label = DirectCast(item.FindControl("lblRightSigTitle"), Label)
            Dim Newlbl17 As Label = DirectCast(item.FindControl("lblRightSigTitle1"), Label)

            Newlbl.Font.Name = mainFont.Familyname.ToString()
            Newlbl1.Font.Name = mainFont.Familyname.ToString()
            Newlbl3.Font.Name = mainFont3.Familyname.ToString()
            Newlbl4.Font.Name = mainFont4.Familyname.ToString()

            Newlbl6.Font.Name = mainFont3.Familyname.ToString()
            Newlbl7.Font.Name = mainFont3.Familyname.ToString()
            Newlbl8.Font.Name = mainFont3.Familyname.ToString()
            Newlbl9.Font.Name = mainFont3.Familyname.ToString()
            Newlbl10.Font.Name = mainFont3.Familyname.ToString()
            Newlbl11.Font.Name = mainFont3.Familyname.ToString()
            Newlbl12.Font.Name = mainFont2.Familyname.ToString()
            Newlbl13.Font.Name = mainFont2.Familyname.ToString()
            Newlbl14.Font.Name = mainFont2.Familyname.ToString()
            Newlbl15.Font.Name = mainFont2.Familyname.ToString()
            Newlbl16.Font.Name = mainFont2.Familyname.ToString()
            Newlbl17.Font.Name = mainFont2.Familyname.ToString()

            Newlbl.Attributes.Add("Style", "font-face:" + Str1 + ";font-size:36pt;font-weight:bold;;")
            Newlbl1.Attributes.Add("Style", "font-face:" + Str + ";font-size:26pt;font-weight:bold;")
            Newlbl3.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
            Newlbl4.Attributes.Add("Style", "font-face:" + Str4 + ";font-size:12pt;")

            Newlbl6.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
            Newlbl7.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
            Newlbl8.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
            Newlbl9.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
            Newlbl10.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
            Newlbl11.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl12.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl13.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl14.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl15.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl16.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
            Newlbl17.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
        End If
    End Sub





    Protected Function GetContestNo(ByVal ChildNumber As Double, ByVal DBLeft As String, ByVal SessionLeft As String) As String
        Try
            Dim ContestNo As Integer = 0
            Dim dsProductNames As New DataSet
            Dim badge As New Badge()
            dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
            If dsProductNames.Tables(0).Rows.Count > 0 Then
                If dsProductNames.Tables(0).Rows.Count > 0 Then
                    Return DBLeft
                Else
                    Return SessionLeft
                End If
            Else
                Return SessionLeft
            End If
        Catch ex As Exception
            Response.Write(ex.ToString() & "<br> test " & ChildNumber)
        End Try
        Return SessionLeft

    End Function

    Public Function ShowImage(ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String)
        Dim f As New IO.FileInfo(Server.MapPath(imagePath))
        'Dim path As String = "https://www.northsouth.org" & imagePath
        'Dim f As New IO.FileInfo(path)
        If f.Exists Then
            Return imageURL
        Else
            Return spacerURL
        End If

    End Function

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
    End Sub

    Public Function GetLeftShowImage(ByVal ChildNumber As Double, ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String, ByVal SessimageURL As String)
        Dim ContestNo As Integer = 0
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            If dsProductNames.Tables(0).Rows.Count > 0 Then
                Dim f As New IO.FileInfo(Server.MapPath(imagePath))
                'Dim path As String = "https://www.northsouth.org" & imagePath
                'Dim f As New IO.FileInfo(path)
                If f.Exists Then

                    Return imageURL
                Else
                    Return spacerURL
                End If
            Else
                Return SessimageURL
            End If
        Else
            Return SessimageURL
        End If
        Return SessimageURL
    End Function
End Class
