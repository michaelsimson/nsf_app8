Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.IO
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data

Namespace VRegistration
    Partial Class EmailNotification
        Inherits System.Web.UI.Page
        Private Sub Page_Load(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Session("EntryToken") = "VOLUNTEER"
            'Session("RoleId") = 1 '89
            'Session("LoginID") = 4240 '22214       
            'Session("LoggedIn") = "true"
            Try


                'If (Session("LoginID") = Nothing) Then

                '    Response.Redirect("~/Maintest.aspx")

                'End If

                'If (Session("ChapterID") = Nothing And Session("entryToken").ToString() <> "Volunteer" And Session("entryToken").ToString() <> "Donor") Then

                '    Response.Redirect("~/login.aspx?entry=p")
                'End If
                'If (Convert.ToInt32(Session("RoleId") <> 1)) Then

                '    Response.Redirect("~/VolunteerFunctions.aspx")
                'End If

                If IsPostBack Then
                End If
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click

            lblMsg.ForeColor = Color.Red
            btnLogout.Visible = False
            If txtOldEmail.Text.Trim = "" And txtPassword.Text.Trim = "" Then
                lblMsg.Text = "Enter Email ID and Password"
                divEmailCommunicaiton.Visible = False
            ElseIf txtOldEmail.Text.Trim = "" Then
                lblMsg.Text = "Enter Email ID"
                divEmailCommunicaiton.Visible = False
            ElseIf ValidateEmailID(txtOldEmail.Text) = False Then
                lblMsg.Text = "Invalid Primary E-mail address."
                divEmailCommunicaiton.Visible = False
            ElseIf txtPassword.Text = "" Then
                lblMsg.Text = "Enter Password"
                divEmailCommunicaiton.Visible = False
            Else
                If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from login_master where user_email='" & txtOldEmail.Text & "' and user_pwd='" & txtPassword.Text & "' ") > 0) Then
                    removeMenuItem("LOGIN")
                    btnLogout.Visible = True
                    btnContinue.Enabled = False
                    btnForgotPwd.Enabled = False
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ind.automemberid indid, isnull(ind.newsletter,9) as indnewsletter,isnull(sp.newsletter,9) as spnewsletter,sp.automemberid spid,ind.FirstName +' ' + ind.LastName IndName, sp.FirstName+ ' ' + sp.LastName  spousename from indspouse ind inner join login_master lm on ind.email= lm.user_email left join indspouse sp on sp.relationship = ind.automemberid where lm.user_email='" & txtOldEmail.Text & "' ")
                    Dim dt As DataTable
                    dt = ds.Tables(0)
                    hdIndID.Value = dt.Rows(0)("indid").ToString()
                    hdSpID.Value = dt.Rows(0)("spid").ToString()

                    LitInd2.Text = dt.Rows(0)("IndName").ToString()
                    litSp2.Text = dt.Rows(0)("spousename").ToString()
                    lblMsg.Text = ""
                    divEmailCommunicaiton.Visible = True
                    Dim NewsletterValueInd As String
                    Dim NewsletterValueSp As String
                    NewsletterValueInd = dt.Rows(0)("indnewsletter").ToString()
                    NewsletterValueSp = dt.Rows(0)("spnewsletter").ToString()
                    ddlECommInd.SelectedIndex = ddlECommInd.Items.IndexOf(ddlECommInd.Items.FindByValue(NewsletterValueInd))
                    ddlECommSp.SelectedIndex = ddlECommSp.Items.IndexOf(ddlECommSp.Items.FindByValue(NewsletterValueSp))

                Else
                    lblMsg.Text = "Enter Valid Email & Password"
                    divEmailCommunicaiton.Visible = False
                End If


            End If

        End Sub
        Public Sub removeMenuItem(ByVal menuText As String)
            Dim NavLinks As Menu = Page.Master.FindControl("NavLinks")
            Dim item As MenuItem = NavLinks.FindItem(menuText)
            If (Not item Is Nothing) Then
                NavLinks.Items.Remove(NavLinks.FindItem(menuText))
            End If
        End Sub
        Private Function ValidateEmailID(ByVal mailstring As String) As Boolean
            ''Added for testing - 20-09-2013 
            Dim Valid As Boolean
            Try
                Valid = Regex.IsMatch(mailstring, "\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase)
            Catch ex As Exception
            End Try
            If Not Valid Then
                Return False
            Else
                Return True
            End If
        End Function
        Protected Sub btnECommInd_Click(sender As Object, e As EventArgs) Handles btnECommInd.Click
            Try
                removeMenuItem("LOGIN")
            If ddlECommInd.SelectedValue = 9 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter=Null,Modifydate=Getdate(),ModifiedBy=" & hdIndID.Value & "  WHERE Automemberid=" & hdIndID.Value)
            Else
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter='" & ddlECommInd.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & hdIndID.Value & "  WHERE Automemberid=" & hdIndID.Value)
            End If
            If hdSpID.Value <> 0 And ddlECommSp.SelectedValue = 9 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter=Null,Modifydate=Getdate(),ModifiedBy=" & hdIndID.Value & "  WHERE Relationship=" & hdIndID.Value)
            ElseIf hdSpID.Value <> 0 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter='" & ddlECommSp.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & hdIndID.Value & "  WHERE Relationship=" & hdIndID.Value)
            End If
                divEmailCommunicaiton.Visible = False
                lblMsg.ForeColor = Color.Blue
                lblMsg.Text = "Updated Successfully"

            Catch ex As Exception
                'Response.Write(ex.ToString)
            End Try
        End Sub

        Protected Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
            'Server.Transfer("login.aspx", True)
            Session("ChapterID") = Nothing
            Session("ContestsSelected") = Nothing
            Session("CustIndID") = Nothing
            Session("CustSpouseID") = Nothing
            Session("Donation") = Nothing
            Session("DonationFor") = Nothing
            Session("EvenID") = Nothing
            Session("EventYear") = Nothing
            Session("FatherID") = Nothing
            Session("LateFee") = Nothing
            Session("LoggedIn") = Nothing
            Session("LoginChapterID") = Nothing
            Session("LoginEmail") = Nothing
            Session("LoginEventID") = Nothing
            Session("LoginID") = Nothing
            Session("LoginRole") = Nothing
            Session("LoginTeamLead") = Nothing
            Session("LoginTeamMember") = Nothing
            Session("LoginZoneID") = Nothing
            Session("MealsAmount") = Nothing
            Session("MotherID") = Nothing
            Session("NavPath") = Nothing
            Session("RoleId") = Nothing
            Session("sSQL") = Nothing
            Session("sSQLORG") = Nothing
            Session.Abandon()
            Session.RemoveAll()
            Response.Redirect("login.aspx")
        End Sub

     

        Protected Sub btnForgotPwd_Click(sender As Object, e As EventArgs) Handles btnForgotPwd.Click
            Response.Redirect("Forgot.aspx")
        End Sub
    End Class
End Namespace

