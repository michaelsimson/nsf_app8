﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections;
using VRegistration;

public partial class PractisingSessions : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.RegisterStartupScript("myScript", "<script language=JavaScript> $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });</script>");
        lblerr.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                loadyear();
                loadPhase(ddlPhase);
                LoadEvent(ddEvent);
                if (Session["RoleId"].ToString() != "88")
                {
                    fillYearFiletr();
                    loadProductGroupFilter();
                    loadProductFilter();

                    loadLevelFilter();
                    loadCoachFilter();
                    btnSubmit.Visible = false;
                }
                else
                {

                    dvSearchPracticeSessions.Visible = false;
                    btnSubmit.Visible = true;
                }
                fillMeetingGrid("", "", "", "", "");

                // TestCreateTrainingSession();
            }
        }
    }

    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }
    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlPhase.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            // ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 0;
                ddlProduct.Enabled = false;
                loadLevel();
                LoadSessionNo();

                if (Session["RoleID"].ToString() == "88")
                {
                }

            }
            else
            {
                loadLevel();
                ddlProduct.Enabled = true;
            }

        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }

    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedValue + "' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        // populateRecClassDate();
        loadLevel();
    }

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and V.Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }
        }

    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;

                ddchapter.SelectedValue = "112";
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    fillCoach();
                }
                else
                {
                    fillCoach();
                    fillProductGroup();
                }
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }

        }
        else
        {

            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validatemeeting() == "1")
        {
            if (btnCreateMeeting.Text == "Create Practice Session")
            {

                //  ValidatePractiseSession();
                timeOverLapValdiation();
            }
            else
            {
                deleteMeeting();
                timeOverLapValdiation();
            }
        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid("", "", "", "", "");

    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {

        string CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null  and CS.Level='" + ddlLevel.SelectedValue + "'";
        try
        {

            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        string Semester = dr["Semester"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string Time = dr["Time"].ToString();

                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }

                        if (txtDuration.Text != "")
                        {
                            int Duration = Convert.ToInt32(txtDuration.Text);
                        }
                        string timeZoneID = string.Empty;

                        timeZoneID = ddlTimeZone.SelectedValue;

                        string TimeZone = ddlTimeZone.SelectedItem.Text;
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;
                        string sDateFrom = BeginTime;
                        string sDateTo = EndTime;
                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                        {
                            TimeSpan TS = dTo - dFrom;
                            Mins = TS.TotalMinutes;

                        }
                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }


                        txtDuration.Text = durationHrs;
                        ddlPhase.SelectedValue = Semester;
                        ddlLevel.SelectedValue = Level;
                        txtDate.Text = startDate;

                        string userID = Session["LoginID"].ToString();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }

    }

    public void ValidatePractiseSession()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;


            CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.SubstituteMeetKey,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "'";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        count++;

                        int IsSameDay = 0;
                        int IsSameTime = 0;
                        string WebExID = dr["UserID"].ToString();
                        string Pwd = dr["PWD"].ToString();
                        int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                        string ScheduleType = dr["ScheduleType"].ToString();

                        string year = dr["EventYear"].ToString();
                        string eventID = dr["EventID"].ToString();
                        string chapterId = ddchapter.SelectedValue;
                        string ProductGroupID = dr["ProductGroupID"].ToString();
                        string ProductGroupCode = dr["ProductGroupCode"].ToString();
                        string ProductID = dr["ProductID"].ToString();
                        string ProductCode = dr["ProductCode"].ToString();
                        string Semester = dr["Semester"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string MeetingPwd = "training";

                        string Date = txtDate.Text;
                        string Time = dr["Time"].ToString();
                        string Day = dr["Day"].ToString();
                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }

                        if (txtDuration.Text != "")
                        {
                            int Duration = Convert.ToInt32(txtDuration.Text);
                        }
                        string timeZoneID = string.Empty;

                        timeZoneID = ddlTimeZone.SelectedValue;

                        string TimeZone = ddlTimeZone.SelectedItem.Text;
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;


                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }

                        if (timeZoneID == "4")
                        {
                            TimeZone = "EST/EDT – Eastern";
                        }
                        else if (timeZoneID == "7")
                        {
                            TimeZone = "CST/CDT - Central";
                        }
                        else if (timeZoneID == "6")
                        {
                            TimeZone = "MST/MDT - Mountain";
                        }
                        string userID = Session["LoginID"].ToString();

                    }
                }
                else
                {
                    // lblerr.Text = "No child is assigned for the selected Coach..";
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
            //throw new Exception(ex.Message);
        }

    }



    public void fillMeetingGrid(string EventYear, string Semester, string Coach, string Product, string Level)
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        //if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        //{
        //    cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,Vl.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp Vl on(Vl.UserID=VC.UserID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.MemberID=" + ddlCoach.SelectedValue + " and VC.SessionType='Practice'  order by VC.StartDate DESC";
        //}
        //else
        //{
        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime, VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookup VL on(VL.userID=VC.userID) where  VC.EventID=" + ddEvent.SelectedValue + " and VC.SessionType='Practice'";
        //}

        if (Session["RoleId"].ToString() != "88")
        {
            if (EventYear == "")
            {
                if (ddlEventyearFilter.SelectedValue != "0")
                {
                    cmdtext = cmdtext + " and VC.EventYear=" + ddlEventyearFilter.SelectedValue + "";
                }
            }
            else
            {
                cmdtext = cmdtext + " and VC.EventYear=" + EventYear + "";
            }

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                cmdtext = cmdtext + "  and VC.ProductGroupID='" + ddlProductGroupFilter.SelectedValue + "'";
            }
            if (Product == "")
            {
                if ((ddlProductFilter.SelectedValue != "0"))
                {
                    cmdtext = cmdtext + "  and VC.ProductID='" + ddlProductFilter.SelectedValue + "'";
                }
            }
            else
            {
                cmdtext = cmdtext + "  and VC.ProductID='" + Product + "'";
            }

            if (Level == "")
            {


                if ((ddlLevelFilter.SelectedValue != "0"))
                {
                    cmdtext = cmdtext + "  and VC.Level='" + ddlLevelFilter.SelectedValue + "'";
                }
            }
            else
            {
                cmdtext = cmdtext + "  and VC.Level='" + Level + "'";
            }
            if (Semester == "")
            {
                if ((ddlPhaseFilter.SelectedValue != "0"))
                {
                    cmdtext = cmdtext + "  and VC.Semester='" + ddlPhaseFilter.SelectedValue + "'";
                }
            }
            else
            {
                cmdtext = cmdtext + "  and VC.Semester='" + Semester + "'";
            }
            if (Coach == "")
            {
                if ((ddlCoachFilter.SelectedValue != "0") && (ddlCoachFilter.Items.Count > 0))
                {
                    cmdtext = cmdtext + "  and VC.memberID='" + ddlCoachFilter.SelectedValue + "'";
                }
            }
            else
            {
                cmdtext = cmdtext + "  and VC.memberID='" + Coach + "'";
            }
            if ((ddlSessionFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Session='" + ddlSessionFilter.SelectedValue + "'";
            }

            if ((ddlDayFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Day='" + ddlDayFilter.SelectedValue + "'";
            }

        }
        else
        {
            cmdtext = cmdtext + " and VC.MemberId=" + ddlCoach.SelectedItem.Value + "";
            if (ddYear.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.EventYear=" + ddYear.SelectedValue + "";
            }
            if (ddlPhase.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.Semester='" + ddlPhase.SelectedValue + "'";
            }
            if (ddlProductGroup.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.ProductId=" + ddlProduct.SelectedValue + "";
            }

            if (ddlLevel.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.Level='" + ddlLevel.SelectedValue + "'";
            }

            if (ddlSession.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.Session=" + ddlSession.SelectedValue + "";
            }
        }
        cmdtext += " order by VC.StartDate DESC";

        try
        {


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }

                        string startDate = GrdMeeting.Rows[i].Cells[15].Text;
                        DateTime dtStartDate = Convert.ToDateTime(startDate);
                        string currentDate = DateTime.Now.ToShortDateString();
                        DateTime dtCurrentDate = Convert.ToDateTime(currentDate);
                        if (dtStartDate >= dtCurrentDate)
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = true;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = false;
                        }
                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }
    }


    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "0")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (txtDate.Text == "")
        {
            lblerr.Text = "Please fill Date";
            retVal = "-1";
        }
        else if (ddlMakeupTime.SelectedValue == "")
        {
            lblerr.Text = "Please fill Time";
            retVal = "-1";
        }
        else if (txtDuration.Text == "")
        {
            lblerr.Text = "Please fill Duration";
            retVal = "-1";
        }
        else if (txtDuration.Text != "" && Convert.ToInt32(txtDuration.Text) <= 0)
        {
            lblerr.Text = "Please enter the Duration value greater than 0.";
            retVal = "-1";
        }
        else if (txtDate.Text != "")
        {
            DateTime dtCurrent = DateTime.Now;
            DateTime dtCurrentDateTime = DateTime.Now.AddMinutes(60);
            string strCurrentDate = DateTime.Now.ToString("MM/dd/yyyy");

            //string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");

            dtCurrent = Convert.ToDateTime(strCurrentDate);
            string startDate = txtDate.Text;
            DateTime dtStartDate = Convert.ToDateTime(startDate.ToString());

            string startDateTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
            DateTime dtStartDateTime = Convert.ToDateTime(startDateTime.ToString());

            if (dtStartDate < dtCurrent)
            {
                lblerr.Text = "Practise Date should be greater than or equal to today date";
                retVal = "-1";
            }
            else if (dtStartDateTime <= dtCurrentDateTime)
            {
                lblerr.Text = "Practise Time should be greater than to current time";
                retVal = "-1";
            }
            else
            {
                retVal = "1";
            }
        }
        if (txtDuration.Text != "")
        {
            if (Convert.ToInt32(txtDuration.Text) <= 0)
            {
                lblerr.Text = "Duration could be greater than 0.";
                retVal = "-1";
            }
        }
        return retVal;
    }




    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;


            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string hostID = string.Empty;

                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnHostID.Value = hostID;

                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddlCoach.Enabled = false;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();

                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;


                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                LoadSessionNo();
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;

                txtDate.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartDate") as Label).Text;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;



                ddlMakeupTime.SelectedValue = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartTime") as Label).Text;

                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                hdnSessionKey.Value = sessionKey.Trim();


                btnCreateMeeting.Text = "Update Practise Session";




                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[14].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();


            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;

                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    lblerr.Text = hdnMeetingStatus.Value;
                }
            }
            else if (e.CommandName == "Join")
            {
                try
                {

                    row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                    int selIndex = row.RowIndex;
                    GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                    string sessionKey = string.Empty;
                    sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;

                    string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                    string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                    string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                    string duration = "-" + ((Label)GrdMeeting.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                    int iduration = Convert.ToInt32(duration);
                    if (duration == "-")
                    {
                        iduration = 30;
                    }
                    else
                    {
                        iduration = Convert.ToInt32(duration);
                    }

                    DateTime dtFromS = new DateTime();
                    DateTime dtEnds = DateTime.Now.AddMinutes(60);
                    double mins = 40.0;
                    if (DateTime.TryParse(beginTime, out dtFromS))
                    {
                        TimeSpan TS = dtFromS - dtEnds;
                        mins = TS.TotalMinutes;

                    }
                    string today = DateTime.Now.DayOfWeek.ToString();
                    string status = string.Empty;
                    string coachName = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text; ;
                    if (mins <= 30 && day == today)
                    {


                        string cmdText = "";
                        if (mins < iduration)
                        {
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                        }
                        else
                        {
                            try
                            {
                                CoachingExceptionLog.createExceptionLog(coachName, "Success", "", "Practice Sessions");

                            }
                            catch
                            {
                            }
                            hdnHostID.Value = hostID;
                            try
                            {
                                listLiveMeetings();
                            }
                            catch
                            {
                            }

                            getmeetingIfo(sessionKey, hostID);
                            cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                            string meetingLink = hdnHostURL.Value;

                            hdnWebExMeetURL.Value = meetingLink;

                            hdnZoomURL.Value = meetingLink;
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                        }
                    }
                    else
                    {
                        try
                        {
                            CoachingExceptionLog.createExceptionLog(coachName, "Failed", "", "Practice Sessions");
                        }
                        catch (Exception ex)
                        {
                        }
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);

                        // System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                    }
                }
                catch (Exception EX)
                {
                    CoachingExceptionLog.createExceptionLog("", "Exceptions", "", "Practice Sessions");
                    //logerrors("", "", ex.Message.ToString());
                }
            }
            else if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                hdnSessionKey.Value = sessionKey;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnHostID.Value = hostID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);
            }
        }
        catch
        {
            CoachingExceptionLog.createExceptionLog("", "Exceptions", "", "Practice Sessions");
            // logerrors("", "", ex.Message.ToString());
        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid("", "", "", "", "");
    }


    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        deleteMeeting();
    }

    public void deleteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            urlParameter += "&id=" + hdnSessionKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    protected void BtnCreateSession_Click(object sender, EventArgs e)
    {

    }


    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }


    public void createZoomMeeting(string mode)
    {
        try
        {


            string URL = string.Empty;
            string durationHrs = txtDuration.Text;
            string service = "1";
            if (mode == "1")
            {
                URL = "https://api.zoom.us/v1/meeting/create";
            }
            else if (mode == "2")
            {
                URL = "https://api.zoom.us/v1/meeting/update";
            }
            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = txtDate.Text;
            string time = ddlMakeupTime.SelectedValue;
            string MakeupDateTime = date + " " + time;
            DateTime dtMakeupDate = Convert.ToDateTime(MakeupDateTime).AddHours(4);

            string StrMakeupDate = dtMakeupDate.ToString("yyyy-MM-dd");
            string StrMakeupTime = dtMakeupDate.ToString("HH:mm:ss");

            string StrMakeupDateTime = StrMakeupDate + "T" + StrMakeupTime + "Z";

            string dateTime = date + "T" + time + "Z";
            string topic = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "-" + ddlSession.SelectedValue;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            if (DateTime.TryParse(time, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;
                dtTo = dtFromS.AddHours(4);
            }
            string timeFormat = dtTo.Hour + ":" + dtTo.Minute + ":" + dtTo.Second;

            dateTime = date + "T" + timeFormat + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            if (mode == "1")
            {
                urlParameter += "&topic=" + topic + "";
                urlParameter += "&password=training";
                urlParameter += "&type=2";
                urlParameter += "&start_time=" + StrMakeupDateTime + "";
                urlParameter += "&duration=" + txtDuration.Text + "";
                urlParameter += "&timezone=America/Chicago";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }
            else if (mode == "2")
            {
                service = "2";
                urlParameter += "&id=" + hdnSessionKey.Value + "";
                urlParameter += "&start_time=" + StrMakeupDateTime + "";
                urlParameter += "&duration=" + txtDuration.Text + "";
                urlParameter += "&timezone=America/Chicago";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch
        {
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                // hdnHostURL.Value = json["start_url"].ToString();

                hdnHostURL.Value = json["join_url"].ToString();
                hdnMeetingUrl.Value = json["join_url"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "2")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "3")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnSessionKey.Value + "");


                fillMeetingGrid("","","","","");
                lblSuccess.Text = "Practise Session deleted successfully";
            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }
            if (serviceType == "6")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    if (m["host_id"].ToString() == hdnHostID.Value)
                    {
                        termianteMeeting(m["id"].ToString(), hdnHostID.Value);
                    }

                }

            }
            if (serviceType == "7")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);

                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            }

            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);


        }
        catch
        {
            hdnMeetingStatus.Value = "Failure";
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }


    public void makeZoomSession(string StartDate, string Time, string SignupID, string CoachID, string Day, string EndTime, int duration)
    {
        createZoomMeeting("1");
        hdnMeetingStatus.Value = "SUCCESS";
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            string cmdText = "";

            string meetingURL = hdnHostURL.Value;

            string practiseDate = txtDate.Text;

            string strQuery;
            SqlCommand cmd;
            try
            {

                strQuery = "insert into WebConfLog (EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD, SessionType, Status) values(@EventYear,@EventID,@ChapterID,@ProductGroupID,@ProductGroupCode,@ProductID,@ProductCode,@MemberID,@SessionKey,@StartDate,@EndDate,@BeginTime,@EndTime,@Semester,@[Level],@Session,@MeetingPwd,@Duration,@TimeZoneID,@TimeZone,@CreatedDate,@CreatedBy,@Meetingurl,@Day,@UserID,@PWD)";

                cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@EventYear", ddYear.SelectedValue);
                cmd.Parameters.AddWithValue("@EventID", ddEvent.SelectedValue);
                cmd.Parameters.AddWithValue("@ChapterID", ddchapter.SelectedValue);
                cmd.Parameters.AddWithValue("@ProductGroupID", ddlProductGroup.SelectedValue);
                cmd.Parameters.AddWithValue("@ProductGroupCode", ddlProductGroup.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@ProductID", ddlProduct.SelectedValue);
                cmd.Parameters.AddWithValue("@ProductCode", ddlProduct.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@MemberID", CoachID);
                cmd.Parameters.AddWithValue("@SessionKey", hdnSessionKey.Value);
                cmd.Parameters.AddWithValue("@StartDate", txtDate.Text);
                cmd.Parameters.AddWithValue("@EndDate", txtDate.Text);
                cmd.Parameters.AddWithValue("@BeginTime", ddlMakeupTime.SelectedValue);
                cmd.Parameters.AddWithValue("@EndTime", EndTime);
                cmd.Parameters.AddWithValue("@Semester", ddlPhase.SelectedValue);
                cmd.Parameters.AddWithValue("@[Level]", ddlLevel.SelectedValue);
                cmd.Parameters.AddWithValue("@Session", 1);
                cmd.Parameters.AddWithValue("@MeetingPwd", "training");

                cmd.Parameters.AddWithValue("@Duration", txtDuration.Text);
                cmd.Parameters.AddWithValue("@TimeZoneID", ddlTimeZone.SelectedValue);
                cmd.Parameters.AddWithValue("@TimeZone", ddlTimeZone.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["LoginID"].ToString());
                cmd.Parameters.AddWithValue("@Meetingurl", meetingURL);
                cmd.Parameters.AddWithValue("@Day", Day);
                cmd.Parameters.AddWithValue("@UserID", hdnWebExID.Value);
                cmd.Parameters.AddWithValue("@PWD", hdnWebExPwd.Value);
                //InsertUpdateData(cmd);

            }
            catch
            {
            }
            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SessionType, Status)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + CoachID + ",'" + hdnSessionKey.Value + "','" + practiseDate + "','" + practiseDate + "','" + ddlMakeupTime.SelectedValue + "','" + EndTime + "','" + ddlPhase.SelectedValue + "','" + ddlLevel.SelectedValue + "',1,'training'," + duration + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + hdnWebExID.Value + "','" + hdnWebExPwd.Value + "','Practice','Active')";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                if (btnCreateMeeting.Text == "Update Practise Session")
                {
                    lblSuccess.Text = "Practise session updated successfully";
                    clear();
                }
                else
                {
                    lblSuccess.Text = "Practise session Created successfully";
                    clear();
                }
                string EventYear = ddYear.SelectedValue;
                string Semester = ddlPhase.SelectedValue;
                fillMeetingGrid(EventYear, Semester, ddlCoach.SelectedValue, ddlProduct.SelectedValue, ddlLevel.SelectedValue);

            }
            catch (Exception ex)
            {
                CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
            }

        }
    }

    public void updateZoomSession()
    {

        createZoomMeeting("2");
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            string cmdText = "";
            string meetingURL = hdnHostURL.Value;
            string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
            cmdText = "Update WebConfLog set StartDate='" + txtDate.Text + "', EndDate='" + txtDate.Text + "',BeginTime='" + ddlMakeupTime.SelectedValue + "', Duration=" + txtDuration.Text + ",Day='" + day + "',ModifiedDate=GetDate(), ModifiedBy=" + Session["LoginID"].ToString() + " where SessionKey=" + hdnSessionKey.Value + "";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


                fillMeetingGrid("","","","","");
            }
            catch (Exception ex)
            {
                CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
            }
            lblSuccess.Text = "Practise session updated successfully";
        }
    }



    //public void updateZoomSession()
    //{
    //    createZoomMeeting("2");
    //    if (hdnMeetingStatus.Value == "SUCCESS")
    //    {
    //        string cmdText = "";
    //        string meetingURL = hdnHostURL.Value;
    //        cmdText = "Update WebConfLog set StartDate='" + txtDate.Text + "', EndDate='" + txtDate.Text + "',BeginTime='" + ddlMakeupTime.SelectedValue + "', Duration=" + txtDuration.Text + " where SessionKey=" + hdnSessionKey.Value + "";
    //        try
    //        {
    //            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


    //            fillMeetingGrid();
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        lblSuccess.Text = "Practise session updated successfully";
    //    }
    //}


    public void timeOverLapValdiation()
    {
        string cmdText = string.Empty;
        cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "' and CS.SessionNo=" + ddlSession.SelectedValue + "";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
        //string cmdDuration = "";
        //int iYr = SqlHelper.ExecuteScalar(Application["ConnectionString"], CommandType.Text, cmdText);
        string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
        DataSet dsTime = new DataSet();
        string strStartTime = string.Empty;
        string strEndTime = string.Empty;
        int duration = 0;
        bool isScheduled = false;

        string vcUserID = string.Empty;
        string vcPwd = string.Empty;
        string userID = string.Empty;
        string pwd = string.Empty;
        string signupId = string.Empty;
        string coachId = string.Empty;


        if (null != ds && ds.Tables != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                userID = ds.Tables[0].Rows[0]["UserID"].ToString();
                pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();

                signupId = ds.Tables[0].Rows[0]["SignupID"].ToString();
                coachId = ds.Tables[0].Rows[0]["MemberID"].ToString();
                strStartTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                DateTime dtStartTime = Convert.ToDateTime(strStartTime);
                dtStartTime = dtStartTime.AddMinutes(-30);
                strStartTime = dtStartTime.ToShortTimeString() + ":" + "00";
                strEndTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                DateTime dtEndTime = Convert.ToDateTime(strEndTime);
                dtEndTime = dtEndTime.AddMinutes(duration);
                dtEndTime = dtEndTime.AddMinutes(30);
                strEndTime = dtEndTime.ToShortTimeString();

                duration = Convert.ToInt32(txtDuration.Text);
                string hostID = checkAvailableVrooms(duration);
                if (hostID != "")
                {
                    string zoomUserID = hdnWebExID.Value;
                    string zoomPwd = hdnWebExPwd.Value;

                    hdnHostID.Value = hostID;

                    makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, signupId, coachId, day, strEndTime, duration);
                }
                else
                {
                    lblerr.Text = "No Vroom is available at this time. Please choose different time.";
                }

                //string cmdTimeText = "select count(*) as CountSet from CalSignup where UserID='" + userID + "' and Pwd='" + pwd + "' and Day='" + day + "' and EventYear=" + ddYear.SelectedValue + " and [Begin] not between '" + strStartTime + "' and '" + strEndTime + "' and [End] not between '" + strStartTime + "' and '" + strEndTime + "'";
                //dsTime = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdTimeText);
                //if (null != dsTime && dsTime.Tables != null)
                //{
                //    if (dsTime.Tables[0].Rows.Count > 0)
                //    {
                //        if (Convert.ToInt32(dsTime.Tables[0].Rows[0]["CountSet"].ToString()) > 0)
                //        {
                //            isScheduled = false;
                //        }
                //        else
                //        {
                //            isScheduled = true;
                //        }
                //    }
                //}

                //cmdTimeText = "select count(*) as CountSet from WebConfLog where UserID='" + userID + "' and Pwd='" + pwd + "' and Day='" + day + "' and EventYear=" + ddYear.SelectedValue + " and [BeginTime]  not between '" + strStartTime + "' and '" + strEndTime + "' and [EndTime] not between '" + strStartTime + "' and '" + strEndTime + "'";
                //dsTime = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdTimeText);
                //if (null != dsTime && dsTime.Tables != null)
                //{
                //    if (dsTime.Tables[0].Rows.Count > 0)
                //    {
                //        if (Convert.ToInt32(dsTime.Tables[0].Rows[0]["CountSet"].ToString()) > 0)
                //        {
                //            isScheduled = false;
                //        }
                //        else
                //        {
                //            isScheduled = true;
                //        }
                //    }
                //}

                //if (isScheduled == true)
                //{
                //    cmdTimeText = "select distinct CS.UserID,CS.Pwd from CalSignup CS inner join WebConfLog VC on (VC.EventID=CS.EventID) where CS.EventYear=" + ddYear.SelectedValue + " and [Begin] not between '" + strStartTime + "' and '" + strEndTime + "' and [End] not between '" + strStartTime + "' and '" + strEndTime + "' and CS.Accepted='Y'  and VC.BeginTime not between '" + strStartTime + "' and '" + strEndTime + "' and VC.EndTime not between '" + strStartTime + "' and '" + strEndTime + "' and CS.UserID is not null and CS.UserID <>'" + userID + "' and CS.Day<>'" + day + "'";

                //    dsTime = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdTimeText);
                //    if (null != dsTime && dsTime.Tables != null)
                //    {
                //        if (dsTime.Tables[0].Rows.Count > 0)
                //        {
                //            vcUserID = dsTime.Tables[0].Rows[0]["UserID"].ToString();
                //            vcPwd = ds.Tables[0].Rows[0]["Pwd"].ToString();

                //            makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, signupId, coachId, day, strEndTime, duration);
                //        }
                //        else
                //        {
                //            lblerr.Text = "Time is not available.  Already scheduled during " + strStartTime + " - " + strEndTime + "";
                //        }
                //    }

                //}
                //else if (isScheduled == false)
                //{

                //    vcUserID = userID;
                //    vcPwd = pwd;
                //    makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, signupId, coachId, day, strEndTime, duration);
                //}

            }
            else
            {
                lblerr.Text = "Coaching Calendar is not yet set for this semester.";
            }
        }
        else
        {
            lblerr.Text = "Coaching Calendar is not yet set for this semester.";
        }

    }


    public string checkAvailableVrooms(int Duration)
    {

        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        string hostID = string.Empty;

        string strStartTime = ddlMakeupTime.SelectedValue;
        string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
        Duration = Duration + 30;
        string strEndTime = Convert.ToDateTime(strStartTime).ToString();
        strStartTime = Convert.ToDateTime(strStartTime).ToString();

        DateTime dtStarttime = new DateTime();
        DateTime dtEndTime = new DateTime();
        DateTime dtPrStartTime = Convert.ToDateTime(strStartTime).AddMinutes(-30);
        DateTime dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);

        string StrSHr = strStartTime.Substring(0, 2);
        //int iShr = Convert.ToInt32(StrSHr);
        //string StrEhr = Convert.ToDateTime(strEndTime).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
        //int iEhr = Convert.ToInt32(StrEhr);
        //if ((iShr > 7 & iEhr < 8))
        //{
        //    dtPrEndTime = Convert.ToDateTime("23:59:00");
        //}
        //else
        //{
        dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);
        //}

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day, cs.Productgroupcode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" + ddYear.SelectedValue + " and cs.Accepted='Y' order by Time";

        Double iDurationBasedOnPG = 0;


        try
        {
            string vRooms = string.Empty;

            List<Vroom> ObjVroomList = new List<Vroom>();
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            dtStarttime = Convert.ToDateTime(dr["Begin"].ToString());
                            if (dr["ProductGroupCode"].ToString() == "COMP")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "GB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "LSP")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "MB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SAT")
                            {
                                iDurationBasedOnPG = 1.5;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SC")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "UV")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            //string StrSHr1 = Convert.ToDateTime(dr["Begin"].ToString()).ToString().Substring(0, 2);
                            //int iShr1 = Convert.ToInt32(StrSHr1);
                            //string StrEhr1 = Convert.ToDateTime(dr["Begin"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                            //int iEhr1 = Convert.ToInt32(StrEhr1);
                            //if ((iShr1 > 7 & iEhr1 < 8))
                            //{
                            //    dtEndTime = Convert.ToDateTime("23:59:00");
                            //}
                            //else
                            //{
                            dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                            //   }

                            // dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                            string Meetingday = dr["Day"].ToString();
                            string meetVroom = dr["Vroom"].ToString();




                            if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                            {
                                Vroom objVroom = new Vroom();
                                objVroom.Day = dr["Day"].ToString();
                                objVroom.HostId = dr["HostId"].ToString();
                                ObjVroomList.Add(objVroom);

                                vRooms += dr["VRoom"].ToString() + ",";
                            }
                        }
                        catch
                        {
                        }

                        //vRooms += dr["VRoom"].ToString() + ",";
                    }
                }
            }



            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" + ddYear.SelectedValue + " and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by VC.BeginTime";
            try
            {
                DataSet dsSingle = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            try
                            {
                                dtStarttime = Convert.ToDateTime(dr["BeginTime"].ToString());
                                if (dr["ProductGroupCode"].ToString() == "COMP")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "GB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "LSP")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "MB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SAT")
                                {
                                    iDurationBasedOnPG = 1.5;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SC")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "UV")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                //string StrSHr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).ToString().Substring(0, 2);
                                //int iShr1 = Convert.ToInt32(StrSHr1);
                                //string StrEhr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                                //int iEhr1 = Convert.ToInt32(StrEhr1);
                                //if ((iShr1 > 7 & iEhr1 < 8))
                                //{
                                //    dtEndTime = Convert.ToDateTime("23:59:00");
                                //}
                                //else
                                //{
                                dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);
                                // }
                                // dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);

                                string Meetingday = dr["Day"].ToString();
                                string meetVroom = dr["Vroom"].ToString();

                                if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                                {
                                    Vroom objVroom = new Vroom();
                                    objVroom.Day = dr["Day"].ToString();
                                    objVroom.HostId = dr["HostId"].ToString();
                                    ObjVroomList.Add(objVroom);

                                    vRooms += dr["VRoom"].ToString() + ",";
                                }

                                //if ((dtPrStartTime >= dtStarttime || dtPrEndTime <= dtEndTime) && (strStartDay == dr["Day"].ToString()))
                                //{
                                //    Vroom objVroom = new Vroom();
                                //    objVroom.Day = dr["Day"].ToString();
                                //    objVroom.HostId = dr["HostId"].ToString();
                                //    ObjVroomList.Add(objVroom);

                                //    vRooms += dr["VRoom"].ToString() + ",";
                                //}
                            }
                            catch
                            {
                            }

                            // vRooms += dr["VRoom"].ToString() + ",";
                        }
                    }
                }
            }
            catch
            {
            }


            if (!string.IsNullOrEmpty(vRooms))
            {
                vRooms = vRooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(vRooms))
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom not in (" + vRooms + ") order by vroom desc";
            }
            else
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp order by vroom desc";
            }

            DataSet dsVroom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (dsVroom.Tables.Count > 0)
            {
                if (dsVroom.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsVroom.Tables[0].Rows)
                    {
                        hostID = dr["HostID"].ToString();
                        hdnWebExID.Value = dr["UserID"].ToString();
                        hdnWebExPwd.Value = dr["PWD"].ToString();
                    }
                }
                else
                {
                    hostID = "";
                }
            }

            // hostID = "";
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }
        return hostID;
    }




    public void logerrors(string coachName, string status, string exception)
    {
        string filepath = string.Empty;
        try
        {
            string pageName = Path.GetFileName(Request.Path);
            string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";
            filepath = Server.MapPath("~/" + filename);

            if (File.Exists(filepath))
            {
                using (StreamWriter stwriter = new StreamWriter(filepath, true))
                {
                    string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                    message += Environment.NewLine;
                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;
                    message += string.Format("Date & Time: {0}", DateTime.Now);
                    message += Environment.NewLine;
                    message += string.Format("Coach Name: {0}", coachName);
                    message += Environment.NewLine;
                    message += string.Format("Validation Status: {0}", status);
                    message += Environment.NewLine;

                    message += Environment.NewLine;
                    message += string.Format("Exception: {0}", exception);
                    message += Environment.NewLine;

                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;

                    stwriter.WriteLine(message);
                }
            }
            else
            {
                StreamWriter stwriter = File.CreateText(filepath);
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Date & Time: {0}", DateTime.Now);
                message += Environment.NewLine;
                message += string.Format("Coach Name: {0}", coachName);
                message += Environment.NewLine;
                message += string.Format("Validation Status: {0}", status);
                message += Environment.NewLine;

                message += Environment.NewLine;
                message += string.Format("Exception: {0}", exception);
                message += Environment.NewLine;

                message += "-----------------------------------------------------------";
                message += Environment.NewLine;

                stwriter.WriteLine(message);
                stwriter.Close();
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
            // lblerr.Text = ex.Message;
        }
    }

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }

    }


    public void clear()
    {
        btnCreateMeeting.Text = "Create Makeup Session";
        txtDate.Text = "";
        ddlMakeupTime.SelectedValue = "";
        txtDuration.Text = "";
        btnCreateMeeting.Text = "Create Practice Session";
    }

    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }
    }

    public void termianteMeeting(string sessionkey, string hostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "7";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hostID + "";
            urlParameter += "&id=" + sessionkey + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }


    private Boolean InsertUpdateData(SqlCommand cmd)
    {
        String strConnString = System.Configuration.ConfigurationManager.AppSettings["DBConnection"];

        SqlConnection con = new SqlConnection(strConnString);
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            return true;
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
            return false;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
    }

    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
        fillProductGroup();
    }
    public class Vroom
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string HostId { get; set; }
        public string Day { get; set; }
    }

    public void fillYearFiletr()
    {
        int Year = Convert.ToInt32(DateTime.Now.Year);

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)));

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)));

        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(eventyear) from EventFees where EventId=13").ToString();

    }

    public void loadProductGroupFilter()
    {
        try
        {
            string strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId inner join webconflog wc on (wc.ProductGroupId=P.ProductGroupId) where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND  P.EventId=13 and wc.SessionType='Practice'  ";

            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + " order by P.ProductGroupID";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductGroupFilter.DataValueField = "ProductGroupID";
            ddlProductGroupFilter.DataTextField = "Name";

            ddlProductGroupFilter.DataSource = drproductgroup;
            ddlProductGroupFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {
                ddlProductGroupFilter.Items.Insert(0, new ListItem("Select", "0"));

                ddlProductGroupFilter.Enabled = true;
            }
            else
            {
                ddlProductGroupFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadProductFilter()
    {
        try
        {
            string strSql = " Select distinct P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId inner join Webconflog wc on (wc.ProductId=p.productId)  where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND P.EventID=13 and wc.sessiontype='Practice' ";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and P.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + "order by P.ProductID";


            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());


            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductFilter.DataValueField = "ProductID";
            ddlProductFilter.DataTextField = "Name";

            ddlProductFilter.DataSource = drproductgroup;
            ddlProductFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlProductFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlProductFilter.Enabled = true;
            }
            else
            {
                ddlProductFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadLevelFilter()
    {
        try
        {
            string strSql = " select distinct P.LevelCode from ProdLevel P inner join Webconflog wc on (wc.Level=P.LevelCode) where p.EventYear=" + ddlEventyearFilter.SelectedValue + "  and p.EventID=13 and wc.SessionType='Practice'";
            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and ProductID=" + ddlProductFilter.SelectedValue + "";
            }

            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
            ddlLevelFilter.DataValueField = "LevelCode";
            ddlLevelFilter.DataTextField = "LevelCode";
            ddlLevelFilter.DataSource = drproductgroup;
            ddlLevelFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlLevelFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlLevelFilter.Enabled = true;
            }
            else
            {
                ddlLevelFilter.Enabled = false;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadCoachFilter()
    {
        try
        {
            string strSql = " select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignup V On (V.MemberID = I.AutoMemberID and V.EventYear=" + ddlEventyearFilter.SelectedValue + ") inner join Webconflog wc on (wc.MemberId=V.MemberId)  Where V.EventYear= " + ddlEventyearFilter.SelectedValue + "  and wc.SessionType='Practice' and wc.EventYear=" + ddlEventyearFilter.SelectedValue + " ";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductID=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlLevelFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Level=" + ddlLevelFilter.SelectedValue + "";
            }
            if (ddlPhaseFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }


            strSql = strSql + " order by I.FirstName,I.LastName";

            DataSet drproductgroup = new DataSet();
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlCoachFilter.DataValueField = "MemberID";
            ddlCoachFilter.DataTextField = "Name";

            ddlCoachFilter.DataSource = drproductgroup;
            ddlCoachFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlCoachFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlCoachFilter.Enabled = true;
            }
            else
            {
                ddlCoachFilter.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    protected void ddlProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }

        fillMeetingGrid("", "", "", "", "");

    }

    protected void ddlProductFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevelFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");
    }

    protected void btnClearFilter_Click(object sender, EventArgs e)
    {

        //ddlProductGroupFilter.SelectedValue = "0";
        //ddlProductFilter.SelectedValue = "0";
        //ddlLevelFilter.SelectedValue = "0";
        //ddlCoachFilter.SelectedValue = "0";

        ddlPhaseFilter.SelectedValue = "0";
        ddlSessionFilter.SelectedValue = "0";
        ddlDayFilter.SelectedValue = "0";

        fillYearFiletr();
        loadProductGroupFilter();
        loadProductFilter();

        loadLevelFilter();
        loadCoachFilter();

        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");
    }

    protected void ddlLevelFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;


        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");
    }

    protected void ddlSessionFilter_SelectedIndexChanged(object sender, EventArgs e)
    {


        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", ""); 

    }
    protected void ddlDayFilter_SelectedIndexChanged1(object sender, EventArgs e)
    {


        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");
    }
    protected void ddlPhaseFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");

    }

    protected void ddlEventyearFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid("", "", "", "", "");
    }

    protected void ddlCoachFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }

        fillMeetingGrid("", "", "", "", "");
    }
}