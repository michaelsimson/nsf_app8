﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using VRegistration;

public partial class RegCountByCoach : System.Web.UI.Page
{
    string wherecntn = "";
    string EventYear = "";
    string NoregCond = "";
    string WhereNoregCond = "";
    string sorting = "";
    string DayTimeCase = "";
    string Calsign = "";
    DataTable dtnCalendar = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"] = "4240";
        lberrcoach.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            int year = Convert.ToInt32(DateTime.Now.Year);
            int year_index;
            // lstYear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            //lstYear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            // lstYear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));


            if (DateTime.Now.Month <= 3)
            {
                year_index = DateTime.Now.Year - 1;
            }
            else
            {
                year_index = DateTime.Now.Year;
            }

            lstYear.SelectedIndex = lstYear.Items.IndexOf(lstYear.Items.FindByText(year_index.ToString()));
            //lstYear.Items[1].Selected = true;
            loadyear();
            loadPhase();
            GetProductGroup(lstProductGroup);
            //if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "89")))
            //{

            //}
            //else
            //    Response.Redirect("~/VolunteerFunctions.aspx");
            DisplayGrid();
            HeadText.Visible = true;
        }

    }
    private void loadPhase()
    {
        ddlSemester.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(lstYear.SelectedValue);
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        lstYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            lstYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        lstYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    private void GetProductGroup(DropDownList ddlObject)
    {
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where   P.EventId=13  order by P.ProductGroupID"); //EF.EventYear>=YEAR(GETDATE()) AND
        ddlObject.DataSource = dsProductGroup;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "ProductGroupID";
        ddlObject.DataBind();
        if (ddlObject.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";

        }
        else if (ddlObject.Items.Count > 0)
        {
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
            GetProduct();
            lblerr.Text = "";
        }

    }
    private void GetProduct()
    {
        string prodGrp = "";
        if (lstProductGroup.Items[0].Selected == false)
        {
            prodGrp = " and P.ProductGroupID in (0";
            foreach (ListItem i in lstProductGroup.Items)
            {
                if (i.Selected)
                {
                    prodGrp = prodGrp + "," + i.Value;

                }
            }
            prodGrp = prodGrp + ") ";
        }

        DataSet dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select Distinct P.ProductID, Case when P.CoachName is Not Null THEN P.CoachName Else P.Name END  as Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where P.EventID=13 " + prodGrp + " order by P.ProductID"); // EF.EventYear>=YEAR(GETDATE()) AND
        lstProduct.DataSource = dsProduct;
        lstProduct.DataTextField = "Name";
        lstProduct.DataValueField = "ProductID";
        lstProduct.DataBind();
        if (lstProduct.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
        }
        else if (lstProduct.Items.Count > 0)
        {
            lstProduct.Items.Insert(0, new ListItem("All", "0"));
            lstProduct.SelectedIndex = 0;
            lstProduct.Enabled = true;
            lblerr.Text = "";
            if (lstProductGroup.Items[0].Selected == true)
                lstProduct.Enabled = true;
        }
    }

    protected void lstProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct();
        DisplayGrid();
        HeadText.Visible = true;
    }
    protected void btnGetCount_Click(object sender, EventArgs e)
    {
        DisplayGrid();
        HeadText.Visible = true;
    }
    protected void DisplayGrid()
    {
        try
        {
            lblerr.Text = "";
            EventYear = "";
            // if (lstProductGroup.Items[0].Selected != true)
            // {
            if (lstProduct.Items[0].Selected == true)
            {
                NoregCond = NoregCond + " AND ProductID in (0";
                wherecntn = wherecntn + " AND CR.ProductID in (0";
                foreach (ListItem i in lstProduct.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                    NoregCond = NoregCond + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";
                NoregCond = NoregCond + ") ";
            }
            else
            {
                NoregCond = NoregCond + " AND ProductID in (0";
                wherecntn = wherecntn + " AND CR.ProductID in (0";
                foreach (ListItem i in lstProduct.Items)
                {
                    if (i.Selected)
                    {

                        wherecntn = wherecntn + "," + i.Value;
                        NoregCond = NoregCond + "," + i.Value;
                    }
                }
                wherecntn = wherecntn + ") ";
                NoregCond = NoregCond + ") ";
            }
            // }
            NoregCond = NoregCond + " AND EventYear in (0";
            wherecntn = wherecntn + " AND CR.EventYear in (0";
            EventYear = EventYear + "EventYear in (0";
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;
                    EventYear = EventYear + "," + i.Value;
                    NoregCond = NoregCond + "," + i.Value;
                }
            }
            EventYear = EventYear + ") ";
            wherecntn = wherecntn + ") ";
            Calsign = wherecntn + " AND CR.Semester='" + ddlSemester.SelectedValue + "'";
            wherecntn = wherecntn + " AND CR.Semester='" + ddlSemester.SelectedValue + "' and C.Accepted='Y' ";
            WhereNoregCond = wherecntn + " AND CR.Semester='" + ddlSemester.SelectedValue + "'";
            NoregCond = NoregCond + ") AND Semester='" + ddlSemester.SelectedValue + "'";
            if (ddSorting.SelectedValue == "DayTime")
            {
                DayTimeCase = "case when C.Day='Saturday' then 1 when C.day='Sunday' then  2 when C.day='Monday' then  3 when C.day='Tuesday' then  4 when C.day='Wednesday' then  5 when C.day='Thursday' then  6 when C.day='Friday' then  7  else 8  end as dayorder";
                sorting = "dayorder,Time asc";
            }

            else
            {
                DayTimeCase = " '' as dayorder";
                sorting = ddSorting.SelectedValue;
            }

            //string StrSQL = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND' AND (newsletter not in ('5') OR (Newsletter is null)) and automemberid in (select PMemberid from CoachReg where " + wherecntn + "))) or (donortype = 'SPOUSE' AND (newsletter not in ('5') OR (Newsletter is null))  and relationship in (select pMemberid from CoachReg  where " + wherecntn + ")) group by Email";
            string StrSQL = " Select FirstName,LastName,ProductCode,Level,Semester,day,Time,dayorder,SUM(Paid) as Paid, Sum(Pending) as Pending, Sum (Approved) as Approved, SUM(PaidAmt) as PaidAmt,MaxCapacity as Capacity,ProductCode,SessionNo from";
            StrSQL = StrSQL + " ((select I.FirstName,I.LastName,CR.Level,CR.Semester,C.day,C.Time," + DayTimeCase + ",count(CR.CoachRegID) as Paid, 0 as Pending, 0 as Approved, count(CR.CoachRegID) * EF.RegFee as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID  from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as RegFee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.PaymentReference is Not NUll " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,CR.Semester,C.day,C.Time,CR.Semester,C.day,C.Time,cr.ProductCode ,c.MaxCapacity,EF.RegFee,C.SessionNo) Union All "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) //Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
            StrSQL = StrSQL + " (select I.FirstName,I.LastName,CR.Level,CR.Semester,C.day,C.Time," + DayTimeCase + ",0 as Paid, count(CR.CoachRegID) as Pending, 0 as Approved, 0.00 as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.PaymentReference is NUll " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,CR.Semester,C.day,C.Time,cr.ProductCode ,c.MaxCapacity,C.SessionNo) Union All"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
            StrSQL = StrSQL + " select I.FirstName,I.LastName,CR.Level,CR.Semester,C.day,C.Time," + DayTimeCase + ",0 as Paid, 0 as Pending, count(distinct(CR.CoachRegID)) as Approved, 0.00 as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.Approved='Y' " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,CR.Semester,C.day,C.Time,cr.ProductCode ,c.MaxCapacity,C.SessionNo ) temp"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
            StrSQL = StrSQL + " Group by temp.ProductCode,Level,CMemberID,ProductCode,FirstName,LastName,MaxCapacity,SessionNo,Semester,day,Time,dayorder order By  " + sorting + "";
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            //Response.Write(StrSQL.ToString ());
            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

            if (dsCount.Tables[0].Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SNo", typeof(string));
                dt.Columns.Add("FirstName", typeof(string));
                dt.Columns.Add("LastName", typeof(string));
                dt.Columns.Add("ProductCode", typeof(string));
                dt.Columns.Add("Level", typeof(string));
                dt.Columns.Add("Semester", typeof(string));
                dt.Columns.Add("SessionNo", typeof(string));
                dt.Columns.Add("Day", typeof(string));
                dt.Columns.Add("Time", typeof(string));
                dt.Columns.Add("Paid", typeof(int));
                dt.Columns.Add("Pending", typeof(int));
                dt.Columns.Add("Approved", typeof(int));
                dt.Columns.Add("Capacity", typeof(int));
                dt.Columns.Add("PaidAmt", typeof(double));
                DataRow dr;
                int Paid = 0, Pending = 0, Approved = 0, Capacity = 0;
                double PaidAmt = 0.00;
                int srNo = 0;
                for (int i = 0; i < dsCount.Tables[0].Rows.Count; i++)
                {
                    if (dsCount.Tables[0].Rows[i]["Approved"].ToString() == "0")
                        continue;
                    Div5.Visible = true;
                    dr = dt.NewRow();
                    srNo = srNo + 1;
                    dr["SNo"] = (srNo).ToString();
                    dr["FirstName"] = dsCount.Tables[0].Rows[i]["FirstName"].ToString();
                    dr["LastName"] = dsCount.Tables[0].Rows[i]["LastName"].ToString();
                    dr["ProductCode"] = dsCount.Tables[0].Rows[i]["ProductCode"].ToString();
                    dr["Level"] = dsCount.Tables[0].Rows[i]["Level"].ToString();
                    dr["Semester"] = dsCount.Tables[0].Rows[i]["Semester"].ToString();
                    dr["SessionNo"] = dsCount.Tables[0].Rows[i]["SessionNo"].ToString();
                    dr["Day"] = dsCount.Tables[0].Rows[i]["Day"].ToString();
                    dr["Time"] = dsCount.Tables[0].Rows[i]["Time"].ToString();
                    dr["Paid"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Paid"].ToString());
                    dr["Pending"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Pending"].ToString());
                    dr["Approved"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Approved"].ToString());
                    dr["Capacity"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Capacity"].ToString());
                    dr["PaidAmt"] = Convert.ToDouble(dsCount.Tables[0].Rows[i]["PaidAmt"].ToString());
                    Paid = Paid + Convert.ToInt32(dr["Paid"].ToString());
                    Pending = Pending + Convert.ToInt32(dr["Pending"].ToString());
                    Approved = Approved + Convert.ToInt32(dr["Approved"].ToString());
                    Capacity = Capacity + Convert.ToInt32(dr["Capacity"].ToString());
                    PaidAmt = PaidAmt + Convert.ToDouble(dr["PaidAmt"].ToString());
                    dt.Rows.Add(dr);
                }
                dr = dt.NewRow();
                dr["SNo"] = "";
                dr["FirstName"] = "Total ";
                dr["LastName"] = "";
                dr["ProductCode"] = "";
                dr["Level"] = "";
                dr["Semester"] = "";
                dr["SessionNo"] = "";
                dr["Day"] = "";
                dr["time"] = "";
                dr["Paid"] = Paid;
                dr["Pending"] = Pending;
                dr["Approved"] = Approved;
                dr["Capacity"] = Capacity;
                dr["PaidAmt"] = PaidAmt;
                dt.Rows.Add(dr);
                grdCoaching.Visible = true;
                grdCoaching.DataSource = dt;
                grdCoaching.DataBind();
            }
            else
            {
                lberrcoach.Text = "Sorry, No Coaching Registration Found";
                grdCoaching.Visible = false;
            }
            NoReg();
            calendarSignnup();
            contactInfo();
        }
        catch (Exception ex)
        {
            lblerr.Text = ex.ToString();
        }
    }
    protected void NoReg()
    {
        if ((ddSorting.SelectedValue == "Paid desc") || (ddSorting.SelectedValue == "Approved desc"))
        {
            sorting = "LastName,FirstName";
        }
        string StrSQL = " select  distinct C.MemberID,''  AS 'SNo', I.FirstName,I.LastName," + DayTimeCase + ", C.EventYear, C.EventCode, C.ProductGroupCode, C.ProductCode,";
        StrSQL = StrSQL + "  C.Level ,C.Semester, C.SessionNo, C.Day, C.Time from CalSignUp C "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) //Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + "  left join Indspouse I on I.AutoMemberID=c.MemberID where " + EventYear + " and Accepted='Y' "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        //StrSQL = StrSQL + " and not exists (select * from CoachReg cr where C.MemberID=CR.CMemberID " + WhereNoregCond + ")" + NoregCond + " order by " + sorting + " "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + " and ( select count(*) from coachreg where approved='y' and " + EventYear + " and cmemberid=c.memberid and  productid =c.productid ) = 0 " + " order by " + sorting + " ";


        DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        DataTable dtn = dsCount.Tables[0];
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsCount.Tables[0].Rows.Count; i++)
            {
                dsCount.Tables[0].Rows[i]["SNo"] = i + 1;
            }
            lblNoreg.Text = "";
            DataGridNoreg.DataSource = dtn;
            DataGridNoreg.DataBind();
        }
        else
        {
            lblNoreg.Text = "No Records found";
        }
    }

    protected void calendarSignnup()
    {
        if ((ddSorting.SelectedValue == "Paid desc") || (ddSorting.SelectedValue == "Approved desc"))
        {
            sorting = "LastName,FirstName";
        }

        string StrSQL = "  select  distinct C.MemberID, I.FirstName,I.LastName, C.EventYear ,C.EventCode, C.ProductGroupCode, C.ProductCode, ";
        StrSQL = StrSQL + " C.Level ,C.Semester, C.SessionNo ,''  AS 'SNo',C.Day," + DayTimeCase + ", C.Time from CalSignUp C   left join Indspouse I on I.AutoMemberID=c.MemberID ";
        StrSQL = StrSQL + "  where " + EventYear + " and (C.accepted!='Y' or C.Accepted is null)"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) //Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + "  and not exists (select * from CoachReg cr where C.MemberID=CR.CMemberID " + Calsign + ")" + NoregCond + "  order by   " + sorting + " "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        dtnCalendar = dsCount.Tables[0];
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsCount.Tables[0].Rows.Count; i++)
            {
                dsCount.Tables[0].Rows[i]["SNo"] = i + 1;
            }
            Div3.Visible = true;
            lbcalsignup.Text = "";
            Session["DtCalendar"] = dtnCalendar;
            DGSigned.DataSource = dtnCalendar;
            DGSigned.DataBind();
        }
        else
        {
            lbcalsignup.Text = "No Records found";
        }
    }

    protected void contactInfo()
    {
        if ((ddSorting.SelectedValue == "Paid desc") || (ddSorting.SelectedValue == "Approved desc") || (ddSorting.SelectedValue == "DayTime"))
        {
            sorting = "LastName,FirstName";
        }

        string StrSQL = "  select  distinct C.MemberID, I.FirstName,I.LastName, I.Email,I.hphone,I.Cphone,I.city,I.state, ";
        StrSQL = StrSQL + " ''  AS 'SNo'from CalSignUp C   inner join Indspouse I on I.AutoMemberID=c.MemberID ";
        StrSQL = StrSQL + "  where " + EventYear + " and (C.accepted!='Y' or C.Accepted is null)"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) //Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + "  and not exists (select * from CoachReg cr where C.MemberID=CR.CMemberID " + Calsign + ")" + NoregCond + "  order by   " + sorting + " "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        DataTable dtn = dsCount.Tables[0];
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsCount.Tables[0].Rows.Count; i++)
            {
                dsCount.Tables[0].Rows[i]["SNo"] = i + 1;
            }
            Div6.Visible = true;
            lbcalsignup.Text = "";
            Session["ContactInfo"] = dtn;
            DgridContactInfo.DataSource = dtn;
            DgridContactInfo.DataBind();
        }
        else
        {
            lbcalsignup.Text = "No Records found";
        }
    }


    protected void ddSorting_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayGrid();
    }
    protected void DGSigned_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DGSigned.CurrentPageIndex = e.NewPageIndex;

        DGSigned.DataSource = (DataTable)Session["DtCalendar"];
        DGSigned.DataBind();
    }
    protected void DgridContactInfo_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DgridContactInfo.CurrentPageIndex = e.NewPageIndex;
        DgridContactInfo.DataSource = (DataTable)Session["ContactInfo"];
        DgridContactInfo.DataBind();
    }
    protected void lstProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayGrid();
        HeadText.Visible = true;
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(lstProductGroup);
        GetProduct();
        DisplayGrid();
        HeadText.Visible = true;
    }
    protected void lstYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(lstProductGroup);
        GetProduct();
        DisplayGrid();
        HeadText.Visible = true;
    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/RegCountByCoach.aspx");
    }
}