<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContestantRegistration.aspx.vb"
    Inherits="ContestantRegistration" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">
    <table cellspacing="1" cellpadding="1" border="0" id="Table1" runat="server" style="width: 900px">
        <tr>
            <td class="Heading" align="center" colspan="2">
                Contestant Registration
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 18px">
                <asp:HyperLink CssClass="btn_02" ID="hlinkChild" runat="server" NavigateUrl="MainChild.aspx">Back to Children List</asp:HyperLink></td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
    </table>
    <asp:Label ID="lblNoContestMsg" runat="server" Font-Bold="True" ForeColor="ForestGreen"></asp:Label>
    <asp:Panel ID="pnChild" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table id="tblEligibleContests" runat="server" width="900px">
            <tr>
                <td style="width: 35%;" class="mediumwordingbold">
                    <asp:Label ID="lblSelectedChild" runat="server" CssClass="smallwordingbold" Width="100%"
                        Font-Bold="True"></asp:Label>
                </td>
                <td style="font-weight: bold; color: red; font-size: 11px; width: 50%;">
                   </td>
            </tr>
            <tr>
                <td style="width: 50%;" class="mediumwordingbold">
                    Children:
                    <asp:DropDownList ID="ddlChildren" runat="server" AutoPostBack="true" CssClass="mediumwordingbold"
                        Width="50%" DataTextField="First_Name" DataValueField="ChildNumber" OnSelectedIndexChanged="ddlChildren_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="mediumwordingbold" style="color: Maroon">
                    Register and submit selections for one child at a time</td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlSelectionList" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table id="tblSelectionList" runat="server" width="100%" border="0">
            <tr>
                <td class="SmallFont" id="tdWorkShop" runat="server">  
                  
                </td>
            </tr>
         
            <tr>
                <td align="left">
                    <asp:Label ID="lblTotalFeeMsg" runat="server" Text="Amount For Selected Contests "
                        CssClass="SmallFont">
                    </asp:Label>
                    <asp:Label ID="lblTotalFee" runat="server" Text="0" CssClass="SmallFont">
                    </asp:Label>
                    <span style="padding-left:120px;font-size:10pt"><b>Table 1: Contests to be added to the Cart</b></span> 
                 </td>
            </tr>
            <tr>
                <td style="width: 819px">
                    <asp:DataGrid ID="dgSelectionList" ItemStyle-BackColor="#ffff66" runat="server" CssClass="GridStyle" DataKeyField="ContestID"
                        CellPadding="4" OnItemDataBound="dgSelectionList_ItemCreated" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%">
                        <FooterStyle CssClass="GridFooter"></FooterStyle>
                        <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="SmallFont" Wrap="False"></AlternatingItemStyle>
                        <ItemStyle CssClass="SmallFont" Wrap="False"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                        <Columns>
                        <%--0--%>
                            <asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkEvent" runat="server" OnCheckedChanged="chkEvent_OnCheckedChanged"
                                        CssClass="SmallFont" AutoPostBack="true"></asp:CheckBox>
                                    <asp:Label ID="lblRegId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestantID") %>'
                                        CssClass="SmallFont" Visible="false">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                         <%--1--%>
                            <asp:TemplateColumn HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                        CssClass="SmallFont"></asp:Label>
                                     <asp:Label ID="lblProductGroupCode" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'
                                        CssClass="SmallFont"></asp:Label>

                                        <asp:Label ID="lblProductCode" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>'
                                        CssClass="SmallFont"></asp:Label>
                                        <asp:Label ID="lblfee" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.Fee") %>'
                                        CssClass="SmallFont"></asp:Label>
                                         <asp:Label ID="lblDuplicate" runat="server" Visible="false"   CssClass="SmallFont"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                          <%--2--%>
							<ASP:TEMPLATECOLUMN HeaderText="Contest" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
								<ITEMTEMPLATE><%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
								</ITEMTEMPLATE>
							</ASP:TEMPLATECOLUMN>
							<%--3--%>
							<ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ContestID" Visible="False"></ASP:BOUNDCOLUMN>
							<%--5 fee switched to 17--%>
							<ASP:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter"  Visible="true">
                                <HeaderStyle Font-Bold="True" ForeColor="#990000" Font-Underline="false"/>
							</ASP:BOUNDCOLUMN>
                            <%--6--%>
                            <ASP:TemplateColumn HeaderText="Contest Date">
                                <HeaderStyle Font-Bold="True"  ForeColor="#990000" Font-Underline="false"/>
                                <ItemTemplate>
						                <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> 
						               &nbsp;&nbsp;
						                <%#DataBinder.Eval(Container.DataItem, "ContestTime")%> <br />
									    Reg. Deadline: <%#DataBinder.Eval(Container.DataItem, "RegistrationDeadline", "{0:d}")%>
                               </ItemTemplate>
                            </ASP:TemplateColumn>
							<ASP:BOUNDCOLUMN DataField="ContestCategoryID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="EventID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="EventCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductGroupID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductGroupCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="NSFChapterID" Visible="False"></ASP:BOUNDCOLUMN>
							<%--15--%>
							<ASP:BOUNDCOLUMN DataField="RegistrationDeadline" Visible="false" DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
							<%--17 Chapter Switched to 5--%>
							<ASP:BOUNDCOLUMN DataField="Fee" HeaderText="Fee" DataFormatString="${0:N2}">
                                <HeaderStyle Font-Bold="True" ForeColor="#990000" Font-Underline="false"/>
                            </ASP:BOUNDCOLUMN>
                            	<ASP:BOUNDCOLUMN DataField="ContestDesc" Visible="False"></ASP:BOUNDCOLUMN>
                          					</Columns>
                    </asp:DataGrid>
                    </td></tr>
                      <tr>
                <td style="font-weight: bold; color: red; font-size: 11px; width: 50%;">
                    <asp:Label ID="lblWarning" runat="server" Visible="false">
                    </asp:Label>
                    <br />
                     <asp:Label ID="lblWarning2" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 15%;" class="mediumwordingbold" align="center">
                    <asp:Button ID="btnSubmitSelections" OnClick="btnSubmitSelections_Click" runat="server" Text="Submit Selections" ForeColor="Red"
                        Font-Bold="true" />
                </td>
            </tr>
            <tr><td align="center" >
                    <table id="tblGradeWarng" runat="server" visible="false">
                    <tr><td align="center"><asp:Label ID="lblGradeWarning" runat="server" ForeColor="Red" Text="Are you sure you want to register for this contest, although your child is only in KG, but will be competing with children in higher grades?"></asp:Label> </td></tr>
                    <tr><td align="center"><asp:Button ID="btnYes" runat="server" Text="Yes"/>&nbsp;<asp:Button ID="btnNo" runat="server" Text="No" /> </td></tr>
                    </table>
            </td></tr>
            <tr>
                <td style="width: 819px; display: none" align="right" id="tdSubmit" runat="server">
                    I have <font style="color: Red">submitted</font> sections
                </td>
            </tr>
            <tr>
                <td style="width: 819px; display: none" align="right" id="tdWant" runat="server">
                    for each child.I want to:</td>
            </tr>
                    </table>
                     </asp:Panel>
                    
                     <br />
    <asp:Panel ID="PnlPending" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table id="tblPending" runat="server" width="100%" border="0">
            <tr>
                <td class="SmallFont" id="td1" runat="server">
                </td>
            </tr>
            <tr>
                <td align="left">
                <span class="subHeading">Pending Contests</span>
                    <span style="padding-left:250px;font-size:10pt"><b>Table 2: Contests in the Cart</b></span> 
                </td>
            </tr>
            <tr>
                <td style="width: 819px">
                    <asp:DataGrid ID="DGPending" ItemStyle-BackColor="#99ccff"  runat="server" CssClass="GridStyle" DataKeyField="ContestantID"
                        CellPadding="4" OnItemDataBound="dgPending_ItemCreated" OnItemCommand="dgPending_ItemCommand" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%">
                        <FooterStyle CssClass="GridFooter"></FooterStyle>
                        <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="SmallFont" Wrap="False"></AlternatingItemStyle>
                        <ItemStyle CssClass="SmallFont" Wrap="False"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                        <Columns>
                        <%--0--%>
                               <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ></asp:buttoncolumn>
                          
                         <%--1--%>
                            <asp:TemplateColumn HeaderText="Contestant Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                <asp:Label ID="lblContestantId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestantID") %>' CssClass="SmallFont" Visible="false"></asp:Label>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestantName") %>'
                                        CssClass="SmallFont"></asp:Label>
                                        <asp:Label ID="lblProductCode" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>'
                                        CssClass="SmallFont"></asp:Label>
                                        <asp:Label ID="lblfee" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.Fee") %>'
                                        CssClass="SmallFont"></asp:Label>                                        
                                </ItemTemplate>
                            </asp:TemplateColumn>
                          <%--2--%>
							<ASP:TEMPLATECOLUMN HeaderText="Contest" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
								<ITEMTEMPLATE><%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
								</ITEMTEMPLATE>
							</ASP:TEMPLATECOLUMN>
							<%--3--%>
							<ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ContestID" Visible="False"></ASP:BOUNDCOLUMN>
							<%--5 fee switched to 17--%>
							<ASP:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter"  Visible="true">
                                <HeaderStyle Font-Bold="True" ForeColor="#990000" Font-Underline="false"/>
							</ASP:BOUNDCOLUMN>
							<%--6--%>
                            <ASP:TemplateColumn HeaderText="Contest Date">
                                <HeaderStyle Font-Bold="True"  ForeColor="#990000" Font-Underline="false"/>
                                <ItemTemplate>
						                <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> 
						               &nbsp;&nbsp;
						                <%#DataBinder.Eval(Container.DataItem, "ContestTime")%> <br />
									    Reg. Deadline: <%#DataBinder.Eval(Container.DataItem, "RegistrationDeadline", "{0:d}")%>
                               </ItemTemplate>
                            </ASP:TemplateColumn>
							<ASP:BOUNDCOLUMN DataField="ContestCategoryID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="EventID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="EventCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductGroupID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductGroupCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductID" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="False"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="NSFChapterID" Visible="False"></ASP:BOUNDCOLUMN>
							<%--15--%>
							<ASP:BOUNDCOLUMN DataField="RegistrationDeadline" Visible="false" DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
							<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
							<%--17 chapter switeceh to 5--%>
							<ASP:BOUNDCOLUMN DataField="Fee" HeaderText="Fee" DataFormatString="${0:N2}">
                                <HeaderStyle Font-Bold="True" ForeColor="#990000" Font-Underline="false"/>
                            </ASP:BOUNDCOLUMN>
                            	<ASP:BOUNDCOLUMN DataField="ContestDesc" Visible="False"></ASP:BOUNDCOLUMN>
                             
					</COLUMNS>
                    </asp:DataGrid></td>
            </tr>
             <tr>
                <td style="font-weight: bold; color: red; font-size: 14px; width: 50%;">
                    <asp:Label ID="lblPendingErr" runat="server" Visible="false">
                    </asp:Label>                    
                </td>
            </tr>
             <tr>
                <td style="width: 819px" align="right">
                  <asp:Label ID="lblPending" runat="server" Text="Amount For Pending Contests $"
                        CssClass="announcement_text">
                    </asp:Label>
                    <asp:Label ID="lblPendAmt" runat="server" Text="0" CssClass="announcement_text">
                    </asp:Label><br />
                    <asp:Button ID="btnPayNow" runat="server" Text="Pay Now" ForeColor="Blue" Font-Bold="true"
                        Enabled="false" /></td>
            </tr>
            <tr>
                <td style="width: 819px; font-weight: bold; color: Red" align="center">
                    Complete selections for all children before pressing Pay Now.</td>
            </tr>
            <tr>
                <td style="width: 819px; font-weight: bold; color: Red" align="center">
                    Without submitting, your selections will not be registered.</td>
            </tr>
            <tr>
                <td class="ItemCenter" align="center">
                    <asp:Label ID="lblSelectionList" runat="server" CssClass="SmallFont" ForeColor="Red"
                        Visible="False"></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>
     <br />
    <asp:Panel ID="pnlPaidContests" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table>
            <tr id="trChild1" runat="server">
                <td > <span class="subHeading">
                    Paid&nbsp; (Completed Transactions)</span></td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgPaidContests" ItemStyle-BackColor="#ccff99" runat="server" CssClass="GridStyle" DataKeyField="Contestant_Id"
                        CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                        <FooterStyle CssClass="GridFooter"></FooterStyle>
                        <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="SmallFont" Wrap="False"></AlternatingItemStyle>
                        <ItemStyle CssClass="SmallFont" HorizontalAlign="Left" Wrap="True"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                       <Columns>
									<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant">
                                        <HeaderStyle Width="15%"  Font-Bold="True" ForeColor="#990000"  Font-Underline="false"  />
                                    </asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Contest">
										<HeaderStyle Width="15%" Font-Bold="True" ForeColor="#990000"  Font-Underline="false"  />
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Date &amp; Time">
										<HeaderStyle Wrap="False" Font-Bold="True"  Width="23%" ForeColor="#990000"  Font-Underline="false"  />
										<ItemTemplate>
											    <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> <br />
											    CheckInTime: <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%> <%--<br />
											    ContestTime: <%#DataBinder.Eval(Container.DataItem, "StartTime")%> - <%#DataBinder.Eval(Container.DataItem, "EndTime")%>--%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contact Info">
										<HeaderStyle Width="15%"  Font-Bold="True" ForeColor="#990000"  Font-Underline="false"  />
										<ItemTemplate>
										      <asp:Label ID="lblContactInfo" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<ASP:BOUNDCOLUMN DataField="VolunteerName" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="VolunteerPhone" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="City" Visible="False"></ASP:BOUNDCOLUMN>
									<asp:TemplateColumn HeaderText="Contest Info">
										<HeaderStyle Width="40%"  Font-Bold="True" ForeColor="#990000"  Font-Underline="false"  />
										<ItemTemplate>
											Payment Info<br />
											Amount : US <%#DataBinder.Eval(Container.DataItem, "Fee", "{0:c}")%><br />
											Date Paid :<%# DataBinder.Eval(Container.DataItem, "PaymentDate", "{0:d}") %><br />Reference :<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %><br /><asp:Label id="lblScore" runat="server"></asp:Label><br />
											<br />
											<asp:HyperLink id="hlDownloadLink" runat="server" Visible="False">Click here to 
download practice words as PDF File</asp:HyperLink><br />
											<asp:HyperLink id="hlDOCDownloadLink" runat="server" Visible="False">Click here to 
download practice words as Word Document</asp:HyperLink>
										</ItemTemplate>
									</asp:TemplateColumn>
							</Columns>
                    </asp:DataGrid></td>
            </tr>
            <tr>
                <td class="ItemCenter" align="center">
                    <asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red"
                        Visible="False"></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    
    <table style="width: 819px;">
        <tr>
            <td class="ContentSubTitle" align="left" colspan="2">
                <asp:HyperLink ID="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink></td>
        </tr>
    </table>
</asp:Content>
