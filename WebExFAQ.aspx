﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebExFAQ.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="WebExFAQ" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="clear: both; margin-bottom: 10px;"></div>
    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div style="margin-left: auto; margin-right: auto; width: 800px; margin-bottom: 20px; border: 4px solid; min-height: 700px;">
        <div style="font-size: 26px; font-weight: bold; margin-left: auto; margin-right: auto; width: 700px; color: brown; margin-top: 10px;">
            Frequently Asked Questions on NSF WebEx Sessions
        </div>
        <div style="clear: both; margin-top: 10px; border-top: 1px solid blue; width: 700px; height: 5px; margin-left: auto; margin-right: auto;"></div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="width: 700px; height: 5px; margin-left: auto; margin-right: auto; color: brown; font-weight: bold; font-size: 20px; font-family: 'Trebuchet MS';">
            WebEx Session
        </div>
        <div style="clear: both; margin-bottom: 30px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            1. Where to find the URL?
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            There are two kinds of URL.
             <br />
            <b>Coach URL:</b>
            <br />
            By click on the Meeting URL, coach can start their own session automatically, which means the coach does not have to login the WebEx site with a UserID/PWD. Coach can find their own meeting URL by click on the following links. 
            <br />
            <a href="https://www.northsouth.org/app9/CreateMeetings.aspx">https://www.northsouth.org/app9/CreateMeetings.aspx</a>
            <br />
            <a href="https://www.northsouth.org/app9/StudentEnrollment.aspx">https://www.northsouth.org/app9/StudentEnrollment.aspx</a>
            <br />
            <br />
            <b>Child URL:</b>
            By click on the Meeting URL, child can join their registered session. Child/Parent can find their Meeting URL by click on the following link. 
            <br />
            <a href="https://www.northsouth.org/app9/CoachingStatus.aspx">https://www.northsouth.org/app9/CoachingStatus.aspx</a>
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            2. How soon before the class start, they can join?
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            Session will be opened before 20 minutes of the exact meeting time. Child can join their session after the Coach starts the session.
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            3. What are the restrictions if coach does not join?
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            All the students of a particular session can join the Session only after the coach starts the session. Until the coach doesn’t start the session, an alert message is shown to the child in the alert box. Child can join the session after the coach starts the session.
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            4. Single login per user - user needs to log out if they are switching devices
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            Only one child can join the session using the Meeting URL. If a child wants to switch from one device to another device, they have to log out from the current session. After logging out from the current session, child can switch to the other device and join the session again.
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>

        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            5. Pop-up blocker should be disabled
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            Before clicking on the Meeting URL, please make sure the pop-up blocker is disabled in the device’s browser. This can be done by enabling the default pop-up blocker in the browser. So while clicking on the Meeting URL, browser will ask for permission to disable the pop-up blocker to the user. User must allow disabling the pop-blocker for host/join into the session.
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>

        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            6. Trying different browser if a particular browser does not work
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            At Present, WebEx session supports the following browsers:
            <ul>
                <li>Mozilla Firefox</li>
                <li>Google Chrome</li>
                <li>Mozilla Firefox</li>
                <li>Internet Explorer(7+)</li>
                <li>Safari</li>
                <li>Opera</li>
            </ul>

        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>

        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; color: #5a4daf; font-weight: bold; font-size: 18px;">
            7. Trying different device if a particular device does not work
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 700px; font-family: 'Trebuchet MS'; margin-left: auto; margin-right: auto; font-size: 14px; text-align: justify;">
            WebEx is not supported on the Chrome book currently. So the windows, Linux and Mac devices can be used.
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>


    </div>
</asp:Content>
