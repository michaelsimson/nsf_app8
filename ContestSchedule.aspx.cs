﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContestSchedule : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                Events();
                Yearscount();
                ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                ChapterDrop(ddchapter);

                if (Session["LoginChapterID"].ToString() != "")
                {
                    ddchapter.SelectedValue = Session["LoginChapterID"].ToString();
                    if (Session["LoginChapterID"].ToString() == "1")
                    {
                        ddEvent.SelectedValue = "1";
                    }
                    else
                    {
                        ddEvent.SelectedValue = "2";
                    }
                }
            }
        }
    }
    string ConnectionString = "ConnectionString";
    string retFlag = "";
    bool isExport = false;
    protected void Events()
    {
        try
        {
            ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
            ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
            ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
            ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
            ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
            ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
            ddEvent.Items.Insert(0, new ListItem("Finals", "1"));

            ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();
            list.Add(new ListItem("2014", "2014"));
            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();

            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {



        string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        try
        {
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {

                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }



    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddchapter.SelectedValue != "0")
        {
            if (ddchapter.SelectedValue == "1")
            {
                ddEvent.SelectedValue = "1";
            }
            else
            {
                ddEvent.SelectedValue = "2";
            }
        }
    }





    private TimeSpan GetTimeFromString(string timeString)
    {
        DateTime dateWithTime = DateTime.MinValue;
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.TimeOfDay;
    }


    public void highLevelChart()
    {
        dvDay1ScheduleChart.Visible = true;
        dvDay1Schedule.Visible = true;
        btnExport.Visible = true;
        lblMsg.Text = "";
        DataSet objDs = new DataSet();
        string minDate = "";
        string maxDate = "";
        string cmdDateText = "";

        string HeaderDateText = "";

        cmdDateText = "select distinct min(ContestDate) as MinDate,max(ContestDate) as MaxDate from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "'";
        objDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdDateText);
        if (objDs.Tables[0] != null)
        {
            if (objDs.Tables[0].Rows.Count > 0)
            {
                if (objDs.Tables[0].Rows[0]["MinDate"] != null && objDs.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                    maxDate = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");
                    HeaderDateText = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MinDate"].ToString()).ToString("MMMM-dd, yyyy");
                }
            }
        }
        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "8";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);
        ArrayList chartColors = new ArrayList();
        //chartColors.Add("#E94C6F");
        //chartColors.Add("#A23BEC");
        //chartColors.Add("#7F38EC");
        //chartColors.Add("#DB3340");
        //chartColors.Add("#E8B71A");
        chartColors.Add("#FA6900");
        chartColors.Add("#00FFFF");
        chartColors.Add("#CCFB5D");
        chartColors.Add("#77BFC7");
        chartColors.Add("#F778A1");
        chartColors.Add("#FFA500");
        chartColors.Add("#FFFF00");
        chartColors.Add("#FFFF00");

        string locText = "";
        string sDate = null;
        string sVenue = null;
        string sDy = "";
        int color = 0;
        locText = " select distinct  rl.Venue Venue, rs.Date, DATENAME(MONTH,rs.date) mnth, year(rs.date) yr ,day(rs.date) dy FROM ROOMSCHEDULE rs  inner join RoomList rl on rs.BldgId= rl.BldgId and rs.ContestYear = rl.ContestYear and  rs.roomnumber=rl.roomnumber inner join ProductGroup pg on pg.productgroupid=rs.productgroupid  inner join product p on p.productid= rs.productid where rs.ContestYear = " + ddYear.SelectedValue + " and rs.EventId=" + ddEvent.SelectedValue;
        locText = locText + " and rl.eventid=" + ddEvent.SelectedValue + " and rl.ChapterId=" + ddchapter.SelectedValue;
        DataSet dsLoc = default(DataSet);
        dsLoc = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, locText);
        if (minDate != "")
        {
            if (maxDate != "")
            {

                if (dsLoc.Tables[0] != null)
                {
                    if (dsLoc.Tables[0].Rows.Count > 0)
                    {

                        sVenue = dsLoc.Tables[0].Rows[0]["Venue"].ToString();
                        sDate = dsLoc.Tables[0].Rows[0]["mnth"].ToString();
                        foreach (DataRow dr1 in dsLoc.Tables[0].Rows)
                        {
                            if (sDy.Length == 0)
                            {
                                sDy = dr1["dy"].ToString();
                            }
                            else
                            {
                                sDy = sDy + "-" + dr1["dy"].ToString();
                            }
                        }
                        sDate = sDate + " " + sDy + " , " + dsLoc.Tables[0].Rows[0]["yr"];
                    }
                }
                TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
                ArrayList arrList = new ArrayList();
                DataSet ds = new DataSet();
                string cmdText = "select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as Contest,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.ContestID,C.ProductGroupId,C.ProductId,C.EventId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + " and C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + minDate + "' order by ProductId ASC";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                // cmdText = "select ";
                string tblHtml = "";
                tblHtml = "<table style='border:1px solid black; border-collapse:collapse;' width='1200px' cellspacing='0' cellpadding='0'>";
                tblHtml += "<tr><td class='tdClass' style='font-weight:bold; border: 1px solid black;text-align:center;height:30px; width:1200px;' colspan=55>  " + ddEvent.SelectedItem.Text + " - " + HeaderDateText + " Day 1 Schedule</br>" + sVenue + " </td></tr>";
                //tblHtml += "<tr><td style='font-weight:bold;text-align:center;height:30px; width:1200px;' colspan=45>" + sVenue + "</td></tr>";

                tblHtml += "<tr style='background-color:#808080;'>";
                tblHtml += "<td class='tdClass' style='width:100px; color:#000000; border: 1px solid black; font-weight:bold; background-color:#ffffff; vertical-align:middle; font-size:15px;'> Contest</td>";

                int columnCount = 0;
                while (true)
                {
                    tblHtml += "<td style='width:10px; height:40px; color:#000000; border: 1px solid black; font-weight:bold; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";
                    //tblHtml += "<td style='width:10px; height:40px;><div class='rotate' style='-moz-transform:rotate(90deg); width:10px; height:30px;'>" +  + "</div></td>";

                    arrList.Add(timeInterval);
                    timeInterval = timeInterval + TimeSpan.FromMinutes(15);

                    columnCount++;
                    if (timeInterval.Hours == 19)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }

                }
                int rowCount = 0;
                tblHtml += "</tr>";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    color++;
                    string productId = "";
                    string productGroupId = "";
                    string eventId = "";
                    string ph1StartTime = "";
                    string ph2StartTime = "";
                    string ph3StartTime = "";

                    productGroupId = dr["ProductGroupId"].ToString();
                    productId = dr["ProductId"].ToString();
                    eventId = dr["EventId"].ToString();

                    string phase1StartTime = "";
                    string phase2StartTime = "";
                    string phase3StartTime = "";

                    phase1StartTime = dr["Ph1Start"].ToString();
                    phase2StartTime = dr["Ph2Start"].ToString();
                    phase3StartTime = dr["Ph3Start"].ToString();
                    if (phase1StartTime.Length <= 4)
                    {
                        phase1StartTime = "0" + phase1StartTime + ":00";
                    }
                    else
                    {
                        phase1StartTime = phase1StartTime + ":00";
                    }
                    if (phase2StartTime.Length <= 4)
                    {
                        phase2StartTime = "0" + phase2StartTime + ":00";
                    }
                    else
                    {
                        phase2StartTime = phase2StartTime + ":00";
                    }
                    if (phase3StartTime.Length <= 4)
                    {
                        phase3StartTime = "0" + phase3StartTime + ":00";
                    }
                    else
                    {
                        phase3StartTime = phase3StartTime + ":00";
                    }
                    ph1StartTime = (dr["Ph1Start"].ToString().Replace(":", ""));
                    ph2StartTime = (dr["Ph2Start"].ToString().Replace(":", ""));
                    ph3StartTime = (dr["Ph3Start"].ToString().Replace(":", ""));

                    string cmdDurText = "";
                    int phase1Duration = 0;
                    int phase2Duration = 0;
                    int phase3Duration = 0;
                    if (dr["Ph1Start"] != null && dr["Ph1Start"].ToString() != "")
                    {
                        DataSet ds1 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=1 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds1.Tables[0] != null)
                        {
                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                if (ds1.Tables[0].Rows[0]["Duration"] != null && ds1.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase1Duration = Convert.ToInt32(ds1.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }
                    if ((dr["Ph2Start"] != null && dr["Ph2Start"].ToString() != ""))
                    {
                        DataSet ds2 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=2 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds2.Tables[0] != null)
                        {
                            if (ds2.Tables[0].Rows.Count > 0)
                            {
                                if (ds2.Tables[0].Rows[0]["Duration"] != null && ds2.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase2Duration = Convert.ToInt32(ds2.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }
                    if ((dr["Ph3Start"] != null && dr["Ph3Start"].ToString() != ""))
                    {
                        DataSet ds3 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=3 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds3 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds3.Tables[0] != null)
                        {
                            if (ds3.Tables[0].Rows.Count > 0)
                            {
                                if (ds3.Tables[0].Rows[0]["Duration"] != null && ds3.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase3Duration = Convert.ToInt32(ds3.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }

                    int ph1Start = 0;
                    int ph2Start = 0;
                    int ph3Start = 0;


                    tblHtml += "<tr>";
                    tblHtml += "<td style='border: 1px solid black; font-weight:bold;'>" + dr["Contest"].ToString() + "";
                    tblHtml += "</td>";
                    int colCount = 0;
                    int colspanCount = 1;
                    int i2 = 0;
                    int i3 = 0;

                    for (int i = 0; i < columnCount; i++)
                    {
                        if (phase1Duration > 0 && phase2Duration == 0 && phase3Duration == 0)
                        {
                            colspanCount = 1;
                            TimeSpan t4 = GetTimeFromString1(phase1StartTime, phase1Duration);

                            if (arrList[i].ToString() == phase1StartTime)
                            {
                                colCount = ph1Start;
                                TimeSpan t11 = GetTimeFromString(phase1StartTime);
                                while (true)
                                {
                                    colspanCount++;

                                    t11 = t11 + TimeSpan.FromMinutes(15);
                                    if (t11 == t4)
                                    {

                                        break;
                                    }
                                }
                                tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 1</td>";

                                i = i + colspanCount - 1;
                            }

                            else
                            {
                                tblHtml += "<td class='tdClass' style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else if (phase1Duration > 0 && phase2Duration > 0 && phase3Duration == 0)
                        {
                            //if (i < columnCount)
                            //{
                            TimeSpan t5 = GetTimeFromString1(phase1StartTime, phase1Duration);
                            TimeSpan t6 = GetTimeFromString1(phase2StartTime, phase2Duration);
                            if (arrList[i].ToString() == phase1StartTime || arrList[i].ToString() == phase2StartTime)
                            {
                                colspanCount = 1;
                                i2++;
                                if (i2 == 1)
                                {
                                    colCount = ph1Start;
                                    TimeSpan t21 = GetTimeFromString(phase1StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t21 = t21 + TimeSpan.FromMinutes(15);
                                        if (t21 == t5)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; font-weight:bold; border: 1px solid black; text-align:center;'>Phase 1</td>";

                                    i = i + colspanCount - 1;

                                }
                                else
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t22 = GetTimeFromString(phase1StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t22 = t22 + TimeSpan.FromMinutes(15);
                                        if (t22 == t6)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 2</td>";

                                    i = i + colspanCount - 1;
                                }
                            }

                            //}
                            else
                            {
                                tblHtml += "<td class='tdClass' style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else if (phase1Duration > 0 && phase2Duration > 0 && phase3Duration > 0)
                        {
                            TimeSpan t1 = GetTimeFromString1(phase1StartTime, phase1Duration);
                            TimeSpan t2 = GetTimeFromString1(phase2StartTime, phase2Duration);
                            TimeSpan t3 = GetTimeFromString1(phase3StartTime, phase3Duration);
                            if (arrList[i].ToString() == phase1StartTime || arrList[i].ToString() == phase2StartTime || arrList[i].ToString() == phase3StartTime)
                            {


                                i3++;
                                if (i3 == 1)
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase1StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 == t1)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; font-weight:bold; border: 1px solid black; text-align:center;'>Phase 1</td>";

                                    i = i + colspanCount - 1;
                                }
                                else if (i3 == 2)
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase2StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 == t2)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; font-weight:bold; border: 1px solid black; text-align:center;'>Phase 2</td>";

                                    i = i + colspanCount - 1;
                                }
                                else
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase3StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 == t3)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td class='tdClass' Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 3</td>";

                                    i = i + colspanCount - 1;
                                }
                            }

                            else
                            {
                                tblHtml += "<td class='tdClass' style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else
                        {
                            tblHtml += "<td class='tdClass' style='border: 1px solid black;'>";
                            tblHtml += "</td>";
                        }
                        //tblHtml += "<td>";
                        //tblHtml += "</td>";
                    }

                    tblHtml += "</tr>";
                    rowCount++;
                }

                tblHtml += "</table>";
                ltrTable.Text = tblHtml;
            }
            else
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "No Record Exists!";
                btnExport.Visible = false;
                ltrTable.Text = "";
                ltrTableDay2.Text = "";
                dvDay1Schedule.Visible = false;
                dvDay2Schedule.Visible = false;
                dvDay1ScheduleChart.Visible = false;
                dvDay2ScheduleChart.Visible = false;
                btnExport.Visible = false;
            }
        }
        else
        {
            lblMsg.ForeColor = Color.Red;
            lblMsg.Text = "No Record Exists!";
            btnExport.Visible = false;
            ltrTable.Text = "";
            ltrTableDay2.Text = "";
            dvDay1Schedule.Visible = false;
            dvDay2Schedule.Visible = false;
            dvDay1ScheduleChart.Visible = false;
            dvDay2ScheduleChart.Visible = false;
            btnExport.Visible = false;
        }
    }

    public void highLevelChartDay2()
    {
        dvDay2ScheduleChart.Visible = true;
        dvDay2Schedule.Visible = true;
        lblMsg.Text = "";
        DataSet objDs = new DataSet();
        string minDate = "";
        string maxDate = "";
        string cmdDateText = "";
        string HeaderDateText = "";
        cmdDateText = "select distinct min(ContestDate) as MinDate,max(ContestDate) as MaxDate from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "'";
        objDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdDateText);
        if (objDs.Tables[0] != null)
        {
            if (objDs.Tables[0].Rows.Count > 0)
            {
                if (objDs.Tables[0].Rows[0]["MinDate"] != null && objDs.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                    maxDate = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");
                    HeaderDateText = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMMM-dd, yyyy");
                }
            }
        }

        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "8";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + maxDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);

        ArrayList chartColors = new ArrayList();
        chartColors.Add("#FA6900");
        chartColors.Add("#00FFFF");
        chartColors.Add("#00FFFF");
        chartColors.Add("#00FFFF");
        chartColors.Add("#77BFC7");
        chartColors.Add("#CCFB5D");
        chartColors.Add("#FFA500");
        chartColors.Add("#F778A1");
        chartColors.Add("#F778A1");
        chartColors.Add("#F778A1");
        chartColors.Add("#FFA500");
        chartColors.Add("#FFA500");
        chartColors.Add("#FFFF00");

        string locText = "";
        string sDate = null;
        string sVenue = null;
        string sDy = "";

        locText = " select distinct  rl.Venue Venue, rs.Date, DATENAME(MONTH,rs.date) mnth, year(rs.date) yr ,day(rs.date) dy FROM ROOMSCHEDULE rs  inner join RoomList rl on rs.BldgId= rl.BldgId and rs.ContestYear = rl.ContestYear and  rs.roomnumber=rl.roomnumber inner join ProductGroup pg on pg.productgroupid=rs.productgroupid  inner join product p on p.productid= rs.productid where rs.ContestYear = " + ddYear.SelectedValue + " and rs.EventId=" + ddEvent.SelectedValue;
        locText = locText + " and rl.eventid=" + ddEvent.SelectedValue + " and rl.ChapterId=" + ddchapter.SelectedValue;
        DataSet dsLoc = default(DataSet);
        dsLoc = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, locText);
        if (minDate != "")
        {
            if (maxDate != "")
            {
                if (dsLoc.Tables[0] != null)
                {
                    if (dsLoc.Tables[0].Rows.Count > 0)
                    {

                        sVenue = dsLoc.Tables[0].Rows[0]["Venue"].ToString();
                        sDate = dsLoc.Tables[0].Rows[0]["mnth"].ToString();
                        foreach (DataRow dr1 in dsLoc.Tables[0].Rows)
                        {
                            if (sDy.Length == 0)
                            {
                                sDy = dr1["dy"].ToString();
                            }
                            else
                            {
                                sDy = sDy + "-" + dr1["dy"].ToString();
                            }
                        }
                        sDate = sDate + " " + sDy + " , " + dsLoc.Tables[0].Rows[0]["yr"];
                    }
                }
                TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
                ArrayList arrList = new ArrayList();
                DataSet ds = new DataSet();
                string cmdText = "select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as Contest,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.ContestID,C.ProductGroupId,C.ProductId,C.EventId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + " and C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + maxDate + "' order by ProductId ASC";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                // cmdText = "select ";
                string tblHtml = "";
                tblHtml = "<table style='border: 1px solid black; border-collapse:collapse;' width='1200px' cellspacing='0' cellpadding='0'>";
                tblHtml += "<tr><td style='font-weight:bold; border: 1px solid black; text-align:center;height:30px; width:1200px;' colspan=55>  " + ddEvent.SelectedItem.Text + " - " + HeaderDateText + " Day 2 Schedule</br>" + sVenue + " </td></tr>";
                //tblHtml += "<tr><td style='font-weight:bold;text-align:center;height:30px; width:1200px;' colspan=49>" + sVenue + "</td></tr>";

                tblHtml += "<tr style='background-color:#808080;'>";
                tblHtml += "<td style='width:100px; color:#000000; border: 1px solid black; font-weight:bold; background-color:#ffffff; font-size:15px; vertical-align:middle;'> Contest</td>";

                int columnCount = 0;

                while (true)
                {
                    tblHtml += "<td style='width:10px; height:40px; font-weight:bold; border: 1px solid black; color:#000000; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";
                    //tblHtml += "<td style='width:10px; height:40px;><div class='rotate' style='-moz-transform:rotate(90deg); width:10px; height:30px;'>" +  + "</div></td>";

                    arrList.Add(timeInterval);
                    timeInterval = timeInterval + TimeSpan.FromMinutes(15);

                    columnCount++;
                    if (timeInterval.Hours == 19)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }

                }
                int rowCount = 0;
                int color = 0;
                tblHtml += "</tr>";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    color++;
                    string productId = "";
                    string productGroupId = "";
                    string eventId = "";
                    string ph1StartTime = "";
                    string ph2StartTime = "";
                    string ph3StartTime = "";

                    productGroupId = dr["ProductGroupId"].ToString();
                    productId = dr["ProductId"].ToString();
                    eventId = dr["EventId"].ToString();

                    string phase1StartTime = "";
                    string phase2StartTime = "";
                    string phase3StartTime = "";

                    phase1StartTime = dr["Ph1Start"].ToString();
                    phase2StartTime = dr["Ph2Start"].ToString();
                    phase3StartTime = dr["Ph3Start"].ToString();
                    if (phase1StartTime.Length <= 4)
                    {
                        phase1StartTime = "0" + phase1StartTime + ":00";
                    }
                    else
                    {
                        phase1StartTime = phase1StartTime + ":00";
                    }
                    if (phase2StartTime.Length <= 4)
                    {
                        phase2StartTime = "0" + phase2StartTime + ":00";
                    }
                    else
                    {
                        phase2StartTime = phase2StartTime + ":00";
                    }
                    if (phase3StartTime.Length <= 4)
                    {
                        phase3StartTime = "0" + phase3StartTime + ":00";
                    }
                    else
                    {
                        phase3StartTime = phase3StartTime + ":00";
                    }
                    ph1StartTime = (dr["Ph1Start"].ToString().Replace(":", ""));
                    ph2StartTime = (dr["Ph2Start"].ToString().Replace(":", ""));
                    ph3StartTime = (dr["Ph3Start"].ToString().Replace(":", ""));

                    string cmdDurText = "";
                    int phase1Duration = 0;
                    int phase2Duration = 0;
                    int phase3Duration = 0;
                    if (dr["Ph1Start"] != null && dr["Ph1Start"].ToString() != "")
                    {
                        DataSet ds1 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=1 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds1.Tables[0] != null)
                        {
                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                if (ds1.Tables[0].Rows[0]["Duration"] != null && ds1.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase1Duration = Convert.ToInt32(ds1.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }
                    if ((dr["Ph2Start"] != null && dr["Ph2Start"].ToString() != ""))
                    {
                        DataSet ds2 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=2 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds2.Tables[0] != null)
                        {
                            if (ds2.Tables[0].Rows.Count > 0)
                            {
                                if (ds2.Tables[0].Rows[0]["Duration"] != null && ds2.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase2Duration = Convert.ToInt32(ds2.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }
                    if ((dr["Ph3Start"] != null && dr["Ph3Start"].ToString() != ""))
                    {
                        DataSet ds3 = new DataSet();
                        cmdDurText = "select Duration from ContestSettings where EventId=" + eventId + " and ProductGroupID=" + productGroupId + "and ProductID=" + productId + " and Phase=3 and ContestYear='" + ddYear.SelectedValue + "'";
                        ds3 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
                        if (ds3.Tables[0] != null)
                        {
                            if (ds3.Tables[0].Rows.Count > 0)
                            {
                                if (ds3.Tables[0].Rows[0]["Duration"] != null && ds3.Tables[0].Rows[0]["Duration"].ToString() != "")
                                {
                                    phase3Duration = Convert.ToInt32(ds3.Tables[0].Rows[0]["Duration"]);
                                }
                            }
                        }
                    }

                    int ph1Start = 0;
                    int ph2Start = 0;
                    int ph3Start = 0;




                    tblHtml += "<tr>";
                    tblHtml += "<td style='border: 1px solid black; font-weight:bold;'>" + dr["Contest"].ToString() + "";
                    tblHtml += "</td>";
                    int colCount = 0;
                    int colspanCount = 1;
                    int i2 = 0;
                    int i3 = 0;

                    for (int i = 0; i < columnCount; i++)
                    {
                        if (phase1Duration > 0 && phase2Duration == 0 && phase3Duration == 0)
                        {
                            colspanCount = 1;
                            TimeSpan t4 = GetTimeFromString1(phase1StartTime, phase1Duration);

                            if (arrList[i].ToString() == phase1StartTime)
                            {
                                colCount = ph1Start;
                                TimeSpan t11 = GetTimeFromString(phase1StartTime);
                                while (true)
                                {
                                    colspanCount++;

                                    t11 = t11 + TimeSpan.FromMinutes(15);
                                    if (t11 >= t4)
                                    {

                                        break;
                                    }
                                }
                                tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 1</td>";

                                i = i + colspanCount - 1;
                            }

                            else
                            {
                                tblHtml += "<td style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else if (phase1Duration > 0 && phase2Duration > 0 && phase3Duration == 0)
                        {
                            //if (i < columnCount)
                            //{
                            TimeSpan t5 = GetTimeFromString1(phase1StartTime, phase1Duration);
                            TimeSpan t6 = GetTimeFromString1(phase2StartTime, phase2Duration);
                            TimeSpan t21 = GetTimeFromString(phase1StartTime);

                            if (arrList[i].ToString() == phase1StartTime || arrList[i].ToString() == phase2StartTime)
                            {
                                colspanCount = 1;
                                i2++;
                                if (i2 == 1)
                                {
                                    colCount = ph1Start;
                                    t21 = GetTimeFromString(phase1StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t21 = t21 + TimeSpan.FromMinutes(15);
                                        if (t21 >= t5)
                                        {

                                            break;
                                        }
                                    }
                                    if (phase2StartTime == Convert.ToString(t5))
                                    {
                                        TimeSpan t22 = GetTimeFromString(phase2StartTime);
                                        while (true)
                                        {
                                            colspanCount++;

                                            t22 = t22 + TimeSpan.FromMinutes(15);
                                            if (t22 >= t6)
                                            {

                                                break;
                                            }
                                        }
                                    }
                                    if (phase2StartTime == Convert.ToString(t5))
                                    {
                                        tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 1 & Phase 2</td>";
                                    }
                                    else
                                    {
                                        tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 1</td>";
                                    }
                                    i = i + colspanCount - 1;



                                }
                                else
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t22 = GetTimeFromString(phase2StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t22 = t22 + TimeSpan.FromMinutes(15);
                                        if (t22 >= t6)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; font-weight:bold; border: 1px solid black; text-align:center;'>Phase 2</td>";

                                    i = i + colspanCount - 1;
                                }
                            }

                            //}
                            else
                            {
                                tblHtml += "<td style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else if (phase1Duration > 0 && phase2Duration > 0 && phase3Duration > 0)
                        {
                            TimeSpan t1 = GetTimeFromString1(phase1StartTime, phase1Duration);
                            TimeSpan t2 = GetTimeFromString1(phase2StartTime, phase2Duration);
                            TimeSpan t3 = GetTimeFromString1(phase3StartTime, phase3Duration);
                            if (arrList[i].ToString() == phase1StartTime || arrList[i].ToString() == phase2StartTime || arrList[i].ToString() == phase3StartTime)
                            {


                                i3++;
                                if (i3 == 1)
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase1StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 >= t1)
                                        {

                                            break;
                                        }
                                    }

                                    tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; font-weight:bold; border: 1px solid black; text-align:center;'>Phase 1</td>";


                                    i = i + colspanCount - 1;
                                }
                                else if (i3 == 2)
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase2StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 >= t2)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 2</td>";

                                    i = i + colspanCount - 1;
                                }
                                else
                                {
                                    colspanCount = 1;
                                    colCount = ph1Start;
                                    TimeSpan t31 = GetTimeFromString(phase3StartTime);
                                    while (true)
                                    {
                                        colspanCount++;

                                        t31 = t31 + TimeSpan.FromMinutes(15);
                                        if (t31 >= t3)
                                        {

                                            break;
                                        }
                                    }
                                    tblHtml += "<td Colspan=" + colspanCount + " style='background-color:" + chartColors[color] + "; Color:black; border: 1px solid black; font-weight:bold; text-align:center;'>Phase 3</td>";

                                    i = i + colspanCount - 1;
                                }
                            }

                            else
                            {
                                tblHtml += "<td style='border: 1px solid black;'>";
                                tblHtml += "</td>";
                            }
                        }
                        else
                        {
                            tblHtml += "<td style='border: 1px solid black;'>";
                            tblHtml += "</td>";
                        }
                        //tblHtml += "<td>";
                        //tblHtml += "</td>";
                    }

                    tblHtml += "</tr>";
                    rowCount++;
                }

                tblHtml += "</table>";
                ltrTableDay2.Text = tblHtml;
            }
            else
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "No Record Exists!";
                btnExport.Visible = false;
                ltrTable.Text = "";
                ltrTableDay2.Text = "";
                dvDay1Schedule.Visible = false;
                dvDay2Schedule.Visible = false;
                dvDay1ScheduleChart.Visible = false;
                dvDay2ScheduleChart.Visible = false;
            }
        }
        else
        {
            lblMsg.ForeColor = Color.Red;
            lblMsg.Text = "No Record Exists!";
            btnExport.Visible = false;
            ltrTable.Text = "";
            ltrTableDay2.Text = "";
            dvDay1Schedule.Visible = false;
            dvDay2Schedule.Visible = false;
            dvDay1ScheduleChart.Visible = false;
            dvDay2ScheduleChart.Visible = false;
        }
    }
    private TimeSpan GetTimeFromString1(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    public void exportExcelContestSchedule()
    {
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;

        string teamName = (ddlLevel.SelectedItem.Text).Replace(" ", "");
        string filename = "ContestSchedule_" + teamName + "_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter stringWrite = new StringWriter();
        HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        if (ddlLevel.SelectedValue == "3")
        {

            dvSchedulePostingReport.RenderControl(htmlWrite);
        }
        else if (ddlLevel.SelectedValue == "1" || ddlLevel.SelectedValue == "2")
        {

            dvHighLevelChart.RenderControl(htmlWrite);
        }
        else
        {
            dvTechStaffScheduleReport.RenderControl(htmlWrite);
        }

        Response.Write(stringWrite.GetStringBuilder().ToString());

        Response.End();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        isExport = true;
        //if (ddlLevel.SelectedValue == "2")
        //{
        //    generateChart();
        //    generateChartDay2();
        //}
        exportExcelContestSchedule();
        isExport = false;
        //if (ddlLevel.SelectedValue == "2")
        //{
        //    generateChart();
        //    generateChartDay2();
        //}


    }
    public void generateReportSchedulePosting()
    {
        btnExport.Visible = true;
        lblMsg.Text = "";
        string cmdText = "";
        DataSet ds = new DataSet();
        DataSet dsDate = new DataSet();
        string cmdDatetext = "";
        string tblSPHtml = "";
        string minDate = "";
        tblSPHtml = "<table border='1' align='center' width='900px' cellspacing='0' cellpadding='0'>";
        tblSPHtml += "<tr>";
        tblSPHtml += "<td style='width:300px; font-weight:bold; text-align:center; font-size:14px;'>Contest</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Time</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Group</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Building Room</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Badge No</td>";
        tblSPHtml += "</tr>";

        cmdText = "select distinct P.Name as Contest,C.ProductId,C.ProductCode as pgCode,C.ProductGroupCode,C.Ph1Start,C.Ph2Start,C.Ph3Start,RC.Phase,RC.RoomNumber,RC.BldgID,RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery,CS.Duration,rtrim(ltrim(rc.productcode))+'-' + case rc.phase when 1 then 'I' when 2 then 'II' when 3 then 'III' when 4 then 'IV' end  +' - '+ convert(char(3),rc.SeqNo) ProductCode,rc.StartBadgeNo,rc.EndBadgeNo,C.ProductGroupId from Contest C left join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' ) left join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and  RC.BldgID=RL.BldgID and RC.RoomNumber=RL.RoomNumber) left join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase) inner join Product P on P.productId=c.ProductId where C.Contest_Year='" + ddYear.SelectedValue + "' and         DATENAME(dw,c.[ContestDate]) ='Saturday'  and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "";
        //C.ContestDate='2015-08-29'

        //cmdText = "SELECT P.Name as Contest,rl.eventid,rl.event,rl.BldgName BldgName,rl.BldgID BldgID,case rl.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery, rl.RoomNumber RoomNumber, rl.capacity,rtrim(ltrim(rs.productcode))+'-' + case rs.phase when 1 then 'I' when 2 then 'II' when 3 then 'III' when 4 then 'IV' end  +' - '+ convert(char(3),rs.SeqNo) ProductCode, rs.ProductGroupCode PgCode,rs.Phase,rs.StartBadgeNo,rs.EndBadgeNo,rs.ProductGroupId,rs.ProductId";
        //cmdText = cmdText + " FROM ROOMSCHEDULE rs inner join RoomList rl on rs.BldgId= rl.BldgId and rs.ContestYear = rl.ContestYear and  rs.roomnumber=rl.roomnumber  and  DATENAME(dw,rs.[date]) ='Saturday' and rl.contestyear=" + ddYear.SelectedValue;
        //cmdText = cmdText + " and rl.eventid=" + ddEvent.SelectedValue + " and rl.ChapterId=" + ddchapter.SelectedValue + " inner join Product P on P.productId=rs.ProductId";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


        string contestname = "";
        string phase = "";
        string contest1 = "";
        string phase1 = "";
        int count = 0;
        string borderStyle = "1px solid black;";
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count >= 0)
            {
                cmdDatetext = "select Min(ContestDate) as MinDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and eventid=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + "";
                dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDatetext);
                if (dsDate.Tables[0] != null)
                {
                    if (dsDate.Tables[0].Rows.Count > 0)
                    {
                        if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                        {
                            minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("MMM dd,yyyy");
                            dvScheduleTextDay1.InnerText = "Saturday " + minDate + "";
                        }
                    }
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    contest1 = dr["Contest"].ToString();
                    phase1 = dr["Phase"].ToString();

                    if (contestname != dr["Contest"].ToString() || phase != dr["Phase"].ToString())
                    {
                        string startTime = "";
                        string EndTime = "";
                        string resultEndTime = "";
                        tblSPHtml += "<tr>";
                        tblSPHtml += "<td style='text-align:center; vertical-align:middle;'>" + dr["Contest"].ToString() + " " + "-" + " " + "Phase " + dr["Phase"] + "";
                        tblSPHtml += "</td>";
                        DataSet dsEnd = new DataSet();
                        string cmdEndText = "";
                        if (dr["Phase"].ToString() == "1")
                        {
                            cmdEndText = "select distinct Ph1Start,Ph1End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph1Start"] != null && dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph1End"] != null && dsEnd.Tables[0].Rows[0]["Ph1End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph1End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }
                        else if (dr["Phase"].ToString() == "2")
                        {
                            cmdEndText = "select distinct Ph2Start,Ph2End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph2Start"] != null && dsEnd.Tables[0].Rows[0]["Ph2Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph2Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph2End"] != null && dsEnd.Tables[0].Rows[0]["Ph2End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph2End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }
                        else if (dr["Phase"].ToString() == "3")
                        {
                            cmdEndText = "select distinct Ph3Start,Ph3End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph3Start"] != null && dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph3End"] != null && dsEnd.Tables[0].Rows[0]["Ph3End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph3End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }

                        tblSPHtml += "<td style='text-align:center; vertical-align:middle;'>" + startTime + " " + "-" + " " + resultEndTime + "";
                        tblSPHtml += "</td>";
                        for (int i = 0; i < 3; i++)
                        {
                            count = 0;
                            tblSPHtml += "<td style='text-align:center;'>";

                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (contest1 == dr1["Contest"].ToString() && phase1 == dr1["Phase"].ToString())
                                {
                                    //count++;
                                    //if (count > 1)
                                    //{
                                    borderStyle = "border-bottom:1px solid black;";
                                    //}
                                    //else
                                    //{
                                    //    borderStyle = "border-bottom-style:hidden.";
                                    //}

                                    if (i == 0)
                                    {
                                        tblSPHtml += "<div style='" + borderStyle + "' >" + dr1["ProductCode"].ToString() + "</div>";
                                    }
                                    else if (i == 1)
                                    {
                                        tblSPHtml += "<div style='" + borderStyle + "' >" + dr1["BldgId"].ToString() + " " + dr1["RoomNumber"].ToString() + "</div>";
                                    }
                                    else if (i == 2)
                                    {
                                        tblSPHtml += "<div style='" + borderStyle + "' >" + dr1["ProductCode"].ToString() + " " + "-" + " " + dr1["StartBadgeNo"].ToString() + " " + "-" + " " + dr1["EndBadgeNo"].ToString() + "</div>";
                                    }

                                }

                            }



                            tblSPHtml += "</td>";
                        }



                        tblSPHtml += "</tr>";
                    }
                    contestname = dr["Contest"].ToString();
                    phase = dr["Phase"].ToString();
                }

            }
            else
            {
                lblMsg.Text = "No Record Exist";
                lblMsg.ForeColor = Color.Red;
                btnExport.Visible = false;
                dvSchedulePosting.Visible = false;
                dvScheduleReportingDay2.Visible = false;
            }
        }
        tblSPHtml += "</table>";
        ltrSchedulePosting.Text = tblSPHtml;
    }
    public void generateReportSchedulePostingDay2()
    {
        btnExport.Visible = true;
        lblMsg.Text = "";
        string cmdText = "";
        DataSet ds = new DataSet();
        string tblSPHtml = "";
        DataSet dsDate = new DataSet();
        string cmdDatetext = "";
        string maxDate = "";
        tblSPHtml = "<table border='1' align='center' width='900px' cellspacing='0' cellpadding='0'>";
        tblSPHtml += "<tr>";
        tblSPHtml += "<td style='width:300px; font-weight:bold; text-align:center; font-size:14px;'>Contest</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Time</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Group</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Building Room</td>";
        tblSPHtml += "<td style='width:200px; font-weight:bold; text-align:center; font-size:14px;'>Badge No</td>";
        tblSPHtml += "</tr>";

        cmdText = "select distinct P.Name as Contest,C.ProductId,C.ProductCode as pgCode,C.ProductGroupCode,C.Ph1Start,C.Ph2Start,C.Ph3Start,RC.Phase,RC.RoomNumber,RC.BldgID,RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery,CS.Duration,rtrim(ltrim(rc.productcode))+'-' + case rc.phase when 1 then 'I' when 2 then 'II' when 3 then 'III' when 4 then 'IV' end  +' - '+ convert(char(3),rc.SeqNo) ProductCode,rc.StartBadgeNo,rc.EndBadgeNo,C.ProductGroupId from Contest C left join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' ) left join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and  RC.BldgID=RL.BldgID and RC.RoomNumber=RL.RoomNumber) left join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase) inner join Product P on P.productId=c.ProductId where C.Contest_Year='" + ddYear.SelectedValue + "' and         DATENAME(dw,c.[ContestDate]) ='Sunday'  and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "";

        //cmdText = "SELECT P.Name as Contest,rl.eventid,rl.event,rl.BldgName BldgName,rl.BldgID BldgID,case rl.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery, rl.RoomNumber RoomNumber, rl.capacity,rtrim(ltrim(rs.productcode))+'-' + case rs.phase when 1 then 'I' when 2 then 'II' when 3 then 'III' when 4 then 'IV' end  +' - '+ convert(char(3),rs.SeqNo) ProductCode, rs.ProductGroupCode PgCode,rs.Phase,rs.StartBadgeNo,rs.EndBadgeNo,rs.ProductgroupId,rs.ProductId";
        //cmdText = cmdText + " FROM ROOMSCHEDULE rs inner join RoomList rl on rs.BldgId= rl.BldgId and rs.ContestYear = rl.ContestYear and  rs.roomnumber=rl.roomnumber  and  DATENAME(dw,rs.[date]) ='Sunday' and rl.contestyear=" + ddYear.SelectedValue;
        //cmdText = cmdText + " and rl.eventid=" + ddEvent.SelectedValue + " and rl.ChapterId=" + ddchapter.SelectedValue + " inner join Product P on P.productId=rs.ProductId";



        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        string contestname = "";
        string phase = "";
        string contest1 = "";
        string phase1 = "";
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                cmdDatetext = "select Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and eventid=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + "";
                dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDatetext);
                if (dsDate.Tables[0] != null)
                {
                    if (dsDate.Tables[0].Rows.Count > 0)
                    {
                        if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                        {
                            maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMM dd,yyyy");
                            dvScheduleTextDay2.InnerText = "Sunday " + maxDate + "";
                        }
                    }
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    contest1 = dr["Contest"].ToString();
                    phase1 = dr["Phase"].ToString();
                    if (contestname != dr["Contest"].ToString() || phase != dr["Phase"].ToString())
                    {
                        string startTime = "";
                        string EndTime = "";
                        string resultEndTime = "";
                        tblSPHtml += "<tr>";
                        tblSPHtml += "<td style='text-align:center; vertical-align:middle;'>" + dr["Contest"].ToString() + " " + "-" + " " + "Phase " + dr["Phase"] + "";
                        tblSPHtml += "</td>";
                        DataSet dsEnd = new DataSet();
                        string cmdEndText = "";
                        if (dr["Phase"].ToString() == "1")
                        {
                            cmdEndText = "select distinct Ph1Start,Ph1End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + maxDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph1Start"] != null && dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph1End"] != null && dsEnd.Tables[0].Rows[0]["Ph1End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph1End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }
                        else if (dr["Phase"].ToString() == "2")
                        {
                            cmdEndText = "select distinct Ph2Start,Ph2End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + maxDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph2Start"] != null && dsEnd.Tables[0].Rows[0]["Ph2Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph2Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph2End"] != null && dsEnd.Tables[0].Rows[0]["Ph2End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph2End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }
                        else if (dr["Phase"].ToString() == "3")
                        {
                            cmdEndText = "select distinct Ph3Start,Ph3End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + maxDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                            if (dsEnd.Tables[0] != null)
                            {
                                if (dsEnd.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd.Tables[0].Rows[0]["Ph3Start"] != null && dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString() != "")
                                    {
                                        startTime = dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString();
                                    }
                                    if (dsEnd.Tables[0].Rows[0]["Ph3End"] != null && dsEnd.Tables[0].Rows[0]["Ph3End"].ToString() != "")
                                    {
                                        EndTime = dsEnd.Tables[0].Rows[0]["Ph3End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                        }
                        tblSPHtml += "<td style='text-align:center; vertical-align:middle;'>" + startTime + " " + "-" + " " + resultEndTime + "";
                        tblSPHtml += "</td>";
                        for (int i = 0; i < 3; i++)
                        {
                            tblSPHtml += "<td style='text-align:center;'>";

                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (contest1 == dr1["Contest"].ToString() && phase1 == dr1["Phase"].ToString())
                                {

                                    if (i == 0)
                                    {
                                        tblSPHtml += "<div style='border-bottom:1px solid #000000;'>" + dr1["ProductCode"].ToString() + "</div>";
                                    }
                                    else if (i == 1)
                                    {
                                        tblSPHtml += "<div style='border-bottom:1px solid #000000;'>" + dr1["BldgId"].ToString() + " " + dr1["RoomNumber"].ToString() + "</div>";
                                    }
                                    else if (i == 2)
                                    {
                                        tblSPHtml += "<div style='border-bottom:1px solid #000000;'>" + dr1["ProductCode"].ToString() + " " + "-" + " " + dr1["StartBadgeNo"].ToString() + " " + "-" + " " + dr1["EndBadgeNo"].ToString() + "</div>";
                                    }

                                }

                            }



                            tblSPHtml += "</td>";
                        }



                        tblSPHtml += "</tr>";
                    }
                    contestname = dr["Contest"].ToString();
                    phase = dr["Phase"].ToString();
                }

            }
            else
            {
                lblMsg.Text = "No Record Exist";
                btnExport.Visible = false;
            }
        }
        tblSPHtml += "</table>";
        ltrSchedulePostingDay2.Text = tblSPHtml;

        cmdText = "SELECT P.Name as Contest,rl.eventid,rl.event,rl.BldgName BldgName,rl.BldgID BldgID,case rl.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery, rl.RoomNumber RoomNumber, rl.capacity,rtrim(ltrim(rs.productcode))+'-' + case rs.phase when 1 then 'I' when 2 then 'II' when 3 then 'III' when 4 then 'IV' end  +' - '+ convert(char(3),rs.SeqNo) ProductCode, rs.ProductGroupCode PgCode,rs.Phase,rs.StartBadgeNo,rs.EndBadgeNo,rs.ProductGroupId,rs.ProductId";
        cmdText = cmdText + " FROM ROOMSCHEDULE rs inner join RoomList rl on rs.BldgId= rl.BldgId and rs.ContestYear = rl.ContestYear and  rs.roomnumber=rl.roomnumber  and  DATENAME(dw,rs.[date]) ='Saturday' and rl.contestyear=" + ddYear.SelectedValue;
        cmdText = cmdText + " and rl.eventid=" + ddEvent.SelectedValue + " and rl.ChapterId=" + ddchapter.SelectedValue + " inner join Product P on P.productId=rs.ProductId";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "Room List is not yet available.  Please follow up.";
            }
        }
        else
        {
            lblMsg.ForeColor = Color.Red;
            lblMsg.Text = "Room List is not yet available.  Please follow up.";
        }
    }
    public void generateTechStaffScheduleReport()
    {
        string tblStaffHtml = "";
        lblMsg.Text = "";
        tblStaffHtml += "<table border='1' width='1200px' cellspacing='0' cellpadding='0' style='margin-bottom:10px;'>";
        tblStaffHtml += "<tr style='font-weight:bold;'>";
        tblStaffHtml += " <td style='text-align:center; width:75px;'>Bee</td>";
        tblStaffHtml += " <td style='text-align:center; width:100px;'>Level</td>";
        tblStaffHtml += " <td style='text-align:center; width:175px;'>Phase I (Written)";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Room</td>";
        tblStaffHtml += "<td style='width: 100px;'>Team Members</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";
        tblStaffHtml += " <td style='text-align:center; width:75px;'>Time</td>";
        tblStaffHtml += " <td style='text-align:center;'>Phase II";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Team/Room</td>";
        tblStaffHtml += "<td style='width: 150px;'>Chief Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Associate Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Laptop Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Pronouncer</td>";
        tblStaffHtml += "<td style='width: 150px;'>Proctor</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";

        tblStaffHtml += " <td style='text-align:center;'>Phase III";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Room</td>";
        tblStaffHtml += "<td style='width: 100px;'>Team Members</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";

        tblStaffHtml += "</tr>";

        // table body starts
        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                    headertext = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMM dd");
                    dvDay1StaffSchedule.InnerText = "Day 1 : Saturday " + headertext + "th - Volunteer Assignments";
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");
                    headertext = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMM dd");
                }

            }
        }
        if (minDate != "" && maxDate != "")
        {
            dvTechStaffScheduleReport.Visible = true;
            string cmdText = "";
            cmdText += "select PG.Name,P.Name as Product,CT.ProductID,CT.ProductGroupID,CT.BldgID,CT.RoomNumber,CT.Phase,ID.FirstName as ChiefJudge";
            cmdText += ",ID1.FirstName as AssociateJudge, ID2.FirstName as LaptopJudge, ID3.FirstName as Pronouncer,ID4.FirstName as Proctor";
            cmdText += " from ContestTeamSchedule CT inner join IndSpouse ID on (CT.CJMemberID=ID.AutoMemberID)";
            cmdText += "left join IndSpouse ID1 on (CT.AJMemberID=ID1.AutoMemberID)";
            cmdText += "left join IndSpouse ID2 on (CT.LTJudMemberID=ID2.AutoMemberID)";
            cmdText += "left join IndSpouse ID3 on (CT.PronMemberID=ID3.AutoMemberID)";
            cmdText += "left join IndSpouse ID4 on (CT.ProcMemberID=ID4.AutoMemberID)";

            cmdText += "inner join ProductGroup PG on(CT.ProductGroupID=PG.ProductGroupId)";
            cmdText += "inner join Product P on(CT.ProductID=P.ProductId)";
            cmdText += "where CT.ContestYear='" + ddYear.SelectedValue + "' and CT.EventID=" + ddEvent.SelectedValue + " and CT.ChapterID=" + ddchapter.SelectedValue + " and CT.Date='" + minDate + "' Order by CT.ProductID,CT.Phase ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            string contest = "";
            string productname = "";
            string productContest = "";
            string teamContest = "";
            string phase2Contest = "";
            int rowSapnCount = 0;
            int i1 = 0;
            int i2 = 0;
            int i3 = 0;

            string rowspanContest = "";

            string rowSapnProduct = "";



            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string startTime = "";
                        string EndTime = "";
                        string resultEndTime = "";

                        i2 = 0;
                        if (i1 > 1 || i1 > 2)
                        {
                            i1 = 0;
                        }
                        if (contest != dr["Name"].ToString() || productname != dr["Product"].ToString())
                        {
                            i1++;
                            tblStaffHtml += "<tr>";
                            DataRow[] dtCurRow = ds.Tables[0].Select("Name ='" + dr["Name"].ToString() + "' and Product = '" + dr["Product"].ToString() + "'");
                            if (dtCurRow.Length > 1)
                            {
                                if (i1 == 1)
                                {
                                    tblStaffHtml += "<td style='vertical-align:middle;' rowspan='2'>" + dr["Name"].ToString() + "</td>";
                                }
                            }
                            else
                            {
                                tblStaffHtml += "<td style='vertical-align:middle;'>" + dr["Name"].ToString() + "</td>";
                            }
                            tblStaffHtml += "<td>";

                            tblStaffHtml += "<table>";

                            foreach (DataRow drProd in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drProd["Name"].ToString() && dr["Product"].ToString() == drProd["Product"].ToString())
                                {
                                    if (productContest != drProd["Product"].ToString())
                                    {
                                        tblStaffHtml += "<tr>";
                                        tblStaffHtml += "<td style='vertical-align:middle;'>" + drProd["Product"].ToString() + "</td>";
                                        tblStaffHtml += "</tr>";
                                    }
                                }

                                productContest = drProd["Product"].ToString();
                            }
                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drTm in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drTm["Name"].ToString() && dr["Product"].ToString() == drTm["Product"].ToString())
                                {
                                    //if (teamContest != drTm["Product"].ToString())
                                    //{

                                    if (drTm["Phase"].ToString() == "1")
                                    {
                                        DataSet dsEnd = new DataSet();
                                        string cmdEndText = "";
                                        cmdEndText = "select distinct Ph1Start,Ph1End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                                        dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                                        if (dsEnd.Tables[0] != null)
                                        {
                                            if (dsEnd.Tables[0].Rows.Count > 0)
                                            {
                                                if (dsEnd.Tables[0].Rows[0]["Ph1Start"] != null && dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString() != "")
                                                {
                                                    startTime = dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString();
                                                }
                                                if (dsEnd.Tables[0].Rows[0]["Ph1End"] != null && dsEnd.Tables[0].Rows[0]["Ph1End"].ToString() != "")
                                                {
                                                    EndTime = dsEnd.Tables[0].Rows[0]["Ph1End"].ToString();
                                                    resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                                }
                                            }
                                        }

                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:75px;'>" + drTm["BldgId"].ToString() + " " + drTm["RoomNumber"].ToString() + "<br>" + startTime + "-" + resultEndTime + "</td>";

                                        tblStaffHtml += "<td  style='width:100px;'><div>" + drTm["ChiefJudge"].ToString() + "" + (drTm["ChiefJudge"].ToString() == "" ? " " : "(CJ)") + "</div><div>" + drTm["AssociateJudge"].ToString() + "" + (drTm["AssociateJudge"].ToString() == "" ? " " : "(AJ)") + "</div><div>" + drTm["LaptopJudge"].ToString() + "" + (drTm["LaptopJudge"].ToString() == "" ? " " : "(LP)") + "</div><div>" + drTm["Pronouncer"].ToString() + "" + (drTm["Pronouncer"].ToString() == "" ? " " : "(P)") + "</div><div>" + drTm["Proctor"].ToString() + "" + (drTm["Proctor"].ToString() == "" ? " " : "(Proc)") + "</div></td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }
                                teamContest = drTm["Product"].ToString();
                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            DataSet dsEnd1 = new DataSet();
                            string cmdEndText1 = "";
                            cmdEndText1 = "select distinct Ph2Start,Ph2End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText1);
                            if (dsEnd1.Tables[0] != null)
                            {
                                if (dsEnd1.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd1.Tables[0].Rows[0]["Ph2Start"] != null && dsEnd1.Tables[0].Rows[0]["Ph2Start"].ToString() != "")
                                    {
                                        startTime = dsEnd1.Tables[0].Rows[0]["Ph2Start"].ToString();
                                    }
                                    if (dsEnd1.Tables[0].Rows[0]["Ph2End"] != null && dsEnd1.Tables[0].Rows[0]["Ph2End"].ToString() != "")
                                    {
                                        EndTime = dsEnd1.Tables[0].Rows[0]["Ph2End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }
                            tblStaffHtml += "<td style='vertical-align:middle;'>" + startTime + "-" + resultEndTime + "</td>";

                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drPh2 in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drPh2["Name"].ToString() && dr["Product"].ToString() == drPh2["Product"].ToString())
                                {
                                    //if (phase2Contest != drPh2["Product"].ToString())
                                    //{

                                    if (drPh2["Phase"].ToString() == "2")
                                    {


                                        i2++;
                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:120px;'>" + i2 + "/" + drPh2["BldgId"].ToString() + " " + drPh2["RoomNumber"].ToString() + "</td>";

                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["ChiefJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["AssociateJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["LaptopJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["Pronouncer"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["Proctor"].ToString() + "</td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }
                                phase2Contest = drPh2["Product"].ToString();
                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drph3 in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drph3["Name"].ToString() && dr["Product"].ToString() == drph3["Product"].ToString())
                                {
                                    //if (teamContest != drTm["Product"].ToString())
                                    //{

                                    if (drph3["Phase"].ToString() == "3")
                                    {
                                        DataSet dsEnd = new DataSet();
                                        string cmdEndText = "";
                                        cmdEndText = "select distinct Ph3Start,Ph3End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                                        dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                                        if (dsEnd.Tables[0] != null)
                                        {
                                            if (dsEnd.Tables[0].Rows.Count > 0)
                                            {
                                                if (dsEnd.Tables[0].Rows[0]["Ph3Start"] != null && dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString() != "")
                                                {
                                                    startTime = dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString();
                                                }
                                                if (dsEnd.Tables[0].Rows[0]["Ph3End"] != null && dsEnd.Tables[0].Rows[0]["Ph3End"].ToString() != "")
                                                {
                                                    EndTime = dsEnd.Tables[0].Rows[0]["Ph3End"].ToString();
                                                    resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                                }
                                            }
                                        }

                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:75px;'>" + drph3["BldgId"].ToString() + " " + drph3["RoomNumber"].ToString() + "<br>" + startTime + "-" + resultEndTime + "</td>";

                                        tblStaffHtml += "<td  style='width:100px;'><div>" + drph3["ChiefJudge"].ToString() + "" + (drph3["ChiefJudge"].ToString() == "" ? " " : "(CJ)") + "</div><div>" + drph3["AssociateJudge"].ToString() + "" + (drph3["AssociateJudge"].ToString() == "" ? " " : "(AJ)") + "</div><div>" + drph3["LaptopJudge"].ToString() + "" + (drph3["LaptopJudge"].ToString() == "" ? " " : "(LP)") + "</div><div>" + drph3["Pronouncer"].ToString() + "" + (drph3["Pronouncer"].ToString() == "" ? " " : "(P)") + "</div><div>" + drph3["Proctor"].ToString() + "" + (drph3["Proctor"].ToString() == "" ? " " : "(Proc)") + "</div></td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }

                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            tblStaffHtml += "</tr>";
                        }
                        contest = dr["Name"].ToString();
                        productname = dr["Product"].ToString();
                    }
                }
            }



            tblStaffHtml += "</table>";
            ltrTechStaffSchedule.Text = tblStaffHtml;
        }
        else
        {
            lblMsg.Text = "No Record Exist.";
            lblMsg.ForeColor = Color.Red;
            dvTechStaffScheduleReport.Visible = false;
        }

    }
    public void generateTechStaffScheduleReportDay2()
    {
        string tblStaffHtml = "";
        lblMsg.Text = "";
        tblStaffHtml += "<table border='1' width='1200px' cellspacing='0' cellpadding='0'>";
        tblStaffHtml += "<tr style='font-weight:bold;'>";
        tblStaffHtml += " <td style='text-align:center; width:75px;'>Bee</td>";
        tblStaffHtml += " <td style='text-align:center; width:100px;'>Level</td>";
        tblStaffHtml += " <td style='text-align:center; width:175px;'>Phase I (Written)";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Room</td>";
        tblStaffHtml += "<td style='width: 100px;'>Team Members</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";
        tblStaffHtml += " <td style='text-align:center; width:75px;'>Time</td>";
        tblStaffHtml += " <td style='text-align:center;'>Phase II";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Team/Room</td>";
        tblStaffHtml += "<td style='width: 150px;'>Chief Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Associate Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Laptop Judge</td>";
        tblStaffHtml += "<td style='width: 150px;'>Pronouncer</td>";
        tblStaffHtml += "<td style='width: 150px;'>Proctor</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";

        tblStaffHtml += " <td style='text-align:center;'>Phase III";

        tblStaffHtml += "<table style='border-top:1px solid black; border-collapse:collapse;'>";
        tblStaffHtml += "<tr>";
        tblStaffHtml += "<td style='width: 75px;'>Room</td>";
        tblStaffHtml += "<td style='width: 100px;'>Team Members</td>";
        tblStaffHtml += "</tr>";
        tblStaffHtml += "</table>";


        tblStaffHtml += "</td>";

        tblStaffHtml += "</tr>";


        // table body starts
        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");
                    headertext = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMM dd");
                    dvDay2StaffSchedule.InnerText = "Day 2 : Sunday " + headertext + "th - Volunteer Assignments";
                }

            }
        }
        if (minDate != "" && maxDate != "")
        {
            string cmdText = "";
            cmdText += "select PG.Name,P.Name as Product,CT.ProductID,CT.ProductGroupID,CT.BldgID,CT.RoomNumber,CT.Phase,ID.FirstName as ChiefJudge";
            cmdText += ",ID1.FirstName as AssociateJudge, ID2.FirstName as LaptopJudge, ID3.FirstName as Pronouncer,ID4.FirstName as Proctor";
            cmdText += " from ContestTeamSchedule CT inner join IndSpouse ID on (CT.CJMemberID=ID.AutoMemberID)";
            cmdText += "left join IndSpouse ID1 on (CT.AJMemberID=ID1.AutoMemberID)";
            cmdText += "left join IndSpouse ID2 on (CT.LTJudMemberID=ID2.AutoMemberID)";
            cmdText += "left join IndSpouse ID3 on (CT.PronMemberID=ID3.AutoMemberID)";
            cmdText += "left join IndSpouse ID4 on (CT.ProcMemberID=ID4.AutoMemberID)";

            cmdText += "inner join ProductGroup PG on(CT.ProductGroupID=PG.ProductGroupId)";
            cmdText += "inner join Product P on(CT.ProductID=P.ProductId)";
            cmdText += "where CT.ContestYear='" + ddYear.SelectedValue + "' and CT.EventID=" + ddEvent.SelectedValue + " and CT.ChapterID=" + ddchapter.SelectedValue + " and CT.Date='" + maxDate + "' Order by CT.ProductID,CT.Phase ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            string contest = "";
            string productname = "";
            string productContest = "";
            string teamContest = "";
            string phase2Contest = "";
            int rowSapnCount = 0;
            int i1 = 0;
            int i2 = 0;
            int i3 = 0;

            string rowspanContest = "";
            string rowSapnProduct = "";
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string startTime = "";
                        string EndTime = "";
                        string resultEndTime = "";
                        i2 = 0;
                        if (i1 > 1 || i1 > 2)
                        {
                            i1 = 0;
                        }
                        if (contest != dr["Name"].ToString() || productname != dr["Product"].ToString())
                        {
                            i1++;
                            tblStaffHtml += "<tr>";
                            DataRow[] dtCurRow = ds.Tables[0].Select("Name ='" + dr["Name"].ToString() + "' and Product = '" + dr["Product"].ToString() + "'");
                            if (dtCurRow.Length > 1)
                            {
                                if (i1 == 1)
                                {
                                    tblStaffHtml += "<td style='vertical-align:middle;' rowspan='2'>" + dr["Name"].ToString() + "</td>";
                                }
                            }
                            else
                            {
                                tblStaffHtml += "<td style='vertical-align:middle;'>" + dr["Name"].ToString() + "</td>";
                            }
                            tblStaffHtml += "<td>";

                            tblStaffHtml += "<table>";

                            foreach (DataRow drProd in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drProd["Name"].ToString() && dr["Product"].ToString() == drProd["Product"].ToString())
                                {
                                    if (productContest != drProd["Product"].ToString())
                                    {
                                        tblStaffHtml += "<tr>";
                                        tblStaffHtml += "<td style='vertical-align:middle;'>" + drProd["Product"].ToString() + "</td>";
                                        tblStaffHtml += "</tr>";
                                    }
                                }

                                productContest = drProd["Product"].ToString();
                            }
                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drTm in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drTm["Name"].ToString() && dr["Product"].ToString() == drTm["Product"].ToString())
                                {
                                    //if (teamContest != drTm["Product"].ToString())
                                    //{

                                    if (drTm["Phase"].ToString() == "1")
                                    {
                                        DataSet dsEnd = new DataSet();
                                        string cmdEndText = "";
                                        cmdEndText = "select distinct Ph1Start,Ph1End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + maxDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                                        dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                                        if (dsEnd.Tables[0] != null)
                                        {
                                            if (dsEnd.Tables[0].Rows.Count > 0)
                                            {
                                                if (dsEnd.Tables[0].Rows[0]["Ph1Start"] != null && dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString() != "")
                                                {
                                                    startTime = dsEnd.Tables[0].Rows[0]["Ph1Start"].ToString();
                                                }
                                                if (dsEnd.Tables[0].Rows[0]["Ph1End"] != null && dsEnd.Tables[0].Rows[0]["Ph1End"].ToString() != "")
                                                {
                                                    EndTime = dsEnd.Tables[0].Rows[0]["Ph1End"].ToString();
                                                    resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                                }
                                            }
                                        }

                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:75px;'>" + drTm["BldgId"].ToString() + " " + drTm["RoomNumber"].ToString() + "<br>" + startTime + "-" + resultEndTime + "</td>";

                                        tblStaffHtml += "<td  style='width:100px;'><div>" + drTm["ChiefJudge"].ToString() + "" + (drTm["ChiefJudge"].ToString() == "" ? " " : "(CJ)") + "</div><div>" + drTm["AssociateJudge"].ToString() + "" + (drTm["AssociateJudge"].ToString() == "" ? " " : "(AJ)") + "</div><div>" + drTm["LaptopJudge"].ToString() + "" + (drTm["LaptopJudge"].ToString() == "" ? " " : "(LP)") + "</div><div>" + drTm["Pronouncer"].ToString() + "" + (drTm["Pronouncer"].ToString() == "" ? " " : "(P)") + "</div><div>" + drTm["Proctor"].ToString() + "" + (drTm["Proctor"].ToString() == "" ? " " : "(Proc)") + "</div></td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }
                                teamContest = drTm["Product"].ToString();
                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            DataSet dsEnd1 = new DataSet();
                            string cmdEndText1 = "";
                            cmdEndText1 = "select distinct Ph2Start,Ph2End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                            dsEnd1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText1);
                            if (dsEnd1.Tables[0] != null)
                            {
                                if (dsEnd1.Tables[0].Rows.Count > 0)
                                {
                                    if (dsEnd1.Tables[0].Rows[0]["Ph2Start"] != null && dsEnd1.Tables[0].Rows[0]["Ph2Start"].ToString() != "")
                                    {
                                        startTime = dsEnd1.Tables[0].Rows[0]["Ph2Start"].ToString();
                                    }
                                    if (dsEnd1.Tables[0].Rows[0]["Ph2End"] != null && dsEnd1.Tables[0].Rows[0]["Ph2End"].ToString() != "")
                                    {
                                        EndTime = dsEnd1.Tables[0].Rows[0]["Ph2End"].ToString();
                                        resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                    }
                                }
                            }

                            tblStaffHtml += "<td>" + startTime + "-" + resultEndTime + "</td>";

                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drPh2 in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drPh2["Name"].ToString() && dr["Product"].ToString() == drPh2["Product"].ToString())
                                {
                                    //if (phase2Contest != drPh2["Product"].ToString())
                                    //{

                                    if (drPh2["Phase"].ToString() == "2")
                                    {
                                        i2++;
                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:120px;'>" + i2 + "/" + drPh2["BldgId"].ToString() + " " + drPh2["RoomNumber"].ToString() + "<br>" + startTime + "-" + resultEndTime + "</td>";

                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["ChiefJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["AssociateJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["LaptopJudge"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["Pronouncer"].ToString() + "</td>";
                                        tblStaffHtml += "<td style='width:150px;'>" + drPh2["Proctor"].ToString() + "</td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }
                                phase2Contest = drPh2["Product"].ToString();
                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            //tblStaffHtml += "<td>" + dr["Name"].ToString() + "</td>";
                            tblStaffHtml += "<td>";
                            tblStaffHtml += "<table>";
                            foreach (DataRow drph3 in ds.Tables[0].Rows)
                            {
                                if (dr["Name"].ToString() == drph3["Name"].ToString() && dr["Product"].ToString() == drph3["Product"].ToString())
                                {
                                    //if (teamContest != drTm["Product"].ToString())
                                    //{

                                    if (drph3["Phase"].ToString() == "3")
                                    {
                                        DataSet dsEnd = new DataSet();
                                        string cmdEndText = "";
                                        cmdEndText = "select distinct Ph3Start,Ph3End from Contest where EventID=" + ddEvent.SelectedValue + " and NsfChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "' and ProductGroupID=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + "";
                                        dsEnd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdEndText);
                                        if (dsEnd.Tables[0] != null)
                                        {
                                            if (dsEnd.Tables[0].Rows.Count > 0)
                                            {
                                                if (dsEnd.Tables[0].Rows[0]["Ph3Start"] != null && dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString() != "")
                                                {
                                                    startTime = dsEnd.Tables[0].Rows[0]["Ph3Start"].ToString();
                                                }
                                                if (dsEnd.Tables[0].Rows[0]["Ph3End"] != null && dsEnd.Tables[0].Rows[0]["Ph3End"].ToString() != "")
                                                {
                                                    EndTime = dsEnd.Tables[0].Rows[0]["Ph3End"].ToString();
                                                    resultEndTime = EndTime.Remove(EndTime.Length - 3);
                                                }
                                            }
                                        }

                                        tblStaffHtml += "<tr>";

                                        tblStaffHtml += "<td style='width:75px;'>" + drph3["BldgId"].ToString() + " " + drph3["RoomNumber"].ToString() + "</td>";

                                        tblStaffHtml += "<td  style='width:100px;'><div>" + drph3["ChiefJudge"].ToString() + "" + (drph3["ChiefJudge"].ToString() == "" ? " " : "(CJ)") + "</div><div>" + drph3["AssociateJudge"].ToString() + "" + (drph3["AssociateJudge"].ToString() == "" ? " " : "(AJ)") + "</div><div>" + drph3["LaptopJudge"].ToString() + "" + (drph3["LaptopJudge"].ToString() == "" ? " " : "(LP)") + "</div><div>" + drph3["Pronouncer"].ToString() + "" + (drph3["Pronouncer"].ToString() == "" ? " " : "(P)") + "</div><div>" + drph3["Proctor"].ToString() + "" + (drph3["Proctor"].ToString() == "" ? " " : "(Proc)") + "</div></td>";

                                        tblStaffHtml += "</tr>";
                                    }
                                    //}
                                }

                            }

                            tblStaffHtml += "</table>";

                            tblStaffHtml += "</td>";

                            tblStaffHtml += "</tr>";
                        }
                        contest = dr["Name"].ToString();
                        productname = dr["Product"].ToString();
                    }
                }
            }



            tblStaffHtml += "</table>";
            ltrTechStaffScheduleDay2.Text = tblStaffHtml;
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count == 0)
                {
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = "Room List and Member List are not yet available.  Please follow up.";
                }
            }
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlLevel.SelectedValue != "0")
        {

            if (ddlLevel.SelectedValue == "1")
            {
                highLevelChart();
                highLevelChartDay2();
                dvTechStaffSchedule.Visible = false;
                dvSchedulePosting.Visible = false;
                dvScheduleReportingDay2.Visible = false;
                dvTechStaffScheduleDay2.Visible = false;
                dvTechStaffScheduleReport.Visible = false;
            }
            else if (ddlLevel.SelectedValue == "2")
            {
                detailScheduleChart();
                detailScheduleChart2v3();

                //detailScheduleChart2();
                //generateDetailSchedule1();
                //generateDetailSchedule2();

                // OLD Chart

                // generateDetailedSchedule();
                //generateDetailedSchedule2();
                //generateChart();
                //generateChartDay2();
                dvTechStaffSchedule.Visible = false;
                dvSchedulePosting.Visible = false;
                dvScheduleReportingDay2.Visible = false;
                dvTechStaffScheduleReport.Visible = false;

            }

            else if (ddlLevel.SelectedValue == "4")
            {
                dvTechStaffSchedule.Visible = true;
                dvDay1ScheduleChart.Visible = false;
                dvDay2ScheduleChart.Visible = false;
                dvSchedulePosting.Visible = false;
                dvScheduleReportingDay2.Visible = false;
                dvTechStaffScheduleDay2.Visible = true;
                dvDay1StaffSchedule.Visible = true;
                dvDay2StaffSchedule.Visible = true;
                btnExport.Visible = true;
                dvTechStaffScheduleReport.Visible = true;
                generateTechStaffScheduleReport();
                generateTechStaffScheduleReportDay2();

            }
            else
            {
                dvDay1ScheduleChart.Visible = false;
                dvDay2ScheduleChart.Visible = false;
                dvTechStaffSchedule.Visible = false;
                dvSchedulePosting.Visible = true;
                dvScheduleReportingDay2.Visible = true;
                dvTechStaffScheduleReport.Visible = false;
                generateReportSchedulePosting();
                generateReportSchedulePostingDay2();
            }
        }
        else
        {
            lblMsg.Text = "Select Level";
            lblMsg.ForeColor = Color.Red;
        }



    }

    public void detailedScheduleReport()
    {
        string tableHtml = "";
        tableHtml += "";
    }


    public void generateDetailSchedule1()
    {
        string[] array = {

        "08:00 - 09:00",
        "09:15 - 10:15",
        "10:30 - 11:30",
        " 1/2 hr",
        "12:00 - 13:00",
        "13:15 - 14:15",
        "14:30 - 15:30",
        "15:30 - 16:30",
        "16:30 - 18:30"
    };

        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");

                }

            }
        }
        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "8";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);
        string tblhtml = "";
        string cmdText = "";
        DataSet ds = new DataSet();

        cmdText += "select distinct C.ProductId,C.ProductCode,C.ProductGroupCode,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,RC.Phase,RC.SeqNo,RC.RoomNumber,RC.BldgID ";
        cmdText += ",RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery,CS.Duration from Contest C ";
        cmdText += "inner join RoomSchedule RC on ";
        cmdText += "(C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )";
        cmdText += "inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber=RL.RoomNumber)";
        cmdText += "inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)";
        cmdText += "where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + minDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + " order by RC.RoomNumber,RC.Phase, ProductGroupCode,RC.SeqNo ASC";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

        tblhtml += "<table border=1 style='border-collapse:collapse;' width='1250px' cellspacing='0' cellpadding='0'>";
        tblhtml += "<tr><td style='font-weight:bold;text-align:center; border:1px solid black; font-size:10pt;background-color:#CD7F32;color:#fdd017;height:10px' colspan='52'> <h1> Day 1 Saturday</h1></td></tr>";
        tblhtml += "<tr style='font-weight:bold;text-align:center;vertical-align:middle;'>";
        tblhtml += "<td style='font-weight:bold; width:120px;'>Bldg</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Room</td>";
        tblhtml += "<td style='font-weight:bold; width:30px;'>Cap</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Registration<br>6:30</td>";
        TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
        ArrayList arrList = new ArrayList();
        string color = "";
        int columnCount = 0;

        while (true)
        {
            tblhtml += "<td style='width:10px; height:40px; font-weight:bold; border-right: 1px solid black; color:#000000; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";
            //tblHtml += "<td style='width:10px; height:40px;><div class='rotate' style='-moz-transform:rotate(90deg); width:10px; height:30px;'>" +  + "</div></td>";

            arrList.Add(timeInterval);
            timeInterval = timeInterval + TimeSpan.FromMinutes(15);
            columnCount++;
            if (timeInterval.Hours == 19)
            {
                break; // TODO: might not be correct. Was : Exit While
            }

        }


        tblhtml += "</tr>";
        string roomNo = "";
        int p1RowCount = 0;
        string productGroup = "";
        string phase = "";
        string seqNo = "";
        string ProductGroupCode = string.Empty;
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                btnExport.Visible = true;
                string RoomNumber = string.Empty;
                string Rm = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    if (RoomNumber != dr["RoomNumber"].ToString())
                    {

                        DataSet dsTime = new DataSet();
                        string CmdTimeText = "select  C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ProductId,C.ProductCode,C.ProductGroupCode,RC.Phase,RC.SeqNo,RC.RoomNumber from Contest C inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + dr["RoomNumber"].ToString().Trim() + "')inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + minDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "  order by RC.Phase, convert(time,C.Ph2Start,100),convert(time,C.Ph2End,100) ASC";

                        dsTime = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdTimeText);


                        tblhtml += "<tr style='font-weight:bold;'>";
                        tblhtml += "<td>" + dr["BldgName"].ToString() + " (" + dr["BldgId"].ToString() + ")</td>";
                        tblhtml += "<td>" + dr["BldgId"].ToString() + " " + dr["RoomNumber"].ToString() + "</td>";
                        tblhtml += "<td>" + dr["Capacity"].ToString() + "</td>";
                        tblhtml += "<td>" + dr["Gallery"].ToString() + "</td>";

                        string time = "";
                        string sphase = "";
                        string sProductCode = "";
                        int iCount = 0;


                        if (dsTime.Tables[0].Rows.Count > 0)
                        {

                            foreach (DataRow dr3 in dsTime.Tables[0].Rows)
                            {
                                string ph1Start = "";
                                string ph2Start = "";
                                string ph3Start = "";

                                string ph1End = "";
                                string ph2End = "";
                                string ph3End = "";

                                if (dr3["ProductCode"].ToString() + "-" + sphase != sProductCode)
                                {
                                    if (dr3["ProductCode"].ToString() == "JSB")
                                    {
                                        color = "#00FFFF";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "SSB")
                                    {
                                        color = "#CCFB5D";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "JVB")
                                    {
                                        color = "#99CCFF";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "IVB")
                                    {
                                        color = "#FF99CC";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "PS3")
                                    {
                                        color = "#FFFF00";
                                    }
                                    else
                                    {
                                        color = "#FFA500";
                                    }

                                    if (dr3["Ph1Start"].ToString().Length < 5)
                                    {
                                        ph1Start = "0" + dr3["Ph1Start"].ToString();
                                    }
                                    else
                                    {
                                        ph1Start = dr3["Ph1Start"].ToString();
                                    }

                                    if (dr3["Ph2Start"].ToString().Length < 5)
                                    {
                                        ph2Start = "0" + dr3["Ph2Start"].ToString();
                                    }
                                    else
                                    {
                                        ph2Start = dr3["Ph2Start"].ToString();
                                    }


                                    if (dr3["Ph3Start"].ToString().Length < 5)
                                    {
                                        ph3Start = "0" + dr3["Ph3Start"].ToString();
                                    }
                                    else
                                    {
                                        ph3Start = dr3["Ph3Start"].ToString();
                                    }


                                    if (dr3["ph1End"].ToString().Length < 5)
                                    {
                                        ph1End = "0" + dr3["ph1End"].ToString();
                                    }
                                    else
                                    {
                                        ph1End = dr3["ph1End"].ToString();
                                    }

                                    if (dr3["ph2End"].ToString().Length < 5)
                                    {
                                        ph2End = "0" + dr3["ph2End"].ToString();
                                    }
                                    else
                                    {
                                        ph2End = dr3["ph2End"].ToString();
                                    }


                                    if (dr3["ph3End"].ToString().Length < 5)
                                    {
                                        ph3End = "0" + dr3["ph3End"].ToString();
                                    }
                                    else
                                    {
                                        ph3End = dr3["ph3End"].ToString();
                                    }


                                    for (int i = iCount; i < columnCount; i++)
                                    {
                                        if (dr3["Phase"].ToString() == "2")
                                        {

                                            if (arrList[i].ToString().Substring(0, 5) == ph2Start)
                                            {

                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    if (arrList[j].ToString().Substring(0, 5) == ph2End)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - II - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }
                                        else if (dr3["Phase"].ToString() == "1")
                                        {

                                            if (arrList[i].ToString().Substring(0, 5) == ph1Start)
                                            {

                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    if (arrList[j].ToString().Substring(0, 5) == ph1End)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - I - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }
                                        else if (dr3["Phase"].ToString() == "3")
                                        {
                                            if (arrList[i].ToString().Substring(0, 5) == ph3Start)
                                            {

                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    if (arrList[j].ToString().Substring(0, 5) == ph3End)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - III - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }

                                    }
                                }

                                sphase = dr3["Phase"].ToString();
                                sProductCode = dr3["ProductCode"].ToString() + "-" + sphase;
                            }

                        }




                        tblhtml += "</tr>";



                    }

                    RoomNumber = dr["RoomNumber"].ToString();


                }
                tblhtml += "</table>";
            }
            ltrTable.Text = tblhtml;
            dvDay1ScheduleChart.Visible = true;
            dvDay1Schedule.Visible = true;
        }
    }


    public void generateDetailSchedule2()
    {
        string[] array = {

        "08:00 - 09:00",
        "09:15 - 10:15",
        "10:30 - 11:30",
        " 1/2 hr",
        "12:00 - 13:00",
        "13:15 - 14:15",
        "14:30 - 15:30",
        "15:30 - 16:30",
        "16:30 - 18:30"
    };

        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");

                }

            }
        }
        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "7";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);
        string tblhtml = "";
        string cmdText = "";
        DataSet ds = new DataSet();

        cmdText += "select distinct C.ProductId,C.ProductCode,C.ProductGroupCode,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,RC.Phase,RC.SeqNo,RC.RoomNumber,RC.BldgID ";
        cmdText += ",RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery,CS.Duration from Contest C ";
        cmdText += "inner join RoomSchedule RC on ";
        cmdText += "(C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )";
        cmdText += "inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber=RL.RoomNumber)";
        cmdText += "inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)";
        cmdText += "where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + maxDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + " order by RC.RoomNumber,RC.Phase, ProductGroupCode,RC.SeqNo ASC";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

        tblhtml += "<table border=1 style='border-collapse:collapse;' width='1250px' cellspacing='0' cellpadding='0'>";
        tblhtml += "<tr><td style='font-weight:bold;text-align:center; border:1px solid black; font-size:10pt;background-color:#CD7F32;color:#fdd017;height:10px' colspan='52'> <h1> Day 2 Sunday</h1></td></tr>";
        tblhtml += "<tr style='font-weight:bold;text-align:center;vertical-align:middle;'>";
        tblhtml += "<td style='font-weight:bold; width:100px;'>Bldg</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Room</td>";
        tblhtml += "<td style='font-weight:bold; width:30px;'>Cap</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Registration<br>6:30</td>";
        TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
        ArrayList arrList = new ArrayList();
        string color = "";
        int columnCount = 0;

        while (true)
        {
            tblhtml += "<td style='width:10px; height:40px; font-weight:bold; border-right: 1px solid black; color:#000000; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";
            //tblHtml += "<td style='width:10px; height:40px;><div class='rotate' style='-moz-transform:rotate(90deg); width:10px; height:30px;'>" +  + "</div></td>";

            arrList.Add(timeInterval);
            timeInterval = timeInterval + TimeSpan.FromMinutes(15);
            columnCount++;
            if (timeInterval.Hours == 19)
            {
                break; // TODO: might not be correct. Was : Exit While
            }

        }


        tblhtml += "</tr>";
        string roomNo = "";
        int p1RowCount = 0;
        string productGroup = "";
        string phase = "";
        string seqNo = "";
        string ProductGroupCode = string.Empty;
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                btnExport.Visible = true;
                string RoomNumber = string.Empty;
                string Rm = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    if (RoomNumber != dr["RoomNumber"].ToString())
                    {

                        DataSet dsTime = new DataSet();
                        string CmdTimeText = "select  C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ProductId,C.ProductCode,C.ProductGroupCode,RC.Phase,RC.SeqNo,RC.RoomNumber from Contest C inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + dr["RoomNumber"].ToString().Trim() + "')inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + maxDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=1  order by RC.Phase, convert(time,C.Ph1Start,100),convert(time,C.Ph2Start,100) ASC";

                        dsTime = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdTimeText);


                        tblhtml += "<tr style='font-weight:bold;'>";
                        tblhtml += "<td>" + dr["BldgName"].ToString() + " (" + dr["BldgId"].ToString() + ")</td>";
                        tblhtml += "<td>" + dr["BldgId"].ToString() + " " + dr["RoomNumber"].ToString() + "</td>";
                        tblhtml += "<td>" + dr["Capacity"].ToString() + "</td>";
                        tblhtml += "<td>" + dr["Gallery"].ToString() + "</td>";

                        string time = "";
                        string sphase = "";
                        string sProductCode = "";
                        int iCount = 0;


                        if (dsTime.Tables[0].Rows.Count > 0)
                        {

                            foreach (DataRow dr3 in dsTime.Tables[0].Rows)
                            {
                                string ph1Start = "";
                                string ph2Start = "";
                                string ph3Start = "";

                                string ph1End = "";
                                string ph2End = "";
                                string ph3End = "";

                                if (dr3["ProductCode"].ToString() + "-" + sphase != sProductCode)
                                {
                                    if (dr3["ProductCode"].ToString() == "BB")
                                    {
                                        color = "#FFA500";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "JGB")
                                    {
                                        color = "#99CCFF";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "SGB")
                                    {
                                        color = "#CCFB5D";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "JSC" || dr3["ProductCode"].ToString() == "ISC" || dr3["ProductCode"].ToString() == "SSC")
                                    {
                                        color = "#FF99CC";
                                    }
                                    else if (dr3["ProductCode"].ToString() == "MB3" || dr3["ProductCode"].ToString() == "MB2" || dr3["ProductCode"].ToString() == "MB1")
                                    {
                                        color = "#00FFFF";
                                    }
                                    else
                                    {
                                        color = "#FFA500";
                                    }




                                    if (dr3["Ph1Start"].ToString().Length < 5)
                                    {
                                        ph1Start = "0" + dr3["Ph1Start"].ToString();
                                    }
                                    else
                                    {
                                        ph1Start = dr3["Ph1Start"].ToString();
                                    }

                                    if (dr3["Ph2Start"].ToString().Length < 5)
                                    {
                                        ph2Start = "0" + dr3["Ph2Start"].ToString();
                                    }
                                    else
                                    {
                                        ph2Start = dr3["Ph2Start"].ToString();
                                    }


                                    if (dr3["Ph3Start"].ToString().Length < 5)
                                    {
                                        ph3Start = "0" + dr3["Ph3Start"].ToString();
                                    }
                                    else
                                    {
                                        ph3Start = dr3["Ph3Start"].ToString();
                                    }


                                    if (dr3["ph1End"].ToString().Length < 5)
                                    {
                                        ph1End = "0" + dr3["ph1End"].ToString();
                                    }
                                    else
                                    {
                                        ph1End = dr3["ph1End"].ToString();
                                    }

                                    if (dr3["ph2End"].ToString().Length < 5)
                                    {
                                        ph2End = "0" + dr3["ph2End"].ToString();
                                    }
                                    else
                                    {
                                        ph2End = dr3["ph2End"].ToString();
                                    }


                                    if (dr3["ph3End"].ToString().Length < 5)
                                    {
                                        ph3End = "0" + dr3["ph3End"].ToString();
                                    }
                                    else
                                    {
                                        ph3End = dr3["ph3End"].ToString();
                                    }


                                    for (int i = iCount; i < columnCount; i++)
                                    {
                                        if (dr3["Phase"].ToString() == "2")
                                        {
                                            string p2Start = ph2Start.Replace(":", "");
                                            string endStart = arrList[i].ToString().Substring(0, 5).Replace(":", "");

                                            int ipStart = Convert.ToInt32(p2Start.ToString());
                                            int iIndStart = Convert.ToInt32(endStart.ToString());

                                            if (arrList[i].ToString().Substring(0, 5) == ph2Start || iIndStart >= ipStart)
                                            {
                                                if (iIndStart > ipStart)
                                                {
                                                    break;
                                                }
                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    string p2End = ph2End.Replace(":", "");
                                                    string endInt = arrList[j].ToString().Substring(0, 5).Replace(":", "");

                                                    int ipEnd = Convert.ToInt32(p2End.ToString());
                                                    int iIndex = Convert.ToInt32(endInt.ToString());

                                                    if (arrList[j].ToString().Substring(0, 5) == ph2End || iIndex >= ipEnd)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + dr3["ProductCode"].ToString() + " - II - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }
                                        else if (dr3["Phase"].ToString() == "1")
                                        {
                                            string p1Start = ph1Start.Replace(":", "");
                                            string endStart = arrList[i].ToString().Substring(0, 5).Replace(":", "");

                                            int ipStart = Convert.ToInt32(p1Start.ToString());
                                            int iIndStart = Convert.ToInt32(endStart.ToString());

                                            if (arrList[i].ToString().Substring(0, 5) == ph1Start || iIndStart >= ipStart)
                                            {
                                                if (iIndStart > ipStart)
                                                {
                                                    break;
                                                }
                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    string p1End = ph1End.Replace(":", "");
                                                    string endInt = arrList[j].ToString().Substring(0, 5).Replace(":", "");

                                                    int ipEnd = Convert.ToInt32(p1End.ToString());
                                                    int iIndex = Convert.ToInt32(endInt.ToString());
                                                    if (arrList[j].ToString().Substring(0, 5) == ph1End || iIndex >= ipEnd)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + dr3["ProductCode"].ToString() + " - I - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }
                                        else if (dr3["Phase"].ToString() == "3")
                                        {
                                            string p3Start = ph3Start.Replace(":", "");
                                            string endStart = arrList[i].ToString().Substring(0, 5).Replace(":", "");

                                            int ipStart = Convert.ToInt32(p3Start.ToString());
                                            int iIndStart = Convert.ToInt32(endStart.ToString());

                                            if (arrList[i].ToString().Substring(0, 5) == ph3Start || iIndStart >= ipStart)
                                            {
                                                if (iIndStart > ipStart)
                                                {
                                                    break;
                                                }
                                                int colcnt = 0;
                                                for (int j = i; j < columnCount; j++)
                                                {
                                                    colcnt++;
                                                    string p3End = ph3End.Replace(":", "");
                                                    string endInt = arrList[j].ToString().Substring(0, 5).Replace(":", "");

                                                    int ipEnd = Convert.ToInt32(p3End.ToString());
                                                    int iIndex = Convert.ToInt32(endInt.ToString());
                                                    if (arrList[j].ToString().Substring(0, 5) == ph3End || iIndex >= ipEnd)
                                                    {
                                                        break;
                                                    }

                                                    //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                    //{
                                                    //    break;
                                                    //}

                                                }
                                                i = i + colcnt;
                                                iCount = i + colcnt;
                                                iCount = iCount - 1;
                                                i = i - 1;
                                                iCount = i;
                                                tblhtml += "<td colspan='" + colcnt + "' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + dr3["ProductCode"].ToString() + " - III - " + dr3["SeqNo"].ToString() + "</td>";
                                                break;
                                            }
                                            else
                                            {
                                                tblhtml += "<td>&nbsp;</td>";
                                            }
                                        }

                                    }
                                }

                                sphase = dr3["Phase"].ToString();
                                sProductCode = dr3["ProductCode"].ToString() + "-" + sphase;
                            }

                        }

                        tblhtml += "</tr>";

                    }

                    RoomNumber = dr["RoomNumber"].ToString();


                }
                tblhtml += "</table>";
            }
            ltrTableDay2.Text = tblhtml;
            dvDay2ScheduleChart.Visible = true;
            dvDay2Schedule.Visible = true;
        }
    }

    public void detailScheduleChart()
    {
        string[] array = {

        "08:00 - 09:00",
        "09:15 - 10:15",
        "10:30 - 11:30",
        " 1/2 hr",
        "12:00 - 13:00",
        "13:15 - 14:15",
        "14:30 - 15:30",
        "15:30 - 16:30",
        "16:30 - 18:30"
    };

        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");

                }

            }
        }
        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "8";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);
        string tblhtml = "";
        string cmdText = "";
        DataSet ds = new DataSet();

        cmdText = "select C.ProductId,C.ProductCode,C.ProductGroupCode,RC.RoomNumber from Contest C  inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' ) where C.Contest_Year=" + ddYear.SelectedValue + " and C.ContestDate='" + minDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "";

        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

        tblhtml += "<table border=1 style='border-collapse:collapse;' width='1250px' cellspacing='0' cellpadding='0'>";
        tblhtml += "<tr><td style='font-weight:bold;text-align:center; border:1px solid black; font-size:10pt;background-color:#CD7F32;color:#fdd017;height:10px' colspan='52'> <h1> Day 1 Saturday</h1></td></tr>";
        tblhtml += "<tr style='font-weight:bold;text-align:center;vertical-align:middle;'>";
        tblhtml += "<td style='font-weight:bold; width:100px;'>Bldg</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Room</td>";
        tblhtml += "<td style='font-weight:bold; width:30px;'>Cap</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Registration<br>6:30</td>";
        TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
        ArrayList arrList = new ArrayList();
        string color = "";
        int columnCount = 0;

        while (true)
        {
            tblhtml += "<td style='width:10px; height:40px; font-weight:bold; border-right: 1px solid black; color:#000000; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";
            //tblHtml += "<td style='width:10px; height:40px;><div class='rotate' style='-moz-transform:rotate(90deg); width:10px; height:30px;'>" +  + "</div></td>";

            arrList.Add(timeInterval);
            timeInterval = timeInterval + TimeSpan.FromMinutes(15);
            columnCount++;
            if (timeInterval.Hours == 19)
            {
                break; // TODO: might not be correct. Was : Exit While
            }

        }


        tblhtml += "</tr>";
        string roomNo = "";
        int p1RowCount = 0;
        string productGroup = "";
        string phase = "";
        string seqNo = "";
        string ProductGroupCode = string.Empty;
        ArrayList ArrRoomNumber = new ArrayList();
        int iRoomCount = 0;
        if (null != ds.Tables && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr2 in ds.Tables[0].Rows)
                {
                    if (iRoomCount == 0)
                    {
                        ArrRoomNumber.Add(dr2["RoomNumber"].ToString().Trim());
                    }
                    else
                    {
                        string RoomNumber = dr2["RoomNumber"].ToString().Trim();
                        string Sstat = "";
                        for (int r = 0; r < ArrRoomNumber.Count; r++)
                        {
                            if (ArrRoomNumber[r].ToString() == RoomNumber)
                            {
                                Sstat = "Y";
                            }
                        }

                        if (Sstat == "")
                        {
                            ArrRoomNumber.Add(dr2["RoomNumber"].ToString().Trim());
                        }
                    }
                    iRoomCount++;
                }

                foreach (string room in ArrRoomNumber)
                {
                    string SRoomNumber = room.ToString();

                    string RoomDetText = "";
                    RoomDetText = "select distinct RC.RoomNumber,RC.BldgID ,RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery from RoomSchedule RC inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + SRoomNumber + "') where RC.EventId=" + ddEvent.SelectedValue + " and RC.ChapterID=" + ddchapter.SelectedValue + "";
                    DataSet objRoomDs = new DataSet();
                    objRoomDs = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, RoomDetText);

                    tblhtml += "<tr style='font-weight:bold;'>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["BldgName"].ToString() + "(" + objRoomDs.Tables[0].Rows[0]["BldgID"].ToString() + ")</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["RoomNumber"].ToString() + "</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["Capacity"].ToString() + "</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["Gallery"].ToString() + "</td>";

                    DataSet dsTime = new DataSet();
                    string CmdTimeText = "select  C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ProductId,C.ProductCode,C.ProductGroupCode,RC.Phase,RC.SeqNo,RC.RoomNumber from Contest C inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + SRoomNumber + "')inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + minDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "  order by RC.Phase, convert(time,C.Ph2Start,100),convert(time,C.Ph2End,100) ASC";

                    dsTime = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdTimeText);

                    string time = "";
                    string sphase = "";
                    string sProductCode = "";
                    int iCount = 0;


                    if (dsTime.Tables[0].Rows.Count > 0)
                    {

                        foreach (DataRow dr3 in dsTime.Tables[0].Rows)
                        {
                            string ph1Start = "";
                            string ph2Start = "";
                            string ph3Start = "";

                            string ph1End = "";
                            string ph2End = "";
                            string ph3End = "";

                            if (dr3["ProductCode"].ToString() + "-" + dr3["Phase"].ToString() != sProductCode)
                            {
                                if (dr3["ProductCode"].ToString() == "JSB")
                                {
                                    color = "#00FFFF";
                                }
                                else if (dr3["ProductCode"].ToString() == "SSB")
                                {
                                    color = "#CCFB5D";
                                }
                                else if (dr3["ProductCode"].ToString() == "JVB")
                                {
                                    color = "#99CCFF";
                                }
                                else if (dr3["ProductCode"].ToString() == "IVB")
                                {
                                    color = "#FF99CC";
                                }
                                else if (dr3["ProductCode"].ToString() == "PS3")
                                {
                                    color = "#FFFF00";
                                }
                                else
                                {
                                    color = "#FFA500";
                                }

                                if (dr3["Ph1Start"].ToString().Length < 5)
                                {
                                    ph1Start = "0" + dr3["Ph1Start"].ToString();
                                }
                                else
                                {
                                    ph1Start = dr3["Ph1Start"].ToString();
                                }

                                if (dr3["Ph2Start"].ToString().Length < 5)
                                {
                                    ph2Start = "0" + dr3["Ph2Start"].ToString();
                                }
                                else
                                {
                                    ph2Start = dr3["Ph2Start"].ToString();
                                }


                                if (dr3["Ph3Start"].ToString().Length < 5)
                                {
                                    ph3Start = "0" + dr3["Ph3Start"].ToString();
                                }
                                else
                                {
                                    ph3Start = dr3["Ph3Start"].ToString();
                                }


                                if (dr3["ph1End"].ToString().Length < 5)
                                {
                                    ph1End = "0" + dr3["ph1End"].ToString();
                                }
                                else
                                {
                                    ph1End = dr3["ph1End"].ToString();
                                }

                                if (dr3["ph2End"].ToString().Length < 5)
                                {
                                    ph2End = "0" + dr3["ph2End"].ToString();
                                }
                                else
                                {
                                    ph2End = dr3["ph2End"].ToString();
                                }


                                if (dr3["ph3End"].ToString().Length < 5)
                                {
                                    ph3End = "0" + dr3["ph3End"].ToString();
                                }
                                else
                                {
                                    ph3End = dr3["ph3End"].ToString();
                                }


                                for (int i = iCount; i < columnCount; i++)
                                {
                                    if (dr3["Phase"].ToString() == "2")
                                    {

                                        if (arrList[i].ToString().Substring(0, 5) == ph2Start)
                                        {

                                            int colcnt = 0;
                                            for (int j = i; j < columnCount; j++)
                                            {
                                                colcnt++;
                                                if (arrList[j].ToString().Substring(0, 5) == ph2End)
                                                {
                                                    break;
                                                }

                                                //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                //{
                                                //    break;
                                                //}

                                            }
                                            i = i + colcnt;
                                            iCount = i + colcnt;
                                            iCount = iCount - 1;
                                            i = i - 1;
                                            iCount = i;
                                            tblhtml += "<td colspan='" + (colcnt - 1) + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - II - " + dr3["SeqNo"].ToString() + "</td>";
                                            break;
                                        }
                                        else
                                        {
                                            tblhtml += "<td>&nbsp;</td>";
                                        }
                                    }
                                    else if (dr3["Phase"].ToString() == "1")
                                    {

                                        if (arrList[i].ToString().Substring(0, 5) == ph1Start)
                                        {

                                            int colcnt = 0;
                                            for (int j = i; j < columnCount; j++)
                                            {
                                                colcnt++;
                                                if (arrList[j].ToString().Substring(0, 5) == ph1End)
                                                {
                                                    break;
                                                }

                                                //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                //{
                                                //    break;
                                                //}

                                            }
                                            i = i + colcnt;
                                            iCount = i + colcnt;
                                            iCount = iCount - 1;
                                            i = i - 1;
                                            iCount = i;
                                            tblhtml += "<td colspan='" + (colcnt - 1) + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - I - " + dr3["SeqNo"].ToString() + "</td>";
                                            break;
                                        }
                                        else
                                        {
                                            tblhtml += "<td>&nbsp;</td>";
                                        }
                                    }
                                    else if (dr3["Phase"].ToString() == "3")
                                    {
                                        if (arrList[i].ToString().Substring(0, 5) == ph3Start)
                                        {

                                            int colcnt = 0;
                                            for (int j = i; j < columnCount; j++)
                                            {
                                                colcnt++;
                                                if (arrList[j].ToString().Substring(0, 5) == ph3End)
                                                {
                                                    break;
                                                }

                                                //if (arrList[j].ToString() == dr["ph1End"].ToString() )
                                                //{
                                                //    break;
                                                //}

                                            }
                                            i = i + colcnt;
                                            iCount = i + colcnt;
                                            iCount = iCount - 1;
                                            i = i - 1;
                                            iCount = i;
                                            tblhtml += "<td colspan='" + (colcnt - 1) + "' style='background-color:" + color + "; text-align:center; font-weight:bold;'>" + dr3["ProductCode"].ToString() + " - III - " + dr3["SeqNo"].ToString() + "</td>";
                                            break;
                                        }
                                        else
                                        {
                                            tblhtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                }
                            }

                            sphase = dr3["Phase"].ToString();
                            sProductCode = dr3["ProductCode"].ToString() + "-" + sphase;
                        }

                    }


                    tblhtml += "</tr>";

                }

            }


        }


        tblhtml += "</table>";

        ltrTable.Text = tblhtml;
        dvDay1ScheduleChart.Visible = true;
        dvDay1Schedule.Visible = true;
    }


    public void detailScheduleChart2v3()
    {
        string[] array = {

        "08:00 - 09:00",
        "09:15 - 10:15",
        "10:30 - 11:30",
        " 1/2 hr",
        "12:00 - 13:00",
        "13:15 - 14:15",
        "14:30 - 15:30",
        "15:30 - 16:30",
        "16:30 - 18:30"
    };

        DataSet dsDate = new DataSet();
        string minDate = "";
        string maxDate = "";
        string headertext = "";
        string cmdDateText = "select Min(ContestDate) as MinDate,Max(ContestDate) as MaxDate from Contest where Contest_Year='" + ddYear.SelectedValue + "' and EventID=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + "";
        dsDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDateText);
        if (dsDate.Tables[0] != null)
        {
            if (dsDate.Tables[0].Rows.Count > 0)
            {
                if (dsDate.Tables[0].Rows[0]["MinDate"] != null && dsDate.Tables[0].Rows[0]["MinDate"].ToString() != "")
                {
                    minDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MinDate"].ToString()).ToString("yyyy-MM-dd");
                }
                if (dsDate.Tables[0].Rows[0]["MaxDate"] != null && dsDate.Tables[0].Rows[0]["MaxDate"].ToString() != "")
                {
                    maxDate = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("yyyy-MM-dd");

                }

            }
        }
        DataSet objDs1 = new DataSet();
        string cmdTimeText = "";
        string minTime = "8";
        cmdTimeText = "select MIN(Convert(time,Ph1Start,108)) as MinTime from Contest where EventId=" + ddEvent.SelectedValue + " and NSFChapterID=" + ddchapter.SelectedValue + " and Contest_Year='" + ddYear.SelectedValue + "' and ContestDate='" + minDate + "'";
        objDs1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdTimeText);
        if (objDs1.Tables[0] != null)
        {
            if (objDs1.Tables[0].Rows.Count > 0)
            {
                if (objDs1.Tables[0].Rows[0]["MinTime"] != null && objDs1.Tables[0].Rows[0]["MinTime"].ToString() != "")
                {
                    minTime = objDs1.Tables[0].Rows[0]["MinTime"].ToString();
                    minTime = minTime[1].ToString();
                }
            }
        }
        if (minTime == "0")
        {
            minTime = "6";
        }
        int iminTime = Convert.ToInt32(minTime);
        string tblhtml = "";
        string cmdText = "";
        DataSet ds = new DataSet();

        cmdText = "select C.ProductId,C.ProductCode,C.ProductGroupCode,RC.RoomNumber,RC.Phase from Contest C  inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' ) where C.Contest_Year=" + ddYear.SelectedValue + " and C.ContestDate='" + maxDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=" + ddchapter.SelectedValue + "";

        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

        tblhtml += "<table border=1 style='border-collapse:collapse;' width='1250px' cellspacing='0' cellpadding='0'>";
        tblhtml += "<tr><td style='font-weight:bold;text-align:center; border:1px solid black; font-size:10pt;background-color:#CD7F32;color:#fdd017;height:10px' colspan='52'> <h1> Day 2 Sunday</h1></td></tr>";
        tblhtml += "<tr style='font-weight:bold;text-align:center;vertical-align:middle;'>";
        tblhtml += "<td style='font-weight:bold; width:100px;'>Bldg</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Room</td>";
        tblhtml += "<td style='font-weight:bold; width:30px;'>Cap</td>";
        tblhtml += "<td style='font-weight:bold; width:50px;'>Registration<br>6:30</td>";
        TimeSpan timeInterval = new TimeSpan(iminTime, 0, 0);
        ArrayList arrList = new ArrayList();
        string color = "";
        int columnCount = 0;

        while (true)
        {
            tblhtml += "<td style='width:10px; height:40px; font-weight:bold; border-right: 1px solid black; color:#000000; background-color:#ffffff;'> <div class='rotate' style='-moz-transform:rotate(270deg); -ms-transform:rotate(270deg); -webkit-transform:rotate(270deg); progid:DXImageTransform.Microsoft.BasicImage(rotation=1); transform: rotate(270deg); width:10px; height:11px; margin-top:24px;'>" + timeInterval.ToString().Substring(0, 5) + "</div></td>";

            arrList.Add(timeInterval);
            timeInterval = timeInterval + TimeSpan.FromMinutes(15);
            columnCount++;
            if (timeInterval.Hours == 19)
            {
                break; // TODO: might not be correct. Was : Exit While
            }

        }


        tblhtml += "</tr>";
        string roomNo = "";
        int p1RowCount = 0;
        string productGroup = "";
        string phase = "";
        string seqNo = "";
        string ProductGroupCode = string.Empty;
        ArrayList ArrRoomNumber = new ArrayList();
        int iRoomCount = 0;
        if (null != ds.Tables && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr2 in ds.Tables[0].Rows)
                {
                    if (iRoomCount == 0)
                    {
                        ArrRoomNumber.Add(dr2["RoomNumber"].ToString().Trim());
                    }
                    else
                    {
                        string RoomNumber = dr2["RoomNumber"].ToString().Trim();
                        string Sstat = "";
                        for (int r = 0; r < ArrRoomNumber.Count; r++)
                        {
                            if (ArrRoomNumber[r].ToString() == RoomNumber)
                            {
                                Sstat = "Y";
                            }
                        }

                        if (Sstat == "")
                        {
                            ArrRoomNumber.Add(dr2["RoomNumber"].ToString().Trim());
                        }
                    }
                    iRoomCount++;
                }

                foreach (string room in ArrRoomNumber)
                {
                    string SRoomNumber = room.ToString();
                    ArrayList ArrPhaseTime = new ArrayList();
                    ArrayList ArrPhase = new ArrayList();
                    ArrayList ArrSeq = new ArrayList();
                    ArrayList ArrProductCode = new ArrayList();

                    string RoomDetText = "";
                    RoomDetText = "select distinct RC.RoomNumber,RC.BldgID ,RL.BldgName,RL.Capacity,case RL.Gallery When 'Y' Then 'Flippers' when 'N' Then 'Tables' End Gallery from RoomSchedule RC inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + SRoomNumber + "') where RC.EventId=" + ddEvent.SelectedValue + " and RC.ChapterID=" + ddchapter.SelectedValue + "";
                    DataSet objRoomDs = new DataSet();
                    objRoomDs = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, RoomDetText);

                    tblhtml += "<tr style='font-weight:bold;'>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["BldgName"].ToString() + "(" + objRoomDs.Tables[0].Rows[0]["BldgID"].ToString() + ")</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["RoomNumber"].ToString() + "</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["Capacity"].ToString() + "</td>";
                    tblhtml += "<td>" + objRoomDs.Tables[0].Rows[0]["Gallery"].ToString() + "</td>";

                    DataSet dsTime = new DataSet();
                    string CmdTimeText = "select  distinct convert(time,C.Ph1Start,100),C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ProductId,C.ProductCode,C.ProductGroupCode,RC.Phase,RC.SeqNo,RC.RoomNumber from Contest C inner join RoomSchedule RC on (C.ProductGroupId=RC.ProductGroupID and C.ProductId=RC.ProductID and C.EventId=RC.EventID and C.NSFChapterID=RC.ChapterID and RC.ContestYear='" + ddYear.SelectedValue + "' )inner join RoomList RL on (RC.EventId=RL.EventID and RC.ChapterID=RL.ChapterID and RL.ContestYear='" + ddYear.SelectedValue + "' and RC.BldgID=RL.BldgID and RC.RoomNumber='" + SRoomNumber + "')inner join ContestSettings CS on (C.EventId=CS.EventID and C.Contest_Year=CS.ContestYear and C.ProductId=CS.ProductID and C.ProductGroupId=CS.ProductGroupID and RC.Phase=CS.Phase)where C.Contest_Year='" + ddYear.SelectedValue + "' and C.ContestDate='" + maxDate + "' and C.EventId=" + ddEvent.SelectedValue + " and C.NSFChapterID=1  order by convert(time,C.Ph1Start,100),RC.Phase ASC";

                    dsTime = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdTimeText);

                    string time = "";
                    string sphase = "";
                    string sProductCode = "";
                    int iCount = 0;
                    string statusFlag = "";
                    string sColor = "";

                    if (dsTime.Tables[0].Rows.Count > 0)
                    {
                        btnExport.Visible = true;
                        foreach (DataRow dr4 in dsTime.Tables[0].Rows)
                        {
                            int timeCount = 0;
                            string tStat = "";

                            foreach (DataRow dr3 in dsTime.Tables[0].Rows)
                            {

                                string ph1Start = "";
                                string ph2Start = "";
                                string ph3Start = "";

                                string ph1End = "";
                                string ph2End = "";
                                string ph3End = "";

                                string sPhaseTime = "";
                                string sPhase = dr3["Phase"].ToString();
                                if (sPhase == "1")
                                {
                                    sPhaseTime = dr3["Ph1Start"].ToString().Length < 5 ? "0" + dr3["Ph1Start"].ToString() : dr3["Ph1Start"].ToString();
                                }
                                else if (sPhase == "2")
                                {
                                    sPhaseTime = dr3["Ph2Start"].ToString().Length < 5 ? "0" + dr3["Ph2Start"].ToString() : dr3["Ph2Start"].ToString();
                                }
                                else if (sPhase == "3")
                                {
                                    sPhaseTime = dr3["Ph3Start"].ToString().Length < 5 ? "0" + dr3["Ph3Start"].ToString() : dr3["Ph3Start"].ToString();
                                }
                                //ph1Start = dr3["Ph1Start"].ToString().Length < 5 ? "0" + dr3["Ph1Start"].ToString() : dr3["Ph1Start"].ToString();
                                //ph2Start = dr3["Ph2Start"].ToString().Length < 5 ? "0" + dr3["Ph2Start"].ToString() : dr3["Ph2Start"].ToString();
                                //ph3Start = dr3["Ph3Start"].ToString().Length < 5 ? "0" + dr3["Ph3Start"].ToString() : dr3["Ph3Start"].ToString();


                                ArrPhaseTime.Add(sPhaseTime);


                            }
                            ArrPhaseTime.Sort();
                            ArrayList a = new ArrayList(ArrPhaseTime);



                            int icount = 0;
                            string priorPhaseEnd = "";
                            for (int i = 0; i < ArrPhaseTime.Count; i++)
                            {
                                string phaseEnd = "";
                                string sequence = "";
                                string ProductCode = "";

                                foreach (DataRow dr5 in dsTime.Tables[0].Rows)
                                {

                                    string ph1Start = "";
                                    string ph2Start = "";
                                    string ph3Start = "";

                                    string ph1End = "";
                                    string ph2End = "";
                                    string ph3End = "";

                                    ph1Start = dr5["Ph1Start"].ToString().Length < 5 ? "0" + dr5["Ph1Start"].ToString() : dr5["Ph1Start"].ToString();
                                    ph2Start = dr5["Ph2Start"].ToString().Length < 5 ? "0" + dr5["Ph2Start"].ToString() : dr5["Ph2Start"].ToString();
                                    ph3Start = dr5["Ph3Start"].ToString().Length < 5 ? "0" + dr5["Ph3Start"].ToString() : dr5["Ph3Start"].ToString();
                                    if (ArrPhaseTime[i].ToString() == ph1Start || ArrPhaseTime[i].ToString() == ph2Start || ArrPhaseTime[i].ToString() == ph3Start)
                                    {
                                        if (ArrPhaseTime[i].ToString() == ph1Start)
                                        {
                                            phaseEnd = dr5["Ph1End"].ToString().Length < 5 ? "0" + dr5["Ph1End"].ToString() : dr5["Ph1End"].ToString();
                                            sphase = "I";
                                            sequence = dr5["SeqNo"].ToString();
                                            ProductCode = dr5["ProductCode"].ToString();
                                            break;
                                        }
                                        else if (ArrPhaseTime[i].ToString() == ph2Start)
                                        {
                                            phaseEnd = dr5["Ph2End"].ToString().Length < 5 ? "0" + dr5["Ph2End"].ToString() : dr5["Ph2End"].ToString();
                                            sphase = "II";
                                            sequence = dr5["SeqNo"].ToString();
                                            ProductCode = dr5["ProductCode"].ToString();
                                            break;
                                        }
                                        else if (ArrPhaseTime[i].ToString() == ph3Start)
                                        {
                                            phaseEnd = dr5["Ph3End"].ToString().Length < 5 ? "0" + dr5["Ph3End"].ToString() : dr5["Ph3End"].ToString();
                                            sphase = "III";
                                            sequence = dr5["SeqNo"].ToString();
                                            ProductCode = dr5["ProductCode"].ToString();
                                            break;
                                        }
                                    }
                                }
                                int icnt = 0;
                                for (int j = icount; j < arrList.Count; j++)
                                {
                                    int p = 0;
                                    if (ArrPhaseTime.Count == i)
                                    {
                                        p = -1;
                                    }
                                    else if ((ArrPhaseTime.Count) - 1 == i)
                                    {
                                        p = 0;
                                    }
                                    else
                                    {
                                        p = 1;
                                    }
                                    if (ArrPhaseTime[i + p].ToString() == phaseEnd)
                                    {
                                        string pEndV = "";

                                        foreach (DataRow dr5 in dsTime.Tables[0].Rows)
                                        {

                                            string ph1Start = "";
                                            string ph2Start = "";
                                            string ph3Start = "";

                                            string ph1End = "";
                                            string ph2End = "";
                                            string ph3End = "";

                                            ph1Start = dr5["Ph1Start"].ToString().Length < 5 ? "0" + dr5["Ph1Start"].ToString() : dr5["Ph1Start"].ToString();
                                            ph2Start = dr5["Ph2Start"].ToString().Length < 5 ? "0" + dr5["Ph2Start"].ToString() : dr5["Ph2Start"].ToString();
                                            ph3Start = dr5["Ph3Start"].ToString().Length < 5 ? "0" + dr5["Ph3Start"].ToString() : dr5["Ph3Start"].ToString();
                                            if (ArrPhaseTime[i + p].ToString() == ph1Start || ArrPhaseTime[i + p].ToString() == ph2Start || ArrPhaseTime[i + p].ToString() == ph3Start)
                                            {
                                                if (ArrPhaseTime[i + p].ToString() == ph1Start)
                                                {
                                                    pEndV = dr5["Ph1End"].ToString().Length < 5 ? "0" + dr5["Ph1End"].ToString() : dr5["Ph1End"].ToString();
                                                    sphase = "I";
                                                    sequence = dr5["SeqNo"].ToString();
                                                    ProductCode = dr5["ProductCode"].ToString();
                                                    break;
                                                }
                                                else if (ArrPhaseTime[i + p].ToString() == ph2Start)
                                                {
                                                    pEndV = dr5["Ph2End"].ToString().Length < 5 ? "0" + dr5["Ph2End"].ToString() : dr5["Ph2End"].ToString();
                                                    sphase = "II";
                                                    sequence = dr5["SeqNo"].ToString();
                                                    ProductCode = dr5["ProductCode"].ToString();
                                                    break;
                                                }
                                                else if (ArrPhaseTime[i + p].ToString() == ph3Start)
                                                {
                                                    pEndV = dr5["Ph3End"].ToString().Length < 5 ? "0" + dr5["Ph3End"].ToString() : dr5["Ph3End"].ToString();
                                                    sphase = "III";
                                                    sequence = dr5["SeqNo"].ToString();
                                                    ProductCode = dr5["ProductCode"].ToString();
                                                    break;
                                                }
                                            }
                                        }

                                        if (ProductCode == "BB")
                                        {
                                            color = "#FFA500";
                                        }
                                        else if (ProductCode == "JGB")
                                        {
                                            color = "#99CCFF";
                                        }
                                        else if (ProductCode == "SGB")
                                        {
                                            color = "#CCFB5D";
                                        }
                                        else if (ProductCode == "JSC" || ProductCode == "ISC" || ProductCode == "SSC")
                                        {
                                            color = "#FF99CC";
                                        }
                                        else if (ProductCode == "MB3" || ProductCode == "MB2" || ProductCode == "MB1")
                                        {
                                            color = "#00FFFF";
                                        }
                                        else
                                        {
                                            color = "#FFA500";
                                        }

                                        if (arrList[j].ToString().Substring(0, 5) == ArrPhaseTime[i].ToString())
                                        {
                                            for (int k = j; k < arrList.Count; k++)
                                            {
                                                icnt++;
                                                string pEnd = pEndV.Replace(":", "");
                                                priorPhaseEnd = phaseEnd;
                                                string endInt = arrList[k].ToString().Substring(0, 5).Replace(":", "");
                                                int ipEnd = 0;
                                                int iIndex = 0;
                                                ipEnd = Convert.ToInt32(pEnd.ToString());
                                                iIndex = Convert.ToInt32(endInt.ToString());


                                                if (SRoomNumber == "EL1008")
                                                {
                                                    pEndV = "13:20";
                                                    ipEnd = 1320;
                                                    if (arrList[k].ToString().Substring(0, 5) == pEndV || iIndex >= ipEnd)
                                                    {
                                                        i = i + 1;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (arrList[k].ToString().Substring(0, 5) == pEndV || iIndex >= ipEnd)
                                                    {
                                                        i = i + 1;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (SRoomNumber == "EL1008")
                                            {
                                                int colspan = icnt / 2;
                                                tblhtml += "<td colspan='6' style='background-color:#FFA500; text-align:center; font-weight:bold; font-size:11px;'>BB - II - 1</td>";
                                                tblhtml += "<td colspan='4' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + ProductCode + " - I & II - " + sequence + "</td>";
                                            }
                                            else
                                            {
                                                tblhtml += "<td colspan='" + (icnt - 1) + "' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + ProductCode + " - I & II - " + sequence + "</td>";
                                            }
                                            j = j + icnt;
                                            j = j - 1;
                                            icount = j;

                                            break;
                                        }
                                        else
                                        {
                                            tblhtml += "<td>&nbsp;</td>";
                                        }
                                    }
                                    else
                                    {
                                        if (ProductCode == "BB")
                                        {
                                            color = "#FFA500";
                                        }
                                        else if (ProductCode == "JGB")
                                        {
                                            color = "#99CCFF";
                                        }
                                        else if (ProductCode == "SGB")
                                        {
                                            color = "#CCFB5D";
                                        }
                                        else if (ProductCode == "JSC" || ProductCode == "ISC" || ProductCode == "SSC")
                                        {
                                            color = "#FF99CC";
                                        }
                                        else if (ProductCode == "MB3" || ProductCode == "MB2" || ProductCode == "MB1")
                                        {
                                            color = "#00FFFF";
                                        }
                                        else
                                        {
                                            color = "#FFA500";
                                        }
                                        string p1End = phaseEnd.Replace(":", "");
                                        priorPhaseEnd = phaseEnd;
                                        string endInt1 = arrList[j].ToString().Substring(0, 5).Replace(":", "");
                                        int ipEnd1 = 0;
                                        int iIndex1 = 0;
                                        ipEnd1 = Convert.ToInt32(p1End.ToString());
                                        iIndex1 = Convert.ToInt32(endInt1.ToString());

                                        if (arrList[j].ToString().Substring(0, 5) == ArrPhaseTime[i].ToString() || iIndex1 >= ipEnd1)
                                        {
                                            if (iIndex1 > ipEnd1)
                                            {
                                                break;
                                            }
                                            for (int k = j; k < arrList.Count; k++)
                                            {
                                                icnt++;
                                                string pEnd = phaseEnd.Replace(":", "");
                                                priorPhaseEnd = phaseEnd;
                                                string endInt = arrList[k].ToString().Substring(0, 5).Replace(":", "");
                                                int ipEnd = 0;
                                                int iIndex = 0;
                                                ipEnd = Convert.ToInt32(pEnd.ToString());
                                                iIndex = Convert.ToInt32(endInt.ToString());



                                                if (arrList[k].ToString().Substring(0, 5) == phaseEnd || iIndex >= ipEnd)
                                                {

                                                    break;
                                                }
                                            }
                                            tblhtml += "<td colspan='" + (icnt - 1) + "' style='background-color:" + color + "; text-align:center; font-weight:bold; font-size:11px;'>" + ProductCode + " - " + sphase + " - " + sequence + "</td>";
                                            j = j + icnt;
                                            j = j - 1;
                                            icount = j;

                                            break;
                                        }
                                        else
                                        {
                                            tblhtml += "<td>&nbsp;</td>";
                                        }
                                    }
                                }
                            }
                            ArrPhaseTime.Clear();
                            tblhtml += "</tr>";
                            break;
                        }
                    }
                }
            }

            tblhtml += "</table>";

            ltrTableDay2.Text = tblhtml;
            dvDay2ScheduleChart.Visible = true;
            dvDay2Schedule.Visible = true;
        }

    }
}