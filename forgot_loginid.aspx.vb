Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net
Imports System.Net.Mail
Imports System.IO
Imports System.Diagnostics
Imports NorthSouth.BAL
Partial Class NEW_forgot_loginid
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click2(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim blnCheck As Boolean = False
        Dim Email As String = txtEmail.Text
        Dim FirstName As String = txtFirstName.Text.ToUpper().Trim()
        Dim LastName As String = txtLastName.Text.ToUpper().Trim()
        Dim City As String = txtCity.Text
        Dim Zip As String = txtZip.Text
        Dim state As String = ddlState.Text
        Dim HomePhone As String = txtHomePhone.Text
        Dim SpouseFirstName As String = txtSpouseFirstName.Text
        Dim SpouseLastName As String = txtSpouseLastName.Text
        Dim strSql As String
        strSql = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,LastName,EMail from IndSpouse WHERE DeletedFlag <> 'Yes'"

        If (Len(Trim(FirstName)) > 0) Then
            strSql = strSql & " and UPPER(FIRSTNAME) like '%" & FirstName & "%'"
            blnCheck = True
        End If

        If (Len(Trim(LastName)) > 0) Then
            strSql = strSql & " and UPPER(LASTNAME) like '%" & LastName & "%'"
            blnCheck = True
        End If
        'If (Len(Trim(City)) > 0) Then
        '    strSql = strSql & " and CITY like '%" & City & "%'"
        '    blnCheck = True
        'End If

        'If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
        '    strSql = strSql & " and STATE like '%" & state & "%'"
        '    blnCheck = True
        'End If
        'If (Len(Trim(Zip)) > 0) Then
        '    strSql = strSql & " and ZIP like '%" & Zip & "%'"
        '    blnCheck = True
        'End If

        'If (Len(Trim(HomePhone)) > 0) Then
        '    strSql = strSql & " and HPHONE like '%" & HomePhone & "%'"
        '    blnCheck = True
        'End If
        Dim dsForgotLogin As DataSet
        Dim strEmail1 As String
        Dim strEmail2 As String
        Dim StrIndSpouse As String
        Dim objIndSpouse As New IndSpouse10
        Dim objSpouse As IndSpouse10
        Dim intIndID As Double
        Dim dsIndSpouse As New DataSet
        Dim StrSpouse As String = ""
        Dim intSpouseID As Integer = 0
        Dim dsSpouse As New DataSet
        Dim sBody As String
        Dim i As Integer
        Dim k As Integer
        Dim bIsMatchingEmailFound As Boolean
        Dim strAutoMemberIDs As String
        strAutoMemberIDs = String.Empty
        Dim iCount As Integer
        dsForgotLogin = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        If dsForgotLogin.Tables(0).Rows.Count > 0 Then
            If dsForgotLogin.Tables(0).Rows.Count = 1 Then
                strEmail1 = dsForgotLogin.Tables(0).Rows(0).Item("Email").ToString()
                StrIndSpouse = "AutoMemberID=" & dsForgotLogin.Tables(0).Rows(0).Item("AutoMemberID").ToString()
                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)
                If dsIndSpouse.Tables.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            End If
                        Else
                            If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            Else
                                intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                            End If
                        End If
                    End If
                    If intIndID > 0 Then
                        Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
                        objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), intIndID)
                        If objIndSpouseExt.Id > 0 Then
                            objIndSpouse = objIndSpouseExt
                            strEmail1 = objIndSpouse.Email
                        End If
                        StrSpouse = "Relationship='" & intIndID & "'"
                        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                        If dsSpouse.Tables.Count > 0 Then
                            If dsSpouse.Tables(0).Rows.Count > 0 Then
                                intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                            End If
                        End If
                        If intSpouseID > 0 Then
                            objSpouse = New IndSpouse10
                            objSpouse.GetIndSpouseByID(Application("ConnectionString"), intSpouseID)
                            strEmail2 = objSpouse.Email
                        End If
                    End If
                End If
                If Len(strEmail1) > 0 Or Len(strEmail2) > 0 Then
                    If Len(strEmail1) > 0 Then
                        strSql = "SELECT * FROM LOGIN_MASTER WHERE Upper(User_Email)='" & strEmail1.ToUpper().Trim() & "'"
                        Dim drLoginEmail1 As SqlDataReader
                        drLoginEmail1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                        If drLoginEmail1.Read() Then
                            sBody = "Thank you for your query. Here are your login id and password." & vbCrLf
                            sBody = sBody & "Login ID(Email): " & drLoginEmail1("User_Email") & vbCrLf
                            sBody = sBody & "Password: " & drLoginEmail1("User_Pwd")
                            SendEmail("NSF Login ID", sBody, strEmail1)
                            bIsMatchingEmailFound = True
                            lblMessage.Visible = True
                            lblMessage.Text = "Your login ID and Password were sent to your Email(s) in our records."
                        End If
                    End If
                    If Len(strEmail2) > 0 Then
                        strSql = "SELECT * FROM LOGIN_MASTER WHERE Upper(User_Email)='" & strEmail2.ToUpper().Trim() & "'"
                        Dim drLoginEmail2 As SqlDataReader
                        drLoginEmail2 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                        If drLoginEmail2.Read() Then
                            sBody = "Thank you for your query. Here are your login id and password." & vbCrLf
                            sBody = sBody & "Login ID(Email): " & drLoginEmail2("User_Email") & vbCrLf
                            sBody = sBody & "Password: " & drLoginEmail2("User_Pwd")
                            SendEmail("NSF Login ID", sBody, strEmail2)
                            bIsMatchingEmailFound = True
                            lblMessage.Visible = True
                            lblMessage.Text = "Your login ID and Password were sent to your Email(s) in our records."
                        End If
                    End If
                    If bIsMatchingEmailFound = False Then
                        sBody = " Matching Automember(ID): " & intIndID & vbCrLf
                        sBody = sBody & vbCrLf
                        sBody = sBody & "Data Entered By User" & vbCrLf
                        sBody = sBody & "First Name: " & txtFirstName.Text & vbCrLf
                        sBody = sBody & "Last Name: " & txtLastName.Text & vbCrLf
                        sBody = sBody & "City: " & txtCity.Text & vbCrLf
                        sBody = sBody & "State: " & ddlState.SelectedItem.Text & vbCrLf
                        sBody = sBody & "Zip: " & txtZip.Text & vbCrLf
                        sBody = sBody & "Spouse First Name: " & txtSpouseFirstName.Text & vbCrLf
                        sBody = sBody & "Spouse Last Name: " & txtSpouseLastName.Text & vbCrLf
                        sBody = sBody & "Home Phone: " & txtHomePhone.Text & vbCrLf
                        sBody = sBody & "Email to Contact: " & txtEmail.Text & vbCrLf
                        SendEmail("Forgot Login ID / No matching email was found in Login_Master ", sBody, "nsfcontests@gmail.com")
                        lblMessage.Visible = True
                        lblMessage.Text = "We are not able to identify your record.  A message is being sent to Customer Service for further investigation.  You will get a response within 48 hours."
                    End If
                Else
                    sBody = "Matching AutoMemberID: " & intIndID & vbCrLf
                    sBody = sBody & vbCrLf
                    sBody = sBody & "Data Entered By User" & vbCrLf
                    sBody = sBody & "First Name: " & txtFirstName.Text & vbCrLf
                    sBody = sBody & "Last Name: " & txtLastName.Text & vbCrLf
                    sBody = sBody & "City: " & txtCity.Text & vbCrLf
                    sBody = sBody & "State: " & ddlState.SelectedItem.Text & vbCrLf
                    sBody = sBody & "Zip: " & txtZip.Text & vbCrLf
                    sBody = sBody & "Spouse First Name: " & txtSpouseFirstName.Text & vbCrLf
                    sBody = sBody & "Spouse Last Name: " & txtSpouseLastName.Text & vbCrLf
                    sBody = sBody & "Home Phone: " & txtHomePhone.Text & vbCrLf
                    sBody = sBody & "Email to Contact: " & txtEmail.Text & vbCrLf
                    SendEmail("Forgot Login ID / No Email Found in IndSpouse", sBody, "nsfcontests@gmail.com")
                    lblMessage.Visible = True
                    lblMessage.Text = "We are not able to identify your record.  A message is being sent to Customer Service for further investigation.  You will get a response within 48 hours."
                End If
            Else
                'Multiple Records Matching
                iCount = dsForgotLogin.Tables(0).Rows.Count
                If (iCount > 1) Then
                    If iCount > 5 Then
                        iCount = 5
                    End If
                    For i = 0 To iCount - 1
                        strEmail1 = dsForgotLogin.Tables(0).Rows(i).Item("Email").ToString()
                        StrIndSpouse = "AutoMemberID=" & dsForgotLogin.Tables(0).Rows(i).Item("AutoMemberID").ToString()
                        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)
                        If dsIndSpouse.Tables.Count > 0 Then
                            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(i).Item("AutoMemberID")
                                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(i).Item("AutoMemberID")
                                    End If
                                Else
                                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    Else
                                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                                    End If
                                End If
                            End If
                            If intIndID > 0 Then
                                Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
                                objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), intIndID)
                                If objIndSpouseExt.Id > 0 Then
                                    objIndSpouse = objIndSpouseExt
                                    strEmail1 = objIndSpouse.Email
                                End If
                                StrSpouse = "Relationship='" & intIndID & "'"
                                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                                If dsSpouse.Tables.Count > 0 Then
                                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    End If
                                End If
                                If intSpouseID > 0 Then
                                    objSpouse = New IndSpouse10
                                    objSpouse.GetIndSpouseByID(Application("ConnectionString"), intSpouseID)
                                    strEmail2 = objSpouse.Email
                                End If
                            End If
                        End If
                        If Len(strAutoMemberIDs) > 0 Then
                            strAutoMemberIDs = strAutoMemberIDs & vbCrLf & " Auto MemberID" & i + 1 & ": " & intIndID
                        Else
                            strAutoMemberIDs = "Auto MemberID" & i + 1 & ": " & intIndID
                        End If
                    Next
                    sBody = "Matching Auto MemberIDs" & vbCrLf
                    sBody = sBody & strAutoMemberIDs & vbCrLf
                    sBody = sBody & "Data Entered By User" & vbCrLf
                    sBody = sBody & "First Name: " & txtFirstName.Text & vbCrLf
                    sBody = sBody & "Last Name: " & txtLastName.Text & vbCrLf
                    sBody = sBody & "City: " & txtCity.Text & vbCrLf
                    sBody = sBody & "State: " & ddlState.SelectedItem.Text & vbCrLf
                    sBody = sBody & "Zip: " & txtZip.Text & vbCrLf
                    sBody = sBody & "Spouse First Name: " & txtSpouseFirstName.Text & vbCrLf
                    sBody = sBody & "Spouse Last Name: " & txtSpouseLastName.Text & vbCrLf
                    sBody = sBody & "Home Phone: " & txtHomePhone.Text & vbCrLf
                    sBody = sBody & "Email to Contact: " & txtEmail.Text & vbCrLf
                    SendEmail("Forgot Login ID/ No Unique Match in IndSpouse table", sBody, "nsfcontests@gmail.com")
                    lblMessage.Visible = True
                    lblMessage.Text = "We are not able to identify your record.  A message is being sent to Customer Service for further investigation.  You will get a response within 48 hours."
                End If
            End If
        Else
            sBody = "Data Entered By User" & vbCrLf
            sBody = sBody & "First Name: " & txtFirstName.Text & vbCrLf
            sBody = sBody & "Last Name: " & txtLastName.Text & vbCrLf
            sBody = sBody & "City: " & txtCity.Text & vbCrLf
            sBody = sBody & "State: " & ddlState.SelectedItem.Text & vbCrLf
            sBody = sBody & "Zip: " & txtZip.Text & vbCrLf
            sBody = sBody & "Spouse First Name: " & txtSpouseFirstName.Text & vbCrLf
            sBody = sBody & "Spouse Last Name: " & txtSpouseLastName.Text & vbCrLf
            sBody = sBody & "Home Phone: " & txtHomePhone.Text & vbCrLf
            sBody = sBody & "Email to Contact: " & txtEmail.Text & vbCrLf
            SendEmail("Forgot Login ID/ Record not Found in IndSpouse", sBody, "nsfcontests@gmail.com")
            lblMessage.Visible = True
            lblMessage.Text = "We are not able to identify your record.  A message is being sent to Customer Service for further investigation.  You will get a response within 48 hours."
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim SecureUrl As String
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost") Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
            LoadStates()
        End If
    End Sub
    Private Sub LoadStates()
        Dim strSqlQuery As String
        strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME"
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drStates.Read()
            ddlstate.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlstate.Items.Insert(0, New ListItem("Select State", ""))
        ddlstate.SelectedIndex = 0
    End Sub
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@gmail.com")
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody

        Dim client As New SmtpClient()
        Dim ok As Boolean = True

        Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

        client.Host = host
        client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
        Try
            client.Send(email)
        Catch e As Exception
            ok = False
        End Try
    End Sub
End Class
