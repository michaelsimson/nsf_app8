﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Drawing;

public partial class SupportTicketContacts : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {

                fillEvent();
                fillSupportContactGrid();
            }
        }
        lblErrMsg.Text = "";
        lblMsg.Text = "";
    }
    string ConnectionString = "ConnectionString";

    public void fillEvent()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        cmdText = "select EventId, Name from Event";
        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            ddEvent.DataSource = ds;
            ddEvent.DataTextField = "Name";
            ddEvent.DataValueField = "EventId";
            ddEvent.DataBind();
            ddEvent.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }

    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            //  cmdText = "select ProductGroupId,Name from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + year + ")";
            cmdText = "select ProductGroupID,Name from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "Name";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            //year = 2014;
            // cmdText = "select ProductId,Name from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + year + ")";
            if (ddlProductGroup.SelectedValue != "0")
            {
                cmdText = "select ProductID,Name from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else
            {
                cmdText = "select ProductID,Name from Product where EventId=" + ddEvent.SelectedValue + "";
            }
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "ProductId";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProductGroup.SelectedValue == "0")
        {
            ddlProduct.Items.Clear();
        }
        else
        {
            fillProduct();
        }
    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue == "0")
        {
            ddlProduct.Items.Clear();
            ddlProductGroup.Items.Clear();
        }
        else
        {
            fillProductGroup();
            fillProduct();
        }
    }
    protected void ddlDonorType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlDonorType.SelectedValue == "Organization")
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtOrgName.Text = String.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true;
        }
        else
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
        }
    }
    protected void btnSearch_onClick(object sender, EventArgs e)
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        state = ddlState.SelectedItem.Value.ToString();
        email = txtEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = "";
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");
        }

        if (lastName.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
                //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append("  I.lastName like '%" + lastName + "%'");
                // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");
            else
                strSql.Append("  I.Email like '%" + email + "%'");
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
                strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");

        }
        if (firstName.Length > 0 || orgname.Length > 0 || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
                SearchMembers(strSql.ToString());
            else
                SearchOrganization(strSqlOrg.ToString());
        }
        else
        {

            lblMsg.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";

            return;
        }
    }
    protected void btnIndClose_onclick(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;

        Panel4.Visible = false;
        //if ((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
        //{
        //    panel3.Visible = true;
        //}
    }
    private void SearchMembers(string sqlSt)
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
            DataTable dt = ds.Tables[0];
            int Count = dt.Rows.Count;
            DataView dv = new DataView(dt);
            GridMemberDt.DataSource = dt;
            GridMemberDt.DataBind();
            if (Count > 0)
            {
                Panel4.Visible = true;
                lblIndSearch.Text = "";
                lblIndSearch.Visible = false;
            }
            else
            {
                lblIndSearch.Text = "No match found";
                lblIndSearch.Visible = true;
                Panel4.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }
    private void SearchOrganization(string sqlSt)
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
            DataTable dt = ds.Tables[0];
            int Count = dt.Rows.Count;
            DataView dv = new DataView(dt);
            GridMemberDt.DataSource = dt;
            GridMemberDt.DataBind();
            if (Count > 0)
            {
                Panel4.Visible = true;
                lblIndSearch.Text = "";
                lblIndSearch.Visible = false;
            }
            else
            {
                lblIndSearch.Text = "No match found";
                lblIndSearch.Visible = true;
                Panel4.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnVolunteerFind_onClick(object sender, EventArgs e)
    {
        hdnMemberFlag.Value = "Primary";
        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = false;

        Panel4.Visible = false;
    }
    protected void btnVendorFind_onClick(object sender, EventArgs e)
    {

        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = true;

        Panel4.Visible = false;
    }

    private void GetStates()
    {
        DataSet dsStates;
        try
        {
            dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
            ddlState.DataSource = dsStates.Tables[0];
            ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
            ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem("Select State", String.Empty));
            ddlState.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
        }
    }
    protected void GridMemberDt_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int index = int.Parse(e.CommandArgument.ToString());
        GridViewRow row = GridMemberDt.Rows[index];
        int IncurMemberID = (int)GridMemberDt.DataKeys[index].Value;

        if (hdnMemberFlag.Value == "Primary")
        {
            hdnAutoMemberID.Value = IncurMemberID.ToString();

            txtVolunteerInCharge.Text = (GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtVolunteerInCharge.Text = (txtVolunteerInCharge.Text == "&nbsp;" ? " " : txtVolunteerInCharge.Text);
        }
        else if (hdnMemberFlag.Value == "Backup1")
        {
            hdnBackupMemberID1.Value = IncurMemberID.ToString();
            txtBMemberID.Text = (GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtBMemberID.Text = (txtBMemberID.Text == "&nbsp;" ? " " : txtBMemberID.Text);
        }
        else if (hdnMemberFlag.Value == "Backup2")
        {
            hdnBackupMemberID2.Value = IncurMemberID.ToString();
            txtCMemberID.Text = (GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtCMemberID.Text = (txtCMemberID.Text == "&nbsp;" ? " " : txtCMemberID.Text);
        }
        pIndSearch.Visible = false;
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        tblContactGroup.Visible = true;
        txtVolunteerInCharge.Text = "";
        btnSaveConatct.Text = "Add";
        txtBMemberID.Text = "";
        txtCMemberID.Text = "";
        hdnAutoMemberID.Value = "";
        hdnBackupMemberID1.Value = "";
        hdnBackupMemberID2.Value = "";
    }
    public void fillSupportContactGrid()
    {
        dvSupportContactList.Visible = true;
        string cmdText = "";
        DataSet ds = new DataSet();

        cmdText = "select ST.SupTicketContID, IP.AutoMemberID,ST.MemberID,ST.BMemberID,ST.CMemberID,IP.FirstName, IP.LastName,IP.FirstName +' '+ IP.LastName as Pname,IP1.FirstName +' '+ IP1.LastName as BackUpName1,IP2.FirstName +' '+ IP2.LastName as BackUpName2,E.Name as Event,E.EventID,PG.ProductGroupID,PG.Name as ProductGroup,P.ProductID,P.Name as Product, IP.Email,IP.HPhone,IP.CPhone,IP.WPhone, IP1.Email as BMail, IP1.CPhone as BPhone, IP2.Email as CMail, IP2.CPhone as BCPhone from      SupportTicketContact ST inner join IndSpouse IP on IP.AutoMemberID=ST.MemberID left join IndSpouse IP1 on IP1.AutoMemberID=ST.BMemberID left join IndSpouse IP2 on IP2.AutoMemberID=ST.CMemberID inner join Event E on (E.EventID=ST.EventID) left join ProductGroup PG on (ST.ProductGroupID=PG.ProductGroupID) left join Product P on (ST.ProductID=P.ProductID) order by St.EventID ASC";


        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdSupportContactList.DataSource = ds;
                    grdSupportContactList.DataBind();
                }
                else
                {
                    grdSupportContactList.DataSource = ds;
                    grdSupportContactList.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validateContactList() == 1)
        {
            fillSupportContactGrid();
        }
        else
        {
            validateContactList();
        }
    }
    public void savesupportContact()
    {
        try
        {

            string cmdText = string.Empty;
            int MemberID = Convert.ToInt32(hdnAutoMemberID.Value);
            int EventId = Convert.ToInt32(ddEvent.SelectedValue);
            string Event = ddEvent.SelectedItem.Text;
            string ProdcutGroupId = ddlProductGroup.SelectedValue;
            string ProductGroup = string.Empty;
            string ProductId = ddlProduct.SelectedValue;
            string Product = string.Empty;
            string userID = Session["LoginId"].ToString();
            string BackupMemberID1 = hdnBackupMemberID1.Value;
            string BackupMemberID2 = hdnBackupMemberID2.Value;
            if (ProdcutGroupId == "" || ProdcutGroupId == "0")
            {
                ProdcutGroupId = "NULL";
                ProductGroup = "NULL";
            }
            else
            {
                ProdcutGroupId = "'" + ddlProductGroup.SelectedValue + "'";
                ProductGroup = "'" + ddlProductGroup.SelectedItem.Text + "'";
            }

            if (ProductId == "" || ProductId == "0")
            {
                ProductId = "NULL";
                Product = "NULL";
            }
            else
            {
                ProductId = "'" + ddlProduct.SelectedValue + "'";
                Product = "'" + ddlProduct.SelectedItem.Text + "'";
            }

            if (btnSaveConatct.Text == "Add")
            {
                int dupCheck = 0;
                string PMemberID = string.Empty;
                string BMemberID = string.Empty;
                string CMemberID = string.Empty;
                DataSet ds = new DataSet();
                cmdText = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + EventId;
                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue;
                }
                else
                {
                    cmdText += " and ProductGroupID is NULL";
                }
                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and ProductID=" + ddlProduct.SelectedValue + "";
                }
                else
                {
                    cmdText += " and ProductID is null";
                }
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["MemberID"] != null && dr["MemberID"].ToString() != "")
                            {
                                PMemberID = dr["MemberID"].ToString();
                            }
                            if (dr["BMemberID"] != null && dr["BMemberID"].ToString() != "")
                            {
                                BMemberID = dr["BMemberID"].ToString();
                            }
                            if (dr["CMemberID"] != null && dr["CMemberID"].ToString() != "")
                            {
                                CMemberID = dr["CMemberID"].ToString();
                            }
                        }


                    }
                }

                if (PMemberID == "")
                {
                    cmdText = "select count(*) as CountSet from supportTicketContact where MemberId=" + MemberID + " and EventID=" + EventId;
                    if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                    {
                        cmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue;
                    }
                    if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                    {
                        cmdText += " and ProductID=" + ddlProduct.SelectedValue + "";
                    }
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            dupCheck = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                        }
                    }


                    if (dupCheck == 0)
                    {

                        if (txtBMemberID.Text != "")
                        {

                            BMemberID = BackupMemberID1.ToString();
                        }
                        else
                        {
                            BMemberID = "NULL";
                        }
                        if (txtCMemberID.Text != "")
                        {
                            CMemberID = BackupMemberID2.ToString();

                        }
                        else
                        {
                            CMemberID = "NULL";
                        }

                        cmdText = "insert into supportTicketContact (MemberID,BMemberID,CMemberID,Event,EventID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,CreatedDate,CreatedBy) values(" + MemberID + "," + BMemberID + "," + CMemberID + ",'" + Event + "'," + EventId + "," + ProdcutGroupId + "," + ProductGroup + "," + ProductId + "," + Product + ",GetDate()," + userID + ")";
                        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        lblMsg.Text = "Contact saved successfully";
                        tblContactGroup.Visible = false;
                        pIndSearch.Visible = false;
                        lblMsg.ForeColor = System.Drawing.Color.Blue;
                    }
                    else
                    {
                        lblMsg.Text = "Duplicate exists.";
                        lblMsg.ForeColor = System.Drawing.Color.Red;


                    }

                }
                else
                {
                    lblMsg.Text = "Duplicate exists.";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                string PMemberID = string.Empty;
                string BMemberID = string.Empty;
                string CMemberID = string.Empty;
                if (txtVolunteerInCharge.Text != "")
                {
                    PMemberID = MemberID.ToString();

                }
                if (txtBMemberID.Text != "")
                {

                    BMemberID = BackupMemberID1.ToString();
                }
                else
                {
                    BMemberID = "NULL";
                }
                if (txtCMemberID.Text != "")
                {
                    CMemberID = BackupMemberID2.ToString();

                }
                else
                {
                    CMemberID = "NULL";
                }
                cmdText = "Update supportTicketContact set MemberID=" + MemberID + ",BMemberID=" + BMemberID + ",CMemberID=" + CMemberID + ",Event='" + Event + "',EventID=" + EventId + ",ProductGroupID=" + ProdcutGroupId + ",ProductGroupCode=" + ProductGroup + ",ProductID=" + ProductId + ",ProductCode=" + Product + ", ModifyDate=GetDate(), ModifiedBy=" + userID + " where SupTicketContID=" + hdnSupportContactID.Value + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                lblMsg.Text = "Contact updated successfully";
                tblContactGroup.Visible = false;
                pIndSearch.Visible = false;
                lblMsg.ForeColor = System.Drawing.Color.Blue;
            }
            fillSupportContactGrid();
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnSaveConatct_Click(object sender, EventArgs e)
    {
        if (validateContact() == 1)
        {
            savesupportContact();
        }
        else
        {
            validateContact();
        }
    }
    protected void btnCancelContact_Click(object sender, EventArgs e)
    {
        tblContactGroup.Visible = false;
        pIndSearch.Visible = false;
    }

    public int validateContact()
    {
        int RetVal = 1;
        if (ddEvent.SelectedValue == "0")
        {
            RetVal = -1;
            lblErrMsg.Text = "Please select Event";
        }

        else if (txtVolunteerInCharge.Text == "")
        {
            RetVal = -1;
            lblErrMsg.Text = "Please fill Volunteer Incharge";
        }
        else if (txtVolunteerInCharge.Text == txtBMemberID.Text)
        {
            RetVal = -1;
            lblErrMsg.Text = "Primary Member and Backup 1 Member should not be same";
        }
        else if (txtVolunteerInCharge.Text == txtCMemberID.Text)
        {
            RetVal = -1;
            lblErrMsg.Text = "Primary Member and Backup 2 Member should not be same";
        }
        else if (txtBMemberID.Text != "" && txtCMemberID.Text != "")
        {
            if (txtBMemberID.Text == txtCMemberID.Text)
            {
                RetVal = -1;
                lblErrMsg.Text = "Backup 1 Member and Backup 2 Member should not be same";
            }
        }
        return RetVal;
    }
    public int validateContactList()
    {
        int RetVal = 1;
        if (ddEvent.SelectedValue == "0")
        {
            RetVal = -1;
            lblErrMsg.Text = "Please select Event";
        }

        return RetVal;
    }
    public void grdSupportContactList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                hdnRowIndex.Value = selIndex.ToString();
                grdSupportContactList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string eventID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                string ProductGroupID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                string ProductID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                string AutoMemberID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string SupportContactID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblSupportContID") as Label).Text;

                string BMemberID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblBMemberID") as Label).Text;
                string CMemberID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string BMemberName = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblBackUpName1") as Label).Text;
                string CMemberName = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblBackUpName2") as Label).Text;

                hdnBackupMemberID1.Value = BMemberID;
                hdnBackupMemberID2.Value = CMemberID;

                hdnSupportContactID.Value = SupportContactID;
                hdnAutoMemberID.Value = AutoMemberID;
                ddEvent.SelectedValue = eventID;
                if (ddlProductGroup.Items.Count > 0 && ProductGroupID != "")
                {
                    ddlProductGroup.SelectedValue = ProductGroupID;
                }
                else
                {
                    if (ProductGroupID != "")
                    {
                        fillProductGroup();
                        ddlProductGroup.SelectedValue = ProductGroupID;
                    }
                    else
                    {
                        fillProductGroup();
                    }
                }
                if (ddlProduct.Items.Count > 0 && ProductID != "")
                {
                    fillProduct();
                    ddlProduct.SelectedValue = ProductID;
                }
                else
                {
                    if (ProductID != "")
                    {
                        fillProduct();
                        ddlProduct.SelectedValue = ProductID;
                    }
                    else
                    {
                        fillProduct();
                    }
                }

                txtVolunteerInCharge.Text = (grdSupportContactList.Rows[selIndex].Cells[2].Text + " " + grdSupportContactList.Rows[selIndex].Cells[3].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
                txtVolunteerInCharge.Text = (txtVolunteerInCharge.Text == "&nbsp;" ? " " : txtVolunteerInCharge.Text);
                txtBMemberID.Text = BMemberName;
                txtCMemberID.Text = CMemberName;

                if (txtVolunteerInCharge.Text == "")
                {
                    btnDeletePMember.Visible = false;
                }
                else
                {
                    btnDeletePMember.Visible = true;
                }
                if (BMemberName == "")
                {
                    BtnDeleteBmemberID.Visible = false;
                }
                else
                {
                    BtnDeleteBmemberID.Visible = true;
                }
                if (CMemberName == "")
                {
                    btnDeleteCMemberID.Visible = false;
                }
                else
                {
                    btnDeleteCMemberID.Visible = true;
                }

                tblContactGroup.Visible = true;
                btnSaveConatct.Text = "Update";
            }
            else if (e.CommandName == "BMember")
            {
                spnMemberTitle.InnerText = "Table 2: BMember Information";
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;

                string BMemberID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblBMemberID") as Label).Text;
                string eventID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                string ProductGroupID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                string ProductID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductID") as Label).Text;

                string cmdText = "";
                DataSet ds = new DataSet();

                cmdText = "select ST.SupTicketContID, IP.AutoMemberID,ST.BMemberID As MemberID,IP.FirstName, IP.LastName,E.Name as Event,E.EventID,PG.ProductGroupID,PG.Name as ProductGroup,P.ProductID,P.Name as Product, IP.Email,IP.HPhone,IP.CPhone,IP.WPhone from      SupportTicketContact ST inner join IndSpouse IP on IP.AutoMemberID=ST.BMemberID inner join Event E on (E.EventID=ST.EventID) left join ProductGroup PG on (ST.ProductGroupID=PG.ProductGroupID) left join Product P on (ST.ProductID=P.ProductID) where IP.AutoMemberID=" + BMemberID + "  order by St.EventID ASC";
                try
                {
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grdMemberInfo.DataSource = ds;
                            grdMemberInfo.DataBind();
                        }
                        else
                        {
                            grdMemberInfo.DataSource = ds;
                            grdMemberInfo.DataBind();
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
            }
            else if (e.CommandName == "CMember")
            {
                spnMemberTitle.InnerText = "Table 2: CMember Information";
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string CMemberID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string eventID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                string ProductGroupID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                string ProductID = ((Label)grdSupportContactList.Rows[selIndex].FindControl("lblProductID") as Label).Text;

                string cmdText = "";
                DataSet ds = new DataSet();

                cmdText = "select ST.SupTicketContID, IP.AutoMemberID,ST.CMemberID As MemberID,IP.FirstName, IP.LastName,E.Name as Event,E.EventID,PG.ProductGroupID,PG.Name as ProductGroup,P.ProductID,P.Name as Product, IP.Email,IP.HPhone,IP.CPhone,IP.WPhone from      SupportTicketContact ST inner join IndSpouse IP on IP.AutoMemberID=ST.CMemberID inner join Event E on (E.EventID=ST.EventID) left join ProductGroup PG on (ST.ProductGroupID=PG.ProductGroupID) left join Product P on (ST.ProductID=P.ProductID) where IP.AutoMemberID=" + CMemberID + "  order by St.EventID ASC";
                try
                {
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            grdMemberInfo.DataSource = ds;
                            grdMemberInfo.DataBind();
                        }
                        else
                        {
                            grdMemberInfo.DataSource = ds;
                            grdMemberInfo.DataBind();
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
            }

        }
        catch (Exception ex)
        {
        }

    }

    protected void btnBMemberID_onClick(object sender, EventArgs e)
    {
        hdnMemberFlag.Value = "Backup1";
        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = false;

        Panel4.Visible = false;
    }
    protected void btnCMemberID_onClick(object sender, EventArgs e)
    {
        hdnMemberFlag.Value = "Backup2";
        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = false;

        Panel4.Visible = false;
    }
    protected void btnDeletePMember_onClick(object sender, EventArgs e)
    {
        hdnMemberFlag.Value = "Primary";
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "getConfirmfromUser('Primary Contact',1);", true);
    }
    protected void BtnDeleteBmemberID_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "getConfirmfromUser('Backup 1 Contact',2);", true);

    }
    protected void btnDeleteCMemberID_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "getConfirmfromUser('Backup 2 Contact',3);", true);

    }
    protected void btnDeleteP_Click(object sender, EventArgs e)
    {
        try
        {

            string cmdtext = string.Empty;
            cmdtext = "update SupportTicketContact set MemberID=null where SupTicketContID=" + hdnSupportContactID.Value + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            lblMsg.Text = "Primary contact deleted successfully";
            lblMsg.ForeColor = Color.Blue;
            txtVolunteerInCharge.Text = "";
            btnDeletePMember.Visible = false;
            fillSupportContactGrid();
            int index = Convert.ToInt32(hdnRowIndex.Value);
            grdSupportContactList.Rows[index].BackColor = Color.FromName("#EAEAEA");
        }
        catch
        {
        }
    }
    protected void btnDeleteB_Click(object sender, EventArgs e)
    {
        try
        {

            string cmdtext = string.Empty;
            cmdtext = "update SupportTicketContact set BmemberID=null where SupTicketContID=" + hdnSupportContactID.Value + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            lblMsg.Text = "Backup 1 contact deleted successfully";
            lblMsg.ForeColor = Color.Blue;
            txtBMemberID.Text = "";
            BtnDeleteBmemberID.Visible = false;
            fillSupportContactGrid();
            int index = Convert.ToInt32(hdnRowIndex.Value);
            grdSupportContactList.Rows[index].BackColor = Color.FromName("#EAEAEA");
        }
        catch { }
    }
    protected void btnDeleteC_Click(object sender, EventArgs e)
    {
        try
        {

            string cmdtext = string.Empty;
            cmdtext = "update SupportTicketContact set CmemberID=null where SupTicketContID=" + hdnSupportContactID.Value + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            lblMsg.Text = "Backup 2 contact deleted successfully";
            lblMsg.ForeColor = Color.Blue;
            txtCMemberID.Text = "";
            btnDeleteCMemberID.Visible = false;
            fillSupportContactGrid();
            int index = Convert.ToInt32(hdnRowIndex.Value);
            grdSupportContactList.Rows[index].BackColor = Color.FromName("#EAEAEA");
        }
        catch
        {
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strSql = "select productgroupid from product where productid=" + ddlProduct.SelectedValue + "";
        int PgId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[ConnectionString].ToString(), CommandType.Text, strSql));
        ddlProductGroup.SelectedValue = Convert.ToString(PgId);
    }
}