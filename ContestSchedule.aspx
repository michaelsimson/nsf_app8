﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="ContestSchedule.aspx.cs" Inherits="ContestSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <style type="text/css">
            .tdClass {
                border: 2px solid black;
            }
        </style>

        <%--<script src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">

            function loadStaffSchedule() {

                alert("");
                var year = $("#<%=ddYear.ClientID%>").value;
                var eventId = $("#<%=ddEvent.ClientID%>").value;
                alert(eventId);
                var level = document.getElementById("<%=ddlLevel.ClientID%>").value;

                if (level == "4") {
                    alert(level);
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:49160/App8Full/ContestSchedule.aspx/ListStaffSchedule",
                        data: JSON.stringify({ ojSchedule: { EventId: eventId, Year: year } }),
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        success: function (json) {
                            alert(JSON.stringify(json));
                        },
                        error: function (e) {
                            alert(JSON.stringify(e));
                            return false;
                        }
                    });
                    alert("");
                    return false;
                } else {
                    return true;
                }
            }
        </script>--%>
    </div>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  

     <table id="tblContestSchedule" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
         <tr>
             <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                 <center>
                     <h2>Contest Schedule</h2>
                 </center>
             </td>
         </tr>
         <tr>
             <td>
                 <table style="margin-left: auto; margin-right: auto;">
                     <tr>
                         <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                         <td align="left" nowrap="nowrap">
                             <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                                 AutoPostBack="True">
                                 <asp:ListItem Value="0">Select year</asp:ListItem>
                                 <asp:ListItem Value="-1">All</asp:ListItem>
                                 <asp:ListItem Value="2014">2014</asp:ListItem>
                                 <asp:ListItem Value="2015">2015</asp:ListItem>
                             </asp:DropDownList>

                         </td>

                         <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                         <td style="width: 100px" align="left">
                             <asp:DropDownList ID="ddEvent" Enabled="false" runat="server" Width="130px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                             </asp:DropDownList>
                         </td>
                         <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                         <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                             <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" Enabled="false" DataValueField="ChapterID" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                             </asp:DropDownList>
                         </td>
                         <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Level </b></td>
                         <td class="ContentSubTitle" valign="top" align="center" colspan="2">

                             <asp:DropDownList ID="ddlLevel" runat="server" Width="150px"
                                 AutoPostBack="True">
                                 <asp:ListItem Value="0">Select</asp:ListItem>
                                 <asp:ListItem Value="1">High Level Schedule</asp:ListItem>
                                 <asp:ListItem Value="2">Detailed Schedule</asp:ListItem>
                                 <asp:ListItem Value="3">Schedule For Posting</asp:ListItem>
                                 <asp:ListItem Value="4">Tech Staff Schedule</asp:ListItem>
                             </asp:DropDownList>

                         </td>
                         <%--  <td>Day</td>
                         <td>
                             <asp:DropDownList ID="ddlDay" runat="server" Width="90px">
                                 <asp:ListItem Value="-1">Select Day</asp:ListItem>
                                 <asp:ListItem Value="1">Saturday</asp:ListItem>
                                 <asp:ListItem Value="2">Sunday</asp:ListItem>
                             </asp:DropDownList></td>--%>
                         <td>
                             <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return loadStaffSchedule();" />
                             <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Visible="false" OnClick="btnExport_Click" />
                         </td>
                     </tr>
                 </table>
             </td>
         </tr>
         <tr>
             <td>

                 <center>
                     <asp:Label ID="lblMsg" runat="server"></asp:Label>


                 </center>


                 <div>

                     <div style="float: right;">
                     </div>
                     <br />
                     <div id="dvHighLevelChart" runat="server" style="margin-top: 10px;">
                         <div style="width: 1200px; overflow-x: scroll; margin-bottom: 10px;" id="dvDay1ScheduleChart" runat="server" visible="false">
                             <div id="dvDay1Schedule" runat="server" visible="false" style="font-weight: bold;">Day 1 Schedule</div>
                             <div style="clear: both;"></div>
                             <asp:Literal ID="ltrTable" runat="server"></asp:Literal>
                         </div>
                         <div style="clear: both;"></div>
                         <div style="width: 1200px; overflow-x: scroll;" id="dvDay2ScheduleChart" runat="server" visible="false">
                             <div id="dvDay2Schedule" runat="server" visible="false" style="font-weight: bold;">Day 2 Schedule</div>
                             <div style="clear: both;"></div>
                             <asp:Literal ID="ltrTableDay2" runat="server"></asp:Literal>
                         </div>
                     </div>
                 </div>
             </td>
         </tr>
         <tr>
             <td>
                 <div id="dvTechStaffScheduleReport" runat="server">
                     <div style="width: 1200px;" id="dvTechStaffSchedule" runat="server" visible="false">
                         <div id="dvDay1StaffSchedule" align="center" runat="server" visible="false" style="font-weight: bold; margin-bottom: 10px;">Day 1 Schedule</div>
                         <div style="clear: both;"></div>
                         <asp:Literal ID="ltrTechStaffSchedule" runat="server"></asp:Literal>
                     </div>
                     <div style="clear: both;"></div>
                     <div style="width: 1200px; margin-top: 10px;" id="dvTechStaffScheduleDay2" runat="server" visible="false">
                         <div align="center" id="dvDay2StaffSchedule" runat="server" visible="false" style="font-weight: bold; margin-bottom: 10px;">Day 2 Schedule</div>
                         <div style="clear: both;"></div>
                         <asp:Literal ID="ltrTechStaffScheduleDay2" runat="server"></asp:Literal>
                     </div>
                 </div>
             </td>

         </tr>
         <tr>
             <td>
                 <div id="dvSchedulePostingReport" runat="server">
                     <div style="width: 1200px; margin-bottom: 20px;" id="dvSchedulePosting" runat="server" visible="false">
                         <div align="center" id="dvScheduleTextDay1" runat="server" style="font-weight: bold; margin-bottom: 10px; font-size: 16px;">Day 1 Saturday</div>
                         <div style="clear: both;"></div>
                         <asp:Literal ID="ltrSchedulePosting" runat="server"></asp:Literal>
                     </div>
                     <div style="clear: both;"></div>
                     <div style="width: 1200px;" id="dvScheduleReportingDay2" runat="server" visible="false">
                         <div align="center" id="dvScheduleTextDay2" runat="server" style="font-weight: bold; margin-bottom: 10px; font-size: 16px;">Day 2 Sunday</div>
                         <asp:Literal ID="ltrSchedulePostingDay2" runat="server"></asp:Literal>
                     </div>
                 </div>
             </td>
         </tr>

         
       
     </table>














</asp:Content>
