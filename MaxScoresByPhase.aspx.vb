﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class MaxScoresByPhase
    Inherits System.Web.UI.Page

    Private eventid As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("~/Maintest.aspx")
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Response.Redirect("~/Maintest.aspx")
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Response.Redirect("~/Maintest.aspx")
        End If

        Dim roleid As Integer = Session("RoleID")
        'User code to initialize the page here
        'only Roleid = 1, 2, 3, 4, 5 Can access this page
        If Not Page.IsPostBack Then
            Session("Productid") = ""
            If Session("LoginChapterID") = "1" Then
                eventid = 1
            Else
                eventid = 2
            End If
            Session("EventId") = eventid
            ' Load Year into ListBox
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlYear.Items.Insert(2, Convert.ToString(year - 1))
            ddlYear.Items.Insert(3, Convert.ToString(year - 2))
            ddlYear.Items.Insert(4, Convert.ToString(year - 3))
            ddlYear.Items.Insert(5, Convert.ToString(year - 4))
            ddlYear.Items(1).Selected = True
            Session("Year") = ddlYear.Items(1).Text
            LoadProductGroup()   'Fill Product group names in dropdownlist
            GetRecords()
            populateLatestYear()
        End If
    End Sub

    Private Sub LoadProductGroup()

        ddlProductGroupCode.Items.Clear()
        ddlProductGroupCode.Items.Insert(0, New ListItem("Select Product Group"))
        ddlProductGroupCode.Items(0).Selected = True
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        Dim strsql As String = "SELECT * FROM ProductGroup where EventId=" & Session("EventId") & " order by ProductGroupId Desc"
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        While dsRecords.Read
            ddlProductGroupCode.Items.Insert(++i, New ListItem(dsRecords("ProductGroupCode"), dsRecords("ProductGroupId")))
        End While

    End Sub

    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        Dim CmdText As String = String.Empty
        CmdText = "select MP.MaxScoresId,MP.EventID,E.Name,MP.ProductGroupId,MP.ProductGroupCode,MP.Year,MP.Phase1, MP.Phase2,MP.MaxScores from MaxScoresByPhase MP left join Event E on (MP.EventID=E.EventId) Order BY Year Desc,MP.EventID ASC"

        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        gvMaxScores.DataSource = dt
        gvMaxScores.DataBind()
        gvMaxScores.Columns(1).Visible = True
        pnlMaxScores.Visible = True
        lblStatus.Visible = False
        If (dt.Rows.Count = 0) Then
            lblStatus.Visible = True
            lblStatus.Text = "No Records to Display"
        End If
    End Sub

    Private Sub clear()
        txtPhase1.Text = ""
        txtPhase2.Text = ""
        txtMaxScores.Text = ""

        btnAdd.Text = "Add"
        ddlProductGroupCode.ClearSelection()
        ddlProductGroupCode.Items.FindByText("Select Product Group").Selected = True
        ddlEvent.SelectedValue = "0"
        lblMsg.Text = ""
    End Sub

    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from MaxScoresByphase where MaxScoresId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        ddlEvent.SelectedValue = hdnEventID.Value
        ddlProductGroupCode.SelectedIndex = ddlProductGroupCode.Items.IndexOf(ddlProductGroupCode.Items.FindByText(dsRecords.Tables(0).Rows(0)("ProductGroupCode").ToString()))
        ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(dsRecords.Tables(0).Rows(0)("Year").ToString()))

        txtPhase1.Text = dsRecords.Tables(0).Rows(0)("Phase1").ToString()
        txtPhase2.Text = dsRecords.Tables(0).Rows(0)("Phase2").ToString()
        txtMaxScores.Text = dsRecords.Tables(0).Rows(0)("MaxScores").ToString()

    End Sub


    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click

        Dim strsql As String
        Dim Phase2Score As String = String.Empty

        If btnAdd.Text = "Add" Then
            If ddlEvent.SelectedValue = "0" Then
                lblMsg.Text = "Please Select Event"
            ElseIf ddlProductGroupCode.SelectedItem.Text = "Select Product Group" Then
                lblMsg.Text = "Please Select Product Group Code"
            ElseIf txtPhase1.Text.Length < 1 Then
                lblMsg.Text = "Please Enter Phase1 Value"
            ElseIf ddlProductGroupCode.SelectedItem.Text <> "BB" And ddlProductGroupCode.SelectedItem.Text <> "SC" Then
                If txtPhase2.Text.Length < 1 Then
                    lblMsg.Text = "Please Enter Phase2 Value"
                End If

            Else
                Dim cnt As Integer = 0

                If txtPhase2.Text = "0" Or txtPhase2.Text = "" Then
                    Phase2Score = "NULL"
                Else
                    Phase2Score = "'" & txtPhase2.Text & "'"
                End If
                strsql = "Select count(ProductGroupId) from MaxScoresbyPhase where ProductGroupId=" & ddlProductGroupCode.SelectedItem.Value & " AND Year=" & ddlYear.SelectedItem.Text & " AND EventID=" & ddlEvent.SelectedValue & " "
                cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, strsql)
                If cnt = 0 Then
                    strsql = "insert Into MaxScoresbyPhase(ProductGroupId, ProductGroupCode,Year, Phase1, Phase2, MaxScores,EventID,CreatedDate,CreatedBy) Values ("
                    strsql = strsql & ddlProductGroupCode.SelectedItem.Value & ",'" & ddlProductGroupCode.SelectedItem.Text & "'," & ddlYear.SelectedItem.Text & ","
                    strsql = strsql & txtPhase1.Text & "," & Phase2Score & "," & txtMaxScores.Text & "," & ddlEvent.SelectedValue & ",GetDate()," & Session("LoginID").ToString() & ")"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, strsql)
                    GetRecords()
                    clear()
                    lblMsg.Text = "Record Added Successfully"
                    lblStatus.Visible = False
                Else
                    lblStatus.Visible = False
                    lblMsg.Text = "The max scores for same Event, Product group code and Year is already Exist"
                End If
            End If
        ElseIf btnAdd.Text = "Update" Then
            If ddlProductGroupCode.SelectedItem.Text = "Select Product Group" Then
                lblMsg.Text = "Please Select Product Group Code"
            ElseIf txtPhase1.Text.Length < 1 Then
                lblMsg.Text = "Please Enter Phase1 Value"
            ElseIf txtPhase2.Text.Length < 1 Then
                lblMsg.Text = "Please Enter Phase2 Value"
            Else
                If txtPhase2.Text = "0" Or txtPhase2.Text = "" Then
                    Phase2Score = "NULL"
                Else
                    Phase2Score = "'" & txtPhase2.Text & "'"
                End If
                strsql = "Update MaxScoresByPhase Set ProductGroupId=" & ddlProductGroupCode.SelectedItem.Value & ", ProductGroupCode='" & ddlProductGroupCode.SelectedItem.Text & "', Year=" & ddlYear.SelectedItem.Text & ", Phase1=" & txtPhase1.Text & ", Phase2=" & Phase2Score & ", MaxScores=" & txtMaxScores.Text & ",EventID=" & ddlEvent.SelectedValue & ",ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID").ToString() & "  Where MaxScoresId=" & Session("MaxScoresId") & ""
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, strsql)
                GetRecords()
                clear()
                lblStatus.Visible = False
                lblMsg.Text = "Record Updated Successfully"

            End If
        End If
    End Sub

    Protected Sub gvMaxScores_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        btnAdd.Text = "Update"
        lblMsg.Text = ""
        'Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
        'gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
        'Dim RowIndex As Integer = gvRow.RowIndex

        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        gvMaxScores.Rows(index).BackColor = ColorTranslator.FromHtml("#EAEAEA")
        Dim TransID As Integer
        Dim EventID As String = String.Empty
        EventID = Val(gvMaxScores.Rows(index).Cells(3).Text)

        EventID = DirectCast(gvMaxScores.Rows(index).FindControl("lblEventID"), Label).Text
        hdnEventID.Value = EventID
        TransID = Val(gvMaxScores.DataKeys(index).Value)

        If (TransID) And TransID > 0 Then
            Session("MaxScoresId") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub

    Private Sub populateLatestYear()
        Try


            Dim ds As New DataSet
            Dim CmdText As String = String.Empty
            CmdText = "select distinct top 5 Year from MaxScoresByPhase"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, CmdText)

            If ds.Tables.Count > 0 Then

                ddlReplicateFrom.DataSource = ds
                ddlReplicateFrom.DataValueField = "Year"
                ddlReplicateFrom.DataTextField = "Year"
                ddlReplicateFrom.DataBind()
                'ddlReplicateFrom.Items.Insert(0, "Select")

                ddlReplicateTo.Items.Clear()
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim year As Integer = Convert.ToInt32(dr("Year"))
                    Dim dt As New DateTime()
                    dt = Convert.ToDateTime(" " & year & ", 1, 1")
                    Dim fnlYr As Integer = 2016
                    dt = dt.AddYears(1)
                    fnlYr = dt.Year
                    ddlReplicateTo.Items.Add(New ListItem(fnlYr, fnlYr))
                Next



            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs)
        Try
            If ddlReplicateFrom.SelectedValue <> "Select" And ddlReplicateTo.SelectedValue <> "-1" And ddlReplicateTo.SelectedValue <> "" Then

                Dim CmdText As String = String.Empty
                Dim ds As DataSet = New DataSet()
                CmdText = "select count(*) as CountSet from MaxScoresByPhase where [Year]=" & ddlReplicateTo.SelectedValue & ""

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                Dim count As Integer = 0

                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        count = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString())
                    End If
                End If
                If (count <= 0) Then


                    CmdText = "insert into MaxScoresByPhase (EventID,ProductGroupId,ProductGroupCode,Phase1,Phase2,MaxScores,Year,CreatedDate,CreatedBy) select EventID,ProductGroupId,ProductGroupCode,Phase1,Phase2,MaxScores," & ddlReplicateTo.SelectedValue & ",GetDate()," & Session("LoginID").ToString() & " from MaxScoresByPhase where [Year]=" & ddlReplicateFrom.SelectedValue & ""
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                    lblMsg.Text = "Records replicated successfully"
                    GetRecords()
                Else
                    lblMsg.Text = "Duplicate Exists"
                End If
            Else
                If ddlReplicateFrom.SelectedValue = "Select" Then
                    lblMsg.Text = "Please select Replicate From"
                Else
                    lblMsg.Text = "Please select Replicate To"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlReplicateFrom_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

            ddlReplicateTo.Items.Clear()
            Dim year As Integer = Convert.ToInt32(ddlReplicateFrom.SelectedValue)
            Dim dt As New DateTime()
            dt = Convert.ToDateTime(" " & year & ", 1, 1")
            Dim fnlYr As Integer = 2016
            dt = dt.AddYears(1)
            fnlYr = dt.Year

            'ddlReplicateTo.Items.Add(New ListItem("Select", -1))
            ddlReplicateTo.Items.Add(New ListItem(fnlYr, fnlYr))

        Catch ex As Exception

        End Try
    End Sub

End Class
