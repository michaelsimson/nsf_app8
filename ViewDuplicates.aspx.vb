Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Net
Imports System.Net.Mail
Imports NorthSouth.DAL
Imports System.Data.SqlClient

Partial Class ViewDuplicates
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
                Server.Transfer("login.aspx")
            End If
        End If
    End Sub
    Sub LoadGrid()
        Dim strSql As String
        Dim top As String
        If DdlRecords.SelectedItem.Text = "All" Then
            top = ""
        Else
            top = "TOP(" & DdlRecords.SelectedItem.Value & ")"
        End If

        Select Case ddlStatus.SelectedValue
            Case "Open"
                strSql = "SELECT " & top & " * FROM INDDUPLICATE WHERE STATUS <> 'Completed' and AutomemberId <> 0 order by CreateDate desc,IndDupID Asc" 'AutoMemberId desc,ORDER BY CreateDate DESC"
                'Case "FORGOT PASSWORD"
                ' strSql = "SELECT " & top & " * FROM INDDUPLICATE WHERE STATUS = '" + ddlStatus.SelectedValue + "' or STATUS = 'Pending' ORDER BY CreateDate DESC"
            Case Else
                strSql = "SELECT " & top & " * FROM INDDUPLICATE WHERE STATUS = '" + ddlStatus.SelectedValue + "' and AutomemberId <> 0 order by CreateDate desc,IndDupID Asc" 'ORDER BY CreateDate DESC"
        End Select
        Dim dsDuplicates As New DataSet
        dsDuplicates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        If dsDuplicates.Tables(0).Rows.Count > 0 Then
            dgDuplicates.Visible = True
            dgDuplicates.DataSource = dsDuplicates
            dgDuplicates.DataBind()
            lblAlert.Visible = False
            If Session("RoleId") = 29 Then
                dgDuplicates.Columns(2).Visible = False
            End If
        Else
            dgDuplicates.Visible = False
            lblAlert.Visible = True
            lblAlert.Text = "No Duplicate Records Found."
        End If
    End Sub

    Protected Sub dgDuplicates_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDuplicates.PageIndexChanged
        dgDuplicates.CurrentPageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub dgDuplicates_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDuplicates.UpdateCommand
        Dim strSql As String
        Dim sBody As String
        'strSql = "SELECT * FROM INDDUPLICATE WHERE AutoMemberID=" & Request.QueryString("ID")
        'strSql = strSql & " AND DonorType IN('IND','SPOUSE') AND Status<>'Completed' ORDER BY DonorType"
        strSql = "SELECT * FROM INDDUPLICATE WHERE IndDupID=" & CType(e.Item.FindControl("lblDupID"), Label).Text
        strSql = strSql & " AND DonorType IN('IND') AND Status<>'Completed' ORDER BY DonorType"
        'Dim drIndDuplicate As SqlDataReader
        'drIndDuplicate = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        Dim dsIndDuplicate As New DataSet
        dsIndDuplicate = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        Dim objIndividual As IndSpouse10
        Dim objSpouse As IndSpouse10
        Dim indID As Double
        Dim spouseID As Double
        Dim bIsIndividual As Boolean
        Dim bIsSpouse As Boolean
        bIsIndividual = False
        Dim iChapterID As Integer
        Dim strChapter As String
        bIsSpouse = False
        iChapterID = 0
        Dim strDonorType As String = ""
        Dim i As Integer
        Dim strEmail As String
        Dim strIndName As String
        Dim strSpouseName As String
        Dim strEmail1 As String
        Dim strEmail2 As String
        If dsIndDuplicate.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsIndDuplicate.Tables(0).Rows.Count - 1
                strDonorType = dsIndDuplicate.Tables(0).Rows(i)("DonorType").ToString()
                If strDonorType = "IND" And bIsIndividual = False Then
                    strEmail = "'" & dsIndDuplicate.Tables(0).Rows(i)("Email").ToString() & "'"
                    strEmail1 = dsIndDuplicate.Tables(0).Rows(i)("Email").ToString()

                End If
            Next
        End If
        strSql = "SELECT * FROM INDDUPLICATE WHERE DonorType IN('SPOUSE') "
        strSql = strSql & " AND Status<>'Completed' AND INDDUPID=" & CDbl(CType(e.Item.FindControl("lblDupID"), Label).Text) + 1 & " ORDER BY DonorType "
        Dim dsSpouse As New DataSet
        dsSpouse = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        Dim j As Integer
        For j = 0 To dsSpouse.Tables(0).Rows.Count - 1
            If Len(strEmail) > 0 Then
                strEmail = strEmail & ",'" & dsSpouse.Tables(0).Rows(j)("Email").ToString() & "'"
            Else
                strEmail = "'" & dsSpouse.Tables(0).Rows(j)("Email").ToString() & "'"
            End If
            strEmail2 = dsSpouse.Tables(0).Rows(j)("Email").ToString()
        Next
        Dim strName As String
        If Len(strSpouseName) > 0 Then
            strName = strIndName & " and " & strSpouseName & ","
        Else
            strName = strIndName & ","
        End If
        strSql = "SELECT * FROM LOGIN_MASTER WHERE Upper(User_Email) IN(" & strEmail & ")"
        Dim drLoginEmail1 As SqlDataReader

        drLoginEmail1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        sBody = "Dear " & strName & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & "The following are your log in ID and password: " & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & vbCrLf
        While drLoginEmail1.Read()
            sBody = sBody & "Login Email: " & drLoginEmail1("User_Email") & vbCrLf
            sBody = sBody & "Password: " & drLoginEmail1("User_Pwd") & vbCrLf
            sBody = sBody & vbCrLf
        End While
        If Len(strEmail1) > 0 And Len(strEmail2) > 0 Then
            sBody = sBody & "You can use either of the login IDs to access your records on the NSF system." & vbCrLf
        End If
        sBody = sBody & "Please keep these in a safe place." & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & "Thank you" & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & "With regards," & vbCrLf
        sBody = sBody & vbCrLf
        sBody = sBody & "NSF Customer Service Team" & vbCrLf
        If Len(strEmail1) > 0 Then
            SendEmail("Registration Problem / NSF", sBody, strEmail1)
        End If
        If Len(strEmail2) > 0 Then
            SendEmail("Registration Problem / NSF", sBody, strEmail2)
        End If

        strSql = "UPDATE INDDUPLICATE SET STATUS='" & CType(e.Item.FindControl("ddlStatus"), DropDownList).SelectedValue & "',"
        strSql = strSql & "Remarks = '" & CType(e.Item.FindControl("txtRemarks"), TextBox).Text & "'"
        strSql = strSql & " WHERE AutoMemberID=" & CType(e.Item.FindControl("lblAutomemberID"), Label).Text
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
        dgDuplicates.EditItemIndex = -1
        LoadGrid()
    End Sub
    Protected Sub dgDuplicates_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDuplicates.EditCommand
        Dim intIndexKey As Integer
        Dim i As Integer
        intIndexKey = dgDuplicates.DataKeys(e.Item.ItemIndex)
        For i = 0 To dgDuplicates.DataKeys.Count - 1
            If (dgDuplicates.DataKeys(i) = intIndexKey) Then
                dgDuplicates.EditItemIndex = i
                Exit For
            End If
        Next
        Page.DataBind()
        LoadGrid()
    End Sub
    Protected Sub dgDuplicates_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDuplicates.CancelCommand
        dgDuplicates.EditItemIndex = -1
        Page.DataBind()
        LoadGrid()
    End Sub
    Protected Sub dgDuplicates_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDuplicates.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("hlnkView"), HyperLink).NavigateUrl = "javascript:var w=window.open('ShowIndRecord.aspx?First=" & e.Item.DataItem("FirstName") & "&Last=" & e.Item.DataItem("LastName") & "&Email=" & e.Item.DataItem("Email") & "','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"
            CType(e.Item.FindControl("hlnkCreate"), HyperLink).NavigateUrl = "javascript:var w=window.open('CreateIndSpouse.aspx?ID=" & e.Item.DataItem("AutomemberID") & "&DupID=" & e.Item.DataItem("IndDupID") & "','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"
            CType(e.Item.FindControl("hlnkEmailChange"), HyperLink).NavigateUrl = "javascript:var w=window.open('VolChangeEmail.aspx?ID=" & e.Item.DataItem("AutomemberID") & "&DupID=" & e.Item.DataItem("IndDupID") & "','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"
            If DataBinder.Eval(e.Item.DataItem, "Status") = "Completed" Then
                CType(e.Item.FindControl("hlnkCreate"), HyperLink).Visible = False
                CType(e.Item.FindControl("hlnkEmailChange"), HyperLink).Visible = False
                e.Item.Cells(0).Enabled = False
                e.Item.Cells(2).Enabled = False
                
            Else
                'Dim btn As LiteralControl = Nothing
                'btn = CType(e.Item.Cells(2).Controls(0), LiteralControl)
                'btn..Attributes.Add("onclick", "return confirm('Do you want to replace the old with the new as shown?');")
                e.Item.Cells(0).Enabled = True
            End If
            If DataBinder.Eval(e.Item.DataItem, "DonorType") = "SPOUSE" Then
                CType(e.Item.FindControl("hlnkView"), HyperLink).Visible = False
                CType(e.Item.FindControl("hlnkCreate"), HyperLink).Visible = False
                CType(e.Item.FindControl("hlnkEmailChange"), HyperLink).Visible = False
                e.Item.Cells(2).Enabled = False
                e.Item.Cells(0).Enabled = False
            End If
        End If
    End Sub

    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
        LoadGrid()
    End Sub
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@gmail.com")
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody

        Dim client As New SmtpClient()
        Dim ok As Boolean = True

        Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

        client.Host = host
        client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
        Try
            client.Send(email)
        Catch e As Exception
            ok = False
        End Try
    End Sub

End Class
