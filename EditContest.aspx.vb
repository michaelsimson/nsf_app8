'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/EditContest.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page allows the Chapter Coordinator to Edit the Contest
'           Details with Venue Information. 
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetEvent
'           2) NSF\usp_UpdateEvent
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Text
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration


Partial Class EditContest
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEndTime As System.Web.UI.WebControls.TextBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'If Session("LoggedIn") <> "LoggedIn" Then
            '    Server.Transfer("login.aspx")
            'End If
        If Page.IsPostBack = False Then
            If Not Request("ContestID") Is Nothing Then
                Dim objContest As Contest
                objContest = New Contest
                objContest.GetContestByID(Application("ConnectionString"), Request("ContestID"))
                With objContest
                    ViewState("ContestTypeID") = Convert.ToInt16(.ContestTypeID)
                    ViewState("ContestCategoryID") = Convert.ToInt16(.ContestCategoryID)
                    ViewState("ContestYear") = .Contest_Year
                    txtDescription.Text = GetContestName(Convert.ToInt16(.ContestCategoryID))
                    txtContestYear.Text = .Contest_Year.ToString
                    If Not IsDBNull(.ContestDate) And Year(.ContestDate) >= Application("ContestYear") Then
                        txtContestDate.Text = Convert.ToDateTime(.ContestDate).ToShortDateString
                    Else
                        txtContestDate.Text = ""
                    End If
                    rblContestType.Items.FindByValue(.ContestTypeID).Selected = True
                    ViewState("ChapterID") = Convert.ToInt16(.NSFChapterID)
                    lblChapter.Text = GetChapterName(Convert.ToInt16(.NSFChapterID))
                    If Not IsDBNull(.StartTime) Then ddlStartTime.Items.FindByValue(.StartTime.ToString).Selected = True
                    If Not IsDBNull(.EndTime) Then ddlEndTime.Items.FindByValue(.EndTime.ToString).Selected = True
                    txtBuilding.Text = .Building.ToString
                    txtRoom.Text = .Room.ToString
                    If Not IsDBNull(.CheckinTime) Then ddlCheckInTime.Items.FindByValue(.CheckinTime.ToString).Selected = True
                    If Not IsDBNull(.RegistrationDeadline) And Year(.RegistrationDeadline) >= Application("ContestYear") Then
                        txtRegistrationDeadlineDate.Text = Convert.ToDateTime(.RegistrationDeadline).ToShortDateString
                    Else
                        txtRegistrationDeadlineDate.Text = ""
                    End If

                End With
            End If
        End If
    End Sub
    Private Function GetContestName(ByVal ContestCategoryID As Integer) As String

        Dim dsContestCategory As DataSet
        Dim ddlContestCategory As New DropDownList

        Dim prmArray(1) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ContestYear"
        prmArray(0).Value = Application("ContestYear")
        prmArray(0).Direction = ParameterDirection.Input

        dsContestCategory = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestCategoryAll", prmArray)
        If dsContestCategory.Tables.Count > 0 Then
            ddlContestCategory.DataSource = dsContestCategory.Tables(0)
            ddlContestCategory.DataTextField = dsContestCategory.Tables(0).Columns("ContestDesc").ToString
            ddlContestCategory.DataValueField = dsContestCategory.Tables(0).Columns("ContestCategoryID").ToString
            ddlContestCategory.DataBind()
        End If
        Return ddlContestCategory.Items.FindByValue(ContestCategoryID).Text
    End Function
        Private Function GetChapterName(ByVal ChapterID As Integer) As String
            Dim dsChapters As New DataSet
            Dim ddlChapters As New DropDownList
            Dim objChapters As New Chapter
            '  objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
            ' From CreateUser.aspx
            Try
                dsChapters = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapters")
            Catch se As SqlException
              
            End Try
            If dsChapters.Tables.Count > 0 Then
                ddlChapters.DataSource = dsChapters.Tables(0)
                ddlChapters.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlChapters.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlChapters.DataBind()
            End If
            If ChapterID.ToString <> "" Then
                Return ddlChapters.Items.FindByValue(ChapterID).Text.ToString
            Else
                Return ChapterID.ToString
            End If
        End Function

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim objContest As Contest
        objContest = New Contest
        objContest.Id = Request("ContestID")
        If Not Server.HtmlEncode(txtContestDate.Text) = "" Then
            objContest.ContestDate = Convert.ToDateTime(Server.HtmlEncode(txtContestDate.Text))
        Else
            objContest.ContestDate = Convert.ToDateTime("1900/01/01")
        End If

        If Not Server.HtmlEncode(ddlStartTime.SelectedItem.Value) = "" Then
            objContest.StartTime = Server.HtmlEncode(ddlStartTime.SelectedItem.Value)
        End If

        If Not Server.HtmlEncode(ddlEndTime.SelectedItem.Value) = "" Then
            objContest.EndTime = Server.HtmlEncode(ddlEndTime.SelectedItem.Value)
        End If

        If Not Server.HtmlEncode(txtBuilding.Text) = "" Then
            objContest.Building = Server.HtmlEncode(txtBuilding.Text)
        End If

        If Not Server.HtmlEncode(txtRoom.Text) = "" Then
            objContest.Room = Server.HtmlEncode(txtRoom.Text)
        End If

        If Not Server.HtmlEncode(txtRegistrationDeadlineDate.Text) = "" Then
            objContest.RegistrationDeadline = Convert.ToDateTime(Server.HtmlEncode(txtRegistrationDeadlineDate.Text))
        Else
            objContest.RegistrationDeadline = DateTime.MinValue
        End If
        If Not Server.HtmlEncode(txtRegistrationDeadlineDate.Text) = "" Then
            objContest.RegistrationDeadline = Convert.ToDateTime(Server.HtmlEncode(txtRegistrationDeadlineDate.Text))
        Else
            objContest.RegistrationDeadline = Convert.ToDateTime("1900/01/01")
        End If
        If Not Server.HtmlEncode(ddlCheckInTime.SelectedItem.Value) = "" Then
            objContest.CheckinTime = Server.HtmlEncode(ddlCheckInTime.SelectedItem.Value)
        End If
        objContest.ContestCategoryID = ViewState("ContestCategoryID")
        objContest.NSFChapterID = ViewState("ChapterID")
        objContest.ContestTypeID = ViewState("ContestTypeID")
        objContest.Contest_Year = ViewState("ContestYear")
        objContest.UpdateContest(Application("ConnectionString"))
        Response.Redirect("EventsList.aspx")

    End Sub
        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
            Dim client As New SmtpClient()
            Dim email As New MailMessage

            'Build Email Message
            client.Host = ""

            email.From = New MailAddress("contests@northsouth.org")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = "<strong>Hello Steve!</strong>"

            Try
                client.Send(email)
            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function
End Class

End Namespace

