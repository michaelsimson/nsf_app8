
Partial Class DonationReceipt
    Inherits System.Web.UI.Page
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Response.Redirect("ShowDonationReceipt.aspx?Year=" & ddlYear.SelectedItem.Text & "&Mail=Y")
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim thisdate As DateTime
            thisdate = DateTime.Now
            Dim dtcurryear As DateTime
            Dim dtPrevYear As DateTime
            dtcurryear = thisdate
            dtPrevYear = DateTime.Now.AddYears(-1)
            Dim i As Integer
            For i = dtPrevYear.Year To dtcurryear.Year
                ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            Next
        End If
    End Sub
End Class
