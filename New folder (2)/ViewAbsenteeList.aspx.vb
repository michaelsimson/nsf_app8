﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ViewAbsenteeList
    Inherits System.Web.UI.Page
    Dim EventCode, ProductCode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Not IsPostBack() Then
            'If Session("RoleId") = "1" Or Session("RoleId") = "2" Or Session("RoleId") = "9" Or Session("RoleId") = "16" Or Session("RoleId") = "18" Then
            getContestYear(ddlYear)
            getEvent()
            LoadChapter(ddlChapter)
            'End If
        End If
    End Sub
    Private Sub getContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub getEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
        'getProductGroup()
    End Sub
    Private Sub LoadChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        ddlChapter.DataTextField = "ChapterCode"
        ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub getProductGroup()
        lblAbsentErr.Text = ""
        Dim strSQl As String = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join ProductGroup P On C.ProductGroupId = p.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & " Order BY P.ProductGroupID "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        If ds.Tables(0).Rows.Count = 1 Then
            ddlProductGroup.DataSource = ds
            ddlProductGroup.DataBind()
            LoadProduct()
            ddlProductGroup.Enabled = False
            BtnViewList.Enabled = True
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            ddlProductGroup.DataSource = ds
            ddlProductGroup.DataBind()
            ddlProductGroup.Enabled = True
            BtnViewList.Enabled = True
            LoadProduct()
        Else
            ShowError()
        End If
    End Sub
    Public Sub LoadProduct()
        Dim StrSQL As String = ""
        lblAbsentErr.Text = ""
        StrSQL = "select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' AND P.EventID=" & ddlEvent.SelectedValue & " Order By P.ProductId"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
        If ds.Tables(0).Rows.Count = 1 Then
            ddlProduct.DataSource = ds
            ddlProduct.DataBind()
            ddlProduct.Enabled = False
            BtnViewList.Enabled = True
        ElseIf ds.Tables(0).Rows.Count > 0 Then
            ddlProduct.Enabled = True
            ddlProduct.DataSource = ds
            ddlProduct.DataBind()
            BtnViewList.Enabled = True
        Else
            ShowError()
         End If
        'LoadRoom()
    End Sub
    Private Sub LoadRoom()
        Dim NRooms As Integer = 0
        Dim ds As DataSet
        'NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From RoomSchedule R1 Inner Join RoomSchedule R2 On R2.ContestYear = R1.ContestYear And R2.EventID = R1.EventID And R2.ChapterID = R1.ChapterID And R2.ProductGroupID = R1.ProductGroupID and R2.ProductID=R1.ProductID  Where R1.ContestYear=" & ddlYear.SelectedValue & " and R1.EventID=" & ddlEvent.SelectedValue & " and R1.ChapterID=" & ddlChapter.SelectedValue & " and R1.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and R1.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and R1.Phase=" & ddlPhase.SelectedValue & "") 'and R2.Phase=3 ")
        If Request.QueryString("ID") = 1 Then
            Dim SQLRoleRooms = "Select c.SeqNo From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID"
            SQLRoleRooms = SQLRoleRooms & " and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlEvent.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & ""
            SQLRoleRooms = SQLRoleRooms & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.Phase=" & ddlPhase.SelectedValue & " and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleRooms)
            NRooms = ds.Tables(0).Rows.Count
        Else
            NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and  Phase=" & ddlPhase.SelectedValue & "")
        End If

        If NRooms <= 0 Then
            NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph" & ddlPhase.SelectedValue & "Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
        End If
        Dim i As Integer
        ddlRoom.Items.Clear()

        If Request.QueryString("ID") = 1 And ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To NRooms - 1
                ddlRoom.Items.Insert(i, New ListItem(ds.Tables(0).Rows(i)(0), ds.Tables(0).Rows(i)(0)))
            Next
        Else
            For i = 0 To NRooms - 1
                ddlRoom.Items.Insert(i, New ListItem(i + 1, i + 1))
            Next
        End If
       

    End Sub

    Protected Sub BtnViewAbsentee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewList.Click
        If ddlEvent.SelectedIndex = 0 Then
            lblAbsentErr.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAbsentErr.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlProductGroup.SelectedValue = "" Then 'Is Nothing
            lblAbsentErr.Text = "Please Select Contest"
            Exit Sub
        ElseIf ddlProduct.SelectedValue = "" Then
            lblAbsentErr.Text = "Please Select Contest-Level"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAbsentErr.Text = "Please Select Phase"
            Exit Sub
        ElseIf BtnViewList.Enabled = False Then
            lblAbsentErr.Text = "Contest Details Yet to Uploaded"
            Exit Sub
        End If
        If ddlEvent.SelectedValue = "1" Then
            EventCode = "Fin"
        Else
            EventCode = "Reg"
        End If
        Try
            ProductCode = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode From Contest Where ContestId=" & ddlProduct.SelectedValue & "")
            ViewAbsenteeList()
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Page_Load(sender, e)
    End Sub
    Protected Sub ViewAbsenteeList()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheet", New SqlParameter("@ContestID", ddlProduct.SelectedValue))

        If ds.Tables(0).Rows.Count > 0 Then
            Dim ds1 As DataSet
            Dim NRooms As Integer
            Dim RoomSchflag As Boolean = True
            Dim SQLRoomSch As String = ""
            'NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph" & ddlPhase.SelectedValue & "Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
            'NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From RoomSchedule R1 Inner Join RoomSchedule R2 On R2.ContestYear = R1.ContestYear And R2.EventID = R1.EventID And R2.ChapterID = R1.ChapterID And R2.ProductGroupID = R1.ProductGroupID and R2.ProductID=R1.ProductID  Where R1.ContestYear=" & ddlYear.SelectedValue & " and R1.EventID=" & ddlEvent.SelectedValue & " and R1.ChapterID=" & ddlChapter.SelectedValue & " and R1.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and R1.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and R1.Phase=" & ddlPhase.SelectedValue & "2 ") 'and R2.Phase=3 ")
            If Request.QueryString("ID") = 1 Then
                Dim SQLRoleRooms = "Select  c.SeqNo From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID"
                SQLRoleRooms = SQLRoleRooms & " and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlEvent.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & ""
                SQLRoleRooms = SQLRoleRooms & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.Phase=" & ddlPhase.SelectedValue & " and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))"
                ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleRooms)
                NRooms = ds1.Tables(0).Rows.Count
            Else
                NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and  Phase=" & ddlPhase.SelectedValue & "")
            End If

            If NRooms <= 0 Then
                NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph" & ddlPhase.SelectedValue & "Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                RoomSchflag = False
            Else
                RoomSchflag = True
            End If

            Dim NContestants As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM Contestant WHERE ContestID =" & ddlProduct.SelectedValue & " AND BadgeNumber IS NOT NULL AND PaymentReference IS NOT NULL ")
            Dim RoomCount, StartRoomCount, EndRoomCount, RoomCountRest, RoomNumber, MaxValue, x, i, j, StRoomSchCount, EndRoomschCount As Integer
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\AbsenteesList\Absentees_List.xls")) '-- if the file exists on the server
            If Not file.Exists Then
                Response.Write("File Does not Exist")
                Exit Sub
            End If
            If file.Exists Then
                Dim xlWorkBook_abs As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\AbsenteesList\Absentees_List.xls"))
                Dim SheetCount As Integer = xlWorkBook_abs.Worksheets.Count
                For rvalue As Integer = NRooms + 1 To SheetCount - 1
                    xlWorkBook_abs.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                Next
                Dim SheetValue As IWorksheet

                Dim str As IRange '= SheetValue.Range(4, 28)
                Dim PWD As String '= str.Value.ToString()

                MaxValue = NRooms

                For RoomNumberCount As Integer = 1 To NRooms
                    i = 7
                    j = 6
                    RoomNumber = RoomNumberCount
                    If NRooms > 0 Then
                        If RoomSchflag = False Then
                            'To split Contestants uniformly in NRooms
                            'Number of contestants in a room  = Truncated as Integer(total number of contestants / No. of Rooms + 0.95)

                            RoomCount = Math.Floor((NContestants / NRooms) + 0.95)
                            RoomCountRest = NContestants - ((NRooms - 1) * RoomCount)
                            StartRoomCount = (RoomNumber - 1) * RoomCount
                            EndRoomCount = (RoomNumber * RoomCount)
                            If RoomNumber = MaxValue Then
                                If Not RoomCountRest = 0 Then '(RoomCount / 2) 
                                    EndRoomCount = StartRoomCount + RoomCountRest 'EndRoomCount1 +
                                End If
                            End If
                        ElseIf RoomSchflag = True Then
                            ''To get Data From RoomSchedule
                            SQLRoomSch = "Select StartBadgeNo,EndBadgeNo,SeqNo From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and  Phase=" & ddlPhase.SelectedValue & " and SeqNo=" & ddlRoom.SelectedValue & ""
                            Dim dsRoomSch As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomSch)
                            If dsRoomSch.Tables(0).Rows.Count > 0 Then
                                Integer.TryParse(dsRoomSch.Tables(0).Rows(0)("StartBadgeNo").ToString.Trim(), StRoomSchCount)
                                Integer.TryParse(dsRoomSch.Tables(0).Rows(0)("EndBadgeNo").ToString.Trim(), EndRoomschCount)
                                StartRoomCount = StRoomSchCount - 1
                                EndRoomCount = EndRoomschCount
                            Else
                                'lbldwError.Text = "Badge numbers are missing in Room Schedule for the Sequence number '" & RoomNumberCount & "'"
                                Exit Sub
                            End If
                        End If
                    Else
                        'lbldwError.Text = "Room Data Is Not available"
                        StartRoomCount = 0
                        EndRoomCount = ds.Tables(0).Rows.Count
                    End If

                    SheetValue = xlWorkBook_abs.Worksheets(RoomNumberCount)

                    str = SheetValue.Range(4, 28)
                    PWD = str.Value.ToString()
                    SheetValue.Unprotect(PWD)

                    For x = StartRoomCount To EndRoomCount - 1 'ds.Tables(0).Rows.Count - 1
                        If ds.Tables(0).Rows(x)("CheckinFlag").ToString <> "Y" Then  'Is DBNull.Value
                            SheetValue.Range(i, 3).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                            SheetValue.Range(i, 4).Value = ds.Tables(0).Rows(x)("DOB")
                            SheetValue.Range(i, 5).Value = ds.Tables(0).Rows(x)("ParticipantName")
                            SheetValue.Range(i, 6).Value = ds.Tables(0).Rows(x)("Grade")
                            'store the contestant id in column AA
                            SheetValue.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")
                            i = i + 1
                        Else
                            xlWorkBook_abs.Worksheets(10).Range(j, 3).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                            xlWorkBook_abs.Worksheets(10).Range(j, 4).Value = ds.Tables(0).Rows(x)("DOB")
                            xlWorkBook_abs.Worksheets(10).Range(j, 5).Value = ds.Tables(0).Rows(x)("ParticipantName")
                            xlWorkBook_abs.Worksheets(10).Range(j, 6).Value = ds.Tables(0).Rows(x)("Grade")
                            j = j + 1
                        End If

                    Next

                    If Not ddlRoom.SelectedValue = "ALL" Then
                        For rvalue As Integer = 1 To NRooms
                            If Not ddlRoom.SelectedValue = rvalue Then
                                xlWorkBook_abs.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                            End If
                        Next
                    End If

                    SheetValue.Protect(PWD)
                Next
                Response.Clear()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AddHeader("Content-Disposition", "attachment;filename=" & ddlYear.SelectedValue & "_" & EventCode & "_CheckinStatus_" & ddlProductGroup.SelectedValue & "_" & ProductCode & "_" & ddlChapter.SelectedItem.Text & "_Phase" & ddlPhase.SelectedValue & "_R" & ddlRoom.SelectedValue & ".xls") '_P2(Absentees_List).xls")
                xlWorkBook_abs.SaveAs(Response.OutputStream)
                Response.End()
            Else
                Response.Write("Absentees File Does not Exist")
            End If

        End If

    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        LoadChapter(ddlChapter)
        BtnViewList.Enabled = True
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlPhase.SelectedIndex = 0
        ddlRoom.Items.Clear()

        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
        Else
            ddlChapter.Enabled = True
        End If
        getProductGroup()
    End Sub
    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        getProductGroup()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        LoadProduct()
        'LoadRoom()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        getEvent()
        ddlChapter.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlPhase.SelectedIndex = 0
        ddlRoom.Items.Clear()
    End Sub
    Private Sub ShowError()
        BtnViewList.Enabled = False
        ddlProductGroup.Enabled = False
        ddlProduct.Enabled = False
        lblAbsentErr.Text = "Contest Data not Yet Updated"
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        LoadRoom()
    End Sub
End Class
