<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ManageScoresheet.aspx.vb" Inherits="ManageScoresheet" title="Manage Score Sheets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

    <script language="javascript" type="text/javascript">
 function fnOpen(fl)
 {
    window.open('DownloadScoreSheets.aspx?file=' + fl);
 }
 </script>



    <div align="left" >
<asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
<div align="center" style ="width:100%">
<table cellpadding ="3" cellspacing = "0" border = "0" align="center">
<tr><td colspan = "3" align ="center" >
    <asp:Label ID="lblChapter" Visible="false"  CssClass="title02" runat="server"></asp:Label></td></tr>
<tr runat="server" id="trAll">
<td style="width: 291px">
<table cellpadding ="3" cellspacing = "0" border = "0" align="center">
<tr><td colspan ="2" align="center" class="title04">Download Score Sheet with Contestant Data</td></tr>
<%--<tr><td align="left">Select Product & Chapter </td><td align="left" >
    <asp:DropDownList ID="ddlContest" DataTextField="text" Width="300px" DataValueField ="contestID" runat="server">
    </asp:DropDownList>
</td></tr>--%>
<tr><td align="left">Year</td><td align="left" style="width: 77px">
    <asp:DropDownList Width="150px" ID="ddlYear" AutoPostBack="true" runat="server">
    </asp:DropDownList></td></tr>
     <tr><td align="left">Event</td><td align="left" 
             style="width: 77px">
    <asp:DropDownList Width="150px" ID="ddlContest" AutoPostBack="true" runat="server">
        <asp:ListItem Value = "1">Finals</asp:ListItem>
        <asp:ListItem Value = "2" Selected="True">Regionals</asp:ListItem>
    </asp:DropDownList></td></tr>  
<tr><td align="left">Chapter</td><td align="left" 
        style="width: 77px">
    <asp:DropDownList Width="150px" ID="ddlChapter" AutoPostBack="true"  runat="server" DataTextField="Chaptercode" DataValueField="ChapterID">
    </asp:DropDownList></td></tr>
      
  <tr><td align="left">Contest </td><td align="left" 
          style="width: 77px"><asp:DropDownList  Width="149px" Enabled="false" AutoPostBack="true" ID="ddlProductGroup" DataTextField="Name"   DataValueField="ProductGroupCode" runat="server"  EnableTheming="True"></asp:DropDownList></td></tr>
<tr><td align="left">Level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
    <td align="left" style="width: 77px"><asp:DropDownList  Width="150px" Enabled="false" AutoPostBack="true" ID="ddlProduct" DataTextField="Name" DataValueField="ContestID" runat="server"></asp:DropDownList></td></tr>
    
<tr runat="server" id="TrPhase" visible ="False">
    <td align ="left"
style="height: 8px" >Type of Data</td>
    <td align="left" 
style="width: 77px; height: 8px">
        <asp:DropDownList ID="ddlPhase" AutoPostBack="True" runat="server" style="margin-left: 0px; height: 22px;" 
            Width="149px">
            <asp:ListItem Value="1">Phase 1 - Contestant List Only</asp:ListItem>
            <asp:ListItem Value="2">Phase 2 - Contestant List Only</asp:ListItem>
            <asp:ListItem Value="3">Phase 2 - Phase 1 Attendance Only</asp:ListItem>
            <asp:ListItem Value="4">Phase 1 &amp; 2 - Composite Score Data </asp:ListItem>
            <asp:ListItem Value="9" Enabled="false">Phase 3 � List to Publish</asp:ListItem>
            <asp:ListItem Value="5" Enabled="false">Phase 3 - Score Sheet</asp:ListItem>
            <asp:ListItem Value="6" Enabled="false">Phase 1,2& 3 - Composite Scores</asp:ListItem>
            <asp:ListItem Value="7" Enabled="false">List of Top Contestants</asp:ListItem>
            <asp:ListItem Value="8" Enabled="false">Rank Certificates</asp:ListItem>
        </asp:DropDownList>
    </td></tr>

<tr runat="server" id="TrRoom" visible ="false ">
    <td align ="left"
style="height: 8px" >Room Number</td>
    <td align="left" 
style="width: 77px; height: 8px">
        <asp:DropDownList  Width="150px" AutoPostBack ="True" ID="ddlRoom" runat="server" >
          </asp:DropDownList>
    </td></tr>

<tr runat="server" id="TrTopRank" visible ="false">
    <td align ="left" style="height: 8px" >Top Ranks</td>
    <td align="left" style="width: 77px; height: 8px">
        <asp:DropDownList  Width="150px" ID="ddlTopRank" runat="server" AutoPostBack ="True">
            <asp:ListItem Value="3">Top 3</asp:ListItem>
            <asp:ListItem Value="4">Top 4</asp:ListItem>
            <asp:ListItem Value="5">Top 5</asp:ListItem>
            <asp:ListItem Value="6">Top 6</asp:ListItem>
            <asp:ListItem Value="7">Top 7</asp:ListItem>
            <asp:ListItem Value="8">Top 8</asp:ListItem>
            <asp:ListItem Value="9">Top 9</asp:ListItem>
            <asp:ListItem Value="10">Top 10</asp:ListItem>
            <asp:ListItem Value="11">Top 11</asp:ListItem>
            <asp:ListItem Value="12">Top 12</asp:ListItem>
            <asp:ListItem Value="13">Top 13</asp:ListItem>
            <asp:ListItem Value="14">Top 14</asp:ListItem>
            <asp:ListItem Value="15">Top 15</asp:ListItem>
             <asp:ListItem Value="16">Top 16</asp:ListItem>
              <asp:ListItem Value="17">Top 17</asp:ListItem>
               <asp:ListItem Value="18">Top 18</asp:ListItem>
               <asp:ListItem Value="19">Top 19</asp:ListItem>
               <asp:ListItem Value="20">Top 20</asp:ListItem>
        </asp:DropDownList>
    </td>
</tr>
<tr id ="TrConfirmDwnload"  runat="server" visible="false">
  <td class="announcement_text"  colspan="2">
    <br />
    <center><asp:Label ID="lblConfirmDwnload" runat="server" align="center" ForeColor="Red"></asp:Label><br />
           <asp:Button ID="BtnConfirmDwnload" OnClick="BtnConfirmDwnload_Click"  runat="server" Text="Confirm" />&nbsp
           <asp:Button ID="BtnCancelDwnload" runat="server" OnClick ="BtnCancelDwnload_Click" Text="Cancel" />
             <br />
    </center>
  </td>
</tr>

<tr><td align="center" colspan="2">
   <asp:Label ID="lblUploadCondn" runat="server" forecolor="Red" ></asp:Label><br />
    <br />
    <asp:Button ID="BtnDownload" runat="server" 
        Text="Download" Height="25px" style="margin-bottom: 0px" />
    <br />
    <br />
    <asp:Label ID="lbldwError" runat="server" forecolor="Red" ></asp:Label>
</td></tr>


<tr runat ="server" id="trupload" visible="false" ><td align="center" colspan="2"><b> Upload Filled Scoresheet</b></td></tr>
<tr runat="server" id="TrTypeofData1" visible ="False">
    <td align ="left" style="height: 8px" >Type of Data</td>
    <td align="left"  style="width: 77px; height: 8px">
    <asp:DropDownList ID="ddlTypeofData1" AutoPostBack="True" runat="server" style="margin-left: 0px"
         Width="149px">
        <asp:ListItem Value="0">Phase 1 - Attendance Only</asp:ListItem>
        <asp:ListItem Value="1">Phase 1 - Score Data</asp:ListItem>
        <asp:ListItem Value="2">Phase 2 - Score Data</asp:ListItem>
        <asp:ListItem Value="5" Enabled="false">Ranks after Phase 1&amp;2</asp:ListItem>
        <asp:ListItem Value="6" Enabled="false">Phase 3 Score Data</asp:ListItem>
        <asp:ListItem Value="3">Ranks for Certificates</asp:ListItem>
        <asp:ListItem Value="4">Official Scores &amp; Ranks</asp:ListItem>
       
        
    </asp:DropDownList>
</td></tr>

<tr runat="server" id="TrRoom1" visible ="false">
    <td align ="left" style="height: 8px" >Room Number</td>
    <td align="left" 
style="width: 77px; height: 8px">
    <asp:DropDownList  Width="150px" AutoPostBack="true" ID="ddlRoom1" runat="server">
    </asp:DropDownList>
</td></tr>
<tr><td align="center" colspan="2" style="height: 58px">
  <asp:FileUpload ID="FileUpload" Visible="false"  Width="200px" runat="server" />
</td></tr>

<tr><td align="center" colspan="2">
    <asp:Button ID="btnUpload" Visible="false" runat="server" Text="Upload" Height="26px" Width="60px" /><br />
    <asp:Label ForeColor ="Red" ID="lblErr" runat="server" Text =""></asp:Label><br />
      
    <asp:HiddenField
        ID="HdnexecQuery" runat="server" />
    <asp:HiddenField ID="hdnChapterID" runat="server" />
    <asp:HiddenField ID="HdnScoreDetailSQL" runat="server" />
</td></tr>

</table> 
</td>
<td width="25px">&nbsp;&nbsp;
</td>
<td><table runat="server" visible = "false" id="tblMaster" cellpadding ="3" cellspacing = "0" border = "0" align="center">
<tr><td colspan ="2" align="center" class="title04"> Master Scoresheets Templates<br /></td></tr>
<tr><td> </td></tr>
<tr><td align="left">Year</td><td align="left">
    <asp:DropDownList Width="150px" ID="ddlMYear" AutoPostBack="True" runat="server">
    </asp:DropDownList></td></tr>
<%--<tr><td align="left">Chapter</td><td align="left">
    <asp:DropDownList Width="150px" ID="ddlMChapter"  runat="server"  DataTextField="Chaptercode"  DataValueField="ChapterID">
    </asp:DropDownList></td><td align="left"></td></tr>--%>
    <tr><td align="left">Event</td><td align="left">
    <asp:DropDownList Width="150px" ID="ddlMContest" AutoPostBack="true" runat="server">
        <asp:ListItem Value="1">Finals</asp:ListItem>
        <asp:ListItem Value ="2" Selected="True">Regionals</asp:ListItem>
    </asp:DropDownList></td></tr> 
  <tr><td align="left">Contest </td><td align="left"><asp:DropDownList  Width="150px" Enabled="false" ID="ddlMProductGroup"  AutoPostBack="true" DataTextField="Name" DataValueField="ProductGroupCode" runat="server"></asp:DropDownList></td></tr>
<tr><td align="left">Level </td><td align="left"><asp:DropDownList  Width="150px" Enabled="false"  AutoPostBack ="True" ID="ddlMProduct" DataTextField="Name" DataValueField="ProductCode" runat="server"></asp:DropDownList></td></tr>
<tr><td align="left">Score Sheet Type</td><td align="left"><asp:DropDownList  Width="150px" AutoPostBack ="True" Enabled="false" ID="ddlMPhase" runat="server">
    <asp:ListItem Selected="True" Value="0">Global</asp:ListItem>
    <asp:ListItem Value="2">Phase 2</asp:ListItem>
   </asp:DropDownList></td></tr>
<tr><td align="center" colspan="2"><asp:Button ID="BtnMDownload" 
        OnClick ="BtnMDownload_Click" runat="server" Text="Download" Height="26px" /><br />
    <asp:Label Forecolor="Red" ID="lblMdwError" runat="server"></asp:Label>
</td></tr>

<tr><td align="center" colspan="2"><b> Upload Master Scoresheet</b></td></tr>
<tr><td align="center" colspan="2">
    <asp:FileUpload ID="FileUploadMaster" Width="200px" runat="server" />
</td></tr>
<tr><td align="center" colspan="2">
    <asp:Button ID="btnUploadMaster_" OnClick="btnUploadMaster_Click" 
        runat="server" Text="Upload" style="height: 26px" /><br />
    <asp:Label ForeColor ="Red"  ID="LblMasterErr" runat="server"></asp:Label>
    <asp:HiddenField ID="ExamRecNational" runat="server" />
</td></tr>
</table></td></tr>


<tr runat ="server" id="trconfirm"  visible = "false" >
<td class="announcement_text" align="left" colspan="3">
<br />

<center><span class="title04">Upload Score Sheet with Contestant Data</span></center>

    <br />
    <asp:Label ID="lblConfirm" runat="server" align="center"></asp:Label>
<br />

   <br />
 
   
   <center>
    <asp:Button ID="BtnConfirm" OnClick="BtnConfirm_Click" runat="server" Text="Confirm" /> &nbsp;&nbsp;
    <asp:Button ID="BtnCancel" runat="server" OnClick ="BtnCancel_Click" Text="Cancel" /></center>
</td>
</tr>
<tr><td colspan="3"><asp:Label ID="lblWarngMsg" runat="server" ForeColor="#FF6600" ></asp:Label></td></tr>
</table> 
</div>
    
</asp:Content>

