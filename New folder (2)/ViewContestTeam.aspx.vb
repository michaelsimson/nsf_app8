﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class ViewContestTeam
    Inherits System.Web.UI.Page
    Dim dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If

        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            ' GetProductGroup(ddlProductGroup)
            ' GetDate()
            'LoadDisplayTime()
        End If
        ' End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        'ddlPurpose.DataTextField = "Purpose"
        'ddlPurpose.DataValueField = "Purpose"
        ddlPurpose.DataBind()
        ' ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 0
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        'ddlChapter.DataTextField = "ChapterCode"
        'ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlObject.DataSource = ds
        'ddlProductGroup.DataTextField = "ProductGroup"
        'ddlProductGroup.DataValueField = "ProductGroupId"
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, "Select ProductGroup")
        ddlObject.SelectedIndex = 0
    End Sub
    Private Sub GetDate()
        Dim StrDateSQL As String = ""
        StrDateSQL = "Select Distinct  convert(VarChar(10), Date, 101) as Date  From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
        ddlDate.DataSource = ds
        'ddlDate.DataTextField = "Date"
        'ddlDate.DataValueField = "Date"
        ddlDate.DataBind()
        ddlDate.Items.Insert(0, "Select ContestDate")
        ddlDate.SelectedIndex = 0
    End Sub
    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        ddlEvent.SelectedIndex = 0
        ddlChapter.SelectedIndex = 0
        ClearDropDowns()
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            ddlPurpose.SelectedIndex = 0
            ClearDropDowns()
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        lblShowContest.Text = ""
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        'GetProductGroup(ddlProductGroup)
        'GetDate()
        ddlPurpose.SelectedIndex = 0
        ClearDropDowns()
        'LoadBldgID()
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDropDowns()
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Event/Chapter"
            Exit Sub
        End If
        LoadRoomSchData()
    End Sub
    Private Sub LoadRoomSchData()

        Dim SQLRoomsch As String = "Select * from Roomschedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterId=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomsch)
        Try
            GetDate()
            GetProductGroup(ddlProductGroup) '
            'LoadBldgID()
            lblShowContest.Text = ""
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        DGRoomSchedule.Visible = False
        lblShowContest.Text = ""
        LoadProduct(ddlProductGroup, ddlProduct)
        ' LoadPhase()
    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblShowContest.Text = ""
        DGRoomSchedule.Visible = False
        LoadPhase()
    End Sub
    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ddlProductObj As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlObject.SelectedValue & " order by ProductId" 'and BldgId='" & ddlBldgID.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlProductObj.DataSource = ds
        'ddlProductObj.DataTextField = "ProductCode"
        'ddlProductObj.DataValueField = "ProductId"
        ddlProductObj.DataBind()

        ddlProductObj.Items.Insert(0, New ListItem("Select Product ", "0"))
        ddlProductObj.SelectedIndex = 0

    End Sub

    Private Sub LoadPhase()

        Dim StrSQL As String = "Select Distinct Phase from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlPhase.DataSource = ds
        'ddlPhase.DataTextField = "Phase"
        'ddlPhase.DataValueField = "Phase"
        ddlPhase.DataBind()

        ddlPhase.Items.Insert(0, New ListItem("Select Phase ", "0"))
        ddlPhase.SelectedIndex = 0
    End Sub

    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        DGRoomSchedule.Visible = False
        lblShowContest.Text = ""
        ddlRoomNo.Enabled = True
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
     
    End Sub
    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)

        If Request.QueryString("ID") = 1 Then
            Dim SQLRoleRooms = "Select Distinct RoomNumber From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "' and CJMemberId in(" & Session("loginId") & ") Or LTJudMemberId in(" & Session("loginId") & ")"
            Dim dsLog As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleRooms)

            If dsLog.Tables(0).Rows.Count > 0 Then
                ddlRoomNo.DataSource = dsLog
                ddlRoomNo.DataBind()
            End If
        Else

            Dim StrRoomNoSQl As String = "Select Distinct RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

            ddlRoomNo.DataSource = ds
            ddlRoomNo.DataBind()

        End If
        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0
        
    End Sub

    Private Sub LoadBldgID()
        ddlRoomNo.Enabled = True

        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""

        StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)
        ddlBldgID.DataSource = ds
        ddlBldgID.DataTextField = "BldgID"
        ddlBldgID.DataValueField = "BldgID"
        ddlBldgID.DataBind()

        ddlBldgID.Items.Insert(0, "Select BldgID")
        ddlBldgID.SelectedIndex = 0
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        lblShowContest.Text = ""
        DGRoomSchedule.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Or ddlProductGroup.SelectedIndex = 0 Then
            lblShowContest.Text = "Please do all the Selections"
            Exit Sub
        End If
        LoadBldgID()
    End Sub
   
    Private Sub LoadDGRoomSchedule()
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim lblCapcty As Label
        Dim i As Integer = 0
        Dim RoomGCnt, RoomGcntEnd As Integer
        Dim RoleStIndex, RoleEndIndex, RoomGStIndex, RoomGEndindex As Integer

        Try
            Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "' Order by SeqNo"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)

            DGRoomSchedule.DataSource = ds
            DGRoomSchedule.DataBind()
            Dim ds2 As DataSet  'ds1,


            For Each dgItem In DGRoomSchedule.Items
                RoleStIndex = 15
                RoleEndIndex = 23

                RoomGStIndex = 24
                RoomGEndindex = 33

                RoomGCnt = 1
                RoomGcntEnd = 10

                lblCapcty = dgItem.FindControl("lblCapacity")

                Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")
                If ds1.Tables(0).Rows.Count > 0 Then
                    lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")
                Else
                    lblCapcty.Text = ""
                End If

                For dgCol As Integer = RoleStIndex To RoomGEndindex
                    DGRoomSchedule.Columns(dgCol).Visible = True
                Next

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") > 0 Then
                    ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'") ' " & IIf(ddlBldgID.SelectedIndex = 0, "", "  ''''' ") & IIf(ddlRoomNo.SelectedIndex <= 0, "", "
                    If ds2.Tables(0).Rows.Count > 0 Then
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RMcMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RMcMemberID")), dgItem.FindControl("lblRoomMc")) ' lbl_RoomMc)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("PronMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("PronMemberID")), dgItem.FindControl("lblPronouncer")) ' lbl_Pronouncer)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("CJMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("CJMemberID")), dgItem.FindControl("lblChiefJudge")) ' ) 'lbl_ChiefJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("AJMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("AJMemberID")), dgItem.FindControl("lblAssociateJudge")) 'lbl_AssoJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("LTJudMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("LTJudMemberID")), dgItem.FindControl("lblLaptopJudge")) 'lbl_LTJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("DictHMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("DictHMemberID")), dgItem.FindControl("lblDictHandler")) ' lbl_DictH)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID")), dgItem.FindControl("lblGrading")) ' lbl_Grading)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("ProcMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("ProcMemberID")), dgItem.FindControl("lblProctor")) ' lbl_Proctor)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("TimerMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("TimerMemberID")), dgItem.FindControl("lblTimer")) ' lbl_Timer)
                    Else
                        For i = RoleStIndex To RoleEndIndex
                            DGRoomSchedule.Columns(i).Visible = False
                        Next
                    End If
                Else
                    For i = RoleStIndex To RoleEndIndex
                        DGRoomSchedule.Columns(i).Visible = False
                    Next
                End If

                Dim dsRoomG As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomGuideSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")

                If dsRoomG.Tables(0).Rows.Count > 0 Then
                    For j As Integer = RoomGCnt To RoomGcntEnd
                        If Not dsRoomG.Tables(0).Rows(0)("RGMemberID_" & j) Is DBNull.Value Then
                            DisplayName(IIf(dsRoomG.Tables(0).Rows(0)("RGMemberID_" & j) Is DBNull.Value, 0, dsRoomG.Tables(0).Rows(0)("RGMemberID_" & j)), dgItem.FindControl("lblRoomG_" & j))
                        Else
                            DGRoomSchedule.Columns(RoleEndIndex + j).Visible = False
                        End If
                    Next
                Else
                    For j As Integer = RoomGCnt To RoomGcntEnd
                        DGRoomSchedule.Columns(RoleEndIndex + j).Visible = False
                    Next
                End If

                i = i + 1
            Next
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Private Sub DisplayName(ByVal MemberID As Integer, ByVal lblRole As Label)
        If MemberID <> 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select (Title + '.' +FirstName +''+ LastName ) as Name  From IndSpouse Where AutoMemberID =" & MemberID)
            lblRole.Text = ds.Tables(0).Rows(0)("Name")
        End If
    End Sub
    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        lblShowContest.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 0
        ClearDropDowns()

    End Sub
    Private Sub ClearDropDowns()
        DGRoomSchedule.Visible = False
        lblShowContest.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlPhase.Items.Clear()
        ddlDate.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlRoomNo.Items.Clear()
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        If ddlEvent.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlProduct.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Product"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select Phase"
            Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblShowContest.Text = "Please Select RoomNo"
            Exit Sub
        End If
        LoadDGRoomSchedule()
    End Sub

    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged
        lblShowContest.Text = ""
        DGRoomSchedule.Visible = False
    End Sub
   
End Class

