Imports System
Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Script.Serialization


Namespace VRegistration
    Partial Class Login
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents hlinkNewUser As System.Web.UI.WebControls.HyperLink


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            'Put user code to initialize the page here

            Try
                Session("PreviousPage") = ""
                Dim SecureUrl As String
                Dim entryToken As String

                If Not Page.IsPostBack Then
                    If (Not Request.IsSecureConnection And Not ((Request.Url.Host = "localhost") Or (Request.Url.Host = "cs11"))) Then
                        SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                        Response.Redirect(SecureUrl, True)
                    End If
                    ResetSessionVariables()
                    entryToken = Request.QueryString("entry")

                    If (entryToken Is Nothing Or entryToken = "") Then  'see if it is in the session
                        entryToken = Session("entryToken")
                    End If
                    If (entryToken <> Nothing) Then
                        If (entryToken = "P") Or (entryToken = "p") Then
                            Session("entryToken") = "Parent"
                        ElseIf (entryToken = "V") Or (entryToken = "v") Then
                            Session("entryToken") = "Volunteer"
                        ElseIf (entryToken = "D") Or (entryToken = "d") Then
                            Session("entryToken") = "Donor"
                        ElseIf (entryToken = "G") Or (entryToken = "g") Then
                            Session("entryToken") = "Game"
                        ElseIf (entryToken = "W") Or (entryToken = "w") Then
                            Session("entryToken") = "Walkathon_Marathon"
                        ElseIf (entryToken = "WD") Or (entryToken = "wd") Then
                            Session("entryToken") = "WMDonor"
                        ElseIf (entryToken = "S") Or (entryToken = "s") Then
                            Session("entryToken") = "Student"
                        End If
                    Else
                        'tblMissingToken.Visible = True
                        'Return
                        Response.Redirect("Maintest.aspx")
                    End If

                    ViewState("TryCount") = 0
                    tblErrorLogin.Visible = False
                    tblMissingProfile.Visible = False
                End If
                If Session("entryToken").ToString.ToUpper() = "PARENT" Then
                    pnlDonors.Visible = False
                    pnlVolunteers.Visible = False
                    pnlStudent.Visible = False
                    pnlParent.Visible = True
                    PnlGame.Visible = False
                    lbllogin.Text = "E-Mail"
                ElseIf Session("entryToken").ToString.ToUpper() = "DONOR" Then
                    pnlDonors.Visible = True
                    pnlVolunteers.Visible = False
                    pnlStudent.Visible = False
                    pnlParent.Visible = False
                    PnlGame.Visible = False
                    lbllogin.Text = "E-Mail"
                ElseIf Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    pnlDonors.Visible = False
                    pnlVolunteers.Visible = True
                    pnlParent.Visible = False
                    pnlStudent.Visible = False
                    PnlGame.Visible = False
                    lbllogin.Text = "E-Mail"
                ElseIf Session("entryToken").ToString.ToUpper() = "GAME" Then
                    pnlDonors.Visible = False
                    pnlVolunteers.Visible = False
                    pnlStudent.Visible = False
                    PnlGame.Visible = True
                    pnlParent.Visible = False
                    Table2.Visible = False
                    RegularExpressionValidator1.Enabled = False
                    lbllogin.Text = "Login ID"
                ElseIf Session("entryToken").ToString.ToUpper() = "WALKATHON_MARATHON" Or Session("entryToken").ToString.ToUpper() = "WMDONOR" Then
                    pnlDonors.Visible = False
                    pnlVolunteers.Visible = False
                    pnlStudent.Visible = False
                    pnlTHON.Visible = True
                    pnlParent.Visible = False
                    PnlGame.Visible = False
                    lbllogin.Text = "E-Mail"
                ElseIf Session("entryToken").ToString.ToUpper() = "STUDENT" Then
                    pnlDonors.Visible = False
                    pnlVolunteers.Visible = False
                    pnlParent.Visible = False
                    PnlGame.Visible = False
                    pnlTHON.Visible = False
                    pnlStudent.Visible = True
                    Table2.Visible = False
                    lbllogin.Text = "E-Mail"
                End If
            Catch ex As Exception
                'Response.Redirect("Maintest.aspx")
                'Response.Write(ex.ToString())
            End Try

        End Sub

        Private Function IsGoogleCaptchaValid() As Boolean
            Try
                Dim recaptchaResponse As String = Request.Form("g-recaptcha-response")
                Dim Valid As Boolean = False
                If Not String.IsNullOrEmpty(recaptchaResponse) Then
                    Dim request As HttpWebRequest = Net.WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ&response=" & recaptchaResponse)
                    'request.Method = "POST"
                    'request.ContentType = "application/json; charset=utf-8"
                    Dim postData As String = ""

                    ''get a reference to the request-stream, and write the postData to it
                    'Using s As IO.Stream = request.GetRequestStream()
                    '    Using sw As New IO.StreamWriter(s)
                    '        sw.Write(postData)
                    '    End Using
                    'End Using
                    Dim wResponse As WebResponse = request.GetResponse()
                    ''get response-stream, and use a streamReader to read the content
                    Using s As IO.Stream = wResponse.GetResponseStream()
                        Using sr As New IO.StreamReader(s)

                            'decode jsonData with javascript serializer
                            Dim jsonData = sr.ReadToEnd()
                            Dim jss As New JavaScriptSerializer()
                            Dim model As MyObject = jss.Deserialize(Of MyObject)(jsonData)
                            'If jsonData = "{" & vbLf & "  ""success"": true" & vbLf & "}" Then

                            Valid = Convert.ToBoolean(model.success)
                            '    Return True
                            'End If
                        End Using
                    End Using
                End If
                Return Valid
            Catch ex As Exception
                'Dont show the error
                '  lbllogin.Text = ex.ToString
            End Try

            Return False
        End Function

        Public Class MyObject

            Private _success As String
            Public Property success As String
                Get
                    Return _success
                End Get
                Set(value As String)
                    _success = value
                End Set
            End Property
        End Class
        'Public Function Validate() As Boolean
        '    Try
        '        Dim Response As String = Request("g-recaptcha-response")
        '        'Getting Response String Append to Post Method
        '        Dim Valid As Boolean = False
        '        'Request to Google Server
        '        Dim req As HttpWebRequest = WebRequest.Create(" https://www.google.com/recaptcha/api/siteverify?secret=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ&response=" & Response)

        '        'Google recaptcha Response
        '        Dim wResponse As WebResponse = req.GetResponse()
        '        Dim readStream As StreamReader = New StreamReader(wResponse.GetResponseStream())
        '        Dim jsonResponse As String = readStream.ReadToEnd()

        '        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        '        Dim Data As MyObject = js.Deserialize < MyObject > (jsonResponse)
        '            // Deserialize Json
        '            Valid = Convert.ToBoolean(data.success);

        '    Catch ex As Exception

        '    End Try
        'End Function
        'Public Class MyObject
        '    Dim _success As String
        '    Public 
        'private string _success;
        '       public string success
        '       {
        '           get { return _success; }
        '           set { _success = value; }
        '       }


        'End Class



        Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

            'spCaptcha.Visible = False
            'If IsGoogleCaptchaValid() = False Then
            '    spCaptcha.Visible = True
            '    Exit Sub
            'End If

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim userID As String = ""  'Login_master
            Dim autoMemberID As String = ""  'IndSpouse
            Dim volunteerMemberID As String = "" 'Volunteer
            Dim entryToken As String = ""
            If (Not Session("entryToken") Is Nothing) Then
                entryToken = Session("entryToken")
            Else

            End If
            Session("UserID") = txtUserId.Text

            ' if user fails to login for 3 attempts, userid will be locked for 24 hours
            Dim date1 As DateTime = DateTime.Now.AddDays(-1)
            Dim i As Integer
            Dim srr As String = "select Sum(attempt) from login_attempts where userid = '" + txtUserId.Text + "' and login_time < '" + DateTime.Now + "' and login_time > '" + date1 + "' and entrytoken = '" + entryToken.ToString + "'"
            'Response.Write(srr)
            Try
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, srr)
                'If Not DBNull.Value.Equals(ds.Tables(0).Rows(0)(0)) Then
                '    i = ds.Tables(0).Rows(0)(0)
                'End If
            Catch ex As Exception
                'Response.Write(srr)
                'Response.Write(ex.ToString)
            End Try
            If i >= 3 Then
                tblloginlock.Visible = True
                lblloginlock.Text = "Account for userid = " + txtUserId.Text + " is locked, please try after 24 hours or contact nsfgame@gmail.com for more Information."
            Else
                Dim StrSql2 As String = "Delete from login_attempts where userid = '" + txtUserId.Text + "' and entrytoken = '" + entryToken.ToString + "'"
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql2)

                tblErrorLogin.Visible = False

                If ViewState("TryCount") > 2 Then
                    btnLogin.Visible = False
                    tblLogin.Visible = False
                    Session.Abandon()
                End If
                If entryToken.ToString = "Game" Then
                    Session("Childloginid") = txtUserId.Text
                    Session("ChildPswd") = txtPassword.Text

                    Dim strSql As String = "select count(*) from game where ChildLoginID ='" & Session("Childloginid") & "' and ChildPWD ='" & Session("ChildPswd") & "'"
                    Dim strSql1 As String = "select ChildNumber from game where ChildLoginID ='" & Session("Childloginid") & "' and ChildPWD ='" & Session("ChildPswd") & "'"
                    Dim child As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                    Dim GameMemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql)

                    If GameMemberID > 0 Then
                        Session("LoggedIn") = "True"
                        Session("childNumber") = child.Tables(0).Rows(0)(0)
                        Response.Redirect("GameFunctions.aspx")
                    End If
                End If

                If entryToken.ToString = "Student" Then
                    Session("Childloginid") = txtUserId.Text
                    Session("ChildPswd") = txtPassword.Text
                    Dim lid As String = Session("Childloginid").ToString
                    Dim cnt As Integer
                    cnt = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from child where email='" & Session("Childloginid").ToString() & "' and PwD = '" & Session("ChildPswd").ToString() & "'")
                    If cnt > 0 Then
                        Dim StudentID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select ChildNumber  from child where email='" & Session("Childloginid") & "' and PwD = '" & Session("ChildPswd") & "'")
                        autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select MEMBERID  from child where email='" & Session("Childloginid") & "' and PwD = '" & Session("ChildPswd") & "'")
                        If (autoMemberID <> "") Then
                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                        End If
                        Session("LoggedIn") = "True"
                        Session("LoginID") = autoMemberID
                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                        Session("StudentId") = StudentID
                        Response.Redirect("StudentFunctions.aspx")
                        tblInvalidEmail.Visible = False
                    Else
                        Session.Remove("LoggedIn")
                        Session.Remove("LoginID")
                        Session.Remove("LoginEmail")
                        Session.Remove("Childloginid")
                        Session.Remove("ChildPswd")
                        Session.Remove("StudentId")
                        lblstudentloginerror.Text = "Login Attempt failed. Invalid Email and/or password. Please try again. "

                    End If

                End If

                'End If
                'authenticate
                userID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
                ' Response.Write("userID" & userID)
                If (userID <> "") Then
                    Dim emailcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT COUNT(*) FROM IndSpouse where email = '" & Session("UserID") & "' GROUP BY Email HAVING Count(*)>1")
                    If emailcnt > 0 Then
                        Dim INDcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(AutoMemberID) from IndSpouse where email = '" & Session("UserID") & "' AND donortype = 'IND'")
                        Dim Spousecnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(AutoMemberID) 	from IndSpouse where email = '" & Session("UserID") & "' AND donortype = 'SPOUSE'")
                        ' If INDcnt > 1 Or Spousecnt > 1 Then
                        pnlParent.Visible = False
                        pnlVolunteers.Visible = False
                        pnlDonors.Visible = False
                        Table2.Visible = False
                        tblMissingProfile.Visible = False
                        tblInvalidEmail.Visible = True
                        tblLogin.Visible = False
                        tblErrorLogin.Visible = False
                        Dim strsq As String = "Insert into EmailError(Email, IndCount, SpouseCount, CreatedDate,Status,Remarks) Values ('" & Session("UserID") & "'," & INDcnt & "," & Spousecnt & ",'" & DateTime.Now & "','Pending','Email has a duplicate in IndSpouse Table')"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                        SendNSFmail(Session("UserID"))
                        'End If
                    End If
                    If tblInvalidEmail.Visible = False Then
                        If entryToken.ToString <> "" Then
                            Select Case entryToken.ToString.ToUpper
                                Case "PARENT"
                                    ' catch users with no profile info in IndSpouse
                                    autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                                    If (autoMemberID <> "") Then
                                        SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                        'Get the ChapterId 
                                        Session("CustIndChapterID") = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", Session("CustIndID")))
                                        ' silva added for testing
                                        Session("ChapterID") = Session("CustIndChapterID")
                                        ''**SendEmailValidationMail(Session("CustIndID"))
                                        Response.Redirect("UserFunctions.aspx")
                                    Else
                                        'user profile not present in Indspouse
                                        Session("LoggedIn") = "True"
                                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                                        tblMissingProfile.Visible = True
                                        pnlParent.Visible = False
                                        Table2.Visible = False
                                        tblLogin.Visible = False
                                        tblErrorLogin.Visible = False
                                    End If
                                Case "VOLUNTEER"
                                    ' ############### Checking for Volunteers Unique Email  #####################
                                    Dim cnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from IndSpouse where email = '" & Session("userid") & "'")
                                    If cnt = 1 Then
                                        ' catch users with no profile info in IndSpouse
                                        autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                                        'match autoMemberID to volunteer memberID
                                        volunteerMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetVolunteerMemberID", New SqlParameter("@autoMemberID", autoMemberID))

                                        If (volunteerMemberID <> "") Then
                                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                            '*************Added on 13-02-2014 to shows Terms and Conditions Page for every 90 days***********'
                                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from TCVol Where DATEDIFF(DAY,DisplayDate,GETDATE())< 90 and MemberID =" & Session("LoginID")) > 0 Then
                                                Response.Redirect("VolunteerFunctions.aspx")
                                            Else
                                                Response.Redirect("TermsAndConditions_Vol.aspx")
                                            End If
                                            'Response.Redirect("VolunteerFunctions.aspx")
                                        Else
                                            '' Done on August 16, 2012 as per Dr.C's PPT by ferdine
                                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                            '**********Added on 13-02-2014 to shows Terms and Conditions Page for every 90 days**********************'
                                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from TCVol Where DATEDIFF(DAY,DisplayDate,GETDATE())< 90 and MemberID =" & Session("LoginID")) > 0 Then
                                                Response.Redirect("VolunteerFunctions.aspx")
                                            Else
                                                Response.Redirect("TermsAndConditions_Vol.aspx")
                                            End If
                                            'Response.Redirect("VolunteerFunctions.aspx")
                                            ''tblMissingRole.Visible = True
                                            ''Table2.Visible = False
                                            ''pnlVolunteers.Visible = False
                                            ''tblLogin.Visible = False
                                            ''Session.Abandon()
                                        End If
                                    Else
                                        Session("LoggedIn") = "True"
                                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                                        Response.Redirect("VolUniqueEmail.aspx")
                                    End If

                                Case "DONOR"
                                    autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                                    If (autoMemberID <> "") Then
                                        SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                        Response.Redirect("DonorFunctions.aspx")
                                    Else
                                        'user profile not present in Indspouse
                                        Session("LoggedIn") = "True"
                                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                                        tblMissingProfile.Visible = True
                                        pnlDonors.Visible = False
                                        Table2.Visible = False
                                        tblLogin.Visible = False
                                        tblErrorLogin.Visible = False
                                    End If
                                Case "WALKATHON_MARATHON"
                                    autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                                    If (autoMemberID <> "") Then
                                        SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                        Response.Redirect("WalkathonMarathonFunctions.aspx")
                                    Else
                                        'user profile not present in Indspouse
                                        Session("LoggedIn") = "True"
                                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                                        tblMissingProfile.Visible = True
                                        pnlTHON.Visible = False
                                        Table2.Visible = False
                                        tblLogin.Visible = False
                                        tblErrorLogin.Visible = False
                                    End If
                                Case "WMDONOR"
                                    autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                                    If (autoMemberID <> "") Then
                                        SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                        Session("EventID") = 12
                                        Response.Redirect("Don_athonDonorDetails.aspx?id=3")
                                    Else
                                        'user profile not present in Indspouse
                                        Session("LoggedIn") = "True"
                                        Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                                        tblMissingProfile.Visible = True
                                        tblLogin.Visible = False
                                        tblErrorLogin.Visible = False
                                    End If
                            End Select
                        Else
                            tblMissingToken.Visible = True
                        End If
                    End If
                Else
                    Dim str As String = "insert into login_attempts(UserId,attempt,login_time, entrytoken) values ('" + txtUserId.Text + "', 1, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + entryToken.ToString() + " ')"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str)
                    tblErrorLogin.Visible = True

                End If
            End If

        End Sub

        Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
            Response.Redirect("Registration.aspx")
        End Sub

        Private Sub ResetSessionVariables()
            ''Session("ChapterID") = Nothing
            ''Session("ContestsSelected") = Nothing
            ''Session("CustIndID") = Nothing
            ''Session("CustSpouseID") = Nothing
            ''Session("Donation") = Nothing
            ''Session("DonationFor") = Nothing
            ''Session("EvenID") = Nothing
            ''Session("FatherID") = Nothing
            ''Session("LateFee") = Nothing
            ''Session("LoggedIn") = Nothing
            ''Session("LoginChapterID") = Nothing
            ''Session("LoginEmail") = Nothing
            ''Session("LoginEventID") = Nothing
            ''Session("LoginID") = Nothing
            ''Session("LoginRole") = Nothing
            ''Session("LoginTeamLead") = Nothing
            ''Session("LoginTeamMember") = Nothing
            ''Session("LoginZoneID") = Nothing
            ''Session("MealsAmount") = Nothing
            ''Session("MotherID") = Nothing
            ''Session("NavPath") = Nothing
            ''Session("RoleId") = Nothing
            ''Session("sSQL") = Nothing
            ''Session("sSQLORG") = Nothing
            Session.Remove("ChapterID")
            Session.Remove("ContestsSelected")
            Session.Remove("CustIndID")
            Session.Remove("CustSpouseID")
            Session.Remove("Donation")
            Session.Remove("DonationFor")
            Session.Remove("EvenID")
            Session.Remove("FatherID")
            Session.Remove("LateFee")
            Session.Remove("LoggedIn")
            Session.Remove("LoginChapterID")
            Session.Remove("LoginEmail")
            Session.Remove("LoginEventID")
            Session.Remove("LoginID")
            Session.Remove("LoginRole")
            Session.Remove("LoginTeamLead")
            Session.Remove("LoginTeamMember")
            Session.Remove("LoginZoneID")
            Session.Remove("MealsAmount")
            Session.Remove("MotherID")
            Session.Remove("NavPath")
            Session.Remove("RoleId")
            Session.Remove("sSQL")
            Session.Remove("sSQLORG")
        End Sub

        Private Sub SetSessionVariables(ByVal email As Object, ByVal autoMemberId As Integer)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim drIndSpouse As SqlDataReader

            drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndAndSpouseRecords", New SqlParameter("@autoMemberID", autoMemberId))

            Dim donorType As String
            Dim gender As String
            Dim relationshipId As Integer

            Session("LoginID") = autoMemberId
            Session("LoggedIn") = "True"
            Session("LoginEmail") = email
            Session("CustIndID") = Nothing
            Session("FatherID") = Nothing
            Session("MotherID") = Nothing
            Session("CustSpouseID") = Nothing
            gender = Nothing

            If (Session("entryToken") = "Parent" Or Session("entryToken") = "Donor" Or Session("entryToken") = "Student") Then
                If (drIndSpouse.HasRows()) Then ''drIndSpouse will have b oth Ind and Spouse records (if there is any)
                    Do While (drIndSpouse.Read())
                        donorType = drIndSpouse("DonorType")
                        If Not (drIndSpouse("Gender") Is DBNull.Value) Then
                            gender = drIndSpouse("Gender")
                            If (gender <> Nothing And gender.ToLower = "male") Then
                                Session("FatherID") = autoMemberId
                            Else
                                Session("MotherID") = autoMemberId
                            End If
                        End If

                        autoMemberId = drIndSpouse("autoMemberId")
                        relationshipId = drIndSpouse("Relationship")

                        If (donorType <> Nothing And donorType.ToUpper() = "IND") Then
                            Session("CustIndID") = autoMemberId
                        ElseIf (donorType <> Nothing And donorType.ToUpper() = "SPOUSE") Then
                            Session("CustSpouseID") = autoMemberId
                            Session("CustIndID") = relationshipId
                        End If
                    Loop

                End If
                'ElseIf Session("entryToken").ToString().ToUpper = "VOLUNTEER" Then
                '    Session("CustIndID") = Nothing
                '    Session("FatherID") = Nothing
                '    Session("MotherID") = Nothing
                '    Session("CustSpouseID") = Nothing
                '    Dim strSql As String
                '    strSql = "SELECT * FROM VOLUNTEER WHERE "

                '    Session("LoginChapterID") = 
            ElseIf Session("entryToken").ToString.ToUpper() = "WALKATHON_MARATHON" Then
                Session("CustIndID") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & email & "'")
            ElseIf Session("entryToken").ToString.ToUpper() = "WMDONOR" Then
                Session("CustIndID") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & email & "'")
            End If

        End Sub

        Private Function CheckDuplicateReAttempt(ByVal LoginID As String) As Boolean
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim prmArray(2) As SqlParameter

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@Email"
            prmArray(0).Value = Session("LoginEmail")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@RetValue"
            prmArray(1).SqlDbType = SqlDbType.Bit
            prmArray(1).Direction = ParameterDirection.Output


            SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetDuplicateLogin", prmArray)
            Select Case prmArray(1).Value.ToString
                Case "1"
                    Return True
                Case "0"
                    Return False
            End Select
        End Function
        Private Sub SendNSFmail(ByVal user_email As String)
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@northsouth.org" 'nsfcontests@gmail.com
            'sMailTo = "fsilva@redegginfoexpert.com"
            sMailTo = "nsfcontests@northsouth.org"
            sSubject = "Email Error Discovered"
            Dim mm As New MailMessage(SMailFrom, sMailTo)
            mm.Subject = sSubject

            sBody = Server.HtmlEncode("")
            sBody = sBody & "Dear Sir, <br>"
            sBody = sBody & "Ind Records with this email : " & user_email
            sBody = sBody & "<br>Spouse Records with this email : " & user_email
            sBody = sBody & "<br>Please escalate this issue for further investigation."


            mm.Body = sBody
            mm.IsBodyHtml = True

            '(3) Create the SmtpClient object
            Dim client As New SmtpClient()

            '(4) Send the MailMessage (will use the Web.config settings)
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

            'client.Host = host
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            'client.Timeout = 20000


            Try

                client.Send(mm)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try

        End Sub

        'Private Sub SendEmailValidationMail(ByVal AutoMemberID As Integer)
        '    Dim SMailFrom As String
        '    Dim sMailTo As String
        '    Dim sSubject As String
        '    Dim sBody As String
        '    SMailFrom = "nsfcontests@northsouth.org"
        '    sMailTo = "nsfcontests@northsouth.org"
        '    sSubject = "Invalid Email Record"
        '    Dim mm As New MailMessage(SMailFrom, sMailTo)
        '    mm.Subject = sSubject
        '    Dim strSql As String
        '    strSql = "SELECT * FROM IndSpouse WHERE ValidEmailFlag='N' AND AutoMemberID=" & AutoMemberID
        '    Dim drIndDup As SqlDataReader
        '    drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        '    If (drIndDup.Read()) Then
        '        sBody = "First Name: " & drIndDup("FirstName") & vbCrLf
        '        sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
        '        sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
        '        sBody = sBody & "City: " & drIndDup("City") & vbCrLf
        '        sBody = sBody & "State: " & drIndDup("State") & vbCrLf
        '        sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
        '        sBody = sBody & "Email: " & drIndDup("Email") & vbCrLf
        '        sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
        '        sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
        '        sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
        '        sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
        '    Else
        '        Exit Sub
        '    End If
        '    mm.Body = sBody
        '    '(3) Create the SmtpClient object
        '    Dim client As New SmtpClient()
        '    Try
        '        client.Send(mm)
        '    Catch ex As Exception
        '        ' MsgBox(ex.ToString)
        '    End Try
        'End Sub

        Public Sub NSFMsgBox(ByVal Message As String)
            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=""JavaScript"">" & vbCrLf)
            System.Web.HttpContext.Current.Response.Write("alert(""" & Message & """)" & vbCrLf)
            System.Web.HttpContext.Current.Response.Write("</SCRIPT>")
        End Sub

        Protected Sub btnContinue1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue1.Click
            Response.Redirect("http://northsouth.org")
        End Sub
    End Class

End Namespace

