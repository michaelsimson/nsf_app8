﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="PracticeSession.aspx.cs" Inherits="PractisingSessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: left">

        <style type="text/css">
            .ui-datepicker-trigger {
                padding-left: 5px;
                position: relative;
                top: 3px;
            }

            .ui-datepicker {
                font-size: 8pt !important;
            }

            .style8 {
                width: 99px;
            }

            .style10 {
                width: 147px;
            }

            .style12 {
                width: 84px;
                text-align: left;
            }

            .style16 {
                width: 126px;
            }

            .style17 {
                width: 88px;
            }

            .style20 {
                width: 113px;
                text-align: left;
            }

            .style47 {
                width: 100%;
            }

            .style53 {
                width: 179px;
            }

            .style60 {
                width: 68px;
            }

            .style67 {
                width: 15px;
                font-weight: bold;
            }

            .style70 {
                width: 286px;
            }

            .style87 {
            }

            .style90 {
                width: 184px;
            }

            .style95 {
                width: 93px;
            }

            .style97 {
                width: 117px;
            }

            .style98 {
                width: 37px;
            }

            .style99 {
                width: 120px;
                text-align: left;
            }

            .style101 {
                width: 116px;
            }

            .style102 {
                width: 10px;
            }

            .style118 {
                width: 286px;
                text-align: left;
            }

            .style120 {
                width: 15px;
            }

            .style121 {
                width: 63px;
            }

            .style129 {
                width: 133px;
            }

            .style1 {
                width: 100%;
                height: 92px;
            }

            .style158 {
                width: 95px;
            }

            .style160 {
                width: 271px;
                text-align: left;
            }

            .style162 {
                width: 121px;
            }

            .style163 {
                width: 4px;
            }

            .style165 {
            }

            .style167 {
                text-align: left;
            }

            .style176 {
                width: 199px;
                text-align: left;
            }

            .style177 {
                width: 101px;
                text-align: left;
            }

            .style178 {
                width: 95px;
                text-align: left;
            }

            .style185 {
                width: 152px;
            }

            .style186 {
                width: 101px;
            }

            .style187 {
                width: 181px;
                text-align: left;
            }

            .style191 {
                width: 158px;
            }

            .style195 {
                font-weight: bold;
                width: 158px;
            }

            .style197 {
                width: 54px;
                font-weight: bold;
            }

            .style198 {
                width: 54px;
            }

            .style199 {
                text-align: left;
                width: 182px;
            }

            .style200 {
                width: 120px;
            }

            .style201 {
                width: 179px;
                height: 29px;
            }

            .style202 {
                width: 68px;
                height: 29px;
            }

            .style203 {
                width: 15px;
                height: 29px;
            }

            .style204 {
                width: 10px;
                height: 29px;
            }

            .style205 {
                width: 117px;
                height: 29px;
            }

            .style206 {
                height: 29px;
            }

            .style207 {
                width: 286px;
                height: 29px;
            }

            .inpuField {
                border: 1px solid #c6cccd;
                font-size: 14px;
                height: auto;
                margin: 0;
                outline: 0;
                /*padding: 11px;*/
                width: 100%;
                /*background-color: #e8eeef;*/
                color: #8a97a0;
                box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
                margin-bottom: 30px;
            }
        </style>

        <script src="Scripts/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

        <script language="javascript" type="text/javascript">

            $(function (e) {
                var pickerOpts = {
                    dateFormat: $.datepicker.regional['fr']
                };
                $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });
                $("#<%=txtDate.ClientID %>").datepicker({ dateFormat: 'yy-mm-dd' });

            });
            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to cancel this meeting?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }

            function StartMeeting() {

                JoinMeeting();

            }

            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();
                //var win = window.open(url, '_blank');

                //if (win) {
                //    //Browser has allowed it to be opened
                //    win.focus();
                //} else {
                //    //Broswer has blocked it
                //    alert('Please allow popups for this site');
                //}

            }
            //function joinChildMeeting() {
            //    startChildMeeting();
            //}

            function showAlert(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }


            function JoinMeeting() {

                var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

                window.open(url, '_blank');

            }

            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }

        </script>
    </div>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Practice Session
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>


    <div style="margin-left: auto; margin-right: auto; padding: 10px; width: 100%;">
        <center>
            <h2>Create Practice Session</h2>
        </center>
        <div align="center">
            <asp:Label ID="Label1" runat="server" align="center" Style="color: red;"></asp:Label>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>

        <table id="Table1" runat="server" visible="true" align="center" style="width: 100%;">
            <tr class="ContentSubTitle">
                <td align="left">Event year</td>
                <td align="left">
                    <asp:DropDownList ID="ddYear" runat="server" Width="85px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="left">Event</td>
                <td align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                    </asp:DropDownList></td>
                <td align="left">Chapter</td>

                <td align="left">
                    <asp:DropDownList ID="ddchapter" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                    </asp:DropDownList></td>
                <td align="left">Semester</td>
                <td align="left">
                    <asp:DropDownList ID="ddlPhase" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td align="left">Coach</td>
                <td align="left">
                    <asp:DropDownList ID="ddlCoach" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" runat="server" Width="200px" AutoPostBack="True">
                    </asp:DropDownList>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                </td>
            </tr>
            <tr class="ContentSubTitle">
                <td align="left">Product Group</td>

                <td align="left">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                    </asp:DropDownList></td>

                <td align="left">product</td>
                <td align="left">
                    <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList></td>
                <td align="left">Level</td>

                <td align="left">
                    <asp:DropDownList ID="ddlLevel" runat="server" Width="100px" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td align="left">Session No</td>
                <td align="left">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="75px" AutoPostBack="True">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList></td>

                <td align="left" id="Td3" runat="server" visible="true">Time Zone </td>
                <td runat="server" align="left" id="Td4" visible="true">
                    <asp:DropDownList ID="ddlTimeZone" runat="server" Enabled="false" Width="250px"
                        AutoPostBack="True">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="11" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                        <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                        <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                        <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                    </asp:DropDownList>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                </td>

            </tr>

            <tr class="ContentSubTitle">


                <td align="left">Class Date</td>

                <td align="left">
                    <asp:TextBox ID="txtDate" runat="server" Style="width: 95px;"></asp:TextBox>
                    <br />
                    YYYY-MM-DD</td>

                <td align="left">Begin Time </td>

                <td align="left">


                    <asp:DropDownList ID="ddlMakeupTime" runat="server" Width="110px">
                        <asp:ListItem Value="">Select</asp:ListItem>

                        <asp:ListItem Value="08:00:00">08:00AM</asp:ListItem>
                        <asp:ListItem Value="09:00:00">09:00AM</asp:ListItem>
                        <asp:ListItem Value="10:00:00">10:00AM</asp:ListItem>
                        <asp:ListItem Value="11:00:00">11:00AM</asp:ListItem>
                        <asp:ListItem Value="12:00:00">12:00PM</asp:ListItem>
                        <asp:ListItem Value="13:00:00">01:00PM</asp:ListItem>
                        <asp:ListItem Value="14:00:00">02:00PM</asp:ListItem>
                        <asp:ListItem Value="15:00:00">03:00PM</asp:ListItem>
                        <asp:ListItem Value="16:00:00">04:00PM</asp:ListItem>
                        <asp:ListItem Value="17:00:00">05:00PM</asp:ListItem>
                        <asp:ListItem Value="18:00:00">06:00PM</asp:ListItem>
                        <asp:ListItem Value="19:00:00">07:00PM</asp:ListItem>
                        <asp:ListItem Value="20:00:00">08:00PM</asp:ListItem>
                        <asp:ListItem Value="21:00:00">09:00PM</asp:ListItem>
                        <asp:ListItem Value="22:00:00">10:00PM</asp:ListItem>
                        <asp:ListItem Value="23:00:00">11:00PM</asp:ListItem>
                        <asp:ListItem Value="24:00:00">12:00AM</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left">Duration(Mins)</td>

                <td align="left">
                    <asp:TextBox ID="txtDuration" runat="server" Width="93px"></asp:TextBox>
                    <br />
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />

                </td>
                <td>
                    <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Practice Session" OnClick="btnCreateMeeting_Click" />

                </td>
                <td>
                    <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" OnClick="btnCancelMeeting_Click" /></td>
                <td></td>
            </tr>



        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>




    <%-- <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; padding: 10px;">
        <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
            <td align="left">Event year</td>
            <td align="left">Event</td>
            <td align="left">Chapter</td>
            <td align="left">Semester</td>
            <td align="left">Coach</td>
            <td align="left">Product Group</td>
            <td align="left">product</td>
            <td align="left">Level</td>
            <td align="left">Session No</td>
        </tr>
        <tr class="ContentSubTitle" align="center">
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
        </tr>
        <tr>
            <td align="center" colspan="9"></td>
        </tr>
    </table>--%>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblSuccess" runat="server" Style="color: blue;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>


    <div id="dvSearchPracticeSessions" runat="server" style="border: 1px solid grey; height: 120px; margin: auto; width: 1000px;">
        <div style="margin-top: 10px; margin-left: 20px;">
            <center>
                <h2>Search Practice Session</h2>
            </center>

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlEventyearFilter" AutoPostBack="true" OnSelectedIndexChanged="ddlEventyearFilter_SelectedIndexChanged" runat="server" Width="100">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlPhaseFilter" runat="server" OnSelectedIndexChanged="ddlPhaseFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Fall">Fall</asp:ListItem>
                            <asp:ListItem Value="Spring">Spring</asp:ListItem>
                            <asp:ListItem Value="Summer">Summer</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroupFilter" OnSelectedIndexChanged="ddlProductGroupFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductFilter" OnSelectedIndexChanged="ddlProductFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 36px;">
                        <asp:DropDownList ID="ddlLevelFilter" runat="server" OnSelectedIndexChanged="ddlLevelFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left;">
                    <div style="float: left; margin-left: 60px;">
                        <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Session#"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 13px;">
                        <asp:DropDownList ID="ddlSessionFilter" runat="server" OnSelectedIndexChanged="ddlSessionFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 62px;">
                    <div style="float: left;">
                        <asp:Label ID="Label6" runat="server" Font-Bold="true" Text="Coach"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 54px;">
                        <asp:DropDownList ID="ddlCoachFilter" runat="server" Width="100px" OnSelectedIndexChanged="ddlCoachFilter_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label8" runat="server" Font-Bold="true">Day </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 34px;">
                        <asp:DropDownList ID="ddlDayFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDayFilter_SelectedIndexChanged1" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                            <asp:ListItem Value="Monday">Monday</asp:ListItem>
                            <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                            <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                            <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                            <asp:ListItem Value="Friday">Friday</asp:ListItem>
                            <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;">

                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnClearFilter" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>


    <div align="center" style="font-weight: bold; color: #64A81C;">
        <span id="spnTable1Title" runat="server" visible="false">Table 1 : Practising Sessions</span>


    </div>
    <div style="clear: both;"></div>
    <asp:Button ID="btnAddNewMeeting" runat="server" Visible="false" Text="New Meeting" OnClick="btnAddNewMeeting_Click" Style="float: right;" />
    <div style="clear: both;"></div>
    <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
        <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                <ItemTemplate>
                    <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                <ItemTemplate>
                    <div style="display: none;">
                        <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                        <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                        <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                        <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                        <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                        <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartDate","{0:MM-dd-yyyy}") %>'>'></asp:Label>
                        <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>

                        <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'>'></asp:Label>
                        <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'>'></asp:Label>
                        <asp:Label ID="lblHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'>'></asp:Label>


                    </div>
                    <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                    <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="DeleteMeeting" />
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Ser#
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSRNO" runat="server"
                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
            <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
            <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
            <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>

            <asp:TemplateField HeaderText="Coach">

                <ItemTemplate>

                    <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
            <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
            <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


            <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
            <asp:BoundField DataField="StartDate" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
            <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
            <asp:TemplateField HeaderText="Begin Time">

                <ItemTemplate>
                    <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Time" Visible="false">

                <ItemTemplate>
                    <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
            <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
            <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
            <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


            <asp:TemplateField HeaderText="meeting URL">

                <ItemTemplate>

                    <div style="display: none;">
                        <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>

                        <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'></asp:Label>
                        <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                        <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                    </div>
                    <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="VRoom" HeaderText="Vroom"></asp:BoundField>
        </Columns>

        <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

        <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
    </asp:GridView>

    <div style="clear: both; margin-bottom: 10px;"></div>

    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnChildID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
    <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />

    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="HdnLevel" value="0" runat="server" />

    <input type="hidden" id="hdnCoachname" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />

    <input type="hidden" id="hdnZoomURL" value="0" runat="server" />
    <input type="hidden" id="hdnHostID" value="0" runat="server" />
</asp:Content>
