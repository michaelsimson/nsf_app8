<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdProd.aspx.vb" Inherits="AddUpdProd" title="Add/Update Product" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <asp:hyperlink id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink><br />
  <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004"><tr><td align = "center"> 
        <center>
    <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
        <tr>
            <td align="center" colspan="3">
                <h2>
                ADD/UPDATE PRODUCT</h2>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
        </tr>
         <tr>
            <td align="left">
                Event Code</td>
            <td>
            </td>
            <td align="left"><asp:DropDownList ID="ddlEvent" runat="server" Width="155px" Height="23px" AutoPostBack="True">
                <asp:ListItem Selected="True">Select Event</asp:ListItem>
    </asp:DropDownList>
            </td>
        </tr>
    <tr><td align = "left">
        Product Group</td><td></td><td align="left"><asp:DropDownList ID="ddlProd" runat="server" Width="155px" Height="23px" Enabled="False">
            <asp:ListItem Selected="True">Select Product Group</asp:ListItem>
    </asp:DropDownList></td>
    </tr>    
       
        <tr>
            <td align="left">
                Product Code</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtProdCode" runat="server" Width="150px" Height="18px"></asp:TextBox>&nbsp;
                </td>
        </tr>
        <tr>
            <td align="left">
                Product Name</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtProdName" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                Status</td>
            <td>
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlStatus" runat="server" Width="155px" Height="23px">
                    <asp:ListItem Value="O">Open</asp:ListItem>
                    <asp:ListItem Value="N">Not Open</asp:ListItem>
                    <asp:ListItem Selected="True">Select Status</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="panel3"   Visible="False">

    <center> <asp:Label ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>    </center>
     <br />
     <br />

    <asp:GridView ID="GridView1" runat="server" DataKeyNames="ProductId" AutoGenerateColumns="False"  OnRowCommand="GridView1_RowCommand" >
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="ProductId"  HeaderText="ProductId" />
            <asp:BoundField DataField="ProductCode" HeaderText="Product Code"/>
            <asp:BoundField DataField="Name" HeaderText="Product Name"  ItemStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId"/>
             <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"/>
              <asp:BoundField DataField="EventId" HeaderText="EventId"/>            
            <asp:BoundField DataField="EventCode" HeaderText="EventCode" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="CreateDate" HeaderText="Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
            <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By"/>           
        </Columns>
    
    </asp:GridView>


</asp:Panel>
    </center>
    </td></tr></table>
</asp:Content>
