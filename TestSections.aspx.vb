﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports VRegistration
Partial Class TestSections
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblValText.Text = ""
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then

            LoadYear()
            loadPhase()
            LoadProductGroup()
            LoadDropdown(ddlSetNo, 40)
            'LoadDropdown(ddlWeekID, 40)
            LoadDropdown(ddlSections, 10)
            ddlSections.SelectedValue = 1
            LoadPrdStuff()

            ' LoadGrid_CoachPapers()

        End If
    End Sub

    Public Sub LoadPrdStuff()
        'ReqFieldValTimelt.EnableClientScript = False
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "30") Then
            LoadProductGroup()
            Dim sqltext As String = "    select distinct P.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " & ddlEventYear.SelectedValue & " and cs.Accepted = 'Y' and cs.Semester='" & ddlSemester.SelectedValue & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, sqltext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlProduct.DataSource = ds
                    ddlProduct.DataBind()
                    If ddlProduct.Items.Count > 1 Then
                        ddlProduct.Items.Insert(0, New ListItem("Select Product"))
                        ddlProduct.Items(0).Selected = True
                        ddlProduct.Enabled = True

                    End If
                End If
            End If

        ElseIf Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                'more than one 
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup()
            Else
                'only one
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                If ds.Tables(0).Rows.Count > 0 Then
                    prd = ds.Tables(0).Rows(0)(1).ToString()
                    Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                    lblPrd.Text = prd
                    lblPrdGrp.Text = Prdgrp
                End If
                LoadProductGroup()
            End If
        End If
    End Sub
    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 8
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))


        Next
        ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub

    Private Sub LoadSectionNumber()
        For i As Integer = 0 To 5
            ddlSectionNo.Items.Insert(0, i + 1)
        Next
    End Sub

    Private Sub LoadProductGroup()
        ddlProduct.Items.Clear()
        Dim strSql As String = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y')  WHERE PG.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND PG.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & " order by PG.ProductGroupID"
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlProductGroup.DataSource = drproductgroup
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count < 1 Then
            lblErr.Text = "No Product is opened."
        ElseIf ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Items.Insert(0, New ListItem("Select", "0"))
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        Else
            ddlProductGroup.Enabled = False
            LoadProductID()
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        End If
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try
                strSql = "Select distinct P.ProductID, P.Name from Product  P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y') where P.EventID=" & ddlEvent.SelectedValue & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " order by P.ProductID"
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product"))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    ' LoadGrid_CoachPapers()
                    'LoadGrid_TestSections(True)
                End If
            Catch ex As Exception
                lblErr.Text = ex.ToString
            End Try
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProduct.Items.Clear()
        LoadProductID()
        LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        '  LoadGrid_CoachPapers()
    End Sub

    Protected Sub LoadDropdown(ByVal ddlObject As DropDownList, ByVal MaxLimit As Integer)
        ddlObject.Items.Clear()
        For i As Integer = 0 To MaxLimit - 1
            ddlObject.Items.Insert(i, Convert.ToString(i + 1))
        Next
        If ddlObject.Items.Count > 1 Then
            ddlObject.Items.Insert(0, "Select")
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblError.Text = ""
            lblErr.Text = ""
            'ReqFieldValTimelt.EnableClientScript = True
            'ReqFieldValTimelt.Enabled = True
            If ddlSectionNo.Items.Count > 1 And ddlSectionNo.SelectedIndex = 0 Then
                lblErr.Text = "Please Select SectionNo"
            ElseIf ddlSubject.Enabled = True And ddlSubject.Items.Count > 1 And ddlSubject.SelectedIndex = 0 Then
                lblErr.Text = "Please Select Subject"
            ElseIf txtTimeLimit.Text = "" Then
                lblErr.Text = "Please Select Section Time Limit"
            ElseIf ddlQuestionType.SelectedIndex = 0 Then
                lblErr.Text = "Please Select Question Type"
            ElseIf ddlQuestionType.SelectedValue = "RadioButton" And ddlNoOfChoices.SelectedIndex = 0 Then
                lblErr.Text = "Please Select Number Of Choices"
            ElseIf txtNoOfQues.Text = "" Then
                lblErr.Text = "Please Select # of Questions"
            ElseIf txtQuesNoFrom.Text = "" Then
                lblErr.Text = "Please Select Question Number From"
            ElseIf txtQuesNoTo.Text = "" Then
                lblErr.Text = "Please Select Question Number To"
            ElseIf Convert.ToInt32(txtQuesNoTo.Text) < Convert.ToInt32(txtQuesNoFrom.Text) Then
                lblErr.Text = " Question Number To must be greater than QuestionNumber From"
            ElseIf Convert.ToInt32(txtQuesNoTo.Text) > Convert.ToInt32(txtNoOfQues.Text) Or Convert.ToInt32(txtQuesNoFrom.Text) > Convert.ToInt32(txtNoOfQues.Text) Then
                lblErr.Text = " Question Number From /Question Number To cannot be greater than the Number of Questions"
            Else
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select NumberOfQuestions,QuestionNumberFrom,QuestionNumberto,QuestionType From TestSetUpSections Where CoachPaperID=" & txtTestNumber.Text & " and SectionNumber=" & ddlSectionNo.SelectedValue & " order by NumberOfQuestions desc")
                If ds.Tables(0).Rows.Count > 0 Then
                    If btnAdd.Text = "Add" Then
                        If ds.Tables(0).Rows(0)("NumberOfQuestions") > 0 And Convert.ToInt32(txtNoOfQues.Text) <> ds.Tables(0).Rows(0)("NumberOfQuestions") Then
                            lblErr.Text = " Number of Questions must be the same in all rows of the same section/paper"
                            Exit Sub
                        End If
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(i)("QuestionNumberFrom") <= Convert.ToInt32(txtQuesNoFrom.Text) And ds.Tables(0).Rows(i)("QuestionNumberTo") >= Convert.ToInt32(txtQuesNoTo.Text) Then
                                lblErr.Text = "QuestionNumbers already exists for the same section/paper"
                            ElseIf ds.Tables(0).Rows(i)("QuestionNumberFrom") = Convert.ToInt32(txtQuesNoFrom.Text) Or ds.Tables(0).Rows(i)("QuestionNumberTo") = Convert.ToInt32(txtQuesNoTo.Text) Then
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo already exists for the same section/paper"
                            ElseIf ds.Tables(0).Rows(i)("QuestionNumberFrom") = Convert.ToInt32(txtQuesNoTo.Text) Or ds.Tables(0).Rows(i)("QuestionNumberTo") = Convert.ToInt32(txtQuesNoFrom.Text) Then
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo mismatch exists for the same section/paper"
                            End If
                        Next
                        'ElseIf btnAdd.Text = "Update" Then
                    End If
                End If

            End If
            If lblErr.Text <> "" Then
                Exit Sub
            End If

            Dim StrInsert As String = ""
            Dim StrUpdate As String = ""
            Dim AddFlag As Boolean = False

            StrInsert = " Insert into TestSetUpSections (CoachPaperID,SectionNumber,Level,Subject,EventYear,SectionTimeLimit,NumberOfQuestions,QuestionType,NumberOfChoices,Penalty,QuestionNumberFrom,QuestionNumberTo,CreatedBy,CreateDate) Values "
            StrInsert = StrInsert & "(" & txtTestNumber.Text & "," & ddlSectionNo.SelectedValue & "," & IIf(ddlLevel1.Items.Count <= 0 Or ddlLevel1.SelectedItem.Text = "Select", "NULL", "'" & ddlLevel1.SelectedItem.Text & "'") & "," & IIf(ddlSubject.Items.Count <= 0, "NULL", "'" & ddlSubject.SelectedValue & "'") & "," & ddlEventYear.SelectedValue & "," & txtTimeLimit.Text & "," & txtNoOfQues.Text & ",'" & ddlQuestionType.SelectedValue & "'," & IIf(ddlNoOfChoices.Enabled = True, ddlNoOfChoices.SelectedValue, "Null") & ",'" & ddlPenalty.SelectedValue & "'," & txtQuesNoFrom.Text & "," & txtQuesNoTo.Text & "," & Session("LoginID") & ",GETDATE()" & ")"

            StrUpdate = " Update TestSetUpSections set CoachPaperID=" & txtTestNumber.Text & ",SectionNumber=" & ddlSectionNo.SelectedValue & ",Subject=" & IIf(ddlSubject.Items.Count <= 0, "NULL", "'" & ddlSubject.SelectedValue & "'") & ",EventYear=" & ddlEventYear.SelectedValue & ",SectionTimeLimit=" & txtTimeLimit.Text & ",NumberOfQuestions=" & txtNoOfQues.Text & ",QuestionType='" & ddlQuestionType.SelectedValue & "',Penalty='" & ddlPenalty.SelectedValue & "',NumberOfChoices=" & IIf(ddlNoOfChoices.Enabled = True, ddlNoOfChoices.SelectedValue, "Null") & " ,QuestionNumberFrom=" & txtQuesNoFrom.Text & ",QuestionNumberTo=" & txtQuesNoTo.Text & ",ModifiedBy=" & Session("LoginID") & ",ModifyDate=GETDATE() "
            StrUpdate = StrUpdate & " Where EventYear=" & ddlEventYear.SelectedValue & " and TestSetUpSectionsID=" & hdnTestSetUpSectionsId.Value & " and CoachPaperID=" & txtTestNumber.Text & " and SectionNumber=" & ddlSectionNo.SelectedValue & IIf(ddlLevel1.Items.Count <= 0 Or ddlLevel1.SelectedItem.Text = "Select", "", " and Level='" & ddlLevel1.SelectedItem.Text & "'") '& " and QuestionNumberFrom =" & txtQuesNoFrom.Text & " and QuestionNumberTo=" & txtQuesNoTo.Text

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID=" & txtTestNumber.Text & " and SectionNumber=" & ddlSectionNo.SelectedValue & IIf(ddlLevel1.Items.Count > 0, " and Level='" & ddlLevel1.SelectedItem.Text & "'", "") & " and QuestionNumberFrom =" & txtQuesNoFrom.Text & " and QuestionNumberTo=" & txtQuesNoTo.Text) = 0 And btnAdd.Text = "Add" Then
                If ddlSectionNo.Items.Count > 1 And ddlSectionNo.SelectedValue > 1 Then
                    For i As Integer = 1 To ddlSectionNo.SelectedValue - 1
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID=" & txtTestNumber.Text & " and SectionNumber=" & i & IIf(ddlLevel1.Items.Count > 0, " and Level='" & ddlLevel1.SelectedItem.Text & "'", "")) = 0 Then '& " and QuestionNumberFrom =" & txtQuesNoFrom.Text & " and QuestionNumberTo=" & txtQuesNoTo.Text) = 0 Then 'And btnAdd.Text = "Add" Then
                            AddFlag = True
                        End If
                    Next
                    If AddFlag = True Then
                        lblErr.Text = "Please add the Sections in Sequential Order."
                        Exit Sub
                    End If
                End If
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrInsert) > 0 Then
                    lblErr.Text = "Inserted Successfully"
                    LoadGrid_TestSections(txtTestNumber.Text)
                End If
            ElseIf btnAdd.Text = "Update" Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate) > 0 Then
                    lblErr.Text = "Updated Successfully"
                    LoadGrid_TestSections(txtTestNumber.Text)
                    btnAdd.Text = "Add"
                End If
            Else
                lblErr.Text = "Data already exists.Please test the Test Sections data."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'LoadGrid_TestSections()
    End Sub

    Private Sub LoadGrid_TestSections(ByVal CoachPaperId As Integer)
        lblError.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" & CoachPaperId & " and EventYear=" & ddlEventYear.SelectedValue & " order by SectionNumber,QuestionNumberFrom") 'txtTestNumber.Text & "") ' and SectionNumber=" & ddlSectionNo.SelectedValue & "")

        If ds.Tables(0).Rows.Count > 0 Then

            tblDGTestSection.Visible = True
            DGTestSection.Visible = True
            lblTestSection.Text = "Test SetUp Sections"
            DGTestSection.DataSource = ds
            DGTestSection.DataBind()
        Else
            lblError.Text = "No records exists in the Test Setup Sections table."
            tblDGTestSection.Visible = False
            DGTestSection.Visible = False
        End If

    End Sub

    Public Sub LoadGrid_CoachPapers()
        lblError.Text = ""
        Dim StrWhereCndn As String = ""

        StrWhereCndn = StrWhereCndn & "EventYear=" & ddlEventYear.SelectedValue

        StrWhereCndn = StrWhereCndn & " and Semester='" & ddlSemester.SelectedValue & "'"

        If ddlPaperType.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and PaperType= '" & ddlPaperType.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID= " & ddlProductGroup.SelectedValue & ""
        End If
        If ddlProduct.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductID=" & ddlProduct.SelectedValue & ""
        End If
        If ddlLevel.Enabled = True And ddlLevel.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and Level='" & ddlLevel.SelectedItem.Text & "'"
        End If
        If ddlSetNo.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and SetNum=" & ddlSetNo.SelectedValue & ""
        End If
        If ddlWeekID.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " And WeekId = " & ddlWeekID.SelectedItem.Text & ""
        End If
        If ddlSections.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & "  And Sections = " & ddlSections.SelectedValue & ""
        End If

        If lblPrdGrp.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID in (" & lblPrdGrp.Text & ")"
        End If
        If lblPrd.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductID in (" & lblPrd.Text & ")"
        End If
        StrWhereCndn = StrWhereCndn & " and DocType in ('Q')"


        Dim cmdCount As String = "Select count(CoachPaperID) from CoachPapers Where " & StrWhereCndn & ""
        Dim coachPaperIdCount As Integer = 0
        coachPaperIdCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdCount)

        Dim cmdtext As String = "Select Case when C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ") then 'Yes' else 'No' End as TestSetup,C.* From CoachPapers C Where C.CoachPaperID in (Select Top " & coachPaperIdCount & " CoachPaperID from CoachPapers Where " & StrWhereCndn & " order by CreateDate desc) order by C.CoachPaperID asc "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdtext)

        If ds.Tables(0).Rows.Count > 0 Then
            tblDGCoachPaper.Visible = True
            DGCoachPapers.Visible = True
            DGCoachPapers.DataSource = ds
            DGCoachPapers.DataBind()
            lblCoachPaper.Text = "Coach Papers"

            'TrCopy.Visible = True
            FillCopyCoachPapers()
        Else
            lblCoachPaper.Text = "No Matching Records exist in Coach Papers."
            tblDGCoachPaper.Visible = True
            DGCoachPapers.Visible = False
            tblTestSection.Visible = False
            tblDGTestSection.Visible = False
            '  TrCopy.Visible = False
            ' tblCopy.Visible = False
        End If

    End Sub

    Sub FillCopyCoachPapers()
        Dim StrWhereCndn As String = ""
        StrWhereCndn = StrWhereCndn & "EventYear=" & ddlEventYear.SelectedValue

        If ddlPaperType.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and PaperType= '" & ddlPaperType.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID= " & ddlProductGroup.SelectedValue & ""
        End If
        If ddlProduct.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductID=" & ddlProduct.SelectedValue & ""
        End If
        If ddlLevel.Enabled = True And ddlLevel.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and Level='" & ddlLevel.SelectedItem.Text & "'"
        End If
        If ddlSetNo.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and SetNum=" & ddlSetNo.SelectedValue & ""
        End If
        If ddlWeekID.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " And WeekId = " & ddlWeekID.SelectedItem.Text & ""
        End If
        If ddlSections.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & "  And Sections = " & ddlSections.SelectedValue & ""
        End If

        If lblPrdGrp.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID in (" & lblPrdGrp.Text & ")"
        End If
        If lblPrd.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductID in (" & lblPrd.Text & ")"
        End If
        StrWhereCndn = StrWhereCndn & " and DocType in ('Q')"

        ddlCopyFrom.Items.Clear()
        ddlCopyFrom.Items.Insert(0, New ListItem("Select PaperID", 0))
        ddlCopyFrom.Enabled = False
        ddlCopyTo.Items.Clear()
        ddlCopyTo.Items.Insert(0, New ListItem("Select PaperID", 0))
        ddlCopyTo.Enabled = False
        Dim dsCopyfrom As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select Distinct C.CoachPaperID From CoachPapers C Where C.CoachPaperID in (Select Top 50 CoachPaperID from CoachPapers Where " & StrWhereCndn & " order by CreateDate desc)  and C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ") order by C.CoachPaperID asc ")
        If dsCopyfrom.Tables(0).Rows.Count > 0 Then
            ddlCopyFrom.Enabled = True
            ddlCopyFrom.DataSource = dsCopyfrom
            ddlCopyFrom.DataBind()
            If ddlCopyFrom.Items.Count > 1 Then
                '                ddlCopyFrom.Items.Insert(0, New ListItem("Select PaperID", 0))
            ElseIf ddlCopyFrom.Items.Count = 1 Then
                LoadCopyToPapers()
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub
    Private Sub clear()
        lblErr.Text = ""
        lblError.Text = ""
        If ddlSectionNo.Items.Count > 0 Then
            ddlSectionNo.Enabled = True
            ddlSectionNo.SelectedIndex = 0
        End If
        ' ddlSubject.SelectedIndex = 0
        ddlQuestionType.SelectedIndex = 0
        ddlNoOfChoices.Enabled = False
        txtTimeLimit.Text = ""
        txtNoOfQues.Text = ""
        txtQuesNoFrom.Text = ""
        txtQuesNoTo.Text = ""
        btnAdd.Text = "Add"
        For i As Integer = 0 To DGTestSection.Items.Count - 1
            DGTestSection.Items(i).BackColor = Color.White
        Next
    End Sub

    Protected Sub DGTestSection_Itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        lblErr.Text = ""
        Dim TestSetUpSectionsId As Integer = CInt(e.Item.Cells(2).Text)
        Dim CoachPaperID As Integer = CInt(e.Item.Cells(4).Text)
        Dim Level As String = CStr(e.Item.Cells(5).Text)
        Dim SectionNumber As Integer = CInt(e.Item.Cells(6).Text)
        Dim QuestionNumberFrom As Integer = CInt(e.Item.Cells(13).Text)
        Dim QuestionNumberTo As Integer = CInt(e.Item.Cells(14).Text)


        Dim Flag_DelAnsKey As Boolean = False



        For i As Integer = 0 To DGTestSection.Items.Count - 1
            DGTestSection.Items(i).BackColor = Color.White 'e.Item.BackColor = Color.White
        Next

        If e.CommandName = "Delete" Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From TestAnswerKey Where CoachPaperID = " & CoachPaperID & " and  Level='" & Level & "' and SectionNumber =" & SectionNumber & " and QuestionNumber  between " & QuestionNumberFrom & " and " & QuestionNumberTo) > 0 Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete From TestAnswerKey Where CoachPaperID = " & CoachPaperID & " and  Level='" & Level & "' and SectionNumber =" & SectionNumber & " and QuestionNumber  between " & QuestionNumberFrom & " and " & QuestionNumberTo) > 0 Then
                    Flag_DelAnsKey = True
                Else
                    Flag_DelAnsKey = False
                End If
            Else
                Flag_DelAnsKey = True
            End If
            If Flag_DelAnsKey = True And SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete From TestSetUpSections Where TestSetUpSectionsId=" & TestSetUpSectionsId) > 0 Then
                lblErr.Text = "Deleted Successfully"
                LoadGrid_TestSections(txtTestNumber.Text)
            End If
        ElseIf e.CommandName = "Edit" Then
            btnAdd.Text = "Update"
            e.Item.BackColor = Color.Gainsboro
            hdnTestSetUpSectionsId.Value = TestSetUpSectionsId
            LoadforUpdate(TestSetUpSectionsId)
        Else
            e.Item.BackColor = Color.White
        End If
    End Sub
    Private Sub LoadforUpdate(ByVal TestSetUpSectionsId As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * from TestSetUpSections Where TestSetUpSectionsID =" & TestSetUpSectionsId)
        If ds.Tables(0).Rows.Count > 0 Then
            txtTestNumber.Text = ds.Tables(0).Rows(0)("CoachPaperID")
            ddlSectionNo.SelectedIndex = ddlSectionNo.Items.IndexOf(ddlSectionNo.Items.FindByValue(ds.Tables(0).Rows(0)("SectionNumber")))
            ddlSectionNo.Enabled = False
            If Not ds.Tables(0).Rows(0)("Level") Is DBNull.Value Then
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(ds.Tables(0).Rows(0)("Level")))
                ddlLevel.Enabled = False
            End If
            If ddlSubject.Enabled = True Then
                ddlSubject.SelectedIndex = ddlSubject.Items.IndexOf(ddlSubject.Items.FindByValue(ds.Tables(0).Rows(0)("Subject")))
            End If
            txtTimeLimit.Text = ds.Tables(0).Rows(0)("SectionTimeLimit")
            txtNoOfQues.Text = ds.Tables(0).Rows(0)("NumberOfQuestions")
            ddlQuestionType.SelectedIndex = ddlQuestionType.Items.IndexOf(ddlQuestionType.Items.FindByValue(ds.Tables(0).Rows(0)("Questiontype")))

            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Penalty") Is DBNull.Value, "N", ds.Tables(0).Rows(0)("Penalty"))))

            If ds.Tables(0).Rows(0)("QuestionType") = "RadioButton" Then
                ddlPenalty.Enabled = True
            Else
                ddlPenalty.Enabled = False
            End If
            If ds.Tables(0).Rows(0)("NumberOfChoices") Is DBNull.Value Then
                ddlNoOfChoices.SelectedIndex = -1
                ddlNoOfChoices.Enabled = False
            Else
                LoadDropdown(ddlNoOfChoices, 10)
                ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.IndexOf(ddlNoOfChoices.Items.FindByValue(ds.Tables(0).Rows(0)("NumberOfChoices")))
                ddlNoOfChoices.Enabled = True
            End If

            txtQuesNoFrom.Text = ds.Tables(0).Rows(0)("QuestionNumberFrom")
            txtQuesNoTo.Text = ds.Tables(0).Rows(0)("QuestionNumberTo")

        End If
    End Sub

    Protected Sub DGCoachPapers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim CoachPaperID As Integer = CInt(e.Item.Cells(3).Text)
        Dim sections As Integer = CInt(e.Item.Cells(10).Text)
        Dim ProductCode As String = CStr(e.Item.Cells(5).Text)
        Dim ProductGroupCode As String = CStr(e.Item.Cells(4).Text)

        If e.CommandName = "Select" Then
            DGTestSection.Visible = True
            clear()
            LoadTestSectionSetUp(CoachPaperID, sections, ProductCode)
            LoadLevel(ddlLevel1, ProductGroupCode)
            If ddlLevel1.Items.Count > 0 Then
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(CStr(e.Item.Cells(6).Text)))
                ddlLevel1.Enabled = False
            End If
            LoadGrid_TestSections(CoachPaperID)
            LoadProductGroup()
            Dim PGId As String = CType(e.Item.FindControl("lblProductGroupID"), Label).Text
            ddlProductGroup.SelectedValue = PGId
            LoadProductID()
            Dim PId As String = CType(e.Item.FindControl("lblProductID"), Label).Text
            ddlProduct.SelectedValue = PId
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
            Dim Level As String = CStr(e.Item.Cells(6).Text)
            ddlLevel.SelectedValue = Level
            Dim PaperType As String = CStr(e.Item.Cells(11).Text)
            ddlPaperType.SelectedValue = PaperType
            Dim WeekNo As String = CStr(e.Item.Cells(8).Text)
            ddlWeekID.SelectedValue = WeekNo

            Dim Setnumber As String = CStr(e.Item.Cells(9).Text)
            ddlSetNo.SelectedValue = Setnumber
            'LoadTestSectionSetUp(CoachPaperID, sections, ProductCode)
            LoadLevel(ddlLevel1, ProductGroupCode)
            ddlLevel1.SelectedValue = Level
        End If
    End Sub
    Private Sub LoadTestSectionSetUp(ByVal CoachPaperID As Integer, ByVal sections As Integer, ByVal ProductCode As String)
        tblTestSection.Visible = True
        txtTestNumber.Text = CoachPaperID
        LoadDropdown(ddlSectionNo, sections)
        LoadSubject(ProductCode)
    End Sub
    Private Sub LoadSubject(ByVal ProductCode As String)
        ddlSubject.Enabled = True
        ddlSubject.Items.Clear()
        If ProductCode.Contains("SATM") Or ProductCode.Contains("MB2") Or ProductCode.Contains("MB3") Then
            ddlSubject.Items.Insert(0, New ListItem("Math", "Math"))
            ddlSubject.Enabled = False
        ElseIf ProductCode.Contains("SATE") Then
            ddlSubject.Items.Insert(0, New ListItem("Select", ""))
            ddlSubject.Items.Insert(1, New ListItem("Critical Reading", "Critical Reading"))
            ddlSubject.Items.Insert(2, New ListItem("Writing", "Writing"))
        Else
            ddlSubject.Enabled = False
        End If
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String)
        ddlObject.Items.Clear()
        ddlObject.Enabled = True
        Try


            ddlObject.Items.Clear()
            ddlObject.Enabled = True

            Dim cmdText As String = String.Empty
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" & ddlEventYear.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & ""
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim level As SqlDataReader
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText)
            ddlObject.DataSource = level
            ddlObject.DataValueField = "LevelCode"
            ddlObject.DataTextField = "LevelCode"
            ddlObject.DataBind()
            Dim dt As DataTable = New DataTable()
            dt.Load(level)
            If (ddlObject.Items.Count > 1) Then
                ddlObject.Items.Insert(0, "Select Level")
            Else
                PopulateWeekNo()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlQuestionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuestionType.SelectedIndexChanged
        If ddlQuestionType.SelectedValue = "RadioButton" Then
            ddlNoOfChoices.Enabled = True
            LoadDropdown(ddlNoOfChoices, 10)
            ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.Count - 1

            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("Yes"))
            ddlPenalty.Enabled = True
        Else
            ddlNoOfChoices.Enabled = False

            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("No"))
            ddlPenalty.Enabled = False

        End If
    End Sub


    Protected Sub ddlCopyFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCopyFrom.SelectedIndexChanged
        Try
            ddlCopyTo.Items.Clear()
            ddlCopyTo.Items.Insert(0, New ListItem("Select PaperID", 0))
            ddlCopyTo.Enabled = False

            If ddlCopyFrom.SelectedValue > 0 Then
                LoadCopyToPapers()
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Private Sub LoadCopyToPapers()
        Try
            If ddlCopyFrom.SelectedValue > -1 Then
                Dim StrCopyTo As String = ""
                lblCopyErr.Text = ""
                StrCopyTo = StrCopyTo & " Select Distinct C1.CoachPaperID From CoachPapers C Inner Join CoachPapers C1 on C.EventYear =C1.EventYear and C.ProductGroupId =C1.ProductGroupId and C.ProductId=C1.ProductId and C.WeekId =C1.WeekId and C.SetNum =C1.SetNum and C.Sections =C1.Sections and C.PaperType =C1.PaperType and C.DocType =C1.DocType "
                StrCopyTo = StrCopyTo & " and C.CoachPaperID <> C1.CoachPaperID and C.Level <> C1.Level Where C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ") and C1.CoachPaperId not in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ")"
                StrCopyTo = StrCopyTo & " and C.CoachPaperId = " & ddlCopyFrom.SelectedValue
                Dim dsCopyTo As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, StrCopyTo)
                If dsCopyTo.Tables(0).Rows.Count > 0 Then
                    ddlCopyTo.DataSource = dsCopyTo
                    ddlCopyTo.DataBind()
                    ddlCopyTo.Enabled = True
                    If ddlCopyTo.Items.Count > 1 Then
                        ddlCopyTo.Items.Insert(0, New ListItem("Select PaperID", 0))
                    End If
                Else
                    ddlCopyTo.Enabled = False
                    lblCopyErr.Text = "No eligible coach paper available to copy into."
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        Response.Write("<script language='javascript'>window.open('TestSections_AnsKeyCopy.aspx?ID=AnsKey&From=" & ddlCopyFrom.SelectedValue & "&To=" & ddlCopyTo.SelectedValue & "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ")
    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim PgId As String = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" & ddlProduct.SelectedValue & "").ToString()
        ddlProductGroup.SelectedValue = PgId
        LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        '  LoadGrid_CoachPapers()
    End Sub

    Protected Sub btnRep_Click(sender As Object, e As EventArgs) Handles btnRep.Click

        If (ValdiateTestSections() = "1") Then
            tblCopy.Visible = True
            btnRep.Visible = False
            FillCopyCoachPapers()
        End If

    End Sub

    Protected Sub btnCancelCopy_Click(sender As Object, e As EventArgs) Handles btnCancelCopy.Click
        btnRep.Visible = True
        tblCopy.Visible = False
    End Sub

    Protected Sub ddlWeekID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWeekID.SelectedIndexChanged
        ddlSetNo.SelectedIndex = ddlWeekID.SelectedIndex
        ' LoadGrid_CoachPapers()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLevel.SelectedIndexChanged
        PopulateWeekNo()
        ' LoadGrid_CoachPapers()
    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadProductGroup()
        LoadPrdStuff()
    End Sub
    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2

            ddlSemester.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
    End Sub
    Protected Sub PopulateWeekNo()

        Dim CmdText As String = ""

        CmdText = " select distinct CP.WeekId, cp.Coachpaperid from  CoachPapers CP where CP.eventyear=" & ddlEventYear.SelectedValue & " and CP.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CP.ProductId=" & ddlProduct.SelectedValue & " and CP.level='" & ddlLevel.SelectedValue & "' and CP.Semester='" & ddlSemester.SelectedValue & "' and DOCType='Q' order by WeekId ASC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, CmdText)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlWeekID.DataValueField = "CoachPaperId"
            ddlWeekID.DataTextField = "WeekId"
            ddlWeekID.DataSource = ds
            ddlWeekID.DataBind()

            ddlWeekID.Items.Insert(0, New ListItem("Select", "0"))
        Else
            ddlWeekID.DataSource = ds
            ddlWeekID.DataBind()

        End If
    End Sub

    Protected Sub btnClearFilter_Click(sender As Object, e As EventArgs)

        'ddlProductGroupFilter.SelectedValue = "-1";
        'ddlProductFilter.SelectedValue = "-1";
        'ddlLevelFilter.SelectedValue = "-1";

        ''ddlWeekFilter.SelectedValue = "-1";

        'ddlPaperTypeFilter.SelectedValue = "HW"
        'ddlPhaseFilter.SelectedValue = "Fall"
        'ddlSectionsFilter.SelectedValue = "1"


        'fillYearFiletr()
        'loadProductGroupFilter()
        'loadProductFilter()

        'loadLevelFilter()
        'loadWeekFilter()

        'LoadGrid_CoachPapers()
        'DGTestSection.Visible = False
        LoadTestSections()
        ' LoadGrid_CoachPapers()

    End Sub



    Protected Sub LoadTestSections()
        '    clear()
        If (ValdiateTestSections() = "1") Then

            DGTestSection.Visible = True

            LoadTestSectionSetUp(ddlWeekID.SelectedValue, ddlSections.SelectedValue, ddlProduct.SelectedValue)
            LoadLevel(ddlLevel1, ddlProductGroup.SelectedValue)
            If ddlLevel1.Items.Count > 0 Then
                ' ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(CStr(e.Item.Cells(6).Text)))
                ddlLevel1.Enabled = False
            End If
            LoadGrid_TestSections(ddlWeekID.SelectedValue)

            LoadLevel(ddlLevel1, ddlProductGroup.SelectedValue)
            ddlLevel1.SelectedValue = ddlLevel.SelectedValue
        End If
    End Sub

    Public Function ValdiateTestSections() As String
        Dim retVal As String = "1"
        If ddlEventYear.SelectedValue = "-1" Then
            lblValText.Text = "Please select Event Year"
            retVal = "-1"

        ElseIf ddlSemester.SelectedValue = "0" Then
            lblValText.Text = "Please select Semester"
            retVal = "-1"
        ElseIf ddlProductGroup.SelectedValue = "0" OrElse ddlProductGroup.SelectedValue = "" Then
            lblValText.Text = "Please select Product Group"
            retVal = "-1"
        ElseIf ddlProduct.SelectedValue = "Select Product" OrElse ddlProduct.SelectedValue = "" Then
            lblValText.Text = "Please select Product"
            retVal = "-1"

        ElseIf ddlLevel.SelectedValue = "Select Level" OrElse ddlLevel.SelectedValue = "0" Then
            lblValText.Text = "Please select Level"
            retVal = "-1"

        ElseIf ddlPaperType.SelectedValue = "" OrElse ddlPaperType.SelectedValue = "0" Then
            lblValText.Text = "Please select Papertype"
            retVal = "-1"
        ElseIf ddlWeekID.SelectedValue = "-1" OrElse ddlWeekID.SelectedValue = "0" Then
            lblValText.Text = "Please select WeekId"
            retVal = "-1"
        ElseIf ddlSections.SelectedValue = "Select" OrElse ddlSections.SelectedValue = "0" Then
            lblValText.Text = "Please select Sections"
            retVal = "-1"
        End If

        Return retVal
    End Function

End Class

