﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="RegCountSummarynew.aspx.cs" Inherits="RegCountSummarynew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
            &nbsp&nbsp&nbsp&nbsp
            <asp:LinkButton ID="LBbacktofront" CssClass="btn_02" runat="server" Text="Previous" 
                 Visible="false" 
        >Back to Front Page</asp:LinkButton>
          
    </div>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
               Registration Count Summary
            </div>
             <div align="center" id="IDchapter" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
               Registration Count Summary By Chapter
            </div>
               <div align="center" id="Divevent" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
               Registration Count Summary By Event
            </div>
    <div align="center" id="Divcontest" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
               Registration Count Summary By Contest
            </div>
      <div align="center" id="Divcontestgroup" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
               Registration Count Summary By Contest Group
            </div>
               <br />
              <br />
              <div>  
                  <asp:LinkButton ID="lbprevious" runat="server" Text="Previous" 
                      Visible="false" 
        >Previous</asp:LinkButton></div>
              <div runat="server" id="Divchoice" align="center" style="font-size: 26px; font-weight: bold;">
                  <asp:DropDownList ID="DDchoice" runat="server"  AutoPostBack="True" 
                      onselectedindexchanged="DDchoice_SelectedIndexChanged">
                  </asp:DropDownList>
                
              </div>
              
              
              <asp:Panel ID="Pnldisp" runat="server">
      
        <table style="width: 50%;" border="0">
            <tr>
                <td style="width: 157px">
                    <asp:Label ID="lblevent" runat="server" Text="Event"></asp:Label>
                    <asp:DropDownList ID="ddevent" runat="server" Width="135px" AutoPostBack="True" onselectedindexchanged="ddevent_SelectedIndexChanged" 
                         >
                        <asp:ListItem Value="0">[Select Event]</asp:ListItem>
                    </asp:DropDownList>
                </td>
             
                <td style="width: 157px">
                    <asp:Label ID="lblZone" runat="server" Text="Zone" Width="87px"></asp:Label>
                    <asp:DropDownList ID="ddZone" runat="server" AutoPostBack="True" Width="150px" 
                        onselectedindexchanged="ddZone_SelectedIndexChanged" >
                        <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                    </asp:DropDownList>
              
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblCluster" runat="server" Text="Cluster" Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddCluster" runat="server" Width="150px" 
                        AutoPostBack="True" onselectedindexchanged="ddCluster_SelectedIndexChanged"
                        >
                        <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblchapter" runat="server" Text="Chapter " Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddchapter" runat="server" Width="150px" 
                        AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged"
                        >
                        <asp:ListItem Value="0">[Select Chapter]</asp:ListItem>
                    </asp:DropDownList>
               
                </td>
                <td style="width: 157px" visible="false">
                    <asp:Label ID="Lblcat" runat="server" Text="Expense Category" Width="87px" Visible="false"></asp:Label>
                    <asp:DropDownList ID="DDcategory" runat="server" AutoPostBack="True" Width="175px"
                        Visible="false" >
                        <asp:ListItem Value="0">[Select Category]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblNoyear" runat="server" Text="No of Years" Width="87px"></asp:Label>
                    <asp:DropDownList ID="ddNoyear" runat="server" 
                        Width="175px" >
                        <asp:ListItem Value="0">[Select No of Years]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblyear" runat="server" Text="Year" Width="87px"></asp:Label>
                    <asp:DropDownList ID="DDyear" runat="server" Width="175px">
                        <asp:ListItem Value="0">All</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                 <td style="width: 157px">
                    <asp:Label ID="Lbreport" runat="server" Text="Reports" Width="87px" Visible="false"></asp:Label>
                    <asp:DropDownList ID="DDReport" runat="server" Width="175px" Visible="false">
                      <asp:ListItem Value="0">Select Report</asp:ListItem>
                        <asp:ListItem Value="2">Report by Parent</asp:ListItem>
                        <asp:ListItem Value="1">Report by Coach</asp:ListItem>
                      
                    </asp:DropDownList>
                  
                </td>
                
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td width="200px">
                </td>
                <td width="220px">
                </td>
                <td>
                    
                    <asp:Button ID="Button1" runat="server" Text="Submit" onclick="Button1_Click" 
                         />
                    <asp:Button ID="Button2" runat="server" Text="Export To Excel" 
                        Enabled="false" onclick="Button2_Click"  />
                </td>
            </tr>
            
        </table>
        <br />
           <div>
            <asp:GridView ID="Gridcontestant" runat="server" align="center"  ItemStyle-HorizontalAlign="Right"    
                EnableViewState="true" AllowPaging="true" PageSize="50"
              
               >
                  <PagerSettings PageButtonCount="30" />
                  <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
           <asp:HiddenField ID="Hdchapterid" Value='<%# Bind("chapterID")%>'  runat="server"  />  
               
               
              
   <asp:Button runat="server"  ID="btnview"   Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
            </asp:GridView>
        
        </div>
               <asp:Label ID="lbldisp" runat="server" Visible="false" Text="No Record Found" ForeColor="Red"></asp:Label>
        <asp:Label ID="lblall" runat="server" Text="Enter All The Fields" Visible="false"
            ForeColor="Red"></asp:Label>
        <asp:Label ID="lblMesasenorecord" runat="server" EnableViewState="false" 
            ForeColor="Red"></asp:Label>
             <asp:Label ID="lblNoPermission" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        </asp:Panel>
            <br />
    <asp:TextBox ID="Txthidden" Visible="false" runat="server"></asp:TextBox>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
</asp:Content>

