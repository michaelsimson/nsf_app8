<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FolderMgmt.aspx.vb" Inherits="FolderMgmt" title="Folder Management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  <asp:LinkButton ID="LinkButton1" PostBackUrl="WebPageMgmtMain.aspx" runat="server">Back</asp:LinkButton>
</div>
<div style ="width:800px; text-align : center ">
<table border="0px" width="100%" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align = "center" colspan="2" style="font-weight :bold " > Folder Management</td></tr>
<tr runat ="server" id="trmultiple" visible = "false"><td align = "center" colspan="2" >Select Folder : 
    <asp:DropDownList ID="ddlselectFolder" AutoPostBack="true" OnSelectedIndexChanged=" ddlselectFolder_SelectedIndexChanged" DataValueField="FolderListID" DataTextField="DisplayName" runat="server">
    </asp:DropDownList> <br/>
    <asp:Label ID="lblVErr" runat="server" ></asp:Label>
    </td> </tr> 
<tr visible="true" id="trdrp" runat ="server"><td>
<table border="0px" cellpadding = "3px" cellspacing = "0px">
<tr><td align = "left" > Level 1 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel1"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged" Width="150px" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 2 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel2"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 3 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel3"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 4 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel4"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 5 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel5"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel5_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 6 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel6"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel6_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 7 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel7"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel7_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 8 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel8"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel8_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 9 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel9"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel9_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 10 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel10"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel10_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 11 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel11"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel11_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 12 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel12"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel12_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "center" colspan="2"><asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>&nbsp;</td></tr>
<tr><td align = "left" colspan="2" > 
    <asp:Button ID="btnRename" runat="server" Text="Rename" OnClick ="btnRename_Click"/>&nbsp;&nbsp;
     <asp:Button ID="BtnNew" runat="server" Text="Create New" OnClick ="BtnNew_Click" />&nbsp;&nbsp;
          <asp:Button ID="BtnDelete" runat="server" Text="Delete" OnClick ="BtnDelete_Click" />&nbsp;&nbsp;
               <asp:Button ID="BtnClearAll" runat="server" Text="Clear" OnClick ="BtnClearAll_Click" />
</td></tr>

</table> 
</td><td style ="vertical-align :bottom " >
<table id="tblNew" border="0" runat="server" visible="false" cellpadding = "3" cellspacing = "0">
<tr><td align = "left" colspan="2" > URL :    <asp:Label ID="lblUrl" ForeColor="Blue" runat="server" Text="http://www.northsouth.org/public/"></asp:Label>
    </td></tr>
<tr><td align = "left" > &nbsp;</td><td align="left">&nbsp;</td></tr>
<tr><td align = "left" > Folder Name</td><td align="left"> <asp:TextBox ID="TxtFname" runat="server"></asp:TextBox><asp:Label
        ID="lblrname" Visible = "false" runat="server" Text="Enter new folder Name" ForeColor="Red" ></asp:Label></td></tr>
<tr><td align = "left" > Folder Display Name</td><td align="left"> <asp:TextBox ID="txtDisplayName" runat="server"></asp:TextBox></td></tr>
<tr><td align = "center" colspan="2" >&nbsp;<asp:Label ID="lblErr" runat="server"  ForeColor = "Red"></asp:Label></td></tr>
<tr><td align = "center" colspan="2" >
    <asp:Button ID="btnSubmit" runat="server" Text="Add" OnClick="btnSubmit_Click" /> &nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click"  /></td></tr>

<tr runat="server" visible = "false"><td align = "center" colspan="2" >
    <asp:Label ID="lblLevel" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblParentFID" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblPath" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblRenameFolder" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblRenamePath" runat="server" visible = "false"></asp:Label>
    </td></tr>
</table>
</td></tr>
</table>
    <asp:Label ID="lblNoaccess" runat="server" ForeColor="Red" Font-Bold ="true"  Text="Sorry You have no Folder to access" Visible ="false" ></asp:Label>
</div>
</asp:Content>

