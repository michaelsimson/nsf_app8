using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

#region " Class ManageTestPapers "
public partial class ManageTestPapers : System.Web.UI.Page
{
    #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion

    #region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            gvSortDirection = "ASC";
            gvSortExpression = "ProductCode";


            
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2")))
            {
                GetDropDownChoice(dllfileChoice, true);
                dllfileChoice.Visible = true;
                
               // LoadUploadPanel();
                //LoadSearchAndDownloadPanels(true);
                //Loads the Grid.....
                //GetTestPapers(m_objETP);
                //Panel1.Visible = true;
               // GetProductGroupCodes(ddlProductGroup, true);
               // GetContextYear(ddlContestYear, true);
               // GetWeek(ddlWeek, true);
               // GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
               //// GetWeeks(ddlWeek, true);
               // PopulateContestants(ddlNoOfContestants, true);
               // PopulateSets(ddlSet, true);
               // ddlDocType.Items.Insert(0, new ListItem("[Select Doc Type]", "-1"));
               // GetProductGroupCodes(ddlFlrProductGroup, true);
               // GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
               // GetWeek(ddlFlrWeek, true);
               // PopulateContestants(ddlFlrNoOfContestants, true);
               // PopulateSets(ddlFlrSet, true);
               // gvTestPapers.Columns[12].Visible = false;
               // GetTestPapers(int.Parse(Session["RoleId"].ToString()));
            }
            //else
            //{
            //    Panel1.Visible = false;
            //}
            else if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33")))
            {
                //Panel2.Visible = true;
                //Panel3.Visible = true;
                LoadSearchAndDownloadPanels(false);
                //Loads the Grid.....


                int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), System.DateTime.Now.Year.ToString());
                if (Count == 0)
                {
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "There are no records";
                }
               // dllfileChoice.Visible = false;
                //GetProductGroupCodes(ddlFlrProductGroup, true);
                //GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
                //GetWeek(ddlFlrWeek, true);
                //PopulateContestants(ddlFlrNoOfContestants, true);
                //PopulateSets(ddlFlrSet, true);
                //if ((Session["RoleId"] != null))
                //{
                //    Panel2.Visible = false;
                //    gvTestPapers.Columns[12].Visible = false;
                //    GetTestPapers(int.Parse(Session["RoleId"].ToString()));
                //}
                //else
                //{
                //    GetTestPapers(m_objETP);
                //}
            }
            else
            {
                Panel2.Visible = false;
                Panel3.Visible = false;
                lblNoPermission.Visible = true;
                lblNoPermission.Text = "Sorry, you are not authorized to access sthis page ";
            }
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
           
            LoadSearchAndDownloadPanels(true);
            //Loads the Grid.....
            GetTestPapers(m_objETP);
        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
           
            LoadUploadPanel();
            GetTestPapers(m_objETP);
            
        }
    }

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        int value = 0;
        if (FileUpLoad1.HasFile)
        {
            
            EntityTestPaper objETP = new EntityTestPaper();

            objETP.ProductId = int.Parse(ddlProduct.SelectedValue);
            objETP.ProductCode = ddlProduct.SelectedItem.Text;
            objETP.ProductGroupId = int.Parse(ddlProductGroup.SelectedValue);
            objETP.ProductGroupCode = GetProductGroupCode(ddlProductGroup.SelectedItem.Text);
            objETP.EventCode = GetEventCode(ddlProductGroup.SelectedItem.Text);
            objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
            objETP.SetNum = int.Parse(ddlSet.SelectedValue);
            objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
            objETP.ContestYear = ddlContestYear.SelectedItem.Text;
            objETP.DocType = ddlDocType.SelectedItem.Value;
            objETP.TestFileName = FileUpLoad1.FileName;
            objETP.Description = txtDescription.Text;
            objETP.Password = TxtPassword.Text;
            objETP.CreateDate = System.DateTime.Now;
            objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
           

            if (ValidateFileName(objETP))
            {
                try
                {
                   value =  TestPapers_Insert(objETP);
                    //string ftpPath = String.Format("{0}/{1}",
                    //    ConfigurationSettings.AppSettings["FTPTestPapersPath"],
                    //    FileUpLoad1.FileName);

                    //uploadFileUsingFTP(ftpPath, FileUpLoad1.FileContent);
                   if (value != 0)
                   {
                       SaveFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], FileUpLoad1);

                       lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                       GetTestPapers(m_objETP);
                   }
                   else
                   {
                       lblMessage.Text = "You are inserting duplicate record";
                   }
                }
                catch(Exception err)
                {
                    lblMessage.Text = err.Message;
                }
            }
        }
        else
        {
            lblMessage.Text = "No File Uploaded.";
        }
    }


    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                objETP.TestPaperId = int.Parse(gvTestPapers.Rows[index].Cells[0].Text);
                objETP.TestFileName = gvTestPapers.Rows[index].Cells[8].Text;

                if (e.CommandName == "DeleteTestPaper")
                {
                    DeleteTestPaper(objETP);
                    //lblMessage.Text = DeleteTestPaperFromFTPSite(objETP);
                    DeleteFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                    GetTestPapers(m_objETP);
                }
                else if (e.CommandName == "Download")
                {
                    
                    //DownloadFileFromFTP(objETP);
                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                }
                
            }
        }
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        if (e.SortExpression == "CreateDate")
        {
            if (this.gvSortDirection == "ASC")
            {
                e.SortExpression = "YEAR DESC, MONTH DESC, DAY DESC";
                this.gvSortDirection = "DESC";
            }
            else
            {
                e.SortExpression = "YEAR ASC, MONTH ASC, DAY ASC";
                this.gvSortDirection = "ASC";
            }
        }

        else if (e.SortExpression == "ProductId")
        {
            if(this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {

                this.gvSortDirection = "ASC";
            }
            
            this.gvSortExpression = "ProductId";

        }
        else
        {
            if (this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {
                this.gvSortDirection = "ASC";
            }
        }
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33")))
        {
            GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), System.DateTime.Now.Year.ToString());
        }
        else
        {
            GetTestPapers(m_objETP);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        m_objETP.ProductId = int.Parse(ddlFlrProduct.SelectedValue);
        m_objETP.ProductGroupId = int.Parse(ddlFlrProductGroup.SelectedValue);
        m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedItem.Value);
        m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
        m_objETP.NoOfContestants = int.Parse(ddlFlrNoOfContestants.SelectedValue);
        m_objETP.Description = tbxFlrDescription.Text;
        m_objETP.TestFileName = tbxFlrTestFileName.Text;
        GetTestPapers(m_objETP);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ddlFlrProduct.SelectedIndex = 0;
        ddlFlrProductGroup.SelectedIndex = 0;
        ddlFlrWeek.SelectedIndex = 0;
        ddlFlrSet.SelectedIndex = 0;
        ddlFlrNoOfContestants.SelectedIndex = 0;
        tbxFlrDescription.Text = String.Empty;
        tbxFlrTestFileName.Text = string.Empty;
        GetTestPapers(m_objETP);
    }

    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
    }

    #endregion

    #region " Private Methods - Data Access Layer "
    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Insert";
        SqlParameter[] param = new SqlParameter[17];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@EventCode", objETP.EventCode);
        param[5] = new SqlParameter("@WeekId", objETP.WeekId);
        param[6] = new SqlParameter("@SetNum", objETP.SetNum);
        param[7] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants == -1 ? 0 : objETP.NoOfContestants);
        param[8] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[9] = new SqlParameter("@Description", objETP.Description);
        param[10] = new SqlParameter("@Password", objETP.Password);
        param[11] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[12] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[13] = new SqlParameter("@ModifyDate", objETP.ModifyDate);
        param[14] = new SqlParameter("@ModifiedBy", objETP.ModifiedBy);
        param[15] = new SqlParameter("@DocType", objETP.DocType);
        param[16] = new SqlParameter("@ContestYear", objETP.ContestYear);
       
        value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
        if(value == null)
        {
            return -1;
           // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CurrentYear", System.DateTime.Now.Year);
            DataSet dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetproductGroup", param);
            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {
        object year;
        try
        {
          
            year =  SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct EventYear from Event");
         if (blnCreateEmptyItem)
            {
       
             ddlContestYear.Items.Insert(0, new ListItem(year.ToString()));
           
            }
         }

        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;

        }


    }

    private void GetWeek(DropDownList ddlWeek, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select WeekID, (Convert(varchar(14),SatDay1,106) +  ' - ' + Convert(varchar(14),sunDay2,106)) as Week  from weekCalendar");
            ddlWeek.DataSource = dsWeek;
            ddlWeek.DataTextField = "Week";
            ddlWeek.DataValueField = "WeekID";
            ddlWeek.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlWeek.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlWeek.SelectedIndex = 0;
        }
        catch(SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            if (ProductGroupId != -1)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ProductGroupId", ProductGroupId);
                DataSet dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "Product_GetByProductGroupId", param);
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "ProductCode";
                ddlObject.DataValueField = "ProductId";
                ddlObject.DataBind();
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 5, 10, 15, 20, 25, 30 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select #Of Children]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }


    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "UploadScreen", "DownloadScreen" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;



    }
    private void GetTestPapers(EntityTestPaper objETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[11];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];

        DataView dv = new DataView(dt);
        dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

        gvTestPapers.DataSource = dv;
        gvTestPapers.DataBind();
    }
    private int GetTestPapers(int memberid, int roleid, string contestYear)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetTestPapersforDownload";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@memberid", memberid);
        param[1] = new SqlParameter("@roleid", roleid);
        param[2] = new SqlParameter("@contestyear", contestYear);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];

        DataView dv = new DataView(dt);
        dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

        gvTestPapers.DataSource = dv;
        gvTestPapers.DataBind();

        return dt.Rows.Count;
    }
    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TestPaperId", objDelETP.TestPaperId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }
    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion
    
    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName));
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }

    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {
       
        string ProductGroupCode = EventCodeAndProductGroupCode.Substring(EventCodeAndProductGroupCode.Length - 2);
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = EventCodeAndProductGroupCode.Substring(0, EventCodeAndProductGroupCode.Length - 5);
        return EventCode;
    }


    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {

        Panel1.Visible = false;
        Panel2.Visible = searchPanel;
        Panel3.Visible = true;
        GetProductGroupCodes(ddlFlrProductGroup, true);
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
        GetWeek(ddlFlrWeek, true);
        PopulateContestants(ddlFlrNoOfContestants, true);
        PopulateSets(ddlFlrSet, true);
       // GetTestPapers(int.Parse(Session["RoleId"].ToString()));
    }

    private void LoadUploadPanel()
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
        Panel3.Visible = true;
        GetProductGroupCodes(ddlProductGroup, true);
        GetContextYear(ddlContestYear, true);
        GetWeek(ddlWeek, true);
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
        // GetWeeks(ddlWeek, true);
        PopulateContestants(ddlNoOfContestants, true);
        PopulateSets(ddlSet, true);
        ddlDocType.Items.Insert(0, new ListItem("[Select Doc Type]", "-1"));
        //GetTestPapers(m_objETP);
    }
    #endregion

    #region " Private Methods - Validations "
    private bool ValidateFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode;
        ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        string NoOfContestants = objETP.NoOfContestants.ToString();
        bool IsValidFileName = false;
        // Correct number of contestants if the selected value is "5"
        if (NoOfContestants == "5")
        {
            NoOfContestants = "05";
        }

        // Any product code other than MB, EW, and PS should have number of contestants
        if (ProductGroupCode == "MB" || ProductGroupCode == "EW" || ProductGroupCode == "PS")
        { 
            // No need to do anything here
        }
        else if (objETP.NoOfContestants == -1)
        {
            lblMessage.Text = "No. of Contestants is a required field.";
            return false;         
        }

        // Get the initial part of the file name
        if (objETP.NoOfContestants != -1)
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}_{4}_{5}", objETP.SetNum, System.DateTime.Now.Year, objETP.ProductGroupCode, objETP.ProductCode, objETP.DocType, NoOfContestants); //Set1_SB_05_JSB_2007.pdf or .doc
        }
        else
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}", objETP.SetNum,System.DateTime.Now.Year,objETP.ProductGroupCode,objETP.ProductCode,NoOfContestants); //Set1_SB_JSB_2007.pdf or .doc
        }

        // For MB the file name can have Q or A as suffix
        if (ProductGroupCode == "MB")
        {
            if (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip"))
                //||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_q", ".doc")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".pdf")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".doc")))
            {
                IsValidFileName = true;
            }
        }
        else if ((objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")) || (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")))
        {
            IsValidFileName = true;
        }

        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention.";
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion

   
}
#endregion

#region "Enums" 
public enum ScreenChoice
{
    DownloadScreen,
    UploadScreen

}

#endregion

#region " Class EntityTestPaper (ETP) "
public class EntityTestPaper
{
    public Int32 TestPaperId = -1;
    public Int32 ProductId = -1;
    public string ProductCode = string.Empty;
    public Int32 ProductGroupId = -1;
    public string ProductGroupCode = string.Empty;
    public string EventCode = string.Empty;
    public Int32 WeekId = -1;
    public Int32 SetNum = -1;
    public Int32 NoOfContestants = -1;
    public string TestFileName = string.Empty;
    public string Description = string.Empty;
    public string Password = string.Empty;
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime? ModifyDate = null;
    public Int32? ModifiedBy = null;
    public string ContestYear = string.Empty;
     public string DocType = string.Empty;
    public string WeekOf = string.Empty;
    public string ReceivedBy = string.Empty;
    public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
   
   

    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
                  System.Configuration.ConfigurationManager.AppSettings["FTPTestPapersPath"], TestFileName);
    }
    public EntityTestPaper()
    {
    }

}
#endregion

 