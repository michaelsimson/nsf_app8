﻿Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Net
Imports System.Net.Mail
Imports NorthSouth.DAL
Imports System.Data.SqlClient
Partial Class VolChangeEmail
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
            Server.Transfer("login.aspx")
        End If
        If Not IsPostBack Then
            Dim strSql, strSqlSp As String

            Dim IndEmail As String = ""
            Dim IndOldEmail As String = ""
            Dim SpEmail As String = ""
            Dim SpOldEmail As String = ""
            strSql = "SELECT Email FROM INDDUPLICATE WHERE IndDupID=" & Request.QueryString("DupID")
            strSql = strSql & " AND DonorType IN('IND') AND Status<>'Completed' ORDER BY DonorType"
            IndEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            IndOldEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT ISNull(Email,'') FROM indspouse WHERE AutoMemberID=" & Request.QueryString("ID"))


            strSqlSp = "SELECT Email FROM INDDUPLICATE WHERE DUPRelid=" & Request.QueryString("DupID")
            strSqlSp = strSqlSp & " AND DonorType IN('SPOUSE') AND Status<>'Completed' ORDER BY DonorType"
            SpEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSqlSp)
            SpOldEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT ISNull(Email,'') FROM indspouse WHERE Relationship=" & Request.QueryString("ID"))
            Dim SpEmailCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From INDDUPLICATE where DUPRelid=" & Request.QueryString("DupID") & " and Donortype in ('SPOUSE') AND Status<>'Completed'")

            lblMessage.Text = "The New Ind Email is : " & IndEmail & "<br> The Old Ind Email is : " & IndOldEmail & "<br>" 'Do you need to Change the Email?"     
            lblMessage.Text = lblMessage.Text + "<br />The New Spouse Email is : " & SpEmail & "<br> The Old Spouse Email is : " & SpOldEmail & " <br />"

            If SpEmail = "" And SpEmailCount = 0 Then
                lblMessage.Text = lblMessage.Text + "<br /> Old Spouse Email will not be changed<br />"
            End If
            lblMessage.Text = lblMessage.Text + "<br />  Do you need to Change the Email?"

        End If
    End Sub
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)

        Try
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody

            Dim client As New SmtpClient()
            Dim ok As Boolean = True

            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

            'client.Host = host
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            Try
                client.Send(email)
            Catch e As Exception
                ok = False
            End Try

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim strSql, strSqlSp As String
            Dim sBody As String
            Dim IndEmail As String = ""
            Dim IndOldEmail As String = ""
            Dim SpEmail As String = ""
            Dim SpOldEmail As String = ""
            Dim SpEmailFlag As Boolean = False

            strSql = "SELECT Email FROM INDDUPLICATE WHERE IndDupID=" & Request.QueryString("DupID")
            strSql = strSql & " AND DonorType IN('IND') AND Status<>'Completed' ORDER BY DonorType"
            IndEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            IndOldEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Email FROM indspouse WHERE AutoMemberID=" & Request.QueryString("ID"))

            strSqlSp = "SELECT Email FROM INDDUPLICATE WHERE DUPRelid=" & Request.QueryString("DupID")
            strSqlSp = strSqlSp & " AND DonorType IN('SPOUSE') AND Status<>'Completed' ORDER BY DonorType"
            SpEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSqlSp)
            SpOldEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT ISNull(Email,'') FROM indspouse WHERE Relationship=" & Request.QueryString("ID"))

            Dim SpEmailCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From INDDUPLICATE where DUPRelid=" & Request.QueryString("DupID") & " and Donortype in ('SPOUSE') AND Status<>'Completed'")

            Dim N_EIndCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", IndEmail))

            Dim NwLogincnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from login_master where user_email='" & IndEmail & "'")

            If NwLogincnt = 0 And N_EIndCount = 0 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into login_master (user_email,user_pwd,date_added) values   ('" & IndEmail & "','WelcomeNSF',GetDate())")
            End If
            If N_EIndCount = 0 Then
                If SpEmail = "" Then
                    strSql = "Update indspouse set ValidEmailFlag = Null, email = '" & IndEmail & "', Modifydate=GetDate(), PrimaryEmail_Old=Email,  ModifiedBy='" & Session("LoginID") & "' where (AutoMemberID = " & Request.QueryString("ID") & IIf(SpEmailCount > 0, " OR RelationShip=" & Request.QueryString("ID"), "") & ") AND Email = '" & IndOldEmail & "'"
                    If (SpEmailCount > 0) Then
                        SpEmailFlag = True
                    End If
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                Else
                    strSql = "Update indspouse set  ValidEmailFlag = Null, email = '" & IndEmail & "', Modifydate=GetDate(), PrimaryEmail_Old=Email,  ModifiedBy='" & Session("LoginID") & "' where (AutoMemberID = " & Request.QueryString("ID") & ") AND Email = '" & IndOldEmail & "'"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                    strSql = "Update indspouse set  ValidEmailFlag = Null, email = '" & SpEmail & "', Modifydate=GetDate(), PrimaryEmail_Old=Email,  ModifiedBy='" & Session("LoginID") & "' where (Relationship = " & Request.QueryString("ID") & ") AND Email = '" & SpOldEmail & "'"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                    SpEmailFlag = True

                End If

                strSql = "UPDATE INDDUPLICATE SET Status='Completed',Remarks='Email Changed in IndSpouse Table' WHERE (INDDUPID=" & Request.QueryString("DupID") & " OR DUPRelID= " & Request.QueryString("DupID") & ") AND AutoMemberID = " & Request.QueryString("ID") & ""
                ''''strSql = "UPDATE INDDUPLICATE SET Status='Completed',Remarks='Email Changed in IndSpouse Table' WHERE (INDDUPID=" & Request.QueryString("DupID") & " OR INDDUPID=" & CDbl(Request.QueryString("DupID")) + 1 & ") AND AutoMemberID = " & Request.QueryString("ID")
                strSql = strSql & " AND Status<>'Completed' "
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                strSql = "Delete From login_master WHERE user_email='" & IndOldEmail & "'"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                If SpEmail <> "" Then
                    strSql = "Delete From login_master WHERE user_email='" & SpOldEmail & "'"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                End If
            Else
                lblMessage.Text = "New Email Already Exist." & N_EIndCount
                Exit Sub
            End If

            strSql = "SELECT * FROM INDDUPLICATE WHERE DonorType IN('SPOUSE') "
            strSql = strSql & " AND Status<>'Completed' AND INDDUPID=" & CDbl(Request.QueryString("DupID")) + 1 & " ORDER BY DonorType "
            strSql = "SELECT * FROM LOGIN_MASTER WHERE Upper(User_Email) IN('" & IndEmail & "')"

            Dim drLoginEmail1 As SqlDataReader
            Dim strName As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT FirstName + ' ' +  LastName FROM indspouse WHERE AutoMemberID=" & Request.QueryString("ID"))
            drLoginEmail1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            sBody = "Dear " & strName & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "We are sorry that you had problems with our online registration.  We have updated your old mail " & IndOldEmail & " with new EmailID as per your request " & IndEmail & vbCrLf
            sBody = sBody & ". By going to the home page at " & vbCrLf
            sBody = sBody & "www.northsouth.org and logging back in, you can retrieve your record and " & vbCrLf
            sBody = sBody & "update as appropriate.  The following are your log in ID and password:" & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & vbCrLf
            While drLoginEmail1.Read()
                sBody = sBody & "Login Email: " & drLoginEmail1("User_Email") & vbCrLf
                sBody = sBody & "Password: " & drLoginEmail1("User_Pwd") & vbCrLf
                sBody = sBody & vbCrLf
            End While
            sBody = sBody & "Please keep these in a safe place." & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "Thank you" & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "With regards," & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "NSF Customer Service Team" & vbCrLf
            SendEmail("EmailID Changed to this active EmailID", sBody, IndEmail)

          
            If SpEmailFlag = False Then
                lblMessage.Text = "Ind Email Change Completed Successfully."
                lblMessage.Text = lblMessage.Text & "<br />No Spouse Email Change done."
            Else
                lblMessage.Text = "Ind and Spouse Email Change Completed Successfully."
            End If
            trChange.Visible = False
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
End Class