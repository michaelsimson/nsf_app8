﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.IO
Imports System.Net.Mail
Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Net
Imports System.Web.Script.Serialization

Partial Class Don_athonDonorDetails
    Inherits System.Web.UI.Page

    Private CAPTCHA_ID As String = "__CaptchaImage__"
    Dim _result As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Request.QueryString("id") = 5 for finals 2017 fundraising         
        If Not IsPostBack Then
            tblContinue.Visible = False
            Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            addMenuItemAt(0, "Home", homeURL)
            If (Session("LoggedIn") = "True") Then
                addLogoutMenuItem()
            Else
                If (loginURL <> Nothing And Session("entryToken") <> Nothing) Then
                    loginURL = loginURL + Session("entryToken").ToString().Substring(0, 1)
                    addMenuItem("Login", loginURL)
                End If
            End If

            If Not Request.QueryString("id") = Nothing Then
                ltlTitle.Text = "North South Foundation : Noble Cause Through Brilliant Minds!"
                lblHeading.Text = "NSF Donation"
            Else
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Top 1 Name as EName,EventID from Event where EventId = (Select Top 1 EventId from WalkMarathon where WalkMarID =" & Session("WalkMarathonID") & " )")
                lblHeading.Text = ds.Tables(0).Rows(0)(0).ToString()
                hdnEventID.Value = ds.Tables(0).Rows(0)(1).ToString()
                ltlTitle.Text = "North South Foundation - " & lblHeading.Text & " : Noble Cause Through Brilliant Minds!"
                 
            End If
            loadStates()
            If Not Session("MId") Is Nothing Then
                Try
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT ES.Email, Count(I.Email) FROM  DonateAThonMailStatus ES Left Join IndSpouse I ON ES.Email=I.Email where Id=" & Session("MId") & " Group by ES.Email")
                    TxtEMail.Text = ds1.Tables(0).Rows(0)(0).ToString()
                    txtUserId.Text = ds1.Tables(0).Rows(0)(0).ToString()
                    If ds1.Tables(0).Rows(0)(1) = 0 Then
                        rbtnUser.SelectedIndex = rbtnUser.Items.IndexOf(rbtnUser.Items.FindByValue("2"))
                        trall.Visible = True
                        RFVReCahpcha.Enabled = True
                         
                        RFVLEmail.Enabled = False
                        RFVPwd.Enabled = False
                        trlogin.Visible = False
                        trrecapcha.Visible = True
                        trNewUser.Visible = True
                        'recaptcha.Enabled = True
                        rfvHomePhoneInd.Enabled = True
                        Dim dsChapters As New DataSet
                        Dim objChapters As New NorthSouth.BAL.Chapter
                        objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
                    End If
                Catch ex As Exception
                    '  Response.Write(ex.ToString())
                    'Response.Write("SELECT ES.Email, Count(I.Email) FROM  DonateAThonMailStatus ES Left Join IndSpouse I ON ES.Email=I.Email where Id=" & Session("MId") & " Group by ES.Email")
                End Try
            ElseIf Not Request.QueryString("id") = Nothing Then
                rbtnUser.SelectedIndex = 0
                rbtnUser_SelectedIndexChanged(rbtnUser, New EventArgs)
                chk1.Visible = False
                chkAmt.Visible = False
                trDn1.Visible = False
                trDn2.Visible = False
                Dim id As String = Request.QueryString("id")
                If (id = "5") Then
                    ddlCampaign.SelectedIndex = ddlCampaign.Items.IndexOf(ddlCampaign.Items.FindByValue("1"))
                    ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByValue("INScholar"))
                    tdNormal.Visible = False
                    tdFinals.Visible = True
                    ddlCampaign.Enabled = False
                    ddlPurpose.Enabled = False
                    lblPageCaption.Text = "NSF - 2017 National Finals in Houston, TX"
                    Session("FinalsDonation") = "true"
                    Session("CustIndChapterID") = "1"
                End If
            End If
        End If
        If Request.QueryString("spons") <> Nothing Then
            ddlCampaign.SelectedIndex = ddlCampaign.Items.IndexOf(ddlCampaign.Items.FindByValue(Session("EventID")))
            ddlCampaign.Enabled = False
        End If
    End Sub

    Public Sub addMenuItem(ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addMenuItem(ByVal itemText As String, ByVal href As String)
        addMenuItem(New MenuItem("&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal itemText As String, ByVal href As String)
        addMenuItemAt(index, New MenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addLogoutMenuItem()
        addMenuItemAt(NavLinks.Items.Count, "Logout", "logout.aspx") 'add to the end
    End Sub

    Protected Sub Populate(ByVal automemberid As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select top 1 * from IndSpouse Where automemberid=" & automemberid & " order by DonorType")
        ' MsgBox(ds.Tables(0).Rows.Count)
        If ds.Tables(0).Rows.Count > 0 Then
            hdnMemberID.Text = ds.Tables(0).Rows(0)("AutoMemberID")
            'ddlTitle.SelectedIndex = ddlTitle.Items.IndexOf(ddlTitle.Items.FindByValue(ds.Tables(0).Rows(0)("Title")))
            txtFname.Text = ds.Tables(0).Rows(0)("FirstName")
            txtLname.Text = ds.Tables(0).Rows(0)("lastName")
            txtAddress.Text = ds.Tables(0).Rows(0)("Address1")
            TxtEMail.Text = ds.Tables(0).Rows(0)("EMail")
            txtCity.Text = ds.Tables(0).Rows(0)("City")
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(ds.Tables(0).Rows(0)("Country")))
            loadStates()

            If ddlCountry.SelectedValue = "CA" Then
                txtState.Text = ds.Tables(0).Rows(0)("State")
                txtState.Visible = True
                ddlState.Visible = False
            Else
                txtState.Visible = False
                ddlState.Visible = True
                ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(ds.Tables(0).Rows(0)("State")))
            End If
            txtZip.Text = ds.Tables(0).Rows(0)("Zip")
        End If
    End Sub
    Public Function Validate() As Boolean
        Try

            Dim Response As String = Request("g-recaptcha-response")

            'Getting Response String Append to Post Method
            Dim Valid As Boolean = False
            'Request to Google Server
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(" https://www.google.com/recaptcha/api/siteverify?secret=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ&response=" & Response), HttpWebRequest)

            'Google recaptcha Response
            Using wResponse As WebResponse = req.GetResponse()

                Using readStream As New StreamReader(wResponse.GetResponseStream())
                    Dim jsonResponse As String = readStream.ReadToEnd()

                    Dim js As New JavaScriptSerializer()
                    Dim data As MyObject = js.Deserialize(Of MyObject)(jsonResponse)
                    ' Deserialize Json
                    Valid = Convert.ToBoolean(data.success)
                End Using
            End Using

            Return Valid
        Catch ex As WebException
            'Response.Write("Error :" & ex.ToString())
            Return False
        End Try

        ' Return True
    End Function

    Public Class MyObject
        Private _success As String
        Public Property success() As String
            Get
                Return _success
            End Get
            Set(value As String)
                _success = value
            End Set
        End Property


    End Class
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMessage.Text = ""
        spCaptcha.Visible = False
        lblErr.Text = ""
        If rbtnUser.SelectedIndex = 1 Then
            spCaptcha.Visible = False
            If txtFname.Text.Trim.Length = 0 Then
                lblMessage.Text = "Please enter First Name"
                Exit Sub
            ElseIf txtLname.Text.Trim.Length = 0 Then
                lblMessage.Text = "Please enter Last Name"
                Exit Sub
            ElseIf TxtEMail.Text.Trim.Length = 0 Then
                lblMessage.Text = "Please enter Email"
                Exit Sub
            End If
            Dim objReg As New Regex("")
            If Not Regex.IsMatch(TxtEMail.Text.Trim(), "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase) Then
                lblMessage.Text = "EMail id Should be a Valid Email Address"
                Exit Sub
            End If
            If Trim(txtAddress.Text) = "" Then
                lblMessage.Text = "Address should not be Blank"
                Exit Sub
            End If
            If Trim(txtCity.Text) = "" Then
                lblMessage.Text = "City should not be Blank"
                Exit Sub
            End If
            Dim state As String
            If (ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US") Then
                If ddlState.SelectedValue = "" Then
                    lblMessage.Text = "Please select State"
                    Exit Sub
                Else
                    lblMessage.Text = ""
                    state = ddlState.SelectedItem.Value
                End If
            Else
                state = Trim(txtState.Text)
                lblMessage.Text = "Please Enter State"
            End If
            If Len(txtZip.Text.Trim) = 0 Then
                lblMessage.Text = "Please enter zipcode"
                Exit Sub
            End If
            If ddlGenderInd.SelectedIndex = 0 Then
                lblMessage.Text = "Please select Gender"
                Exit Sub
            End If
            If txtComments.Text.Length > 100 Then
                lblMessage.Text = "Max chars in Comments Exceeded."
                Exit Sub
            End If
            If state.Length < 1 Then
                lblMessage.Text = "Select State"
                Exit Sub
            ElseIf rbtnothr.Checked = True And IsNumeric(txtAmt.Text) = False And tdNormal.Visible = True Then
                lblMessage.Text = "Enter Numeric Data in amount"
                Exit Sub
            ElseIf rbtnothr.Checked = True And Val(txtAmt.Text) < 1 And tdNormal.Visible = True Then
                lblMessage.Text = "Enter Amount greater than zero"
                Exit Sub
            ElseIf rbtnothF.Checked = True And IsNumeric(txtAmtF.Text) = False And tdFinals.Visible = True Then
                lblMessage.Text = "Enter Numeric Data in amount"
                Exit Sub
            ElseIf rbtnothF.Checked = True And Val(txtAmtF.Text) < 1 And tdFinals.Visible = True Then
                lblMessage.Text = "Enter Amount greater than zero"
                Exit Sub
            Else
                lblMessage.Text = ""
                Dim amount As Decimal
                If tdNormal.Visible = True Then
                    If rbtn20.Checked = True Then
                        amount = 20.0
                    ElseIf rbtn50.Checked = True Then
                        amount = 50.0
                    ElseIf rbtn100.Checked = True Then
                        amount = 100.0
                    Else
                        If CInt(txtAmt.Text.Trim) <= 0 Then
                            lblErr.ForeColor = Color.Red
                            lblErr.Text = "Please give a possitive amount"
                            Exit Sub
                        End If
                        amount = txtAmt.Text
                    End If
                Else
                    If rbtnF50T.Checked = True Then
                        amount = 5000.0
                    ElseIf rbtnF25T.Checked = True Then
                        amount = 2500.0
                    ElseIf rbtnF10T.Checked = True Then
                        amount = 1000.0
                    ElseIf rbtnF5H.Checked = True Then
                        amount = 500.0
                    ElseIf rbtnF25H.Checked = True Then
                        amount = 250.0
                    Else
                        If CInt(txtAmtF.Text.Trim) <= 0 Then
                            lblErr.ForeColor = Color.Red
                            lblErr.Text = "Please give a possitive amount"
                            Exit Sub
                        End If
                        amount = txtAmtF.Text
                    End If
                End If

                'Insert the values into Walkathon_WMSponsor
                Dim chapterID, ChapterCode, parentName, ChildName, Email As String
                If Request.QueryString("id") = Nothing Then
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select I.Chapter,I.ChapterID,I.FirstName,I.LastName,W.Email, CASE WHEN C.ChildNumber IS Not NUll then C.FIRST_NAME + ' '+ C.LAST_NAME Else I1.FirstName+' '+I1.LastName End as WMName    from WalkMarathon W Inner Join IndSpouse I ON I.AutoMemberID = W.MemberID  left Join Child C On C.ChildNumber = W.WMemberID and W.Relationship = 'Child' left Join IndSpouse I1 ON  I1.AutoMemberID = W.WMemberID and W.Relationship <> 'Child' where W.WalkMarID=" & Session("WalkMarathonID"))
                    If ds.Tables(0).Rows.Count > 0 Then
                        chapterID = ds.Tables(0).Rows(0)("ChapterID")
                        ChapterCode = ds.Tables(0).Rows(0)("Chapter")
                        parentName = ds.Tables(0).Rows(0)("FirstName") & " " & ds.Tables(0).Rows(0)("LastName")
                        ChildName = ds.Tables(0).Rows(0)("WMName")
                        Email = ds.Tables(0).Rows(0)("Email")
                    Else
                        chapterID = ""
                        ChapterCode = ""
                        parentName = ""
                        ChildName = ""
                        Email = ""
                    End If
                Else
                    chapterID = "1"
                    ChapterCode = "Home"
                    parentName = ""
                    ChildName = ""
                    Email = ""
                End If
                Dim strSQL As String
                Try
                    'Comment on June10, 2015- to call this page directly from home page
                    ' If rbtnUser.SelectedValue = 2 Then
                    'Email address matches in IndSpouse.  Is Donor Type IND?
                    Dim cmdTxt As String = " declare @auto int "
                    cmdTxt = cmdTxt & " set @auto=0 "
                    cmdTxt = cmdTxt & " select @auto =count(*) from login_master where user_email ='" & TxtEMail.Text.Trim & "' "
                    cmdTxt = cmdTxt & " if (@auto=0) BEGIN "

                    cmdTxt = cmdTxt & " select @auto= CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END FROM indspouse where email ='" & TxtEMail.Text.Trim & "' "
                    cmdTxt = cmdTxt & " IF (@auto=null or @auto =0) BEGIN "
                    cmdTxt = cmdTxt & " set @auto=0 "
                    cmdTxt = cmdTxt & " IF EXISTS(select * FROM  indspouse where FirstName ='" & txtFname.Text.Trim & "' AND LastName ='" & txtLname.Text.Trim & "') BEGIN set @auto=0 "
                    cmdTxt = cmdTxt & " select @auto= CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END  FROM  indspouse where FirstName ='" & txtFname.Text.Trim & "' AND LastName ='" & txtLname.Text.Trim & "' and address1= '" & txtAddress.Text.Trim & "' "
                    cmdTxt = cmdTxt & " End "
                    cmdTxt = cmdTxt & " ELSE BEGIN set @auto=0 End "
                    cmdTxt = cmdTxt & " End END SELECT @auto "
                    Dim iIndID As Integer = Nothing
                    iIndID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdTxt)
                    If iIndID > 0 Then
                        lblMessage.Text = "This is a duplicate.  Your record already exists."
                        Exit Sub
                    Else
                        If Validate() = False Then
                            spCaptcha.Visible = True
                            Exit Sub
                        End If
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(user_email) from login_master where user_email='" & TxtEMail.Text & "'") > 0 Then
                        Else
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into login_master (user_email,user_pwd,date_added) values   ('" & TxtEMail.Text & "','" & GetRandomPasswordUsingGUID(5) & "',GetDate())")
                        End If
                        strSQL = "INSERT INTO IndSpouse(ReferredBy,Relationship,MemberSince,DeletedFlag,chapter,Gender,HPhone,DonorType,ChapterID,Email, FirstName, LastName, Address1,Address2, City, State, Zip, Country, CreateDate) VALUES("
                        strSQL = strSQL & "'" & lblHeading.Text.Replace("-", "") & "',0,GetDate(),'No','" & ChapterCode & "'," & IIf(ddlGenderInd.SelectedValue = " ", "Null", "'" & ddlGenderInd.SelectedValue & "'") & ",'" & txtHomePhoneInd.Text & "','IND'," & chapterID & ","
                        strSQL = strSQL & "'" & TxtEMail.Text & "','" & txtFname.Text.Replace("'", "''") & "','" & txtLname.Text.Replace("'", "''") & "','" & txtAddress.Text.Replace("'", "''") & "','','" & txtCity.Text.Replace("'", "''") & "','" & state & "','" & txtZip.Text & "','" & ddlCountry.SelectedValue & "',GETDATE()); Select Scope_Identity()"
                        hdnMemberID.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSQL)
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO SuspectIndSpouse(MemberId,Status,CreateDate,CreatedBy,ModifyDate,ModifiedBy) VALUES(" & hdnMemberID.Text & ",'Possible Duplicate',GETDATE()," & hdnMemberID.Text & ",GETDATE()," & hdnMemberID.Text & ")")

                        '*** Sending New Registration Confirmation Message 
                        Dim re As TextReader
                        Dim emailbody As String
                        re = File.OpenText(Server.MapPath("NewSponsorRegistration.htm"))
                        emailbody = re.ReadToEnd
                        re.Close()
                        emailbody = emailbody.Replace("PatronName", txtFname.Text & " " & txtLname.Text)
                        emailbody = emailbody.Replace("[USERID]", TxtEMail.Text)
                        emailbody = emailbody.Replace("[PASSWORD]", SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Top 1 user_pwd from login_master where user_email='" & TxtEMail.Text & "'"))
                        ''NOTE Remember to uncomment the following before sending the source
                        SendEmail("North South Foundation  - New Patron", emailbody, TxtEMail.Text)
                        '   End If
                    End If

                    Dim WMSponsorID As Integer = 0
                    If Request.QueryString("id") = Nothing Then
                        strSQL = "INSERT INTO WMSPONSOR(MemberID,Anonymous,PledgeAmount,Comment, WMMemberID, CreateDate,WalkMarID, DonAnonymous,PaymentMode,EventId) VALUES("
                        strSQL = strSQL & hdnMemberID.Text & ",'" & IIf(chk1.Checked = True, "Y", "N") & "'," & amount & ",'" & txtComments.Text.Replace("'", "''") & "'," & Session("WMMemberID") & ",Getdate()," & Session("WalkMarathonID") & ",'" & IIf(chkAmt.Checked = True, "Y", "N") & "','" & RbtnRegType.SelectedItem.Text & "'," & Session("EventID") & "); Select Scope_Identity()"
                        WMSponsorID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSQL)
                    End If
                    ''** testing
                     
                    If RbtnRegType.SelectedValue = 3 Then
                        Session("CustIndID") = hdnMemberID.Text
                        Session("LoginEmail") = TxtEMail.Text
                        Session("loginID") = hdnMemberID.Text
                        Session("entryToken") = "Donor"
                        Session("Donation") = FormatCurrency(amount)
                        Session("LoggedIn") = "True"
                        Session("WMSponsorID") = WMSponsorID
                        Session("DONATIONFOR") = ddlPurpose.SelectedValue 'lblHeading.Text.Replace("-", "").Replace("Donation", "") & " Donation through " & ChildName
                        Session("Campaign") = ddlCampaign.SelectedItem.Text
                        If ddlCampaign.SelectedIndex = 0 Then
                            Session("Campaign") = "25th Year"
                        End If
                        Session("EventID") = ddlCampaign.SelectedValue
                        Dim eventCode As String = ""
                        eventCode = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", Session("EventId")))
                        Session("@EID") = ddlCampaign.SelectedValue
                        Session("@EVE") = eventCode
                        Session("@Anonymous1") = IIf(chk1.Checked, "Yes", "No")
                        tblinput.Visible = False
                        Session("DoPay") = "Don_athonDonor"
                        Response.Redirect("Donor_Pay.aspx?id=" & Session("EventID"))
                        hlink.NavigateUrl = "Donor_Pay.aspx"
                    Else
                        Dim EmailBody As String = ""
                        If Not hdnEventID.Value = "18" Then
                            EmailBody = EmailBody & " <table border='0' align='center' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'><tr><td align='left' ><img src='http://northsouth.org/app8/newsletter/NSFLogo.gif'></td></tr></table><br>"
                            EmailBody = EmailBody & "<p align='justify'>" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1Convert(varchar,Getdate())+'<br>'+FirstName+' '+LastName+'<br>'+Address1+'<br>'+City +',' + state + ' '+ Zip  from IndSpouse where AutoMemberID =" & hdnMemberID.Text & "")
                            EmailBody = EmailBody & "<br><br>Dear " & txtFname.Text & ",<br><br>Thank you kindly for your donation to North South Foundation in their quest to help needy children have scholarships to pursue higher education.  Your donation of $" & amount & " is tax deductible: <B> Tax ID: 36-3659998</B>. Please keep this receipt for you tax records.<br><br> Please contact " & ChildName & " for instructions on how to send cash/check.<br><br>If your employer has a matching gift program, please fill out their gift form and send to North South Foundation, 2 Marissa Ct, Burr Ridge, IL 60527-6864. You can double your money!<br><br>"
                            EmailBody = EmailBody & "NSF was started over 22 years ago with the purpose of providing educational scholarships to needy children who display academic excellence. NSF funds these scholarships by raising donations in the US through spelling bees and direct donations. The NSF Scholarship program is designed to encourage excellence among the poor children who excel academically but need financial help to attend college. Each scholarship is $250 USD per student per year. NSF has distributed more than 5,000 scholarships to students who need financial support to pursue their quest for knowledge in engineering, medicine, polytechnic, science and other fields. The scholarship is an annual award and not a one-time payment. The student is eligible for the scholarship until graduation as long as he/she maintains high academic standards. The local chapters in various states of India invite applications from students, screen them and select the neediest students who eventually become NSF scholarship recipients. Scholarship amounts range from INR 5,000 to INR 12,000 per student per year.  A list of scholarship recipients is published in the NSF Annual Newsletter. <br><br>"
                            EmailBody = EmailBody & "<B>Education is the most valuable gift one can provide to someone. Thank you again for your generous donation toward our work promoting excellence in education.</B><br><br>Sincere Thanks,<br><br>Ratnam Chitturi<br>Founder<br>North South Foundation - - Celebrating 23rd Year<br>Contact Information: North South Foundation, 2 Marissa Court, Burr Ridge, IL 60527. 630-323-1966;"
                            EmailBody = EmailBody & "<br><a href='http://www.northsouth.org/' target='_blank'>www.northsouth.org</a></p>"
                        End If
                        If hdnEventID.Value = "18" Then
                            SendEmail("Thank you on behalf of North South Foundation", "Dear " & txtFname.Text & " " & txtLname.Text & ",<br><br>Thank you very much for your donation of $" & amount & " to  North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ".  Your gift makes a difference - by helping the North South Foundation encourage excellence among the poor children who excel academically but need help to attend college.<br><br>Please contact " & parentName & " for instructions on how to send cash/check.<br><br>Once again, thank you for your generous contribution and support.<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", TxtEMail.Text)
                            SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & parentName & ",<br><br>" & txtFname.Text & " " & txtLname.Text & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & TxtEMail.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                        ElseIf hdnEventID.Value = "5" Then
                            SendEmail("Thank you on behalf of North South Foundation", EmailBody, TxtEMail.Text)
                            SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & ChildName & ",<br><br>" & txtFname.Text & " " & txtLname.Text & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & TxtEMail.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                        Else
                            SendEmail("Thank you on behalf of North South Foundation", "Dear " & txtFname.Text & " " & txtLname.Text & ",<br><br>Thank you very much for your donation of $" & amount & " to  North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ".  Your gift makes a difference - by helping the North South Foundation encourage excellence among the poor children who excel academically but need help to attend college.<br><br>Please contact " & ChildName & " for instructions on how to send cash/check. <br><br>You donation of " & amount & " is tax deductible:  Tax ID: 36-3659998.  Please keep this receipt for you tax records. <br><br>If your employer has a matching gift program, please fill out their gift form and send to North South Foundation, 2 Marissa Ct, Burr Ridge, IL 60527-6864. You can double your money!<br><br>Once again, thank you for your generous contribution and support.<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation", TxtEMail.Text)
                            SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & ChildName & ",<br><br>" & txtFname.Text & " " & txtLname.Text & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & TxtEMail.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                        End If
                        tblinput.Visible = False
                        tblContinue.Visible = True
                        hlink.NavigateUrl = "Don_athon_custom.aspx?NSFDONATE=" + encode(Session("WalkMarathonID"))

                    End If
                Catch ex As Exception
                    lblMessage.Text = ex.ToString()
                End Try
            End If
        Else
            btnLogin_Click(btnLogin, New EventArgs)
        End If
    End Sub

    Public Function GetRandomPasswordUsingGUID(ByVal length As Integer) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function

    Function encode(ByVal inputText As String) As String
        Dim bytesToEncode As Byte() = Encoding.UTF8.GetBytes(inputText)
        Dim encodedText As String = Convert.ToBase64String(bytesToEncode)
        Return encodedText
    End Function

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = "nsfcontests@gmail.com"
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Try
            client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        loadStates()
    End Sub
    Private Sub loadStates()
        If ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US" Then
            ddlState.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet
            Dim strCountry As String = IIf(ddlCountry.SelectedValue = "IN", "usp_GetIndiaStates", "usp_GetStates")
            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, strCountry)
            ddlState.Items.Clear()
            ddlState.DataSource = dsIndiaStates.Tables(0)
            ddlState.DataTextField = dsIndiaStates.Tables(0).Columns(2).ToString
            ddlState.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
            ddlState.DataBind()
            ddlState.Items.Insert(0, New ListItem("Select State", String.Empty))
            txtState.Visible = False
        Else
            ddlState.Visible = False
            txtState.Visible = True
        End If
    End Sub

    Protected Sub rbtnUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnUser.SelectedIndexChanged
        If rbtnUser.SelectedValue = "1" Then
            trlogin.Visible = True
            trNewUser.Visible = False
            rfvHomePhoneInd.Enabled = False
            RFVPwd.Enabled = True
            RFVLEmail.Enabled = True
            trall.Visible = False
            trrecapcha.Visible = False
            RFVReCahpcha.Enabled = False
        Else
            trall.Visible = True
            RFVReCahpcha.Enabled = True
            RFVLEmail.Enabled = False
            RFVPwd.Enabled = False
            trlogin.Visible = False
            trrecapcha.Visible = True
            trNewUser.Visible = True
            rfvHomePhoneInd.Enabled = True
            Dim dsChapters As New DataSet
            Dim objChapters As New NorthSouth.BAL.Chapter
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        spCaptcha.Visible = False

        Dim userID As String = ""  'Login_master
        Dim iMemberId As Integer ' Indspouse
        userID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
        Try
            If (userID <> "") Then
                If Validate() = False Then
                    spCaptcha.Visible = True
                    Exit Sub
                End If
                Dim amount As Decimal
                If tdNormal.Visible = True Then
                    If rbtn20.Checked = True Then
                        amount = 20.0
                    ElseIf rbtn50.Checked = True Then
                        amount = 50.0
                    ElseIf rbtn100.Checked = True Then
                        amount = 100.0
                    Else
                        If CInt(txtAmt.Text.Trim) <= 0 Then
                            lblErr.ForeColor = Color.Red
                            lblErr.Text = "Please give a possitive amount"
                            Exit Sub
                        End If
                        amount = txtAmt.Text
                    End If
                Else
                    If rbtnF50T.Checked = True Then
                        amount = 5000.0
                    ElseIf rbtnF25T.Checked = True Then
                        amount = 2500.0
                    ElseIf rbtnF10T.Checked = True Then
                        amount = 1000.0
                    ElseIf rbtnF5H.Checked = True Then
                        amount = 500.0
                    ElseIf rbtnF25H.Checked = True Then
                        amount = 250.0
                    Else
                        If CInt(txtAmtF.Text.Trim) <= 0 Then
                            lblErr.ForeColor = Color.Red
                            lblErr.Text = "Please give a possitive amount"
                            Exit Sub
                        End If
                        amount = txtAmtF.Text
                    End If
                End If
                ' iMemberId = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                'Insert the values into Walkathon_WMSponsor
                Dim strFName As String, strLName As String
                ''iMemberId = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                Dim dsIndSp As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select AutoMemberId,FirstName,LastName from IndSpouse where Email='" & Server.HtmlEncode(txtUserId.Text) & "'")
                If dsIndSp.Tables(0).Rows.Count > 0 Then
                    iMemberId = dsIndSp.Tables(0).Rows(0)("AutoMemberId")
                    strFName = dsIndSp.Tables(0).Rows(0)("FirstName")
                    strLName = dsIndSp.Tables(0).Rows(0)("LastName")
                End If
                Dim WMSponsorID As Integer = 0, strSQL As String
                If Request.QueryString("id") = Nothing Then
                    strSQL = "INSERT INTO WMSPONSOR(MemberID, Anonymous,PledgeAmount,Comment, WMMemberID, CreateDate,WalkMarID, DonAnonymous,PaymentMode,EventId) VALUES("
                    strSQL = strSQL & iMemberId & ",'N'," & amount & ",'" & txtComments.Text.Replace("'", "''") & "'," & Session("WMMemberID") & ",Getdate()," & Session("WalkMarathonID") & ",'" & IIf(chkAmt.Checked = True, "Y", "N") & "','" & RbtnRegType.SelectedItem.Text & "'," & Session("EventId") & "); Select Scope_Identity()"
                    WMSponsorID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSQL)
                End If
                If RbtnRegType.SelectedValue = 3 Then
                    Session("CustIndID") = iMemberId
                    Session("LoginEmail") = txtUserId.Text.Trim
                    Session("loginID") = iMemberId
                    Session("entryToken") = "Donor"
                    Session("Donation") = FormatCurrency(amount)
                    Session("LoggedIn") = "True"
                    Session("WMSponsorID") = WMSponsorID
                    Session("DONATIONFOR") = ddlPurpose.SelectedValue
                    Session("Campaign") = ddlCampaign.SelectedItem.Text
                    If txtInMemoryOf.Text.Trim.Length > 0 Then
                        Session("@inmemory") = txtInMemoryOf.Text
                    End If
                    'If ddlCampaign.SelectedIndex = 0 Then
                    '    Session("Campaign") = "25th Year"
                    'End If
                    Session("EventID") = ddlCampaign.SelectedValue
                    Session("@EID") = ddlCampaign.SelectedValue
                    Dim eventCode As String = ""
                    eventCode = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", Session("EventId")))
                    Session("@EVE") = eventCode
                    Session("@Anonymous1") = "No"
                    tblinput.Visible = False
                    Session("DoPay") = "Don_athonDonor"
                    Response.Redirect("Donor_Pay.aspx?id=" & Session("EventID"))
                Else
                    Dim chapterID, ChapterCode, parentName, ChildName, Email As String
                    If Request.QueryString("id") = Nothing Then
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select I.Chapter,I.ChapterID,I.FirstName,I.LastName,W.Email, CASE WHEN C.ChildNumber IS Not NUll then C.FIRST_NAME + ' '+ C.LAST_NAME Else I1.FirstName+' '+I1.LastName End as WMName    from WalkMarathon W Inner Join IndSpouse I ON I.AutoMemberID = W.MemberID  left Join Child C On C.ChildNumber = W.WMemberID and W.Relationship = 'Child' left Join IndSpouse I1 ON  I1.AutoMemberID = W.WMemberID and W.Relationship <> 'Child' where W.WalkMarID=" & Session("WalkMarathonID"))
                        If ds.Tables(0).Rows.Count > 0 Then
                            chapterID = ds.Tables(0).Rows(0)("ChapterID")
                            ChapterCode = ds.Tables(0).Rows(0)("Chapter")
                            parentName = ds.Tables(0).Rows(0)("FirstName") & " " & ds.Tables(0).Rows(0)("LastName")
                            ChildName = ds.Tables(0).Rows(0)("WMName")
                            Email = ds.Tables(0).Rows(0)("Email")
                        Else
                            chapterID = ""
                            ChapterCode = ""
                            parentName = ""
                            ChildName = ""
                            Email = ""
                        End If
                    Else
                        chapterID = "1"
                        ChapterCode = "Home"
                        parentName = ""
                        ChildName = ""
                        Email = ""
                    End If

                    Dim EmailBody As String = ""
                    If Not hdnEventID.Value = "18" Then
                        EmailBody = EmailBody & " <table border='0' align='center' cellpadding='2' cellspacing='0' bgcolor='#FFFFFF'><tr><td align='left' ><img src='http://northsouth.org/app8/newsletter/NSFLogo.gif'></td></tr></table><br>"
                        EmailBody = EmailBody & "<p align='justify'>" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1Convert(varchar,Getdate())+'<br>'+FirstName+' '+LastName+'<br>'+Address1+'<br>'+City +',' + state + ' '+ Zip  from IndSpouse where AutoMemberID =" & iMemberId & "")
                        EmailBody = EmailBody & "<br><br>Dear " & strFName & ",<br><br>Thank you kindly for your donation to North South Foundation in their quest to help needy children have scholarships to pursue higher education.  Your donation of $" & amount & " is tax deductible: <B> Tax ID: 36-3659998</B>. Please keep this receipt for you tax records.<br><br> Please contact " & ChildName & " for instructions on how to send cash/check.<br><br>If your employer has a matching gift program, please fill out their gift form and send to North South Foundation, 2 Marissa Ct, Burr Ridge, IL 60527-6864. You can double your money!<br><br>"
                        EmailBody = EmailBody & "NSF was started over 22 years ago with the purpose of providing educational scholarships to needy children who display academic excellence. NSF funds these scholarships by raising donations in the US through spelling bees and direct donations. The NSF Scholarship program is designed to encourage excellence among the poor children who excel academically but need financial help to attend college. Each scholarship is $250 USD per student per year. NSF has distributed more than 5,000 scholarships to students who need financial support to pursue their quest for knowledge in engineering, medicine, polytechnic, science and other fields. The scholarship is an annual award and not a one-time payment. The student is eligible for the scholarship until graduation as long as he/she maintains high academic standards. The local chapters in various states of India invite applications from students, screen them and select the neediest students who eventually become NSF scholarship recipients. Scholarship amounts range from INR 5,000 to INR 12,000 per student per year.  A list of scholarship recipients is published in the NSF Annual Newsletter. <br><br>"
                        EmailBody = EmailBody & "<B>Education is the most valuable gift one can provide to someone. Thank you again for your generous donation toward our work promoting excellence in education.</B><br><br>Sincere Thanks,<br><br>Ratnam Chitturi<br>Founder<br>North South Foundation - - Celebrating 23rd Year<br>Contact Information: North South Foundation, 2 Marissa Court, Burr Ridge, IL 60527. 630-323-1966;"
                        EmailBody = EmailBody & "<br><a href='http://www.northsouth.org/' target='_blank'>www.northsouth.org</a></p>"
                    End If
                    If hdnEventID.Value = "18" Then
                        SendEmail("Thank you on behalf of North South Foundation", "Dear " & strFName & " " & strLName & ",<br><br>Thank you very much for your donation of $" & amount & " to  North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ".  Your gift makes a difference - by helping the North South Foundation encourage excellence among the poor children who excel academically but need help to attend college.<br><br>Please contact " & parentName & " for instructions on how to send cash/check.<br><br>Once again, thank you for your generous contribution and support.<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", txtUserId.Text)
                        SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & parentName & ",<br><br>" & strFName & " " & strLName & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & txtUserId.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                    ElseIf hdnEventID.Value = "5" Then
                        SendEmail("Thank you on behalf of North South Foundation", EmailBody, txtUserId.Text)
                        SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & ChildName & ",<br><br>" & strFName & " " & strLName & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & txtUserId.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                    Else
                        SendEmail("Thank you on behalf of North South Foundation", "Dear " & strFName & " " & strLName & ",<br><br>Thank you very much for your donation of $" & amount & " to  North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ".  Your gift makes a difference - by helping the North South Foundation encourage excellence among the poor children who excel academically but need help to attend college.<br><br>Please contact " & ChildName & " for instructions on how to send cash/check. <br><br>You donation of " & amount & " is tax deductible:  Tax ID: 36-3659998.  Please keep this receipt for you tax records. <br><br>If your employer has a matching gift program, please fill out their gift form and send to North South Foundation, 2 Marissa Ct, Burr Ridge, IL 60527-6864. You can double your money!<br><br>Once again, thank you for your generous contribution and support.<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation", txtUserId.Text)
                        SendEmail("One Pledge of Check/Cash payment was registered ", "Dear " & ChildName & ",<br><br>" & strFName & " " & strLName & " had pledged donation of $" & amount & " to North South Foundation on behalf of " & lblHeading.Text & ": " & ChildName & ". You can contact the donor through EmailID : " & txtUserId.Text & ".<br><br>Sincerely,<br><br>" & lblHeading.Text & " Team<br>A Noble Cause Through a Brilliant Mind!<br>North South Foundation.", Email)
                    End If
                    tblinput.Visible = False
                    tblContinue.Visible = True
                    hlink.NavigateUrl = "Don_athon_custom.aspx?NSFDONATE=" + encode(Session("WalkMarathonID"))
                End If
                'RFVLEmail.Enabled = False
                'RFVPwd.Enabled = False
                'trlogin.Visible = False
                'trall.Visible = True
                'rbtnUser.Enabled = False
                'Populate(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'"))
            Else
                lblErr.ForeColor = Color.Red
                lblErr.Text = "Not a Valid UsedId/Password"
            End If
        Catch ex As Exception
            lblErr.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub RbtnRegType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If RbtnRegType.SelectedValue = 1 Then
            lblCashCheckmsg.Visible = True
            lblCashCheckmsg.Text = "Please pay the cash to the Child or Parent."
            'Response.Redirect("Don_athon_custom.aspx?id=" & Session("WalkMarathonID"))
        ElseIf RbtnRegType.SelectedValue = 2 Then
            lblCashCheckmsg.Visible = True
            lblCashCheckmsg.Text = "Please send the check to  Child or Parent."
        Else
            lblCashCheckmsg.Visible = False
        End If
    End Sub

End Class