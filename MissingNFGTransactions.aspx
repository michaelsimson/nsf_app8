<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="MissingNFGTransactions.aspx.cs" Inherits="MissingNFGTransactions_" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <div align="left" >
     	<asp:HyperLink ID="backToVolunteerFunctions"  CssClass="btn_02"  NavigateUrl="VolunteerFunctions.aspx" runat="server" >  Back to Volunteer Functions</asp:HyperLink>
 </div>
<div align="center" style="font-size: large">

    <b style="color: #009900">Processing Missing NFG Transactions � Credit Card Revenues
</b>
</div>

<div align="left">
    <asp:HiddenField ID="hdnPaydate" runat="server" />
    <asp:HiddenField ID="hdnPayRef" runat="server" />
    <asp:HiddenField ID="hdnPayNote" runat="server" />
<br />
    <table  border="0" runat="server" id="tblnfgtran" 
        style="text-align:center; height: 10px; width: 23%;" visible="true" 
        __designer:mapid="37f" >	
        <tr __designer:mapid="380">
            <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381" 
                bgcolor="Honeydew" style="text-align: left">Event Year</td>
			<td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381" 
                bgcolor="Honeydew" style="text-align: left">Event Name</td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>		
	    <tr __designer:mapid="380">
            <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381">
                <asp:DropDownList ID="ddlYear" runat="server" Height="22px" Width="117px">
                </asp:DropDownList>
            </td>
			<td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381">
                <asp:DropDownList ID="ddlEvent" runat="server" Height="21px" Width="127px">
                </asp:DropDownList>
            </td>
    	</tr>
        <tr __designer:mapid="380">
            <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381">
                <asp:Button ID="bttnsubmit" runat="server" Text="Submit" Width="83px" 
                    onclick="bttnsubmit_Click" />
            </td>
			<td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="381"></td>
    	</tr>
    	</table>
	
	<div align="left" >	
	    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="#33CC33"></asp:Label>	
	</div>

    	<br />
	
</div>

    <asp:GridView ID="gvLog" runat="server" onrowcommand="gvLog_RowCommand" 
        onrowupdated="gvLog_RowUpdated" onrowupdating="gvLog_RowUpdating" 
        onselectedindexchanged="gvLog_SelectedIndexChanged">
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Update" Text="Update" />
        </Columns>
    </asp:GridView>

</asp:Content>

