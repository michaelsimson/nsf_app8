<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.CreateUser" CodeFile="CreateUser.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
			 <div style="margin-left:50px" >  
			 <table width="100%" runat ="server" id="trNewuser"  >
				<tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<H1 align="center"><FONT face="Arial" color="#0000ff" size="3"><B>Create New User</B></FONT>&nbsp;<BR>
						</H1>
					</td>
				</tr>
				<tr>
					<td style="height: 220px">
						<P><FONT class=MidFont>
						  Welcome to the North South Foundation family.  We like to make this a positive experience for you.
                             Your feedback is always welcome to make the NSF website more user-friendly.
                             Please read the following carefully before you begin registration.
                             <b>Do not skip and proceed to the next step.</b>
						
						</FONT>&nbsp;<BR>											
						</P>
						<table  runat="server" id="tblCreateUser" align="center"  width=65%>				
				<TBODY>
					<tr>
						<td class="ItemLabel" vAlign="top" noWrap align="right">User Name:
						</td>
						<td><asp:textbox id="txtUserName" runat="server" CssClass="SmallFont"></asp:textbox></td>
					</tr>
					<tr>
						<td class="ItemLabel" vAlign="top" noWrap align="right">User E-Mail:
						</td>
						<td vAlign="top" noWrap align="left"><asp:textbox id="txtUserEmail" runat="server" CssClass="SmallFont" MaxLength="50" Width="300px"></asp:textbox>&nbsp;
                            <font class="SmallFont">Enter
                            E-mail you use routinely.&nbsp;<br />
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;NSF uses this &nbsp;to &nbsp;communicate with you.<br /></font>
                            <asp:regularexpressionvalidator id="regUserEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
								ErrorMessage="E-Mail Address should be a Valid one" Display="Dynamic" ControlToValidate="txtUserEmail" CssClass="SmallFont"></asp:regularexpressionvalidator><asp:requiredfieldvalidator id="rfvUserEmail" runat="server" ErrorMessage="Enter User E-Mail" Display="Dynamic"
								ControlToValidate="txtUserEmail"></asp:requiredfieldvalidator></td>
					</tr>
					<TR>
						<TD class="ItemLabel" style="WIDTH: 282px">Password:</TD>
						<TD vAlign="top" noWrap align="left" width="25%"><asp:textbox id="txtPassword" runat="server" CssClass="SmallFont" TextMode="Password"></asp:textbox><asp:requiredfieldvalidator id="rfvPassword" runat="server" ErrorMessage="Enter Password" Display="Dynamic"
								ControlToValidate="txtPassword" CssClass="SmallFont"></asp:requiredfieldvalidator></TD>
					</TR>
					<tr>
						<td class="ItemLabel" vAlign="top" noWrap align="right" style="HEIGHT: 25px">ReType 
							Password:</td>
						<td noWrap align="left" width="25%" style="HEIGHT: 25px"><asp:textbox id="txtPasswordReType" runat="server" CssClass="SmallFont" TextMode="Password"></asp:textbox>
							<asp:requiredfieldvalidator id="rfvReTypePassword" runat="server" CssClass="SmallFont" ControlToValidate="txtPasswordReType"
								Display="Dynamic" ErrorMessage="Retype Password "></asp:requiredfieldvalidator>
							<asp:CompareValidator id="cmpPasswordReType" runat="server" CssClass="SmallFont" ControlToValidate="txtPasswordReType"
								ErrorMessage="Retyped Password do not match with the Password above" ControlToCompare="txtPassword"></asp:CompareValidator></td>
					</tr>
					<tr>
						<td class="ItemLabel" valign="top" align="right">
                                                   Role:</td>
						<td vAlign="top" noWrap align="left" width="25%">
						
						<asp:Label ID="roleLabel" runat="server" Visible="false"  CssClass="SmallFont" />&nbsp;
                            <asp:RadioButtonList visible=false ID="RolesList" runat="server">
                                <asp:ListItem >Parent</asp:ListItem>
                                <asp:ListItem >Donor</asp:ListItem>
                                <asp:ListItem >Volunteer</asp:ListItem>
                            </asp:RadioButtonList></td>
                            
					</tr>
					<TR>
						<td class="ItemLabel" vAlign="top" noWrap align="right" width="25%">NSF Chapter:</td>
						<td vAlign="top" noWrap align="left" width="25%" colSpan="3"><asp:dropdownlist id="ddlChapter" tabIndex="7" runat="server" CssClass="SmallFont"></asp:dropdownlist>
							<asp:requiredfieldvalidator id="rfvNSFChapter" runat="server" ControlToValidate="ddlChapter" Display="Dynamic"
								ErrorMessage="NSF Chapter should be selected" CssClass="SmallFont" InitialValue="0" SetFocusOnError="True" Text="NSF Chapter should be selected"></asp:requiredfieldvalidator></td>
					</TR>
					<tr>
						<td class="ItemLabelCenter" colSpan="2">
							<asp:button id="btnSubmit" runat="server" Text="Submit" CssClass="FormButton"></asp:button><br />
							<asp:Label id="lblErrord" runat="server" Visible="False" ForeColor="Red">An account with that email already exists in our system.</asp:Label>
						</td>
					</tr>
				</TBODY>
			</table>
						<P style="text-align:center"><FONT  class=MidFont> <u>General</u></FONT>
						</P>
						<P><FONT  class=MidFont>
						1. If you get stuck or get an error message, please send an email to <A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com</U></A>
						 providing your full name, chapter, your address and phone number where you can be contacted.
						 </FONT>&nbsp;
						</P>
						<P><FONT  class=MidFont>
						2. Please use the Microsoft Internet Explorer only.  Our volunteers did not test this system with
                            other browsers like Firefox.
						 </FONT>&nbsp;
						</P>
						<P><FONT  class=MidFont>
						3. Please do not use this New User Login process, if you have been a donor to NSF in the past or 
                            your child had participated in the contests in the last three years.  In such a case, your record already 
                            exists in our system and you should login as a regular user.  If you have a doubt, please send an email
                            to <A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com</U></A> with your full details (providing your full name, chapter, your address and 
                            phone number where you can be contacted) and NSF will confirm the existence of your record.
						 </FONT>&nbsp;</P>
						 <div id="divparents" runat ="server">
						<P  style="text-align:center"><FONT  class=MidFont>
						<u>Parents </u>
						 </FONT></P>
						<P><FONT  class=MidFont>
						4. Registration for the contests is processed online including payment through a credit card. 
                            As part of the registration, you need to provide information on both parents as well as each child 
                            participating in the contest(s).  Please enter data in all fields.  
						 </FONT>&nbsp;</P>
							<P><FONT  class=MidFont>
						5. NSF offers a variety of contests including spelling, vocabulary, math, geography,
                             essay writing and public speaking.  Based on the availability of resources and participation, 
                             your chapter decides on a particular array of contests to be offered in your center.  
                             If your chapter does not offer a particular contest, but it is, however, offered in a neighboring 
                             center, you may be able to avail the opportunity by registering in the neighboring center. 
						 </FONT>&nbsp;	</P>
						 </div>
						<P><FONT  class=MidFont>
						  <B><font class=errorFont>Caution</font></B>: Some chapters are put into a cluster if they are in a driving distance.
                            If you belong to a cluster, the system will show you all chapters where you can register. 
                            <u>Please read the chapter names and select the precise location where you want your child to
                            participate. Otherwise you will get stuck with the wrong location</u>
						 </FONT>&nbsp;
						
						<P>
						<P></P>
						<P></P>
					</td>
				</tr>							
			</table>
			
			
			
			<table id="tblConfirmation" align="center" runat="server" width=65%>
				<tr>
					<td>
						
						<asp:Label id="lblConfirmation" runat="server" Visible="False" ForeColor="navy"><b>						
						Congratulations!  Your email address and password were accepted for your use in the future in accessing our system. 
						 Keep them in a safe place. Without them, you cannot access our system. <br />  Press the button below to continue.</b>
						
						</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<asp:Button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" Visible="False"></asp:Button>
					</td>
				</tr>
			</table>
			
				<table>
				<tr>
					<td align="center">
						<asp:Button id="Button1" runat="server" CssClass="FormButtonCenter" Text="Continue" Visible="False"></asp:Button>
					</td>
				</tr>
			</table>
			<!--
			<table id="tblLinks" runat="server" width=100%>
				<tr>
					<td>
						<a id="homeLink" runat="server">  Home</a>
						<a id="loginLink" runat="server">Login</a>
						
					</td>
				</tr>
				</table>
				-->
		</div>
</asp:Content>
 

 
 
 