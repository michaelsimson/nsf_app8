Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Partial Class GenerateParticipantCertificates
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim badge As New Badge()
            Dim dsContestDates As New DataSet
            dsContestDates = badge.GetContestDates(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year - 1)
            If Session("Page") = "ManageScoresheet.aspx" Then
                dsContestDates = badge.GetContestDates_Score(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year - 1, Session("ContestID"))
            End If
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                lblChapter.Text = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If

            lblChapter.Font.Bold = True
            If dsContestDates.Tables(0).Rows.Count > 0 Then
                pnlData.Visible = True
                lstContestDates.DataTextField = "ContestDate"
                lstContestDates.DataValueField = "ContestDate"
                lstContestDates.DataSource = dsContestDates
                lstContestDates.DataBind()
                lstContestDates.SelectedIndex = 0
                pnlMessage.Visible = False
            Else
                pnlData.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Contest Dates found."
                lstContestDates.Visible = False
                btnGenerate.Enabled = False
            End If
            Dim dsSignatures As New DataSet
            dsSignatures = badge.GetAllSignatures(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Convert.ToInt32(Now.Year - 1))
            If dsSignatures.Tables(0).Rows.Count > 0 Then
                dgSignatures.DataSource = dsSignatures
                dgSignatures.DataBind()
            Else
                dgSignatures.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No signature names."
                btnGenerate.Enabled = False
            End If

            Session("LeftSignatureName") = Nothing
            Session("RightSignatureName") = Nothing
            Session("LeftSignatureTitle") = Nothing
            Session("RightSignatureTitle") = Nothing
            Session("LeftTitle") = Nothing
            Session("RightTitle") = Nothing

            Session("LeftSignatureImage") = Nothing
            Session("LeftSignatureImage") = Nothing
            setRange()
            If Session("Page") = "ManageScoresheet.aspx" Then
                lblPage.Text = "(ScoreSheet RankCertificates)"
                ddlCertificate.SelectedValue = "Rank"
                ddlCertificate.Enabled = False
                trrange.Visible = False
                'Server.Transfer("ShowRankCertificatesPhase3.aspx")
                For i As Integer = 0 To dsSignatures.Tables(0).Rows.Count - 1
                    If dsSignatures.Tables(0).Rows(i)("ProductCode") = Session("ProductCode") Then
                        dgSignatures.SelectedIndex = i
                    Else
                        dgSignatures.Items.Item(i).Visible = False
                    End If
                Next
            Else
                lblPage.Text = "(CC-Contests)"
            End If

        End If
        If Convert.ToInt32(Session("SelChapterID")) = 1 Then
            btnGenerate.Enabled = True
        End If

    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            trrange.Visible = True
            If ddlCertificate.SelectedValue = "Rank" Then
                trrange.Visible = False
                If Session("Page") = "ManageScoresheet.aspx" Then ' Session("Page") =
                    Server.Transfer("ShowRankCertificatesPhase3.aspx")
                Else
                    Server.Transfer("ShowRankCertificate.aspx")
                End If

            Else
                trrange.Visible = True
                If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                    pnlMessage.Visible = True
                    lblMessage.Text = "Select Valid Signature Name"

                Else
                    If ddlCertificate.SelectedValue = "Participant" Then
                        If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                        Server.Transfer("ShowParticipantCertificatePDF.aspx")
                    Else
                        Server.Transfer("ShowVolunteerCertificateNewPDF.aspx")
                    End If
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Public ReadOnly Property ContestDates() As String
        Get
            Return GetSelectedContestDates()
        End Get
    End Property
    Private Function GetSelectedContestDates() As String
        Dim strSelectedContestDates As String
        Dim i As Integer = 0
        For i = 0 To lstContestDates.Items.Count - 1
            If lstContestDates.Items(i).Selected = True Then
                If Len(strSelectedContestDates) > 0 Then
                    strSelectedContestDates = strSelectedContestDates & ",'" & lstContestDates.Items(i).Text & "'"
                Else
                    strSelectedContestDates = "'" & lstContestDates.Items(i).Text & "'"
                End If
            End If
        Next
        Return strSelectedContestDates
    End Function
    Protected Sub dgSignatures_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgSignatures.SelectedIndexChanged
        Session("LeftSignatureName") = dgSignatures.SelectedItem.Cells(3).Text & " " & dgSignatures.SelectedItem.Cells(4).Text
        Session("LeftSignatureTitle") = dgSignatures.SelectedItem.Cells(5).Text
        Session("LeftTitle") = dgSignatures.SelectedItem.Cells(2).Text
        Session("RightSignatureName") = dgSignatures.SelectedItem.Cells(7).Text & " " & dgSignatures.SelectedItem.Cells(8).Text
        Session("RightSignatureTitle") = dgSignatures.SelectedItem.Cells(9).Text
        Session("RightTitle") = dgSignatures.SelectedItem.Cells(6).Text
        If Convert.ToInt32(Session("SelChapterID")) = 1 Then
            Session("LeftSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(10).Text
            Session("RightSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(11).Text
        Else
            'Session("LeftSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            'Session("RightSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            If dgSignatures.SelectedItem.Cells(10).Text = "'" Then
                Session("LeftSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            Else
                Session("LeftSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(10).Text
            End If
            If dgSignatures.SelectedItem.Cells(11).Text = "'" Then
                Session("RightSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            Else
                Session("RightSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(11).Text
            End If
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Me.Export = True
        trrange.Visible = True
        If ddlCertificate.SelectedValue = "Rank" Then
            trrange.Visible = False
            If Session("Page") = "ManageScoresheet.aspx" Then
                Server.Transfer("ShowRankCertificatesPhase3.aspx")
            Else
                Server.Transfer("ShowRankCertificate.aspx")
            End If
        Else
            trrange.Visible = True
            If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                pnlMessage.Visible = True
                lblMessage.Text = "Select Valid Signature Name"
            Else
                If ddlCertificate.SelectedValue = "Participant" Then
                    If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                    Server.Transfer("ShowParticipantCertificateNew.aspx")
                Else
                    Server.Transfer("ShowVolunteerCertificateNew.aspx")
                End If
            End If
        End If
    End Sub
    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPDF.Click
        Me.Export = True
        trrange.Visible = True
        If ddlCertificate.SelectedValue = "Rank" Then
            trrange.Visible = False
            If Session("Page") = "ManageScoresheet.aspx" Then
                Server.Transfer("ShowRankCertificatesPhase3.aspx")
            Else
                Server.Transfer("ShowRankCertificatePDF.aspx")
            End If
        Else
            trrange.Visible = True
            If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                pnlMessage.Visible = True
                lblMessage.Text = "Select Valid Signature Name"
            Else
                If ddlCertificate.SelectedValue = "Participant" Then
                    If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                    Server.Transfer("ShowParticipantCertificatePDF.aspx")
                Else
                    Server.Transfer("ShowVolunteerCertificateNewPDF.aspx")
                End If
            End If
        End If
    End Sub
  
    Private _export As Boolean
    Public Property Export() As Boolean
        Get
            Return _export
        End Get
        Set(ByVal value As Boolean)
            _export = value
        End Set
    End Property

    Protected Sub ddlCertificate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Participant
        If ddlCertificate.SelectedValue = "Participant" Then
            setRange()
        Else
            trrange.Visible = False
        End If
    End Sub

    Private Sub setRange()
        If Session("SelChapterID") = "1" Then
            Dim Strdates As String = String.Empty
            Dim i As Integer
            'If lstContestDates.Items.Count = 0 Then Exit Sub
            For i = 0 To lstContestDates.Items.Count - 1
                If Strdates.Length = 0 Then
                    Strdates = "'" & lstContestDates.Items(i).Text & "'"
                Else
                    Strdates = Strdates & ",'" & lstContestDates.Items(i).Text & "'"
                End If
            Next
            'Range for participants
            trrange.Visible = True
            Dim childcnt As Integer
            Try
                childcnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(Now.Year - 1) & "")
            Catch ex As Exception
                'Response.Write("select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(Now.Year-1-1) & "")
                'Response.Write(ex.ToString)
                Exit Sub
            End Try
            Dim Itemcount As Integer
            Itemcount = childcnt / 200
            If Itemcount * 200 < childcnt Then
                Itemcount = Itemcount + 1
            End If
            For i = 1 To Itemcount
                Dim li As ListItem = New ListItem()
                li.Text = (((i - 1) * 200) + 1) & "  To  " & (i * 200)
                li.Value = (((i - 1) * 200) + 1) & "_" & (i * 200)
                ddlRange.Items.Insert(i - 1, li)
            Next

            'test
            ''ddlRange.Items.Insert(0, New ListItem("1 to 25 ", "1_25"))
            ' ''ddlRange.Items.Insert(0, New ListItem("1 to 2 ", "1_2"))
            ''ddlRange.Items.Insert(1, New ListItem("501 to 525 ", "501_525"))
            ''ddlRange.Items.Insert(2, New ListItem("901 to 925 ", "901_925"))
            ddlRange.SelectedIndex = 0
        End If
    End Sub
End Class
