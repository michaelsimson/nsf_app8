﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports GDE4
Imports GDE4.Service
Imports GDE4.Transaction

Partial Class Donor_Donate
    Inherits System.Web.UI.Page
    Private d1 As String
    Private d2 As String
    Private d3 As String
    Private d4 As String
    Private d5 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ''ddlMonth.Items.Add(" ")
            ddlMonth.Items.Add("01")
            ddlMonth.Items.Add("02")
            ddlMonth.Items.Add("03")
            ddlMonth.Items.Add("04")
            ddlMonth.Items.Add("05")
            ddlMonth.Items.Add("06")
            ddlMonth.Items.Add("07")
            ddlMonth.Items.Add("08")
            ddlMonth.Items.Add("09")
            ddlMonth.Items.Add("10")
            ddlMonth.Items.Add("11")
            ddlMonth.Items.Add("12")
            populateControls()
            txtIP.Text = GetIPAddress()
            txtMail.Text = txtName.Text.Replace(" ", "") + "@mail.com"


            ' lblIP.Text = "Client IP: " + GetIPAddress()
        End If
        ' '' ''  Dim a As String = ConfigurationSettings.GetConfig("LPAPP_Config/client")
        '' ''Dim a As NameValueCollection = CType(ConfigurationManager.GetSection("LPAPP_Config/client"), NameValueCollection)

        '' ''d1 = a("configfile")
        '' ''d2 = a("Keyfile")
        '' ''d3 = a("Host")
        '' ''d4 = a("port")
        '' ''d5 = a("result")
        '' ''lblStatus.Text = "Config File : " + d1 _
        '' ''+ "<br>" + "Key File : " + d2 + "<br>" _
        '' ''+ "Host : " + d3 + "<br>" + "Port : " + d4 _
        '' ''+ "<br>" + "Result : " + d5
        ' '' ''port = Int32.Parse(config("Port"))
        ' '' ''result = config("result")

        'configCDE4()
        'lblStatus.Text = "User Name : " + d1 + "<br>" + "Password : " + d2
    End Sub
    Private Sub populateControls()
        ddlYear.Items.Clear()
        ' Dim li As ListItem = New ListItem(" ", "")
        Dim li As ListItem = New ListItem()
        'ddlYear.Items.Add(li)
        Dim i As Integer = System.DateTime.Today.Year + 1
        Do While (i _
                    <= (System.DateTime.Today.Year + 17))
            Dim j As Integer = (i - 2000)
            li = New ListItem(i.ToString, j.ToString)
            ddlYear.Items.Add(li)
            i = (i + 1)
        Loop
        'ddlYear.Items.Add("2025")
        'ddlMonth.Items.Add("12")
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim damt As String
        Dim edate As String
        edate = gDate()
        damt = AmtD()
        ' Label2.Text = edate
        Dim ws As New GDE4.Service()
        ' Dim Ws As New GDE4.ServiceSoapClient()
        Dim txn As New GDE4.Transaction()
        Try
            txn.ExactID = "AD7642-07"
            txn.Password = "kg5vh7t2"
            txn.Transaction_Type = "04"
            txn.Card_Number = txtcNum.Text
            txn.CardHoldersName = txtName.Text
            txn.VerificationStr2 = txtcvv2.Text
            txn.DollarAmount = txtAmt.Text
            txn.Expiry_Date = edate
            txn.Client_Email = txtMail.Text
            txn.Client_IP = txtIP.Text
            txn.Customer_Ref = txtCR.Text
            txn.Reference_No = txtRefN.Text

            Dim result As GDE4.TransactionResult = ws.SendAndCommit(txn)
            lblStatus.ForeColor = Color.Blue
            result.AVS.ToString()
            lblStatus.Text = "Test" + "<br>" + result.AVS.ToString() + "<br>" + result.CTR + "<br>" + "#################### REFUND RESPONSE#######################" + "<br>" + "Aurthorization Numer: " + result.Authorization_Num.ToString _
            + "<br>" + "Response Code : " + result.EXact_Resp_Code.ToString() + "<br>" + "Exact Message : " _
            + result.EXact_Message.ToString() + "<br>" + "Bank Message : " + result.Bank_Message.ToString() _
            + "<br>" + "Bank Response Code: " + result.Bank_Resp_Code.ToString() + "<br>" + " Reference No: " + result.Reference_No.ToString() _
            + "<br>" + "Customer Ref : " + result.Customer_Ref.ToString() + "<br>" + "Client IP: " + result.Client_IP _
            + "<br>" + "Client Email :" + result.Client_Email _
            + "<br>" + "Retrieval Ref No : " + result.Retrieval_Ref_No.ToString()

            'Response.Write(result.CTR)
            'Response.Write(result.Authorization_Num.ToString())
            'Response.Write(result.EXact_Resp_Code.ToString())
            'Response.Write(result.EXact_Message.ToString())
            'Response.Write(result.Bank_Message.ToString())
            'Response.Write(result.Bank_Resp_Code.ToString())
            'Response.Write(result.Reference_No.ToString())
            'Response.Write(result.Customer_Ref.ToString())
            'result.Reference_No '--> A merchant defined value that can be used to internally
            'identify the transaction. This value is passed through to the Global Gateway e4℠
            'unmodified, and may be searched in First Data Global Gateway e4℠ 
            'Real-time Payment Manager (RPM). 
        Catch ex As Exception
            lblStatus.ForeColor = Color.Red
            lblStatus.Text = ex.ToString()
            'Label1.Text = ex.ToString()
            'Response.Write(ex.ToString())
            'lblStatus.Text = ex.Message.ToString()
        End Try
    End Sub
    Private Function AmtD() As String
        Dim am As Decimal
        am = Decimal.Parse(txtAmt.Text)
        Return Decimal.Round(am, 2).ToString("f2")
    End Function
    Private Function gDate() As String
        Dim dt As String
        dt = ddlMonth.SelectedValue.ToString() + Right(ddlYear.Text, 2)
        Return dt
    End Function
    Public Function IsCreditCard(ByVal strToCheck As String) As Boolean
        Dim objAlphaNumericPattern As Regex = New Regex("^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$")
        Return objAlphaNumericPattern.IsMatch(strToCheck)
    End Function
    Public Function GetIPAddress() As String
        ''Dim context As System.Web.HttpContext = System.Web.HttpContext.Current()
        ''Dim sIPAddress As String = context.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        ''If String.IsNullOrEmpty(sIPAddress) Then
        ''    Return context.Request.ServerVariables("REMOTE_ADDR")
        ''    'Else
        ''    'Dim ipArray As String() = sIPAddress.Split(New [Char]() {","c})
        ''    'Return ipArray(0)
        ''End If

        ''Return System.Web.HttpContext.Current.Request.UserHostAddress.ToString()
        '' ''Dim CustomerIP As String = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_VIA")
        '' ''If String.IsNullOrEmpty(CustomerIP) Then
        '' ''    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        '' ''Else
        '' ''    CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        '' ''End If
        '' ''Return CustomerIP

        Dim IPAdd As String = String.Empty
        IPAdd = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If String.IsNullOrEmpty(IPAdd) Then
            IPAdd = Request.ServerVariables("REMOTE_ADDR")
        End If
        Return  IPAdd
    End Function
    Sub configCDE4()
        Dim G4 As NameValueCollection = CType(ConfigurationManager.GetSection("GDE4/crd"), NameValueCollection)
        d1 = G4("U_Name")
        d2 = G4("U_Pass")
        'lblStatus.Text = "Config File : " + d1 _
        '+ "<br>" + "Key File : " + d2 + "<br>" _
        '+ "Host : " + d3 + "<br>" + "Port : " + d4 _
        '+ "<br>" + "Result : " + d5
    End Sub
    'Protected Sub GetConfigParams()
    '    ' get some stuff from app config
    '    Dim config As NameValueCollection = CType(ConfigurationManager.GetSection("LPAPP_Config/client"), NameValueCollection)
    '    configfile = config("Configfile")
    '    keyfile = config("Keyfile")
    '    host = config("Host")
    '    port = Int32.Parse(config("Port"))
    '    result = config("result")
    '    config = CType(ConfigurationManager.GetSection("LPAPP_Config/opts"), NameValueCollection)
    '    ' not in use anymore
    '    Try
    '        dbg = Boolean.Parse(config("dbg"))
    '    Catch e As Exception

    '    End Try
    'End Sub

    Protected Sub txtName_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        txtMail.Text = txtName.Text.Replace(" ", "") + "@mail.com"
    End Sub

    
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x
        For Each x In Request.ServerVariables
            Response.Write(x & ":" & Request.ServerVariables(x) & " </BR>  ")
        Next
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("DonorFunctions.aspx")
    End Sub

    Protected Sub txtRefN_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefN.TextChanged

    End Sub
End Class
'End Namespace