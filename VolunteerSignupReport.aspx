﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" MasterPageFile="~/NSFMasterPage.master" CodeFile="VolunteerSignupReport.aspx.cs" Inherits="VolunteerSignupReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
        &nbsp&nbsp&nbsp&nbsp
         
          
    </div>

    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <strong>Volunteer Signup Report

        </strong>
        <br />


    </div>
    <div align="center" style="margin-top: 10px;">
        <asp:Label ID="lbacces" Font-Bold="true" runat="server" Text="" ForeColor="Red"></asp:Label>

    </div>
    <br />

    <div id="IDContent" runat="server">

        <table style="margin-left: 0px;">
            <tr>
                <td style="width: 150px"></td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                    </asp:DropDownList>

                </td>

                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                <td style="width: 141px" runat="server" align="left" id="ddchapterdrop">
                    <asp:DropDownList ID="ddchapter" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddTeams" runat="server" Width="155px" AutoPostBack="True" OnSelectedIndexChanged="ddTeams_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>







            </tr>

        </table>


        <table>
            <tr>
                <td></td>
                <td style="width: 50px"></td>

                <td>


                    <table>
                        <tr>
                            <td align="left" nowrap="nowrap">&nbsp;</td>
                            <td style="width: 141px" align="left">&nbsp;</td>
                        </tr>
                        <%-- <tr>
        <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product Group</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproductgroup" runat="server" Width="150px" 
                    AutoPostBack="True" OnSelectedIndexChanged="ddproductgroup_SelectedIndexChanged" 
                    >
                    
                </asp:DropDownList>
            </td>
</tr>--%>
                        <%--     <tr>
         <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproduct" runat="server" Width="150px" 
                    OnSelectedIndexChanged="ddproduct_SelectedIndexChanged" AutoPostBack="True" 
                    >
                    
                </asp:DropDownList>
            </td></tr>--%>
                    </table>

                </td>


            </tr>
        </table>

        <%--  </div>--%>


        <div id="Div1" align="center" runat="server">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:Button ID="btnExcel" runat="server" Visible="false" Text="Export to Excel" OnClick="btnExcel_Click" />
            <br />
            <br />

            <b>
                <div id="Div2" align="center" runat="server">
                    <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>
            </b>
            <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
            <div style="float: left;">
                <asp:Label ID="lblSignupCount" Font-Bold="true" runat="server" Text=""></asp:Label>
            </div>
            <div style="clear: both;"></div>
            <div id="Div4" align="center" runat="server" style='width: 1000px; overflow: auto;'>
                <asp:GridView ID="GvVolsignup" runat="server" DataKeyNames="VolsignupId"
                    AutoGenerateColumns="False" OnRowEditing="GvVolsignup_RowEditing" OnRowUpdating="GvVolsignup_RowUpdating" OnRowCancelingEdit="GvVolsignup_RowCancelingEdit1" EnableModelValidation="True" AllowPaging="True" OnPageIndexChanging="GvVolsignup_PageIndexChanging" PageSize="20">

                    <Columns>
                        <asp:TemplateField>

                            <ItemTemplate>
                                <asp:Button ID="btnEdit" Text="Modify" runat="server" CommandName="Edit" />

                            </ItemTemplate>

                            <EditItemTemplate>
                                <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" />

                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                            </EditItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MemberID">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FirstName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="LastName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Hphone">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Cphone">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="TeamId">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Team">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Year">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Eventname">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ChapterName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="ProductGroup">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnProductgroup" Value='<%# Bind("ProductGroupId")%>' runat="server" />
                                <asp:DropDownList ID="productGroup" runat="server" Width="120px" AutoPostBack="True" OnPreRender="DDproductGroupEdit" OnSelectedIndexChanged="ddday_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="TxtprodGroup" Text='<%#Eval("ProductGroup") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                        <asp:TemplateField HeaderText="Product">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnProduct" Value='<%# Bind("ProductId")%>' runat="server" />
                                <asp:DropDownList ID="product" runat="server" Width="120px" AutoPostBack="false" OnPreRender="DDproduct">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="TxtProduct" Text='<%#Eval("product") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AvailHours">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnAvailhrs" Value='<%# Bind("AvailHours")%>' runat="server" />
                                <asp:DropDownList ID="ddavailhrs" runat="server" Width="120px" AutoPostBack="false" OnPreRender="DDAvailhrs">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="Txthrs" Text='<%#Eval("AvailHours") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="City">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>


                </asp:GridView>
            </div>
            <br />
            <asp:Label ID="lbnorec1" runat="server" ForeColor="Red"></asp:Label>
        </div>
    </div>

    <div id="Div3" align="center" runat="server">
        <br />
        <asp:Label ID="lblRoles" Font-Bold="true" runat="server" Text=""></asp:Label>
        <div style="clear: both;"></div>
        <div style="float: left; margin-left: 85px;">
            <asp:Label ID="lblVolunteerCount" Font-Bold="true" runat="server" Text=""></asp:Label>
        </div>

        <asp:GridView ID="GridView1" runat="server" ItemStyle-HorizontalAlign="left">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <br />
        <asp:Label ID="lbnorec" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>

    <div style="clear: both; margin-bottom: 20px;">
    </div>
    <div id="dvReturningCoachesNotCal" align="center" runat="server" visible="false" style='width: 1000px; overflow: auto;'>
        <center>
            <span style="font-weight: bold;">Table3: Returning coaches from Table 1, but did not do the calendar signup
            </span>
        </center>
        <div style="clear: both; margin-bottom: 10px;">
            <center>
                <asp:Label ID="lblNoRec3" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </center>
        </div>
        <div style="clear: both;">
        </div>
        <div>
            <div style="float: left;">
                <asp:Button ID="btnSendEmailRCNCL" runat="server" Text="Send Email for Calendar Signup" OnClick="btnSendEmailRCNCL_Click" />
            </div>
            <div style="float: left;">
                <asp:Label ID="lblReturningCoachNotCal" Font-Bold="true" runat="server" Text=""></asp:Label>
            </div>
            <div style="float: right;">
                <asp:Button ID="btnExportToExcelRCNCL" runat="server" Text="Export To Excel" OnClick="btnExportToExcelRCNCL_Click" />
            </div>
        </div>

        <div style="clear: both;">
        </div>
        <asp:GridView ID="GrdReturnCoachesNotCal" runat="server"
            AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="20" OnPageIndexChanging="GrdReturnCoachesNotCal_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MemberID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Hphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="TeamId">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Eventname">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ChapterName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="AvailHours">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>



            </Columns>


        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;">
    </div>
    <div id="dvReturnCoachCal" align="center" runat="server" visible="false" style='width: 1000px; overflow: auto;'>
        <center>
            <span style="font-weight: bold;">Table4: Returning coaches, also did the calendar signup (all set)

            </span>
        </center>
        <div style="clear: both; margin-bottom: 10px;">
            <center>
                <asp:Label ID="lblNoRec4" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </center>
        </div>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <asp:Label ID="lblReturnCoachCal" Font-Bold="true" runat="server" Text=""></asp:Label>
        </div>
        <div>

            <div style="float: right;">
                <asp:Button ID="btnExportExcelRCCL" runat="server" Text="Export To Excel" OnClick="btnExportExcelRCCL_Click" />
            </div>
        </div>

        <div style="clear: both;">
        </div>
        <asp:GridView ID="GrdReturnCoachCal" runat="server"
            AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="20" OnPageIndexChanging="GrdReturnCoachCal_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MemberID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Hphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="TeamId">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Eventname">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ChapterName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="AvailHours">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>



            </Columns>


        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;">
    </div>
    <div id="dvNewCoachesNotSM" align="center" visible="false" runat="server" style='width: 1000px; overflow: auto;'>
        <center>
            <span style="font-weight: bold;">Table5: New Coaches from Table 1, but did not fill out Survey Monkey


            </span>
        </center>
        <div style="clear: both; margin-bottom: 10px;">
            <center>
                <asp:Label ID="lblNoRec5" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </center>
        </div>
        <div style="clear: both;">
        </div>
        <div>
            <div style="float: left;">
                <asp:Button ID="btnSendEmailFilloutSur" runat="server" Text="Send Email to Fill out Survey" OnClick="btnSendEmailFilloutSur_Click" />
            </div>
            <div style="float: left; margin-left:10px;">
                <asp:Label ID="lblNewCoachesNotSM" runat="server" Font-Bold="true" Text=""></asp:Label>
            </div>
            <div style="float: right;">
                <asp:Button ID="btnExportExcelNCNSM" runat="server" Text="Export To Excel" OnClick="btnExportExcelNCNSM_Click" />
            </div>
        </div>

        <div style="clear: both;">
        </div>
        <asp:GridView ID="GrdNewCoachNotSM" runat="server"
            AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="20" OnPageIndexChanging="GrdNewCoachNotSM_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MemberID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Hphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="TeamId">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Eventname">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ChapterName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="AvailHours">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>



            </Columns>


        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;">
    </div>
    <div id="dvNewCoachSMNotCL" align="center" visible="false" runat="server" style='width: 1000px; overflow: auto;'>
        <center>
            <span style="font-weight: bold;">Table6: New Coaches, filled out Survey Monkey, but did not do the calendar signup

            </span>
        </center>
        <div style="clear: both; margin-bottom: 10px;">
            <center>
                <asp:Label ID="lblNoRec6" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </center>
        </div>
        <div style="clear: both;">
        </div>
        <div>
            <div style="float: left;">
                <asp:Button ID="btnSendEmailClSignup" runat="server" Text="Send Email for Calendar Signup" OnClick="btnSendEmailClSignup_Click" />
            </div>
            <div style="float: right;">
                <asp:Button ID="btnExportToExcelSMNotCL" runat="server" Text="Export To Excel" OnClick="btnExportToExcelSMNotCL_Click" />
            </div>
        </div>

        <div style="clear: both;">
        </div>
        <asp:GridView ID="GrdNewCoachSMNotCL" runat="server"
            AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="20" OnPageIndexChanging="GrdNewCoachSMNotCL_PageIndexChanging" Width="100%">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MemberID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Hphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="TeamId">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Eventname">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ChapterName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <%--     <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="AvailHours">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                    </ItemTemplate>

                </asp:TemplateField>




                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>



            </Columns>


        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;">
    </div>
    <div id="dvNewCoachCalSM" align="center" visible="false" runat="server" style='width: 1000px; overflow: auto;'>
        <center>
            <span style="font-weight: bold;">Table7: New Coaches, filled out Survey Monkey and did the calendar signup (all set)

            </span>
        </center>
        <div style="clear: both; margin-bottom: 10px;">
            <center>
                <asp:Label ID="lblNoRec7" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </center>
        </div>
        <div style="clear: both;">
        </div>

        <div>

            <div style="float: right;">
                <asp:Button ID="btnExportToExcelCalSM" runat="server" Text="Export To Excel" OnClick="btnExportToExcelCalSM_Click" />
            </div>
        </div>

        <div style="clear: both;">
        </div>

        <asp:GridView ID="GrdNewCoachCalSM" runat="server"
            AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="20" OnPageIndexChanging="GrdNewCoachCalSM_PageIndexChanging" Width="100%">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MemberID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Hphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cphone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="TeamId">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Eventname">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ChapterName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>



                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <%--      <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="AvailHours">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>



            </Columns>


        </asp:GridView>
    </div>


    <asp:Label ID="lbevent" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbPrd" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbEventId" runat="server" Visible="false"></asp:Label>
    </div>
    <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>

    <input type="hidden" id="hdnVolSignupCount" runat="server" value="" />
</asp:Content>
