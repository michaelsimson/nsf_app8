﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Drawing;
using System.Collections;

public partial class AddUpdProdLevel : System.Web.UI.Page
{
    public static string StrProdLevelId = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        lblerr.Text = "";
        if (!IsPostBack)
        {
            LoadEvent(ddEvent);
            PopulateYear(ddlYear);


            populateLevel();

            loadAllLevels();
            fillProductGroup();


        }
    }


    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;

            ArrayList list = new ArrayList();
            ArrayList listPr = new ArrayList();

            for (int i = (MaxYear + 1); i >= 2010; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));

            }
            //for (int i = (MaxYear + 1); i >= 2010; i--)
            //{
            //    listPr.Add(new ListItem(i.ToString(), i.ToString()));

            //}

            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();


            ddlProposedYr.DataSource = list;
            ddlProposedYr.DataTextField = "Text";
            ddlProposedYr.DataValueField = "Value";
            ddlProposedYr.DataBind();
            ddlObject.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }


    public void LoadEvent(DropDownList ddlObject)
    {
        try
        {

            string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID");
            //  Where EF.EventYear ="
            //+ (ddlYear.SelectedValue + ""))
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 0))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
                    ddlObject.SelectedValue = "13";
                }
                else
                {
                    ddlObject.Enabled = false;
                    fillProductGroup();
                }

            }
            else
            {

                lblerr.Text = "No Events present for the selected year";
            }
        }
        catch
        {
        }
    }

    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = Convert.ToInt32(ddlYear.SelectedValue);
            cmdText = "select ProductGroupId, ProductGroupCode as Name from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + year + ")";

            //cmdText = "select ProductGroupID,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "Name";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            ddlProduct.Items.Clear();
            if (ddlProductGroup.SelectedValue == "0")
            {

            }
            else
            {
                int year = Convert.ToInt32(ddlYear.SelectedValue);
                //year = 2014;
                cmdText = "select ProductId,ProductCode as Name from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + year + ")";

                //cmdText = "select ProductID,ProductCode from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "Name";
                ddlProduct.DataValueField = "ProductId";
                ddlProduct.DataBind();
                //ddlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlProduct.Enabled = true;
                }
                else
                {
                    ddlProduct.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void populateLevel()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {

            int year = Convert.ToInt32(ddlYear.SelectedValue);
            //year = 2014;
            cmdText = "select distinct LevelID,LevelCode from ProdLevel order by LevelId";

            //cmdText = "select ProductID,ProductCode from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            ddlLevelCode.DataSource = ds;
            ddlLevelCode.DataTextField = "LevelCode";
            ddlLevelCode.DataValueField = "LevelID";
            ddlLevelCode.DataBind();
            ddlLevelCode.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevelCode.Enabled = true;
            }
            else
            {
                ddlLevelCode.Enabled = false;
            }

        }
        catch (Exception ex)
        {
        }

    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProductGroup.SelectedValue != "-1")
        {
            fillProduct();
            populateLevel();
            if (ddlProductGroup.SelectedValue != "-1" && ddlProduct.SelectedValue != "-1" && ddlLevelCode.SelectedValue != "-1")
            {
                PopulateGradeFrom();
            }
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue != "-1")
        {
            loadAllLevels();
            //LoadEvent(ddEvent);
        }
    }

    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue != "-1")
        {

            fillProductGroup();
            loadAllLevels();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        InsertUpdateSession();
    }
    public void loadAllLevels()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        bool IsEvent = false;

        CmdText = "select WC.EventYear,WC.ProdLevelID, WC.EventID,E.Name as EventName,WC.ProductGroupID,WC.ProductID,PG.Name as ProductGroupName,P.Name as ProductName,WC.LevelCode, WC.GradeFrom, WC.GradeTo, WC.Mandatory, WC.LevelId from ProdLevel WC inner join Event E on (WC.EventID=E.EventiD) left join ProductGroup PG on(WC.ProductGroupID=PG.ProductGroupID) left join Product P on (WC.ProductID=P.ProductId) ";
        if (ddEvent.SelectedValue != "-1")
        {
            IsEvent = true;
            CmdText += " where WC.Eventid=" + ddEvent.SelectedValue + "";
        }
        if (ddlYear.SelectedValue != "-1")
        {
            if (IsEvent == true)
            {
                CmdText += " and WC.EventYear=" + ddlYear.SelectedValue + "";
            }
            else
            {
                CmdText += " where WC.EventYear=" + ddlYear.SelectedValue + "";
            }
        }
        CmdText += " order by WC.EventYear DESC,ProductGroupID, ProductID, LevelID";

        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                GrdLevel.DataSource = ds;
                GrdLevel.DataBind();
                lblLevelStatus.Text = "";
            }
            else
            {
                GrdLevel.DataSource = ds;
                GrdLevel.DataBind();
                lblLevelStatus.Text = "No record exists";
            }
        }

    }

    public void InsertUpdateSession()
    {
        try
        {

            if (ValidateSession() == "1")
            {
                string CmdText = string.Empty;
                string productGroup = string.Empty;
                string ProductGroupID = string.Empty;
                string Product = string.Empty;
                string ProductID = string.Empty;
                string Msg = string.Empty;


                if (btnSubmit.Text == "Save")
                {

                    Msg = "Inserted Successfully";

                }
                else
                {

                    Msg = "Updated Successfully";
                }
                string Mandatory = ddlMandatory.SelectedValue;

                if (btnSubmit.Text == "Save")
                {
                    int count = 0;
                    string cmdCount = "select count(*) as CountSet from ProdLevel where EventYear=" + ddlYear.SelectedValue + " and EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and LevelCode='" + ddlLevelCode.SelectedItem.Text + "'";

                    DataSet ds = new DataSet();
                    ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdCount);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());

                        }

                    }
                    if (count > 0)
                    {
                        lblerr.Text = "Duplicate exists..!";
                    }
                    else
                    {
                        if (Mandatory == "N")
                        {
                            CmdText = "Insert into ProdLevel(EventID,EventYear,ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, CreatedDate, CreatedBy, GradeFrom, GradeTo, Mandatory) values(" + ddEvent.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "','" + ddlLevelCode.SelectedItem.Text + "','" + ddlLevelCode.SelectedValue + "', GetDate(), " + Session["LoginID"].ToString() + ", " + ddlGradefrom.SelectedValue + ", " + ddlGradeTo.SelectedValue + ",null)";
                        }
                        else if (Mandatory == "Y")
                        {
                            CmdText = "Insert into ProdLevel(EventID,EventYear,ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, CreatedDate, CreatedBy, GradeFrom, GradeTo, Mandatory) values(" + ddEvent.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "','" + ddlLevelCode.SelectedItem.Text + "','" + ddlLevelCode.SelectedValue + "', GetDate(), " + Session["LoginID"].ToString() + ", " + ddlGradefrom.SelectedValue + ", " + ddlGradeTo.SelectedValue + ",'Y')";
                        }


                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                        lblerr.Text = Msg;
                        btnSubmit.Text = "Save";
                        populateLevel();
                        // ddlLevelCode.SelectedItem.Text = "Select";
                    }
                }
                else
                {
                    if (Mandatory == "Y")
                    {
                        CmdText = "Update ProdLevel set EventID=" + ddEvent.SelectedValue + ",EventYear=" + ddlYear.SelectedValue + ",ProductGroupID=" + ddlProductGroup.SelectedValue + ", ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "', ProductID=" + ddlProduct.SelectedValue + ", ProductCode='" + ddlProduct.SelectedItem.Text + "', LevelCode='" + ddlLevelCode.SelectedItem.Text + "',  LevelID=" + ddlLevelCode.SelectedValue + ", ModifiedDate=GetDate(), ModifiedBy=" + Session["LoginID"].ToString() + ",  GradeFrom =" + ddlGradefrom.SelectedValue + ", GradeTo=" + ddlGradeTo.SelectedValue + ", Mandatory='Y' where ProdLevelID=" + hdnProdLevelID.Value + "";
                    }
                    else if (Mandatory == "N")
                    {
                        CmdText = "Update ProdLevel set EventID=" + ddEvent.SelectedValue + ",EventYear=" + ddlYear.SelectedValue + ",ProductGroupID=" + ddlProductGroup.SelectedValue + ", ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "', ProductID=" + ddlProduct.SelectedValue + ", ProductCode='" + ddlProduct.SelectedItem.Text + "', LevelCode='" + ddlLevelCode.SelectedItem.Text + "',  LevelID=" + ddlLevelCode.SelectedValue + ", ModifiedDate=GetDate(), ModifiedBy=" + Session["LoginID"].ToString() + ", GradeFrom =" + ddlGradefrom.SelectedValue + ", GradeTo=" + ddlGradeTo.SelectedValue + ", Mandatory=null where ProdLevelID=" + hdnProdLevelID.Value + "";
                    }


                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = Msg;
                    btnSubmit.Text = "Save";
                    //ddlLevelCode.SelectedItem.Value = "Select";
                    populateLevel();
                }

                loadAllLevels();
            }
            else
            {
                ValidateSession();
            }
        }

        catch
        {
        }
    }

    public string ValidateSession()
    {
        string Retval = "1";
        if (ddlYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            Retval = "-1";
        }
        else if (ddEvent.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Event";
            Retval = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Product Group";
            Retval = "-1";
        }
        else if (ddlProduct.SelectedValue == "0")
        {
            lblerr.Text = "Please select Product";
            Retval = "-1";
        }
        else if (ddlLevelCode.SelectedItem.Text == "Select")
        {
            lblerr.Text = "Please fill Level Code";
            Retval = "-1";
        }
        else if (ddlGradefrom.SelectedItem.Value == "-1")
        {
            lblerr.Text = "Please select Grade From";
            Retval = "-1";
        }
        else if (ddlGradeTo.SelectedItem.Value == "-1")
        {
            lblerr.Text = "Please select Grade To";
            Retval = "-1";
        }
        else if (ddlMandatory.SelectedItem.Value == "-1")
        {
            lblerr.Text = "Please select Mandatory";
            Retval = "-1";
        }
        else if (ddlGradefrom.SelectedValue != "-1" && ddlGradeTo.SelectedValue != "-1")
        {
            int GradeFrom = Convert.ToInt32(ddlGradefrom.SelectedValue);
            int GradeTo = Convert.ToInt32(ddlGradeTo.SelectedValue);
            if (GradeTo < GradeFrom)
            {
                lblerr.Text = "GradeTo should be greater than or equal to GradeFrom.";
                Retval = "-1";
            }
            if (Retval == "1")
            {
                string cmdText = " select GradeFrom, GradeTo from prodLevel where EventYear=" + ddlYear.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and GradeFrom is not null and GradeTo is not null";
                if (hdnProdLevelID.Value != "0")
                {
                    cmdText += " and ProdLevelId not in (" + hdnProdLevelID.Value + ")";
                }
                //int GradeFrom = Convert.ToInt32(ddlGradefrom.SelectedValue);
                //int GradeTo = Convert.ToInt32(ddlGradeTo.SelectedValue);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                bool isExists = false;
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            int ExtGradeFrom = Convert.ToInt32(dr["GradeFrom"].ToString());
                            int ExtGradeTo = Convert.ToInt32(dr["GradeTo"].ToString());
                            if (GradeFrom == ExtGradeFrom || (GradeFrom > ExtGradeFrom && GradeFrom <= ExtGradeTo) || (GradeTo == ExtGradeTo) || (GradeTo > ExtGradeFrom && GradeTo <= ExtGradeTo))
                            {
                                isExists = true;
                            }

                        }
                    }
                }
                if (isExists == true)
                {
                    Retval = "-1";
                    lblerr.Text = "Grade range already existing to other level.";
                }
            }
        }
        else
        {


        }
        return Retval;
    }

    protected void GrdLevel_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                btnSubmit.Text = "Update";
                GridViewRow row = null;
                GrdLevel.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdLevel.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string prodLevel = string.Empty;

                prodLevel = ((Label)GrdLevel.Rows[selIndex].FindControl("lblProdLevelID") as Label).Text;
                string LevelId = ((Label)GrdLevel.Rows[selIndex].FindControl("lblLevelId") as Label).Text;

                hdnProdLevelID.Value = prodLevel;

                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                ProductGroupID = ((Label)GrdLevel.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdLevel.Rows[selIndex].FindControl("LblProductID") as Label).Text;

                // ddlLevelCode.SelectedItem.Value = LevelId;

                ddlYear.SelectedValue = ((Label)GrdLevel.Rows[selIndex].FindControl("lbEventYear") as Label).Text;
                LoadEvent(ddEvent);
                ddEvent.SelectedValue = ((Label)GrdLevel.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;
                string Level = string.Empty;
                Level = ((Label)GrdLevel.Rows[selIndex].FindControl("lblLevel") as Label).Text;
                ddlLevelCode.SelectedValue = LevelId;
                PopulateGradeFrom();
                int GradeFrom = 0;
                int GradeTo = 0;
                string Mandatory = string.Empty;
                try
                {
                    GradeFrom = Convert.ToInt32(((Label)GrdLevel.Rows[selIndex].FindControl("lblGradeFrom") as Label).Text);
                    GradeTo = Convert.ToInt32(((Label)GrdLevel.Rows[selIndex].FindControl("lblGradeTo") as Label).Text);
                    Mandatory = ((Label)GrdLevel.Rows[selIndex].FindControl("lblMandatory") as Label).Text;
                }
                catch
                {

                }


                ddlGradefrom.SelectedValue = GradeFrom.ToString();
                ddlGradeTo.SelectedValue = GradeTo.ToString();
                if (Mandatory == "")
                {
                    ddlMandatory.SelectedValue = "N";
                }
                else if (Mandatory == "Y")
                {
                    ddlMandatory.SelectedValue = "Y";
                }


            }
            else if (e.CommandName == "DeleteLevel")
            {

                GridViewRow row = null;
                GrdLevel.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdLevel.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string prodLevel = string.Empty;
                prodLevel = ((Label)GrdLevel.Rows[selIndex].FindControl("lblProdLevelID") as Label).Text;
                hdnProdLevelID.Value = prodLevel;

                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                ProductGroupID = ((Label)GrdLevel.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdLevel.Rows[selIndex].FindControl("LblProductID") as Label).Text;
                string year = ((Label)GrdLevel.Rows[selIndex].FindControl("lbEventYear") as Label).Text;
                string eventID = ((Label)GrdLevel.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                string Level = string.Empty;
                Level = ((Label)GrdLevel.Rows[selIndex].FindControl("lblLevel") as Label).Text;
                string cmdText = "select count(*) as calSIgnupCount from CalSignup where EventYear=" + year + " and EventID=" + eventID + " and ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and Level='" + Level + "'; select count(*) as coachRegCount from CoachReg where EventYear=" + year + " and EventID=" + eventID + " and ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and Level='" + Level + "';";

                int coachRegCount = 0;
                int calSIgnupCount = 0;

                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && (ds.Tables[0].Rows.Count > 0 || ds.Tables[1].Rows.Count > 0))
                {
                    calSIgnupCount = Convert.ToInt32(ds.Tables[0].Rows[0]["calSIgnupCount"].ToString());
                    coachRegCount = Convert.ToInt32(ds.Tables[1].Rows[0]["coachRegCount"].ToString());
                }

                if (calSIgnupCount <= 0 && coachRegCount <= 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteConfirm();", true);
                }
                else
                {
                    lblerr.Text = "Cannot be deleted. Data exists in CalSignUp and/or in CoachReg table.";
                }

            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnSubmit.Text = "Save";
        ddlLevelCode.SelectedValue = "0";
        ddlProductGroup.SelectedValue = "-1";
        ddlProduct.Items.Clear();
        ddlGradefrom.Items.Clear();
        ddlGradeTo.Items.Clear();
        ddlGradeTo.Items.Insert(0, new ListItem("Select", "-1"));
        ddlGradefrom.Items.Insert(0, new ListItem("Select", "-1"));
        ddlLevelCode.Items.Insert(0, new ListItem("Select", "-1"));
        ddlProduct.Items.Insert(0, new ListItem("Select", "-1"));

    }

    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        string CmdText = "delete from ProdLevel where ProdLevelID=" + hdnProdLevelID.Value + "";

        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        lblerr.Text = "Level deleted successfully";
        loadAllLevels();
    }



    protected void GrdLevel_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdLevel.PageIndex = e.NewPageIndex;
            loadAllLevels();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }

    public void PopulateGradeFrom()
    {
        try
        {


            ddlGradefrom.Items.Clear();
            ddlGradeTo.Items.Clear();
            string Cmdtext = " select GradeFrom, GradeTo from EventFees where EventYear=" + ddlYear.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + "";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            int GradFrom = 0;
            int GradeTo = 0;
            if (ds.Tables.Count > 0)
            {
                GradFrom = Convert.ToInt32(ds.Tables[0].Rows[0]["GradeFrom"].ToString());
                GradeTo = Convert.ToInt32(ds.Tables[0].Rows[0]["gradeTo"].ToString());
            }

            if (GradFrom > 0 && GradeTo > 0)
            {
                ArrayList list = new ArrayList();
                for (int i = GradFrom; i <= GradeTo; i++)
                {


                    list.Add(new ListItem(i.ToString(), i.ToString()));

                }
                ddlGradefrom.DataSource = list;
                ddlGradefrom.DataTextField = "Text";
                ddlGradefrom.DataValueField = "Value";
                ddlGradefrom.DataBind();

                ddlGradefrom.Items.Insert(0, new ListItem("Select", "-1"));

                ddlGradeTo.DataSource = list;
                ddlGradeTo.DataTextField = "Text";
                ddlGradeTo.DataValueField = "Value";
                ddlGradeTo.DataBind();

                ddlGradeTo.Items.Insert(0, new ListItem("Select", "-1"));
            }
        }
        catch
        {

        }
    }
    protected void ddlLevelCode_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlProductGroup.SelectedValue != "-1" && ddlProduct.SelectedValue != "-1" && ddlLevelCode.SelectedValue != "-1")
        {
            PopulateGradeFrom();
        }
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlProductGroup.SelectedValue != "-1" && ddlProduct.SelectedValue != "-1" && ddlLevelCode.SelectedValue != "-1")
        {
            PopulateGradeFrom();
        }
    }
    protected void BtnAddNew_Click(object sender, EventArgs e)
    {
        dvAddLevel.Visible = true;
    }
    protected void BtnReplicateAll_Click(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
        }
        else if (ddlProposedYr.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Proposed Year";
        }
        else if (Convert.ToInt32(ddlYear.SelectedValue) >= Convert.ToInt32(ddlProposedYr.SelectedValue))
        {
            lblerr.Text = "Proposed Year should be greater than Year.";
        }
        else
        {
            ReplicateAll();
        }

    }
    protected void BtnReplicateSel_Click(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
        }
        else if (ddlProposedYr.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Proposed Year";
        }
        else if (Convert.ToInt32(ddlYear.SelectedValue) >= Convert.ToInt32(ddlProposedYr.SelectedValue))
        {
            lblerr.Text = "Proposed Year should be greater than Year.";
        }
        else
        {
            bool Checked = false;
            for (int i = 0; i < GrdLevel.Rows.Count; i++)
            {
                bool IsChecked = ((CheckBox)GrdLevel.Rows[i].FindControl("ChkLevel") as CheckBox).Checked;
                if (IsChecked == true)
                {
                    Checked = true;
                }
            }
            if (Checked == true)
            {
                ReplicateSelected();
            }
            else
            {
                lblerr.Text = "Please select the Level from the Table 1: Product Levels.";
            }
        }

    }
    public void ReplicateAll()
    {
        string Cmdtext = string.Empty;
        string ProdLevelId = string.Empty;
        Cmdtext = " Select count(*) from ProdLevel where EventYEar=" + ddlYear.SelectedValue + "";
        int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext));
        if (count > 0)
        {
            Cmdtext = "Select prodLevelId, ProductGroupid, productId, LevelCode from prodLevel where EventYear=" + ddlYear.SelectedValue + "";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (null != ds)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            Cmdtext = "select count(*) from ProdLevel where EventYear=" + ddlProposedYr.SelectedValue + " and Productgroupid=" + dr["ProductGroupid"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + " and LevelCode='" + dr["levelCode"].ToString() + "'";
                            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext)) < 1)
                            {
                                ProdLevelId += dr["ProdLevelid"].ToString() + ",";
                            }
                        }
                        if (ProdLevelId != "")
                        {
                            ProdLevelId = ProdLevelId.TrimEnd(',');

                            Cmdtext = "Insert into ProdLevel(EventID,EventYear,ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, CreatedDate, CreatedBy, GradeFrom, GradeTo, Mandatory) select EventID," + ddlProposedYr.SelectedValue + ",ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, getDate(), " + Session["LoginId"].ToString() + ", GradeFrom, GradeTo, Mandatory from ProdLevel where ProdLevelId in (" + ProdLevelId + ") ";


                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
                            lblerr.Text = "Replicated successfully.";
                            ddlYear.SelectedValue = ddlProposedYr.SelectedValue;
                            loadAllLevels();
                        }
                    }
                }
            }
        }
    }
    public void ReplicateSelected()
    {
        string Cmdtext = string.Empty;
        string ProdLevelId = string.Empty;

        for (int i = 0; i < GrdLevel.Rows.Count; i++)
        {
            bool IsChecked = ((CheckBox)GrdLevel.Rows[i].FindControl("ChkLevel") as CheckBox).Checked;
            if (IsChecked == true)
            {
                ProdLevelId += ((Label)GrdLevel.Rows[i].FindControl("lblPrdLevelId") as Label).Text + ",";
            }
        }

        ProdLevelId = ProdLevelId.TrimEnd(',');
        Cmdtext = "Select prodLevelId, ProductGroupid, productId, LevelCode from prodLevel where prodLevelId in (" + ProdLevelId + ")";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (null != ds)
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProdLevelId = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        Cmdtext = "select count(*) from ProdLevel where EventYear=" + ddlProposedYr.SelectedValue + " and Productgroupid=" + dr["ProductGroupid"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + " and LevelCode='" + dr["levelCode"].ToString() + "'";
                        if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext)) < 1)
                        {
                            ProdLevelId += dr["ProdLevelid"].ToString() + ",";
                        }
                    }
                }
            }
        }
        if (ProdLevelId != "")
        {
            ProdLevelId = ProdLevelId.TrimEnd(',');
            Cmdtext = "Insert into ProdLevel(EventID,EventYear,ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, CreatedDate, CreatedBy, GradeFrom, GradeTo, Mandatory) select EventID," + ddlProposedYr.SelectedValue + ",ProductGroupID, ProductGroupCode, ProductID, ProductCode, LevelCode,LevelID, getDate(), " + Session["LoginId"].ToString() + ", GradeFrom, GradeTo, Mandatory from ProdLevel where ProdLevelId in (" + ProdLevelId + ") ";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            lblerr.Text = "Replicated successfully.";
            ddlYear.SelectedValue = ddlProposedYr.SelectedValue;
            loadAllLevels();
        }
    }
}