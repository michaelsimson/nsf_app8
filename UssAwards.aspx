﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="UssAwards.aspx.cs" Inherits="UssAwards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    uu<script type="text/javascript">

    function ValidateEnterKey(evt) {
        if (evt.keyCode == 13) //detect Enter key
        {
            return false;
        }
        else {
            return true;
        }
    }
</script><asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
<div align="center" style="font-size: large" ><strong>US Scholarship Awards & Payments  
    </strong> </div>
       <br />
     <div  align="center"> 
    <table style="width: 50%">
        <tr>
            <td align="center" style="width: 224px">   
                <asp:DropDownList ID="ddlChoice" runat="server" AutoPostBack="True" Height="17px" OnSelectedIndexChanged="ddlChoice_SelectedIndexChanged">
                </asp:DropDownList> &nbsp;&nbsp;<asp:Button ID="Btnexcel" runat="server" Visible="false"  Text="Export to Excel" 
            onclick="Btnexcel_Click" />
            </td>
        </tr>
    </table>
    </div>
    <div align="left" id="divuntil" runat ="server">

        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Until : "></asp:Label>
        <asp:DropDownList ID="ddlYearUntil" runat="server">
        </asp:DropDownList> &nbsp;&nbsp;
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />

        <asp:Label ID="lbluntil" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>

    </div>
<div align="left" id="divibutton" runat ="server" >

    <asp:Button ID="btnWinList" runat="server" Text="Download Winners List" Visible="False" OnClick="btnWinList_Click"/>
    &nbsp;&nbsp;
    <asp:Label ID="lblupStatus" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>

</div>
    <br />
    <div align="left" id="divMain" runat="server">
    <table style="width: 25%; margin-left: 0px;">
        <tr>
            <td align="left" nowrap="nowrap">Child Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td align="left" nowrap="nowrap" style="width: 237px">
                <asp:TextBox ID="txtChildName" runat="server" Width="147px" onkeypress="return ValidateEnterKey(event);"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsearchChild" runat="server" Text="Search" OnClick="btnsearchChild_Click" />
                <asp:HiddenField ID="hdnChildID" runat="server" />
                <asp:HiddenField ID="hdnMemberid" runat="server" />
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Product Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddlProductGroup" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="width: 141px" align="left"><span style="position:relative;left:50px;">
                 &nbsp;&nbsp;&nbsp;&nbsp;
                City&nbsp;</span></td>
                  <td style="width: 141px" align="left">
                <asp:TextBox ID="TxtCity" runat="server" Width="147px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Contest Year</td>
            <td align="left" style="width: 237px;">
                <asp:DropDownList ID="ddlContestYear" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px; " width="140px"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; ">&nbsp;Product</td>
            <td align="left" style="width: 141px">
                <asp:DropDownList ID="ddlProduct" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td align="left" style="width: 141px"><span style="position:relative;left:50px;">
                 &nbsp;&nbsp;&nbsp;&nbsp;
                 State&nbsp;</span></td>
                  <td style="width: 141px" align="left">
                <asp:DropDownList ID="DDState" runat="server" Width="150px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap" style="height: 3px">Grade</td>
            <td align="left" style="height: 3px; width: 237px">
                <asp:DropDownList ID="ddlGrade" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px; height: 3px;"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; height: 3px;">Rank&nbsp;</td>
            <td align="left" style="height: 3px; width: 141px;">
                <asp:DropDownList ID="ddlRank" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td align="left" style="height: 3px;  width: 141px;">
            <span style="position:relative;left:50px;">
                 &nbsp;&nbsp;&nbsp;&nbsp;  No Dollar</span>
              </td>
                  <td style="width: 141px" align="left">
                <asp:DropDownList ID="DDNoDoller" runat="server" Width="150px" AutoPostBack="True" 
                          OnSelectedIndexChanged="DDNoDoller_SelectedIndexChanged">
                    <asp:ListItem Value="1">No</asp:ListItem>
              <asp:ListItem Value="2">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Chapter</td>
            <td align="left" style="width: 237px">
                <asp:DropDownList ID="ddlChapter" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px"></td>
            <td align="left" nowrap="nowrap" style="width: 103px">Amount</td>
            <td align="left" style="width: 141px">
                <asp:TextBox ID="txtAmount" runat="server" Width="147px" onkeypress="return ValidateEnterKey(event);"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtAmount" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" style="width: 141px">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap" style="height: 20px">Sponsor</td>
            <td align="left" style="width: 237px; height: 20px;">
                <asp:TextBox ID="txtSponsor" runat="server" Width="147px" onkeypress="return ValidateEnterKey(event);"></asp:TextBox>
                <asp:Button ID="Button4" runat="server" Text="Search" OnClick="Button4_Click" />
            </td>
            <td nowrap="nowrap" style="width: 142px; height: 20px;"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; height: 20px;">Paid Full</td>
            <td align="left" nowrap="nowrap" style="height: 20px; width: 141px">
               <asp:DropDownList ID="ddlPFull" runat="server" Width="150px">
                    <asp:ListItem Value="-1">[Select Paid Full]</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="height: 20px; width: 141px">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="left" style="width: 237px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="75px" OnClick="btnAdd_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="75px" OnClick="btnCancel_Click" />
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">&nbsp;</td>
            <td align="left" style="width: 141px">
                &nbsp;</td>
            <td align="left" style="width: 141px">
                <asp:Button ID="btnExport1" runat="server" OnClick="btnExport1_Click" Text="Export to Excel" Width="158px" />
            </td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="left" style="width: 237px">
                <asp:Label ID="lblA" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">&nbsp;</td>
            <td align="left" style="width: 141px">
                <asp:HiddenField ID="hdnSponsorId" runat="server" />
                <asp:HiddenField ID="hdnDonorType" runat="server" />
                </td>
            <td align="left" style="width: 141px">
                &nbsp;</td>
        </tr>
    </table>
        <asp:GridView ID="gvAwards" runat="server" EnableModelValidation="True" OnRowCommand="gvAwards_RowCommand" AllowPaging="True" OnPageIndexChanging="gvAwards_PageIndexChanging" PageSize="50" OnRowDataBound="gvAwards_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" HeaderText="Modify" Text="Modify" />
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
        </div>
    <div align="left" id="divPay" runat="server" dir="ltr">
    <table style="width: 25%; margin-left: 0px;">
        <tr>
            <td align="left" nowrap="nowrap">Contest Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td align="left" nowrap="nowrap" style="width: 237px">
                <asp:DropDownList ID="ddlYear" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Date Paid&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td align="left" nowrap="nowrap">
                <asp:TextBox ID="txtPaid" runat="server" Width="100px"></asp:TextBox>
                <span style="font-size: xx-small"><strong>(MM/DD/YYYY)</strong></span><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtPaid" Display="Dynamic" ErrorMessage="Invalid Date" SetFocusOnError="True" ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$"></asp:RegularExpressionValidator></td>
            <td align="left" nowrap="nowrap" style="width: 89px">
                &nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">
                Rest.Amount</td>
            <td align="left" nowrap="nowrap" style="width: 101px">
                <asp:TextBox ID="txtRest" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtRest" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" nowrap="nowrap">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Child Name</td>
            <td align="left" style="width: 237px;">
                <asp:DropDownList ID="ddlChName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChName_SelectedIndexChanged" Width="150px">
                </asp:DropDownList>
                <asp:LinkButton ID="LinkButton3" runat="server" Font-Bold="True" OnClick="LinkButton3_Click1">Details</asp:LinkButton>
            </td>
            <td nowrap="nowrap"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; ">Check Number</td>
            <td align="left">
                <asp:TextBox ID="txtcheck" runat="server" Width="100px"></asp:TextBox>
            </td>
            <td align="left" nowrap="nowrap" style="width: 89px">
                &nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">
                UnRest. Amount</td>
            <td align="left" nowrap="nowrap" style="width: 101px">
                <asp:TextBox ID="txtUnrest" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtUnrest" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" nowrap="nowrap">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap" style="height: 3px">Grantee</td>
            <td align="left" style="height: 3px; width: 237px" nowrap="nowrap">
                <asp:TextBox ID="txtGrantee" runat="server" Width="147px"></asp:TextBox>
                &nbsp;&nbsp;<asp:HiddenField ID="hdnGranteeID" runat="server" />
                <asp:HiddenField ID="hdnGrDonorType" runat="server" />
                &nbsp;
                <asp:Button ID="btnGranteeSearch" runat="server" Text="Search" OnClick="btnGranteeSearch_Click" />
            </td>
            <td nowrap="nowrap" style="height: 3px;"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; height: 3px;">Amount</td>
            <td align="left" style="height: 3px">
                <asp:TextBox ID="txtPaidAmount" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPaidAmount" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" style="height: 3px; width: 89px;" nowrap="nowrap">
                &nbsp;</td>
            <td align="left" style="height: 3px; width: 103px;" nowrap="nowrap">
                Excess of Cap</td>
            <td align="left" style="height: 3px; width: 101px;" nowrap="nowrap">
                <asp:TextBox ID="txtExcess" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtExcess" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" style="height: 3px" nowrap="nowrap">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Bank</td>
            <td align="left" style="width: 237px">
                <asp:DropDownList ID="ddlBank" runat="server" Width="150px">
                    <asp:ListItem>[Select Bank ID]</asp:ListItem>
                    <asp:ListItem Selected="True">4</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap"></td>
            <td align="left" nowrap="nowrap" style="width: 103px">Donation to NSF</td>
            <td align="left">
                <asp:TextBox ID="txtDntoNSF" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDntoNSF" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" nowrap="nowrap" style="width: 89px">
                &nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">
                Interest</td>
            <td align="left" nowrap="nowrap" style="width: 101px">
                <asp:TextBox ID="txtinterest" runat="server" Width="100px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtinterest" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>
            <td align="left" nowrap="nowrap">
                &nbsp;</td>
        </tr>
        <td>
            <td align="left" nowrap="nowrap">
                <asp:Button ID="btnPadd" runat="server" Text="Add" Width="75px" OnClick="btnPadd_Click" />
                <asp:Button ID="bttnPcancel" runat="server" Text="Cancel" Width="75px" OnClick="bttnPcancel_Click" />
            </td>
            <td align="left" nowrap="nowrap">
                &nbsp;</td>
            <td nowrap="nowrap" style="width: 55px">Paid Full</td>
            <td align="left" nowrap="nowrap" style="width: 103px">
       
                <asp:DropDownList ID="ddlPaidFull" runat="server" Width="104px">
                    <asp:ListItem Value="-1">[Paid Full]</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                </asp:DropDownList>
               
            </td>
            <td align="left" nowrap="nowrap">
            </td&nbsp;</td>
       
                <td align="left" nowrap="nowrap" style="width: 89px">
                &nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">
                &nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 101px">
               <asp:Button ID="btnExport2" runat="server" OnClick="btnExport2_Click" Text="Export to Excel" style="margin-top: 0px" />
        </td>
            <td align="left" nowrap="nowrap">
                &nbsp;</td>
        </table>
        <div align="left" id="divchdetails" runat ="server" >
         <div align="left" >

             <asp:LinkButton ID="LinkButton2" runat="server" Font-Bold="True" OnClick="LinkButton2_Click1">Close Details</asp:LinkButton>

         </div>
            <asp:GridView ID="gvChildDetails" runat="server" OnRowDataBound="gvChildDetails_RowDataBound">
            </asp:GridView>
         

        </div>
        
                <br />
                <asp:Label ID="lblB" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        <asp:GridView ID="gvPayment" runat="server" EnableModelValidation="True" OnRowCommand="gvPayment_RowCommand" AllowPaging="True" OnPageIndexChanging="gvPayment_PageIndexChanging" OnSelectedIndexChanging="gvPayment_SelectedIndexChanging" PageSize="50" OnRowDataBound="gvPayment_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" HeaderText="Modify" Text="Modify" />
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
        </div>
    <div align="center" id="divChildSearch" runat ="server" title="Search Child"> 
    <div align="left" style="font-size: medium" >
        <asp:Label ID="lblSearch" runat="server" CssClass="btn_02" ></asp:Label>
        <strong style="font-size: small" class="btn_02"></strong></div>

            <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" __designer:mapid="1ca" >	
                 <tr __designer:mapid="1d1">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="1d2">&nbsp;Name:</td>        
                    <td align="left" __designer:mapid="1d3" ><asp:TextBox ID="txtChName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	        <tr __designer:mapid="1d5">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="1d6">&nbsp;Principal Parent Name:</td>
			        <td  align ="left" __designer:mapid="1d7" ><asp:TextBox ID="txtParentName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	        <tr __designer:mapid="1d9">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="1da">Spouse Name:</td>
			        <td  align ="left" __designer:mapid="1db" ><asp:TextBox ID="txtSpouseName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	         <tr __designer:mapid="1dd">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="1de">&nbsp;E-Mail:</td>
			        <td align="left" __designer:mapid="1df"><asp:TextBox ID="txtEmail" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	         <tr __designer:mapid="1e1">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="1e2">&nbsp;Phone Number Contains:</td>
			        <td align="left" __designer:mapid="1e3" >
                        <asp:TextBox ID="txtPhone" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
            	
    	        <tr __designer:mapid="1e5">
    	            <td align="right" __designer:mapid="1e6">
    	                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	                <asp:Button ID="btnclose" runat="server" OnClick="btnclose_Click" Text="Close" />
    	            </td>
    	          <%--  <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>--%>
    	        </tr>		
	        </table>
            <table  border="1" runat="server" id="tblIndSearch1" style="text-align:center"  width="30%" visible="true" bgcolor="silver" __designer:mapid="8a" >	
                 <tr __designer:mapid="8b">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="8c" style="height: 28px; width: 131px;">&nbsp;Donor Type:</td>
			        <td align="left" __designer:mapid="8d" style="height: 28px" >
                        <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server" 
                            onselectedindexchanged="ddlDonorType_SelectedIndexChanged">
                            <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                            <asp:ListItem Value="Organization">Organization</asp:ListItem>
                        </asp:DropDownList></td>
    	        </tr>
                <tr __designer:mapid="91">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="92" style="height: 28px; width: 131px;">&nbsp;Last Name:</td>
			        <td align="left" __designer:mapid="93" style="height: 28px" ><asp:TextBox ID="txtLastName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	        <tr __designer:mapid="95">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" style="height: 28px; width: 131px;" __designer:mapid="96">&nbsp;First Name:</td>
			        <td  align ="left" style="height: 28px" __designer:mapid="97" ><asp:TextBox ID="txtFirstName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	        <tr __designer:mapid="99">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="9a" style="width: 131px">&nbsp;Organization Name:</td>
			        <td  align ="left" __designer:mapid="9b" ><asp:TextBox ID="txtOrgName" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	         <tr __designer:mapid="9d">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="9e" style="width: 131px">&nbsp;E-Mail:</td>
			        <td align="left" __designer:mapid="9f"><asp:TextBox ID="txtEmail1" runat="server" onkeypress="return ValidateEnterKey(event);"></asp:TextBox></td>
    	        </tr>
    	         <tr __designer:mapid="a1">
                    <td class="ItemLabel" vAlign="top" noWrap align="right" __designer:mapid="a2" style="width: 131px">&nbsp;State:</td>
			        <td align="left" __designer:mapid="a3" >
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList></td>
    	        </tr>
            	
    	        <tr __designer:mapid="a5">
    	            <td align="right" __designer:mapid="a6" style="width: 131px">
    	                <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Find" CausesValidation="False" />
    	                <asp:Button ID="btncloseG" runat="server" OnClick="btncloseG_Click" Text="Close" />
    	            </td>
    	          <%--  <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>--%>
    	        </tr>		
	        </table>
	        <br />
        <asp:Label ID="lblchError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        <br />
        <asp:GridView ID="gvChSearch" runat="server" EnableModelValidation="True" OnRowCommand="gvChSearch_RowCommand" OnRowDataBound="gvChSearch_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select" />
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
        <br />
    </div>
    <div align="center" id="divimport" runat="server" >

        <asp:Label ID="lblimportTitle" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btndYes" runat="server" Text="Yes" OnClick="btndYes_Click" />
        &nbsp;
        <asp:Button ID="btndNo" runat="server" Text="No" OnClick="btndNo_Click" />

        <br />

        <asp:HiddenField ID="hdnYear" runat="server" />
        <br />
        <asp:GridView ID="gvPayAwards" runat="server" EnableModelValidation="True" OnRowCommand="gvAwards_RowCommand">
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>

    </div>
    <div align="left" id="divawardlist" runat ="server" >
        <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click" />
        <br />
         <br />
        <asp:GridView ID="gvAwardList" runat="server" AllowPaging="True" OnPageIndexChanging="gvAwardList_PageIndexChanging" OnRowDataBound="gvAwardList_RowDataBound" PageSize="50">
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>

    </div>
    <div align="center" id="divrecon" runat ="server">
        <asp:Button ID="btnextoexcel" runat="server" Text="Export to Excel" OnClick="btnextoexcel_Click" />
        <br />
        <br />
        <asp:GridView ID="gvReconsile" runat="server" EnableModelValidation="True" OnRowDataBound="gvReconsile_RowDataBound" OnRowCommand="gvReconsile_RowCommand">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Details" />
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
    </div>
    <div align="center" id ="divdrildown" runat="server" >
        <div align="left" >
            <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="True" Font-Italic="False" OnClick="LinkButton1_Click">Previous Page</asp:LinkButton>
        </div>
        <asp:GridView ID="gvdrilaward" runat="server" EnableModelValidation="True" OnRowDataBound="gvdrilaward_RowDataBound" Caption="USSAward" AutoGenerateColumns="False" OnRowCancelingEdit="gvdrilaward_RowCancelingEdit" OnRowEditing="gvdrilaward_RowEditing" OnRowUpdating="gvdrilaward_RowUpdating">
            <Columns>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="USSAwardID">
                    <ItemTemplate>
                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("USSAwardID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ContestYear">
                    <ItemTemplate>
                        <asp:Label ID="lblyear" runat="server" Text='<%# Eval("ContestYear") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductGroupID">
                    <ItemTemplate>
                        <asp:Label ID="lblPGID" runat="server" Text='<%# Eval("ProductGroupID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductGroupCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPGCode" runat="server" Text='<%# Eval("ProductGroupCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductID">
                    <ItemTemplate>
                        <asp:Label ID="lblPID" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPCode" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChildNumber">
                    <ItemTemplate>
                        <asp:Label ID="lblChNo" runat="server" Text='<%# Eval("ChildNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChildName">
                    <ItemTemplate>
                        <asp:Label ID="lblChName" runat="server" Text='<%# Eval("ChildName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade">
                    <ItemTemplate>
                        <asp:Label ID="lblGrade" runat="server" Text='<%# Eval("Grade") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rank">
                    <ItemTemplate>
                        <asp:Label ID="lblRank" runat="server" Text='<%# Eval("Rank") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GrossAmount">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtGrossAmount" runat="server" Text='<%# Eval("GrossAmount") %>' Width="75px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="rev11" runat="server" ControlToValidate="txtGrossAmount" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblGrossAmount" runat="server" Text='<%# Eval("GrossAmount") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SponsorID">
                    <ItemTemplate>
                        <asp:Label ID="lblSpID" runat="server" Text='<%# Eval("SponsorID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SponsorName">
                    <ItemTemplate>
                        <asp:Label ID="lblSpName" runat="server" Text='<%# Eval("SponsorName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DonorType">
                    <ItemTemplate>
                        <asp:Label ID="lblDtype" runat="server" Text='<%# Eval("DonorType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PaidFull">
                    <ItemTemplate>
                        <asp:Label ID="lblPaidFull" runat="server" Text='<%# Eval("PaidFull") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView> 
        <br />
        <br />
       <asp:GridView ID="gvdrilpayment" runat="server" EnableModelValidation="True" OnRowDataBound="gvdrilpayment_RowDataBound" Caption="USSPayment" AutoGenerateColumns="False" OnRowCancelingEdit="gvdrilpayment_RowCancelingEdit" OnRowEditing="gvdrilpayment_RowEditing" OnRowUpdating="gvdrilpayment_RowUpdating">
           <Columns>
               <asp:TemplateField>
                   <EditItemTemplate>
                       <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                       <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="USSPaidID">
                   <ItemTemplate>
                       <asp:Label ID="lblId" runat="server" Text='<%# Eval("USSPaidID") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Childnumber">
                   <ItemTemplate>
                       <asp:Label ID="lblChNo" runat="server" Text='<%# Eval("ChildNumber") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ChildName">
                   <ItemTemplate>
                       <asp:Label ID="lblChName" runat="server" Text='<%# Eval("ChildName") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="MemberID">
                   <ItemTemplate>
                       <asp:Label ID="lblMID" runat="server" Text='<%# Eval("MemberID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PaidAmount">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtPaidAmt" runat="server" Text='<%# Eval("PaidAmount") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtPaidAmt" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblPaidAmt" runat="server" Text='<%# Eval("PaidAmount") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="RestAmt">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtRestAmt" runat="server" Text='<%# Eval("RestAmt") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev2" runat="server" ControlToValidate="txtRestAmt" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblRestAmt" runat="server" Text='<%# Eval("RestAmt") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="UnrestAmt">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtUrAmt" runat="server" Text='<%# Eval("UnrestAmt") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev3" runat="server" ControlToValidate="txtUrAmt" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblUnrestAmt" runat="server" Text='<%# Eval("UnrestAmt") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ExcessOfCap">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtExcessC" runat="server" Text='<%# Eval("ExcessOfCap") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev4" runat="server" ControlToValidate="txtExcessC" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblExcess" runat="server" Text='<%# Eval("ExcessOfCap") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Interest">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtIntrst" runat="server" Text='<%# Eval("Interest") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev5" runat="server" ControlToValidate="txtIntrst" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblImtrest" runat="server" Text='<%# Eval("Interest") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="DatePaid">
                   <ItemTemplate>
                       <asp:Label ID="lblDtPaid" runat="server" Text='<%# Eval("DatePaid") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="CheckNumber">
                   <ItemTemplate>
                       <asp:Label ID="lblCkNum" runat="server" Text='<%# Eval("CheckNumber") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="BankID">
                   <ItemTemplate>
                       <asp:Label ID="lblBID" runat="server" Text='<%# Eval("BankID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="GranteeID">
                   <ItemTemplate>
                       <asp:Label ID="lblGTID" runat="server" Text='<%# Eval("GranteeID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="GranteeName">
                   <ItemTemplate>
                       <asp:Label ID="lblGTName" runat="server" Text='<%# Eval("GranteeName") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="DonorType">
                   <ItemTemplate>
                       <asp:Label ID="lblDonorType" runat="server" Text='<%# Eval("DonorType") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PaidFull">
                   <ItemTemplate>
                       <asp:Label ID="lblPaidFull" runat="server" Text='<%# Eval("PaidFull") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>
           <RowStyle HorizontalAlign="Left" />
        </asp:GridView> 

    </div>
    <div align ="left" id="divSummary" runat ="server" >

        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Year From :"></asp:Label>
        <asp:DropDownList ID="ddlYearFrom" runat="server">
        </asp:DropDownList> 
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Year To:"></asp:Label>
        <asp:DropDownList ID="ddlYearTo" runat="server">
        </asp:DropDownList>
        <asp:Button ID="btnCalculate" runat="server" OnClick="btnCalculate_Click" Text="Submit" />
        <asp:Button ID="btnExportSummary" runat="server" Enabled="False" OnClick="btnExportSummary_Click" Text="Export to Excel" />
        <br />
        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        <br />
        <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" EnableModelValidation="True" OnRowCommand="GridView1_RowCommand">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Details" />
            </Columns>
        </asp:GridView>      

    </div>
    <div align="left" id="divsumdrildown" runat="server">

        <asp:LinkButton ID="LinkButton4" runat="server" Font-Bold="True" OnClick="LinkButton4_Click">Back to Summary</asp:LinkButton>
        <asp:GridView ID="gvDinfo" runat="server" Caption="DonationsInfo" OnRowDataBound="gvDinfo_RowDataBound">
        </asp:GridView>
        <br />
        <asp:GridView ID="gvAwardDetails1" runat="server" EnableModelValidation="True"  Caption="USSAward" AutoGenerateColumns="False" OnRowCancelingEdit="gvAwardDetails1_RowCancelingEdit" OnRowCommand="gvAwardDetails1_RowCommand" OnRowDataBound="gvAwardDetails1_RowDataBound" OnRowEditing="gvAwardDetails1_RowEditing" OnRowUpdating="gvAwardDetails1_RowUpdating" >
            <Columns>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:Button ID="btnUpdate0" runat="server" CommandName="Update" Text="Update" />
                        <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="btnEdit0" runat="server" CommandName="Edit" Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="USSAwardID">
                    <ItemTemplate>
                        <asp:Label ID="lblId1" runat="server" Text='<%# Eval("USSAwardID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ContestYear">
                    <ItemTemplate>
                        <asp:Label ID="lblyear0" runat="server" Text='<%# Eval("ContestYear") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductGroupID">
                    <ItemTemplate>
                        <asp:Label ID="lblPGID0" runat="server" Text='<%# Eval("ProductGroupID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductGroupCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPGCode0" runat="server" Text='<%# Eval("ProductGroupCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductID">
                    <ItemTemplate>
                        <asp:Label ID="lblPID0" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPCode0" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChildNumber">
                    <ItemTemplate>
                        <asp:Label ID="lblChNo0" runat="server" Text='<%# Eval("ChildNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChildName">
                    <ItemTemplate>
                        <asp:Label ID="lblChName0" runat="server" Text='<%# Eval("ChildName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade">
                    <ItemTemplate>
                        <asp:Label ID="lblGrade0" runat="server" Text='<%# Eval("Grade") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rank">
                    <ItemTemplate>
                        <asp:Label ID="lblRank0" runat="server" Text='<%# Eval("Rank") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GrossAmount">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtGrossAmount1" runat="server" Text='<%# Eval("GrossAmount") %>' Width="75px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="drev12" runat="server" ControlToValidate="txtGrossAmount1" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblGrossAmount0" runat="server" Text='<%# Eval("GrossAmount") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DepositeDate">
                    <ItemTemplate>
                        <asp:Label ID="dlbDdate" runat="server" Text='<%# Eval("DepositeDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SponsorID">
                    <ItemTemplate>
                        <asp:Label ID="lblSpID0" runat="server" Text='<%# Eval("SponsorID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SponsorName">
                    <ItemTemplate>
                        <asp:Label ID="lblSpName0" runat="server" Text='<%# Eval("SponsorName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DonorType">
                    <ItemTemplate>
                        <asp:Label ID="lblDtype0" runat="server" Text='<%# Eval("DonorType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PaidFull">
                    <ItemTemplate>
                        <asp:Label ID="lblPaidFull0" runat="server" Text='<%# Eval("PaidFull") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView> 
        <br />
        
       <asp:GridView ID="gvPaymentDetails" runat="server" EnableModelValidation="True" Caption="USSPayment" AutoGenerateColumns="False" OnRowCancelingEdit="gvPaymentDetails_RowCancelingEdit" OnRowDataBound="gvPaymentDetails_RowDataBound1" OnRowEditing="gvPaymentDetails_RowEditing" OnRowUpdating="gvPaymentDetails_RowUpdating">
           <Columns>
               <asp:TemplateField>
                   <EditItemTemplate>
                       <asp:Button ID="btnUpdate1" runat="server" CommandName="Update" Text="Update" />
                       <asp:Button ID="btnCancel1" runat="server" CommandName="Cancel" Text="Cancel" />
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Button ID="btnEdit2" runat="server" CommandName="Edit" Text="Edit" />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="USSPaidID">
                   <ItemTemplate>
                       <asp:Label ID="lblId2" runat="server" Text='<%# Eval("USSPaidID") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Childnumber">
                   <ItemTemplate>
                       <asp:Label ID="lblChNo1" runat="server" Text='<%# Eval("ChildNumber") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ChildName">
                   <ItemTemplate>
                       <asp:Label ID="lblChName1" runat="server" Text='<%# Eval("ChildName") %>' />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="MemberID">
                   <ItemTemplate>
                       <asp:Label ID="lblMID0" runat="server" Text='<%# Eval("MemberID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PaidAmount">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtPaidAmt2" runat="server" Text='<%# Eval("PaidAmount") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev122" runat="server" ControlToValidate="txtPaidAmt2" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblPaidAmt0" runat="server" Text='<%# Eval("PaidAmount") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="RestAmt">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtRestAmt2" runat="server" Text='<%# Eval("RestAmt") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev132" runat="server" ControlToValidate="txtRestAmt2" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblRestAmt0" runat="server" Text='<%# Eval("RestAmt") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="UnrestAmt">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtUrAmt2" runat="server" Text='<%# Eval("UnrestAmt") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev142" runat="server" ControlToValidate="txtUrAmt2" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblUnrestAmt0" runat="server" Text='<%# Eval("UnrestAmt") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ExcessOfCap">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtExcessC2" runat="server" Text='<%# Eval("ExcessOfCap") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev152" runat="server" ControlToValidate="txtExcessC2" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblExcess0" runat="server" Text='<%# Eval("ExcessOfCap") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Interest">
                   <EditItemTemplate>
                       <asp:TextBox ID="txtIntrst2" runat="server" Text='<%# Eval("Interest") %>' Width="75px"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rev162" runat="server" ControlToValidate="txtIntrst2" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
                   </EditItemTemplate>
                   <ItemTemplate>
                       <asp:Label ID="lblImtrest0" runat="server" Text='<%# Eval("Interest") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="DatePaid">
                   <ItemTemplate>
                       <asp:Label ID="lblDtPaid0" runat="server" Text='<%# Eval("DatePaid") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="CheckNumber">
                   <ItemTemplate>
                       <asp:Label ID="lblCkNum0" runat="server" Text='<%# Eval("CheckNumber") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="BankID">
                   <ItemTemplate>
                       <asp:Label ID="lblBID0" runat="server" Text='<%# Eval("BankID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="GranteeID">
                   <ItemTemplate>
                       <asp:Label ID="lblGTID0" runat="server" Text='<%# Eval("GranteeID") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="GranteeName">
                   <ItemTemplate>
                       <asp:Label ID="lblGTName0" runat="server" Text='<%# Eval("GranteeName") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="DonorType">
                   <ItemTemplate>
                       <asp:Label ID="lblDonorType2" runat="server" Text='<%# Eval("DonorType") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PaidFull">
                   <ItemTemplate>
                       <asp:Label ID="lblPaidFull1" runat="server" Text='<%# Eval("PaidFull") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>
           <RowStyle HorizontalAlign="Left" />
        </asp:GridView> 

    </div>
    <div style="float: left; width: 85%; text-align: center;">    <asp:Label ID="lborg" runat="server" Text="Organization" ForeColor="#3366FF" Visible="false" ></asp:Label></div>
<br />
<div> 
    
     <asp:GridView ID="GVliability" runat="server" AllowPaging="true" PageSize="50" onpageindexchanging="GVliability_PageIndexChanging"  
            >
        </asp:GridView></div>
    <div> 
    
     <asp:GridView ID="Gvlist" runat="server"   
            >
        </asp:GridView></div>
        <br />
         <div style="float: left; width: 85%; text-align: center;"> 
       <asp:Label ID="lbind" runat="server" Text="Individual / Spouse" 
        ForeColor="#3366FF" Visible="false"></asp:Label></div>
        <br />
        <div></div>
         <div>
     <asp:GridView ID="Gvgrantee" runat="server" >
        </asp:GridView></div>
</asp:Content>

