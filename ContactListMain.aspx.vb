Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL

Imports Microsoft.ApplicationBlocks.Data
Partial Class ContactListMain
    Inherits System.Web.UI.Page


    Protected Sub btnSubmit_onclick(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim userEmail As String
        userEmail = email.Text
        Dim type As String = ddlOption.SelectedValue.ToString

        Dim conn As SqlConnection
        conn = New SqlConnection(Application("ConnectionString"))

        Dim dr As SqlDataReader
        Dim loginId As Integer
        Try
            Dim strSql As StringBuilder = New StringBuilder
            strSql.Append("select automemberid from indspouse where email='" + Server.HtmlEncode(userEmail) + "'")
            strSql.Append(" and automemberid in ( select distinct memberid from volunteer ) ")
            dr = SqlHelper.ExecuteReader(Application("connectionString"), CommandType.Text, strSql.ToString())
            ' Iterate through DataReader, should only be one 
            While (dr.Read())
                If Not dr.IsDBNull(0) Then
                    loginId = CInt(dr("automemberid"))
                End If
            End While
            If Not dr Is Nothing Then dr.Close()
        Finally
            dr = Nothing
        End Try
        Select Case type
            Case "Admin"
                Session("RoleId") = 1
            Case "NationalC"
                Session("RoleId") = 2
            Case "ZonalC"
                Session("RoleId") = 3
            Case "ClusterC"
                Session("RoleId") = 4
            Case "ChapterC"
                Session("RoleId") = 5
            Case "FinalsCoreT"
                Session("RoleId") = 36
            Case "TeamLead"
                Session("RoleId") = 6
        End Select
        Session("LoginID") = loginId

        Response.Redirect("CoachClassContactList.aspx")
    End Sub

End Class

