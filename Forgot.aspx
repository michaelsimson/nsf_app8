<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.Forgot" CodeFile="Forgot.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
			
			<table align="center">
				<TBODY>
					<tr>
						<td>
							<h4 align="center">Forgot Password</h4>
						</td>
					</tr>
					<tr>
						<td>Enter your e-mail address and click Send. Your password will be e-mailed to 
							your
						</td>
					</tr>
					<tr>
						<td><div><asp:label id="Label3" runat="server">E-Mail </asp:label><asp:textbox id="txtEmail" runat="server" CssClass="SmallFont" MaxLength="50" Width="300px"></asp:textbox>
								<asp:RegularExpressionValidator id="regEmail" runat="server" ErrorMessage="Please type in a Valid Email address"
									ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="SmallFont" ControlToValidate="txtEmail"
									Display="Dynamic"></asp:RegularExpressionValidator></div>
						</td>
					</tr>
					<tr>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td align="center"><div><asp:button id="btnSend" runat="server" Text="Send" CssClass="FormButtonCenter"></asp:button></div>
						</td>
					</tr>
					<tr>
						<td><asp:label id="lblConfirm" runat="server" Visible="False" CssClass="SmallFont" ForeColor="green"
								Font-Bold="True">Password has been sent your email address.</asp:label></td>
					</tr>
					<tr>
						<td><asp:label id="lblErrMsg" runat="server" CssClass="SmallFont" ForeColor="red" Font-Bold="True">Email address could not be found. Please Contact us at <A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com </U></A>.</asp:label></td>
					</tr>					
				</TBODY>
			</table>
			
		</asp:Content>
 

 
 
 