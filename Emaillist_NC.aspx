<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Emaillist_NC.aspx.vb" Inherits="Emaillist_NC" title="Untitled Page" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table style="width: 680px">
<tr><td style="width: 300px; height: 18px;">
<asp:HyperLink ID="hybbackvol" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="254px" ></asp:HyperLink>
</td><td><asp:HyperLink ID="Hypbackemail" runat="server" Text="Back to Email" NavigateUrl="~/email_NC.aspx" Width="284px" ></asp:HyperLink>
</td></tr></table>
<table id="tableemail" cellspacing="1" cellpadding="3" width="700" style="margin-left:10px" border="0" runat="server">
<tr><td><asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label> </td></tr>
      <tr><td><asp:Label ID="lblfrom" runat="server" Text="From:" Width="50px" ></asp:Label> 
      <asp:TextBox ID="txtfrom" runat="server" Enabled="false" BackColor="#FFE0C0" ></asp:TextBox></td></tr>
      <tr>
    <td><asp:Label ID="lblemailsub" runat="server" Text="Please Type the Subject of the Email Below" ></asp:Label> 
    </td></tr><tr><td style="height: 14px">
    <asp:TextBox id="txtEmailSubject" runat="server" Width="800px" MaxLength="1000" BackColor="#FFE0C0"></asp:TextBox>&nbsp;
    </td></tr>
     <tr style="height: 25px;">
    <td><asp:Label ID="lblattfile" Text="File to be Attached:" runat="server"></asp:Label> <asp:FileUpload ID="AttachmentFile" runat="server"/></td>
    
    </tr><tr><td>
		<asp:Label ID="lblemailbody" Text="Please Type the Body of the Email Below" runat="server" ></asp:Label> 
        <FTB:FreeTextBox EnableHtmlMode="true" id="txtEmailBody"  runat="Server" Width="99%" Focus="false" SupportFolder="aspnet_client/FreeTextBox/" />
    </td></tr>
    <tr><td>
				<asp:Button id="btnsendemail" runat="server" Text="Send Email" CssClass="FormButton"></asp:Button>
			</td></tr><tr><td>
					<asp:label id="lblEMailError" runat="server"></asp:label></td></tr>
		</table>
                    <table>
					<tr><td style="height:5px"><asp:TextBox id="txtSendList" runat="server" Width="800px" Enabled="false" TextMode="MultiLine" BorderColor="Black">
					</asp:TextBox></td></tr>
    </table>
</asp:Content>


