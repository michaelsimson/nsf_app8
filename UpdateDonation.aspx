<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" Debug="true"  AutoEventWireup="false" CodeFile="UpdateDonation.aspx.vb" Inherits="UpdateDonation" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

    <script language="javascript" type="text/javascript">
        function numeric()
          {   
		  if((event.keyCode > 64 && event.keyCode<91) || (event.keyCode > 96 && event.keyCode<123)||(event.keyCode >= 32 && event.keyCode<=45) || (event.keyCode >= 58 && event.keyCode<=64) ||(event.keyCode >= 91 && event.keyCode<=96) ||(event.keyCode >= 123 && event.keyCode<=126)||(event.keyCode==47) ) 
			{
			event.keyCode=0;
			}
			else	
			 {
			 event.keyCode=event.keyCode;
			 }
        }        
    </script>    
        <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >                
			    <tr bgcolor="#FFFFFF" align="middle" >
			        <td class="Heading" colSpan="2" align="center" >Update Donation</td>
				</tr>
				<tr bgcolor="#FFFFFF" > 
				    <td colspan="2"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/volunteerfunctions.aspx">Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				    <asp:hyperlink id="hlnkSearch" runat="server" NavigateUrl="~/dbsearchresults.aspx">Back to Search Results</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				    <asp:HyperLink runat="server" id="lnkMenu" NavigateUrl="~/ViewDonations.aspx">Back to Donations List</asp:HyperLink></td>
                </tr>
                <tr>
					<td vAlign="top" width="50%">
						<table id="tblIndividual" align="center" style="width: 845px">
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td  valign="top" align="left" style="height: 25px" >Donation [US $]:</td>
			        <td style="width: 313px; height: 25px;"  ><asp:TextBox CssClass="inputBox" runat="server" ID="txtDonation" MaxLength="15" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDonation"
                            ErrorMessage="Enter Amount"></asp:RequiredFieldValidator>
                              </td><td style="height: 25px; width: 200px;">
                        <asp:Label ID="lbldonationtype" runat="server" Text="Donation Type: " Width="175px"></asp:Label></td>
                        <td style="height: 25px;">                                    
                        <asp:DropDownList ID="drpdonationtype" runat="server" Width="120px">
                        <asp:ListItem Value="1" Text="Unrestricted" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Perm Restricted"></asp:ListItem></asp:DropDownList>
                        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td  valign="top" align="left" >Check/Transaction#:</td>
			        <td style="width: 313px;" ><asp:TextBox CssClass="inputBox" runat="server" ID="txtTrnsNumber" MaxLength="50" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTrnsNumber"
                            ErrorMessage="Enter Data"></asp:RequiredFieldValidator></td>
                            <td style="width: 200px" ><asp:Label ID="lblchapterid" runat="server" Text="ChapterId: " Width="180px"></asp:Label>
                       </td><td ><asp:DropDownList ID="drpchapterid" runat="server" Width="120px"></asp:DropDownList>
                        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px" >
			        <td  valign="top" align="left" >Donation Date:</td>
			        <td style="width: 313px"  ><asp:TextBox CssClass="inputBox" runat="server" ID="txtDonationDt" MaxLength="10" Width="100px"></asp:TextBox>
                         <asp:Label ID="lblErrDate" runat="server" ForeColor="red"></asp:Label></td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDonationDt"
                            ErrorMessage="Enter Date in MM/DD/YYYY" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator></td>
			    <td  valign="top" align="left" >Method:</td>
			        <td style="width: 200px"  >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlMethod" Width="120px">
			                <asp:ListItem Text="Check" Value="Check"></asp:ListItem>
			                <asp:ListItem Text="Credit Card" Value="Credit Card"></asp:ListItem>
			                <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
			            </asp:DropDownList>
			        </td></tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px" >
			        <td  valign="top" align="left" >Anonymous:</td>
			        <td style="width: 313px"  ><asp:DropDownList runat="server" id="ddlAnonymous">
			            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			            <asp:ListItem Text="No" Value="No"></asp:ListItem>
			        </asp:DropDownList>
			        </td> 

                     <td style="width: 200px"> <asp:Label ID="lbldsn" runat="server" Text="Deposit Slip#" Width="180px"></asp:Label> </td> 
			       <td><asp:TextBox ID="txtdsn" runat="server" Width="118px" ></asp:TextBox>
			        <asp:Label ID="lblerr1" runat="server" ForeColor="red"></asp:Label></td>
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			       <td >
			       <asp:Label ID="lblddate" runat="server" Text="Deposit Date:"></asp:Label> 
			       </td>
			       <td style=" width: 313px;">
			       <asp:TextBox  CssClass="inputBox" ID="txtddate" runat="server" Width="100px" ></asp:TextBox>
			        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtddate"
                            ErrorMessage="Enter Date in MM/DD/YYYY" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>--%> 
			        <asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label></td>
			        
			     
			         
			          <td >
                Bank :
            </td>
            <td style="width: 320px" >
           <asp:DropDownList ID="ddlBankId" DataTextField="BankCode" DataValueField ="BankID" runat="server" Width="120px" />
            </td>
			      <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtddate"
                            ErrorMessage="Enter Deposit Date"></asp:RequiredFieldValidator>--%>
                        
			      <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtdsn"
                            ErrorMessage="Enter Deposit Slip NUmber"></asp:RequiredFieldValidator>--%>
                    
			      
			    </tr>
			    <tr><td><asp:Label ID="lblProject" runat="server" Text="Project:"></asp:Label> </td>
			    <td style=" width: 313px;">
			       <asp:TextBox  CssClass="inputBox" ID="txtProject" runat="server" Width="100px" ></asp:TextBox>
			       <asp:Label ID="lblPrjtErr" runat="server" Text=""></asp:Label> </td>
			   <td  valign="top" align="left" style="height: 24px; width: 200px;" >Donation Event:</td>
			        <td style="width: 334px; height: 24px;"  >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlDonationEvent" Width="210px">   
			             
			            </asp:DropDownList>
                        <asp:RequiredFieldValidator ControlToValidate="ddlDonationEvent" ID="RequiredFieldValidator6" runat="server" ErrorMessage="Select Event"></asp:RequiredFieldValidator></td>
			      
			    </tr>
               
			  
       
			    <tr bgcolor="#FFFFFF" style="height: 25px" >
			        <td  valign="top" align="left" style="height: 24px" >Purpose:</td>
			        <td style="width: 313px; height: 24px;"  >
			            <asp:DropDownList  CssClass="inputBox" runat="server" ID="ddlDonationFor" Width="170px">   			                
			            </asp:DropDownList>
                        <asp:RequiredFieldValidator ControlToValidate="ddlDonationFor" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Purpose"></asp:RequiredFieldValidator></td>
			   <%-- </tr>
			    <tr bgcolor="#FFFFFF">--%>
			   <td style="width: 100px" >Supp Code:</td>
			        <td style="width: 320px" ><asp:DropDownList CssClass="inputBox" runat="server" id="DDSuppCode" Width="150px">			            
			             <asp:ListItem Value="0" >Select SuppCode</asp:ListItem>
                                <asp:ListItem Text="Advertising" Value="Advertising"  >Advertising</asp:ListItem>
                                <asp:ListItem Text="Award"  Value="Award">Award</asp:ListItem>
                                <asp:ListItem Text="Employee Gift" Value="Employee Gift" >Employee Gift</asp:ListItem>
                                 <asp:ListItem Text="Matching Gift" Value="Matching Gift" >Matching Gift</asp:ListItem>
                                 <asp:ListItem Text="Sponsorship" Value="Sponsorship" >Sponsorship</asp:ListItem>
                                <asp:ListItem Text="Volunteer Hours"  Value="Volunteer Hours">Volunteer Hours</asp:ListItem>
                                 <asp:ListItem Text="Other" Value="Other">Other</asp:ListItem>
                                  <asp:ListItem Text="None"  Value="None" >None</asp:ListItem>
			        </asp:DropDownList></td>
                <tr>
                <td  valign="top" align="left" >Endowment:</td>
			        <td style="width: 313px"  >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlEndowment" Width="170px">   
			                
			            </asp:DropDownList>
			        </td>

                           <td  valign="top" align="left" style="width: 200px" >In Memory Of:</td>
			        <td style="width: 268px"  ><asp:TextBox runat="server" ID="txtInNameOf" MaxLength="100" Width="283px"></asp:TextBox></td>
                        
                </tr>
			    <tr  runat = "server" id="TrEndowment" bgcolor="#FFFFFF" style="height: 25px">
			        
                     <td  valign="top" align="left" >
                        EventYear:</td>
			        <td style="width: 313px"  >  <asp:DropDownList runat="server" id="ddlEventYear" CssClass="inputBox" Width="120px">
			            </asp:DropDownList></td>


                      <td  valign="top" align="left" style="width: 200px" >Matching Employer:</td>
			        <td style="width: 334px"><!--<asp:TextBox CssClass="inputBox" runat="server" ID="txtEmployer" MaxLength="50" ></asp:TextBox>-->
			            <asp:DropDownList runat="server" id="ddlEmployer" CssClass="inputBox" Width="290px">
			            </asp:DropDownList>
			        </td> 
			    </tr>
			    <tr  runat = "server" id="TrDesign" bgcolor="#FFFFFF" style="height: 25px" >
			        <td  valign="top" align="left" >Designation:</td>
			        <td style="width: 313px"  >
			            <asp:DropDownList  CssClass="inputBox" runat="server" ID="ddlDesignation" Width="100px">   
			                <asp:ListItem Text="None" Value=""></asp:ListItem>
			                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
			                <asp:ListItem Text="College" Value="College"></asp:ListItem>
			                <asp:ListItem Text="High School" Value="High School"></asp:ListItem>
			                <asp:ListItem Text="Primary School" Value="Primary School"></asp:ListItem>			                
			            </asp:DropDownList>
			        </td>
			    <%--</tr>
			    <tr bgcolor="#FFFFFF" style="height: 40px">
			    --%>   
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td  valign="top" align="left" >
                        Status:</td>
			        <td style="width: 313px"  >
			            <asp:DropDownList  CssClass="inputBox" runat="server" ID="ddlEvent" Width="100px">
			                <asp:ListItem Text="None" Value="None"></asp:ListItem>
			                <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
			                <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
			            </asp:DropDownList>
                        <asp:RequiredFieldValidator ControlToValidate="ddlEvent" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Valid Status"></asp:RequiredFieldValidator></td>
			    </tr>
			    <tr  runat = "server" id="Trmatch" bgcolor="#FFFFFF" style="height: 25px">
			        <td  valign="top" align="left" >Matching Flag:</td>
			        <td style="width: 313px"  >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlMachingFlag">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>			                
			            </asp:DropDownList>
			            <asp:Label ID="lbl3" runat="server" ForeColor="red"></asp:Label>
			         </td>
			    <%--</tr>			    
			    
			    <tr bgcolor="#FFFFFF" style="height: 40px">
			    --%>   
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td  valign="top" align="left" >
                        Remarks:</td>
			        <td style="width: 313px"  ><asp:TextBox CssClass="inputBox" runat="server" ID="txtRemarks" TextMode="MultiLine" MaxLength="150" ></asp:TextBox></td>
			   
			    </tr>
                  <tr runat = "server" id="TrEduGame" bgcolor="#FFFFFF" >
            <td >
                 <asp:Label ID="lblDdlGame" runat="server" Text="Education Game:"></asp:Label>
            </td>
            <td style="width: 313px" >
                <asp:DropDownList runat="server" ID="dllEGames" Width="100px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredfieldvalidatorGame" runat="server"
                    ControlToValidate="dllEGames" ErrorMessage="Select Education Games"
                    CssClass="mediumwording"></asp:RequiredFieldValidator>
            </td><td></td><td></td>
        
        </tr>
			    <tr bgcolor="#FFFFFF" style="height: 40px">
					<td class="ItemCenter" align="center" colSpan="2">
					    <asp:button id="btnContinue" runat="server" CssClass="FormButton" Text="Update"></asp:button><br />
                        <asp:Label ID="lblError" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>                        
				</tr>
			</table>
			
</table>
			
</asp:Content>



 
 
 