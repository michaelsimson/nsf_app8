<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="RoomListFacilities.aspx.cs" Inherits="RoomListFacilities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server" >

<div align="left" >
     <asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
  
<table id="RoomListTable" runat="server" align="center">
    <tr  align = "center" ><td style="width: 109px" >  Room List Facilities </td></tr>
</table>

<table id="RoomListSelTable" runat="server" style="height: 39px; width: 1100px">

   <tr id="TrYear" runat="server">  
      <td align="left" style="width: 109px"> ContestYear</td>
        <td>
                <asp:DropDownList ID="ddlContestYear" AutoPostBack="true" runat="server" width="150px" onselectedindexchanged="ddlContestYear_SelectedIndexChanged" >
                </asp:DropDownList>
        </td>
     <td align="left" style="width: 77px"> </td>
       <td width="100px"> Event</td>
        <td>
                <asp:DropDownList ID="ddlEvent" AutoPostBack="true" runat="server" 
                    width="150px" onselectedindexchanged="ddlEvent_SelectedIndexChanged">
                </asp:DropDownList>
        </td>
     <td align="left" style="width: 77px"></td>
       <td width="100px" runat ="server"> Chapter</td>
        <td>
                <asp:DropDownList ID="ddlChapter" AutoPostBack = "True" runat="server" 
                    width="150px" onselectedindexchanged="ddlChapter_SelectedIndexChanged">
                </asp:DropDownList>
         </td>
      <td align="left" style="width: 77px"></td>
      <td id="TdContestDate" width="100px" runat ="server" Visible="False"> ContestDates</td>
      <td>
                <asp:DropDownList ID="ddlContestDate" AutoPostBack = "True" runat="server"  DataTextField="ContestDate"  DataValueField="ContestDate"
                    width="150px" onselectedindexchanged="ddlContestDate_SelectedIndexChanged" Visible="False">
                </asp:DropDownList>
      </td>   
         
         
     <td align="left" style="width: 77px"></td>
      <td width="100px" runat ="server"> Venue</td> 
        <td>
                <asp:TextBox ID="TextVendor" runat="server" width="150px" 
                    Visible ="True">
                </asp:TextBox>
                
        </td>
   </tr>
   <tr  id ="TrBldg" runat="server">                                                                       
      <td align="left" style="width: 109px">BldgID</td>
      <td>
                <asp:TextBox ID="TextBldgID"  runat="server" width="150px" Visible ="True">
                </asp:TextBox>
                <asp:DropDownList ID="ddlBldgID" AutoPostBack = "True" runat="server" 
                    width="150px" onselectedindexchanged="ddlBldgID_SelectedIndexChanged">
                </asp:DropDownList>
      </td>
      <td align="left" style="width: 77px"> </td>
      <td width="100px"> BldgName </td>
      <td>
                <asp:TextBox ID="TextBldgName" runat="server" width="150px" Visible ="True">
                </asp:TextBox>
                <asp:DropDownList ID="ddlBldgName" AutoPostBack = "True" runat="server" width="150px"
                 onselectedindexchanged="ddlBldgName_SelectedIndexChanged">
                </asp:DropDownList>
      </td>
      <td align="left" style="width: 77px"></td>
    </tr>
    <tr><td></td></tr>
     <tr align="center"><td>Rooms</td></tr>
    </table>
   <%--<form id="Roomform" runat ="server">--%>
   <asp:table ID="RoomListTableEnd" runat="server">
    </asp:table> 
			
  <asp:datagrid id="DataGridRoom" style=" width: 1200px;" CellSpacing="25"
				runat="server" AutoGenerateColumns="False"  AutoPostBack="false"
        OnItemDataBound="DataGridRoom_ItemDataBound" >
                      		
        		<Columns>
					<asp:TemplateColumn HeaderText="">
						<ItemTemplate>
							<asp:Label id="LabelName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"LabelName")%>'>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="1" runat="server">
						<ItemTemplate>
					        <asp:DropDownList id="Col1" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue">
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol1" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
                   <asp:TemplateColumn HeaderText="2">
						<ItemTemplate>
							<asp:DropDownList id="Col2" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue">
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol2" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
        				</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="3">
						<ItemTemplate>
							<asp:DropDownList id="Col3" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue">
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol3" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="4">
						<ItemTemplate>
							<asp:DropDownList id="Col4" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol4" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="5">
						<ItemTemplate>
							<asp:DropDownList id="Col5" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol5" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="6">
						<ItemTemplate>
							<asp:DropDownList id="Col6" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol6" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="7">
						<ItemTemplate>
							<asp:DropDownList id="Col7" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol7" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="8">
						<ItemTemplate>
							<asp:DropDownList id="Col8" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol8" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="9">
						<ItemTemplate>
							<asp:DropDownList id="Col9" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol9" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="10">
						<ItemTemplate>
							<asp:DropDownList id="Col10" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol10" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="11">
						<ItemTemplate>
							<asp:DropDownList id="Col11" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol11" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="12">
						<ItemTemplate>
							<asp:DropDownList id="Col12" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol12" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="13">
						<ItemTemplate>
							<asp:DropDownList id="Col13" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol13" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="14">
						<ItemTemplate>
							<asp:DropDownList id="Col14" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol14" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="15">
						<ItemTemplate>
							<asp:DropDownList id="Col15" width="90px" runat="server" AutoPostBack="false" DataTextField ="ddlText" DataValueField="ddlValue" >
							</asp:DropDownList>
							<asp:TextBox ID= "TextCol15" width="90px" runat="server" AutoPostBack="false" visible="false" ></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
			</Columns>
			</asp:datagrid>
			<table id= "buttons" visible="true" runat="server" 
            style="margin-left: 0px; width: 1129px;">
	        <br>
	        <tr align ="center"><td align="center"> 
                <asp:Button ID="BtnAddUpdate" runat="server" Text="Add/Update" 
                    onclick="BtnAddUpdate_Click" />&nbsp;
                <asp:Button ID="BtnUpdateCancel" runat="server" Text="Cancel" 
                    onclick="BtnUpdateCancel_Click" />
            </td></tr>
            <tr align="center"><td align="center"><asp:Label ID="LblAddUpdate" ForeColor="Red" runat ="server"></asp:Label></td></tr>
            <tr align="center"><td align="center"><asp:Label ID="LblError" ForeColor="Red" runat="server"></asp:Label></td></tr>
            <tr align="center"><td align="center"><asp:Label ID="LblRecError" ForeColor="Red" runat ="server"></asp:Label></td></tr>  
	        </table>
			
<asp:DataGrid ID="DGRoomList" runat="server" AutoGenerateColumns="False" CellPadding="4" 
       Borderwidth="2px" width="1200px">
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                    />
               <ItemStyle BackColor="White" />
               <COLUMNS>

            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Chapter"  HeaderText="Chapter" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgName" HeaderText="BldgName"/>
             <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="RoomNumber" HeaderText="RoomNumber"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Floor" HeaderText="Floor"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Gallery" HeaderText="Gallery"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="FlippersToWrite" HeaderText="FlippersToWrite"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="TablesToWrite" HeaderText="TablesToWrite" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="FixedChairs" HeaderText="FixedChairs" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Podium" HeaderText="Podium" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="PodMic" HeaderText="PodMic"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="PodLT" HeaderText="PodLT"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Projector" HeaderText="Projector"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="JTable" HeaderText="JTable" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="JChair" HeaderText="JChair" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="CJMic" HeaderText="CJMic"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChildMic" HeaderText="ChildMic"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="AMixer" HeaderText="AMixer"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChMicStand" HeaderText="ChMicStand" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="DeskMicStand" HeaderText="DeskMicStand"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="AWires" HeaderText="AWires"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ExtraMic" HeaderText="ExtraMic"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="IntAccess" HeaderText="IntAccess" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Laptop" HeaderText="Laptop"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Tables" HeaderText="Tables"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Chair" HeaderText="Chair"/>
           <%-- <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Mic" HeaderText="Mic" />--%>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="MicStand" HeaderText="MicStand" />
  </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>			
	
	 
	
 </asp:Content>