﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="TeamPlanning.aspx.cs" Inherits="TeamPlanning_TeamPlanning" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
     
    <table id="tblTeamSettings" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Team Planning</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <asp:DropDownList ID="ddlTeamPlanning" runat="server" Width="150px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlTeamPlanning_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="Team Plan Settings">Team Plan Settings</asp:ListItem>
                        <asp:ListItem Value="Team Schedule">Team Schedule</asp:ListItem>
                    </asp:DropDownList>
                </center>
            </td>
        </tr>
    </table>
</asp:Content>
