﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="ContestTimings.aspx.cs" Inherits="ContestTimings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  

    <table id="tblContestTimings" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Contest Timings</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td>

                <table style="margin-left: auto; margin-right: auto;">

                    <tr>
                        <td style="width: 70px"></td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select year</asp:ListItem>
                                <asp:ListItem Value="-1">All</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                            </asp:DropDownList>

                        </td>

                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                        <td style="width: 100px" align="left">
                            <asp:DropDownList ID="ddEvent" Enabled="false" runat="server" Width="130px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                        <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                            <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" Enabled="false" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <%--<td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>--%>
                    </tr>
                </table>
                <br />
                <div style="text-align: center">
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <table style="margin-left: auto; margin-right: auto; width: 75%;">
                    <tr runat="server" visible="false" id="trContestDay1">
                        <td style="background-color: #2ECCFA; font-weight: bold;">
                            <asp:Label ID="lblContestDay1" runat="server" Style="Width: 200px;">Day1</asp:Label><asp:Label ID="lblContestDay1Stime" runat="server" Style="margin-left: 390px;">Starting Times</asp:Label><asp:Label ID="lblContestEndTime" runat="server" Style="margin-left: 125px;">Ending Times</asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="grdContestTiming" runat="server" OnRowCommand="grdContestTiming_RowCommand" AutoGenerateColumns="false" EnableViewState="true" Width="75%" Style="margin-left: auto; margin-right: auto; margin-top: 10px; table-layout: fixed;" HeaderStyle-BackColor="#ffffcc" OnRowEditing="grdContestTiming_RowEditing" OnRowCancelingEdit="grdContestTiming_RowCancelingEdit" OnRowUpdating="grdContestTiming_RowUpdating">
                                <Columns>                                   
                                    <asp:TemplateField HeaderText="Product Group" HeaderStyle-Width="200px" SortExpression="ProductGroupCode">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProductGroup" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupName") %>'></asp:Label>
                                            <div style="display: none;">
                                                <asp:Label ID="lblContestID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ContestID") %>'>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product" HeaderStyle-Width="240px" SortExpression="ProductCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProduct" Text='<%#DataBinder.Eval(Container.DataItem,"ProductName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade Range" HeaderStyle-Width="65px" SortExpression="GradeFrom">

                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblGradeRange" Text='<%# String.Format("{0} - {1}", Eval("GradeFrom"), Eval("GradeTo")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="Ph1Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase1" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase1" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="Ph2Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase2" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="Ph3Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase3" Text='<%#DataBinder.Eval(Container.DataItem,"Ph3Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase3" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="ProductGroupCode"> 
                                        <ItemTemplate> 
                                            <asp:Label runat="server" ID="lblPhase1End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1End") %>'></asp:Label> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="ProductCode"> 
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="GradeFrom"> 
                                        <ItemTemplate> 
                                            <asp:Label runat="server" ID="lblPhase3End" Text='<%# DataBinder.Eval(Container.DataItem,"Ph3End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" Visible="false" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" Visible="false" />
                                            <div style="display: none;">
                                                <asp:Label ID="lblProductGroupId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupId") %>'>'></asp:Label>
                                                <asp:Label ID="lblProductId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductId") %>'>'></asp:Label>
                                            </div> 
                                        </ItemTemplate> 
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <div style="clear: both;"></div>
                <table style="margin-left: auto; margin-right: auto; width: 75%;">
                    <tr runat="server" visible="false" id="trContestDay2">
                        <td style="background-color: #2ECCFA; font-weight: bold;">
                            <asp:Label ID="lblContestDay2" Style="Width: 200px;" runat="server">Day2</asp:Label><asp:Label ID="lblDay2Stime" runat="server" Style="margin-left: 390px;">Starting Times</asp:Label><asp:Label ID="lblContestEndTimeDay2" runat="server" Style="margin-left: 125px;">Ending Times</asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="grdContestTimingsDay2" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="75%" Style="margin-left: auto; margin-right: auto; margin-top: 10px; table-layout: fixed;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdContestTimingsDay2_RowCommand" OnRowEditing="grdContestTimingsDay2_RowEditing" OnRowCancelingEdit="grdContestTimingsDay2_RowCancelingEdit" OnRowUpdating="grdContestTimingsDay2_RowUpdating">
                                <Columns>

                                    <asp:TemplateField HeaderText="Product Group" HeaderStyle-Width="200px" SortExpression="ProductGroupCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProductGroup2" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupName") %>'></asp:Label>
                                            <div style="display: none;">
                                                <asp:Label ID="lblContestID2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ContestID") %>'>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product" HeaderStyle-Width="240px" SortExpression="ProductCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProduct2" Text='<%#DataBinder.Eval(Container.DataItem,"ProductName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade Range" HeaderStyle-Width="65px" SortExpression="GradeFrom">

                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblGradeRange2" Text='<%# String.Format("{0} - {1}", Eval("GradeFrom"), Eval("GradeTo")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="Ph1Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase1Day2" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase1Day2" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="Ph2Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2Day2" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase2Day2" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="Ph3Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase3Day3" Text='<%#DataBinder.Eval(Container.DataItem,"Ph3Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlPhase3Day3" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                  
                                     <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="ProductGroupCode">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase1Day2End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1End") %>'></asp:Label> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="ProductCode">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2Day2End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="GradeFrom"> 
                                        <ItemTemplate> 
                                            <asp:Label runat="server" ID="lblPhase3Day2End" Text='<%# DataBinder.Eval(Container.DataItem,"Ph3End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEditDay2" runat="server" Text="Edit" CommandName="Edit" />
                                            <asp:Button ID="btnUpdateDay2" runat="server" Text="Update" CommandName="Update" Visible="false" />
                                            <asp:Button ID="btnCancelDay2" runat="server" Text="Cancel" CommandName="Cancel" Visible="false" />
                                            <div style="display: none;">
                                                <asp:Label ID="lblProductGroupIdDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupId") %>'>'></asp:Label>
                                                <asp:Label ID="lblProductIdDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductId") %>'>'></asp:Label>
                                            </div>
                                        </ItemTemplate>




                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>


                 <div style="clear: both;"></div>
                <table style="margin-left: auto; margin-right: auto; width: 75%;">
                    <tr runat="server" visible="false" id="trContestDay3">
                        <td style="background-color: #2ECCFA; font-weight: bold;">
                            <asp:Label ID="lblContestDay3" Style="Width: 200px;" runat="server">Day3</asp:Label><asp:Label ID="lblDay3Stime" runat="server" Style="margin-left: 390px;">Starting Times</asp:Label><asp:Label ID="Label3" runat="server" Style="margin-left: 125px;">Ending Times</asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="grdContestTimingsDay3" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="75%" Style="margin-left: auto; margin-right: auto; margin-top: 10px; table-layout: fixed;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdContestTimingsDay3_RowCommand" OnRowEditing="grdContestTimingsDay3_RowEditing" OnRowCancelingEdit="grdContestTimingsDay3_RowCancelingEdit" OnRowUpdating="grdContestTimingsDay3_RowUpdating">
                                <Columns>

                                    <asp:TemplateField HeaderText="Product Group" HeaderStyle-Width="200px" SortExpression="ProductGroupCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProductGroup2" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupName") %>'></asp:Label>
                                            <div style="display: none;">
                                                <asp:Label ID="lblContestID2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ContestID") %>'>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product" HeaderStyle-Width="240px" SortExpression="ProductCode"> 
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProduct2" Text='<%#DataBinder.Eval(Container.DataItem,"ProductName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade Range" HeaderStyle-Width="65px" SortExpression="GradeFrom"> 
                                        <ItemTemplate> 
                                            <asp:Label runat="server" ID="lblGradeRange2" Text='<%# String.Format("{0} - {1}", Eval("GradeFrom"), Eval("GradeTo")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="Ph1Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase1Day2" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase1Day2" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="Ph2Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2Day2" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase2Day2" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>

                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="Ph3Start">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase3Day3" Text='<%#DataBinder.Eval(Container.DataItem,"Ph3Start") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:DropDownList ID="ddlPhase3Day3" runat="server">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                <asp:ListItem Value="6:00">6:00</asp:ListItem>
                                                <asp:ListItem Value="6:15">6:15</asp:ListItem>
                                                <asp:ListItem Value="6:30">6:30</asp:ListItem>
                                                <asp:ListItem Value="6:45">6:45</asp:ListItem>
                                                <asp:ListItem Value="7:00">7:00</asp:ListItem>
                                                <asp:ListItem Value="7:15">7:15</asp:ListItem>
                                                <asp:ListItem Value="7:30">7:30</asp:ListItem>
                                                <asp:ListItem Value="7:45">7:45</asp:ListItem>
                                                <asp:ListItem Value="8:00">8:00</asp:ListItem>
                                                <asp:ListItem Value="8:15">8:15</asp:ListItem>
                                                <asp:ListItem Value="8:30">8:30</asp:ListItem>
                                                <asp:ListItem Value="8:45">8:45</asp:ListItem>
                                                <asp:ListItem Value="9:00">9:00</asp:ListItem>
                                                <asp:ListItem Value="9:15">9:15</asp:ListItem>
                                                <asp:ListItem Value="9:30">9:30</asp:ListItem>
                                                <asp:ListItem Value="9:45">9:45</asp:ListItem>
                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                        
                                     <asp:TemplateField HeaderText="Phase 1" HeaderStyle-Width="70px" SortExpression="ProductGroupCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase1Day2End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph1End") %>'></asp:Label>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 2" HeaderStyle-Width="70px" SortExpression="ProductCode">

                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPhase2Day2End" Text='<%#DataBinder.Eval(Container.DataItem,"Ph2End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase 3" HeaderStyle-Width="70px" SortExpression="GradeFrom">

                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblPhase3Day2End" Text='<%# DataBinder.Eval(Container.DataItem,"Ph3End") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEditDay2" runat="server" Text="Edit" CommandName="Edit" />
                                            <asp:Button ID="btnUpdateDay2" runat="server" Text="Update" CommandName="Update" Visible="false" />
                                            <asp:Button ID="btnCancelDay2" runat="server" Text="Cancel" CommandName="Cancel" Visible="false" />
                                            <div style="display: none;">
                                                <asp:Label ID="lblProductGroupIdDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupId") %>'>'></asp:Label>
                                                <asp:Label ID="lblProductIdDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductId") %>'>'></asp:Label>
                                            </div>
                                        </ItemTemplate>




                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</asp:Content>
