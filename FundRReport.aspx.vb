﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class FundRReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            'loadgrid()
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                hlnkMainPage.NavigateUrl = "userfunctions.aspx"
                hlnkMainPage.Text = "Back to Parent Functions"
            ElseIf Session("entryToken").ToString.ToUpper() = "DONOR" Then
                hlnkMainPage.NavigateUrl = "DonorFunctions.aspx"
                hlnkMainPage.Text = "Back to Donor Functions"
            Else
                hlnkMainPage.Text = "Back to Volunteer Functions"
                hlnkMainPage.NavigateUrl = "VolunteerFunctions.aspx"
            End If


            loadDropdown()
        End If
    End Sub
    Private Sub loadgrid(ByVal FundRCalID As Integer)
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FF.ProductID=73 and FR.FundRCalID = " & FundRCalID
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim dv As DataView = New DataView(dt)
            If dt.Rows.Count > 0 Then
                Dim totAmount As Integer = 0
                For i As Integer = 0 To dt.Rows.Count - 1
                    totAmount = totAmount + Convert.ToInt32(dt.Rows(i)("TotalAmt"))
                Next
                Dim R As DataRow = dt.NewRow()
                dt.Rows.Add(R)
                dt.Rows(dt.Rows.Count - 1)("TotalAmt") = totAmount & ".00"
                spnTable1.Visible = True
                dgCatalog.DataSource = dt
                dgCatalog.DataBind()
                dgCatalog.Visible = True
            Else
                spnTable1.Visible = False
                dgCatalog.Visible = False
                ltrl1.Text = "No fundraising registration found"
            End If
        Catch ex As Exception
            ltrl1.Text = "No fundraising registration found"
        End Try

    End Sub

    Private Sub loadDropdown()
        Dim SQLstr As String = "select  FC.FundRCalID, FC.ChapterID, FC.ChapterCode, FC.EventYear, FC.VenueID, FC.EventDate, FC.StartTime + ' - ' + FC.EndTime as Timings, FC.Building, FC.CampStartDate, FC.CampEndDate, "
        SQLstr = SQLstr & " FC.EventDescription, O.ORGANIZATION_NAME as VenueName from FundRaisingCal FC Inner JOIN OrganizationInfo O ON FC.VenueID = O.AutoMemberID  "
        SQLstr = SQLstr & " where FC.EventYear=YEAR(GETDATE()) order By FC.EventDate "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLstr)
        If ds.Tables(0).Rows.Count > 0 Then
            gvEvents.DataSource = ds
            gvEvents.DataBind()
            gvEvents.SelectedIndex = -1
            gvEvents.Visible = True
        Else
            gvEvents.DataSource = Nothing
            gvEvents.DataBind()
            gvEvents.Visible = False
        End If
    End Sub

    Protected Sub gvEvents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEvents.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        'loadgrid(gvEvents.DataKeys(index).Value)
        'loadContestChildgrid(gvEvents.DataKeys(index).Value)
        'loadContestgrid(gvEvents.DataKeys(index).Value)
        BreakUpContestByTable(gvEvents.DataKeys(index).Value)

    End Sub

    Protected Sub dgCatalog_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, TotAmt As Label
                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                'TotAmt = CType(e.Item.FindControl("lblFamilyTotAmt"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                'TotAmt.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("PaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf PaymentMode = "Check" Then
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                ElseIf PaymentMode = "In Kind" Then
                    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Text = lblamount.Text
                    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    'Function CalcAmount() As Decimal
    '    Dim totamount As Decimal = 0.0
    '    Dim chkAmt As Decimal = 0.0
    '    Dim CreditAmt As Decimal = 0.0
    '    Dim InkindAmt As Decimal = 0.0
    '    Dim item As DataGridItem
    '    For Each item In dgCatalog.Items
    '        Dim lblamount As Label = CType(item.FindControl("lblAmount"), Label)
    '        Dim lblCreditCardAmt As Label = CType(item.FindControl("lblCreditCardAmt"), Label)
    '        Dim lblCheckAmt As Label = CType(item.FindControl("lblCheckAmt"), Label)
    '        Dim lblInKindAmt As Label = CType(item.FindControl("lblInKindAmt"), Label)
    '        Dim PaymentMode As String = CType(item.FindControl("lblSelPaymentMode"), Label).Text.Trim
    '        'If PaymentMode = "Credit Card" Then
    '        '    lblCreditCardAmt.Text = lblamount.Text
    '        '    lblCreditCardAmt.Visible = True
    '        '    lblCheckAmt.Visible = False
    '        '    lblInKindAmt.Visible = False
    '        '    If lblamount.Text.Length > 0 Then
    '        '        CreditAmt = CreditAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '        '    End If
    '        'Else
    '        If PaymentMode = "Check" Then
    '            lblCreditCardAmt.Visible = False
    '            lblCheckAmt.Visible = True
    '            lblInKindAmt.Visible = False
    '            lblCheckAmt.Text = lblamount.Text
    '            If lblamount.Text.Length > 0 Then
    '                chkAmt = chkAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '            End If
    '            'ElseIf PaymentMode = "In Kind" Then
    '            '    lblInKindAmt.Text = lblamount.Text
    '            '    lblCreditCardAmt.Visible = False
    '            '    lblCheckAmt.Visible = False
    '            '    lblInKindAmt.Visible = True
    '            '    If lblamount.Text.Length > 0 Then
    '            '        InkindAmt = InkindAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '            '    End If
    '        End If
    '        'lblCheck.Text = String.Format(chkAmt, "{0:c}")
    '        ''lblCredit.Text = String.Format(CreditAmt, "{0:F2}")
    '        'lblInKind.Text = String.Format(InkindAmt, "{0:F2}")
    '        If lblamount.Text.Length > 0 Then
    '            totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '        End If
    '    Next
    '    Return totamount
    'End Function

    Private Sub loadContestChildgrid(ByVal FundRCalID As Integer)
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FF.ProductID= 105 and FR.FundRCalID = " & FundRCalID
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim dv As DataView = New DataView(dt)
            If dt.Rows.Count > 0 Then
                Dim totAmount As Integer = 0
                For i As Integer = 0 To dt.Rows.Count - 1
                    totAmount = totAmount + Convert.ToInt32(dt.Rows(i)("TotalAmt"))
                Next
                Dim R As DataRow = dt.NewRow()
                dt.Rows.Add(R)
                spnTable2.Visible = True

                DataGridContestChildA.DataSource = dt
                DataGridContestChildA.DataBind()
                DataGridContestChildA.Visible = True
            Else
                spnTable2.Visible = False
                DataGridContestChildA.Visible = False
                ltrl1.Text = "No fundraising registration found"
            End If
        Catch ex As Exception
            ltrl1.Text = "No fundraising registration found"
        End Try

    End Sub

    Private Sub loadContestgrid(ByVal FundRCalID As Integer)
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FF.ProductID<>73 and FF.ProductID<>105 and FR.FundRCalID = " & FundRCalID
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim dv As DataView = New DataView(dt)
            If dt.Rows.Count > 0 Then
                Dim totAmount As Integer = 0
                For i As Integer = 0 To dt.Rows.Count - 1
                    totAmount = totAmount + Convert.ToInt32(dt.Rows(i)("TotalAmt"))
                Next
                Dim R As DataRow = dt.NewRow()
                dt.Rows.Add(R)
                dt.Rows(dt.Rows.Count - 1)("TotalAmt") = totAmount & ".00"

                DataGridContests.DataSource = dt
                DataGridContests.DataBind()
                DataGridContests.Visible = True
            Else
                DataGridContests.Visible = False
                ltrl1.Text = "No fundraising registration found"
            End If
        Catch ex As Exception
            ltrl1.Text = "No fundraising registration found"
        End Try

    End Sub

    Protected Sub DataGridContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, TotAmt As Label
                lblStatus = CType(e.Item.FindControl("lblContestStatus"), Label)
                lblamount = CType(e.Item.FindControl("lblContestAmount"), Label)
                'TotAmt = CType(e.Item.FindControl("lblContestTotAmt"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                'TotAmt.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("PaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblContestCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf PaymentMode = "Check" Then
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblContestCheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                ElseIf PaymentMode = "In Kind" Then
                    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblContestInKindAmt"), Label)
                    lblInKindAmt.Text = lblamount.Text
                    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    Protected Sub DataGridContestChildA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, TotAmt As Label
                lblStatus = CType(e.Item.FindControl("lblChildAStatus"), Label)
                lblamount = CType(e.Item.FindControl("lblChildAAmount"), Label)
                'TotAmt = CType(e.Item.FindControl("lblChildATotAmt"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                'TotAmt.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("PaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblChildACreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf PaymentMode = "Check" Then
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblChildACheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                ElseIf PaymentMode = "In Kind" Then
                    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblChildAInKindAmt"), Label)
                    lblInKindAmt.Text = lblamount.Text
                    lblInKindAmt.Visible = True

                End If
        End Select
    End Sub

    Public Sub BreakUpContestByTable(ByVal FundRCalID As Integer)
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference, "
        strSQL = strSQL & "case when (select count(*) from FundRReg where PaymentReference is not null and FundRCalID= " & FundRCalID & " and MemberID=FR.MemberID) >0 then 'Paid' else 'Pending' end as status "

        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FR.FundRCalID = " & FundRCalID
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim ProductCode As String = String.Empty
            Dim tblHtml As String = String.Empty
            Dim ProductArray As ArrayList = New ArrayList()
            Dim ProductName As ArrayList = New ArrayList()
            Dim ProductID As ArrayList = New ArrayList()
            For Each dr As DataRow In ds.Tables(0).Rows
                If dr("ProductCode").ToString() <> ProductCode Then
                    If ProductArray.Count > 0 Then
                        Dim retval As String = ""
                        For k As Integer = 0 To ProductArray.Count - 1
                            If dr("ProductCode").ToString() = ProductArray(k).ToString() Then
                                retval = "false"

                            End If
                        Next
                        If retval = "" Then
                            ProductArray.Add(dr("ProductCode").ToString())
                            ProductName.Add(dr("ProductName").ToString())
                            ProductID.Add(dr("ProductID").ToString())
                        End If


                    Else
                        ProductArray.Add(dr("ProductCode").ToString())
                        ProductName.Add(dr("ProductName").ToString())
                        ProductID.Add(dr("ProductID").ToString())
                    End If

                End If
                ProductCode = dr("ProductCode").ToString()
            Next


            For i As Integer = 0 To ProductArray.Count - 1

                Dim count As Integer = 0
                Dim oddEven As Integer = 0
                Dim RowColor As String = String.Empty

                Dim PaidCount As Integer = 0
                Dim PaidAmount As Double = 0
                Dim PendingCount As Integer = 0
                Dim PendingAmount As Double = 0

                tblHtml += "<span style='font-weight:bold;'>Table " & (i + 1) & ": " & ProductName(i).ToString() & "</span>"
                tblHtml += "<div style='clear:both;'></div>"
                tblHtml += "<div style='float:right;'><input type='button' value='Export To Excel' onclick='add(" & ProductID(i) & "," & FundRCalID & ")' /></div>"
                tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1000px;'>"

                tblHtml += "<tr style='color:white;background-color:#4A3C8C;font-weight:bold;'>"
                tblHtml += "<td style='color:white;'>Name</td>"
                tblHtml += "<td style='color:white;'>Fee</td>"
                tblHtml += "<td style='color:white;'>Pay_Method</td>"
                tblHtml += "<td style='color:white;'>Quantity</td>"
                tblHtml += "<td style='color:white;'>Check</td>"
                tblHtml += "<td style='color:white;'>Credit_Card</td>"
                tblHtml += "<td style='color:white;'>In Kind</td>"
                tblHtml += "<td style='color:white;'>Payment_Info</td>"

                tblHtml += "</tr>"

                For Each dr As DataRow In ds.Tables(0).Rows
                    If dr("ProductCode").ToString() = ProductArray(i).ToString() Then
                        count = count + 1
                        oddEven = count Mod 2
                        If oddEven = 0 Then
                            RowColor = "#F7F7F7"
                        Else
                            RowColor = "#E7E7FF"
                        End If
                        tblHtml += "<tr style='color:#F7F7F7;background-color:" & RowColor & ";'>"
                        tblHtml += "<td>" & dr("ProductName").ToString() & "</td>"
                        tblHtml += "<td>" & String.Format("{0:0.00}", dr("Fee")) & "</td>"
                        tblHtml += "<td>" & dr("PaymentMode").ToString() & "</td>"
                        tblHtml += "<td>" & dr("Quantity").ToString() & "</td>"
                        If dr("PaymentMode").ToString().Trim() = "Check" Then
                            tblHtml += "<td>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td></td>"
                        End If

                        If dr("PaymentMode").ToString().Trim() = "Credit Card" Then
                            tblHtml += "<td>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td></td>"
                        End If

                        If dr("PaymentMode").ToString().Trim() = "In Kind" Then
                            tblHtml += "<td>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td></td>"
                        End If

                        If dr("ProductID").ToString <> "134" And dr("ProductID").ToString <> "135" Then

                            If dr("PaymentDate").ToString() <> "" Then
                                tblHtml += "<td>" & Convert.ToDateTime(dr("PaymentDate")).ToString("MM-dd-yyyy") & " " & dr("PaymentMode").ToString() & " <br>" & dr("PaymentReference").ToString() & "</td>"
                                PaidCount = PaidCount + dr("Quantity")
                                PaidAmount = PaidAmount + dr("TotalAmt")
                            Else

                                tblHtml += "<td>" & dr("PaymentMode").ToString() & "<br> " & dr("PaymentReference").ToString() & "</td>"
                                PendingCount = PendingCount + dr("Quantity")
                                PendingAmount = PendingAmount + dr("TotalAmt")
                            End If
                        Else

                            If dr("Status").ToString() <> "Pending" Then
                                tblHtml += "<td>" & dr("PaymentMode").ToString() & " <br>" & dr("Status").ToString() & "</td>"
                                PaidCount = PaidCount + dr("Quantity")
                                PaidAmount = PaidAmount + dr("TotalAmt")
                            Else

                                tblHtml += "<td>" & dr("PaymentMode").ToString() & " <br>" & dr("Status").ToString() & "</td>"
                                PendingCount = PendingCount + dr("Quantity")
                                PendingAmount = PendingAmount + dr("TotalAmt")
                            End If
                        End If



                        tblHtml += "</tr>"
                    End If
                Next


                tblHtml += "</table>"
                tblHtml += "<div style='clear:both;'></div>"
                tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1000px;'>"
                tblHtml += "<tr style='background-color:#F7F7F7;font-weight:bold;'>"
                tblHtml += "<td>Paid Count = " & PaidCount & "</td>"
                tblHtml += "<td>Paid Amount = " & String.Format("{0:0.00}", PaidAmount) & "</td>"
                tblHtml += "<td></td>"
                tblHtml += "<td>Pending Count= " & PendingCount & "</td>"
                tblHtml += "<td></td>"

                tblHtml += "<td>Pending Amount = " & String.Format("{0:0.00}", PendingAmount) & "</td>"
                tblHtml += "<td></td>"
                tblHtml += "<td>Total Amount = " & String.Format("{0:0.00}", PendingAmount + PaidAmount) & "</td>"


                tblHtml += "</tr>"
                tblHtml += "</table>"
                tblHtml += "<div style='clear:both; margin-bottom:20px;'></div>"
            Next


            LtrBreakTablesByContest.Text = tblHtml
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ltrl1.Text = "No fundraising registration found"
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FR.FundRCalID = " & hdnFundRID.Value & " and FR.ProductID=" & hdnProductID.Value & ""
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim ProductCode As String = String.Empty
            Dim tblHtml As String = String.Empty
            Dim ProductArray As ArrayList = New ArrayList()
            Dim ProductName As ArrayList = New ArrayList()
            Dim ProductID As ArrayList = New ArrayList()
            For Each dr As DataRow In ds.Tables(0).Rows
                If dr("ProductCode").ToString() <> ProductCode Then
                    If ProductArray.Count > 0 Then
                        Dim retval As String = ""
                        For k As Integer = 0 To ProductArray.Count - 1
                            If dr("ProductCode").ToString() = ProductArray(k).ToString() Then
                                retval = "false"

                            End If
                        Next
                        If retval = "" Then
                            ProductArray.Add(dr("ProductCode").ToString())
                            ProductName.Add(dr("ProductName").ToString())
                            ProductID.Add(dr("ProductID").ToString())
                        End If


                    Else
                        ProductArray.Add(dr("ProductCode").ToString())
                        ProductName.Add(dr("ProductName").ToString())
                        ProductID.Add(dr("ProductID").ToString())
                    End If

                End If
                ProductCode = dr("ProductCode").ToString()
            Next


            For i As Integer = 0 To ProductArray.Count - 1

                Dim count As Integer = 0
                Dim oddEven As Integer = 0
                Dim RowColor As String = String.Empty

                Dim PaidCount As Integer = 0
                Dim PaidAmount As Double = 0
                Dim PendingCount As Integer = 0
                Dim PendingAmount As Double = 0

                'tblHtml += "<span style='font-weight:bold;'>Table " & (i + 1) & ": " & ProductName(i).ToString() & "</span>"
                'tblHtml += "<div style='clear:both;'></div>"
                tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse;'>"

                tblHtml += "<tr style='color:white;background-color:#4A3C8C;font-weight:bold;'>"
                tblHtml += "<td style='color:white; width:200px;'>Name</td>"
                tblHtml += "<td style='color:white; width:100px;'>Fee</td>"
                tblHtml += "<td style='color:white; width:100px;'>Pay_Method</td>"
                tblHtml += "<td style='color:white; width:100px;'>Quantity</td>"
                tblHtml += "<td style='color:white; width:100px;'>Check</td>"
                tblHtml += "<td style='color:white; width:100px;'>Credit_Card</td>"
                tblHtml += "<td style='color:white; width:100px;'>In Kind</td>"
                tblHtml += "<td style='color:white; width:200px;'>Payment_Info</td>"

                tblHtml += "</tr>"

                For Each dr As DataRow In ds.Tables(0).Rows
                    If dr("ProductCode").ToString() = ProductArray(i).ToString() Then
                        count = count + 1
                        oddEven = count Mod 2
                        If oddEven = 0 Then
                            RowColor = "#F7F7F7"
                        Else
                            RowColor = "#E7E7FF"
                        End If
                        tblHtml += "<tr style='color:black;background-color:" & RowColor & ";'>"
                        tblHtml += "<td style='width:200px;'>" & dr("ProductName").ToString() & "</td>"
                        tblHtml += "<td style='width:100px; text-align:left;'>" & String.Format("{0:0.00}", dr("Fee")) & "</td>"
                        tblHtml += "<td style='width:100px; text-align:center;'>" & dr("PaymentMode").ToString() & "</td>"
                        tblHtml += "<td style='width:100px; text-align:left;'>" & dr("Quantity").ToString() & "</td>"
                        If dr("PaymentMode").ToString().Trim() = "Check" Then
                            tblHtml += "<td style='width:100px; text-align:left;'>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td style='width:100px;'></td>"
                        End If

                        If dr("PaymentMode").ToString().Trim() = "Credit Card" Then
                            tblHtml += "<td style='width:100px; text-align:left;'>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td style='width:100px;'></td>"
                        End If

                        If dr("PaymentMode").ToString().Trim() = "In Kind" Then
                            tblHtml += "<td style='width:100px; text-align:left;'>" & String.Format("{0:0.00}", dr("TotalAmt")) & "</td>"
                        Else
                            tblHtml += "<td style='width:100px;'></td>"
                        End If


                        If dr("PaymentDate").ToString() <> "" Then
                            tblHtml += "<td  style='width:200px;'>" & Convert.ToDateTime(dr("PaymentDate")).ToString("MM-dd-yyyy") & " " & dr("PaymentMode").ToString() & " <br>" & dr("PaymentReference").ToString() & "</td>"
                            PaidCount = PaidCount + dr("Quantity")
                            PaidAmount = PaidAmount + dr("TotalAmt")
                        Else

                            tblHtml += "<td  style='width:200px;'>" & dr("PaymentMode").ToString() & "<br> " & dr("PaymentReference").ToString() & "</td>"
                            PendingCount = PendingCount + dr("Quantity")
                            PendingAmount = PendingAmount + dr("TotalAmt")
                        End If


                        tblHtml += "</tr>"
                    End If
                Next


                tblHtml += "</table>"
                'tblHtml += "<div style='clear:both;'></div>"
                tblHtml += "<table style='background-color:Black;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse;'>"
                tblHtml += "<tr style='background-color:#F7F7F7;font-weight:bold;'>"
                tblHtml += "<td>Paid Count = " & PaidCount & "</td>"
                tblHtml += "<td>Paid Amount = " & String.Format("{0:0.00}", PaidAmount) & "</td>"
                tblHtml += "<td></td>"
                tblHtml += "<td>Pending Count= " & PendingCount & "</td>"
                tblHtml += "<td></td>"

                tblHtml += "<td>Pending Amount = " & String.Format("{0:0.00}", PendingAmount) & "</td>"
                tblHtml += "<td></td>"
                tblHtml += "<td>Total Amount = " & String.Format("{0:0.00}", PendingAmount + PaidAmount) & "</td>"


                tblHtml += "</tr>"
                tblHtml += "</table>"
                'tblHtml += "<div style='clear:both; margin-bottom:20px;'></div>"
            Next


            Dim dt As DateTime = DateTime.Now
            Dim month As String = dt.ToString("MMM")
            Dim day As String = dt.ToString("dd")
            Dim year As String = dt.ToString("yyyy")
            Dim monthDay As String = month & "" & day


            Dim filename As String = "FundRReport_" & monthDay & "_" & year & ".xls"

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")
            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Response.ContentType = "application/vnd.ms-excel"

            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim stringWrite As StringWriter = New StringWriter()
            Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
            Response.Write(tblHtml.ToString())

            Response.End()

        Catch ex As Exception

            ltrl1.Text = "No fundraising registration found"
        End Try
    End Sub
End Class
