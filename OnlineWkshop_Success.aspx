<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="OnlineWkshop_Success.aspx.vb" Inherits="OnlineWkshop_Success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <div>
        <script type="text/javascript">
            var accessToken;
            var orgAnizerKey;
            $(function (e) {
                accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
                orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;

                registerAttendees();
            });

            function registerAttendees() {

                var memberid = document.getElementById("<%=hdnMemberId.ClientID%>").value;
                var eventyear = document.getElementById("<%=hdnEventyear.ClientID%>").value;
                var onlineCalId = document.getElementById("<%=hdnOnlineWsCalId.ClientID%>").value;

                var source = "OW";
                var jsonData = JSON.stringify({ EventYear: eventyear, AutoMemberId: memberid, OnlineWsCalID: onlineCalId });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListAttendeesFromWS",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var onlineWsCalId;
                        var i = 0;
                        var firstName;
                        var lastName;
                        var email;
                        var source = "NSF";
                        var address;
                        var city;
                        var state;
                        var zipCode;
                        var country;
                        var phone;
                        var onlineWsCalId;
                        var regId;
                        var webinarKey;
                        var spouseEmail;
                        var secondaryEmail;
                        var parentName;
                        var parentEmail;
                        var parentPhone;
                        var childName;
                        var date;
                        var time;
                        var productName;

                        $.each(data.d, function (index, value) {
                            i++;

                            firstName = value.FirstName
                            lastName = value.LastName;
                            email = value.Email;
                            source = "NSF";
                            address = value.Address1;
                            city = value.City;
                            state = value.State;
                            zipCode = value.Zip;
                            country = value.Country;
                            phone = value.Phone;
                            onlineWsCalId = value.OnlineWsCalID;
                            regId = value.RegId;
                            webinarKey = value.Webinarkey;
                            spouseEmail = value.SpouseEmail;
                            secondaryEmail = value.SecondaryEmail;
                            parentName = value.ParentName;
                            parentEmail = value.Email;
                            parentPhone = value.Phone;
                            date = value.Date;
                            time = value.Time;
                            childName = firstName + " " + lastName;
                            productName = value.ProductName;

                            if (value.JoinURL == "" && value.RegistrantKey == "") {
                                if (i > 1) {
                                    if (value.Email != secondaryEmail) {
                                        email = (secondaryEmail == "" ? spouseEmail : secondaryEmail);
                                    } else {
                                        email = spouseEmail;
                                    }
                                } else if (i > 2) {
                                    email = spouseEmail;
                                }

                                var jsonObject = { "firstName": firstName, "lastName": lastName, "email": email, "source": source, "address": address, "city": city, "state": state, "zipCode": zipCode, "Country": country, "phone": phone };
                                $.ajax({
                                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/registrants",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader("Authorization", accessToken);
                                    },
                                    type: 'POST',
                                    crossDomain: true,
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    processData: false,
                                    data: JSON.stringify(jsonObject),
                                    success: function (data) {

                                        var obj = $.parseJSON(JSON.stringify(data));

                                        updateJoinURLToAttendees(obj.joinUrl, obj.registrantKey, regId);

                                    },
                                    error: function (data) {
                                        var obj = $.parseJSON(JSON.stringify(data.responseJSON));
                                        var description = obj.description;


                                        exceptionLog(description);

                                        // sendEmailToAdmin(parentName, parentEmail, parentPhone, productName, date, time, childName, description)


                                    }
                                });
                            }

                        });



                    }, error: function (e) {
                        var obj = $.parseJSON(JSON.stringify(e.responseJSON));
                        var description = obj.description;
                        exceptionLog(description);
                    }
                });
            }

            function updateJoinURLToAttendees(joinURL, registrantKey, regId) {
                var jsonData = JSON.stringify({ ObjWebinar: { JoinURL: joinURL, RegistrantKey: registrantKey, RegId: regId } });
                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/UpdateJoinURLToAttendees",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d) > 0) {

                            //  location.href = "onlineWkShop_Success.aspx";
                        }
                    }, error: function (e) {
                        var obj = $.parseJSON(JSON.stringify(e.responseJSON));
                        var description = obj.description;
                        exceptionLog(description);
                    }
                });
            }

            function sendEmailToAdmin(parentName, parentEmail, parentPhone, productName, date, time, childName, issueName) {
                var jsonData = JSON.stringify({ ParentName: parentName, ParentEmail: parentEmail, ParentPhone: parentPhone, ProductName: productName, Date: date, Time: time, ChildName: childName, IssueName: issueName });
                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/SendEmailToAdmin",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d) > 0) {

                            //  location.href = "onlineWkShop_Success.aspx";
                        }
                    }, error: function (e) {
                        var obj = $.parseJSON(JSON.stringify(e.responseJSON));
                        var description = obj.description;
                        exceptionLog(description);
                    }
                });
            }

            function exceptionLog(msg) {
                var jsonData = JSON.stringify({ Msg: msg });
                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ExceptionLog",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                    }, error: function (e) {
                    }
                });
            }
        </script>
    </div>
    <table width="100%" id='tabId0'>
        <tr>
            <td class="Heading" align="center" colspan="2">2 Marissa Ct, Burr Ridge, IL60527<br />
                Tax ID:363659998<br />
                Receipt<br />
            </td>
        </tr>
        <tr>
            <td class="SmallFont" style="height: 17px">Parent Detailed Information
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 819px">
                <table style="width: 671px" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblParentName" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblPaymentDateMsg" Text="Payment Date :" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblPaymentDate" runat="server" CssClass="SmallFont" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="Label1" Text="Payment Reference :" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblReference" runat="server" CssClass="SmallFont" />
                        </td>
                    </tr>
                    <tr id="trAddress2" runat="server">
                        <td>
                            <asp:Label ID="lblAddress2" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <%-- <tr>
                  <td>
                      <asp:Label ID="lblCity" runat="server" CssClass="SmallFont"></asp:Label></td>
                  </tr>--%>
                    <tr>
                        <td>
                            <asp:Label ID="lblStateZip" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblHomePhone" runat="server" CssClass="SmallFont"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr id="trChild1" runat="server">
            <td class="SmallFont">Child/Children Detailed Information
            </td>
        </tr>
        <tr>
            <td style="width: 819px">
                <asp:DataGrid ID="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="OnlineWSCalID"
                    CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                    Width="100%">
                    <FooterStyle CssClass="GridFooter"></FooterStyle>
                    <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                    <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'
                                    CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Online Workshops Selected" HeaderStyle-Font-Bold="true"
                            HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblOnlineWS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>'
                                    CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>

                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:C2}") %>' CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>

                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LateFee" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblLateFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LateFee","{0:C2}") %>' CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>

                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Online Worshops Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                            HeaderStyle-ForeColor="#990000">
                            <ItemTemplate>
                                <asp:Label ID="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>'
                                    CssClass="SmallFont"></asp:Label>
                                &nbsp;
                            </ItemTemplate>

                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Time" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                            HeaderStyle-ForeColor="#990000">
                            <ItemTemplate>
                                <asp:Label ID="lblTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time")  %>'
                                    CssClass="SmallFont"></asp:Label>
                                &nbsp;
                            </ItemTemplate>

                            <HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Web Conf Link" Visible="false">
                            <HeaderStyle Width="300px"></HeaderStyle>
                            <ItemStyle BackColor="Yellow" />
                            <ItemTemplate>

                                <a href='<%# DataBinder.Eval(Container.DataItem, "webinarlink") %>' target="_blank">Click here</a> and provide your NSF registered email address and full name to receive Webinar joining instructions via email
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <br />
    <table align="center" cellspacing="1" cellpadding="1" border="0" style="border-color: Gray; border-width: 1px; border-style: solid; width: 500px">
        <tr>
            <td colspan="3" align="right" class="GridHeader" style="color: Maroon">Tax-deductible
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-bottom-color: Black; border-style: dotted; border-width: 1px;"></td>
        </tr>
        <tr>
            <td style="width: 200px">Registration Fee
            </td>
            <td style="width: 150px" align="right">
                <asp:Label ID="lblRegFee" runat="server" />
            </td>
            <td style="width: 160px" align="right">
                <asp:Label ID="lblRegFeeTax" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 200px">Late Fee
            </td>
            <td style="width: 150px" align="right">
                <asp:Label ID="lblLateFee" runat="server" />
            </td>
            <td style="width: 160px" align="right">
                <asp:Label ID="lblLateFeeTax" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Donation
            </td>
            <td align="right">
                <asp:Label ID="lblDonation" runat="server" />
            </td>
            <td align="right">
                <asp:Label ID="lblDonationTax" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Total Amount Paid
            </td>
            <td align="right">
                <asp:Label ID="lblTotAmt" runat="server" />
            </td>
            <td align="right">
                <asp:Label ID="lblTotAmtTax" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="lblNoteMsg" Style="color: blue" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td class="announcement_text">
                <asp:Label ID="lblEmailStatus" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="Table1">
        <tr>
            <td class="ContentSubTitle" align="left" colspan="2">
                <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink>
            </td>
        </tr>
    </table>
    <input type="hidden" runat="server" id="hdnMemberId" value="" />
    <input type="hidden" runat="server" id="hdnEventyear" value="" />
    <input type="hidden" id="hdnAccessToken" value="" runat="server" />
    <input type="hidden" id="hdnOrganizerKey" value="" runat="server" />
    <input type="hidden" id="hdnOnlineWsCalId" value="" runat="server" />
</asp:Content>

