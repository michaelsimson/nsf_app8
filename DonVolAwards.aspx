﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="DonVolAwards.aspx.cs" Inherits="DonVolAwards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>


     <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Volunteer / Donor Recognition Awards 
    </strong>
             <br />
        <br />
    </div>
       <br />
  

    <br />
    <div align="left" id="divMain" runat="server">
    <table style="width: 25%; margin-left: 0px;">
        <tr>
        <td align="left" nowrap="nowrap" >Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            
            <td align="left" nowrap="nowrap" style="width: 237px">
                <asp:TextBox ID="txtName" runat="server" Width="147px" Enabled="false"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsearchChild" runat="server" Text="Search" 
                    OnClick="btnsearchChild_Click" />
              
            </td>
        <td nowrap="nowrap" style="width: 142px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="DDYear" runat="server" Width="150px" 
                    OnSelectedIndexChanged="ddlType_SelectedIndexChanged" >
                    
                </asp:DropDownList>
            </td>

          
        </tr>
       
       <tr>
       <td nowrap="nowrap" style="width: 42px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td nowrap="nowrap" style="width: 142px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td nowrap="nowrap" style="width: 42px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Award&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td align="center" style="width: 224px">   
                <asp:DropDownList ID="ddlChoice" runat="server" AutoPostBack="True" 
                    Height="17px" OnSelectedIndexChanged="ddlChoice_SelectedIndexChanged" >
              <asp:ListItem Value="0">Select Awards</asp:ListItem>
                <asp:ListItem Value="1">Donor Recognition Awards</asp:ListItem>
              <asp:ListItem Value="2">Volunteer Recognition Awards</asp:ListItem>
                </asp:DropDownList> &nbsp;&nbsp;
            </td>
             
            <td align="left" nowrap="nowrap" style="width: 50px">
                <asp:Label ID="LbType" runat="server" Text="Type" Visible="false"></asp:Label></td>
             <td style="width: 35px" align="left">
                <asp:DropDownList ID="DDTypeselection" runat="server" Width="150px" Visible="false" >
                </asp:DropDownList>
            </td>

            </tr>
      
        <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="left" style="width: 237px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="75px" 
                    OnClick="btnAdd_Click"  />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="75px" 
                    OnClick="btnCancel_Click"  />
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Anonymous
</td>
            <td align="left" style="width: 141px">
                <asp:DropDownList ID="DDAnonymous" runat="server" AutoPostBack="True" 
                    Height="17px">
                <asp:ListItem Value="1">No</asp:ListItem>
              <asp:ListItem Value="2">Yes</asp:ListItem>
                </asp:DropDownList></td>
                  <td align="left" nowrap="nowrap" style="width: 103px">Spouse
</td>
            <td align="left" style="width: 141px">
                <asp:DropDownList ID="DDSpouse" runat="server" AutoPostBack="True" 
                    Height="17px">
                <asp:ListItem Value="1">No</asp:ListItem>
              <asp:ListItem Value="2">Yes</asp:ListItem>
                </asp:DropDownList></td> 
        
        </tr>
    </table>
    <br />
    <div runat="server" align="center">
        <asp:Label ID="Lbsearchresult" runat="server" Text="Please Select All values" ForeColor="Red" Visible="false"></asp:Label></div>
         <asp:Label ID="LbDuplicateresults" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></div>
   


  
                  <table border="0" width="900px" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
                            <b>Search NSF member</b>
                            <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%"
                                visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server"  Text="Find"
                                            CausesValidation="False" OnClick="btnSearch_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnIndClose" runat="server" Text="Close" 
                            CausesValidation="False" OnClick="btnIndClose_Click" />
                                    </td>
                                </tr>
                            </table>
                          </asp:Panel>


    <asp:Label ID="lbsearchresults" runat="server"  ForeColor="Red"></asp:Label>
    </td></tr>
    </table>

     <div>


             <asp:Panel ID="Panel4" runat="server"  HorizontalAlign="Center" Visible="false">
                                <b>Search Result</b>
                                <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridSearchSoln"
                                    DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server"  
                                    AllowPaging="true"  PageSize="15"
                                    RowStyle-CssClass="SmallFont" OnRowCommand="GridSearchSoln_RowCommand" 
                                    OnPageIndexChanging="GridSearchSoln_PageIndexChanging">
                                    <Columns>
                                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                          <asp:BoundField DataField="donortype" HeaderText="Donortype"></asp:BoundField>
                                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                        <asp:BoundField DataField="Employer" HeaderText="Employer">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                        <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                        
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                  </div>
                   <div>


        
                  </div>
                  <br />
                   <div align="center">
                   <table runat="server" id="Tbldonor"><tr><td   >
             <asp:Panel ID="Panel1" runat="server"  HorizontalAlign="Center">
             <asp:Button ID="BtnExport" runat="server" Text="Export to Excel" 
             OnClick="BtnExport_Click" />
             <br />
                                <b>Table 1: Donor Recognition Awards</b>
                      <asp:GridView ID="GridDonor" runat="server" align="center"    
                EnableViewState="true" AutoGenerateColumns="false"   DataKeyNames="DonVolAwardsID"
                                    OnRowCancelingEdit="GridDonor_RowCancelingEdit" 
                                    OnRowEditing="GridDonor_RowEditing" OnRowUpdating="GridDonor_RowUpdating" 
                >
                          <Columns>
                            <asp:TemplateField>
 
      <ItemTemplate>
      <asp:Button ID="btnEdit" Text="Modify" runat="server"   CommandName="Edit" />

      </ItemTemplate>
 
      <EditItemTemplate>
       <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update"  BackColor="SkyBlue" /> 

<asp:Button  ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel"  BackColor="SkyBlue" />
   
      </EditItemTemplate>

 
      </asp:TemplateField>
                                       <asp:TemplateField HeaderText="ID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblAwardID"   Text='<%#Eval("DonVolAwardsID") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
                                       <asp:TemplateField HeaderText="YEAR">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblYear"   Text='<%#Eval("YEAR") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

<asp:TemplateField HeaderText="Chapter">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblchapter"   Text='<%#Eval("Chapter") %>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDChapterUp" runat="server" Width="120px"  AutoPostBack="false" OnPreRender="DDChapterEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtChapter" Text='<%#Eval("Chapter") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Firstname">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblFirstName"   Text='<%#Eval("Firstname") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="Lastname">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblLastname"   Text='<%#Eval("lastname") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="City">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblCity"   Text='<%#Eval("City") %>' />
     </ItemTemplate>

</asp:TemplateField>
<asp:TemplateField HeaderText="state">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblState"   Text='<%#Eval("state") %>' />
     </ItemTemplate>
     
</asp:TemplateField>
<%--<asp:TemplateField HeaderText="MemberID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblMemberID"   Text='<%#Eval("MemberID") %>' />
     </ItemTemplate>
   
</asp:TemplateField>--%>

   <asp:TemplateField HeaderText="MemberID">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbMemberId" Text='<%#Eval("MemberID")%>' />
     </ItemTemplate>
     <EditItemTemplate>
    <asp:Label runat="server" ID="lbMember" Text='<%#Eval("MemberID")%>' />
         <asp:Button ID="BtnSearch" runat="server" Text="search"   OnClick="SearchMemberID"
            />
             <asp:TextBox   runat="server" Visible="false"  ID="TxtMember" Text='<%#Eval("MemberID") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
<asp:TemplateField HeaderText="DonorType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblDonortype"   Text='<%#Eval("DonorType") %>' />
     </ItemTemplate>
   
</asp:TemplateField>


           <asp:TemplateField HeaderText="RecType">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbRec" Text='<%#Eval("RecType")%>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDNewRecType" runat="server" Width="150px"  AutoPostBack="true" OnPreRender="GidRecType"  OnSelectedIndexChanged="DDNewRecType_SelectedIndexChanged" >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false"  ID="TxtRecType" Text='<%#Eval("RecType") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
  <asp:TemplateField HeaderText="AwardsType">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbawardRec" Text='<%#Eval("AwardsType")%>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDNewRecAwardType" runat="server" Width="150px"  AutoPostBack="false" OnPreRender="ddProductGroup"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecAwardType" Text='<%#Eval("AwardsType") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
<asp:TemplateField HeaderText="Anonymous">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblAnonD"   Text='<%#Eval("Anon") %>' />
     </ItemTemplate>
      <EditItemTemplate>
         <asp:DropDownList ID="DDAnon" runat="server" Width="70px"  AutoPostBack="false" OnPreRender="AnonandSpueEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecAnon" Text='<%#Eval("Anon") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Spouse">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblSpouseD"   Text='<%#Eval("Spouse") %>' />
     </ItemTemplate>
       <EditItemTemplate>
         <asp:DropDownList ID="DDSpouse" runat="server" Width="70px"  AutoPostBack="false" OnPreRender="AnonandSpueEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecSpouse" Text='<%#Eval("Spouse") %>' />
              </EditItemTemplate>
</asp:TemplateField>

                                       <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>
                                       
                                      
                                        
                                    </Columns>
            </asp:GridView>
            </asp:Panel></td></tr>
           </table>
           </div>

            <br />
            <div align="center">
          <table><tr><td  align="center" >
             <asp:Panel ID="Panel2" runat="server"  HorizontalAlign="Center">
              <asp:Button ID="Button1" runat="server" Text="Export to Excel" OnClick="Button1_Click" 
            />
            <br />
                                <b>Table 2: Volunteer Recognition Awards
</b>
                      <asp:GridView ID="GridVolunter" runat="server" align="center"    
              EnableViewState="true" AutoGenerateColumns="false"   DataKeyNames="DonVolAwardsID" 
                                    OnRowCancelingEdit="GridVolunter_RowCancelingEdit" 
                                    OnRowEditing="GridVolunter_RowEditing" OnRowUpdating="GridVolunter_RowUpdating"
                >
                                <Columns>
                                       
                                                                   <asp:TemplateField>
 
      <ItemTemplate>
      <asp:Button ID="btnEdit" Text="Modify" runat="server"   CommandName="Edit"  />

      </ItemTemplate>
 
      <EditItemTemplate>
       <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" /> 

<asp:Button  ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue"/>
   
      </EditItemTemplate>

 
      </asp:TemplateField>
                                       <asp:TemplateField HeaderText="ID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblAwardID"   Text='<%#Eval("DonVolAwardsID") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
                                       <asp:TemplateField HeaderText="YEAR">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblYear"   Text='<%#Eval("YEAR") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

<asp:TemplateField HeaderText="Chapter">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblchapter"   Text='<%#Eval("Chapter") %>' />
     </ItemTemplate>
      <EditItemTemplate>
         <asp:DropDownList ID="DDChapterUp" runat="server" Width="120px"  AutoPostBack="false" OnPreRender="DDChapterEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtChapter" Text='<%#Eval("Chapter") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Firstname">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblFirstName"   Text='<%#Eval("Firstname") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="Lastname">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblLastname"   Text='<%#Eval("lastname") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="City">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblCity"   Text='<%#Eval("City") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
<asp:TemplateField HeaderText="state">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblState"   Text='<%#Eval("state") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
   <asp:TemplateField HeaderText="MemberID">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbMemberId" Text='<%#Eval("MemberID")%>' />
     </ItemTemplate>
     <EditItemTemplate>
    <asp:Label runat="server" ID="lbMember" Text='<%#Eval("MemberID")%>' />
         <asp:Button ID="BtnSearch" runat="server" Text="search"   OnClick="SearchMemberID"
            />
             <asp:TextBox   runat="server" Visible="false"  ID="TxtMember" Text='<%#Eval("MemberID") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
<asp:TemplateField HeaderText="DonorType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblDonortype"   Text='<%#Eval("DonorType") %>' />
     </ItemTemplate>
   
</asp:TemplateField>


           <asp:TemplateField HeaderText="RecType">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbRec" Text='<%#Eval("RecType")%>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDNewRecType" runat="server" Width="150px"  AutoPostBack="true" OnPreRender="GidRecType"  OnSelectedIndexChanged="DDNewRecType_SelectedIndexChanged" >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false"  ID="TxtRecType" Text='<%#Eval("RecType") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
  <asp:TemplateField HeaderText="AwardsType">
     <ItemTemplate>
      
              <asp:Label runat="server" ID="lbawardRec" Text='<%#Eval("AwardsType")%>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDNewRecAwardType" runat="server" Width="150px"  AutoPostBack="false" OnPreRender="ddProductGroup"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecAwardType" Text='<%#Eval("AwardsType") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
                                      
                                        <asp:TemplateField HeaderText="Anonymous">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblAnon"   Text='<%#Eval("Anon") %>' />
     </ItemTemplate>
       <EditItemTemplate>
         <asp:DropDownList ID="DDAnon" runat="server" Width="70px"  AutoPostBack="false" OnPreRender="AnonandSpueEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecAnon" Text='<%#Eval("Anon") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Spouse">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblSpouse"   Text='<%#Eval("Spouse") %>' />
     </ItemTemplate>
      <EditItemTemplate>
         <asp:DropDownList ID="DDSpouse" runat="server" Width="70px"  AutoPostBack="false" OnPreRender="AnonandSpueEdit"  >
         
         </asp:DropDownList>
             <asp:TextBox   runat="server" Visible="false" ID="TxtRecSpouse" Text='<%#Eval("Spouse") %>' />
              </EditItemTemplate>
</asp:TemplateField>
                                    </Columns>
            </asp:GridView>
            </asp:Panel></td></tr>
           </table>
                  </div>
                
    
      <asp:TextBox   runat="server" ID="TxtRecType1" Visible="false"  />
       <asp:TextBox   runat="server" ID="TextMemberIDVal" Visible="false"  />
</asp:Content>

