USE [NSF]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 04/29/2014 13:02:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventCode] [varchar](10) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[EventYear] [int] NULL,
	[Status] [varchar](10) NULL,
	[CreateDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifyDate] [smalldatetime] NULL,
	[ModifiedBy] [int] NULL,
	[Game_Flag] [nvarchar](20) NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Event] UNIQUE NONCLUSTERED 
(
	[EventCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Event] ON
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (1, N'Finals', N'National Finals', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (2, N'Chapter', N'Chapter Contests', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA2A70000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (3, N'WkShop', N'Workshop', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (4, N'Game', N'Educational Game', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, N'Regional_Flag')
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (5, N'Walk', N'Walk-a-thon', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (6, N'DAS', N'Dollar-a-Square (DAS)', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (7, N'MatchGift', N'Matching Gifts', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (8, N'EmpVol', N'Employer Volunteer Program', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (9, N'FundRD', N'Fund Raising', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (10, N'Shop', N'Shop', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (11, N'Donation', N'Donation', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (12, N'Marathon', N'Marathon', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (13, N'Coaching', N'Coaching', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA2C20000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (14, N'Tournmnt', N'Tournment', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (15, N'GenAdmin', N'General and Admin', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (16, N'Grant', N'Grant', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (17, N'Transfer', N'Transfer', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (18, N'Excelathon', N'Excel-a-thon', 2014, N'O', CAST(0xA29E0000 AS SmallDateTime), 4240, CAST(0xA29F0000 AS SmallDateTime), 4240, NULL)
INSERT [dbo].[Event] ([EventId], [EventCode], [Name], [EventYear], [Status], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy], [Game_Flag]) VALUES (19, N'PrepClub', N'Prep-Clubs Coaching', 2014, N'N', CAST(0xA29E0000 AS SmallDateTime), 4240, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Event] OFF
/****** Object:  Default [DF__Event__CreateDat__214BF109]    Script Date: 04/29/2014 13:02:35 ******/
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF__Event__CreateDat__214BF109]  DEFAULT (getdate()) FOR [CreateDate]
GO
