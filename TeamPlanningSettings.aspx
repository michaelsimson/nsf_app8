﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="TeamPlanningSettings.aspx.cs" Inherits="TeamPlanning_TeamPlanning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="Scripts/jquery-1.9.1.js"></script>

    <div style="text-align: left">
        <script language="javascript" type="text/javascript">
            function PopupPicker(ctl, w, h) {
                var PopupWindow = null;
                settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
                PopupWindow.focus();
            }
            function confirmMsgToDelete() {
                if (confirm("Along with the Activity Group, all the activities underneath it will be deleted. Do you still want to do this?")) {
                    document.getElementById('<%= btnConfirmDelete.ClientID%>').click();
                }
            }
            function confirmActivityMshDelete() {
                if (confirm("Are you sure you want to delete this activity?")) {
                    document.getElementById('<%= btnConfirmActivityDelete.ClientID%>').click();
                }

            }

            function volunteerDelete() {
                if (confirm("Are you sure you want to delete this Volunteer In Charge?")) {
                    document.getElementById('<%= btnVolunteerConfirm.ClientID%>').click();
                }
            }
            function vendorDelete() {
                if (confirm("Are you sure you want to delete this Vendor?")) {
                    document.getElementById('<%= btnVendorConfirm.ClientID%>').click();
                }
            }
            function actVolunteerDelete() {
                if (confirm("Are you sure you want to delete this Volunteer In Charge?")) {
                    document.getElementById('<%= btnActVolunteerConfirm.ClientID%>').click();
                }
            }
            function actVendorDelete() {
                if (confirm("Are you sure you want to delete this Vendor?")) {
                    document.getElementById('<%= btnActVendorConfirm.ClientID%>').click();
                }
            }
            //Add date functuion
            //function AddDate(){
            //if (duration != "" && duration.length > 0) {
            //    duration = parseInt($(this).val());
            //    var day = endDate.substr(0, 2);
            //    var month = endDate.substr(3, 2);
            //    var year = endDate.substr(6, 4);
            //if (endDate.length < 10) {
            //    month = endDate.substr(0, 1);
            //    day = endDate.substr(2, 1);
            //    year = endDate.substr(4, 4);
            //}
            //var dateStr = day + " " + month + " " + year;
            //var dt = new Date(dateStr);
            //var newDate = new Date(dt);

            //newDate.setDate(dt.getDate() + duration);
            //var day = (newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate());
            //var month = newDate.getMonth();
            //var getMonth = parseInt(month) + 1;
            //var resultMonth = (getMonth < 10 ? "0" + getMonth : getMonth);
            //var resultEndDate = (resultMonth + "-" + day + "-" + newDate.getFullYear());
            //if (duration != "" && endDate != "") {


            //}
            //}
            //}

        </script>
        <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
         <asp:LinkButton ID="LinkButton1" CausesValidation="false" PostBackUrl="~/TeamPlanning.aspx" runat="server">Back to Team Planning</asp:LinkButton>
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
    </div>
    <asp:Button ID="btnConfirmDelete" Style="display: none;" runat="server" OnClick="btnConfirmDelete_Click" />
    <asp:Button ID="btnConfirmActivityDelete" Style="display: none;" runat="server" OnClick="btnConfirmActivityDelete_Click" />

    <asp:Button ID="btnVolunteerConfirm" Style="display: none;" runat="server" OnClick="btnVolunteerConfirm_Click" />
    <asp:Button ID="btnVendorConfirm" Style="display: none;" runat="server" OnClick="btnVendorConfirm_Click" />

    <asp:Button ID="btnActVolunteerConfirm" Style="display: none;" runat="server" OnClick="btnActVolunteerConfirm_Click" />
    <asp:Button ID="btnActVendorConfirm" Style="display: none;" runat="server" OnClick="btnActVendorConfirm_Click" />


    <table id="tblBeeBookSchedule" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Team Plan Settings</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                <div style="text-align: center">
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                </div>
                <br />


                <table style="margin-left: auto; margin-right: auto; width: 75%;">

                    <tr>
                        <td style="width: 70px"></td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select year</asp:ListItem>
                                <asp:ListItem Value="-1">All</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                            </asp:DropDownList>

                        </td>

                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                        <td style="width: 100px" align="left">
                            <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                        <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                            <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddTeams" DataTextField="TeamName" DataValueField="TeamID" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddTeams_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>

                        <td>

                            <asp:Button ID="btnAddNew" runat="server" Text="AddNew" OnClick="btnAddNew_Click" />
                        </td>
                        <td>

                            <asp:Button ID="btnExportToExcel" Visible="false" runat="server" Text="Export To Excel" OnClick="btnExportToExcel_Click" />

                        </td>



                    </tr>

                </table>

                <div style="text-align: center">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <div style="background-color: #FFFFCC; width: 75%; margin-bottom: 10px;">
                        <div style="background-color: #FFFFCC; width: 75%;">
                            <table width="75%" id="tblActivityGroup" runat="server" style="margin-left: auto; display: none; margin-bottom: 10px; margin-right: auto; font-weight: bold; background-color: #ffffcc;">
                                <tr class="ContentSubTitle">
                                    <td>Activity Group</td>
                                    <td>
                                        <asp:TextBox ID="txtActivityGroup" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Unit Of Time</td>
                                    <td>
                                        <asp:DropDownList ID="ddlUnitOfTime" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">Select Unit Of Time</asp:ListItem>
                                            <asp:ListItem Value="None">None</asp:ListItem>
                                            <asp:ListItem Value="Month">Month</asp:ListItem>
                                            <asp:ListItem Value="Week">Week</asp:ListItem>
                                            <asp:ListItem Value="Day">Day</asp:ListItem>
                                            <asp:ListItem Value="Hour">Hour</asp:ListItem>
                                            <asp:ListItem Value="15 Minutes">15 Minutes</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Begin Date</td>
                                    <td style="width: 250px;">
                                        <asp:TextBox ID="txtBeginDate" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        (MM-DD-YYYY)
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">End Date</td>
                                    <td>
                                        <asp:TextBox ID="txtGroupEndDate" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        (MM-DD-YYYY)
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Volunteer In Charge</td>
                                    <td>
                                        <asp:TextBox ID="txtVolunteerInCharge" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnVolunteerFind" OnClick="btnVolunteerFind_onClick" runat="server" Text="Search" Width="60" />
                                        <asp:Button ID="btnVolunteerDelete" runat="server" Text="Delete" Width="60" Visible="false" OnClick="btnVolunteerDelete_onClick" />
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Vendor</td>
                                    <td>
                                        <asp:TextBox ID="txtVendor" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnVendorFind" runat="server" Text="Search" Width="60" OnClick="btnVendorFind_onClick" />
                                        <asp:Button ID="btnVendorDelete" runat="server" Text="Delete" Width="60" Visible="false" OnClick="btnVendorDelete_onClick" />
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Status</td>
                                    <td>
                                        <asp:DropDownList ID="ddlStatus" runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                            <asp:ListItem Value="In Progress">In Progress</asp:ListItem>
                                            <asp:ListItem Value="Delayed">Delayed</asp:ListItem>
                                            <asp:ListItem Value="Finished">Finished</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Finished Date</td>
                                    <td>
                                        <asp:TextBox ID="txtFinishedDate" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        (MM-DD-YYYY)
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="visibility: hidden;">
                                        <asp:Button ID="Button3" runat="server" Text="Add" Width="60" />
                                        <asp:Button ID="Button4" runat="server" Text="Cancel" Width="60" />
                                    </td>
                                    <td align="left" style="width: 150px;">
                                        <asp:Button ID="btnSaveGroup" runat="server" Text="Add" Width="60" OnClick="btnSaveGroup_Click" />
                                        <asp:Button ID="btnCancelGroup" runat="server" Text="Cancel" Width="60" OnClick="btnCancelGroup_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                        <div style="clear: both;"></div>
                        <div id="dvActivityGroupDelete" runat="server" style="margin-left: 623px; margin-top: -34px; display: none;">
                            <asp:Button ID="btnActivityGroupDelete" runat="server" Text="Delete" Width="60" OnClick="btnActivityGroupDelete_Click" />
                        </div>
                    </div>
                </center>

                <asp:Panel ID="pIndSearch" runat="server" Width="1200px" Visible="False" HorizontalAlign="Center">
                    <b>Search NSF member</b>
                    <div align="center" style="width: 1200px;">
                        <table border="1" runat="server" id="tblIndSearch" style="text-align: center; margin-bottom: 10px;" width="30%" visible="true" bgcolor="silver">
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Donor Type:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server"
                                        OnSelectedIndexChanged="ddlDonorType_SelectedIndexChanged">
                                        <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                                        <asp:ListItem Value="Organization">Organization</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Organization Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlState" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>

                        <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Search Result</b>
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" OnRowCommand="GridMemberDt_RowCommand" HeaderStyle-BackColor="#ffffcc">
                                <Columns>
                                    <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </asp:Panel>

                <div id="dvActivityGroup" runat="server" style="width: 15%; margin-left: auto; margin-right: auto; display: none;"><span style="font-weight: bold;">Table 1 : </span><span>Activity Group</span></div>

                <div style="clear: both;"></div>
                <asp:GridView ID="grdTeamSettings" runat="server" OnRowCommand="grdTeamSettings_RowCommand" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>
                        <asp:TemplateField HeaderText="Up" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgRowUp" CssClass="activityRowUp" ImageUrl="~/Images/up-icon.png" runat="server" CommandName="Up" Width="18" Height="18" ToolTip="Up" Style="margin-top: 3px;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Down" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgRowDown" CssClass="activityRowDown" ImageUrl="~/Images/down-icon.png" CommandName="Down" runat="server" Width="18" Height="18" ToolTip="Down" Style="margin-top: 3px;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="175px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnModifyGroup" runat="server" Text="Modify" CommandName="Modify" />
                                <asp:Button ID="btnSelectGroup" runat="server" Text="Select" CommandName="Select" />
                                <div style="display: none;">
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                    <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTeamID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TeamID") %>'>'></asp:Label>
                                    <asp:Label ID="lblYear" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Year") %>'>'></asp:Label>
                                    <asp:Label ID="lblRowOrder" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Order") %>'>'></asp:Label>
                                    <asp:Label ID="lblGroupStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BegDate") %>'>'></asp:Label>
                                    <asp:Label ID="lblGroupEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndDate") %>'>'></asp:Label>
                                    <asp:Label ID="lblMemberID1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                    <asp:Label ID="lblVendorID1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VendorID") %>'>'></asp:Label>
                                     <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VDonorType") %>'>'></asp:Label>
                                </div>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:BoundField DataField="Order" Visible="true" HeaderText="Order" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="TeamSettingsID" Visible="true" HeaderText="ID" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="ActivityGroup" Visible="true" HeaderText="Activity Group" HeaderStyle-Width="200px" />
                        <asp:BoundField DataField="UnitOfTime" Visible="true" HeaderText="Unit Of Time" HeaderStyle-Width="90px"></asp:BoundField>
                        <asp:BoundField DataField="BegDate" Visible="true" HeaderText="Beg Date" HeaderStyle-Width="100px" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>

                        <asp:BoundField DataField="EndDate" Visible="true" HeaderText="End Date" HeaderStyle-Width="100px" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="Name" Visible="true" HeaderText="Vol Name" HeaderStyle-Width="150px" />
                        <asp:BoundField DataField="VendorName" Visible="true" HeaderText="Vendor Name" HeaderStyle-Width="150px" />

                        <asp:BoundField DataField="Status" Visible="true" HeaderText="Status" HeaderStyle-Width="100px"></asp:BoundField>
                        <asp:BoundField DataField="FinishDate" Visible="true" HeaderText="Finish Date" HeaderStyle-Width="100px" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    </Columns>
                </asp:GridView>
                <br />
                <div style="text-align: center">
                    <asp:Label ID="lblActivityMsg" runat="server"></asp:Label>
                </div>
                <center>
                    <div style="background-color: #FFFFCC; width: 75%; margin-bottom: 10px;">
                        <div style="background-color: #FFFFCC; width: 75%;">
                            <table width="75%" id="tblTeamActivity" runat="server" style="margin-left: auto; display: none; margin-right: auto; font-weight: bold; background-color: #ffffcc;">

                                <tr class="ContentSubTitle">

                                    <td>Activity</td>
                                    <td>
                                        <asp:TextBox ID="txtActivity" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Activity Group</td>
                                    <td>
                                        <asp:TextBox ID="txtGroupActivity" runat="server" Enabled="false" Width="150px" Height="18px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Duration</td>
                                    <td>
                                        <asp:TextBox ID="txtDuration" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td style="width: 125px;">End Date</td>
                                    <td>
                                        <asp:TextBox ID="txtEndDate" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        (MM-DD-YYYY)
                                        <a id="ancDatePicker" runat="server" href="javascript:PopupPicker('<%=txtEndDate.ClientID%>', 200, 200);"></a>
                                        <%--<asp:RequiredFieldValidator ControlToValidate="txtEndDate" ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                    </td>


                                </tr>

                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Volunteer In Charge</td>
                                    <td>
                                        <asp:TextBox ID="txtActivityVolInCharge" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnActivityVolFind" OnClick="btnActivityVolFind_onClick" runat="server" Text="Search" Width="60" />
                                        <asp:Button ID="btnActVolunteerDelete" runat="server" Text="Delete" Width="60" Visible="false" OnClick="btnActVolunteerDelete_onClick" />
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Vendor</td>
                                    <td>
                                        <asp:TextBox ID="txtActivityVendor" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnActivityVendorFind" runat="server" Text="Search" Width="60" OnClick="btnActivityVendorFind_onClick" />
                                        <asp:Button ID="btnActVendorDelete" runat="server" Text="Delete" Width="60" Visible="false" OnClick="btnActVendorDelete_onClick" />
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Status</td>
                                    <td>
                                        <asp:DropDownList ID="ddlActivityStatus" runat="server" OnSelectedIndexChanged="ddlActivityStatus_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                            <asp:ListItem Value="In Progress">In Progress</asp:ListItem>
                                            <asp:ListItem Value="Delayed">Delayed</asp:ListItem>
                                            <asp:ListItem Value="Finished">Finished</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td style="width: 125px;">Finished Date</td>
                                    <td>
                                        <asp:TextBox ID="txtActivityFinishDate" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        (MM-DD-YYYY)
                                    </td>
                                </tr>


                                <tr>
                                    <td align="left" style="visibility: hidden;">
                                        <asp:Button ID="Button1" runat="server" Text="Add" Width="60" />
                                        <asp:Button ID="Button2" runat="server" Text="Cancel" Width="60" />
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSaveActivity" runat="server" Text="Add" Width="60" OnClick="btnSaveActivity_Click" />
                                        <asp:Button ID="btnCancelActivity" runat="server" Text="Cancel" Width="60" OnClick="btnCancelActivity_Click" />
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div style="clear: both;"></div>
                        <div id="dvDeleteActivity" runat="server" style="margin-left: 623px; margin-top: -24px; display: none;">
                            <asp:Button ID="btnDeleteActivity" runat="server" Text="Delete" Width="60" OnClick="btnDeleteActivity_Click" />
                        </div>
                    </div>
                </center>

                <div style="clear: both;"></div>
                <asp:Panel ID="pnlActivitySearch" runat="server" Width="1200px" Visible="False" HorizontalAlign="Center">
                    <b>Search NSF member</b>
                    <div align="center" style="width: 1200px;">
                        <table border="1" runat="server" id="Table1" style="text-align: center; margin-bottom: 10px;" width="30%" visible="true" bgcolor="silver">
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Donor Type:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlDonorActivity" AutoPostBack="true" runat="server"
                                        OnSelectedIndexChanged="ddlDonorActivity_SelectedIndexChanged">
                                        <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                                        <asp:ListItem Value="Organization">Organization</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastnameActivity" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstnameActivity" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Organization Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtOrganizationName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtActivityEmail" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlSate" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnActivitVolSearch" runat="server" OnClick="btnActivitVolSearch_onClick" Text="Find" CausesValidation="False" />
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnActivityVolClose" runat="server" Text="Close" OnClick="btnActivityVolClose_onclick" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="Label1" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>

                        <asp:Panel ID="Panel2" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Search Result</b>
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdVolTeamActivity" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" OnRowCommand="grdVolTeamActivity_RowCommand" HeaderStyle-BackColor="#ffffcc">
                                <Columns>
                                    <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <div id="dvaddNewActivity" align="right" runat="server" style="margin-bottom: -24px; margin-left: auto; display: none; margin-right: auto;">
                    <asp:Button ID="btnAddNewActivity" runat="server" Text="Add New Activity"
                        Enabled="true" OnClick="btnAddNewActivity_Click" />

                </div>

                <div id="dvCalculateEndDate" align="right" runat="server" style="margin-bottom: -10px; width: 7%; display: none; margin-right: auto;">
                    <asp:Button ID="btnReCalculateEndDate" runat="server" Text="Recalculate End Date"
                        Enabled="true" OnClick="btnReCalculateEndDate_Click" />

                </div>
                <div id="dvTeamActivity" runat="server" style="width: 15%; margin-left: auto; margin-right: auto; display: none;"><span style="font-weight: bold;">Table 2 : </span><span>Team Activity</span></div>

                <asp:GridView ID="grdteamActivity" runat="server" OnRowCommand="grdteamActivity_RowCommand" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-bottom: 10px; margin-right: auto;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>
                        <asp:TemplateField HeaderText="Up" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgActivityRowUp" ToolTip="Up" CssClass="activityRowUp" ImageUrl="~/Images/up-icon.png" runat="server" CommandName="Up" Width="18" Height="18" Style="margin-top: 3px;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Down" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgActivityRowDown" ToolTip="Down" CssClass="activityRowDown" ImageUrl="~/Images/down-icon.png" CommandName="Down" runat="server" Width="18" Height="18" Style="margin-top: 3px;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnModifyGroup" runat="server" Text="Modify" CommandName="Modify" />
                                <div style="display: none;">
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                    <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTeamID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TeamID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTeamSettingsID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TeamSettingsID") %>'>'></asp:Label>
                                    <asp:Label ID="lblActivityRowOrder" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Order") %>'>'></asp:Label>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndDate") %>'>'></asp:Label>
                                    <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                    <asp:Label ID="lblVendorID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VendorID") %>'>'></asp:Label>
                                     <asp:Label ID="lblDonorType1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VDonorType") %>'>'></asp:Label>
                                </div>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:BoundField DataField="Order" Visible="true" HeaderText="Order" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="TeamActID" Visible="true" HeaderText="ID" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="Activity" Visible="true" HeaderText="Activity" HeaderStyle-Width="175px"></asp:BoundField>
                        <asp:BoundField DataField="ActivityGroup" Visible="true" HeaderText="Activity Group" HeaderStyle-Width="125px" />
                        <asp:BoundField DataField="Duration" Visible="true" HeaderText="Duration" HeaderStyle-Width="50px"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" Visible="true" HeaderText="End Date" HeaderStyle-Width="100px" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>

                        <asp:BoundField DataField="Name" Visible="true" HeaderText="Vol Name" HeaderStyle-Width="150px" />
                        <asp:BoundField DataField="VendorName" Visible="true" HeaderText="Vendor Name" HeaderStyle-Width="150px" />

                        <asp:BoundField DataField="Status" Visible="true" HeaderText="Status" HeaderStyle-Width="100px"></asp:BoundField>
                        <asp:BoundField DataField="FinishDate" Visible="true" HeaderText="Finish Date" HeaderStyle-Width="100px" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>

                    </Columns>
                </asp:GridView>
                <br />
                <div style="clear: both;">
                </div>
                <div runat="server" id="dvCloseActivitytable" visible="false" align="center" width="75%">
                    <asp:Button ID="btnCloseActivityTabel" runat="server" Text="Close Table 2" OnClick="btnCloseActivityTabel_Click" />
                </div>
                <div style="clear: both;">
                </div>
                <div style="text-align: center">
                    <asp:Label ID="lblTeamActivityGridMsg" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnEventID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnChapterID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnTeamID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnTeamSettingsID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnYear" Value="0" runat="server" />
    <asp:HiddenField ID="hdnTeamName" Value="0" runat="server" />
    <asp:HiddenField ID="hdnActivityGroup" Value="0" runat="server" />
    <asp:HiddenField ID="hdnActivityGroupID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnTeamActID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnEndDate" Value="0" runat="server" />
    <asp:HiddenField ID="hdnBeginDate" Value="0" runat="server" />
    <asp:HiddenField ID="hdnFinalDate" Value="0" runat="server" />
    <asp:HiddenField ID="hdnGroupDuration" Value="0" runat="server" />
    <asp:HiddenField ID="hdnMemberID" Value="" runat="server" />
    <asp:HiddenField ID="hdnVendorID" Value="" runat="server" />
    <asp:HiddenField ID="hdnMemberName" Value="" runat="server" />
    <asp:HiddenField ID="hdnDonorType" Value="" runat="server" />

    <asp:HiddenField ID="hdnMemberText" Value="" runat="server" />
</asp:Content>
