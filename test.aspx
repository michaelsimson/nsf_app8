﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="test.aspx.vb" Inherits="test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=Now%></title>
    <style type="text/css">
        #fountainTextG {
            width: 234px;
            margin: auto;
        }

        .fountainTextG {
            color: rgb(0,0,0);
            font-family: Arial;
            font-size: 24px;
            text-decoration: none;
            font-weight: normal;
            font-style: normal;
            float: left;
            animation-name: bounce_fountainTextG;
            -o-animation-name: bounce_fountainTextG;
            -ms-animation-name: bounce_fountainTextG;
            -webkit-animation-name: bounce_fountainTextG;
            -moz-animation-name: bounce_fountainTextG;
            animation-duration: 2.09s;
            -o-animation-duration: 2.09s;
            -ms-animation-duration: 2.09s;
            -webkit-animation-duration: 2.09s;
            -moz-animation-duration: 2.09s;
            animation-iteration-count: infinite;
            -o-animation-iteration-count: infinite;
            -ms-animation-iteration-count: infinite;
            -webkit-animation-iteration-count: infinite;
            -moz-animation-iteration-count: infinite;
            animation-direction: normal;
            -o-animation-direction: normal;
            -ms-animation-direction: normal;
            -webkit-animation-direction: normal;
            -moz-animation-direction: normal;
            transform: scale(.5);
            -o-transform: scale(.5);
            -ms-transform: scale(.5);
            -webkit-transform: scale(.5);
            -moz-transform: scale(.5);
        }

        #fountainTextG_1 {
            animation-delay: 0.75s;
            -o-animation-delay: 0.75s;
            -ms-animation-delay: 0.75s;
            -webkit-animation-delay: 0.75s;
            -moz-animation-delay: 0.75s;
        }

        #fountainTextG_2 {
            animation-delay: 0.9s;
            -o-animation-delay: 0.9s;
            -ms-animation-delay: 0.9s;
            -webkit-animation-delay: 0.9s;
            -moz-animation-delay: 0.9s;
        }

        #fountainTextG_3 {
            animation-delay: 1.05s;
            -o-animation-delay: 1.05s;
            -ms-animation-delay: 1.05s;
            -webkit-animation-delay: 1.05s;
            -moz-animation-delay: 1.05s;
        }

        #fountainTextG_4 {
            animation-delay: 1.2s;
            -o-animation-delay: 1.2s;
            -ms-animation-delay: 1.2s;
            -webkit-animation-delay: 1.2s;
            -moz-animation-delay: 1.2s;
        }

        #fountainTextG_5 {
            animation-delay: 1.35s;
            -o-animation-delay: 1.35s;
            -ms-animation-delay: 1.35s;
            -webkit-animation-delay: 1.35s;
            -moz-animation-delay: 1.35s;
        }

        #fountainTextG_6 {
            animation-delay: 1.5s;
            -o-animation-delay: 1.5s;
            -ms-animation-delay: 1.5s;
            -webkit-animation-delay: 1.5s;
            -moz-animation-delay: 1.5s;
        }

        #fountainTextG_7 {
            animation-delay: 1.64s;
            -o-animation-delay: 1.64s;
            -ms-animation-delay: 1.64s;
            -webkit-animation-delay: 1.64s;
            -moz-animation-delay: 1.64s;
        }




        @keyframes bounce_fountainTextG {
            0% {
                transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                transform: scale(.5);
                color: rgb(255,255,255);
            }
        }

        @-o-keyframes bounce_fountainTextG {
            0% {
                -o-transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                -o-transform: scale(.5);
                color: rgb(255,255,255);
            }
        }

        @-ms-keyframes bounce_fountainTextG {
            0% {
                -ms-transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                -ms-transform: scale(.5);
                color: rgb(255,255,255);
            }
        }

        @-webkit-keyframes bounce_fountainTextG {
            0% {
                -webkit-transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                -webkit-transform: scale(.5);
                color: rgb(255,255,255);
            }
        }

        @-moz-keyframes bounce_fountainTextG {
            0% {
                -moz-transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                -moz-transform: scale(.5);
                color: rgb(255,255,255);
            }
        }

        .windows8 {
            position: relative;
            width: 78px;
            height: 78px;
            margin: auto;
        }

            .windows8 .wBall {
                position: absolute;
                width: 74px;
                height: 74px;
                opacity: 0;
                transform: rotate(225deg);
                -o-transform: rotate(225deg);
                -ms-transform: rotate(225deg);
                -webkit-transform: rotate(225deg);
                -moz-transform: rotate(225deg);
                animation: orbit 6.96s infinite;
                -o-animation: orbit 6.96s infinite;
                -ms-animation: orbit 6.96s infinite;
                -webkit-animation: orbit 6.96s infinite;
                -moz-animation: orbit 6.96s infinite;
            }

                .windows8 .wBall .wInnerBall {
                    position: absolute;
                    width: 10px;
                    height: 10px;
                    background: rgb(30,209,101);
                    left: 0px;
                    top: 0px;
                    border-radius: 10px;
                }

            .windows8 #wBall_1 {
                animation-delay: 1.52s;
                -o-animation-delay: 1.52s;
                -ms-animation-delay: 1.52s;
                -webkit-animation-delay: 1.52s;
                -moz-animation-delay: 1.52s;
            }

            .windows8 #wBall_2 {
                animation-delay: 0.3s;
                -o-animation-delay: 0.3s;
                -ms-animation-delay: 0.3s;
                -webkit-animation-delay: 0.3s;
                -moz-animation-delay: 0.3s;
            }

            .windows8 #wBall_3 {
                animation-delay: 0.61s;
                -o-animation-delay: 0.61s;
                -ms-animation-delay: 0.61s;
                -webkit-animation-delay: 0.61s;
                -moz-animation-delay: 0.61s;
            }

            .windows8 #wBall_4 {
                animation-delay: 0.91s;
                -o-animation-delay: 0.91s;
                -ms-animation-delay: 0.91s;
                -webkit-animation-delay: 0.91s;
                -moz-animation-delay: 0.91s;
            }

            .windows8 #wBall_5 {
                animation-delay: 1.22s;
                -o-animation-delay: 1.22s;
                -ms-animation-delay: 1.22s;
                -webkit-animation-delay: 1.22s;
                -moz-animation-delay: 1.22s;
            }



        @keyframes orbit {
            0% {
                opacity: 1;
                z-index: 99;
                transform: rotate(180deg);
                animation-timing-function: ease-out;
            }

            7% {
                opacity: 1;
                transform: rotate(300deg);
                animation-timing-function: linear;
                origin: 0%;
            }

            30% {
                opacity: 1;
                transform: rotate(410deg);
                animation-timing-function: ease-in-out;
                origin: 7%;
            }

            39% {
                opacity: 1;
                transform: rotate(645deg);
                animation-timing-function: linear;
                origin: 30%;
            }

            70% {
                opacity: 1;
                transform: rotate(770deg);
                animation-timing-function: ease-out;
                origin: 39%;
            }

            75% {
                opacity: 1;
                transform: rotate(900deg);
                animation-timing-function: ease-out;
                origin: 70%;
            }

            76% {
                opacity: 0;
                transform: rotate(900deg);
            }

            100% {
                opacity: 0;
                transform: rotate(900deg);
            }
        }

        @-o-keyframes orbit {
            0% {
                opacity: 1;
                z-index: 99;
                -o-transform: rotate(180deg);
                -o-animation-timing-function: ease-out;
            }

            7% {
                opacity: 1;
                -o-transform: rotate(300deg);
                -o-animation-timing-function: linear;
                -o-origin: 0%;
            }

            30% {
                opacity: 1;
                -o-transform: rotate(410deg);
                -o-animation-timing-function: ease-in-out;
                -o-origin: 7%;
            }

            39% {
                opacity: 1;
                -o-transform: rotate(645deg);
                -o-animation-timing-function: linear;
                -o-origin: 30%;
            }

            70% {
                opacity: 1;
                -o-transform: rotate(770deg);
                -o-animation-timing-function: ease-out;
                -o-origin: 39%;
            }

            75% {
                opacity: 1;
                -o-transform: rotate(900deg);
                -o-animation-timing-function: ease-out;
                -o-origin: 70%;
            }

            76% {
                opacity: 0;
                -o-transform: rotate(900deg);
            }

            100% {
                opacity: 0;
                -o-transform: rotate(900deg);
            }
        }

        @-ms-keyframes orbit {
            0% {
                opacity: 1;
                z-index: 99;
                -ms-transform: rotate(180deg);
                -ms-animation-timing-function: ease-out;
            }

            7% {
                opacity: 1;
                -ms-transform: rotate(300deg);
                -ms-animation-timing-function: linear;
                -ms-origin: 0%;
            }

            30% {
                opacity: 1;
                -ms-transform: rotate(410deg);
                -ms-animation-timing-function: ease-in-out;
                -ms-origin: 7%;
            }

            39% {
                opacity: 1;
                -ms-transform: rotate(645deg);
                -ms-animation-timing-function: linear;
                -ms-origin: 30%;
            }

            70% {
                opacity: 1;
                -ms-transform: rotate(770deg);
                -ms-animation-timing-function: ease-out;
                -ms-origin: 39%;
            }

            75% {
                opacity: 1;
                -ms-transform: rotate(900deg);
                -ms-animation-timing-function: ease-out;
                -ms-origin: 70%;
            }

            76% {
                opacity: 0;
                -ms-transform: rotate(900deg);
            }

            100% {
                opacity: 0;
                -ms-transform: rotate(900deg);
            }
        }

        @-webkit-keyframes orbit {
            0% {
                opacity: 1;
                z-index: 99;
                -webkit-transform: rotate(180deg);
                -webkit-animation-timing-function: ease-out;
            }

            7% {
                opacity: 1;
                -webkit-transform: rotate(300deg);
                -webkit-animation-timing-function: linear;
                -webkit-origin: 0%;
            }

            30% {
                opacity: 1;
                -webkit-transform: rotate(410deg);
                -webkit-animation-timing-function: ease-in-out;
                -webkit-origin: 7%;
            }

            39% {
                opacity: 1;
                -webkit-transform: rotate(645deg);
                -webkit-animation-timing-function: linear;
                -webkit-origin: 30%;
            }

            70% {
                opacity: 1;
                -webkit-transform: rotate(770deg);
                -webkit-animation-timing-function: ease-out;
                -webkit-origin: 39%;
            }

            75% {
                opacity: 1;
                -webkit-transform: rotate(900deg);
                -webkit-animation-timing-function: ease-out;
                -webkit-origin: 70%;
            }

            76% {
                opacity: 0;
                -webkit-transform: rotate(900deg);
            }

            100% {
                opacity: 0;
                -webkit-transform: rotate(900deg);
            }
        }

        @-moz-keyframes orbit {
            0% {
                opacity: 1;
                z-index: 99;
                -moz-transform: rotate(180deg);
                -moz-animation-timing-function: ease-out;
            }

            7% {
                opacity: 1;
                -moz-transform: rotate(300deg);
                -moz-animation-timing-function: linear;
                -moz-origin: 0%;
            }

            30% {
                opacity: 1;
                -moz-transform: rotate(410deg);
                -moz-animation-timing-function: ease-in-out;
                -moz-origin: 7%;
            }

            39% {
                opacity: 1;
                -moz-transform: rotate(645deg);
                -moz-animation-timing-function: linear;
                -moz-origin: 30%;
            }

            70% {
                opacity: 1;
                -moz-transform: rotate(770deg);
                -moz-animation-timing-function: ease-out;
                -moz-origin: 39%;
            }

            75% {
                opacity: 1;
                -moz-transform: rotate(900deg);
                -moz-animation-timing-function: ease-out;
                -moz-origin: 70%;
            }

            76% {
                opacity: 0;
                -moz-transform: rotate(900deg);
            }

            100% {
                opacity: 0;
                -moz-transform: rotate(900deg);
            }
        }
    </style>
    <link href="css/Loader/Loading.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="js/jquery.isloading.min.js"></script>
    <script src="js/jquery.isloading.js"></script>
    <script type="text/javascript">

        $(function (e) {
            $("#bgTest").css("backgroud-color", "black");
        });

    </script>
</head>
<body id="bgTest">
    <form id="form1" runat="server">
        <div>
            <center>
                <asp:TextBox ID="TextBox1" Width="300px" Text="d:\inetpub\wwwroot\northsouth\app9" runat="server"></asp:TextBox><br />
                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Find Files" />
                &nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" Text="Folders" OnClick="Button1_Click" /></center>

        </div>
        <asp:TextBox ID="txtIP" runat="server"></asp:TextBox>
        <asp:Button ID="Button2" OnClick="Button2_Click" runat="server" Text="testIP" />
        <br />
        <br />
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Button ID="Button3" runat="server" Text="decode" />
        <asp:Label ID="lblerr" runat="server"></asp:Label>
        <br />
        <br />

        <div class="windows8">
            <div class="wBall" id="wBall_1">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_2">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_3">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_4">
                <div class="wInnerBall"></div>
            </div>
            <div class="wBall" id="wBall_5">
                <div class="wInnerBall"></div>
            </div>
        </div>
    </form>
</body>
</html>
