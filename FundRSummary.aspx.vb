﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class FundRSummary
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            loadgrid()
            loadCheckgrid()
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
                hlnkMainPage.NavigateUrl = "FundRReg.aspx"
                hlnkMainPage.Text = "Back to Fundraising Registration"
            Else
                hlnkMainPage.Text = "Back to Fundraising Registration"
                hlnkMainPage.NavigateUrl = "FundRReg.aspx?id=1"
            End If
        End If
        If Not Session("EventDesc") Is Nothing Then
            lblEventDesc.Text = Session("EventDesc")
        End If
    End Sub
    Private Sub loadgrid()
        Dim inxAdult, inxChild As Integer
        lblAdult.Text = 0
        lblChildren.Text = 0
        inxAdult = -1
        inxChild = -1
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.ChapterID, FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID, CASE WHEN FR.Quantity IS NULL THEN 0 else FR.Quantity END AS Quantity,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN  FF.Share = 'No' THEN FF.FeeFrom ELSE 100.00 END else FR.UnitFee END AS  FEE,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN FF.FeeTo IS NULL AND FF.Share = 'No' THEN 'False' ELSE 'True' END else 'True' END AS  FeeEnabled, Case When FF.Exclusive = 'Yes' OR FF.Share ='Yes' THEN 'False' ELSE 'TRUE' END AS QuantityEnabled, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.PaymentMode, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode as SelPaymentMode"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear and FR.PaymentMode = 'Credit Card' and FR.MemberId = " & Session("CustIndID")
        strSQL = strSQL & " WHERE FF.FundRCalID = " & Session("FundRCalID") & " AND FR.PaymentReference is Null  AND FR.DonorType ='" & Session("FundDonorType") & "' "
        strSQL = strSQL & " order by FF.ProductGroupID,FF.FeeFrom Desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        
        If dt.Rows.Count > 0 Then
            dgCatalog.Visible = True
            Session("FundRItems") = "(" & Session("CustIndID") & ")(" & dt.Rows(0)("ChapterID") & ") : "
            Session("CustIndChapterID") = dt.Rows(0)("ChapterID")
            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(i)
                If dr("ProductCode").ToString.ToLower = "adult" Then
                    lblAdult.Text = dr("Quantity")
                    inxAdult = i
                    Continue For
                End If
                If dr("ProductCode").ToString.ToLower = "child" Then
                    lblChildren.Text = dr("Quantity")
                    inxChild = i
                    Continue For
                End If
                If i > 0 Then
                    Session("FundRItems") = Session("FundRItems") & ", "
                End If
                Session("FundRItems") = Session("FundRItems") & "(" & dt.Rows(i)("FundRFeesID") & ")(" & dt.Rows(i)("Quantity") & ")(" & String.Format("{0:f2}", CType(dt.Rows(i)("FEE"), Decimal)) & ")"
            Next
           
            If inxAdult > 0 Then
                dt.Rows(inxAdult).Delete()
            End If
            If inxChild > 0 Then
                dt.Rows(inxChild).Delete()
            End If
            dgCatalog.DataSource = dt
            dgCatalog.DataBind()
            Session("FundRAmt") = CalcAmount(dgCatalog)

            If Not Session("Discount") Is Nothing Then
                lblTotalDiscount.Visible = True
                lblTotalDiscount.Text = "Discount  :  $" & Session("Discount")
            End If
            lblTotal.Text = "Total  :  $" & Session("FundRAmt")
        Else
            btnPay.Enabled = False
        End If
    End Sub

    Protected Sub dgCatalog_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, lblFeeFrom As Label, lblFeeTo As Label, lblPaymentMethod As Label, lblAmt As Label
                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                lblPaymentMethod = CType(e.Item.FindControl("lblPaymentMethod"), Label)
                lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("SelPaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                    'ElseIf PaymentMode = "Check" Then
                    '    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                    '    lblCheckAmt.Text = lblamount.Text
                    '    lblCheckAmt.Visible = True
                    'ElseIf PaymentMode = "In Kind" Then
                    '    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                    '    lblInKindAmt.Text = lblamount.Text
                    '    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    Private Sub loadCheckgrid()
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.ChapterID, FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID, CASE WHEN FR.Quantity IS NULL THEN 0 else FR.Quantity END AS Quantity,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN  FF.Share = 'No' THEN FF.FeeFrom ELSE 100.00 END else FR.UnitFee END AS  FEE,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN FF.FeeTo IS NULL AND FF.Share = 'No' THEN 'False' ELSE 'True' END else 'True' END AS  FeeEnabled, Case When FF.Exclusive = 'Yes' OR FF.Share ='Yes' THEN 'False' ELSE 'TRUE' END AS QuantityEnabled, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.PaymentMode, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode as SelPaymentMode"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear and FR.PaymentMode = 'Check' and FR.MemberId = " & Session("CustIndID")
        strSQL = strSQL & " WHERE FF.FundRCalID = " & Session("FundRCalID") & " AND FR.PaymentReference is Null  AND FR.DonorType ='" & Session("FundDonorType") & "'"
        strSQL = strSQL & " order by FF.ProductGroupID,FF.FeeFrom Desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        dgCheck.DataSource = dt
        dgCheck.DataBind()
        If dt.Rows.Count > 0 Then
            dgCheck.Visible = True
            btnExit.Visible = True
            lblCheckAmt.Text = "Total  :  $" & CalcAmount(dgCheck)
        End If
    End Sub

    Protected Sub dgCheck_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, lblFeeFrom As Label, lblFeeTo As Label, lblPaymentMethod As Label, lblAmt As Label
                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                lblPaymentMethod = CType(e.Item.FindControl("lblPaymentMethod"), Label)
                lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("SelPaymentMode").ToString().Trim
                'If PaymentMode = "Credit Card" Then
                '    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                '    lblCreditCardAmt.Text = lblamount.Text
                '    lblCreditCardAmt.Visible = True
                '    'Else
                If PaymentMode = "Check" Then
                    divCheck.Visible = True
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                    'ElseIf PaymentMode = "In Kind" Then
                    '    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                    '    lblInKindAmt.Text = lblamount.Text
                    '    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    Function CalcAmount(ByVal dg As DataGrid) As Decimal
        Dim totamount As Decimal = 0.0
        Dim chkAmt As Decimal = 0.0
        Dim CreditAmt As Decimal = 0.0
        Dim InkindAmt As Decimal = 0.0
        Dim item As DataGridItem
        For Each item In dg.Items
            Dim lblamount As Label = CType(item.FindControl("lblAmount"), Label)
            Dim lblCreditCardAmt As Label = CType(item.FindControl("lblCreditCardAmt"), Label)
            Dim lblCheckAmt As Label = CType(item.FindControl("lblCheckAmt"), Label)
            Dim lblInKindAmt As Label = CType(item.FindControl("lblInKindAmt"), Label)
            Dim PaymentMode As String = CType(item.FindControl("lblSelPaymentMode"), Label).Text.Trim
            'If PaymentMode = "Credit Card" Then
            '    lblCreditCardAmt.Text = lblamount.Text
            '    lblCreditCardAmt.Visible = True
            '    lblCheckAmt.Visible = False
            '    lblInKindAmt.Visible = False
            '    If lblamount.Text.Length > 0 Then
            '        CreditAmt = CreditAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
            '    End If
            'Else
            If PaymentMode = "Check" Then
                lblCreditCardAmt.Visible = False
                divCheck.Visible = True
                lblCheckAmt.Visible = True
                lblInKindAmt.Visible = False
                lblCheckAmt.Text = lblamount.Text
                If lblamount.Text.Length > 0 Then
                    chkAmt = chkAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
                End If
                'ElseIf PaymentMode = "In Kind" Then
                '    lblInKindAmt.Text = lblamount.Text
                '    lblCreditCardAmt.Visible = False
                '    lblCheckAmt.Visible = False
                '    lblInKindAmt.Visible = True
                '    If lblamount.Text.Length > 0 Then
                '        InkindAmt = InkindAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
                '    End If
            End If
            'lblCheck.Text = String.Format(chkAmt, "{0:c}")
            ''lblCredit.Text = String.Format(CreditAmt, "{0:F2}")
            'lblInKind.Text = String.Format(InkindAmt, "{0:F2}")
            If lblamount.Text.Length > 0 Then
                totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
            End If
        Next
        Return totamount
    End Function

    Protected Sub btnPay_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("TermsAndConditions.aspx")
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
            Response.Redirect("FundRReg.aspx")
        Else
            Response.Redirect("FundRReg.aspx?id=1")
        End If
    End Sub
End Class
