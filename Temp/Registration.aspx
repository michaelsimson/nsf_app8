<%@ Page Language="vb" EnableEventValidation="true"  MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.Registration" CodeFile="Registration.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">	
        <Div>
		<script language="javascript">
			window.history.forward(1);
		</script>
		<script type="text/javascript"  language="jscript">
		
		function StateNameClientCallback(result,context)
		{	
		    alert(context);
		    if(context== "ddlCountryOfOriginInd")
		    {
		        if(result != "")
		        {	
		             var names = result.split('|');                    
                     var elSel = document.getElementById('_ctl0:Content_main:ddlStateOfOriginInd');
                     //document.getElementById('_ctl0:Content_main:txtStateOfOriginInd').style.visibility = "hidden";
                     document.getElementById('_ctl0:Content_main:ddlStateOfOriginInd').style.visibility ="visible";
                     elSel.options.length = 0;
                     for (var i = 0; i <names.length; ++i)
                     {                      
                        var values = names[i].split('^');
                        if(values =="") break;                     
                        var option = document.createElement("OPTION");
                        option.value = values[1];
                        option.innerHTML = values[0];                     
                        document.getElementById('_ctl0:Content_main:ddlStateOfOriginInd').appendChild(option);                                        
                     }                
		        }
		        else
		        {		        
		            //document.getElementById('_ctl0:Content_main:ddlStateOfOriginInd').style.visibility = "hidden";
		            document.getElementById('_ctl0:Content_main:txtStateOfOriginInd').style.visibility = "visible";
		        }
		    }
		    else if(context == "ddlCountryOfOriginSp")
		    {
		        if(result != "")
		        {	
		             var names = result.split('|');                    
                     var elSel = document.getElementById('_ctl0:Content_main:ddlStateOfOriginSp');
                     //document.getElementById('_ctl0:Content_main:txtStateOfOriginSp').style.visibility = "hidden";
                     document.getElementById('_ctl0:Content_main:ddlStateOfOriginSp').style.visibility ="visible";
                     elSel.options.length = 0;
                     for (var i = 0; i <names.length; ++i)
                     {                      
                        var values = names[i].split('^');
                        if(values =="") break;                     
                        var option = document.createElement("OPTION");
                        option.value = values[1];
                        option.innerHTML = values[0];                     
                        document.getElementById('_ctl0:Content_main:ddlStateOfOriginSp').appendChild(option);                                        
                     }                
		        }
		        else
		        {		        
		           // document.getElementById('_ctl0:Content_main:ddlStateOfOriginSp').style.visibility = "hidden";
		            document.getElementById('_ctl0:Content_main:txtStateOfOriginSp').style.visibility = "visible";
		        }
		    }
		 }
		function PopualteAddress()
		{		    
			document.getElementById('_ctl0:Content_main:txtAddress1Sp').value  = document.getElementById('_ctl0:Content_main:txtAddress1Ind').value;
			document.getElementById('_ctl0:Content_main:txtAddress2Sp').value  = document.getElementById('_ctl0:Content_main:txtAddress2Ind').value;
			document.getElementById('_ctl0:Content_main:txtCitySp').value  = document.getElementById('_ctl0:Content_main:txtCityInd').value;
			document.getElementById('_ctl0:Content_main:txtZipSp').value  = document.getElementById('_ctl0:Content_main:txtZipInd').value;
			document.getElementById('_ctl0:Content_main:txtHomePhoneSp').value  = document.getElementById('_ctl0:Content_main:txtHomePhoneInd').value;
			document.getElementById('_ctl0:Content_main:ddlStateSp').value = document.getElementById('_ctl0:Content_main:ddlStateInd').value;
			if(document.getElementById('_ctl0:Content_main:ddlGenderInd').value == "Male")
			    document.getElementById('_ctl0:Content_main:ddlGenderSp').value = "Female";
			else if(document.getElementById('_ctl0:Content_main:ddlGenderInd').value == "Female")
			    document.getElementById('_ctl0:Content_main:ddlGenderSp').value = "Male";
			else
			    document.getElementById('_ctl0:Content_main:ddlGenderSp').selectedIndex = 0;
			document.getElementById('_ctl0:Content_main:ddlCountrySp').value = document.getElementById('_ctl0:Content_main:ddlCountryInd').value;
		}
		</script>
		</Div>
		<div>
	    
			<table cellspacing="1" cellpadding="3" width="80%"   border="0" >                
			    <tr>
			        <td>
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
			            <asp:HyperLink runat="server" ID="hlnkPrevPage" Visible="false" Text="Back Search Results" ></asp:HyperLink>
			            			        </td>
			    </tr>
				<tr bgcolor="#FFFFFF" align="middle" >
					<TD class="Heading" colspan="2" align="center" ><b>Personal Information</b>
					</TD>       
				</tr>
				</table>
				<table cellspacing="1" cellpadding="3" width="60%"   border="0" >                
				<tr bgcolor="#FFFFFF">
					<td >
						<table id="tblIndividual" width="60%" cellspacing="1" cellpadding="3">
							<tr bgcolor="#FFFFFF">
								<th style="height: 43px">
								</th>
								<th noWrap align="left" colSpan="2" style="height: 43px">
									<h2>
                                        Father/Head</h2>
								</th>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Title:</TD>
								<td noWrap align="left"><asp:dropdownlist id="ddlTitleInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Title</asp:ListItem>
										<asp:ListItem Value="Mr">Mr.</asp:ListItem>
										<asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
										<asp:ListItem Value="Miss">Miss.</asp:ListItem>
										<asp:ListItem Value="Dr">Dr.</asp:ListItem>
										<asp:ListItem Value="Ms">Ms.</asp:ListItem>
										<asp:ListItem Value="Prof">Prof.</asp:ListItem>
                                    <asp:ListItem>Late</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvTitleInd" runat="server" ControlToValidate="ddlTitleInd" Display="Dynamic"
										ErrorMessage="Title Should be Selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">First Name:</td>
								<td noWrap align="left"><asp:textbox id="txtFirstNameInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstName" runat="server" ControlToValidate="txtFirstNameInd" ErrorMessage="First Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Last Name:</td>
								<td noWrap align="left"><asp:textbox id="txtLastNameInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvLastName" runat="server" ControlToValidate="txtLastNameInd" ErrorMessage="Last Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Address1:</td>
								<td noWrap align="left"><asp:textbox id="txtAddress1Ind" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvAddress1" runat="server" ControlToValidate="txtAddress1Ind" ErrorMessage="Address is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Address2:</td>
								<td noWrap align="left"><asp:textbox id="txtAddress2Ind" runat="server" CssClass="SmallFont"></asp:textbox></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">City:</td>
								<td noWrap align="left"><asp:textbox id="txtCityInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvCity" runat="server" ControlToValidate="txtCityInd" ErrorMessage="City is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   align="right">
                                    State:</td>
								<td  align="left"><asp:dropdownlist id="ddlStateInd" runat="server" CssClass="SmallFont"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvStateInd" runat="server" ControlToValidate="ddlStateInd" Display="Dynamic"
										ErrorMessage="State should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">ZIP/Postal 
									Code:</td>
								<td  noWrap align="left">
								    <asp:textbox id="txtZipInd" runat="server" CssClass="SmallFont"></asp:textbox>
								        <asp:requiredfieldvalidator id="rfvZip" runat="server" ControlToValidate="txtZipInd" ErrorMessage="Zip is required."></asp:requiredfieldvalidator>
                                    <br />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtZipInd"
                                        ErrorMessage="Zip code should contain 5 digits" ValidationExpression="\d{5}(-\d{4})?"></asp:RegularExpressionValidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 20px" noWrap align="right">Country:</td>
								<td style="HEIGHT: 20px" noWrap align="left"><asp:dropdownlist id="ddlCountryInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" " Selected="True">Select Country</asp:ListItem>
										<asp:ListItem Value="US">United States</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountry" runat="server" ControlToValidate="ddlCountryInd" Display="Dynamic"
										ErrorMessage="Country is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right" style="height: 10px">Gender:</td>
								<td  noWrap align="left" style="height: 10px"><asp:dropdownlist id="ddlGenderInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Gender</asp:ListItem>
										<asp:ListItem Value="Male">Male</asp:ListItem>
										<asp:ListItem Value="Female">Female</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvGenderInd" runat="server" ControlToValidate="ddlGenderInd" Display="Dynamic"
										ErrorMessage="Gender should be Selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right" style="height: 14px">Home Phone:</td>
								<td noWrap align="left" style="height: 14px"><asp:textbox id="txtHomePhoneInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic"
										ErrorMessage="Home Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator>
                                    <br />
										<asp:requiredfieldvalidator id="rfvHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic" ErrorMessage="Home Phone Number is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Cell Phone:</td>
								<td noWrap align="left"><asp:textbox id="txtCellPhoneInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revCellPhoneInd" runat="server" ControlToValidate="txtCellPhoneInd" Display="Dynamic"
										ErrorMessage="Cell Phone No should be in xxx-xxx-xxxx format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator>&nbsp;
								</td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Work Phone:</td>
								<td noWrap align="left"><asp:textbox id="txtWorkPhoneInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revWorkPhoneInd" runat="server" ControlToValidate="txtWorkPhoneInd" Display="Dynamic"
										ErrorMessage="Work Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator>&nbsp;
										</td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right" style="height: 38px">Work Fax:</td>
								<td noWrap align="left" style="height: 38px"><asp:textbox id="txtWorkFaxInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revWorkFaxInd" runat="server" ControlToValidate="txtWorkFaxInd" Display="Dynamic"
										ErrorMessage="Work Fax should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator>										
							    </td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Primary E-mail:</td>
								<td noWrap align="left"><asp:textbox id="txtPrimaryEmailInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator><br />
                                    <asp:requiredfieldvalidator id="rfvEmail" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="Email is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Secondary E-mail:</td>
								<td noWrap align="left"><asp:textbox id="txtSecondaryEmailInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revSecondaryEMail" runat="server" ControlToValidate="txtSecondaryEmailInd" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Educational Specialty:</td>
								<td noWrap align="left"><asp:dropdownlist id="ddlEducationalInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select specialty</asp:ListItem>
										<asp:ListItem Value="Accounting">Accounting</asp:ListItem>
										<asp:ListItem Value="Arts">Arts</asp:ListItem>
										<asp:ListItem Value="Business School">Business School</asp:ListItem>
										<asp:ListItem Value="Dentistry">Dentistry</asp:ListItem>
										<asp:ListItem Value="Economics">Economics</asp:ListItem>
										<asp:ListItem Value="Engineering">Engineering</asp:ListItem>
										<asp:ListItem Value="English">English</asp:ListItem>
										<asp:ListItem Value="Finance">Finance</asp:ListItem>
										<asp:ListItem Value="Geography">Geography</asp:ListItem>
										<asp:ListItem Value="History">History</asp:ListItem>
										<asp:ListItem Value="Humanities">Humanities</asp:ListItem>
										<asp:ListItem Value="International Relations">International Relations</asp:ListItem>
										<asp:ListItem Value="IT">IT</asp:ListItem>
										<asp:ListItem Value="Journalism">Journalism</asp:ListItem>
										<asp:ListItem Value="Law">Law</asp:ListItem>
										<asp:ListItem Value="Math">Math</asp:ListItem>
										<asp:ListItem Value="Medicine">Medicine</asp:ListItem>
										<asp:ListItem Value="Pharmacy">Pharmacy</asp:ListItem>
										<asp:ListItem Value="Public Relations">Public Relations</asp:ListItem>
										<asp:ListItem Value="Science">Science</asp:ListItem>
										<asp:ListItem Value="Statistics">Statistics</asp:ListItem>
										<asp:ListItem Value="Teaching">Teaching</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Career Specialty:</td>
								<td noWrap align="left"><asp:dropdownlist id="ddlCareerInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select specialty</asp:ListItem>
										<asp:ListItem Value="Advertising">Advertising</asp:ListItem>
										<asp:ListItem Value="Banking">Banking</asp:ListItem>
										<asp:ListItem Value="Brokerage House">Brokerage House</asp:ListItem>
										<asp:ListItem Value="Dentist">Dentist</asp:ListItem>
										<asp:ListItem Value="Doctor">Doctor</asp:ListItem>
										<asp:ListItem Value="Engineer">Engineer</asp:ListItem>
										<asp:ListItem Value="Finance">Finance</asp:ListItem>
										<asp:ListItem Value="Human Resources">Human Resources</asp:ListItem>
										<asp:ListItem Value="Insurance">Insurance</asp:ListItem>
										<asp:ListItem Value="Investment Banking">Investment Banking</asp:ListItem>
										<asp:ListItem Value="IT">IT</asp:ListItem>
										<asp:ListItem Value="Journalism">Journalism</asp:ListItem>
										<asp:ListItem Value="Lawyer">Lawyer</asp:ListItem>
										<asp:ListItem Value="Library">Library</asp:ListItem>
										<asp:ListItem Value="Marketing">Marketing</asp:ListItem>
										<asp:ListItem Value="Media">Media</asp:ListItem>
										<asp:ListItem Value="Public Service">Public Service</asp:ListItem>
										<asp:ListItem Value="Real Estate">Real Estate</asp:ListItem>
										<asp:ListItem Value="Social Service">Social Service</asp:ListItem>
										<asp:ListItem Value="Teaching">Teaching</asp:ListItem>
										<asp:ListItem Value="Trading">Trading</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Employer:</td>
								<td noWrap align="left"><asp:textbox id="txtEmployerInd" runat="server" CssClass="SmallFont"></asp:textbox></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 26px" noWrap align="right">Country 
									of Origin:</td>
								<td noWrap align="left"><asp:dropdownlist id="ddlCountryOfOriginInd" runat="server" CssClass="SmallFont" AutoPostBack="True">										
										<asp:ListItem Value=" " Selected="True">Select Country</asp:ListItem>
										<asp:ListItem Value="IN" >India</asp:ListItem>
										<asp:ListItem Value="US">United States</asp:ListItem>
										<asp:ListItem Value="AF">Afghanistan</asp:ListItem>
										<asp:ListItem Value="AL">Albania</asp:ListItem>
										<asp:ListItem Value="DZ">Algeria</asp:ListItem>
										<asp:ListItem Value="AS">American Samoa</asp:ListItem>
										<asp:ListItem Value="AD">Andorra</asp:ListItem>
										<asp:ListItem Value="AO">Angola</asp:ListItem>
										<asp:ListItem Value="AI">Anguilla</asp:ListItem>
										<asp:ListItem Value="AQ">Antarctica</asp:ListItem>
										<asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
										<asp:ListItem Value="AR">Argentina</asp:ListItem>
										<asp:ListItem Value="AM">Armenia</asp:ListItem>
										<asp:ListItem Value="AW">Aruba</asp:ListItem>
										<asp:ListItem Value="AU">Australia</asp:ListItem>
										<asp:ListItem Value="AT">Austria</asp:ListItem>
										<asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
										<asp:ListItem Value="BS">Bahamas</asp:ListItem>
										<asp:ListItem Value="BH">Bahrain</asp:ListItem>
										<asp:ListItem Value="BD">Bangladesh</asp:ListItem>
										<asp:ListItem Value="BB">Barbados</asp:ListItem>
										<asp:ListItem Value="BY">Belarus</asp:ListItem>
										<asp:ListItem Value="BE">Belgium</asp:ListItem>
										<asp:ListItem Value="BZ">Belize</asp:ListItem>
										<asp:ListItem Value="BJ">Benin</asp:ListItem>
										<asp:ListItem Value="BM">Bermuda</asp:ListItem>
										<asp:ListItem Value="BT">Bhutan</asp:ListItem>
										<asp:ListItem Value="BO">Bolivia</asp:ListItem>
										<asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
										<asp:ListItem Value="BW">Botswana</asp:ListItem>
										<asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
										<asp:ListItem Value="BR">Brazil</asp:ListItem>
										<asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
										<asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
										<asp:ListItem Value="BG">Bulgaria</asp:ListItem>
										<asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
										<asp:ListItem Value="BI">Burundi</asp:ListItem>
										<asp:ListItem Value="KH">Cambodia</asp:ListItem>
										<asp:ListItem Value="CM">Cameroon</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>
										<asp:ListItem Value="CV">Cape Verde</asp:ListItem>
										<asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
										<asp:ListItem Value="CF">Central African Republic</asp:ListItem>
										<asp:ListItem Value="TD">Chad</asp:ListItem>
										<asp:ListItem Value="CL">Chile</asp:ListItem>
										<asp:ListItem Value="CN">China</asp:ListItem>
										<asp:ListItem Value="CX">Christmas Island</asp:ListItem>
										<asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
										<asp:ListItem Value="CO">Colombia</asp:ListItem>
										<asp:ListItem Value="KM">Comoros</asp:ListItem>
										<asp:ListItem Value="CG">Congo</asp:ListItem>
										<asp:ListItem Value="CK">Cook Islands</asp:ListItem>
										<asp:ListItem Value="CR">Costa Rica</asp:ListItem>
										<asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
										<asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
										<asp:ListItem Value="CU">Cuba</asp:ListItem>
										<asp:ListItem Value="CY">Cyprus</asp:ListItem>
										<asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
										<asp:ListItem Value="DK">Denmark</asp:ListItem>
										<asp:ListItem Value="DJ">Djibouti</asp:ListItem>
										<asp:ListItem Value="DM">Dominica</asp:ListItem>
										<asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
										<asp:ListItem Value="TP">East Timor</asp:ListItem>
										<asp:ListItem Value="EC">Ecuador</asp:ListItem>
										<asp:ListItem Value="EG">Egypt</asp:ListItem>
										<asp:ListItem Value="SV">El Salvador</asp:ListItem>
										<asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
										<asp:ListItem Value="ER">Eritrea</asp:ListItem>
										<asp:ListItem Value="EE">Estonia</asp:ListItem>
										<asp:ListItem Value="ET">Ethiopia</asp:ListItem>
										<asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
										<asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
										<asp:ListItem Value="FJ">Fiji</asp:ListItem>
										<asp:ListItem Value="FI">Finland</asp:ListItem>
										<asp:ListItem Value="FR">France</asp:ListItem>
										<asp:ListItem Value="GF">French Guiana</asp:ListItem>
										<asp:ListItem Value="PF">French Polynesia</asp:ListItem>
										<asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
										<asp:ListItem Value="GA">Gabon</asp:ListItem>
										<asp:ListItem Value="GM">Gambia</asp:ListItem>
										<asp:ListItem Value="GE">Georgia</asp:ListItem>
										<asp:ListItem Value="DE">Germany</asp:ListItem>
										<asp:ListItem Value="GH">Ghana</asp:ListItem>
										<asp:ListItem Value="GI">Gibraltar</asp:ListItem>
										<asp:ListItem Value="GR">Greece</asp:ListItem>
										<asp:ListItem Value="GL">Greenland</asp:ListItem>
										<asp:ListItem Value="GD">Grenada</asp:ListItem>
										<asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
										<asp:ListItem Value="GU">Guam</asp:ListItem>
										<asp:ListItem Value="GT">Guatemala</asp:ListItem>
										<asp:ListItem Value="GN">Guinea</asp:ListItem>
										<asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
										<asp:ListItem Value="GY">Guyana</asp:ListItem>
										<asp:ListItem Value="HT">Haiti</asp:ListItem>
										<asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>
										<asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
										<asp:ListItem Value="HN">Honduras</asp:ListItem>
										<asp:ListItem Value="HK">Hong Kong</asp:ListItem>
										<asp:ListItem Value="HU">Hungary</asp:ListItem>
										<asp:ListItem Value="IS">Iceland</asp:ListItem>
										<asp:ListItem Value="ID">Indonesia</asp:ListItem>
										<asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
										<asp:ListItem Value="IQ">Iraq</asp:ListItem>
										<asp:ListItem Value="IE">Ireland</asp:ListItem>
										<asp:ListItem Value="IL">Israel</asp:ListItem>
										<asp:ListItem Value="IT">Italy</asp:ListItem>
										<asp:ListItem Value="JM">Jamaica</asp:ListItem>
										<asp:ListItem Value="JP">Japan</asp:ListItem>
										<asp:ListItem Value="JO">Jordan</asp:ListItem>
										<asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
										<asp:ListItem Value="KE">Kenya</asp:ListItem>
										<asp:ListItem Value="KI">Kiribati</asp:ListItem>
										<asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>
										<asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
										<asp:ListItem Value="KW">Kuwait</asp:ListItem>
										<asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
										<asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>
										<asp:ListItem Value="LV">Latvia</asp:ListItem>
										<asp:ListItem Value="LB">Lebanon</asp:ListItem>
										<asp:ListItem Value="LS">Lesotho</asp:ListItem>
										<asp:ListItem Value="LR">Liberia</asp:ListItem>
										<asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
										<asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
										<asp:ListItem Value="LT">Lithuania</asp:ListItem>
										<asp:ListItem Value="LU">Luxembourg</asp:ListItem>
										<asp:ListItem Value="MO">Macau</asp:ListItem>
										<asp:ListItem Value="MK">Macedonia</asp:ListItem>
										<asp:ListItem Value="MG">Madagascar</asp:ListItem>
										<asp:ListItem Value="MW">Malawi</asp:ListItem>
										<asp:ListItem Value="MY">Malaysia</asp:ListItem>
										<asp:ListItem Value="MV">Maldives</asp:ListItem>
										<asp:ListItem Value="ML">Mali</asp:ListItem>
										<asp:ListItem Value="MT">Malta</asp:ListItem>
										<asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
										<asp:ListItem Value="MQ">Martinique</asp:ListItem>
										<asp:ListItem Value="MR">Mauritania</asp:ListItem>
										<asp:ListItem Value="MU">Mauritius</asp:ListItem>
										<asp:ListItem Value="YT">Mayotte</asp:ListItem>
										<asp:ListItem Value="MX">Mexico</asp:ListItem>
										<asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
										<asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
										<asp:ListItem Value="MC">Monaco</asp:ListItem>
										<asp:ListItem Value="MN">Mongolia</asp:ListItem>
										<asp:ListItem Value="MS">Montserrat</asp:ListItem>
										<asp:ListItem Value="MA">Morocco</asp:ListItem>
										<asp:ListItem Value="MZ">Mozambique</asp:ListItem>
										<asp:ListItem Value="MM">Myanmar</asp:ListItem>
										<asp:ListItem Value="NA">Namibia</asp:ListItem>
										<asp:ListItem Value="NR">Nauru</asp:ListItem>
										<asp:ListItem Value="NP">Nepal</asp:ListItem>
										<asp:ListItem Value="NL">Netherlands</asp:ListItem>
										<asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
										<asp:ListItem Value="NC">New Caledonia</asp:ListItem>
										<asp:ListItem Value="NZ">New Zealand</asp:ListItem>
										<asp:ListItem Value="NI">Nicaragua</asp:ListItem>
										<asp:ListItem Value="NE">Niger</asp:ListItem>
										<asp:ListItem Value="NG">Nigeria</asp:ListItem>
										<asp:ListItem Value="NU">Niue</asp:ListItem>
										<asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
										<asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
										<asp:ListItem Value="NO">Norway</asp:ListItem>
										<asp:ListItem Value="OM">Oman</asp:ListItem>
										<asp:ListItem Value="PK">Pakistan</asp:ListItem>
										<asp:ListItem Value="PW">Palau</asp:ListItem>
										<asp:ListItem Value="PA">Panama</asp:ListItem>
										<asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
										<asp:ListItem Value="PY">Paraguay</asp:ListItem>
										<asp:ListItem Value="PE">Peru</asp:ListItem>
										<asp:ListItem Value="PH">Philippines</asp:ListItem>
										<asp:ListItem Value="PN">Pitcairn</asp:ListItem>
										<asp:ListItem Value="PL">Poland</asp:ListItem>
										<asp:ListItem Value="PT">Portugal</asp:ListItem>
										<asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
										<asp:ListItem Value="QA">Qatar</asp:ListItem>
										<asp:ListItem Value="RE">Reunion</asp:ListItem>
										<asp:ListItem Value="RO">Romania</asp:ListItem>
										<asp:ListItem Value="RU">Russian Federation</asp:ListItem>
										<asp:ListItem Value="RW">Rwanda</asp:ListItem>
										<asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>
										<asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
										<asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
										<asp:ListItem Value="WS">Samoa</asp:ListItem>
										<asp:ListItem Value="SM">San Marino</asp:ListItem>
										<asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
										<asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
										<asp:ListItem Value="SN">Senegal</asp:ListItem>
										<asp:ListItem Value="SC">Seychelles</asp:ListItem>
										<asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
										<asp:ListItem Value="SG">Singapore</asp:ListItem>
										<asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
										<asp:ListItem Value="SI">Slovenia</asp:ListItem>
										<asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
										<asp:ListItem Value="SO">Somalia</asp:ListItem>
										<asp:ListItem Value="ZA">South Africa</asp:ListItem>
										<asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>
										<asp:ListItem Value="ES">Spain</asp:ListItem>
										<asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
										<asp:ListItem Value="SH">St.  Helena</asp:ListItem>
										<asp:ListItem Value="PM">St.  Pierre And Miquelon</asp:ListItem>
										<asp:ListItem Value="SD">Sudan</asp:ListItem>
										<asp:ListItem Value="SR">Suriname</asp:ListItem>
										<asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
										<asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>
										<asp:ListItem Value="SE">Sweden</asp:ListItem>
										<asp:ListItem Value="CH">Switzerland</asp:ListItem>
										<asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
										<asp:ListItem Value="TW">Taiwan</asp:ListItem>
										<asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
										<asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
										<asp:ListItem Value="TH">Thailand</asp:ListItem>
										<asp:ListItem Value="TG">Togo</asp:ListItem>
										<asp:ListItem Value="TK">Tokelau</asp:ListItem>
										<asp:ListItem Value="TO">Tonga</asp:ListItem>
										<asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
										<asp:ListItem Value="TN">Tunisia</asp:ListItem>
										<asp:ListItem Value="TR">Turkey</asp:ListItem>
										<asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
										<asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
										<asp:ListItem Value="TV">Tuvalu</asp:ListItem>
										<asp:ListItem Value="UG">Uganda</asp:ListItem>
										<asp:ListItem Value="UA">Ukraine</asp:ListItem>
										<asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
										<asp:ListItem Value="GB">United Kingdom</asp:ListItem>
										<asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
										<asp:ListItem Value="UY">Uruguay</asp:ListItem>
										<asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
										<asp:ListItem Value="VU">Vanuatu</asp:ListItem>
										<asp:ListItem Value="VE">Venezuela</asp:ListItem>
										<asp:ListItem Value="VN">Viet Nam</asp:ListItem>
										<asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
										<asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
										<asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
										<asp:ListItem Value="EH">Western Sahara</asp:ListItem>
										<asp:ListItem Value="YE">Yemen</asp:ListItem>
										<asp:ListItem Value="YU">Yugoslavia</asp:ListItem>
										<asp:ListItem Value="ZR">Zaire</asp:ListItem>
										<asp:ListItem Value="ZM">Zambia</asp:ListItem>
										<asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountryInd" runat="server" ControlToValidate="ddlCountryOfOriginInd" Display="Dynamic"
										ErrorMessage="Country of Origin should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">State of Origin:</td>
								<td noWrap align="left"><asp:textbox id="txtStateOfOriginInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:dropdownlist id="ddlStateOfOriginInd" runat="server" CssClass="SmallFont"></asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">Volunteer 
									Flag:</td>
								<td noWrap align="left"><asp:radiobuttonlist id="rbVolunteerInd" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
										<asp:ListItem Value="Yes">Yes</asp:ListItem>
										<asp:ListItem Value="No">No</asp:ListItem>
									</asp:radiobuttonlist><asp:requiredfieldvalidator id="rfvVolInd" runat="server" ControlToValidate="rbVolunteerInd" Display="Dynamic"
										ErrorMessage="Volunteer flag should be selected" InitialValue="" CssClass="SmallFont"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Referred By:</td>
								<td noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlReferredByInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value="">Select Referred By</asp:ListItem>
										<asp:ListItem Value="None">None</asp:ListItem>
										<asp:ListItem Value="Advertisement">Advertisement</asp:ListItem>
										<asp:ListItem Value="Email">Email</asp:ListItem>
										<asp:ListItem Value="Flyer">Flyer</asp:ListItem>
										<asp:ListItem Value="Friend">Friend</asp:ListItem>
										<asp:ListItem Value="Newspaper">Newspaper</asp:ListItem>
										<asp:ListItem Value="Relative">Relative</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">NSF 
									Chapter:</td>
								<td  noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlChapterInd" runat="server" CssClass="SmallFont"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvChapterInd" runat="server" ControlToValidate="ddlChapterInd" Display="Dynamic"
										ErrorMessage="NSF Chapter should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">
                                    Marital Status:</td>
								<td  noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlMaritalStatusInd" runat="server" CssClass="SmallFont">
                                    <asp:ListItem Value=" ">Select</asp:ListItem>
                                    <asp:ListItem Value="Married">Married</asp:ListItem>
                                    <asp:ListItem Value="NeverM">Never Married</asp:ListItem>
                                    <asp:ListItem Value="Divorced">Divorced</asp:ListItem>
                                    <asp:ListItem Value="Re-Married">Re-Married</asp:ListItem>
                                    <asp:ListItem Value="Widowed">Widowed</asp:ListItem>
                                    <asp:ListItem Value="LegallyS">Legally Separated</asp:ListItem>
                                </asp:DropDownList></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">
                                    Sponsor:</td>
								<td  noWrap align="left" colSpan="3"><asp:radiobuttonlist id="rbSponsorInd" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right" style="height: 18px">
                                    Liaison:</td>
								<td  noWrap align="left" colSpan="3" style="height: 18px"><asp:radiobuttonlist id="rbLiaisonInd" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList></td>
							</tr>
						</table>
					</td>
					<td >
						<table id="tblSpouse" width="60%" cellspacing="1" cellpadding="3">
							<tr bgcolor="#FFFFFF">
								<th>
								</th>
								<th noWrap align="left" colSpan="2">
									<h2>
                                        Mother/Spouse</h2>
								</th>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Title:</TD>
								<td noWrap align="left" ><asp:dropdownlist id="ddlTitleSp" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Title</asp:ListItem>
										<asp:ListItem Value="Mr">Mr.</asp:ListItem>
										<asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
										<asp:ListItem Value="Miss">Miss.</asp:ListItem>
										<asp:ListItem Value="Dr">Dr.</asp:ListItem>
										<asp:ListItem Value="Ms">Ms.</asp:ListItem>
										<asp:ListItem Value="Prof">Prof.</asp:ListItem>
                                    <asp:ListItem>Late</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvTitleSp" runat="server" ControlToValidate="ddlTitleSp" Display="Dynamic"
										ErrorMessage="Title Should be Selected"></asp:requiredfieldvalidator></td>
							</TR>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">First Name:</td>
								<td noWrap align="left"><asp:textbox id="txtFirstNameSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstNameSp" runat="server" ControlToValidate="txtFirstNameSp" ErrorMessage="First Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">Last 
									Name:</td>
								<td  noWrap align="left"><asp:textbox id="txtLastNameSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvLastNameSp" runat="server" ControlToValidate="txtLastNameSp" ErrorMessage="Last Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">Address1:</td>
								<td  noWrap align="left"><span><asp:textbox id="txtAddress1Sp" runat="server" CssClass="SmallFont"></asp:textbox><INPUT id="chkAddress" title="Same Address" onclick="PopualteAddress()" type="checkbox" name="Same Address"> <STRONG>Copy Address from Left side</STRONG> <b>
											<asp:requiredfieldvalidator id="rfvAddress1Sp" runat="server" ErrorMessage="Address is required." ControlToValidate="txtAddress1Sp"></asp:requiredfieldvalidator></b></span></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Address2:</td>
								<td noWrap align="left"><asp:textbox id="txtAddress2Sp" runat="server" CssClass="SmallFont"></asp:textbox></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">City:</td>
								<td noWrap align="left"><asp:textbox id="txtCitySp" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvCitySp" runat="server" ControlToValidate="txtCityInd" ErrorMessage="City is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   align="right">State:</TD>
								<td  align="left"><asp:dropdownlist id="ddlStateSp" runat="server" CssClass="SmallFont"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvStateSp" runat="server" ControlToValidate="ddlStateSp" Display="Dynamic"
										ErrorMessage="State should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">ZIP/Postal Code:</td>
								<td noWrap align="left"><asp:textbox id="txtZipSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvZipSp" runat="server" ControlToValidate="txtZipSp" ErrorMessage="Zip is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">Country:</td>
								<td  noWrap align="left"><asp:dropdownlist id="ddlCountrySp" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" " Selected="True">Select Country</asp:ListItem>
										<asp:ListItem Value="US">United States</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountrySp" runat="server" ControlToValidate="ddlCountrySp" Display="Dynamic"
										ErrorMessage="Country is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right" style="height: 15px">Gender:</td>
								<td  noWrap align="left" style="height: 15px"><asp:dropdownlist id="ddlGenderSp" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Gender</asp:ListItem>
										<asp:ListItem Value="Male">Male</asp:ListItem>
										<asp:ListItem Value="Female">Female</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvGenderSp" runat="server" ControlToValidate="ddlGenderSp" Display="Dynamic"
										ErrorMessage="Gender should be Selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 13px" noWrap align="right">Home 
									Phone:</td>
								<td style="HEIGHT: 13px" noWrap align="left"><asp:textbox id="txtHomePhoneSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revHomePhoneSp" runat="server" ControlToValidate="txtHomePhoneSp" Display="Dynamic"
										ErrorMessage="Home Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator><br />
                                    </td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Cell Phone:</td>
								<td noWrap align="left"><asp:textbox id="txtCellPhoneSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revCellPhoneSp" runat="server" ControlToValidate="txtCellPhoneSp" Display="Dynamic"
										ErrorMessage="Cell Phone No should be in xxx-xxx-xxxx format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Work Phone:</td>
								<td noWrap align="left"><asp:textbox id="txtWorkPhoneSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revWorkPhoneSp" runat="server" ControlToValidate="txtWorkPhoneInd" Display="Dynamic"
										ErrorMessage="Work Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Work Fax:</td>
								<td noWrap align="left"><asp:textbox id="txtWorkFaxSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revWorkFaxSp" runat="server" ControlToValidate="txtWorkPhoneInd" Display="Dynamic"
										ErrorMessage="Work Fax should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Primary E-mail:</td>
								<td noWrap align="left"><asp:textbox id="txtPrimaryEmailSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revPrimaryEMailSp" runat="server" ControlToValidate="txtPrimaryEmailSp" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator><br />
                                    <asp:RequiredFieldValidator ID="rfvPrimaryEmailSp" runat="server" ControlToValidate="txtPrimaryEmailInd"
                                        Display="Dynamic" ErrorMessage="Email is required."></asp:RequiredFieldValidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Secondary E-mail:</td>
								<td noWrap align="left"><asp:textbox id="txtSecondaryEmailSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revSecondaryEMailSp" runat="server" ControlToValidate="txtSecondaryEmailSp"
										Display="Dynamic" ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Educational Specialty:</td>
								<td noWrap align="left"><asp:dropdownlist id="ddlEducationSp" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select specialty</asp:ListItem>
										<asp:ListItem Value="Accounting">Accounting</asp:ListItem>
										<asp:ListItem Value="Arts">Arts</asp:ListItem>
										<asp:ListItem Value="Business School">Business School</asp:ListItem>
										<asp:ListItem Value="Dentistry">Dentistry</asp:ListItem>
										<asp:ListItem Value="Economics">Economics</asp:ListItem>
										<asp:ListItem Value="Engineering">Engineering</asp:ListItem>
										<asp:ListItem Value="English">English</asp:ListItem>
										<asp:ListItem Value="Finance">Finance</asp:ListItem>
										<asp:ListItem Value="Geography">Geography</asp:ListItem>
										<asp:ListItem Value="History">History</asp:ListItem>
										<asp:ListItem Value="Humanities">Humanities</asp:ListItem>
										<asp:ListItem Value="International Relations">International Relations</asp:ListItem>
										<asp:ListItem Value="IT">IT</asp:ListItem>
										<asp:ListItem Value="Journalism">Journalism</asp:ListItem>
										<asp:ListItem Value="Law">Law</asp:ListItem>
										<asp:ListItem Value="Math">Math</asp:ListItem>
										<asp:ListItem Value="Medicine">Medicine</asp:ListItem>
										<asp:ListItem Value="Pharmacy">Pharmacy</asp:ListItem>
										<asp:ListItem Value="Public Relations">Public Relations</asp:ListItem>
										<asp:ListItem Value="Science">Science</asp:ListItem>
										<asp:ListItem Value="Statistics">Statistics</asp:ListItem>
										<asp:ListItem Value="Teaching">Teaching</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Career Specialty:</td>
								<td noWrap align="left"><asp:dropdownlist id="ddlCareerSp" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select specialty</asp:ListItem>
										<asp:ListItem Value="Advertising">Advertising</asp:ListItem>
										<asp:ListItem Value="Banking">Banking</asp:ListItem>
										<asp:ListItem Value="Brokerage House">Brokerage House</asp:ListItem>
										<asp:ListItem Value="Dentist">Dentist</asp:ListItem>
										<asp:ListItem Value="Doctor">Doctor</asp:ListItem>
										<asp:ListItem Value="Engineer">Engineer</asp:ListItem>
										<asp:ListItem Value="Finance">Finance</asp:ListItem>
										<asp:ListItem Value="Human Resources">Human Resources</asp:ListItem>
										<asp:ListItem Value="Insurance">Insurance</asp:ListItem>
										<asp:ListItem Value="Investment Banking">Investment Banking</asp:ListItem>
										<asp:ListItem Value="IT">IT</asp:ListItem>
										<asp:ListItem Value="Journalism">Journalism</asp:ListItem>
										<asp:ListItem Value="Lawyer">Lawyer</asp:ListItem>
										<asp:ListItem Value="Library">Library</asp:ListItem>
										<asp:ListItem Value="Marketing">Marketing</asp:ListItem>
										<asp:ListItem Value="Media">Media</asp:ListItem>
										<asp:ListItem Value="Public Service">Public Service</asp:ListItem>
										<asp:ListItem Value="Real Estate">Real Estate</asp:ListItem>
										<asp:ListItem Value="Social Service">Social Service</asp:ListItem>
										<asp:ListItem Value="Teaching">Teaching</asp:ListItem>
										<asp:ListItem Value="Trading">Trading</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Employer:</td>
								<td noWrap align="left"><asp:textbox id="txtEmployerSp" runat="server" CssClass="SmallFont"></asp:textbox></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 8px" noWrap align="right">Country 
									of Origin:</td>
								<td style="HEIGHT: 8px" noWrap align="left"><asp:dropdownlist id="ddlCountryOfOriginSp" runat="server" CssClass="SmallFont" AutoPostBack="True">
										<asp:ListItem Value=" " Selected="True">Select Country</asp:ListItem>
										<asp:ListItem Value="IN" >India</asp:ListItem>
										<asp:ListItem Value="US">United States</asp:ListItem>
										<asp:ListItem Value="AF">Afghanistan</asp:ListItem>
										<asp:ListItem Value="AL">Albania</asp:ListItem>
										<asp:ListItem Value="DZ">Algeria</asp:ListItem>
										<asp:ListItem Value="AS">American Samoa</asp:ListItem>
										<asp:ListItem Value="AD">Andorra</asp:ListItem>
										<asp:ListItem Value="AO">Angola</asp:ListItem>
										<asp:ListItem Value="AI">Anguilla</asp:ListItem>
										<asp:ListItem Value="AQ">Antarctica</asp:ListItem>
										<asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
										<asp:ListItem Value="AR">Argentina</asp:ListItem>
										<asp:ListItem Value="AM">Armenia</asp:ListItem>
										<asp:ListItem Value="AW">Aruba</asp:ListItem>
										<asp:ListItem Value="AU">Australia</asp:ListItem>
										<asp:ListItem Value="AT">Austria</asp:ListItem>
										<asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
										<asp:ListItem Value="BS">Bahamas</asp:ListItem>
										<asp:ListItem Value="BH">Bahrain</asp:ListItem>
										<asp:ListItem Value="BD">Bangladesh</asp:ListItem>
										<asp:ListItem Value="BB">Barbados</asp:ListItem>
										<asp:ListItem Value="BY">Belarus</asp:ListItem>
										<asp:ListItem Value="BE">Belgium</asp:ListItem>
										<asp:ListItem Value="BZ">Belize</asp:ListItem>
										<asp:ListItem Value="BJ">Benin</asp:ListItem>
										<asp:ListItem Value="BM">Bermuda</asp:ListItem>
										<asp:ListItem Value="BT">Bhutan</asp:ListItem>
										<asp:ListItem Value="BO">Bolivia</asp:ListItem>
										<asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
										<asp:ListItem Value="BW">Botswana</asp:ListItem>
										<asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
										<asp:ListItem Value="BR">Brazil</asp:ListItem>
										<asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
										<asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
										<asp:ListItem Value="BG">Bulgaria</asp:ListItem>
										<asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
										<asp:ListItem Value="BI">Burundi</asp:ListItem>
										<asp:ListItem Value="KH">Cambodia</asp:ListItem>
										<asp:ListItem Value="CM">Cameroon</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>
										<asp:ListItem Value="CV">Cape Verde</asp:ListItem>
										<asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
										<asp:ListItem Value="CF">Central African Republic</asp:ListItem>
										<asp:ListItem Value="TD">Chad</asp:ListItem>
										<asp:ListItem Value="CL">Chile</asp:ListItem>
										<asp:ListItem Value="CN">China</asp:ListItem>
										<asp:ListItem Value="CX">Christmas Island</asp:ListItem>
										<asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
										<asp:ListItem Value="CO">Colombia</asp:ListItem>
										<asp:ListItem Value="KM">Comoros</asp:ListItem>
										<asp:ListItem Value="CG">Congo</asp:ListItem>
										<asp:ListItem Value="CK">Cook Islands</asp:ListItem>
										<asp:ListItem Value="CR">Costa Rica</asp:ListItem>
										<asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
										<asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
										<asp:ListItem Value="CU">Cuba</asp:ListItem>
										<asp:ListItem Value="CY">Cyprus</asp:ListItem>
										<asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
										<asp:ListItem Value="DK">Denmark</asp:ListItem>
										<asp:ListItem Value="DJ">Djibouti</asp:ListItem>
										<asp:ListItem Value="DM">Dominica</asp:ListItem>
										<asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
										<asp:ListItem Value="TP">East Timor</asp:ListItem>
										<asp:ListItem Value="EC">Ecuador</asp:ListItem>
										<asp:ListItem Value="EG">Egypt</asp:ListItem>
										<asp:ListItem Value="SV">El Salvador</asp:ListItem>
										<asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
										<asp:ListItem Value="ER">Eritrea</asp:ListItem>
										<asp:ListItem Value="EE">Estonia</asp:ListItem>
										<asp:ListItem Value="ET">Ethiopia</asp:ListItem>
										<asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
										<asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
										<asp:ListItem Value="FJ">Fiji</asp:ListItem>
										<asp:ListItem Value="FI">Finland</asp:ListItem>
										<asp:ListItem Value="FR">France</asp:ListItem>
										<asp:ListItem Value="GF">French Guiana</asp:ListItem>
										<asp:ListItem Value="PF">French Polynesia</asp:ListItem>
										<asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
										<asp:ListItem Value="GA">Gabon</asp:ListItem>
										<asp:ListItem Value="GM">Gambia</asp:ListItem>
										<asp:ListItem Value="GE">Georgia</asp:ListItem>
										<asp:ListItem Value="DE">Germany</asp:ListItem>
										<asp:ListItem Value="GH">Ghana</asp:ListItem>
										<asp:ListItem Value="GI">Gibraltar</asp:ListItem>
										<asp:ListItem Value="GR">Greece</asp:ListItem>
										<asp:ListItem Value="GL">Greenland</asp:ListItem>
										<asp:ListItem Value="GD">Grenada</asp:ListItem>
										<asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
										<asp:ListItem Value="GU">Guam</asp:ListItem>
										<asp:ListItem Value="GT">Guatemala</asp:ListItem>
										<asp:ListItem Value="GN">Guinea</asp:ListItem>
										<asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
										<asp:ListItem Value="GY">Guyana</asp:ListItem>
										<asp:ListItem Value="HT">Haiti</asp:ListItem>
										<asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>
										<asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
										<asp:ListItem Value="HN">Honduras</asp:ListItem>
										<asp:ListItem Value="HK">Hong Kong</asp:ListItem>
										<asp:ListItem Value="HU">Hungary</asp:ListItem>
										<asp:ListItem Value="IS">Iceland</asp:ListItem>
										<asp:ListItem Value="ID">Indonesia</asp:ListItem>
										<asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
										<asp:ListItem Value="IQ">Iraq</asp:ListItem>
										<asp:ListItem Value="IE">Ireland</asp:ListItem>
										<asp:ListItem Value="IL">Israel</asp:ListItem>
										<asp:ListItem Value="IT">Italy</asp:ListItem>
										<asp:ListItem Value="JM">Jamaica</asp:ListItem>
										<asp:ListItem Value="JP">Japan</asp:ListItem>
										<asp:ListItem Value="JO">Jordan</asp:ListItem>
										<asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
										<asp:ListItem Value="KE">Kenya</asp:ListItem>
										<asp:ListItem Value="KI">Kiribati</asp:ListItem>
										<asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>
										<asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
										<asp:ListItem Value="KW">Kuwait</asp:ListItem>
										<asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
										<asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>
										<asp:ListItem Value="LV">Latvia</asp:ListItem>
										<asp:ListItem Value="LB">Lebanon</asp:ListItem>
										<asp:ListItem Value="LS">Lesotho</asp:ListItem>
										<asp:ListItem Value="LR">Liberia</asp:ListItem>
										<asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
										<asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
										<asp:ListItem Value="LT">Lithuania</asp:ListItem>
										<asp:ListItem Value="LU">Luxembourg</asp:ListItem>
										<asp:ListItem Value="MO">Macau</asp:ListItem>
										<asp:ListItem Value="MK">Macedonia</asp:ListItem>
										<asp:ListItem Value="MG">Madagascar</asp:ListItem>
										<asp:ListItem Value="MW">Malawi</asp:ListItem>
										<asp:ListItem Value="MY">Malaysia</asp:ListItem>
										<asp:ListItem Value="MV">Maldives</asp:ListItem>
										<asp:ListItem Value="ML">Mali</asp:ListItem>
										<asp:ListItem Value="MT">Malta</asp:ListItem>
										<asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
										<asp:ListItem Value="MQ">Martinique</asp:ListItem>
										<asp:ListItem Value="MR">Mauritania</asp:ListItem>
										<asp:ListItem Value="MU">Mauritius</asp:ListItem>
										<asp:ListItem Value="YT">Mayotte</asp:ListItem>
										<asp:ListItem Value="MX">Mexico</asp:ListItem>
										<asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
										<asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
										<asp:ListItem Value="MC">Monaco</asp:ListItem>
										<asp:ListItem Value="MN">Mongolia</asp:ListItem>
										<asp:ListItem Value="MS">Montserrat</asp:ListItem>
										<asp:ListItem Value="MA">Morocco</asp:ListItem>
										<asp:ListItem Value="MZ">Mozambique</asp:ListItem>
										<asp:ListItem Value="MM">Myanmar</asp:ListItem>
										<asp:ListItem Value="NA">Namibia</asp:ListItem>
										<asp:ListItem Value="NR">Nauru</asp:ListItem>
										<asp:ListItem Value="NP">Nepal</asp:ListItem>
										<asp:ListItem Value="NL">Netherlands</asp:ListItem>
										<asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
										<asp:ListItem Value="NC">New Caledonia</asp:ListItem>
										<asp:ListItem Value="NZ">New Zealand</asp:ListItem>
										<asp:ListItem Value="NI">Nicaragua</asp:ListItem>
										<asp:ListItem Value="NE">Niger</asp:ListItem>
										<asp:ListItem Value="NG">Nigeria</asp:ListItem>
										<asp:ListItem Value="NU">Niue</asp:ListItem>
										<asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
										<asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
										<asp:ListItem Value="NO">Norway</asp:ListItem>
										<asp:ListItem Value="OM">Oman</asp:ListItem>
										<asp:ListItem Value="PK">Pakistan</asp:ListItem>
										<asp:ListItem Value="PW">Palau</asp:ListItem>
										<asp:ListItem Value="PA">Panama</asp:ListItem>
										<asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
										<asp:ListItem Value="PY">Paraguay</asp:ListItem>
										<asp:ListItem Value="PE">Peru</asp:ListItem>
										<asp:ListItem Value="PH">Philippines</asp:ListItem>
										<asp:ListItem Value="PN">Pitcairn</asp:ListItem>
										<asp:ListItem Value="PL">Poland</asp:ListItem>
										<asp:ListItem Value="PT">Portugal</asp:ListItem>
										<asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
										<asp:ListItem Value="QA">Qatar</asp:ListItem>
										<asp:ListItem Value="RE">Reunion</asp:ListItem>
										<asp:ListItem Value="RO">Romania</asp:ListItem>
										<asp:ListItem Value="RU">Russian Federation</asp:ListItem>
										<asp:ListItem Value="RW">Rwanda</asp:ListItem>
										<asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>
										<asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
										<asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
										<asp:ListItem Value="WS">Samoa</asp:ListItem>
										<asp:ListItem Value="SM">San Marino</asp:ListItem>
										<asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
										<asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
										<asp:ListItem Value="SN">Senegal</asp:ListItem>
										<asp:ListItem Value="SC">Seychelles</asp:ListItem>
										<asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
										<asp:ListItem Value="SG">Singapore</asp:ListItem>
										<asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
										<asp:ListItem Value="SI">Slovenia</asp:ListItem>
										<asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
										<asp:ListItem Value="SO">Somalia</asp:ListItem>
										<asp:ListItem Value="ZA">South Africa</asp:ListItem>
										<asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>
										<asp:ListItem Value="ES">Spain</asp:ListItem>
										<asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
										<asp:ListItem Value="SH">St.  Helena</asp:ListItem>
										<asp:ListItem Value="PM">St.  Pierre And Miquelon</asp:ListItem>
										<asp:ListItem Value="SD">Sudan</asp:ListItem>
										<asp:ListItem Value="SR">Suriname</asp:ListItem>
										<asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
										<asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>
										<asp:ListItem Value="SE">Sweden</asp:ListItem>
										<asp:ListItem Value="CH">Switzerland</asp:ListItem>
										<asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
										<asp:ListItem Value="TW">Taiwan</asp:ListItem>
										<asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
										<asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
										<asp:ListItem Value="TH">Thailand</asp:ListItem>
										<asp:ListItem Value="TG">Togo</asp:ListItem>
										<asp:ListItem Value="TK">Tokelau</asp:ListItem>
										<asp:ListItem Value="TO">Tonga</asp:ListItem>
										<asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
										<asp:ListItem Value="TN">Tunisia</asp:ListItem>
										<asp:ListItem Value="TR">Turkey</asp:ListItem>
										<asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
										<asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
										<asp:ListItem Value="TV">Tuvalu</asp:ListItem>
										<asp:ListItem Value="UG">Uganda</asp:ListItem>
										<asp:ListItem Value="UA">Ukraine</asp:ListItem>
										<asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
										<asp:ListItem Value="GB">United Kingdom</asp:ListItem>
										<asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
										<asp:ListItem Value="UY">Uruguay</asp:ListItem>
										<asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
										<asp:ListItem Value="VU">Vanuatu</asp:ListItem>
										<asp:ListItem Value="VE">Venezuela</asp:ListItem>
										<asp:ListItem Value="VN">Viet Nam</asp:ListItem>
										<asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
										<asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
										<asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
										<asp:ListItem Value="EH">Western Sahara</asp:ListItem>
										<asp:ListItem Value="YE">Yemen</asp:ListItem>
										<asp:ListItem Value="YU">Yugoslavia</asp:ListItem>
										<asp:ListItem Value="ZR">Zaire</asp:ListItem>
										<asp:ListItem Value="ZM">Zambia</asp:ListItem>
										<asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountryOfOriginSp" runat="server" ControlToValidate="ddlCountryOfOriginSp"
										Display="Dynamic" ErrorMessage="Country of Origin should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 21px" noWrap align="right">State 
									of Origin:</td>
								<td style="HEIGHT: 21px" noWrap align="left"><asp:textbox id="txtStateOfOriginSp" runat="server" CssClass="SmallFont"></asp:textbox><asp:dropdownlist id="ddlStateOfOriginSp" runat="server" CssClass="SmallFont"></asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Volunteer Flag:</td>
								<td noWrap align="left"><asp:radiobuttonlist id="rbVolunteerSp" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
										<asp:ListItem Value="Yes">Yes</asp:ListItem>
										<asp:ListItem Value="No">No</asp:ListItem>
									</asp:radiobuttonlist><asp:requiredfieldvalidator id="rfvVolSp" runat="server" ControlToValidate="rbVolunteerSp" Display="Dynamic"
										ErrorMessage="Volunteer flag should be selected" InitialValue=""></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">
                                    Marital Status:</td>
								<td  noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlMaritalStatusSp" runat="server" CssClass="SmallFont">
                                    <asp:ListItem Value=" ">Select</asp:ListItem>
                                    <asp:ListItem Value="Married">Married</asp:ListItem>
                                    <asp:ListItem Value="NeverM">Never Married</asp:ListItem>
                                    <asp:ListItem Value="Divorced">Divorced</asp:ListItem>
                                    <asp:ListItem Value="Re-Married">Re-Married</asp:ListItem>
                                    <asp:ListItem Value="Widowed">Widowed</asp:ListItem>
                                    <asp:ListItem Value="LegallyS">Legally Separated</asp:ListItem>
                                </asp:DropDownList></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">
                                    Sponsor:</td>
								<td  noWrap align="left" colSpan="3"><asp:radiobuttonlist id="rbSponsorSp" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">
                                    Liaison:</td>
								<td  noWrap align="left" colSpan="3"><asp:radiobuttonlist id="rbLiaisonSp" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Primary Contact
								</td>
								<td noWrap align="left"><asp:checkbox id="ckbPrimaryContact" runat="server" CssClass="SmallFont" ></asp:checkbox>&nbsp;(Select 
									if spouse is the contact.)</td>
							</tr>
						</table>
                        <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td class="ItemCenter"  colSpan="2"><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Submit"  ></asp:button></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td class="Heading"  colSpan="2">
					    <asp:Label runat="server" ID="lblDuplicateMsg" ></asp:Label>
					</td>
				</tr>
			</table>
</div>
</asp:Content>
 

 
 
 