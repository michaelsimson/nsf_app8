Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI
Partial Class Reports_ccreports
    Inherits System.Web.UI.Page
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                    loadyear()
                    loadEvent()
                    Loadrevenuetype()
                    'loadgrid(dgReports, 2009)
                Else
                    Response.Redirect("..\VolunteerFunctions.aspx")
                End If
            Else
                Response.Redirect("..\Login.aspx")
            End If
        End If
    End Sub

    Private Sub loadrevenuetype()
        lstRevenueType.Items.Insert(0, "Registration Fees")
        lstRevenueType.Items.Insert(1, "Donations")
        lstRevenueType.Items.Insert(2, "Meals")
    End Sub
    Private Sub loadEvent()
        lstEvent.Items.Clear()
        Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from Event where EventID in (1,2,3)")
        i = 1
        While read1.Read()
            Dim litem As New ListItem
            litem.Text = read1("Name")
            litem.Value = read1("EventID")
            lstEvent.Items.Add(litem)
            i = i + 1
        End While
        lstEvent.Items.Insert(0, "All Events")
        lstEvent.SelectedIndex = 0
        read1.Close()
    End Sub


    Private Sub loadyear()
        ddlYear1.Items.Clear()
        ddlYear2.Items.Clear()
        Ddlyear3.Items.Clear()
        '** fridine Silvaa
        'Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select  Max(year(PP_Date)) as Maxyear,Min(year(PP_Date)) as MinYear from ChargeRec")
        Dim MaxYear, MinYear As Integer
        'While read1.Read()
        'MaxYear = read1("MaxYear")
        'MinYear = read1("MinYear")
        'End While
        'read1.Close()
        MaxYear = Now.Year
        MinYear = Now.Year - 5
        Dim year As Integer = MinYear
        i = 0
        While year <= MaxYear
            ddlYear1.Items.Insert(i, year)
            ddlYear2.Items.Insert(i, year)
            Ddlyear3.Items.Insert(i, year)
            i = i + 1
            year = year + 1
        End While
    End Sub
   
    Protected Sub loadgrid(ByVal dg As DataGrid)
        ' dgReports.DataSource = Nothing
        'dgReports.DataBind()
        Try
            Dim Eventstr As String = ""
            Dim between As String = " between '" & DdlMonth1.SelectedValue & "/1/" & ddlYear1.SelectedValue & "' and '" & ddlMonth2.SelectedValue & "/31/" & ddlYear2.SelectedValue & "' "
            If lstEvent.Items(0).Selected = True Then
                Eventstr = ""
            Else
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If

            Dim dsCCTrans As New DataSet
            Dim tblCCTrans() As String = {"ChargeRec"}
            Dim StrSQl As String
            Dim RegFee As Boolean = False, Donation As Boolean = False, Meals As Boolean = False
            If lstRevenueType.Items(0).Selected = True Then
                RegFee = True
            End If
            If lstRevenueType.Items(1).Selected = True Then
                Donation = True
            End If
            If lstRevenueType.Items(2).Selected = True Then
                Meals = True
            End If
            StrSQl = "Select t2.ChapterID,t2.State,t2.ChapterCode,Sum(t1.Jan) as Jan,Sum(t1.Feb) as Feb,Sum(t1.Mar) as Mar,Sum(t1.Apr) as Apr,Sum(t1.May) as May,Sum(t1.Jun) as Jun,Sum(t1.July) as July,Sum(t1.Aug) as Aug,Sum(t1.Sept) as Sept,Sum(t1.Oct) as Oct,Sum(t1.Nov) as Nov,Sum(t1.Dec) as Dec,"
            StrSQl = StrSQl & "Sum(t1.Jan)+Sum(t1.Feb)+Sum(t1.Mar)+Sum(t1.Apr)+Sum(t1.May)+Sum(t1.Jun)+Sum(t1.July)+Sum(t1.Aug)+Sum(t1.Sept)+Sum(t1.Oct) +Sum(t1.Nov)+Sum(t1.Dec) as Total from("
            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 1 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(AMOUNT) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 2 Group by  ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(AMOUNT) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 3 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(AMOUNT) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 4 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(AMOUNT) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 5 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(AMOUNT) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 6 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(AMOUNT) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 7 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(AMOUNT) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 8 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(AMOUNT) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 9 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(AMOUNT) as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 10 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(AMOUNT) as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 11 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(AMOUNT) as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 12 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 1  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, Sum(Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 2  Group by  month(PaymentDate)) UNION "
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 3  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 4  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 5  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 6  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 7  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 8  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 9  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(Amount) as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 10 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(Amount) as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 11 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(Amount) as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 12 Group by  month(PaymentDate))"
            ElseIf RegFee = True And Donation = True And Meals = False Then
                'for regfee & donation 
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 1 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(AMOUNT) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 2 Group by  ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(AMOUNT) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 3 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(AMOUNT) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 4 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(AMOUNT) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 5 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(AMOUNT) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 6 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(AMOUNT) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 7 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(AMOUNT) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 8 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(AMOUNT) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 9 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(AMOUNT) as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 10 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(AMOUNT) as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 11 Group by  ChapterID) UNION"
                StrSQl = StrSQl & " select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(AMOUNT) as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 12 Group by  ChapterID)"
            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 1  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, Sum(Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 2  Group by  month(PaymentDate)) UNION "
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 3  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 4  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 5  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 6  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 7  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 8  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 9  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(Amount) as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 10 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(Amount) as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 11 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(Amount) as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 12 Group by  month(PaymentDate))"
            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge
                StrSQl = StrSQl & "(select ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 1 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(AMOUNT) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 2 Group by  ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(AMOUNT) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 3 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(AMOUNT) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 4 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(AMOUNT) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 5 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(AMOUNT) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 6 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(AMOUNT) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 7 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(AMOUNT) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 8 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(AMOUNT) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 9 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(AMOUNT) as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 10 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(AMOUNT) as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 11 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(AMOUNT) as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 12 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 1  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, Sum(Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 2  Group by  month(PaymentDate)) UNION "
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 3  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 4  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 5  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 6  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 7  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 8  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 9  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(Amount) as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 10 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(Amount) as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 11 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(Amount) as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 12 Group by  month(PaymentDate))"
            ElseIf RegFee = True And Meals = False And Donation = False Then
                'for Reg fee
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from contestant   where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Sum(fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 1 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 2 Group by ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 3 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 4 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 5 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 6 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 7 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 8 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 9 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(fee) as Oct, 0 as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 10 Group by ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(fee) as Nov, 0 as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 11 Group by ChapterID) UNION"
                StrSQl = StrSQl & " select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(fee) as Dec   from Registration where paymentdate " & between & " " & Eventstr & " and  month(PaymentDate) = 12 Group by ChapterID)"
            ElseIf RegFee = False And Meals = True And Donation = False Then
                'MealCharge
                StrSQl = StrSQl & "(select 1 as ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 1  Group by Year(PaymentDate), month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, Sum(Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 2  Group by  month(PaymentDate)) UNION "
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 3  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 4  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 5  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 6  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 7  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 8  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 9  Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(Amount) as Oct, 0 as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 10 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(Amount) as Nov, 0 as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 11 Group by  month(PaymentDate)) UNION"
                StrSQl = StrSQl & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(Amount) as Dec   from MealCharge where paymentdate " & between & " and  month(PaymentDate) = 12 Group by  month(PaymentDate))"
            ElseIf RegFee = False And Meals = False And Donation = True Then
                'donation
                StrSQl = StrSQl & "(select ChapterID,Sum(AMOUNT) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 1 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, Sum(AMOUNT) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 2 Group by  ChapterID) UNION "
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, Sum(AMOUNT) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 3 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(AMOUNT) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 4 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(AMOUNT) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 5 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(AMOUNT) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 6 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(AMOUNT) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 7 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(AMOUNT) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 8 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(AMOUNT) as Sept, 0 as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 9 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(AMOUNT) as Oct, 0 as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 10 Group by  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(AMOUNT) as Nov, 0 as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 11 Group by  ChapterID) UNION"
                StrSQl = StrSQl & " select ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(AMOUNT) as Dec   from Donationsinfo where DonationDate " & between & "   and  month(DonationDate) = 12 Group by  ChapterID)"
            Else
                lblErr.Text = "Please Select Revenue Type"
                Exit Sub
            End If
            StrSQl = StrSQl & " t1,Chapter t2 where t1.Chapterid=t2.Chapterid Group by t2.ChapterID,t2.State,t2.ChapterCode order by 2 ,3"
            'lblErr.Text = StrSQl.ToString()
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQl, dsCCTrans, tblCCTrans)
            Dim dr As DataRow
            dr = dsCCTrans.Tables("ChargeRec").NewRow
            dr(0) = 0
            dr(1) = ""
            dr(2) = "Total  "
            Dim j As Integer
            For i = 3 To dsCCTrans.Tables("ChargeRec").Columns.Count - 1
                dr(i) = 0.0
                For j = 0 To dsCCTrans.Tables("ChargeRec").Rows.Count - 1
                    dr(i) = dr(i) + dsCCTrans.Tables("ChargeRec").Rows(j)(i)
                Next
            Next
            dsCCTrans.Tables("ChargeRec").Rows.Add(dr)
            dg.CurrentPageIndex = 0
            dg.DataSource = dsCCTrans
            dg.DataBind()
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub
    Protected Sub dgReports_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        If DdlFreq.SelectedValue = 1 Then
            'Monthly
            loadgrid(dgReports)
        Else
            'Annual
        End If
        dgReports.CurrentPageIndex = e.NewPageIndex
        dgReports.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlYear1.SelectedValue = ddlYear2.SelectedValue Then
            If DdlMonth1.SelectedValue > ddlMonth2.SelectedValue Then
                lblErr.Text = "Please select month greater than the beginning month"
                Exit Sub
            End If
        ElseIf ddlYear1.SelectedValue > ddlYear2.SelectedValue Then
            lblErr.Text = "Please select year greater than the beginning year"
            Exit Sub
        End If


        If DdlFreq.SelectedValue = 1 Then
            'Monthly
            loadgrid(dgReports)
        Else
            'Annual
        End If
        'For i = 0 To ddlCY.Items.Count - 1
        '    If ddlCY.Items(i).Selected = True Then
        '        If selectedyear = "" Then
        '            selectedyear = ddlCY.Items(i).Text
        '        Else
        '            selectedyear = selectedyear & "," & ddlCY.Items(i).Text
        '        End If
        '    End If
        'Next
    End Sub

    Protected Sub DdlFreq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DdlFreq.SelectedValue = 1 Then
            trMonthly.Visible = True
            trAnnual.Visible = False
        Else
            trMonthly.Visible = False
            trAnnual.Visible = True
        End If
    End Sub

    Protected Sub loadAnnualgrid(ByVal dg As DataGrid)
        ' dgReports.DataSource = Nothing
        'dgReports.DataBind()
        Try
            Dim Eventstr As String = ""
            Dim between As String = ""
            Dim betweenDon As String = ""
            For i = 0 To Ddlyear3.Items.Count - 1
                If Ddlyear3.Items(i).Selected = True Then
                    If Not between = "" Then
                        between = between & " OR PaymentDate between '5/1/" & Ddlyear3.SelectedValue & "' and '4/30/" & Ddlyear3.SelectedValue + 1 & "' "
                        betweenDon = betweenDon & " OR DonationDate between '5/1/" & Ddlyear3.SelectedValue & "' and '4/30/" & Ddlyear3.SelectedValue + 1 & "' "
                    Else
                        between = "PaymentDate between '5/1/" & Ddlyear3.SelectedValue & "' and '4/30/" & Ddlyear3.SelectedValue + 1 & "' "
                        betweenDon = "DonationDate between '5/1/" & Ddlyear3.SelectedValue & "' and '4/30/" & Ddlyear3.SelectedValue + 1 & "' "
                    End If
                End If
            Next
            If lstEvent.Items(0).Selected = True Then
                Eventstr = ""
            Else
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If

            Dim dsCCTrans As New DataSet
            Dim tblCCTrans() As String = {"ChargeRec"}
            Dim StrSQl As String
            Dim RegFee As Boolean = False, Donation As Boolean = False, Meals As Boolean = False
            If lstRevenueType.Items(0).Selected = True Then
                RegFee = True
            End If
            If lstRevenueType.Items(1).Selected = True Then
                Donation = True
            End If
            If lstRevenueType.Items(2).Selected = True Then
                Meals = True
            End If
            StrSQl = "Select t1.Year,t2.ChapterID,t2.State,t2.ChapterCode,Sum(t1.fee) as Total from("
            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQl = StrSQl & "(select ChapterID,Case when Month(PaymentDate)>4 then Year(PaymentDate) Else Year(PaymentDate)-1 End as year,Sum(fee) as fee  from contestant   where  " & between & " " & Eventstr & " Group by Year(PaymentDate),  ChapterID, Month(PaymentDate)) UNION"
                StrSQl = StrSQl & "(select ChapterID,Case when Month(PaymentDate)>4 then Year(PaymentDate) Else Year(PaymentDate)-1 End as year,Sum(fee) as fee  from Registration where  " & between & " " & Eventstr & " Group by Year(PaymentDate),  ChapterID) UNION"
                StrSQl = StrSQl & "(select ChapterID,Case when Month(DonationDate)>4 then Year(DonationDate) Else Year(DonationDate)-1 End as year,Sum(AMOUNT) as fee   from Donationsinfo where " & betweenDon & " Group by  ChapterID,year(DonationDate),Month(DonationDate)) UNION"
                StrSQl = StrSQl & "select 1 as ChapterID,Case when Month(PaymentDate)>4 then Year(PaymentDate) Else Year(PaymentDate)-1 End as year,Sum(AMOUNT) as fee from MealCharge where " & between & " Group by  month(PaymentDate),Year(PaymentDate)) UNION"
            ElseIf RegFee = True And Donation = True And Meals = False Then

            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge

            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge

            ElseIf RegFee = True And Meals = False And Donation = False Then
                'for Reg fee

            ElseIf RegFee = False And Meals = True And Donation = False Then
                'MealCharge

            ElseIf RegFee = False And Meals = False And Donation = True Then
                'donation

            Else
                lblErr.Text = "Please Select Revenue Type"
                Exit Sub
            End If
            StrSQl = StrSQl & " t1,Chapter t2 where t1.Chapterid=t2.Chapterid Group by t1.year,t2.ChapterID,t2.State,t2.ChapterCode order by 1 desc,3 ,4"
            lblErr.Text = StrSQl.ToString()
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQl, dsCCTrans, tblCCTrans)
            Dim dr As DataRow
            dr = dsCCTrans.Tables("ChargeRec").NewRow
            dr(0) = 0
            dr(1) = 0
            dr(2) = ""
            dr(3) = "Total  "
            Dim j As Integer
            For i = 4 To dsCCTrans.Tables("ChargeRec").Columns.Count - 1
                dr(i) = 0.0
                For j = 0 To dsCCTrans.Tables("ChargeRec").Rows.Count - 1
                    dr(i) = dr(i) + dsCCTrans.Tables("ChargeRec").Rows(j)(i)
                Next
            Next
            dsCCTrans.Tables("ChargeRec").Rows.Add(dr)
            dg.CurrentPageIndex = 0
            dg.DataSource = dsCCTrans
            dg.DataBind()
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CreditCardReports.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        If DdlFreq.SelectedValue = 1 Then
            'Monthly
            loadgrid(dgexport)
        Else
            'Annual
        End If

        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
