<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ManageWebFiles.aspx.vb" Inherits="ManageWebFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <br />
    <br />
    <div><u><b>NSF Web Page Management</b></u></div>
    <br />
    <br />
    <div>
        <table width="100%">
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="RootFolderList" runat="server" AutoPostBack="true">
                        <asp:ListItem Selected="True" Text="Select your working folder" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblError" runat="server" ForeColor="red" Font-Size="Larger" EnableViewState="false" ></asp:Label>
                </td>
            </tr>
            <tr id="trTreeView" runat="server" visible="false">
                <td style="width:40%" valign="top">
                    <asp:TreeView ID="tvFolders" runat="server" ShowLines="True">
                        <SelectedNodeStyle Font-Bold="True" />
                        <NodeStyle ImageUrl="~/Images/Folder.jpg" />
                    </asp:TreeView>
                    
                </td>
                <td style="width:auto;text-align:left;" valign="top">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ForeColor="red" Font-Size="Larger" EnableViewState="false" ></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label1" runat="server" Text="Selected Folder or File:" ForeColor="green" Font-Bold="true" Font-Size="Larger"></asp:Label>
                    <asp:Label ID="lblFileMessage" runat="server" ForeColor="green" Font-Bold="true" Font-Size="Larger"></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="Label4" runat="server" Text="You can perform the following actions on the selected file or folder:" Font-Bold="true" Font-Size="Larger"></asp:Label>
                    <br />
                    <br />
                    <asp:Button ID="btnDownloadFile" runat="server" Text="Download File" />
                    <asp:Label ID="lblDownloadInst" runat="server" Text="(Click this button to save the file to your hard disk.)"></asp:Label>
                    <br />
                    <br />
                    <asp:Button ID="btnDeleteFile" runat="server" Text="Delete" />
                    <asp:Label ID="lblDeleteInst" runat="server" ></asp:Label>
                    <br />
                    <br />
                    <asp:FileUpload ID="FileToUpload" Width="500" runat="server" />
                    <asp:Button ID="btnUploadFile" runat="server" Text="Upload File" />
                    <br />
                    
                    <asp:Label ID="lblUploadInst" runat="server" Text="(Click Browse button to select a file to upload to the server and then click on Upload File button.)"></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="lblFolderName" runat="server" Text="Folder Name: "></asp:Label>
                    <asp:TextBox ID="txtNewFolderName" runat="server"></asp:TextBox>
                    <asp:Button ID="btnCreateNewFolder" runat="server" Text="Create New Folder" />
                    
                </td>
            </tr>
        </table>
    </div>

</asp:Content>


 
 
 