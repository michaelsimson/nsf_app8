﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using System.Collections;
using VRegistration;

public partial class GuestAttendance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblErrMsg.Text = "";
            if (!IsPostBack)
            {
                PopulateYear(ddYear);
                ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                loadPhase();
                fillCoach();


                fillYearFiletr();
                loadProductGroupFilter();
                loadProductFilter();

                loadLevelFilter();
                loadCoachFilter();

                fillGuestAttendeeGrid("false");

                // TestCreateTrainingSession();
            }
        }
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {

            ArrayList list = new ArrayList();

            for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 2; i--)
            {
                list.Add(new ListItem((i.ToString() + "-" + (i + 1).ToString().Substring(2, 2)), i.ToString()));

            }

            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            //ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
            ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        }
        catch (Exception ex) { }

    }
    public void fillCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.Semester='" + ddlSemester.SelectedValue + "' order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Accepted='Y' and V.Semester='" + ddlSemester.SelectedValue + "' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDlCoach.DataSource = ds;
                DDlCoach.DataTextField = "Name";
                DDlCoach.DataValueField = "AutoMemberID";
                DDlCoach.DataBind();
                DDlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //DDlCoach.Items.Insert(1, new ListItem("All", "All"));
                }
                else if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlCoach.SelectedValue = Session["LoginId"].ToString();
                }

            }
        }
        catch
        {
        }

    }

    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where  EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + DDlCoach.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where  ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlSemester.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                DDlProduct.Enabled = true;
                DDlProduct.DataSource = ds;
                DDlProduct.DataTextField = "ProductCode";
                DDlProduct.DataValueField = "ProductID";
                DDlProduct.DataBind();
                // DDlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = false;



                }
                else
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = true;
                }
                loadLevel();
                LoadSessionNo();
                fillGuestCoach();
                fillGuestStudent();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void DDlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
        fillProduct();
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + DDlCoach.SelectedValue + "' and V.ProductID=" + DDlProduct.SelectedValue + " and V.Semester='" + ddlSemester.SelectedValue + "'";


        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.ProductID=" + DDlProduct.SelectedValue + " and V.Semester='" + ddlSemester.SelectedValue + "'";

        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {


            DDlSessionNo.DataTextField = "SessionNo";
            DDlSessionNo.DataValueField = "SessionNo";
            DDlSessionNo.DataSource = ds;
            DDlSessionNo.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                DDlSessionNo.Enabled = true;
            }
            else
            {
                DDlSessionNo.Enabled = false;
                LoadDayAndTime();
            }
        }

    }
    protected void DDlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel();
        LoadSessionNo();
        fillGuestCoach();
        fillGuestStudent();
    }

    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + DDlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }
        else if (Session["RoleID"].ToString() == "88")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }


        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            //ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                // ddlProductGroup.SelectedIndex = 0;
                ddlProductGroup.Enabled = true;

            }
            else if (ds.Tables[0].Rows.Count == 1)
            {
                //ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
            }
            fillProduct();
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + DDlCoach.SelectedValue + "' and V.Semester='" + ddlSemester.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (DDlProduct.SelectedValue != "0" && DDlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + DDlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + ddlSemester.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (DDlProduct.SelectedValue != "0" && DDlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + DDlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
                LoadSessionNo();
            }
        }
        //if (ddlProductGroup.SelectedValue == "41")
        //{
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlLevel.Items.Insert(2, new ListItem("Senior", "Senior"));
        //}
        //else if (ddlProductGroup.SelectedValue == "42")
        //{
        //    //ddlObject.Enabled = False
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlLevel.Items.Insert(3, new ListItem("Senior", "Senior"));
        //}
        //else if (ddlProductGroup.SelectedValue == "33")
        //{
        //    //ddlObject.Enabled = False
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("One level only", "One level only"));

        //}

        //else
        //{
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Beginner", "Beginner"));
        //    ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlLevel.Items.Insert(3, new ListItem("Advanced", "Advanced"));
        //}
    }

    public void fillGuestCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.Semester='" + ddlSemester.SelectedValue + "' and V.MemberID <>" + DDlCoach.SelectedValue + " order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Accepted='Y' and V.Semester='" + ddlSemester.SelectedValue + "' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDlGuestAttendee.DataSource = ds;
                DDlGuestAttendee.DataTextField = "Name";
                DDlGuestAttendee.DataValueField = "AutoMemberID";
                DDlGuestAttendee.DataBind();
                DDlGuestAttendee.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //DDlCoach.Items.Insert(1, new ListItem("All", "All"));
                }
                else if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlGuestAttendee.SelectedIndex = 1;
                }

            }
        }
        catch
        {
        }

    }


    public void fillGuestStudent()
    {
        string cmdText = "";
        cmdText = " Select Case when CR.AdultId is null then CR.ChildNumber else CR.AdultId end as ChildNumber, case when CR.AdultId is null then CH.First_name else IP.FirstName end as First_Name, Case when CR.AdultId is null then CH.Last_name else IP.LastName end as Last_Name, Case when CR.AdultId is null then CH.First_name +' ' +CH.Last_name + ' - '+ 'Child' else IP.FirstName + ' ' + Ip.LastName + ' - '+ 'Adult' end as ChildName from CoachReg CR left join Child Ch on (Ch.ChildNumber=CR.ChildNumber) left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) where CR.EventYear=" + ddYear.SelectedValue + " and CR.Approved='Y' and CR.ProductGroupId=" + ddlProductGroup.SelectedValue + " and CR.ProductId=" + DDlProduct.SelectedValue + " and CR.Level='" + ddlLevel.SelectedValue + "' and CR.Semester='" + ddlSemester.SelectedValue + "' order by CH.First_name, CH.Last_name ASC";
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDLGuestStudent.DataSource = ds;
                DDLGuestStudent.DataTextField = "ChildName";
                DDLGuestStudent.DataValueField = "ChildNumber";
                DDLGuestStudent.DataBind();
                DDLGuestStudent.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //DDlCoach.Items.Insert(1, new ListItem("All", "All"));
                }
                else if (ds.Tables[0].Rows.Count == 1)
                {
                    DDLGuestStudent.SelectedIndex = 1;
                }

            }
        }
        catch
        {
        }

    }


    public void RegisterGuestAttendee()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term' and CS.Semester=CD.Semester) inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + DDlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + DDlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null and CS.Semester='" + ddlSemester.SelectedValue + "'";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            string SessionKey = string.Empty;
            string userID = string.Empty;
            string Pwd = string.Empty;
            string startTime = string.Empty;
            string Date = string.Empty;
            string CoachName = string.Empty;
            string coachEmail = string.Empty;
            string City = string.Empty;
            string Country = string.Empty;

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SessionKey = ds.Tables[0].Rows[0]["MeetingKey"].ToString();
                    userID = ds.Tables[0].Rows[0]["UserID"].ToString();
                    Pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
                    startTime = ds.Tables[0].Rows[0]["Begin"].ToString();
                    Date = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("MM/dd/yyyy");



                    ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                    DataSet dsURL = new DataSet();
                    CmdText = "select distinct AttendeeJoinURL from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + DDlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + DDlSessionNo.SelectedValue + " and CMemberID=" + DDlCoach.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "'";

                    dsURL = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    if (dsURL.Tables[0] != null)
                    {
                        if (dsURL.Tables[0].Rows.Count > 0)
                        {
                            hdnMeetingUrl.Value = dsURL.Tables[0].Rows[0]["AttendeeJoinURL"].ToString();
                        }
                    }


                    string GuestStudentId = DDLGuestStudent.SelectedValue;
                    string GuestCoachId = DDlGuestAttendee.SelectedValue;
                    if (GuestStudentId == "0")
                    {
                        GuestStudentId = "null";
                    }
                    if (GuestCoachId == "0")
                    {
                        GuestCoachId = "null";
                    }

                    if (GuestCoachId != "null")
                    {
                        string CmdDuplicateText = string.Empty;
                        int GuestAttendanceId = 0;


                        CmdDuplicateText = "select GuestAttendID from GuestAttendance where EventYear=" + ddYear.SelectedValue + " and MemberID=" + DDlGuestAttendee.SelectedValue + " and CMemberId=" + DDlCoach.SelectedValue + " and  Semester='" + ddlSemester.SelectedValue + "'";
                        DataSet dsCount = new DataSet();
                        dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdDuplicateText);
                        if (dsCount.Tables[0] != null)
                        {
                            if (dsCount.Tables[0].Rows.Count > 0)
                            {
                                GuestAttendanceId = Convert.ToInt32(dsCount.Tables[0].Rows[0]["GuestAttendID"].ToString());
                            }
                        }
                        string Msg = string.Empty;
                        if (BtnGenerateReport.Text == "Update Guest Attendee")
                        {
                            if (hdnGuestCoach.Value != "0")
                            {
                                GuestAttendanceId = Convert.ToInt32(hdnGuestAttendanceID.Value);
                            }
                        }
                        if (GuestAttendanceId > 0)
                        {
                            CmdText = "Update GuestAttendance set MemberID=" + DDlGuestAttendee.SelectedValue + ", CmemberID=" + DDlCoach.SelectedValue + " where GuestAttendID=" + GuestAttendanceId + "";
                            Msg = "Updated successfully.";
                        }
                        else
                        {
                            if (SessionKey == "")
                            {
                                SessionKey = "null";
                            }

                            CmdText = "Insert into GuestAttendance (MemberID,CMemberID,EventYear,EventID,ChapterID,StartDate,StartTime,RegisteredID,MeetingURL,CreatedDate,CreatedBy,SessionKey, Semester, Day, Time) values (" + DDlGuestAttendee.SelectedValue + "," + DDlCoach.SelectedValue + "," + ddYear.SelectedValue + ",13,112,'" + Date + "','" + startTime + "',null,'" + hdnMeetingUrl.Value + "',GetDate()," + Session["LoginID"].ToString() + "," + SessionKey + ", '" + ddlSemester.SelectedValue + "', '" + DDLDay.SelectedValue + "', '" + DDLTime.SelectedValue + "')";


                            Msg = "Guest Attendee registered successfully.";
                        }
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                        lblErrMsg.Text = Msg;
                    }

                    if (GuestStudentId != "null")
                    {


                        string CmdDuplicateText = string.Empty;
                        int GuestAttendanceId = 0;
                        string IsAdult = DDLGuestStudent.SelectedItem.Text.Split('-')[1].ToString();
                        if (hdnRegType.Value.Trim() == "Adult")
                        {
                            CmdDuplicateText = "select GuestAttendID from GuestAttendance where EventYear=" + ddYear.SelectedValue + " and Adultid=" + DDLGuestStudent.SelectedValue + " and CMemberId=" + DDlCoach.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "'";
                        }
                        else
                        {
                            CmdDuplicateText = "select GuestAttendID from GuestAttendance where EventYear=" + ddYear.SelectedValue + " and ChildNumber=" + DDLGuestStudent.SelectedValue + " and CMemberId=" + DDlCoach.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "'";
                        }


                        DataSet dsCount = new DataSet();
                        dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdDuplicateText);
                        if (dsCount.Tables[0] != null)
                        {
                            if (dsCount.Tables[0].Rows.Count > 0)
                            {
                                GuestAttendanceId = Convert.ToInt32(dsCount.Tables[0].Rows[0]["GuestAttendID"].ToString());
                            }
                        }
                        string Msg = string.Empty;
                        if (BtnGenerateReport.Text == "Update Guest Attendee")
                        {
                            if (hdnGuestStudent.Value != "0")
                            {
                                GuestAttendanceId = Convert.ToInt32(hdnGuestAttendanceID.Value);
                            }
                        }
                        if (GuestAttendanceId > 0)
                        {
                            IsAdult = DDLGuestStudent.SelectedItem.Text.Split('-')[1].ToString();
                            if (IsAdult.Trim() == "Adult")
                            {
                                CmdText = "Update GuestAttendance set AdultId=" + DDLGuestStudent.SelectedValue + ", childnumber=null, CmemberID=" + DDlCoach.SelectedValue + " where GuestAttendID=" + GuestAttendanceId + "";
                            }
                            else
                            {
                                CmdText = "Update GuestAttendance set ChildNumber=" + DDLGuestStudent.SelectedValue + ", AdultId=null, CmemberID=" + DDlCoach.SelectedValue + " where GuestAttendID=" + GuestAttendanceId + "";
                            }

                            Msg = "Updated successfully.";
                        }
                        else
                        {
                            if (SessionKey == "")
                            {
                                SessionKey = "null";
                            }
                            IsAdult = DDLGuestStudent.SelectedItem.Text.Split('-')[1].ToString();
                            if (IsAdult.Trim() == "Adult")
                            {
                                CmdText = "Insert into GuestAttendance (AdultId,CMemberID,EventYear,EventID,ChapterID,StartDate,StartTime,RegisteredID,MeetingURL,CreatedDate,CreatedBy,SessionKey, Semester) values (" + DDLGuestStudent.SelectedValue + "," + DDlCoach.SelectedValue + "," + ddYear.SelectedValue + ",13,112,'" + Date + "','" + startTime + "',null,'" + hdnMeetingUrl.Value + "',GetDate()," + Session["LoginID"].ToString() + "," + SessionKey + ", '" + ddlSemester.SelectedValue + "')";
                            }
                            else
                            {
                                CmdText = "Insert into GuestAttendance (ChildNumber,CMemberID,EventYear,EventID,ChapterID,StartDate,StartTime,RegisteredID,MeetingURL,CreatedDate,CreatedBy,SessionKey, Semester) values (" + DDLGuestStudent.SelectedValue + "," + DDlCoach.SelectedValue + "," + ddYear.SelectedValue + ",13,112,'" + Date + "','" + startTime + "',null,'" + hdnMeetingUrl.Value + "',GetDate()," + Session["LoginID"].ToString() + "," + SessionKey + ", '" + ddlSemester.SelectedValue + "')";
                            }
                            Msg = "Guest Attendee registered successfully.";
                        }
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                        lblErrMsg.Text = Msg;
                    }



                    fillGuestAttendeeGrid("true");
                    BtnGenerateReport.Text = "Register Guest Attendee";
                    hdnGuestAttendanceID.Value = "0";
                }
                else
                {
                    lblErrMsg.Text = "No zoom sessions found for the coach.";
                }
            }
        }
        catch (Exception)
        {
        }


    }





    public void fillGuestAttendeeGrid(string IsFilter)
    {
        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        cmdtext = "select GA.MemberID,GA.RegisteredID,GA.GuestAttendID,GA.CMemberID,GA.MemberID,CS.UserID,CS.Pwd,CS.MeetingKey,cs.ProductGroupID,cs.ProductID,cs.SessionNo, cs.Level, IP.FirstName +' '+ IP.LastName as Name, IP1.FirstName +' '+ IP1.LastName as CoachName, IP.Email as WebExEmail, cs.ProductGroupCode,cs.ProductCode,cs.Level, GA.MeetingURL, GA.RegisteredID,GA.StartTime,GA.StartDate,GA.EventYear, Cs.Day, CS.Semester, Case when GA.AdultId is null then C.First_name+' '+C.Last_name else IP2.FirstName + ' '+ IP2.LastName end as Childname, Case when GA.AdultId is null then c.email else IP2.Email end as childemail,  Case when GA.AdultId is null then GA.ChildNumber else GA.AdultId end as ChildNumber, cs.Time, case  when GA.AdultId is null then 'Child' else 'Adult' end as Type from GuestAttendance GA inner join CalSignup CS on (GA.SessionKey=CS.MeetingKey) left join IndSpouse IP on(IP.AutoMemberID=GA.MemberID) inner join IndSpouse IP1 on (IP1.AutoMemberID=GA.CMemberID) left join Child C on (C.ChildNumber=GA.ChildNumber) left join Indspouse IP2 on (GA.AdultId=IP2.AutoMemberId) where GA.Semester='" + ddlSemester.SelectedValue + "' and GA.EventYear='" + ddYear.SelectedValue + "'";

        if (IsFilter == "true")
        {
            cmdtext += " and GA.CMemberID=" + DDlCoach.SelectedValue + "";
        }
        if (ddlEventyearFilter.SelectedValue != "0")
        {
            cmdtext = cmdtext + " and GA.EventYear=" + ddlEventyearFilter.SelectedValue + "";
        }

        if (ddlProductGroupFilter.Items.Count > 0 && ddlProductGroupFilter.SelectedValue != "0" && ddlProductGroupFilter.SelectedValue != "Select")
        {
            cmdtext = cmdtext + "  and CS.ProductGroupID='" + ddlProductGroupFilter.SelectedValue + "'";
        }

        if ((ddlProductFilter.Items.Count > 0 && ddlProductFilter.SelectedValue != "0" && ddlProductFilter.SelectedValue != "Select"))
        {
            cmdtext = cmdtext + "  and CS.ProductID='" + ddlProductFilter.SelectedValue + "'";
        }

        if ((ddlLevelFilter.SelectedValue != "0"))
        {
            cmdtext = cmdtext + "  and CS.Level='" + ddlLevelFilter.SelectedValue + "'";
        }

        if ((ddlPhaseFilter.SelectedValue != "0"))
        {
            cmdtext = cmdtext + "  and GA.Semester='" + ddlPhaseFilter.SelectedValue + "'";
        }

        if ((ddlCoachFilter.SelectedValue != "0"))
        {
            cmdtext = cmdtext + "  and GA.CmemberID='" + ddlCoachFilter.SelectedValue + "'";
        }

        if ((ddlSessionFilter.SelectedValue != "0"))
        {
            cmdtext = cmdtext + "  and CS.Session='" + ddlSessionFilter.SelectedValue + "'";
        }

        if ((ddlDayFilter.SelectedValue != "0"))
        {
            cmdtext = cmdtext + "  and CS.Day='" + ddlDayFilter.SelectedValue + "'";
        }
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdGuestAttendee.DataSource = ds;
                    GrdGuestAttendee.DataBind();
                    spntable.InnerText = "";
                }
                else
                {
                    GrdGuestAttendee.DataSource = ds;
                    GrdGuestAttendee.DataBind();
                    spntable.InnerText = "No record exist.";
                }
            }
        }
        catch (Exception)
        {

        }
    }


    protected void BtnGenerateReport_Click(object sender, EventArgs e)
    {
        if (ValdiateGuestAttendance() > 0)
        {
            RegisterGuestAttendee();
        }

    }

    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {

        //if (hdnMeetingStatus.Value == "SUCCESS")
        //{

        //GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

        string CmdText = "Delete from GuestAttendance where GuestAttendID=" + hdnGuestAttendanceID.Value + "";

        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        lblErrMsg.Text = "Guest Attendee removed from the session successfully";
        fillGuestAttendeeGrid("false");
        // }
    }


    protected void GrdGuestAttendee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "DeleteAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                GrdGuestAttendee.Rows[selIndex].BackColor = System.Drawing.Color.Gray;
                string ChidName = GrdGuestAttendee.Rows[selIndex].Cells[2].Text;
                string Email = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblEmail") as Label).Text;

                string GuestAttendID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestAttendID") as Label).Text;
                string CMemberID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                hdnGuestAttendanceID.Value = GuestAttendID;

                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                string WebExID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblUserID") as Label).Text;
                string WebExPwd = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblPwd") as Label).Text;
                string SessionKey = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                string RegType = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblType") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                hdnSessionKey.Value = SessionKey;
                hdnRegType.Value = RegType;

                string CoachRegID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblRegID") as Label).Text;
                GrdGuestAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                HdnChildEmail.Value = Email;
                HdnCoachRegID.Value = CoachRegID;
                string guestAttendanceID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestAttendID") as Label).Text;
                hdnGuestAttendanceID.Value = guestAttendanceID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);

            }
            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdGuestAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string SessionKey = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now.AddMinutes(60);
                double mins = 40.0;
                string day = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblPractiseDate") as Label).Text;
                //day = Convert.ToDateTime(day).ToString("dddd");
                string beginTime = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                hdnSessionKey.Value = SessionKey;
                if (mins <= 30 && day == today)
                {
                    string cmdText = "";

                    string meetingLink = ((LinkButton)GrdGuestAttendee.Rows[selIndex].FindControl("HlAttendeeMeetURL") as LinkButton).Text;

                    hdnZoomURL.Value = meetingLink;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                }
            }
            else if (e.CommandName == "ModifyAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                GrdGuestAttendee.Rows[selIndex].BackColor = System.Drawing.Color.Gray;
                string ChidName = GrdGuestAttendee.Rows[selIndex].Cells[2].Text;
                string Email = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblEmail") as Label).Text;
                string RegType = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblType") as Label).Text;
                string GuestAttendID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestAttendID") as Label).Text;
                string CMemberID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string GuestStudentId = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestStudentId") as Label).Text;
                hdnRegType.Value = RegType;

                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                string WebExID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblUserID") as Label).Text;
                string WebExPwd = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblPwd") as Label).Text;
                string SessionKey = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                hdnSessionKey.Value = SessionKey;

                string CoachRegID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblRegID") as Label).Text;
                GrdGuestAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                HdnChildEmail.Value = Email;
                HdnCoachRegID.Value = CoachRegID;

                string CoachId = string.Empty;
                string eventYear = string.Empty;
                string productGroupID = string.Empty;
                string prodID = string.Empty;
                string level = string.Empty;
                string sessionNo = string.Empty;
                string guestCoach = string.Empty;
                string guestAttendanceID = string.Empty;
                eventYear = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblEventYear") as Label).Text;
                CoachId = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                productGroupID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                prodID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblProductID") as Label).Text;
                level = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblLevel") as Label).Text;
                sessionNo = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("LblSessionNo") as Label).Text;
                guestCoach = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestCoach") as Label).Text;
                guestAttendanceID = ((Label)GrdGuestAttendee.Rows[selIndex].FindControl("lblGuestAttendID") as Label).Text;
                hdnGuestAttendanceID.Value = guestAttendanceID;
                ddYear.SelectedValue = eventYear;
                fillCoach();
                DDlCoach.SelectedValue = CoachId;

                fillProductGroup();
                ddlProductGroup.SelectedValue = productGroupID;
                fillProduct();
                DDlProduct.SelectedValue = prodID;
                loadLevel();
                ddlLevel.SelectedValue = level;
                LoadSessionNo();
                DDlSessionNo.SelectedValue = sessionNo;
                fillGuestCoach();
                fillGuestStudent();
                
                hdnGuestCoach.Value = guestCoach;
                hdnGuestStudent.Value = GuestStudentId;
                BtnGenerateReport.Text = "Update Guest Attendee";
                try
                {
                    DDlGuestAttendee.SelectedValue = (guestCoach == "" ? "0" : guestCoach);
                }
                catch
                {
                    DDlGuestAttendee.SelectedValue = (guestCoach == "" ? "0" : guestCoach);
                }

                DDLGuestStudent.SelectedValue = (GuestStudentId == "" ? "0" : GuestStudentId);
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }
    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillGuestAttendeeGrid("true");
    }
    public int ValdiateGuestAttendance()
    {
        int Retval = 1;
        string CoachId = DDlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = DDlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string GuestCoach = DDlGuestAttendee.SelectedValue;
        string GuestStudent = DDLGuestStudent.SelectedValue;
        string Day = DDLDay.SelectedValue;
        string Time = DDLTime.SelectedValue;
        if (CoachId == "0")
        {
            lblErrMsg.Text = "Please select Coach.";
            Retval = -1;
        }
        else if (PGId == "0")
        {
            lblErrMsg.Text = "Please select Product Group.";
            Retval = -1;
        }
        else if (PId == "0")
        {
            lblErrMsg.Text = "Please select Product.";
            Retval = -1;
        }
        else if (Level == "0")
        {
            lblErrMsg.Text = "Please select Level.";
            Retval = -1;
        }
        else if (Day == "")
        {
            lblErrMsg.Text = "Please select Day.";
            Retval = -1;
        }
        else if (Time == "")
        {
            lblErrMsg.Text = "Please select Time.";
            Retval = -1;
        }
        else if (GuestCoach == "0" && GuestStudent == "0")
        {
            lblErrMsg.Text = "Please select Guest Attendee(Coach) or Guest Attendee(Student).";
            Retval = -1;
        }
        return Retval;
    }

    public void LoadDayAndTime()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Day,V.Time from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + DDlCoach.SelectedValue + "' and V.ProductID=" + DDlProduct.SelectedValue + " and V.Semester='" + ddlSemester.SelectedValue + "' and V.SessionNo=" + DDlSessionNo.SelectedValue + "";


        }
        else
        {
            cmdText = "select distinct V.Day, V.Time from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.ProductID=" + DDlProduct.SelectedValue + " and V.Semester='" + ddlSemester.SelectedValue + "' and V.SessionNo=" + DDlSessionNo.SelectedValue + "";

        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {


            DDLDay.DataTextField = "Day";
            DDLDay.DataValueField = "Day";
            DDLDay.DataSource = ds;
            DDLDay.DataBind();

            DDLTime.DataValueField = "Time";
            DDLTime.DataTextField = "Time";
            DDLTime.DataSource = ds;
            DDLTime.DataBind();

            if (ds.Tables[0].Rows.Count > 1)
            {
                DDLDay.Enabled = true;
                DDLTime.Enabled = true;
            }
            else
            {
                DDLDay.Enabled = false;
                DDLTime.Enabled = false;
            }
        }

    }
    protected void DDlSessionNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadDayAndTime();
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }

    public void fillYearFiletr()
    {
        int Year = Convert.ToInt32(DateTime.Now.Year);

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)));

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)));

        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(eventyear) from EventFees where EventId=13").ToString();

    }

    public void loadProductGroupFilter()
    {
        try
        {
            string strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId inner join CalSignup cs on (cs.ProductGroupId=P.ProductGroupId) inner join GuestAttendance GA on (GA.SessionKey=cs.MeetingKey) where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND  P.EventId=13 and cs.memberId in (Select CMemberId from GuestAttendance where EventYear=" + ddlEventyearFilter.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' ) ";

            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + " order by P.ProductGroupID";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductGroupFilter.DataValueField = "ProductGroupID";
            ddlProductGroupFilter.DataTextField = "Name";

            ddlProductGroupFilter.DataSource = drproductgroup;
            ddlProductGroupFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {
                ddlProductGroupFilter.Items.Insert(0, new ListItem("Select", "0"));

                ddlProductGroupFilter.Enabled = true;
            }
            else
            {
                ddlProductGroupFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadProductFilter()
    {
        try
        {
            string strSql = " Select distinct P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId inner join CalSignup cs on (cs.ProductId=p.productId and cs.Accepted='Y') inner join GuestAttendance GA on (GA.SessionKey=cs.MeetingKey)  where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND P.EventID=13 and cs.memberId in (Select CMemberId from GuestAttendance where EventYear=" + ddlEventyearFilter.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' ) ";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and P.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + "order by P.ProductID";


            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());


            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductFilter.DataValueField = "ProductID";
            ddlProductFilter.DataTextField = "Name";

            ddlProductFilter.DataSource = drproductgroup;
            ddlProductFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlProductFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlProductFilter.Enabled = true;
            }
            else
            {
                ddlProductFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadLevelFilter()
    {
        try
        {
            string strSql = " select distinct P.LevelCode from ProdLevel P inner join CalSignup cs on (cs.Level=P.LevelCode and cs.Accepted='Y') inner join GuestAttendance GA on (GA.SessionKey=cs.MeetingKey) where p.EventYear=" + ddlEventyearFilter.SelectedValue + "  and p.EventID=13 and cs.memberId in (Select CMemberId from GuestAttendance where EventYear=" + ddlEventyearFilter.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' )";
            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and p.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and p.ProductID=" + ddlProductFilter.SelectedValue + "";
            }

            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
            ddlLevelFilter.DataValueField = "LevelCode";
            ddlLevelFilter.DataTextField = "LevelCode";
            ddlLevelFilter.DataSource = drproductgroup;
            ddlLevelFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlLevelFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlLevelFilter.Enabled = true;
            }
            else
            {
                ddlLevelFilter.Enabled = false;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadCoachFilter()
    {
        try
        {
            string strSql = " select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignup V On (V.MemberID = I.AutoMemberID and V.EventYear=" + ddlEventyearFilter.SelectedValue + ") inner join GuestAttendance GA on (GA.CMemberId=V.MemberId)  Where V.EventYear= " + ddlEventyearFilter.SelectedValue + "   and GA.EventYear=" + ddlEventyearFilter.SelectedValue + " ";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductID=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlLevelFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Level=" + ddlLevelFilter.SelectedValue + "";
            }
            if (ddlPhaseFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }


            strSql = strSql + " order by I.FirstName,I.LastName";

            DataSet drproductgroup = new DataSet();
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlCoachFilter.DataValueField = "MemberID";
            ddlCoachFilter.DataTextField = "Name";

            ddlCoachFilter.DataSource = drproductgroup;
            ddlCoachFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlCoachFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlCoachFilter.Enabled = true;
            }
            else
            {
                ddlCoachFilter.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    protected void ddlProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductFilter();
        loadCoachFilter();


        fillGuestAttendeeGrid("No");

    }

    protected void ddlProductFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevelFilter();
        loadCoachFilter();


        fillGuestAttendeeGrid("No");
    }

    protected void btnClearFilter_Click(object sender, EventArgs e)
    {



        fillYearFiletr();
        loadProductGroupFilter();
        loadProductFilter();

        loadLevelFilter();
        loadCoachFilter();

        fillGuestAttendeeGrid("No");
    }

    protected void ddlLevelFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;


        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillGuestAttendeeGrid("No");
    }

    protected void ddlSessionFilter_SelectedIndexChanged(object sender, EventArgs e)
    {



        fillGuestAttendeeGrid("No");

    }
    protected void ddlDayFilter_SelectedIndexChanged1(object sender, EventArgs e)
    {



        fillGuestAttendeeGrid("No");
    }
    protected void ddlPhaseFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadCoachFilter();

        fillGuestAttendeeGrid("No");

    }

    protected void ddlCoachFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillGuestAttendeeGrid("No");
    }
    public void Reset()
    {
        Response.Redirect("~/GuestAttendance.aspx");
    }
    protected void BtnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }
}