﻿using System;
using System.Net;
using System.Text;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using NativeExcel;
/*
References
http://www.socketlabs.com/api-reference/notification-api/
 * http://www.socketlabs.com/api-reference/reporting-api/messagesfblreported/
 * http://www.socketlabs.com/api-reference/reporting-api/messagesfailed/
 * https://github.com/socketlabs/email-on-demand-examples/blob/master/DotNet/Reporting%20API/csharp.api/Program.cs
 * 
*/

public partial class FailComplaint : System.Web.UI.Page
{
    const int serverId = 2722; // YOUR-SERVER-ID
    const string userName = "northsouth";
    const string password = "28a71cb970346ebb5420"; //"CoMuYXi2oh7B";
    int Failedcount = 0;
    int Complaintcount = 0;
    int ComplaintAbusecount = 0;
    int m_PageIndex = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"] = 4240;
        if (!IsPostBack)
        {
            ddlOption.Items[2].Selected = true;

            btnHidSearchFlag.Value = "false";
            if (Request.QueryString["GetAll"] == null)
            {
                if (Session["LoginID"] == null)
                {
                    Response.Redirect("~/Maintest.aspx");
                }
                else
                {
                    changeCompType();
                    loaddata(false);
                }
            }
            else if (Request.QueryString["GetAll"] == "true")
            {
                GetData(ddlOption.Items[0].Value);
                GetData(ddlOption.Items[1].Value);
                //GetData(ddlOption.Items[2].Value);
                SendEmail("Accessing Socket Labs API : Date " + System.DateTime.Now.ToString(), Label1.Text, "ferdin448@gmail.com");
                btnGetReport.Visible = false;
                ddlOption.Visible = false;
                BtnExport.Visible = false;
                Label2.Visible = false;
            }
        }
        // loaddata(false);

    }

    public void loaddata(Boolean Paging)
    {

        GVFailComplaints.Visible = true;
        GrdAbuseActive.Visible = false;
        lblNoRecord.Text = "Table 1: " + ddlOption.SelectedItem.Text + "";

        String sql = "SELECT FailCompID,MailFrom,MailTo,Date,Type,MailingID,MessageID,ServerID,SecretKey,ComplaintType,UserAgent,NewsLetter,BounceStatus,DiagnosticCode,FailureCode,FailureType,Reason,RemoteMta,Status FROM FailComplaint Left join Indspouse on mailTo=Email where";

        if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
            sql = sql + " Type = 'Complaint' ";
        else
            sql = sql + " Type = '" + ddlOption.SelectedItem.Text + "'";

        if (ddlType.SelectedItem.Value != "all")
        {
            if (ddlOption.SelectedItem.Text == "Failed")
                sql = sql + " AND FailureType='" + ddlType.SelectedItem.Value + "'";
            //else if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
            //    sql = sql + "AND ComplaintType='Abuse' ";              
            else
                sql = sql + "AND ComplaintType='" + ddlType.SelectedItem.Value + "'";
        }

        if (btnHidSearchFlag.Value == "true")
        {

            sql = sql + " AND MailTo like '%" + txtSearch.Text.Trim() + "%' ";
        }

        sql = sql + " ORDER BY [DATE] DESC";
        try
        {
            DataSet dsFailComplaints = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sql);

            if (dsFailComplaints.Tables[0].Rows.Count > 0)
            {
                GVFailComplaints.DataSource = dsFailComplaints;
                BtnExport.Visible = true;
                GVFailComplaints.Visible = true;
                Label1.Text = "";
                if (Paging == true)
                    GVFailComplaints.PageIndex = m_PageIndex;
                else
                    GVFailComplaints.PageIndex = 0;
                GVFailComplaints.DataBind();
            }
            else
            {
                GVFailComplaints.DataSource = "";
                BtnExport.Visible = false;
                GVFailComplaints.Visible = false;
                Label1.Text = "No Record to display.";
            }
        }
        catch (Exception e)
        {
            lblerr.Text = e.ToString();
        }
    }
    protected void GVFailComplaints_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        m_PageIndex = e.NewPageIndex;
        loaddata(true);
    }
    private void SendEmail(string sSubject, string sBody, string sMailTo)
    {
        string sFrom = "nsfmailalerts@gmail.com";
        MailMessage mail = new MailMessage(sFrom, sMailTo, sSubject, sBody);
        mail.To.Add("nsfdevteam@gmail.com");
        SmtpClient client = new SmtpClient();
        // string host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost");
        //client.Host = host;
        mail.IsBodyHtml = true;

        bool ok = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
            ok = false;
        }
    }

    protected void btnRetrieve_Click(object sender, EventArgs e)
    {
        GetData(ddlOption.SelectedItem.Value);
    }


    public void GetData(String Selection)
    {
        String ServerId = serverId.ToString();
        String DateTime;
        // String MessageId;
        // String MailingId;
        String ToAddress; //OriginalRecipient
        String FromAddress;
        String FailureType;
        String FailureCode;
        String Reason;
        String Type;
        String UserAgent;
        String Isp;
        DateTime MailDate = Convert.ToDateTime("2013-03-17 00:12:00");
        String GetDate = "";


        try
        {
            String sSQL;
            if (Selection == "messagesFailed")
                sSQL = "SELECT TOP 1 [DATE] FROM FailComplaint WHERE Type='Failed' ORDER BY [DATE] DESC";
            else //if (Selection == "messagesFblReported")
                sSQL = "SELECT TOP 1 [DATE] FROM FailComplaint WHERE Type='Complaint' ORDER BY [DATE] DESC";
            //else
            //    sSQL = "SELECT TOP 1 [DATE] FROM FailComplaint WHERE Type='Complaint' and ComplaintType='Abuse' ORDER BY [DATE] DESC";
            //Label1.Text = sSQL;
            GetDate = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sSQL).ToString();
            if (GetDate.Length > 0)
                MailDate = Convert.ToDateTime(GetDate);
        }
        catch (Exception ex)
        {
            //Label1.Text = ex.ToString();
        }

        //lblerr.Text = "https://api.socketlabs.com/v1/" + Selection + "?type=xml&serverId=" + serverId + "&startDate=" + MailDate.ToString();
        Uri apiUri = new Uri("https://api.socketlabs.com/v1/" + Selection + "?type=xml&serverId=" + serverId + "&startDate=" + MailDate.ToString());

        //WebRequest request = WebRequest.Create(apiUri);
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUri);
        string authInfo = userName + ":" + password;
        authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

        request.Headers["Authorization"] = "Basic " + authInfo;
        //ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(customXertificateValidation);
        ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
        XmlDocument apiResponse = new XmlDocument();
        String sql = "";
        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {

                    Label1.Text = "";

                    apiResponse.Load(reader);

                    switch (Selection)
                    {
                        case "messagesFailed":


                            foreach (System.Xml.XmlNode node in apiResponse.SelectNodes("//item"))
                            {
                                //ServerId,DateTime,MessageId,MailingId,ToAddress,FromAddress,FailureType,FailureCode,Reason
                                // ServerId = node["ServerId"].InnerText;
                                DateTime = node["DateTime"].InnerText;
                                DateTime = DateTime.ToString().Replace("T", " ").Replace("Z", "");
                                //MessageId = node["MessageId"].InnerText;
                                //MailingId = node["MailingId"].InnerText;
                                ToAddress = node["ToAddress"].InnerText;
                                FromAddress = node["FromAddress"].InnerText;
                                FailureType = node["FailureType"].InnerText;
                                FailureCode = node["FailureCode"].InnerText;
                                Reason = node["Reason"].InnerText;
                                //FailCompID, MailFrom, MailTo, Date, Type, MailingID, MessageID, ServerID, SecretKey, UserAgent, BounceStatus, DiagnosticCode, FailureCode, FailureType, Reason, RemoteMta FROM         FailComplaint
                                if (Convert.ToDateTime(DateTime) > MailDate)
                                {
                                    sql = sql + "INSERT INTO FailComplaint(Type,Date,MailTo,MailFrom,ServerID,FailureType,FailureCode,Reason) VALUES ('Failed','" + DateTime.ToString().Replace("T", " ").Replace("Z", "") + "','" + ToAddress + "','" + FromAddress + "','" + ServerId + "'," + FailureType + ",'" + FailureCode + "','" + Reason.Replace("'", "''") + "');";
                                    Failedcount++;
                                }
                            }
                            break;
                        case "messagesFblReported":

                            foreach (System.Xml.XmlNode node in apiResponse.SelectNodes("//item"))
                            {
                                //ServerId,DateTime,MessageId,MailingId,OriginalRecipient,FromAddress,UserAgent,Type,Isp
                                DateTime = node["DateTime"].InnerText;
                                DateTime = DateTime.ToString().Replace("T", " ").Replace("Z", "");
                                //MessageId = node["MessageId"].InnerText;
                                ToAddress = node["OriginalRecipient"].InnerText;
                                FromAddress = node["FromAddress"].InnerText;
                                UserAgent = node["UserAgent"].InnerText;
                                Type = node["Type"].InnerText;
                                Isp = node["Isp"].InnerText;
                                //FailCompID, MailFrom, MailTo, Date, Type, MailingID, MessageID, ServerID, SecretKey, UserAgent, BounceStatus, DiagnosticCode, FailureCode, FailureType, Reason, RemoteMta FROM         FailComplaint
                                if (Convert.ToDateTime(DateTime) > MailDate)
                                {
                                    sql = sql + "INSERT INTO FailComplaint (Type,Date,MailTo,MailFrom,ServerID,UserAgent,ComplaintType) Select 'Complaint','" + DateTime + "','" + ToAddress + "','" + FromAddress + "','" + ServerId + "','" + UserAgent + "','" + Type + "' Where not exists(select * from FailComplaint where CONVERT (DATE,[DATE])=CONVERT (DATE,'" + DateTime + "') AND MailTo='" + ToAddress + "' AND Type='Complaint');";
                                    Complaintcount++;
                                }
                            }
                            break;
                        //case "messagesAbuse":

                        //    foreach (System.Xml.XmlNode node in apiResponse.SelectNodes("//item"))
                        //    {
                        //        //ServerId,DateTime,MessageId,MailingId,OriginalRecipient,FromAddress,UserAgent,Type,Isp
                        //        DateTime = node["DateTime"].InnerText;
                        //        DateTime = DateTime.ToString().Replace("T", " ").Replace("Z", "");
                        //        //MessageId = node["MessageId"].InnerText;
                        //        ToAddress = node["OriginalRecipient"].InnerText;
                        //        FromAddress = node["FromAddress"].InnerText;
                        //        UserAgent = node["UserAgent"].InnerText;
                        //        Type = node["Type"].InnerText;
                        //        Isp = node["Isp"].InnerText;
                        //        //FailCompID, MailFrom, MailTo, Date, Type, MailingID, MessageID, ServerID, SecretKey, UserAgent, BounceStatus, DiagnosticCode, FailureCode, FailureType, Reason, RemoteMta FROM         FailComplaint
                        //        if (Convert.ToDateTime(DateTime) > MailDate)
                        //        {
                        //            sql = sql + "INSERT INTO FailComplaint (Type,Date,MailTo,MailFrom,ServerID,UserAgent,ComplaintType) Select 'Complaint','" + DateTime + "','" + ToAddress + "','" + FromAddress + "','" + ServerId + "','" + UserAgent + "','" + Type + "' Where not exists(select * from FailComplaint where CONVERT (DATE,[DATE])=CONVERT (DATE,'" + DateTime + "') AND MailTo='" + ToAddress + "' AND Type='Complaint' and ComplaintType='Abuse' );";
                        //            ComplaintAbusecount++;

                        //        }
                        //    }
                        //    break;
                    }
                    if (sql.Length > 0)
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
                    Label1.Text = "Total Number of Failed Records inserted Now : " + Failedcount.ToString() + " ### Total Number of Complaint Records inserted Now : " + Complaintcount + ".";
                    //+" ### Total Number of Compaint_Abuse Records inserted Now:" + ComplaintAbusecount + ".";
                }
            }
        }
        catch (Exception ex)
        {
            Label1.Text = ex.ToString() + sql;
        }
        // Label1.Text = sql;
    }

    protected void ddlOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOption.SelectedValue == "AbuseActive")
        {
            btnHidSearchFlag.Value = "false";
            changeCompType();
            LoadAbuseActive(false);
        }
        else
        {
            btnHidSearchFlag.Value = "false";
            changeCompType();
            loaddata(false);
        }
    }
    public void changeCompType()
    {
        btnBulkUnsubscribe.Visible = false;
        if (ddlOption.SelectedItem.Text == "Failed")
        {
            ddlType.Items.Clear();
            ddlType.Items.Insert(0, new ListItem("All", "all"));
            /* 0 - Soft/Temporary failures
             * 1 - Hard/Permanent failures
             * 2 - Suppressions
            */
            ddlType.Items.Insert(1, new ListItem("Soft/Temporary", "0"));
            ddlType.Items.Insert(2, new ListItem("Hard/Permanent", "1"));
            ddlType.Items.Insert(3, new ListItem("Suppressions", "2"));
            ddlType.SelectedIndex = 0;
        }
        else if (ddlOption.SelectedItem.Text == "Complaint")
        {
            ddlType.Items.Clear();
            ddlType.Items.Insert(0, new ListItem("All", "all"));
            /* 0 - Complaint – Abuse
                Complaint - Bounce
                Complaint – Fail
            */
            ddlType.Items.Insert(1, new ListItem("Abuse", "Abuse"));
            ddlType.Items.Insert(2, new ListItem("Bounce", "Bounce"));
            ddlType.Items.Insert(3, new ListItem("Fail", "Fail"));
            ddlType.SelectedIndex = 0;
        }
        else
        {
            ddlType.Items.Clear();
            ddlType.Items.Insert(0, new ListItem("Abuse", "Abuse"));
            btnBulkUnsubscribe.Visible = true;
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnHidSearchFlag.Value = "false";
        loaddata(false);
    }

    protected void btnGetReport_Click(object sender, EventArgs e)
    {
        loaddata(false);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblerr.Text = "";

        //Response.Clear();
        //Response.AppendHeader("content-disposition", "attachment;filename=Report_" + ddlOption.SelectedItem.Text + ".xls");
        //Response.Charset = "";
        ////Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        //Response.ContentType = "application/vnd.xls";
        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        string sqlText = "";
        if (ddlOption.SelectedValue == "AbuseActive")
        {
            sqlText = "SELECT FailCompID,MailFrom,MailTo,Date,Type,MailingID,MessageID,ServerID,SecretKey,ComplaintType,UserAgent,NewsLetter,BounceStatus,DiagnosticCode,FailureCode,FailureType,Reason,RemoteMta,Status FROM FailComplaint Left join Indspouse on mailTo=Email where";


            sqlText = sqlText + " Type = 'Complaint' ";

            if (ddlType.SelectedItem.Value != "all")
            {
                if (ddlOption.SelectedItem.Text == "Failed")
                    sqlText = sqlText + " AND FailureType='" + ddlType.SelectedItem.Value + "'";
                //else if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
                //    sql = sql + "AND ComplaintType='Abuse' ";              
                else
                    sqlText = sqlText + "AND ComplaintType='" + ddlType.SelectedItem.Value + "'";
            }

            if (btnHidSearchFlag.Value == "true")
            {

                sqlText = sqlText + " AND MailTo like '%" + txtSearch.Text.Trim() + "%' ";
            }

            sqlText = sqlText + " and Date <(select isnull( max(dt), Date) from ((select max(CreateDate)  as dt from contestant where parentID=AutoMemberId)union all (select max(CreateDate) as dt from CoachReg where PMemberID=AutoMemberId) union all (select max(CreateDate) as dt from Registration where MemberID=AutoMemberId and EventId=3) union all (select max(CreateDate) as dt from Registration_OnlineWkshop where MemberID=AutoMemberId) union all (select max(CreateDate) as dt from Game where MemberID=AutoMemberId) union all (select max(CreateDate) as dt from DonationsInfo where MemberID=AutoMemberId)) as dt1)";

            sqlText = sqlText + " ORDER BY [DATE] DESC";
        }
        else
        {
            sqlText = sqlText + " SELECT FailCompID,MailFrom,MailTo,Date,Type,MailingID,MessageID,ServerID,SecretKey,ComplaintType,UserAgent,NewsLetter,BounceStatus,DiagnosticCode,FailureCode,FailureType,Reason,RemoteMta,Status FROM FailComplaint Left join Indspouse on mailTo=Email WHERE ";

            if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
                sqlText = sqlText + " Type = 'Complaint' ";
            else
                sqlText = sqlText + " Type = '" + ddlOption.SelectedItem.Text + "'";

            if (ddlType.SelectedItem.Value != "all")
            {
                if (ddlOption.SelectedItem.Text == "Failed")
                    sqlText = sqlText + " AND FailureType='" + ddlType.SelectedItem.Value + "'";
                else if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
                    sqlText = sqlText + "AND ComplaintType='Abuse' ";
                else
                    sqlText = sqlText + "AND ComplaintType='" + ddlType.SelectedItem.Value + "'";
            }

            //if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
            //    sqlText=sqlText + " Type='Complaint' and ComplaintType='Abuse' " ;
            //else
            //    sqlText=sqlText+ " Type='" + ddlOption.SelectedItem.Text + "' ";

            if (btnHidSearchFlag.Value == "true" & txtSearch.Text.Trim().Length > 0)
                sqlText = sqlText + " AND MailTo like '%" + txtSearch.Text + "%'";

            sqlText = sqlText + " ORDER BY [DATE] DESC";
        }
        lblerr.Visible = true;
        lblerr.Text = sqlText;
        DataSet dsFailComplaints = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlText);
        IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
        IWorksheet oSheet = default(IWorksheet);
        oSheet = oWorkbooks.Worksheets.Add();

        string FileName = "Report_" + ddlOption.SelectedItem.Text + ".xls";
        string Digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int CharStIndex = 0;
        int iRowIndex = 2;
        IRange CRange = default(IRange);
        int i = 0, j = 0;
        DataTable dt = dsFailComplaints.Tables[0];
        for (i = 0; i < dt.Columns.Count; i++)
        {
            CRange = oSheet.Range[Digits[CharStIndex] + "1"];
            CRange.Value = dt.Columns[i].ColumnName;
            CRange.Font.Bold = true;
            CharStIndex = CharStIndex + 1;
        }
        CharStIndex = 0;
        for (i = 0; i < dt.Rows.Count; i++)
        {

            CharStIndex = 0;
            for (j = 0; j < dt.Columns.Count; j++)
            {
                CRange = oSheet.Range[Digits[CharStIndex] + iRowIndex.ToString()];
                CRange.Value = dt.Rows[i][j];
                CharStIndex = CharStIndex + 1;
            }
            iRowIndex = iRowIndex + 1;
        }
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        oWorkbooks.SaveAs(Response.OutputStream);
        Response.End();

        //GridView Grd = new GridView();
        //Grd.DataSource = dsFailComplaints;
        //Grd.DataBind();
        //Grd.RenderControl(hw);
        //Response.Write(sw.ToString());
        //Response.End();

    }
    protected string GetUrl(object id)
    {
        return "javascript:var w=window.open('ShowIndRecord.aspx?First=T&Last=T&Email=" + id + "','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;')";
        //
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblerr.Text = "";
        lblerr.ForeColor = System.Drawing.Color.Red;
        if (txtSearch.Text.Trim().Length == 0)
        {
            btnHidSearchFlag.Value = "false";
            lblerr.Text = "Please enter email";
        }
        else
        {
            btnHidSearchFlag.Value = "true";
            loaddata(false);
        }
    }
    protected void lnkBtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("FailComplaint.aspx");
    }
    protected void btnBulkUnsubscribe_Click(object sender, EventArgs e)
    {
        string search = "";
        if (btnHidSearchFlag.Value == "true")
        {
            search = " AND MailTo like '%" + txtSearch.Text.Trim() + "%' ";
        }
        String sql = " update a SET NewsLetter=3 FROM Indspouse a ";
        sql = sql + " Inner join FailComplaint on mailTo=Email and Type = 'Complaint' AND ComplaintType='Abuse' and isnull(a.NewsLetter,0)<>3 " + search;
        sql = sql + " LEFT JOIN (SELECT Automemberid, Max(date123) AS latestdate ";
        sql = sql + " FROM   (SELECT Automemberid,(select  max(CreateDate) from contestant where parentID=I.Automemberid ) DTCn, ";
        sql = sql + " (select max(CreateDate) from CoachReg where PMemberID=I.Automemberid) DTCR,";
        sql = sql + " (select max(CreateDate) from Registration where MemberID=I.Automemberid ) DTReg,";
        sql = sql + " (select max(CreateDate) from Registration_OnlineWkshop where MemberID=I.Automemberid ) DTOnWk,";
        sql = sql + " (select max(CreateDate) from Game where MemberID=I.Automemberid ) DTGame ,";
        sql = sql + " (select convert(smalldatetime,max(CreateDate)) from DonationsInfo where MemberID=I.Automemberid) DTDon";
        sql = sql + " FROM Indspouse I Inner join FailComplaint on mailTo=Email and Type = 'Complaint' AND ComplaintType='Abuse' and isnull(I.NewsLetter,0)<>3 " + search + ") src";
        sql = sql + " UNPIVOT (date123 FOR dates IN (DTCn,DTCR,DTReg,DTOnWk,DTGame,DTDon";
        sql = sql + " )) unpvt GROUP BY Automemberid ) b on a.Automemberid=b.Automemberid";
        sql = sql + " and DATEDIFF(MONTH,a.createdate,getdate())>36";
        sql = sql + " and DATEDIFF(MONTH,b.latestdate,getdate())>36 ";

        try
        {
            int iRCnt = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
            if (iRCnt > 0)
            {
                lblerr.Text = iRCnt.ToString() + " complainant newsletter was unsubscribed successfully";
                lblerr.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblerr.Text = "No record found to unsubscribe";
                lblerr.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void GrdAbuseActive_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        m_PageIndex = e.NewPageIndex;
        LoadAbuseActive(true);
    }

    public void LoadAbuseActive(Boolean Paging)
    {
        GVFailComplaints.Visible = false;
        GrdAbuseActive.Visible = true;
        lblNoRecord.Text = "Table 1: Abuse_Active";
        String sql = "SELECT FailCompID,MailFrom,MailTo,Date,Type,MailingID,MessageID,ServerID,SecretKey,ComplaintType,UserAgent,NewsLetter,BounceStatus,DiagnosticCode,FailureCode,FailureType,Reason,RemoteMta,Status FROM FailComplaint Left join Indspouse on mailTo=Email where";


        sql = sql + " Type = 'Complaint' ";

        if (ddlType.SelectedItem.Value != "all")
        {
            if (ddlOption.SelectedItem.Text == "Failed")
                sql = sql + " AND FailureType='" + ddlType.SelectedItem.Value + "'";
            //else if (ddlOption.SelectedItem.Text == "Complaint_Abuse")
            //    sql = sql + "AND ComplaintType='Abuse' ";              
            else
                sql = sql + "AND ComplaintType='" + ddlType.SelectedItem.Value + "'";
        }

        if (btnHidSearchFlag.Value == "true")
        {

            sql = sql + " AND MailTo like '%" + txtSearch.Text.Trim() + "%' ";
        }

        sql = sql + " and Date <(select isnull( max(dt), Date) from ((select max(CreateDate)  as dt from contestant where parentID=AutoMemberId)union all (select max(CreateDate) as dt from CoachReg where PMemberID=AutoMemberId) union all (select max(CreateDate) as dt from Registration where MemberID=AutoMemberId and EventId=3) union all (select max(CreateDate) as dt from Registration_OnlineWkshop where MemberID=AutoMemberId) union all (select max(CreateDate) as dt from Game where MemberID=AutoMemberId) union all (select max(CreateDate) as dt from DonationsInfo where MemberID=AutoMemberId)) as dt1)";

        sql = sql + " ORDER BY [DATE] DESC";
        try
        {
            DataSet dsFailComplaints = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sql);

            if (dsFailComplaints.Tables[0].Rows.Count > 0)
            {
                GrdAbuseActive.DataSource = dsFailComplaints;
                BtnExport.Visible = true;
                GrdAbuseActive.Visible = true;
                Label1.Text = "";
                if (Paging == true)
                    GrdAbuseActive.PageIndex = m_PageIndex;
                else
                    GrdAbuseActive.PageIndex = 0;
                GrdAbuseActive.DataBind();
            }
            else
            {
                GrdAbuseActive.DataSource = "";
                BtnExport.Visible = false;
                GrdAbuseActive.Visible = false;
                Label1.Text = "No Record to display.";
            }
        }
        catch (Exception e)
        {
            lblerr.Text = e.ToString();
        }
    }
}




