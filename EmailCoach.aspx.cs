﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using VRegistration;


public partial class EmailCoach : System.Web.UI.Page
{
    string StrTableName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //    Session["LoginID"] = 4240;
        //    Session["RoleId"] = 1;
        //    Session["LoggedIn"] = "true";

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (Session["RoleId"].ToString() == "88")
        {
            rdcalsignup.Enabled = false;
            RadioButton1.Enabled = false;
            RbtnVolunteerSignUp.Enabled = false;
        }
        else
        {
            rdcalsignup.Enabled = true;
            RadioButton1.Enabled = true;
            RbtnVolunteerSignUp.Enabled = true;
        }
        Session["EmailSource"] = "EmailCoach";
        if (!IsPostBack)
        {


            Session["ParentChildMailCnt"] = null;
            //GetCoachName(ddlCoach); Commented On 21-03-2013 to list Coaches based on ProductGroup,Product,Semester,Session Drop down Selections
            //int year = Convert.ToInt32(DateTime.Now.Year);
            string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead <>'Y' and MemberID=" + Session["LoginID"].ToString();

            int mcount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql));
            if (mcount > 0)
            {
                rdCoaches.Enabled = false;
            }
            else
            {
                rdCoaches.Enabled = true;
            }
            int first_year = 2006;
            int year = Convert.ToInt32(DateTime.Now.Year) + 2;
            int count = year - first_year;
            int year_index;
            for (int i = 0; i < count; i++)
            {
                lstYear.Items.Insert(i, new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - i).Substring(2, 2)), Convert.ToString(year - (i + 1))));

                lstYearCoach.Items.Insert(i, new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - i).Substring(2, 2)), Convert.ToString(year - (i + 1))));
                lstYearCoach1.Items.Insert(i, new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - i).Substring(2, 2)), Convert.ToString(year - (i + 1))));
                lstYearparentstudents.Items.Insert(i, new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - i).Substring(2, 2)), Convert.ToString(year - (i + 1))));
                //year = year - 1
            }

            string Coachingyear = string.Empty;
            Coachingyear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
            lstYear.SelectedValue = Coachingyear;
            lstYearCoach.SelectedValue = Coachingyear;
            lstYearCoach1.SelectedValue = Coachingyear;
            lstYearparentstudents.SelectedValue = Coachingyear;

            Common cm = new Common();
            ddlSemester.DataSource = cm.GetSemesters();
            ddlSemester.DataBind();
            ddlSemester.SelectedValue = cm.GetDefaultSemester(lstYear.SelectedValue);

            ddlSemester1.DataSource = cm.GetSemesters();
            ddlSemester1.DataBind();
            ddlSemester1.SelectedValue = cm.GetDefaultSemester(lstYearparentstudents.SelectedValue);

            ddlSemesterCoach.DataSource = cm.GetSemesters();
            ddlSemesterCoach.DataBind();
            ddlSemesterCoach.SelectedValue = cm.GetDefaultSemester(lstYearCoach.SelectedValue);

            DropDownList3.DataSource = cm.GetSemesters();
            DropDownList3.DataBind();
            DropDownList3.SelectedValue = cm.GetDefaultSemester(lstYearCoach1.SelectedValue);

            //lstYear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            //lstYear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            //lstYear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));

            //Commented on Jan 8,2015 to show 
            //lstYear.Items[1].Selected = true;
            //lstYearCoach.Items[1].Selected = true;
            //lstYearCoach1.Items[1].Selected = true;
            //lstYearparentstudents.Items[1].Selected = true;

            if (DateTime.Now.Month <= 3)
            {
                year_index = DateTime.Now.Year - 1;
            }
            else
            {
                year_index = DateTime.Now.Year;
            }

            //lstYear.SelectedIndex = lstYear.Items.IndexOf(lstYear.Items.FindByValue(year_index.ToString()));
            //lstYearCoach.SelectedIndex = lstYearCoach.Items.IndexOf(lstYearCoach.Items.FindByValue(year_index.ToString()));
            //lstYearCoach1.SelectedIndex = lstYearCoach1.Items.IndexOf(lstYearCoach1.Items.FindByValue(year_index.ToString()));
            ////  lstYearparentstudents.SelectedIndex = lstYearparentstudents.Items.IndexOf(lstYearparentstudents.Items.FindByValue(year_indexlstYearparentstudents.ToString()));
            //lstYearparentstudents.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();


            if (((Convert.ToInt32(Session["RoleID"])) == 88) || (Convert.ToInt32(Session["RoleID"]) == 89))
            {
                if ((Convert.ToInt32(Session["RoleID"])) == 88)
                {
                    StrTableName = "CalSignUp";
                }
                else
                {
                    StrTableName = "Volunteer";
                }
                //if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select count (*) from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + "  and ProductId is not Null")) > 1 )
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select count (*) from " + StrTableName + " where Memberid=" + Session["LoginID"] + "  and ProductId is not Null")) > 1) //" and RoleId=" + Session["RoleId"] +
                //more than one 
                {
                    //DataSet ds   = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select ProductGroupID,ProductID from CalSignUp where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct ProductGroupID,ProductID from " + StrTableName + " where Memberid=" + Session["LoginID"] + " and ProductId is not Null "); //" and RoleId=" + Session["RoleId"] + 

                    int i;
                    String prd = "";//String.Empty;
                    String Prdgrp = ""; /////String.Empty
                    for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (prd.Length == 0)
                            prd = ds.Tables[0].Rows[i][1].ToString();
                        else
                            prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();


                        if (Prdgrp.Length == 0)
                            Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                        else
                            Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();

                    }
                    lblPrd.Text = prd;
                    lblPrdGrp.Text = Prdgrp;
                    GetProductGroup(lstProductGroup, ddlSemester.SelectedValue);// LoadProductGroup();
                    GetProductGroup(ddlpgCoach, ddlSemesterCoach.SelectedValue);
                    GetProductGroup(ddlpgCoach1, DropDownList3.SelectedValue);
                    GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);
                }
                else
                {
                    //only one
                    //DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct ProductGroupID,ProductID from " + StrTableName + " where Memberid=" + Session["LoginID"] + " and ProductId is not Null "); //" and RoleId=" + Session["RoleId"] + 

                    String prd = ""; //String.Empty
                    String Prdgrp = "";//String.Empty
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        prd = ds.Tables[0].Rows[0][1].ToString();
                        Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                        lblPrd.Text = prd;
                        lblPrdGrp.Text = Prdgrp;
                    }
                    GetProductGroup(lstProductGroup, ddlSemester.SelectedValue);//  LoadProductGroup();
                    GetProductGroup(ddlpgCoach, ddlSemesterCoach.SelectedValue);
                    GetProductGroup(ddlpgCoach1, DropDownList3.SelectedValue);
                    GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);
                }
                //ddlRegistype.SelectedValue = "3";
                //ddlRegistype.Enabled = false;
            }
            else
            {
                lblPrd.Text = "";
                lblPrdGrp.Text = "";
                ddlRegistype.Enabled = true;
                GetProductGroup(lstProductGroup, ddlSemester.SelectedValue); //LoadProductGroup();
                GetProductGroup(ddlpgCoach, ddlSemesterCoach.SelectedValue);
                GetProductGroup(ddlpgCoach1, DropDownList3.SelectedValue);
                GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);

            }
            //GetProductGroup(lstProductGroup);


            GetCoachName(ddlCoach);//Added on 22-03-2013
            GetCoachName(DDcoachparntstudent);
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "89")))
            {
                //if (Session["RoleId"].ToString() == "88")
                //{
                //    ddlCoach.SelectedIndex = ddlCoach.Items.IndexOf(ddlCoach.Items.FindByValue(Session["loginID"].ToString()));
                //    ddlCoach.Enabled = false;
                //}

                getQueryStrings();
            }
            else
                Response.Redirect("~/VolunteerFunctions.aspx");


        }

    }

    public void getQueryStrings()
    {

        string pgId = string.Empty;
        string pId = string.Empty;
        string coachID = string.Empty;
        string weekId = string.Empty;
        string paperType = string.Empty;
        string SNo = string.Empty;
        string setNum = string.Empty;
        string semester = string.Empty;
        string Level = string.Empty;
        try
        {

            if (Request.QueryString["pgId"] == null)
            {
            }
            else
            {
                pgId = Request.QueryString["pgId"].ToString() == "All" ? "0" : Request.QueryString["pgId"].ToString();
            }
            if (Request.QueryString["pId"] == null)
            {
            }
            else
            {
                pId = Request.QueryString["pId"].ToString() == "All" ? "0" : Request.QueryString["pId"].ToString();
            }


            if (Request.QueryString["Semester"] == null)
            {
            }
            else
            {
                semester = Request.QueryString["Semester"].ToString() == "All" ? "0" : Request.QueryString["Semester"].ToString();
            }
            if (Request.QueryString["CoachID"] == null)
            {
            }
            else
            {
                coachID = Request.QueryString["CoachID"].ToString() == "All" ? "0" : Request.QueryString["CoachID"].ToString();
            }
            if (Request.QueryString["SNo"] == null)
            {
            }
            else
            {
                SNo = Request.QueryString["SNo"].ToString() == "All" ? "0" : Request.QueryString["SNo"].ToString();
            }
            if (Request.QueryString["level"] == null)
            {
            }
            else
            {
                Level = Request.QueryString["level"].ToString() == "All" ? "0" : Request.QueryString["level"].ToString();
            }

            if (coachID != "" && semester != "")
            {
                rdParentandstudent.Checked = true;
                ShowHideContentsParentsAndStudents();
                GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);
                GetProduct(ddlSemester1.SelectedValue);
                GetCoachName(DDcoachparntstudent);
                ddlSemester1.SelectedValue = semester;
                DDproductGroup.SelectedValue = pgId;
                DDproduct.SelectedValue = pId;
                ddlSessionNo1.SelectedValue = SNo;
                DDcoachparntstudent.SelectedValue = coachID;
                loadLevel(ddlLevelParentStudent, pgId, pId);
                ddlLevelParentStudent.SelectedValue = Level;
            }

        }
        catch (Exception ex)
        {
        }

    }
    private void GetCoachName(DropDownList ddlObject)
    {
        lblerr1.Text = "";
        lblerr.Text = "";
        // DataSet dsCoachName = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (88) and v.MemberID = I.AutoMemberID order by I.FirstName");
        String wherecntn = "";
        String SQlCoach = " Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId,I.Firstname  from IndSpouse I";
        SQlCoach = SQlCoach + " Inner Join Volunteer V On V.MemberID = I.AutoMemberID";
        SQlCoach = SQlCoach + " Inner Join CalSignUp C On C.MemberID = I.AutoMemberID and C.Accepted='Y'  ";
        SQlCoach = SQlCoach + " Where V.RoleId in (88) ";

        wherecntn = wherecntn + "(";
        if ((rdParentandstudent.Checked == true))
        {
            foreach (ListItem i in lstYearparentstudents.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + i.Value + ",";
                }
            }

            wherecntn = wherecntn.TrimEnd(',') + ") ";

            SQlCoach = SQlCoach + " and C.EventYear in " + wherecntn;
            if (DDproductGroup.SelectedIndex != 0 || (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text != "All"))
            {
                SQlCoach = SQlCoach + " and C.ProductGroupId=" + DDproductGroup.SelectedValue;
            }
            else if (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text == "All")
            {
                SQlCoach = SQlCoach + " and C.ProductGroupId in (0";
                foreach (ListItem i in DDproductGroup.Items)
                {
                    SQlCoach = SQlCoach + "," + i.Value;
                }
                SQlCoach = SQlCoach + ") ";
            }

            if (DDproduct.SelectedIndex != 0 || (DDproduct.SelectedIndex == 0 && DDproduct.SelectedItem.Text != "All"))
            {
                SQlCoach = SQlCoach + " and C.ProductId=" + DDproduct.SelectedValue;
            }
            else if (DDproduct.SelectedIndex == 0 && DDproduct.SelectedItem.Text == "All")
            {
                SQlCoach = SQlCoach + " and C.ProductId in (0";
                foreach (ListItem i in DDproduct.Items)
                {
                    SQlCoach = SQlCoach + "," + i.Value;
                }
                SQlCoach = SQlCoach + ") ";
            }

            SQlCoach = SQlCoach + " and C.Semester='" + ddlSemester1.SelectedValue + "'";
            if (ddlSessionNo.SelectedIndex != 0)
            {
                SQlCoach = SQlCoach + " and C.SessionNo=" + ddlSessionNo1.SelectedValue;
            }

            if (ddlLevelParentStudent.SelectedValue != "0")
            {
                SQlCoach = SQlCoach + " and C.Level='" + ddlLevelParentStudent.SelectedValue + "'";
            }
        }
        else
        {
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + i.Value + ",";
                }
            }

            wherecntn = wherecntn.TrimEnd(',') + ") ";

            SQlCoach = SQlCoach + " and C.EventYear in " + wherecntn;
            if (lstProductGroup.SelectedIndex != 0 || (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text != "All"))
            {
                SQlCoach = SQlCoach + " and C.ProductGroupId=" + lstProductGroup.SelectedValue;
            }
            else if (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text == "All")
            {
                SQlCoach = SQlCoach + " and C.ProductGroupId in (0";
                foreach (ListItem i in lstProductGroup.Items)
                {
                    SQlCoach = SQlCoach + "," + i.Value;
                }
                SQlCoach = SQlCoach + ") ";
            }

            if (lstProduct.SelectedIndex != 0 || (lstProduct.SelectedIndex == 0 && lstProduct.SelectedItem.Text != "All"))
            {
                SQlCoach = SQlCoach + " and C.ProductId=" + lstProduct.SelectedValue;
            }
            else if (lstProduct.SelectedIndex == 0 && lstProduct.SelectedItem.Text == "All")
            {
                SQlCoach = SQlCoach + " and C.ProductId in (0";
                foreach (ListItem i in lstProduct.Items)
                {
                    SQlCoach = SQlCoach + "," + i.Value;
                }
                SQlCoach = SQlCoach + ") ";
            }

            SQlCoach = SQlCoach + " and C.Semester='" + ddlSemester.SelectedValue + "'";
            if (ddlSessionNo.SelectedIndex != 0)
            {
                SQlCoach = SQlCoach + " and C.SessionNo=" + ddlSessionNo.SelectedValue;
            }
            if (ddlLevelparent.SelectedValue != "0")
            {
                SQlCoach = SQlCoach + " and C.Level='" + ddlLevelparent.SelectedValue + "'";
            }
        }
        SQlCoach = SQlCoach + " order by I.FirstName";


        try
        {
            DataSet dsCoachName = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQlCoach);

            if (dsCoachName.Tables[0].Rows.Count > 0)
            {
                ddlObject.DataSource = dsCoachName;
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "AutoMemberId";
                ddlObject.DataBind();

                if (Session["RoleId"].ToString() == "88")
                {
                    if (ddlObject.Items.Contains(ddlObject.Items.FindByValue(Session["loginID"].ToString())))
                    {
                        ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByValue(Session["loginID"].ToString()));
                        ddlObject.Enabled = false;
                    }
                    else
                    {
                        ddlObject.Items.Clear();
                        lblerr.Text = "Coach not matching with the selections done";
                    }

                    // ddlRegistype.SelectedValue = "3";
                    // ddlRegistype.Enabled = false;
                }
                else
                {
                    ddlObject.Items.Insert(0, new ListItem("All", "0"));
                    ddlObject.SelectedIndex = 0;
                    ddlObject.Enabled = true;

                    ddlRegistype.Enabled = true;
                }
            }
            else
            {
                if ((rdParentandstudent.Checked == true))
                {
                    lblerr1.Text = "No Coach Found";
                    DDcoachparntstudent.Items.Clear();
                }
                else
                {
                    lblerr.Text = "No Coach Found";
                    ddlCoach.Items.Clear();
                }

            }

        }
        catch (Exception ex)
        {
            //Response.Write(SQlCoach);
            //Response.Write(ex.ToString());
        }
    }
    private void GetProductGroup(DropDownList ddlObject, string Semester)
    {

        String year = "(0";

        if (rdParents.Checked == true)
        {
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    year = year + "," + i.Value;
                }
            }

            year = year + ") ";
        }
        else if (rdParentandstudent.Checked == true)
        {
            foreach (ListItem i in lstYearparentstudents.Items)
            {
                if (i.Selected)
                {
                    year = year + "," + i.Value;
                }
            }
            year = year + ") ";
        }

        else if (rdCoaches.Checked == true)
        {
            foreach (ListItem i in lstYearCoach.Items)
            {
                if (i.Selected)
                {
                    year = year + "," + i.Value;
                }
            }
            year = year + ") ";
        }
        else if (rdcalsignup.Checked == true)
        {
            foreach (ListItem i in lstYearCoach1.Items)
            {
                if (i.Selected)
                {
                    year = year + "," + i.Value;
                }
            }
            year = year + ") ";
        }

        String StrPrdGrp = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN CalSignup EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where P.EventId=13 and EF.Semester='" + Semester + "' and EF.EventYear in " + year;


        // Response.Write(StrPrdGrp);
        if (lblPrdGrp.Text != "")
        {
            StrPrdGrp = StrPrdGrp + " and P.ProductGroupId in(" + lblPrdGrp.Text + ")";
        }
        StrPrdGrp = StrPrdGrp + " order by P.ProductGroupID";

        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrPrdGrp); //EF.EventYear>=YEAR(GETDATE()) AND
        ddlObject.DataSource = dsProductGroup;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "ProductGroupID";
        ddlObject.DataBind();

        if (ddlObject.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
            lblerr1.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
            lblerrcoach.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";

        }
        else if (ddlObject.Items.Count > 1)// && lblPrdGrp.Text == "")
        {
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
            GetProduct(Semester);
            lblerr.Text = "";
            lblerr1.Text = "";
        }
        else if (ddlObject.Items.Count == 1)
        {
            ddlObject.Enabled = false;
            ddlObject.SelectedIndex = 0;
            GetProduct(Semester);
            lblerr.Text = "";
            lblerr1.Text = "";
        }
    }
    private void GetProduct(string Semester)
    {
        try
        {
            //DropDownList ddlProductGroup;
            //DropDownList ddlProduct;
            //DropDownList ddlLevel;

            string prodGrp = "";
            String year = string.Empty;
            if (rdParents.Checked == true)
            {
                //ddlProductGroup = lstProductGroup;
                //ddlProduct = lstProduct;
                //ddlLevel = ddlLevelparent;
                if ((lstProductGroup.SelectedIndex != 0) || (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text != "All"))
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in lstProductGroup.Items)
                    {
                        if (i.Selected)
                        {
                            prodGrp = prodGrp + "," + i.Value;
                        }
                    }
                    prodGrp = prodGrp + ") ";
                }
                else if (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text == "All")
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in lstProductGroup.Items)
                    {
                        prodGrp = prodGrp + "," + i.Value;
                    }
                    prodGrp = prodGrp + ") ";
                }

                year = "(0";
                foreach (ListItem i in lstYear.Items)
                {
                    if (i.Selected)
                    {
                        year = year + "," + i.Value;
                    }
                }
                year = year + ") ";
            }

            else if (rdParentandstudent.Checked == true)
            {
                //ddlProductGroup = DDproductGroup;
                //ddlProduct = DDproduct;
                //ddlLevel = ddlLevelParentStudent;
                if ((DDproductGroup.SelectedIndex != 0) || (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text != "All"))
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in DDproductGroup.Items)
                    {
                        if (i.Selected)
                        {
                            prodGrp = prodGrp + "," + i.Value;
                        }
                    }
                    prodGrp = prodGrp + ") ";
                }
                else if (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text == "All")
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in DDproductGroup.Items)
                    {
                        prodGrp = prodGrp + "," + i.Value;
                    }
                    prodGrp = prodGrp + ") ";
                }

                year = "(0";
                foreach (ListItem i in lstYearparentstudents.Items)
                {
                    if (i.Selected)
                    {
                        year = year + "," + i.Value;
                    }
                }
                year = year + ") ";
            }


            else if (rdCoaches.Checked == true)
            {
                //ddlProductGroup = ddlpgCoach;
                //ddlProduct = ddlprCoach;
                //ddlLevel = ddlLevelCoach;

                if ((ddlpgCoach.SelectedIndex != 0) || (ddlpgCoach.SelectedIndex == 0 && ddlpgCoach.SelectedItem.Text != "All"))
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in ddlpgCoach.Items)
                    {
                        if (i.Selected)
                        {
                            prodGrp = prodGrp + "," + i.Value;
                        }
                    }
                    prodGrp = prodGrp + ") ";
                }
                else if (ddlpgCoach.SelectedIndex == 0 && ddlpgCoach.SelectedItem.Text == "All")
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in ddlpgCoach.Items)
                    {
                        prodGrp = prodGrp + "," + i.Value;
                    }
                    prodGrp = prodGrp + ") ";
                }
                year = "(0";

                foreach (ListItem i in lstYearCoach.Items)
                {
                    if (i.Selected)
                    {
                        year = year + "," + i.Value;
                    }
                }
                year = year + ") ";
            }


            else if (rdcalsignup.Checked == true)
            {
                //ddlProductGroup = ddlpgCoach1;
                //ddlProduct = ddlprCoach1;
                //ddlLevel = DDlSignupCoacheslevel;

                if ((ddlpgCoach1.SelectedIndex != 0) || (ddlpgCoach1.SelectedIndex == 0 && ddlpgCoach1.SelectedItem.Text != "All"))
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in ddlpgCoach1.Items)
                    {
                        if (i.Selected)
                        {
                            prodGrp = prodGrp + "," + i.Value;
                        }
                    }
                    prodGrp = prodGrp + ") ";
                }
                else if (ddlpgCoach1.SelectedIndex == 0 && ddlpgCoach1.SelectedItem.Text == "All")
                {
                    prodGrp = " and P.ProductGroupID in (0";
                    foreach (ListItem i in ddlpgCoach1.Items)
                    {
                        prodGrp = prodGrp + "," + i.Value;
                    }
                    prodGrp = prodGrp + ") ";
                }
                year = "(0";

                foreach (ListItem i in lstYearCoach1.Items)
                {
                    if (i.Selected)
                    {
                        year = year + "," + i.Value;
                    }
                }
                year = year + ") ";
            }



            String StrPrd = "Select Distinct P.ProductID, CASE WHEN P.CoachName IS NULL THEN P.Name ELSE P.CoachName END as Name from Product P INNER JOIN CalSignup EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where  P.EventID=13 " + prodGrp + " and EF.Semester='" + Semester + "' and EF.EventYear in " + year;
            if (lblPrd.Text != "")
            {
                StrPrd = StrPrd + " and P.ProductID in(" + lblPrd.Text + ")";
            }
            StrPrd = StrPrd + " order by P.ProductID ";
            //Response.Write(StrPrd);
            DataSet dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrPrd); //EF.EventYear>=YEAR(GETDATE()) AND
            if (rdParents.Checked == true)
            {
                lstProduct.DataSource = dsProduct;
                lstProduct.DataTextField = "Name";
                lstProduct.DataValueField = "ProductID";
                lstProduct.DataBind();
                if (lstProduct.Items.Count < 1)
                {
                    lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
                }

                else if (lstProduct.Items.Count > 1)//&& lblPrd.Text == "")
                {
                    lstProduct.Items.Insert(0, new ListItem("All", "0"));
                    lstProduct.SelectedIndex = 0;
                    if (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text == "All")
                        lstProduct.Enabled = false;
                    else
                        lstProduct.Enabled = true;
                    lblerr.Text = "";
                    // if (lstProductGroup.Items[0].Selected == true)
                    //  lstProduct.Enabled = false;
                    loadLevel(ddlLevelparent, lstProductGroup.SelectedValue, lstProduct.SelectedValue);
                }
                else if (lstProduct.Items.Count == 1)
                {
                    lstProduct.SelectedIndex = 0;
                    lstProduct.Enabled = false;
                    lblerr.Text = "";
                    loadLevel(ddlLevelparent, lstProductGroup.SelectedValue, lstProduct.SelectedValue);
                    GetCoachName(ddlCoach);
                }
            }
            else if (rdParentandstudent.Checked == true)
            {
                DDproduct.DataSource = dsProduct;
                DDproduct.DataTextField = "Name";
                DDproduct.DataValueField = "ProductID";
                DDproduct.DataBind();
                if (DDproduct.Items.Count < 1)
                {
                    lblerr1.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
                }

                else if (DDproduct.Items.Count > 1)//&& lblPrd.Text == "")
                {
                    DDproduct.Items.Insert(0, new ListItem("All", "0"));
                    DDproduct.SelectedIndex = 0;
                    if (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text == "All")
                        DDproduct.Enabled = false;
                    else
                        DDproduct.Enabled = true;
                    lblerr1.Text = "";
                    // if (lstProductGroup.Items[0].Selected == true)
                    //  lstProduct.Enabled = false;
                    loadLevel(ddlLevelParentStudent, DDproductGroup.SelectedValue, DDproduct.SelectedValue);
                }
                else if (DDproduct.Items.Count == 1)
                {
                    DDproduct.SelectedIndex = 0;
                    DDproduct.Enabled = false;
                    lblerr1.Text = "";
                    loadLevel(ddlLevelParentStudent, DDproductGroup.SelectedValue, DDproduct.SelectedValue);
                    GetCoachName(DDcoachparntstudent);
                }
            }
            else if (rdCoaches.Checked == true)
            {

                ddlprCoach.DataSource = dsProduct;
                ddlprCoach.DataTextField = "Name";
                ddlprCoach.DataValueField = "ProductID";
                ddlprCoach.DataBind();
                if (ddlprCoach.Items.Count < 1)
                {
                    lblerrcoach.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
                }

                else if (ddlprCoach.Items.Count > 1)//&& lblPrd.Text == "")
                {
                    ddlprCoach.Items.Insert(0, new ListItem("All", "0"));
                    ddlprCoach.SelectedIndex = 0;

                    if (ddlpgCoach.SelectedIndex == 0 && ddlpgCoach.SelectedItem.Text == "All")
                        ddlprCoach.Enabled = false;
                    else
                        ddlprCoach.Enabled = true;
                    lblerrcoach.Text = "";
                    // if (lstProductGroup.Items[0].Selected == true)
                    //  lstProduct.Enabled = false;
                    loadLevel(ddlLevelCoach, ddlpgCoach.SelectedValue, ddlprCoach.SelectedValue);
                }
                else if (ddlprCoach.Items.Count == 1)
                {
                    ddlprCoach.SelectedIndex = 0;
                    ddlprCoach.Enabled = false;
                    lblerrcoach.Text = "";
                    loadLevel(ddlLevelCoach, ddlpgCoach.SelectedValue, ddlprCoach.SelectedValue);
                }
            }


            else if (rdcalsignup.Checked == true)
            {

                ddlprCoach1.DataSource = dsProduct;
                ddlprCoach1.DataTextField = "Name";
                ddlprCoach1.DataValueField = "ProductID";
                ddlprCoach1.DataBind();
                if (ddlprCoach1.Items.Count < 1)
                {
                    lblerrcoach.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
                }

                else if (ddlprCoach1.Items.Count > 1)//&& lblPrd.Text == "")
                {
                    ddlprCoach1.Items.Insert(0, new ListItem("All", "0"));
                    ddlprCoach1.SelectedIndex = 0;

                    if (ddlpgCoach1.SelectedIndex == 0 && ddlpgCoach1.SelectedItem.Text == "All")
                        ddlprCoach1.Enabled = false;
                    else
                        ddlprCoach1.Enabled = true;
                    lblerrcoach.Text = "";
                    // if (lstProductGroup.Items[0].Selected == true)
                    //  lstProduct.Enabled = false;
                    loadLevel(DDlSignupCoacheslevel, ddlpgCoach1.SelectedValue, ddlprCoach1.SelectedValue);
                }
                else if (ddlprCoach1.Items.Count == 1)
                {
                    ddlprCoach1.SelectedIndex = 0;
                    ddlprCoach1.Enabled = false;
                    lblerrcoach.Text = "";
                    loadLevel(DDlSignupCoacheslevel, ddlpgCoach1.SelectedValue, ddlprCoach1.SelectedValue);
                }
            }


        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    public void loadLevel(DropDownList ddlLevelObject, string productGroupid, string ProductId)
    {
        string cmdText = "";
        ddlLevelObject.Items.Clear();
        if (productGroupid != "0" && ProductId != "0")
        {
            cmdText = " select LevelCode from ProdLevel where EventYear=" + lstYear.SelectedValue + "";

            if (productGroupid != "0" && productGroupid != "")
            {
                cmdText += " and ProductGroupID=" + productGroupid + "";
            }

            if (ProductId != "0" && ProductId != "")
            {
                cmdText += " and ProductID=" + ProductId + "";
            }


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                ddlLevelObject.DataTextField = "LevelCode";
                ddlLevelObject.DataValueField = "LevelCode";
                ddlLevelObject.DataSource = ds;
                ddlLevelObject.DataBind();
                ddlLevelObject.Items.Insert(0, new ListItem("All", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlLevelObject.Enabled = true;
                }
                else
                {
                    ddlLevelObject.SelectedValue = ds.Tables[0].Rows[0]["LevelCode"].ToString().Trim();
                    ddlLevelObject.Enabled = false;
                }
            }
        }
        else
        {
            ddlLevelObject.Items.Insert(0, new ListItem("All", "0"));
            ddlLevelObject.Enabled = false;
        }
    }
    protected void btnSendEMail_Click(object sender, EventArgs e)
    {
        try
        {
            string wherecntn = "";
            lblerr.Text = "";
            if (lstProductGroup.Items.Count <= 0)
                lblerr.Text = " No Product Groups present";
            else if (lstProduct.Items.Count <= 0)
                lblerr.Text = " No Products present";
            else if (ddlCoach.Items.Count <= 0)
                lblerr.Text = " No Coaches present for the selection done";
            if (lblerr.Text != "")
                return;
            if (ddlCoach.SelectedItem.Text != "All")
                wherecntn = " CMemberID=" + ddlCoach.SelectedValue;
            else
            {
                wherecntn = " CMemberID in(0";
                for (int i = 0; i < ddlCoach.Items.Count; i++)
                {
                    wherecntn = wherecntn + "," + ddlCoach.Items[i].Value;
                }
                wherecntn = wherecntn + ")";

            }

            if ((lstProductGroup.Items[0].Selected != true) || (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text != "All"))
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0," + lstProductGroup.SelectedValue + ")";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0," + lstProductGroup.SelectedValue + ")";

                if (lstProduct.Items[0].Selected == true && lstProduct.SelectedItem.Text == "All")
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in lstProduct.Items)
                    {
                        wherecntn = wherecntn + "," + i.Value;
                    }
                    wherecntn = wherecntn + ") ";
                }
                else
                {

                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in lstProduct.Items)
                    {
                        if (i.Selected)
                        {
                            wherecntn = wherecntn + "," + i.Value;
                        }
                    }
                    wherecntn = wherecntn + ") ";
                }
            }
            else if (lstProductGroup.Items[0].Selected == true && lstProductGroup.SelectedItem.Text == "All")//&& (lblPrdGrp.Text != "")
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0";
                foreach (ListItem i in lstProductGroup.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";

                if (wherecntn == "" && lblPrd.Text != "")
                    wherecntn = " ProductID in (" + lblPrd.Text + ")";
                else if (lblPrd.Text != "")
                    wherecntn = wherecntn + " and ProductID in (" + lblPrd.Text + ")";
            }


            if (wherecntn == "")
                wherecntn = " EventYear in (0";
            else
                wherecntn = wherecntn + " AND EventYear in (0";
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;

                }
            }
            wherecntn = wherecntn + ") ";

            if (ddlRegistype.SelectedValue == "3")
                wherecntn = wherecntn + " And PaymentReference is not null";
            else if (ddlRegistype.SelectedValue == "2")
                wherecntn = wherecntn + " And PaymentReference is null And Approved='N'";
            else if (ddlRegistype.SelectedValue == "1")
                wherecntn = wherecntn + " And Approved='Y' ";

            wherecntn = wherecntn + " And Semester='" + ddlSemester.SelectedValue + "'";

            if (ddlSessionNo.SelectedValue != "0")
                wherecntn = wherecntn + " And SessionNo=" + ddlSessionNo.SelectedValue;
            if (ddlLevelparent.SelectedValue != "0")
            {
                wherecntn = wherecntn + " And Level='" + ddlLevelparent.SelectedValue + "'";
            }

            string StrSQL = "select distinct EmailID as EmailID from(SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select PMemberid from CoachReg where " + wherecntn + "))) or (donortype = 'SPOUSE'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select pMemberid from CoachReg  where " + wherecntn + ")) group by Email)as Email where (EmailID is not null and len(EmailID)<>'0' )";
            // Response.Write(StrSQL);
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    sbEmailList.Append(dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString());
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }
                Session["emaillist"] = sbEmailList.ToString();

                Session["sentemaillistenable"] = "No";
                Session["EmailSource"] = "EmailCoach";
                Response.Redirect("emaillist.aspx");
            }
            else
                lblerr.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void lstYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(lstProductGroup, ddlSemester.SelectedValue);
        GetProduct(ddlSemester.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void lstProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct(ddlSemester.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void lstProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel(ddlLevelparent, lstProductGroup.SelectedValue, lstProduct.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {

        GetProductGroup(lstProductGroup, ddlSemester.SelectedValue);
        GetCoachName(ddlCoach);//Added on 22-03-2013
        GetCoachName(DDcoachparntstudent);
    }
    protected void ddlSessionNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCoachName(ddlCoach);
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        PnlSheduled.Visible = false;
        Panel1.Visible = true;
        Panel2.Visible = false;
        rdCoaches.Checked = false;
        Panel3.Visible = false;
        rdParentandstudent.Checked = false;
        RbtnVolunteerSignUp.Checked = false;
        RadioButton1.Checked = false;
        pnlVolunteerSignUp.Visible = false;
    }
    protected void rdCoaches_CheckedChanged(object sender, EventArgs e)
    {
        PnlSheduled.Visible = false;
        rdcalsignup.Checked = false;
        rdParents.Checked = false;
        Panel2.Visible = true;
        Panel1.Visible = false;
        Panel3.Visible = false;
        rdParentandstudent.Checked = false;
        RadioButton1.Checked = false;
        Pnlcoach.Visible = false;
        RbtnVolunteerSignUp.Checked = false;
        pnlVolunteerSignUp.Visible = false;
        GetProduct(ddlSemesterCoach.SelectedValue);
    }
    protected void lstYearCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(ddlpgCoach, ddlSemesterCoach.SelectedValue);
        GetProduct(ddlSemesterCoach.SelectedValue);
    }
    protected void ddlpgCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct(ddlSemesterCoach.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void btnSendEmailCoaches_Click(object sender, EventArgs e)
    {
        try
        {
            string StrSQL;
            string wherecntn = "";
            string coachwherecntn = "";

            lblerrcoach.Text = "";

            if (ddlpgCoach.Items.Count <= 0)
                lblerrcoach.Text = " No Product Groups present";
            else if (ddlpgCoach.Items.Count <= 0)
                lblerrcoach.Text = " No Products present";
            else if (ddlpgCoach.Items.Count <= 0)
                lblerrcoach.Text = " No Coaches present for the selection done";
            if (lblerrcoach.Text != "")
                return;

            if ((ddlpgCoach.Items[0].Selected != true) || (ddlpgCoach.SelectedIndex == 0 && ddlpgCoach.SelectedItem.Text != "All"))
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0," + ddlpgCoach.SelectedValue + ")";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0," + ddlpgCoach.SelectedValue + ")";

                if (ddlprCoach.Items[0].Selected == true && ddlprCoach.SelectedItem.Text == "All")
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in ddlprCoach.Items)
                    {
                        wherecntn = wherecntn + "," + i.Value;
                    }
                    wherecntn = wherecntn + ") ";
                }
                else
                {

                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in ddlprCoach.Items)
                    {
                        if (i.Selected)
                        {
                            wherecntn = wherecntn + "," + i.Value;
                        }
                    }
                    wherecntn = wherecntn + ") ";
                }
            }

            else if (ddlpgCoach.Items[0].Selected == true && ddlpgCoach.SelectedItem.Text == "All")//&& (lblPrdGrp.Text != "")
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0";
                foreach (ListItem i in ddlpgCoach.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";

                if (wherecntn == "" && lblPrd.Text != "")
                    wherecntn = " ProductID in (" + lblPrd.Text + ")";
                else if (lblPrd.Text != "")
                    wherecntn = wherecntn + " and ProductID in (" + lblPrd.Text + ")";
            }

            //assign condition to coach condition - to get mail list
            coachwherecntn = wherecntn;

            if (wherecntn == "")
                wherecntn = " EventYear in (0";
            else
                wherecntn = wherecntn + " AND EventYear in (0";

            foreach (ListItem i in lstYearCoach.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;

                }
            }
            wherecntn = wherecntn + "  )";

            wherecntn = wherecntn + " And Semester='" + ddlSemesterCoach.SelectedValue + "'";
            if (ddlsessionCoach.SelectedValue != "0")
                wherecntn = wherecntn + " And SessionNo=" + ddlsessionCoach.SelectedValue;

            if (ddlLevelCoach.SelectedValue != "0")
            {
                wherecntn = wherecntn + " And Level='" + ddlLevelCoach.SelectedValue + "'";
            }

            // added on 21-10-14 to get email list of roleid=88 with teamlead='y' depending upon product selected
            string coachEmailList = "";
            string cmdText = "declare @eMail varchar(max) select @eMail=COALESCE(@eMail,'')+Email+',' from IndSpouse I inner join volunteer V on I.AutomemberId=V.MemberId where V.RoleId=88 and V.TeamLead='y' and  " + coachwherecntn + " select @eMail";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.ToString() != string.Empty) coachEmailList = ds.Tables[0].Rows[0][0].ToString();

            if (Checkboxcoach.Checked == true)
            {
                StrSQL = "SELECT Email as EmailID From IndSpouse where exists (Select * from CalSignUp CR where CR.MemberID=automemberid and   " + wherecntn + "  and Accepted='Y' ) and not exists (select * from CalSignUp C where C.MemberID=AutoMemberID and Accepted='Y' and EventYear=" + DateTime.Now.Year + ") ";
            }
            else
            {
                StrSQL = " SELECT Email as EmailID From IndSpouse where exists (Select * from CalSignUp CR where CR.MemberID=automemberid and   " + wherecntn + "  and Accepted='Y') ";
            }
            // Response.Write(StrSQL);

            string contactEmail;
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    contactEmail = dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString();

                    // avoid duplicate list in emaillist when concate sbemaillist with coachmail list
                    if (coachEmailList != string.Empty)
                    {
                        int inx = coachEmailList.ToString().IndexOf(contactEmail);
                        if (inx != -1)
                        {
                            if (coachEmailList.Contains(contactEmail + ",")) coachEmailList = coachEmailList.Remove(inx, contactEmail.Length + 1);
                            else if (coachEmailList.Contains(contactEmail)) coachEmailList = coachEmailList.Remove(inx, contactEmail.Length);
                        }
                        if (coachEmailList.StartsWith(",")) coachEmailList = coachEmailList.Remove(0, 1);
                        if (coachEmailList.EndsWith(",")) coachEmailList = coachEmailList.Remove(coachEmailList.LastIndexOf(","), 1);
                    }

                    sbEmailList.Append(contactEmail);
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }

                //Response.Write("<br>Coach =" + coachEmailList + "<br> Mail List =" );

                if (coachEmailList.Length > 0 && sbEmailList.Length > 0) sbEmailList.Append("," + coachEmailList);

                Session["emaillist"] = sbEmailList.ToString();
                Session["sentemaillistenable"] = "No";
                Session["EmailSource"] = "EmailCoach";
                Response.Redirect("emaillist.aspx");
            }
            else
                lblerrcoach.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    public void ShowHideContentsParentsAndStudents()
    {
        PnlSheduled.Visible = false;
        Panel3.Visible = true;
        Panel2.Visible = false;
        Pnlcoach.Visible = false;
        PnlSheduled.Visible = false;
        Panel1.Visible = false;
        rdParents.Checked = false;
        rdCoaches.Checked = false;
        rdParentandstudent.Checked = true;
        RadioButton1.Checked = false;
        RbtnVolunteerSignUp.Checked = false;
        pnlVolunteerSignUp.Visible = false;
        GetProduct(ddlSemester1.SelectedValue);
    }
    protected void rdParentandstudent_CheckedChanged(object sender, EventArgs e)
    {
        ShowHideContentsParentsAndStudents();
    }
    protected void lstYearparentstudents_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);
        GetProduct(ddlSemester1.SelectedValue);
        GetCoachName(DDcoachparntstudent);
    }
    protected void ParentStudEmail_Click(object sender, EventArgs e)
    {
        try
        {
            string wherecntn = "";
            lblerr1.Text = "";
            if (DDproductGroup.Items.Count <= 0)
                lblerr1.Text = " No Product Groups present";
            else if (DDproduct.Items.Count <= 0)
                lblerr1.Text = " No Products present";
            else if (DDcoachparntstudent.Items.Count <= 0)
                lblerr1.Text = " No Coaches present for the selection done";
            if (lblerr1.Text != "")
                return;
            if (DDcoachparntstudent.SelectedItem.Text != "All")
                wherecntn = " CMemberID=" + DDcoachparntstudent.SelectedValue;
            else
            {
                wherecntn = " CMemberID in(0";
                for (int i = 0; i < DDcoachparntstudent.Items.Count; i++)
                {
                    wherecntn = wherecntn + "," + DDcoachparntstudent.Items[i].Value;
                }
                wherecntn = wherecntn + ")";

            }

            if ((DDproductGroup.Items[0].Selected != true) || (DDproductGroup.SelectedIndex == 0 && DDproductGroup.SelectedItem.Text != "All"))
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0," + DDproductGroup.SelectedValue + ")";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0," + DDproductGroup.SelectedValue + ")";

                if (DDproduct.Items[0].Selected == true && DDproduct.SelectedItem.Text == "All")
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in DDproduct.Items)
                    {
                        wherecntn = wherecntn + "," + i.Value;
                    }
                    wherecntn = wherecntn + ") ";
                }
                else
                {

                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in DDproduct.Items)
                    {
                        if (i.Selected)
                        {
                            wherecntn = wherecntn + "," + i.Value;
                        }
                    }
                    wherecntn = wherecntn + ") ";
                }
            }
            else if (DDproductGroup.Items[0].Selected == true && DDproductGroup.SelectedItem.Text == "All")//&& (lblPrdGrp.Text != "")
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0";
                foreach (ListItem i in DDproductGroup.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";

                if (wherecntn == "" && lblPrd.Text != "")
                    wherecntn = " ProductID in (" + lblPrd.Text + ")";
                else if (lblPrd.Text != "")
                    wherecntn = wherecntn + " and ProductID in (" + lblPrd.Text + ")";
            }


            if (wherecntn == "")
                wherecntn = " EventYear in (0";
            else
                wherecntn = wherecntn + " AND EventYear in (0";
            foreach (ListItem i in lstYearparentstudents.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;

                }
            }
            wherecntn = wherecntn + ") ";

            if (ddlRegistype1.SelectedValue == "3")
                wherecntn = wherecntn + " And PaymentReference is not null";
            else if (ddlRegistype1.SelectedValue == "2")
                wherecntn = wherecntn + " And PaymentReference is null And Approved='N'";
            else if (ddlRegistype1.SelectedValue == "1")
                wherecntn = wherecntn + " And Approved='Y' ";

            wherecntn = wherecntn + " And Semester='" + ddlSemester1.SelectedValue + "'";

            if (ddlSessionNo1.SelectedValue != "0")
                wherecntn = wherecntn + " And SessionNo=" + ddlSessionNo1.SelectedValue;
            if (ddlLevelParentStudent.SelectedValue != "0")
            {
                wherecntn = wherecntn + " And Level='" + ddlLevelParentStudent.SelectedValue + "'";
            }

            string StrSQL = "select distinct EmailID as EmailID from(SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select PMemberid from CoachReg where "
                + wherecntn + "))) or (donortype = 'SPOUSE'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select pMemberid from CoachReg  where "
                + wherecntn + "))   union all  SELECT distinct(ch.Email) as EmailID From   coachreg C left join  child Ch  on C.ChildNumber =ch.ChildNumber where ch.email<>'' and  " + wherecntn + " group by Email  union all  SELECT distinct(IP.Email) as EmailID From   coachreg C left join  Indspouse IP  on C.AdultId =IP.AutoMemberId where IP.email<>'' and  " + wherecntn + " group by Email )  as Email where (EmailID is not null and len(EmailID)<>'0' )";
            // Response.Write(StrSQL);
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    sbEmailList.Append(dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString());
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }
                Session["emaillist"] = sbEmailList.ToString();
                Session["sentemaillistenable"] = "No";
                //Get Parent Student Count
                string StrSQLParent = "declare @Pcnt int,@Childcnt int";

                StrSQLParent = StrSQLParent + " SELECT @Pcnt=Count(distinct(Email))  From IndSpouse where email<>'' and ((donortype = 'IND'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select PMemberid from CoachReg where "
                + wherecntn + "))) or (donortype = 'SPOUSE'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select pMemberid from CoachReg  where " + wherecntn + "))";
                StrSQLParent = StrSQLParent + " SELECT @Childcnt=Count(distinct(ch.Email)) From Coachreg C left join  child Ch  on C.ChildNumber =ch.ChildNumber where ch.email<>'' and  (ch.email is not null and len(ch.email)<>'0' ) and  " + wherecntn; ;

                StrSQLParent = StrSQLParent + " SELECT @Pcnt ParentMailCnt,@Childcnt ChildMailCnt";
                DataSet dsEmailCnt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQLParent);

                if (dsEmailCnt.Tables[0].Rows.Count > 0)
                {
                    Session["ParentChildMailCnt"] = dsEmailCnt.Tables[0].Rows[0][0].ToString() + ":" + dsEmailCnt.Tables[0].Rows[0][1].ToString();
                }
                Session["EmailSource"] = "EmailCoach";

                Response.Redirect("emaillist.aspx");
            }
            else
                lblerr1.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void ddlSemester1_SelectedIndexChanged(object sender, EventArgs e)
    {

        GetProductGroup(DDproductGroup, ddlSemester1.SelectedValue);
        GetCoachName(DDcoachparntstudent);
    }
    protected void ddlSessionNo1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCoachName(DDcoachparntstudent);
    }
    protected void DDproduct_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevel(ddlLevelParentStudent, DDproductGroup.SelectedValue, DDproduct.SelectedValue);
        GetCoachName(DDcoachparntstudent);
    }
    protected void DDproductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct(ddlSemester1.SelectedValue);
        GetCoachName(DDcoachparntstudent);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string StrSQL;
            if (CheckEmail.Checked == true)
            {
                int CurrentYear = DateTime.Now.Year;
                StrSQL = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email as EmailID, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from CalSignUp where EventYear<" + CurrentYear + " and B.AutoMemberID=MemberID and accepted='Y') and not exists (select * from CalSignUp where EventYear=" + CurrentYear + " and b.AutoMemberID=MemberID ) order by LastName, FirstName";

                //StrSQL = "Select distinct  I.email as EmailID,V.memberid from Volunteer V left join indspouse I on I.automemberID=V.memberID where V.RoleID in (88, 89) and V.EventID=13 and  not exists(select * from CalSignUp C where C.MemberID=AutoMemberID and EventYear=" + DateTime.Now.Year + ") and I.email is not null order by I.email";
            }
            else
            {
                StrSQL = "Select distinct  I.email as EmailID,V.memberid from Volunteer V left join indspouse I on I.automemberID=V.memberID where V.RoleID in (88, 89) and V.EventID=13 and I.email is not null order by I.email";
            }
            //Response.Write(StrSQL);
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    sbEmailList.Append(dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString());
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }
                Session["emaillist"] = sbEmailList.ToString();
                Session["sentemaillistenable"] = "No";
                Session["EmailSource"] = "EmailCoach";

                Response.Redirect("emaillist.aspx");
            }
            else
                lblerrcoach.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    protected void RadioButton1_CheckedChanged1(object sender, EventArgs e)
    {
        PnlSheduled.Visible = true;
        Panel1.Visible = false;
        Panel3.Visible = false;
        rdParentandstudent.Checked = false;
        rdCoaches.Checked = false;
        rdParents.Checked = false;
        Pnlcoach.Visible = false;
        rdcalsignup.Checked = false;
        Panel2.Visible = false;
        RbtnVolunteerSignUp.Checked = false;
        pnlVolunteerSignUp.Visible = false;
    }
    protected void RbtnVolunteerSignUp_CheckedChanged(object sender, EventArgs e)
    {
        PnlSheduled.Visible = false;
        Panel1.Visible = false;
        Panel3.Visible = false;
        rdParentandstudent.Checked = false;
        rdCoaches.Checked = false;
        rdParents.Checked = false;
        Pnlcoach.Visible = false;
        rdcalsignup.Checked = false;
        Panel2.Visible = false;
        RadioButton1.Checked = false;
        pnlVolunteerSignUp.Visible = true;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            string wherecntn = "";
            string coachwherecntn = "";
            lblerrcoach1.Text = "";

            if (ddlpgCoach1.Items.Count <= 0)
                lblerrcoach1.Text = " No Product Groups present";
            else if (ddlpgCoach1.Items.Count <= 0)
                lblerrcoach1.Text = " No Products present";
            else if (ddlpgCoach1.Items.Count <= 0)
                lblerrcoach1.Text = " No Coaches present for the selection done";
            if (lblerrcoach1.Text != "")
                return;

            if ((ddlpgCoach1.Items[0].Selected != true) || (ddlpgCoach1.SelectedIndex == 0 && ddlpgCoach1.SelectedItem.Text != "All"))
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0," + ddlpgCoach1.SelectedValue + ")";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0," + ddlpgCoach1.SelectedValue + ")";

                if (ddlprCoach1.Items[0].Selected == true && ddlprCoach1.SelectedItem.Text == "All")
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in ddlprCoach1.Items)
                    {
                        wherecntn = wherecntn + "," + i.Value;
                    }
                    wherecntn = wherecntn + ") ";
                }
                else
                {

                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in ddlprCoach1.Items)
                    {
                        if (i.Selected)
                        {
                            wherecntn = wherecntn + "," + i.Value;
                        }
                    }
                    wherecntn = wherecntn + ") ";
                }
            }

            else if (ddlpgCoach1.Items[0].Selected == true && ddlpgCoach1.SelectedItem.Text == "All")//&& (lblPrdGrp.Text != "")
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0";
                foreach (ListItem i in ddlpgCoach1.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";

                if (wherecntn == "" && lblPrd1.Text != "")
                    wherecntn = " ProductID in (" + lblPrd1.Text + ")";
                else if (lblPrd1.Text != "")
                    wherecntn = wherecntn + " and ProductID in (" + lblPrd1.Text + ")";
            }
            //assign condition to coach condition - to get mail list
            coachwherecntn = wherecntn;
            // added on 21-10-14 to get email list of roleid=88 with teamlead='y' depending upon product selected
            string coachEmailList = "";
            string cmdText = "declare @eMail varchar(max) select @eMail=COALESCE(@eMail,'')+Email+',' from IndSpouse I inner join volunteer V on I.AutomemberId=V.MemberId where V.RoleId=88 and V.TeamLead='y' and  " + coachwherecntn + " select @eMail";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.ToString() != string.Empty) coachEmailList = ds.Tables[0].Rows[0][0].ToString();


            if (wherecntn == "")
                wherecntn = " EventYear in (0";
            else
                wherecntn = wherecntn + " AND EventYear in (0";

            foreach (ListItem i in lstYearCoach1.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;

                }
            }
            wherecntn = wherecntn + ") ";

            wherecntn = wherecntn + " And Semester='" + DropDownList3.SelectedValue + "'";
            if (ddlsessionCoach1.SelectedValue != "0")
                wherecntn = wherecntn + " And SessionNo=" + ddlsessionCoach1.SelectedValue;
            if (DDlSignupCoacheslevel.SelectedValue != "0")
            {
                wherecntn = wherecntn + " And Level='" + DDlSignupCoacheslevel.SelectedValue + "'";
            }
            string contactEmail;
            string StrSQL = "SELECT distinct(Email) as EmailID From IndSpouse where  automemberid in (Select MemberID  from CalSignUp where   " + wherecntn + " ) group by Email ";


            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    contactEmail = dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString();

                    // avoid duplicate list in emaillist when concate sbemaillist with coachmail list
                    if (coachEmailList != string.Empty)
                    {
                        int inx = coachEmailList.ToString().IndexOf(contactEmail);
                        if (inx != -1)
                        {
                            if (coachEmailList.Contains(contactEmail + ",")) coachEmailList = coachEmailList.Remove(inx, contactEmail.Length + 1);
                            else if (coachEmailList.Contains(contactEmail)) coachEmailList = coachEmailList.Remove(inx, contactEmail.Length);
                        }
                        if (coachEmailList.StartsWith(",")) coachEmailList = coachEmailList.Remove(0, 1);
                        if (coachEmailList.EndsWith(",")) coachEmailList = coachEmailList.Remove(coachEmailList.LastIndexOf(","), 1);
                    }

                    sbEmailList.Append(contactEmail);
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }

                // Response.Write("<br>Coach =" + coachEmailList + "<br> Mail List =" );

                if (coachEmailList.Length > 0 && sbEmailList.Length > 0) sbEmailList.Append("," + coachEmailList);

                Session["emaillist"] = sbEmailList.ToString();
                Session["sentemaillistenable"] = "No";
                Session["EmailSource"] = "EmailCoach";
                Response.Redirect("emaillist.aspx");
            }
            else
                lblerrcoach.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            //  Response.Write(ex.ToString());
        }
    }
    protected void rdcalsignup_CheckedChanged(object sender, EventArgs e)
    {
        rdParentandstudent.Checked = false;
        Panel2.Visible = false;
        rdCoaches.Checked = false;
        rdParents.Checked = false;
        RadioButton1.Checked = false;
        PnlSheduled.Visible = false;
        Panel1.Visible = false;
        Panel3.Visible = false;
        Pnlcoach.Visible = true;
        Panel2.Visible = false;
        RbtnVolunteerSignUp.Checked = false;
        pnlVolunteerSignUp.Visible = false;
        GetProduct(DropDownList3.SelectedValue);
    }
    protected void lstYearCoach1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(ddlpgCoach1, DropDownList3.SelectedValue);
        GetProduct(DropDownList3.SelectedValue);
    }
    protected void ddlprCoach1_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevel(DDlSignupCoacheslevel, ddlpgCoach1.SelectedValue, ddlprCoach1.SelectedValue);
        //GetCoachName(ddlCoach1);
    }
    protected void ddlpgCoach1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct(ddlSemesterCoach.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void btnSendEmailToNewVolunteer_Click(object sender, EventArgs e)
    {
        try
        {
            string StrSQL;

            StrSQL = "select distinct(I.email) as EmailID from VolSignUp V Inner Join IndSpouse I on I.AutoMemberID=V.MemberID where EventId in (13) and TeamId=7 and not Exists (select * from CalSignUp C where C.MemberID = V.MemberID)";

            //Response.Write(StrSQL);
            string[] tblEmails = new string[] { "EmailContacts" };
            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    sbEmailList.Append(dsEmails.Tables[0].Rows[ctr]["EmailID"].ToString());
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }
                Session["emaillist"] = sbEmailList.ToString();
                Session["EmailSource"] = "EmailCoach";
                Response.Redirect("emaillist.aspx");
            }
            else
                lblerrcoach.Text = "Sorry No email found";
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void ddlSemesterCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(ddlpgCoach, ddlSemesterCoach.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(ddlpgCoach1, DropDownList3.SelectedValue);
        GetCoachName(ddlCoach);
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCoachName(ddlCoach);
    }

    protected void ddlLevelParentStudent_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCoachName(DDcoachparntstudent);
    }
    protected void ddlLevelCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        //   GetCoachName(ddlCoach);
    }
    protected void DDlSignupCoacheslevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GetCoachName(ddlCoach);
    }
    protected void ddlprCoach_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevel(ddlLevelCoach, ddlpgCoach.SelectedValue, ddlprCoach.SelectedValue);
        // GetCoachName(ddlCoach);
    }
}
