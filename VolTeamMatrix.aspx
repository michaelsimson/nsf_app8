﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VolTeamMatrix.aspx.cs" Inherits="VolTeamMatrix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
<br />
     <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Volunteer Team Matrix

    </strong>
             <br />
        <br />
    </div>
       <br />
  <div id="DvTeam" runat="server" align="center">
  <table><tr><td>
  <asp:DataGrid ID="GVTeamMatrix" runat="server" AutoGenerateColumns="false"> 
  <Columns>
       <asp:BoundColumn DataField="TeamId"   HeaderText="TeamId"></asp:BoundColumn>  
     <asp:BoundColumn DataField="TeamName"   HeaderText="Teams"></asp:BoundColumn>  
   <asp:TemplateColumn HeaderText="Finals"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd1" runat="server"  SelectedValue="<%# Bind('Finals') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>

                                             <asp:TemplateColumn HeaderText="ChapterContests"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd2" runat="server"   SelectedValue="<%# Bind('ChapterContests') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
                                             <asp:TemplateColumn HeaderText="Workshop"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd3" runat="server"  SelectedValue="<%# Bind('Workshop') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
                                             <asp:TemplateColumn HeaderText="Coaching"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd4" runat="server"  SelectedValue="<%# Bind('Coaching') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
                                             <asp:TemplateColumn HeaderText="Prepclub"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd5" runat="server"  SelectedValue="<%# Bind('Prepclub') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
                                          <asp:TemplateColumn HeaderText="OnlineWorkshop"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd6" runat="server"  SelectedValue="<%# Bind('OnlineWorkshop') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="National"   HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                          
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAdd7" runat="server"  SelectedValue="<%# Bind('National') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Yes</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>


                                          </asp:TemplateColumn>
  </Columns>
  </asp:DataGrid></td><td></td>
  <td>
      <asp:Button ID="btnUp"
        runat="server" Text="Update" OnClick="btnUp_Click" /></td></tr></table>
      <asp:Label ID="lbErr" runat="server" ForeColor="Red"></asp:Label><br />
    <asp:TextBox ID="TxtTeam" runat="server" Height="22px"></asp:TextBox>
      <asp:Button ID="btnAdd"
        runat="server" Text="Add" OnClick="btnAdd_Click" />
        <br />
        <br />
         <asp:Label ID="Label1" runat="server" Text="Add column Click here" ></asp:Label><br />
       
          <asp:Button ID="BTnClick"
        runat="server" Text="Add column" OnClick="BTnClick_Click" />
        <br /> 
          <asp:Label ID="lblist" runat="server" Text="Column List"  Visible="false"></asp:Label><br />
        <asp:GridView ID="GvTeam" runat="server" Visible="false"></asp:GridView>
        <br />
          <table border="0" cellpadding="0" cellspacing="0" align="center" id="tblAddDel" runat="server" visible="false">
            <tr>
              
               <td align="right" class="btn_02">Id:&nbsp; &nbsp;  </td>
                    <td align="left" > <asp:TextBox ID="TxtId" runat="server" Height="22px"></asp:TextBox></td>
           <td align="right" class="btn_02">       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;TeamName:&nbsp; &nbsp;  </td>
                    <td align="left" > <asp:TextBox ID="TxtTeamName" runat="server" Height="22px"></asp:TextBox></td>
            
              
            </tr>
             
               
          
        </table>
        <br />
        <table id="tblButton" runat="server" visible="false"> <tr>
              
               <td align="right" class="btn_02" style="width: 75px"> </td>
                    <td align="left" style="width: 28px"> <asp:Button ID="btnAddcol" runat="server" 
                            Text="Add" OnClick="btnAddcol_Click" 
                   /></td>
               <td align="right" class="btn_02" style="width: 15px"></td>
                    <td align="left" style="width: 133px"> <asp:Button ID="btnDel" runat="server" Text="Delete"  Visible="false"
                   /></td>
            
              
            </tr>
          </table>

         <asp:Label ID="lbdup" runat="server" ForeColor="Red"></asp:Label><br />
        </div>
        <asp:GridView ID = "gv" runat = "server"  >
    <Columns>

    </Columns>
</asp:GridView>
</asp:Content>

