Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Collections.Generic


Partial Public Class AddDonation
    Inherits System.Web.UI.Page

    Dim strSqlQuery As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not IsPostBack Then

            LoadYear()
            LoadEvents()
            loadbank()
            LoadOrganizations()
            LoadDonationPurpose()
            LoadEducationGame()
            LoadChapter()
            If Request.QueryString("Id") Is Nothing Then
                hlnkSearch.Enabled = False
            Else
                If Session("NavPath") = "Sponsor" Then
                    hlnkSearch.Enabled = True
                    hlnkSearch.NavigateUrl = "Search_Sponsor.aspx"
                End If
            End If
            If Not Request.QueryString("type") Is Nothing Then
                If Request.QueryString("type").ToUpper = "OWN" Then
                    'TrEduGame.Visible = False
                    dllEGames.Visible = False
                    lblDdlGame.Visible = False
                    RequiredfieldvalidatorGame.EnableClientScript = False
                    TrEndowment.Visible = True
                    TrDesign.Visible = False
                    Trmatch.Visible = True
                End If

            End If
        End If
        If Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
            hlinkParentRegistration.NavigateUrl = "VolunteerFunctions.aspx"
            hlinkParentRegistration.Text = "Back to Main Page"
        End If
        txtAmount.Attributes.Add("onkeypress", "return numeric();")
    End Sub

    Private Sub loadbank()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank")
        ddlBankId.DataSource = ds
        ddlBankId.DataBind()
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        lblerr.Text = ""
        lblerr2.Text = ""
        lblerr3.Text = ""
        lblError.Text = ""
        lbldate.Text = ""
        If Not IsDate(txtDonationDate.Text) Then
            lbldate.Text = "Please enter Donation date in mm/dd/yyyy format"
            Exit Sub
        End If
        If ddlMethod.SelectedValue.ToString <> "Credit Card" Then
            If Not IsDate(txtddate.Text) Then
                lblerr.Text = "Please enter Deposit date in mm/dd/yyyy format"
                Exit Sub
            End If
        Else
            If Len(txtddate.Text.Trim) > 0 Then
                If Not IsDate(txtddate.Text) Then
                    lblerr.Text = "Please enter Deposit date in mm/dd/yyyy format"
                    Exit Sub
                End If
            End If
        End If


        If ddlStatus.SelectedItem.Text = "Select Status" Then
            lblError.Text = "Please select valid status"
            Exit Sub
        ElseIf ddlDonationEvent.SelectedItem.Text = "Select Event" Then
            lblError.Text = "Please select valid Donation Event"
            Exit Sub
        End If
        If Request.QueryString("Type").ToString.ToUpper.Trim() = "OWN" Then
            If DDSuppCode.SelectedItem.Text = "Select SuppCode" Then
                lblError.Text = "Please select valid SuppCode"
                Exit Sub
            End If
        End If

        If ddlMethod.SelectedItem.Text = "Check" Or (ddlMatchingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Or (ddlMatchingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
            If (txtddate.Text = Nothing Or txtdsn.Text = Nothing) Or (ddlMatchingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Or (ddlMatchingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
                If ddlMethod.SelectedItem.Text = "Check" Then
                    If txtddate.Text = "" Then
                        lblerr.Text = "Enter Deposit Date"
                    End If
                    If txtdsn.Text = "" Then
                        lblerr2.Text = "Enter Deposit Slip Number"
                    End If
                End If
                If ddlMatchingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "") Then
                    lblerr3.Text = "When the matching flag is No, then Matching employer should not be selected."
                End If
                If (ddlMatchingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
                    lblerr3.Text = "When the matching flag is Yes, then Matching employer should be selected."
                End If
            Else
                ' added By Ferdine to Avoid Duplicate on Oct 01, 2010
                Dim memberid As Integer
                If Request.QueryString("Id") Is Nothing Then
                    memberid = Session("LoginID")
                Else
                    memberid = Request.QueryString("Id")
                End If
                If ddlMethod.SelectedItem.Text = "Check" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from DonationsInfo where DonationDate ='" & txtDonationDate.Text & "' and TRANSACTION_NUMBER='" & txtTransactionNo.Text & "' and MEMBERID =" & memberid & " and AMOUNT=" & txtAmount.Text.Trim & " and  DepositSlip=" & txtdsn.Text.Trim & "") > 0 Then
                    lblError.Text = "Duplicate entries for the same check."
                    Exit Sub
                End If
                ADD()
            End If
        Else
            ADD()
        End If
    End Sub

    Protected Sub ADD()
        Try




            Dim donation As New Donation()
            Dim iMemberID As Integer
            donation.Amount = Convert.ToDouble(txtAmount.Text)
            donation.CreateDate = Convert.ToDateTime(Now).ToShortDateString
            donation.ModifyDate = Convert.ToDateTime(Now).ToShortDateString
            donation.BankID = ddlBankId.SelectedItem.Value
            If Request.QueryString("Id") Is Nothing Then
                donation.MemberID = Session("LoginID")
                iMemberID = Session("LoginID")
            Else
                donation.MemberID = Request.QueryString("Id")
                iMemberID = Request.QueryString("Id")
            End If
            If Not Session("CustIndID") Is DBNull.Value Then
                donation.DonorType = "IND"
            Else
                donation.DonorType = "SPOUSE"
            End If
            If Not Request.QueryString("Type") Is Nothing Then
                If Request.QueryString("Type").ToString.ToUpper.Trim() = "OWN" Then
                    donation.DonorType = Request.QueryString("Type")
                Else
                    donation.DonorType = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select DonorType From IndSpouse where Automemberid=" & Request.QueryString("Id") & "") '
                End If
                donation.DonorType = Request.QueryString("Type") 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select DonorType From IndSpouse where Automemberid=" & Request.QueryString("Id") & "") '
            End If
            Dim strSql As String
            strSql = "SELECT ISNULL(MAX(DonationNumber),0) AS Expr1 FROM DonationsInfo WHERE MemberID=" & iMemberID
            Response.Write(strSql)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim iMaxDonationNumber As Integer
            iMaxDonationNumber = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql))
            donation.DonationNumber = iMaxDonationNumber + 1
            donation.TransactionNumber = txtTransactionNo.Text.Replace("'", "''")
            donation.DonationDate = txtDonationDate.Text.Replace("'", "''")
            donation.Method = ddlMethod.SelectedItem.Value
            donation.Purpose = ddlPurpose.SelectedItem.Value
            donation.Event = ddlDonationEvent.SelectedItem.ToString
            donation.Endowment = ddlEndowment.SelectedItem.Value
            donation.Designation = ddlDesignation.SelectedItem.Value
            donation.InMemoryOf = txtMemoryOf.Text.Replace("'", "''")
            donation.Status = ddlStatus.SelectedItem.Value

            'If donation.MatchingFlag = "No" Then
            '    donation.MatchingEmployer = DBNull.Value.ToString
            'Else
            If ddlEmployer.SelectedItem.Text <> "Select Employer Name" Then
                donation.MatchingEmployer = ddlEmployer.SelectedItem.Text
            End If

            donation.MatchingFlag = ddlMatchingFlag.SelectedItem.Value

            lblError.Visible = True
            lblError.Text = ddlEmployer.SelectedItem.Text
            lblError.Text = ddlEmployer.SelectedValue

            'End If
            'donation.MatchingEmployer = ddlEmployer.SelectedItem.ToString
            If (donation.MatchingEmployer = "Select Employer Name" Or donation.MatchingEmployer = "") Then
                donation.MatchEmpID = 0
            Else
                donation.MatchEmpID = ddlEmployer.SelectedItem.Value
            End If
            'donation.MatchEmpID = ddlEmployer.SelectedItem.Value
            donation.Remarks = txtRemarks.Text.Replace("'", "''")
            donation.DeleteReason = ""
            donation.DeletedFlag = "No"
            donation.Anonymous = ddlAnonymous.SelectedValue
            donation.ChapterID = drpchapterid.SelectedValue
            '  donation.ChapterID = donation.GetChapterID(iMemberID, Application("ConnectionString"), donation.DonorType)
            donation.EventID = ddlDonationEvent.SelectedItem.Value
            donation.RecurrenceFlag = ""
            donation.TaxDeduction = 100
            donation.CreatedBy = Session("LoginID")
            donation.ModifiedBy = 0
            donation.EventYear = ddlEventYear.SelectedValue 'Year(CDate(txtDonationDate.Text))
            Dim access As String = dllEGames.SelectedItem.Value
            If access = "None" Then
                access = ""
            Else
                access = access.ToString
            End If
            donation.Access = access
            If ddlMethod.SelectedValue.ToString <> "Credit Card" Then
                donation.DepositDate = txtddate.Text.Replace("'", "''")
            Else
                If Len(txtddate.Text.Trim) > 0 Then
                    donation.DepositDate = txtddate.Text.Replace("'", "''")
                Else
                    donation.DepositDate = DBNull.Value.ToString
                End If
            End If
            ' donation.DepositDate = txtddate.Text
            donation.DepositSlip = txtdsn.Text.Replace("'", "''")
            donation.DonationType = drpdonationtype.SelectedItem.Text.ToString
            donation.Project = txtProject.Text.Replace("'", "''")
            If DDSuppCode.SelectedItem.Text = "Select SuppCode" Or DDSuppCode.SelectedItem.Text = "None" Then
                donation.SuppCode = DBNull.Value.ToString

            Else
                donation.SuppCode = DDSuppCode.SelectedItem.Text
            End If


            ''Update Access value with NULL for user memeberid
            'Dim custIndId As String = DBNull.Value.ToString

            'custIndId = Session("CustIndId").ToString
            ''Dim conn1 As SqlConnection = New SqlConnection(Application("ConnectionString"))
            'Dim StrSql3 As String
            'StrSql3 = "Update DonationsInfo Set Access = NULL Where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " + custIndId + " and Access = 0)"
            'Dim ds1 As DataSet
            'ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSql3)

            Try
                donation.AddDonation(Application("ConnectionString"))
            Catch ex As Exception
                Response.Write(ex.Message)
                Response.End()
            End Try
            'Disabled by Ferdine on Nov 02
            '' ''Update value with NULL for user memeberid
            ' ''Dim strSql1 As String
            ' ''Dim strSql2 As String
            ' ''Dim strSql3 As String
            ' ''Dim strSql4 As String
            ' ''strSql = "Update DonationsInfo Set Access = NULL Where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & iMemberID & " and Access = 0)"
            ' ''strSql1 = "Update DonationsInfo Set MatchEmpID = NULL, MATCHINGEMPLOYER = NULL Where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & iMemberID & " and MatchEmpID = 0)"
            ' ''strSql2 = "Update DonationsInfo Set DepositDate = NULL where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & iMemberID & " and DepositDate = '1/1/1900')"
            ' ''strSql3 = "Update DonationsInfo Set DepositSlip = NULL where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & iMemberID & " and DepositSlip = 0)"
            ' ''strSql4 = "Update DonationsInfo Set MATCHINGEMPLOYER = NULL where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & iMemberID & " and MATCHINGFLAG = 'No')"
            ' ''Dim ds1 As DataSet
            ' ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ' ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            '' ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql2)
            ' ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql3)
            ' ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)

            If Not Request.QueryString("Id") Is Nothing Then
                Response.Redirect("dbsearchresults.aspx")
            Else
                Response.Redirect("ViewDonations.aspx")
            End If

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub LoadEvents()
        Dim drEvents1 As DataSet
        'Dim drEvents As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))

        strSqlQuery = "Select EventID,EventCode, Name from event order by Name asc"
        drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
        Dim Count1 As Integer = 0
        If (drEvents1.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                ddlDonationEvent.DataTextField = "Name"
                ddlDonationEvent.DataValueField = "EventID"
                Dim li As ListItem = New ListItem(Name)
                li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                ddlDonationEvent.Items.Add(li)
            Next i
            ddlDonationEvent.Items.Insert(0, New ListItem("Select Event", ""))
            ' ddlDonationEvent.Items.Insert(1, "None")
            ddlDonationEvent.SelectedIndex = 0

        End If

        'strSqlQuery = "SELECT EventCode,Name FROM Event "
        'Dim drEvents As SqlDataReader
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        'drEvents = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        'While drEvents.Read()
        '    ddlDonationEvent.Items.Add(New ListItem(drEvents(1).ToString(), drEvents(0).ToString()))
        'End While
        'ddlDonationEvent.Items.Insert(0, New ListItem("Select Event", ""))
        'ddlDonationEvent.SelectedIndex = 0
    End Sub

    Private Sub LoadOrganizations()
        strSqlQuery = "SELECT AutoMemberID,Organization_Name FROM OrganizationInfo order BY Organization_Name ASC"
        Dim drEvents As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drEvents = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drEvents.Read()
            ddlEmployer.Items.Add(New ListItem(drEvents(1).ToString(), drEvents(0).ToString()))
        End While
        ddlEmployer.Items.Insert(0, New ListItem("Select Employer Name", ""))
        ddlEmployer.SelectedIndex = 0
    End Sub

    Private Sub LoadDonationPurpose()
        strSqlQuery = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose   "
        Dim drPurpose As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drPurpose = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drPurpose.Read()
            ddlPurpose.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
            ddlEndowment.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
        End While
        ddlPurpose.Items.Insert(0, New ListItem("Select Purpose", ""))
        ddlPurpose.SelectedIndex = 4
        ddlEndowment.Items.Insert(0, New ListItem("Select Endowment", ""))
        ddlEndowment.SelectedIndex = 0
    End Sub

    Private Sub LoadEducationGame()
        strSqlQuery = "select ProductID, Name from Product where EventID =4 and Status = 'O'"
        Dim drGames As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim Count1 As Integer = 0


        drGames = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)

        If (drGames.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(drGames.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim Name As String = drGames.Tables(0).Rows(i)(1).ToString()
                Dim Product As String = drGames.Tables(0).Rows(i)(0).ToString()
                dllEGames.DataValueField = "Product"
                dllEGames.DataTextField = "Name"
                Dim li As ListItem = New ListItem(Name)
                li.Text = drGames.Tables(0).Rows(i)(1).ToString()
                li.Value = drGames.Tables(0).Rows(i)(0).ToString()

                dllEGames.Items.Add(li)
            Next i

            'dllEGames.Items.Insert(0, New ListItem("None", ""))
            dllEGames.Items.Insert(0, "None")
            dllEGames.SelectedIndex = 0

        End If

    End Sub

    Private Sub LoadChapter()
        Dim dschapters As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "Select chapterID, ChapterCode from Chapter order by state,chaptercode"
        dschapters = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        Dim Count1 As Integer = 0
        If (dschapters.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(dschapters.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim chapterID As String = dschapters.Tables(0).Rows(i)(0).ToString()
                Dim Name As String = dschapters.Tables(0).Rows(i)(1).ToString()
                drpchapterid.DataTextField = "Name"
                drpchapterid.DataValueField = "EventID"
                Dim li As ListItem = New ListItem(Name)
                li.Text = dschapters.Tables(0).Rows(i)(1).ToString()
                li.Value = dschapters.Tables(0).Rows(i)(0).ToString()
                drpchapterid.Items.Add(li)
            Next i
            ' drpchapterid.Items.Insert(0, New ListItem("Select Event", ""))
            'drpchapterid.SelectedValue = 1
            drpchapterid.SelectedIndex = drpchapterid.Items.IndexOf(drpchapterid.Items.FindByValue("109"))

        End If
    End Sub
    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)
        'ddlEventYear.Items.Insert(0, Convert.ToString(year))
        For i As Integer = 0 To 11
            ddlEventYear.Items.Insert(i, year - (i))
        Next
        If Now.Month < 5 Then
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year - 1))
        Else
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year))
        End If

    End Sub
End Class
