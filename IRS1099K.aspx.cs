﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Drawing;


public partial class IRS1099K : System.Web.UI.Page
{
    string StrQryCheck;
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
       Session["LoginID"] = "4240";
       if (Session["LoginID"] == null)
       {
           Response.Redirect("~/Maintest.aspx");
       }
        else
        {
           if (!IsPostBack)
            {
                //Session["LoginID"] = "4240";
             
                //Display();
                btnSaveUpdate.Text = "Save";
                PopulateYear(ddlYear);
             
                displayGridView();
                //displayrecords();
            }
       }
    }
    //protected void displayrecords()
    //{
    //    lblMsg.Visible = false;
    //    lblError.Visible = false;
    //    string strDisplayqry = "select i.RevID,i.Year,i.CCType,Count,January,February,March,April,May,June,July,August,September,October,November,December,i.CreatedBy,convert(nvarchar,i.CreatedDate,110) AS CreatedDate,i.ModifiedBy,convert(nvarchar,i.ModifyDate,110) AS ModifyDate from IRS1099KRev i inner join IRS1099KTrans ii on (i.Year=ii.Year and i.CCType=ii.CCType) where i.Year=" + ddlYear.SelectedValue + "";
    //    DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, strDisplayqry);
    //    gvIRS1099K.DataSource = ds;
    //    gvIRS1099K.DataBind();
    //    if (ds.Tables[0].Rows.Count == 0)
    //    {
    //        lblError.Visible = true;
    //        lblError.Text = "No Record Exists!";
    //        enterinputFields.Visible = true;
    //    }
    //}
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            //for (int i = 2011; i <= MaxYear; i++)
            //{
            //    list.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            for (int i = MaxYear; i >= (DateTime.Now.Year) - 4; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();

           // ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }
  

 
    
   
    

   
  
    //protected void GriDescription_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    lblError.Text = "";
    //    btnAdd.Text = "Update";
    //    string sDatakey;
      
    //    sDatakey = GriDescription.DataKeys[e.NewEditIndex].Value.ToString();
    //    ViewState["VolSignupGid"] = sDatakey;
    //    DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, "select * from VolSignupGuide where VolSignupGID=" + sDatakey + " ");
    //    TxtDescription.Text = dsCheck.Tables[0].Rows[0]["Description"].ToString();
    //    ddEvent.SelectedValue = dsCheck.Tables[0].Rows[0]["EventId"].ToString();
    //    Team();
    //    ddTeam.SelectedValue = dsCheck.Tables[0].Rows[0]["TeamId"].ToString();
       
    //    Button btn = (Button)GriDescription.Rows[e.NewEditIndex].FindControl("btnEdit");
    //   // btn.BackColor = System.Drawing.Color.SkyBlue;
    //    if (dsCheck.Tables[0].Rows[0]["Global"].ToString() == "Y")
    //    {
    //        ddGlobal.SelectedValue = "Y";
    //    }
    //    else
    //    {
    //        ddGlobal.SelectedValue = "";
    //    }
       
    //}
    //protected void GriDescription_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    GriDescription.EditIndex = -1;
    //    Display();
    //}
    protected void GriDescription_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        
        
    }
   
    protected void ddGlobal_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
    }
    
    //protected void GriDescription_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Edit")
    //    {
    //        GridViewRow gvr = (GridViewRow)((Button)e.CommandSource).NamingContainer;
    //        int rowIndex = gvr.RowIndex;
    //          GriDescription.Rows[rowIndex].BackColor = System.Drawing.Color.SkyBlue;
    //          for (int i = 0; i <= GriDescription.Rows.Count - 1; i++)
    //          {
    //              if (i != rowIndex)
    //              {
    //                  GriDescription.Rows[i].BackColor = System.Drawing.Color.White;
    //              }
    //          }
          


    //    }
    //    else
    //    {
    //    }
       
    //}
    protected void btnSaveUpdate_Click(object sender, EventArgs e)
    {
        if (btnSaveUpdate.Text.Equals("Save"))
        {
            string strCheckDuplication = "select * from IRS1099KRev where Year=" + ddlYear.SelectedValue + " and CCType='" + ddlCCType.SelectedValue + "' ";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, strCheckDuplication);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Not Allowed to Save, Record Exists for the selected Year/CCType!";
            }
            else
            {
                insertIRS1099KRevTable();
            }

        }
        else
        {
            string strUpdateqry = "update IRS1099KRev set January=" + tbJanuaryData.Text + ",February=" + tbFebruaryData.Text + ",March=" + tbMarchData.Text + ",April=" + tbAprilData.Text + ",May=" + tbMayData.Text + ",June=" + tbJuneData.Text + ",July=" + tbJulyData.Text + ",August=" + tbAugustData.Text + ",September=" + tbSeptemberData.Text + ",October=" + tbOctoberData.Text + ",November=" + tbNovemberData.Text + ",December=" + tbDecemberData.Text + ", ModifyDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where Year=" + ViewState["editedYear"] + " and CCType='" + ViewState["editedCCType"] + "'";

            int i = SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, strUpdateqry);

            string strUpdateTransqry = "update IRS1099KTrans set Count=" + tbNumberofYears.Text + ", ModifyDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where Year=" + ViewState["editedYear"] + " and CCType='" + ViewState["editedCCType"] + "'";
            int j = SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, strUpdateTransqry);
                if(i>0 && j>0)
                {


                    lblError.ForeColor = Color.Blue;
                    lblError.Text="Updated Successfully";
                    displayGridView();
                    btnSaveUpdate.Text = "Save";
                    emptyAllinputFields();
               
                    enterinputFields.Visible = false;
                }


        }
       
    }
   
    protected void insertIRS1099KRevTable()
    {
        try
        {
            if (ddlCCType.SelectedIndex == 0 || tbNumberofYears.Text.Trim().Equals("") || tbJanuaryData.Text.Trim().Equals("") || tbFebruaryData.Text.Trim().Equals("") || tbMarchData.Text.Trim().Equals("") || tbAprilData.Text.Trim().Equals("") || tbMayData.Text.Trim().Equals("") || tbJuneData.Text.Trim().Equals("") || tbJulyData.Text.Trim().Equals("") || tbAugustData.Text.Trim().Equals("") || tbSeptemberData.Text.Trim().Equals("") || tbOctoberData.Text.Trim().Equals("") || tbNovemberData.Text.Trim().Equals("") || tbDecemberData.Text.Trim().Equals(""))
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Please Enter All Fields!";
            }
            else
            {

                string strInsertSQLqry = "insert into IRS1099KTrans(Year,CCType,Count,CreatedBy,CreatedDate,ModifiedBy,ModifyDate) values(" + ddlYear.SelectedValue + ",'" + ddlCCType.SelectedValue + "',";
                strInsertSQLqry = strInsertSQLqry + "" + tbNumberofYears.Text + "," + Session["LoginID"] + ",getDate()," + Session["LoginID"] + ",getDate())";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, strInsertSQLqry);

                string strInsertQuery = "";
                strInsertQuery = "insert into IRS1099KRev(Year,CCType,January,February,March,April,May,June,July,August,September,October,November,December";
                strInsertQuery = strInsertQuery + ",CreatedBy,CreatedDate,ModifiedBy,ModifyDate) values(" + ddlYear.SelectedValue + ",'" + ddlCCType.SelectedValue + "',";
                strInsertQuery = strInsertQuery + "" + tbJanuaryData.Text + "," + tbFebruaryData.Text + "," + tbMarchData.Text + "," + tbAprilData.Text + "," + tbMayData.Text + ",";
                strInsertQuery = strInsertQuery + "" + tbJuneData.Text + "," + tbJulyData.Text + "," + tbAugustData.Text + "," + tbSeptemberData.Text + "," + tbOctoberData.Text + ",";
                strInsertQuery = strInsertQuery + "" + tbNovemberData.Text + "," + tbDecemberData.Text + "," + Session["LoginID"] + ",getDate()," + Session["LoginID"] + ",getDate())";
                int i = SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, strInsertQuery);
                if (i > 0)
                {
                   
                    lblError.ForeColor = Color.Blue;
                    lblError.Text = "Saved Successfully";
                    emptyAllinputFields();
                }
           
                
            }

        }
        catch (Exception ex) { Response.Write(ex); }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        enterinputFields.Visible = false;
        lblError.ForeColor = Color.Red;
        lblError.Text = "";
        displayGridView();

        if (gvIRS1099K.Rows.Count == 0)
        {
        
            lblError.Text = "Data doesn’t exist!";
            
            if (ddlCCType.SelectedIndex == 0)
            {
                
                
                lblError.Text = lblError.Text+" Please select CC Type";
            }
            else
            {

                lblError.Text = "";
                enterinputFields.Visible = true;
            }

        }
        
    }
    protected void displayGridView()
    {
        //if (ddlCCType.SelectedIndex == 0)
        //{
        //    lblMsg.Visible = false;
        //    lblError.Visible = true;
        //    lblError.Text = "Please select CCType";
        //}
       
        
         
            string strDisplayqry = "select i.RevID,i.Year,i.CCType,Count,January,February,March,April,May,June,July,August,September,October,November,December,i.CreatedBy,convert(nvarchar,i.CreatedDate,110) AS CreatedDate,i.ModifiedBy,convert(nvarchar,i.ModifyDate,110) AS ModifyDate from IRS1099KRev i inner join IRS1099KTrans ii on (i.Year=ii.Year and i.CCType=ii.CCType) where i.Year=" + ddlYear.SelectedValue + " ";
            if (ddlCCType.SelectedIndex > 0)
            {
                strDisplayqry = strDisplayqry + "and i.CCType='" + ddlCCType.SelectedValue + "'";
            }

            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, strDisplayqry);
            gvIRS1099K.DataSource = ds;
            gvIRS1099K.DataBind();
            
      

    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        ClearBackColor();
        lblError.Text = "";
    
        btnSaveUpdate.Text = "Save";
        emptyAllinputFields();
        enterinputFields.Visible = false;
    }
    protected void emptyAllinputFields()
    {
        
     
        tbNumberofYears.Text = "";
        tbJanuaryData.Text = "";
        tbFebruaryData.Text = "";
        tbMarchData.Text = "";
        tbAprilData.Text = "";
        tbMayData.Text = "";
        tbJuneData.Text = "";
        tbJulyData.Text = "";
        tbAugustData.Text = "";
        tbSeptemberData.Text = "";
        tbOctoberData.Text = "";
        tbNovemberData.Text = "";
        tbDecemberData.Text = "";
    }
    protected void gvIRS1099K_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
      
    }
   protected void ClearBackColor()
   {
    foreach (GridViewRow row in gvIRS1099K.Rows)
    {
        row.BackColor = System.Drawing.Color.Transparent;
    }
    }
    protected void gvIRS1099K_RowEditing(object sender, GridViewEditEventArgs e)
    {
        ClearBackColor();
        GridViewRow row = gvIRS1099K.Rows[e.NewEditIndex];
        row.BackColor = Color.LightBlue;

        //gvIRS1099K.EditIndex = e.NewEditIndex;
        //displayGridView();
        enterinputFields.Visible = true;
        string revid;
        HiddenField hfId = (gvIRS1099K.Rows[e.NewEditIndex].Cells[0].FindControl("RevID") as HiddenField);
        revid = hfId.Value;
        //ddlYear.SelectedValue = (gvIRS1099K.Rows[e.NewEditIndex].Cells[1].FindControl("lblYear") as Label).Text;
        //ddlCCType.SelectedValue = (gvIRS1099K.Rows[e.NewEditIndex].Cells[2].FindControl("lblCCType") as Label).Text.Trim();

        ViewState["editedYear"] = (gvIRS1099K.Rows[e.NewEditIndex].Cells[1].FindControl("lblYear") as Label).Text;
        ViewState["editedCCType"] = (gvIRS1099K.Rows[e.NewEditIndex].Cells[2].FindControl("lblCCType") as Label).Text.Trim();
        string selectNumofTransaction = "select Count from IRS1099KTrans where Year=" + (gvIRS1099K.Rows[e.NewEditIndex].Cells[1].FindControl("lblYear") as Label).Text + " and CCType='" + (gvIRS1099K.Rows[e.NewEditIndex].Cells[2].FindControl("lblCCType") as Label).Text + "'";
        DataSet ds=SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, selectNumofTransaction);
        if (ds.Tables[0].Rows.Count > 0)
        {

            tbNumberofYears.Text = ds.Tables[0].Rows[0]["Count"].ToString();
        }
        
        tbJanuaryData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[3].FindControl("lblJanuary") as Label).Text;
        tbFebruaryData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[4].FindControl("lblFebruary") as Label).Text;
        tbMarchData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[5].FindControl("lblMarch") as Label).Text;
        tbAprilData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[6].FindControl("lblApril") as Label).Text;
        tbMayData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[7].FindControl("lblMay") as Label).Text;
        tbJuneData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[8].FindControl("lblJune") as Label).Text;
        tbJulyData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[9].FindControl("lblJuly") as Label).Text;
        tbAugustData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[10].FindControl("lblAugust") as Label).Text;
        tbSeptemberData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[11].FindControl("lblSeptember") as Label).Text;
        tbOctoberData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[12].FindControl("lblOctober") as Label).Text;
        tbNovemberData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[13].FindControl("lblNovember") as Label).Text;
        tbDecemberData.Text = (gvIRS1099K.Rows[e.NewEditIndex].Cells[14].FindControl("lblDecember") as Label).Text;
        btnSaveUpdate.Text = "Update";

    }
    
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCCType.SelectedIndex = 0;
    }
   
    
}