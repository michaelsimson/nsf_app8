
<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="BankTrans.aspx.vb" Inherits="VRegistration.BankTransactions" title="Bank Transactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="SmallFont" OnClick="lbtnVolunteerFunctions_Click" runat="server">Back to Volunteer Functions</asp:LinkButton>
   <%--<asp:hyperlink CssClass="SmallFont" id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>
      --%>&nbsp;&nbsp;&nbsp;<br />
   <table cellpadding="3" cellspacing="0" style="width:1000px">
      <tr>
         <td align="center">
            <table cellpadding="3" cellspacing="0" bgcolor="#3366cc">
               <tr>
                  <td>
                     <table border ="0" cellpadding ="2px" cellspacing ="0" bgcolor="white">
                        <tr>
                           <td colspan ="4" align = "center" class="title" bgcolor="#3366cc">
                              Bank Transactions
                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnUploadFile" Text="Upload Transaction Files" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlCSVUpload" runat="server">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left" colspan="2">
                              <asp:FileUpLoad id="FileUpLoad1" runat="server" />
                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVerifyTrans" Text="Verify Transactions" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlVerifyTrans" runat="server">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"></td>
                           <td></td>
                        </tr>
                        <tr id="TrMsg" runat= "server" visible = "false">
                           <td colspan="4" align="center">
                              <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                              &nbsp;&nbsp;
                              <asp:Button ID="BtnNo" runat="server" Text="Yes"  Width="50px" OnClick="BtnNo_Click"  />
                              &nbsp;&nbsp;&nbsp; 
                              <asp:Button ID="BtnYes" runat="server" Text="No" OnClick="BtnYes_Click" Width="50px" />
                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnEditUpdate" Text="Edit/Update Transactions" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlEditUpdate" runat="server" OnSelectedIndexChanged = "ddlEditUpdate_SelectedIndexChanged" AutoPostBack="True">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlEditUpdateColumn" runat="server" Enabled="false">
                                 <asp:ListItem Value="0">Search Column</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td>
                              <asp:DropDownList ID="ddlEditUpdateSel" runat="server" Enabled="false">
                                 <asp:ListItem Value="0">Null</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="1">Yes/Not Null</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPostDB" Text="Post Transactions to DB" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlPostDB" runat="server">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"></td>
                           <td></td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnEditUpdateDB" Text="Edit/Update Trans DB" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlEditUpdateDB" runat="server" OnSelectedIndexChanged="ddlEditUpdateDB_SelectedIndexChanged" AutoPostBack="True">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="DdlTransCat" runat="server" Enabled="False" DataTextField="TransCat" DataValueField="TransCat">
                                <%-- <asp:ListItem Value="1">Check</asp:ListItem>
                                 <asp:ListItem Value="2">Deposit</asp:ListItem>
                                 <asp:ListItem Value="3">CreditCard</asp:ListItem>
                                 <asp:ListItem Value="4">Fee</asp:ListItem>
                                 <asp:ListItem Value="5">Grant</asp:ListItem>
                                 <asp:ListItem Value="6">Interest</asp:ListItem>
                                 <asp:ListItem Value="7">Donation</asp:ListItem>
                                 <asp:ListItem Value="8">DepositReturn</asp:ListItem>
                                 <asp:ListItem Value="10">DepositError</asp:ListItem>
                                 <asp:ListItem Value="9">Shipping</asp:ListItem>
                                 <asp:ListItem Value="11">Expense</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Trans Category</asp:ListItem>--%>
                              </asp:DropDownList>
                           </td>
                           <td></td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnReports" Text="Reports" GroupName="Uploadfiles" runat="server"  Enabled="false"/>
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlReports" runat="server" Enabled="false">
                                 <asp:ListItem Value="1">Chase - General</asp:ListItem>
                                 <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
                                 <asp:ListItem Value="3">HBT - Other</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"></td>
                           <td></td>
                        </tr>
                        <tr>
                           <td colspan = "4" align ="center" >
                              <asp:Button id="Button1" Text="Continue" OnClick="Button1_Click" runat="server" />
                           </td>
                        </tr>
                        <tr>
                           <td colspan = "4" align ="center" >
                              <asp:Label id="Label1" runat="server" ForeColor="Red" />
                           </td>
                        </tr>
                        <tr>
                           <td colspan = "4" align ="center" style="height :5px; background-color:#3366cc" ></td>
                        </tr>
                        <tr>
                           <td align="left" colspan="4">
                              <asp:Label ID="Label3" CssClass="SmallFont" ForeColor="Black" runat="server" Text="Add/Modify Tables : "></asp:Label>
                              <asp:Button ID="btnChase" runat="server" Text="ChaseDesc" Width="80px" />
                              &nbsp;
                              <asp:Button ID="btnChaseTrans" runat="server" Text="ChaseTransType" Width="120px" />
                              &nbsp;
                              <asp:Button
                                 ID="btnHBTAddinfo" runat="server" Text="HBTAddinfo" Width="88px" />
                              &nbsp;
                              <asp:Button
                                 ID="BtnHBTTransType" runat="server" Text="HBTDesc" />
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
   <asp:Panel ID="PnlChaseTemp" runat="server" Visible="false">
      <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Label ID="lblChaseTemp" CssClass="keytext" runat="server" Text="ChaseTemp"></asp:Label>
      <br />     
      <asp:DataGrid ID="ChaseResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="ChaseTempID"
         Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double" 
         BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
         <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
         <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>
         <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
         <FooterStyle BackColor="Gainsboro" ></FooterStyle>
         <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
         <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
            <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
            <asp:BoundColumn DataField="ChaseTempID"   HeaderText="ChaseTempID" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="TransType" ItemStyle-Width="10%">
               <ItemTemplate>
                  <asp:Label ID="lblTransType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtTransType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="TDate" headerText="Date" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Description" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblDescription_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtDescription_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Amount"  HeaderText="Amount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Parse_TransType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_TransType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_TransType")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_TransType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_TransType")%>'  ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Parse_Desc" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_Desc_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_Desc_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>' ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Posted"  HeaderText="Posted" readonly=true Visible="True"></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_BankID" HeaderText="Bk_BankID" readonly=true  Visible="True" ></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_TransDate" headerText="Bk_TransDate" ReadOnly=true Visible="True" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Bk_TransType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransType_1" runat="server" DataTextField="Bk_TransType" DataValueField="Bk_TransType" 
                     OnPreRender="ChaseBk_TransType_PreRender"  OnInit="ChaseBk_TransType_onInit" AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_TransCat" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransCat_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransCat_1" runat="server" DataTextField="Bk_TransCat" DataValueField="Bk_TransCat" 
                     OnPreRender="ChaseBk_TransCat_PreRender" OnInit="ChaseBk_TransCat_onInit"  AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_DepositNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DepositNumber_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_DepositNumber_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_CheckNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_CheckNumber_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_CheckNumber_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Bk_Amount" headerText="Bk_Amount" ReadOnly=true Visible="True" DataFormatString="{0:c}"></asp:BoundColumn  >
            <asp:TemplateColumn HeaderText="Bk_VendCust" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_VendCust_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_VendCust_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_Reason" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_Reason_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_Reason_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="MemberID" HeaderText="MemberID" readonly="true" Visible="true" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="DonorType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DonorType_1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlBk_DonorType_1" runat="server" OnPreRender="SetDropDownBk_DonorType">
                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                     <asp:ListItem Value="IND">Individual</asp:ListItem>
                     <asp:ListItem Value="OWN">Corporations</asp:ListItem>
                     <%--<asp:ListItem Value="OWN" Text="Unrestricted"></asp:ListItem>
                        <asp:ListItem Value="Temp Restricted" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>--%>
                  </asp:DropDownList>
               </EditItemTemplate>
            </asp:TemplateColumn>
         </Columns>
      </asp:DataGrid>
   </asp:Panel>
   <asp:Panel ID="pnlHBT1" runat="server" Visible="false">
      <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Label ID="lblHBT1Temp" CssClass="keytext" runat="server" Text="HBT1Temp"></asp:Label>
      <br />     
      <asp:DataGrid ID="HBT1Results" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="HBT1TempID"
         Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
         BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
         <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
         <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>
         <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
         <FooterStyle BackColor="Gainsboro" ></FooterStyle>
         <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
         <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
            <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
            <asp:BoundColumn DataField="HBT1TempID"   HeaderText="HBT1TempID" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:BoundColumn DataField="TDate" headerText="Date" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="CheckNum" headerText="CheckNum" ReadOnly=true Visible="true"></asp:BoundColumn>
            <asp:BoundColumn DataField="Description" headerText="Description" ReadOnly=true Visible="true"></asp:BoundColumn>
            <%--<asp:TemplateColumn HeaderText="Description" ItemStyle-Width="30%">
               <ItemTemplate>
               <asp:Label ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" ></asp:Label>
               </ItemTemplate>      
               <EditItemTemplate>
               <asp:TextBox ID="txtDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" TextMode="MultiLine"></asp:TextBox>
               </EditItemTemplate>                                                                  
               </asp:TemplateColumn>--%>
            <asp:BoundColumn DataField="WithdrawalAmount"  HeaderText="WithdrawalAmount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="DepositAmount"  HeaderText="DepositAmount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="AdditionalInfo"  HeaderText="AdditionalInfo" readonly=true Visible="true"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Parse_Desc" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_Desc_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_Desc_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>' ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Parse_AddInfo" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_AddInfo_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_AddInfo")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_AddInfo_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_AddInfo")%>'  ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Posted"  HeaderText="Posted" readonly=true Visible="True"></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_BankID" HeaderText="Bk_BankID" readonly=true  Visible="True" ></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_TransDate" headerText="Bk_TransDate" ReadOnly=true Visible="True" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Bk_TransType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransType_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransType_2" runat="server" DataTextField="Bk_TransType" DataValueField="Bk_TransType" 
                     OnPreRender="HBT1Bk_TransType_PreRender"  OnInit="HBT1Bk_TransType_onInit" AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_TransCat" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransCat_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransCat_2" runat="server" DataTextField="Bk_TransCat" DataValueField="Bk_TransCat" 
                     OnPreRender="HBT1Bk_TransCat_PreRender" OnInit="HBT1Bk_TransCat_onInit"  AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_CheckNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_CheckNumber_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_CheckNumber_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_DepositNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DepositNumber_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_DepositNumber_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Bk_Amount" headerText="Bk_Amount" ReadOnly=true Visible="True" DataFormatString="{0:c}"></asp:BoundColumn  >
            <asp:TemplateColumn HeaderText="Bk_VendCust" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_VendCust_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_VendCust_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_Reason" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_Reason_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_Reason_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="MemberID" HeaderText="MemberID" readonly="true" Visible="true" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="DonorType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DonorType_2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlBk_DonorType_2" runat="server" OnPreRender="SetDropDownBk_DonorType">
                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                     <asp:ListItem Value="IND">Individual</asp:ListItem>
                     <asp:ListItem Value="OWN">Corporations</asp:ListItem>
                     <%--<asp:ListItem Value="OWN" Text="Unrestricted"></asp:ListItem>
                        <asp:ListItem Value="Temp Restricted" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>--%>
                  </asp:DropDownList>
               </EditItemTemplate>
            </asp:TemplateColumn>
         </Columns>
      </asp:DataGrid>
   </asp:Panel>
   <asp:Panel ID="pnlHBT2" runat="server" Visible="false">
      <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Label ID="lblHBT2Temp" CssClass="keytext" runat="server" Text="HBT2Temp"></asp:Label>
      <br />     
      <asp:DataGrid ID="HBT2Results" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="HBT2TempID"
         Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
         BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
         <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
         <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>
         <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
         <FooterStyle BackColor="Gainsboro" ></FooterStyle>
         <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
         <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
            <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
            <asp:BoundColumn DataField="HBT2TempID"   HeaderText="HBT2TempID" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:BoundColumn DataField="TDate" headerText="Date" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="CheckNum" headerText="CheckNum" ReadOnly=true Visible="true"></asp:BoundColumn>
            <asp:BoundColumn DataField="Description" headerText="Description" ReadOnly=true Visible="true"></asp:BoundColumn>
            <asp:BoundColumn DataField="WithdrawalAmount"  HeaderText="WithdrawalAmount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="DepositAmount"  HeaderText="DepositAmount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:BoundColumn DataField="AdditionalInfo"  HeaderText="AdditionalInfo" readonly=true Visible="true"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Parse_Desc" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_Desc_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_Desc_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>' ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Parse_AddInfo" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblParse_AddInfo_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_AddInfo")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtParse_AddInfo_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_AddInfo")%>'  ></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Posted"  HeaderText="Posted" readonly=true Visible="True"></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_BankID" HeaderText="Bk_BankID" readonly=true  Visible="True" ></asp:BoundColumn>
            <asp:BoundColumn DataField="Bk_TransDate" headerText="Bk_TransDate" ReadOnly=true Visible="True" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Bk_TransType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransType_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransType_3" runat="server" DataTextField="Bk_TransType" DataValueField="Bk_TransType" 
                     OnPreRender="HBT2Bk_TransType_PreRender"  OnInit="HBT2Bk_TransType_onInit" AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransType")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_TransCat" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_TransCat_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList id="ddlBk_TransCat_3" runat="server" DataTextField="Bk_TransCat" DataValueField="Bk_TransCat" 
                     OnPreRender="HBT2Bk_TransCat_PreRender" OnInit="HBT2Bk_TransCat_onInit"  AutoPostBack="false">
                  </asp:DropDownList>
                  <%--                                <asp:TextBox ID="txtBk_TransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_TransCat")%>'></asp:TextBox>
                     --%>                                
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_CheckNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_CheckNumber_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_CheckNumber_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_CheckNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_DepositNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DepositNumber_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_DepositNumber_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_DepositNumber")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Bk_Amount" headerText="Bk_Amount" ReadOnly=true Visible="True" DataFormatString="{0:c}"></asp:BoundColumn  >
            <asp:TemplateColumn HeaderText="Bk_VendCust" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_VendCust_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_VendCust_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_VendCust")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Bk_Reason" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_Reason_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtBk_Reason_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Bk_Reason")%>'></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="MemberID" HeaderText="MemberID" readonly="true" Visible="true" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="DonorType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblBk_DonorType_3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlBk_DonorType_3" runat="server" OnPreRender="SetDropDownBk_DonorType">
                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                     <asp:ListItem Value="IND">Individual</asp:ListItem>
                     <asp:ListItem Value="OWN">Corporations</asp:ListItem>
                     <%--<asp:ListItem Value="OWN" Text="Unrestricted"></asp:ListItem>
                        <asp:ListItem Value="Temp Restricted" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>--%>
                  </asp:DropDownList>
               </EditItemTemplate>
            </asp:TemplateColumn>
         </Columns>
      </asp:DataGrid>
   </asp:Panel>
   <asp:Panel ID="pnlBankTrans" runat="server" Visible="false">
      <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Label ID="Label2" CssClass="keytext" runat="server" Text="BankTrans"></asp:Label>
      <br />     
      <asp:DataGrid ID="BankTransResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="BankTransID"
         Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
         BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
         <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
         <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>
         <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
         <FooterStyle BackColor="Gainsboro" ></FooterStyle>
         <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
         <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
            <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
            <asp:BoundColumn DataField="BankTransID"   HeaderText="BankTransID" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:BoundColumn DataField="BankID"   HeaderText="BankID" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:BoundColumn DataField="TransDate" headerText="TransDate" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="TransType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblTransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                   <asp:DropDownList ID="ddlTransType" runat="server" OnPreRender="ddlTransType_PreRender" DataTextField="TransType" DataValueField="TransType">
                   </asp:DropDownList>
                  <asp:TextBox ID="txtTransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>' Visible="false" ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="TransCat" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblTransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransCat")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                    <asp:DropDownList ID="ddlTransCat" runat="server" OnPreRender="ddlTransCat_PreRender" DataTextField="TransCat" DataValueField="TransCat">
                   </asp:DropDownList>

                  <asp:TextBox ID="txtTransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransCat")%>' Visible="false" ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="CheckNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckNumber")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckNumber")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="DepositNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblDepositNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositNumber")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtDepositNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositNumber")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Amount"   HeaderText="Amount" readonly=true Visible="true"  DataFormatString="{0:c}" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="VendCust" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblVendCust" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.VendCust")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtVendCust" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.VendCust")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Reason" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblReason" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Reason")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlReason" runat="server" OnPreRender="SetDropDown_Reason">
                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                     <asp:ListItem Value="Fee" Text="Fee"></asp:ListItem>
                     <asp:ListItem Value="Grant" Text="Grant"></asp:ListItem>
                     <asp:ListItem Value="Transfer" Text="Transfer"></asp:ListItem>
                  </asp:DropDownList>
                  <%-- <asp:TextBox ID="txtReason" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Reason")%>' ></asp:TextBox>--%>                            
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Description"   HeaderText="Description" readonly=true Visible="true" ></asp:BoundColumn>
            <asp:BoundColumn DataField="AddInfo" headerText="AddInfo" ReadOnly=true Visible="true"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="DepSlipNumber" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblDepSlipNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepSlipNumber")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtDepSlipNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepSlipNumber")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="GLAccount" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblGLAccount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.GLAccount")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtGLAccount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.GLAccount")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="RestType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblRestType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RestType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlRestType" runat="server" OnPreRender="SetDropDown_RestType">
                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                        <asp:ListItem Value="Unrestricted" Text="Unrestricted"></asp:ListItem>
                        <asp:ListItem Value="Temp Restricted" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>
                  </asp:DropDownList>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="MemberID" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:TextBox ID="txtMemberID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>' ></asp:TextBox>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="DonorType" ItemStyle-Width="30%">
               <ItemTemplate>
                  <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>' ></asp:Label>
               </ItemTemplate>
               <EditItemTemplate>
                  <asp:DropDownList ID="ddlDonorType_Bk" runat="server" OnPreRender="SetDropDown_DonorType">
                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                     <asp:ListItem Value="IND">Individual</asp:ListItem>
                     <asp:ListItem Value="OWN">Corporations</asp:ListItem>
                     <%--<asp:ListItem Value="OWN" Text="Unrestricted"></asp:ListItem>
                        <asp:ListItem Value="Temp Restricted" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>--%>
                  </asp:DropDownList>
               </EditItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateColumn>
         </Columns>
          <PagerStyle BackColor="#99CCCC" Font-Size="Small" Mode="NumericPages" />
      </asp:DataGrid>
   </asp:Panel>
   <asp:Panel ID="pnlChaseDesc" runat="server" Visible="false">
      <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004">
         <tr>
            <td align = "center">
               <center>
                  <table cellpadding="3" cellspacing="0" border="2"  >
                     <tr>
                        <td align = "center">
                           <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
                              <tr>
                                 <td align="center" colspan="3">
                                    <b>
                                    Add/Update ChaseDesc</b>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Token
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtToken" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align = "left">
                                    Bk_TransCat
                                 </td>
                                 <td></td>
                                 <td align="left">
                                    <asp:TextBox ID="txtBk_TransCat" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_VendorCust
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtBk_VendorCust" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_Reason
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtBk_Reason" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_DonorType
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:DropDownList ID="ddlDonorType" runat="server" Width="150px" Height="18px">
                                       <asp:ListItem>Select </asp:ListItem>
                                       <asp:ListItem>Individual - Unrestricted</asp:ListItem>
                                       <asp:ListItem>Non-Profit - Unrestricted</asp:ListItem>
                                       <asp:ListItem>Corporate - Unrestricted</asp:ListItem>
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    MemberID
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtMembID1" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    DonorType
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:DropDownList ID="ddlDonorType_1" runat="server" Width="150px" Height="18px">
                                       <asp:ListItem Value="">Select </asp:ListItem>
                                       <asp:ListItem Value="IND">IND</asp:ListItem>
                                       <asp:ListItem Value="OWN">OWN</asp:ListItem>
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center"></td>
                                 <td align="left" colspan="2">
                                    <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" />
                                    <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close Panel" />
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                    <asp:Label ID="lblChaseDesc" runat="server" Text="" ForeColor="Red"></asp:Label>
                                 </td>
                              </tr>
                           </table>
                           <asp:GridView ID="GridChaseDesc" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"  OnRowCommand="GridChaseDesc_RowCommand" >
                              <Columns>
                                 <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                 <asp:BoundField DataField="ID"  HeaderText="ID" />
                                 <asp:BoundField DataField="Token" HeaderText="Token"/>
                                 <asp:BoundField DataField="Bk_TransCat" HeaderText="Bk_TransCat" />
                                 <asp:BoundField DataField="Bk_VendorCust" HeaderText="Bk_VendorCust"/>
                                 <asp:BoundField DataField="Bk_Reason" HeaderText="Bk_Reason"/>
                                 <asp:BoundField DataField="Bk_DonorType" HeaderText="Bk_DonorType"/>
                                 <asp:BoundField DataField="MemberID" HeaderText="MemberID"/>
                                 <asp:BoundField DataField="DonorType" HeaderText="DonorType"/>
                              </Columns>
                           </asp:GridView>
                        </td>
                     </tr>
                  </table>
               </center>
            </td>
         </tr>
      </table>
   </asp:Panel>
   <asp:Panel ID="pnlChaseTransType" runat="server" Visible="false">
      <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004">
         <tr>
            <td align = "center">
               <center>
                  <table cellpadding="3" cellspacing="0" border="2"  >
                     <tr>
                        <td align = "center" style="height: 347px">
                           <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
                              <tr>
                                 <td align="center" colspan="3">
                                    <b>
                                    Add/Update ChaseTransType</b>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Token
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtTransToken" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align = "left">
                                    Bk_TransType
                                 </td>
                                 <td></td>
                                 <td align="left">
                                    <asp:TextBox ID="txtTransType" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center"></td>
                                 <td align="left" colspan="2">
                                    <asp:Button ID="BtnTransAdd" runat="server" Text="Add" Width="60" />
                                    <asp:Button ID="BtnTransCancel" runat="server" Text="Cancel" Width = "60" />
                                    <asp:Button ID="btnTransClose" runat="server" Text="Close Panel" />
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                    <asp:Label ID="lblChaseTransType" runat="server" Text=""></asp:Label>
                                 </td>
                              </tr>
                           </table>
                           <asp:GridView ID="GridChaseTransType" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"  OnRowCommand="GridChaseTransType_RowCommand" >
                              <Columns>
                                 <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                 <asp:BoundField DataField="ID"  HeaderText="ID" />
                                 <asp:BoundField DataField="Token" HeaderText="Token"/>
                                 <asp:BoundField DataField="Bk_TransType" HeaderText="TransType" />
                              </Columns>
                           </asp:GridView>
                        </td>
                     </tr>
                  </table>
               </center>
            </td>
         </tr>
      </table>
   </asp:Panel>
   <asp:Panel ID="pnlHBTAddinfo" runat="server" Visible="false">
      <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004">
         <tr>
            <td align = "center">
               <center>
                  <table cellpadding="3" cellspacing="0" border="2"  >
                     <tr>
                        <td align = "center">
                           <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
                              <tr>
                                 <td align="center" colspan="3">
                                    <b>
                                    Add/Update HBTAddinfo</b>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Token
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTToken" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align = "left">
                                    Bk_TransCat
                                 </td>
                                 <td></td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTBk_TransCat" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_VendorCust
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTBk_VendorCust" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_Reason
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTBk_Reason" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_DonorType
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:DropDownList ID="ddlDonorType_HBT" runat="server" Width="150px" Height="18px">
                                       <asp:ListItem>Select </asp:ListItem>
                                       <asp:ListItem>Individual - Unrestricted</asp:ListItem>
                                       <asp:ListItem>Non-Profit - Unrestricted</asp:ListItem>
                                       <asp:ListItem>Corporate - Unrestricted</asp:ListItem>
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    MemberID
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtMembID2" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    DonorType
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:DropDownList ID="ddlDonorType_2" runat="server" Width="150px" Height="18px">
                                       <asp:ListItem Value="">Select </asp:ListItem>
                                       <asp:ListItem Value="IND">IND</asp:ListItem>
                                       <asp:ListItem Value="OWN">OWN</asp:ListItem>
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center"></td>
                                 <td align="left" colspan="2">
                                    <asp:Button ID="BtnHBTAdd" runat="server" Text="Add" Width="60" />
                                    <asp:Button ID="BtnHBTCancel" runat="server" Text="Cancel" Width = "60" />
                                    <asp:Button ID="BtnHBTClose" runat="server" Text="Close Panel" />
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                    <asp:Label ID="lblHBTAddinfo" runat="server" Text="" ForeColor="Red"></asp:Label>
                                 </td>
                              </tr>
                           </table>
                           <asp:GridView ID="GridHBTAddinfo" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"  OnRowCommand="GridHBTAddinfo_RowCommand" >
                              <Columns>
                                 <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                 <asp:BoundField DataField="ID"  HeaderText="ID" />
                                 <asp:BoundField DataField="Token" HeaderText="Token"/>
                                 <asp:BoundField DataField="Bk_TransCat" HeaderText="Bk_TransCat" />
                                 <asp:BoundField DataField="Bk_VendorCust" HeaderText="Bk_VendorCust"/>
                                 <asp:BoundField DataField="Bk_Reason" HeaderText="Bk_Reason"/>
                                 <asp:BoundField DataField="Bk_DonorType" HeaderText="Bk_DonorType"/>
                                 <asp:BoundField DataField="MemberID" HeaderText="MemberID"/>
                                 <asp:BoundField DataField="DonorType" HeaderText="DonorType"/>
                              </Columns>
                           </asp:GridView>
                        </td>
                     </tr>
                  </table>
               </center>
               <br />
            </td>
         </tr>
      </table>
   </asp:Panel>
   <asp:Panel ID="pnlHBTTransType" runat="server" Visible="false">
      <table align="left" border="0" cellpadding="0" cellspacing="0" width="1004">
         <tr>
            <td align="center">
               <center>
                  <table border="2" cellpadding="3" cellspacing="0">
                     <tr>
                        <td align="center">
                           <table border="0" cellpadding="4" cellspacing="0" width="400">
                              <tr>
                                 <td align="center" colspan="3">
                                    <b>Add/Update HBT Desc</b>&nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Token
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTTransToken" runat="server" Height="18px" Width="150px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_TransType
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTTransType" runat="server" Height="18px" Width="150px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    Bk_TransCat
                                 </td>
                                 <td>
                                 </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtHBTTransCat" runat="server" Height="18px" Width="150px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center">
                                 </td>
                                 <td align="left" colspan="2">
                                    <asp:Button ID="BtnHBTTransAdd" runat="server" Text="Add" Width="60" />
                                    <asp:Button ID="BtnHBTTransCancel" runat="server" Text="Cancel" Width="60" />
                                    <asp:Button ID="btnHBTTransClose" runat="server" Text="Close Panel" />
                                 </td>
                              </tr>
                              <tr>
                                 <td align="center" colspan="3">
                                    <asp:Label ID="lblHBTTransType" runat="server" Text=""></asp:Label>
                                 </td>
                              </tr>
                           </table>
                           <asp:GridView ID="GridHBTTransType" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                              OnRowCommand="GridHBTTransType_RowCommand">
                              <Columns>
                                 <asp:ButtonField ButtonType="Button" CommandName="Modify" HeaderText="Modify" Text="Modify" />
                                 <asp:BoundField DataField="ID" HeaderText="ID" />
                                 <asp:BoundField DataField="Token" HeaderText="Token" />
                                 <asp:BoundField DataField="Bk_TransType" HeaderText="TransType" />
                                 <asp:BoundField DataField="Bk_TransCat" HeaderText="TransCat" />
                              </Columns>
                           </asp:GridView>
                        </td>
                     </tr>
                  </table>
               </center>
            </td>
         </tr>
      </table>
   </asp:Panel>
</asp:Content>

