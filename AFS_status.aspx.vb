Imports System
Imports System.Text
Imports System.Configuration
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections.Specialized
Imports System.Globalization
Imports LinkPointTransaction
Imports System.Text.RegularExpressions
Imports NorthSouth.BAL
Imports nsf.Entities
Imports System.Net.Mail
'*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
' 
' This software is the proprietary information of LinkPoint International, Inc.  
' Use is subject to license terms.
'
'******************************************************************************    
Namespace VRegistration
    Public Class status
        Inherits LinkPointAPI_cs.LinkPointTxn_Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim bc As HttpBrowserCapabilities = Request.Browser
            If (bc.Browser = "IE" And bc.MajorVersion > 4) Then
                fIE5 = True
            End If

            If Not Nothing = Session("outXml") Then
                order = Session("outXml")
            End If
            If Not Nothing = Session("resp") Then
                resp = Session("resp")
            End If

            ParseResponse(resp)
            LPTxn = New LinkPointTransaction.LinkPointTxn()


        End Sub

        Protected order As String = "<order>NO DATA IN WAS SENT IN THE ORDER</order>"
        Protected resp As String = "<r_error>NO DATA IN THE RESPONSE</r_error>"
        Protected fIE5 As Boolean = False
    End Class
End Namespace
