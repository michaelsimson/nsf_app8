﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using System.IO;
using System.Text;

public partial class TeamPlanning_TeamSchedule : System.Web.UI.Page
{
    string RoleID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            RoleID = Session["RoleID"].ToString();
            if (!IsPostBack)
            {

                if (Session["LoginTPChapterID"].ToString() != "0")
                {
                    Events();
                    Yearscount();
                    ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                    ChapterDrop(ddchapter);
                    if (Session["LoginTPChapterID"].ToString() == "1")
                    {
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddEvent.SelectedValue = "1";
                        ddEvent.Enabled = false;
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                        Team();
                    }
                    else if (Session["LoginTPChapterID"].ToString() == "109")
                    {
                        ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                        ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                        ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                    }
                    else if (Session["LoginTPChapterID"].ToString() != "1" && Session["LoginTPChapterID"].ToString() != "109")
                    {
                        ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                        ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                        ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));

                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                    }


                }
                else
                {
                    if (Convert.ToInt32(RoleID) <= 5)
                    {
                        Events();
                        Yearscount();
                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                        ChapterDrop(ddchapter);

                        ddchapter.Enabled = true;
                        ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                        ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                        ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                        ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                        ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        Yearscount();
                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                        ChapterDrop(ddchapter);
                    }
                    else
                    {
                        Yearscount();
                        string MemberID = Session["LoginID"].ToString();
                        string cmdText = "";
                        DataSet ds = new DataSet();
                        cmdText = "select distinct E.EventID,E.EventCode from Volunteer V inner join Event E on (V.EventID=E.EventID) where MemberID=" + MemberID + " and RoleID=" + Convert.ToInt32(RoleID) + " order by E.EventID Asc";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddEvent.DataSource = ds;
                        ddEvent.DataTextField = "EventCode";
                        ddEvent.DataValueField = "EventID";
                        ddEvent.DataBind();

                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddEvent.Enabled = true;
                            }
                            else
                            {
                                ddEvent.Enabled = false;
                                ddEvent.SelectedIndex = 1;
                            }
                        }

                        cmdText = "select distinct E.ChapterID,E.ChapterCode from Volunteer V inner join Chapter E on (V.ChapterID=E.ChapterID) where MemberID=" + MemberID + " and RoleID=" + Convert.ToInt32(RoleID) + " order by E.ChapterID Asc";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddchapter.DataSource = ds;
                        ddchapter.DataTextField = "ChapterCode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();

                        ddchapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddchapter.Enabled = true;
                            }
                            else
                            {
                                ddchapter.Enabled = false;
                                ddchapter.SelectedIndex = 1;
                            }
                        }

                        cmdText = "select E.TeamID,E.TeamName from Role V inner join VolTeamMatrix E on (V.TeamID=E.TeamID) where V.RoleID=" + RoleID + "";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddTeams.DataSource = ds;
                        ddTeams.DataTextField = "TeamName";
                        ddTeams.DataValueField = "TeamID";
                        ddTeams.DataBind();

                        ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddTeams.Enabled = true;
                            }
                            else
                            {
                                ddTeams.Enabled = false;
                                ddTeams.SelectedIndex = 1;
                                if (Convert.ToInt32(RoleID) == 8)
                                {
                                    ddTeams.Enabled = true;
                                    ddTeams.SelectedIndex = 0;
                                }
                            }
                        }


                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);

                    }
                }

            }
        }
        lblErrMsg.Text = "";
        lblMsg.Text = "";
    }
    string ConnectionString = "ConnectionString";
    string xmlData = "";
    protected void Events()
    {
        try
        {
            //ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
            //ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
            //ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
            //ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
            //ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
            //ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
            //ddEvent.Items.Insert(0, new ListItem("Finals", "1"));

            //ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();

            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {
        try
        {
            //if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
            //{
            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }
            //}
            //else if (ddEvent.SelectedValue == "1")
            //{
            //string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            //DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            //if (dschapter.Tables[0].Rows.Count > 0)
            //{
            //    ddChapter.Enabled = true;
            //    ddChapter.DataSource = dschapter;
            //    ddChapter.DataTextField = "chaptercode";
            //    ddChapter.DataValueField = "ChapterID";
            //    ddChapter.DataBind();

            //    ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            //}
            //ddchapter.SelectedValue = "1";
            //ddChapter.Enabled = false;
            //}
            else
            {
                ddchapter.Items.Clear();
                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
                ddChapter.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected void Team()
    {
        string StrQryCheck = "";
        if (ddEvent.SelectedValue != "0")
        {
            string eventText = ddEvent.SelectedItem.Text;
            if (ddEvent.SelectedItem.Text == "Chapter")
            {
                eventText = "ChapterContests";
            }
            StrQryCheck = "select * from  VolTeamMatrix where " + eventText + "='Y'";
        }
        else
        {
            StrQryCheck = "select * from  VolTeamMatrix";
        }

        try
        {
            DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryCheck);
            if (dsCheck.Tables[0].Rows.Count > 0)
            {

                ddTeams.DataSource = dsCheck;
                ddTeams.DataTextField = "TeamName";
                ddTeams.DataValueField = "TeamId";
                ddTeams.DataBind();

                ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedItem.Text != "Select Event")
        {
            Team();
        }
        try
        {

            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else
            {
                ddchapter.SelectedValue = "0";
                ddchapter.Enabled = true;
            }
        }
        catch (Exception ex)
        {
        }
        //ChapterDrop(ddchapter);


    }
    protected void ddTeams_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
       
    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
    }

    public void generateGantChart()
    {
        btnExportToExcel.Visible = true;
        lblErrMsg.Text = "";
        ArrayList chartColors = new ArrayList();
        chartColors.Add("#E94C6F");
        chartColors.Add("#A23BEC");
        chartColors.Add("#7F38EC");
        chartColors.Add("#DB3340");
        chartColors.Add("#E8B71A");
        chartColors.Add("#FA6900");
        chartColors.Add("#6AA121");
        chartColors.Add("#982395");
        chartColors.Add("#0020C2");
        chartColors.Add("#461B7E");
        chartColors.Add("#83BF17");
        chartColors.Add("#1352A2");

        chartColors.Add("#333300");
        chartColors.Add("#CC0063");
        chartColors.Add("#3F0082");
        chartColors.Add("#FF8C00");
        chartColors.Add("#ff8000");
        chartColors.Add("#8CC403");
        chartColors.Add("#FF3C08");

        ArrayList lstOfProdGrp = new ArrayList();
        DateTime dStartDate;
        DateTime dEndDate;
        string cmdText = "";
        DataSet dsDetails = new DataSet();
        DataSet dsActivityroup = new DataSet();
        xmlData = "";
        try
        {
            if (validateSearch() == "1")
            {
                if (ddYear.SelectedValue == "-1")
                {
                    //cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList";
                    cmdText = "select Activity,Duration,EndDate,TeamSettingsID from TeamActivity order by [Order] ASC";
                }
                else
                {
                    //cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList";
                    cmdText = "select Activity,Duration,EndDate,TeamSettingsID from TeamActivity where EventID=" + ddEvent.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and Year='" + ddYear.SelectedValue + "' order by [Order] ASC";
                }
                dsDetails = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
                if (ddYear.SelectedValue == "-1")
                {
                    //cmdText = "select distinct Components,ComponentID from BeeBookSchedule";
                    cmdText = "select ActivityGroup,TeamSettingsID from TeamSettings order by [Order] ASC";
                }
                else
                {
                    //cmdText = "select distinct Components,ComponentID from BeeBookSchedule";
                    cmdText = "select ActivityGroup,TeamSettingsID,UnitOfTime from TeamSettings where EventID=" + ddEvent.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and Year='" + ddYear.SelectedValue + "' order by [Order] ASC";
                }
                dsActivityroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
                if (dsDetails.Tables[0].Rows.Count == 0)
                {
                    lblMsg.ForeColor = Color.Red;
                    tblTeamScheduleChart.Style.Add("display", "none");
                    div1.Style.Add("visibility", "hidden");
                    lblMsg.Text = "No Record Exists!";
                }
                else
                {
                    tblTeamScheduleChart.Style.Add("display", "table");
                    div1.Style.Add("visibility", "visible");
                    lblMsg.Text = "";
                }
                string startDate = "";
                int i = 0;
                int duration;
                string unitOfTime = "";

                foreach (DataRow drGrp in dsActivityroup.Tables[0].Rows)
                {
                    if (lstOfProdGrp.Contains(drGrp["TeamSettingsID"]) == false)
                    {
                        lstOfProdGrp.Add(drGrp["TeamSettingsID"]);
                    }
                    unitOfTime = drGrp["UnitOfTime"].ToString();
                    xmlData += "    <group>";
                    xmlData += "      <name>" + drGrp["ActivityGroup"] + "</name>";
                    xmlData += "      <UnitOfTime>" + unitOfTime + "</UnitOfTime>";
                    xmlData += "      <ChartFlag>GUI</ChartFlag>";
                    xmlData += "      <rowspan>1</rowspan>";
                    //xmlData += "      <blockcolor>" + chartColors.IndexOf(lstOfProdGrp.IndexOf(drGrp["ComponentID"])) + "</blockcolor>";
                    foreach (DataRow dr in dsDetails.Tables[0].Select("TeamSettingsID =" + drGrp["TeamSettingsID"]))
                    {
                        if (startDate == "")
                        {
                            startDate = dr["EndDate"].ToString();
                        }
                        if (i >= 12)
                        {
                            i = 0;
                        }
                        duration = Convert.ToInt32(dr["Duration"]);
                        if (duration == 0)
                        {
                            duration = 7;
                        }
                        else if (duration < 6)
                        {
                            duration = 8;
                        }
                        else if (duration < 10)
                        {
                            duration = 9;
                        }
                        else if (duration > 10 && duration < 15)
                        {
                            duration = 13;
                        }
                        duration = duration * 8;
                        if (duration > 200 || duration > 175)
                        {
                            duration = 150;
                        }
                        if (duration > 150 && duration < 175)
                        {
                            duration = 125;
                        }
                        string Activity = "";
                        Activity = dr["Activity"].ToString();
                        Activity = Activity.Replace("&", " and ");
                        xmlData += "      <block>";
                        xmlData += "      <ChartFlag>GUI</ChartFlag>";
                        xmlData += "        <href>activity.aspx?ActID=7</href>";
                        xmlData += "        <StartDate>" + Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM-dd-yyyy") + "</StartDate>";
                        xmlData += "        <EndDate>" + Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM-dd-yyyy") + "</EndDate>";
                        xmlData += "        <name>" + Activity + "</name>";
                        xmlData += "        <color>" + chartColors[i] + "</color>";
                        xmlData += "        <colspan>" + dr["Duration"] + "</colspan>";
                        xmlData += "        <Duration>" + duration + "</Duration>";
                        xmlData += "      </block>";
                        startDate = dr["EndDate"].ToString();
                        i++;
                    }
                    xmlData += "    </group>";

                }
                xmlData = "<NewDataSet>" + xmlData + "</NewDataSet>";
                EventCalendarControl1.XMLData = xmlData;
                EventCalendarControl1.BlankGifPath = "trans.gif";
                //EventCalendarControl1.Year = Now.Year;
                //EventCalendarControl1.BeginDate = "01/11/2014";
                //EventCalendarControl1.EndDate = "01/12/2015";

                //EventCalendarControl1.BeginDate = "11/01/2014";
                //EventCalendarControl1.EndDate = "12/01/2015";

                EventCalendarControl1.Quarter = 4;
                EventCalendarControl1.BlockColor = "blue";
                EventCalendarControl1.ToggleColor = "#dcdcdc";
                EventCalendarControl1.CellHeight = 60;
                EventCalendarControl1.CellWidth = 20;
                EventCalendarControl1.EnableViewState = true;
            }
            else
            {
                validateSearch();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }





    public string validateSearch()
    {
        string retVal = "1";
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
        if (ddYear.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select year";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select event";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select chapter";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddTeams.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select team";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        return retVal;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validateSearch() == "1")
        {
            generateGantChart();

        }
        else
        {
            validateSearch();
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        exportExcel();
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "loadtest();", true);

        //Response.Clear();
        //Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
        //Response.Charset = "";
        //Response.ContentType = "application/vnd.xls";
        //Response.ContentType = "application/vnd.ms-excel";
        //div3.Visible = true;

        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter stringWrite = new StringWriter();
        //HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //var sb = new StringBuilder();
        //dvChart2.RenderControl(new HtmlTextWriter(new StringWriter(sb)));

        ////string s = sb.ToString();
        ////this.EnableViewState = false;

        //dvChart2.RenderControl(htmlWrite);
        //Response.Write(stringWrite.GetStringBuilder().ToString());

        //Response.End();
        //div3.Visible = false;
    }


    //public void exportdivToExce(){
    //    var htmlContent = GetHtmlContent();
    //var excelName="ProjectTarget_" + CampaignID;
    //response.Clear();
    //response.ClearHeaders();
    //response.ClearContent();
    //response.ContentEncoding = Encoding.GetEncoding("windows-1254");
    //response.Charset = "windows-1254"; //ISO-8859-13 ISO-8859-9  windows-1254
    //response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", excelName));
    //response.ContentType = "application/ms-excel";
    ////response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //const string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";
    //response.Write(header + htmlContent);
    //response.End();
    //}
    public void exportExcel()
    {
        Response.Clear();
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;
        string teamName = (ddTeams.SelectedItem.Text).Replace(" ", "");
        string filename = "TeamSchedule_" + teamName + "_" + monthDay + "_" + year + ".xls";
        Response.AppendHeader("content-disposition", "attachment;filename=" + filename + "");
        btnExportToExcel.Visible = true;
        lblErrMsg.Text = "";
        ArrayList chartColors = new ArrayList();
        chartColors.Add("#E94C6F");
        chartColors.Add("#A23BEC");
        chartColors.Add("#7F38EC");
        chartColors.Add("#DB3340");
        chartColors.Add("#E8B71A");
        chartColors.Add("#FA6900");
        chartColors.Add("#6AA121");
        chartColors.Add("#982395");
        chartColors.Add("#0020C2");
        chartColors.Add("#461B7E");
        chartColors.Add("#83BF17");
        chartColors.Add("#1352A2");

        chartColors.Add("#333300");
        chartColors.Add("#CC0063");
        chartColors.Add("#3F0082");
        chartColors.Add("#FF8C00");
        chartColors.Add("#ff8000");
        chartColors.Add("#8CC403");
        chartColors.Add("#FF3C08");

        ArrayList lstOfProdGrp = new ArrayList();
        DateTime dStartDate;
        DateTime dEndDate;
        string cmdText = "";
        DataSet dsDetails = new DataSet();
        DataSet dsActivityroup = new DataSet();
        xmlData = "";
        int i = 0;
        int duration;
        try
        {
            if (validateSearch() == "1")
            {
                if (ddYear.SelectedValue == "-1")
                {
                    //cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList";
                    cmdText = "select Activity,Duration,EndDate,TeamSettingsID from TeamActivity order by [Order] ASC";
                }
                else
                {
                    //cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList";
                    cmdText = "select Activity,Duration,EndDate,TeamSettingsID from TeamActivity where EventID=" + ddEvent.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and Year='" + ddYear.SelectedValue + "' order by [Order] ASC";
                }
                dsDetails = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
                if (ddYear.SelectedValue == "-1")
                {
                    //cmdText = "select distinct Components,ComponentID from BeeBookSchedule";
                    cmdText = "select ActivityGroup,TeamSettingsID from TeamSettings order by [Order] ASC";
                }
                else
                {
                    //cmdText = "select distinct Components,ComponentID from BeeBookSchedule";
                    cmdText = "select ActivityGroup,TeamSettingsID,UnitOfTime from TeamSettings where EventID=" + ddEvent.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and Year='" + ddYear.SelectedValue + "' order by [Order] ASC";
                }
                dsActivityroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                Response.Write("<table style='width:1200px;'>");
                Response.Write("<tbody>");
                Response.Write("<tr>");
                Response.Write("<td width='30' valign='top' nowrap='' align='right'>");
                Response.Write("<table>");
                Response.Write("<tbody>");
                Response.Write("<tr>");
                Response.Write("<td style='background-color: #2B60DE; color: white; font-weight: bold;'>Task</td>");
                Response.Write("</tr>");

                foreach (DataRow drGrp in dsActivityroup.Tables[0].Rows)
                {
                    Response.Write("<tr>");
                    Response.Write("<td style='background-color: #D1D0CE; border-bottom-style: none;'");
                    Response.Write("&nbsp;");
                    Response.Write("</td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    Response.Write("<td width='20' bgcolor='#dcdcdc' style='vertical-align: middle; height: 70px; background-color: #D1D0CE;'>");
                    Response.Write(drGrp["ActivityGroup"]);
                    Response.Write("</td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    Response.Write("<td style='background-color: #D1D0CE; border-bottom:1px solid black;'");
                    Response.Write("<span style='font-size: 10pt'>(" + drGrp["UnitOfTime"] + ")</span>");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                }

                Response.Write("</tbody>");
                Response.Write("</table>");
                Response.Write("</td>");


                Response.Write("<td width='100%' valign='top' nowrap='' align='left'>");
                Response.Write("<table cellspacing='0' cellpadding='0' bordercolor='#000000' style='BORDER-COLLAPSE: collapse; width: 100%; table-layout: fixed;'>");

                Response.Write("<tbody>");

                Response.Write("<tr>");
                Response.Write("<td style='background-color: #2B60DE; color: white; font-weight: bold;' colspan='40'>");
                Response.Write("Activities");
                Response.Write("</td>");
                Response.Write("</tr>");
                foreach (DataRow drGrp in dsActivityroup.Tables[0].Rows)
                {

                    Response.Write("<tr>");
                    foreach (DataRow dr in dsDetails.Tables[0].Select("TeamSettingsID =" + drGrp["TeamSettingsID"]))
                    {
                        duration = Convert.ToInt32(dr["Duration"]);
                        if (duration == 0)
                        {
                            duration = 1;
                        }

                        else
                        {
                            duration = duration / 7;
                        }



                        Response.Write("<td width='20' align='center' colspan='" + duration + "'");
                        Response.Write("<span style='font-size: 10pt'>" + Convert.ToInt32(dr["Duration"]) + "</span>");
                        Response.Write("</td>");
                    }
                    Response.Write("</tr>");


                    Response.Write("<tr>");
                    foreach (DataRow dr in dsDetails.Tables[0].Select("TeamSettingsID =" + drGrp["TeamSettingsID"]))
                    {
                        duration = Convert.ToInt32(dr["Duration"]);
                        if (duration == 0)
                        {
                            duration = 1;
                        }

                        else
                        {
                            duration = duration / 7;
                        }

                        if (i >= 12)
                        {
                            i = 0;
                        }
                        Response.Write("<td  width='20' align='center' style='height: 70px; word-wrap: break-word; white-space: pre-line; background-color: " + chartColors[i] + "' colspan='" + duration + "'>");
                        Response.Write("<span style='font-size: 10pt; word-wrap: break-word; white-space: pre-line; color:white;'>" + dr["Activity"] + "</span>");
                        Response.Write("</td>");
                        i++;
                    }
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    foreach (DataRow dr in dsDetails.Tables[0].Select("TeamSettingsID =" + drGrp["TeamSettingsID"]))
                    {
                        duration = Convert.ToInt32(dr["Duration"]);
                        if (duration == 0)
                        {
                            duration = 1;
                        }

                        else
                        {
                            duration = duration / 7;
                        }


                        Response.Write("<td width='20' align='center' colspan='" + duration + "'");
                        Response.Write("<span style='font-size: 10pt'>" + Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM-dd-yyyy") + "</span>");
                        Response.Write("</td>");
                    }
                    Response.Write("</tr>");


                }
                Response.Write("</tbody>");

                Response.Write("</td>");

                Response.Write("</tr>");
                Response.Write("</tbody>");
                Response.Write("</table>");
            }
            Response.Flush();
            Response.SuppressContent = true;
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}

