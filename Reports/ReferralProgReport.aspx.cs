﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using NativeExcel;
using Microsoft.ApplicationBlocks.Data;


public partial class Reports_ReferralProgReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            string[] roleIDArr = { "1", "2", "3", "4", "5" };
            string roleID = Session["RoleID"].ToString();
            int isExist = Array.IndexOf(roleIDArr, roleID);
            if (isExist > -1)
            {
                populateReferralProgReport();
            }
            else
            {

            }
        }

    }

    public void populateReferralProgReport()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            // cmdText = "select  distinct AutoMemberID,FirstName, LastName, Email, Chapter, State,IP.HPhone,(Select count(*) from ReferralProg where RefType=2 and RMemberID =Ip.AutoMemberID) as Recommended  from ReferralProg RP inner join  Indspouse IP on(RP.RMemberID=IP.AutoMemberID) where RP.RefType=2";
            string RoleID = Session["RoleID"].ToString();
            if (RoleID == "1" || RoleID == "2")
            {
                cmdText = "select  distinct IP.AutoMemberID ,IP.FirstName , IP.LastName , IP.Email, IP.Chapter ,IP.State , IP.HPhone , RP.RefType, (Select count(*) from ReferralProg where RefType=2 and RMemberID =Ip.AutoMemberID) as Recommended  from ReferralProg RP inner join  Indspouse IP on (RP.RMemberID=IP.AutoMemberID and RP.RefType=2) order by LastName, FirstName";
            }
            else if (RoleID == "3" || RoleID == "4" || RoleID == "5")
            {

                cmdText = "select  distinct IP.AutoMemberID ,IP.FirstName , IP.LastName , IP.Email, IP.Chapter ,IP.State , IP.HPhone , RP.RefType, (Select count(*) from ReferralProg where RefType=2 and RMemberID =Ip.AutoMemberID) as Recommended  from ReferralProg RP inner join  Indspouse IP on (RP.RMemberID=IP.AutoMemberID and RP.RefType=2) where IP.Chapter='" + Session["selChapterText"].ToString() + "' order by LastName, FirstName";


            }

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    btnExportToExcel.Visible = true;
                    spnStatus.Visible = false;
                    grdReferralProgReport.DataSource = ds;
                    grdReferralProgReport.DataBind();
                }
                else
                {
                    spnStatus.Visible = true;
                }
            }
            else
            {
                spnStatus.Visible = true;
            }
        }
        catch
        {
        }

    }

    public void grdReferralProgReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "MemberCount")
            {

                int selIndex = int.Parse(e.CommandArgument.ToString());

                string memberID = ((Label)grdReferralProgReport.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string refType = ((Label)grdReferralProgReport.Rows[selIndex].FindControl("lblRefType") as Label).Text;
                hdnAutoMemberID.Value = memberID;
                hdnRefType.Value = refType;
                populateMemberDetails(memberID, refType);
                grdMemberInfo.PageIndex = 0;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);

            }
        }
        catch
        {
        }
    }

    public void populateMemberDetails(string memberID, string refType)
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        if (refType == "1")
        {
            cmdText = "select AutoMemberID,FirstName, LastName, Email, Chapter, State,IP.HPhone,IP.CPhone,IP.WPhone from ReferralProg RP inner join  Indspouse IP on(RP.RMemberID=IP.AutoMemberID) where RP.RefType=1 and RP.MemberID=" + memberID + "";
        }
        else if (refType == "2")
        {
            cmdText = "select AutoMemberID,FirstName, LastName, Email, Chapter, State,IP.HPhone,IP.CPhone,IP.WPhone from ReferralProg RP inner join  Indspouse IP on(RP.MemberID=IP.AutoMemberID) where RP.RefType=2 and RP.RMemberID=" + memberID + "";
        }
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdMemberInfo.DataSource = ds;
                    grdMemberInfo.DataBind();
                }
                else
                {
                    grdMemberInfo.DataSource = ds;
                    grdMemberInfo.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ExportExcel()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            string RoleID = Session["RoleID"].ToString();
            if (RoleID == "1" || RoleID == "2")
            {
                cmdText = "select  distinct IP.AutoMemberID ,IP.FirstName , IP.LastName , IP.Email, IP.Chapter ,IP.State , IP.HPhone , RP.RefType, (Select count(*) from ReferralProg where RefType=2 and RMemberID =Ip.AutoMemberID) as Recommended  from ReferralProg RP inner join  Indspouse IP on (RP.RMemberID=IP.AutoMemberID and RP.RefType=2) order by LastName, FirstName";
            }
            else if (RoleID == "3" || RoleID == "4" || RoleID == "5")
            {

                cmdText = "select  distinct IP.AutoMemberID ,IP.FirstName , IP.LastName , IP.Email, IP.Chapter ,IP.State , IP.HPhone , RP.RefType, (Select count(*) from ReferralProg where RefType=2 and RMemberID =Ip.AutoMemberID) as Recommended  from ReferralProg RP inner join  Indspouse IP on (RP.RMemberID=IP.AutoMemberID and RP.RefType=2) where IP.Chapter='" + Session["selChapterText"].ToString() + "' order by LastName, FirstName";


            }
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Referral Program Report";

                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "New Chapters";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Chapter Name";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "First Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Last Name";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "Email";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "HPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Ref Type";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "AutoMemberID";
                    oSheet.Range["H3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["Chapter"];

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["FirstName"];

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["LastName"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["RefType"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["AutoMemberID"];


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }


                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;
                    string FileName = "ReferralProgReport";

                    string filename = "" + FileName + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();
                }
            }

        }
        catch
        {
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        ExportExcel();
    }

    protected void grdReferralProgReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdReferralProgReport.PageIndex = e.NewPageIndex;
        populateReferralProgReport();
    }

    protected void grdMemberInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdMemberInfo.PageIndex = e.NewPageIndex;
        populateMemberDetails(hdnAutoMemberID.Value, hdnRefType.Value);
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
    }
}