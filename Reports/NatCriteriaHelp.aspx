

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>National Criteria Help</title>
     <style type="text/css">
<!--
body {
	margin-left: 10px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style1 {
	font-family: Verdana;
	font-size: 30px;
	color: #FFFFFF;
}
.style2 {
	font-family: Arial;
	font-size: 12pt;
}
.style3 {color: #FFFFFF}
a:link {
	color: #FFFFFF;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #FFFFFF;
}
a:hover {
	text-decoration: underline;
	color: #999999;
}
a:active {
	text-decoration: none;
	color: #FFFFFF;
}

-->
</style>
</head>
<body bgcolor="#333333">
    <form id="form1" runat="server">
    <div>
    <table border ="0" cellpadding ="0" cellspacing ="0" style="width :600px" bgcolor="#FFFFAE">
<tr>
<td align="center" bgcolor="#3366CC" style="vertical-align :middle; height :60px">
<span class="style1">North South Foundation</span>
</td>
</tr>   
<tr>
<td align="center" class="style2"><br /><b> National Invitee Year to Year Comparison </b></td></tr>
<%--<tr><td align="center"> 
    <asp:Literal ID="Literal1" runat="server"></asp:Literal></td></tr>--%> 
<tr><td align="center" >  <br />
<table border ="1" align="center" width="600px" height="400px" style="font-size:18px">
<tr><td align="center"> MinScore�</td><td> Min Score for current year from National Invitee table</td></tr> 
<tr><td align="center"> Invitees </td><td> Count of Invitees from Inviteecount table for the current year.</td></tr> 
<tr><td align="center"> Exp_Reg </td><td> Invitees*Last_Invitees/Last_Registrations</td></tr> 
<tr><td align="center"> Exp_shows </td><td>�Exp_Reg *Last_Shows/Last_Registrations</td></tr> 
<tr><td align="center"> Last_Min_Score </td><td> Min Score for last year</td></tr> 
<tr><td align="center"> Last_Invitees </td><td> Count of Invitees from Inviteecount table for Last year</td></tr> 
<tr><td align="center"> Last_Registrations </td><td> Registration Count for last year from Contestant table</td></tr> 
<tr><td align="center"> Last_Shows </td><td> Count of shows (with scores) last year from contestant table</td></tr> 
<tr><td align="center"> Registrations% </td><td>�(Last_Invitees + 0.0) / (Last_Registrations + 0.0) * 100</td></tr> 
<tr><td align="center"> Shows%</td><td> (Last_Shows + 0.0) / (Last_Registrations + 0.0) * 100</td></tr> 
</table>
</td>
</tr> 
<tr>
    <td bgcolor="#3366CC" align="right" style="padding-right:25px; padding-bottom:3px; padding-top:3px"><span class="style3">� 1993-2012 North South Foundation. All worldwide rights reserved.
        <a href="/public/main/privacy.aspx" target="_blank">Copyright</a>
    </span></td>
  </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
