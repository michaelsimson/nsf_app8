<%@ Page Language="C#" AutoEventWireup="true" CodeFile="xlReport3.aspx.cs" Inherits="Reports_xlReport1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Tight Schedule Alert</title>
</head>
<body>
   <form id="Form1" method="post" runat="server">
			<asp:DataGrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 80px; POSITION: absolute; TOP: 64px" runat="server"
				Height="248px" Width="432px" AutoGenerateColumns="False">
				<FooterStyle Wrap="False"></FooterStyle>
				<SelectedItemStyle Wrap="False"></SelectedItemStyle>
				<EditItemStyle Wrap="False"></EditItemStyle>
				<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
				<ItemStyle Wrap="False"></ItemStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Ser#" HeaderText="Ser#"></asp:BoundColumn>
					<asp:BoundColumn DataField="Rank" Visible="false" HeaderText="Rank"></asp:BoundColumn>
					<asp:BoundColumn DataField="FirstName" HeaderText="FirstName"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastName" HeaderText="LastName"></asp:BoundColumn>
					<asp:BoundColumn DataField="DOB" Visible="false"  HeaderText="DOB"></asp:BoundColumn>
					<asp:BoundColumn DataField="Gr" HeaderText="Gr"></asp:BoundColumn>
					<asp:BoundColumn DataField="SpellingBee" HeaderText="SB"></asp:BoundColumn>
					<asp:BoundColumn DataField="Vocabulary" HeaderText="VB"></asp:BoundColumn>
					<asp:BoundColumn DataField="MathBee" HeaderText="MB"></asp:BoundColumn>
					<asp:BoundColumn DataField="Science" HeaderText="SC"></asp:BoundColumn>
					<asp:BoundColumn DataField="Geography" HeaderText="GB"></asp:BoundColumn>
					<asp:BoundColumn DataField="EssayWriting" HeaderText="EW"></asp:BoundColumn>
					<asp:BoundColumn DataField="PublicSpeaking" HeaderText="PS"></asp:BoundColumn>
                    <asp:BoundColumn DataField="BrainBee" HeaderText="BB"></asp:BoundColumn>
					<asp:BoundColumn DataField="Day1" HeaderText="Day1"></asp:BoundColumn>
					<asp:BoundColumn DataField="Day2" HeaderText="Day2"></asp:BoundColumn>
					<asp:BoundColumn DataField="Day3" HeaderText="Day3"></asp:BoundColumn>
					<asp:BoundColumn DataField="ParentFName" HeaderText="ParentFName"></asp:BoundColumn>
					<asp:BoundColumn DataField="ParentLName" HeaderText="ParentLName"></asp:BoundColumn>
					<asp:BoundColumn DataField="Email" HeaderText="Email"></asp:BoundColumn>
					<asp:BoundColumn DataField="HPhone" HeaderText="HPhone"></asp:BoundColumn>
					<asp:BoundColumn DataField="CPhone" HeaderText="CPhone"></asp:BoundColumn>
				</Columns>
				<PagerStyle Wrap="False"></PagerStyle>
			</asp:DataGrid>
       &nbsp;&nbsp;
			<asp:Label id="lblChapter" style="Z-INDEX: 103; LEFT: 489px; POSITION: absolute; TOP: 14px"
				runat="server" Width="144px"></asp:Label>
			<asp:Button id="btnSave"  runat="server"
				Text="Save File" onclick="btnSave_Click"></asp:Button>&nbsp; &nbsp;<asp:Button id="btnBack"  runat="server"
				Text="Back" OnClick="btnBack_Click" ></asp:Button>
		</form>
</body>
</html>


 
 
 