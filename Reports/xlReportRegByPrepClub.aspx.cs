using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class Reports_xlReportRegByPrepClub : System.Web.UI.Page
{
    int chapID = -1;
    string Year = "";
    DateTime day1 = DateTime.Parse("1/1/2020");
    DateTime day2 = DateTime.Parse("1/1/2000");
    int iYear = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        if (!IsPostBack)
        {
            int year = Convert.ToInt32(DateTime.Now.Year);
            ddlyear.Items.Insert(0,new ListItem(Convert.ToString(year - 1)));
            ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
           // ddlyear.Items(1).Selected = true;
            ddlyear.SelectedIndex = 1;
            prod(chapID, year);
            ChkReport(chapID, year);
            ddlReport.SelectedIndex = 2;
            ddlReport.Enabled = false;
        }
        if (Session["LoggedIn"].ToString() == "True")
            HyperLink2.Visible = true;
        else
            HyperLink2.Visible = false;
           
    }
    private void ChkReport(int chapID, int yy)
    {
        int future = 0, past = 0;
        future = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from Registration_PrepClub where EventDate>Getdate() AND ChapterId="+ chapID  +" AND EventYear=" + yy+ ""));
        past = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from Registration_PrepClub where EventDate<=Getdate() AND ChapterId=" + chapID + " AND EventYear=" + yy + ""));
        if ((future > 0) && (past > 0))
        {
            HFlag.Value = "Y";
            ReportSel.Visible = true;
        }
        else
        {
            HFlag.Value = "N";
            ReportSel.Visible = false;
        }
    }

    private void prod(int chapID, int yy)
    {
        ddlprod.Items.Clear();
        ddlprod.Items.Add("All");
        ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("All"));
        int j = 0;
        string str = "Select distinct(e.productid), p.name from PrepClubCal e, product p where e.productid = p.productid and e.eventyear = " + yy + " and e.chapterid = " + chapID ;
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.Connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListItem li = new ListItem();
                String af = reader["productid"].ToString();
                li.Value = af;
                li.Text = reader[1].ToString();
                ddlprod.Items.Add(li);
                j = 1;

            }
            cmd.Connection.Close();
            cmd.Connection.Dispose();
            if (j == 0)
            {
                ddlprod.Items.Clear();
                ddlprod.Items.Add("No Contest");
                ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("No Contest"));
            }
        }
        catch (Exception a)
        {
            //Response.Write(a.ToString());
        }        
    }

    private int GetData(int chapID, int EventYear)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd;
            if (HFlag.Value == "Y")
            {
                if (ddlReport.SelectedValue == "Past")
                    cmd = new SqlCommand("GetRegistrationDetailsByEventPast_PrebClub", conn);
                else if (ddlReport.SelectedValue == "ALL")
                    cmd = new SqlCommand("GetRegistrationDetailsByEventAll_PrebClub", conn);
                else 
                    cmd = new SqlCommand("GetRegistrationDetailsByEventFuture_Prepclub", conn);
            }
            else
                cmd = new SqlCommand("GetregistrationDetailsByEvent_PrepClub", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ChapterId", chapID);
            cmd.Parameters.AddWithValue("EventYear", EventYear);
            cmd.Parameters.AddWithValue("EventID", 19);
            cmd.Connection.Open();
//            SqlDataSource dsResultsFromQuery = cmd.ExecuteReader();
            grdRegByContest.DataSource = cmd.ExecuteReader();
            grdRegByContest.DataBind();

            if (grdRegByContest.Items.Count > 0)
            {
                grdRegByContest.Visible = true;
                lblGrdMsg.Text = "";
                btnSave.Enabled = true;
            }
            else
            {
                grdRegByContest.Visible = false;
                lblGrdMsg.Text = "No Data Found";
                btnSave.Enabled = false;
            }
            cmd.Connection.Close();
            cmd.Connection.Dispose();
        }
        catch (Exception e)
        {
          //Response.Write(e.ToString());
        }
        return 0;
    }
    private int GetData(int chapID, int EventYear, int Proid)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd;
            //new SqlCommand("GetregistrationDetailsByEventprod", conn);
            if (HFlag.Value == "Y")
            {
                if (ddlReport.SelectedValue == "Past")
                    cmd = new SqlCommand("GetregistrationDetailsByEventprodPast_PrepClub", conn);
                else if (ddlReport.SelectedValue == "ALL")
                    cmd = new SqlCommand("GetregistrationDetailsByEventprodAll_PrebClub", conn);
                else
                    cmd = new SqlCommand("GetregistrationDetailsByEventprodFuture_PrepClub", conn);
            }
            else
                cmd = new SqlCommand("GetregistrationDetailsByEventprod_PrepClub", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("ChapterId", chapID);
            cmd.Parameters.AddWithValue("EventYear", EventYear);
            cmd.Parameters.AddWithValue("EventID", 19);
            cmd.Parameters.AddWithValue("EventProd", Proid);
            cmd.Connection.Open();
            //            SqlDataSource dsResultsFromQuery = cmd.ExecuteReader();
            grdRegByContest.DataSource = cmd.ExecuteReader();
            grdRegByContest.DataBind();

            if (grdRegByContest.Items.Count > 0)
            {
                grdRegByContest.Visible = true;
                lblGrdMsg.Text = "";
                btnSave.Enabled = true;
            }
            else 
            {
                grdRegByContest.Visible = false;
                lblGrdMsg.Text = "No Data Found";
                btnSave.Enabled = false;
            }

            cmd.Connection.Close();
            cmd.Connection.Dispose();
        }
        catch (Exception e)
        {
          // Response.Write(e.ToString());
        }
        return 0;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        String strChapterName = GetChapterName(chapID);
        strChapterName = strChapterName.Replace(",", "");
        Year = ddlyear.SelectedValue;
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=CheckinList_" + strChapterName + "_" + Year + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        grdRegByContest.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
        
    }

    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
          Response.Redirect("../VolunteerFunctions.aspx");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ddlprod.SelectedItem.Text == "No Contest")
        { }
        else
        {

            if (ddlprod.SelectedItem.Text == "All")
            {
                Year = ddlyear.SelectedItem.Text;
                iYear = Convert.ToInt32(Year);
                lblChapter.Text = Year.ToString() + " ";
                lblChapter.Text = lblChapter.Text + GetChapterName(chapID);
                prod(chapID, iYear);
                GetData(chapID, iYear);
            }
            else
            {
                Year = ddlyear.SelectedItem.Text;
                string prodi = "";

                foreach (ListItem i in ddlprod.Items)
                {
                    if (i.Selected)
                    {
                        prodi = i.Value;
                        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                        SqlCommand cmd = new SqlCommand("PrepClubtemp", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("id", Convert.ToInt32(prodi));
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                        cmd.Connection.Dispose();
                    }
                }
                int prodid = Convert.ToInt32(ddlprod.SelectedItem.Value);

                iYear = Convert.ToInt32(Year);
                lblChapter.Text = Year.ToString() + " ";
                lblChapter.Text = lblChapter.Text + GetChapterName(chapID);
                //prod(chapID, iYear);
                GetData(chapID, iYear, prodid);
            }
        }
    }

    protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Year = ddlyear.SelectedItem.Text;
        iYear = Convert.ToInt32(Year);
        prod(chapID, iYear);
        ChkReport(chapID, iYear);
    }
}
