﻿<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="DonationSummary.aspx.cs" Inherits="DonationSummary" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
            &nbsp&nbsp&nbsp&nbsp
            <asp:LinkButton ID="LBbacktofront" CssClass="btn_02" runat="server" Text="Previous" 
                 Visible="false" onclick="LBbacktofront_Click" 
        >Back to Front Page</asp:LinkButton>
          
    </div>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
                Donation Summary Report
            </div>
              <div align="center" id="Iddonor" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
                Donation Summary Report By Donor
            </div>
             <div align="center" id="Idevent" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
                Donation Summary Report By Event
            </div>
             <div  id="Idchapter"  align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);" runat="server" visible="false">
                Donation Summary Report By Chapter
            </div>
            <br />
              <br />
              <div>  <asp:LinkButton ID="lbprevious" runat="server" Text="Previous" 
                      Visible="false" onclick="lbprevious_Click" 
        >Previous</asp:LinkButton></div>
              <div runat="server" id="Divchoice" align="center" style="font-size: 26px; font-weight: bold;">
                  <asp:DropDownList ID="DDchoice" runat="server"  AutoPostBack="True" 
                      onselectedindexchanged="DDchoice_SelectedIndexChanged">
                  </asp:DropDownList>
                
              </div>
              
              
              <asp:Panel ID="Pnldisp" runat="server">
      
        <table style="width: 50%;" border="0">
            <tr>
                <td style="width: 157px">
                    <asp:Label ID="lblevent" runat="server" Text="Event"></asp:Label>
                    <asp:DropDownList ID="ddevent" runat="server" Width="135px" AutoPostBack="True" onselectedindexchanged="ddevent_SelectedIndexChanged" 
                         >
                        <asp:ListItem Value="0">[Select Event]</asp:ListItem>
                    </asp:DropDownList>
                </td>
             
                <td style="width: 157px">
                    <asp:Label ID="lblZone" runat="server" Text="Zone" Width="87px" Style="height: 20px"></asp:Label>
                    <asp:DropDownList ID="ddZone" runat="server" AutoPostBack="True" Width="150px" 
                        onselectedindexchanged="ddZone_SelectedIndexChanged" >
                        <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                    </asp:DropDownList>
              
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblCluster" runat="server" Text="Cluster" Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddCluster" runat="server" Width="150px" 
                        AutoPostBack="True" onselectedindexchanged="ddCluster_SelectedIndexChanged"
                        >
                        <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblchapter" runat="server" Text="Chapter " Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddchapter" runat="server" Width="150px" AutoPostBack="True"
                        >
                        <asp:ListItem Value="0">[Select Chapter]</asp:ListItem>
                    </asp:DropDownList>
               
                </td>
                <td style="width: 157px" visible="false">
                    <asp:Label ID="Lblcat" runat="server" Text="Expense Category" Width="87px" Visible="false"></asp:Label>
                    <asp:DropDownList ID="DDcategory" runat="server" AutoPostBack="True" Width="175px"
                        Visible="false" >
                        <asp:ListItem Value="0">[Select Category]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblNoyear" runat="server" Text="No of Years" Width="87px"></asp:Label>
                    <asp:DropDownList ID="ddNoyear" runat="server" 
                        Width="175px" >
                        <asp:ListItem Value="0">[Select No of Years]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblyear" runat="server" Text="Year" Width="87px"></asp:Label>
                    <asp:DropDownList ID="DDyear" runat="server" Width="175px">
                        <asp:ListItem Value="0">All</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td width="200px">
                </td>
                <td width="200px">
                </td>
                <td>
                    
                    <asp:Button ID="Button1" runat="server" Text="Submit" 
                        onclick="Button1_Click"  />
                    <asp:Button ID="Button2" runat="server" Text="Export To Excel" 
                        Enabled="false" onclick="Button2_Click" />
                </td>
            </tr>
            
        </table>
        <asp:Label ID="lbldisp" runat="server" Visible="false" Text="No Record Found" ForeColor="Red"></asp:Label>
        <asp:Label ID="lblall" runat="server" Text="Enter All The Fields" Visible="false"
            ForeColor="Red"></asp:Label>
        <asp:Label ID="lblMesasenorecord" runat="server" EnableViewState="false" 
            ForeColor="Red"></asp:Label>
             <asp:Label ID="lblNoPermission" runat="server" Visible="false" ForeColor="Red"></asp:Label>
    </asp:Panel>
    <br />
    <div>
        <asp:GridView ID="GridDisplay" runat="server" align="center" 
            DataKeyNames="MEMBERID" EnableViewState="true" AllowPaging="true" PageSize="50"  
            onrowdatabound="GridDisplay_RowDataBound" 
            onpageindexchanging="GridDisplay_PageIndexChanging"  >
            <PagerSettings PageButtonCount="30" />
          <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
                <asp:HiddenField ID="Hdnmemberid" Value='<%# Bind("MEMBERID")%>'  runat="server"  />  
               
               
              
   <asp:Button runat="server"  ID="btnview" OnClick="lnkView_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
        </asp:GridView>
    </div>
     <div>
        <asp:GridView ID="GVEventt" runat="server" align="center" onrowdatabound="GVEventt_RowDataBound1" 
          
              AllowPaging="true" PageSize="50" 
             onpageindexchanging="GVEventt_PageIndexChanging" >
              <PagerSettings PageButtonCount="30" />
              <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
               <asp:HiddenField ID="HdnEvent" Value='<%# Bind("EventName")%>'  runat="server"  /> 
                <asp:HiddenField ID="Hdnmember" Value='<%# Bind("MemberId")%>'  runat="server"  /> 
   <asp:Button runat="server"  ID="btnviewEvent" OnClick="lnkviewEvent_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
          
        </asp:GridView>
    </div>
         <div>
        <asp:GridView ID="GridViewforall" runat="server" align="center" onrowdatabound="GridViewforall_RowDataBound" 
          
              AllowPaging="true" PageSize="50" onpageindexchanging="GridViewforall_PageIndexChanging" 
              >
              <PagerSettings PageButtonCount="30" />
              <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
               <asp:HiddenField ID="HdnEventidall" Value='<%# Bind("EventName")%>'  runat="server"  /> 
               
   <asp:Button runat="server"  ID="btnviewEventall" OnClick="lnkviewEventall_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
          
        </asp:GridView>
    </div>
            <div>
        <asp:GridView ID="Gridchapterall" runat="server" align="center" 
          
              AllowPaging="true" PageSize="50" 
                    onpageindexchanging="Gridchapterall_PageIndexChanging" onrowdatabound="Gridchapterall_RowDataBound" 
              >
              <PagerSettings PageButtonCount="30" />
              <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
               <asp:HiddenField ID="Hdnchaptidall" Value='<%# Bind("ChapterId")%>'  runat="server"  /> 
               
   <asp:Button runat="server"  ID="btnviewchapttall" OnClick="lnkviewEventall_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
          
        </asp:GridView>
    </div>
    
      <div>
        <asp:GridView ID="GridViewchapter"  runat="server" align="center" onrowdatabound="GridViewchapter_RowDataBound" 
          
             AllowPaging="true" PageSize="50" 
              onpageindexchanging="GridViewchapter_PageIndexChanging1"  >
                <PagerSettings PageButtonCount="30" />
                <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
             <asp:HiddenField ID="Hdnchapterid" Value='<%# Bind("ChapterId")%>'  runat="server"  /> 
               <asp:HiddenField ID="Hdnmember" Value='<%# Bind("MemberId")%>'  runat="server"  /> 
   <asp:Button runat="server"  ID="btnviewchapter" OnClick="lnkviewChapter_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
            
          
        </asp:GridView>
    </div>
     <div>
        <asp:GridView ID="GVindividual" AutoGenerateColumns="false"  runat="server" align="center" 
             onrowdatabound="GVindividual_RowDataBound" AllowPaging="true" PageSize="50" DataKeyNames="TRANSACTION_NUMBER" 
             onpageindexchanging="GVindividual_PageIndexChanging" 
             onrowcancelingedit="GVindividual_RowCancelingEdit" 
             onrowediting="GVindividual_RowEditing" 
             onrowupdating="GVindividual_RowUpdating"  >
                       <Columns> 
             
             <asp:TemplateField>
 
      <ItemTemplate>
      <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />

      </ItemTemplate>
 
      <EditItemTemplate>
       <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" /> 

<asp:Button  ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
   
      </EditItemTemplate>
 
      </asp:TemplateField>
 
        <asp:TemplateField HeaderText="Donorname">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblId"   Text='<%#Eval("Donorname") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
 
   <asp:TemplateField HeaderText="MemberID">
     <ItemTemplate>
 <asp:HiddenField ID="Hdnmemberdedit" Value='<%# Bind("MemberId")%>'  runat="server"  />
           <asp:Label runat="server" ID="lblmembid"   Text='<%#Eval("MemberId") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
 
                <asp:TemplateField HeaderText="TRANSACTION_NUMBER">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbtransca"   Text='<%#Eval("TRANSACTION_NUMBER") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
         <asp:TemplateField HeaderText="DonorType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbdonor"   Text='<%#Eval("DonorType") %>' />
     </ItemTemplate>
    
</asp:TemplateField>


         <asp:TemplateField HeaderText="DonationDate">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbdnationdate"   Text='<%#Eval("DonationDate") %>' />
     </ItemTemplate>
    
</asp:TemplateField>

    <asp:TemplateField HeaderText="Amount">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbeventamt"   Text='<%#Eval("Amount") %>' />
            
     </ItemTemplate>
    
</asp:TemplateField>
    <asp:TemplateField HeaderText="EventYear">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbeventyear"   Text='<%#Eval("EventYear") %>' />
     </ItemTemplate>
     <EditItemTemplate> 
      &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <asp:Label runat="server" ID="lbeventyear"    Text='<%#Eval("EventYear") %>' />
         <asp:DropDownList ID="ddyear" runat="server" AutoPostBack="false"  OnPreRender="ddyear" >
         </asp:DropDownList>
        
          <asp:TextBox Visible="false"  runat="server" ID="TxtEddyear" Text='<%#Eval("EventYear") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>
    <asp:TemplateField HeaderText="Context">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbevent"   Text='<%#Eval("event") %>' />
     </ItemTemplate>
      <EditItemTemplate>
         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:Label runat="server" ID="lbevent"   Text='<%#Eval("event") %>' />
         <asp:DropDownList ID="DDEvent1" runat="server" AutoPostBack="false"  OnPreRender="Event" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="TxtEvente" Text='<%#Eval("event") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>

   <asp:TemplateField HeaderText="EventID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbevent1"   Text='<%#Eval("EventID") %>' />
     </ItemTemplate>
   
    
</asp:TemplateField>


    <asp:TemplateField HeaderText="DepositDate">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbdeposite"   Text='<%#Eval("DepositDate") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
    <asp:TemplateField HeaderText="PURPOSE">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbpurpose"   Text='<%#Eval("PURPOSE") %>' />
     </ItemTemplate>
     <EditItemTemplate>
      &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:Label runat="server" ID="lbpurpose"   Text='<%#Eval("PURPOSE") %>' />
         <asp:DropDownList ID="DDpurpose" runat="server" AutoPostBack="false"  OnPreRender="purpose" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="Txtpurpose" Text='<%#Eval("PURPOSE") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>
    <asp:TemplateField HeaderText="	chaptercode">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbchapter"   Text='<%#Eval("chaptercode") %>' />
     </ItemTemplate>
      <EditItemTemplate>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <asp:Label runat="server" ID="lbchapter"  Text='<%#Eval("chaptercode") %>' />
         <asp:DropDownList ID="DDfrmchaptercode" runat="server" AutoPostBack="false"  OnPreRender="ddchaptercode" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="TxtfrmChaptercode" Text='<%#Eval("chaptercode") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>



    <asp:TemplateField HeaderText="BankName">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbbankt"   Text='<%#Eval("BankName") %>' />
     </ItemTemplate>
       <EditItemTemplate>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:Label runat="server" ID="lbbankt"   Text='<%#Eval("BankName") %>' />
         <asp:DropDownList ID="DDfbank" runat="server" AutoPostBack="false"  OnPreRender="ddBankid" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="Txtbankdep" Text='<%#Eval("BankId") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>
      

    <asp:TemplateField HeaderText="Project">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbprj"   Text='<%#Eval("Project") %>' />
     </ItemTemplate>
    
</asp:TemplateField>

    <asp:TemplateField HeaderText="DonationType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbdonatype"   Text='<%#Eval("DonationType") %>' />
     </ItemTemplate>
      <EditItemTemplate>
       &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:Label runat="server" ID="lbdonatype"   Text='<%#Eval("DonationType") %>' />
         <asp:DropDownList ID="DDdonationtype" runat="server" AutoPostBack="false"  OnPreRender="donationtype" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="Txtdonationtype" Text='<%#Eval("donationtype") %>' />
              </EditItemTemplate>
    
</asp:TemplateField>
<%--      <asp:TemplateField HeaderText="EventYear">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblyear"   Text='<%#Eval("EventYear") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="Txtyear" Text='<%#Eval("EventYear") %>' />
              </EditItemTemplate>
</asp:TemplateField>
--%>



 
        </Columns>
          
        </asp:GridView>
      
         <asp:TextBox ID="Txthidden" Visible="false" runat="server"></asp:TextBox>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
        
    </div>
</asp:Content>

