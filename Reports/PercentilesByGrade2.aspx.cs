using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_PercentilesByGrade2 : System.Web.UI.Page
{
    string Year;
    string contestType;
    DataSet dsContestants;
    DataTable dt2 = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Year = DateTime.Now.Year.ToString(); //System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        // Put user code to initialize the page here
        contestType = ddlContest.SelectedValue;
        ReadAllScores();
    }
    void ReadAllScores()
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select ChildNumber, ContestCode, contestant_id, " +
            "Score1, Score2, Rank, BadgeNumber,ContestCategoryID, GRADE  from Contestant " +
            " Where Score1 IS NOT NULL AND Score2 IS NOT NULL  AND ContestYear=" +
            Year + " AND (Score1+Score2)>0 AND ProductCode = '" + contestType + "' AND EventId=2 ORDER BY ChapterID";

        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        
        ProcessData(dsContestants);
        
    }

    void ProcessData(DataSet ds)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Contestant", typeof(int));
        dt.Columns.Add("score", typeof(double));
        dt.Columns.Add("Grade", typeof(int));

        double[] aryTscore = new double[ds.Tables[0].Rows.Count];
        double[] grScore = new double[ds.Tables[0].Rows.Count];
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
            dr["score"] = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[4]);
            dr["Grade"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            grScore[i] = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[8]);
            aryTscore[i] = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[4]);
            dt.Rows.Add(dr);
        }

        SortArrays(aryTscore, grScore);

        dt2 = new DataTable();
        dt2.Columns.Add("score", typeof(double));
        switch (contestType)
        {
            case "MB1":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr1-%tile", typeof(string));
                dt2.Columns.Add("Gr1-Count", typeof(string));
                dt2.Columns.Add("Gr1-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr2-%tile", typeof(string));
                dt2.Columns.Add("Gr2-Count", typeof(string));
                dt2.Columns.Add("Gr2-CumeCnt", typeof(string));
                break;
            case "MB2":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                break;
            case "MB3":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "MB4":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr9-%tile", typeof(string));
                dt2.Columns.Add("Gr9-Count", typeof(string));
                dt2.Columns.Add("Gr9-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr10-%tile", typeof(string));
                dt2.Columns.Add("Gr10-Count", typeof(string));
                dt2.Columns.Add("Gr10-CumeCnt", typeof(string));
                break;
            case "JSB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr1-%tile", typeof(string));
                dt2.Columns.Add("Gr1-Count", typeof(string));
                dt2.Columns.Add("Gr1-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr2-%tile", typeof(string));
                dt2.Columns.Add("Gr2-Count", typeof(string));
                dt2.Columns.Add("Gr2-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                break;
            case "SSB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "JVB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr1-%tile", typeof(string));
                dt2.Columns.Add("Gr1-Count", typeof(string));
                dt2.Columns.Add("Gr1-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr2-%tile", typeof(string));
                dt2.Columns.Add("Gr2-Count", typeof(string));
                dt2.Columns.Add("Gr2-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                break;
            case "IVB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "SVB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr9-%tile", typeof(string));
                dt2.Columns.Add("Gr9-Count", typeof(string));
                dt2.Columns.Add("Gr9-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr10-%tile", typeof(string));
                dt2.Columns.Add("Gr10-Count", typeof(string));
                dt2.Columns.Add("Gr10-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr11-%tile", typeof(string));
                dt2.Columns.Add("Gr11-Count", typeof(string));
                dt2.Columns.Add("Gr11-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr12-%tile", typeof(string));
                dt2.Columns.Add("Gr12-Count", typeof(string));
                dt2.Columns.Add("Gr12-CumeCnt", typeof(string));
                break;
            case "JGB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr1-%tile", typeof(string));
                dt2.Columns.Add("Gr1-Count", typeof(string));
                dt2.Columns.Add("Gr1-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr2-%tile", typeof(string));
                dt2.Columns.Add("Gr2-Count", typeof(string));
                dt2.Columns.Add("Gr2-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                break;
            case "SGB":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "EW1":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                break;
            case "EW2":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "EW3":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr9-%tile", typeof(string));
                dt2.Columns.Add("Gr9-Count", typeof(string));
                dt2.Columns.Add("Gr9-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr10-%tile", typeof(string));
                dt2.Columns.Add("Gr10-Count", typeof(string));
                dt2.Columns.Add("Gr10-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr11-%tile", typeof(string));
                dt2.Columns.Add("Gr11-Count", typeof(string));
                dt2.Columns.Add("Gr11-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr12-%tile", typeof(string));
                dt2.Columns.Add("Gr12-Count", typeof(string));
                dt2.Columns.Add("Gr12-CumeCnt", typeof(string));
                break;
            case "PS1":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
            case "PS3":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr9-%tile", typeof(string));
                dt2.Columns.Add("Gr9-Count", typeof(string));
                dt2.Columns.Add("Gr9-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr10-%tile", typeof(string));
                dt2.Columns.Add("Gr10-Count", typeof(string));
                dt2.Columns.Add("Gr10-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr11-%tile", typeof(string));
                dt2.Columns.Add("Gr11-Count", typeof(string));
                dt2.Columns.Add("Gr11-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr12-%tile", typeof(string));
                dt2.Columns.Add("Gr12-Count", typeof(string));
                dt2.Columns.Add("Gr12-CumeCnt", typeof(string));
                break;
            case "JSC":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr1-%tile", typeof(string));
                dt2.Columns.Add("Gr1-Count", typeof(string));
                dt2.Columns.Add("Gr1-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr2-%tile", typeof(string));
                dt2.Columns.Add("Gr2-Count", typeof(string));
                dt2.Columns.Add("Gr2-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr3-%tile", typeof(string));
                dt2.Columns.Add("Gr3-Count", typeof(string));
                dt2.Columns.Add("Gr3-CumeCnt", typeof(string));
                break;
            case "ISC":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr4-%tile", typeof(string));
                dt2.Columns.Add("Gr4-Count", typeof(string));
                dt2.Columns.Add("Gr4-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr5-%tile", typeof(string));
                dt2.Columns.Add("Gr5-Count", typeof(string));
                dt2.Columns.Add("Gr5-CumeCnt", typeof(string));
                break;
            case "SSC":
                dt2.Columns.Add("All-%tile", typeof(string));
                dt2.Columns.Add("All-Count", typeof(string));
                dt2.Columns.Add("All-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr6-%tile", typeof(string));
                dt2.Columns.Add("Gr6-Count", typeof(string));
                dt2.Columns.Add("Gr6-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr7-%tile", typeof(string));
                dt2.Columns.Add("Gr7-Count", typeof(string));
                dt2.Columns.Add("Gr7-CumeCnt", typeof(string));
                dt2.Columns.Add("Gr8-%tile", typeof(string));
                dt2.Columns.Add("Gr8-Count", typeof(string));
                dt2.Columns.Add("Gr8-CumeCnt", typeof(string));
                break;
        }


        dr = dt2.NewRow();
        if (aryTscore.Length > 0)
            dr["score"] = aryTscore[0];

        int[] grCount = new int[13];
        int[] grCCount = new int[13];
        int[] grTCount = new int[13];
        int[] agrCount = new int[13];
        float[] pc = new float[13];
        float[] pcile = new float[13];
        for (int k = 0; k < grScore.Length; k++)
        {
            //switch (grScore[k])
            //{
                //case 0:
                //case 1:
            if(grScore[k] >= -1 && grScore[k] <= 1.5)
                    grTCount[1]++;
            else if(grScore[k]> 1.5 && grScore[k] <2.5)
                    grTCount[2]++;
            else if (grScore[k] >2.5 && grScore[k] <3.5)
                    grTCount[3]++;
            else if(grScore[k]>3.5 && grScore[k] <4.5)
                    grTCount[4]++;
            else if (grScore[k] > 4.5 && grScore[k] <= 5.5)
                    grTCount[5]++;
            else if (grScore[k] >5.5 && grScore[k] <6.5)
                    grTCount[6]++;
            else if (grScore[k] > 6.5 && grScore[k] < 7.5)
                    grTCount[7]++;
            else if (grScore[k] > 7.5 && grScore[k] < 8.5)
                    grTCount[8]++;
            else if (grScore[k] > 8.5 && grScore[k] < 9.5)
                    grTCount[9]++;
            else if(grScore[k] > 9.5 && grScore[k] < 10.5)
                    grTCount[10]++;
            else if (grScore[k] >10.5 && grScore[k] < 11.5)
                    grTCount[11]++;
            else if (grScore[k] > 11.5)
                    grTCount[12]++;
                       //}
        }
        double lastScore = 0;
        if (aryTscore.Length > 0)
            lastScore = aryTscore[0];

        int j;
        for (j = 0; j < aryTscore.Length; j++)
        {
            if (lastScore == aryTscore[j])
            {
                if ((grScore[j] == 1) || (grScore[j] == 0) || (grScore[j] == -1))
                {
                    grCount[1] = grCount[1] + 1;
                }
                if (grScore[j] == 2)
                {
                    grCount[2] = grCount[2] + 1;
                }
                if (grScore[j] == 3)
                {
                    grCount[3] = grCount[3] + 1;
                }
                if (grScore[j] == 4)
                {
                    grCount[4] = grCount[4] + 1;
                }
                if (grScore[j] == 5)
                {
                    grCount[5] = grCount[5] + 1;
                }
                if (grScore[j] == 6)
                {
                    grCount[6] = grCount[6] + 1;
                }
                if (grScore[j] == 7)
                {
                    grCount[7] = grCount[7] + 1;
                }
                if (grScore[j] == 8)
                {
                    grCount[8] = grCount[8] + 1;
                }
                if (grScore[j] == 9)
                {
                    grCount[9] = grCount[9] + 1;
                }
                if (grScore[j] == 10)
                {
                    grCount[10] = grCount[10] + 1;
                }
                if (grScore[j] == 11)
                {
                    grCount[11] = grCount[11] + 1;
                }
                if (grScore[j] == 12)
                {
                    grCount[12] = grCount[12] + 1;
                }
            }
            else
            { 
               
                lastScore = aryTscore[j];

                switch (contestType)
                {
                    case "MB1":
                        //	pc[1] = (float)grCount[1]/grTCount[1];
                        //	dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));

                        pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                        dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                        dr["Gr1-Count"] = grCount[1];
                        dr["Gr1-CumeCnt"] = agrCount[1] = grTCount[1] - grCCount[1];
                        grCCount[1] += grCount[1];
                        //pc[2] = (float)grCount[2]/grTCount[2];
                        //	dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                        pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                        dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                        dr["Gr2-Count"] = grCount[2];
                        dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                        grCCount[2] += grCount[2];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2]) / 2, 3)); ;
                        dr["All-Count"] = grCount[1] + grCount[2];
                        dr["All-CumeCnt"] = agrCount[1] + agrCount[2];
                        break;
                    case "MB2":
                    case "EW1":
                        //	pc[3] = (float)grCount[3]/grTCount[3];
                        //	dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                        pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                        dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                        dr["Gr3-Count"] = grCount[3];
                        dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                        grCCount[3] += grCount[3];
                        //	pc[4] = (float)grCount[4]/grTCount[4];
                        //	dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                        pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                        dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                        dr["Gr4-Count"] = grCount[4];
                        dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                        grCCount[4] += grCount[4];
                        //	pc[5] = (float)grCount[5]/grTCount[5];
                        //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                        pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                        dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                        dr["Gr5-Count"] = grCount[5];
                        dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                        grCCount[5] += grCount[5];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[3] + pcile[4] + pcile[5]) / 3, 3)); ;
                        dr["All-Count"] = grCount[3] + grCount[4] + grCount[5];
                        dr["All-CumeCnt"] = agrCount[3] + agrCount[4] + agrCount[5];
                        break;
                    case "ISC":
                        
                        pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                        dr["Gr4-%tile"] = String.Format("{0:F1}", 100 * Math.Round(pcile[4], 3));
                        dr["Gr4-Count"] = grCount[4];
                        dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                        grCCount[4] += grCount[4];
                        //	pc[5] = (float)grCount[5]/grTCount[5];
                        //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                        pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                        dr["Gr5-%tile"] = String.Format("{0:F1}", 100 * Math.Round(pcile[5], 3));
                        dr["Gr5-Count"] = grCount[5];
                        dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                        grCCount[5] += grCount[5];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 * Math.Round((pcile[4] + pcile[5]) / 2, 3)); ;
                        dr["All-Count"] =  grCount[4] + grCount[5];
                        dr["All-CumeCnt"] =  agrCount[4] + agrCount[5];
                        break;
                    case "MB3":
                    case "SSC":
                    case "EW2":
                    case "PS1":
                        //	pc[6] = (float)grCount[6]/grTCount[6];
                        //	dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                        pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                        dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                        dr["Gr6-Count"] = grCount[6];
                        dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                        grCCount[6] += grCount[6];
                        //	pc[7] = (float)grCount[7]/grTCount[7];
                        //	dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                        pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                        dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                        dr["Gr7-Count"] = grCount[7];
                        dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                        grCCount[7] += grCount[7];
                        //	pc[8] = (float)grCount[8]/grTCount[8];
                        //	dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                        pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                        dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                        dr["Gr8-Count"] = grCount[8];
                        dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                        grCCount[8] += grCount[8];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[6] + pcile[7] + pcile[8]) / 3, 3)); ;
                        dr["All-Count"] = grCount[6] + grCount[7] + grCount[8];
                        dr["All-CumeCnt"] = agrCount[6] + agrCount[7] + agrCount[8];
                        break;
                    case "MB4":
                        //	pc[9] = (float)grCount[9]/grTCount[9];
                        //	dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                        pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                        dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                        dr["Gr9-Count"] = grCount[9];
                        dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                        grCCount[9] += grCount[9];
                        //	pc[10] = (float)grCount[10]/grTCount[10];
                        //	dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                        pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                        dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                        dr["Gr10-Count"] = grCount[10];
                        dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                        grCCount[10] += grCount[10];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10]) / 2, 3)); ;
                        dr["All-Count"] = grCount[9] + grCount[10];
                        dr["All-CumeCnt"] = agrCount[9] + agrCount[10];
                        break;

                    case "JSB":
                    case "JSC":
                    case "JGB":
                       
                        //	pc[1] = (float)grCount[1]/grTCount[1];
                        //	dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));
                        pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                        dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                        dr["Gr1-Count"] = grCount[1];
                        dr["Gr1-CumeCnt"] = agrCount[1] = (grTCount[1] - grCCount[1]);
                        grCCount[1] += grCount[1];
                        //	pc[2] = (float)grCount[2]/grTCount[2];
                        //	dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                        pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                        dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                        dr["Gr2-Count"] = grCount[2];
                        dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                        grCCount[2] += grCount[2];
                        //	pc[3] = (float)grCount[3]/grTCount[3];
                        //	dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                        pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                        dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                        dr["Gr3-Count"] = grCount[3];
                        dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                        grCCount[3] += grCount[3];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2] + pcile[3]) / 3, 3)); ;
                        dr["All-Count"] = grCount[1] + grCount[2] + grCount[3];
                        dr["All-CumeCnt"] = agrCount[1] + agrCount[2] + agrCount[3];
                        break;
                    case "SSB":
                    case "SGB":
                        //	pc[4] = (float)grCount[4]/grTCount[4];
                        //	dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                        pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                        dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                        dr["Gr4-Count"] = grCount[4];
                        dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                        grCCount[4] += grCount[4];
                        //	pc[5] = (float)grCount[5]/grTCount[5];
                        //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                        pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                        dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                        dr["Gr5-Count"] = grCount[5];
                        dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                        grCCount[5] += grCount[5];
                        //	pc[6] = (float)grCount[6]/grTCount[6];
                        //	dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                        pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                        dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                        dr["Gr6-Count"] = grCount[6];
                        dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                        grCCount[6] += grCount[6];
                        //	pc[7] = (float)grCount[7]/grTCount[7];
                        //	dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                        pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                        dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                        dr["Gr7-Count"] = grCount[7];
                        dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                        grCCount[7] += grCount[7];
                        //	pc[8] = (float)grCount[8]/grTCount[8];
                        //	dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                        pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                        dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                        dr["Gr8-Count"] = grCount[8];
                        dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                        grCCount[8] += grCount[8];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[4] + pcile[5] + pcile[6] + pcile[7] + pcile[8]) / 5, 3)); ;
                        dr["All-Count"] = grCount[4] + grCount[5] + grCount[6] + grCount[7] + grCount[8];
                        dr["All-CumeCnt"] = agrCount[4] + agrCount[5] + agrCount[6] + agrCount[7] + agrCount[8];
                        break;
                    case "JVB":
                        //		pc[1] = (float)grCount[1]/grTCount[1];
                        //		dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));

                        pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                        dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                        dr["Gr1-Count"] = grCount[1];
                        dr["Gr1-CumeCnt"] = agrCount[1] = grTCount[1] - grCCount[1];
                        grCCount[1] += grCount[1];
                        //		pc[2] = (float)grCount[2]/grTCount[2];
                        //		dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                        pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                        dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                        dr["Gr2-Count"] = grCount[2];
                        dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                        grCCount[2] += grCount[2];
                        //		pc[3] = (float)grCount[3]/grTCount[3];
                        //		dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                        pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                        dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                        dr["Gr3-Count"] = grCount[3];
                        dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                        grCCount[3] += grCount[3];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2] + pcile[3]) / 3, 3)); ;
                        dr["All-Count"] = grCount[1] + grCount[2] + grCount[3];
                        dr["All-CumeCnt"] = agrCount[1] + agrCount[2] + agrCount[3];
                        break;
                    case "IVB":
                        //		pc[4] = (float)grCount[4]/grTCount[4];
                        //		dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                        pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                        dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                        dr["Gr4-Count"] = grCount[4];
                        dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                        grCCount[4] += grCount[4];
                        //		pc[5] = (float)grCount[5]/grTCount[5];
                        //		dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                        pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                        dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                        dr["Gr5-Count"] = grCount[5];
                        dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                        grCCount[5] += grCount[5];
                        //		pc[6] = (float)grCount[6]/grTCount[6];
                        //		dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                        pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                        dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                        dr["Gr6-Count"] = grCount[6];
                        dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                        grCCount[6] += grCount[6];
                        //		pc[7] = (float)grCount[7]/grTCount[7];
                        //		dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                        pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                        dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                        dr["Gr7-Count"] = grCount[7];
                        dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                        grCCount[7] += grCount[7];
                        //		pc[8] = (float)grCount[8]/grTCount[8];
                        //		dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                        pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                        dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                        dr["Gr8-Count"] = grCount[8];
                        dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                        grCCount[8] += grCount[8];
                        dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[4] + pcile[5] + pcile[6] + pcile[7]) / 4, 3)); ;
                        dr["All-Count"] = grCount[4] + grCount[5] + grCount[6] + grCount[7];
                        dr["All-CumeCnt"] = agrCount[4] + agrCount[5] + agrCount[6] + agrCount[7];
                        break;
                    case "SVB":
                        //		pc[9] = (float)grCount[9]/grTCount[9];
                        //		dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                        pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                        dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                        dr["Gr9-Count"] = grCount[9];
                        dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                        grCCount[9] += grCount[9];
                        //		pc[10] = (float)grCount[10]/grTCount[10];
                        //		dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                        pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                        dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                        dr["Gr10-Count"] = grCount[10];
                        dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                        grCCount[10] += grCount[10];
                        //	pc[11] = (float)grCount[11]/grTCount[11];
                        //	dr["Gr11%"] =String.Format("{0:F1}", 100 * Math.Round(pc[11],3));

                        pcile[11] = (float)(grTCount[11] - grCCount[11]) / grTCount[11];
                        dr["Gr11-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[11], 3));
                        dr["Gr11-Count"] = grCount[11];
                        dr["Gr11-CumeCnt"] = agrCount[11] = grTCount[11] - grCCount[11];
                        grCCount[11] += grCount[11];
                        if (grTCount[12] == 0)
                        {
                            //		pc[12] = 0;
                            //		dr["Gr12%"] = "";
                            pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                            dr["Gr12-%tile"] = "";
                            dr["Gr12-Count"] = grCount[12];
                            dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                            grCCount[12] += grCount[12];
                        }
                        else
                        {
                            //	pc[12] = (float)grCount[12]/grTCount[12];
                            //	dr["Gr12%"] =String.Format("{0:F1}", 100 * Math.Round(pc[12],3));

                            pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                            dr["Gr12-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[12], 3));
                            dr["Gr12-Count"] = grCount[12];
                            dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                            grCCount[12] += grCount[12];
                        }
                        if (grTCount[12] == 0)
                            dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[8] + pcile[9] + pcile[10] + pcile[11]) / 4, 3));
                        else
                            dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[8] + pcile[9] + pcile[10] + pcile[11] + pcile[12]) / 5, 3));
                        dr["All-Count"] = grCount[8] + grCount[9] + grCount[10] + grCount[11] + grCount[12];
                        dr["All-CumeCnt"] = agrCount[8] + agrCount[9] + agrCount[10] + agrCount[11] + agrCount[12];
                        break;
                    case "EW3":
                    case "PS3":
                        //	pc[9] = (float)grCount[9]/grTCount[9];
                        //	dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                        pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                        dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                        dr["Gr9-Count"] = grCount[9];
                        dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                        grCCount[9] += grCount[9];
                        //	pc[10] = (float)grCount[10]/grTCount[10];
                        //	dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                        pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                        dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                        dr["Gr10-Count"] = grCount[10];
                        dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                        grCCount[10] += grCount[10];
                        //	pc[11] = (float)grCount[11]/grTCount[11];
                        //	dr["Gr11%"] =String.Format("{0:F1}", 100 * Math.Round(pc[11],3));
                        pcile[11] = (float)(grTCount[11] - grCCount[11]) / grTCount[11];
                        dr["Gr11-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[11], 3));
                        dr["Gr11-Count"] = grCount[11];
                        dr["Gr11-CumeCnt"] = agrCount[11] = grTCount[11] - grCCount[11];
                        grCCount[11] += grCount[11];
                        if (grTCount[12] == 0)
                        {
                            //		pc[12] = 0;
                            //		dr["Gr12%"] = "";
                            dr["Gr12-%tile"] = "";
                            pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                        }
                        else
                        {
                            //		pc[12] = (float)grCount[12]/grTCount[12];
                            //		dr["Gr12%"] =String.Format("{0:F1}", 100 * Math.Round(pc[12],3));

                            pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                            dr["Gr12-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[12], 3));
                        }
                        dr["Gr12-Count"] = grCount[12];
                        dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                        grCCount[12] += grCount[12];
                        if (grTCount[12] == 0)
                            dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10] + pcile[11]) / 3, 3));
                        else
                            dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10] + pcile[11] + pcile[12]) / 4, 3));
                        dr["All-Count"] = grCount[9] + grCount[10] + grCount[11] + grCount[12];
                        dr["All-CumeCnt"] = agrCount[9] + agrCount[10] + agrCount[11] + agrCount[12];
                        break;
                }
                dt2.Rows.Add(dr);
                dr = dt2.NewRow();
                dr["score"] = aryTscore[j];
                for (int q = 0; q < 13; q++)
                    grCount[q] = 0;
                if ((grScore[j] == 1) || (grScore[j] == 0) || (grScore[j] == -1))
                {
                    grCount[1] = 1;
                }
                if (grScore[j] == 2)
                {
                    grCount[2] = 1;
                }
                if (grScore[j] == 3)
                {
                    grCount[3] = 1;
                }
                if (grScore[j] == 4)
                {
                    grCount[4] = 1;
                }
                if (grScore[j] == 5)
                {
                    grCount[5] = 1;
                }
                if (grScore[j] == 6)
                {
                    grCount[6] = 1;
                }
                if (grScore[j] == 7)
                {
                    grCount[7] = 1;
                }
                if (grScore[j] == 8)
                {
                    grCount[8] = 1;
                }
                if (grScore[j] == 9)
                {
                    grCount[9] = 1;
                }
                if (grScore[j] == 10)
                {
                    grCount[10] = 1;
                }
                if (grScore[j] == 11)
                {
                    grCount[11] = 1;
                }
                if (grScore[j] == 12)
                {
                    grCount[12] = 1;
                }
            }
        }

        switch (contestType)
        {
            case "MB1":
                //	pc[1] = (float)grCount[1]/grTCount[1];
                //	dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));

                pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                dr["Gr1-Count"] = grCount[1];
                dr["Gr1-CumeCnt"] = agrCount[1] = grTCount[1] - grCCount[1];
                grCCount[1] += grCount[1];
                //pc[2] = (float)grCount[2]/grTCount[2];
                //	dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                dr["Gr2-Count"] = grCount[2];
                dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                grCCount[2] += grCount[2];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2]) / 2, 3)); ;
                dr["All-Count"] = grCount[1] + grCount[2];
                dr["All-CumeCnt"] = agrCount[1] + agrCount[2];
                break;
            case "MB2":
            case "EW1":
                //	pc[3] = (float)grCount[3]/grTCount[3];
                //	dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                dr["Gr3-Count"] = grCount[3];
                dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                grCCount[3] += grCount[3];
                //	pc[4] = (float)grCount[4]/grTCount[4];
                //	dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                dr["Gr4-Count"] = grCount[4];
                dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                grCCount[4] += grCount[4];
                //	pc[5] = (float)grCount[5]/grTCount[5];
                //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                dr["Gr5-Count"] = grCount[5];
                dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                grCCount[5] += grCount[5];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[3] + pcile[4] + pcile[5]) / 3, 3)); ;
                dr["All-Count"] = grCount[3] + grCount[4] + grCount[5];
                dr["All-CumeCnt"] = agrCount[3] + agrCount[4] + agrCount[5];
                break;
            case "ISC":
                pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                dr["Gr4-%tile"] = String.Format("{0:F1}", 100 * Math.Round(pcile[4], 3));
                dr["Gr4-Count"] = grCount[4];
                dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                grCCount[4] += grCount[4];
                //	pc[5] = (float)grCount[5]/grTCount[5];
                //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                dr["Gr5-%tile"] = String.Format("{0:F1}", 100 * Math.Round(pcile[5], 3));
                dr["Gr5-Count"] = grCount[5];
                dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                grCCount[5] += grCount[5];
                dr["All-%tile"] = String.Format("{0:F1}", 100 * Math.Round((pcile[4] + pcile[5]) / 2, 3)); ;
                dr["All-Count"] =  grCount[4] + grCount[5];
                dr["All-CumeCnt"] = agrCount[4] + agrCount[5];
                break;
            case "MB3":
            case "SSC":
            case "EW2":
            case "PS1":
                //	pc[6] = (float)grCount[6]/grTCount[6];
                //	dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                dr["Gr6-Count"] = grCount[6];
                dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                grCCount[6] += grCount[6];
                //	pc[7] = (float)grCount[7]/grTCount[7];
                //	dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                dr["Gr7-Count"] = grCount[7];
                dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                grCCount[7] += grCount[7];
                //	pc[8] = (float)grCount[8]/grTCount[8];
                //	dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                dr["Gr8-Count"] = grCount[8];
                dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                grCCount[8] += grCount[8];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[6] + pcile[7] + pcile[8]) / 3, 3)); ;
                dr["All-Count"] = grCount[6] + grCount[7] + grCount[8];
                dr["All-CumeCnt"] = agrCount[6] + agrCount[7] + agrCount[8];
                break;
            case "MB4":
                //	pc[9] = (float)grCount[9]/grTCount[9];
                //	dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                dr["Gr9-Count"] = grCount[9];
                dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                grCCount[9] += grCount[9];
                //	pc[10] = (float)grCount[10]/grTCount[10];
                //	dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                dr["Gr10-Count"] = grCount[10];
                dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                grCCount[10] += grCount[10];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10]) / 2, 3)); ;
                dr["All-Count"] = grCount[9] + grCount[10];
                dr["All-CumeCnt"] = agrCount[9] + agrCount[10];
                break;

            case "JSB":
            case "JSC":
            case "JGB":
                //	pc[1] = (float)grCount[1]/grTCount[1];
                //	dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));
                pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                dr["Gr1-Count"] = grCount[1];
                dr["Gr1-CumeCnt"] = agrCount[1] = (grTCount[1] - grCCount[1]);
                grCCount[1] += grCount[1];
                //	pc[2] = (float)grCount[2]/grTCount[2];
                //	dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                dr["Gr2-Count"] = grCount[2];
                dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                grCCount[2] += grCount[2];
                //	pc[3] = (float)grCount[3]/grTCount[3];
                //	dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                dr["Gr3-Count"] = grCount[3];
                dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                grCCount[3] += grCount[3];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2] + pcile[3]) / 3, 3)); ;
                dr["All-Count"] = grCount[1] + grCount[2] + grCount[3];
                dr["All-CumeCnt"] = agrCount[1] + agrCount[2] + agrCount[3];

                break;
            case "SSB":
            case "SGB":
                //	pc[4] = (float)grCount[4]/grTCount[4];
                //	dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                dr["Gr4-Count"] = grCount[4];
                dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                grCCount[4] += grCount[4];
                //	pc[5] = (float)grCount[5]/grTCount[5];
                //	dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                dr["Gr5-Count"] = grCount[5];
                dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                grCCount[5] += grCount[5];
                //	pc[6] = (float)grCount[6]/grTCount[6];
                //	dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                dr["Gr6-Count"] = grCount[6];
                dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                grCCount[6] += grCount[6];
                //	pc[7] = (float)grCount[7]/grTCount[7];
                //	dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                dr["Gr7-Count"] = grCount[7];
                dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                grCCount[7] += grCount[7];
                //	pc[8] = (float)grCount[8]/grTCount[8];
                //	dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                dr["Gr8-Count"] = grCount[8];
                dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                grCCount[8] += grCount[8];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[4] + pcile[5] + pcile[6] + pcile[7] + pcile[8]) / 5, 3)); ;
                dr["All-Count"] = grCount[4] + grCount[5] + grCount[6] + grCount[7] + grCount[8];
                dr["All-CumeCnt"] = agrCount[4] + agrCount[5] + agrCount[6] + agrCount[7] + agrCount[8];
                break;
            case "JVB":
                //		pc[1] = (float)grCount[1]/grTCount[1];
                //		dr["Gr1%"] =String.Format("{0:F1}", 100 * Math.Round(pc[1],3));

                pcile[1] = (float)(grTCount[1] - grCCount[1]) / grTCount[1];
                dr["Gr1-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[1], 3));
                dr["Gr1-Count"] = grCount[1];
                dr["Gr1-CumeCnt"] = agrCount[1] = grTCount[1] - grCCount[1];
                grCCount[1] += grCount[1];
                //		pc[2] = (float)grCount[2]/grTCount[2];
                //		dr["Gr2%"] =String.Format("{0:F1}", 100 * Math.Round(pc[2],3));

                pcile[2] = (float)(grTCount[2] - grCCount[2]) / grTCount[2];
                dr["Gr2-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[2], 3));
                dr["Gr2-Count"] = grCount[2];
                dr["Gr2-CumeCnt"] = agrCount[2] = grTCount[2] - grCCount[2];
                grCCount[2] += grCount[2];
                //		pc[3] = (float)grCount[3]/grTCount[3];
                //		dr["Gr3%"] =String.Format("{0:F1}", 100 * Math.Round(pc[3],3));

                pcile[3] = (float)(grTCount[3] - grCCount[3]) / grTCount[3];
                dr["Gr3-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[3], 3));
                dr["Gr3-Count"] = grCount[3];
                dr["Gr3-CumeCnt"] = agrCount[3] = grTCount[3] - grCCount[3];
                grCCount[3] += grCount[3];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[1] + pcile[2] + pcile[3]) / 3, 3));
                dr["All-Count"] = grCount[1] + grCount[2] + grCount[3];
                dr["All-CumeCnt"] = agrCount[1] + agrCount[2] + agrCount[3];
                break;
            case "IVB":
                //		pc[4] = (float)grCount[4]/grTCount[4];
                //		dr["Gr4%"] =String.Format("{0:F1}", 100 * Math.Round(pc[4],3));

                pcile[4] = (float)(grTCount[4] - grCCount[4]) / grTCount[4];
                dr["Gr4-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[4], 3));
                dr["Gr4-Count"] = grCount[4];
                dr["Gr4-CumeCnt"] = agrCount[4] = grTCount[4] - grCCount[4];
                grCCount[4] += grCount[4];
                //		pc[5] = (float)grCount[5]/grTCount[5];
                //		dr["Gr5%"] =String.Format("{0:F1}", 100 * Math.Round(pc[5],3));

                pcile[5] = (float)(grTCount[5] - grCCount[5]) / grTCount[5];
                dr["Gr5-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[5], 3));
                dr["Gr5-Count"] = grCount[5];
                dr["Gr5-CumeCnt"] = agrCount[5] = grTCount[5] - grCCount[5];
                grCCount[5] += grCount[5];
                //		pc[6] = (float)grCount[6]/grTCount[6];
                //		dr["Gr6%"] =String.Format("{0:F1}", 100 * Math.Round(pc[6],3));

                pcile[6] = (float)(grTCount[6] - grCCount[6]) / grTCount[6];
                dr["Gr6-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[6], 3));
                dr["Gr6-Count"] = grCount[6];
                dr["Gr6-CumeCnt"] = agrCount[6] = grTCount[6] - grCCount[6];
                grCCount[6] += grCount[6];
                //		pc[7] = (float)grCount[7]/grTCount[7];
                //		dr["Gr7%"] =String.Format("{0:F1}", 100 * Math.Round(pc[7],3));

                pcile[7] = (float)(grTCount[7] - grCCount[7]) / grTCount[7];
                dr["Gr7-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[7], 3));
                dr["Gr7-Count"] = grCount[7];
                dr["Gr7-CumeCnt"] = agrCount[7] = grTCount[7] - grCCount[7];
                grCCount[7] += grCount[7];
                //		pc[8] = (float)grCount[8]/grTCount[8];
                //		dr["Gr8%"] =String.Format("{0:F1}", 100 * Math.Round(pc[8],3));

                pcile[8] = (float)(grTCount[8] - grCCount[8]) / grTCount[8];
                dr["Gr8-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[8], 3));
                dr["Gr8-Count"] = grCount[8];
                dr["Gr8-CumeCnt"] = agrCount[8] = grTCount[8] - grCCount[8];
                grCCount[8] += grCount[8];
                dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[4] + pcile[5] + pcile[6] + pcile[7]) / 4, 3));
                dr["All-Count"] = grCount[4] + grCount[5] + grCount[6] + grCount[7];
                dr["All-CumeCnt"] = agrCount[4] + agrCount[5] + agrCount[6] + agrCount[7];
                break;
            case "SVB":
                //		pc[9] = (float)grCount[9]/grTCount[9];
                //		dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                dr["Gr9-Count"] = grCount[9];
                dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                grCCount[9] += grCount[9];
                //		pc[10] = (float)grCount[10]/grTCount[10];
                //		dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                dr["Gr10-Count"] = grCount[10];
                dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                grCCount[10] += grCount[10];
                //	pc[11] = (float)grCount[11]/grTCount[11];
                //	dr["Gr11%"] =String.Format("{0:F1}", 100 * Math.Round(pc[11],3));

                pcile[11] = (float)(grTCount[11] - grCCount[11]) / grTCount[11];
                dr["Gr11-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[11], 3));
                dr["Gr11-Count"] = grCount[11];
                dr["Gr11-CumeCnt"] = agrCount[11] = grTCount[11] - grCCount[11];
                grCCount[11] += grCount[11];
                if (grTCount[12] == 0)
                {
                    //		pc[12] = 0;
                    //		dr["Gr12%"] = "";
                    pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                    dr["Gr12-%tile"] = "";
                    dr["Gr12-Count"] = grCount[12];
                    dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                    grCCount[12] += grCount[12];
                }
                else
                {
                    //	pc[12] = (float)grCount[12]/grTCount[12];
                    //	dr["Gr12%"] =String.Format("{0:F1}", 100 * Math.Round(pc[12],3));

                    pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                    dr["Gr12-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[12], 3));
                    dr["Gr12-Count"] = grCount[12];
                    dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                    grCCount[12] += grCount[12];
                }
                if (grTCount[12] == 0)
                    dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[8] + pcile[9] + pcile[10] + pcile[11]) / 4, 3));
                else
                    dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[8] + pcile[9] + pcile[10] + pcile[11] + pcile[12]) / 5, 3)); ;
                dr["All-Count"] = grCount[8] + grCount[9] + grCount[10] + grCount[11] + grCount[12];
                dr["All-CumeCnt"] = agrCount[8] + agrCount[9] + agrCount[10] + agrCount[11] + agrCount[12];
                break;
            case "EW3":
            case "PS3":
                //	pc[9] = (float)grCount[9]/grTCount[9];
                //	dr["Gr9%"] =String.Format("{0:F1}", 100 * Math.Round(pc[9],3));

                pcile[9] = (float)(grTCount[9] - grCCount[9]) / grTCount[9];
                dr["Gr9-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[9], 3));
                dr["Gr9-Count"] = grCount[9];
                dr["Gr9-CumeCnt"] = agrCount[9] = grTCount[9] - grCCount[9];
                grCCount[9] += grCount[9];
                //	pc[10] = (float)grCount[10]/grTCount[10];
                //	dr["Gr10%"] =String.Format("{0:F1}", 100 * Math.Round(pc[10],3));

                pcile[10] = (float)(grTCount[10] - grCCount[10]) / grTCount[10];
                dr["Gr10-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[10], 3));
                dr["Gr10-Count"] = grCount[10];
                dr["Gr10-CumeCnt"] = agrCount[10] = grTCount[10] - grCCount[10];
                grCCount[10] += grCount[10];
                //	pc[11] = (float)grCount[11]/grTCount[11];
                //	dr["Gr11%"] =String.Format("{0:F1}", 100 * Math.Round(pc[11],3));
                pcile[11] = (float)(grTCount[11] - grCCount[11]) / grTCount[11];
                dr["Gr11-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[11], 3));
                dr["Gr11-Count"] = grCount[11];
                dr["Gr11-CumeCnt"] = agrCount[11] = grTCount[11] - grCCount[11];
                grCCount[11] += grCount[11];
                if (grTCount[12] == 0)
                {
                    //		pc[12] = 0;
                    //		dr["Gr12%"] = "";
                    dr["Gr12-%tile"] = "";
                    pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                }
                else
                {
                    //		pc[12] = (float)grCount[12]/grTCount[12];
                    //		dr["Gr12%"] =String.Format("{0:F1}", 100 * Math.Round(pc[12],3));

                    pcile[12] = (float)(grTCount[12] - grCCount[12]) / grTCount[12];
                    dr["Gr12-%tile"] = String.Format("{0:F1}", 100 *  Math.Round(pcile[12], 3));
                }
                dr["Gr12-Count"] = grCount[12];
                dr["Gr12-CumeCnt"] = agrCount[12] = grTCount[12] - grCCount[12];
                grCCount[12] += grCount[12];
                if (grTCount[12] == 0)
                    dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10] + pcile[11]) / 3, 3));
                else
                    dr["All-%tile"] = String.Format("{0:F1}", 100 *  Math.Round((pcile[9] + pcile[10] + pcile[11] + pcile[12]) / 4, 3));
                dr["All-Count"] = grCount[9] + grCount[10] + grCount[11] + grCount[12];
                dr["All-CumeCnt"] = agrCount[9] + agrCount[10] + agrCount[11] + agrCount[12];
                break;
        }
        dt2.Rows.Add(dr);
        DataGrid1.Caption = contestType + " as of " + DateTime.Today.ToShortDateString();
        DataGrid1.DataSource = dt2;
        DataGrid1.DataBind();

    }

    void SortArrays(double[] aryTscore,double[] grScore)
    {
        int x = aryTscore.Length;
        int i;
        int j;
        double temp1, temp2;

        for (i = (x - 1); i >= 0; i--)
        {
            for (j = 1; j <= i; j++)
            {
                if (aryTscore[j - 1] > aryTscore[j])
                {
                    temp1 = aryTscore[j - 1];
                    aryTscore[j - 1] = aryTscore[j];
                    aryTscore[j] = temp1;
                    temp2 = grScore[j - 1];
                    grScore[j - 1] = grScore[j];
                    grScore[j] = temp2;
                }
            }
        }


    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=PercentRankOf" + contestType + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }

}
