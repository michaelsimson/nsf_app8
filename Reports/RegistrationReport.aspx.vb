Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL

Partial Class Reports_RegistrationReport
    Inherits System.Web.UI.Page
    Protected CurrentYear As Int16 = Year(Date.Today())
    Private _iChapterid As Integer
    Private _iYear As Integer
    Private _sChapterName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Param As String
        Param = Request.QueryString("Param")

        If Param = "ByMonth" Then
            PnlByMonth.Visible = True
            'ddYear.SelectedValue = CurrentYear
        ElseIf Param = "ByDate" Then
            PnlByDate.Visible = True
            'ddYear.SelectedValue = CurrentYear
        ElseIf Param = "ByContest" Then
            pnlByContest.Visible = True
            'ddlContestYear.SelectedValue = CurrentYear
            _iChapterid = Request.QueryString("ChapterID")
            _iYear = Request.QueryString("Year")
            If _iYear <> 0 Then
                ddlContestYear.SelectedValue = _iYear
                lblContestHeader.Text = "Request by Contest (" & _sChapterName & ")"
                BindGridContestView()
            Else
                lblContestHeader.Text = "Request by Contest"
            End If
        ElseIf Param = "ByChapter" Then
            pnlByChapter.Visible = True
            'ddlChapterYear.SelectedValue = CurrentYear
        End If

        If Not (Me.IsPostBack) Then
            dvRegistration.Visible = False
            btnExcel.Visible = False
            dvRegistrationbyday.Visible = False
            btndayExcel.Visible = False
            dvregistrationchapter.Visible = False
            btnChapterExcel.Visible = False
        End If
    End Sub
    Private Sub BindGridView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear As Integer
        Try
            If ddMonth.SelectedItem.Value > 0 Or ddYear.SelectedItem.Value > 0 Then
                If (ddMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddMonth.SelectedItem.Value)
                    lblHeader.Text = "Registration by Month"
                Else
                    iMonth = 0
                End If
                If (ddYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddYear.SelectedItem.Value)
                    lblHeader.Text = "Registration by Year"
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If

            Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                   CommandType.StoredProcedure, "rpt_RegistrationByMonth", _
                                   New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth))

            ObjDt = objDS.Tables(0)
            If ObjDt.Rows.Count > 0 Then
                dvRegistration.Visible = True
                btnExcel.Visible = True
                dvRegistration.DataSource = ObjDt
                dvRegistration.DataBind()
            Else
                lblNoData.Text = "No data found."
            End If

        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Try
            BindGridView()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub dvRegistration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvRegistration.RowDataBound
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim iPaid As Integer = 0
            Dim iPending As Integer = 0
            Dim iTotal As Integer = 0
            Dim iAmountPaid As Integer = 0
            Dim iAmountPending As Integer = 0
            Dim iAmountTotal As Integer = 0

            iPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[paid]"))
            iPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Pending]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total]"))
            iAmountPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount paid]"))
            iAmountPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount pending]"))
            iAmountTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total amount]"))
            If iPaid > 0 Then
                e.Row.Cells(3).Text = iPaid.ToString("d")
                e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(3).Text = ""
            End If

            If iPending > 0 Then
                e.Row.Cells(4).Text = iPending.ToString("d")
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If

            If iTotal > 0 Then
                e.Row.Cells(5).Text = iTotal.ToString("d")
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If

            If iAmountPaid > 0 Then
                e.Row.Cells(6).Text = FormatNumber(iAmountPaid.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If

            If iAmountPending > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iAmountPending.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If

            If iAmountTotal > 0 Then
                e.Row.Cells(8).Text = FormatNumber(iAmountTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(8).Text = 0
            End If

        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=RegistrationReport.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvRegistration.RenderControl(htmlWrite)
        'MsgBox(stringWrite.ToString())
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Protected Sub btnDayFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDayFind.Click
        Try
            BindGridDayView()
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridDayView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear As Integer
        Try
            lblDayHeader.Text = "Registration by Day"
            If ddlMonth.SelectedItem.Value > 0 Or ddlYear.SelectedItem.Value > 0 Then
                If (ddlMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddlMonth.SelectedItem.Value)
                Else
                    iMonth = 0
                End If
                If (ddlYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddlYear.SelectedItem.Value)
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If


            Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                   CommandType.StoredProcedure, "rpt_RegistrationByDay", _
                                   New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth))
            ObjDt = objDS.Tables(0)
            If ObjDt.Rows.Count > 0 Then
                dvRegistrationbyday.Visible = True
                btndayExcel.Visible = True
                dvRegistrationbyday.DataSource = ObjDt
                dvRegistrationbyday.DataBind()
            Else
                lblNoData.Text = "No data found."
            End If

        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub dvRegistrationbyday_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvRegistrationbyday.RowDataBound
        e.Row.Cells(0).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim iPaid As Integer = 0
            Dim iPending As Integer = 0
            Dim iTotal As Integer = 0
            Dim iAmountPaid As Integer = 0
            Dim iAmountPending As Integer = 0
            Dim iAmountTotal As Integer = 0

            iPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[paid]"))
            iPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[pending]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total]"))
            iAmountPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount paid]"))
            iAmountPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount pending]"))
            iAmountTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total amount]"))
            If iPaid > 0 Then
                e.Row.Cells(2).Text = iPaid.ToString("d")
                e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(2).Text = ""
            End If
            If iPending > 0 Then
                e.Row.Cells(3).Text = iPending.ToString("d")
                e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(3).Text = ""
            End If
            If iTotal > 0 Then
                e.Row.Cells(4).Text = iTotal.ToString("d")
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If
            If iAmountPaid > 0 Then
                e.Row.Cells(5).Text = FormatNumber(iAmountPaid.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If
            If iAmountPending > 0 Then
                e.Row.Cells(6).Text = FormatNumber(iAmountPending.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If
            If iAmountTotal > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iAmountTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If
        End If
    End Sub

    Protected Sub btndayExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndayExcel.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=RegistrationReport.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvRegistrationbyday.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub btnContestFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContestFind.Click
        Try
            BindGridContestView()
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridContestView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear, iChapterID As Integer
        btnContestExcel.Visible = False
        Try
            iChapterID = 0
            If _iChapterid > 0 Then
                iChapterID = _iChapterid
                _sChapterName = SqlHelper.ExecuteScalar(ConfigurationManager.AppSettings.Item("DBConnection"), CommandType.Text, "select chaptercode from chapter where chapterid = " & iChapterID)
                lblContestHeader.Text = "Request by Contest (" & _sChapterName & ")"
            Else
                lblContestHeader.Text = "Request by Contest"
            End If
            If ddlContestMonth.SelectedItem.Value > 0 Or ddlContestYear.SelectedItem.Value > 0 Then
                If (ddlContestMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddlContestMonth.SelectedItem.Value)
                Else
                    iMonth = 0
                End If
                If (ddlContestYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddlContestYear.SelectedItem.Value)
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If
            If iYear > 0 Then
                Label3.Text = ""
                Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                       CommandType.StoredProcedure, "rpt_RegistrationByContest", _
                                       New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth), New SqlParameter("@chapterid", iChapterid))
                ObjDt = objDS.Tables(0)
                If ObjDt.Rows.Count > 0 Then
                    dvregistrationcontest.Visible = True
                    btnContestExcel.Visible = True
                    dvregistrationcontest.DataSource = ObjDt
                    dvregistrationcontest.DataBind()
                Else
                    dvregistrationcontest.DataSource = Nothing
                    dvregistrationcontest.DataBind()
                    lblNoData.Text = "No data found."
                End If
            Else
                dvregistrationcontest.DataSource = Nothing
                dvregistrationcontest.DataBind()
                Label3.Text = "Please Select Year"
            End If

        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub dvregistrationcontest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvregistrationcontest.RowDataBound
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim sDisplayName As String = ""
            Dim sEventName As String = ""
            Dim iPaid As Integer = 0
            Dim iPending As Integer = 0
            Dim iTotal As Integer = 0
            Dim iAmountPaid As Integer = 0
            Dim iAmountPending As Integer = 0
            Dim iAmountTotal As Integer = 0

            sDisplayName += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "[Name]"))
            sEventName += Convert.ToString(DataBinder.Eval(e.Row.DataItem, "[event name]"))
            iPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[paid]"))
            iPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[pending]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total]"))
            iAmountPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount paid]"))
            iAmountPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount pending]"))
            iAmountTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total amount]"))
            If iPaid > 0 Then
                e.Row.Cells(4).Text = FormatNumber(iPaid.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If
            If iPending > 0 Then
                e.Row.Cells(5).Text = FormatNumber(iPending.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If
            If iTotal > 0 Then
                e.Row.Cells(6).Text = FormatNumber(iTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If
            If iAmountPaid > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iAmountPaid.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If
            If iAmountPending > 0 Then
                e.Row.Cells(8).Text = FormatNumber(iAmountPending.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(8).Text = ""
            End If
            If iAmountTotal > 0 Then
                e.Row.Cells(9).Text = FormatNumber(iAmountTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(9).Text = ""
            End If
        End If
    End Sub

    Protected Sub btnContestExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChapterExcel.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=RegistrationReport.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        ' Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvregistrationchapter.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Protected Sub btnChapterFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChapterFind.Click
        Try
            BindGridChapterView()
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGridChapterView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear, iChapterid As Integer
        Try
            iChapterid = 0
            If ddlChapterMonth.SelectedItem.Value > 0 Or ddlChapterYear.SelectedItem.Value > 0 Then
                If (ddlChapterMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddlChapterMonth.SelectedItem.Value)
                Else
                    iMonth = 0
                End If
                If (ddlChapterYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddlChapterYear.SelectedItem.Value)
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If

            If iYear > 0 Then
                Label4.Text = ""
                Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                       CommandType.StoredProcedure, "rpt_RegistrationByChapter", _
                                       New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth), New SqlParameter("@peventID", ddlChapterEvent.SelectedValue))
                ObjDt = objDS.Tables(0)
                If ObjDt.Rows.Count > 0 Then
                    dvregistrationchapter.Visible = True
                    btnChapterExcel.Visible = True
                    dvregistrationchapter.DataSource = ObjDt
                    dvregistrationchapter.DataBind()
                Else
                    dvregistrationchapter.DataSource = Nothing
                    dvregistrationchapter.DataBind()
                    lblNoData.Text = "No data found."
                End If
            Else
                dvregistrationchapter.DataSource = Nothing
                dvregistrationchapter.DataBind()
                Label4.Text = "Please Select Year"
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

    End Sub
    Protected Sub btnChapterExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChapterExcel.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=RegistrationByChapter.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvregistrationchapter.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Protected Sub dvregistrationchapter_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvregistrationchapter.RowDataBound
        e.Row.Cells(0).Visible = False
        e.Row.Cells(2).Visible = False
        e.Row.Cells(3).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim sDisplayName As String = ""
            Dim iPaid As Integer = 0
            Dim iPending As Integer = 0
            Dim iTotal As Integer = 0
            Dim iAmountPaid As Integer = 0
            Dim iAmountPending As Integer = 0
            Dim iAmountTotal As Integer = 0

            If Convert.ToString(DataBinder.Eval(e.Row.DataItem, "[Name]")) = "Total" Then
                sDisplayName += Convert.ToString("<b>" & DataBinder.Eval(e.Row.DataItem, "[Name]") & "</b>")
            Else
                sDisplayName += Convert.ToString("<a href=RegistrationReport.aspx?Param=ByContest&Year=" & ddlChapterYear.SelectedItem.Value & "&ChapterID=" & DataBinder.Eval(e.Row.DataItem, "[ChapterID]") & ">" & DataBinder.Eval(e.Row.DataItem, "[Name]") & "</a>")
            End If
            e.Row.Cells(1).Text = sDisplayName
            iPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[paid]"))
            iPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[pending]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total]"))
            iAmountPaid += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount paid]"))
            iAmountPending += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[amount pending]"))
            iAmountTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[total amount]"))
            If iPaid > 0 Then
                e.Row.Cells(4).Text = iPaid.ToString("d")
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If
            If iPending > 0 Then
                e.Row.Cells(5).Text = iPending.ToString("d")
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If
            If iTotal > 0 Then
                e.Row.Cells(6).Text = iTotal.ToString("d")
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If
            If iAmountPaid > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iAmountPaid.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If
            If iAmountPending > 0 Then
                e.Row.Cells(8).Text = FormatNumber(iAmountPending.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(8).Text = ""
            End If
            If iAmountTotal > 0 Then
                e.Row.Cells(9).Text = FormatNumber(iAmountTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(9).Text = ""
            End If
        End If
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvregistrationchapter As Control)
    End Sub

    Protected Sub btnContestExcel_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContestExcel.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=RegistrationReport.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvregistrationcontest.RenderControl(htmlWrite)
        'MsgBox(stringWrite.ToString())
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
End Class
