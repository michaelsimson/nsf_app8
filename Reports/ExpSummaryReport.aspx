﻿<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="ExpSummaryReport.aspx.cs" Inherits="ExpSummaryReport" 
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
            &nbsp&nbsp&nbsp&nbsp
             <asp:LinkButton ID="LBbacktofront" CssClass="btn_02" runat="server" Text="Previous" 
                 Visible="false" onclick="LBbacktofront_Click" 
        >Back to Front Page</asp:LinkButton>
           
    </div>
 
    <asp:Panel ID="Pnlsummary" runat="server">
        <div align="center">
            <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri;
                color: rgb(73, 177, 23);">
                Expense Summary Report
            </div>
            <asp:Label ID="lblNoPermission" runat="server" Visible="false" ForeColor="Red"></asp:Label>
            <br />
            <br />
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlChapterdisp" runat="server" Visible="false">
        <div align="center" runat="server" id="divchapex" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Expense Summary Report By Chapter
            <br />
        </div>
        <br />
        <br />
    </asp:Panel>
       <asp:Panel ID="Pnlcategorytitle" runat="server" Visible="false">
        <div align="center" runat="server" id="divcat" visible="false" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Expense Summary Report By Category
            <br />
        </div>
        <br />
        <br />
    </asp:Panel>
      <asp:Panel ID="PnlAllocated" runat="server" Visible="false">
        <div align="center" runat="server" id="divall" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Allocated Cost
            <br />
        </div>
         <div align="center" runat="server" visible="false" id="divallrep" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Allocated Cost Report
            <br />
        </div>
         <div align="center" runat="server" visible="false" id="divcontestrep" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Contestant Registration Report
            <br />
        </div>
        <div align="center" runat="server" visible="false" id="divmerc" style="font-size: 26px; font-weight: bold; font-family: Calibri;
            color: rgb(73, 177, 23);">
             Merchandise
            <br />
        </div>
        <br />
        <br />
    </asp:Panel>
    
     
        <asp:LinkButton ID="lbprevious" runat="server" Text="Previous" Visible="false" 
        onclick="lbprevious_Click">Previous</asp:LinkButton>
         <asp:LinkButton ID="lbchapterpre" runat="server" Text="Previous" 
        Visible="false" onclick="lbchapterpre_Click" 
        >Previous</asp:LinkButton>
         <asp:LinkButton ID="Lballocated" runat="server" Text="Previous" 
        Visible="false" onclick="Lballocated_Click" 
        >Previous</asp:LinkButton>
        <div align="center" runat="server" id="Divpan" style="font-size: 3px; font-weight: bold; font-family: Calibri;
               ">
               <table><tr><td align="center" style= "font-family:Arial; font-weight:bold; font-size :20px">
            <asp:Label ID="LbCategory" runat="server" Visible="false" ></asp:Label></td></tr>
            <tr><td align="center" style= "font-family:Arial; font-weight:bold; font-size :20px">
            <asp:Label ID="Lbchapter" runat="server" Visible="false" ></asp:Label></td></tr></table>
           
            
            </div>
          <asp:Panel ID="Pnlfront" runat="server" >
       <div align="center" style="font-size: 26px; font-weight: bold;">
       <table style="width: 50%;" border="0">
            <tr>
                <td style="width: 95px"></td> 
                 <td><asp:Label ID="lbReport" runat="server" Text="Reports" Visible="false"></asp:Label></td>
                 <td style="width: 157px">
                     <asp:DropDownList ID="DDFront" runat="server">
                     </asp:DropDownList>
                 </td><td> <asp:Button ID="BtnClick" runat="server" Text="Continue" 
                        onclick="BtnClick_Click" /></td>
              </tr>
               <tr>
                <td style="width: 95px"></td> 
                 <td> <asp:Label ID="Lboption" runat="server" Text="Options"></asp:Label></td>
                 <td style="width: 157px">
                     <asp:DropDownList ID="DDAllocatechoice" runat="server" Visible="false">
                     </asp:DropDownList>
                 </td><td> 
                    <asp:Button ID="Btncontinue" runat="server" Text="Continue" onclick="Btncontinue_Click" Visible="false" 
                        /></td>
              </tr>
                <tr>
                <td style="width: 95px"></td> 
                 <td> <asp:Label ID="Lbcontestyear" runat="server" Text="Contest Year"></asp:Label>  </td>
                 <td style="width: 157px">
                    <asp:DropDownList ID="DDAddupdate" runat="server" Visible="false">
                     </asp:DropDownList>
                 </td><td> 
                    <asp:Button ID="BtnAddupdate" runat="server" Text="Add/Update"  Visible="false" onclick="BtnAddupdate_Click" 
                        /></td>
              </tr></table></div>
            <br />
    </asp:Panel>
   
   
     <asp:Panel ID="Panlvalid"  runat="server" BorderWidth="1px" BorderColor="black"  Width="100%" Visible="False">
       <div align="center" >
        <asp:Label ID="lblReplace" ForeColor="Red"  Text="Date already exists.  Do you want to override?" runat="server"></asp:Label><br />
        <asp:Button ID="btnYes" runat="server" Text="Yes" onclick="btnYes_Click" /> &nbsp;&nbsp;<asp:Button 
               ID="btnNo" runat="server" Text="No" onclick="btnNo_Click" />
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
       <div align="center" style="font-size: 26px; font-weight: bold;">
      </div>
            <br />
    </asp:Panel>
   <%-- <asp:Button ID="Btnsummary" runat="server" Text="Summary" onclick="Button3_Click" Visible="false" />--%>
    <asp:Panel ID="Pnldisp" runat="server">
      
        <table style="width: 50%;" border="0">
            <tr>
                <td style="width: 157px">
                    <asp:Label ID="lblevent" runat="server" Text="Event"></asp:Label>
                    <asp:DropDownList ID="ddevent" runat="server" Width="135px" AutoPostBack="True" OnSelectedIndexChanged="ddevent_SelectedIndexChanged">
                        <asp:ListItem Value="0">[Select Event]</asp:ListItem>
                    </asp:DropDownList>
                </td>
             
                <td style="width: 157px">
                    <asp:Label ID="lblZone" runat="server" Text="Zone" Width="87px" Style="height: 20px"></asp:Label>
                    <asp:DropDownList ID="ddZone" runat="server" AutoPostBack="True" Width="175px" OnSelectedIndexChanged="ddZone_SelectedIndexChanged">
                        <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                    </asp:DropDownList>
              
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblCluster" runat="server" Text="Cluster" Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddCluster" runat="server" Width="150px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddCluster_SelectedIndexChanged">
                        <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblchapter" runat="server" Text="Chapter " Width="87px" Height="20px"></asp:Label>
                    <asp:DropDownList ID="ddchapter" runat="server" Width="150px" AutoPostBack="True"
                        >
                        <asp:ListItem Value="0">[Select Chapter]</asp:ListItem>
                    </asp:DropDownList>
               
                </td>
                <td style="width: 157px" visible="false">
                    <asp:Label ID="Lblcat" runat="server" Text="Expense Category" Width="87px" Visible="false"></asp:Label>
                    <asp:DropDownList ID="DDcategory" runat="server" AutoPostBack="True" Width="175px"
                        Visible="false" onselectedindexchanged="DDcategory_SelectedIndexChanged">
                        <asp:ListItem Value="0">[Select Category]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblNoyear" runat="server" Text="No of Years" Width="87px"></asp:Label>
                    <asp:DropDownList ID="ddNoyear" runat="server" 
                        Width="175px" onselectedindexchanged="ddNoyear_SelectedIndexChanged">
                        <asp:ListItem Value="0">[Select No of Years]</asp:ListItem>
                    </asp:DropDownList>
                 
                </td>
                <td style="width: 157px">
                    <asp:Label ID="lblyear" runat="server" Text="Year" Width="87px"></asp:Label>
                    <asp:DropDownList ID="DDyear" runat="server" Width="175px">
                        <asp:ListItem Value="0">All</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td width="200px">
                </td>
                <td width="200px">
                </td>
                <td>
                    
                    <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
                    <asp:Button ID="Button2" runat="server" Text="Export To Excel" OnClick="Button2_Click"
                        Enabled="false" />
                </td>
            </tr>
        </table>
        <asp:Label ID="lbldisp" runat="server" Visible="false" Text="No Record Found" ForeColor="Red"></asp:Label>
        <asp:Label ID="lblall" runat="server" Text="Enter All The Fields" Visible="false"
            ForeColor="Red"></asp:Label>
        <asp:Label ID="lblMesasenorecord" runat="server" EnableViewState="false" 
            ForeColor="Red"></asp:Label>
    </asp:Panel>
    <br />
    
  
   <asp:Panel ID="pnlview" runat="server" Visible="false">
    <asp:GridView ID="Griddisp" runat="server" Style="width: 90%; margin: 0 auto;" 
        DataKeyNames="ExpCatId" EnableViewState="false"  AutoGenerateColumns="False"  
            onrowdatabound="Griddisp_RowDataBound1"     
            
           >
        
        <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
             
                <asp:HiddenField ID="hdnid" Value='<%# Bind("ExpcatId")%>'  runat="server" />  
                <asp:HiddenField ID="Hncatedesc"   Value='<%# Bind("ExpCatDesc")%>'  runat="server" />  
   <asp:Button runat="server"  ID="btnview" OnClick="lnkView_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
    </asp:GridView> </asp:Panel> 
    
    
    <asp:Panel ID="pnlcategory" runat="server" >
    
    <asp:GridView ID="GRcategory" runat="server" Style="width: 90%; margin: 0 auto;" onrowdatabound="GRcategory_RowDataBound"  
             >
     <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
         <asp:HiddenField ID="hdcatid" Value='<%# Bind("catId")%>' runat="server" />
          <asp:HiddenField ID="Hdchapter" Value='<%# Bind("Chapterid")%>' runat="server" />     
           <asp:HiddenField ID="Hdchaptername" Value='<%# Bind("Chapter")%>' runat="server" />      
   <asp:Button runat="server" ID="btDetails" OnClick="btDetails_Click" Enabled="true"     Text="Details" />
                 
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
    </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server">

    <asp:GridView ID="GridEdit" runat="server" Style="width: 90%; margin: 0 auto;" AutoGenerateColumns="false" 
            DataKeyNames="TransactionID"  OnRowUpdating="GridEdit_RowUpdating" 
               onrowediting="GridEdit_RowEditing" 
            onrowcancelingedit="gvDetails_RowCancelingEdit" 
            onrowdatabound="GridEdit_RowDataBound"  >
             <Columns> 
             
             <asp:TemplateField>
 
      <ItemTemplate>
      <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />

      </ItemTemplate>
 
      <EditItemTemplate>
       <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" /> 

<asp:Button  ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
   
      </EditItemTemplate>
 
      </asp:TemplateField>
            <asp:TemplateField HeaderText="TransactionID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblId"   Text='<%#Eval("TransactionID") %>' />
     </ItemTemplate>
    
</asp:TemplateField>
            
                  <asp:TemplateField HeaderText="FromChapter">
     <ItemTemplate>
      
            <asp:Label runat="server" ID="lbchapterId"   Text='<%#Eval("frmcode") %>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDfrmchaptercode" runat="server" AutoPostBack="false"  OnPreRender="ddchaptercode" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="TxtfrmChaptercode" Text='<%#Eval("frmcode") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>
      <asp:TemplateField HeaderText="EventYear">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblyear"   Text='<%#Eval("EventYear") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="Txtyear" Text='<%#Eval("EventYear") %>' />
              </EditItemTemplate>
</asp:TemplateField>

 <asp:TemplateField HeaderText="EventId">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbleventId"   Text='<%#Eval("EventID") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxteventId" Text='<%#Eval("EventID") %>' />
              </EditItemTemplate>
</asp:TemplateField>

 <asp:TemplateField HeaderText="ReimbMemberID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblmembername"   Text='<%#Eval("MemberName") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

 <asp:TemplateField HeaderText="DonorType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblDonortype"   Text='<%#Eval("DonorType") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

 <asp:TemplateField HeaderText="ToChapter">
     <ItemTemplate>
      <asp:HiddenField ID="hdchapid" Value='<%# Bind("TochapterId")%>' runat="server" />
            <asp:Label runat="server" ID="lblChaptercode"   Text='<%#Eval("tochaptercode") %>' />
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDchaptercode" runat="server" AutoPostBack="false" OnPreRender="ddchaptercode" >
         </asp:DropDownList>
          <asp:TextBox Visible="false"  runat="server" ID="TxtChaptercode" Text='<%#Eval("tochaptercode") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="ExpCatCode">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblcatcode"   Text='<%#Eval("ExpCatCode") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtExpcatcode" Text='<%#Eval("ExpCatCode") %>' />
              </EditItemTemplate>
</asp:TemplateField>
            
            <asp:TemplateField HeaderText="ExpenseAmount">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblExpamount"     Text='<%#Eval("ExpenseAmount") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtExpamount" Text='<%#Eval("ExpenseAmount") %>' />
              </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="DatePaid">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblDatePaid"   Text='<%#Eval("DatePaid") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="CheckNumber">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblCheckNumber"   Text='<%#Eval("CheckNumber") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

 <asp:TemplateField HeaderText="TreatID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblTreatID"   Text='<%#Eval("TreatID") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
<asp:TemplateField HeaderText="NationalApprovalDate">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblNational"   Text='<%#Eval("NationalApprovalDate") %>' />
     </ItemTemplate>
   
</asp:TemplateField>
 <asp:TemplateField HeaderText="TreatCode">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblTreatCode"   Text='<%#Eval("TreatCode") %>' />
     </ItemTemplate>
   
</asp:TemplateField>


<asp:TemplateField HeaderText="Comments">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblComments"   Text='<%#Eval("Comments") %>' />
     </ItemTemplate>
      <EditItemTemplate>
          <asp:TextBox  runat="server" ID="Txtcomment" Text='<%#Eval("Comments") %>' />
              </EditItemTemplate>
   
</asp:TemplateField>

 
            <asp:TemplateField HeaderText="BanKID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblBanKID"   Text='<%#Eval("BanKID") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtBanKID" Text='<%#Eval("BanKID") %>' />
              </EditItemTemplate>
</asp:TemplateField>
 
            <asp:TemplateField HeaderText="ToBankID">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblToBankID"   Text='<%#Eval("ToBankID") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtToBankID" Text='<%#Eval("ToBankID") %>' />
              </EditItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Paid">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblPaid"   Text='<%#Eval("Paid") %>' />
     </ItemTemplate>
   
</asp:TemplateField>

          
             <asp:TemplateField HeaderText="TransType">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblTrans"   Text='<%#Eval("TransType") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtTrans" Text='<%#Eval("TransType") %>' />
              </EditItemTemplate>
</asp:TemplateField>  

   <asp:TemplateField HeaderText="Account">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblAccount"   Text='<%#Eval("Account") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtAccount" Text='<%#Eval("Account") %>' />
              </EditItemTemplate>
</asp:TemplateField> 
            
       <asp:TemplateField HeaderText="PaymentMethod">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblPayment"   Text='<%#Eval("PaymentMethod") %>' />
     </ItemTemplate>
   
</asp:TemplateField> 
<asp:TemplateField HeaderText="ReportDate">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblReportDate"   Text='<%#Eval("ReportDate") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtReportDate" Text='<%#Eval("ReportDate") %>' />
              </EditItemTemplate>
</asp:TemplateField> 
  
<asp:TemplateField HeaderText="RestTypeFrom">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblRestTypeFrom"   Text='<%#Eval("RestTypeFrom") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="TxtRestTypeFrom" Text='<%#Eval("RestTypeFrom") %>' />
              </EditItemTemplate>
</asp:TemplateField> 
<asp:TemplateField HeaderText="RestTypeTo">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblresttype"   Text='<%#Eval("RestTypeTo") %>' />
     </ItemTemplate>
     <EditItemTemplate>
          <asp:TextBox  runat="server" ID="Txtresttype" Text='<%#Eval("RestTypeTo") %>' />
              </EditItemTemplate>
</asp:TemplateField>    
 
        </Columns>
            </asp:GridView>
    
    </asp:Panel>
 
    
    
  <asp:Panel ID="PnlDown" runat="server">
    <asp:GridView ID="Griddrop" runat="server" >
    </asp:GridView></asp:Panel><br />
   
   <div> <asp:GridView ID="GridExcel" runat="server">
    </asp:GridView></div>
    <asp:Panel ID="pnlgrid" runat="server">
    <asp:GridView ID="GridView1" runat="server" Style="width: 90%; margin: 0 auto;" 
            onrowdatabound="GridView1_RowDataBound">
    </asp:GridView></asp:Panel>
    
      <asp:Panel ID="Pnlchapter" runat="server">
    <asp:GridView ID="GridChapter" runat="server" Style="width: 90%; margin: 0 auto;" 
              onrowdatabound="GridChapter_RowDataBound">
       <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
               <asp:HiddenField ID="hdcatid" Value='<%# Bind("ChapterId")%>'  runat="server" />  
                <asp:HiddenField ID="Hncatedesc"   Value='<%# Bind("Chaptercode")%>'  runat="server" /> 
   <asp:Button runat="server"  ID="btnview" OnClick="lnkViewchapter_Click"     Text="View"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
    </asp:GridView></asp:Panel>
   
   <asp:Panel ID="PnlExpcat" runat="server">
    <asp:GridView ID="GridExpcat" runat="server" Style="width: 90%; margin: 0 auto;" 
           onrowdatabound="GridExpcat_RowDataBound" >
       <Columns> 
            <asp:TemplateField ItemStyle-Width="20px">
                <ItemTemplate>
              <asp:HiddenField ID="hdcatid" Value='<%# Bind("ExpCatID")%>' runat="server" />
        <asp:HiddenField ID="Hdchapter" Value='<%# Bind("Chapterid")%>' runat="server" /> 
          <asp:HiddenField ID="Hncatedesc"   Value='<%# Bind("Expcatdesc")%>'  runat="server" /> 
   <asp:Button runat="server"  ID="btndetail1" OnClick="btDetailschapter_Click"    Text="Detail"  />
                  
                </ItemTemplate>
            </asp:TemplateField>
            
            
 
        </Columns>
    </asp:GridView></asp:Panel>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
    <asp:TextBox ID="Txthidden" runat="server" Visible="false"></asp:TextBox>
     <asp:TextBox ID="Txtchapterhidden" runat="server" Visible="false"></asp:TextBox>
    <asp:HiddenField ID="hdnTechNational" runat="server" />
</asp:Content>
