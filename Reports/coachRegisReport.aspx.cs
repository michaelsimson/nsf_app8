﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using VRegistration;

public partial class Reports_xlReportRegByProduct : System.Web.UI.Page
{
    string Year = "";
    DateTime day1 = DateTime.Parse("1/1/2020");
    DateTime day2 = DateTime.Parse("1/1/2000");
    int iYear = -1;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Put user code to initialize the page here

        if (!IsPostBack)
        {
            int year = Convert.ToInt32(DateTime.Now.Year);

            ddlyear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
            for (int i = 0; i < 3; i++)
            {

                ddlyear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


            }
            ddlyear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            loadSemester();
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "89")))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=89 and ProductId is not Null ");
                String prd = "";
                for (int i = 0; i < (ds.Tables[0].Rows.Count); i++)
                {
                    if (prd.Length == 0)
                        prd = ds.Tables[0].Rows[i][0].ToString();
                    else
                        prd = prd + "," + ds.Tables[0].Rows[i][0].ToString();

                }
                lblPrd.Text = prd;
            }
            prod(Convert.ToInt32(ddlyear.SelectedValue));//year;

        }
        if (Session["LoggedIn"].ToString() == "True")
        {
            HyperLink2.Visible = true;
        }
        else
            HyperLink2.Visible = false;


    }
    private void prod(int yy)
    {
        ddlprod.Items.Clear();
        ddlprod.Items.Insert(0, new ListItem("All", "0"));
        ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("All"));
        int j = 0;

        string str = "Select distinct P.ProductID, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName from Product P INNER JOIN CalSignup EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" + yy + " AND P.EventID=13 and EF.Semester='" + ddlSemester.SelectedValue + "'";
        if (lblPrd.Text.Length > 0) //lblPrd.Text != "") 
            str = str + " AND P.ProductID In (" + lblPrd.Text + ")";
        str = str + " order by P.ProductID";
        //Response.Write(str);
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.Connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListItem li = new ListItem();
                String af = reader["productid"].ToString();
                li.Value = af;
                li.Text = reader[1].ToString();
                ddlprod.Items.Add(li);
                j = 1;

            }
            cmd.Connection.Close();
            cmd.Connection.Dispose();
            if (j == 0)
            {
                ddlprod.Items.Clear();
                ddlprod.Items.Add("No Product");
                ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("No Product"));
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    private int GetData(int EventYear, string ProdIDs)
    {
        try
        {
            lblError.Text = "";
            String SQLStr = "select CR.CoachRegID,C.SignUpID,CR.Approved, case when CR.AdultId is null then Ch.FIRST_NAME + ' ' + Ch.LAST_NAME else IP.FirstName + ' '+IP.LastName end AS ChildName,CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName, I1.Email,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,case when CR.AdultId is null then CR.ChildNumber else CR.AdultId end as Childnumber,CR.SessionNo, CR.Semester,C.Day,CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.PMemberID as MemberId, Cp.ChapterCode as Chapter, case when CR.AdultId is null then 'Child' else 'Adult' end as Type ";
            SQLStr = SQLStr + " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId)  INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Chapter Cp ON I1.ChapterID =Cp.ChapterID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON ";
            SQLStr = SQLStr + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.Semester='" + ddlSemester.SelectedValue + "' and C.Accepted='Y' AND CR.EventYear=" + EventYear + ProdIDs + "  ORDER BY P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME"; //((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)// Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
            //Response.Write(SQLStr);
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdCoaching.Visible = true;
                grdCoaching.DataSource = ds;
                grdCoaching.DataBind();
                grdCoaching.Columns[8].Visible = false;
                grdCoaching.Columns[11].Visible = false;
            }
            else
            {
                grdCoaching.Visible = false;
                lblError.Text = "No records found";
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
        return 0;
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {


        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=CoachingRegistrationReport_" + ddlyear.SelectedValue + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        grdCoaching.Columns[8].Visible = true;
        grdCoaching.Columns[11].Visible = true;
        grdCoaching.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();

    }


    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("../VolunteerFunctions.aspx");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        if (ddlprod.SelectedIndex < 0)//"No Product")
        {
            lblError.Text = "No Product Selected";
            return;
        }
        else
        {
            lblError.Text = "";
            if (ddlprod.SelectedItem.Text == "All")
            {
                //  Response.Write("ALL");
                Year = ddlyear.SelectedItem.Value;
                iYear = Convert.ToInt32(Year);
                string prodi = " AND CR.ProductID in (0";
                prod(iYear);
                foreach (ListItem i in ddlprod.Items)
                {
                    prodi = prodi + "," + i.Value;
                }
                prodi = prodi + ") ";
                GetData(iYear, prodi);
            }

            else
            {
                Year = ddlyear.SelectedItem.Value;
                string prodi = " AND CR.ProductID in (0";
                foreach (ListItem i in ddlprod.Items)
                {
                    if (i.Selected)
                    {
                        prodi = prodi + "," + i.Value;

                    }
                }
                prodi = prodi + ") ";
                iYear = Convert.ToInt32(Year);
                GetData(iYear, prodi);
            }
        }

    }

    protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdCoaching.Visible = false;
        Year = ddlyear.SelectedItem.Value;
        iYear = Convert.ToInt32(Year);
        prod(iYear);
    }

    private void loadSemester()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlyear.SelectedValue);
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        prod(Convert.ToInt32(ddlyear.SelectedValue));
    }
}
