﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Reports_MealsStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadyear()
            loadgrid()
        End If
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub loadgrid()
        If ddlyear.SelectedValue < 2010 Then
            lblErr.Text = "Meal Statistics is not available before 2010 as Age column in not available before it."
        Else
            lblErr.Text = ""
        End If
        '
        '
        btnExport.Enabled = True
        Dim Mealtype() As String = {"Breakfast", "Lunch", "Dinner"}
        Dim MealtypeCnt As Integer = 2
        If ddlyear.SelectedValue > 2011 Then
            Mealtype = New String() {"Breakfast", "Lunch-Indian Food", "Lunch-Pizza", "Dinner"}
            MealtypeCnt = 3
        End If
        Dim age() As String = {"Under 5", "Minor", "Adult"}
        Dim dates(3) As String
        Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Convert(Varchar(10),dateadd(d,-1,MIN(ContestDate)),101) as Day0 ,Convert(Varchar(10),MIN(ContestDate),101) as Day1,convert(Varchar(10),MAX(ContestDate),101)  as Day2  from Contest  where EventId=1 AND YEAR(ContestDate) =" & ddlyear.SelectedValue & "")
        Try

            While readr.Read()
                dates(0) = readr(0)
                dates(1) = readr(1)
                dates(2) = readr(2)
            End While
        Catch ex As Exception
            GVMeals.DataSource = Nothing
            GVMeals.DataBind()
            btnExport.Enabled = False
            lblErr.Text = "No Date Found for Current Year"
            Exit Sub
        End Try
        readr.Close()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Type", Type.GetType("System.String"))
        dt.Columns.Add("Day0", Type.GetType("System.Int32"))
        dt.Columns.Add("Day1", Type.GetType("System.Int32"))
        dt.Columns.Add("Day2", Type.GetType("System.Int32"))
        dt.Columns.Add("Total", Type.GetType("System.Int32"))
        Dim i, j, k, l As Integer
        Dim LunchType(), strsql As String
        l = 0
        Dim array(21) As Integer

        For i = 0 To MealtypeCnt 'Mealtype
            For j = 0 To 2 'age
                For k = 0 To 2 'dates
                    LunchType = Mealtype(i).Split("-")
                    strsql = "select COUNT(mealchargeid) from MealCharge where age='" & age(j) & "' and Mealtype='" & LunchType(0) & "'"
                    If LunchType(0) = "Lunch" And ddlyear.SelectedValue > 2011 Then
                        strsql = strsql & " and LunchType='" & LunchType(1) & "'"
                    End If
                    strsql = strsql & " and PaymentReference is not Null AND  contestdate='" & dates(k) & "'"
                    array(l) = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strsql)
                    l = l + 1
                Next
                dr = dt.NewRow
                dr("Type") = Mealtype(i) & " - " & age(j)
                dr("day0") = array(0)
                dr("day1") = array(1)
                dr("day2") = array(2)
                dr("Total") = array(0) + array(1) + array(2)
                dt.Rows.Add(dr)
                l = 0
            Next
        Next
        Dim day0, day1, day2 As Integer
        day0 = 0
        day1 = 0
        day2 = 0
        For i = 0 To dt.Rows.Count - 1
            day0 = day0 + dt.Rows(i)("day0")
            day1 = day1 + dt.Rows(i)("day1")
            day2 = day2 + dt.Rows(i)("day2")
        Next
        dr = dt.NewRow
        dr("Type") = "Total"
        dr("day0") = day0
        dr("day1") = day1
        dr("day2") = day2
        dr("Total") = day0 + day1 + day2
        dt.Rows.Add(dr)
        GVMeals.Columns(1).HeaderText = dates(0).ToString()
        GVMeals.Columns(2).HeaderText = dates(1).ToString()
        GVMeals.Columns(3).HeaderText = dates(2).ToString()
        GVMeals.DataSource = dt
        GVMeals.DataBind()
    End Sub

    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 4 Step -1
            ddlyear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=MealStatistics.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        'Dim dgexport As New GridView
        'loadgrid(dgexport)
        GVMeals.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    'Private Sub loadgrid(ByVal gv As GridView)
    '    Dim Mealtype() As String = {"Breakfast", "Lunch", "Dinner"}
    '    Dim age() As String = {"Under 5", "Minor", "Adult"}
    '    Dim dates(3) As String
    '    Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Convert(Varchar(10),dateadd(d,-1,satday1),101) as Day0,Convert(Varchar(10),satday1,101) as Day1,Convert(Varchar(10),sunday2,101)  as Day2 from WeekCalendar where YEAR(satday1) =YEAR(Getdate())")
    '    While readr.Read()
    '        dates(0) = readr(0)
    '        dates(1) = readr(1)
    '        dates(2) = readr(2)
    '    End While
    '    readr.Close()
    '    Dim dt As DataTable = New DataTable()
    '    Dim dr As DataRow
    '    dt.Columns.Add("Type", Type.GetType("System.String"))
    '    dt.Columns.Add("Day0", Type.GetType("System.Int32"))
    '    dt.Columns.Add("Day1", Type.GetType("System.Int32"))
    '    dt.Columns.Add("Day2", Type.GetType("System.Int32"))
    '    dt.Columns.Add("Total", Type.GetType("System.Int32"))
    '    Dim i, j, k, l As Integer
    '    l = 0
    '    Dim array(21) As Integer
    '    For i = 0 To 2 'Mealtype
    '        For j = 0 To 2 'age
    '            For k = 0 To 2 'dates
    '                array(l) = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(mealchargeid) from MealCharge where age='" & age(j) & "' and Mealtype='" & Mealtype(i) & "' and PaymentReference is not Null AND  contestdate='" & dates(k) & "'")
    '                l = l + 1
    '            Next
    '            dr = dt.NewRow
    '            dr("Type") = Mealtype(i) & " - " & age(j)
    '            dr("day0") = array(0)
    '            dr("day1") = array(1)
    '            dr("day2") = array(2)
    '            dr("Total") = array(0) + array(1) + array(2)
    '            dt.Rows.Add(dr)
    '            l = 0
    '        Next
    '    Next
    '    Dim day0, day1, day2 As Integer
    '    day0 = 0
    '    day1 = 0
    '    day2 = 0
    '    For i = 0 To dt.Rows.Count - 1
    '        day0 = day0 + dt.Rows(i)("day0")
    '        day1 = day1 + dt.Rows(i)("day1")
    '        day2 = day2 + dt.Rows(i)("day2")
    '    Next
    '    dr = dt.NewRow
    '    dr("Type") = "Total"
    '    dr("day0") = day0
    '    dr("day1") = day1
    '    dr("day2") = day2
    '    dr("Total") = day0 + day1 + day2
    '    dt.Rows.Add(dr)
    '    gv.DataSource = dt
    '    gv.DataBind()
    'End Sub

    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlyear.SelectedIndexChanged
        loadgrid()
    End Sub
End Class
