﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;

public partial class ReviewExpTrans : System.Web.UI.Page
{
    string LoginID = "LoginID";
    string RoleID = "RoleId";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrMsg.Text = "";
        lblNotChashedTitle.Text = "";
        lblVoidedChecksTitle.Text = "";

        if (Session[LoginID] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            ddReviewBy.SelectedValue = "Payee";
            if (ddReviewBy.SelectedValue == "Payee")
            {

                string cmdText = "";
                DataSet ds = new DataSet();

                tdReviewTitle.Visible = false;
                tdReviewDContent.Visible = false;

                lblReviewTitle.Text = "payee";
                lblReviewTitle.Visible = true;
                ddReviewContent.Visible = false;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = true;
                tdOrgContent.Visible = true;
                tdIndividualTitle.Visible = true;
                tdIndividualContent.Visible = true;

                tdorgCheck.Visible = true;
                tdIndvidualCheck.Visible = true;

                cmdText = "select distinct EJ.ReimbMemberID,(IP.FirstName+' '+IP.LastName) as Name from EXPJournal EJ inner join IndSpouse IP on(EJ.ReimbMemberID=IP.AutoMemberID) where EJ.DonorType='IND' or EJ.DonorType='SPOUSE' Order by Name ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ddlIndividualContent.DataSource = ds;
                        ddlIndividualContent.DataTextField = "Name";
                        ddlIndividualContent.DataValueField = "ReimbMemberID";
                        ddlIndividualContent.DataBind();

                        ddlIndividualContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
                cmdText = "select distinct EJ.ReimbMemberID,(OI.ORGANIZATION_NAME)  as Name from EXPJournal EJ inner join OrganizationInfo OI on(EJ.ReimbMemberID=OI.AutoMemberID) where EJ.DonorType='OWN' order by Name ASC";


                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ddlOrganization.DataSource = ds;
                        ddlOrganization.DataTextField = "Name";
                        ddlOrganization.DataValueField = "ReimbMemberID";
                        ddlOrganization.DataBind();

                        ddlOrganization.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
        }
    }
    string ConnectionString = "ConnectionString";
    protected void ddReviewBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cmdText = "";
        DataSet ds = new DataSet();

        if (ddReviewBy.SelectedValue != "0")
        {

            tdSubmit.Visible = true;

            tdReviewTitle.Visible = true;
            tdReviewDContent.Visible = true;
            if (ddReviewBy.SelectedValue == "Payee")
            {
                tdReviewTitle.Visible = false;
                tdReviewDContent.Visible = false;

                lblReviewTitle.Text = "payee";
                lblReviewTitle.Visible = true;
                ddReviewContent.Visible = false;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = true;
                tdOrgContent.Visible = true;
                tdIndividualTitle.Visible = true;
                tdIndividualContent.Visible = true;

                tdorgCheck.Visible = true;
                tdIndvidualCheck.Visible = true;

                cmdText = "select distinct EJ.ReimbMemberID,(IP.FirstName+' '+IP.LastName) as Name from EXPJournal EJ inner join IndSpouse IP on(EJ.ReimbMemberID=IP.AutoMemberID) where EJ.DonorType='IND' or EJ.DonorType='SPOUSE' Order by Name ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ddlIndividualContent.DataSource = ds;
                        ddlIndividualContent.DataTextField = "Name";
                        ddlIndividualContent.DataValueField = "ReimbMemberID";
                        ddlIndividualContent.DataBind();

                        ddlIndividualContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
                cmdText = "select distinct EJ.ReimbMemberID,(OI.ORGANIZATION_NAME)  as Name from EXPJournal EJ inner join OrganizationInfo OI on(EJ.ReimbMemberID=OI.AutoMemberID) where EJ.DonorType='OWN' order by Name ASC";


                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ddlOrganization.DataSource = ds;
                        ddlOrganization.DataTextField = "Name";
                        ddlOrganization.DataValueField = "ReimbMemberID";
                        ddlOrganization.DataBind();

                        ddlOrganization.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
            else if (ddReviewBy.SelectedValue == "Check Number")
            {

                lblReviewTitle.Text = "Check Number";
                ddReviewContent.Visible = false;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = true;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;

            }
            else if (ddReviewBy.SelectedValue == "Bank")
            {

                lblReviewTitle.Text = "Bank";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;

                cmdText = "select BankID,BankCode from Bank";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "BankCode";
                        ddReviewContent.DataValueField = "BankID";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
               
            }
            else if (ddReviewBy.SelectedValue == "Expense Category")
            {

                lblReviewTitle.Text = "Expense Category";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;


                cmdText = "select distinct(EC.ExpCatDesc) as ExpCatCode, EC.ExpCatID from ExpJournal EJ inner join ExpenseCategory EC on (EJ.ExpCatID=EC.ExpCatID) Order by EC.ExpCatDesc ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "ExpCatCode";
                        ddReviewContent.DataValueField = "ExpCatID";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
               
            }
            else if (ddReviewBy.SelectedValue == "Transaction Type")
            {

                lblReviewTitle.Text = "Transaction Type";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;


                cmdText = "select distinct(TransType) as TransType from ExpJournal Order by TransType ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "TransType";
                        ddReviewContent.DataValueField = "TransType";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
                
            }
            else if (ddReviewBy.SelectedValue == "Restriction Type")
            {

                lblReviewTitle.Text = "Restriction Type";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;



                cmdText = "select distinct(RestTypeFrom) as RestType from ExpJournal where RestTypeFrom is not null";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "RestType";
                        ddReviewContent.DataValueField = "RestType";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
               
            }
            else if (ddReviewBy.SelectedValue == "Event")
            {

                lblReviewTitle.Text = "Event";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;
                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;


                cmdText = "Select EventId,Name from Event Order by EventID ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "Name";
                        ddReviewContent.DataValueField = "EventId";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));

                        ddReviewContent.Items.RemoveAt(7);
                        ddReviewContent.Items.RemoveAt(7);
                        ddReviewContent.Items.RemoveAt(9);
                        ddReviewContent.Items.RemoveAt(13);
                        ddReviewContent.Items.RemoveAt(13);

                    }
                }
               
            }
            else if (ddReviewBy.SelectedValue == "Chapter")
            {

                lblReviewTitle.Text = "Chapter";
                ddReviewContent.Visible = true;
                lblReviewTitle.Visible = true;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;


                cmdText = "Select ChapterID,ChapterCode,State from Chapter order by State, ChapterCode ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddReviewContent.DataSource = ds;
                        ddReviewContent.DataTextField = "ChapterCode";
                        ddReviewContent.DataValueField = "ChapterID";
                        ddReviewContent.DataBind();

                        ddReviewContent.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
                
            }
            else if (ddReviewBy.SelectedValue == "Checks Not Issued")
            {

                ddReviewContent.Visible = false;
                lblReviewTitle.Visible = false;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;
                tdReviewDContent.Visible = false;

            }
            else if (ddReviewBy.SelectedValue == "Voided/Not Cashed")
            {
                ddReviewContent.Visible = false;
                lblReviewTitle.Visible = false;
                txtReviewContent.Visible = false;

                tdOrgTitle.Visible = false;
                tdOrgContent.Visible = false;
                tdIndividualTitle.Visible = false;
                tdIndividualContent.Visible = false;
                tdorgCheck.Visible = false;
                tdIndvidualCheck.Visible = false;
                tdReviewDContent.Visible = false;
            }

        }
        else
        {
            tdOrgTitle.Visible = false;
            tdOrgContent.Visible = false;
            tdIndividualTitle.Visible = false;
            tdIndividualContent.Visible = false;
            tdorgCheck.Visible = false;
            tdIndvidualCheck.Visible = false;
            tdReviewTitle.Visible = false;
            tdReviewDContent.Visible = false;
            tdSubmit.Visible = false;
            tdExport.Visible = false;
            tblUpdateTransactions.Visible = false;
        }
    }
    protected void rbtOrgCheck_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtOrgCheck.Checked == true)
        {
            rbtIndividualCheck.Checked = false;
            ddlOrganization.Enabled = true;
            ddlIndividualContent.Enabled = false;

        }

    }
    protected void rbtIndividualCheck_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtIndividualCheck.Checked == true)
        {
            rbtOrgCheck.Checked = false;
            ddlOrganization.Enabled = false;
            ddlIndividualContent.Enabled = true;
            rbtIndividualCheck.Checked = true;
        }

    }

    public void bindReviewExpenseTran()
    {

        string cmdText = "";
        string seacrhCondition = "";
        string orderBy = "";
        lblTransTitle.Visible = true;
        if (ddReviewBy.SelectedValue == "Payee")
        {
            if (rbtOrgCheck.Checked == true)
            {
                seacrhCondition = "EJ.ReimbMemberID=" + ddlOrganization.SelectedValue + " and EJ.DonorType='OWN' and EJ.CheckNumber is not null";
            }
            else
            {
                seacrhCondition = "EJ.ReimbMemberID=" + ddlIndividualContent.SelectedValue + " and (EJ.DonorType='IND' OR EJ.DonorType='SPOUSE') and EJ.CheckNumber is not null";
            }
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";
            // orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Check Number")
        {
            seacrhCondition = "EJ.CheckNumber='" + txtReviewContent.Text + "' and CheckNumber is not null";
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";

            //orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Bank")
        {
            seacrhCondition = "EJ.BankID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";

            //orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Expense Category")
        {
            seacrhCondition = "EJ.ExpCatID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";
            //orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Transaction Type")
        {
            seacrhCondition = "EJ.TransType='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            //orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";
            orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Restriction Type")
        {
            seacrhCondition = "EJ.RestTypeFrom='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC";

        }
        else if (ddReviewBy.SelectedValue == "Event")
        {
            seacrhCondition = "EJ.EventID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,ReImbName,EC.ExpCatDesc ASC";
        }
        else if (ddReviewBy.SelectedValue == "Chapter")
        {
            seacrhCondition = "EJ.ToChapterID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
            orderBy = "EJ.ReportDate DESC,EJ.TransactionID DESC,EC.ExpCatDesc,ReImbName ASC";
        }
        else if (ddReviewBy.SelectedValue == "Checks Not Issued")
        {
            seacrhCondition = "EJ.CheckNumber is null";
            orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";
        }
        //else if (ddReviewBy.SelectedValue == "Voided/Not Cashed")
        //{
        //    seacrhCondition = "EJ.CheckNumber Not In (select CheckNumber from BankTrans where CheckNumber=EJ.CheckNumber and  BanKID=EJ.BanKID)";
        //    orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,EJ.bankID DESC";
        //}
        cmdText += "select EJ.TransactionID,Ej.DonorType,EJ.ExpenseAmount";
        cmdText += ", case when EJ.DonorType='OWN' then OI.ORGANIZATION_NAME";
        cmdText += " when EJ.DonorType='IND' then (IP.FirstName+' '+IP.LastName)";
        cmdText += " when EJ.DonorType='SPOUSE' then (IP.FirstName+' '+IP.LastName) else '' end as ReImbName";
        cmdText += ",EJ.DatePaid, EJ.CheckNumber,B.BankCode,EJ.BanKID,EC.ExpCatDesc,";
        cmdText += "EJ.TransType,EJ.RestTypeFrom,E.Name as Event,C.ChapterCode as Chapter,";
        cmdText += "EJ.ReimbMemberID,EJ.BanKID,EJ.ToBankID,EJ.ExpCatID,EJ.TransType,EJ.RestTypeFrom,EJ.EventID,EJ.ToChapterID,";
        cmdText += " B1.BankCode as ToBankCode,EJ.EventYear as Year,EJ.Account,EJ.ReportDate,EJ.ReimbMemberID";

        cmdText += ",case when EJ.CheckNumber IS null then 'Checks Not Issued' when EJ.BanKID<4 and (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber";
        cmdText += "  then 'Cashed'";
        cmdText += "when EJ.BanKID>=4 and (select ExpCheckNo from BrokTrans where BankID=EJ.BankID and ExpCheckNo=EJ.CheckNumber)=EJ.CheckNumber then 'Cashed'";
        cmdText += " else 'Not Cashed' end as TransStatus";

        cmdText += " ,(select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber) as ckNo";
        cmdText += " ,case when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber ";
        cmdText += " then (select TransDate from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber) ";
        cmdText += " else null end as StatusDate";

        cmdText += " from ExpJournal EJ ";
        cmdText += "  left join Event E on(E.EventID=EJ.EventID)";
        cmdText += " left join Chapter C on (C.ChapterID=EJ.ToChapterID)";
        cmdText += " inner join ExpenseCategory EC on(EC.ExpCatID=EJ.ExpCatID and EC.Account=EJ.Account)";
        cmdText += " left join Bank B on(B.BankID=EJ.BanKID)";
        cmdText += " left join Bank B1 on(B1.BankID=EJ.ToBankID)";
        cmdText += "  left join OrganizationInfo OI on (OI.AutomemberID=EJ.ReimbMemberID)";
        cmdText += " left join INDSpouse IP on (IP.AutoMemberID=EJ.ReimbMemberID)";

        cmdText += " where " + seacrhCondition + " Order by " + orderBy + "";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            grdReviewExpenseTran.Visible = true;
            lblTransTitle.Visible = true;
            grdNotCashed.Visible = false;
            grdVoidedChecks.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                tdExport.Visible = true;
                grdReviewExpenseTran.DataSource = ds.Tables[0];
                grdReviewExpenseTran.DataBind();

                if (Array.IndexOf(new string[] { "1", "84" }, Session[RoleID].ToString()) > -1)
                {
                    for (int i = 0; i < grdReviewExpenseTran.Rows.Count; i++)
                    {
                        ((Button)grdReviewExpenseTran.Rows[i].FindControl("btnDeleteTrans") as Button).Visible = true;
                    }
                }
                else
                {
                    for (int i = 0; i < grdReviewExpenseTran.Rows.Count; i++)
                    {
                        ((Button)grdReviewExpenseTran.Rows[i].FindControl("btnDeleteTrans") as Button).Visible = false;
                    }
                }

            }
            else
            {

                tdExport.Visible = false;
                grdReviewExpenseTran.DataSource = null;
                grdReviewExpenseTran.DataBind();
                lblErrMsg.Text = "No record exist";
                lblErrMsg.ForeColor = Color.Red;
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validateSearch() == "1")
        {
            tblUpdateTransactions.Visible = false;
            grdReviewExpenseTran.PageIndex = 0;
            if (ddReviewBy.SelectedValue == "Voided/Not Cashed")
            {
                BindNotCashed();
                BindVoidedChecks();
            }
            else
            {
                bindReviewExpenseTran();
            }
        }
        else
        {
            validateSearch();
        }
    }
    public string validateSearch()
    {
        string retval = "1";
        if (ddReviewContent.Visible == true)
        {
            if (ddReviewContent.SelectedValue == "0")
            {
                retval = "-1";

                lblErrMsg.Text = "Please select " + ddReviewBy.SelectedValue + "";
                lblErrMsg.ForeColor = Color.Red;
            }
        }
        else if (txtReviewContent.Visible == true)
        {
            if (txtReviewContent.Text == "")
            {
                retval = "-1";
                lblErrMsg.Text = "Please select " + ddReviewBy.SelectedValue + "";
                lblErrMsg.ForeColor = Color.Red;
            }
        }
        else if (ddlOrganization.Visible == true || ddlIndividualContent.Visible == true)
        {
            if (ddlOrganization.SelectedValue == "0" && ddlIndividualContent.SelectedValue == "0")
            {
                retval = "-1";
                lblErrMsg.Text = "Please select any one " + ddReviewBy.SelectedValue + "";
                lblErrMsg.ForeColor = Color.Red;
            }
        }
        return retval;
    }

    protected void grdReviewExpenseTran_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdReviewExpenseTran.PageIndex = e.NewPageIndex;
        bindReviewExpenseTran();
    }

    public void ExportExcelAll()
    {
        if (ddReviewBy.SelectedValue != "Voided/Not Cashed")
        {
            DataSet ds = new DataSet();
            string cmdText = "";
            string seacrhCondition = "";
            string orderBy = "";
            if (ddReviewBy.SelectedValue == "Payee")
            {
                if (rbtOrgCheck.Checked == true)
                {
                    seacrhCondition = "EJ.ReimbMemberID=" + ddlOrganization.SelectedValue + " and EJ.DonorType='OWN' and EJ.CheckNumber is not null";
                }
                else
                {
                    seacrhCondition = "EJ.ReimbMemberID=" + ddlIndividualContent.SelectedValue + " and (EJ.DonorType='IND' OR EJ.DonorType='SPOUSE') and EJ.CheckNumber is not null";
                }
                orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,Payee ASC,EJ.TransactionID DESC";
            }
            else if (ddReviewBy.SelectedValue == "Check Number")
            {
                seacrhCondition = "EJ.CheckNumber='" + txtReviewContent.Text + "' and CheckNumber is not null";
                orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,Payee ASC,EJ.TransactionID DESC";
            }
            else if (ddReviewBy.SelectedValue == "Bank")
            {
                seacrhCondition = "EJ.BankID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,Payee ASC,EJ.TransactionID DESC";
            }
            else if (ddReviewBy.SelectedValue == "Expense Category")
            {
                seacrhCondition = "EJ.ExpCatID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,Payee ASC,EJ.TransactionID DESC";
            }
            else if (ddReviewBy.SelectedValue == "Transaction Type")
            {
                seacrhCondition = "EJ.TransType='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                //orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC,EJ.TransactionID DESC";
                orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,Payee,EC.ExpCatDesc ASC";
            }
            else if (ddReviewBy.SelectedValue == "Restriction Type")
            {
                seacrhCondition = "EJ.RestTypeFrom='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                //orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,ReImbName ASC";
                orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,Payee,EC.ExpCatDesc ASC";
            }
            else if (ddReviewBy.SelectedValue == "Event")
            {
                seacrhCondition = "EJ.EventID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                orderBy = "EJ.DatePaid DESC,B.BankCode,EJ.CheckNumber,Payee,EC.ExpCatDesc ASC";
            }
            else if (ddReviewBy.SelectedValue == "Chapter")
            {
                seacrhCondition = "EJ.ToChapterID='" + ddReviewContent.SelectedValue + "' and CheckNumber is not null";
                orderBy = "EJ.ReportDate DESC,EJ.TransactionID DESC,EC.ExpCatDesc,Payee ASC";
            }
            else if (ddReviewBy.SelectedValue == "Checks Not Issued")
            {
                seacrhCondition = "EJ.CheckNumber is null";
                orderBy = "EJ.ReportDate DESC,EC.ExpCatDesc,Payee ASC,EJ.TransactionID DESC";
            }
            cmdText += "select EJ.TransactionID,";

            cmdText += "case when EJ.CheckNumber IS null then 'Checks Not Issued' when EJ.BanKID<4 and (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber";
            cmdText += "  then 'Cashed'";
            cmdText += "when EJ.BanKID>=4 and (select ExpCheckNo from BrokTrans where BankID=EJ.BankID and ExpCheckNo=EJ.CheckNumber)=EJ.CheckNumber then 'Cashed'";
            cmdText += " else 'Not Cashed' end as [Status]";


            //cmdText += " case when EJ.CheckNumber IS null then 'Checks Not Issued' when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber";
            //cmdText += "  then 'Cashed' else 'Not Cashed' end as [Status]";

            cmdText += " ,case when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber ";
            cmdText += " then (select TransDate from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber) ";
            cmdText += " else null end as CashedDate,";



            cmdText += " Ej.DonorType,EJ.ExpenseAmount";
            cmdText += ", case when EJ.DonorType='OWN' then OI.ORGANIZATION_NAME";
            cmdText += " when EJ.DonorType='IND' then (IP.FirstName+' '+IP.LastName)";
            cmdText += " when EJ.DonorType='SPOUSE' then (IP.FirstName+' '+IP.LastName) else '' end as Payee";
            cmdText += ",EJ.DatePaid, EJ.CheckNumber,B.BankCode as BankID,EC.ExpCatDesc,";
            cmdText += "EJ.TransType,EJ.RestTypeFrom,E.Name as Event,C.ChapterCode as Chapter,";
            cmdText += " B1.BankCode as ToBankID,EJ.EventYear as Year,EJ.Account,EJ.ReportDate";

            cmdText += " from ExpJournal EJ ";

            cmdText += "  left join Event E on(E.EventID=EJ.EventID)";
            cmdText += " left join Chapter C on (C.ChapterID=EJ.ToChapterID)";
            cmdText += " inner join ExpenseCategory EC on EC.ExpCatID=EJ.ExpCatID and EC.Account=EJ.Account";
            cmdText += " left join Bank B on(B.BankID=EJ.BanKID)";
            cmdText += " left join Bank B1 on(B1.BankID=EJ.ToBankID)";
            cmdText += "  left join OrganizationInfo OI on (OI.AutomemberID=EJ.ReimbMemberID)";
            cmdText += " left join INDSpouse IP on (IP.AutoMemberID=EJ.ReimbMemberID)";
            cmdText += " where " + seacrhCondition + " Order by " + orderBy + "";
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string payeeName = (ddReviewBy.SelectedItem.Text).Replace(" ", "");
                string filename = "ExpenseTransactions_" + payeeName + "_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }

    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (ddReviewBy.SelectedValue != "Voided/Not Cashed")
        {
            ExportExcelAll();
        }
        else
        {
            ExportToExcelVoidNotCashedChecks();
        }
    }

    public void DeleteExpenseTrans()
    {
        string cmdText = "";
        cmdText = "Delete from ExpJournal where TransactionID=" + hdnTrnsactionID.Value + "";
        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        lblErrMsg.Text = "Transaction deleted successfully";
        lblErrMsg.ForeColor = Color.Blue;
        bindReviewExpenseTran();
    }

    protected void grdReviewExpenseTran_RowCommand(object sender, GridViewCommandEventArgs e)
    {


        try
        {
            GridViewRow row = null;


            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                grdReviewExpenseTran.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                tblUpdateTransactions.Visible = true;
                string donorType = "";
                donorType = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblDonorType") as Label).Text;
                hdnDonorType.Value = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblDonorType") as Label).Text;
                fillPayee(donorType);
                fillbank();
                fillExpenseCat();
                fillTransType();
                fillRestType();
                fillEvent();
                fillChapter();
                hdnTrnsactionID.Value = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblTransaID") as Label).Text;
                ddlpayee.SelectedValue = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblReImbID") as Label).Text;
                txtAmount.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblAmount") as Label).Text;
                txtCheckDate.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblCheckDate") as Label).Text;
                txtCheckNo.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblCheckNumber") as Label).Text;
                ddlbank.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblbankIDFill") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblbankIDFill") as Label).Text);
                ddlExpCat.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblExpID") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblExpID") as Label).Text);
                ddlTransType.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblTransTypeID") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblTransTypeID") as Label).Text);
                ddlRestType.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblRestTypeID") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblRestTypeID") as Label).Text);
                ddlEvent.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblEventID") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblEventID") as Label).Text);
                ddlChapter.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblToChapterID") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblToChapterID") as Label).Text);
                txtEventYear.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblYear") as Label).Text;
                txtAccount.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblAccount") as Label).Text;
                txtReportDate.Text = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblReportDate") as Label).Text;
                ddlTobank.SelectedValue = (((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblToBankIDFill") as Label).Text == "" ? "0" : ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblToBankIDFill") as Label).Text);

            }
        }
        catch (Exception ex1)
        {
            throw new Exception(ex1.Message);
        }

    }
    public void grdReviewExpenseTran_RowDeleting(Object sender, GridViewDeleteEventArgs e)
    {
        tblUpdateTransactions.Visible = false;
        int selIndex = e.RowIndex;
        grdReviewExpenseTran.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
        hdnTrnsactionID.Value = ((Label)grdReviewExpenseTran.Rows[selIndex].FindControl("lblTransaID") as Label).Text;
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "transDelete();", true);

    }
    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        DeleteExpenseTrans();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        grdReviewExpenseTran.DataSource = null;
        grdReviewExpenseTran.DataBind();
        ddReviewBy.SelectedValue = "0";
        tdExport.Visible = false;
        tdSubmit.Visible = false;
        tdReviewDContent.Visible = false;
        tdorgCheck.Visible = false;
        tdOrgContent.Visible = false;
        tdOrgTitle.Visible = false;

        tdIndividualContent.Visible = false;
        tdIndividualTitle.Visible = false;
        tdIndvidualCheck.Visible = false;
        txtReviewContent.Visible = false;
        ddReviewContent.Visible = false;
        lblTransTitle.Visible = false;
        tdReviewTitle.Visible = false;

        tblUpdateTransactions.Visible = false;
    }

    public void fillPayee(string DonorType)
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        if (DonorType == "OWN")
        {

            cmdText = "select distinct EJ.ReimbMemberID,(OI.ORGANIZATION_NAME)  as Name from EXPJournal EJ inner join OrganizationInfo OI on(EJ.ReimbMemberID=OI.AutoMemberID) where EJ.DonorType='OWN' order by Name ASC";

        }
        else
        {
            cmdText = "select distinct EJ.ReimbMemberID,(IP.FirstName+' '+IP.LastName) as Name from EXPJournal EJ inner join IndSpouse IP on(EJ.ReimbMemberID=IP.AutoMemberID) where EJ.DonorType='IND' or EJ.DonorType='SPOUSE' Order by Name ASC";

        }
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                ddlpayee.DataSource = ds;
                ddlpayee.DataTextField = "Name";
                ddlpayee.DataValueField = "ReimbMemberID";
                ddlpayee.DataBind();

                ddlpayee.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }
    public void fillbank()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "select BankID,BankCode from Bank";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlbank.DataSource = ds;
                ddlbank.DataTextField = "BankCode";
                ddlbank.DataValueField = "BankID";
                ddlbank.DataBind();

                ddlbank.Items.Insert(0, new ListItem("Select", "0"));

                ddlTobank.DataSource = ds;
                ddlTobank.DataTextField = "BankCode";
                ddlTobank.DataValueField = "BankID";
                ddlTobank.DataBind();

                ddlTobank.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }
    public void fillExpenseCat()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "select distinct(EC.ExpCatDesc) as ExpCatCode, EC.ExpCatID from ExpJournal EJ inner join ExpenseCategory EC on (EJ.ExpCatID=EC.ExpCatID) Order by EC.ExpCatDesc ASC";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlExpCat.DataSource = ds;
                ddlExpCat.DataTextField = "ExpCatCode";
                ddlExpCat.DataValueField = "ExpCatID";
                ddlExpCat.DataBind();

                ddlExpCat.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }

    public void fillTransType()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "select distinct(TransType) as TransType from ExpJournal Order by TransType ASC";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlTransType.DataSource = ds;
                ddlTransType.DataTextField = "TransType";
                ddlTransType.DataValueField = "TransType";
                ddlTransType.DataBind();

                ddlTransType.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }
    public void fillRestType()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "select distinct(RestTypeFrom) as RestType from ExpJournal where RestTypeFrom is not null";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlRestType.DataSource = ds;
                ddlRestType.DataTextField = "RestType";
                ddlRestType.DataValueField = "RestType";
                ddlRestType.DataBind();

                ddlRestType.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }
    public void fillEvent()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "Select EventId,Name from Event Order by EventID ASC";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlEvent.DataSource = ds;
                ddlEvent.DataTextField = "Name";
                ddlEvent.DataValueField = "EventId";
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("Select", "0"));

                ddlEvent.Items.RemoveAt(7);
                ddlEvent.Items.RemoveAt(7);
                ddlEvent.Items.RemoveAt(9);
                ddlEvent.Items.RemoveAt(13);
                ddlEvent.Items.RemoveAt(13);

            }
        }
    }

    public void fillChapter()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "Select ChapterID,ChapterCode from Chapter";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlChapter.DataSource = ds;
                ddlChapter.DataTextField = "ChapterCode";
                ddlChapter.DataValueField = "ChapterID";
                ddlChapter.DataBind();

                ddlChapter.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }

    public void updateTransactions()
    {
        if (validateTransForUpdate() == "1")
        {

            string payee = ddlpayee.SelectedValue;

            double Amount = Convert.ToDouble(txtAmount.Text);
            string CheckDate = txtCheckDate.Text;
            string CheckNumber = txtCheckNo.Text;
            string BankID = ddlbank.SelectedValue;
            string ToBank = ddlTobank.SelectedValue;
            string ExpCat = ddlExpCat.SelectedValue;
            string TransType = ddlTransType.SelectedValue;
            string RestType = ddlRestType.SelectedValue;
            string EventID = ddlEvent.SelectedValue;
            string Chapter = ddlChapter.SelectedValue;
            string Account = txtAccount.Text;
            string Year = txtEventYear.Text;
            string ReportDate = txtReportDate.Text;
            string DonorType = hdnDonorType.Value;
            DataSet ds = new DataSet();
            string cmdText = "";
            if (ToBank == "0")
            {
                ToBank = "null";
            }
            else
            {
                ToBank = "'" + ToBank + "'";
            }
            cmdText = "select ExpCatCode from ExpenseCategory where ExpCatID=" + ExpCat + "";
            string expCode = "";
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    expCode = ds.Tables[0].Rows[0]["ExpCatCode"].ToString();
                }
            }
            cmdText = "Update ExpJournal set ReimbMemberID=" + payee + ",DonorType='" + DonorType + "',CheckNumber='" + CheckNumber + "',DatePaid='" + CheckDate + "',ExpenseAmount='" + Amount + "',BanKID=" + BankID + ",ToBankID=" + ToBank + ",ExpCatID=" + ExpCat + ",ExpCatCode='" + expCode + "',TransType='" + TransType + "',RestTypeFrom='" + RestType + "',EventID=" + EventID + ",ToChapterID=" + Chapter + ",EventYear='" + Year + "',Account='" + Account + "',ReportDate='" + ReportDate + "' where TransactionID=" + hdnTrnsactionID.Value + "";


            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblErrMsg.Text = "Transactions updated successfully";
            lblErrMsg.ForeColor = Color.Blue;
            bindReviewExpenseTran();
            tblUpdateTransactions.Visible = false;
        }
        else
        {
            validateTransForUpdate();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        updateTransactions();

    }
    public string validateTransForUpdate()
    {
        string retVal = "1";
        if (ddlpayee.SelectedValue == "0")
        {
            retVal = "-1";

            setMessage("Please fill Payee");
        }
        else if (txtAmount.Text == "")
        {
            retVal = "-1";

            setMessage("Please fill Amount");
        }
        else if (txtCheckDate.Text == "")
        {
            retVal = "-1";

            setMessage("Please fill Check Date");
        }
        else if (txtCheckNo.Text == "")
        {
            retVal = "-1";
            lblErrMsg.Text = "Please fill Check No";
            lblErrMsg.ForeColor = Color.Red;
        }
        else if (txtEventYear.Text == "")
        {
            retVal = "-1";

            setMessage("Please fill Year");
        }
        else if (txtAccount.Text == "")
        {
            retVal = "-1";
            lblErrMsg.Text = "Please fill Account";
            lblErrMsg.ForeColor = Color.Red;
        }
        else if (txtReportDate.Text == "")
        {
            retVal = "-1";

            setMessage("Please fill Report Date");
        }
        else if (ddlbank.SelectedValue == "0")
        {
            retVal = "-1";

            setMessage("Please select Bank");
        }

        else if (ddlExpCat.SelectedValue == "0")
        {
            retVal = "-1";

            setMessage("Please select Exp Cat");
        }
        else if (ddlTransType.SelectedValue == "0")
        {
            retVal = "-1";

            setMessage("Please select Trans Type");
        }
        else if (ddlRestType.SelectedValue == "0")
        {
            retVal = "-1";

            setMessage("Please select Rest Type");
        }
        else if (ddlEvent.SelectedValue == "0")
        {
            retVal = "-1";
            setMessage("Please select Event");

        }
        else if (ddlChapter.SelectedValue == "0")
        {
            retVal = "-1";
            setMessage("Please select Chapter");

        }


        return retVal;
    }
    public void setMessage(string Message)
    {
        lblErrMsg.Text = Message;
        lblErrMsg.ForeColor = Color.Red;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        tblUpdateTransactions.Visible = false;
        lblErrMsg.Text = "";
    }

    public void BindNotCashed()
    {
        DataSet ds = new DataSet();
        string cmdText = "";
        cmdText = "select EJ.DatePaid,EJ.BankID,EJ.CheckNumber,EJ.ReimbMemberID,Ej.DonorType,SUM(ExpenseAmount)Amount,EJ.BanKID, case when EJ.DonorType='OWN' then(OI.ORGANIZATION_NAME) else IP.FirstName end as ReImbName,case when EJ.CheckNumber IS null then 'Checks Not Issued' when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber  then 'Cashed' else 'Not Cashed' end as TransStatus ,(select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber) as ckNo ,case when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber  then (select TransDate   from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)  else null end as StatusDate,B.BankCode from ExpJournal EJ   left join Event E on(E.EventID=EJ.EventID) left join Chapter C on (C.ChapterID=EJ.ToChapterID) inner join ExpenseCategory EC on(EC.ExpCatID=EJ.ExpCatID and EC.Account=EJ.Account)left join Bank B on(B.BankID=EJ.BanKID) left join Bank B1 on(B1.BankID=EJ.ToBankID)  left join OrganizationInfo OI on (OI.AutomemberID=EJ.ReimbMemberID) left join INDSpouse IP on (IP.AutoMemberID=EJ.ReimbMemberID) where EJ.CheckNumber is not null and EJ.CheckNumber Not In (select CheckNumber from BankTrans where CheckNumber=EJ.CheckNumber and  BanKID=EJ.BanKID) and  EJ.CheckNumber Not In (select ExpCheckNo from BrokTrans where ExpCheckNo=EJ.CheckNumber and  BanKID=EJ.BanKID)  and EJ.CheckNumber<100000 group by EJ.DatePaid, EJ.BankID, EJ.CheckNumber, EJ.ReimbMemberID, EJ.DonorType,OI.ORGANIZATION_NAME,IP.FirstName,B.BankCode order by EJ.BankID";

        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                grdNotCashed.Visible = true;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    tdExport.Visible = true;
                    grdReviewExpenseTran.Visible = false;
                    lblTransTitle.Visible = false;
                    lblNotChashedTitle.Text = "Table 2 : Not Cashed Checks";
                    grdNotCashed.DataSource = ds.Tables[0];
                    grdNotCashed.DataBind();

                }
                else
                {
                    grdNotCashed.DataSource = ds.Tables[0];
                    grdNotCashed.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }



    }
    protected void grdNotCashed_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdNotCashed.PageIndex = e.NewPageIndex;
        BindNotCashed();
    }

    public void BindVoidedChecks()
    {
        DataSet ds = new DataSet();
        string CmdText = "";

        ArrayList arr = new ArrayList();

        CmdText = "select distinct(bankID) from ExpJournal where BankID is not null order by BankID asc";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0] != null)
        {

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    arr.Add(dr["BankID"].ToString());
                }
            }
        }

        CmdText = "";
        CmdText += " Declare @i as int=1 ";
        CmdText += "Declare @j as int=200 ";
        CmdText += "Declare @count as int ";

        CmdText += " DECLARE @Table TABLE( ";
        CmdText += " CheckNumber int NOT NULL,";
        CmdText += " bankID varchar(30) NOT NULL,";
        CmdText += " TransStatus varchar(50) Not Null,";
        CmdText += " CashedDate varchar(100),";
        CmdText += " CheckDate varchar(100),";
        CmdText += " Amount varchar(100),";
        CmdText += " Payee varchar(100),";
        CmdText += " DonorType varchar(100)";
        CmdText += " ); ";

        for (int i = 0; i < arr.Count; i++)
        {
            int bankID = Convert.ToInt32(arr[i]);

            string bankText = "select bankCode from Bank where BankID =" + bankID + "";
            string bankName = string.Empty;
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, bankText);
            if (ds.Tables[0] != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    bankName = ds.Tables[0].Rows[0]["bankCode"].ToString();
                }
            }
            if (bankID != 4)
            {
                CmdText += " set @i=(select min(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
                CmdText += " set @j=(select max(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
            }
            else
            {
                CmdText += " set @i=140";
                CmdText += " set @j=(select max(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
            }
            CmdText += " while @i<@j";
            CmdText += " Begin";

            CmdText += " set @Count=(select COUNT(*) from ExpJournal where CheckNumber=@i and BankID=" + bankID + ")";
            CmdText += " if @count=0";
            CmdText += " Begin";
            CmdText += " insert into @Table(CheckNumber,BankID,TransStatus)values(@i,'" + bankName + "','Voided') End";
            CmdText += " set @i=@i+1 End";
        }
        CmdText += " select * from @Table";
        ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0] != null)
            {
                grdNotCashed.Visible = true;
                grdVoidedChecks.Visible = true;
                lblVoidedChecksTitle.Text = "Table 1 : Voided Checks";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    tdExport.Visible = true;
                    grdReviewExpenseTran.Visible = false;
                    lblTransTitle.Visible = false;
                    lblVoidedChecksTitle.Text = "Table 1 : Voided Checks";
                    grdVoidedChecks.DataSource = ds.Tables[0];
                    grdVoidedChecks.DataBind();

                }
                else
                {
                    grdVoidedChecks.DataSource = ds.Tables[0];
                    grdVoidedChecks.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grdVoidedChecks_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdVoidedChecks.PageIndex = e.NewPageIndex;
        BindVoidedChecks();
    }
    public void ExportToExcelVoidNotCashedChecks()
    {
        DataSet ds = new DataSet();

        string CmdText = "";

        ArrayList arr = new ArrayList();

        CmdText = "select distinct(bankID) from ExpJournal where BankID is not null order by BankID asc";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0] != null)
        {

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    arr.Add(dr["BankID"].ToString());
                }
            }
        }

        CmdText = "";
        CmdText += " Declare @i as int=1 ";
        CmdText += "Declare @j as int=200 ";
        CmdText += "Declare @count as int ";

        CmdText += " DECLARE @Table TABLE( ";
        CmdText += " CheckNumber int NOT NULL,";
        CmdText += " bankID varchar(30) NOT NULL,";
        CmdText += " Status varchar(50) Not Null,";
        CmdText += " CashedDate varchar(100),";
        CmdText += " CheckDate varchar(100),";
        CmdText += " Amount varchar(100),";
        CmdText += " Payee varchar(100),";
        CmdText += " DonorType varchar(100)";
        CmdText += " ); ";

        for (int i = 0; i < arr.Count; i++)
        {
            int bankID = Convert.ToInt32(arr[i]);

            string bankText = "select bankCode from Bank where BankID =" + bankID + "";
            string bankName = string.Empty;
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, bankText);
            if (ds.Tables[0] != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    bankName = ds.Tables[0].Rows[0]["bankCode"].ToString();
                }
            }
            if (bankID != 4)
            {
                CmdText += " set @i=(select min(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
                CmdText += " set @j=(select max(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
            }
            else
            {
                CmdText += " set @i=140";
                CmdText += " set @j=(select max(CheckNumber) from ExpJournal where BankID=" + bankID + " and CheckNumber<100000)";
            }
            CmdText += " while @i<@j";
            CmdText += " Begin";

            CmdText += " set @Count=(select COUNT(*) from ExpJournal where CheckNumber=@i and BankID=" + bankID + ")";
            CmdText += " if @count=0";
            CmdText += " Begin";
            CmdText += " insert into @Table(CheckNumber,BankID,Status)values(@i,'" + bankName + "','Voided') End";
            CmdText += " set @i=@i+1 End";
        }
        CmdText += " select * from @Table;";

        CmdText += " select EJ.DatePaid,EJ.BankID,B.BankCode,EJ.CheckNumber,EJ.ReimbMemberID,case when EJ.DonorType='OWN' then(OI.ORGANIZATION_NAME) else IP.FirstName end as Payee,Ej.DonorType,SUM(ExpenseAmount)Amount, case when EJ.CheckNumber IS null then 'Checks Not Issued' when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber  then 'Cashed' else 'Not Cashed' end as [Status] ,case when (select CheckNumber from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)=EJ.CheckNumber  then (select TransDate   from BankTrans where BankID=EJ.BankID and checkNumber=EJ.CheckNumber)  else null end as CashedDate from ExpJournal EJ   left join Event E on(E.EventID=EJ.EventID) left join Chapter C on (C.ChapterID=EJ.ToChapterID) inner join ExpenseCategory EC on(EC.ExpCatID=EJ.ExpCatID and EC.Account=EJ.Account)left join Bank B on(B.BankID=EJ.BanKID) left join Bank B1 on(B1.BankID=EJ.ToBankID)  left join OrganizationInfo OI on (OI.AutomemberID=EJ.ReimbMemberID) left join INDSpouse IP on (IP.AutoMemberID=EJ.ReimbMemberID) where EJ.CheckNumber is not null and EJ.CheckNumber Not In (select CheckNumber from BankTrans where CheckNumber=EJ.CheckNumber and  BanKID=EJ.BanKID) and  EJ.CheckNumber Not In (select ExpCheckNo from BrokTrans where ExpCheckNo=EJ.CheckNumber and  BanKID=EJ.BanKID) and EJ.CheckNumber<100000 group by EJ.DatePaid, EJ.BankID, EJ.CheckNumber, EJ.ReimbMemberID, EJ.DonorType,OI.ORGANIZATION_NAME,IP.FirstName,B.BankCode order by EJ.BankID;";

        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);

        if (ds.Tables[0] != null)
        {
            ds.Tables[0].TableName = "Voided Checks";
            ds.Tables[1].TableName = "Not Cashed Checks";

            DateTime dt = DateTime.Now;
            string month = dt.ToString("MMM");
            string day = dt.ToString("dd");
            string year = dt.ToString("yyyy");
            string monthDay = month + "" + day;

            string payeeName = (ddReviewBy.SelectedItem.Text).Replace(" ", "");
            string filename = "ExpenseTransactions_" + payeeName + "_" + monthDay + "_" + year + ".xls";
            ExcelHelper.ToExcel(ds, filename, Page.Response);
        }
    }
}