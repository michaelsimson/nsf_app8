﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_xlReportRegPrepByChild : System.Web.UI.Page
{
    int chapID;
    string Year = "";
    DataTable dt;
    DateTime[] day = { DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900") };

    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        Year = SqlHelper.ExecuteScalar(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(), CommandType.Text, "Select max(Eventyear) From registration_prepclub").ToString();
        // System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        lblChapter.Text = GetChapterName(chapID);
        GetData(chapID);
        GetVenueData(chapID);
    }
    private void GetData(int chapterid)
    {

        string commandString = " select distinct First_Name+''+Last_Name as Name ,DENSE_RANK() OVER(ORDER BY c.First_name) AS 'Ser#', " +

            " c.First_name,c.Last_name ,C.DATE_OF_BIRTH, C.GRADE ,p.MemberId from child c " + //,registration_prepclub.ParentID as MemberID 

            " left join registration_prepclub p on p.childnumber=c.childnumber  left join chapter ch on ch.ChapterId=p.chapterid where ch.chapterid=" + chapterid + "";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandString);
        DataTable dtmerge = new DataTable();

        DataGridVIEW.DataSource = ds.Tables[0];
        DataGridVIEW.DataBind();


    }
    private void GetVenueData(int chapterid)
    {

        string commandString = "select distinct organization_name from  Registration_PrepClub  Rp inner join organizationinfo o on" +

            " Rp.VenueID=o.automemberid where Rp.ChapterId=" + chapterid + "";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandString);
        DataTable dtmerge = new DataTable();

        GridVenueDetails.DataSource = ds.Tables[0];
        GridVenueDetails.DataBind();


    }
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        //String strChapterName = GetChapterName(chapID);
        //strChapterName = strChapterName.Replace(",", "");
        //Response.Clear();
        //Response.AppendHeader("content-disposition", "attachment;filename=RegistrationMatrix_" + strChapterName + "_" + Year + ".xls");
        //Response.Charset = "";
        ////Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error

        //Response.ContentType = "application/vnd.xls";
        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        ////DataGrid1.RenderControl(hw);
        //Response.Write(sw.ToString());
        //Response.End();
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("../VolunteerFunctions.aspx");
    }
}