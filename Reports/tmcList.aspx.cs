using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_tmcList : System.Web.UI.Page
{
   DataView DV; 
   string Year = "";
   string connectionString =
           System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

   // create and open the connection object
   System.Data.SqlClient.SqlConnection connection; 
   string commandString = ""; 

   private void Page_Load(object sender, EventArgs e)
    {
        //Year = System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        Year = SqlHelper.ExecuteScalar(connectionString.ToString(), CommandType.Text, "Select Max(ContestYear) From Contestant").ToString();
        int iRoleSelected = 1;
        int iZoneId = -1;
        int iClusterId = -1;
        int iChapterId = -1;
        String strSQLCommand;

        String strRoleSelected = "";
        String strZoneId = "";
        String strClusterId = "";
        String strChapterId = "";
         
        try
        {
            if (((Session["selZoneID"] != null) && ((String)Session["selZoneID"] != "")))
            {
                strZoneId = (String)Session["selZoneID"];
            }
//            Response.Write("strZoneId::" + strZoneId + "\n");

            if ((strZoneId != null) && (strZoneId.Length > 0))
            {
                iZoneId = Convert.ToInt32(strZoneId);
            }

            if (((Session["selClusterID"] != null) && ((String)Session["selClusterID"] != "")))
            {
                strZoneId = (String)Session["selClusterID"];
            }
 //           Response.Write("strClusterId::" + strClusterId + "\n");
            if ((strClusterId != null) && (strClusterId.Length > 0))
            {
                iClusterId = Convert.ToInt32(strClusterId);
            }

            if (((Session["selChapterID"] != null) && ((String)Session["selChapterID"] != "")))
            {
                strChapterId = (String)Session["selChapterID"];
            }
 //           Response.Write("strChapterId::" + strChapterId + "\n");
            if ((strChapterId != null) && (strChapterId.Length > 0))
            {
                iChapterId = Convert.ToInt32(strChapterId);
            }

            if (((Session["RoleId"] != null) && ((String)Session["RoleId"] != "")))
            {
                strRoleSelected = (String)Session["RoleId"];
            }
 //           Response.Write("strRoleSelected::" + strRoleSelected + "\n");

            if ((strRoleSelected != null) && (strRoleSelected.Length > 0))
            {
                iRoleSelected = Convert.ToInt32(strRoleSelected);
            }
        }
        catch (Exception e123)
        {
            Response.Write("e123::" + e123 + "\n");
        }

        // create the command object and set its
        // command string and connection
        if ((iRoleSelected == 3) && (iZoneId != -1)) //Zonal coordinator
        {
          strSQLCommand = "Select ChapterID, name, State, ChapterCode, zoneid, zonecode from Chapter WHERE status ='A' and zoneid = " + iZoneId + " order by name";
        }
        else if ((iRoleSelected == 4) && (iClusterId != -1)) // Cluster coordinator
        {
          strSQLCommand = "Select ChapterID, name, State, ChapterCode, clusterid, clustercode from Chapter WHERE status ='A' and clusterid = " + iClusterId + " order by name";
        }
        else if ((iRoleSelected == 5) && (iChapterId != -1)) // Chapter coordinator
        {
          strSQLCommand = "Select ChapterID, name, State from Chapter WHERE status ='A' and ChapterID = " + iChapterId + " order by name";
        }
        else
        {
          strSQLCommand = "Select ChapterID, name, State from Chapter WHERE status ='A' order by name";
        }
        BindGrid(strSQLCommand);
    }

    public void BindGrid(String commandString)
    {
        // connect to the peoducts database
        connection = new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        DataSet dsContests = new DataSet();
        daContests.Fill(dsContests);
        DataTable dt = new DataTable();
        connection.Close();

        dt.Columns.Add("chapterid", typeof(int));
        dt.Columns.Add("name", typeof(string));
        dt.Columns.Add("state", typeof(string));
        dt.Columns.Add("date", typeof(string));

        int chapterid = 0;
        DataRow dr;

        for (int i = 0; i < dsContests.Tables[0].Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["chapterid"] = Convert.ToInt32(dsContests.Tables[0].Rows[i].ItemArray[0]);
            chapterid = Convert.ToInt32(dsContests.Tables[0].Rows[i].ItemArray[0]);
            dr["name"] = dsContests.Tables[0].Rows[i].ItemArray[1];
            dr["state"] = dsContests.Tables[0].Rows[i].ItemArray[2];
            dr["date"] = GetDate(chapterid);
            dt.Rows.Add(dr);
        }
        DV = dt.DefaultView;
        DV.Sort = "date";
        DataGrid1.DataSource = DV;
        DataGrid1.DataBind();
    }
            // connect to the peoducts database
           // string conString =
            //System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
           // System.Data.SqlClient.SqlConnection con =
           //     new System.Data.SqlClient.SqlConnection(conString);
            //con.Open();

       private string GetDate(int ChapterID)
       {
            connection = new System.Data.SqlClient.SqlConnection(connectionString);
            connection.Open();

            // get records from the products table
             commandString = "Select ContestDate from Contest where NSFChapterID = " + ChapterID +
                " AND Contest_Year=" + Year;

            // create the command object and set its
            // command string and connection
            System.Data.SqlClient.SqlCommand command =
                new System.Data.SqlClient.SqlCommand();
            command.CommandText = commandString;
            command.Connection = connection;
            System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
            string aString = "";
            DateTime firstValue = DateTime.Parse("1/1/2020");
            DateTime readValue = DateTime.Parse("1/1/1900");
            while (reader.Read())
            {
                aString = reader.GetValue(0).ToString();
                if (aString != "")
                {
                    readValue = Convert.ToDateTime(aString);
                    if (readValue < firstValue && readValue > DateTime.Parse("1/1/1900"))
                        firstValue = readValue;
                }
            }
            // close connection, return values
            string mmddyyy = string.Format("{0: MM/dd/yyyy}", firstValue);

            connection.Close();
            if (readValue > DateTime.Parse("1/1/1900"))
                return mmddyyy;
            else
                return "";
                            
        }
        public void DataGrid1_Sort(Object s, DataGridSortCommandEventArgs e)
        {
            DV.Sort = e.SortExpression;
            DataGrid1.DataSource = DV;
            DataGrid1.DataBind();
        }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("../VolunteerFunctions.aspx");
    }
}
    

