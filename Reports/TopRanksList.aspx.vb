﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Reports_TopRanksList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        ' lblmessage.Text = ""
        'lblselected.Text = ""
        Dim roleid As Integer = Session("RoleID")

        If roleid = 1 Or roleid = 2 Then
            chkAllChapter.Visible = True
        Else
            chkAllChapter.Visible = False

        End If
        'User code to initialize the page here
        'only Roleid = 1, 2, 3, 4, 5 Can access this page
        If Not Page.IsPostBack Then

            Session("Productid") = ""
            ' Load Year into ListBox
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlYear.Items.Insert(2, Convert.ToString(year - 1))
            ddlYear.Items.Insert(3, Convert.ToString(year - 2))
            ddlYear.Items.Insert(4, Convert.ToString(year - 3))
            ddlYear.Items.Insert(5, Convert.ToString(year - 4))
            ddlYear.Items(1).Selected = True
            Session("Year") = ddlYear.Items(1).Text
            LoadProductGroup()   ' method to  Load Product group ListBox
            LoadProductID()      ' method to  Load Product ID ListBox

            'lblChapter.Text = Session("LoginChapterName").ToString() 'Session("loginChapterID").ToString()
            lblChapter.Text = (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ChapterCode from Chapter Where ChapterID=" & Session("loginChapterID"))).ToString.Replace(",", "_")

        End If
    End Sub



    Private Sub LoadProductGroup()
        Dim roleid As Integer = Session("RoleID")

        Dim strSql As String '= "Select ProductGroupID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
        If Session("LoginChapterID") = "1" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
        Else
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear = " & ddlYear.SelectedValue & " and c.RegionalStatus = 'Active' and P.EventId =2  order by P.ProductGroupID"
        End If
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()

        lstProductGroup.Items.Insert(0, "All")


        lstProductGroup.Items(0).Selected = True
    End Sub

    Private Sub LoadProductID()

        Dim roleid As Integer = Session("RoleID")
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim eventid As String
        If Session("LoginChapterID") = "1" Then
            eventid = 1
        Else
            eventid = 2
        End If
        Dim productgroup As String = ""
        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            lstProductid.Enabled = False
            Dim sbvalues As New StringBuilder
            Dim strSql1 As String '= "Select ProductGroupID as ID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
            ' strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            If eventid = "1" Then
                strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
            ElseIf eventid = "2" Then
                strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If

        Dim strSql As String '= "Select ProductID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        strSql = "Select ProductCode, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductid.DataSource = drproductid
        lstProductid.DataBind()

        lstProductid.Items.Insert(0, "All")


        lstProductid.Items(0).Selected = True
    End Sub

    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (lstProductGroup.SelectedItem.Text.ToLower = "essay writing" Or lstProductGroup.SelectedItem.Text.ToLower = "public speaking") Then
            rdoPerfectScoreList.Visible = False
        Else
            rdoPerfectScoreList.Visible = True
        End If

        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productid") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub btnShowList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowList.Click

        Session("ChapterId") = Session("LoginChapterID")

        If (chkAllChapter.Checked) Then
            Dim strSQLChapter As String, sbChapterId As New StringBuilder
            strSQLChapter = "Select Chapterid from Chapter"
            Dim dsChapter As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQLChapter)
            If dsChapter.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsChapter.Tables(0).Rows.Count - 1
                    sbChapterId.Append("'")
                    sbChapterId.Append(dsChapter.Tables(0).Rows(i).Item("ChapterId").ToString)
                    sbChapterId.Append("'")
                    If i < dsChapter.Tables(0).Rows.Count - 1 Then
                        sbChapterId.Append(",")
                    End If
                Next
                Session("ChapterId") = sbChapterId.ToString
            End If


        End If



        'check whether any option is selected - 
        'show report as topranklist
        Dim eventid As String
        If Session("LoginChapterID") = "1" Then
            eventid = 1
        Else
            eventid = 2
        End If
        GVList.Visible = rdoTopRankList.Checked
        GVPerfectScoreList.Visible = Not rdoTopRankList.Checked

        If (rdoTopRankList.Checked) Then

            Dim wherecntn As String = ""
            If Not Session("ProductGroupId") = "" Then
                If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item of ProductId is ALL
                    Dim sbvalues As New StringBuilder
                    Dim StrSQL1 As String
                    StrSQL1 = "Select ProductCode as ID, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL1)
                    If ds1.Tables.Count > 0 Then
                        Dim i As Integer
                        For i = 0 To ds1.Tables(0).Rows.Count - 1
                            sbvalues.Append("'")
                            sbvalues.Append(ds1.Tables(0).Rows(i).Item("ID").ToString)
                            sbvalues.Append("'")
                            If i < ds1.Tables(0).Rows.Count - 1 Then
                                sbvalues.Append(",")
                            End If
                        Next
                        Session("Productid") = sbvalues.ToString
                    End If
                End If
                wherecntn = " and a.Productcode in (" & Session("Productid") & ")"
            End If
            Dim StrSQl As String = "select a.contestYear, a.productcode, a.rank,c.Last_Name as CLName , c.First_Name as CFname, a.Grade ,a.parentid, "
            StrSQl = StrSQl & " CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.email END as emailFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.lastname END as LastNameFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.firstname END as FirstNameFather, b.hphone as HPhoneFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.cphone END as CPhoneFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.wphone END as WPhoneFather,"
            StrSQl = StrSQl & " CASE isnull(d.gender,'Female') WHEN 'Female' then d.email ELSE b.email END as emailMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.lastname ELSE b.lastname END as LastNameMother, CASE isnull(d.gender,'Female') WHEN 'Female' then d.firstname ELSE b.firstname END as FirstNameMother, d.hphone as HPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.CPhone ELSE b.CPhone  END as CPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.wPhone ELSE b.wPhone END as wPhoneMother ,b.address1,"
            StrSQl = StrSQl & " isnull(c.SchoolName,'') as SchoolName, isnull(c.Acheivements,'') as Achievements, isnull(c.Hobbies,'') as Hobbies,b.address2, b.city, b.state, b.zip  from contestant a " '
            StrSQl = StrSQl & " Inner Join indspouse b on b.automemberid = a.parentid"
            StrSQl = StrSQl & " Inner Join child c on c.childnumber=a.childnumber"
            StrSQl = StrSQl & " Left Join indspouse d on d.Relationship = a.Parentid   "
            StrSQl = StrSQl & " where a.contestyear in (" & GetSelYear() & ") and a.chapterid in (" & Session("ChapterId") & ") and a.PaymentReference <>'' " & wherecntn & " and (a.rank between 1 and " & ddlTopCount.SelectedValue & ") order by a.productid,a.contestYear desc, a.rank"
            'Response.Write(StrSQl)
            Try
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
                If ds.Tables(0).Rows.Count > 0 Then
                    GVList.Visible = True
                    GVList.DataSource = ds
                    GVList.DataBind()
                    If chkIncludeAchievements.Checked Then
                        GVList.Columns(18).Visible = True
                        GVList.Columns(19).Visible = True
                        GVList.Columns(20).Visible = True
                    Else
                        GVList.Columns(18).Visible = False
                        GVList.Columns(19).Visible = False
                        GVList.Columns(20).Visible = False
                    End If



                    btnExport.Visible = True
                    lblErr.Text = ""
                Else
                    GVList.Visible = False
                    btnExport.Visible = False
                    lblErr.Text = "Sorry No Record Found "
                End If
            Catch ex As Exception
                ' Response.Write(ex.ToString)
                'Response.Write(StrSQl)
            End Try

        Else 'show report by perfect scores list
            Try


                Dim prdgrpCntn As String
                prdgrpCntn = " ("
                Dim strSQLMaxScores As String
                strSQLMaxScores = "SELECT  * from MaxScoresByPhase where year = " & ddlYear.SelectedItem.Text & " and  productgroupid in (" & Session("ProductGroupId") & ")"

                Dim dsMaxScores As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQLMaxScores)
                If dsMaxScores.Tables.Count > 0 And dsMaxScores.Tables(0).Rows.Count > 0 Then
                    Dim indx As Integer
                    Dim imax As Integer
                    imax = dsMaxScores.Tables(0).Rows.Count - 1
                    For indx = 0 To dsMaxScores.Tables(0).Rows.Count - 1
                        prdgrpCntn = prdgrpCntn & " (CTST.Score1+CTST.Score2=" & dsMaxScores.Tables(0).Rows(indx).Item("MaxScores").ToString & " and CTST.ProductGroupId=" & dsMaxScores.Tables(0).Rows(indx).Item("ProductGroupId").ToString & ")"
                        If indx <> imax Then

                            prdgrpCntn &= " or "
                        End If

                    Next
                    prdgrpCntn = prdgrpCntn & ")"

                    Dim wherecntn As String = ""
                    If Not Session("ProductGroupId") = "" Then
                        If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item of ProductId is ALL
                            Dim sbvalues As New StringBuilder
                            Dim StrSQL1 As String
                            StrSQL1 = "Select ProductCode as ID, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL1)
                            If ds1.Tables.Count > 0 Then
                                Dim i As Integer
                                For i = 0 To ds1.Tables(0).Rows.Count - 1
                                    sbvalues.Append("'")
                                    sbvalues.Append(ds1.Tables(0).Rows(i).Item("ID").ToString)
                                    sbvalues.Append("'")
                                    If i < ds1.Tables(0).Rows.Count - 1 Then
                                        sbvalues.Append(",")
                                    End If
                                Next

                                Session("Productid") = sbvalues.ToString
                            End If
                        End If
                        wherecntn = " and CTST.Productcode in (" & Session("Productid") & ")"
                    End If
                    Dim strSQL As String = "Select CTST.badgenumber, PROD.Name 'productname', CONVERT(VARCHAR(10),CTST.createdate,101) as createdate , CHLD.ChildNumber 'childnumber', CTST.Parentid 'parentid', CHLD.First_Name as Cfirstname, CHLD.Last_Name as Clastname, chld.gender as Cgender,"
                    strSQL = strSQL & " CTST.Grade, CONVERT(VARCHAR(10),CHLD.DATE_OF_BIRTH,101) as DOB, ind.firstname as Pfirstname, ind.lastname as Plastname, ind.gender as Pgender, ind.hphone, ind.cphone, ind.email, CHPT.ChapterCode as PartChapter,"
                    strSQL = strSQL & " IND.Chapter as HomeChapter, Ind.City, Ind.State, CTST.ContestYear,  CTST.Score1,	CTST.Score2, CTST.Score3, CTST.Rank, ind.city, ind.state, isnull(chld.SchoolName,'') as SchoolName,isnull(chld.acheivements,'') as Achievements,isnull(chld.hobbies,'') as Hobbies "
                    strSQL = strSQL & " From	dbo.Contestant CTST	Inner Join dbo.Child CHLD ON CTST.ChildNumber = CHLD.ChildNumber "
                    strSQL = strSQL & " INNER JOIN dbo.IndSpouse IND ON IND.AutoMemberID = CTST.ParentID "
                    strSQL = strSQL & " INNER JOIN dbo.Product	PROD ON PROD.ProductID = CTST.ProductID "
                    strSQL = strSQL & " INNER JOIN dbo.Chapter  CHPT ON CHPT.ChapterID = CTST.ChapterID  where CTST.ContestYear =  (" & GetSelYear() & ")  and  "
                    strSQL = strSQL & " CTST.chapterid in (" & Session("ChapterId") & ") and CTST.paymentreference<>''  " & wherecntn & " and CTST.ProductGroupID In (" & Session("ProductGroupId") & ") and CTST.EventId=" & eventid & " and " & prdgrpCntn & ""
                    strSQL = strSQL & " and (CTST.rank between 1 and " & ddlTopCount.SelectedValue & ") "
                    strSQL = strSQL & " order by CTST.productid, CTST.Rank"

                    Try
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
                        If ds.Tables(0).Rows.Count > 0 Then
                            GVPerfectScoreList.Visible = True
                            GVPerfectScoreList.DataSource = ds
                            GVPerfectScoreList.DataBind()

                            If chkIncludeAchievements.Checked Then
                                GVPerfectScoreList.Columns(22).Visible = True
                                GVPerfectScoreList.Columns(23).Visible = True
                                GVPerfectScoreList.Columns(24).Visible = True
                            Else
                                GVPerfectScoreList.Columns(22).Visible = False
                                GVPerfectScoreList.Columns(23).Visible = False
                                GVPerfectScoreList.Columns(24).Visible = False
                            End If


                            btnExport.Visible = True
                            lblErr.Text = ""
                        Else
                            GVPerfectScoreList.Visible = False
                            btnExport.Visible = False
                            lblErr.Text = "Sorry No Record Found "
                        End If
                    Catch ex As Exception
                        ' Response.Write(ex.ToString)
                        'Response.Write(StrSQl)
                    End Try


                Else
                    GVPerfectScoreList.Visible = False
                    btnExport.Visible = False
                    lblErr.Text = "Sorry No Record Found "
                End If
            Catch ex As Exception
                Response.Write("Errror :" & ex.ToString)
            End Try


        End If

    End Sub

    Function GetSelYear() As String
        Dim i As Integer
        Dim st As String = ""
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To ddlYear.Items.Count - 1
            If ddlYear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = ddlYear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            st = sbvalues.ToString
        End If
        Return st
    End Function
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Dim stringWrite As New System.IO.StringWriter()

        If (GVList.Visible) Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=TopRanksList_" & ddlYear.SelectedValue & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            GVList.RenderControl(htmlWrite)
        ElseIf (GVPerfectScoreList.Visible) Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=PerfectScoresList_" & ddlYear.SelectedValue & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            GVPerfectScoreList.RenderControl(htmlWrite)
        End If

        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub rdoTopRankList_CheckedChanged(sender As Object, e As EventArgs) Handles rdoTopRankList.CheckedChanged
        btnShowList_Click(sender, e)
    End Sub

    Protected Sub rdoPerfectScoreList_CheckedChanged(sender As Object, e As EventArgs) Handles rdoPerfectScoreList.CheckedChanged
        btnShowList_Click(sender, e)
    End Sub


End Class
