﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Data;
public partial class Reports_FundRContestRegReport : System.Web.UI.Page
{
    int chapID = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                chapID = Convert.ToInt32(Request.QueryString["Chap"]);
                int year = Convert.ToInt32(DateTime.Now.Year);
                ddlYear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(year)));
                ddlYear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
                ddlYear.SelectedIndex = 1;
                prod(chapID, year);
            }
        }
    }

    private void prod(int chapID, int yy)
    {
        LstProducts.Items.Clear();
        LstProducts.Items.Add("All");
        LstProducts.SelectedIndex = LstProducts.Items.IndexOf(LstProducts.Items.FindByValue("All"));
        int j = 0;
        string str = "Select distinct(FR.productid), p.name from FundRContestReg FR, product p where FR.productid = p.productid and FR.Eventyear = " + ddlYear.SelectedValue + " and FR.Chapterid = " + chapID + "";
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.Connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListItem li = new ListItem();
                String af = reader["productid"].ToString();
                li.Value = af;
                li.Text = reader[1].ToString();
                LstProducts.Items.Add(li);
                j = 1;

                BtnGenerateReport.Enabled = true;
            }
            cmd.Connection.Close();
            cmd.Connection.Dispose();
            if (j == 0)
            {
                LstProducts.Items.Clear();
                LstProducts.Items.Add("No Contest");
                LstProducts.SelectedIndex = LstProducts.Items.IndexOf(LstProducts.Items.FindByValue("No Contest"));
                BtnGenerateReport.Enabled = false;
            }
        }
        catch (Exception a)
        {

        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        string Year = ddlYear.SelectedItem.Text;
        int iYear = Convert.ToInt32(Year);
        prod(chapID, iYear);
    }

    public void LoadFundRContestRegReport()
    {
        dvFundRContestReg.Visible = true;
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        if (LstProducts.SelectedValue == "All")
        {
            CmdText = "select FR.EventYear,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,IP.Email,C.First_Name,C.Last_Name,C.Gender,FR.Grade,FR.Approved,E.Name as EventName,CH.ChapterCode,FC.EventDescription,FC.EventDate,FC.StartTime,FC.EndTime,C.DATE_OF_BIRTH,C.SchoolName,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.CPhone as MFCPhone,IP1.Email as MFEmail,IP1.HPhone as MFCHPhone,IP1.Gender,FR.ProductCode from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=FR.MemberId) left join Child C on (FR.ChildNumber=C.ChildNumber) inner join Event E on FR.EventID=E.EventID inner join Chapter CH on FR.ChapterID=CH.ChapterID inner join FundRaisingCal FC on (FR.EventID=FC.EventID and FR.ChapterID=FC.ChapterID)";
        }
        else
        {
            CmdText = "select FR.EventYear,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,IP.Email,C.First_Name,C.Last_Name,C.Gender,FR.Grade,FR.Approved,E.Name as EventName,CH.ChapterCode,FC.EventDescription,FC.EventDate,FC.StartTime,FC.EndTime,C.DATE_OF_BIRTH,C.SchoolName,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.CPhone as MFCPhone,IP1.Email as MFEmail,IP1.HPhone as MFCHPhone,IP1.Gender,FR.ProductCode from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=FR.MemberId) left join Child C on (FR.ChildNumber=C.ChildNumber) inner join Event E on FR.EventID=E.EventID inner join Chapter CH on FR.ChapterID=CH.ChapterID inner join FundRaisingCal FC on (FR.EventID=FC.EventID and FR.ChapterID=FC.ChapterID) where FR.EventYear=" + ddlYear.SelectedValue + " and FR.ProductID=" + LstProducts.SelectedValue + "";
        }
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                BtnExportToExcel.Visible = true;
                GrdFundRContestReg.DataSource = ds;
                GrdFundRContestReg.DataBind();
            }
            else
            {
                GrdFundRContestReg.DataSource = ds;
                GrdFundRContestReg.DataBind();
            }
        }
    }
    protected void BtnGenerateReport_Click(object sender, EventArgs e)
    {
        LoadFundRContestRegReport();
    }
    protected void BtnExportToExcel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        if (LstProducts.SelectedValue == "All")
        {
            CmdText = "select FR.EventYear,E.Name as EventName,FC.EventDate,FC.StartTime,FC.EndTime,CH.ChapterCode,FR.ProductCode as Contest,C.First_Name as 'Child FName',C.Last_Name as 'Child LName',C.Gender,C.DATE_OF_BIRTH,FR.Grade,C.SchoolName,IP.FirstName as 'Father FName',IP.LastName as 'Father LName',IP.Email as 'Father E-Mail',IP.HPhone as FatherHPhone,IP.CPhone as FatherCPhone,FR.Approved,IP1.FirstName as MotherFName,IP1.LastName as MotherLName,IP1.CPhone as MotherCPhone,IP1.Email as MotherEmail,IP1.HPhone as MotherHPhone from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=FR.MemberId) inner join Child C on (FR.ChildNumber=C.ChildNumber) inner join Event E on FR.EventID=E.EventID inner join Chapter CH on FR.ChapterID=CH.ChapterID inner join FundRaisingCal FC on (FR.EventID=FC.EventID and FR.ChapterID=FC.ChapterID)";
        }
        else
        {
            CmdText = "select FR.EventYear,E.Name as EventName,FC.EventDate,FC.StartTime,FC.EndTime,CH.ChapterCode,FR.ProductCode as Contest,C.First_Name as 'Child FName',C.Last_Name as 'Child LName',C.Gender,C.DATE_OF_BIRTH,FR.Grade,C.SchoolName,IP.FirstName as 'Father FName',IP.LastName as 'Father LName',IP.Email as 'Father E-Mail',IP.HPhone as FatherHPhone,IP.CPhone as FatherCPhone,FR.Approved,IP1.FirstName as MotherFName,IP1.LastName as MotherLName,IP1.CPhone as MotherCPhone,IP1.Email as MotherEmail,IP1.HPhone as MotherHPhone from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=FR.MemberId) inner join Child C on (FR.ChildNumber=C.ChildNumber) inner join Event E on FR.EventID=E.EventID inner join Chapter CH on FR.ChapterID=CH.ChapterID inner join FundRaisingCal FC on (FR.EventID=FC.EventID and FR.ChapterID=FC.ChapterID) where FR.EventYear=" + ddlYear.SelectedValue + " and FR.ProductID=" + LstProducts.SelectedValue + "";
        }
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["Ser#"] = i + 1;

                }
                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;
                string contestName = LstProducts.SelectedItem.Text;
                string filename = "ContestRegistrationReport_" + monthDay + "_" + year + ".xls";
                if (contestName == "All")
                {
                    filename = "ContestRegistrationReport_" + monthDay + "_" + year + ".xls";
                }
                else
                {
                    filename = "ContestRegistrationReport_" + contestName + "_" + monthDay + "_" + year + ".xls";
                }
                ds.Tables[0].TableName = "ContestRegistrationReport";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
    }
    protected void GrdFundRContestReg_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdFundRContestReg.PageIndex = e.NewPageIndex;
        LoadFundRContestRegReport();
    }
}