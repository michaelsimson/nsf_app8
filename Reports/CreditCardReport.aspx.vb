Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL

Partial Class Reports_CreditCardReport
    Inherits System.Web.UI.Page
    Protected CurrentYear As Int16 = Year(Date.Today())
    Private _iChapterid As Integer
    Private _iYear As Integer
    Private _sChapterName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Param As String
        Param = Request.QueryString("Param")

        If Param = "ByMonth" Then
            PnlByMonth.Visible = True
        ElseIf Param = "ByChapter" Then
            pnlByChapter.Visible = True
        End If

        If Not (Me.IsPostBack) Then
            dvCreditCardMonth.Visible = False
            btnExcelMonth.Visible = False
            dvCreditCardchapter.Visible = False
            btnExcelChapter.Visible = False
        End If
    End Sub
    Protected Sub btnFindMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindMonth.Click
        Try
            BindGridMonthView()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridMonthView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear As Integer
        Try

            If ddMonth.SelectedItem.Value > 0 Or ddYear.SelectedItem.Value > 0 Then
                If (ddMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddMonth.SelectedItem.Value)
                    lblHeader.Text = "CreditCard by Month"
                Else
                    iMonth = 0
                End If
                If (ddYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddYear.SelectedItem.Value)
                    lblHeader.Text = "CreditCard by Year"
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If

            Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                   CommandType.StoredProcedure, "rpt_CreditCardRepotByMonth", _
                                   New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth))

            ObjDt = objDS.Tables(0)
            If ObjDt.Rows.Count > 0 Then
                lblNoData.Text = ""
                dvCreditCardMonth.Visible = True
                btnExcelMonth.Visible = True
                dvCreditCardMonth.DataSource = ObjDt
                dvCreditCardMonth.DataBind()
            Else
                dvCreditCardMonth.DataSource = Nothing
                dvCreditCardMonth.DataBind()
                lblNoData.Text = "No data found."
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Protected Sub dvCreditCardMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvCreditCardMonth.RowDataBound
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim iRegFees As Integer = 0
            Dim iRegMeals As Integer = 0
            Dim iRegDonations As Integer = 0
            Dim iSubTotal As Integer = 0
            Dim iHomePageDonations As Integer = 0
            Dim iTotal As Integer = 0

            iRegFees += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Fees]"))
            iRegMeals += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Meals]"))
            iRegDonations += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Donations]"))
            iSubTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[SubTotal]"))
            iHomePageDonations += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Home Page Donations]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Total]"))

            e.Row.Cells(2).Width = 150
            If iRegFees > 0 Then
                e.Row.Cells(3).Text = FormatNumber(iRegFees.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(3).Text = ""
            End If

            If iRegMeals > 0 Then
                e.Row.Cells(4).Text = FormatNumber(iRegMeals.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If

            If iRegDonations > 0 Then
                e.Row.Cells(5).Text = FormatNumber(iRegDonations.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If

            If iSubTotal > 0 Then
                e.Row.Cells(6).Text = FormatNumber(iSubTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If

            If iHomePageDonations > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iHomePageDonations.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If

            If iTotal > 0 Then
                e.Row.Cells(8).Text = FormatNumber(iTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(8).Text = ""
            End If
        End If
    End Sub

    Protected Sub btnExcelMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcelMonth.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CreditCardMonth.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvCreditCardMonth.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
    Protected Sub btnFindChapter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindChapter.Click
        Try
            BindGridChapterView()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridChapterView()
        Dim ObjDt As New DataTable
        Dim iMonth, iYear, iChapterid As Integer
        Try
            iChapterid = 0
            If ddlChapterMonth.SelectedItem.Value > 0 Or ddlChapterYear.SelectedItem.Value > 0 Then
                If (ddlChapterMonth.SelectedItem.Value > 0) Then
                    iMonth = CInt(ddlChapterMonth.SelectedItem.Value)
                Else
                    iMonth = 0
                End If
                If (ddlChapterYear.SelectedItem.Value > 0) Then
                    iYear = CInt(ddlChapterYear.SelectedItem.Value)
                Else
                    iYear = 0
                End If
            Else
                iMonth = 0
                iYear = 0
            End If

            Dim objDS As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings.Item("DBConnection"), _
                                       CommandType.StoredProcedure, "rpt_CreditCardRepotByChapter", _
                                       New SqlParameter("@pyear", iYear), New SqlParameter("@pmonth", iMonth))
            ObjDt = objDS.Tables(0)
            If ObjDt.Rows.Count > 0 Then
                dvCreditCardchapter.Visible = True
                btnExcelChapter.Visible = True
                dvCreditCardchapter.DataSource = ObjDt
                dvCreditCardchapter.DataBind()
            Else
                dvCreditCardchapter.DataSource = Nothing
                dvCreditCardchapter.DataBind()
                lblNoData.Text = "No data found."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Protected Sub dvCreditCardchapter_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dvCreditCardchapter.RowDataBound
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        e.Row.Cells(2).Visible = False
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim iRegFees As Integer = 0
            Dim iRegMeals As Integer = 0
            Dim iRegDonations As Integer = 0
            Dim iSubTotal As Integer = 0
            Dim iHomePageDonations As Integer = 0
            Dim iTotal As Integer = 0

            iRegFees += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Fees]"))
            iRegMeals += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Meals]"))
            iRegDonations += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Reg Donations]"))
            iSubTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[SubTotal]"))
            iHomePageDonations += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Home Page Donations]"))
            iTotal += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "[Total]"))

            e.Row.Cells(3).Width = 150
            If iRegFees > 0 Then
                e.Row.Cells(4).Text = FormatNumber(iRegFees.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(4).Text = ""
            End If

            If iRegMeals > 0 Then
                e.Row.Cells(5).Text = FormatNumber(iRegMeals.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(5).Text = ""
            End If

            If iRegDonations > 0 Then
                e.Row.Cells(6).Text = FormatNumber(iRegDonations.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(6).Text = ""
            End If

            If iSubTotal > 0 Then
                e.Row.Cells(7).Text = FormatNumber(iSubTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(7).Text = ""
            End If

            If iHomePageDonations > 0 Then
                e.Row.Cells(8).Text = FormatNumber(iHomePageDonations.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(8).Text = ""
            End If

            If iTotal > 0 Then
                e.Row.Cells(9).Text = FormatNumber(iTotal.ToString("d"), 0, , , TriState.True)
                e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            Else
                e.Row.Cells(9).Text = ""
            End If
        End If
    End Sub
    Protected Sub btnChapterExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcelChapter.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CreditCardByChapter.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dvCreditCardchapter.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvCreditCardchapter As Control)
    End Sub

End Class
