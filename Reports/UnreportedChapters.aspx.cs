using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_UnreportedChapters : System.Web.UI.Page
{
    int Year = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
    
    protected void Page_Load(object sender, EventArgs e)
    {
        // connect to the peoducts database
        string connectionString =
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select Contest.ContestID, Contest.ContestCategoryID, Contest.ContestDate, Contest.NSFChapterID, Contest.ProductCode FROM Contest " +
            " INNER JOIN Contestant ON Contest.ContestID = Contestant.ContestCode WHERE Contest.Contest_Year='" + Year + "' AND Contestant.EventID = 2 AND Contestant.PaymentReference IS NOT NULL " +
            " AND (Contestant.score1 is null or Contestant.score2 is null) AND Contest.ContestDate <'" + DateTime.Today.ToShortDateString() +"' ORDER BY Contest.NSFChapterID, Contest.ContestID";
            
        //+ DateTime.Today.ToShortDateString() + 
        SqlDataAdapter daChapters = new SqlDataAdapter(commandString, connection);
        DataSet dsChapters = new DataSet();
        int count = daChapters.Fill(dsChapters);
        if (count < 1)
            lblMessage.Text = "No records to display";
        else
        {
            ProcessData(dsChapters);
            
        }
    }

    private void ProcessData(DataSet ds)
    {
        int tempChap, tempContestCat;
        string tempContestName;
        
        tempChap = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[3]);
        tempContestCat = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[1]);
        tempContestName = ds.Tables[0].Rows[0].ItemArray[4].ToString();
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Chapter", typeof(string));
        dt.Columns.Add("Contests", typeof(string));
        dr = dt.NewRow();
        dr["Chapter"] = GetChapterName(tempChap);
        dr["Contests"] = tempContestName;
        dt.Rows.Add(dr);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (tempChap == Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]))
            {
                if (tempContestCat != Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1]))
                {
                    dr = dt.NewRow();
                    dr["Chapter"] = GetChapterName(tempChap);
                    tempContestCat = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1]);
                    tempContestName = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    dr["Contests"] = tempContestName;
                    dt.Rows.Add(dr);
                }
            }
            else
            {
                tempChap = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]);
                tempContestCat = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1]);
                tempContestName = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                dr = dt.NewRow();
                dr["Chapter"] = GetChapterName(tempChap);
                dr["Contests"] = tempContestName;
                dt.Rows.Add(dr);
            }
        }

        DataView dv = new DataView(dt);
        dv.Sort = "Chapter ASC";
        gvList.DataSource = dv;
        gvList.DataBind();

    }

    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select City, State from Chapter where ChapterID = " + idNumber;
        SqlDataAdapter daChapters = new SqlDataAdapter(commandString, connection);
        DataSet dsChapters = new DataSet();
        daChapters.Fill(dsChapters);
        
        if (dsChapters.Tables[0].Rows.Count > 0)
            retValue = dsChapters.Tables[0].Rows[0].ItemArray[0].ToString() + ", " +
                dsChapters.Tables[0].Rows[0].ItemArray[1].ToString();
        return retValue;

    }
}
