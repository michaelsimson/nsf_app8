﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="xlReportRegByOnLineWorkshop.aspx.cs" Inherits="xlReportRegByOnLineWorkshop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div>
        <script type="text/javascript">

            function showConfirmation() {


                if (confirm("Are you sure want to approve the registartion?")) {

                    document.getElementById("<%=btnConfApprove.ClientID%>").click();
                }
            }

        </script>

    </div>
    <asp:Button ID="btnConfApprove" runat="server" OnClick="btnConfApprove_Click" Style="display: none;" />
    <table id="tblLogin" border="0" cellpadding="0" cellspacing="0" width="100%" runat="server" align="center" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>
        </tr>
        <tr>

            <td>
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Online Workshop Registration Report</strong>
                </div>
            </td>
        </tr>

        <tr>

            <td align="right">
                <asp:Button ID="excel" runat="server" Text="Export To Excel"
                    OnClick="excel_Click" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center">


                <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
                <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold" runat="server" id="tblCoach" visible="true">

                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>

                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="True" runat="server" DataTextField="Name" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Workshop Date</td>
                        <td>
                            <asp:DropDownList ID="ddlworkshopDate" runat="server" AutoPostBack="True" DataValueField="EventDate" Width="110px" OnSelectedIndexChanged="ddlworkshopDate_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Status</td>
                        <td>
                            <asp:DropDownList ID="DDLStatus" runat="server" OnSelectedIndexChanged="DDLStatus_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="Paid">Paid</asp:ListItem>
                                <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                <asp:ListItem Value="Both">Both</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="12" align="center">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="Button1_Click" />
                            <br />
                            <asp:Label ID="lblErr" runat="server" Font-egBold="true" ForeColor="Red" Visible="true"></asp:Label>
                        </td>


                    </tr>
                    <tr align="center" id="trApprove" runat="server" visible="false">
                        <td colspan="12" align="center">
                            <div style="clear: both; margin-bottom: 10px;"></div>
                            <div style="margin: auto; width: 300px;">
                                <div style="float: left;">
                                    <asp:Label ID="lblApprove" Text="Approve" runat="server"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 20px;">
                                    <asp:DropDownList ID="ddlApproved" runat="server">
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div style="float: left; margin-left: 20px;">
                                    <asp:Button ID="BtnApprove" runat="server" Text="Approve" OnClick="BtnApprove_Click" />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr align="center">

                        <td colspan="12">
                            <div align="left" id="dvCount" runat="server" visible="false">Total Registration Count: <span id="spanTotRegCount" runat="server"></span></div>
                            <div style="clear: both;"></div>
                            <center>
                                <asp:Label ID="lblNorecord" runat="server" Text="No record exists" ForeColor="Red" Visible="false"></asp:Label>
                            </center>
                            <div style="clear: both;"></div>
                            <div style="width: 1200px; height: 400px; overflow: scroll;">

                                <asp:GridView ID="gvOnlineWorkshopRegReport" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" OnRowCommand="gvOnlineWorkshopRegReport_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Action
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="display: none;">
                                                    <asp:Label ID="lblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegID") %>'></asp:Label>
                                                </div>
                                                <asp:Button ID="btnApproved" runat="server" Text="Approve" CommandName="Approve" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Ser#
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSRNO" runat="server"
                                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RegistrationDate" HeaderText="RegistrationDate" />
                                        <asp:BoundField DataField="WkshopDate" HeaderText="WkshopDate" />
                                        <asp:BoundField DataField="Wkshop" HeaderText="Wkshop" />
                                        <asp:BoundField DataField="ChildLastName" HeaderText="ChildLastName" />
                                        <asp:BoundField DataField="ChildFirstName" HeaderText="ChildFirstName" />
                                        <asp:BoundField DataField="Grade" HeaderText="Grade" />
                                        <asp:BoundField DataField="DOB" Visible="false" HeaderText="DOB" />
                                        <asp:BoundField DataField="GenderChild" Visible="false" HeaderText="GenderChild" />
                                        <asp:BoundField DataField="CPhoneFather" HeaderText="CPhoneFather" />
                                        <asp:BoundField DataField="HomePhone" HeaderText="HomePhone" />
                                        <asp:BoundField DataField="LastNameFather" HeaderText="LastNameFather" />
                                        <asp:BoundField DataField="FirstNameFather" HeaderText="FirstNameFather" />
                                        <asp:BoundField DataField="EmailFather" HeaderText="EmailFather" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="SchoolName" Visible="false" HeaderText="SchoolName" />
                                        <asp:BoundField DataField="ProductID" Visible="false" HeaderText="ProductID" />
                                        <asp:BoundField DataField="LastNameMother" HeaderText="LastNameMother" />
                                        <asp:BoundField DataField="FirstNameMother" HeaderText="FirstNameMother" />
                                        <asp:BoundField DataField="EmailMother" HeaderText="EmailMother" />
                                        <asp:BoundField DataField="CPhoneMother" HeaderText="CPhoneMother" />
                                        <asp:BoundField DataField="WkshopName" HeaderText="WkshopName" />
                                        <asp:BoundField DataField="RegID" HeaderText="RegID" />
                                        <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                                        <asp:BoundField DataField="MemberID" HeaderText="MemberID" />
                                        <asp:BoundField DataField="Approved" HeaderText="Approved" />

                                         <asp:BoundField DataField="JoinURL" HeaderText="JoinURL" />
                                        <%-- <asp:BoundField DataField="Approved" HeaderText="Approved" />--%>
                                    </Columns>

                                </asp:GridView>
                            </div>
                        </td>

                    </tr>




                </table>
            </td>

        </tr>



    </table>
    <asp:HiddenField ID="hdnRegId" runat="server" Value="" />
</asp:Content>
