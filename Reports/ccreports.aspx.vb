Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI
Partial Class Reports_ccreports
    Inherits System.Web.UI.Page
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                    loadyear()
                    loadEvent()
                    loadrevenuetype()
                    Session("StrSQL") = Nothing
                    'loadgrid(dgReports, 2009)
                Else
                    Response.Redirect("..\VolunteerFunctions.aspx")
                End If
            Else
                Response.Redirect("..\Login.aspx")
            End If
        End If
    End Sub

    Private Sub loadrevenuetype()
        lstRevenueType.Items.Insert(0, "Registration Fees")
        lstRevenueType.Items.Insert(1, "Donations")
        lstRevenueType.Items.Insert(2, "Meals")
    End Sub
    Private Sub loadEvent()
        lstEvent.Items.Clear()
        Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from Event where EventID in (1,2,3,5,11,13,18)")
        i = 1
        While read1.Read()
            Dim litem As New ListItem
            litem.Text = read1("Name")
            litem.Value = read1("EventID")
            lstEvent.Items.Add(litem)
            i = i + 1
        End While
        lstEvent.Items.Insert(0, "All Events")
        lstEvent.SelectedIndex = 0
        read1.Close()
    End Sub


    Private Sub loadyear()
        ddlYear1.Items.Clear()
        ddlYear2.Items.Clear()
        '** ferdine Silvaa
        'Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select  Max(year(PP_Date)) as Maxyear,Min(year(PP_Date)) as MinYear from ChargeRec")
        Dim MaxYear, MinYear As Integer
        'While read1.Read()
        'MaxYear = read1("MaxYear")
        'MinYear = read1("MinYear")
        'End While
        'read1.Close()
        MaxYear = Now.Year
        MinYear = Now.Year - 5
        Dim year As Integer = MinYear
        i = 0
        While year <= MaxYear
            ddlYear1.Items.Insert(i, year)
            ddlYear2.Items.Insert(i, year)
            i = i + 1
            year = year + 1
        End While
        ddlYear1.Items(5).Selected = True
        ddlYear2.Items(5).Selected = True
    End Sub
   
    Protected Sub dgReports_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        'Monthly         
        loadgrid(dgReports, Session("StrSQL"))
        dgReports.CurrentPageIndex = e.NewPageIndex
        dgReports.DataBind()
    End Sub

    Protected Sub dgYearly_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        'Annual
        loadAnnualgrid(dgYearly, Session("StrSQL"), 1)
        dgYearly.CurrentPageIndex = e.NewPageIndex
        dgYearly.DataBind()
    End Sub

    Protected Sub dgfiscal_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        'Monthly Fiscal
        loadgrid(DGfiscal, Session("StrSQL"))
        DGfiscal.CurrentPageIndex = e.NewPageIndex
        DGfiscal.DataBind()
    End Sub

    Protected Sub dgdaily_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        'Daily
        loadDailygrid(DGDaily, Session("StrSQL"))
        DGDaily.CurrentPageIndex = e.NewPageIndex
        DGDaily.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        cleargrid()
        If DdlFreq.SelectedValue = 1 Then
            If ddlYear1.SelectedValue = ddlYear2.SelectedValue Then
                If Integer.Parse(DdlMonth1.SelectedValue) > Integer.Parse(ddlMonth2.SelectedValue) Then
                    ' Response.Write(DdlMonth1.SelectedValue & " " & ddlMonth2.SelectedValue)
                    lblErr.Text = "Please select month greater than the beginning month"
                    Exit Sub
                End If
            ElseIf ddlYear1.SelectedValue > ddlYear2.SelectedValue Then
                lblErr.Text = "Please select year greater than the beginning year"
                Exit Sub
            ElseIf ddlYear1.SelectedValue < ddlYear2.SelectedValue Then
                If Integer.Parse(DdlMonth1.SelectedValue) < Integer.Parse(ddlMonth2.SelectedValue) Or Integer.Parse(DdlMonth1.SelectedValue) = Integer.Parse(ddlMonth2.SelectedValue) Then
                    lblErr.Text = "Report can be processed for 12 months only"
                    Exit Sub
                End If
            ElseIf (ddlYear2.SelectedValue - ddlYear1.SelectedValue) > 1 Then
                lblErr.Text = "Report can be processed for 12 months only"
                Exit Sub
            End If
            'Monthly
            Dim Eventstr As String = ""
            Dim between As String = " between '" & DdlMonth1.SelectedValue & "/1/" & ddlYear1.SelectedValue & "' and '" & ddlMonth2.SelectedValue & "/" & System.DateTime.DaysInMonth(ddlYear2.SelectedValue, ddlMonth2.SelectedValue) & "/" & ddlYear2.SelectedValue & "' "
            If lstEvent.Items(0).Selected = True Then
                Eventstr = ""
            Else
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And C.Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If
            Dim StrSQL, StrSQlAll, StrSQlAllEnd, StrSQLReg, StrSQLDon, StrSQLMeals, StrSQLAdd As String
            '** Have to use this while we have combinations
            Dim StrSQLOpen As String = "" ' "("
            Dim StrSQLUnion As String = "" ' " Union All"
            Dim StrSQLOpen1 As String = "" ' "("
            Dim StrSQLUnion1 As String = "" ' " Union All"
            Dim StrSQLOpen2 As String = "" ' "("
            Dim StrSQLUnion2 As String = "" ' " Union All"
            Dim RegFee As Boolean = False, Donation As Boolean = False, Meals As Boolean = False
            If lstRevenueType.Items(0).Selected = True Then
                RegFee = True
            End If
            If lstRevenueType.Items(1).Selected = True Then
                Donation = True
            End If
            If lstRevenueType.Items(2).Selected = True And (lstEvent.Items(0).Selected = True Or lstEvent.Items(1).Selected = True) Then
                Meals = True
            End If

            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
                StrSQLOpen1 = "("
                StrSQLUnion1 = " Union All"
                StrSQLOpen2 = "("
                StrSQLUnion2 = " Union All"

            ElseIf RegFee = True And Donation = True And Meals = False Then
                'for regfee & donation 
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge
                StrSQLOpen1 = "("
                StrSQLUnion1 = " Union All"
            End If

            StrSQlAll = "Select t2.ChapterID,t2.State,t2.ChapterCode,Sum(t1.Jan) as Jan,Sum(t1.Feb) as Feb,Sum(t1.Mar) as Mar,Sum(t1.Apr) as Apr,Sum(t1.May) as May,Sum(t1.Jun) as Jun,Sum(t1.July) as July,Sum(t1.Aug) as Aug,Sum(t1.Sept) as Sept,Sum(t1.Oct) as Oct,Sum(t1.Nov) as Nov,Sum(t1.Dec) as Dec, Sum(t1.Jan)+Sum(t1.Feb)+Sum(t1.Mar)+Sum(t1.Apr)+Sum(t1.May)+Sum(t1.Jun)+Sum(t1.July)+Sum(t1.Aug)+Sum(t1.Sept)+Sum(t1.Oct) +Sum(t1.Nov)+Sum(t1.Dec) as Total from( "

            'For Reg fee
            StrSQLReg = "(select C.ChapterID,Sum(C.fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 1  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, Sum(C.fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 2  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, Sum(C.fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 3  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(C.fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 4  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(C.fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 5  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(C.fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 6  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(C.fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 7  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(C.fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 8  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(C.fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 9  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(C.fee) as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 10 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(C.fee) as Nov, 0 as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 11 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(C.fee) as Dec from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 12 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"

            'For Workshop
            StrSQLReg = StrSQLReg & "(select C.ChapterID,Sum(C.fee) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 1  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, Sum(C.fee) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 2  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, Sum(C.fee) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 3  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(C.fee) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 4  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(C.fee) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 5  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(C.fee) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 6  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(C.fee) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 7  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(C.fee) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 8  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(C.fee) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 9  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(C.fee) as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 10 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(C.fee) as Nov, 0 as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 11 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & StrSQLOpen & " select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(C.fee) as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 12 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)" & StrSQLUnion
            'StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(C.fee) as Dec from ChargeRec CR,Registration C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 12 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"

            'Added to add the refunded amounts in home chapter
            StrSQLAdd = "(select 1 as ChapterID,Sum(MS_Amount) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 1  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, Sum(MS_Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 2  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(MS_Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 3  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(MS_Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 4  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(MS_Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 5  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(MS_Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 6  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(MS_Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 7  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(MS_Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 8  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(MS_Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 9  and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(MS_Amount) as Oct, 0 as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 10 and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(MS_Amount) as Nov, 0 as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 11 and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"
            'StrSQLAdd = StrSQLAdd & StrSQLOpen & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(MS_Amount) as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 12 and MS_Amount<0  Group by  month(MS_TransDate))" & StrSQLUnion
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(MS_Amount) as Dec from ChargeRec where MS_TransDate " & between & "   and  month(MS_TransDate) = 12 and MS_Amount<0  Group by  month(MS_TransDate))UNION ALL"

            'Amount Not in Databasae
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,Sum(MS_Amount) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 1  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, Sum(MS_Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 2  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(MS_Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 3  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(MS_Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 4  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(MS_Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 5  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(MS_Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 6  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(MS_Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 7  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(MS_Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 8  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(MS_Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 9  and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(MS_Amount) as Oct, 0 as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 10 and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(MS_Amount) as Nov, 0 as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 11 and MS_Amount>0  Group by  month(MS_TransDate))UNION ALL"
            StrSQLAdd = StrSQLAdd & StrSQLOpen2 & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(MS_Amount) as Dec from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between & "   and  month(MS_TransDate) = 12 and MS_Amount>0  Group by  month(MS_TransDate))" & StrSQLUnion2


            'MealCharge
            StrSQLMeals = "(select 1 as ChapterID,Sum(C.Amount) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 1  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, Sum(C.Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 2  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, Sum(C.Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 3  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(C.Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 4  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(C.Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 5  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(C.Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 6  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(C.Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 7  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(C.Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 8  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(C.Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 9  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(C.Amount) as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 10 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(C.Amount) as Nov, 0 as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 11 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & StrSQLOpen1 & " select 1 as ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(C.Amount) as Dec from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between & "  and  month(CR.MS_TransDate) = 12 and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))" & StrSQLUnion1

            'donation
            StrSQLDon = "(select C.ChapterID,Sum(C.Amount) as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & "   and  month(CR.MS_TransDate) = 1  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, Sum(C.Amount) as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 2  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, Sum(C.Amount) as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 3  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, Sum(C.Amount) as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 4  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, Sum(C.Amount) as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 5  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, Sum(C.Amount) as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 6  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, Sum(C.Amount) as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 7  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, Sum(C.Amount) as Aug, 0 as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 8  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, Sum(C.Amount) as Sept, 0 as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 9  and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, Sum(C.Amount) as Oct, 0 as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 10 and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, Sum(C.Amount) as Nov, 0 as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 11 and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & " select C.ChapterID,0 as Jan, 0 as Feb, 0 as Mar, 0 as Apr, 0 as May, 0 as Jun, 0 as July, 0 as Aug, 0 as Sept, 0 as Oct, 0 as Nov, Sum(C.Amount) as Dec from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between & " " & Eventstr & " and  month(CR.MS_TransDate) = 12 and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)"

            StrSQlAllEnd = " t1,Chapter t2 where t1.Chapterid=t2.Chapterid Group by t2.ChapterID,t2.State,t2.ChapterCode order by 2 ,3"

            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQL = StrSQlAll & StrSQLReg & StrSQLAdd & StrSQLMeals & StrSQLDon & StrSQlAllEnd

            ElseIf RegFee = True And Donation = True And Meals = False Then
                'for regfee & donation 
                StrSQL = StrSQlAll & StrSQLReg & StrSQLDon & StrSQlAllEnd

            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge
                StrSQL = StrSQlAll & StrSQLReg & StrSQLMeals & StrSQlAllEnd
            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge
                StrSQL = StrSQlAll & StrSQLMeals & StrSQLDon & StrSQlAllEnd
            ElseIf RegFee = True And Meals = False And Donation = False Then
                'Reg Fee alone
                StrSQL = StrSQlAll & StrSQLReg & StrSQlAllEnd
            ElseIf RegFee = False And Meals = True And Donation = False Then
                'Meals Alone
                StrSQL = StrSQlAll & StrSQLMeals & StrSQlAllEnd
            ElseIf RegFee = False And Meals = False And Donation = True Then
                'Donation alone
                StrSQL = StrSQlAll & StrSQLDon & StrSQlAllEnd
            Else
                lblErr.Text = "Please Select Revenue Type"
                Exit Sub
            End If
            Session("StrSQL") = StrSQL
            If Integer.Parse(DdlMonth1.SelectedValue) >= 5 Then
                loadgrid(DGfiscal, StrSQL)
            Else
                loadgrid(dgReports, StrSQL)
            End If

        ElseIf DdlFreq.SelectedValue = 2 Then
            'Annual
            Dim Eventstr As String = ""
            Dim year As Integer = Now.Year
            Dim between1, between2, between3, between4, between5 As String
            If ddlyeartype.SelectedValue = 0 Then
                between1 = " between '5/1/" & year & "' and '4/30/" & year + 1 & "' "
                between2 = " between '5/1/" & year - 1 & "' and '4/30/" & year & "' "
                between3 = " between '5/1/" & year - 2 & "' and '4/30/" & year - 1 & "' "
                between4 = " between '5/1/" & year - 3 & "' and '4/30/" & year - 2 & "' "
                between5 = " between '5/1/" & year - 4 & "' and '4/30/" & year - 3 & "' "
            Else
                between1 = " between '1/1/" & year & "' and '12/31/" & year & "' "
                between2 = " between '1/1/" & year - 1 & "' and '12/31/" & year - 1 & "' "
                between3 = " between '1/1/" & year - 2 & "' and '12/31/" & year - 2 & "' "
                between4 = " between '1/1/" & year - 3 & "' and '12/31/" & year - 3 & "' "
                between5 = " between '1/1/" & year - 4 & "' and '12/31/" & year - 4 & "' "
            End If
            'between5 = " between '5/1/" & year - 5 & "' and '4/30/" & year - 4 & "' "

            If lstEvent.Items(0).Selected = True Then
                Eventstr = ""
            Else
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And C.Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If


            Dim StrSQL, StrSQlAll, StrSQlAllEnd, StrSQLReg, StrSQLDon, StrSQLMeals, StrSQLAdd As String
            '** Have to use this while we have combinations
            Dim StrSQLOpen As String = "" ' "("
            Dim StrSQLUnion As String = "" ' " Union All"
            Dim StrSQLOpen1 As String = "" ' "("
            Dim StrSQLUnion1 As String = "" ' " Union All"
            Dim StrSQLOpen2 As String = "" ' "("
            Dim StrSQLUnion2 As String = "" ' " Union All"
            Dim RegFee As Boolean = False, Donation As Boolean = False, Meals As Boolean = False
            If lstRevenueType.Items(0).Selected = True Then
                RegFee = True
            End If
            If lstRevenueType.Items(1).Selected = True Then
                Donation = True
            End If
            If lstRevenueType.Items(2).Selected = True And (lstEvent.Items(0).Selected = True Or lstEvent.Items(1).Selected = True) Then
                Meals = True
            End If

            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
                StrSQLOpen1 = "("
                StrSQLUnion1 = " Union All"
                StrSQLOpen2 = "("
                StrSQLUnion2 = " Union All"
            ElseIf RegFee = True And Donation = True And Meals = False Then
                'for regfee & donation 
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge
                StrSQLOpen = "("
                StrSQLUnion = " Union All"
            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge
                StrSQLOpen1 = "("
                StrSQLUnion1 = " Union All"
            End If

            'StrSQlAll = "Select t2.ChapterID,t2.State,t2.ChapterCode,CONVERT(decimal(10, 2), Sum(t1.year1)) as '" & year.ToString() & "',CONVERT(decimal(10, 2), Sum(t1.year2))  as '" & (year - 1).ToString() & "',CONVERT(decimal(10, 2), Sum(t1.year3)) as '" & (year - 2).ToString() & "',CONVERT(decimal(10, 2), Sum(t1.year4)) as  '" & (year - 3).ToString() & "',CONVERT(decimal(10, 2), Sum(t1.year5)) as  '" & (year - 4).ToString() & "', CONVERT(decimal(10, 2), Sum(t1.year1)+Sum(t1.year2)+Sum(t1.year3)+Sum(t1.year4)+Sum(t1.year5)) as Total from( "
            StrSQlAll = "Select t2.ChapterID,t2.State,t2.ChapterCode,Sum(t1.year1) as year1,Sum(t1.year2) as year2,Sum(t1.year3) as year3,Sum(t1.year4) as year4,Sum(t1.year5) as  year5, Sum(t1.year1)+Sum(t1.year2)+Sum(t1.year3)+Sum(t1.year4)+Sum(t1.year5) as Total from( "
            'For Reg fee
            StrSQLReg = "(select C.ChapterID,Sum(C.fee) as year1, 0 as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between1 & " " & Eventstr & " and  CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, Sum(C.fee) as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between2 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, 0 as year2, Sum(C.fee) as year3, 0 as year4, 0 as year5 from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between3 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, 0 as year2, 0 as year3, Sum(C.fee) as year4, 0 as year5 from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between4 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, 0 as year2, 0 as year3, 0 as year4, Sum(C.fee) as year5 from ChargeRec CR,Contestant   C where CR.MS_TransDate " & between5 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,Sum(C.fee) as year1, 0 as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between1 & " " & Eventstr & " and  CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, Sum(C.fee) as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between2 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, 0 as year2, Sum(C.fee) as year3, 0 as year4, 0 as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between3 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & "(select C.ChapterID,0 as year1, 0 as year2, 0 as year3, Sum(C.fee) as year4, 0 as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between4 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)Union All"
            StrSQLReg = StrSQLReg & StrSQLOpen & " select C.ChapterID,0 as year1, 0 as year2, 0 as year3, 0 as year4, Sum(C.fee) as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between5 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID)" & StrSQLUnion
            'StrSQLReg = StrSQLReg & StrSQLOpen & " select C.ChapterID,0 as year1, 0 as year2, 0 as year3, 0 as year4, Sum(C.fee) as year5 from ChargeRec CR,Registration C where CR.MS_TransDate " & between5 & " " & Eventstr & " and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  C.ChapterID) " & StrSQLUnion

            'Added to add the refunded amounts in home chapter
            StrSQLAdd = "(select 1 as ChapterID,MS_Amount as Year1, 0 as Year2, 0 as Year3, 0 as Year4, 0 as Year5 from ChargeRec where MS_TransDate " & between1 & "   and MS_Amount<0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1,MS_Amount as Year2, 0 as Year3, 0 as Year4, 0 as Year5 from ChargeRec where MS_TransDate " & between2 & "   and MS_Amount<0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1, 0 as Year2,MS_Amount as Year3, 0 as Year4, 0 as Year5 from ChargeRec where MS_TransDate " & between3 & "   and MS_Amount<0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1, 0 as Year2, 0 as Year3,MS_Amount as Year4, 0 as Year5 from ChargeRec where MS_TransDate " & between4 & "   and MS_Amount<0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1, 0 as Year2, 0 as Year3, 0 as Year4,MS_Amount as Year5 from ChargeRec where MS_TransDate " & between5 & "   and MS_Amount<0)UNION ALL"

            'Amount Not in Databasae
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,MS_Amount as Year1, 0 as Year2, 0 as Year3, 0 as Year4, 0 as Year5 from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between1 & " and MS_Amount>0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1,MS_Amount as Year2, 0 as Year3, 0 as Year4, 0 as Year5 from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between2 & " and MS_Amount>0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1, 0 as Year2,MS_Amount as Year3, 0 as Year4, 0 as Year5 from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between3 & " and MS_Amount>0)UNION ALL"
            StrSQLAdd = StrSQLAdd & "(select 1 as ChapterID,0 as Year1, 0 as Year2, 0 as Year3,MS_Amount as Year4, 0 as Year5 from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between4 & " and MS_Amount>0)UNION ALL"
            StrSQLAdd = StrSQLAdd & StrSQLOpen2 & " select 1 as ChapterID,0 as Year1, 0 as Year2, 0 as Year3, 0 as Year4,MS_Amount as Year5 from ChargeRec where PP_orderNumber not in (select paymentreference from registration where eventid=3 and paymentreference is not null)  AND PP_orderNumber not in (select paymentreference from contestant where paymentreference is not null) and PP_orderNumber not in (select Transaction_Number from DonationsInfo where Transaction_Number is not null) AND PP_orderNumber not in (select paymentreference from mealcharge where paymentreference is not null) AND  MS_TransDate " & between5 & " and MS_Amount>0)" & StrSQLUnion2

            'MealCharge
            StrSQLMeals = "(select 1 as ChapterID,Sum(C.Amount) as year1, 0 as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between1 & "  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as year1, Sum(C.Amount) as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between2 & "  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as year1, 0 as year2, Sum(C.Amount) as year3, 0 as year4, 0 as year5 from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between3 & "  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & "(select 1 as ChapterID,0 as year1, 0 as year2, 0 as year3, Sum(C.Amount) as year4, 0 as year5 from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between4 & "  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))Union All"
            StrSQLMeals = StrSQLMeals & StrSQLOpen1 & " select 1 as ChapterID,0 as year1, 0 as year2, 0 as year3, 0 as year4, Sum(C.Amount) as year5 from ChargeRec CR,MealCharge C where CR.MS_TransDate " & between5 & "  and CR.PP_OrderNumber = C.PaymentReference and CR.MS_Amount>0 Group by  month(CR.MS_TransDate))" & StrSQLUnion1

            'donation
            StrSQLDon = "(select C.ChapterID,Sum(C.Amount) as year1, 0 as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between1 & " " & Eventstr & " and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as year1, Sum(C.Amount) as year2, 0 as year3, 0 as year4, 0 as year5 from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between2 & " " & Eventstr & " and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as year1, 0 as year2, Sum(C.Amount) as year3, 0 as year4, 0 as year5 from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between3 & " " & Eventstr & " and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & "(select C.ChapterID,0 as year1, 0 as year2, 0 as year3, Sum(C.Amount) as year4, 0 as year5 from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between4 & " " & Eventstr & " and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)Union All"
            StrSQLDon = StrSQLDon & " select C.ChapterID,0 as year1, 0 as year2, 0 as year3, 0 as year4, Sum(C.Amount) as year5 from ChargeRec CR,DonationsInfo C where CR.MS_TransDate " & between5 & " " & Eventstr & " and CR.PP_OrderNumber = C.TRANSACTION_NUMBER and CR.MS_Amount>0 and C.Amount>0 Group by C.ChapterID)"
            StrSQlAllEnd = " t1,Chapter t2 where t1.Chapterid=t2.Chapterid Group by t2.ChapterID,t2.State,t2.ChapterCode order by 2 ,3"

            If RegFee = True And Donation = True And Meals = True Then
                'for reg fee, donation and mealcharge
                StrSQL = StrSQlAll & StrSQLReg & StrSQLAdd & StrSQLMeals & StrSQLDon & StrSQlAllEnd

            ElseIf RegFee = True And Donation = True And Meals = False Then
                'for regfee & donation 
                StrSQL = StrSQlAll & StrSQLReg & StrSQLDon & StrSQlAllEnd

            ElseIf RegFee = True And Meals = True And Donation = False Then
                'regfee and mealcharge
                StrSQL = StrSQlAll & StrSQLReg & StrSQLMeals & StrSQlAllEnd
            ElseIf Donation = True And Meals = True And RegFee = False Then
                ' Donation and MealCharge
                StrSQL = StrSQlAll & StrSQLMeals & StrSQLDon & StrSQlAllEnd
            ElseIf RegFee = True And Meals = False And Donation = False Then
                'Reg Fee alone
                StrSQL = StrSQlAll & StrSQLReg & StrSQlAllEnd
            ElseIf RegFee = False And Meals = True And Donation = False Then
                'Meals Alone
                StrSQL = StrSQlAll & StrSQLMeals & StrSQlAllEnd
            ElseIf RegFee = False And Meals = False And Donation = True Then
                'Donation alone
                StrSQL = StrSQlAll & StrSQLDon & StrSQlAllEnd
            Else
                lblErr.Text = "Please Select Revenue Type"
                Exit Sub
            End If
            Session("StrSQL") = StrSQL
            loadAnnualgrid(dgYearly, StrSQL, 1)
        ElseIf DdlFreq.SelectedValue = 0 Then
            If IsDate(txtBeginDate.Text) = True And IsDate(txtEndDate.Text) = True Then
                Dim strSQL As String = "SELECT  ChargeRecID, PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, MS_TransDate, MS_Amount, MS_CN1, MS_CN3,  PP_MS_Match, Brand, Reference, Comments, NFG_PaymentDate FROM ChargeRec where MS_TransDate Between '" & txtBeginDate.Text & "' and '" & txtEndDate.Text & "'"
                Session("StrSQL") = strSQL
                loadDailygrid(DGDaily, strSQL)
            Else
                lblErr.Text = "Please provide right Begin and End Dates"
            End If
        End If
    End Sub

    Protected Sub DdlFreq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cleargrid()
        If DdlFreq.SelectedValue = 1 Then
            trMonthly.Visible = True
            trAnnual.Visible = False
            trdate.Visible = False
        ElseIf DdlFreq.SelectedValue = 2 Then
            trMonthly.Visible = False
            trAnnual.Visible = True
            trdate.Visible = False
        ElseIf DdlFreq.SelectedValue = 0 Then
            trMonthly.Visible = False
            trAnnual.Visible = False
            trdate.Visible = True
        End If
    End Sub

    Protected Sub loadDailygrid(ByVal dg As DataGrid, ByVal StrSQL As String)
        ' lblErr.Text = StrSQL.ToString()
        Try
            Dim dsCCTrans As New DataSet
            Dim tblCCTrans() As String = {"ChargeRec"}
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsCCTrans, tblCCTrans)
            dg.CurrentPageIndex = 0
            dg.DataSource = dsCCTrans
            dg.DataBind()
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try

    End Sub

    Protected Sub loadgrid(ByVal dg As DataGrid, ByVal StrSQL As String)
        ' lblErr.Text = StrSQL.ToString()
        Try
            Dim dsCCTrans As New DataSet
            Dim tblCCTrans() As String = {"ChargeRec"}
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsCCTrans, tblCCTrans)
            Dim dr As DataRow
            dr = dsCCTrans.Tables("ChargeRec").NewRow
            dr(0) = 0
            dr(1) = ""
            dr(2) = "Total  "
            Dim j As Integer
            For i = 3 To dsCCTrans.Tables("ChargeRec").Columns.Count - 1
                dr(i) = 0.0
                For j = 0 To dsCCTrans.Tables("ChargeRec").Rows.Count - 1
                    dr(i) = dr(i) + dsCCTrans.Tables("ChargeRec").Rows(j)(i)
                Next
            Next
            dsCCTrans.Tables("ChargeRec").Rows.Add(dr)
            dsCCTrans.Tables("ChargeRec").Columns.Remove("State")
            dsCCTrans.Tables("ChargeRec").Columns.Remove("ChapterID")
            dg.CurrentPageIndex = 0
            dg.DataSource = dsCCTrans
            dg.DataBind()
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try

    End Sub

    Protected Sub loadAnnualgrid(ByVal dg As DataGrid, ByVal StrSQL As String, ByVal flag As Integer)
        Try
            'lblErr.Text = StrSQL.ToString()
            Dim year As Integer = Now.Year
            Dim dsCCTrans As New DataSet
            Dim tblCCTrans() As String = {"ChargeRec"}
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsCCTrans, tblCCTrans)
            Dim dr As DataRow
            dr = dsCCTrans.Tables("ChargeRec").NewRow
            dr(0) = 0
            dr(1) = ""
            dr(2) = "Total  "
            Dim j As Integer
            For i = 3 To dsCCTrans.Tables("ChargeRec").Columns.Count - 1
                dr(i) = 0.0
                For j = 0 To dsCCTrans.Tables("ChargeRec").Rows.Count - 1
                    dr(i) = dr(i) + dsCCTrans.Tables("ChargeRec").Rows(j)(i)
                Next
            Next
            dsCCTrans.Tables("ChargeRec").Rows.Add(dr)
            dsCCTrans.Tables("ChargeRec").Columns.Remove("State")
            dsCCTrans.Tables("ChargeRec").Columns.Remove("ChapterID")
            If flag = 0 Then
                dsCCTrans.Tables("ChargeRec").Columns(1).ColumnName = year.ToString()
                dsCCTrans.Tables("ChargeRec").Columns(2).ColumnName = (year - 1).ToString()
                dsCCTrans.Tables("ChargeRec").Columns(3).ColumnName = (year - 2).ToString()
                dsCCTrans.Tables("ChargeRec").Columns(4).ColumnName = (year - 3).ToString()
                dsCCTrans.Tables("ChargeRec").Columns(5).ColumnName = (year - 4).ToString()
                dg.DataSource = dsCCTrans
            ElseIf flag = 1 Then
                dg.CurrentPageIndex = 0
                dg.DataSource = dsCCTrans
                dg.Columns(1).HeaderText = year.ToString()
                dg.Columns(2).HeaderText = (year - 1).ToString()
                dg.Columns(3).HeaderText = (year - 2).ToString()
                dg.Columns(4).HeaderText = (year - 3).ToString()
                dg.Columns(5).HeaderText = (year - 4).ToString()
            End If
            dg.DataBind()
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CreditCardReports.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        'Dim dgexport As New DataGrid
        'If DdlFreq.SelectedValue = 1 And Session("StrSQL") <> Nothing Then
        '    'Monthly
        '    loadgrid(dgexport, Session("StrSQL"))
        'ElseIf DdlFreq.SelectedValue = 2 And Session("StrSQL") <> Nothing Then
        '    'Annual
        '    loadAnnualgrid(dgexport, Session("StrSQL"), 0)
        'ElseIf DdlFreq.SelectedValue = 0 And Session("StrSQL") <> Nothing Then
        '    'Annual
        '    loadDailygrid(dgexport, Session("StrSQL"))
        'Else
        '    lblErr.Text = "Please Select Options to Export"
        '    Exit Sub
        'End If
        If DGfiscal.Items.Count > 0 Then
            DGfiscal.AllowPaging = False
            loadgrid(DGfiscal, Session("StrSQL"))
            DGfiscal.RenderControl(htmlWrite)
            'DGfiscal.AllowPaging = True
            'DGfiscal.DataBind()
        ElseIf DGDaily.Items.Count > 0 Then
            DGDaily.AllowPaging = False
            loadDailygrid(DGDaily, Session("StrSQL"))
            DGDaily.RenderControl(htmlWrite)
        ElseIf dgYearly.Items.Count > 0 Then
            dgYearly.AllowPaging = False
            loadAnnualgrid(dgYearly, Session("StrSQL"), 1)
            dgYearly.RenderControl(htmlWrite)
        Else
            dgReports.AllowPaging = False
            loadgrid(dgReports, Session("StrSQL"))
            dgReports.RenderControl(htmlWrite)
        End If
        'dgexport.RenderControl(htmlWrite)
        If DdlFreq.SelectedValue = 1 And Session("StrSQL") <> Nothing Then
            Response.Write(" Report Generated from " & DdlMonth1.SelectedItem.Text & " " & ddlYear1.SelectedValue & " To " & ddlMonth2.SelectedItem.Text & " " & ddlYear2.SelectedValue)
        End If
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Private Sub cleargrid()
        ' btnExport.Enabled = False
        dgReports.DataSource = Nothing
        dgReports.DataBind()
        dgYearly.DataSource = Nothing
        dgYearly.DataBind()
        DGfiscal.DataSource = Nothing
        DGfiscal.DataBind()
        DGDaily.DataSource = Nothing
        DGDaily.DataBind()
    End Sub

    Protected Sub lbtnVolunteerFunctions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("StrSQL")
        Response.Redirect("~/VolunteerFunctions.aspx")
    End Sub
  
End Class
