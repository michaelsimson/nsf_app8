﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OWkShopRegCounts.aspx.cs" Inherits="Reports_OWkShopRegCounts" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Online Workshop Registartion Counts
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">

        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center"><span id="spnTable2Title" style="font-weight: bold;" runat="server" visible="true">Table 1: Online Workshop Registration Counts</span></div>

        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdWkShopRegCounts" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 700px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
            <Columns>



                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventDate" HeaderText="Workshop Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                <asp:BoundField DataField="PaidReg" HeaderText="Paid Reg"></asp:BoundField>
                <asp:BoundField DataField="PendingReg" HeaderText="Pending"></asp:BoundField>
                <asp:BoundField DataField="RegistrationDeadline" HeaderText="Reg Deadline" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="LateFeeDeadLine" HeaderText="Late Fee Deadline" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>

            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both;"></div>
        <div align="center">
            <span id="spnStatus" runat="server" style="color: red;" visible="false">No record found</span>
        </div>
    </div>
</asp:Content>
