using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;


public partial class Reports_xlReport2 : System.Web.UI.Page
{
    int chapID;
    string Year = "";
    DataTable dt;
    DateTime day1 = DateTime.Parse("1/1/2020");
    DateTime day2 = DateTime.Parse("1/1/2000");
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
            Response.Redirect("../login.aspx?entry=v");
        Year = System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        chapID = Convert.ToInt32(Session["loginChapterID"]);
        lblChapter.Text = GetChapterName(chapID);
        GetData();
    }
    private void GetData()
    {
         // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        /*
        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contest.ContestCategoryID from Contestant "
            +" INNER JOIN Contest ON Contestant.ContestCode = Contest.ContestID "+
            " Where Contestant.PaymentReference IS NOT NULL AND Contestant.ChapterID=" +chapID+" AND Contestant.ContestYear='"+
            Year+"' ORDER BY Contestant.ChildNumber";
        */
        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Child.FIRST_NAME, " +
            "Child.MIDDLE_INITIAL, Child.LAST_NAME, Child.DATE_OF_BIRTH, Child.GRADE, Contestant.BadgeNumber  from Contestant " +
            "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber " +
            " Where Contestant.PaymentReference IS NOT NULL AND Contestant.ChapterID=" + chapID + " AND Contestant.ContestYear=" +
            Year + " ORDER BY Child.LAST_NAME, Child.FIRST_NAME";
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        DataSet dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        if (dsContestants.Tables[0].Rows.Count > 0)
            ProcessData(dsContestants);
        else
        {
            Response.Write("No records found!");
            btnSave.Visible = false;
        }
    }
    void ProcessData(DataSet ds)
    {
       DataSet dsContests = GetContests();
        dt = new DataTable();
        dt.Columns.Add("Ser#", typeof(int));
        dt.Columns.Add("FirstName", typeof(string));
        dt.Columns.Add("LastName", typeof(string));
        dt.Columns.Add("DOB", typeof(string));
        dt.Columns.Add("Gr", typeof(string));
        dt.Columns.Add("SpellingBee", typeof(string));
        dt.Columns.Add("Vocabulary", typeof(string));
        dt.Columns.Add("MathBee", typeof(string));
        dt.Columns.Add("Geography", typeof(string));
        dt.Columns.Add("EssayWriting", typeof(string));
        dt.Columns.Add("PublicSpeaking", typeof(string));
        dt.Columns.Add("Rank", typeof(string));
        dt.Columns.Add("Day1", typeof(string));
        dt.Columns.Add("Day2", typeof(string));
        string value = "";
        string prevChild = ds.Tables[0].Rows[0].ItemArray[0].ToString();
        int contestantNo = 1;
        DataRow dr = dt.NewRow();
        dr["Ser#"] = contestantNo.ToString();
        dr["FirstName"] = ds.Tables[0].Rows[0].ItemArray[2].ToString();
        dr["LastName"] = ds.Tables[0].Rows[0].ItemArray[4].ToString();
        value = ds.Tables[0].Rows[0].ItemArray[5].ToString();
        if (value != "")
            dr["DOB"] = Convert.ToDateTime(value).ToShortDateString();
        dr["Gr"] = ds.Tables[0].Rows[0].ItemArray[6].ToString();

        int i = 0;
        for (; i < ds.Tables[0].Rows.Count; i++)
        {

            if (prevChild == ds.Tables[0].Rows[i].ItemArray[0].ToString())
            {
                AddRow(ds, dsContests, dr, i, contestantNo);
            }
            else
            {
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                AddRow(ds, dsContests, dr, i, contestantNo + 1);
                contestantNo++;
                prevChild = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                dr["Ser#"] = contestantNo.ToString();
                dr["FirstName"] = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                dr["LastName"] = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                dr["DOB"] = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[5]).ToShortDateString();
                dr["Gr"] = ds.Tables[0].Rows[i].ItemArray[6].ToString();
            }
        }
        AddRow(ds, dsContests, dr, i - 1, contestantNo);
        dt.Rows.Add(dr);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
    }
    void AddRow(DataSet ds, DataSet dsContests, DataRow dr, int i, int contestantNo)
    {
        
        int catID = 0;
        string value = "";
        for (int j = 0; j < dsContests.Tables[0].Rows.Count; j++)
            if (Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1].ToString().Trim()) ==
                Convert.ToInt32(dsContests.Tables[0].Rows[j].ItemArray[0].ToString().Trim()))
            {
                value = dsContests.Tables[0].Rows[j].ItemArray[2].ToString().Trim();
                if (value != "")
                    if (Convert.ToDateTime(value) == day1)
                    {
                        dr["Day1"] = 1;
                    }
                    else if (Convert.ToDateTime(value) == day2)
                    {
                        dr["Day2"] = 1;
                    }
                value = dsContests.Tables[0].Rows[j].ItemArray[3].ToString().Trim();
            }
        if (value != "")
        // catID = Convert.ToInt32(value);
        {


            if (value == "SB")
            {
                dr["SpellingBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }
            if (value == "VB")
            {
                dr["Vocabulary"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }
            if (value == "MB")
            {
                dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }
            if (value == "GB")
            {
                dr["Geography"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }
            if (value == "EW")
            {
                dr["EssayWriting"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }
            if (value == "PS")
            {
                dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
            }

        }
        //switch (catID)
        //{
        //    case 1:
        //    case 16:
        //        dr["SpellingBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 2:
        //    case 17:
        //        dr["SpellingBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 3:
        //    case 18:
        //        dr["Vocabulary"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 19:
        //        dr["Vocabulary"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 4:
        //    case 20:
        //        dr["Vocabulary"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 5:
        //    case 21:
        //        dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 6:
        //    case 22:
        //        dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 7:
        //    case 23:
        //        dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 8:
        //    case 24:
        //        dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 9:
        //    case 25:
        //        dr["Geography"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 10:
        //    case 26:
        //        dr["Geography"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 11:
        //    case 27:
        //        dr["EssayWriting"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 12:
        //    case 28:
        //        dr["EssayWriting"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 13:
        //    case 29:
        //        dr["EssayWriting"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 14:
        //    case 30:
        //        dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 31:
        //        dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;
        //    case 15:
        //    case 32:
        //        dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
        //        break;

        //    default:
        //        dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim(); // "?" + String.Format("{0:000}", (i + 1));
        //        break;
        // }

    }
    DataSet GetContests()
    {
        DataSet ds = new DataSet();
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        string commandString = "Select ContestID, ContestCategoryID, ContestDate,ProductGroupCode FROM Contest" +
            " Where Contest_Year=" + Year + " AND NSFChapterID=" + chapID;
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        daContests.Fill(ds);
        string value = "";
        value = ds.Tables[0].Rows[0].ItemArray[2].ToString().Trim();
        if (value != "")
            day1 = day2 = Convert.ToDateTime(value);
        for (int q = 1; q < ds.Tables[0].Rows.Count; q++)
        {
            value = ds.Tables[0].Rows[q].ItemArray[2].ToString().Trim();

            if (value != "")
            {
                if (Convert.ToDateTime(value) <= day1)
                    day1 = Convert.ToDateTime(value);
                if (Convert.ToDateTime(value) >= day2)
                    day2 = Convert.ToDateTime(value);
            }
        }
        return ds;
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=NSF-"+
            Year.Substring(2,2)+"-Reg-" + GetChapterName(chapID) + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
             System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
         
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }

}

