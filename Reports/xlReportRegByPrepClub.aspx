<%@ Page Language="C#" AutoEventWireup="true" CodeFile="xlReportRegByPrepClub.aspx.cs" Inherits="Reports_xlReportRegByPrepClub" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Report Registered By PrebClubs</title>
    </head>
    <body>
      <form id="form1" runat="server">
          <center>  <h2>PREPCLUB REGISTRATION</h2></center>
                 <asp:Button id="btnSave" runat="server" Text="Save File" onclick="btnSave_Click"></asp:Button>&nbsp;
		         <asp:Button id="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" ></asp:Button>
         <center >
             <table border="0" cellpadding="2" cellspacing = "0">
                     <tr>
                         <td align = "Left">Year</td>
                         <td></td>
                         <td><asp:DropDownList ID="ddlyear" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged"></asp:DropDownList></td>
                     </tr>
                     <tr>
                         <td>Contest</td>
                         <td></td>
                         <td><asp:ListBox ID="ddlprod" runat="server" Width="150px" Height="100px" SelectionMode="Multiple">
                                    <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                             </asp:ListBox>
                         </td>
                     </tr>
                     <tr id="ReportSel" runat = "server" visible = "false" >
                         <td align = "Left">Report for</td>
                         <td></td>
                         <td><asp:DropDownList ID="ddlReport" runat="server" Width="150px">
              
                                 <asp:ListItem>Past</asp:ListItem>
                                 <asp:ListItem>Future</asp:ListItem>     
                                 <asp:ListItem>ALL</asp:ListItem>       
                             </asp:DropDownList>
                             <asp:HiddenField ID="HFlag" runat="server" />
                         </td>
                     </tr>
                     <tr><td align="center" colspan="3"> <asp:Button ID="btnsubmit" runat="server" Text="Generate Report" OnClick="btnsubmit_Click" /></td></tr>
                     <tr><td colspan ="3" align ="center"><asp:Label id="lblChapter" runat="server" Width="144px"></asp:Label></td></tr>
              </table>
         </center>
         <center><asp:Label ID="lblGrdMsg" runat="server"  ForeColor="Red" Text=""></asp:Label></center>
                <asp:DataGrid ID="grdRegByContest"   runat="server"  Width="432px" AutoGenerateColumns="False" >
            	    <FooterStyle Wrap="False"></FooterStyle>
				    <HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                            <asp:BoundColumn HeaderText="RegistrationDate" DataField="RegistrationDate" SortExpression="RegistrationDate" />
                            <asp:BoundColumn HeaderText="PrepClubDate" DataField="PrepClubDate" SortExpression="PrepClubDate" />
                            <asp:BoundColumn HeaderText="PrepClub" DataField="PrepClub" SortExpression="PrepClub" />
                            <asp:BoundColumn HeaderText="ChildLastName" DataField="ChildLastName" SortExpression="ChildLastName" />
                            <asp:BoundColumn HeaderText="ChildFirstName" DataField="ChildFirstName" SortExpression="ChildFirstName" />
                            <asp:BoundColumn HeaderText="Grade" DataField="Grade" SortExpression="Grade" />
                            <asp:BoundColumn HeaderText="DOB" DataField="DOB" SortExpression="DOB" />
                            <asp:BoundColumn HeaderText="GenderChild" DataField="GenderChild" SortExpression="GenderChild" />
                            <asp:BoundColumn HeaderText="CPhoneFather" DataField="CPhoneFather" SortExpression="CPhoneFather" />
                            <asp:BoundColumn HeaderText="HomePhone" DataField="HomePhone" SortExpression="HomePhone" />
                            <asp:BoundColumn HeaderText="LastNameFather" DataField="LastNameFather" SortExpression="LastNameFather" />
                            <asp:BoundColumn HeaderText="FirstNameFather" DataField="FirstNameFather" SortExpression="FirstNameFather" />
                            <asp:BoundColumn HeaderText="EmailFather" DataField="EmailFather" SortExpression="EmailFather" />
                            <asp:BoundColumn HeaderText="Status" DataField="Status" SortExpression="Status" />
                            <asp:BoundColumn HeaderText="SchoolName" DataField="SchoolName" SortExpression="SchoolName" />
                            <asp:BoundColumn HeaderText="ProductID" DataField="ProductID" SortExpression="ProductID" />
                            <asp:BoundColumn HeaderText="LastNameMother" DataField="LastNameMother" SortExpression="LastNameMother" />
                            <asp:BoundColumn HeaderText="FirstNameMother" DataField="FirstNameMother" SortExpression="FirstNameMother" />
                            <asp:BoundColumn HeaderText="EmailMother" DataField="EmailMother" SortExpression="EmailMother" />
                            <asp:BoundColumn HeaderText="CPhoneMother" DataField="CPhoneMother" SortExpression="CPhoneMother" />
                            <asp:BoundColumn HeaderText="PrepClubName" DataField="PrepClubName" SortExpression="PrepClubName" />
                           <asp:BoundColumn HeaderText="ChapterName" DataField="ChapterName" SortExpression="ChapterName" />
                    </Columns>
                    <PagerStyle Wrap="False"></PagerStyle>
                </asp:DataGrid><br />
                <table border="0" cellpadding = "2" cellspacing = "0" >
                    <tr><td><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink></td>
                        <td width="10px"></td>
                        <td><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="False">[Logout]</asp:HyperLink></td>
                    </tr>
                </table>
       </form>
    </body>
</html>


 
 
 