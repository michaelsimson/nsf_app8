﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master"  CodeFile="TopRanksList.aspx.vb" Inherits="Reports_TopRanksList" %>
 <asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
   
    <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
     <br />
<table cellpadding="3" cellspacing = "0" border = "0" style="width: 775px">
<tr>
    <td align="left" width="300px" > 
</td>
    <td colspan = "2" align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif;COLOR: #003366;font-size:	1em;font-weight:	700;Font-style:	normal;" >Top Rank List 
        at Regionals</td>
    <td><asp:Label ID="lblChapter" runat="server" Text="" ForeColor="Green"></asp:Label>
        <asp:CheckBox ID="chkAllChapter" runat="server" Text="All" ToolTip="Select All Chapter" />
    </td></tr>
<tr><td align="left" width="300px">&nbsp;</td>
    
    <td align="left" colspan="3"> <asp:RadioButton ID="rdoTopRankList" runat="server" Text="Top Rank List at Regionals" Checked="True" GroupName="RankList" />
    &nbsp;&nbsp;&nbsp;
    <asp:RadioButton ID="rdoPerfectScoreList" runat="server" Text="Perfect Scores List at Regionals" GroupName="RankList" />  &nbsp;&nbsp;&nbsp; </td> </tr>
   
<tr><td align="left">&nbsp;</td>
     <td align="left">Product Group: </td>
     <td  align="left"><asp:ListBox id="lstProductGroup" DataValueField="ProductGroupID" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" >
                                               </asp:ListBox></td>
    <td></td></tr>
 <tr><td align="left">&nbsp;</td> <td align="left">Product : </td> <td align = "left" ><asp:ListBox id="lstProductid" Enabled="false" DataValueField="ProductCode" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged">
                                                </asp:ListBox></td><td></td></tr>
 <tr><td align="left">&nbsp;</td>  <td align="left">Event Year : </td>  <td align="left"> 
  <asp:ListBox id="ddlYear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server">
   </asp:ListBox>
    
     
 </td> <td></td></tr> 
<tr><td align="left">&nbsp;</td> <td align="left">Top Count : </td> <td align="left">
    <asp:DropDownList ID="ddlTopCount" runat="server" Width="40px">
        <asp:ListItem>1</asp:ListItem>
        <asp:ListItem>2</asp:ListItem>
        <asp:ListItem>3</asp:ListItem>
        <asp:ListItem>4</asp:ListItem>
        <asp:ListItem>5</asp:ListItem>
        <asp:ListItem>6</asp:ListItem>
        <asp:ListItem>7</asp:ListItem>
        <asp:ListItem>8</asp:ListItem>
        <asp:ListItem>9</asp:ListItem>
        <asp:ListItem Selected="True">10</asp:ListItem>
    </asp:DropDownList>
    <asp:CheckBox ID="chkIncludeAchievements" runat="server" Text="Include Achievements" />
</td><td></td> </tr> 
<tr>
    <td align="center"> 
        &nbsp;</td> <td align="center" colspan ="2"> 
    <asp:Button ID="btnShowList" runat="server" Text="Show List" OnClick ="btnShowList_Click" />  &nbsp;&nbsp;&nbsp;&nbsp; <asp:Button Visible="false"  ID="btnExport" runat="server" Text="Export to Excel" OnClick="BtnExport_Click" Width="130px" /></td><td></td> </tr> 
<tr> 
    <td align="center">
        &nbsp;</td> <td align="center" colspan="2">
    <asp:Label ID="lblErr" runat="server"></asp:Label>
    </td><td></td> </tr> 

</table> 
      
     <br />
 
<div align="center" style="width: 1004px; overflow: scroll;">

        <asp:GridView ID="GVList" runat="server" BackColor="White" AutoGenerateColumns = "false" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical">
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
             <Columns>
                <asp:BoundField  DataField="parentid"   HeaderText="MemberID"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="ContestYear"   HeaderText="ContestYear"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="productcode"   HeaderText="ProductCode"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="rank"   HeaderText="Rank"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="CFname"   HeaderText="Child FName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="CLName"   HeaderText="Child LName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Grade"   HeaderText="Grade"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="emailFather"   HeaderText="EmailFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="firstnameFather"   HeaderText="FirstNameFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="lastnameFather"   HeaderText="LastNameFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="hphoneFather"   HeaderText="HphoneFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="cphoneFather"   HeaderText="CphoneFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="wphoneFather"   HeaderText="WphoneFather"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="emailMother"   HeaderText="EmailMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="firstnameMother"   HeaderText="FirstNameMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="lastnameMother"   HeaderText="LastNameMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
               <%-- <asp:BoundField  DataField="hphoneMother"   HeaderText="HphoneMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                --%><asp:BoundField  DataField="cphoneMother"   HeaderText="CphoneMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="wphoneMother"   HeaderText="WphoneMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="SchoolName"   HeaderText="SchoolName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Achievements" ItemStyle-Width="200px"   HeaderText="Achievements"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Hobbies"  ItemStyle-Width="200px"  HeaderText="Hobbies"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="address1"   HeaderText="address1"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="address2"   HeaderText="address2"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="city"   HeaderText="city"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="state"   HeaderText="state"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="zip"   HeaderText="Zip Code"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
             </Columns>     
        </asp:GridView>
        <br />
 
        <asp:GridView ID="GVPerfectScoreList" runat="server" BackColor="White" AutoGenerateColumns = "False" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical" EnableModelValidation="True" Visible="False">
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
             <Columns>
               <%-- <asp:BoundField  DataField="hphoneMother"   HeaderText="HphoneMother"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                --%>
                <asp:BoundField  DataField="badgenumber"   HeaderText="BadgeNumber"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " />
                <asp:BoundField  DataField="productname"  ItemStyle-Width="200px"  HeaderText="ProductName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " />
                <asp:BoundField  DataField="createdate"   HeaderText="CreateDate"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " />
                <asp:BoundField  DataField="childnumber"   HeaderText="ChildNumber"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="Cfirstname"   HeaderText="ChildFName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="Clastname"   HeaderText="ChildLName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                 <asp:BoundField DataField="Cgender" HeaderText="ChildGender"   HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                 <asp:BoundField DataField="DOB" HeaderText="ChildDOB"   HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true"/>
                <asp:BoundField  DataField="Grade"   HeaderText="Grade"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="pfirstname"   HeaderText="PFirstName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true"/>
                <asp:BoundField  DataField="plastname"   HeaderText="PLastName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true"/>
                <asp:BoundField  DataField="hphone"   HeaderText="Hphone"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true"/>
                <asp:BoundField  DataField="cphone"   HeaderText="Cphone"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="email"   HeaderText="Email"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="partchapter"  ItemStyle-Width="230px" HeaderText="PartChapter"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="homechapter"  ItemStyle-Width="230px" HeaderText="HomeChapter"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="contestyear"   HeaderText="ContestYear"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                 <asp:BoundField  DataField="score1"   HeaderText="Score1"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="score2"   HeaderText="Score2"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="score3"    HeaderText="Score3"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="city"   HeaderText="City"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                <asp:BoundField  DataField="state"   HeaderText="State"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "/>
                  <asp:BoundField  DataField="SchoolName"   HeaderText="SchoolName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Achievements" ItemStyle-Width="200px"   HeaderText="Achievements"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Hobbies"  ItemStyle-Width="200px"  HeaderText="Hobbies"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
             </Columns>     
        </asp:GridView>
</div>
 

	</asp:Content>		

