<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="DisplayPercentilesNationals.aspx.cs" Inherits="DisplayPercentilesNationals"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <div class="title02">
 
 Percentile Values for Nationals </div> 
    <table align="center" class="btn_01"><tr><td colspan="3">
           
                              <a href="DisplayPercentilesNationals.aspx?Contest=JSB"> Junior Spelling  </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=SSB"> Senior Spelling </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=JVB"> Junior Vocabulary </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=IVB"> Intermediate Vocabulary </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=MB1"> Math Level 1</a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=MB2"> Math Level 2</a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=MB3"> Math Level 3</a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=JGB"> Junior Geography</a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=SGB"> Senior Geography</a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=JSC"> Junior Science </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=ISC"> Intermediate Science </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=SSC"> Senior Science </a>|
	                          <a href="DisplayPercentilesNationals.aspx?Contest=BB"> Brain Bee </a>

                    </td>
                    </tr> 
                    <tr><td align="center"><asp:Label ID="lblProductCode" runat="server" Font-Bold="False" Font-Size="Large" ForeColor="Green"></asp:Label></td></tr> 
        <tr>
             <td>  <asp:datagrid id="DGShowPercentiles"  runat="server" Visible="true" AutoGenerateColumns="true" 
		                    Width="945px" Height="304px" BorderColor="#E7E7FF" BorderStyle="Inset" BorderWidth="1px" BackColor="White"
		                    CellPadding="3" GridLines="Both" class="btn_01" Font-Bold="false">
		                    <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
		                    <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
		                    <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
		                    <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
		                    <HeaderStyle  Font-Bold="True"   Font-Size="Medium" ForeColor="#F7F7F7" BackColor="#A9A6A6"></HeaderStyle>
		                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		                    <Columns>
		                    </Columns> 
		               </asp:datagrid> 
		    </td>
        </tr>
        <tr><td><asp:Label ID="lblError" runat="server" Text=""></asp:Label></td></tr>
   </table>   
         
</asp:Content>

 
 
 