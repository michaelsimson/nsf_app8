using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.SessionState;
using System.IO;
using Microsoft.ApplicationBlocks.Data;
using NativeExcel;
using System.Runtime.InteropServices;
using System.Web.Services;
using System.Collections.Generic;
using System.Drawing;

//using System.Runtime.InteropServices.Marshal;

public partial class Reports_PercentilesByGrade : System.Web.UI.Page
{
    // string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
    //string Year ;
    string contestType;
    int ProductId, ProductGroupID, ContestPeriod;
    String Product_Code, ProductGroupcode, ContestDays;
    int StartGrade, EndGrade;
    DataSet dsContestants;
    DataSet dsContests;
    DataTable dt2 = new DataTable();
    String SQLExecString = "";
    Boolean DuplicateFlag;
    Boolean ContestFlag = false;
    int EventID;
    String ContestData;
    int countflag = 0;
    int isExcel;
    int cutOffFlag;

    //Response.Cache.SetCacheability(HttpCacheability.NoCache);

    protected void Page_Load(object sender, EventArgs e)
    {


        //  Year = DateTime.Now.Year.ToString();    //System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        // contestType = ddlContest.SelectedValue;
        //string MyFile = @"c:\sample.xls";
        //CreateWorkbook(MyFile);
        //Console.WriteLine("File Saved to " + MyFile);
        //Console.ReadLine();
        lblSuccess.Text = "";

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!this.IsPostBack)
            {
                isExcel = 0;
                cutOffFlag = 0;
                //DuplicateFlag = false;
                EventID = Convert.ToInt32(Request.QueryString["EventID"]);
                LoadEvent(EventID);
                LoadContestYear();
                LoadProduct(EventID);

                if (ddlEvent.SelectedValue == "1")
                {
                    lblPercentile.Text = "Percentiles By Grade Nationals";
                }
                else
                {
                    lblPercentile.Text = "Percentiles by Contest/Grade and Cutoff Scores";
                }

                fillStartingScore();
            }
        }
        //ReadAllScores(); Commented on 14-02-2013 to standardise the module
    }
    void LoadEvent(int EventID)
    {
        String SQLStr = "SELECT Distinct EventId,Name FROM Event WHERE EventID in (" + EventID + ")";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlEvent.DataSource = ds;
            ddlEvent.DataTextField = "Name";
            ddlEvent.DataValueField = "EventId";
            ddlEvent.DataBind();
        }
        ddlEvent.Enabled = false;
        //ddlEvent.Items.Insert(0, "Select");
        // ddlEvent.SelectedIndex = 0;

    }
    void LoadContestYear()
    {
        int year = 0;
        //  'year = Convert.ToInt32(DateTime.Now.Year)
        int first_year = 2008;

        year = Convert.ToInt32(DateTime.Now.Year) + 1;
        int count = year - first_year;
        int i;
        for (i = 0; i < count; i++) //first_year To Convert.ToInt32(DateTime.Now.Year)
        {
            ddlYear.Items.Insert(i, new ListItem(Convert.ToString(year - (i + 1)), Convert.ToString(year - (i + 1))));
        }
        ddlYear.Items.Insert(0, "Select");
        ddlYear.SelectedIndex = 0;
    }
    void LoadProduct(int EventID)
    {
        String SQLStr = "SELECT Distinct ProductId,ProductCode FROM Product WHERE EventID=" + EventID + " and Status='O' and ProductGroupCode not in ('EW','PS')";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlContest.DataSource = ds;
            ddlContest.DataTextField = "ProductCode";
            ddlContest.DataValueField = "ProductId";
            ddlContest.DataBind();
        }
        ddlContest.Items.Insert(0, "All");
        ddlContest.SelectedIndex = 0;
    }
    void ReadAllScores(String ProductCode, String Contest_Day, int Contest_Period)
    {
        try
        {
            //lblSuccess.Text = "";
            String[] Contestdates;
            String ContestDate = "";

            ContestDays = Contest_Day;
            ContestPeriod = Contest_Period;
            // connect to the database
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(Application["ConnectionString"].ToString());
            connection.Open();
            // get records from the Contestant table
            contestType = ProductCode;// ddlContest.SelectedValue;

            //Product_Code = ProductCode;
            DataSet ds_product = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ProductId,ProductCode,ProductGroupId,ProductgroupCode FROM Product Where ProductCode ='" + contestType + "' and eventId =" + ddlEvent.SelectedValue);
            if (ds_product.Tables[0].Rows.Count > 0)
            {
                ProductId = Convert.ToInt32(ds_product.Tables[0].Rows[0]["ProductID"]);
                ProductGroupID = Convert.ToInt32(ds_product.Tables[0].Rows[0]["ProductGroupId"]);
                ProductGroupcode = ds_product.Tables[0].Rows[0]["ProductGroupcode"].ToString();
            }

            if (Contest_Day != "All") //(ddlcontestdate.SelectedItem.Text != "All")
            {
                Contestdates = Contest_Day.Split(','); ;// ddlcontestdate.SelectedItem.Text.Split(',');
                ContestDate = "'" + Contestdates[0] + "," + ddlYear.SelectedValue + "','" + Contestdates[1] + "," + ddlYear.SelectedValue + "'";
            }
            string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
                                    "Contestant.Score1, Contestant.Score2, IsNull(Contestant.Rank,0) as Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID, Contestant.GRADE from Contestant " +
                                    "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber ";
            if (Contest_Day != "All") //ddlcontestdate.SelectedItem.Text
                commandString = commandString + "INNER JOIN Contest c ON C.ContestID=Contestant.ContestCode AND C.ContestDate IN (" + ContestDate + ")"; //CONVERT(VARCHAR(10),C.ContestDate,110) IN // ddlcontestdate.SelectedItem.Attributes["ContestDates"] 

            commandString = commandString + " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear=" + ddlYear.SelectedValue + //year
                             " AND (Contestant.Score1>0 OR Contestant.Score2>0) AND Contestant.ProductCode = '" + contestType + "' And Contestant.EventId= " + ddlEvent.SelectedValue + " and Contestant.Grade <>-1 ORDER BY Contestant.GRADE";

            // create the command object and set its
            // command string and connection
            //Response.Write(commandString);
            SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
            dsContestants = new DataSet();
            daContestants.Fill(dsContestants);

            if (dsContestants.Tables[0].Rows.Count > 0)
            {
                ProcessData(dsContestants);
            }
            else
            {
                if (ddlContest.SelectedItem.Text == "All")
                {
                    if (countflag == 0)
                    {
                        ContestData = ContestData + contestType + ",";
                        countflag++;
                    }
                    else
                    {
                        if (!ContestData.Contains(contestType + ","))
                        {
                            ContestData = ContestData + contestType + ",";
                        }
                    }
                    ContestFlag = true;
                }
                else
                {
                    lblError.Text = "No Contestant Data present for the Contest " + contestType;
                }

            }
            if (ContestFlag == true)
            {
                lblError.Text = " No Contestant Data present for the Contests " + ContestData.TrimEnd(',');
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    void ProcessData(DataSet ds)
    {
        try
        {
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("Contestant", typeof(int));
            dt.Columns.Add("TotalScore", typeof(double));
            dt.Columns.Add("Grade", typeof(int));
            int curGrade = -1;
            int count = -1;
            int[] aryCCount = new int[5];
            double[] aryTscore = new double[ds.Tables[0].Rows.Count];
            double[] grScore = new double[ds.Tables[0].Rows.Count];

            // if (ds.Tables[0].Rows.Count > 0)
            // {
            //ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductID"]);
            //ProductGroupID = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductGroupId"]);
            //ProductGroupcode = ds.Tables[0].Rows[0]["ProductGroupcode"].ToString();
            //  }

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                dr = dt.NewRow();
                dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
                dr["TotalScore"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                    Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
                dr["Grade"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);

                //if ((curGrade == Convert.ToInt32(dr["Grade"]) || ((curGrade == 0) || (curGrade == 1)) && (Convert.ToInt32(dr["Grade"]) == 0) || (Convert.ToInt32(dr["Grade"]) == 1)))
                if ((curGrade == Convert.ToInt32(dr["Grade"])) || ((curGrade == 0) || (curGrade == 1)) && ((Convert.ToInt32(dr["Grade"]) == 0) || (Convert.ToInt32(dr["Grade"]) == 1)))
                {
                    aryCCount[count] = aryCCount[count] + 1;
                }
                else
                {
                    curGrade = Convert.ToInt32(dr["Grade"]);
                    aryCCount[++count] = 1;
                }

                grScore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
                aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                    Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
                dt.Rows.Add(dr);
            }

            SortArrays(aryTscore, grScore);

            dt2 = new DataTable();
            dt2.Columns.Add("TotalScore", typeof(double));
            switch (contestType)
            {
                case "MB1":
                    dt2.Columns.Add("Gr1%", typeof(string));
                    dt2.Columns.Add("Gr2%", typeof(string));
                    dt2.Columns.Add("Gr1%tile", typeof(string));
                    dt2.Columns.Add("Gr1cnt", typeof(string));
                    dt2.Columns.Add("Gr1cum", typeof(string));
                    dt2.Columns.Add("Gr2%tile", typeof(string));
                    dt2.Columns.Add("Gr2cnt", typeof(string));
                    dt2.Columns.Add("Gr2cum", typeof(string));
                    StartGrade = 1;
                    EndGrade = 2;
                    break;
                case "MB2":
                    dt2.Columns.Add("Gr3%", typeof(string));
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr3%tile", typeof(string));
                    dt2.Columns.Add("Gr3cnt", typeof(string));
                    dt2.Columns.Add("Gr3cum", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    StartGrade = 3;
                    EndGrade = 5;
                    break;
                case "MB3":
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
                    break;
                case "MB4":
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    StartGrade = 9;
                    EndGrade = 10;
                    break;
                case "JSB":
                case "JSC":
                    dt2.Columns.Add("Gr1%", typeof(string));
                    dt2.Columns.Add("Gr2%", typeof(string));
                    dt2.Columns.Add("Gr3%", typeof(string));
                    dt2.Columns.Add("Gr1%tile", typeof(string));
                    dt2.Columns.Add("Gr1cnt", typeof(string));
                    dt2.Columns.Add("Gr1cum", typeof(string));
                    dt2.Columns.Add("Gr2%tile", typeof(string));
                    dt2.Columns.Add("Gr2cnt", typeof(string));
                    dt2.Columns.Add("Gr2cum", typeof(string));
                    dt2.Columns.Add("Gr3%tile", typeof(string));
                    dt2.Columns.Add("Gr3cnt", typeof(string));
                    dt2.Columns.Add("Gr3cum", typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
                    break;
                case "ISC":
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    StartGrade = 4;
                    EndGrade = 5;
                    break;
                case "SSC":
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
                    break;
                case "SSB":
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
                    break;
                case "JVB":
                    dt2.Columns.Add("Gr1%", typeof(string));
                    dt2.Columns.Add("Gr2%", typeof(string));
                    dt2.Columns.Add("Gr3%", typeof(string));
                    dt2.Columns.Add("Gr1%tile", typeof(string));
                    dt2.Columns.Add("Gr1cnt", typeof(string));
                    dt2.Columns.Add("Gr1cum", typeof(string));
                    dt2.Columns.Add("Gr2%tile", typeof(string));
                    dt2.Columns.Add("Gr2cnt", typeof(string));
                    dt2.Columns.Add("Gr2cum", typeof(string));
                    dt2.Columns.Add("Gr3%tile", typeof(string));
                    dt2.Columns.Add("Gr3cnt", typeof(string));
                    dt2.Columns.Add("Gr3cum", typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
                    break;
                case "IVB":
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
                    break;
                case "SVB":
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr11%", typeof(string));
                    dt2.Columns.Add("Gr12%", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    dt2.Columns.Add("Gr11%tile", typeof(string));
                    dt2.Columns.Add("Gr11cnt", typeof(string));
                    dt2.Columns.Add("Gr11cum", typeof(string));
                    dt2.Columns.Add("Gr12%tile", typeof(string));
                    dt2.Columns.Add("Gr12cnt", typeof(string));
                    dt2.Columns.Add("Gr12cum", typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
                    break;
                case "JGB":
                    dt2.Columns.Add("Gr1%", typeof(string));
                    dt2.Columns.Add("Gr2%", typeof(string));
                    dt2.Columns.Add("Gr3%", typeof(string));
                    dt2.Columns.Add("Gr1%tile", typeof(string));
                    dt2.Columns.Add("Gr1cnt", typeof(string));
                    dt2.Columns.Add("Gr1cum", typeof(string));
                    dt2.Columns.Add("Gr2%tile", typeof(string));
                    dt2.Columns.Add("Gr2cnt", typeof(string));
                    dt2.Columns.Add("Gr2cum", typeof(string));
                    dt2.Columns.Add("Gr3%tile", typeof(string));
                    dt2.Columns.Add("Gr3cnt", typeof(string));
                    dt2.Columns.Add("Gr3cum", typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
                    break;
                case "SGB":
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
                    break;
                case "EW1":
                    dt2.Columns.Add("Gr3%", typeof(string));
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr3%tile", typeof(string));
                    dt2.Columns.Add("Gr3cnt", typeof(string));
                    dt2.Columns.Add("Gr3cum", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    StartGrade = 3;
                    EndGrade = 5;
                    break;
                case "EW2":
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
                    break;
                case "EW3":
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr11%", typeof(string));
                    dt2.Columns.Add("Gr12%", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    dt2.Columns.Add("Gr11%tile", typeof(string));
                    dt2.Columns.Add("Gr11cnt", typeof(string));
                    dt2.Columns.Add("Gr11cum", typeof(string));
                    dt2.Columns.Add("Gr12%tile", typeof(string));
                    dt2.Columns.Add("Gr12cnt", typeof(string));
                    dt2.Columns.Add("Gr12cum", typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
                    break;
                case "PS1":
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
                    break;
                case "PS3":
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr11%", typeof(string));
                    dt2.Columns.Add("Gr12%", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    dt2.Columns.Add("Gr11%tile", typeof(string));
                    dt2.Columns.Add("Gr11cnt", typeof(string));
                    dt2.Columns.Add("Gr11cum", typeof(string));
                    dt2.Columns.Add("Gr12%tile", typeof(string));
                    dt2.Columns.Add("Gr12cnt", typeof(string));
                    dt2.Columns.Add("Gr12cum", typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
                    break;
                case "BB":
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr11%", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    dt2.Columns.Add("Gr11%tile", typeof(string));
                    dt2.Columns.Add("Gr11cnt", typeof(string));
                    dt2.Columns.Add("Gr11cum", typeof(string));
                    StartGrade = 8;
                    EndGrade = 11;
                    break;
            }
            dt2.Columns.Add("TotalPercentile", typeof(string));
            dt2.Columns.Add("TotalCount", typeof(double));
            dt2.Columns.Add("TotalCumeCount", typeof(double));

            dr = dt2.NewRow();

            if (aryTscore.Length > 0)
                dr["TotalScore"] = aryTscore[0];

            int[] grCount = new int[13];
            int[] grCCount = new int[13];
            int[] grTCount = new int[13];
            float[] pc = new float[13];
            float[] pcile = new float[13];
            for (int k = 0; k < grScore.Length; k++)
            {
                if (grScore[k] >= -0.1 && grScore[k] <= 1.5)
                    grTCount[1]++;
                else if (grScore[k] > 1.5 && grScore[k] < 2.5)
                    grTCount[2]++;
                else if (grScore[k] > 2.5 && grScore[k] < 3.5)
                    grTCount[3]++;
                else if (grScore[k] > 3.5 && grScore[k] < 4.5)
                    grTCount[4]++;
                else if (grScore[k] > 4.5 && grScore[k] <= 5.5)
                    grTCount[5]++;
                else if (grScore[k] > 5.5 && grScore[k] < 6.5)
                    grTCount[6]++;
                else if (grScore[k] > 6.5 && grScore[k] < 7.5)
                    grTCount[7]++;
                else if (grScore[k] > 7.5 && grScore[k] < 8.5)
                    grTCount[8]++;
                else if (grScore[k] > 8.5 && grScore[k] < 9.5)
                    grTCount[9]++;
                else if (grScore[k] > 9.5 && grScore[k] < 10.5)
                    grTCount[10]++;
                else if (grScore[k] > 10.5 && grScore[k] < 11.5)
                    grTCount[11]++;
                else if (grScore[k] > 11.5)
                    grTCount[12]++;
            }
            double lastScore = 0;
            if (aryTscore.Length > 0)
                lastScore = aryTscore[0];

            String SQLInsertCutOff = "";
            String SQLInsertCutOffVal = "";
            Boolean Contest_Flag = false;
            int j;

            int totalCumCount = 0, totalCount = 0, tScoreCount = 0, cCount = 0;
            double totalPercentile = 0.0;

            totalCount = aryTscore.Length;

            for (j = 0; j < aryTscore.Length; j++)
            {
                if (lastScore == aryTscore[j])
                {
                    if ((grScore[j] == 1) || (grScore[j] == 0))
                    {
                        grCount[1] = grCount[1] + 1;
                    }
                    if (grScore[j] == 2)
                    {
                        grCount[2] = grCount[2] + 1;
                    }
                    if (grScore[j] == 3)
                    {
                        grCount[3] = grCount[3] + 1;
                    }
                    if (grScore[j] == 4)
                    {
                        grCount[4] = grCount[4] + 1;
                    }
                    if (grScore[j] == 5)
                    {
                        grCount[5] = grCount[5] + 1;
                    }
                    if (grScore[j] == 6)
                    {
                        grCount[6] = grCount[6] + 1;
                    }
                    if (grScore[j] == 7)
                    {
                        grCount[7] = grCount[7] + 1;
                    }
                    if (grScore[j] == 8)
                    {
                        grCount[8] = grCount[8] + 1;
                    }
                    if (grScore[j] == 9)
                    {
                        grCount[9] = grCount[9] + 1;
                    }
                    if (grScore[j] == 10)
                    {
                        grCount[10] = grCount[10] + 1;
                    }
                    if (grScore[j] == 11)
                    {
                        grCount[11] = grCount[11] + 1;
                    }
                    if (grScore[j] == 12)
                    {
                        grCount[12] = grCount[12] + 1;
                    }

                    cCount++;
                }
                else
                {
                    lastScore = aryTscore[j];

                    SQLInsertCutOff = "Insert into PercentilesByGrade ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
                    SQLInsertCutOffVal = ") Values (" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + "," + ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + aryTscore[j - 1] + ""; //Year

                    switch (contestType)
                    {
                        case "MB1":
                            pc[1] = (float)grCount[1] / grTCount[1];
                            dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                            //grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
                            pcile[1] = (float)grCCount[1] / grTCount[1];
                            dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                            dr["Gr1cnt"] = grCount[1];
                            dr["Gr1cum"] = grCCount[1];
                            pc[2] = (float)grCount[2] / grTCount[2];
                            dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                            //grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
                            pcile[2] = (float)grCCount[2] / grTCount[2];
                            dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));

                            dr["Gr2cnt"] = grCount[2];
                            dr["Gr2cum"] = grCCount[2];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];
                            break;
                        case "MB2":
                        case "EW1":
                            pc[3] = (float)grCount[3] / grTCount[3];
                            dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                            //grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[0];
                            aryCCount[0] -= grCount[3];
                            pcile[3] = (float)grCCount[3] / grTCount[3];
                            dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                            dr["Gr3cnt"] = grCount[3];
                            dr["Gr3cum"] = grCCount[3];
                            pc[4] = (float)grCount[4] / grTCount[4];
                            dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                            //grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[1];
                            aryCCount[1] -= grCount[4];
                            pcile[4] = (float)grCCount[4] / grTCount[4];
                            dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                            dr["Gr4cnt"] = grCount[4];
                            dr["Gr4cum"] = grCCount[4];
                            pc[5] = (float)grCount[5] / grTCount[5];
                            dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                            //grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[2];
                            aryCCount[2] -= grCount[5];
                            pcile[5] = (float)grCCount[5] / grTCount[5];
                            dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                            dr["Gr5cnt"] = grCount[5];
                            dr["Gr5cum"] = grCCount[5];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

                            break;
                        case "MB3":
                        case "EW2":
                        case "PS1":
                            pc[6] = (float)grCount[6] / grTCount[6];
                            dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                            //grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[0];
                            aryCCount[0] -= grCount[6];
                            pcile[6] = (float)grCCount[6] / grTCount[6];
                            dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                            dr["Gr6cnt"] = grCount[6];
                            dr["Gr6cum"] = grCCount[6];
                            pc[7] = (float)grCount[7] / grTCount[7];
                            dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                            //grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[1];
                            aryCCount[1] -= grCount[7];
                            pcile[7] = (float)grCCount[7] / grTCount[7];
                            dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                            dr["Gr7cnt"] = grCount[7];
                            dr["Gr7cum"] = grCCount[7];
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[2];
                            aryCCount[2] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                            break;
                        case "MB4":
                            pc[9] = (float)grCount[9] / grTCount[9];
                            dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                            //grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
                            pcile[9] = (float)grCCount[9] / grTCount[9];
                            dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                            dr["Gr9cnt"] = grCount[9];
                            dr["Gr9cum"] = grCCount[9];
                            pc[10] = (float)grCount[10] / grTCount[10];
                            dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                            //grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[2];
                            aryCCount[2] -= grCount[10];
                            pcile[10] = (float)grCCount[10] / grTCount[10];
                            dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                            dr["Gr10cnt"] = grCount[10];
                            dr["Gr10cum"] = grCCount[10];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

                            break;

                        case "JSB":
                        case "JSC":
                        case "JGB":

                            pc[1] = (float)grCount[1] / grTCount[1];
                            dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                            //grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
                            pcile[1] = (float)grCCount[1] / grTCount[1];
                            dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                            dr["Gr1cnt"] = grCount[1];
                            dr["Gr1cum"] = grCCount[1];
                            pc[2] = (float)grCount[2] / grTCount[2];
                            dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                            //grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
                            pcile[2] = (float)grCCount[2] / grTCount[2];
                            dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                            dr["Gr2cnt"] = grCount[2];
                            dr["Gr2cum"] = grCCount[2];
                            pc[3] = (float)grCount[3] / grTCount[3];
                            dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                            //grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[2];
                            aryCCount[2] -= grCount[3];
                            pcile[3] = (float)grCCount[3] / grTCount[3];
                            dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                            dr["Gr3cnt"] = grCount[3];
                            dr["Gr3cum"] = grCCount[3];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

                            break;
                        case "ISC":
                            pc[4] = (float)grCount[4] / grTCount[4];
                            dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                            //grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
                            pcile[4] = (float)grCCount[4] / grTCount[4];
                            dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                            dr["Gr4cnt"] = grCount[4];
                            dr["Gr4cum"] = grCCount[4];
                            pc[5] = (float)grCount[5] / grTCount[5];
                            dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                            //grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
                            pcile[5] = (float)grCCount[5] / grTCount[5];
                            dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                            dr["Gr5cnt"] = grCount[5];
                            dr["Gr5cum"] = grCCount[5];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
                            break;
                        case "SSC":
                            pc[6] = (float)grCount[6] / grTCount[6];
                            dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                            //grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[0];
                            aryCCount[0] -= grCount[6];
                            pcile[6] = (float)grCCount[6] / grTCount[6];
                            dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                            dr["Gr6cnt"] = grCount[6];
                            dr["Gr6cum"] = grCCount[6];
                            pc[7] = (float)grCount[7] / grTCount[7];
                            dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                            //grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[1];
                            aryCCount[1] -= grCount[7];
                            pcile[7] = (float)grCCount[7] / grTCount[7];
                            dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                            dr["Gr7cnt"] = grCount[7];
                            dr["Gr7cum"] = grCCount[7];
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[2];
                            aryCCount[2] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];
                            break;
                        case "SSB":
                        case "SGB":
                            pc[4] = (float)grCount[4] / grTCount[4];
                            dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                            //grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
                            pcile[4] = (float)grCCount[4] / grTCount[4];
                            dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                            dr["Gr4cnt"] = grCount[4];
                            dr["Gr4cum"] = grCCount[4];
                            pc[5] = (float)grCount[5] / grTCount[5];
                            dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                            //grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
                            pcile[5] = (float)grCCount[5] / grTCount[5];
                            dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                            dr["Gr5cnt"] = grCount[5];
                            dr["Gr5cum"] = grCCount[5];
                            pc[6] = (float)grCount[6] / grTCount[6];
                            dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                            //grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[2];
                            aryCCount[2] -= grCount[6];
                            pcile[6] = (float)grCCount[6] / grTCount[6];
                            dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                            dr["Gr6cnt"] = grCount[6];
                            dr["Gr6cum"] = grCCount[6];
                            pc[7] = (float)grCount[7] / grTCount[7];
                            dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                            //grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[3];
                            aryCCount[3] -= grCount[7];
                            pcile[7] = (float)grCCount[7] / grTCount[7];
                            dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                            dr["Gr7cnt"] = grCount[7];
                            dr["Gr7cum"] = grCCount[7];
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[4];
                            aryCCount[4] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                            break;
                        case "JVB":
                            pc[1] = (float)grCount[1] / grTCount[1];
                            dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                            //grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
                            pcile[1] = (float)grCCount[1] / grTCount[1];
                            dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                            dr["Gr1cnt"] = grCount[1];
                            dr["Gr1cum"] = grCCount[1];
                            pc[2] = (float)grCount[2] / grTCount[2];
                            dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                            //grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
                            pcile[2] = (float)grCCount[2] / grTCount[2];
                            dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                            dr["Gr2cnt"] = grCount[2];
                            dr["Gr2cum"] = grCCount[2];
                            pc[3] = (float)grCount[3] / grTCount[3];
                            dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                            //grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[2];
                            aryCCount[2] -= grCount[3];
                            pcile[3] = (float)grCCount[3] / grTCount[3];
                            dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                            dr["Gr3cnt"] = grCount[3];
                            dr["Gr3cum"] = grCCount[3];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

                            break;
                        case "IVB":
                            pc[4] = (float)grCount[4] / grTCount[4];
                            dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                            //grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
                            pcile[4] = (float)grCCount[4] / grTCount[4];
                            dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                            dr["Gr4cnt"] = grCount[4];
                            dr["Gr4cum"] = grCCount[4];
                            pc[5] = (float)grCount[5] / grTCount[5];
                            dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                            //grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
                            pcile[5] = (float)grCCount[5] / grTCount[5];
                            dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                            dr["Gr5cnt"] = grCount[5];
                            dr["Gr5cum"] = grCCount[5];
                            pc[6] = (float)grCount[6] / grTCount[6];
                            dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                            //grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[2];
                            aryCCount[2] -= grCount[6];
                            pcile[6] = (float)grCCount[6] / grTCount[6];
                            dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                            dr["Gr6cnt"] = grCount[6];
                            dr["Gr6cum"] = grCCount[6];
                            pc[7] = (float)grCount[7] / grTCount[7];
                            dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                            //grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[3];
                            aryCCount[3] -= grCount[7];
                            pcile[7] = (float)grCCount[7] / grTCount[7];
                            dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                            dr["Gr7cnt"] = grCount[7];
                            dr["Gr7cum"] = grCCount[7];
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[4];
                            aryCCount[4] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                            break;
                        case "SVB":
                            pc[9] = (float)grCount[9] / grTCount[9];
                            dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                            //grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
                            pcile[9] = (float)grCCount[9] / grTCount[9];
                            dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                            dr["Gr9cnt"] = grCount[9];
                            dr["Gr9cum"] = grCCount[9];
                            pc[10] = (float)grCount[10] / grTCount[10];
                            dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                            //grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[1];
                            aryCCount[1] -= grCount[10];
                            pcile[10] = (float)grCCount[10] / grTCount[10];
                            dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                            dr["Gr10cnt"] = grCount[10];
                            dr["Gr10cum"] = grCCount[10];
                            pc[11] = (float)grCount[11] / grTCount[11];
                            dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                            //grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[2];
                            aryCCount[2] -= grCount[11];
                            pcile[11] = (float)grCCount[11] / grTCount[11];
                            dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                            dr["Gr11cnt"] = grCount[11];
                            dr["Gr11cum"] = grCCount[11];
                            if (grTCount[12] == 0)
                            {
                                pc[12] = 0;
                                dr["Gr12%"] = "";
                                dr["Gr12%tile"] = "";
                                dr["Gr12cnt"] = grCount[12];
                                dr["Gr12cum"] = grCCount[12];
                            }
                            else
                            {
                                pc[12] = (float)grCount[12] / grTCount[12];
                                dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                                //grCCount[12] += grCount[12];
                                grCCount[12] = aryCCount[3];
                                aryCCount[3] -= grCount[12];
                                pcile[12] = (float)grCCount[12] / grTCount[12];
                                dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                                dr["Gr12cnt"] = grCount[12];
                                dr["Gr12cum"] = grCCount[12];
                            }

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

                            break;
                        case "EW3":
                        case "PS3":
                            pc[9] = (float)grCount[9] / grTCount[9];
                            dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                            //grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
                            pcile[9] = (float)grCCount[9] / grTCount[9];
                            dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                            dr["Gr9cnt"] = grCount[9];
                            dr["Gr9cum"] = grCCount[9];
                            pc[10] = (float)grCount[10] / grTCount[10];
                            dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                            //grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[1];
                            aryCCount[1] -= grCount[10];
                            pcile[10] = (float)grCCount[10] / grTCount[10];
                            dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                            dr["Gr10cnt"] = grCount[10];
                            dr["Gr10cum"] = grCCount[10];
                            pc[11] = (float)grCount[11] / grTCount[11];
                            dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                            //grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[2];
                            aryCCount[2] -= grCount[11];
                            pcile[11] = (float)grCCount[11] / grTCount[11];
                            dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                            dr["Gr11cnt"] = grCount[11];
                            dr["Gr11cum"] = grCCount[11];
                            if (grTCount[12] == 0)
                            {
                                pc[12] = 0;
                                dr["Gr12%"] = "";
                                dr["Gr12%tile"] = "";
                            }
                            else
                            {
                                pc[12] = (float)grCount[12] / grTCount[12];
                                dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                                //grCCount[12] += grCount[12];
                                grCCount[12] = aryCCount[3];
                                aryCCount[3] -= grCount[12];
                                pcile[12] = (float)grCCount[12] / grTCount[12];
                                dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                            }
                            dr["Gr12cnt"] = grCount[12];
                            dr["Gr12cum"] = grCCount[12];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

                            break;
                        case "BB":
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[0];
                            aryCCount[0] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];
                            pc[9] = (float)grCount[9] / grTCount[9];
                            dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                            //grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[1];
                            aryCCount[1] -= grCount[9];
                            pcile[9] = (float)grCCount[9] / grTCount[9];
                            dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                            dr["Gr9cnt"] = grCount[9];
                            dr["Gr9cum"] = grCCount[9];
                            pc[10] = (float)grCount[10] / grTCount[10];
                            dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                            //grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[2];
                            aryCCount[2] -= grCount[10];
                            pcile[10] = (float)grCCount[10] / grTCount[10];
                            dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                            dr["Gr10cnt"] = grCount[10];
                            dr["Gr10cum"] = grCCount[10];
                            pc[11] = (float)grCount[11] / grTCount[11];
                            dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                            //grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[3];
                            aryCCount[3] -= grCount[11];
                            pcile[11] = (float)grCCount[11] / grTCount[11];
                            dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                            dr["Gr11cnt"] = grCount[11];
                            dr["Gr11cum"] = grCCount[11];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"] + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"];
                            break;

                    }
                    dt2.Rows.Add(dr);

                    totalCumCount = (grCCount[1] + grCCount[2] + grCCount[3] + grCCount[4] + grCCount[5] + grCCount[6] + grCCount[7] + grCCount[8] + grCCount[9] + grCCount[10] + grCCount[11] + grCCount[12]);
                    tScoreCount = (grCount[1] + grCount[2] + grCount[3] + grCount[4] + grCount[5] + grCount[6] + grCount[7] + grCount[8] + grCount[9] + grCount[10] + grCount[11] + grCount[12]);

                    totalPercentile = (float)totalCumCount / totalCount;// aryTscore.Length);

                    dr["TotalPercentile"] = String.Format("{0:P1}", Math.Round(totalPercentile, 3));// String.Format("{0:F1}", 100 * Math.Round(totalPercentile, 3));
                    dr["TotalCount"] = tScoreCount;
                    dr["TotalCumeCount"] = totalCumCount;

                    dr = dt2.NewRow();
                    dr["TotalScore"] = aryTscore[j];

                    SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,ContestPeriod,ContestDate,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + tScoreCount + "," + totalCumCount + ",'" + ContestPeriod + "','" + ContestDays + "','" + Session["LoginID"] + "', GETDATE())";

                    //SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + Session["LoginID"] + "', GETDATE())";
                    SQLInsertCutOff = "";
                    SQLInsertCutOffVal = "";

                    totalCumCount = totalCumCount - cCount;
                    cCount = 1;

                    for (int q = 0; q < 13; q++)
                        grCount[q] = 0;
                    if ((grScore[j] == 1) || (grScore[j] == 0))
                    {
                        grCount[1] = 1;
                    }
                    if (grScore[j] == 2)
                    {
                        grCount[2] = 1;
                    }
                    if (grScore[j] == 3)
                    {
                        grCount[3] = 1;
                    }
                    if (grScore[j] == 4)
                    {
                        grCount[4] = 1;
                    }
                    if (grScore[j] == 5)
                    {
                        grCount[5] = 1;
                    }
                    if (grScore[j] == 6)
                    {
                        grCount[6] = 1;
                    }
                    if (grScore[j] == 7)
                    {
                        grCount[7] = 1;
                    }
                    if (grScore[j] == 8)
                    {
                        grCount[8] = 1;
                    }
                    if (grScore[j] == 9)
                    {
                        grCount[9] = 1;
                    }
                    if (grScore[j] == 10)
                    {
                        grCount[10] = 1;
                    }
                    if (grScore[j] == 11)
                    {
                        grCount[11] = 1;
                    }
                    if (grScore[j] == 12)
                    {
                        grCount[12] = 1;
                    }
                }
            }

            SQLInsertCutOff = "Insert into PercentilesByGrade ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
            SQLInsertCutOffVal = ")Values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + "," + ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + lastScore + ""; // Year

            switch (contestType)
            {
                case "MB1":
                    pc[1] = (float)grCount[1] / grTCount[1];
                    dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                    //grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
                    pcile[1] = (float)grCCount[1] / grTCount[1];
                    dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                    dr["Gr1cnt"] = grCount[1];
                    dr["Gr1cum"] = grCCount[1];
                    pc[2] = (float)grCount[2] / grTCount[2];
                    dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                    //grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
                    pcile[2] = (float)grCCount[2] / grTCount[2];
                    dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                    dr["Gr2cnt"] = grCount[2];
                    dr["Gr2cum"] = grCCount[2];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];

                    break;
                case "MB2":
                case "EW1":
                    pc[3] = (float)grCount[3] / grTCount[3];
                    dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                    //grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[0];
                    aryCCount[0] -= grCount[3];
                    pcile[3] = (float)grCCount[3] / grTCount[3];
                    dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                    dr["Gr3cnt"] = grCount[3];
                    dr["Gr3cum"] = grCCount[3];
                    pc[4] = (float)grCount[4] / grTCount[4];
                    dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                    //grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[1];
                    aryCCount[1] -= grCount[4];
                    pcile[4] = (float)grCCount[4] / grTCount[4];
                    dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                    dr["Gr4cnt"] = grCount[4];
                    dr["Gr4cum"] = grCCount[4];
                    pc[5] = (float)grCount[5] / grTCount[5];
                    dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                    //grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[2];
                    aryCCount[2] -= grCount[5];
                    pcile[5] = (float)grCCount[5] / grTCount[5];
                    dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                    dr["Gr5cnt"] = grCount[5];
                    dr["Gr5cum"] = grCCount[5];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

                    break;
                case "MB3":
                case "EW2":
                case "PS1":
                    pc[6] = (float)grCount[6] / grTCount[6];
                    dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                    //grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[0];
                    aryCCount[0] -= grCount[6];
                    pcile[6] = (float)grCCount[6] / grTCount[6];
                    dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                    dr["Gr6cnt"] = grCount[6];
                    dr["Gr6cum"] = grCCount[6];
                    pc[7] = (float)grCount[7] / grTCount[7];
                    dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                    //grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[1];
                    aryCCount[1] -= grCount[7];
                    pcile[7] = (float)grCCount[7] / grTCount[7];
                    dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                    dr["Gr7cnt"] = grCount[7];
                    dr["Gr7cum"] = grCCount[7];
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[2];
                    aryCCount[2] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                    break;
                case "MB4":
                    pc[9] = (float)grCount[9] / grTCount[9];
                    dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                    //grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
                    pcile[9] = (float)grCCount[9] / grTCount[9];
                    dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                    dr["Gr9cnt"] = grCount[9];
                    dr["Gr9cum"] = grCCount[9];
                    pc[10] = (float)grCount[10] / grTCount[10];
                    dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                    //grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
                    pcile[10] = (float)grCCount[10] / grTCount[10];
                    dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                    dr["Gr10cnt"] = grCount[10];
                    dr["Gr10cum"] = grCCount[10];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

                    break;

                case "JSB":
                case "JSC":
                case "JGB":
                    pc[1] = (float)grCount[1] / grTCount[1];
                    dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                    //grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
                    pcile[1] = (float)grCCount[1] / grTCount[1];
                    dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                    dr["Gr1cnt"] = grCount[1];
                    dr["Gr1cum"] = grCCount[1];
                    pc[2] = (float)grCount[2] / grTCount[2];
                    dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                    //grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
                    pcile[2] = (float)grCCount[2] / grTCount[2];
                    dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                    dr["Gr2cnt"] = grCount[2];
                    dr["Gr2cum"] = grCCount[2];
                    pc[3] = (float)grCount[3] / grTCount[3];
                    dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                    //grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[2];
                    aryCCount[2] -= grCount[3];
                    pcile[3] = (float)grCCount[3] / grTCount[3];
                    dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                    dr["Gr3cnt"] = grCount[3];
                    dr["Gr3cum"] = grCCount[3];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

                    break;
                case "ISC":
                    pc[4] = (float)grCount[4] / grTCount[4];
                    dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                    //grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
                    pcile[4] = (float)grCCount[4] / grTCount[4];
                    dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                    dr["Gr4cnt"] = grCount[4];
                    dr["Gr4cum"] = grCCount[4];
                    pc[5] = (float)grCount[5] / grTCount[5];
                    dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                    //grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
                    pcile[5] = (float)grCCount[5] / grTCount[5];
                    dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                    dr["Gr5cnt"] = grCount[5];
                    dr["Gr5cum"] = grCCount[5];
                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
                    break;
                case "SSC":
                    pc[6] = (float)grCount[6] / grTCount[6];
                    dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                    //grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[0];
                    aryCCount[0] -= grCount[6];
                    pcile[6] = (float)grCCount[6] / grTCount[6];
                    dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                    dr["Gr6cnt"] = grCount[6];
                    dr["Gr6cum"] = grCCount[6];
                    pc[7] = (float)grCount[7] / grTCount[7];
                    dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                    //grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[1];
                    aryCCount[1] -= grCount[7];
                    pcile[7] = (float)grCCount[7] / grTCount[7];
                    dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                    dr["Gr7cnt"] = grCount[7];
                    dr["Gr7cum"] = grCCount[7];
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[2];
                    aryCCount[2] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];
                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];
                    break;
                case "SSB":
                case "SGB":
                    pc[4] = (float)grCount[4] / grTCount[4];
                    dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                    //grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
                    pcile[4] = (float)grCCount[4] / grTCount[4];
                    dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                    dr["Gr4cnt"] = grCount[4];
                    dr["Gr4cum"] = grCCount[4];
                    pc[5] = (float)grCount[5] / grTCount[5];
                    dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                    //grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
                    pcile[5] = (float)grCCount[5] / grTCount[5];
                    dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                    dr["Gr5cnt"] = grCount[5];
                    dr["Gr5cum"] = grCCount[5];
                    pc[6] = (float)grCount[6] / grTCount[6];
                    dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                    //grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[2];
                    aryCCount[2] -= grCount[6];
                    pcile[6] = (float)grCCount[6] / grTCount[6];
                    dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                    dr["Gr6cnt"] = grCount[6];
                    dr["Gr6cum"] = grCCount[6];
                    pc[7] = (float)grCount[7] / grTCount[7];
                    dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                    //grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[3];
                    aryCCount[3] -= grCount[7];
                    pcile[7] = (float)grCCount[7] / grTCount[7];
                    dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                    dr["Gr7cnt"] = grCount[7];
                    dr["Gr7cum"] = grCCount[7];
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[4];
                    aryCCount[4] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                    break;
                case "JVB":
                    pc[1] = (float)grCount[1] / grTCount[1];
                    dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                    //grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
                    pcile[1] = (float)grCCount[1] / grTCount[1];
                    dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                    dr["Gr1cnt"] = grCount[1];
                    dr["Gr1cum"] = grCCount[1];
                    pc[2] = (float)grCount[2] / grTCount[2];
                    dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                    //grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
                    pcile[2] = (float)grCCount[2] / grTCount[2];
                    dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                    dr["Gr2cnt"] = grCount[2];
                    dr["Gr2cum"] = grCCount[2];
                    pc[3] = (float)grCount[3] / grTCount[3];
                    dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                    //grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[2];
                    aryCCount[2] -= grCount[3];
                    pcile[3] = (float)grCCount[3] / grTCount[3];
                    dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                    dr["Gr3cnt"] = grCount[3];
                    dr["Gr3cum"] = grCCount[3];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

                    break;
                case "IVB":
                    pc[4] = (float)grCount[4] / grTCount[4];
                    dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                    //grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
                    pcile[4] = (float)grCCount[4] / grTCount[4];
                    dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                    dr["Gr4cnt"] = grCount[4];
                    dr["Gr4cum"] = grCCount[4];
                    pc[5] = (float)grCount[5] / grTCount[5];
                    dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                    //grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
                    pcile[5] = (float)grCCount[5] / grTCount[5];
                    dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                    dr["Gr5cnt"] = grCount[5];
                    dr["Gr5cum"] = grCCount[5];
                    pc[6] = (float)grCount[6] / grTCount[6];
                    dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                    //grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[2];
                    aryCCount[2] -= grCount[6];
                    pcile[6] = (float)grCCount[6] / grTCount[6];
                    dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                    dr["Gr6cnt"] = grCount[6];
                    dr["Gr6cum"] = grCCount[6];
                    pc[7] = (float)grCount[7] / grTCount[7];
                    dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                    //grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[3];
                    aryCCount[3] -= grCount[7];
                    pcile[7] = (float)grCCount[7] / grTCount[7];
                    dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                    dr["Gr7cnt"] = grCount[7];
                    dr["Gr7cum"] = grCCount[7];
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[4];
                    aryCCount[4] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

                    break;
                case "SVB":
                    pc[9] = (float)grCount[9] / grTCount[9];
                    dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                    //grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
                    pcile[9] = (float)grCCount[9] / grTCount[9];
                    dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                    dr["Gr9cnt"] = grCount[9];
                    dr["Gr9cum"] = grCCount[9];
                    pc[10] = (float)grCount[10] / grTCount[10];
                    dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                    //grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
                    pcile[10] = (float)grCCount[10] / grTCount[10];
                    dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                    dr["Gr10cnt"] = grCount[10];
                    dr["Gr10cum"] = grCCount[10];
                    pc[11] = (float)grCount[11] / grTCount[11];
                    dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                    //grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[2];
                    aryCCount[2] -= grCount[11];
                    pcile[11] = (float)grCCount[11] / grTCount[11];
                    dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                    dr["Gr11cnt"] = grCount[11];
                    dr["Gr11cum"] = grCCount[11];
                    if (grTCount[12] == 0)
                    {
                        pc[12] = 0;
                        dr["Gr12%"] = "";
                        dr["Gr12%tile"] = "";
                    }
                    else
                    {
                        pc[12] = (float)grCount[12] / grTCount[12];
                        dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                        //grCCount[12] += grCount[12];
                        grCCount[12] = aryCCount[3];
                        aryCCount[3] -= grCount[12];
                        pcile[12] = (float)grCCount[12] / grTCount[12];
                        dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                    }
                    dr["Gr12cnt"] = grCount[12];
                    dr["Gr12cum"] = grCCount[12];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

                    break;
                case "EW3":
                case "PS3":
                    pc[9] = (float)grCount[9] / grTCount[9];
                    dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                    //grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
                    pcile[9] = (float)grCCount[9] / grTCount[9];
                    dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                    dr["Gr9cnt"] = grCount[9];
                    dr["Gr9cum"] = grCCount[9];
                    pc[10] = (float)grCount[10] / grTCount[10];
                    dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                    //grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
                    pcile[10] = (float)grCCount[10] / grTCount[10];
                    dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                    dr["Gr10cnt"] = grCount[10];
                    dr["Gr10cum"] = grCCount[10];
                    pc[11] = (float)grCount[11] / grTCount[11];
                    dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                    //grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[2];
                    aryCCount[2] -= grCount[11];
                    pcile[11] = (float)grCCount[11] / grTCount[11];
                    dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                    dr["Gr11cnt"] = grCount[11];
                    dr["Gr11cum"] = grCCount[11];
                    if (grTCount[12] == 0)
                    {
                        pc[12] = 0;
                        dr["Gr12%"] = "";
                        dr["Gr12%tile"] = "";
                    }
                    else
                    {
                        pc[12] = (float)grCount[12] / grTCount[12];
                        dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                        //grCCount[12] += grCount[12];
                        grCCount[12] = aryCCount[3];
                        aryCCount[3] -= grCount[12];
                        pcile[12] = (float)grCCount[12] / grTCount[12];
                        dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                    }
                    dr["Gr12cnt"] = grCount[12];
                    dr["Gr12cum"] = grCCount[12];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];
                    break;

                case "BB":
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[0];
                    aryCCount[0] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];
                    pc[9] = (float)grCount[9] / grTCount[9];
                    dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                    //grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[1];
                    aryCCount[1] -= grCount[9];
                    pcile[9] = (float)grCCount[9] / grTCount[9];
                    dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                    dr["Gr9cnt"] = grCount[9];
                    dr["Gr9cum"] = grCCount[9];
                    pc[10] = (float)grCount[10] / grTCount[10];
                    dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                    //grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[2];
                    aryCCount[2] -= grCount[10];
                    pcile[10] = (float)grCCount[10] / grTCount[10];
                    dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                    dr["Gr10cnt"] = grCount[10];
                    dr["Gr10cum"] = grCCount[10];
                    pc[11] = (float)grCount[11] / grTCount[11];
                    dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                    //grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[3];
                    aryCCount[3] -= grCount[11];
                    pcile[11] = (float)grCCount[11] / grTCount[11];
                    dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                    dr["Gr11cnt"] = grCount[11];
                    dr["Gr11cum"] = grCCount[11];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"] + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"];
                    break;
            }

            totalPercentile = (float)totalCumCount / aryTscore.Length;
            dr["TotalPercentile"] = String.Format("{0:P1}", Math.Round(totalPercentile, 3));
            dr["TotalCount"] = cCount;
            dr["TotalCumeCount"] = totalCumCount;

            SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,ContestPeriod,ContestDate,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + cCount + "," + totalCumCount + ",'" + ContestPeriod + "','" + ContestDays + "','" + Session["LoginID"] + "', GETDATE())";
            // SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + cCount + "," + totalCumCount + ",'" + Session["LoginID"] + "', GETDATE())";                                                                                                                                                                                                                              ] = ;

            dt2.Rows.Add(dr);
            //DataGrid1.Caption = contestType + " as of " + ddlYear.SelectedValue;// DateTime.Today.ToShortDateString();
            //DataGrid1.DataSource = dt2;
            //DataGrid1.DataBind();
            // DataGrid1.Visible = true;
            for (int s = StartGrade; s <= EndGrade; s++)
            {
                if (dr["Gr" + s + "%tile"].ToString() == "NaN")
                {
                    Contest_Flag = true;
                }
            }
            if (Contest_Flag == false)
            {
                if (Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, SQLExecString)) > 0)
                {
                    lblSuccess.Text = "Percentiles Calculation done for the Year " + ddlYear.SelectedValue;
                    SQLExecString = "";
                }
            }
        }
        catch (Exception ex)
        {
            // Response.Write(SQLExecString);
            // Response.Write(ex.ToString());
        }
    }

    void SortArrays(double[] aryTscore, double[] grScore)
    {
        int x = aryTscore.Length;
        int i;
        int j;
        double temp1, temp2;

        for (i = (x - 1); i >= 0; i--)
        {
            for (j = 1; j <= i; j++)
            {
                if (aryTscore[j - 1] > aryTscore[j])
                {
                    temp1 = aryTscore[j - 1];
                    aryTscore[j - 1] = aryTscore[j];
                    aryTscore[j] = temp1;
                    temp2 = grScore[j - 1];
                    grScore[j - 1] = grScore[j];
                    grScore[j] = temp2;
                }
            }
        }
    }
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        isExcel = 1;
        if (hdnIsCutOff.Value != "CutOff")
        {
            if (ddlYear.SelectedIndex == 0)
            {
                lblSuccess.Text = "Please Select ContestYear";
                return;
            }
            String FileName = "";
            FileName = FileName + "Percentiles_";
            if (ddlEvent.SelectedValue == "1")
            {
                FileName = FileName + "Nationals_";
            }
            else
            {
                FileName = FileName + "Regionals_";
            }
            if (ddlContest.SelectedIndex == 0)
            {
                FileName = FileName + "All_Contests_";
            }
            else
            {
                FileName = FileName + ddlContest.SelectedItem.Text + "_";
            }
            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                FileName = FileName + "All";
            }
            else
            {
                FileName = FileName + ddlcontestdate.SelectedItem.Value;
            }
            FileName = FileName + "_" + ddlYear.SelectedValue;

            if (ddlContest.SelectedIndex != 0)
            {
                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment;filename=" + FileName + ".xls");
                Response.Charset = "";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
                DGPercentiles.RenderControl(hw);
                //DataGrid1.RenderControl(hw);
                Response.Write(sw.ToString());
                Response.End();
            }
            else
            {
                IWorkbook xlWorkBook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("CutOffScoreSheet\\PercentilesByGrade.xls"));
                IWorksheet Sheet1;
                //Sheet1 = xlWorkBook.Worksheets[1];
                DataTable dt2 = new DataTable();
                String SQLPercent;
                DataSet ds = new DataSet();
                for (int i = 1; i <= xlWorkBook.Worksheets.Count; i++)
                {
                    Sheet1 = xlWorkBook.Worksheets[i];
                    SQLPercent = "";
                    // Sheet1.Range[i, 1].Value = Sheet1.Name ;

                    try
                    {
                        SQLPercent = "Select ContestYear,EventId,ProductCode,TotalScore,";

                        switch (Sheet1.Name)
                        {
                            case "MB1":
                                SQLPercent = SQLPercent + "[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,";
                                break;
                            case "MB2":
                                SQLPercent = SQLPercent + "[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,";
                                break;
                            case "MB3":
                                SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "MB4":
                                SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,";
                                break;
                            case "JSB":
                            case "JSC":
                                SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                                break;
                            case "ISC":
                                SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,";
                                break;
                            case "SSC":
                                SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "SSB":
                                SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "JVB":
                                SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                                break;
                            case "IVB":
                                SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "SVB":
                                SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                                break;
                            case "JGB":
                                SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                                break;
                            case "SGB":
                                SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "EW1":
                                SQLPercent = SQLPercent + "[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,";
                                break;
                            case "EW2":
                                SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "EW3":
                                SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                                break;
                            case "PS1":
                                SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                                break;
                            case "PS3":
                                SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                                break;
                            case "BB":
                                SQLPercent = SQLPercent + "[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,";
                                break;
                        }

                        SQLPercent = SQLPercent + "TotalPercentile,Total_Count,Total_cume_Count,ContestPeriod,ContestDate From PercentilesByGrade Where EventID =" + ddlEvent.SelectedValue + " and ContestYear=" + ddlYear.SelectedValue + " and ProductCode='" + Sheet1.Name + "' and ContestPeriod in(" + ddlcontestdate.SelectedValue + ") Order by ContestPeriod,productGroupID, ProductID,TotalScore";

                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLPercent);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            dt2 = ds.Tables[0];
                            // storing header part in Excel
                            for (int l = 0; l < dt2.Columns.Count; l++)
                            {
                                Sheet1.Cells[1, l + 1].Value = dt2.Columns[l].ColumnName;
                            }
                            //storing data to excel from table in Coressponding Rows and Columns
                            for (int m = 0; m < dt2.Rows.Count; m++)//  ds.Tables[0].Rows.Count-2
                            {
                                for (int j = 0; j < dt2.Columns.Count; j++) // - 2 //ds.Tables[0].Columns.Count
                                {
                                    Sheet1.Cells[m + 2, j + 1].Value = dt2.Rows[m][j].ToString();//ds.Tables[0]
                                }
                            }
                        }
                        else
                        {
                            // Sheet1.Cells[2,2].Value = "No data present for " + Sheet1.Name;
                            Sheet1.Visible = XlSheetVisibility.xlSheetHidden;
                            //return;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.ToString());
                    }
                }
                xlWorkBook.Worksheets[1].Activate();
                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                xlWorkBook.SaveAs(Response.OutputStream);
                Response.End();
            }
        }
        else
        {
            exportToExcelCutOffScore();
        }
    }

    public void exportToExcelCutOffScore()
    {
        fillPercentileByGrade();
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;

        string mark1 = hdnGrade1mark.Value;
        string filename = "CutOffScoreByPercentiles_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter stringWrite = new StringWriter();
        HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        ltrSchedulePostingDay2.RenderControl(htmlWrite);


        Response.Write(stringWrite.GetStringBuilder().ToString());

        Response.End();
    }
    //protected void btnExport_Click(object sender, EventArgs e)
    //{

    //    if (ddlcontestdate.SelectedIndex != ddlcontestdate.Items.Count - 1)//ddlContest.SelectedIndex != 0 ||
    //    {
    //        lblSuccess.Text = "Please select All option for Contestdates to find CutOff Scores";
    //        return;
    //    }

    //    DataSet ds = new DataSet();
    //    DataSet ds1 = new DataSet();
    //    DataSet ds2 = new DataSet();
    //    String FileName;
    //    if (ddlContest.SelectedIndex == 0)
    //    {
    //        ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select distinct cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId from PercentilesByGrade cs Inner Join Product p On cs.ProductCode = p.ProductCode and cs.EventID = p.EventId where cs.ProductCode is not null and ContestYear=" + ddlYear.SelectedValue + " and cs.eventId =" + ddlEvent.SelectedValue + " Group By cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId Order by p.ProductId "); //Year
    //        FileName = ddlContest.SelectedItem.Text + "_Contests";
    //    }
    //    else
    //    {
    //        ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select distinct cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId from PercentilesByGrade cs Inner Join Product p On cs.ProductCode = p.ProductCode and cs.EventID = p.EventId where cs.ProductCode is not null and ContestYear=" + ddlYear.SelectedValue + " and cs.eventId =" + ddlEvent.SelectedValue + " and cs.ProductID=" + ddlContest.SelectedValue + " Group By cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId Order by p.ProductId "); //Year
    //        FileName = ddlContest.SelectedItem.Text;
    //    }
    //    IWorkbook xlWorkBook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("CutOffScoreSheet\\CutoffScoresByPercentiles.xls"));
    //    IWorksheet Sheet1;
    //    Sheet1 = xlWorkBook.Worksheets[1];

    //    double x, y;
    //    double Ptile_Val1, Ptile_Val2, Ptile_1, Ptile_2 = 0.0, Ptile_Sheet, Ptile_Sheet1;
    //    double diff = 0.0, diff1 = 0.0;

    //    int Sht_Col = 2;
    //    int Sht_Row = 11;
    //    int Start_Row = 10;
    //    int s, Col1, Col2, TScore, p_flag, flag_ptile;
    //    int k = 7;

    //    String TScoreValues, Grade_count, Contest_type;

    //    try
    //    {
    //        for (int q = 0; q < ds1.Tables[0].Rows.Count; q++)
    //        {
    //            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select * From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + "and ProductCode='" + ds1.Tables[0].Rows[q]["ProductCode"].ToString() + "' and EventID=" + ddlEvent.SelectedValue + " Order by TotalScore desc"); //Year
    //            Contest_type = ds1.Tables[0].Rows[q]["ProductCode"].ToString();
    //            Sheet1.Range[Start_Row, 1].Value = ds1.Tables[0].Rows[q]["ProductCode"].ToString();
    //            Sheet1.Range[Start_Row, 2].Value = "Total";
    //            Grade_count = "";
    //            TScoreValues = "0";
    //            TScore = 0;
    //            StartGrade = Convert.ToInt32(ds1.Tables[0].Rows[q]["Start_Grade"].ToString());
    //            EndGrade = Convert.ToInt32(ds1.Tables[0].Rows[q]["End_Grade"].ToString());
    //            for (int row = StartGrade; row <= EndGrade; row++)
    //            {
    //                Grade_count = Grade_count + "Gr" + row + "_cum +";
    //            }
    //            Grade_count = Grade_count.TrimEnd('+');

    //            for (int st = StartGrade; st <= EndGrade; st++)
    //            {
    //                Sheet1.Range[Sht_Row, Sht_Col].Value = st;

    //                Col1 = 3;
    //                Col2 = 4;
    //                p_flag = 0;
    //                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //                {
    //                    //   j = i + 1;

    //                    Ptile_Val1 = (Convert.ToDouble(ds.Tables[0].Rows[i]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));

    //                    if (Ptile_Val1 <= 30)
    //                    {
    //                        //if (j < ds.Tables[0].Rows.Count)
    //                        //{
    //                        if (Ptile_Val1 > 0.0 && p_flag < 1)//&& Ptile_Val1 < 1 // To Find the First Value nearest to Percentile value 1
    //                        {
    //                            Ptile_Val2 = (Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));

    //                            if (Ptile_Val2 >= 1)
    //                            {
    //                                if (Ptile_Val1 != Ptile_Val2)
    //                                {
    //                                    x = Math.Abs(1 - Ptile_Val1);//(Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'))));
    //                                    y = Math.Abs(1 - Ptile_Val2);//(Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr1_Ptile"].ToString().TrimEnd('%'))));

    //                                    if (x < y)
    //                                    {
    //                                        //   Sheet1.Range[k, Col1].Value = Ptile_Val1;// Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'));
    //                                        //  Sheet1.Range[k, Col2].Value = Ptile_Val1;
    //                                        Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
    //                                        Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
    //                                    }
    //                                    else if (y < x)
    //                                    {
    //                                        // Sheet1.Range[k, Col1].Value = Ptile_Val2;// Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr1_Ptile"].ToString().TrimEnd('%'));
    //                                        // Sheet1.Range[k, Col2].Value = Ptile_Val2;
    //                                        Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i + 1]["TotalScore"];
    //                                        Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i + 1]["Gr" + st + "_cum"];
    //                                        i = i + 1;
    //                                    }
    //                                    else
    //                                    {
    //                                        //  Sheet1.Range[k, Col1].Value = Ptile_Val1;// Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'));
    //                                        //   Sheet1.Range[k, Col2].Value = Ptile_Val1;
    //                                        Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i + 1]["TotalScore"];
    //                                        Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i + 1]["Gr" + st + "_cum"];
    //                                        i = i + 1;
    //                                    }

    //                                    TScore = Convert.ToInt32(Sheet1.Range[Sht_Row, Col1].Value);

    //                                    p_flag = p_flag + 1;
    //                                }
    //                            }
    //                        }

    //                        else if (p_flag == 1)
    //                        {
    //                            TScore = TScore - 1;
    //                            Col1 = Col1 + 2;
    //                            Col2 = Col2 + 2;
    //                            flag_ptile = 0;

    //                            if (TScore > Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
    //                            {
    //                                if (SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From PercentilesByGrade Where ProductCode='" + Contest_type + "' and ContestYear= " + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and TotalScore= " + TScore + "") != "1")//Year
    //                                {
    //                                    TScore = TScore - 1;
    //                                }
    //                            }

    //                            TScoreValues = TScoreValues + "," + Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString());

    //                            for (int l = 1; l < 12; l++)
    //                            {
    //                                if (TScore == Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
    //                                {
    //                                    Ptile_1 = (Convert.ToDouble(ds.Tables[0].Rows[i]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));//Math.Round
    //                                    if (i < ds.Tables[0].Rows.Count - 1)
    //                                    {
    //                                        Ptile_2 = (Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));//Math.Round
    //                                    }
    //                                    Ptile_Sheet = (Convert.ToDouble(Sheet1.Range[k, Col1].Value));//Math.Round
    //                                    Ptile_Sheet1 = (Convert.ToDouble(Sheet1.Range[k, Col1 + 2].Value));// Math.Round

    //                                    if (Ptile_1 != Ptile_2)
    //                                    {
    //                                        if (Ptile_Sheet <= Ptile_1 && Ptile_1 <= Ptile_Sheet1)
    //                                        {
    //                                            diff = Math.Abs(Ptile_Sheet - Ptile_1);
    //                                            diff1 = Math.Abs(Ptile_Sheet1 - Ptile_1);

    //                                            if (diff <= diff1)
    //                                            {
    //                                                //if (Sheet1.Range[Sht_Row, Col1].Value != "" && Sheet1.Range[Sht_Row, Col2].Value !="")
    //                                                //{

    //                                                Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
    //                                                Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
    //                                                //  }
    //                                                // else
    //                                                // {
    //                                                //     Col1 = Col1 + 2;
    //                                                //     Col2 = Col2 + 2;
    //                                                //     Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
    //                                                //    Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];

    //                                                //}
    //                                                flag_ptile = 1;
    //                                                //goto breakfor;
    //                                            }
    //                                            else if (diff > diff1)
    //                                            {
    //                                                Col1 = Col1 + 2;
    //                                                Col2 = Col2 + 2;
    //                                                Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
    //                                                Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
    //                                                flag_ptile = 1;
    //                                                //goto breakfor;
    //                                            }

    //                                            goto breakfor;
    //                                        }
    //                                        // }
    //                                        else if (Ptile_1 <= Ptile_Sheet)
    //                                        {
    //                                            Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
    //                                            Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
    //                                            goto breakfor;
    //                                        }

    //                                        else
    //                                        {
    //                                            Col1 = Col1 + 2;
    //                                            Col2 = Col2 + 2;
    //                                        }
    //                                    }
    //                                    else
    //                                    {
    //                                        break;
    //                                    }
    //                                }
    //                                //else if (TScore > Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
    //                                //{
    //                                //    if (SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From PercentilesByGrade Where TotalScore= " + TScore + "") == 0)
    //                                //    {
    //                                //        TScore = TScore - 1;
    //                                //    }
    //                                //}
    //                            }
    //                        breakfor:
    //                            flag_ptile = 0;
    //                            // Col1 = Col1 + 2; //Col1;//
    //                            // Col2 = Col2 + 2;//Col2;//

    //                        }

    //                        //}

    //                    }

    //                }
    //                Sht_Row++;
    //            }

    //            ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select (" + Grade_count + ") as Count,TotalScore from  [PercentilesByGrade] Where ProductCode= '" + ds1.Tables[0].Rows[q]["ProductCode"].ToString() + "' and TotalScore in (" + TScoreValues + ") and EventID=" + ddlEvent.SelectedValue + " and ContestYear =" + ddlYear.SelectedValue + "Order by TotalScore desc");
    //            s = 0;

    //            for (int m = 3; m <= 26; m = m + 2)
    //            {
    //                if (s < ds2.Tables[0].Rows.Count)
    //                {
    //                    Sheet1.Range[Start_Row, m].Value = ds2.Tables[0].Rows[s]["TotalScore"];
    //                    Sheet1.Range[Start_Row, m + 1].Value = ds2.Tables[0].Rows[s]["Count"];
    //                    s = s + 1;
    //                }
    //            }
    //            Start_Row = Sht_Row++;

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        //Response.Write(ex.ToString());
    //    }

    //    Response.Clear();
    //    Response.ContentType = "application/vnd.ms-excel";
    //    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
    //    Response.AddHeader("Content-Disposition", "attachment;filename=CutOffScoresByPercentile_" + FileName + "_All_" + ddlYear.SelectedValue + ".xls");
    //    xlWorkBook.SaveAs(Response.OutputStream);
    //    Response.End();
    //}
    string ConnectionString = "ConnectionString";
    protected void btnExport_Click(object sender, EventArgs e)
    {



    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblSuccess.Text = "";
        lblError.Text = "";
        LoadContestDates();

    }
    void LoadContestDates()
    {
        String SQLstr = "SELECT  *,Row_number() over(order by temp.ContestDate1,temp.ContestDate2)as ContestPeriod FROM (";
        SQLstr = SQLstr + " SELECT  Distinct C.ContestDate as ContestDate1,C1.ContestDate as ContestDate2,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + ";
        SQLstr = SQLstr + " CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39) as ContestDates , Convert(Varchar(15),DATENAME(MM, C.ContestDate) ";
        SQLstr = SQLstr + " + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(15),DATENAME(MM, C1.ContestDate) + ' ' + ";
        SQLstr = SQLstr + " CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDate FROM Contest ";
        SQLstr = SQLstr + " C Inner join Contest C1 On C1.Contest_Year = C.Contest_Year Where C.Contest_Year=" + ddlYear.SelectedValue + " and c.eventid=" + ddlEvent.SelectedValue + " and ";
        SQLstr = SQLstr + " cast(DATEDIFF(Day,C.ContestDate,C1.ContestDate) as int) = 1) temp order by temp.ContestDate1,temp.ContestDate2";

        //String SQLstr = "SELECT Distinct C.ContestDate,C1.ContestDate,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39)  as ContestDates , ";
        //SQLstr = SQLstr + " Convert(Varchar(10),DATENAME(MM, C.ContestDate) + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(10),DATENAME(MM, C1.ContestDate) + ' ' + CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDays,";
        //SQLstr = SQLstr + " DATEDIFF(Day,C.ContestDate,C1.ContestDate) FROM Contest C Inner join Contest C1 On C1.Contest_Year = C.Contest_Year Where C.Contest_Year=" + ddlYear.SelectedValue + " and c.eventid=" + ddlEvent.SelectedValue + " and cast(DATEDIFF(Day,C.ContestDate,C1.ContestDate) as int) = 1 order by C.ContestDate,C1.ContestDate";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr);
        ddlcontestdate.Items.Clear();

        int Count = ds.Tables[0].Rows.Count;
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlcontestdate.DataSource = ds;
            ddlcontestdate.DataTextField = "ContestDate".ToString();
            ddlcontestdate.DataValueField = "ContestPeriod".ToString();
            // ddlcontestdate.DataTextField = string.Format("Mmm-dd");
            int i;
            string contestDates = string.Empty;
            for (i = 0; i < Count; i++)
            {
                ddlcontestdate.Items.Insert(i, new ListItem(ds.Tables[0].Rows[i]["ContestDate"].ToString(), (Convert.ToInt32(ds.Tables[0].Rows[i]["ContestPeriod"]).ToString())));
                contestDates += ds.Tables[0].Rows[i]["ContestDate"].ToString() + ",";

            }
            if (ddlEvent.SelectedValue == "2")
            {
                ddlcontestdate.Items.Insert(i, new ListItem("All", (Convert.ToInt32(ds.Tables[0].Rows[i - 1]["ContestPeriod"]) + 1).ToString()));
                ddlcontestdate.SelectedIndex = Count;
            }

            // ddlcontestdate.Items.Insert((Convert.ToInt32(ds.Tables[0].Rows[2]["ContestPeriod"]) + 1), new ListItem("All", (Convert.ToInt32(ds.Tables[0].Rows[Count - 1]["ContestPeriod"]) + 1).ToString()));
            // ddlcontestdate.SelectedIndex = Count;
            contestDates = contestDates.TrimEnd(',');
            hdnContestDates.Value = contestDates;
        }

        // For i = 0 To NRooms - 1
        //    ddlRoom.Items.Insert(i, New ListItem(i + 1, i + 1))
        //Next
        //ddlRoom.Items.Insert(i, New ListItem("ALL", "ALL"))


    }

    protected void ddlPercentile_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblSuccess.Text = "";
        lblError.Text = "";
        hdnIsCutOff.Value = "";
        if (ddlPercentile.SelectedValue == "1" || ddlPercentile.SelectedValue == "2")
        {
            chkAllContest.Visible = true; //trContest.Visible = true;
            btnSave.Enabled = false;
            btnExport.Visible = false;
            tdStart.Visible = true;
            tdEnd.Visible = true;
            tdStart.Visible = false;
            tdEnd.Visible = false;
        }
        else if (ddlPercentile.SelectedValue == "4")
        {
            tdStart.Visible = false;
            tdEnd.Visible = false;
        }
        else
        {
            chkAllContest.Visible = false;
            btnSave.Enabled = true;
            btnExport.Visible = false;
            ddlContest.Enabled = true;
            ddlcontestdate.Enabled = true;


            tdStart.Visible = true;
            tdEnd.Visible = true;

        }
    }

    protected void BtnContinue_Click(object sender, EventArgs e)
    {
        hdnIsCutOff.Value = "";
        lblSuccess.Text = "";
        lblError.Text = "";
        int PercentId_Max, PercentID_Contest;

        if (ddlPercentile.SelectedIndex == 0)
        {
            hdnIsSave.Value = "";
            dvScheduleReportingDay2.Visible = false;
            lblSuccess.Text = "Please select Percentiles Option";
            return;
        }
        //else if (ddlEvent.SelectedIndex == 0)
        //{
        //    lblSuccess.Text = "Please select Event";
        //    return;
        //}
        else if (ddlYear.SelectedIndex == 0)
        {

            lblSuccess.Text = "Please select Contest Year";
            return;
        }

        if (ddlPercentile.SelectedValue == "1")
        {
            hdnIsSave.Value = "";
            dvScheduleReportingDay2.Visible = false;
            if (chkAllContest.Checked == true)
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (1,2,3,4) and EventId=" + ddlEvent.SelectedValue)) > 0)
                {

                    DuplicateFlag = true;
                    //tblDuplicate.Visible = true;

                    PercentID_Contest = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (1,2,3,4) and EventId=" + ddlEvent.SelectedValue));
                    PercentId_Max = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade"));

                    if (PercentID_Contest == PercentId_Max)
                    {
                        InsertPercentiles(DuplicateFlag, true);
                    }
                    else
                    {
                        //DuplicateFlag = true;
                        tblDuplicate.Visible = true;
                    }
                }
                else
                {
                    tblDuplicate.Visible = false;
                    CalculatePercentiles();
                }
            }
            else
            {
                //if (ddlContest.SelectedIndex ==0)
                //{
                //    lblSuccess.Text = "Please select Contest";
                //    return;
                //}
                if (ddlContest.SelectedItem.Text == "All")
                {
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue)) > 0)
                    {
                        DuplicateFlag = true;
                        //tblDuplicate.Visible = true;
                        PercentID_Contest = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue));
                        PercentId_Max = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade"));

                        if (PercentID_Contest == PercentId_Max)
                        {
                            InsertPercentiles(DuplicateFlag, true);
                        }
                        else
                        {
                            //DuplicateFlag = true;
                            tblDuplicate.Visible = true;
                        }
                    }
                    else
                    {
                        tblDuplicate.Visible = false;
                        CalculatePercentiles();
                    }
                }
                else
                {
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ProductCode in ('" + ddlContest.SelectedItem.Text + "') and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue)) > 0)
                    {
                        DuplicateFlag = true;
                        //tblDuplicate.Visible = true;

                        PercentID_Contest = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ProductCode in ('" + ddlContest.SelectedItem.Text + "') and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue));
                        PercentId_Max = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select MAX(PercentID) From PercentilesByGrade"));

                        if (PercentID_Contest == PercentId_Max)
                        {
                            InsertPercentiles(DuplicateFlag, true);
                        }
                        else
                        {
                            //DuplicateFlag = true;
                            tblDuplicate.Visible = true;
                        }
                    }
                    else
                    {
                        tblDuplicate.Visible = false;
                        CalculatePercentiles();
                    }
                }
                //ReadAllScores(ddlContest.SelectedItem.Text,ddlcontestdate.SelectedItem.Text,Convert.ToInt32(ddlcontestdate.SelectedItem.Value));
            }

            DGPercentiles.Visible = false;

        }
        else if (ddlPercentile.SelectedValue == "2")
        {
            hdnIsSave.Value = "";
            dvScheduleReportingDay2.Visible = false;
            if (ddlContest.SelectedIndex == 0)
            {
                lblSuccess.Text = "Contests can be viewed only one at a time";
                DGPercentiles.Visible = false;
                dvScheduleReportingDay2.Visible = false;
            }
            else
            {
                dvScheduleReportingDay2.Visible = false;
                LoadDGPercentiles();
            }
        }
        else if (ddlPercentile.SelectedValue == "3")
        {
            hdnIsCutOff.Value = "CutOff";
            hdnGrade1mark.Value = "";
            hdnGrade2mark.Value = "";
            hdnGrade3mark.Value = "";
            hdnGrade4mark.Value = "";
            hdnGrade5mark.Value = "";
            hdnGrade6mark.Value = "";
            hdnGrade7mark.Value = "";
            hdnGrade8mark.Value = "";

            hdnTotalScore1.Value = "";
            hdnTotalScore2.Value = "";
            hdnTotalScore3.Value = "";
            hdnTotalScore4.Value = "";
            hdnTotalScore5.Value = "";
            hdnTotalScore6.Value = "";
            hdnTotalScore7.Value = "";
            hdnTotalScore8.Value = "";
            hdnGrade1Index.Value = "";
            hdnGrade2Index.Value = "";
            hdnGrade3Index.Value = "";
            hdnTotlaInvitees.Value = "";
            hdnTotalList.Value = "";
            ddlListType.Enabled = false;
            isExcel = 0;
            tdStart.Visible = true;
            tdEnd.Visible = true;
            DGPercentiles.Visible = false;
            if (ddlStartingScore.SelectedValue != "Select" && ddlEndingScore.SelectedValue != "Select" && ddlContest.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlStartingScore.SelectedValue) < Convert.ToInt32(ddlEndingScore.SelectedValue))
                {
                    lblError.Text = "";
                    hdnIsSave.Value = "1";
                    btnExportToExcelAll.Style.Add("display", "block");
                    fillPercentileByGrade();

                    dvScheduleReportingDay2.Visible = true;
                    string grade1CUM = string.Empty;
                    string grade2CUM = string.Empty;
                    string grade3CUM = string.Empty;
                    string grade4CUM = string.Empty;
                    string grade5CUM = string.Empty;
                    string grade6CUM = string.Empty;
                    string grade7CUM = string.Empty;
                    string grade8CUM = string.Empty;

                    string grade1Total = string.Empty;
                    string grade2Total = string.Empty;
                    string grade3Total = string.Empty;
                    string grade4Total = string.Empty;
                    string grade5Total = string.Empty;
                    string grade6Total = string.Empty;
                    string grade7Total = string.Empty;
                    string grade8Total = string.Empty;


                    string totListScoreCS1 = string.Empty;
                    string totListScoreCS2 = string.Empty;
                    string totListScoreCS3 = string.Empty;

                    string totPriorityScoreCS1 = string.Empty;
                    string totPriorityScoreCS2 = string.Empty;
                    string totPriorityScoreCS3 = string.Empty;

                    string cmdText = string.Format("select PriorityCS,TotalCS,Grade from CutOffScores where {0}={3} and {1}={4} and {2}={5}", "ProductGroupID", "ProductID", "ContestYear", hdnProductGroupID.Value, hdnProductID.Value, ddlYear.SelectedValue);
                    DataSet ds = new DataSet();
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if (dr["Grade"] != null)
                                {
                                    if (dr["Grade"].ToString() == "1")
                                    {
                                        grade1CUM = dr["PriorityCS"].ToString();
                                        grade1Total = dr["TotalCS"].ToString();
                                        //totListScoreCS1 = dr["TotalScoreCS"].ToString();
                                        //totPriorityScoreCS1 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "2")
                                    {
                                        grade2CUM = dr["PriorityCS"].ToString();
                                        grade2Total = dr["TotalCS"].ToString();
                                        //totListScoreCS2 = dr["TotalScoreCS"].ToString();
                                        //totPriorityScoreCS2 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "3")
                                    {
                                        grade3CUM = dr["PriorityCS"].ToString();
                                        grade3Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "4")
                                    {
                                        grade4CUM = dr["PriorityCS"].ToString();
                                        grade4Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "5")
                                    {
                                        grade5CUM = dr["PriorityCS"].ToString();
                                        grade5Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "6")
                                    {
                                        grade6CUM = dr["PriorityCS"].ToString();
                                        grade6Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "7")
                                    {
                                        grade7CUM = dr["PriorityCS"].ToString();
                                        grade7Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }
                                    if (dr["Grade"].ToString() == "8")
                                    {
                                        grade8CUM = dr["PriorityCS"].ToString();
                                        grade8Total = dr["TotalCS"].ToString();
                                        // totListScoreCS3 = dr["TotalScoreCS"].ToString();
                                        // totPriorityScoreCS3 = dr["TotalScorePriorityCS"].ToString();
                                    }

                                }
                            }
                        }
                    }
                    hdnGrade1mark.Value = grade1CUM;
                    hdnGrade2mark.Value = grade2CUM;
                    hdnGrade3mark.Value = grade3CUM;
                    hdnGrade4mark.Value = grade4CUM;
                    hdnGrade5mark.Value = grade5CUM;
                    hdnGrade6mark.Value = grade6CUM;
                    hdnGrade7mark.Value = grade7CUM;
                    hdnGrade8mark.Value = grade8CUM;



                    hdnTotalScore1.Value = grade1Total;
                    hdnTotalScore2.Value = grade2Total;
                    hdnTotalScore3.Value = grade3Total;
                    hdnTotalScore4.Value = grade4Total;
                    hdnTotalScore5.Value = grade5Total;
                    hdnTotalScore6.Value = grade6Total;
                    hdnTotalScore7.Value = grade7Total;
                    hdnTotalScore8.Value = grade8Total;


                    //hdnTotalPriorityScoreCS1.Value = totPriorityScoreCS1;
                    //hdnTotalPriorityScoreCS2.Value = totPriorityScoreCS2;
                    //hdnTotalPriorityScoreCS3.Value = totPriorityScoreCS3;

                    //hdnTotalScoreCS1.Value = totListScoreCS1;
                    //hdnTotalScoreCS2.Value = totListScoreCS2;
                    //hdnTotalScoreCS3.Value = totListScoreCS3;

                    int total = 0;
                    if (grade1CUM != "" && grade2CUM != "" && grade3CUM != "")
                    {
                        total = Convert.ToInt32(grade1CUM) + Convert.ToInt32(grade2CUM) + Convert.ToInt32(grade3CUM);
                        hdnTotlaInvitees.Value = total.ToString();
                    }
                    else
                    {
                        hdnTotlaInvitees.Value = "";
                    }


                    if (grade1Total != "" && grade2Total != "" && grade3Total != "")
                    {
                        total = Convert.ToInt32(grade1Total) + Convert.ToInt32(grade2Total) + Convert.ToInt32(grade3Total);
                        hdnTotalList.Value = total.ToString();
                    }
                    else
                    {
                        hdnTotalList.Value = "";
                    }

                    //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "fillScores();", true);
                }
                else
                {
                    lblError.Text = "Ending score should be greater than starting score";
                }
            }
            else
            {
                if (ddlStartingScore.SelectedValue == "Select" && ddlEndingScore.SelectedValue == "Select")
                {
                    lblError.Text = "Please select starting score and ending score";
                }
                else if (ddlContest.SelectedValue == "All")
                {
                    lblError.Text = "Please select contest";
                }
            }
        }
        else if (ddlPercentile.SelectedValue == "4")
        {
            DGPercentiles.Visible = false;
            tdStart.Visible = false;
            tdEnd.Visible = false;
            populateMedianScore();
            populateStdDeviation();
            populateMean();
        }
        Session["DuplicateFlag"] = DuplicateFlag;
    }
    private void LoadDGPercentiles()
    {
        int StartGrade, EndGrade;
        String ptile;
        lblSuccess.Text = "";
        lblError.Text = "";
        String StrSQL = "Select * FROM PercentilesByGrade Where Contestyear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and eventID=" + ddlEvent.SelectedValue + " Order by TotalScore";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DGPercentiles.DataSource = ds;
            DGPercentiles.DataBind();
            DGPercentiles.Visible = true;
            StartGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["Start_Grade"]);
            EndGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["End_Grade"]);

            foreach (DataGridColumn col in DGPercentiles.Columns)
            {
                ptile = col.HeaderText;
                if (ds.Tables[0].Rows[0][ptile] is DBNull)
                {
                    col.Visible = false;
                }
                else
                {
                    col.Visible = true;
                }
            }
        }
        else
        {
            lblSuccess.Text = "No data exists for the options selected. Please use calculate option.";
            DGPercentiles.Visible = false;
        }
    }
    protected void ddlcontestdate_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblSuccess.Text = "";
        lblError.Text = "";
    }
    protected void BtnYes_Click(object sender, EventArgs e)
    {
        //string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        InsertPercentiles(true, false);
    }
    protected void InsertPercentiles(Boolean DuplicateFlag, Boolean PerIDFlag)
    {
        lblSuccess.Text = "";
        lblError.Text = "";
        // DuplicateFlag = Convert.ToBoolean (Session["DuplicateFlag"]) ;

        int PercentID;
        if (DuplicateFlag == true && chkAllContest.Checked == true)
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Delete From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (1,2,3,4) and EventID=" + ddlEvent.SelectedValue);
        }
        else if (DuplicateFlag == true && chkAllContest.Checked == false)
        {
            if (ddlContest.SelectedItem.Text == "All")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Delete From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue);
            }
            else
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Delete From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=" + ddlEvent.SelectedValue);
            }
        }
        if (PerIDFlag == true)
        {
            PercentID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select IsNull(MAX(IsNULL(PercentID,0)),0) From PercentilesByGrade"));
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "DBCC CHECKIDENT(PercentilesByGrade, RESEED," + PercentID + ")");
        }
        CalculatePercentiles();
        tblDuplicate.Visible = false;
    }
    protected void BtnNo_Click(object sender, EventArgs e)
    {
        tblDuplicate.Visible = false;
        lblSuccess.Text = "No Update Done";
    }
    protected void ddlContest_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblSuccess.Text = "";
        lblError.Text = "";
    }

    protected void chkAllContest_CheckedChanged(object sender, EventArgs e)
    {
        if (ddlYear.SelectedIndex == 0)
        {
            lblSuccess.Text = "Please Select ContestYear";
            return;
        }
        if (chkAllContest.Checked == true)
        {
            ddlContest.Enabled = false;
            ddlContest.SelectedIndex = 0;
            ddlcontestdate.SelectedIndex = ddlcontestdate.Items.Count - 1;// 0;
            ddlcontestdate.Enabled = false;
        }
        else
        {
            ddlContest.Enabled = true;
            ddlcontestdate.Enabled = true;
        }
    }

    private void CalculatePercentiles()
    {
        if (chkAllContest.Checked == true)
        {
            for (int j = 0; j < ddlcontestdate.Items.Count; j++)
            {
                for (int i = 1; i < ddlContest.Items.Count; i++)
                {
                    ReadAllScores(ddlContest.Items[i].Text, ddlcontestdate.Items[j].Text, Convert.ToInt32(ddlcontestdate.Items[j].Value));
                }
            }
        }
        else
        {
            if (ddlContest.SelectedItem.Text == "All")
            {
                for (int i = 1; i < ddlContest.Items.Count; i++)
                {
                    ReadAllScores(ddlContest.Items[i].Text, ddlcontestdate.SelectedItem.Text, Convert.ToInt32(ddlcontestdate.SelectedItem.Value));
                }
            }
            else
            {
                ReadAllScores(ddlContest.SelectedItem.Text, ddlcontestdate.SelectedItem.Text, Convert.ToInt32(ddlcontestdate.SelectedItem.Value));
            }
        }
    }

    public void fillStartingScore()
    {
        ArrayList list = new ArrayList();
        for (int i = 1; i < 41; i++)
        {
            list.Add(new ListItem(i.ToString(), i.ToString()));

        }


        ddlStartingScore.DataSource = list;
        ddlStartingScore.DataTextField = "Text";
        ddlStartingScore.DataValueField = "Value";
        ddlStartingScore.DataBind();

        ddlEndingScore.DataSource = list;
        ddlEndingScore.DataTextField = "Text";
        ddlEndingScore.DataValueField = "Value";
        ddlEndingScore.DataBind();

        ddlStartingScore.Items.Insert(0, new ListItem("Select"));
        ddlEndingScore.Items.Insert(0, new ListItem("Select"));
    }

    public void fillPercentileByGrade()
    {
        string startingScore = ddlStartingScore.SelectedValue;
        string EndingScore = (ddlEndingScore.SelectedValue);

        //int endingScoreVal = Convert.ToInt32(EndingScore) + 1;

        String StrSQL = "Select * FROM PercentilesByGrade Where Contestyear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and eventID=" + ddlEvent.SelectedValue + " and TotalScore between " + startingScore + " and " + EndingScore + " Order by TotalScore DESC";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

        if (ds.Tables[0] != null)
        {
            int rowCount = 0;
            rowCount = ds.Tables[0].Rows.Count;

            hdnProductGroupID.Value = ds.Tables[0].Rows[0]["ProductGroupID"].ToString();
            hdnProductGroup.Value = ds.Tables[0].Rows[0]["ProductGroupCode"].ToString();
            hdnProductID.Value = ds.Tables[0].Rows[0]["ProductID"].ToString();
            hdnProduct.Value = ds.Tables[0].Rows[0]["ProductCode"].ToString();
            hdnStartGrade.Value = ds.Tables[0].Rows[0]["Start_Grade"].ToString();
            hdnEndGrade.Value = ds.Tables[0].Rows[0]["End_Grade"].ToString();


            int eCore = Convert.ToInt32(EndingScore);
            int sCore = Convert.ToInt32(startingScore);
            string tblCutOffHtml = "";
            tblCutOffHtml += "<table id='tblScore' style='border:1px solid black; width:1200px; border-collapse:collapse;'>";
            tblCutOffHtml += "<tr style='background-color:#ffffcc; font-weight:bold;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Contest Year</td>";
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black; color:#008000;'>" + ddlYear.SelectedValue + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>S</td>";
            }

            tblCutOffHtml += "<td style='border:1px solid black;'>Selected</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>EventID</td>";

            int count = 0;

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black; '>" + ds.Tables[0].Rows[j]["EventID"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            }
            count++;


            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>ProductCode</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["ProductCode"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalScore</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {

                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "</td>";



                tblCutOffHtml += "<td style='border:1px solid black;'/>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>&nbsp;</td>";

            tblCutOffHtml += "</tr>";

            if (ds.Tables[0].Rows[0]["Gr1%"] != null && ds.Tables[0].Rows[0]["Gr1%"].ToString() != "")
            {
                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr1%</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1%"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr1_ptile</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_ptile"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr1_cnt</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_cnt"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {

                        if (hdnTotalScore1.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr1_cum"].ToString() + "</td>";
                        }
                        else
                        {
                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_cum"].ToString() + "</td>";
                    }
                    tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal1" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal1' /></td>";
                    count++;
                }
                if (isExcel == 1)
                {
                    string totalScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnTotalScore1.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            totalScore = ds.Tables[0].Rows[j]["Gr1_cum"].ToString();
                        }
                    }

                    tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA; font-weight:bold;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore1'></span></td>";
                }
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr1_cum(Priority List)</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {
                        int index = (j + 1) * 2;

                        if (hdnGrade1mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            hdnGrade1ID.Value = Convert.ToString(j);
                            tblCutOffHtml += "<td style='border:1px solid black; background-color: #DE1111; font-weight:bold;'>" + ds.Tables[0].Rows[count]["Gr1_cum"].ToString() + "</td>";
                        }
                        else
                        {
                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr1_cum"].ToString() + "</td>";
                    }

                    if (ds.Tables[0].Rows[j]["Gr1_cum"].ToString() != "")
                    {
                        //tblCutOffHtml += "<td style='width:75px; border:1px solid black;'><select class='selGr1' id='selGr1CutOfMark" + count + "'><option value='select'>Select</option><option value='X'>X</option><option value='Y'>Y</option></select></td>";

                        tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade1" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade1' /></td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    }
                    count++;
                }
                if (isExcel == 1)
                {
                    string priorityScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnGrade1mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            priorityScore = ds.Tables[0].Rows[j]["Gr1_cum"].ToString();
                        }
                    }
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr1mark'></span></td>";
                }

                tblCutOffHtml += "</tr>";

            }
            if (ds.Tables[0].Rows[0]["Gr2%"] != null && ds.Tables[0].Rows[0]["Gr2%"].ToString() != "")
            {
                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr2%</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2%"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr2_ptile</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_ptile"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr2_cnt</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_cnt"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {


                        if (hdnTotalScore2.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                        }
                        else
                        {

                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                    }
                    tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal2" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal2' /></td>";
                    count++;
                }
                if (isExcel == 1)
                {
                    string totalScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnTotalScore2.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            totalScore = ds.Tables[0].Rows[j]["Gr2_cum"].ToString();
                        }
                    }
                    tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA; font-weight:bold;'>" + totalScore + "</td>";

                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore2'></span></td>";
                }
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr2_cum(Priority List)</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {
                        int index = (j + 1) * 2;
                        if (hdnGrade2mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            hdnGrade2ID.Value = Convert.ToString(j);
                            tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111; font-weight:bold;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                        }
                        else
                        {
                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr2_cum"].ToString() + "</td>";
                    }
                    if (ds.Tables[0].Rows[j]["Gr2_cum"].ToString() != "")
                    {
                        //tblCutOffHtml += "<td style='width:75px; border:1px solid black;'><select class='selGr2' id='selGr2CutOfMark" + count + "'><option value='select'>Select</option><option value='X'>X</option><option value='Y'>Y</option></select></td>";

                        tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade2" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade2' /></td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    }
                    count++;
                }
                if (isExcel == 1)
                {
                    string priorityScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnGrade2mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            priorityScore = ds.Tables[0].Rows[j]["Gr2_cum"].ToString();
                        }
                    }
                    tblCutOffHtml += "<td id='tdGr3mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr3mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr2mark'></span></td>";
                }
                tblCutOffHtml += "</tr>";

            }
            if (ds.Tables[0].Rows[0]["Gr3%"] != null && ds.Tables[0].Rows[0]["Gr3%"].ToString() != "")
            {
                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr3%</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3%"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr3_ptile</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_ptile"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr3_cnt</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_cnt"].ToString() + "</td>";
                    tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    count++;
                }
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {

                        if (hdnTotalScore3.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                        }
                        else
                        {
                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                    }
                    tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal3" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal3' /></td>";

                    count++;
                }
                if (isExcel == 1)
                {
                    string totalScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnTotalScore3.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            totalScore = ds.Tables[0].Rows[j]["Gr3_cum"].ToString();
                        }
                    }
                    tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA; font-weight:bold;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore3'></span></td>";

                }
                tblCutOffHtml += "</tr>";

                tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr3_cum(Priority List)</td>";
                count = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (isExcel == 1)
                    {
                        int index = (j + 1) * 2;

                        if (hdnGrade3mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            hdnGrade3ID.Value = Convert.ToString(j);
                            tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111; font-weight:bold;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                        }
                        else
                        {
                            tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                        }
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr3_cum"].ToString() + "</td>";
                    }

                    if (ds.Tables[0].Rows[j]["Gr3_cum"].ToString() != "")
                    {
                        //tblCutOffHtml += "<td style='width:75px; border:1px solid black;'><select class='selGr3' id='selGr3CutOfMark" + count + "'><option value='select'>Select</option><option value='X'>X</option><option value='Y'>Y</option></select></td>";


                        tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade3" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "'  attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade3' /></td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                    }
                    count++;
                }
                if (isExcel == 1)
                {
                    string priorityScore = "";
                    for (int j = 0; j < rowCount; j++)
                    {
                        if (hdnGrade3mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                        {
                            priorityScore = ds.Tables[0].Rows[j]["Gr3_cum"].ToString();
                        }
                    }
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnGr3mark'></span></td>";
                }
                tblCutOffHtml += "</tr>";
            }
            tblCutOffHtml += generatePercentiles(rowCount, ds);

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalPercentile</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["TotalPercentile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalCount</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Total_Count"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Total_Cume_Count</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Total_Cume_Count"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>ContestPeriod</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["ContestPeriod"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>ContestDate</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["ContestDate"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
                count++;
            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Total List</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {

                tblCutOffHtml += "<td style='border-bottom:1px solid black;'>&nbsp</td>";
                tblCutOffHtml += "<td style='border-bottom:1px solid black;'>&nbsp;</td>";
                count++;
            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                int totScore = 0;
                int mark1 = 0;
                int mark2 = 0;
                int mark3 = 0;
                int mark4 = 0;
                int mark5 = 0;
                int mark6 = 0;
                int mark7 = 0;
                int mark8 = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore1.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark1 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr1_cum"].ToString());
                    }
                    if (hdnTotalScore2.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark2 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr2_cum"].ToString());
                    }
                    if (hdnTotalScore3.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark3 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr3_cum"].ToString());
                    }
                    if (hdnTotalScore4.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark4 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr4_cum"].ToString());
                    }
                    if (hdnTotalScore5.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark5 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr5_cum"].ToString());
                    }
                    if (hdnTotalScore6.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark6 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr6_cum"].ToString());
                    }
                    if (hdnTotalScore7.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark7 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr7_cum"].ToString());
                    }
                    if (hdnTotalScore8.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark8 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr8_cum"].ToString());
                    }
                }
                totScore = mark1 + mark2 + mark3 + mark4 + mark5 + mark6 + mark7 + mark8;
                priorityScore = totScore.ToString();
                priorityScore = (priorityScore == "0" ? "" : priorityScore);
                tblCutOffHtml += "<td id='tdTotNoList' style='font-weight:bold; background-color:#03C8FA; border-bottom:1px solid black;'>" + priorityScore + "</td>";
            }
            else
            {
                if (hdnTotalList.Value != "")
                {
                    tblCutOffHtml += "<td id='tdTotNoList' style='font-weight:bold; background-color:#03C8FA; border-bottom:1px solid black;'>" + hdnTotalList.Value + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdTotNoList' style='font-weight:bold; border-bottom:1px solid black;'><span id='splTotalList'></span></td>";
                }
            }
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Total Priority List</td>";
            count = 0;
            for (int j = 0; j < rowCount; j++)
            {

                tblCutOffHtml += "<td>&nbsp</td>";
                tblCutOffHtml += "<td>&nbsp;</td>";
                count++;
            }

            if (isExcel == 1)
            {
                string priorityScore = "";
                int totScore = 0;
                int mark1 = 0;
                int mark2 = 0;
                int mark3 = 0;
                int mark4 = 0;
                int mark5 = 0;
                int mark6 = 0;
                int mark7 = 0;
                int mark8 = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade1mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark1 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr1_cum"].ToString());
                    }
                    if (hdnGrade2mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark2 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr2_cum"].ToString());
                    }
                    if (hdnGrade3mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark3 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr3_cum"].ToString());
                    }
                    if (hdnGrade4mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark4 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr4_cum"].ToString());
                    }
                    if (hdnGrade5mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark5 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr5_cum"].ToString());
                    }
                    if (hdnGrade6mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark6 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr6_cum"].ToString());
                    }
                    if (hdnGrade7mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark7 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr7_cum"].ToString());
                    }
                    if (hdnGrade8mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        mark8 = Convert.ToInt32(ds.Tables[0].Rows[j]["Gr8_cum"].ToString());
                    }
                }
                totScore = mark1 + mark2 + mark3 + mark4 + mark5 + mark6 + mark7 + mark8;
                priorityScore = totScore.ToString();
                priorityScore = (priorityScore == "0" ? "" : priorityScore);
                tblCutOffHtml += "<td id='tdTotNoOfInvitees' style='font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
            }
            else
            {
                if (hdnTotlaInvitees.Value != "")
                {
                    tblCutOffHtml += "<td id='tdTotNoOfInvitees' style='font-weight:bold; background-color:#DE1111;'>" + hdnTotlaInvitees.Value + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdTotNoOfInvitees' style='font-weight:bold;'><span id='spnTotalInvitees'></span></td>";
                }
            }


            //tblCutOffHtml += "<td id='tdTotNoOfInvitees' > &nbsp; </td>";

            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "</table>";

            ltrSchedulePostingDay2.Text = tblCutOffHtml;
        }
    }

    protected void btnSaveCutOffScores_Click(object sender, EventArgs e)
    {

        lblError.Text = "";
        string cmdText = "";
        DataSet ds = new DataSet();


        int Product = Convert.ToInt32(hdnProductID.Value);
        int ProductGroup = Convert.ToInt32(hdnProductGroupID.Value);
        string productName = hdnProduct.Value;
        string ProductgroupName = hdnProductGroup.Value;
        string ContestYear = ddlYear.SelectedValue;
        string ListType = ddlListType.SelectedValue;
        string cutOffScore1 = hdnGrade1mark.Value;
        string cutOffScore2 = hdnGrade2mark.Value;
        string cutOffScore3 = hdnGrade3mark.Value;
        string cutOffScore4 = hdnGrade4mark.Value;
        string cutOffScore5 = hdnGrade5mark.Value;
        string cutOffScore6 = hdnGrade6mark.Value;
        string cutOffScore7 = hdnGrade7mark.Value;
        string cutOffScore8 = hdnGrade8mark.Value;



        string totalScore1 = hdnTotalScore1.Value;
        string totalScore2 = hdnTotalScore2.Value;
        string totalScore3 = hdnTotalScore3.Value;
        string totalScore4 = hdnTotalScore4.Value;
        string totalScore5 = hdnTotalScore5.Value;
        string totalScore6 = hdnTotalScore6.Value;
        string totalScore7 = hdnTotalScore7.Value;
        string totalScore8 = hdnTotalScore8.Value;


        string userID = Session["LoginId"].ToString();

        string totListScoreCS1 = hdnTotalScoreCS1.Value;
        string totListScoreCS2 = hdnTotalScoreCS2.Value;
        string totListScoreCS3 = hdnTotalScoreCS3.Value;

        string totPriorityScoreCS1 = hdnTotalPriorityScoreCS1.Value;
        string totPriorityScoreCS2 = hdnTotalPriorityScoreCS2.Value;
        string totPriorityScoreCS3 = hdnTotalPriorityScoreCS3.Value;
        int count = 0;


        count = 0;
        if (cutOffScore1 != "" && totalScore1 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=1 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=1,PriorityCS='" + cutOffScore1 + "',TotalCS=" + totalScore1 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + "  where Grade=1 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',1,'" + cutOffScore1 + "','" + totalScore1 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore2 != "" && totalScore2 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=2 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=2,PriorityCS='" + cutOffScore2 + "',TotalCS=" + totalScore2 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + " where Grade=2 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',2,'" + cutOffScore2 + "','" + totalScore2 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore3 != "" && totalScore3 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=3 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=3,PriorityCS='" + cutOffScore3 + "',TotalCS=" + totalScore3 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + " where Grade=3 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',3,'" + cutOffScore3 + "'," + totalScore3 + ",GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }


        if (cutOffScore4 != "" && totalScore4 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=4 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=1,PriorityCS='" + cutOffScore4 + "',TotalCS=" + totalScore4 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + "  where Grade=4 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',4,'" + cutOffScore4 + "','" + totalScore4 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore5 != "" && totalScore5 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=5 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=5,PriorityCS='" + cutOffScore5 + "',TotalCS=" + totalScore5 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + " where Grade=5 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',5,'" + cutOffScore5 + "','" + totalScore5 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore6 != "" && totalScore6 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=6 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=6,PriorityCS='" + cutOffScore6 + "',TotalCS=" + totalScore6 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + " where Grade=6 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',6,'" + cutOffScore6 + "'," + totalScore6 + ",GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore7 != "" && totalScore7 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=7 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=1,PriorityCS='" + cutOffScore7 + "',TotalCS=" + totalScore7 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + "  where Grade=7 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',7,'" + cutOffScore7 + "','" + totalScore7 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }
        if (cutOffScore8 != "" && totalScore8 != "")
        {
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}=8 and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=8,PriorityCS='" + cutOffScore8 + "',TotalCS=" + totalScore8 + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + " where Grade=8 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "',8,'" + cutOffScore8 + "','" + totalScore8 + "',GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }

        else
        {
            lblError.Text = "Please select Priority List or Total List";
        }



    }

    public int validateCutOffmarks()
    {
        int retVal = 1;
        if (hdnTotalScore1.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Total Cut Off Score for Grade 1";
        }
        else if (hdnGrade1mark.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Cut Off Score for Grade 1";
        }
        else if (hdnTotalScore2.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Total Cut Off Score for Grade 2";
        }
        else if (hdnGrade2mark.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Cut Off Score for Grade 2";
        }
        else if (hdnTotalScore3.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Total Cut Off Score for Grade 3";
        }
        else if (hdnGrade3mark.Value == "")
        {
            retVal = -1;
            lblError.Text = "Please select Cut Off Score for Grade 3";
        }
        return retVal;
    }

    public string generatePercentiles(int rowCount, DataSet ds)
    {
        string tblCutOffHtml = "";
        if (ds.Tables[0].Rows[0]["Gr4%"] != null && ds.Tables[0].Rows[0]["Gr4%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr4%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr4_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr4_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";


            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnTotalScore4.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal4" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal4' /></td>";

            }
            if (isExcel == 1)
            {
                string totalScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore4.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        totalScore = ds.Tables[0].Rows[j]["Gr4_cum"].ToString();
                    }
                }
                if (totalScore != "")
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#03C8FA;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore4'></span></td>";
            }
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr4_cum(Priority List)</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnGrade4mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr4_cum"].ToString() + "</td>";
                }
                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade4" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade4' /></td>";


            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade4mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        priorityScore = ds.Tables[0].Rows[j]["Gr4_cum"].ToString();
                    }
                }
                if (priorityScore != "")
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr4mark'></span></td>";
            }
            tblCutOffHtml += "</tr>";

        }

        /////////
        if (ds.Tables[0].Rows[0]["Gr5%"] != null && ds.Tables[0].Rows[0]["Gr5%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr5%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr5_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr5_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnTotalScore5.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal5" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal5' /></td>";

            }
            if (isExcel == 1)
            {
                string totalScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore5.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        totalScore = ds.Tables[0].Rows[j]["Gr5_cum"].ToString();
                    }
                }
                if (totalScore != "")
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#03C8FA;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore5'></span></td>";
            }
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr5_cum(Priority List)</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnGrade5mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr5_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade5" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade5' /></td>";


            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade5mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        priorityScore = ds.Tables[0].Rows[j]["Gr5_cum"].ToString();
                    }
                }
                if (priorityScore != "")
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td id='tdGr5mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr5mark'></span></td>";
            }
            tblCutOffHtml += "</tr>";
        }
        //////////
        if (ds.Tables[0].Rows[0]["Gr6%"] != null && ds.Tables[0].Rows[0]["Gr6%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr6%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr6_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr6_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";

            for (int j = 0; j < rowCount; j++)
            {

                if (isExcel == 1)
                {
                    if (hdnTotalScore6.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal6" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal6' /></td>";

            }
            if (isExcel == 1)
            {
                string totalScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore6.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        totalScore = ds.Tables[0].Rows[j]["Gr6_cum"].ToString();
                    }
                }
                if (totalScore != "")
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#03C8FA;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore6'></span></td>";
            }
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr6_cum(Priority List)</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnGrade6mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr6_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade6" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade6' /></td>";


            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade6mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        priorityScore = ds.Tables[0].Rows[j]["Gr6_cum"].ToString();
                    }
                }
                if (priorityScore != "")
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td id='tdGr6mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr6mark'></span></td>";
            }
            tblCutOffHtml += "</tr>";
        }
        //////
        if (ds.Tables[0].Rows[0]["Gr7%"] != null && ds.Tables[0].Rows[0]["Gr7%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr7%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr7_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr7_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";

            for (int j = 0; j < rowCount; j++)
            {

                if (isExcel == 1)
                {
                    if (hdnTotalScore7.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal7" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal7' /></td>";

            }
            if (isExcel == 1)
            {
                string totalScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore7.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        totalScore = ds.Tables[0].Rows[j]["Gr7_cum"].ToString();
                    }
                }
                if (totalScore != "")
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#03C8FA;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore7'></span></td>";
            }

            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr7_cum(Priority List)</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnGrade7mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr7_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade7" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade7' /></td>";


            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade7mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        priorityScore = ds.Tables[0].Rows[j]["Gr7_cum"].ToString();
                    }
                }
                if (priorityScore != "")
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td id='tdGr7mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr7mark'></span></td>";
            }
            tblCutOffHtml += "</tr>";
        }
        ///////////////////
        if (ds.Tables[0].Rows[0]["Gr8%"] != null && ds.Tables[0].Rows[0]["Gr8%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr8%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr8_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr8_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>TotalList</td>";

            for (int j = 0; j < rowCount; j++)
            {

                if (isExcel == 1)
                {
                    if (hdnTotalScore8.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#03C8FA;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtTotal8" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsTotal8' /></td>";

            }
            if (isExcel == 1)
            {
                string totalScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnTotalScore8.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        totalScore = ds.Tables[0].Rows[j]["Gr8_cum"].ToString();
                    }
                }
                if (totalScore != "")
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold; background-color:#03C8FA;'>" + totalScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'><span id='spnTotalScore8'></span></td>";
            }
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";

            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr8_cum(Priority List)</td>";

            for (int j = 0; j < rowCount; j++)
            {
                if (isExcel == 1)
                {
                    if (hdnGrade8mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        tblCutOffHtml += "<td style='border:1px solid black; background-color:#DE1111;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                    }
                    else
                    {
                        tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                    }
                }
                else
                {
                    tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr8_cum"].ToString() + "</td>";
                }

                tblCutOffHtml += "<td style='border:1px solid black;'> <input type='radio' id='rbtnGrade8" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + "' attr-ID=" + ds.Tables[0].Rows[j]["TotalScore"].ToString() + " class='clsGrade8' /></td>";


            }
            if (isExcel == 1)
            {
                string priorityScore = "";
                for (int j = 0; j < rowCount; j++)
                {
                    if (hdnGrade8mark.Value == ds.Tables[0].Rows[j]["TotalScore"].ToString())
                    {
                        priorityScore = ds.Tables[0].Rows[j]["Gr8_cum"].ToString();
                    }
                }
                if (priorityScore != "")
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold; background-color:#DE1111;'>" + priorityScore + "</td>";
                }
                else
                {
                    tblCutOffHtml += "<td id='tdGr1mark' style='border:1px solid black; font-weight:bold;'></td>";
                }
            }
            else
            {
                tblCutOffHtml += "<td id='tdGr8mark' style='border:1px solid black; font-weight:bold;'><span id='spnGr8mark'></span></td>";
            }
            tblCutOffHtml += "</tr>";
        }
        ///////////////////////////
        if (ds.Tables[0].Rows[0]["Gr9%"] != null && ds.Tables[0].Rows[0]["Gr9%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr9%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr9%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr9_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr9_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr9_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr9_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";
        }
        //////////////////
        if (ds.Tables[0].Rows[0]["Gr10%"] != null && ds.Tables[0].Rows[0]["Gr10%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr10%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr10%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr10_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr10_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr10_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr10_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";
        }
        ////////////////////////
        if (ds.Tables[0].Rows[0]["Gr11%"] != null && ds.Tables[0].Rows[0]["Gr11%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr11%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr11%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr11_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr11_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr11_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr11_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";
        }
        ////////////////
        if (ds.Tables[0].Rows[0]["Gr12%"] != null && ds.Tables[0].Rows[0]["Gr12%"].ToString() != "")
        {
            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr12%</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr12%"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr12_ptile</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr12_ptile"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";

            tblCutOffHtml += "<tr style='background-color:#ffffff;'>";
            tblCutOffHtml += "<td style='border:1px solid black; font-weight:bold;'>Gr12_cnt</td>";

            for (int j = 0; j < rowCount; j++)
            {
                tblCutOffHtml += "<td style='border:1px solid black;'>" + ds.Tables[0].Rows[j]["Gr12_cnt"].ToString() + "</td>";
                tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";

            }
            tblCutOffHtml += "<td style='border:1px solid black;'>&nbsp;</td>";
            tblCutOffHtml += "</tr>";
        }

        return tblCutOffHtml;
    }




    protected void btnSaveTotalList_Click(object sender, EventArgs e)
    {

        SaveTotalList();
    }
    protected void btnSavePriorityList_Click(object sender, EventArgs e)
    {
        savePriorityList();
    }

    public void savePriorityList()
    {
        lblError.Text = "";
        string cmdText = "";
        DataSet ds = new DataSet();


        int Product = Convert.ToInt32(hdnProductID.Value);
        int ProductGroup = Convert.ToInt32(hdnProductGroupID.Value);
        string productName = hdnProduct.Value;
        string ProductgroupName = hdnProductGroup.Value;
        string ContestYear = ddlYear.SelectedValue;
        string ListType = ddlListType.SelectedValue;
        string cutOffScore1 = hdnGrade1mark.Value;
        string cutOffScore2 = hdnGrade2mark.Value;
        string cutOffScore3 = hdnGrade3mark.Value;
        string cutOffScore4 = hdnGrade4mark.Value;
        string cutOffScore5 = hdnGrade5mark.Value;
        string cutOffScore6 = hdnGrade6mark.Value;
        string cutOffScore7 = hdnGrade7mark.Value;
        string cutOffScore8 = hdnGrade8mark.Value;

        ArrayList arrScore = new ArrayList();
        arrScore.Add(cutOffScore1);
        arrScore.Add(cutOffScore2);
        arrScore.Add(cutOffScore3);
        arrScore.Add(cutOffScore4);
        arrScore.Add(cutOffScore5);
        arrScore.Add(cutOffScore6);
        arrScore.Add(cutOffScore7);
        arrScore.Add(cutOffScore8);

        string totalScore1 = hdnTotalScore1.Value;
        string totalScore2 = hdnTotalScore2.Value;
        string totalScore3 = hdnTotalScore3.Value;
        string totalScore4 = hdnTotalScore4.Value;
        string totalScore5 = hdnTotalScore5.Value;
        string totalScore6 = hdnTotalScore6.Value;
        string totalScore7 = hdnTotalScore7.Value;
        string totalScore8 = hdnTotalScore8.Value;
        ArrayList arrTotalScore = new ArrayList();
        arrTotalScore.Add(totalScore1);
        arrTotalScore.Add(totalScore2);
        arrTotalScore.Add(totalScore3);
        arrTotalScore.Add(totalScore4);
        arrTotalScore.Add(totalScore5);
        arrTotalScore.Add(totalScore6);
        arrTotalScore.Add(totalScore7);
        arrTotalScore.Add(totalScore8);

        string cutOffScore = "";
        int gradeNo = 0;
        int count = 0;
        string userID = Session["LoginId"].ToString();

        int startGrade = Convert.ToInt32(hdnStartGrade.Value);
        int EndGrade = Convert.ToInt32(hdnEndGrade.Value);
        string totalList = "";
        for (int i = startGrade; i <= EndGrade; i++)
        {
            gradeNo = i;
            cutOffScore = arrScore[i - 1].ToString();
            totalList = arrTotalScore[i - 1].ToString();
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}={7} and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear, gradeNo);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        totalList = (totalList == "" ? "null" : totalList);
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=" + gradeNo + ",PriorityCS=" + (cutOffScore == "" ? "null" : cutOffScore) + ",TotalCS=" + totalList + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + "  where Grade=" + gradeNo + " and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores updated successfully";
                    }
                    else
                    {
                        totalList = (totalList == "" ? "null" : totalList);
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,PriorityCS,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "'," + gradeNo + "," + (cutOffScore == "" ? "null" : cutOffScore) + "," + totalList + ",GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Cut Off Scores saved successfully";
                    }
                }
            }
        }

    }

    public void SaveTotalList()
    {
        lblError.Text = "";
        string cmdText = "";
        DataSet ds = new DataSet();


        int Product = Convert.ToInt32(hdnProductID.Value);
        int ProductGroup = Convert.ToInt32(hdnProductGroupID.Value);
        string productName = hdnProduct.Value;
        string ProductgroupName = hdnProductGroup.Value;
        string ContestYear = ddlYear.SelectedValue;
        string ListType = ddlListType.SelectedValue;

        string totalScore1 = hdnTotalScore1.Value;
        string totalScore2 = hdnTotalScore2.Value;
        string totalScore3 = hdnTotalScore3.Value;
        string totalScore4 = hdnTotalScore4.Value;
        string totalScore5 = hdnTotalScore5.Value;
        string totalScore6 = hdnTotalScore6.Value;
        string totalScore7 = hdnTotalScore7.Value;
        string totalScore8 = hdnTotalScore8.Value;
        ArrayList arrScore = new ArrayList();
        arrScore.Add(totalScore1);
        arrScore.Add(totalScore2);
        arrScore.Add(totalScore3);
        arrScore.Add(totalScore4);
        arrScore.Add(totalScore5);
        arrScore.Add(totalScore6);
        arrScore.Add(totalScore7);
        arrScore.Add(totalScore8);

        string totalList = "";
        int gradeNo = 0;
        int count = 0;
        string userID = Session["LoginId"].ToString();

        int startGrade = Convert.ToInt32(hdnStartGrade.Value);
        int EndGrade = Convert.ToInt32(hdnEndGrade.Value);
        for (int i = startGrade; i <= EndGrade; i++)
        {
            gradeNo = i;
            totalList = arrScore[i - 1].ToString();
            cmdText = string.Format("select count(*) as CountSet from CutOffScores where {0}={4} and {1}={5} and {2}={7} and {3}={6}", "ProductGroupID", "ProductID", "Grade", "ContestYear", ProductGroup, Product, ContestYear, gradeNo);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    if (count > 0)
                    {
                        totalList = (totalList == "" ? "null" : totalList);
                        cmdText = "Update CutOffScores set ProductGroupID=" + ProductGroup + ",ProductGroupCode='" + ProductgroupName + "',ProductID=" + Product + ",ProductCode='" + productName + "', ContestYear=" + ContestYear + ",Grade=" + gradeNo + ",TotalCS=" + totalList + ",ModifiedDate=GetDate(),ModifiedBy=" + userID + "  where Grade=1 and ProductGroupID=" + ProductGroup + " and ProductID=" + Product + " and ContestYear=" + ContestYear + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Total List updated successfully for Grade " + gradeNo + "";
                    }
                    else
                    {
                        totalList = (totalList == "" ? "null" : totalList);
                        cmdText = "insert into CutOffScores (ContestYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Grade,TotalCS,CreatedDate,CreatedBy) values (" + ContestYear + "," + ProductGroup + ",'" + ProductgroupName + "'," + Product + ",'" + productName + "'," + gradeNo + "," + totalList + ",GetDate()," + userID + ");";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblSuccess.Text = "Total List saved successfully for Grade " + gradeNo + "";
                    }
                }
            }
        }
    }

    public void saveExcelAll()
    {
        IWorkbook xlWorkBook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("CutOffScoreSheet\\PercentilesByGrade.xls"));
        //IWorkbook xlWorkBook = NativeExcel.Factory.CreateWorkbook();

        IWorksheet Sheet1;
        //Sheet1 = xlWorkBook.Worksheets.Add();
        Sheet1 = xlWorkBook.Worksheets[1];
        DataTable dt2 = new DataTable();
        String SQLPercent;
        DataSet ds = new DataSet();
        string FileName = "";
        for (int i = 1; i <= xlWorkBook.Worksheets.Count; i++)
        {
            Sheet1 = xlWorkBook.Worksheets[i];
            SQLPercent = "";
            // Sheet1.Range[i, 1].Value = Sheet1.Name ;

            try
            {
                SQLPercent = "Select ContestYear,EventId,ProductCode,TotalScore,";

                switch (Sheet1.Name)
                {
                    case "MB1":
                        SQLPercent = SQLPercent + "[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,";
                        break;
                    case "MB2":
                        SQLPercent = SQLPercent + "[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,";
                        break;
                    case "MB3":
                        SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "MB4":
                        SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,";
                        break;
                    case "JSB":
                    case "JSC":
                        SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                        break;
                    case "ISC":
                        SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,";
                        break;
                    case "SSC":
                        SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "SSB":
                        SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "JVB":
                        SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                        break;
                    case "IVB":
                        SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "SVB":
                        SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                        break;
                    case "JGB":
                        SQLPercent = SQLPercent + "[Gr1%],Gr1_ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,";
                        break;
                    case "SGB":
                        SQLPercent = SQLPercent + "[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "EW1":
                        SQLPercent = SQLPercent + "[Gr3%],Gr3_ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_ptile,Gr5_cnt,Gr5_cum,";
                        break;
                    case "EW2":
                        SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "EW3":
                        SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                        break;
                    case "PS1":
                        SQLPercent = SQLPercent + "[Gr6%],Gr6_ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,";
                        break;
                    case "PS3":
                        SQLPercent = SQLPercent + "[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_ptile,Gr12_cnt,Gr12_cum,";
                        break;
                    case "BB":
                        SQLPercent = SQLPercent + "[Gr8%],Gr8_ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_ptile,Gr11_cnt,Gr11_cum,";
                        break;
                }
                string startingScore = ddlStartingScore.SelectedValue;
                string EndingScore = ddlEndingScore.SelectedValue;

                DataSet dsRange = new DataSet();
                string cmdrangeText = "select min(TotalCS) as MinScore,max(PriorityCS) as MaxScore from CutOffScores where ProductCode='" + Sheet1.Name + "'";
                dsRange = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdrangeText);

                if (dsRange.Tables[0] != null)
                {
                    if (dsRange.Tables[0].Rows.Count > 0)
                    {
                        if (dsRange.Tables[0].Rows[0]["MinScore"] != null && dsRange.Tables[0].Rows[0]["MinScore"].ToString() != "")
                        {
                            startingScore = dsRange.Tables[0].Rows[0]["MinScore"].ToString();
                        }
                        if (dsRange.Tables[0].Rows[0]["MaxScore"] != null && dsRange.Tables[0].Rows[0]["MaxScore"].ToString() != "")
                        {
                            EndingScore = dsRange.Tables[0].Rows[0]["MaxScore"].ToString();
                        }
                    }
                }

                SQLPercent = SQLPercent + "TotalPercentile,Total_Count,Total_cume_Count,ContestPeriod,ContestDate,Start_grade,End_Grade From PercentilesByGrade Where EventID =" + ddlEvent.SelectedValue + " and ContestYear=" + ddlYear.SelectedValue + " and ProductCode='" + Sheet1.Name + "' and ContestPeriod in(" + ddlcontestdate.SelectedValue + ") and TotalScore between " + startingScore + " and " + EndingScore + " Order by ContestPeriod,productGroupID, ProductID,TotalScore DESC";

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLPercent);

                string cmdScoreText = string.Format("select PriorityCS,TotalCS,Grade from CutOffScores where {0}='{2}' and {1}={3}", "ProductCode", "ContestYear", Sheet1.Name, ddlYear.SelectedValue);
                DataSet dsScore = new DataSet();
                dsScore = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdScoreText);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    dt2 = ds.Tables[0];
                    int startGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["Start_grade"].ToString());
                    int endGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["End_Grade"].ToString());
                    int colCount = 0;
                    //for (int k = 0; k < dt2.Columns.Count; k++)
                    //{
                    //    colCount++;
                    //    Sheet1.Cells[colCount, k + 1].Value = dt2.Columns[k].ColumnName;
                    //    for (int s = 1; s < dt2.Rows.Count; s++)
                    //    {
                    //        Sheet1.Cells[colCount, s + 1].Value = dt2.Rows[s][dt2.Columns[k].ColumnName].ToString();
                    //    }
                    //}


                    int count = 0;
                    Sheet1.Cells[1, 0 + 1].Value = "ContestYear";
                    Sheet1.Cells[1, 0 + 1].Font.Bold = true;
                    Sheet1.Cells[1, 0 + 1].ColumnWidth = 20;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[1, l + 1].Value = dt2.Rows[l - 1]["ContestYear"].ToString();
                        Sheet1.Cells[1, l + 1].Font.Bold = true;
                        count++;
                    }
                    Sheet1.Cells[1, count + 2].Value = "Selected";
                    Sheet1.Cells[1, count + 2].Font.Bold = true;

                    count = 0;
                    Sheet1.Cells[2, 0 + 1].Value = "EventID";
                    Sheet1.Cells[2, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[2, l + 1].Value = dt2.Rows[l - 1]["EventID"].ToString();

                        count++;
                    }
                    /////
                    count = 0;
                    Sheet1.Cells[3, 0 + 1].Value = "ProductCode";
                    Sheet1.Cells[3, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[3, l + 1].Value = dt2.Rows[l - 1]["ProductCode"].ToString();

                        count++;
                    }
                    /////
                    count = 0;
                    Sheet1.Cells[4, 0 + 1].Value = "TotalScore";
                    Sheet1.Cells[4, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[4, l + 1].Value = dt2.Rows[l - 1]["TotalScore"].ToString();
                        Sheet1.Range[4, l + 1].NumberFormat = "#,##0_);(#,##0)";

                        count++;
                    }
                    /////
                    count = 0;
                    int grCount = 4;
                    int tempCount = 0;
                    int gradeCounts = 0;
                    for (int n = startGrade; n <= endGrade; n++)
                    {
                        count = 0;
                        grCount++;

                        Sheet1.Cells[grCount, 0 + 1].Value = "Gr" + n + "%";
                        Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                        for (int l = 1; l <= dt2.Rows.Count; l++)
                        {
                            Sheet1.Cells[grCount, l + 1].NumberFormat = "00.0%";
                            Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Gr" + n + "%"].ToString();

                            count++;
                            tempCount = l;
                        }

                        grCount++;
                        count = 0;
                        Sheet1.Cells[grCount, 0 + 1].Value = "Gr" + n + "_ptile";
                        Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                        for (int l = 1; l <= dt2.Rows.Count; l++)
                        {
                            Sheet1.Cells[grCount, l + 1].NumberFormat = "00.0%";
                            Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Gr" + n + "_ptile"].ToString();

                            count++;

                        }
                        grCount++;
                        count = 0;
                        Sheet1.Cells[grCount, 0 + 1].Value = "Gr" + n + "_cnt";
                        Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                        for (int l = 1; l <= dt2.Rows.Count; l++)
                        {
                            Sheet1.Cells[grCount, l + 1].NumberFormat = "#,##0_);(#,##0)";
                            Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Gr" + n + "_cnt"].ToString();

                            count++;

                        }
                        grCount++;
                        count = 0;
                        Sheet1.Cells[grCount, 0 + 1].Value = "TotalList";
                        Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                        for (int l = 1; l <= dt2.Rows.Count; l++)
                        {

                            Sheet1.Cells[grCount, l + 1].NumberFormat = "00.0%";
                            Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Gr" + n + "_cum"].ToString();
                            if (dsScore.Tables[0] != null)
                            {
                                if (dsScore.Tables[0].Rows.Count > 0)
                                {
                                    if (dsScore.Tables[0].Rows[gradeCounts]["TotalCS"].ToString() == dt2.Rows[l - 1]["TotalScore"].ToString())
                                    {
                                        Sheet1.Cells[grCount, l + 1].Interior.ColorIndex = 42;
                                    }
                                }
                            }

                            count++;

                        }
                        if (dsScore.Tables[0] != null)
                        {
                            if (dsScore.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dsScore.Tables[0].Rows[gradeCounts]["Grade"].ToString()) == n)
                                {
                                    for (int l = 0; l < dt2.Rows.Count; l++)
                                    {
                                        if (dsScore.Tables[0].Rows[gradeCounts]["TotalCS"].ToString() == dt2.Rows[l]["TotalScore"].ToString())
                                        {
                                            Sheet1.Cells[grCount, tempCount + 2].Value = Convert.ToInt32(dt2.Rows[l]["Gr" + n + "_cum"].ToString());
                                            Sheet1.Cells[grCount, tempCount + 2].Interior.ColorIndex = 42;
                                        }

                                    }
                                }
                            }
                        }
                        grCount++;
                        count = 0;
                        tempCount = 0;
                        Sheet1.Cells[grCount, 0 + 1].Value = "Gr" + n + "_cum(Priority List)";
                        Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                        for (int l = 1; l <= dt2.Rows.Count; l++)
                        {
                            Sheet1.Cells[grCount, l + 1].NumberFormat = "00.0%";
                            Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Gr" + n + "_cum"].ToString();
                            if (dsScore.Tables[0] != null)
                            {
                                if (dsScore.Tables[0].Rows.Count > 0)
                                {
                                    if (dsScore.Tables[0].Rows[gradeCounts]["PriorityCS"].ToString() == dt2.Rows[l - 1]["TotalScore"].ToString())
                                    {
                                        Sheet1.Cells[grCount, l + 1].Interior.ColorIndex = 3;
                                    }
                                }
                            }
                            count++;
                            tempCount = l;
                        }
                        if (dsScore.Tables[0] != null)
                        {
                            if (dsScore.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dsScore.Tables[0].Rows[gradeCounts]["Grade"].ToString()) == n)
                                {
                                    for (int l = 0; l < dt2.Rows.Count; l++)
                                    {
                                        if (dsScore.Tables[0].Rows[gradeCounts]["PriorityCS"].ToString() == dt2.Rows[l]["TotalScore"].ToString())
                                        {
                                            Sheet1.Cells[grCount, tempCount + 2].Value = Convert.ToInt32(dt2.Rows[l]["Gr" + n + "_cum"].ToString());
                                            Sheet1.Cells[grCount, tempCount + 2].Interior.ColorIndex = 3;
                                        }

                                    }
                                }
                            }
                        }
                        gradeCounts++;
                    }

                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "TotalPercentile";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["TotalPercentile"].ToString();

                        count++;

                    }
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "Total_Count";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Total_Count"].ToString();

                        count++;

                    }
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "Total_Cume_Count";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["Total_Cume_Count"].ToString();

                        count++;

                    }
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "ContestPeriod";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["ContestPeriod"].ToString();

                        count++;

                    }
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "ContestDate";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = dt2.Rows[l - 1]["ContestDate"].ToString();

                        count++;

                    }
                    ////
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "Total List";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = "";

                        count++;
                        tempCount = l;
                    }
                    int totalList = 0;
                    gradeCounts = 0;
                    for (int n = startGrade; n <= endGrade; n++)
                    {

                        if (dsScore.Tables[0] != null)
                        {
                            if (dsScore.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dsScore.Tables[0].Rows[gradeCounts]["Grade"].ToString()) == n)
                                {
                                    for (int l = 0; l < dt2.Rows.Count; l++)
                                    {
                                        if (dsScore.Tables[0].Rows[gradeCounts]["TotalCS"].ToString() == dt2.Rows[l]["TotalScore"].ToString())
                                        {
                                            totalList = totalList + Convert.ToInt32(dt2.Rows[l]["Gr" + n + "_cum"].ToString());
                                        }

                                    }
                                }
                            }
                        }
                        gradeCounts++;
                    }
                    if (totalList > 0)
                    {
                        Sheet1.Cells[grCount, tempCount + 2].Value = totalList;
                        Sheet1.Cells[grCount, tempCount + 2].Interior.ColorIndex = 42;
                    }
                    ////
                    grCount++;
                    count = 0;
                    Sheet1.Cells[grCount, 0 + 1].Value = "Total Priority List";
                    Sheet1.Cells[grCount, 0 + 1].Font.Bold = true;
                    for (int l = 1; l <= dt2.Rows.Count; l++)
                    {

                        Sheet1.Cells[grCount, l + 1].Value = "";

                        count++;
                        tempCount = l;
                    }
                    totalList = 0;
                    gradeCounts = 0;
                    for (int n = startGrade; n <= endGrade; n++)
                    {

                        if (dsScore.Tables[0] != null)
                        {
                            if (dsScore.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dsScore.Tables[0].Rows[gradeCounts]["Grade"].ToString()) == n)
                                {
                                    for (int l = 0; l < dt2.Rows.Count; l++)
                                    {
                                        if (dsScore.Tables[0].Rows[gradeCounts]["PriorityCS"].ToString() == dt2.Rows[l]["TotalScore"].ToString())
                                        {
                                            totalList = totalList + Convert.ToInt32(dt2.Rows[l]["Gr" + n + "_cum"].ToString());
                                        }

                                    }
                                }
                            }
                        }
                        gradeCounts++;
                    }
                    if (totalList > 0)
                    {
                        Sheet1.Cells[grCount, tempCount + 2].Value = totalList;
                        Sheet1.Cells[grCount, tempCount + 2].Interior.ColorIndex = 3;
                    }
                    // storing header part in Excel
                    //for (int l = 0; l < dt2.Columns.Count; l++)
                    //{

                    //    Sheet1.Cells[1, l + 1].Value = dt2.Columns[l].ColumnName;
                    //}
                    ////storing data to excel from table in Coressponding Rows and Columns
                    //for (int m = 0; m < dt2.Rows.Count; m++)//  ds.Tables[0].Rows.Count-2
                    //{
                    //    for (int j = 0; j < dt2.Columns.Count; j++) // - 2 //ds.Tables[0].Columns.Count
                    //    {
                    //        Sheet1.Cells[m + 2, j + 1].Value = dt2.Rows[m][j].ToString();//ds.Tables[0]
                    //    }
                    //}
                }
                else
                {
                    // Sheet1.Cells[2,2].Value = "No data present for " + Sheet1.Name;
                    Sheet1.Visible = XlSheetVisibility.xlSheetHidden;
                    //return;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                //Response.Write(ex.ToString());
            }
        }
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;
        FileName = "CutOffScoresByPriorityTotalList_" + monthDay + "_" + year;
        xlWorkBook.Worksheets[1].Activate();
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
        xlWorkBook.SaveAs(Response.OutputStream);
        Response.End();
    }
    protected void btnExportToExcelAll_Click(object sender, System.EventArgs e)
    {
        saveExcelAll();
    }

    public void populateMedianScore()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string tblHtml = string.Empty;

            tblHtml += "<table align='center' style='border:1px solid #cccccc; border-collapse:collapse; width:60%;'>";
            tblHtml += "<tr style='background-color:#FFFFCC;'>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Contest</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Median</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Median</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Median</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Total Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;' >Total Median</td>";

            tblHtml += "</tr>";


            // cmdText = " Select max(total_Cume_Count) FROM PercentilesByGrade Where Contestyear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and eventID=" + ddlEvent.SelectedValue + " Order by TotalScore";

            cmdText = "select productgroupid,productid,ProductCode,ContestYear, ContestPeriod, (select top 1 Max( total_Cume_Count) from PercentilesByGrade where ContestYear=PG.ContestYear and ProductCode=PG.ProductCode and EventID=PG.EventID and ContestPeriod in(Pg.ContestPeriod)) as TotalCount, (select top 1 Max( total_Cume_Count) from PercentilesByGrade where ContestYear=PG.ContestYear and ProductCode=PG.ProductCode and EventID=PG.EventID and ContestDate='All') as TotCount, ( Select top 1 TotalScore FROM PercentilesByGrade Where Contestyear=2016 and ProductCode=PG.ProductCode and eventID=PG.EventID and ContestPeriod in(PG.ContestPeriod)   group by TotalScore order by ABS(min( Convert(decimal,SUBSTRING(TotalPercentile,0,CHARINDEX('%', TotalPercentile)))-50)) ASC) as TotPercentile, ( Select top 1 TotalScore FROM PercentilesByGrade Where Contestyear=2016 and ProductCode=PG.ProductCode and eventID=2 and ContestDate='All'   group by TotalScore order by ABS(min( Convert(decimal,SUBSTRING(TotalPercentile,0,CHARINDEX('%', TotalPercentile)))-50)) ASC) as TotalMedian from PercentilesByGrade PG Where Contestyear=" + ddlYear.SelectedValue + " and eventID=" + ddlEvent.SelectedValue + " ";

            if (ddlContest.SelectedValue != "All" && ddlcontestdate.SelectedItem.Text != "All")
            {
                cmdText += " and PG.ProductCode='" + ddlContest.SelectedItem.Text + "' and PG.ContestPeriod in (" + ddlcontestdate.SelectedValue + ")";
            }
            else if (ddlContest.SelectedValue != "All" && ddlcontestdate.SelectedItem.Text == "All")
            {
                cmdText += " and PG.ProductCode='" + ddlContest.SelectedItem.Text + "'";
            }
            else if (ddlContest.SelectedValue == "All" && ddlcontestdate.SelectedItem.Text != "All")
            {
                cmdText += " and PG.ContestPeriod in (" + ddlcontestdate.SelectedValue + ")";
            }
            cmdText += "Group by productgroupid,productid, ContestPeriod, ProductCode, ContestYear, EventID order by productGroupID,ProductID";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExportExcel.Visible = true;
                    spnMedianTitle.InnerText = "Table 1: Median Scores by Contest and Date for Chapter Contests";
                    string productCode = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string prdCode = dr["ProductCode"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;
                        if (productCode != dr["ProductCode"].ToString())
                        {
                            tblHtml += "<tr>";
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr["ProductCode"].ToString() + "</td>";
                            int i = 0;
                            string contPeriod = string.Empty;
                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (i < 3)
                                {
                                    if (prdCode == dr1["ProductCode"].ToString())
                                    {
                                        contPeriod = dr1["ContestPeriod"].ToString();
                                        if (i == 0 && dr1["ContestPeriod"].ToString() == "2")
                                        {
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                        }
                                        if (i == 0 && dr1["ContestPeriod"].ToString() == "3")
                                        {
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                        }
                                        if (ds.Tables[0].Rows.Count > 1)
                                        {
                                            if (i == 0 && dr1["ContestPeriod"].ToString() == "1")
                                            {
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotalCount"].ToString() + "</td>";

                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotPercentile"].ToString() + "</td>";
                                            }
                                            else if (i == 1 && dr1["ContestPeriod"].ToString() == "2")
                                            {
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotalCount"].ToString() + "</td>";

                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotPercentile"].ToString() + "</td>";
                                            }
                                            else if (i == 2 && dr1["ContestPeriod"].ToString() == "3")
                                            {
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotalCount"].ToString() + "</td>";

                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotPercentile"].ToString() + "</td>";
                                            }
                                            else
                                            {
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            }
                                        }
                                        else
                                        {
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotalCount"].ToString() + "</td>";

                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr1["TotPercentile"].ToString() + "</td>";
                                        }
                                        if (i == 0 && dr1["ContestPeriod"].ToString() == "2")
                                        {
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                        }

                                        //else
                                        //{
                                        //    tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                        //    tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                        //}

                                        i++;
                                    }

                                }

                            }
                            if (contPeriod == "1" && i == 1)
                            {
                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                                tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                            }
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr["TotCount"].ToString() + "</td>";
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + dr["TotalMedian"].ToString() + "</td>";
                            tblHtml += "</tr>";
                        }
                        productCode = dr["ProductCode"].ToString();

                    }

                }
            }
            tblHtml += "</table>";
            ltrMedianScore.Text = tblHtml;
        }
        catch
        {
        }
    }


    public void ExportToExcelTable1()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string tblHtml = string.Empty;


            cmdText = "select productgroupid,productid,ProductCode,ContestYear, ContestPeriod, (select top 1 Max( total_Cume_Count) from PercentilesByGrade where ContestYear=PG.ContestYear and ProductCode=PG.ProductCode and EventID=PG.EventID and ContestPeriod in(Pg.ContestPeriod)) as TotalCount, (select top 1 Max( total_Cume_Count) from PercentilesByGrade where ContestYear=PG.ContestYear and ProductCode=PG.ProductCode and EventID=PG.EventID and ContestDate='All') as TotCount, ( Select top 1 TotalScore FROM PercentilesByGrade Where Contestyear=2016 and ProductCode=PG.ProductCode and eventID=PG.EventID and ContestPeriod in(PG.ContestPeriod)   group by TotalScore order by ABS(min( Convert(decimal,SUBSTRING(TotalPercentile,0,CHARINDEX('%', TotalPercentile)))-50)) ASC) as TotPercentile, ( Select top 1 TotalScore FROM PercentilesByGrade Where Contestyear=2016 and ProductCode=PG.ProductCode and eventID=2 and ContestDate='All'   group by TotalScore order by ABS(min( Convert(decimal,SUBSTRING(TotalPercentile,0,CHARINDEX('%', TotalPercentile)))-50)) ASC) as TotalMedian from PercentilesByGrade PG Where Contestyear=" + ddlYear.SelectedValue + " and eventID=" + ddlEvent.SelectedValue + " ";

            if (ddlContest.SelectedValue != "All" && ddlcontestdate.SelectedItem.Text != "All")
            {
                cmdText += " and PG.ProductCode='" + ddlContest.SelectedItem.Text + "' and PG.ContestPeriod in (" + ddlcontestdate.SelectedValue + ")";
            }
            else if (ddlContest.SelectedValue != "All" && ddlcontestdate.SelectedItem.Text == "All")
            {
                cmdText += " and PG.ProductCode='" + ddlContest.SelectedItem.Text + "'";
            }
            else if (ddlContest.SelectedValue == "All" && ddlcontestdate.SelectedItem.Text != "All")
            {
                cmdText += " and PG.ContestPeriod in (" + ddlcontestdate.SelectedValue + ")";
            }
            cmdText += "Group by productgroupid,productid, ContestPeriod, ProductCode, ContestYear, EventID order by productGroupID,ProductID";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Median Scores";

                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Median Scores by Contest and Date for Chapter Contests";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Contest";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "WK1 Count";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "WK1 Median";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "WK2 Count";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WK2 Median";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "WK3 Count";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "WK3 Median";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Total Count";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Total Median";
                    oSheet.Range["J3"].Font.Bold = true;
                    spnMedianTitle.InnerText = "Table 1: Median Scores by Contest and Date for Chapter Contests";
                    string productCode = string.Empty;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string prdCode = dr["ProductCode"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;
                        if (productCode != dr["ProductCode"].ToString())
                        {





                            CRange = oSheet.Range["A" + iRowIndex.ToString()];
                            CRange.Value = i + 1;

                            CRange = oSheet.Range["B" + iRowIndex.ToString()];
                            CRange.Value = dr["ProductCode"];


                            int j = 0;
                            string contPeriod = string.Empty;
                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (j < 3)
                                {
                                    if (prdCode == dr1["ProductCode"].ToString())
                                    {
                                        contPeriod = dr1["ContestPeriod"].ToString();
                                        if (j == 0 && dr1["ContestPeriod"].ToString() == "2")
                                        {
                                            CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                            CRange.Value = "";

                                            CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                            CRange.Value = "";


                                        }
                                        if (j == 0 && dr1["ContestPeriod"].ToString() == "3")
                                        {
                                            CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                            CRange.Value = "";

                                            CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                            CRange.Value = "";

                                            CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                            CRange.Value = "";

                                            CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                            CRange.Value = "";
                                        }
                                        if (ds.Tables[0].Rows.Count > 1)
                                        {
                                            if (j == 0 && dr1["ContestPeriod"].ToString() == "1")
                                            {

                                                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();

                                            }
                                            else if (j == 1 && dr1["ContestPeriod"].ToString() == "2")
                                            {
                                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();
                                            }
                                            else if (j == 2 && dr1["ContestPeriod"].ToString() == "3")
                                            {
                                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();
                                            }
                                            else
                                            {
                                                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                                CRange.Value = "";
                                            }
                                        }
                                        else
                                        {
                                            if (j == 0 && dr1["ContestPeriod"].ToString() == "1")
                                            {
                                                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();

                                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                                CRange.Value = "";
                                            }
                                            if (j == 0 && dr1["ContestPeriod"].ToString() == "2")
                                            {

                                                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();



                                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                                CRange.Value = "";
                                            }
                                            if (j == 0 && dr1["ContestPeriod"].ToString() == "3")
                                            {
                                                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                                CRange.Value = "";

                                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotalCount"].ToString();

                                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                                CRange.Value = dr1["TotPercentile"].ToString();




                                            }


                                        }
                                        if (j == 0 && dr1["ContestPeriod"].ToString() == "2")
                                        {

                                            CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                            CRange.Value = "";

                                            CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                            CRange.Value = "";
                                        }


                                        j++;
                                    }

                                }

                            }
                            if (contPeriod == "1" && j == 1)
                            {
                                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                                CRange.Value = "";

                                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                                CRange.Value = "";

                                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                                CRange.Value = "";

                                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                                CRange.Value = "";

                                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                                CRange.Value = "";
                            }



                            CRange = oSheet.Range["I" + iRowIndex.ToString()];
                            CRange.Value = dr["TotCount"].ToString();

                            CRange = oSheet.Range["J" + iRowIndex.ToString()];
                            CRange.Value = dr["TotalMedian"].ToString();




                            iRowIndex = iRowIndex + 1;
                            i = i + 1;


                        }
                        productCode = dr["ProductCode"].ToString();
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;
                    string FileName = "ReferralProgReport";

                    string filename = "MedianScores_" + ddlContest.SelectedItem.Text + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();
                }
            }



        }
        catch
        {
        }

    }




    protected void BtnExportExcel_Click(object sender, System.EventArgs e)
    {

        ExportToExcelTable1();
    }


    public void populateStdDeviation()
    {
        try
        {
            string strDate = string.Empty;
            strDate = ddlcontestdate.SelectedItem.Text;
            string contestDates = string.Empty;
            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDates = hdnContestDates.Value;
            }
            else
            {
                contestDates = ddlcontestdate.SelectedItem.Text;
            }
            string contestDateDay1 = string.Empty;
            string contestDateDay2 = string.Empty;
            string contestDateDay3 = string.Empty;
            string contestDateDay4 = string.Empty;
            string contestDateDay5 = string.Empty;
            string contestDateDay6 = string.Empty;

            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;

                contestDateDay3 = contestDates.Split(',')[2].ToString();
                contestDateDay4 = contestDates.Split(',')[3].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

                contestDateDay5 = contestDates.Split(',')[4].ToString();
                contestDateDay6 = contestDates.Split(',')[5].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "1")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "2")
            {
                contestDateDay3 = contestDates.Split(',')[0].ToString();
                contestDateDay4 = contestDates.Split(',')[1].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

            }
            else if (ddlcontestdate.SelectedValue == "3")
            {
                contestDateDay5 = contestDates.Split(',')[0].ToString();
                contestDateDay6 = contestDates.Split(',')[1].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            string contDateD1 = string.Empty;
            string contDateD2 = string.Empty;
            string contDateD3 = string.Empty;
            string contDateD4 = string.Empty;
            string contDateD5 = string.Empty;
            string contDateD6 = string.Empty;

            if (contestDateDay1 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay1.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay2.ToString());
                contDateD1 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD2 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD1 = "'" + contDateD1 + "'" + "," + "'" + contDateD2 + "'";
            }
            else
            {
                contDateD1 = "''";
            }

            if (contestDateDay3 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay3.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay4.ToString());
                contDateD3 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD4 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD3 = "'" + contDateD3 + "'" + "," + "'" + contDateD4 + "'";

            }
            else
            {
                contDateD3 = "''";
            }
            if (contestDateDay5 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay5.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay6.ToString());
                contDateD5 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD6 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD5 = "'" + contDateD5 + "'" + "," + "'" + contDateD6 + "'";
            }
            else
            {
                contDateD5 = "''";
            }

            string cmdText = string.Empty;
            string tblHtml = string.Empty;
            ArrayList arrProduct = new ArrayList();
            arrProduct.Add("JSB");
            arrProduct.Add("SSB");
            arrProduct.Add("JVB");
            arrProduct.Add("IVB");
            arrProduct.Add("MB1");
            arrProduct.Add("MB2");
            arrProduct.Add("MB3");
            arrProduct.Add("JGB");
            arrProduct.Add("SGB");
            arrProduct.Add("JSC");
            arrProduct.Add("ISC");
            arrProduct.Add("SSC");

            tblHtml += "<table align='center' style='border:1px solid #cccccc; border-collapse:collapse; width:60%;'>";
            tblHtml += "<tr style='background-color:#FFFFCC;'>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Contest</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2  Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Total Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;' >Total Std Dev</td>";

            tblHtml += "</tr>";


            cmdText = "select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD1 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD1 + ") )) and ContestDate in(" + contDateD1 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += "; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD3 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD3 + ") )) and ContestDate in(" + contDateD3 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD5 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD5 + ") )) and ContestDate in(" + contDateD5 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29)) or  (Score1>0 and CT.ProductId in(52,53,54)))  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3') ";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if ((ds.Tables[0].Rows.Count > 0) || (ds.Tables[1].Rows.Count > 0) || (ds.Tables[2].Rows.Count > 0) || (ds.Tables[3].Rows.Count > 0))
                {
                    BtnExportToExcel2.Visible = true;
                    int i = 0;

                    string productID = string.Empty;
                    DataTable dtable = new DataTable();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dtable = ds.Tables[0];
                    }
                    else if (ds.Tables[1].Rows.Count > 0)
                    {
                        dtable = ds.Tables[1];
                    }
                    else if (ds.Tables[2].Rows.Count > 0)
                    {
                        dtable = ds.Tables[2];
                    }
                    else if (ds.Tables[3].Rows.Count > 0)
                    {
                        dtable = ds.Tables[3];
                    }
                    foreach (DataRow dr in dtable.Rows)
                    {
                        tblHtml += "<tr>";
                        string prdID = dr["ProductID"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;
                        double stdDev = 0;
                        stdDev = Math.Round(stdDev, 2);
                        if (ddlContest.SelectedItem.Text == "All")
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + arrProduct[i].ToString() + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ddlContest.SelectedItem.Text + "</td>";
                        }
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[0].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[0].Rows[i]["totCount"].ToString() + "</td>";
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[1].Rows[i]["totCount"].ToString() + "</td>";
                            stdDev = Convert.ToDouble(ds.Tables[1].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }

                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[2].Rows[i]["totCount"].ToString() + "</td>";

                            stdDev = Convert.ToDouble(ds.Tables[2].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }
                        tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[3].Rows[i]["totCount"].ToString() + "</td>";

                        stdDev = Convert.ToDouble(ds.Tables[3].Rows[i]["StdDev"].ToString());
                        stdDev = Math.Round(stdDev, 2);
                        tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        i++;
                        tblHtml += "</tr>";
                    }

                }
            }
            spnTable2.InnerText = "Table 2.  Standard Deviation of the Means by Contests and Date for Chapter Contests ";
            tblHtml += "</table>";
            ltrTable2.Text = tblHtml;
        }
        catch
        {
        }
    }

    public void populateMean()
    {
        try
        {
            string strDate = string.Empty;
            strDate = ddlcontestdate.SelectedItem.Text;
            string contestDates = string.Empty;
            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDates = hdnContestDates.Value;
            }
            else
            {
                contestDates = ddlcontestdate.SelectedItem.Text;
            }
            string contestDateDay1 = string.Empty;
            string contestDateDay2 = string.Empty;
            string contestDateDay3 = string.Empty;
            string contestDateDay4 = string.Empty;
            string contestDateDay5 = string.Empty;
            string contestDateDay6 = string.Empty;

            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;

                contestDateDay3 = contestDates.Split(',')[2].ToString();
                contestDateDay4 = contestDates.Split(',')[3].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

                contestDateDay5 = contestDates.Split(',')[4].ToString();
                contestDateDay6 = contestDates.Split(',')[5].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "1")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "2")
            {
                contestDateDay3 = contestDates.Split(',')[0].ToString();
                contestDateDay4 = contestDates.Split(',')[1].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

            }
            else if (ddlcontestdate.SelectedValue == "3")
            {
                contestDateDay5 = contestDates.Split(',')[0].ToString();
                contestDateDay6 = contestDates.Split(',')[1].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            string contDateD1 = string.Empty;
            string contDateD2 = string.Empty;
            string contDateD3 = string.Empty;
            string contDateD4 = string.Empty;
            string contDateD5 = string.Empty;
            string contDateD6 = string.Empty;

            if (contestDateDay1 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay1.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay2.ToString());
                contDateD1 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD2 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD1 = "'" + contDateD1 + "'" + "," + "'" + contDateD2 + "'";
            }
            else
            {
                contDateD1 = "''";
            }

            if (contestDateDay3 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay3.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay4.ToString());
                contDateD3 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD4 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD3 = "'" + contDateD3 + "'" + "," + "'" + contDateD4 + "'";

            }
            else
            {
                contDateD3 = "''";
            }
            if (contestDateDay5 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay5.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay6.ToString());
                contDateD5 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD6 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD5 = "'" + contDateD5 + "'" + "," + "'" + contDateD6 + "'";
            }
            else
            {
                contDateD5 = "''";
            }






            string cmdText = string.Empty;
            string tblHtml = string.Empty;
            ArrayList arrProduct = new ArrayList();
            arrProduct.Add("JSB");
            arrProduct.Add("SSB");
            arrProduct.Add("JVB");
            arrProduct.Add("IVB");
            arrProduct.Add("MB1");
            arrProduct.Add("MB2");
            arrProduct.Add("MB3");
            arrProduct.Add("JGB");
            arrProduct.Add("SGB");
            arrProduct.Add("JSC");
            arrProduct.Add("ISC");
            arrProduct.Add("SSC");

            tblHtml += "<table align='center' style='border:1px solid #cccccc; border-collapse:collapse; width:60%;'>";
            tblHtml += "<tr style='background-color:#FFFFCC;'>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Contest</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Mean</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Mean</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Mean</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Total Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;' >Total Mean</td>";

            tblHtml += "</tr>";


            cmdText = "select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD1 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD1 + ") )) and ContestDate in(" + contDateD1 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += "; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD3 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD3 + ") )) and ContestDate in(" + contDateD3 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD5 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD5 + ") )) and ContestDate in(" + contDateD5 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29)) or  (Score1>0 and CT.ProductId in(52,53,54)))  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3') ";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if ((ds.Tables[0].Rows.Count > 0) || (ds.Tables[1].Rows.Count > 0) || (ds.Tables[2].Rows.Count > 0) || (ds.Tables[3].Rows.Count > 0))
                {
                    BtnExportToExcel3.Visible = true;
                    int i = 0;

                    string productID = string.Empty;
                    DataTable dtable = new DataTable();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dtable = ds.Tables[0];
                    }
                    else if (ds.Tables[1].Rows.Count > 0)
                    {
                        dtable = ds.Tables[1];
                    }
                    else if (ds.Tables[2].Rows.Count > 0)
                    {
                        dtable = ds.Tables[2];
                    }
                    else if (ds.Tables[3].Rows.Count > 0)
                    {
                        dtable = ds.Tables[3];
                    }
                    foreach (DataRow dr in dtable.Rows)
                    {
                        tblHtml += "<tr>";
                        string prdID = dr["ProductID"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;
                        double stdDev = 0;
                        stdDev = Math.Round(stdDev, 2);
                        if (ddlContest.SelectedItem.Text == "All")
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + arrProduct[i].ToString() + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ddlContest.SelectedItem.Text + "</td>";
                        }
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[0].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[0].Rows[i]["totCount"].ToString() + "</td>";
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[1].Rows[i]["totCount"].ToString() + "</td>";
                            stdDev = Convert.ToDouble(ds.Tables[1].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }

                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[2].Rows[i]["totCount"].ToString() + "</td>";

                            stdDev = Convert.ToDouble(ds.Tables[2].Rows[i]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        }
                        else
                        {
                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";

                            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>&nbsp;</td>";
                        }
                        tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + ds.Tables[3].Rows[i]["totCount"].ToString() + "</td>";

                        stdDev = Convert.ToDouble(ds.Tables[3].Rows[i]["StdDev"].ToString());
                        stdDev = Math.Round(stdDev, 2);
                        tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc;'>" + String.Format("{0:0.0}", stdDev) + "</td>";
                        i++;
                        tblHtml += "</tr>";
                    }

                }
            }
            spnTable3.InnerText = "Table 3.  Mean of the Scores by Contests and Date for Chapter Contests";
            tblHtml += "</table>";
            ltrTable3.Text = tblHtml;
        }
        catch
        {
        }
    }

    public void ExportToExcelTable2()
    {
        try
        {

            string strDate = string.Empty;
            strDate = ddlcontestdate.SelectedItem.Text;
            string contestDates = string.Empty;
            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDates = hdnContestDates.Value;
            }
            else
            {
                contestDates = ddlcontestdate.SelectedItem.Text;
            }
            string contestDateDay1 = string.Empty;
            string contestDateDay2 = string.Empty;
            string contestDateDay3 = string.Empty;
            string contestDateDay4 = string.Empty;
            string contestDateDay5 = string.Empty;
            string contestDateDay6 = string.Empty;

            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;

                contestDateDay3 = contestDates.Split(',')[2].ToString();
                contestDateDay4 = contestDates.Split(',')[3].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

                contestDateDay5 = contestDates.Split(',')[4].ToString();
                contestDateDay6 = contestDates.Split(',')[5].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "1")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "2")
            {
                contestDateDay3 = contestDates.Split(',')[0].ToString();
                contestDateDay4 = contestDates.Split(',')[1].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

            }
            else if (ddlcontestdate.SelectedValue == "3")
            {
                contestDateDay5 = contestDates.Split(',')[0].ToString();
                contestDateDay6 = contestDates.Split(',')[1].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            string contDateD1 = string.Empty;
            string contDateD2 = string.Empty;
            string contDateD3 = string.Empty;
            string contDateD4 = string.Empty;
            string contDateD5 = string.Empty;
            string contDateD6 = string.Empty;

            if (contestDateDay1 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay1.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay2.ToString());
                contDateD1 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD2 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD1 = "'" + contDateD1 + "'" + "," + "'" + contDateD2 + "'";
            }
            else
            {
                contDateD1 = "''";
            }

            if (contestDateDay3 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay3.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay4.ToString());
                contDateD3 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD4 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD3 = "'" + contDateD3 + "'" + "," + "'" + contDateD4 + "'";

            }
            else
            {
                contDateD3 = "''";
            }
            if (contestDateDay5 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay5.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay6.ToString());
                contDateD5 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD6 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD5 = "'" + contDateD5 + "'" + "," + "'" + contDateD6 + "'";
            }
            else
            {
                contDateD5 = "''";
            }

            string cmdText = string.Empty;
            string tblHtml = string.Empty;
            ArrayList arrProduct = new ArrayList();
            arrProduct.Add("JSB");
            arrProduct.Add("SSB");
            arrProduct.Add("JVB");
            arrProduct.Add("IVB");
            arrProduct.Add("MB1");
            arrProduct.Add("MB2");
            arrProduct.Add("MB3");
            arrProduct.Add("JGB");
            arrProduct.Add("SGB");
            arrProduct.Add("JSC");
            arrProduct.Add("ISC");
            arrProduct.Add("SSC");

            tblHtml += "<table align='center' style='border:1px solid #cccccc; border-collapse:collapse; width:60%;'>";
            tblHtml += "<tr style='background-color:#FFFFCC;'>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Contest</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK1 Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK2  Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>WK3 Std Dev</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;'>Total Count</td>";
            tblHtml += "<td style='border-collapse:collapse; border:1px solid #cccccc; font-weight:bold; color:Green; text-align:center;' >Total Std Dev</td>";

            tblHtml += "</tr>";


            cmdText = "select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD1 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD1 + ") )) and ContestDate in(" + contDateD1 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += "; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD3 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD3 + ") )) and ContestDate in(" + contDateD3 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD5 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD5 + ") )) and ContestDate in(" + contDateD5 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, STDEV(Score1)/SQRT(COUNT(*)-1)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29)) or  (Score1>0 and CT.ProductId in(52,53,54)))  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3') ";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if ((ds.Tables[0].Rows.Count > 0) || (ds.Tables[1].Rows.Count > 0) || (ds.Tables[2].Rows.Count > 0) || (ds.Tables[3].Rows.Count > 0))
                {
                    BtnExportToExcel2.Visible = true;


                    DataTable dtable = new DataTable();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dtable = ds.Tables[0];
                    }
                    else if (ds.Tables[1].Rows.Count > 0)
                    {
                        dtable = ds.Tables[1];
                    }
                    else if (ds.Tables[2].Rows.Count > 0)
                    {
                        dtable = ds.Tables[2];
                    }
                    else if (ds.Tables[3].Rows.Count > 0)
                    {
                        dtable = ds.Tables[3];
                    }


                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Standard Deviation of the Means";

                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Standard Deviation of the Means by Contests and Date for Chapter Contests";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Contest";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "WK1 Count";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "WK1 Std Dev";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "WK2 Count";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WK2 Std Dev";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "WK3 Count";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "WK3 Std Dev";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Total Count";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Total Std Dev";
                    oSheet.Range["J3"].Font.Bold = true;

                    string productCode = string.Empty;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;


                    int j = 0;

                    string productID = string.Empty;
                    foreach (DataRow dr in dtable.Rows)
                    {
                        tblHtml += "<tr>";
                        string prdID = dr["ProductID"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;


                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        if (ddlContest.SelectedItem.Text == "All")
                        {
                            CRange.Value = arrProduct[j].ToString();
                        }
                        else
                        {
                            CRange.Value = ddlContest.SelectedItem.Text;
                        }

                        double stdDev = 0;
                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[0].Rows[j]["totCount"].ToString();

                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[0].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[1].Rows[j]["totCount"].ToString();
                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[1].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[2].Rows[j]["totCount"].ToString();
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[2].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = ds.Tables[3].Rows[j]["totCount"].ToString();

                        stdDev = Convert.ToDouble(ds.Tables[3].Rows[j]["StdDev"].ToString());
                        stdDev = Math.Round(stdDev, 2);
                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = String.Format("{0:0.0}", stdDev);


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;

                        j++;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "StandardDeviation_" + ddlContest.SelectedItem.Text + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();
                }
            }

        }
        catch
        {
        }


    }

    public void ExportToExcelTable3()
    {
        try
        {
            string strDate = string.Empty;
            strDate = ddlcontestdate.SelectedItem.Text;
            string contestDates = string.Empty;
            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDates = hdnContestDates.Value;
            }
            else
            {
                contestDates = ddlcontestdate.SelectedItem.Text;
            }
            string contestDateDay1 = string.Empty;
            string contestDateDay2 = string.Empty;
            string contestDateDay3 = string.Empty;
            string contestDateDay4 = string.Empty;
            string contestDateDay5 = string.Empty;
            string contestDateDay6 = string.Empty;

            if (ddlcontestdate.SelectedItem.Text == "All")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;

                contestDateDay3 = contestDates.Split(',')[2].ToString();
                contestDateDay4 = contestDates.Split(',')[3].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

                contestDateDay5 = contestDates.Split(',')[4].ToString();
                contestDateDay6 = contestDates.Split(',')[5].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "1")
            {
                contestDateDay1 = contestDates.Split(',')[0].ToString();
                contestDateDay2 = contestDates.Split(',')[1].ToString();
                contestDateDay1 = contestDateDay1 + "," + ddlYear.SelectedValue;
                contestDateDay2 = contestDateDay2 + "," + ddlYear.SelectedValue;
            }
            else if (ddlcontestdate.SelectedValue == "2")
            {
                contestDateDay3 = contestDates.Split(',')[0].ToString();
                contestDateDay4 = contestDates.Split(',')[1].ToString();
                contestDateDay3 = contestDateDay3 + "," + ddlYear.SelectedValue;
                contestDateDay4 = contestDateDay4 + "," + ddlYear.SelectedValue;

            }
            else if (ddlcontestdate.SelectedValue == "3")
            {
                contestDateDay5 = contestDates.Split(',')[0].ToString();
                contestDateDay6 = contestDates.Split(',')[1].ToString();
                contestDateDay5 = contestDateDay5 + "," + ddlYear.SelectedValue;
                contestDateDay6 = contestDateDay6 + "," + ddlYear.SelectedValue;
            }
            string contDateD1 = string.Empty;
            string contDateD2 = string.Empty;
            string contDateD3 = string.Empty;
            string contDateD4 = string.Empty;
            string contDateD5 = string.Empty;
            string contDateD6 = string.Empty;

            if (contestDateDay1 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay1.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay2.ToString());
                contDateD1 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD2 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD1 = "'" + contDateD1 + "'" + "," + "'" + contDateD2 + "'";
            }
            else
            {
                contDateD1 = "''";
            }

            if (contestDateDay3 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay3.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay4.ToString());
                contDateD3 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD4 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD3 = "'" + contDateD3 + "'" + "," + "'" + contDateD4 + "'";

            }
            else
            {
                contDateD3 = "''";
            }
            if (contestDateDay5 != "")
            {
                DateTime dtD1 = Convert.ToDateTime(contestDateDay5.ToString());
                DateTime dtD2 = Convert.ToDateTime(contestDateDay6.ToString());
                contDateD5 = dtD1.Month + "/" + dtD1.Day + "/" + dtD1.Year;
                contDateD6 = dtD2.Month + "/" + dtD2.Day + "/" + dtD2.Year;
                contDateD5 = "'" + contDateD5 + "'" + "," + "'" + contDateD6 + "'";
            }
            else
            {
                contDateD5 = "''";
            }


            string cmdText = string.Empty;
            string tblHtml = string.Empty;
            ArrayList arrProduct = new ArrayList();
            arrProduct.Add("JSB");
            arrProduct.Add("SSB");
            arrProduct.Add("JVB");
            arrProduct.Add("IVB");
            arrProduct.Add("MB1");
            arrProduct.Add("MB2");
            arrProduct.Add("MB3");
            arrProduct.Add("JGB");
            arrProduct.Add("SGB");
            arrProduct.Add("JSC");
            arrProduct.Add("ISC");
            arrProduct.Add("SSC");




            cmdText = "select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD1 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD1 + ") )) and ContestDate in(" + contDateD1 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += "; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD3 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD3 + ") )) and ContestDate in(" + contDateD3 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29) and ContestDate in(" + contDateD5 + ")) or (Score1>0 and CT.ProductId in(52,53,54) and ContestDate in(" + contDateD5 + ") )) and ContestDate in(" + contDateD5 + ")  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3')";

            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            cmdText += " ; select COUNT(*) as totCount, CT.ProductId, Sum(Score1+score2)/COUNT(*)  as StdDev from Contestant CT inner join Contest C on (CT.ContestYear=" + ddlYear.SelectedValue + " and CT.EventId=2 and CT.ProductId=C.ProductId and CT.ContestID=C.ContestID) where CT.ContestYear=2016 and ((Score1+Score2>0 and CT.ProductId in(19,20,21,22,24,25,26,28,29)) or  (Score1>0 and CT.ProductId in(52,53,54)))  and CT.ProductCode not in('EW1','EW2','EW3','PS1','PS3') ";
            if (ddlContest.SelectedItem.Text != "All")
            {
                cmdText += " and CT.ProductID=" + ddlContest.SelectedValue + "";
            }
            cmdText += " group by CT.ProductId order by CT.ProductId";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if ((ds.Tables[0].Rows.Count > 0) || (ds.Tables[1].Rows.Count > 0) || (ds.Tables[2].Rows.Count > 0) || (ds.Tables[3].Rows.Count > 0))
                {
                    DataTable dtable = new DataTable();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dtable = ds.Tables[0];
                    }
                    else if (ds.Tables[1].Rows.Count > 0)
                    {
                        dtable = ds.Tables[1];
                    }
                    else if (ds.Tables[2].Rows.Count > 0)
                    {
                        dtable = ds.Tables[2];
                    }
                    else if (ds.Tables[3].Rows.Count > 0)
                    {
                        dtable = ds.Tables[3];
                    }


                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Mean of the Scores";

                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Mean of the Scores by Contests and Date for Chapter Contests";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Contest";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "WK1 Count";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "WK1 Mean";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "WK2 Count";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WK2 Mean";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "WK3 Count";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "WK3 Mean";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Total Count";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Total Mean";
                    oSheet.Range["J3"].Font.Bold = true;

                    string productCode = string.Empty;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;


                    int j = 0;

                    string productID = string.Empty;
                    foreach (DataRow dr in dtable.Rows)
                    {
                        tblHtml += "<tr>";
                        string prdID = dr["ProductID"].ToString();
                        int totCount = 0;
                        int totPercentCount = 0;


                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        if (ddlContest.SelectedItem.Text == "All")
                        {
                            CRange.Value = arrProduct[j].ToString();
                        }
                        else
                        {
                            CRange.Value = ddlContest.SelectedItem.Text;
                        }

                        double stdDev = 0;
                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[0].Rows[j]["totCount"].ToString();

                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[0].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);
                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[1].Rows[j]["totCount"].ToString();
                        }
                        else
                        {
                            CRange.Value = "";
                        }
                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[1].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            CRange.Value = ds.Tables[2].Rows[j]["totCount"].ToString();
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            stdDev = Convert.ToDouble(ds.Tables[2].Rows[j]["StdDev"].ToString());
                            stdDev = Math.Round(stdDev, 2);

                            CRange.Value = String.Format("{0:0.0}", stdDev);
                        }
                        else
                        {
                            CRange.Value = "";
                        }

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = ds.Tables[3].Rows[j]["totCount"].ToString();

                        stdDev = Convert.ToDouble(ds.Tables[3].Rows[j]["StdDev"].ToString());
                        stdDev = Math.Round(stdDev, 2);
                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = String.Format("{0:0.0}", stdDev);


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;

                        j++;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "MeanOfTheScores_" + ddlContest.SelectedItem.Text + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }

    protected void BtnExportToExcel2_Click(object sender, System.EventArgs e)
    {

        ExportToExcelTable2();
    }

    protected void BtnExportToExcel3_Click(object sender, System.EventArgs e)
    {

        ExportToExcelTable3();
    }
}

