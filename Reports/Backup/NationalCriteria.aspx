<%@ Page Language="VB" AutoEventWireup="true" CodeFile="NationalCriteria.aspx.vb"
    Inherits="Reports_NationalCriteria" Title="National Selection Criteria" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">


    <%--  <%@ Page Language="VB" AutoEventWireup="false" CodeFile="NationalCriteria.aspx.vb" EnableEventValidation="false" Inherits="Reports_NationalCriteria" %>--%>



    <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:HyperLink>

    <center>
        <h2>Criteria for National Invitees by Contest and Grade
            <asp:Label ID="lblYear" runat="server"></asp:Label></h2>
    </center>
    <br />

    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblPriority" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblUpdateMsg" runat="server" ForeColor="Red"></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnSave" runat="server" Text="Edit/Save Criteria" OnClick="btnSave_Click" />
    <asp:Button ID="btnCalCulate" Text="View/Calc Invitee Counts" OnClick="btnCalculate_Click" runat="server" Visible="True" Width="157px" />
    <asp:Button ID="btnNewChapter" runat="server" OnClick="btnNewChapter_Click" Text="Edit/Save New Chapters" Width="160px" />
    <asp:DropDownList ID="ddlInvite" runat="server" AutoPostBack="true" Width="125px" Style="margin-top: 0px" Visible="false">
        <asp:ListItem Value="0">Select</asp:ListItem>
        <asp:ListItem Value="1">Use Priority List</asp:ListItem>
        <asp:ListItem Value="2">Use Total List</asp:ListItem>
    </asp:DropDownList>
    <%--        <asp:Button ID="BtnUpdateCnst1" runat="server" Text="Set Priority Invitee Flags" OnClick="BtnUpdateCnst1_Click" Visible="false" Width="175px" /> 
    --%>
    <asp:Button ID="btnUpdateCnst" runat="server" Text="Update Total Invite/Priority Columns" OnClick="btnUpdateCnst_Click" Visible="false" Width="232px" />&nbsp; 
        <asp:Button ID="btnY2Y" runat="server" Text="YearToYear Comparison" OnClick="btnYear2Year_Click" Width="170px" Visible="True" />
    <asp:Button ID="btnTies" runat="server" Text="Solve for Ties" OnClick="btnTies_Click" Width="120px" Visible="False" />
    <asp:DropDownList ID="ddlExport" runat="server" Visible="True">
        <asp:ListItem Enabled="false">Export Criteria</asp:ListItem>
        <asp:ListItem Enabled="false">Export Invitee Count</asp:ListItem>
        <asp:ListItem Enabled="false">Export Total Invitee List</asp:ListItem>
        <asp:ListItem Enabled="false">Export Priority Invitee List</asp:ListItem>
        <asp:ListItem Enabled="false">Export YearToYear</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="BtnExport" runat="server" Text="Export" OnClick="BtnExport_Click" Width="100px" Visible="True" />

    <%--    <asp:Button ID="btnExport" runat="server" Text="Export Invitee Counts" OnClick="BtnExport_Click" Width="160px" Visible="False"  /> 
        <asp:Button ID="btnExport1" runat="server" Text="Export Criteria" OnClick="BtnExport1_Click" Width="120px"  Visible="True"  /> 
        <asp:Button ID="btnExportInvitee" runat="server" Text="Export Total Invitee List" OnClick ="btnExportInvitee_Click" Width="153px"  Visible="False" /> 
        <asp:Button ID="btnExportInviteePriority" runat="server" Text="Export Priority Invitee List" OnClick ="btnExportInviteePriority_Click" Width="168px"  Visible="False" /> 
        <asp:Button ID="btnExportY2Y" runat="server" Text="Export YearToYear " OnClick="btnExportY2Y_Click" Width="150px"  Visible="False" /> 
    --%>
    <br />
    <br />
    <asp:DropDownList ID="ddlNatInvite" runat="server" Width="125px" Style="margin-top: 0px" Visible="False">
        <asp:ListItem Value="1">Use Priority List</asp:ListItem>
        <asp:ListItem Value="2">Use Total List</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="btnNatInvite" runat="server" Text="Set Nat�l Invitee Flag to Register" Width="216px" Visible="False" />
    <asp:Label ID="lblerr" runat="server" Text="" ForeColor="Red"></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnLoadInviteeCount" runat="server" Text="Load Invitee Count" OnClick="btnLoadInviteeCount_Click" Width="130px" />
    <asp:Button ID="btnSaveInviteeCount" runat="server" Visible="false" Text="Save Invitee Count" OnClick="btnSaveInviteeCount_Click" Width="130px" /><br />
    <br />
    Notes: - 
             <ol style="color: Green">
                 <li>Year to Year will show up only after <b>Save the Invitee Count.</b></li>
                 <li><b>Save the Invitee Count</b> will show up after <b>View/Export Invitee Count.</b></li>
             </ol>
    <br />

    <asp:Panel ID="pnlNewChapter" runat="server" Visible="false">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td colspan="3" style="text-align: center"><b>New Chapter Flag Set</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td align="center">Selected Chapters</td>
            </tr>
            <tr>
                <td>
                    <asp:ListBox ID="listAll" runat="server" Height="300px" Width="150px" SelectionMode="Multiple"></asp:ListBox>
                </td>
                <td style="vertical-align: middle; text-align: center">
                    <asp:Button ID="BtnAdd" runat="server" Text="(Move)>>" OnClick="BtnAdd_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnDelete" runat="server" Text="Delete" OnClick="BtnDelete_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnDeleteAll" runat="server" Text="Delete All" OnClick="BtnDeleteAll_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnUpdate" runat="server" Text="Save New Chapters" OnClick="BtnUpdate_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="btnClose" runat="server" Text="Close Panel" OnClick="btnClose_Click" Width="120px" />

                </td>
                <td>
                    <asp:ListBox ID="ListSelected" runat="server" Height="300px" Width="150px" SelectionMode="Multiple"></asp:ListBox></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <strong><span style="font-size: 12px; color: #ff3333; font-family: Calibri">* Always Press Update Chapters after making changes</span> </strong>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
    <table>
        <tr>
            <td>
                
                <div>
                    <asp:Button ID="BtnLoadCutOffScores" runat="server" Text="Load Cutoff Scores" OnClick="BtnLoadCutOffScores_Click" Style="position: relative; left: 310px;" />
                </div>
                <div style="clear: both; margin-bottom: 5px;"></div>
                <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Total List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Width="200px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <%--<asp:BoundField DataField="Contest" HeaderText="Contest" HeaderStyle-ForeColor="white"  />--%>
                        <asp:TemplateField HeaderText="Contest" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtContest" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblContest" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimum Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Width="50px" Text='<%# Bind("MinScore") %>'></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rank1Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtRank1Score1" Width="50px" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRank1Score" runat="server" Width="50px" Text='<%# Bind("Rank1Score") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="New Chapter Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNewChScore1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewChScore" runat="server" Width="50px" Text='<%# Bind("NewChScore") %>'></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MaxScore" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMaxScore1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMaxScore" runat="server" Text='<%# Bind("MaxScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="White" Height="2px" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" Font-Size="20px" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 50px"></td>
            <td>
                <asp:GridView ID="GVPriority" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Priority List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Width="200px" Style="position: relative; top: 28px;">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <%--<asp:BoundField DataField="Contest" HeaderText="Contest" HeaderStyle-ForeColor="white"  />--%>
                        <asp:TemplateField HeaderText="Contest" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtContest_pr" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblContest_pr" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGrade_pr" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGrade_pr" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimum Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2_pr" runat="server" Text='<%# Bind("MinScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rank1Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtRank1Score1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRank1Score_pr" runat="server" Text='<%# Bind("Rank1Score") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="New Chapter Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNewChScore1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewChScore_pr" runat="server" Text='<%# Bind("NewChScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MaxScore" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMaxScore1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMaxScore_pr" runat="server" Text='<%# Bind("MaxScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvOut" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Total List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Count" HeaderText="Count" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumCount" HeaderText="Cumulative" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Contestants" HeaderText="# of Contestants" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="Invited" HeaderText="% Invited" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="AddRank1" HeaderText="Incr. Rank 1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="nwchcount" HeaderText="Incr. New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumulWrank1" HeaderStyle-Font-Size="10px" HeaderText="Cum with Rank 1/New Chap" HeaderStyle-ForeColor="white" ControlStyle-Width="25px" />
                        <asp:BoundField DataField="AvgScore" HeaderText="Avg.Score" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 50px">
                <asp:GridView ID="gvRatio" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Ratio"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="Ratio1" HeaderText="Ratio1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Ratio2" HeaderText="Ratio2" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />

                </asp:GridView>
            </td>
            <td>
                <asp:GridView ID="gvOutPriority" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Priority List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Count" HeaderText="Count" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumCount" HeaderText="Cumulative" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Contestants" HeaderText="# of Contestants" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="Invited" HeaderText="% Invited" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField DataField="AddRank1" HeaderText="Incr. Rank 1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="nwchcount" HeaderText="Incr. New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumulWrank1" HeaderText="Cum with Rank 1/New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="AvgScore" HeaderText="Avg.Score" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnY2YHelp" runat="server" Text="Help" Visible="false" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvY2Y" runat="server" AutoGenerateColumns="False" BackColor="White"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Invitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ExRegis" HeaderText="Exp_Reg" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ExpShows" HeaderText="Exp_Shows" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="lastMinScore" HeaderText="Last_Min_Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LasInvitees" HeaderText="Last_Invitees" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LastRegis" HeaderText="Last_Registrations" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LastShows" HeaderText="Last_Shows" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="RegisPers" HeaderText="Registrations %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ShowsPers" HeaderText="Shows %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />

                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
            <td align="left" width="50px"></td>
            <td>
                <%-- <asp:GridView ID="gcY2YPriority" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#3366CC" BorderStyle="Solid"  BorderWidth="1px" 
        CellPadding="4" Visible="False" style="margin-top: 0px">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <Columns>
                <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Invitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ExRegis" HeaderText="Exp_Reg" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ExpShows" HeaderText="Exp_Shows" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="lastMinScore" HeaderText="Min_Score" HeaderStyle-ForeColor="white"  />
                <asp:BoundField DataField="LasInvitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="LastRegis" HeaderText="Registrations" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="LastShows" HeaderText="Shows"  HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="RegisPers" HeaderText="Registrations %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"  HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ShowsPers" HeaderText="Shows %"  DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />

            </Columns>
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        </asp:GridView>--%>
            </td>
        </tr>

        <tr>
            <td>
                <br />
                <br />
                <asp:Label ID="lblTieBreaker" runat="server"></asp:Label><br />
                <asp:GridView ID="GVtieBreaker" AutoGenerateColumns="false" runat="server" Caption="Rank1 Tie Contestants - Total List ">
                    <Columns>
                        <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                        <asp:BoundField DataField="contestant_id" HeaderText="contestant_id" />
                        <asp:BoundField DataField="TotalScore" HeaderText="TotalScore" />
                        <asp:BoundField DataField="Rank" HeaderText="Rank" />
                        <asp:BoundField DataField="GRADE" HeaderText="GRADE" />
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" />
                        <asp:BoundField DataField="ChapterID" HeaderText="ChapterID" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="left" width="50px"></td>
            <td>
                <br />
                <br />
                <asp:Label ID="lblTieBreakerRank" runat="server"></asp:Label><br />
                <asp:GridView ID="GVTieBreakerRank" AutoGenerateColumns="false" runat="server" Caption="Rank1 Tie Contestants - Priority List ">
                    <Columns>
                        <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                        <asp:BoundField DataField="contestant_id" HeaderText="contestant_id" />
                        <asp:BoundField DataField="TotalScore" HeaderText="TotalScore" />
                        <asp:BoundField DataField="Rank" HeaderText="Rank" />
                        <asp:BoundField DataField="GRADE" HeaderText="GRADE" />
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" />
                        <asp:BoundField DataField="ChapterID" HeaderText="ChapterID" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblContestant" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblContestants_Priority" runat="server" Text="" Visible="false"></asp:Label>
    <br />
</asp:Content>

<%--      </form>
</body>

</html>--%>