﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewAbsenteeList.aspx.vb" Inherits="ViewAbsenteeList" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left">
        <asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
  <table id="FacilityTable" runat="server" align="center" class="title04">
         <tr><td> View Absentee List  </td></tr>
  </table>
  
   <table id="SelTable" runat="server"  border="1" align="center">
             <tr><td align="left" width="100px">  ContestYear </td>
                 <td><asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
             <tr><td width="100px"> Event</td>
                 <td><asp:DropDownList ID="ddlEvent"  DataTextField="Name" DataValueField="EventId"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
             <tr><td align="left" width="100px">  Chapter </td>
                 <td><asp:DropDownList ID="ddlChapter" DataTextField="Chapter" DataValueField="ChapterId"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
             <tr><td width="100px">Contest</td>
                 <td><asp:DropDownList ID="ddlProductGroup"  DataTextField="Name" DataValueField="ProductGroupCode"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
             <tr><td align="left" width="100px">  Level </td>
                 <td><asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ContestID"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
             <tr><td align="left" width="100px">  Phase </td>
                 <td><asp:DropDownList ID="ddlPhase" AutoPostBack="true" runat="server" width="150px">
                 <asp:ListItem Text="0" Value ="0">Select Phase</asp:ListItem>
                 <asp:ListItem Text="1" Value ="1"></asp:ListItem>
                 <asp:ListItem Text="2" Value ="2"></asp:ListItem>
                 </asp:DropDownList></td></tr>
             <tr><td width="100px">RoomNumber</td>
                 <td><asp:DropDownList ID="ddlRoom"  DataTextField="Name" DataValueField="RoomNumber"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td></tr>
   </table><br/>
   <table id="ViewAbsTable" runat="server" align="center"> 
       <tr><td align="center"></td></tr> 
       <tr><td>
         <asp:Button ID="BtnViewList" runat="server" Text="View Absentee List" Enabled ="True" />
         <asp:Button ID="BtnCancel"   runat="server"  Text="Cancel" /></td></tr>
       <tr><td align="center" ><asp:Label ForeColor ="Red" ID="lblAbsentErr" runat="server" Visible="true" Text=""></asp:Label></td></tr>
   </table>
   
</asp:Content>

