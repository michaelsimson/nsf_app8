<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RegistrationReport.aspx.vb" Inherits="Reports_RegistrationReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
      <style type="text/css">
	._ctl0_NavLinks_0 { background-color:white;visibility:hidden;display:none;position:absolute;left:0px;top:0px; }
	._ctl0_NavLinks_1 { text-decoration:white; }
	._ctl0_NavLinks_2 {  }

</style>
</head>
<body>

    <form id="form1" runat="server">--%>
<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="Reports_RegistrationReport" CodeFile="RegistrationReport.aspx.vb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
    <asp:hyperlink id="hlinkParentRegistration" runat="server" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink><br />
    
    <table  style="color: White;font-size: 10pt;"   width ="100%" border ="1" cellpadding = "10" >
        <tr>
            <td style="height: 21px; width: 131px;" align="center"><a href ="RegistrationReport.aspx?Param=ByMonth">Registration by Month</a></td>
            <td style="width: 118px; height: 21px" align="center"><a href ="RegistrationReport.aspx?Param=ByDate">Registration by Day</a></td>
            <td style="width: 165px; height: 21px" align="center"><a href ="RegistrationReport.aspx?Param=ByContest">Registration by Contest</a></td>
            <td align="center" style="width: 165px; height: 21px" ><a href ="RegistrationReport.aspx?Param=ByChapter">Registration by Chapter</a></td>
        </tr>
    </table>
    <asp:Panel ID="PnlByMonth" runat="server" Visible="false" >
    <div>
        <table style="width: 806px" border="0">
            <tr>
                <td  style="width: 723px" align="center">
                    <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                        <tr>
                                        <td style="height: 19px; background-color: white;" colspan="2">
                                         <asp:Label ID="lblHeader" runat="server" Text="Registration by Month" Width="192px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                Select Month</td>
                            <td style="height: 19px">
                                <asp:DropDownList ID="ddMonth" runat="server">
                                    <asp:ListItem Value="0">--------------------</asp:ListItem>
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">December</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 82px" class="ItemLabel">
                                Select Year</td>
                            <td>
                                <asp:DropDownList ID="ddYear" runat="server" Width="105px">
                                    <asp:ListItem Value="0">--------------------</asp:ListItem>
                                    <asp:ListItem Value="2005">2005</asp:ListItem>
		        <asp:ListItem Value="2006">2006</asp:ListItem>
                                    <asp:ListItem Value="2007">2007</asp:ListItem>
                                    <asp:ListItem Value="2008">2008</asp:ListItem>
                                    <asp:ListItem Value="2009">2009</asp:ListItem>
                                    <asp:ListItem Value="2010">2010</asp:ListItem>
				    <asp:ListItem Value="2011">2011</asp:ListItem>
				     <asp:ListItem Value="2012">2012</asp:ListItem>
				     <asp:ListItem Value="2013">2013</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 82px">
                            </td>
                            <td>
                                <asp:Button ID="btnFind" runat="server" Text="Find" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 723px; height: 1px">
                    <asp:Button ID="btnExcel" runat="server" Text="ExportToExcel" /></td>
            </tr>
            <tr>
                <td style="width: 723px; height: 129px;">
                    <asp:gridview ID="dvRegistration" runat="server" Width="790px"></asp:gridview>
                    <asp:Label ID="lblNoData" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 723px; height: 18px;">
                </td>
            </tr>
        </table>
    </div>
    
    </asp:Panel> 
    
    
    
    <asp:Panel ID="PnlByDate" runat="server" Visible="false" >
    <div>
        <table width="100%">
					<tr width="100%" >
					<td >
					    <table style="width: 806px">
                                    <tr>
                                        <td style="width: 723px" align="center">
                                            <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                                                <tr>
                                                                <td style="height: 19px" bgcolor="white" colspan="2">
                                                                    <asp:Label ID="lblDayHeader" runat="server" Text="Registration by Day" Width="192px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                                        Select Month</td>
                                                    <td style="height: 19px">
                                                        <asp:DropDownList ID="ddlMonth" runat="server">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="1">January</asp:ListItem>
                                                            <asp:ListItem Value="2">February</asp:ListItem>
                                                            <asp:ListItem Value="3">March</asp:ListItem>
                                                            <asp:ListItem Value="4">April</asp:ListItem>
                                                            <asp:ListItem Value="5">May</asp:ListItem>
                                                            <asp:ListItem Value="6">June</asp:ListItem>
                                                            <asp:ListItem Value="7">July</asp:ListItem>
                                                            <asp:ListItem Value="8">August</asp:ListItem>
                                                            <asp:ListItem Value="9">September</asp:ListItem>
                                                            <asp:ListItem Value="10">October</asp:ListItem>
                                                            <asp:ListItem Value="11">November</asp:ListItem>
                                                            <asp:ListItem Value="12">December</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px" class="ItemLabel">
                                                        Select Year</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlYear" runat="server" Width="105px">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="2006">2006</asp:ListItem>
                                                            <asp:ListItem Value="2007">2007</asp:ListItem>
                                                            <asp:ListItem Value="2008">2008</asp:ListItem>
                                                            <asp:ListItem Value="2009">2009</asp:ListItem>
                                                            <asp:ListItem Value="2010">2010</asp:ListItem>
 							    <asp:ListItem Value="2011">2011</asp:ListItem>
 							     <asp:ListItem Value="2012">2012</asp:ListItem>
 							      <asp:ListItem Value="2013">2013</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px">
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnDayFind" runat="server" Text="Find" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 1px">
                                            <asp:Button ID="btndayExcel" runat="server" Text="ExportToExcel" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 129px;">
                                        <asp:gridview ID="dvRegistrationbyday" runat="server" Width="790px">
                                        </asp:gridview>
                                            <asp:Label ID="Label2" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 18px;">
                                        </td>
                                    </tr>
                                </table>
					    
                                
                          </td>
					</tr>
					
				</table>
					

    </div>
    
    </asp:Panel> 
    
    <asp:Panel ID="pnlByContest" runat="server" Visible="false" >
    <div>
        <table width="100%">
					<tr>
					<td >
					    <table style="width: 806px">
                                    <tr>
                                        <td style="width: 723px" align="center">
                                            <table style="width: 260px; height: 65px; background-color: silver;" border="1">
                                                <tr>
                                                                <td style="height: 19px" bgcolor="white" colspan="2">
                                                                    <asp:Label ID="lblContestHeader" runat="server"></asp:Label></td>                                                                    
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                                        Select Month</td>
                                                    <td style="height: 19px">
                                                        <asp:DropDownList ID="ddlContestMonth" runat="server">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="1">January</asp:ListItem>
                                                            <asp:ListItem Value="2">February</asp:ListItem>
                                                            <asp:ListItem Value="3">March</asp:ListItem>
                                                            <asp:ListItem Value="4">April</asp:ListItem>
                                                            <asp:ListItem Value="5">May</asp:ListItem>
                                                            <asp:ListItem Value="6">June</asp:ListItem>
                                                            <asp:ListItem Value="7">July</asp:ListItem>
                                                            <asp:ListItem Value="8">August</asp:ListItem>
                                                            <asp:ListItem Value="9">September</asp:ListItem>
                                                            <asp:ListItem Value="10">October</asp:ListItem>
                                                            <asp:ListItem Value="11">November</asp:ListItem>
                                                            <asp:ListItem Value="12">December</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px" class="ItemLabel">
                                                        Select Year</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlContestYear" runat="server" Width="105px" AutoPostBack="true">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="2006">2006</asp:ListItem>
                                                            <asp:ListItem Value="2007" Selected="True">2007</asp:ListItem>
                                                            <asp:ListItem Value="2008">2008</asp:ListItem>
                                                            <asp:ListItem Value="2009">2009</asp:ListItem>
                                                            <asp:ListItem Value="2010">2010</asp:ListItem>
				    <asp:ListItem Value="2011">2011</asp:ListItem>
				     <asp:ListItem Value="2012">2012</asp:ListItem>
				      <asp:ListItem Value="2013">2013</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px">
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnContestFind" runat="server" Text="Find" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 1px">
                                            <asp:Button ID="btnContestExcel" runat="server" Text="ExportToExcel" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 129px;">
                                        <asp:gridview ID="dvregistrationcontest" runat="server" Width="790px">
                                        </asp:gridview>
                                            <asp:Label ID="Label3" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 14px;">
                                        </td>
                                    </tr>
                                </table>                                
                          </td>
					</tr>
					
				</table>
    </div>
    </asp:Panel> 
    
    <asp:Panel ID="pnlByChapter" runat="server" Visible="false">
    <div>
        <table style="width: 100%">
        <tr>
            <td style="width: 723px" align="center">
                <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                    <tr>
                        <td style="height: 19px" bgcolor="white" colspan="2">
                            <asp:Label ID="lblChapterHeader" runat="server" Text="Registration by Chapter" Width="192px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                            Select Month</td>
                        <td align="left" style="height: 19px">
                            <asp:DropDownList ID="ddlChapterMonth" runat="server">
                                <asp:ListItem Value="0">--------------------</asp:ListItem>
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td  style="width: 82px" class="ItemLabel">
                            Select Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlChapterYear" runat="server" Width="105px">
                                <asp:ListItem Value="0">--------------------</asp:ListItem>
                                <asp:ListItem Value="2006">2006</asp:ListItem>
                                <asp:ListItem Value="2007">2007</asp:ListItem>
                                <asp:ListItem Value="2008">2008</asp:ListItem>
                                <asp:ListItem Value="2009">2009</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
				    <asp:ListItem Value="2011">2011</asp:ListItem>
				     <asp:ListItem Value="2012">2012</asp:ListItem>
				      <asp:ListItem Value="2013">2013</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                     <tr>
                        <td style="width: 82px" class="ItemLabel">Event 
                        </td>
                        <td> 
                            <asp:DropDownList ID="ddlChapterEvent" runat="server">
                                <asp:ListItem Selected="True" Value="2">Chapter Contests</asp:ListItem>
                                <asp:ListItem Value="1">Finals</asp:ListItem>
                            </asp:DropDownList>
                        </td> </tr> 
                    <tr>
                        <td style="width: 82px">
                        </td>
                        <td>
                            <asp:Button ID="btnChapterFind" runat="server" Text="Find" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 723px; height: 1px">
                <asp:Button ID="btnChapterExcel" runat="server" Text="ExportToExcel" /></td>
        </tr>
        <tr>
            <td style="width: 723px; height: 129px;">
                <asp:gridview ID="dvregistrationchapter" runat="server" Width="790px"></asp:gridview>
                <asp:Label ID="Label4" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 723px; height: 14px;">
            </td>
        </tr>
        </table>
    </div>
    </asp:Panel> 
   <%-- </form>
</body>

</html>--%>

	</asp:Content>		
 
 
 