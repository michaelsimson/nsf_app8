﻿Imports Microsoft.ApplicationBlocks.Data
Imports VRegistration
Imports NativeExcel
Imports System.Data.SqlClient

Partial Class CoachClassCalReport
    Inherits System.Web.UI.Page


    Dim cmdText As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 1 '89
        'Session("LoginID") = 4240 '22214       
        'Session("LoggedIn") = "true"

        Try
            If LCase(Session("LoggedIn")) <> "true" Then

                Response.Redirect("~/maintest.aspx")

            End If

            If Page.IsPostBack = False Then

                Session("WeekCnt") = 0
                LoadYear()


                loadSemester()
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                    FillEvent()
                    FillProductGroup()
                    FillProduct()
                Else
                    Response.Redirect("~/maintest.aspx")
                End If
            End If
        Catch ex As Exception
            ' Response.Write("Err:" & ex.ToString)
        End Try
    End Sub
    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 8
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))


        Next
        ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub
    Private Sub loadSemester()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2

            ddlSemester.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue)
    End Sub

    ' Add events in dropdown for adding and updating
    Private Sub FillEvent()
        ddlEvent.Items.Add(New ListItem("Coaching", "13"))
    End Sub

    ' Get product group details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProductGroup()

        Dim dsProductGrp As DataSet
        If Session("RoleId") = 89 Then
            cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer V inner join CalSignup C on (V.ProductGroupID=C.ProductGroupID) where V.Memberid=" & Session("LoginID") & " and V.RoleId=" & Session("RoleId") & " and V.ProductId is not Null and C.Semester='" & ddlSemester.SelectedValue & "'  Order by ProductGroupId"

        ElseIf (Session("RoleID") = 88) Then
            cmdText = " select distinct ProductGroupID,ProductGroupCode from Calsignup where Memberid=" & Session("LoginID") & " and ProductId is not Null and eventYear=" & ddlYear.SelectedValue & " and Accepted='Y' and Semester='" & ddlSemester.SelectedValue & "'  Order by ProductGroupId"
        Else
            cmdText = " select distinct ProductGroupID,ProductGroupCode from CalSignup where eventid =" & ddlEvent.SelectedValue & " and ProductId is not Null and Eventyear=" & ddlYear.SelectedValue & " and Accepted='Y' and Semester='" & ddlSemester.SelectedValue & "' Order by ProductGroupId"
        End If

        ' "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] AS ProductGroupCode  FROM dbo.[Product] A Where A.EventID in(13) Order by A.ProductGroupCode")
        dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ViewState("PrdGroup") = dsProductGrp.Tables(0)

        ddlProductGroup.DataSource = dsProductGrp.Tables(0)
        ddlProductGroup.DataBind()

        If ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Enabled = True
            ddlProductGroup.Items.Insert(0, "Select Product Group")
        Else
            ddlProductGroup.SelectedIndex = 0
            ddlProductGroup.Enabled = False
            ddlSemester.SelectedValue = "1"
            FillProduct()

        End If

        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)

    End Sub

    ' Get product details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProduct()

        '  If ddlProductGroup.SelectedValue <> "Select Product Group" Then

        If Session("RoleId") = 89 Then
            cmdText = " select distinct ProductID,ProductCode from volunteer V inner join CalSignup CS where V.Memberid=" & Session("LoginID") & " and V.RoleId=" & Session("RoleId") & "  and V.ProductId is not Null and CS.Semester='" & ddlSemester.SelectedValue & "'  "
            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                cmdText = cmdText & " and V.ProductGroupId=" & ddlProductGroup.SelectedValue & ""
            End If
            cmdText = cmdText & " Order by ProductId"

        ElseIf (Session("RoleId") = 88) Then
            cmdText = " select distinct ProductID,ProductCode from CalSignup where Memberid=" & Session("LoginID") & "  and eventyear=" & ddlYear.SelectedValue & " and Accepted='Y' and Semester='" & ddlSemester.SelectedValue & "' and ProductId is not Null "

            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                cmdText = cmdText & " and ProductGroupId=" & ddlProductGroup.SelectedValue & ""
            End If
            cmdText = cmdText & "  Order by ProductId"

        Else
            cmdText = " select distinct ProductID,ProductCode from Calsignup where eventid =" & ddlEvent.SelectedValue & "  and ProductId is not Null  and Eventyear=" & ddlYear.SelectedValue & " and Accepted='Y' and Semester='" & ddlSemester.SelectedValue & "'"

            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                cmdText = cmdText & " and ProductGroupId=" & ddlProductGroup.SelectedValue & ""
            End If
            cmdText = cmdText & "  Order by ProductId"

        End If
        '"SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlProductGroup.SelectedValue & " Order by A.ProductCode"
        Dim dsProduct As DataSet
        dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ddlProduct.DataSource = dsProduct.Tables(0)
        ddlProduct.DataBind()
        If ddlProduct.Items.Count > 1 Then
            ddlProduct.Enabled = True
            ddlProduct.Items.Insert(0, "Select Product")
        Else
            ddlProduct.SelectedIndex = 0
            ddlProduct.Enabled = False
            FillCoach(ddlCoach)
        End If
        ' End If

    End Sub
    Private Sub FillCoach(ddl As DropDownList)
        Dim dsCoach As DataSet
        If Session("RoleId") = 88 Then

            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" & Session("LoginID")
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                ddl.SelectedIndex = 0
                ddl.Enabled = False
            End If

        Else
            ' If Len(strCond) = 0 Then
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.EventYear=" & ddlYear.SelectedValue & " and C.Semester='" & ddlSemester.SelectedValue & "' and C.Accepted='Y' "
            If (ddlProduct.SelectedValue <> "Select Product") Then
                cmdText = cmdText & "  and C.ProductGroupId=" & ddlProductGroup.SelectedValue & " "
            End If
            If (ddlProductGroup.SelectedValue <> "") Then
                cmdText = cmdText & " and C.ProductId=" & ddlProduct.SelectedValue & " "
            End If
            cmdText = cmdText & " order by lastname,firstname"

            '    Else
            '    cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " " & strCond & " order by lastname,firstname"
            'End If
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

            If dsCoach.Tables(0).Rows.Count > 0 Then

                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                If ddl.Items.Count > 0 Then
                    ddl.Items(0).Selected = True
                    If ddl.Items.Count = 1 Then
                        ddl.Enabled = False
                    Else
                        ddl.Enabled = True
                    End If

                End If


            End If

        End If
    End Sub


    'Fill product for each selected product group
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            ddlProduct.Items.Clear()
            FillProduct()
        Catch ex As Exception
            '   Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            Dim PgCode As String = ""
            Dim Cmdtext As String = " Select distinct productgroupid from Product where ProductId=" & ddlProduct.SelectedValue & ""
            PgCode = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, Cmdtext)
            ddlProductGroup.SelectedValue = PgCode
            ddlCoach.Items.Clear()
            FillCoach(ddlCoach)
        Catch ex As Exception
            '  Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub

    Protected Sub rdoCalendar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoCalendar.SelectedIndexChanged

        lblCoachClassCalendar.Text = rdoCalendar.SelectedItem.Text
        If rdoCalendar.SelectedIndex = 0 Then ' Class Calendar by Coach
            tblCoach.Visible = True
            gvCoachClassCal.Visible = False
            dvSearchSignups.Visible = False
            BtnExportMissedCalReport.Visible = False
        Else
            Dim ds As DataSet
            tblCoach.Visible = False
            gvCoachClassCal.Visible = True
            cmdText = cmdText + " select cc.EventYear, 'Coaching' EventCode, cc.ProductGroup ProductGroupCode, cc.Product ProductCode , cc.Semester, cc.[Date], cc.[Day],cc.[Time], cc.Duration, cc.SerNo, cc.WeekNo,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1"
            cmdText = cmdText + " where I1.automemberid=cc.MemberId) as Coach,(select I1.email  from indspouse I1"
            cmdText = cmdText + " where I1.automemberid=cc.MemberId) as coachemail, cc.[Status], (select I2.FirstName + ' ' + I2.LastName from indspouse I2 where I2.automemberid=cc.Substitute) as SubstituteName,  Reason, cc.Semester, "
            cmdText = cmdText + " rel.QReleaseDate,rel.QDeadlineDate, rel.AReleaseDate , cs.UserId, cs.PWD from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
            cmdText = cmdText + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
            cmdText = cmdText + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
            'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
            cmdText = cmdText + " where  "
            If Session("RoleID") = 88 Then
                cmdText = cmdText + "  cc.MemberID=" & ddlCoach.SelectedValue & " and "
            Else

            End If
            fillYearFiletr()
            loadProductGroupFilter()
            loadProductFilter()
            loadLevelFilter()
            loadCoachFilter()
            Select Case rdoCalendar.SelectedIndex

                Case 1 ' Missing Calendar Report 

                    LblReportTitle.Text = "Search Missing Calendar Report"
                    dvSearchSignups.Visible = True
                    Dim Year As Integer
                    Dim Semester As String = "Fall"
                    Try
                        Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                        Dim objCommon As Common = New Common()
                        Semester = objCommon.GetDefaultSemester(Year)
                    Catch ex As Exception

                    End Try
                    BtnExportMissedCalReport.Visible = True
                    cmdText = "    select cc.EventYear, 'Coaching' EventCode, cc.ProductGroupCode, cc.ProductCode , cc.Semester, '' as [Date], cc.[Day],cc.[Time], wc.Duration,  '' as SerNo , '' as WeekNo ,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1 where I1.automemberid=cc.MemberId) as Coach,(select I1.email  from indspouse I1 where I1.automemberid=cc.MemberId) as coachEmail, '' as [Status], '' as SubstituteName,  '' Reason , cc.Semester,  '' as QReleaseDate,'' as QDeadlineDate, '' as AReleaseDate, cc.UserId, cc.PWD from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" + Semester + "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)  order by Coach Asc"

                Case 2 'Cancelled Calendar Report
                    LblReportTitle.Text = "Search Cancelled Calendar Report"
                    BtnExportMissedCalReport.Visible = False
                    dvSearchSignups.Visible = True
                    cmdText = cmdText + "  cc.status ='Cancelled' and cc.EventYear=" & ddlEventyearFilter.SelectedValue & " " ' and cs.MemberId = " & ddlCoach.SelectedItem.Value
                    cmdText = cmdText + " order by cc.EventYear desc, cc.Date, cc.Time, cc.WeekNo, Coach Asc"
                Case 3 'Substitute Calendar Report
                    LblReportTitle.Text = "Search Substitute Calendar Report"
                    BtnExportMissedCalReport.Visible = False
                    dvSearchSignups.Visible = True
                    cmdText = cmdText + "  cc.Substitute is not null and cc.eventYear=" & ddlEventyearFilter.SelectedValue & ""
                    cmdText = cmdText + " order by cc.EventYear, cc.Date, cc.Time, cc.WeekNo, Coach Asc"
                    '   dvSearchSignups.Visible = False
            End Select

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            gvCoachClassCal.DataSource = ds.Tables(0)
            gvCoachClassCal.DataBind()
        End If
    End Sub

    Private Sub BindCoachClassDetailsInRepeater()
        'and productgroupid<>" + hdTable1PrdGrp.Value + " and productid <>" + hdTable1Prd.Value + "
        cmdText = "select count(*) from calsignup  where eventyear=" & ddlYear.SelectedItem.Value & " and eventid=13 and Accepted='y' and MemberId= " & ddlCoach.SelectedItem.Value & " and ProductGroupId= " & ddlProductGroup.SelectedItem.Value & " and  Productid=" & ddlProduct.SelectedItem.Value & " and Semester='" & ddlSemester.SelectedItem.Value & "'"

        If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
            Dim ds As DataSet
            cmdText = "select ProductGroupId,ProductGroupCode,Productid,ProductCode, Semester,level,sessionno from calsignup  where eventyear=" & ddlYear.SelectedItem.Value & " and eventid=13 and Accepted='y' and Semester='" & ddlSemester.SelectedValue & "' and MemberId= " & ddlCoach.SelectedItem.Value & " group by ProductGroupId,ProductGroupCode,Productid,ProductCode, Semester,level,sessionno "
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                rptCoachClass.DataSource = ds.Tables(0)
                rptCoachClass.DataBind()
            End If
        End If

    End Sub
    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView)
        Try
            Dim dt As Date = Now.Date.ToString("yyyy-MM-dd")  ' "2014-12-02"
            Dim rpItem As RepeaterItem = grCtrl.NamingContainer

            Dim iProductId As Integer = DirectCast(rpItem.FindControl("hdProduct"), HiddenField).Value
            Dim iProductGrpId As Integer = DirectCast(rpItem.FindControl("hdProductGrp"), HiddenField).Value
            Dim iSemester As String = DirectCast(rpItem.FindControl("lblSemester"), Label).Text
            Dim strLevel As String = DirectCast(rpItem.FindControl("lblLevel"), Label).Text
            Dim iSessionNo As Integer = DirectCast(rpItem.FindControl("lblSessionNo"), Label).Text


            Dim rep_hdWeekCnt As HiddenField = TryCast(rpItem.FindControl("rep_hdWeekCnt"), HiddenField)

            If Len(Trim(rep_hdWeekCnt.Value)) = 0 Then
                rep_hdWeekCnt.Value = "0"
            End If
            ' '" + dt.ToString("yyyy-MM-dd") + "'
            'cmdText = "select cs.SignupID,isnull(cc.CoachClassCalID, '') CoachClassCalID, cs.MemberId, cs.EventYear,cs.EventID, cs.SessionNo,cs.Level,cs.ProductGroupId,cs.ProductGroupCode,cs.ProductId,cs.Productcode, isnull(CONVERT(char(10),cdc.startdate,126),'')  as startdate,"
            'cmdText = cmdText + " cs.EndDate,cs.Duration,cs.Semester,cc.[date],cs.[Day] [Day],cs.[Time] [Time],cs.UserId,cs.Pwd,cc.SerNo,isnull(cc.WeekNo,0) as WeekNo, cc.[Status], cc.Substitute, cc.Reason, rel.qreleasedate, rel.qdeadlinedate, rel.areleasedate , (select i1.FirstName + ' ' + i1.LastName from indspouse i1 where i1.automemberid=cc.Substitute) as SubstituteName "
            'cmdText = cmdText + "  from calsignup cs "
            'cmdText = cmdText + " left join CoachingDateCal cdc on cs.EventId=cdc.EventId and cdc.EventYear=cs.EventYear and cdc.ProductGroupId= cs.ProductGroupId and cdc.ProductId=cs.ProductId "
            'cmdText = cmdText + " left join coachclasscal cc on cs.MemberId=cc.MemberId and (cc.[date]>= cdc.startdate and cc.[date]<= cdc.enddate)  and cs.eventyear=cc.eventyear and "
            'cmdText = cmdText + " cs.productid=cc.productid and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno  left join coachreldates rel on "
            'cmdText = cmdText + " rel.memberid=cs.memberid  and cs.eventyear=rel.eventyear and cs.productid=rel.productid and cs.Semester=rel.Semester and cs.level=rel.level and cs.sessionno=rel.session "

            cmdText = " select Cs.SignupID, CL.CoachClassCalID, CL.MemberID, CL.eventyear, cl.EventID, cl.Semester, CL.SessionNo, cl.Level, cl.ProductGroupID, cl.ProductGroup as ProductGroupCode, cl.ProductID, cl.Product as ProductCode, '' as StartDate, '' as EndDate,cl.Duration, 'term' as ScheduleType, cl.Day, cl.Time, cs.UserID, cs.Pwd, cl.Date, cl.SerNo, cl.WeekNo, cl.Status, cl.Substitute,'' as SubstituteName, cl.Reason, CD.QReleaseDate, CD.QDeadlineDate, CD.AReleaseDate, CD.SReleaseDate,cl.classtype from CoachClassCal CL inner join CalSignup CS on ( CS.Eventyear = CL.Eventyear and  CS.memberID=CL.memberID and CS.Accepted='Y' and CS.ProductGroupID=CL.ProductGroupID and CS.ProductID=CL.productID and CS.Level =CL.level and CS.SessionNo = CL.SessionNo) left join CoachPapers CP on (cp.DocType='Q' and cp.WeekId = cl.WeekNo and cp.Eventyear=CL.Eventyear and cp.EventID=13 and cp.ProductId=CL.ProductId and Cp.Level=cl.Level) left join CoachRelDates CD on (CD.CoachpaperID=CP.CoachPaperID and CD.MemberID=CL.memberId and CD.ProductGroupID=CL.productGroupID and CD.ProductID=CL.productID and CD.Eventyear=Cl.Eventyear and CD.level=CL.Level and CD.Session=CL.SessionNo and CD.Semester=CL.Semester) "

            cmdText = cmdText + " where  cs.MemberId = " & ddlCoach.SelectedItem.Value
            cmdText = cmdText + " and cs.eventyear=" & ddlYear.SelectedItem.Value & " and  cs.ProductId=" & iProductId & " and cs.Level='" & strLevel & "' and cs.sessionno=" & iSessionNo & " and cs.Accepted='y'and Cs.Semester='" & ddlSemester.SelectedValue & "'  order by cl.WeekNo"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'If ds.Tables.Count > 0 Then
            '    'Row of the Grid from where the SelectedIndex change event is fired.
            '     Dim rp As RepeaterItem = TryCast(grCtrl.NamingContainer, RepeaterItem)
            '    'Dim lblPrdGrp As Label = DirectCast(rp.FindControl("lblPrdGrp"), Label)
            '    'lblPrdGrp.Text = ds.Tables(0).Rows(0).Item("ProductGroupCode")
            '    'Dim hdProductGrp As HiddenField = DirectCast(rp.FindControl("hdProductGrp"), HiddenField)
            '    'hdProductGrp.Value = ds.Tables(0).Rows(0).Item("ProductGroupId")
            '    'Dim lblProduct As Label = DirectCast(rp.FindControl("lblProduct"), Label)
            '    'lblProduct.Text = ds.Tables(0).Rows(0).Item("ProductCode")
            '    'Dim lblSemester As Label = DirectCast(rp.FindControl("lblSemester"), Label)
            '    'lblSemester.Text = ds.Tables(0).Rows(0).Item("Semester")
            '    'Dim lblLevel As Label = DirectCast(rp.FindControl("lblLevel"), Label)
            '    'lblLevel.Text = ds.Tables(0).Rows(0).Item("Level")
            '    'Dim lblSession As Label = DirectCast(rp.FindControl("lblSessionNo"), Label)
            '    'lblSession.Text = ds.Tables(0).Rows(0).Item("SessionNo")
            'End If

            grCtrl.DataSource = ds.Tables(0)
            grCtrl.DataBind()
        Catch ex As Exception
            '   Response.Write("ERR: " & ex.ToString())
        End Try


    End Sub

    Protected Sub rptCoachClass_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCoachClass.ItemDataBound
        Try
            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then

                Dim grid As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)

                'Dim iProductId As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                'Dim iSemester As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                'Dim iProductId As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                BindCoachClassDetailsInGrid(grid)

                Dim lblTableCnt As Label = DirectCast(e.Item.FindControl("lblTableCnt"), Label)
                lblTableCnt.Text = CInt(e.Item.ItemIndex) + 1


            End If
        Catch ex As Exception
            'Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindCoachClassDetailsInRepeater()
    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        FillProductGroup()
        FillProduct()
    End Sub

    Protected Sub BtnExportMissedCalReport_Click(sender As Object, e As EventArgs)

        exportToExcel()

    End Sub

    Public Sub exportToExcel()
        Try
            Dim Year As Integer
            Dim Semester As String = "Fall"
            Try
                Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                Dim objCommon As Common = New Common()
                Semester = objCommon.GetDefaultSemester(Year)
            Catch ex As Exception

            End Try

            Dim cmdText As String = String.Empty
            cmdText = "select cc.memberId, cc.EventYear, 'Coaching' EventCode, cc.ProductGroupCode, cc.ProductCode,cc.level,cc.sessionno , cc.Semester, '' as [Date], cc.[Day],cc.[Time], wc.Duration,  '' as SerNo , '' as WeekNo ,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1 where I1.automemberid=cc.MemberId) as Coach,(select I1.email  from indspouse I1 where I1.automemberid=cc.MemberId) as coachEmail, '' as [Status], '' as SubstituteName,  '' Reason , cc.Semester,  '' as QReleaseDate,'' as QDeadlineDate, '' as AReleaseDate, cc.UserId, cc.PWD from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" + Semester + "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)  order by Coach Asc "



            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                    Dim oSheet As IWorksheet = Nothing

                    oSheet = oWorkbooks.Worksheets.Add()

                    oSheet.Name = "Class Status"
                    oSheet.Range("A1:M1").MergeCells = True
                    oSheet.Range("A1").Value = "Missed Calendar"
                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A1").Font.Color = Color.Black


                    oSheet.Range("A3").Value = "Ser#"
                    oSheet.Range("A3").Font.Bold = True

                    oSheet.Range("B3").Value = "MemberId"
                    oSheet.Range("B3").Font.Bold = True

                    oSheet.Range("C3").Value = "Name"
                    oSheet.Range("C3").Font.Bold = True

                    oSheet.Range("D3").Value = "Email"
                    oSheet.Range("D3").Font.Bold = True

                    oSheet.Range("E3").Value = "ProductGroup"
                    oSheet.Range("E3").Font.Bold = True

                    oSheet.Range("F3").Value = "Product"
                    oSheet.Range("F3").Font.Bold = True

                    oSheet.Range("G3").Value = "Level"
                    oSheet.Range("G3").Font.Bold = True

                    oSheet.Range("H3").Value = "SessionNo"
                    oSheet.Range("H3").Font.Bold = True

                    oSheet.Range("I3").Value = "Semester"
                    oSheet.Range("I3").Font.Bold = True

                    oSheet.Range("J3").Value = "userID"
                    oSheet.Range("J3").Font.Bold = True

                    Dim iRowIndex As Integer = 4
                    Dim CRange As IRange = Nothing
                    Dim i As Integer = 0

                    For Each dr As DataRow In ds.Tables(0).Rows
                        CRange = oSheet.Range("A" + iRowIndex.ToString())
                        CRange.Value = i + 1

                        CRange = oSheet.Range("B" + iRowIndex.ToString())
                        CRange.Value = dr("MemberID").ToString()

                        CRange = oSheet.Range("C" + iRowIndex.ToString())
                        CRange.Value = dr("coach").ToString().Replace("&nbsp;", " ")

                        CRange = oSheet.Range("D" + iRowIndex.ToString())
                        CRange.Value = dr("coachEmail").ToString().Replace("&nbsp;", " ")


                        CRange = oSheet.Range("E" + iRowIndex.ToString())
                        CRange.Value = dr("ProductGroupCode").ToString()

                        CRange = oSheet.Range("F" + iRowIndex.ToString())
                        CRange.Value = dr("ProductCode").ToString()

                        CRange = oSheet.Range("G" + iRowIndex.ToString())
                        CRange.Value = dr("Level").ToString()

                        CRange = oSheet.Range("H" + iRowIndex.ToString())
                        CRange.Value = dr("SessionNo").ToString()


                        CRange = oSheet.Range("I" + iRowIndex.ToString())
                        CRange.Value = dr("Semester").ToString()

                        CRange = oSheet.Range("J" + iRowIndex.ToString())
                        CRange.Value = dr("UserId").ToString()


                        iRowIndex = iRowIndex + 1
                        i = i + 1
                    Next

                    Dim dt As DateTime = DateTime.Now
                    Dim month As String = dt.ToString("MMM")
                    Dim day As String = dt.ToString("dd")
                    Dim Currentyear As String = dt.ToString("yyyy")
                    Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day

                    Dim filename As String = (Convert.ToString((Convert.ToString("ClassStatus_") & monthDay) + "_") & Currentyear) + ".xls"

                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename)
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream)
                    Response.[End]()
                End If
            End If
        Catch
        End Try
    End Sub

    Public Sub fillYearFiletr()

        Dim Year As Integer = Convert.ToInt32(DateTime.Now.Year)

        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)))

        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)))


        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(eventyear) from EventFees where EventId=13")
    End Sub

    Public Sub loadProductGroupFilter()
        Try
            Dim Year As Integer
            Dim Semester As String = "Fall"
            Try
                Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                Dim objCommon As Common = New Common()
                Semester = objCommon.GetDefaultSemester(Year)
            Catch ex As Exception

            End Try

            Dim strSql As String = String.Empty
            If rdoCalendar.SelectedIndex = 1 Then
                strSql = "  select  distinct cc.ProductGroupCode, cc.ProductGroupid from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" & Semester & "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)  order by ProductGroupid Asc "
            Else
                strSql = strSql + " select distinct cc.ProductGroup as ProductGroupCode, cc.productGroupId from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
                strSql = strSql + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
                strSql = strSql + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
                'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
                strSql = strSql + " where  "
                If Session("RoleID") = 88 Then
                    strSql = strSql + "  cc.MemberID=" & ddlCoach.SelectedValue & " and "
                Else

                End If

                If (rdoCalendar.SelectedIndex = 2) Then
                    strSql = strSql + "  cc.status ='Cancelled'"
                Else
                    strSql = strSql + "  cc.substitute is not null"
                End If

            End If



            Dim conn As New SqlConnection(Application("ConnectionString"))

            Dim drproductgroup As New DataSet()
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlProductGroupFilter.DataValueField = "ProductGroupID"
            ddlProductGroupFilter.DataTextField = "ProductGroupCode"

            ddlProductGroupFilter.DataSource = drproductgroup
            ddlProductGroupFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlProductGroupFilter.Items.Insert("0", "Select")
                ddlProductGroupFilter.Enabled = True
            Else
                ddlProductGroupFilter.Enabled = False
            End If



        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadProductFilter()
        Try
            Dim Year As Integer
            Dim Semester As String = "Fall"
            Try
                Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                Dim objCommon As Common = New Common()
                Semester = objCommon.GetDefaultSemester(Year)
            Catch ex As Exception

            End Try
            Dim strSql As String = String.Empty
            If rdoCalendar.SelectedIndex = 1 Then
                strSql = "  select  distinct cc.productCode, cc.productid from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" & Semester & "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)   "
            Else
                strSql = strSql + " select distinct cc.product as ProductCode, cc.productid from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
                strSql = strSql + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
                strSql = strSql + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
                'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
                strSql = strSql + " where  "
                If Session("RoleID") = 88 Then
                    strSql = strSql + "  cc.MemberID=" & ddlCoach.SelectedValue & " and "
                Else

                End If

                If (rdoCalendar.SelectedIndex = 2) Then
                    strSql = strSql + "  cc.status ='Cancelled'"
                Else
                    strSql = strSql + "  cc.substitute is not null"
                End If

            End If

            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If
            If (ddlPhaseFilter.SelectedValue <> "0") Then
                strSql = strSql & " and cc.Semester='" & ddlPhaseFilter.SelectedValue & "'"
            End If

            strSql = strSql & "order by cc.ProductID"


            Dim conn As New SqlConnection(Application("ConnectionString"))


            Dim drproductgroup As New DataSet()
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlProductFilter.DataValueField = "ProductID"
            ddlProductFilter.DataTextField = "productcode"

            ddlProductFilter.DataSource = drproductgroup
            ddlProductFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlProductFilter.Items.Insert(0, "Select")
                ddlProductFilter.Enabled = True
            Else
                ddlProductFilter.Enabled = False
            End If



        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadLevelFilter()
        Try
            Dim Year As Integer
            Dim Semester As String = "Fall"
            Try
                Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                Dim objCommon As Common = New Common()
                Semester = objCommon.GetDefaultSemester(Year)
            Catch ex As Exception

            End Try

            Dim strSql As String = String.Empty
            If rdoCalendar.SelectedIndex = 1 Then
                strSql = "  select  distinct cc.level as LevelCode from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" & Semester & "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)   "
            Else
                strSql = strSql + " select distinct cc.level as LevelCode from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
                strSql = strSql + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
                strSql = strSql + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
                'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
                strSql = strSql + " where  "
                If Session("RoleID") = 88 Then
                    strSql = strSql + "  cc.MemberID=" & ddlCoach.SelectedValue & " and "
                Else

                End If

                If (rdoCalendar.SelectedIndex = 2) Then
                    strSql = strSql + "  cc.status ='Cancelled'"
                Else
                    strSql = strSql + "  cc.substitute is not null"
                End If

            End If

            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If

            If ddlProductFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.ProductID=" & ddlProductFilter.SelectedValue & ""
            End If
            strSql = strSql & "  order by cc.level Asc"

            Dim drproductgroup As New DataSet()

            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)
            ddlLevelFilter.DataValueField = "LevelCode"
            ddlLevelFilter.DataTextField = "LevelCode"
            ddlLevelFilter.DataSource = drproductgroup
            ddlLevelFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlLevelFilter.Items.Insert(0, "Select")
                ddlLevelFilter.Enabled = True
            Else
                ddlLevelFilter.Enabled = False
            End If


        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadCoachFilter()
        Try
            Dim Year As Integer
            Dim Semester As String = "Fall"
            Try
                Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                Dim objCommon As Common = New Common()
                Semester = objCommon.GetDefaultSemester(Year)
            Catch ex As Exception

            End Try
            Dim strSql As String = String.Empty
            If rdoCalendar.SelectedIndex = 1 Then
                strSql = "  select  distinct cc.MemberId, IP.FirstName+ ' ' +IP.LastName as Name from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') inner join Indspouse IP on (IP.AutoMemberId=cc.MemberId) where cc.EventYear=" & Year & " and cc.Semester='" & Semester & "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)   "
            Else
                strSql = strSql + " select distinct cc.MemberId, IP.FirstName+ ' ' +IP.LastName as Name from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo  inner join Indspouse IP on (IP.AutoMemberId=cc.MemberId)"
                strSql = strSql + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
                strSql = strSql + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
                'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
                strSql = strSql + " where  "
                If Session("RoleID") = 88 Then
                    strSql = strSql + "  cc.MemberID=" & ddlCoach.SelectedValue & " and "
                Else

                End If

                If (rdoCalendar.SelectedIndex = 2) Then
                    strSql = strSql + "  cc.status ='Cancelled'"
                Else
                    strSql = strSql + "  cc.substitute is not null"
                End If

            End If

            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If
            If ddlProductFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.ProductID=" & ddlProductFilter.SelectedValue & ""
            End If
            If ddlLevelFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and cc.Level=" & ddlLevelFilter.SelectedValue & ""
            End If
            If ddlPhaseFilter.SelectedValue <> "0" Then
                strSql = strSql & " and cc.Semester='" & ddlPhaseFilter.SelectedValue & "'"
            End If


            strSql = strSql & " order by Name"

            Dim drproductgroup As New DataSet()
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlCoachFilter.DataValueField = "MemberID"
            ddlCoachFilter.DataTextField = "Name"

            ddlCoachFilter.DataSource = drproductgroup
            ddlCoachFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlCoachFilter.Items.Insert(0, "Select")
                ddlCoachFilter.Enabled = True
            Else
                ddlCoachFilter.Enabled = False
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlProductGroupFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

        loadProductFilter()
        loadCoachFilter()
        FilterReports()



    End Sub

    Protected Sub ddlProductFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

        loadLevelFilter()
        loadCoachFilter()
        FilterReports()


    End Sub

    Protected Sub btnSearchFilter_Click(sender As Object, e As EventArgs)

        FilterReports()



    End Sub

    Protected Sub btnClearFilter_Click(sender As Object, e As EventArgs)

        ddlProductGroupFilter.SelectedValue = "Select"
        ddlProductFilter.SelectedValue = "Select"
        ddlLevelFilter.SelectedValue = "Select"
        ddlCoachFilter.SelectedValue = "Select"

        ddlPhaseFilter.SelectedValue = "0"
        ddlSessionFilter.SelectedValue = "Select"
        ddlDayFilter.SelectedValue = "0"
        FilterReports()


    End Sub

    Protected Sub ddlLevelFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

        loadCoachFilter()
        FilterReports()



    End Sub

    Protected Sub ddlSessionFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

        FilterReports()



    End Sub
    Protected Sub ddlDayFilter_SelectedIndexChanged1(sender As Object, e As EventArgs)

        FilterReports()


    End Sub
    Protected Sub ddlPhaseFilter_SelectedIndexChanged(sender As Object, e As EventArgs)

        loadProductGroupFilter()
        loadProductFilter()
        loadLevelFilter()
        loadCoachFilter()
        FilterReports()



    End Sub

    Protected Sub ddlCoachFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        FilterReports()


    End Sub

    Public Sub FilterReports()
        Dim ds As DataSet
        tblCoach.Visible = False
        gvCoachClassCal.Visible = True
        cmdText = cmdText + " select cc.EventYear, 'Coaching' EventCode, cc.ProductGroup ProductGroupCode, cc.Product ProductCode , cc.Semester, cc.[Date], cc.[Day],cc.[Time], cc.Duration, cc.SerNo, cc.WeekNo,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1"
        cmdText = cmdText + " where I1.automemberid=cc.MemberId) as Coach,(select I1.email  from indspouse I1"
        cmdText = cmdText + " where I1.automemberid=cc.MemberId) as coachemail, cc.[Status], (select I2.FirstName + ' ' + I2.LastName from indspouse I2 where I2.automemberid=cc.Substitute) as SubstituteName,  Reason, cc.Semester, "
        cmdText = cmdText + " rel.QReleaseDate,rel.QDeadlineDate, rel.AReleaseDate , cs.UserId, cs.PWD from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
        cmdText = cmdText + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
        cmdText = cmdText + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y' where cc.Eventyear=" & ddlEventyearFilter.SelectedValue & ""
        'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "


        If ddlEventyearFilter.SelectedValue <> "Select" Then
            cmdText = cmdText + " and cc.EventYear=" + ddlEventyearFilter.SelectedValue + ""
        End If

        If ddlProductGroupFilter.SelectedValue <> "Select" Then
            cmdText = cmdText + "  and cc.ProductGroupID='" + ddlProductGroupFilter.SelectedValue + "'"
        End If

        If (ddlProductFilter.SelectedValue <> "Select") Then
            cmdText = cmdText + "  and cc.ProductID='" + ddlProductFilter.SelectedValue + "'"
        End If

        If (ddlLevelFilter.SelectedValue <> "Select") Then
            cmdText = cmdText + "  and cc.Level='" + ddlLevelFilter.SelectedValue + "'"
        End If

        If (ddlPhaseFilter.SelectedValue <> "0") Then
            cmdText = cmdText + "  and cc.Semester='" + ddlPhaseFilter.SelectedValue + "'"
        End If

        If (ddlCoachFilter.SelectedValue <> "Select") Then
            cmdText = cmdText + "  and cc.memberID='" + ddlCoachFilter.SelectedValue + "'"
        End If

        If (ddlSessionFilter.SelectedValue <> "Select") Then
            cmdText = cmdText + "  and cc.Session='" + ddlSessionFilter.SelectedValue + "'"
        End If

        If (ddlDayFilter.SelectedValue <> "0") Then
            cmdText = cmdText + "  and cc.Day='" + ddlDayFilter.SelectedValue + "'"
        End If

        Select Case rdoCalendar.SelectedIndex

            Case 1 ' Missing Calendar Report 

                LblReportTitle.Text = "Search Missing Calendar Report"
                dvSearchSignups.Visible = True
                Dim Year As Integer
                Dim Semester As String = "Fall"
                Try
                    Year = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                    Dim objCommon As Common = New Common()
                    Semester = objCommon.GetDefaultSemester(Year)
                Catch ex As Exception

                End Try
                BtnExportMissedCalReport.Visible = True
                cmdText = "    select cc.EventYear, 'Coaching' EventCode, cc.ProductGroupCode, cc.ProductCode , cc.Semester, '' as [Date], cc.[Day],cc.[Time], wc.Duration,  '' as SerNo , '' as WeekNo ,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1 where I1.automemberid=cc.MemberId) as Coach,(select I1.email  from indspouse I1 where I1.automemberid=cc.MemberId) as coachEmail, '' as [Status], '' as SubstituteName,  '' Reason , cc.Semester,  '' as QReleaseDate,'' as QDeadlineDate, '' as AReleaseDate, cc.UserId, cc.PWD from calsignup  cc inner join webCOnfLog wc on (wc.MemberId=cc.Memberid and wc.ProductgroupId=cc.Productgroupid and wc.productid=cc.productid and wc.level=cc.level and wc.session=cc.sessionno and wc.semester=cc.semester and wc.eventyear=cc.eventyear and wc.sessiontype='Recurring meeting') where cc.EventYear=" & Year & " and cc.Semester='" + Semester + "' and Accepted='Y' and cc.memberId not in (select memberid from coachclasscal where eventyear=" & Year & " and ProductGroupId=cc.ProductGroupId and ProductId=cc.ProductId and MemberId=cc.MemberId)  "

                If ddlEventyearFilter.SelectedValue <> "Select" Then
                    cmdText = cmdText + " and cc.EventYear=" + ddlEventyearFilter.SelectedValue + ""
                End If

                If ddlProductGroupFilter.SelectedValue <> "Select" Then
                    cmdText = cmdText + "  and cc.ProductGroupID='" + ddlProductGroupFilter.SelectedValue + "'"
                End If

                If (ddlProductFilter.SelectedValue <> "Select") Then
                    cmdText = cmdText + "  and cc.ProductID='" + ddlProductFilter.SelectedValue + "'"
                End If

                If (ddlLevelFilter.SelectedValue <> "Select") Then
                    cmdText = cmdText + "  and cc.Level='" + ddlLevelFilter.SelectedValue + "'"
                End If

                If (ddlPhaseFilter.SelectedValue <> "0") Then
                    cmdText = cmdText + "  and cc.Semester='" + ddlPhaseFilter.SelectedValue + "'"
                End If

                If (ddlCoachFilter.SelectedValue <> "Select") Then
                    cmdText = cmdText + "  and cc.memberID='" + ddlCoachFilter.SelectedValue + "'"
                End If

                If (ddlSessionFilter.SelectedValue <> "Select") Then
                    cmdText = cmdText + "  and cc.Session='" + ddlSessionFilter.SelectedValue + "'"
                End If

                If (ddlDayFilter.SelectedValue <> "0") Then
                    cmdText = cmdText + "  and cc.Day='" + ddlDayFilter.SelectedValue + "'"
                End If

                cmdText = cmdText + " order by Coach Asc"

            Case 2 'Cancelled Calendar Report
                LblReportTitle.Text = "Search Cancelled Calendar Report"
                BtnExportMissedCalReport.Visible = False
                dvSearchSignups.Visible = True
                cmdText = cmdText + " and cc.status ='Cancelled' " ' and cs.MemberId = " & ddlCoach.SelectedItem.Value
                cmdText = cmdText + " order by cc.EventYear desc, cc.Date, cc.Time, cc.WeekNo, Coach Asc"
            Case 3 'Substitute Calendar Report
                LblReportTitle.Text = "Search Substitute Calendar Report"
                BtnExportMissedCalReport.Visible = False
                dvSearchSignups.Visible = True
                cmdText = cmdText + " and cc.Substitute is not null"
                cmdText = cmdText + " order by cc.EventYear, cc.Date, cc.Time, cc.WeekNo, Coach Asc"
        End Select

        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        gvCoachClassCal.DataSource = ds.Tables(0)
        gvCoachClassCal.DataBind()
    End Sub


End Class
