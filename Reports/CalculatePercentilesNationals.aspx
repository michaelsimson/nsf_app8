<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CalculatePercentilesNationals.aspx.cs" Inherits="Reports_CalculatePercentilesNationals" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
		<title>CalculatePercentiles</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:Button id="Button1" style="Z-INDEX: 101; LEFT: 672px; POSITION: absolute; TOP: 288px" runat="server"
				Text="Button" Visible="False" onclick="Button1_Click"></asp:Button><br />
			<asp:Label ID ="lblYear" runat="server" Text="Contest Year:"></asp:Label>
            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" 
               style="Z-INDEX: 152; LEFT: 103px; POSITION: absolute; TOP: 33px; height: 22px; width: 75px;" 
                onselectedindexchanged="ddlYear_SelectedIndexChanged" >
              	<asp:ListItem Value="2013">2013</asp:ListItem>
				<asp:ListItem Value="2012">2012</asp:ListItem>
				<asp:ListItem Value="2011">2011</asp:ListItem>
				<asp:ListItem Value="2010">2010</asp:ListItem>
			    <asp:ListItem Value="2009">2009</asp:ListItem>
			</asp:DropDownList><br /><br />
			<asp:Label ID ="lblMessage" runat="server" Text="" ForeColor="Red" ></asp:Label>
            <br />
			<asp:LinkButton id="LinkButton1" style="Z-INDEX: 103; LEFT: 264px; POSITION: absolute; TOP: 80px" runat="server" Visible="False">
				<a href="DisplayPercentilesNationals.aspx">Click here to view percentiles</a>
			</asp:LinkButton>
			<br /><br />
            <div style="margin-left: 400px">
                <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
            </div>
            <br />
		</form>
	</body>
</html>


 
 
 