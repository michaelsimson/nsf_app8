﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Invitee_Decline_Report
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadyear()
            LoadGrid(Now.Year, GVInvitee)
            ltlYear.Text = Now.Year.ToString()
        End If
    End Sub
    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ltlYear.Text = ddlyear.SelectedValue
        LoadGrid(ddlyear.SelectedValue, GVInvitee)
    End Sub
    Private Sub LoadYear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 4 Step -1
            ddlyear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub
    Private Sub LoadGrid(ByVal year As Integer, ByVal GV As GridView)
        Dim dsInviteeDec As New DataSet
        Dim tblInviteeDec() As String = {"InviteeDeclined"}
        Dim i, j, k, l As Integer
        Dim InvDecline() As String = {"Going to India", "Inconvenient Time", "Inconvenient venue", "Expensive to attend", "Not Interested"}
        Dim InDecCount As Integer = 5
        Dim Array(50) As Integer
        Dim GngtoIndia, InconTime, Inconvenue, Expensive, NoInterest As Integer
        GngtoIndia = 0
        InconTime = 0
        Inconvenue = 0
        Expensive = 0
        NoInterest = 0

        Dim StrSQL As String = " select Distinct ChapterId From Contestant Where  NationalInvitee =1  and ContestYear=" & ddlyear.SelectedValue & "and invite_decline_comments is not null "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsInviteeDec, tblInviteeDec)

        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("ChapterCode", Type.GetType("System.String"))
        dt.Columns.Add("GngtoIndia", Type.GetType("System.Int32"))
        dt.Columns.Add("InconTime", Type.GetType("System.Int32"))
        dt.Columns.Add("Inconvenue", Type.GetType("System.Int32"))
        dt.Columns.Add("Expensive", Type.GetType("System.String"))
        dt.Columns.Add("NoInterest", Type.GetType("System.String"))

        For i = 0 To dsInviteeDec.Tables("InviteeDeclined").Rows.Count - 1 'ds.Tables(0).Rows.Count '
            For j = 0 To InDecCount - 1 'Mealtype
                StrSQL = "select COUNT(*) from Contestant where invite_decline_comments='" & InvDecline(j) & "' and ChapterID=" & ds.Tables(0).Rows(i).Item("ChapterId") & " and ContestYear=" & ddlyear.SelectedValue & "" 'InviteeDec.Tables("InviteeDeclined").Rows(i).ToString() & "'"
                Array(l) = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)
                l = l + 1
            Next
            dr = dt.NewRow
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " Select ChapterCode From Chapter Where ChapterID=" & ds.Tables(0).Rows(i).Item("ChapterId") & "")
            dr("ChapterCode") = ds1.Tables(0).Rows(0).Item("ChapterCode")
            
            dr("GngtoIndia") = Array(0)
            dr("InconTime") = Array(1)
            dr("Inconvenue") = Array(2)
            dr("Expensive") = Array(3)
            dr("NoInterest") = Array(4)
            dt.Rows.Add(dr)
            l = 0
        Next
        
        For i = 0 To dt.Rows.Count - 1
            GngtoIndia = GngtoIndia + dt.Rows(i)("GngtoIndia")
            InconTime = InconTime + dt.Rows(i)("InconTime")
            Inconvenue = Inconvenue + dt.Rows(i)("Inconvenue")
            Expensive = Expensive + dt.Rows(i)("Expensive")
            NoInterest = NoInterest + dt.Rows(i)("NoInterest")
        Next

        dr = dt.NewRow()
        dr("ChapterCode") = "Total"
        dr("GngtoIndia") = GngtoIndia
        dr("InconTime") = InconTime
        dr("Inconvenue") = Inconvenue
        dr("Expensive") = Expensive
        dr("NoInterest") = NoInterest
        dt.Rows.Add(dr)
        GV.DataSource = dt
        GV.DataBind()
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=FinalRegReport_" & ddlyear.SelectedValue & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New GridView
        LoadGrid(ddlyear.SelectedValue, dgexport)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
End Class
