Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI
Partial Class Reports_MealsReport
    Inherits System.Web.UI.Page

    Protected Sub dgMeals_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        loadgrid(dgMeals)
        dgMeals.CurrentPageIndex = e.NewPageIndex
        dgMeals.DataBind()
    End Sub
    Protected Sub loadgrid(ByVal dg As DataGrid)
        ' lblErr.Text = StrSQL.ToString()
        Try
            Dim dsMeals As New DataSet
            Dim tblMeals() As String = {"Meals"}
            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, "select automemberID as MemberID,ContestDate,Mealtype,case when LunchType is null then 'NULL' else LunchType End as LunchType,Name,Gender,Age,ChildFlag,RelationType as Relationship, case when paymentreference is null then 'Pending' Else 'Paid' End as Status from mealcharge where PaymentReference is not Null AND  contestyear=" & ltlYear.Text & " order by AutomemberID,ContestDate,MealType,Name", dsMeals, tblMeals)
            dg.CurrentPageIndex = 0
            dg.DataSource = dsMeals
            dg.DataBind()
            'If ddlyear.SelectedValue = Now.Year.ToString Then
            '    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN datediff(d,Getdate(),MIN(contestdate))<6 then 1 else 0 END as Status from contest where contest_year=YEAR(Getdate()) and eventid=1") = 0 Then
            '        btnExport.Enabled = False
            '        lblErr.Text = "You can Export report only from 5 days to the contest start date."
            '    End If
            'Else
            btnExport.Enabled = True
            lblErr.Text = ""
            ''End If
        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadyear()
            ltlYear.Text = Now.Year.ToString()
            loadgrid(dgMeals)
        End If
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 4 Step -1
            ddlyear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub
    Protected Sub lbtnVolunteerFunctions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/VolunteerFunctions.aspx")
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=MealsReports_" & ddlyear.SelectedItem.Text & "_" & Now.Date.ToString() & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        loadgrid(dgexport)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ltlYear.Text = ddlyear.SelectedValue
        loadgrid(dgMeals)
    End Sub
End Class
