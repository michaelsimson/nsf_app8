﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;

public partial class AddUpdateSBVBSelMatrix : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            //Session["LoginID"] = 22214;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            {
                Response.Redirect("~/login.aspx?entry=p");
            }
            if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            {
                Response.Redirect("~/VolunteerFunctions.aspx");
            }
     
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
          
           
        }


       
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }
    //private void FillProductGroup()
    //{
    //    //ddlProductGroup
    //    string ddlproductgroupqry;

    //    //if (Convert.ToInt32(Session["RoleId"]) == 89)
    //    //{
    //    //    ddlproductgroupqry = "select distinct p.Name,v.ProductGroupID,v.ProductGroupCode from volunteer v inner join ProductGroup p on v.ProductGroupID=p.ProductGroupID and  v.Eventid=20  where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and v.ProductId is not Null  Order by v.ProductGroupId";
    //    //}
    //    //else
    //    //{
    //    ddlproductgroupqry = "select  distinct p.ProductGroupCode,p.ProductGroupID,p.Name from  ProductGroup p  where p.eventid=20 and p.ProductGroupId is not Null Order by p.ProductGroupId";
    //        // ddlproductgroupqry = "select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" + ddlEvent.SelectedValue + " and ProductId is not Null Order by ProductGroupId";
    //    //}
    //    DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductgroupqry);

    //    ddlProductGroup.DataSource = dsstate;
    //    //ddlProductGroup.DataTextField = "Name";
    //    ddlProductGroup.DataValueField = "ProductGroupID";
    //    ddlProductGroup.DataBind();
    //    ddlProductGroup.Items.Insert(0, new ListItem("Select ProductGroup", "-1"));



    //}
    //private void FillProduct()
    //{
    //    string ddlproductqry;
    //    try
    //    {
    //        //if (Convert.ToInt32(Session["RoleId"]) == 89)
    //        //{
    //        //    ddlproductqry = "select distinct p.Name,v.ProductID,v.ProductCode from CalSignup c inner join volunteer v on c.ProductID=v.ProductID inner join Product p on v.ProductID=p.ProductID where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and v.ProductGroupId=" + ddlProductGroup.SelectedValue + " and v.ProductId is not Null  Order by v.ProductId";
    //        //}
    //        //else
    //        //{
    //        ddlproductqry = "select  distinct p.ProductID,p.ProductCode,p.Name from  Product p  where p.eventid=20 and p.ProductID is not Null and p.ProductGroupId=" + ddlProductGroup.SelectedValue + " Order by p.ProductID";
    //        //}
    //        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
    //        DataSet myDataSet = new DataSet();
    //        ddlProduct.DataSource = dsstate;
    //        //ddlProduct.DataTextField = "Name";
    //        ddlProduct.DataValueField = "ProductID";
    //        ddlProduct.DataBind();
    //        ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
    //        ddlProduct.Enabled = true;
    //        if (dsstate.Tables[0].Rows.Count==1)
    //        {
    //            ddlProduct.SelectedIndex = 1;
    //            ddlProduct.Enabled = false;
    //            FillWorkshopDate();
    //        }

    //    }
    //    catch (Exception e)
    //    { }
    //}
    //protected void FillWorkshopDate()
    //{
    //    //string ddlproductqry;
    //    try
    //    {

    //        string ddlworkshopDateqry;
    //        ddlworkshopDateqry = "select distinct Convert(nvarchar(10),EventDate, 101) as EventDate from Registration_OnlineWkshop where ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and EventYear=" + ddlYear.SelectedValue + "";

    //        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlworkshopDateqry);
    //        DataSet myDataSet = new DataSet();
    //        ddlworkshopDate.DataSource = dsstate;
    //        //ddlProduct.DataTextField = "Name";
    //        ddlworkshopDate.DataValueField = "EventDate";
    //        ddlworkshopDate.DataBind();
    //        ddlworkshopDate.Items.Insert(0, new ListItem("Select Workshop Date", "-1"));
    //        ddlworkshopDate.Enabled = true;
    //        if (dsstate.Tables[0].Rows.Count==1)
    //        {
    //            ddlworkshopDate.SelectedIndex = 1;
    //            ddlworkshopDate.Enabled = false;
    //        }

    //    }
    //    catch (Exception e)
    //    { }
    //}



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlPhase.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Phase";
            lblErr.Visible = true;
        }
        else
        {
            if (ddlPhase.SelectedIndex == 1)
            {

                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("RoundType");
                dt.Columns.Add("Round Count");
                dt.Columns.Add("Pub/Unpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("Sub-level");
                dt.Columns.Add("Words1");
                dr = dt.NewRow();
                dr["RoundType"] = "Regular";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["RoundType"] = "Regular";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["RoundType"] = "Regular";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["RoundType"] = "Tie Breaker";
                dt.Rows.Add(dr);

                gvVocabSelMatrix.DataSource = dt;
                gvVocabSelMatrix.DataBind();
                gvVocabSelMatrix.Rows[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                gvVocabSelMatrix.Rows[1].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                gvVocabSelMatrix.Rows[2].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                gvVocabSelMatrix.Rows[3].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                gvVocabSelMatrix.Visible = true;
                btnUpdate.Visible = true;
                lblErr.Visible = false;
                gvVocabSelMatrix.Columns[6].Visible = false;
                gvVocabSelMatrix.Columns[7].Visible = false;
                gvVocabSelMatrix.Columns[8].Visible = false;

            }
            else if(ddlPhase.SelectedIndex==2)
            {
                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("RoundType");
                dt.Columns.Add("Round Count");
                dt.Columns.Add("Pub/Unpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("Sub-level");
                dt.Columns.Add("Words1");
                dt.Columns.Add("Words2");
                dt.Columns.Add("Words3");
                dt.Columns.Add("Words4");
                dr = dt.NewRow();
                dr["RoundType"] = "Regular";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["RoundType"] = "Regular";
                dt.Rows.Add(dr);
               

                gvVocabSelMatrix.DataSource = dt;
                gvVocabSelMatrix.DataBind();
                gvVocabSelMatrix.Rows[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                gvVocabSelMatrix.Rows[1].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");

                gvVocabSelMatrix.Visible = true;
                btnUpdate.Visible = true;
                lblErr.Visible = false;
                gvVocabSelMatrix.Columns[6].Visible = true;
                gvVocabSelMatrix.Columns[7].Visible = true;
                gvVocabSelMatrix.Columns[8].Visible = true;

            }
            
        }

       
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {


        fillProduct();
       
    }
    protected void fillProduct() { 
    
   
        //ddlProductGroup
        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
            
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            
            ddlProduct.DataSource = dsstate;
            ddlProductGroup.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "Name";
            ddlProduct.DataBind();


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
        
    
    }
}