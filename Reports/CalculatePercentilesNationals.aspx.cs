using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_CalculatePercentilesNationals : System.Web.UI.Page
{
    string Year;
    string contestType;
    DataSet dsContestants;
    DataTable dt2 = new DataTable();
    //int chapID = 67;
    Boolean updateFlag = false;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        SaveNationalPercentilesData();
    }
    void SaveNationalPercentilesData()
    {
        AddRecords();
        // Put user code to initialize the page here
        Year = DateTime.Now.Year.ToString();//System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        string[] contType = new string[20];
        contType[0] = "JSB";
        contType[1] = "SSB";
        contType[2] = "JVB";
        contType[3] = "IVB";
        contType[4] = "SVB";
        contType[5] = "MB1";
        contType[6] = "MB2";
        contType[7] = "MB3";
        contType[8] = "MB4";
        contType[9] = "JGB";
        contType[10] = "SGB";
        contType[11] = "EW1";
        contType[12] = "EW2";
        contType[13] = "EW3";
        contType[14] = "PS1";
        contType[15] = "PS3";
        contType[16] = "BB";
        contType[17] = "JSC";
        contType[18] = "ISC";
        contType[19] = "SSC";

        for (int i = 0; i < contType.Length; i++)
        {
            contestType = contType[i];
            CleanUpdate();
            ReadAllJSBScores();
            if (dsContestants.Tables[0].Rows.Count > 29)
                SaveData();
        }
        LinkButton1.Visible = true;
        if (updateFlag == true)
        {
            lblMessage.Text = "Percentiles Updated for the year" + ddlYear.SelectedValue;
        }
        else
        {
            lblMessage.Text = "";//No Scores Present for the Year" + ddlYear.SelectedValue;
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion
    void ReadAllJSBScores()
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
            "Contestant.Score1, Contestant.Score2, Contestant.Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID,IsNUll(Contestant.score3,0) as score3 from Contestant " +
            " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear='" + ddlYear.SelectedValue + //            Year + 
            "' AND (Contestant.Score1 >= 1 OR Contestant.Score2 >= 1) AND Contestant.BadgeNumber like '" + contestType + "%' AND Contestant.ChapterID = 1";
        //AND (Contestant.ChapterID=15 OR Contestant.ChapterID=67)
        //
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        if (dsContestants.Tables[0].Rows.Count > 29)
            ProcessData(dsContestants);
        else
        {
            //Response.Write("No records found!");
            DataTable dt3 = new DataTable();
            dt3.Columns.Add("No data to display");
            //DataGrid1.Caption = contestType + ": Results can not be displayed as the sample size is too small!";
            //DataGrid1.DataSource = dt3;
            //DataGrid1.DataBind();
        }
    }
    void ProcessData(DataSet ds)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Contestant", typeof(int));
        dt.Columns.Add("TotalScore", typeof(int));
        //dt.Columns.Add("Percentile",typeof(float));
        //dt.Columns.Add("Count",typeof(int));
        int[] aryTscore = new int[ds.Tables[0].Rows.Count];
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
            dr["TotalScore"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4])+ Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4])+Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            dt.Rows.Add(dr);
        }

        Array.Sort(aryTscore);
        float pc;
        dt2 = new DataTable();
        dt2.Columns.Add("TotalScore", typeof(int));
        dt2.Columns.Add("Percentile", typeof(string));
        dt2.Columns.Add("Count", typeof(int));
        dt2.Columns.Add("TotalCount", typeof(int));
        dt2.Columns.Add("ContestYear", typeof(int));
        dr = dt2.NewRow();
        dr["TotalScore"] = aryTscore[0];
        int cCount = 0, tCount = 0; //cumulative
        int lastScore = aryTscore[0];
        for (int j = 0; j < aryTscore.Length; j++)
        {
            if (lastScore == aryTscore[j])
            {
                cCount++;
                tCount++;
            }
            else
            {
                lastScore = aryTscore[j];
                dr["Count"] = cCount;
                dr["TotalCount"] = tCount;
                pc = (float)tCount / aryTscore.Length;
                dr["Percentile"] = String.Format("{0:P1}", Math.Round(pc, 3));
                dt2.Rows.Add(dr);
                dr = dt2.NewRow();
                dr["TotalScore"] = aryTscore[j];
                cCount = 1;
                tCount++;
            }
        }
        dr["Count"] = cCount;
        dr["TotalCount"] = tCount;
        pc = (float)tCount / aryTscore.Length;
        dr["Percentile"] = String.Format("{0:P1}", Math.Round(pc, 3));
        dr["ContestYear"] = ddlYear.SelectedValue ;
        dt2.Rows.Add(dr);
        //DataGrid1.Caption = contestType + " as of " + DateTime.Today.ToShortDateString();
        //DataGrid1.DataSource = dt2;
        //DataGrid1.DataBind();

        
    }

 /*   private void btnSave_Click(object sender, System.EventArgs e)
    {
        // Update rows
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "UPDATE PercentileRankNationals SET " + contestType + " = @PRV WHERE Score=@Score";
        SqlCommand insCmd = new SqlCommand(commandString, connection);
        insCmd.Parameters.Add("@PRV", SqlDbType.Float);
        insCmd.Parameters.Add("@Score", SqlDbType.Int);
        //DataRow dr;
        foreach (DataRow dr in dt2.Rows)
        {
            insCmd.Parameters["@PRV"].Value = dr["Percentile"];
            insCmd.Parameters["@Score"].Value = dr["TotalScore"];
            insCmd.ExecuteNonQuery();
        }
        connection.Close();
        /*Response.Clear();
        Response.AppendHeader("content-disposition","attachment;filename="+contestType+".html");
        Response.Charset ="";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "text/html";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
    */
    private void AddRecords()
      {
          // Just to add rows
          string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 
	
          // create and open the connection object
          System.Data.SqlClient.SqlConnection connection =
              new System.Data.SqlClient.SqlConnection(connectionString);
          connection.Open();

          //Delete everything /** Commented on 06-02-2013 to calculate and update percentiles on a yearly basis**/
          //string delCommandString = "Delete from PercentileRankNationals";
        //  SqlCommand delCommand = new SqlCommand(delCommandString,connection);
        //  delCommand.ExecuteNonQuery();
        //SET IDENTITY_INSERT PercentileRankNationals ON
          string commandStringTest = "SELECT Count(*) FROM PercentileRankNationals  WHERE Score=@Score and ContestYear= @ContestYear";
          SqlCommand testCmd = new SqlCommand(commandStringTest, connection);
          testCmd.Parameters.Add("@Score", SqlDbType.Int);
          testCmd.Parameters.Add("@ContestYear", SqlDbType.Int);
     
          string commandString = "INSERT INTO PercentileRankNationals (Score,ContestYear) VALUES (@Score,@ContestYear)";
          SqlCommand insCmd = new SqlCommand(commandString,connection);
          insCmd.Parameters.Add("@Score",SqlDbType.Int);
          insCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

          for(int i =0; i <50; i++)
          {
              insCmd.Parameters["@Score"].Value = i;
              insCmd.Parameters["@ContestYear"].Value = ddlYear.SelectedValue;

              testCmd.Parameters["@Score"].Value = i;
              testCmd.Parameters["@ContestYear"].Value = ddlYear.SelectedValue;

              if (Convert.ToInt32(testCmd.ExecuteScalar()) <= 0)
              {
                  insCmd.ExecuteNonQuery();
              }
          }
          connection.Close();
      }
    private void CleanUpdate()
    {
        // Just to update existing rows
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        string commandStringTest = "SELECT Count(*) FROM PercentileRankNationals  WHERE Score=@Score and ContestYear= @ContestYear";
        SqlCommand testCmd = new SqlCommand(commandStringTest, connection);
        testCmd.Parameters.Add("@Score", SqlDbType.Int);
        testCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

        string commandStringUpdate = "UPDATE PercentileRankNationals SET [" + contestType + "-Percentile] = Null, [" + contestType +
         "-Count] = Null, [" + contestType + "-TotalCount] = Null WHERE Score=@Score and ContestYear= @ContestYear";// +ddlYear.SelectedValue;
        SqlCommand updateCmd = new SqlCommand(commandStringUpdate, connection);
        updateCmd.Parameters.Add("@Score", SqlDbType.Int);
        updateCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

        //SET IDENTITY_INSERT PercentileRankNationals ON

        foreach (DataRow dr in dt2.Rows)
        {
            testCmd.Parameters["@Score"].Value = dr["TotalScore"];
            testCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];

            updateCmd.Parameters["@Score"].Value = dr["TotalScore"];
            updateCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];

            if (testCmd.ExecuteNonQuery() > 0)
            {
                updateCmd.ExecuteNonQuery();
            }
        }
        connection.Close();
    }
    private void SaveData()
    {
        // Update rows
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "UPDATE PercentileRankNationals SET [" + contestType + "-Percentile] = @PRV, [" + contestType +
            "-Count] = @Count, [" + contestType + "-TotalCount] = @TCount WHERE Score=@Score and ContestYear=@ContestYear";
        SqlCommand insCmd = new SqlCommand(commandString, connection);
        insCmd.Parameters.Add("@PRV", SqlDbType.VarChar, 4);
        insCmd.Parameters.Add("@Count", SqlDbType.Int);
        insCmd.Parameters.Add("@TCount", SqlDbType.Int);
        insCmd.Parameters.Add("@Score", SqlDbType.Int);
        insCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

        //DataRow dr = dt2.NewRow();
        foreach (DataRow dr in dt2.Rows)
        {
            insCmd.Parameters["@PRV"].Value = dr["Percentile"];
            insCmd.Parameters["@Count"].Value = dr["Count"];
            insCmd.Parameters["@TCount"].Value = dr["TotalCount"];
            insCmd.Parameters["@Score"].Value = dr["TotalScore"];
            insCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];
            if (insCmd.ExecuteNonQuery() > 0)
            {
                updateFlag = true;
            }
        }
        connection.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SaveNationalPercentilesData();
    }
}
