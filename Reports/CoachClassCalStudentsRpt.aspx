﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="CoachClassCalStudentsRpt.aspx.vb" Inherits="CoachClassCalStudentsRpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                <br />
            </td>
        </tr>
        <tr>

            <td class="ContentSubTitle" valign="top" align="center">
                <h2>Coach Class Calendar</h2>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: middle">

                <table width="100%">
                    <tr id="tblChild" runat="server">
                        <td style="width: 40%"></td>
                        <td style="width: 107px"><b>Child Name:</b></td>
                        <td style="width: 213px">
                            <asp:DropDownList ID="ddlChild" runat="server" DataTextField="Name" DataValueField="ChildNumber" Width="110px"></asp:DropDownList></td>
                        <td style="width: 20%"></td>
                    </tr>
                    <tr>
                        <td style="width: 40%"></td>
                        <td style="width: 107px"><b>Event Year:</b></td>
                        <td style="width: 213px">
                            <asp:DropDownList ID="ddlEventYear" runat="server"></asp:DropDownList></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 40%"></td>
                        <td align="right" style="width: 107px">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
                        <td></td>

                        <td></td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMsg" runat="server" Style="color: red"></asp:Label>
                <asp:HiddenField ID="hdTableCnt" runat="server"></asp:HiddenField>
                <asp:Repeater ID="rptCoachClass" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="rep_hdWeekCnt" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdProductGrp" runat="server" Value='<%# Eval("ProductGroupId")%>'></asp:HiddenField>
                        <asp:HiddenField ID="hdProduct" runat="server" Value='<%# Eval("ProductId") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hdHideModify" runat="server" Value="false" />

                        <table width="100%" runat="server" id="tblhead">
                            <tr>
                                <td style="text-align: right;"><b>Table
                                    <asp:Label ID="lblTableCnt" runat="server"></asp:Label>
                                    :</b></td>
                                <%--  <td>&nbsp;</td>
                                --%>
                                <td><b>Product Group: </b>
                                    <asp:Label ID="lblPrdGrp" runat="server" Text='<%# Eval("ProductGroupName")%>'></asp:Label>
                                </td>
                                <td><b>Product : </b>
                                    <asp:Label ID="lblProduct" runat="server" Text='<%# Eval("ProductName")%>'></asp:Label></td>
                                <td><b>Semester : </b>
                                    <asp:Label ID="lblSemester" runat="server" Text='<%# Eval("Semester")%>'></asp:Label></td>
                                <td><b>Level : </b>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Level")%>'></asp:Label></td>
                                <td><b>Session :</b><asp:Label ID="lblSessionNo" runat="server" Text='<%# Eval("SessionNo")%>'></asp:Label></td>
                                <td><b>Coach Name :</b><asp:Label ID="Label1" runat="server" Text='<%# Eval("CoachName")%>'></asp:Label></td>
                            </tr>
                        </table>

                        <asp:GridView ID="gvCoachClassCal" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="1100">
                            <Columns>
                                <asp:BoundField HeaderText="Child Name" DataField="ChildName" />
                                <asp:BoundField HeaderText="Year" DataField="EventYear" />
                                <asp:BoundField HeaderText="Event" DataField="EventCode" />
                                <asp:BoundField HeaderText="Product Group" DataField="ProductGroupCode" />
                                <asp:BoundField HeaderText="Product" DataField="ProductCode" />
                                <asp:BoundField HeaderText="ClassType" DataField="ClassType" />
                                <asp:BoundField HeaderText="Date" DataField="date" DataFormatString="{0:MM/dd/yyyy}" />
                                <asp:BoundField HeaderText="Day" DataField="day" />
                                <asp:BoundField HeaderText="Time" DataField="Time" />
                                <asp:BoundField HeaderText="Duration" DataField="Duration" />
                                <asp:BoundField HeaderText="Ser#" DataField="SerNo" />
                                <asp:BoundField HeaderText="Week#" DataField="WeekNo" />
                                <asp:BoundField HeaderText="Coach" DataField="Coach" />
                                <asp:BoundField HeaderText="Status" DataField="Status" />

                                <asp:BoundField HeaderText="Substitute" DataField="SubstituteName" />
                                <asp:BoundField HeaderText="Makeup" DataField="MakeupName" />
                                <asp:BoundField HeaderText="Reason" DataField="Reason" />
                                <asp:BoundField HeaderText="Home Work" DataField="qreleasedate" DataFormatString="{0:MM/dd/yyyy}" />
                                <asp:BoundField HeaderText="HW Due Date" DataField="qdeadlinedate" DataFormatString="{0:MM/dd/yyyy}" />
                                <asp:BoundField HeaderText="Answers" DataField="areleasedate" DataFormatString="{0:MM/dd/yyyy}" />

                            </Columns>

                        </asp:GridView>



                    </ItemTemplate>
                </asp:Repeater>

                <div id="CoachList" runat="server" visible="false">
                    <asp:Label ID="lblCoachMsg" runat="server" ForeColor="Red">Your coach has not yet opened up the calendar.</asp:Label>
                    <asp:GridView ID="gvCoachReg" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField HeaderText="Child Name" DataField="ChildName" />
                            <asp:BoundField HeaderText="Year" DataField="EventYear" />
                            <asp:BoundField HeaderText="Event" DataField="EventCode" />
                            <asp:BoundField HeaderText="Product Group" DataField="ProductGroupCode" />

                            <asp:BoundField HeaderText="Product GroupName" DataField="ProductGroupName" />
                            <asp:BoundField HeaderText="Product" DataField="ProductCode" />
                            <asp:BoundField HeaderText="ProductName" DataField="ProductName" />
                            <asp:BoundField HeaderText="Semester" DataField="Semester" />
                            <asp:BoundField HeaderText="Level" DataField="Level" />
                            <asp:BoundField HeaderText="SessionNo" DataField="SessionNo" />
                            <asp:BoundField HeaderText="Coach" DataField="Coach" />
                        </Columns>

                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

