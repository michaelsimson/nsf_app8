﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="AddUpdateSBVBSelMatrix.aspx.cs" Inherits="AddUpdateSBVBSelMatrix" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     


    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
         <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>

        </tr>
        <tr>

            <td>
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> Add/Update Vocabulary Selection Matrix</strong> </div>
            </td>
        </tr>
      <tr>

            <td align="right">
                
            </td>
        </tr>
       <tr>
           <td align="center">
               

                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
 <table width="50%" visible="true" >
                     
                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server">
                            </asp:DropDownList>
                        </td>
                         <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" AutoPostBack="True" runat="server"  >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Finals</asp:ListItem>
                          <asp:ListItem Value="2">Chapter</asp:ListItem>
                        
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" >
                                <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="SB" >Spelling</asp:ListItem>
                          <asp:ListItem Value="VB">Vocabulary</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        
                        <td>Product</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True"  >
                               <%-- <asp:ListItem Value="0">Select one</asp:ListItem>
                          <asp:ListItem Value="JVB">Junior Vocabulary</asp:ListItem>
                          <asp:ListItem Value="IVB">Intermediate Vocabulary</asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                        
                        <td>Phase</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPhase" AutoPostBack="True" runat="server"  >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Phase I</asp:ListItem>
                          <asp:ListItem Value="2">Phase II</asp:ListItem>
                        
                            </asp:DropDownList>
                       
                        </td>
                            
                             </tr>
                       
     
        
   
         <td colspan="10" align="center">
                        <asp:Label ID="lblErr" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"></asp:Label>
                        <br /> <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>
                     </td>
         

     </tr>
     <tr align="center">

         <td colspan="10"><div style="width:800px; height:155px; overflow:auto;">
             
             <asp:GridView ID="gvVocabSelMatrix" runat="server" Width="100%" AutoGenerateColumns="false">
                 <Columns>
        <asp:BoundField DataField="RoundType" HeaderText="Round Type" />
        <asp:TemplateField HeaderText="Round Count">
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Pub/Unpub">
            <ItemTemplate>
                 <asp:DropDownList ID="ddlPubUnpub" AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="P" >P</asp:ListItem>
                          <asp:ListItem Value="U">U</asp:ListItem>
                          </asp:DropDownList>
                <%--<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>--%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Level">
            <ItemTemplate>
                  <asp:DropDownList ID="ddlLevel" AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="1" >1</asp:ListItem>
                          <asp:ListItem Value="2">2</asp:ListItem>
                          </asp:DropDownList>
            </ItemTemplate>
            <%--<FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
                 <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" />
            </FooterTemplate>--%>
        </asp:TemplateField>
            <asp:TemplateField HeaderText="Sub-level">
            <ItemTemplate>
                <asp:DropDownList ID="ddlSublevel" AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="P" >1</asp:ListItem>
                          <asp:ListItem Value="U">U</asp:ListItem>
                          </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
          <asp:TemplateField HeaderText="Words1">
            <ItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
        </Columns>


             </asp:GridView>
         </div></td>

     </tr>

<tr align="center">

    <td colspan="10">
        <asp:Button ID="btnUpdate" runat="server" Visible="false" Text="Update" /></td>
</tr>

                  
                  
                </table>
               </td>

       </tr>

  
        
                </table>
       </asp:Content>
    