using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_ChapLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string userID ="";
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        userID = SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, "usp_GetUserLoginID", new SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)), new SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text))).ToString();
        
        if (userID != "")
        {
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);
            connection.Open();

            // get records from the products table
            string commandString = "Select Volunteer.RoleId, Volunteer.MemberId, Volunteer.RoleCode, IndSpouse.AutoMemberID, IndSpouse.ChapterID from IndSpouse " +
                " INNER JOIN Volunteer On Volunteer.MemberId=IndSpouse.AutoMemberID " +
                " Where IndSpouse.Email='" + txtUserId.Text + "'";

            // command string and connection

            SqlDataAdapter daRanks = new SqlDataAdapter(commandString, connection);
            DataSet dsRanks = new DataSet();
            daRanks.Fill(dsRanks);
            bool coordRole = false;
            txtErrMsg.Text = dsRanks.Tables[0].Rows[0].ItemArray[4].ToString();
            if (dsRanks.Tables[0].Rows.Count > 0)
            {
                Session["loginChapterId"] = Convert.ToInt32(dsRanks.Tables[0].Rows[0].ItemArray[4].ToString());
                for (int i = 0; i < dsRanks.Tables[0].Rows.Count; i++)
                    if (dsRanks.Tables[0].Rows[i].ItemArray[2].ToString().Trim() == "ChapterC" ||
                        Convert.ToInt32(dsRanks.Tables[0].Rows[i].ItemArray[0].ToString()) == 5)
                        coordRole = true;
            }
            if (coordRole == true)
            {
                Session["LoggedIn"] = true;
                Response.Redirect("xlReport2.aspx");
            }
            }
            else txtErrMsg.Text = "Your ID is not recognized";
        
    }
}
