<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="OrderMedals.aspx.cs" Inherits="Reports_OrderMedals" Title="Order Medals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="server">
    <%--<link href="~/css/style.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">

        .style1
        {
            width: 152px;
        }
        .style2
        {
            width: 92px;
        }
        .auto-style1 {
            width: 297px;
        }
        .auto-style2 {
            width: 163px;
        }
    </style>
    <script>
        function getValidateSilver(ddl)
        {


            alert(ddl.selectedIndex);
        }
        function ConfirmToSave(sender) {

            var btnlabel = sender.value;

            if (confirm("Are you sure want to save in the database?")) {

                    return true;
                }
                else {

                    return false;

                }
        }
        

   </script>

		<table cellpadding="5px" cellspacing ="0" border ="0">
    <tr><td colspan ="8" align = "center" class="title02" > North South Foundation <br />
			Order Preparation</td> </tr> 
			 <tr><td colspan ="8" align = "left" >
			 <asp:hyperlink  CssClass="btn_02"  id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
			</td> </tr> 
			
			<tr>
			
			<td align="left" style="width: 180px;">
			<asp:label id="lblChapter" runat="server"></asp:label>
</td>
<%--<td> Event <asp:DropDownList ID="ddlEvent" runat="server"> </asp:DropDownList></td>
--%>			<td width="75px" align="right" valign="middle">
			Year :</td><td align="left" class="style2"><asp:DropDownList ID="ddlyear" runat="server" AutoPostBack="True" onselectedindexchanged="ddlyear_SelectedIndexChanged"> </asp:DropDownList>
         </td>
	<td align="left" class="auto-style1">Contest Dates:<asp:DropDownList id="ddlWeek" runat="server"	
            AutoPostBack="true" onselectedindexchanged="ddlWeek_SelectedIndexChanged" Width="124px"></asp:DropDownList> <%--DataTextField="Weektext" DataValueField="weekid"--%> 
        </td>
       <td align="left" class="auto-style2">Round: 
           <asp:DropDownList ID="ddlRound" runat="server" AutoPostBack="true">
           </asp:DropDownList>
        </td>
        <td><asp:Button Id="btnSubmit" runat="server" Text="Submit" Width="100px" 
                onclick="btnSubmit_Click" /></td>
        <td align="left"> <asp:Button id="btnExportOrder" Enabled="false"  runat="server" Text="Export Order" OnClientClick="" OnClick="btnExportOrder_Click"></asp:Button></td>
                <td align="left"> &nbsp;</td>

                <%--<asp:Button ID="hiddenbtn" runat="server"  Text="Button" style="display:none;" />--%>
			</tr>
       <tr><td colspan ="8" align = "center" >
			<asp:label id="lblError"  runat="server"></asp:label>
			<br />
			<%--<asp:datagrid id="DataGrid1" BorderStyle="Solid" BorderColor="Black" ItemStyle-HorizontalAlign="Left" runat="server"></asp:datagrid>--%>
           <asp:GridView ID="DataGrid1" runat="server" AutoGenerateColumns="False" DataKeyNames="IDDataKey" 
                OnRowEditing="DataGrid1_RowEditing" OnRowCancelingEdit="DataGrid1_RowCancelingEdit" OnRowUpdating="DataGrid1_RowUpdating" OnRowCommand="DataGrid1_RowCommand" OnRowDataBound="DataGrid1_RowDataBound">
               <Columns>
                   <asp:TemplateField HeaderText="Contest">
                       <ItemTemplate>
                           <asp:HiddenField ID="hfIDDataKey" Value='<%# Bind("IDDataKey")%>'  runat="server" />
                           <asp:HiddenField ID="hfRankTrophyID" Value='<%# Bind("RankTrophyID")%>'  runat="server" />
                           <asp:HiddenField ID="hfPartMedTrophyID" Value='<%# Bind("PartMedTrophyID")%>' runat="server" />
                           <asp:Label ID="Label1" runat="server" Text='<%#Eval("Contest") %>'></asp:Label>
                       </ItemTemplate>

                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="PartCert">
                       <ItemTemplate>
                           <asp:Label ID="lblPartCert" runat="server" Text='<%#Eval("PartCert") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Gold">
                       <EditItemTemplate>
                           <asp:DropDownList ID="ddlGold" OnPreRender="ddlSelectOptions" runat="server"></asp:DropDownList>
                        <asp:Label ID="lblGold" Visible="false" runat="server" Text='<%#Eval("Gold") %>'></asp:Label>
                       </EditItemTemplate>
                       <ItemTemplate>
                           <asp:Label ID="Label3" runat="server" Text='<%#Eval("Gold") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Silver">
                       <EditItemTemplate>
                           <asp:DropDownList ID="ddlSilver" OnPreRender="ddlSelectOptions" runat="server"></asp:DropDownList>
                        <asp:Label ID="lblSilver" Visible="false" runat="server" Text='<%#Eval("Silver") %>'></asp:Label>
                          
                       </EditItemTemplate>
                       <ItemTemplate>
                           <asp:Label ID="Label4" runat="server" Text='<%#Eval("Silver") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Bronze">
                       <EditItemTemplate>
                           <asp:DropDownList ID="ddlBronze" OnPreRender="ddlSelectOptions" runat="server"></asp:DropDownList>
                          <asp:Label ID="lblBronze" runat="server" Visible="false" Text='<%#Eval("Bronze") %>'></asp:Label>
                       </EditItemTemplate>
                       <ItemTemplate>
                           <asp:Label ID="Label5" runat="server" Text='<%#Eval("Bronze") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="UniqChild">
                        <EditItemTemplate>
                           <asp:TextBox ID="txtUniqChild" Visible="true" style="width: 40px;" runat="server" Text='<%#Eval("UniqChild") %>'></asp:TextBox>
                       </EditItemTemplate>
                       <ItemTemplate>
                           <asp:Label ID="UniqChild" runat="server" Text='<%#Eval("UniqChild") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField>
                       <EditItemTemplate>
                           <asp:LinkButton ID="LinkButton2" CommandName="Update" runat="server">Update</asp:LinkButton>
                           <br />
                           <asp:LinkButton ID="LinkButton3" CommandName="Cancel" runat="server">Cancel</asp:LinkButton>
                       </EditItemTemplate>
                       <ItemTemplate>
                           <asp:LinkButton ID="lnkbtnEdit" Visible="true" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                       </ItemTemplate>
                   </asp:TemplateField>
               </Columns>
            </asp:GridView>
				</td>

       </tr>


		</table>
        <asp:HiddenField ID="hdnFlag" runat="server" />
			</asp:Content>


 
 
 