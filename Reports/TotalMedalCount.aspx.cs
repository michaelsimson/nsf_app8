using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;


public partial class Reports_TotalMedalCount : System.Web.UI.Page
{
    DataSet dsMedals = new DataSet();

    int[] category = new int[33];
    int tCount = 0;
    int chapID;
    string Year = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        //if(Session["Admin"].ToString() != "Yes")
        //	Response.Redirect("Default.aspx");
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        Year =
            System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        //chapID = 15;
        lblChapter.Text = GetChapterName(chapID);
        GetCounts();
    }
    private void GetCounts()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        SqlConnection connection =
            new SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select Contest.ContestID, Contest.ContestCategoryID, Contest.ContestDate, Contest.NSFChapterID,Contestant.ChildNumber FROM Contest " +
            " INNER JOIN Contestant ON Contest.ContestID = Contestant.ContestCode WHERE Contest_Year='" + Year + "' AND Contest.NSFChapterID=" + chapID + " AND Contestant.PaymentReference IS NOT NULL ORDER BY Contest.ContestID ";
        SqlCommand myCommand = new SqlCommand(commandString, connection);


        SqlDataAdapter daMedals = new SqlDataAdapter(commandString, connection);

        tCount = daMedals.Fill(dsMedals);

        GetContestCount();
        //	dsMedals.Tables[0].Columns.Add(dc);
        //DataGrid1.DataSource = dsMedals.Tables[0];
        //DataGrid1.DataBind();

    }
    public void GetContestCount()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
           


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        //connection.Open();

        // get records from the products table
        //string commandString = "Select ContestCode, ChildNumber from Contestant where ChapterID=" + ChapterID;

        //SqlDataAdapter daCounts = new SqlDataAdapter(commandString,connection);
        //DataSet dsCounts = new DataSet();
        //int retValue = daCounts.Fill(dsCounts);
        DataTable dt = new DataTable();
        dt.Columns.Add("Contest", typeof(string));
        dt.Columns.Add("PartCert", typeof(int));
        dt.Columns.Add("ExcelCert", typeof(int));
        dt.Columns.Add("Gold", typeof(int));
        dt.Columns.Add("Silver", typeof(int));
        dt.Columns.Add("Bronze", typeof(int));
        dt.Columns.Add("PartMed", typeof(int));
        dt.Columns.Add("VolCert", typeof(int));
        DataRow dr;
        string[] catName = new string[33];
        int[] catCount = new int[33];
        int cCount = 0;
        int goldCount = 0, silverCount = 0, bronzeCount = 0, excelCount = 0;
        bool found;
        //
        for (int j = 0; j < dsMedals.Tables[0].Rows.Count; j++)
        {
            found = false;
            for (int i = 0; i < cCount; i++)
            {
                if (catName[i] == dsMedals.Tables[0].Rows[j].ItemArray[0].ToString())
                {
                    catCount[i]++;
                    found = true;
                    //lblError.Text = dr["Contest"].ToString();// dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                }
            }
            if (found == false)
            {
                catName[cCount] = dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                catCount[cCount] = 1;
                cCount++;
            }
        }
        ArrayList al = new ArrayList();
        for (int i = 0; i < dsMedals.Tables[0].Rows.Count; i++)
        {
            if (al.Contains(dsMedals.Tables[0].Rows[i].ItemArray[4].ToString()) == false)
                al.Add(dsMedals.Tables[0].Rows[i].ItemArray[4].ToString());
        }
        for (int i = 0; i < cCount; i++)
        {
            dr = dt.NewRow();
            dr["Contest"] = GetLabels(Convert.ToInt32(catName[i]), "ContestCategoryID");
            dr["PartCert"] = catCount[i];
            if (catCount[i] >= 10)
            {
                dr["Gold"] = 1;
                dr["Bronze"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 3;
                goldCount++;
                silverCount++;
                bronzeCount++;
                excelCount += 3;
            }
            else if (catCount[i] >= 8)
            {
                dr["Gold"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 2;
                goldCount++;
                silverCount++;
                excelCount += 2;
            }
            else if (catCount[i] >= 5)
            {
                dr["Gold"] = 1;
                dr["ExcelCert"] = 1;
                goldCount++;
                excelCount += 1;
            }

            dt.Rows.Add(dr);
        }
        int increment = 1;
        if (dsMedals.Tables[0].Rows.Count > 50)
            increment = 2;
        else if (dsMedals.Tables[0].Rows.Count > 100)
            increment = 3;
        else if (dsMedals.Tables[0].Rows.Count > 150)
            increment = 4;
        int distRankMedals = (int)(Math.Round(.67 * (goldCount + .5), 0) + Math.Round(.67 * (silverCount + .5), 0) +
            Math.Round(.67 * (bronzeCount + .5), 0));
        dr = dt.NewRow();
        dr["PartCert"] = dsMedals.Tables[0].Rows.Count;
        dr["ExcelCert"] = excelCount + increment;
        dr["Gold"] = goldCount;
        dr["Silver"] = silverCount;
        dr["Bronze"] = bronzeCount;
        //distinct contestants - distinct rank holders
        dr["PartMed"] = al.Count - distRankMedals + increment;
        if (dsMedals.Tables[0].Rows.Count < 51)
            dr["VolCert"] = 8;
        else if (dsMedals.Tables[0].Rows.Count < 100)
            dr["VolCert"] = 12;
        else if (dsMedals.Tables[0].Rows.Count < 150)
            dr["VolCert"] = 15;
        else if (dsMedals.Tables[0].Rows.Count < 200)
            dr["VolCert"] = 20;
        else dr["VolCert"] = 25;
        dt.Rows.Add(dr);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
        //lblError.Text = dr["Contest"].ToString();
        //lblError.Text = dt.Rows.Count.ToString();

        //	return al.Count;
        //return retValue;

    }
    public string GetLabels(int idNumber, string colName)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
           


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString1 = "Select ContestCategoryID from Contest where ContestID = " + idNumber;


        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString1;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        reader.Read();
        int retVal;
        retVal = reader.GetInt32(0);
        connection.Close();
        string commandString2 = "Select ContestDesc from ContestCategory where " + colName + " = " + retVal;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        string retValue;
        reader2.Read();
        retValue = reader2.GetString(0);
        // close connection return value
        connection.Close();
        return retValue;
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
           
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select City, State from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0) + ", " + reader.GetString(1);
        // close connection, return values
        connection.Close();
        return retValue;

    }
}

 