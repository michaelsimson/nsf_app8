﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;

public partial class ExpSummaryReport : System.Web.UI.Page
{
    bool isChapterRepot=false;
    bool Isupdate = false;
    DataTable dtexcelcate = new DataTable();
    int i;
    string firstquert;
    DataTable Mergecopy = new DataTable();
    int fiscyear;
    public Int32 Expcategorystr = 0;
    public Int32 Expcategorystr1 = 0;
    public Int32 Expcatdesc = 0;
    public Int32 Expchapterystr = 0;
    public Int32 Expchapterystr1 = 0;
    double less;
    double merchless;
    double grossexpense;
    double grantcal;
    double grantmerch;
    string grantval;
    DataSet dsnew;
    DataTable dtnw;
    string clusqry;
    string Zoneqry;
    string Qrycondition;
    string calc;
    int endall;
    int Start;
    string Qrygrants;
    int newvar;
    string str;
    string Qry;
    string StaYear = System.DateTime.Now.Year.ToString();
    DataTable dtnewgrid;
    double grantcalFy;
    double grantmerchFy;
    double lessFy;
    double merchlessFy;
    DataTable dtallcost;
    string allocatedcostCy1;
    double valmerch;
    bool isnorecord = false;
    protected void ddchaptercode(object sender, System.EventArgs e)
    {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            string Chaptercode = "select ChapterCode,[state] from chapter order by [state],ChapterCode";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Chaptercode);
            ddlTemp.DataSource = ds;
            DataTable dt = ds.Tables[0];
            ddlTemp.DataTextField = "ChapterCode";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "[Select Chapter]");
            ddlTemp.SelectedIndex = 0;
    }
    protected void ddfrmchaptercode(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        string Chaptercode = "select ChapterCode,[state] from chapter order by [state],ChapterCode";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Chaptercode);
        ddlTemp.DataSource = ds;
        DataTable dt = ds.Tables[0];
        ddlTemp.DataTextField = "ChapterCode";
        ddlTemp.DataBind();
        ddlTemp.Items.Insert(0, "[Select ToChapter]");
        ddlTemp.SelectedIndex = 0;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Divpan.Visible = false;
        Button1.Visible = true;
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
        {
            ddZone.Enabled = true;
            ddNoyear.Enabled = true;
            DDyear.Enabled = true;
            ddevent.Enabled = true;
            ddCluster.Enabled = true;
            ddchapter.Enabled = true;
            pageloadbutton();
        }
        if (!IsPostBack)
        {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            Pnldisp.Visible = false;
            DDFront.Visible = true;
            DDlfrontchoice(DDFront);
            Lboption.Visible = false;
            Lbcontestyear.Visible = false;
            lbReport.Visible = true;
            if ((Session["RoleId"].ToString() == "38") || (Session["RoleId"].ToString() == "37"))
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=" + Session["RoleId"] + " and [National]='Y' and MemberID = " + Session["LoginID"])) > 0)
                {
                    hdnTechNational.Value = "Y";
                }
                else
                {
                    hdnTechNational.Value = "N";
                }
            }

            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || ((Session["RoleId"].ToString() == "38") && (hdnTechNational.Value == "Y")) || ((Session["RoleId"].ToString() == "37") && (hdnTechNational.Value == "Y")) || (Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5")))
            {
                if ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85"))
                {
                    if (!IsPostBack)
                    {
                        Expcategory();
                    }
                }
                if (!IsPostBack)
                {
                    if ((Session["RoleId"].ToString() == "5"))
                    {
                        Chapter();
                        Cluster();
                        Event();
                        Yearscount();
                        years();
                    }
                    else if ((Session["RoleId"].ToString() == "3"))
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct zoneid from volunteer where MemberID='" + Session["LoginID"] + "' and TeamLead='" + Session["TL"] + "'  and zoneid is not null");
                        DataTable dt = ds.Tables[0];
                        Txthidden.Text = dt.Rows[0]["zoneid"].ToString();
                        if (Txthidden.Text != "")
                        {
                            Zone();
                            Cluster();
                            Chapter();
                            Event();
                            Yearscount();
                            years();
                        }
                        else
                        {
                            Pnldisp.Visible = false;
                            lblNoPermission.Text = "Sorry, you don't have ZoneCode ";
                        }
                    }
                    else if ((Session["RoleId"].ToString() == "4"))
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct clusterid from volunteer where MemberID='" + Session["LoginID"] + "' and clusterid is not null");
                        DataTable dt = ds.Tables[0];
                        Txthidden.Text = dt.Rows[0]["clusterid"].ToString();
                        if (Txthidden.Text != "")
                        {
                            Cluster();
                            Zone();
                            Chapter();
                            Event();
                            Yearscount();
                            years();
                        }
                        else
                        {
                            Pnldisp.Visible = false;
                            lblNoPermission.Text = "Sorry, you don't have Cluster ";
                        }
                    }
                    else
                    {
                        Zone();
                        Event();
                        Yearscount();
                        years();
                    }
                }
            }
            else
            {
                Pnldisp.Visible = false;
                lblNoPermission.Visible = true;
                lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
            }
        }
      }
    }
    protected void pageloadbutton()
    {
        Griddisp.EnableViewState = false;
            buttonclick();
        Griddisp.Visible = false;
        lblall.Visible = false;
        lbldisp.Visible = false;
    }
    protected void DDlfrontchoice(DropDownList DDobject)
    {
        DDobject.Items.Clear();
        DDobject.Items.Insert(0, new ListItem("Merchandise", "4"));
        DDobject.Items.Insert(0, new ListItem("Allocated Cost", "3"));
        DDobject.Items.Insert(0, new ListItem("Chapter", "2"));
        DDobject.Items.Insert(0, new ListItem("Expense category", "1"));
        DDobject.Items.Insert(0, new ListItem("[Select Report]", "0"));
    }
    protected void Event()
    {
            ddevent.Items.Clear();
            ddevent.Items.Insert(0, new ListItem("PrepClub", "19"));
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
            {
                ddevent.Items.Insert(0, new ListItem("Online Coaching", "13"));
            }
            ddevent.Items.Insert(0, new ListItem("Workshop", "3"));
            ddevent.Items.Insert(0, new ListItem("Regionals", "2"));
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
            {
                ddevent.Items.Insert(0, new ListItem("Finals", "1"));
            }
            ddevent.Items.Insert(0, new ListItem("All", "-1"));
            ddevent.Items.Insert(0, new ListItem("[Select Event]", "0"));
    }
    protected void Expcategory()
    {
        string Expcatqry = "select distinct   ExpCatDesc,ExpCatID from ExpenseCategory order by ExpCatDesc";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expcatqry);
        DDcategory.DataSource = ds;
        DataTable dt = ds.Tables[0];
        DDcategory.DataTextField = "ExpCatDesc";
        DDcategory.DataValueField = "ExpCatID";
        DDcategory.DataBind();
        DDcategory.Items.Insert(0, "[Select Category]");
    }
    protected void Yearscount()
    {
        ddNoyear.Items.Clear();
        ddNoyear.Items.Add("[Select No of Years]");
        ddNoyear.Items.Add("1");
        ddNoyear.Items.Add("2");
        ddNoyear.Items.Add("3");
        ddNoyear.Items.Add("4");
        ddNoyear.Items.Add("5");
        ddNoyear.Items.Add("6");
        ddNoyear.Items.Add("7");
        ddNoyear.Items.Add("8");
        ddNoyear.Items.Add("9");
        ddNoyear.Items.Add("10");
    }
    protected void years()
    {
        DDyear.Items.Clear();
        DDyear.Items.Add("Year");
        DDyear.Items.Add("Calendar Year");
        DDyear.Items.Add("Fiscal Year");
    }
    protected void Zone()
    {
        try
        {
            ddZone.Enabled = true;
            ddZone.Items.Clear();
            ddZone.Items.Insert(0, "[Select Zone]");
            if ((Session["RoleId"].ToString() == "5"))
            {
                Zoneqry = "select ZoneCode,ZoneId from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Zone where  ZoneId='" + Txthidden.Text + "'  and ZoneCode is not null";
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Cluster where ClusterId='" + Txthidden.Text + "'";
            }
            else
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || ((Session["RoleId"].ToString() == "38") && (hdnTechNational.Value == "Y")))
                    {
                        Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                    }
                }
                else if (ddevent.SelectedItem.Text == "All")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("All", "-1"));
                    ddCluster.Items.Insert(0, "All");
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Finals")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Finals", "1"));
                    ddZone.Enabled = false;
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Online Coaching")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddZone.Enabled = false;
                    return;
                }
                else
                {
                    Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Zoneqry);
            ddZone.DataSource = ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddZone.Items.Clear();
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    ddZone.Enabled = false;
                }
                else
                {
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        ddZone.Items.Insert(0, "[Select Zone]");
                        ddZone.Items.Insert(0, "All");
                    }
                    else
                    {
                        ddZone.Items.Insert(0, "All");
                        ddZone.Items.Insert(0, "[Select Zone]");
                    }
                }
            }
            else
            {
                ddZone.Items.Insert(0, "[Select Zone]");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Cluster()
    {
        try
        {
            ddCluster.Enabled = true;
            ddCluster.Items.Clear();
            ddCluster.Items.Insert(0, "[Select Cluster]");
            if ((Session["RoleId"].ToString() == "5"))
            {
                string clusqry = "select ClusterId,clustercode from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                string clusqry = "select clustercode,ClusterId from Cluster where clusterId='" + Txthidden.Text + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }

            else if ((Session["RoleId"].ToString() == "3"))
            {
                string clusqry = "select clustercode,clusterid from cluster where ZoneId='" + ddZone.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");
                }
            }
            else
            {
                if (ddZone.SelectedItem.Text != "[Select Zone]")
                {
                    if (ddZone.SelectedItem.Text == "All")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("All", "-1"));
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Finals", "1"));
                        ddCluster.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text == "Online Coaching")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddCluster.Enabled = false;
                    }
                    else
                    {
                        DataSet ds;
                        if (ddZone.SelectedItem.Text == "All")
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A'");
                        }
                        else
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A' and ZoneID='" + ddZone.SelectedValue + "'");
                        }
                        ddCluster.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddCluster.DataTextField = "Name";
                            ddCluster.DataValueField = "ClusterID";
                            ddCluster.DataBind();
                            ddCluster.Items.Insert(0, "All");
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                        else
                        {
                            ddCluster.Items.Clear();
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                    }
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.Items.Insert(0, "[Select Cluster]");
                }
            }
            if (ddZone.SelectedItem.Text == "All")
            {
                string clusqry = "select  distinct clustercode,clusterid from cluster where Status='A' ";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");
                }
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Chapter()
    {
        try
        {
            ddchapter.Enabled = true;
            if ((Session["RoleId"].ToString() == "5"))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from volunteer where  MemberID='" + Session["LoginID"] + "' and ChapterId is not null");
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddchapter.Items.Clear();
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;
                }
                else
                {
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode,[State] from chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        Cluster();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "All")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterid,chaptercode,[state] from chapter where clusterId='" + ddCluster.SelectedValue + "' and ChapterId is not null order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.Enabled = true;
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else
            {
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    if (ddCluster.SelectedItem.Text == "All")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, "All");
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Finals, US", "1"));
                        ddchapter.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text == "Online Coaching")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Coaching,US", "112"));
                        ddchapter.Enabled = false;

                    }
                    else
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ChapterID,[state],chaptercode from Chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                        ddchapter.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddchapter.DataTextField = "Name";
                            ddchapter.DataValueField = "ChapterID";
                            ddchapter.DataBind();
                            ddchapter.Items.Insert(0, "All");
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                        else
                        {
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                    }
                }
                else
                {
                    ddchapter.Items.Clear();
                    ddchapter.Items.Insert(0, "[Select Chapter]");
                }
            }
            if (ddCluster.SelectedItem.Text == "All")
            {
                string ClusterFlqry;
                string WhereCluster;
                ClusterFlqry = "select distinct chapterid,chaptercode,[State] from chapter where  ChapterId is not null";
                WhereCluster = ClusterFlqry + Filterdropdown();
                DataSet ds = new DataSet();
                 ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(),CommandType.Text, WhereCluster);
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                    ddchapter.Enabled = true;
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    ddchapter.Items.Insert(0, "All");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string Filterdropdown()
    {
        string iCondtions = string.Empty;
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ClusterId=" + ddCluster.SelectedValue;
            }
        }
        return iCondtions + "  order by [State],ChapterCode";
    }
    protected void ddZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cluster();
        Chapter();
        if (DDFront.SelectedItem.Text == "Allocated Cost")
        {
            ddevent.Items.Clear();
            ddevent.Items.Insert(0, new ListItem("Regionals", "2"));
            ddevent.Enabled = false;
        }
    }
    protected void ddCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        Chapter();
      
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
            {
                if (DDFront.SelectedItem.Text == "Expense category")
                {
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        Griddisp.Visible = true;
                        if (Griddisp.Rows.Count == 0)
                        {
                            lbldisp.Visible = true;
                        }
                    }
                    else
                    {
                        lblall.Visible = true;
                        Griddisp.Visible = false;
                        GridChapter.Visible = false;
                        lbldisp.Visible = false;
                    }
                    if (!IsPostBack)
                    {
                        buttonclick();
                    }
                }
                else if (DDFront.SelectedItem.Text == "Chapter")
                {
                    isChapterRepot = true;
                    buttonclick();
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        if (lbldisp.Visible == true)
                        {
                            GridChapter.Visible = false;
                        }
                        else
                        {
                            GridChapter.Visible = true;
                        }
                    }
                    else
                    {
                        GridChapter.Visible = false;
                    }
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Allocated Cost Report")
                {
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        AllocatedCostReport();
                    }
                    else
                    {
                        lblall.Visible = true;
                        GridView1.Visible = false;
                    }
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Contest Registrations Report")
                {
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        ContestReport();
                    }
                    else
                    {
                        lblall.Visible = true;
                        GridView1.Visible = false;
                    }
                }
            }
            else
            {
                if (DDFront.SelectedItem.Text == "Expense category")
                {
                    buttonclick();
                }
                else if (DDFront.SelectedItem.Text == "Chapter")
                {
                    isChapterRepot = true;
                    buttonclick();
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        Button2.Enabled = true;
                        GridChapter.Visible = true;
                    }
                    else
                    {
                        GridChapter.Visible = false;
                    }
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Allocated Cost Report")
                {
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        AllocatedCostReport();
                    }
                    else
                    {
                        lblall.Visible = true;
                        GridView1.Visible = false;
                    }
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Contest Registrations Report")
                {
                    if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
                    {
                        ContestReport();
                    }
                    else
                    {
                        lblall.Visible = true;
                        GridView1.Visible = false;
                    }
                }
            }
    }
    protected void buttonclick()
    {
        try
        {
            DataSet emptds = new DataSet();
            DataTable dtempty = new DataTable();
            DataTable dtgrants = new DataTable();
            DataRow drng = dtgrants.NewRow();
            if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
            {
                lblall.Visible = false;
                lbldisp.Visible = false;
                if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
                {
                    calc = ddNoyear.SelectedItem.Text;
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
                }
                else
                {
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - 5;
                }
                bool Isfalse = false;
                for (i = endall; i <= Start; i++)
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        if (isChapterRepot== true)
                        {
                            Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.ChapterID,ch.ChapterCode,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                        }
                    }
                    else
                    {
                        fiscyear = i + 1;
                        if (isChapterRepot == true)
                        {
                            Qrycondition = "with tbl as (select TOP 100 PERCENT  ch.ChapterID,ch.ChapterCode,sum(Ej.ExpenseAmount) as ExpenseAmount ,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min (CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select  EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min(CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and  Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";
                        }
                    }
                        Qry = Qrycondition + genWhereConditons();
                    dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                    DataTable dtmerge = new DataTable();
                    dtmerge = dsnew.Tables[0];
                    if (isChapterRepot == true)
                    {
                        firstquert = "with tbl as (select TOP 100 PERCENT ch.[State],ch.ChapterID,ch.ChapterCode,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                    }
                    else
                    {
                        firstquert = "with tbl as (select  EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                    }
                    firstquert = firstquert + genWhereConditonsnew();
                    DataSet dsnew1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, firstquert);
                    DataTable dtm = new DataTable();
                    dtm = dsnew1.Tables[0];
                    if (Isfalse == false)
                    {
                        Mergecopy = dtm.Copy();
                        if (dtm.Rows.Count > 0)
                        {
                            Isfalse = true;
                            Mergecopy = dtm.Copy();
                        }
                    }
                        if (Isfalse == true)
                        {
                            if (dtmerge.Rows.Count > 0)
                            {
                                for (int j = 0; j <= dtmerge.Rows.Count - 1; j++)
                                {
                                    Label lbn = new Label();
                                    lbn.Text = dtmerge.Rows[j][0].ToString();
                                    for (int h = 0; h <= Mergecopy.Rows.Count - 1; h++)
                                    {
                                        Label lb = new Label();
                                        lb.Text = Mergecopy.Rows[h][0].ToString();
                                        if ((dtmerge.Rows[j][0].ToString() == Mergecopy.Rows[h][0].ToString()))
                                        {
                                            string s = dtmerge.Rows[j]["" + i + ""].ToString();
                                            Mergecopy.Rows[h]["" + i + ""] = s;

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                dtnewgrid = Mergecopy;
                if (Mergecopy.Rows.Count == 0)
                {
                    if (DDFront.SelectedItem.Text == "Chapter")
                    {
                         isnorecord=false;
                        Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.ChapterID,ch.ChapterCode,sum(Ej.ExpenseAmount) as ExpenseAmount,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from  chapter ch left outer join ExpJournal Ej on ej.ToChapterID=ch.ChapterID where ch.status='A'";
                        Qry = Qrycondition + FilterAllocated();
                        dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                        Mergecopy = dsnew.Tables[0];
                    }
                    isChapterRepot = true;
                }
                isnorecord = true;
                dtnewgrid = Mergecopy;
                DataRow drGrossexpense = dtnewgrid.NewRow();
                DataRow drallocated1 = dtnewgrid.NewRow();
                DataRow drEmptall = dtnewgrid.NewRow();
                DataRow drmerchval = dtnewgrid.NewRow();
                DataRow lessval = dtnewgrid.NewRow();
                DataRow drgrants1 = dtnewgrid.NewRow();
                DataRow drlessgrants1 = dtnewgrid.NewRow();
                DataRow drmerVr1 = dtnewgrid.NewRow();
                lessval[1]="Total less Grants less Merchandise for Sale";
                lessval[2] = string.Empty;
                drgrants1[1] = string.Empty;
                drgrants1[2] = string.Empty;
                drlessgrants1[1] = string.Empty;
                drlessgrants1[2] = string.Empty;
                drmerVr1[1] = string.Empty;
                drmerVr1[2] = string.Empty;
                drallocated1[1] = "Allocated Cost:  Medals, Trophies, etc.";
                drallocated1[2] = string.Empty;
                drGrossexpense[1] = "Total Gross Expense";
                drGrossexpense[2] = string.Empty;
                drmerchval[1] = "";
                    drmerchval[2]="";
                double AllocatedVal;
                string Qrymerchval;
                string Qrymerch1;
                string Qrymerchcond1;
                string qrygrant1;
                string Merchval1;
                if (Mergecopy.Rows.Count > 0)
                {
                    if (isChapterRepot == true)
                    {
                        DataRow dr = dtnewgrid.NewRow();
                        Label lballocated = new Label();
                        dr[1] = "Total (Sum)";
                        for (int i = 2; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            String colName1 = dtnewgrid.Columns[i].ColumnName;
                            dr[i] = 0;
                            double sum = 0;
                            string suma = "";
                            foreach (DataRow drnew in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drnew[i]))
                                {
                                    suma = drnew[i].ToString();
                                    if (suma != "")
                                    {
                                        sum += Convert.ToDouble(drnew[i]);
                                    }
                                }
                            }
                            dr[i] = sum;
                        
                        }
                            for (int i = 2; i <= dtnewgrid.Columns.Count - 2; i++)
                            {
                                String colName = dtnewgrid.Columns[i].ColumnName;
                                int fisc = Convert.ToInt32(colName) + 1; 
                                if (DDyear.SelectedItem.Text != "Fiscal Year")
                                {
                                    allocatedcostCy1 = "select SUM(AllocatedCostCy) from AllocatedCost Ac inner join chapter ch on Ac.ChapterID=ch.ChapterID where ContestYear=" + colName + "";
                                    Qrymerchval = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                    Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                    Qrymerch1 = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                }
                                else
                                {
                                    Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where   Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + fisc + "'  and ej.CheckNumber>0";
                                    Qrymerch1 = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + fisc + "'   and ej.CheckNumber>0";
                                    Qrymerchval = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + fisc + "'and ej.CheckNumber>0";
                                    allocatedcostCy1 = "select  SUM(AllocatedCostFy) from AllocatedCost Ac inner join chapter ch on Ac.ChapterID=ch.ChapterID where ContestYear=" + colName + "";
                                }
                                Qrymerchcond1 = Qrymerch1 + gengrantWhereConditons();
                                qrygrant1 = Qrygrants + gengrantWhereConditons();
                                Merchval1 = Qrymerchval + gengrantWhereConditons();
                                string AllocatcostQry = allocatedcostCy1 + FilterAllocated();
                                object allocatedcostcy = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, AllocatcostQry);
                                lballocated.Text = Convert.ToString(allocatedcostcy);
                                if (lballocated.Text == "")
                                {
                                    lballocated.Text = "0";
                                }
                                AllocatedVal = Convert.ToDouble(lballocated.Text);
                                if (ddchapter.SelectedValue == "109")
                                {
                                    AllocatedVal = AllocatedVal * -1;
                                }
                                drallocated1[i] = AllocatedVal;
                                object qrymerch = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerchcond1);
                                object Qrygrantcal = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, qrygrant1);
                                object Merchvalue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Merchval1);
                                Label Merchlabl = new Label();
                                Merchlabl.Text = Convert.ToString(Merchvalue);
                                if (Merchlabl.Text == "")
                                {
                                    Merchlabl.Text = "0.00";
                                }
                                valmerch = Convert.ToDouble(Merchlabl.Text);
                                Label Qrygrant = new Label();
                                Qrygrant.Text = Convert.ToString(Qrygrantcal);
                                if (Qrygrant.Text == "")
                                {
                                    Qrygrant.Text = "0.00";
                                }
                                Qrygrantcal = Convert.ToDouble(Qrygrant.Text);
                                drmerchval[i] = valmerch;
                                drgrants1[i] = Qrygrantcal;
                                drmerVr1[i] = grantmerch;
                                if (!DBNull.Value.Equals(dr[i]))
                                {
                                    double valTot = Convert.ToDouble(dr[i].ToString());
                                    double grants = Convert.ToDouble(drgrants1[i].ToString());
                                    double merchval = Convert.ToDouble(drmerchval[i].ToString());
                                    double Allocatedcostcal = Convert.ToDouble(drallocated1[i].ToString());
                                    less = valTot - grants;
                                    merchless = less - merchval;
                                    grossexpense = merchless + Allocatedcostcal;
                                }
                                drGrossexpense[i] = grossexpense;
                                lessval[i] = merchless;
                            }
                             GRcategory.Visible = false;
                             dtnewgrid.Rows.Add(dr);
                             dtnewgrid.Rows.Add(lessval);
                             dtnewgrid.Rows.Add(drallocated1);
                             dtnewgrid.Rows.Add(drGrossexpense);
                             for (int i = 2; i <= dtnewgrid.Columns.Count - 2; i++)
                             {
                                 foreach (DataRow drnw in dtnewgrid.Rows)
                                 {
                                     if (!DBNull.Value.Equals(drnw[i]))
                                     {
                                         //double st;
                                         double st1;
                                         // st= Convert.ToDouble(drnw["total"]);
                                         st1 = Convert.ToDouble(drnw[i]);
                                         // drnw["total"] = String.Format("{0:#,###0}", st);
                                         drnw[i] = Convert.ToString(drnw[i]);
                                         drnw[i] = String.Format("{0:#,###0}", st1);
                                         // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                                     }
                                 }
                             }
                             for (int j = 0; j <= dtnewgrid.Rows.Count - 1; j++)
                             {

                                 dtnewgrid.Rows[j].BeginEdit();
                                 double total = 0;
                                 string totvalue = "";
                                 for (int k = 2; k < dtnewgrid.Columns.Count - 1; k++)
                                 {
                                     //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                                     totvalue = dtnewgrid.Rows[j][k].ToString();
                                     if (totvalue != "")
                                     {
                                         total += Convert.ToDouble(dtnewgrid.Rows[j][k].ToString());
                                     }
                                 }
                                 dtnewgrid.Rows[j]["Total"] = total;

                                 dtnewgrid.Rows[j].EndEdit();
                             }
                             for (int i = dtnewgrid.Columns.Count - 1; i <= dtnewgrid.Columns.Count - 1; i++)
                             {
                                 foreach (DataRow drn in dtnewgrid.Rows)
                                 {
                                     if (!DBNull.Value.Equals(drn))
                                     {
                                         string st;
                                         double st1;
                                         st1 = Convert.ToDouble(drn["Total"]);
                                         st = String.Format("{0:#,###0}", st1);

                                         //dr[i] = String.Format("{0:#,###0}", st1);
                                         // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                                         drn["Total"] = st.ToString();

                                     }
                                 }
                             }

                             dtnewgrid.AcceptChanges();
                             Session["Gridchapter"]=dtnewgrid;

                             GridChapter.DataSource = dtnewgrid;
                             GridChapter.DataBind();
                    }
                    else
                    {
                        dtnewgrid = Mergecopy;
                        DataRow dr = dtnewgrid.NewRow();
                        DataRow drtotal = dtnewgrid.NewRow();
                        DataRow drblank = dtnewgrid.NewRow();
                        DataRow drgrants = dtnewgrid.NewRow();
                        DataRow drlessgrants = dtnewgrid.NewRow();
                        DataRow drmerVr = dtnewgrid.NewRow();
                        DataRow drGrossexpensecat = dtnewgrid.NewRow();
                        DataRow drallocatedcat = dtnewgrid.NewRow();
                        drallocatedcat[1] = "Allocated Cost:  Medals, Trophies, etc.";
                        drallocatedcat[2] = string.Empty;
                        drGrossexpensecat[1] = "Total Gross Expense";
                        drGrossexpensecat[2] = string.Empty;
                        drmerVr[1] = "Total less Grants less Merchandise for Sale";
                        drmerVr[2] = string.Empty;
                        dr[1] = "Total (Sum)";
                        dr[2] = string.Empty;
                        drgrants[1] = "Grants (Scholarships and Prorgam Related Funding)";
                        drgrants[2] = "";
                        drlessgrants[1] = "Total less Grants";
                        drlessgrants[2] = "";
                        drtotal[1] = "Total (Calculated)";
                        drtotal[2] = "";
                        if (DDcategory.Visible == true)
                        {
                            newvar = 3;
                        }
                        else
                        {
                            newvar = 3;
                        }
                        for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            dr[i] = 0;
                            double sum = 0;
                            string suma = "";
                            foreach (DataRow drnew in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drnew[i]))
                                {
                                    suma = drnew[i].ToString();
                                    if (suma != "")
                                    {
                                        sum += Convert.ToDouble(drnew[i]);
                                    }
                                }
                            }
                            dr[i] = sum;
                        }
                        if (DDcategory.Visible == true)
                        {
                            dtnewgrid.Rows.Add(dr);
                        }
                        if (DDcategory.Visible != true)
                        {
                            for (int i = 3; i <= dtnewgrid.Columns.Count - 2; i++)
                            {
                                string qrygrant;
                                string totalcalc;
                                string totQry;
                                string Qrymerch;
                                string Qrymerchcond;
                                string Allocafil;
                                Label lballocated = new Label();
                                double allocat;
                                String colName = dtnewgrid.Columns[i].ColumnName;
                                if (DDyear.SelectedItem.Text != "Fiscal Year")
                                {
                                    Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                    Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                    totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "' and  CheckNumber>0   and Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                                    allocatedcostCy1 = "select SUM(AllocatedCostCy) from AllocatedCost Ac inner join chapter ch on Ac.ChapterID=ch.ChapterID where  ContestYear=" + colName + "";
                                }
                                else
                                {
                                    int colfisc = Convert.ToInt32(colName) + 1;
                                    allocatedcostCy1 = "select SUM(AllocatedCostCy) from AllocatedCost where  ContestYear=" + colName + "";
                                    Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where   Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'  and ej.CheckNumber>0";
                                    Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'   and ej.CheckNumber>0";
                                    totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "' and  CheckNumber>0   and  Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                                    allocatedcostCy1 = "select SUM(AllocatedCostFy) from AllocatedCost Ac inner join chapter ch on Ac.ChapterID=ch.ChapterID where  ContestYear=" + colName + "";
                                }
                                Qrymerchcond = Qrymerch + gengrantWhereConditons();
                                qrygrant = Qrygrants + gengrantWhereConditons();
                                totQry = totalcalc + gengrantWhereConditons();
                                Allocafil = allocatedcostCy1 + FilterAllocated();
                                object Allocatedval = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Allocafil);
                                DataSet dsnewmerge = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerchcond);
                                DataTable dtmerch = dsnewmerge.Tables[0];
                                dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, qrygrant);
                                DataTable dtgrantcal = dsnew.Tables[0];
                                DataSet dsnewtot = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, totQry);
                                DataTable dtnewtot = dsnewtot.Tables[0];
                                Label lblgrant = new Label();
                                Label lbltotcalc = new Label();
                                Label lblmerch = new Label();
                                Label allocated = new Label();
                                allocated.Text = Convert.ToString(Allocatedval);
                                if (allocated.Text == "")
                                {
                                    allocated.Text = "0.00";
                                }
                                allocat = Convert.ToDouble(allocated.Text);
                                if (ddchapter.SelectedValue == "109")
                                {
                                    allocat = allocat * -1;
                                }
                                lbltotcalc.Text = dtnewtot.Rows[0]["expense"].ToString();
                                lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                                lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                                if (lblgrant.Text == "")
                                {
                                    lblgrant.Text = "0";
                                }
                                else
                                {
                                    lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                                }
                                if (!DBNull.Value.Equals(lblgrant.Text))
                                {
                                    grantcal = Convert.ToDouble(lblgrant.Text);
                                }
                                if (lblmerch.Text == "")
                                {
                                    lblmerch.Text = "0";
                                }
                                else
                                {
                                    lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                                }
                                if (!DBNull.Value.Equals(lblmerch.Text))
                                {
                                    grantmerch = Convert.ToDouble(lblmerch.Text);
                                }
                                drgrants[i] = grantcal;
                                drmerVr[i] = grantmerch;
                                drtotal[i] = lbltotcalc.Text;
                                drallocatedcat[i] = allocat;
                            }
                            for (int i = 3; i <= dtnewgrid.Columns.Count - 2; i++)
                            {
                                if (!DBNull.Value.Equals(dr[i]))
                                {
                                    double valTot = Convert.ToDouble(dr[i].ToString());
                                    double grants = Convert.ToDouble(drgrants[i].ToString());
                                    double merchval = Convert.ToDouble(drmerVr[i].ToString());
                                    double Allocatedcostcal = Convert.ToDouble(drallocatedcat[i].ToString());
                                    less = valTot - grants;
                                    merchless = less - merchval;
                                    grossexpense = merchless + Allocatedcostcal;
                                }
                                drlessgrants[i] = less;
                                drmerVr[i] = merchless;
                                drGrossexpensecat[i] = grossexpense;
                            }
                            dtnewgrid.Rows.Add(dr);
                            dtnewgrid.Rows.Add(drtotal);
                            dtnewgrid.Rows.Add(drblank);
                            dtnewgrid.Rows.Add(drgrants);
                            dtnewgrid.Rows.Add(drlessgrants);
                            dtnewgrid.Rows.Add(drmerVr);
                            dtnewgrid.Rows.Add(drallocatedcat);
                            dtnewgrid.Rows.Add(drGrossexpensecat);
                        }
                        Button2.Enabled = true;
                        GridView1.Visible = false;
                        Griddrop.Visible = false;
                        DataTable dt = new DataTable();
                        dt = dsnew.Tables[0];
                        DataTable dt1 = new DataTable();
                        dt1.Columns.Add("Select Event", typeof(string));
                        if (DDcategory.Visible != true)
                        {
                            dt1.Columns.Add("Select Zone", typeof(string));
                            dt1.Columns.Add("Select Cluster", typeof(string));
                            dt1.Columns.Add("Select Chapter", typeof(string));
                        }
                        dt1.Columns.Add("Select No Of years", typeof(string));
                        dt1.Columns.Add("Select Year", typeof(string));
                        DataRow drow = dt1.NewRow();
                        DataRow drow1 = dt1.NewRow();
                        drow["Select Event"] = ddevent.SelectedItem.Text;
                        if (DDcategory.Visible != true)
                        {
                            drow["Select Zone"] = ddZone.SelectedItem.Text;
                            drow["Select Cluster"] = ddCluster.SelectedItem.Text;
                            drow["Select Chapter"] = ddchapter.SelectedItem.Text;
                        }
                        drow["Select No Of years"] = ddNoyear.SelectedItem.Text;
                        drow["Select Year"] = DDyear.SelectedItem.Text;
                        drow1["Select Event"] = string.Empty;
                        if (DDcategory.Visible != true)
                        {
                            drow1["Select Zone"] = string.Empty;
                            drow1["Select Cluster"] = string.Empty;
                            drow1["Select Chapter"] = string.Empty;
                        }
                        drow1["Select No Of years"] = string.Empty;
                        drow1["Select Year"] = string.Empty;
                        dt1.Rows.InsertAt(drow1, 1);
                        dt1.Rows.InsertAt(drow, 0);
                        Griddisp.Visible = false;
                        int a = Griddisp.Columns.Count;
                        int b = dtnewgrid.Columns.Count;
                        int count = Griddisp.Columns.Count;
                        for (int c = 0; c < dtnewgrid.Columns.Count; c++)
                        {
                            DataColumn col = new DataColumn();
                            BoundField boundField = new BoundField();
                            boundField.DataField = dtnewgrid.Columns[c].ColumnName.ToString();
                            
                            boundField.HeaderText = dtnewgrid.Columns[c].ColumnName.ToString();
                            if (c == 0)
                            {
                            }
                            Griddisp.Columns.Add(boundField);
                        }

                        for (int i = newvar; i <= dtnewgrid.Columns.Count - 2; i++)
                        {
                            foreach (DataRow drnw in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drnw[i]))
                                {
                                    //double st;
                                    double st1;
                                    // st= Convert.ToDouble(drnw["total"]);
                                    st1 = Convert.ToDouble(drnw[i]);
                                    // drnw["total"] = String.Format("{0:#,###0}", st);
                                    drnw[i] = Convert.ToString(drnw[i]);
                                    drnw[i] = String.Format("{0:#,###0}", st1);
                                    // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                                }
                            }
                        }
                       // dtnewgrid.Columns.Add("Total", typeof(String));
                        for (int j = 0; j <= dtnewgrid.Rows.Count - 1; j++)
                        {

                            dtnewgrid.Rows[j].BeginEdit();
                            double total = 0;
                            string totvalue = "";
                            for (int k = newvar; k < dtnewgrid.Columns.Count - 1; k++)
                            {
                                //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                                totvalue = dtnewgrid.Rows[j][k].ToString();
                                if (totvalue != "")
                                {
                                    total += Convert.ToDouble(dtnewgrid.Rows[j][k].ToString());
                                }
                            }
                            dtnewgrid.Rows[j]["Total"] = total;

                            dtnewgrid.Rows[j].EndEdit();
                        }
                        for (int i = dtnewgrid.Columns.Count - 1; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            foreach (DataRow drn in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drn))
                                {
                                    string st;
                                    double st1;
                                    st1 = Convert.ToDouble(drn["Total"]);
                                    st = String.Format("{0:#,###0}",st1);

                                    //dr[i] = String.Format("{0:#,###0}", st1);
                                    // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                                    drn["Total"] = st.ToString();

                                }
                            }
                        }

                        dtnewgrid.AcceptChanges();
                    

                        Session["Griddisplay"] = dtnewgrid;
                        Griddisp.DataSource = dtnewgrid;
                        Griddisp.DataBind();
                        EnableViewState = true;
                        Griddisp.Visible = true;
                        pnlview.Visible = true;
                        lbldisp.Visible = false;
                        Session["Sessionno"] = dsnew.Tables[0];
                        dtnw = dsnew.Tables[0];
                    }
                }
                else
                {
                    lbldisp.Visible = true;
                    Griddisp.Visible = false;
                    GridChapter.Visible = false;
                }
            }
            else
            {
                lblall.Visible = true;
                lbldisp.Visible = false;
                Griddisp.Visible = false;
                GridChapter.Visible = false;
            }
           
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string FilterAllocated()
    {
        string iCondtions = string.Empty;
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                if (isnorecord == false)
                {
                    iCondtions += " and ch.ChapterID=" + ddchapter.SelectedValue;
                }
                else
                {
                    if (ddchapter.SelectedValue != "109")
                    {
                        iCondtions += " and Ac.ChapterID=" + ddchapter.SelectedValue;
                    }
                }
            }
        }

        if (isnorecord == false)
        {
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
            }
            else
            {
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - 5;
            }
            int sta = Convert.ToInt32(Start);
            string WCNT = string.Empty;
            string am = string.Empty;
            for (int i = endall; i <= sta; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),isnull(PVTTBL.[" + i.ToString() + "],0)) AS [" + i.ToString() + "]";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    am = ",CONVERT(varchar(50),isnull(PVTTBL.[" + i.ToString() + "],0)) AS [" + i.ToString() + "]";
                }
            }
            return iCondtions + "group by ch.ChapterCode,ch.ChapterID,year(Ej.DatePaid)) select ChapterID,ChapterCode " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
        }
        else
        {
            return iCondtions;
        }
    }
    protected string gengrantlinkWhereconditions()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }
        }
            if (ddZone.SelectedItem.Text != "[Select Zone]")
            {
                if (ddZone.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
            }
            if (ddCluster.SelectedItem.Text != "[Select Cluster]")
            {
                if (ddCluster.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
            }
        return iCondtions + " and ej.paid='y'";
    }
    protected string genwherecategorycondition()
    {
        string iCondtions = string.Empty;
            if (ddevent.SelectedItem.Text != "[Select Event]")
            {
                if (ddevent.SelectedItem.Text != "All")
                {
                    iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
                }
            }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        return iCondtions + "group by ej.ExpcatId,ch.[state],ej.Tochapterid,ch.ChapterCode,ch.ClusterCode,ch.ZoneCode,year(Ej.DatePaid) order by ch.[state],ej.Tochapterid) select catid,chapterid,Chapter,Cluster,Zone " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
    }
    protected string gengrantWhereConditons()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }
        }
        if (DDcategory.Visible != true)
        {
            if (ddZone.SelectedItem.Text != "[Select Zone]")
            {
                if (ddZone.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
            }
            if (ddCluster.SelectedItem.Text != "[Select Cluster]")
            {
                if (ddCluster.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
            }
            if (ddchapter.SelectedItem.Text != "[Select Chapter]")
            {
                if (ddchapter.SelectedItem.Text != "All")
                {
                    iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
                }
            }
        }
        else
        {
            if (ddZone.SelectedItem.Text != "[Select Category]")
            {
                iCondtions += " and Ej.ExpCatID=" + DDcategory.SelectedValue;
            }
        }
        return iCondtions + " and ej.paid='y'";
    }
    protected string genWhereConditons()
    {
        string iCondtions = string.Empty;
            if (ddevent.SelectedItem.Text != "[Select Event]")
            {
                if (ddevent.SelectedItem.Text != "All")
                {
                    iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
                }
            }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        if (isChapterRepot == true)
        {
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                return iCondtions + "group by ch.ChapterID,ch.ChapterCode,year(Ej.DatePaid)) select ChapterID,ChapterCode " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
            }
            else
            {
                return iCondtions + "group by ch.ChapterID,ch.ChapterCode) select ChapterID,ChapterCode " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
            }
        }
        else 
        {
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                return iCondtions + "group by EC.ExpCatID,EC.ExpCatDesc,EC.ExpCatDesc,EC.AccountName,year(Ej.DatePaid)) select ExpCatID,ExpCatDesc,AccountName " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
            }
            else
            {
                return iCondtions + "group by EC.ExpCatID,EC.ExpCatDesc,EC.AccountName) select ExpCatID,ExpCatDesc,AccountName " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
            }
        }
    }
    protected string genWhereConditonforchapter()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";

            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        return iCondtions + "group by Ch.ChapterID,EC.ExpcatId,EC.ExpCatDesc,EC.AccountName,year(Ej.DatePaid)) select ChapterID,ExpCatID,ExpCatDesc,AccountName " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
    }
    protected string genWhereConditonsnew()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        if (isChapterRepot == true)
        {
            return iCondtions + "group by ch.[State],ch.ChapterID,ch.ChapterCode,year(Ej.DatePaid)) select ChapterID,ChapterCode " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
        }
        else
        {
            return iCondtions + "group by EC.ExpcatId,EC.ExpCatDesc,EC.AccountName,year(Ej.DatePaid)) select ExpCatID,ExpCatDesc,AccountName " + am + ",'' as Total from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        try
        {
            Response.Clear();
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
            {
                if ((divcat.Visible == true) && (lbprevious.Visible == false))
                {
                    divchapex.Visible = false;
                    GridExcel.DataSource = dtnewgrid;
                    GridExcel.DataBind();
                    divcat.Visible = true;
                    GridEdit.Visible = false;
                }
                else if ((divcat.Visible == true) && (lbprevious.Visible == true) && (GridEdit.Visible == false))
                {
                    divchapex.Visible = false;
                    DataTable detailedExcelcal = (DataTable)Session["Detaildatatable"];
                    GridExcel.DataSource = detailedExcelcal;
                    GridExcel.DataBind();
                    divcat.Visible = true;
                    GridEdit.Visible = false;
                }
                else if ((divchapex.Visible == true) && (lbchapterpre.Visible == false))
                {
                    divcat.Visible = false;
                    divchapex.Visible = true;
                    DataTable chapexcel = (DataTable)Session["Gridchapter"];
                    GridExcel.DataSource = chapexcel;
                    GridExcel.DataBind();
                    GridEdit.Visible = false;
                }
                else if ((divchapex.Visible == true) && (lbchapterpre.Visible == true) && (GridEdit.Visible == false))
                {
                    divchapex.Visible = true;
                    divcat.Visible = false;
                    DataTable chapexcel = (DataTable)Session["Expcat"];
                    GridExcel.DataSource = chapexcel;
                    GridExcel.DataBind();
                    GridEdit.Visible = false;
                }
                else if ((DDFront.SelectedItem.Text == "Allocated Cost") && (divcontestrep.Visible == true))
                {
                    divcontestrep.Visible = true;
                    divcat.Visible = false;
                    divchapex.Visible = false;
                    DataTable dtallocatedEx = (DataTable)Session["ContestantReport"];
                    GridExcel.DataSource = dtallocatedEx;
                    GridExcel.DataBind();
                    GridEdit.Visible = false;
                }
                else if ((DDFront.SelectedItem.Text == "Allocated Cost") && (divallrep.Visible == true))
                {
                    divcat.Visible = false;
                    divchapex.Visible = false;
                    divallrep.Visible = true;
                    DataTable dtallocatedEx1 = (DataTable)Session["AllocatedCost"];
                    GridExcel.DataSource = dtallocatedEx1;
                    GridExcel.DataBind();
                    GridEdit.Visible = false;
                }
                else
                {
                    if (DDFront.SelectedItem.Text == "Chapter")
                    {
                        divcat.Visible = false;
                    }
                    else
                    {
                        divchapex.Visible = false;
                    }
                    GridEdit.Visible = true;
                }
            }
            else
            {
                DataTable dtother;
                if ((DDFront.SelectedItem.Text == "Allocated Cost") && (divallrep.Visible == true))
                {
                    divallrep.Visible = true;
                    divchapex.Visible = false;
                    divcat.Visible = false;
                    dtother = (DataTable)Session["AllocatedCost"];
                }
                else if ((DDFront.SelectedItem.Text == "Allocated Cost") && (divcontestrep.Visible == true))
                {
                    divcontestrep.Visible = true;
                    divchapex.Visible = false;
                    divcat.Visible = false;
                    dtother = (DataTable)Session["ContestantReport"];
                }
                else 
                {
                    if (DDFront.SelectedItem.Text == "Chapter")
                    {
                        divcat.Visible = false;
                        dtother = (DataTable)Session["Gridchapter"];
                    }
                    else
                    {
                        divchapex.Visible = false;
                        dtother = (DataTable)Session["Griddisplay"];
                    }
                }
                GridExcel.DataSource = dtother;
                GridExcel.DataBind();
            }
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Select Event", typeof(string));
            if (DDcategory.Visible != true)
            {
                dt1.Columns.Add("Select Zone", typeof(string));
                dt1.Columns.Add("Select Cluster", typeof(string));
                dt1.Columns.Add("Select Chapter", typeof(string));
            }
            dt1.Columns.Add("Select No Of years", typeof(string));
            dt1.Columns.Add("Select Year", typeof(string));
            DataRow drow = dt1.NewRow();
            DataRow drow1 = dt1.NewRow();
            drow["Select Event"] = ddevent.SelectedItem.Text;
            drow["Select Zone"] = ddZone.SelectedItem.Text;
            drow["Select Cluster"] = ddCluster.SelectedItem.Text;
            drow["Select Chapter"] = ddchapter.SelectedItem.Text;
            drow["Select No Of years"] = ddNoyear.SelectedItem.Text;
            drow["Select Year"] = DDyear.SelectedItem.Text;
            drow1["Select Event"] = string.Empty;
            drow1["Select No Of years"] = string.Empty;
            drow1["Select Year"] = string.Empty;
            dt1.Rows.InsertAt(drow1, 1);
            dt1.Rows.InsertAt(drow, 0);
            Griddrop.DataSource = dt1;
            Griddrop.DataBind();
            Griddrop.Visible = true;
            String strZoneName = ddZone.SelectedItem.Text;
            strZoneName = strZoneName.Replace(" ", "");
            String strclusterName = ddCluster.SelectedItem.Text;
            strclusterName = strclusterName.Replace(" ", "");
            String strchapterName = ddchapter.SelectedItem.Text;
            strchapterName = strchapterName.Replace(" ", "");
            String strcategoryName = DDcategory.SelectedItem.Text;
            strcategoryName = strcategoryName.Replace(" ", "");
            String strEventName = ddevent.SelectedItem.Text;
            strEventName = strEventName.Replace(" ", "");
            String strYearName = DDyear.SelectedItem.Text;
            strYearName = strYearName.Replace(" ", "");
            DateTime now = DateTime.Now;
            string month = now.ToString("MMM");
            string day = now.ToString("dd");
            string all = string.Concat(month, day);
            string attachment;
            string yearshort;
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                yearshort = "CY";
            }
            else
            {
                yearshort = "FY";
            }
            if (DDcategory.Visible != true)
            {
                if ((Lbchapter.Visible == true) && (LbCategory.Visible == true))
                {
                    String strchapter = Lbchapter.Text;
                    strchapter = strchapter.Replace(" ", "");
                    attachment = "attachment; filename=ExpSum_" + strEventName + "_" + strZoneName + "_" + strclusterName + "_" + strchapter + "_" + strYearName + "_" + all + "_" + StaYear + ".xls";
                }
                else
                {
                    attachment = "attachment; filename=ExpSum_" + strEventName + "_" + strZoneName + "_" + strclusterName + "_" + strchapterName + "_" + ddNoyear.SelectedItem.Text + "_" + strYearName + "_" + all + "_" + StaYear + ".xls";
                }
            }
            else
            {
                attachment = "attachment; filename=ExpSum_" + strEventName + "_" + strcategoryName + "_" + ddNoyear.SelectedItem.Text + "_" + strYearName + "_" + all + "_" + StaYear + ".xls";
            }
            Response.AppendHeader("content-disposition", attachment);
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
            GridView1.Visible = true;
            divcat.RenderControl(hw);
            divcontestrep.RenderControl(hw);
            divchapex.RenderControl(hw);
            divallrep.RenderControl(hw);
            Panel2.Visible = true;
            Panel2.RenderControl(hw);
            Griddrop.RenderControl(hw);
            GridExcel.RenderControl(hw);
            Griddisp.RenderControl(hw);
            Griddisp.Visible = true;
            GridEdit.RenderControl(hw);
            Response.Write(sw.ToString());
            Griddrop.Visible = true;
            Response.End();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
            Divpan.Visible = true;
            UploadDataTableToExcel(dtnewgrid);
    }
    protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    {
        Zone();
        Cluster();
        Chapter();
    }
    protected void Btnsumm_Click(object sender, EventArgs e)
    {
        GridChapter.Visible = false;
        Griddisp.Visible = false;
        GRcategory.Visible = false;
        GridEdit.Visible = false;
        GridExpcat.Visible = false;
        PnlChapterdisp.Visible = true;
        Pnlsummary.Visible = false;
        LbCategory.Visible = false;
        Lbchapter.Visible = false;
        GridChapter.Visible = false;
        GridChapter.Visible = false;
        Griddisp.Visible = true;
        GRcategory.Visible = false;
        GridEdit.Visible = false;
        isChapterRepot = false;
        Griddrop.Visible = false;
    }
    protected void lnkViewchapter_Click(object sender, EventArgs e)
    {
        try
        {
            GridEdit.Visible = false;
            Divpan.Visible = true;
            lbchapterpre.Visible = true;
            lbprevious.Visible = false;
            GridChapter.Visible = false;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;
            pnlcategory.Visible = false;
            GRcategory.Visible = false;
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
            }
            else
            {
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - 5;
            }
            pnlview.Visible = false;
            GridExpcat.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField kcat = (HiddenField)GridChapter.Rows[index].Cells[0].FindControl("hdcatid");
            HiddenField catdesc = (HiddenField)GridChapter.Rows[index].Cells[0].FindControl("Hncatedesc");
            LbCategory.Text = catdesc.Value;
            LbCategory.Visible = true;
            Expcategorystr = Convert.ToInt32(kcat.Value);
            bool Isfalse = false;
            for (i = endall; i <= Start; i++)
            {
                if (DDyear.SelectedItem.Text != "Fiscal Year")
                {
                    Qrycondition = "with tbl as (select Ch.ChapterID,EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ToChapterId=" + Expcategorystr + " and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                }
                else
                {
                    fiscyear = i + 1;
                    Qrycondition = "with tbl as (select Ch.ChapterID,EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount ,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min (CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ToChapterId=" + Expcategorystr + " and Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";
                }
                Qry = Qrycondition + genWhereConditonforchapter();
                dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                DataTable dtmerge = new DataTable();
                dtmerge = dsnew.Tables[0];
                if (Isfalse == false)
                {
                    firstquert = "with tbl as (select  Ch.ChapterID,EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ToChapterId=" + Expcategorystr + " and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                    firstquert = firstquert + genWhereConditonforchapter();
                    DataSet dsnew1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, firstquert);
                    DataTable dtm = new DataTable();
                    dtm = dsnew1.Tables[0];
                    Mergecopy = dtm.Copy();
                    if (dtm.Rows.Count > 0)
                    {
                        Isfalse = true;
                        Mergecopy = dtm.Copy();

                    }
                }
                if (Isfalse == true)
                {
                    if (dtmerge.Rows.Count > 0)
                    {
                        for (int j = 0; j <= dtmerge.Rows.Count - 1; j++)
                        {
                            Label lbn = new Label();
                            lbn.Text = dtmerge.Rows[j][2].ToString();
                            for (int h = 0; h <= Mergecopy.Rows.Count - 1; h++)
                            {
                                Label lb = new Label();
                                lb.Text = Mergecopy.Rows[h][2].ToString();
                                if ((dtmerge.Rows[j][2].ToString() == Mergecopy.Rows[h][2].ToString()))
                                {
                                    string s = dtmerge.Rows[j]["" + i + ""].ToString();
                                    Mergecopy.Rows[h]["" + i + ""] = s;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            dtnewgrid = Mergecopy;
            if (dtnewgrid.Rows.Count > 0)
            {
                DataRow dr = dtnewgrid.NewRow();
                dr[2] = "Total (Sum)";
                DataRow drtotal = dtnewgrid.NewRow();
                DataRow drblank = dtnewgrid.NewRow();
                DataRow drgrants = dtnewgrid.NewRow();
                DataRow drlessgrants = dtnewgrid.NewRow();
                DataRow drmerVr = dtnewgrid.NewRow();
                DataRow drallocated = dtnewgrid.NewRow();
                DataRow drGrossexpense = dtnewgrid.NewRow();
                DataRow drEmptall = dtnewgrid.NewRow();
                drmerVr[2] = "Total less Grants less Merchandise for Sale";
                drmerVr[3] = string.Empty;
                drallocated[2] = "Allocated Cost:  Medals, Trophies, etc.";
                drallocated[3] = string.Empty;
                drGrossexpense[2] = "Total Gross Expense";
                drgrants[2] = "Grants (Scholarships and Prorgam Related Funding)";
                drGrossexpense[3] = string.Empty;
                drgrants[3] = string.Empty;
                drlessgrants[2] = "Total less Grants";
                drlessgrants[3] = string.Empty;
                drtotal[2] = "Total (Calculated)";
                drtotal[3] = "";
                for (int i = 4; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    dr[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {
                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }
                        }
                    }
                    dr[i] = sum;
                }
                for (int i = 4; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    string qrygrant;
                    string totalcalc;
                    string totQry;
                    string Qrymerch;
                    string Qrymerchcond;
                    string allocatedcostCy;
                    String colName = dtnewgrid.Columns[i].ColumnName;
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "' and Ej.ToChapterId=" + Expcategorystr + " and ej.CheckNumber>0";
                        Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and Ej.ToChapterId=" + Expcategorystr + " and ej.CheckNumber>0";
                        totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "' and  CheckNumber>0 and Ej.ToChapterId=" + Expcategorystr + "  and Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                        if (Expcategorystr == 109)
                        {
                            allocatedcostCy = "select SUM(AllocatedCostCy) from AllocatedCost where  ContestYear=" + colName + "";
                        }
                        else
                        {
                            allocatedcostCy = "select ISNULL (AllocatedCostCY,0) from AllocatedCost where ChapterID=" + Expcategorystr + " and ContestYear=" + colName + "";
                        }
                    }
                    else
                    {
                        int colfisc = Convert.ToInt32(colName) + 1;
                        Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where   Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "' and Ej.ToChapterId=" + Expcategorystr + "  and ej.CheckNumber>0";
                        Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'   and Ej.ToChapterId=" + Expcategorystr + " and ej.CheckNumber>0";
                        totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "' and  CheckNumber>0 and Ej.ToChapterId=" + Expcategorystr + "  and  Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                        if (Expcategorystr == 109)
                        {
                            allocatedcostCy = "select SUM(AllocatedCostFy) from AllocatedCost where  ContestYear=" + colName + "";
                        }
                        else
                        {
                            allocatedcostCy = "select ISNULL (AllocatedCostFY,0) from AllocatedCost where ChapterID=" + Expcategorystr + " and ContestYear=" + colName + "";
                        }
                    }
                    Qrymerchcond = Qrymerch + gengrantlinkWhereconditions();
                    qrygrant = Qrygrants + gengrantlinkWhereconditions();
                    totQry = totalcalc + gengrantlinkWhereconditions();
                    object allocatedcostcy = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, allocatedcostCy);
                    DataSet dsnewmerge = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerchcond);
                    DataTable dtmerch = dsnewmerge.Tables[0];
                    dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, qrygrant);
                    DataTable dtgrantcal = dsnew.Tables[0];
                    DataSet dsnewtot = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, totQry);
                    DataTable dtnewtot = dsnewtot.Tables[0];
                    Label lblgrant = new Label();
                    Label lbltotcalc = new Label();
                    Label lblmerch = new Label();
                    Label lballocated = new Label();
                    lbltotcalc.Text = dtnewtot.Rows[0]["expense"].ToString();
                    lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                    lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                    lballocated.Text = Convert.ToString(allocatedcostcy);
                    if (lballocated.Text == "")
                    {
                        lballocated.Text = "0";
                    }
                    double AllocatedVal = Convert.ToDouble(lballocated.Text);
                    if (Expcategorystr == 109)
                    {
                        AllocatedVal = AllocatedVal * -1;
                    }
                    if (lblgrant.Text == "")
                    {
                        lblgrant.Text = "0";
                    }
                    else
                    {
                        lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                    }
                    if (!DBNull.Value.Equals(lblgrant.Text))
                    {
                        grantcal = Convert.ToDouble(lblgrant.Text);
                    }
                    if (lblmerch.Text == "")
                    {
                        lblmerch.Text = "0";
                    }
                    else
                    {
                        lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                    }
                    if (!DBNull.Value.Equals(lblmerch.Text))
                    {
                        grantmerch = Convert.ToDouble(lblmerch.Text);
                    }
                    drgrants[i] = grantcal;
                    drmerVr[i] = grantmerch;
                    drtotal[i] = lbltotcalc.Text;
                    drallocated[i] = AllocatedVal;
                }
                for (int i = 4; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    if (!DBNull.Value.Equals(dr[i]))
                    {
                        double valTot = Convert.ToDouble(dr[i].ToString());
                        double grants = Convert.ToDouble(drgrants[i].ToString());
                        double merchval = Convert.ToDouble(drmerVr[i].ToString());
                        double Allocatedcostcal = Convert.ToDouble(drallocated[i].ToString());
                        less = valTot - grants;
                        merchless = less - merchval;
                        grossexpense = merchless + Allocatedcostcal;
                    }
                    drlessgrants[i] = less;
                    drmerVr[i] = merchless;
                    drGrossexpense[i] = grossexpense;
                }
                dtnewgrid.Rows.Add(dr);
                dtnewgrid.Rows.Add(drtotal);
                dtnewgrid.Rows.Add(drblank);
                dtnewgrid.Rows.Add(drgrants);
                dtnewgrid.Rows.Add(drlessgrants);
                dtnewgrid.Rows.Add(drmerVr);
                dtnewgrid.Rows.Add(drEmptall);
                dtnewgrid.Rows.Add(drallocated);
                dtnewgrid.Rows.Add(drGrossexpense);
                for (int i = 4; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    foreach (DataRow drnw in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnw[i]))
                        {
                            //double st;
                            double st1;
                            // st= Convert.ToDouble(drnw["total"]);
                            st1 = Convert.ToDouble(drnw[i]);
                            // drnw["total"] = String.Format("{0:#,###0}", st);
                            drnw[i] = Convert.ToString(drnw[i]);
                            drnw[i] = String.Format("{0:#,###0}", st1);
                            // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                        }
                    }
                }
                for (int j = 0; j <= dtnewgrid.Rows.Count - 1; j++)
                {

                    dtnewgrid.Rows[j].BeginEdit();
                    double total = 0;
                    string totvalue = "";
                    for (int h = 4; h < dtnewgrid.Columns.Count - 1; h++)
                    {
                        //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                        totvalue = dtnewgrid.Rows[j][h].ToString();
                        if (totvalue != "")
                        {
                            total += Convert.ToDouble(dtnewgrid.Rows[j][h].ToString());
                        }
                    }
                    dtnewgrid.Rows[j]["Total"] = total;

                    dtnewgrid.Rows[j].EndEdit();
                }

                dtnewgrid.AcceptChanges();
                for (int i = dtnewgrid.Columns.Count - 1; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    foreach (DataRow drn in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drn))
                        {
                            string st;
                            double st1;
                            st1 = Convert.ToDouble(drn["Total"]);
                            st = String.Format("{0:#,###0}", st1);

                            //dr[i] = String.Format("{0:#,###0}", st1);
                            // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                            drn["Total"] = st.ToString();

                        }
                    }
                }

                dtnewgrid.AcceptChanges();
                
                dtexcelcate = dtnewgrid;

                Session["Expcat"] = dtexcelcate;


                GridExpcat.DataSource = dtexcelcate;
                GridExpcat.DataBind();
                Button1.Visible = false;
            }
            else
            {
                lblMessage.Text = "No Record Found In Particular Chapter";
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            GridEdit.Visible = false;
            Divpan.Visible = true;
            Button1.Visible = false;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;
            lbprevious.Visible = true;
            pnlcategory.Visible = true;
            GRcategory.Visible = true;
            string Qrymerch;
            double valgrant;
            string allocatedcostCy;
            double Allocval;
            DataRow drless = dtnewgrid.NewRow();
            drless[2] = "";
            DataRow Merchval = dtnewgrid.NewRow();
            Merchval[2] = "";
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
            }
            else
            {
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - 5;
            }
            pnlview.Visible = false;
            GRcategory.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField k = (HiddenField)Griddisp.Rows[index].Cells[0].FindControl("hdnid");
            HiddenField catdesc = (HiddenField)Griddisp.Rows[index].Cells[0].FindControl("Hncatedesc");
            LbCategory.Text = catdesc.Value;
            LbCategory.Visible = true;
            Expcategorystr = Convert.ToInt32(k.Value);
            bool Isfalse = false;
            for (i = endall; i <= Start; i++)
            {
                if (DDyear.SelectedItem.Text != "Fiscal Year")
                {
                    Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.[State],ch.ChapterCode,ej.ExpcatId as catid,ej.Tochapterid as chapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                   
                }
                else
                {
                    fiscyear = i + 1;
                   
                    Qrycondition = "with tbl as (select TOP 100 PERCENT  ch.[State],ch.ChapterCode,ej.ExpcatId as catid,ej.Tochapterid as chapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min (CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";
                }
                Qry = Qrycondition + genwherecategorycondition();
                dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                DataTable dtmerge = new DataTable();
                dtmerge = dsnew.Tables[0];
                Label lblgrant = new Label();
                Label lblmerch = new Label();
                if (Isfalse == false)
                {
                    firstquert = "with tbl as (select TOP 100 PERCENT ch.[State],ch.ChapterCode,ej.ExpcatId as catid,ej.Tochapterid as Chapterid,ch.ChapterCode as chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                    firstquert = firstquert + genwherecategorycondition();
                    DataSet dsnew1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, firstquert);
                    DataTable dtm = new DataTable();
                    dtm = dsnew1.Tables[0];
                    Mergecopy = dtm.Copy();
                    if (dtm.Rows.Count > 0)
                    {
                        Isfalse = true;
                        Mergecopy = dtm.Copy();
                    }
                }
                if (Isfalse == true)
                {
                    if (dtmerge.Rows.Count > 0)
                    {
                        for (int j = 0; j <= dtmerge.Rows.Count - 1; j++)
                        {
                            Label lbn = new Label();
                            lbn.Text = dtmerge.Rows[j][2].ToString();
                            for (int h = 0; h <= Mergecopy.Rows.Count - 1; h++)
                            {
                                Label lb = new Label();
                                lb.Text = Mergecopy.Rows[h][2].ToString();
                                if ((dtmerge.Rows[j][2].ToString() == Mergecopy.Rows[h][2].ToString()))
                                {
                                    string s = dtmerge.Rows[j]["" + i + ""].ToString();
                                    Mergecopy.Rows[h]["" + i + ""] = s;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            dtnewgrid = Mergecopy;
            if (dtnewgrid.Rows.Count > 0)
            {
                DataRow dr = dtnewgrid.NewRow();
                dr[2] = "Total (Sum)";
                DataRow drlessgrants = dtnewgrid.NewRow();
                drlessgrants[2] = "Total less Grants less Merchandise for Sale";
                DataRow drlessgrantsval = dtnewgrid.NewRow();
                drlessgrantsval[2] = "Total less Grants less Merchandise for Sale";
                DataRow Merchvalcal = dtnewgrid.NewRow();
                DataRow Allocacatedvie = dtnewgrid.NewRow();
                Allocacatedvie[2] = "Allocated Cost:  Medals, Trophies, etc.";
                DataRow drGrossExp = dtnewgrid.NewRow();
                drGrossExp[2] = "Total Gross Expense";
                for (int i = 5; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    dr[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {
                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }
                        }
                    }
                    dr[i] = Math.Round(sum, 2);
                }
                for (int j = 5; j <= dtnewgrid.Columns.Count - 2; j++)
                {
                    String colName = dtnewgrid.Columns[j].ColumnName;

                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "' and Ej.ExpCatID=" + Expcategorystr + " and ej.CheckNumber>0";
                        Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and Ej.ExpCatID=" + Expcategorystr + " and ej.CheckNumber>0";
                        allocatedcostCy = "select SUM(AllocatedCostCy) from AllocatedCost Ac inner join ExpJournal Ej on Ac.ChapterID=Ej.ToChapterID where   Ej.ExpCatID=" + Expcategorystr + " and  ContestYear=" + colName + " and Ej.Paid='y' and ej.CheckNumber>0";
                    }
                    else
                    {
                        int colfisc = Convert.ToInt32(colName) + 1;
                        Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where   Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "' and Ej.ToChapterId=" + Expcategorystr + "  and ej.CheckNumber>0";
                        Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'   and Ej.ToChapterId=" + Expcategorystr + " and ej.CheckNumber>0";
                        allocatedcostCy = "select SUM(AllocatedCostFy) from AllocatedCost Ac inner join ExpJournal Ej on Ac.ChapterID=Ej.ToChapterID where   Ej.ExpCatID=" + Expcategorystr + " and   ContestYear=" + colName + " and Ej.Paid='y' and ej.CheckNumber>0";
                    }
                    object qrymerch = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerch);
                    object Qrygrantcal = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Qrygrants);
                    object Qrylloc = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, allocatedcostCy);
                    Label lbAlloc = new Label();
                    lbAlloc.Text = Convert.ToString(Qrylloc);
                    if (lbAlloc.Text == "")
                    {
                        lbAlloc.Text = "0.00";
                    }
                    Allocval = Convert.ToDouble(lbAlloc.Text);
                    Label Merchlabl = new Label();
                    Merchlabl.Text = Convert.ToString(qrymerch);
                    if (Merchlabl.Text == "")
                    {
                        Merchlabl.Text = "0.00";
                    }
                    valmerch = Convert.ToDouble(Merchlabl.Text);
                    Label qrygrant1 = new Label();
                    qrygrant1.Text = Convert.ToString(Qrygrantcal);
                    if (qrygrant1.Text == "")
                    {
                        qrygrant1.Text = "0.00";
                    }
                    valgrant = Convert.ToDouble(qrygrant1.Text);
                    drlessgrants[j] = valgrant;
                    Merchvalcal[j] = valmerch;
                    Allocacatedvie[j] = Allocval;
                }
                for (int i = 5; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    if (!DBNull.Value.Equals(dr[i]))
                    {
                        double valTot = Convert.ToDouble(dr[i].ToString());
                        double grants = Convert.ToDouble(drlessgrants[i].ToString());
                        double merchval = Convert.ToDouble(Merchvalcal[i].ToString());
                        double Allocatedcostcal = Convert.ToDouble(Allocacatedvie[i].ToString());
                        less = valTot - grants;
                        merchless = less - merchval;
                        grossexpense = merchless + Allocatedcostcal;
                    }
                    drlessgrantsval[i] = merchless;
                    drGrossExp[i] = grossexpense;
                }
                dtnewgrid.Rows.Add(dr);
                dtnewgrid.Rows.Add(drlessgrantsval);
                dtnewgrid.Rows.Add(Allocacatedvie);
                dtnewgrid.Rows.Add(drGrossExp);
                dtexcelcate = dtnewgrid;
                for (int i = 5; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    foreach (DataRow drnw in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnw[i]))
                        {
                            //double st;
                            double st1;
                            // st= Convert.ToDouble(drnw["total"]);
                            st1 = Convert.ToDouble(drnw[i]);
                            // drnw["total"] = String.Format("{0:#,###0}", st);
                            drnw[i] = Convert.ToString(drnw[i]);
                            drnw[i] = String.Format("{0:#,###0}", st1);
                            // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                        }
                    }
                }
                for (int j = 0; j <= dtnewgrid.Rows.Count - 1; j++)
                {

                    dtnewgrid.Rows[j].BeginEdit();
                    double total = 0;
                    string totvalue = "";
                    for (int h = 5; h < dtnewgrid.Columns.Count - 1; h++)
                    {
                        //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                        totvalue = dtnewgrid.Rows[j][h].ToString();
                        if (totvalue != "")
                        {
                            total += Convert.ToDouble(dtnewgrid.Rows[j][h].ToString());
                        }
                    }
                    dtnewgrid.Rows[j]["Total"] = total;

                    dtnewgrid.Rows[j].EndEdit();
                }

                dtnewgrid.AcceptChanges();
                for (int i = dtnewgrid.Columns.Count - 1; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    foreach (DataRow drn in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drn))
                        {
                            string st;
                            double st1;
                            st1 = Convert.ToDouble(drn["Total"]);
                            st = String.Format("{0:#,###0}", st1);

                            //dr[i] = String.Format("{0:#,###0}", st1);
                            // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                            drn["Total"] = st.ToString();

                        }
                    }
                }

                dtnewgrid.AcceptChanges();
                Session["Detaildatatable"] = dtexcelcate;

                GRcategory.DataSource = dtexcelcate;
                GRcategory.DataBind();
            }
            else
            {
                lblMessage.Text = "No Record Found In Particular Category";
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void GridEdit_RowEditing(object sender, GridViewEditEventArgs e)
    {
       GridEdit.EditIndex = e.NewEditIndex;
        bindgrid();
    }
    protected void GridEdit_RowUpdating(object sender, GridViewUpdateEventArgs e)
  {
      try
      {
          Divpan.Visible = true;
          string Qrychapter;
          string chcode;
          string frmchcode;
          string s = GridEdit.DataKeys[e.RowIndex].Value.ToString();
          TextBox Eventyear = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("Txtyear");
          TextBox Eventid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxteventId");
          TextBox Chaptercode = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtChaptercode");
          TextBox frmChaptercode = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtfrmChaptercode");
          DropDownList ddchaptercod = (DropDownList)GridEdit.Rows[e.RowIndex].FindControl("DDchaptercode");
          DropDownList ddfromchaptercod = (DropDownList)GridEdit.Rows[e.RowIndex].FindControl("DDfrmchaptercode");
          if (ddfromchaptercod.SelectedIndex==0)
          {
              frmchcode = frmChaptercode.Text;
          }
          else
          {
              frmchcode = ddfromchaptercod.SelectedItem.Text;
          }
          if (ddchaptercod.SelectedItem.Text == "[Select Chapter]")
          {
                  chcode = Chaptercode.Text;
          }
          else
          {
             chcode = ddchaptercod.SelectedItem.Text;
          }
          if (chcode == "")
          {
          }
          else
          {
              Qrychapter = "Select ChapterID from chapter where chaptercode='" + chcode.ToString() + "'";
              DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrychapter);
              DataTable dt = ds.Tables[0];
              Txthidden.Text = dt.Rows[0]["ChapterID"].ToString();
          }
          if (frmchcode== "")
          {
          }
          else
          {
              Qrychapter = "Select ChapterID from chapter where chaptercode='" + frmchcode.ToString() + "'";
              DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrychapter);
              DataTable dt = ds.Tables[0];
              Txtchapterhidden.Text = dt.Rows[0]["ChapterID"].ToString();
          }
          Session["chcode"] = Txthidden.Text;
          Session["frmchcode"] = Txtchapterhidden.Text;
          TextBox Expcatcode = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtExpcatcode");
          TextBox Expenseamount = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtExpamount");
          TextBox Bankid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtBanKID");
          TextBox Tobankid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtToBankID");
          TextBox Transtype = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtTrans");
          TextBox Account = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtAccount");
          TextBox Comment = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("Txtcomment");
          TextBox reportdate = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtReportDate");
          TextBox Resttypefrom = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtRestTypeFrom");
          TextBox RestypeTo = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("Txtresttype");
          if (Eventid.Text == "")
          {
              Eventid.Text = "0";
          }
          if (Bankid.Text == "")
          {
              Bankid.Text = "0";
          }
          if (Tobankid.Text == "")
          {
              Tobankid.Text = "0";
          }
          if (Transtype.Text == "System.Web.UI.WebControls.TextBox")
          {
              Transtype.Text = "NULL";
          }
          if (Account.Text == "")
          {
              Account.Text = "null";
          }
          if ((Resttypefrom.Text == "System.Web.UI.WebControls.TextBox") || (Resttypefrom.Text == null))
          {
              Resttypefrom.Text = "NULL";
          }
          if (RestypeTo.Text == "System.Web.UI.WebControls.TextBox")
          {
              RestypeTo.Text = "NULL";
          }
          string qry = "update Expjournal set EventYear=" + Eventyear.Text + "  , EventId=" + Eventid.Text + " ,ChapterID=" + Txtchapterhidden.Text + " , TochapterId=" + Txthidden.Text + " ,  Expcatcode='" + Expcatcode.Text + "' , ExpenseAmount=" + Expenseamount.Text + " , BankId=" + Bankid.Text + " , ToBankId=" + Tobankid.Text + " , TransType='" + Transtype.Text + "' , Account=" + Account.Text + ",ReportDate='" + reportdate.Text + "'  ,RestTypeFrom='" + Resttypefrom.Text + "' ,RestTypeTo='" + RestypeTo.Text + "',Comments='" + Comment.Text + "' where Transactionid=" + s + "";
          SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qry);
          Response.Write("<script>alert('Updated successfully')</script>");
          GridEdit.EditIndex = -1;
          bindgrid();
          GridEdit.Visible = true;
      }
      catch (Exception err)
      {
          lblMessage.Text = err.Message;
      }

   }
    protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridEdit.EditIndex = -1;
        bindgrid();
        ddZone.Enabled = false;
        ddNoyear.Enabled = false;
        DDyear.Enabled = false;
        ddevent.Enabled = false;
        ddCluster.Enabled = false;
        ddchapter.Enabled = false;
    }
   
    public void bindgrid()
    {
        try
        {
            Divpan.Visible = true;
            string Expjournal;
            if (lbchapterpre.Visible == true)
            {
                Expjournal = "select ch.ChapterCode as frmcode,ch.ChapterID as frmid ,Ej.ToChapterID as tochapterid,chk.ChapterCode as tochaptercode,Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID, MemberName=Case when Ej.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,CAST((ej.ExpenseAmount)as decimal(10,2))as ExpenseAmount,Ej.TreatID,ej.TreatCode,CONVERT(VARCHAR(10),ej.NationalApprovalDate ,101)as NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,CONVERT(VARCHAR(10),ej.DatePaid ,101) as DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,CONVERT(VARCHAR(10),ej.ReportDate ,101) as ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=Ej.ChapterID inner join chapter chk on chk.ChapterID=Ej.ToChapterID left join OrganizationInfo org on org.AutoMemberID=ej.ReimbMemberID  where Ej.Paid='y' and Ej.CheckNumber>0  and Ej.TochapterID=" + Session["chapter1"] + " and Ej.expcatID=" + Session["Category1"] + "  order by Ej.EventYear, Ej.DatePaid ";
            }
            else
            {
                 Expjournal = "select ch.ChapterCode as frmcode,ch.ChapterID as frmid ,Ej.ToChapterID as tochapterid,chk.ChapterCode as tochaptercode,Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID, MemberName=Case when Ej.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,CAST((ej.ExpenseAmount)as decimal(10,2))as ExpenseAmount,Ej.TreatID,ej.TreatCode,CONVERT(VARCHAR(10),ej.NationalApprovalDate ,101)as NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,CONVERT(VARCHAR(10),ej.DatePaid ,101) as DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,CONVERT(VARCHAR(10),ej.ReportDate ,101) as ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=Ej.ChapterID inner join chapter chk on chk.ChapterID=Ej.ToChapterID left join OrganizationInfo org on org.AutoMemberID=ej.ReimbMemberID  where Ej.Paid='y' and Ej.CheckNumber>0  and Ej.TochapterID=" + Session["chapter"] + " and Ej.expcatID=" + Session["Category"] + "  order by Ej.EventYear, Ej.DatePaid ";
            }
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                GridEdit.DataSource = dt;
                GridEdit.DataBind();
                ddZone.Enabled = false;
                ddNoyear.Enabled = false;
                DDyear.Enabled = false;
                ddevent.Enabled = false;
                ddCluster.Enabled = false;
                ddchapter.Enabled = false;
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    public void btDetailschapter_Click(object sender, EventArgs e)
    {
        try
        {
            Divpan.Visible = true;
            Button1.Visible = false;
            EnableViewState = true;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;
            pnlview.Visible = false;
            pnlcategory.Visible = false;
            pnlEdit.Visible = true;
            GridEdit.Visible = true;
            GridExpcat.Visible = false;
            GridEdit.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField hdcatid = (HiddenField)GridExpcat.Rows[index].Cells[0].FindControl("hdcatid");
            HiddenField hdchapter = (HiddenField)GridExpcat .Rows[index].Cells[1].FindControl("Hdchapter");
            HiddenField hdcatdesc = (HiddenField)GridExpcat.Rows[index].Cells[1].FindControl("Hncatedesc");
            Expcategorystr1 = Convert.ToInt32(hdcatid.Value);
            Lbchapter.Text = hdcatdesc.Value;
            Lbchapter.Visible = true;
            Expchapterystr1 = Convert.ToInt32(hdchapter.Value);
            Session["Category1"] = Expcategorystr1;
            Session["chapter1"] = Expchapterystr1;
            string Expjournal = "select ch.ChapterCode as frmcode,ch.ChapterID as frmid ,Ej.ToChapterID as tochapterid,chk.ChapterCode as tochaptercode,Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID, MemberName=Case when Ej.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,CAST((ej.ExpenseAmount)as decimal(10,2))as ExpenseAmount,Ej.TreatID,ej.TreatCode,CONVERT(VARCHAR(10),ej.NationalApprovalDate ,101)as NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,CONVERT(VARCHAR(10),ej.DatePaid ,101) as DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,CONVERT(VARCHAR(10),ej.ReportDate ,101) as ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=Ej.ChapterID inner join chapter chk on chk.ChapterID=Ej.ToChapterID left join OrganizationInfo org on org.AutoMemberID=ej.ReimbMemberID  where Ej.Paid='y' and Ej.CheckNumber>0  and Ej.TochapterID=" + Expchapterystr1 + " and Ej.expcatID=" + Expcategorystr1 + "  order by Ej.EventYear, Ej.DatePaid ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
            ddCluster.DataSource = ds;
            DataTable dt = ds.Tables[0];
            GridEdit.DataSource = dt;
            GridEdit.DataBind();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    public void btDetails_Click(object sender, EventArgs e)
    {
        try
        {
            Divpan.Visible = true;
            Button1.Visible = false;
            EnableViewState = true;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;
            pnlview.Visible = false;
            pnlcategory.Visible = false;
            pnlEdit.Visible = true;
            GridEdit.Visible = true;
            GridEdit.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField hdcatid = (HiddenField)GRcategory.Rows[index].Cells[0].FindControl("hdcatid");
            HiddenField hdchapter = (HiddenField)GRcategory.Rows[index].Cells[1].FindControl("Hdchapter");
            HiddenField hdchaptername = (HiddenField)GRcategory.Rows[index].Cells[1].FindControl("Hdchaptername");
            Expcategorystr = Convert.ToInt32(hdcatid.Value);
            Lbchapter.Text = hdchaptername.Value;
            Lbchapter.Visible = true;
            Expchapterystr = Convert.ToInt32(hdchapter.Value);
            Session["Category"] = Expcategorystr;
            Session["chapter"] = Expchapterystr;
            string Expjournal = "select ch.ChapterCode as frmcode,ch.ChapterID as frmid ,Ej.ToChapterID as tochapterid,chk.ChapterCode as tochaptercode,Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID, MemberName=Case when Ej.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,CAST((ej.ExpenseAmount)as decimal(10,2))as ExpenseAmount,Ej.TreatID,ej.TreatCode,CONVERT(VARCHAR(10),ej.NationalApprovalDate ,101)as NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,CONVERT(VARCHAR(10),ej.DatePaid ,101) as DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,CONVERT(VARCHAR(10),ej.ReportDate ,101) as ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=Ej.ChapterID inner join chapter chk on chk.ChapterID=Ej.ToChapterID left join OrganizationInfo org on org.AutoMemberID=ej.ReimbMemberID  where Ej.Paid='y' and Ej.CheckNumber>0  and Ej.TochapterID=" + Expchapterystr + " and Ej.expcatID=" + Expcategorystr + "  order by Ej.EventYear, Ej.DatePaid ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
            ddCluster.DataSource = ds;
            DataTable dt = ds.Tables[0];
            GridEdit.DataSource = dt;
            GridEdit.DataBind();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void lbprevious_Click(object sender, EventArgs e)
    {
        pnlview.Visible = true;
        Button1.Visible = false;
            if (GRcategory.Visible == true)
            {
                pnlview.Visible = true;
                Griddisp.Visible = true;
                Button1.Visible = true;
                pnlcategory.Visible = false;
                GRcategory.Visible = false;
                pnlEdit.Visible = false;
                Lbchapter.Visible = false;
                LbCategory.Visible = false;
                ddZone.Enabled = true;
                ddNoyear.Enabled = true;
                DDyear.Enabled = true;
                ddevent.Enabled = true;
                ddCluster.Enabled = true;
                ddchapter.Enabled = true;
                Button1.Visible = true;
                lbprevious.Visible = false;
            }
            else if (GridEdit.Visible == true)
            {
                pnlcategory.Visible = true;
                GRcategory.Visible = true;
                pnlview.Visible = false;
                pnlEdit.Visible = false;
                Lbchapter.Visible = false;
                ddZone.Enabled = false;
                ddNoyear.Enabled = false;
                DDyear.Enabled = false;
                ddevent.Enabled = false;
                ddCluster.Enabled = false;
                ddchapter.Enabled = false;
            }
    }
    protected void Griddisp_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
        {
            if (isChapterRepot != true)
            {
                DataRowView drview = e.Row.DataItem as DataRowView;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Button dpEmpdept = (Button)e.Row.FindControl("btnview");
                    HiddenField hdnid = (HiddenField)e.Row.FindControl("hdnid");
                    if (hdnid.Value == "")
                    {
                        dpEmpdept.Visible = false;
                    }
                }
            }
        }
        else
        {
            Griddisp.Columns[0].Visible = false;
            DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button dpEmpdept = (Button)e.Row.FindControl("btnview");
                HiddenField hdnid = (HiddenField)e.Row.FindControl("hdnid");
                if ((hdnid.Value == "")||(hdnid.Value != ""))
                {
                    dpEmpdept.Visible = false;
                }
            }
        }
        if (e.Row.RowIndex == -1) return;
        if (DDcategory.Visible == true)
        {
            newvar = 3;
        }
        else
        {
            newvar = 4;
        }
        for (int i = newvar; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {

                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
    }
    protected void GridEdit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if ((Session["RoleId"] != null) && (Session["RoleId"].ToString() == "2"))
            {
            GridEdit.Columns[0].Visible = false;
            DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button dpEmpdept = (Button)e.Row.FindControl("btnview");
                HiddenField hdnid = (HiddenField)e.Row.FindControl("hdnid");
                if ((hdnid.Value == "") || (hdnid.Value != ""))
                {
                    dpEmpdept.Visible = false;
                }
            }
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
                if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
            }
          
        }
        catch (Exception ex) { }
    }
    protected void GRcategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
         DataRowView drview = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btDetails");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("hdcatid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
        if (e.Row.RowIndex == -1) return;
        if (DDcategory.Visible == true)
        {
            newvar = 3;
        }
        else
        {
            newvar = 4;
        }
        for (int i = 6; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
               
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
    }
    protected void GridChapter_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowIndex == -1) return;
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
        {
            DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button dpEmpdept = (Button)e.Row.FindControl("btnview");
                HiddenField hdnid = (HiddenField)e.Row.FindControl("hdcatid");
                if (hdnid.Value == "")
                {
                    dpEmpdept.Visible = false;
                }
            }
        }
        else
        {
            GridChapter.Columns[0].Visible = false;
            DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button dpEmpdept = (Button)e.Row.FindControl("btnview");
                HiddenField hdnid = (HiddenField)e.Row.FindControl("hdcatid");
                if ((hdnid.Value == "") || (hdnid.Value != ""))
                {
                    dpEmpdept.Visible = false;
                }
            }
        }
    }
    protected void GridExpcat_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 5; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        DataRowView drview = e.Row.DataItem as DataRowView;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btndetail1");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("hdcatid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void lbchapterpre_Click(object sender, EventArgs e)
    {
        Button1.Visible = false;
          if (GridExpcat.Visible == true)
            {
                Button1.Visible = true;
                Button1.Visible = true;
                GridExpcat.Visible = false;
                GridChapter.Visible = true;
                pnlEdit.Visible = false;
                ddZone.Enabled = false;
                ddNoyear.Enabled = false;
                DDyear.Enabled = false;
                ddevent.Enabled = false;
                ddCluster.Enabled = false;
                ddchapter.Enabled = false;
                Lbchapter.Visible = false;
                LbCategory.Visible = false;
                ddZone.Enabled = true;
                ddNoyear.Enabled = true;
                DDyear.Enabled = true;
                ddevent.Enabled = true;
                ddCluster.Enabled = true;
                ddchapter.Enabled = true;
                Button1.Visible = true;
                lbchapterpre.Visible = false;
            }
          else if (GridEdit.Visible == true)
          {
              pnlview.Visible = false;
              pnlEdit.Visible = false;
              GridExpcat.Visible = true;
              Lbchapter.Visible = false;
              ddZone.Enabled = false;
              ddNoyear.Enabled = false;
              DDyear.Enabled = false;
              ddevent.Enabled = false;
              ddCluster.Enabled = false;
              ddchapter.Enabled = false;
        }
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
    }
    protected void BtnClick_Click(object sender, EventArgs e)
    {
        if (DDFront.SelectedItem.Text != "[Select Report]")
        {
            lbReport.Visible = false;
            if (DDFront.SelectedItem.Text == "Expense category")
            {
                Session["ExpcatExcel"] ="ExpcatExcel";
                divcat.Visible = true;
                Pnlfront.Visible = false;
                Divpan.Visible = false;
                divmerc.Visible = false;
                divall.Visible = false;
                Pnlcategorytitle.Visible = true;
                Pnldisp.Visible = true;
                DDFront.Visible = false;
                BtnClick.Visible = false;
                Pnlsummary.Visible = false;
                LBbacktofront.Visible = true;
            }
            else if (DDFront.SelectedItem.Text == "Chapter")
            {
                Pnlfront.Visible = false;
                Divpan.Visible = false;
                divmerc.Visible = false;
                divall.Visible = false;
                Pnldisp.Visible = true;
                DDFront.Visible = false;
                BtnClick.Visible = false;
                Pnlsummary.Visible = false;
                LBbacktofront.Visible = true;
                PnlChapterdisp.Visible = true;
                Pnlcategorytitle.Visible = false;
            }
            else if (DDFront.SelectedItem.Text == "Allocated Cost")
            {
                divmerc.Visible = false;
                LBbacktofront.Visible = true;
                PnlAllocated.Visible = true;
                Pnlsummary.Visible = false;
                DDlAllocatedcost(DDAllocatechoice);
                Lboption.Visible = true;
                DDAllocatechoice.Visible = true;
                DDFront.Visible = false;
                BtnClick.Visible = false;
                DDAllocatechoice.Visible = true;
                Btncontinue.Visible = true;
            }
            else if (DDFront.SelectedItem.Text == "Merchandise")
            {
                LBbacktofront.Visible = true;
                divmerc.Visible = true;
                Pnlsummary.Visible = false;
                PnlAllocated.Visible = true;
                divall.Visible = false;
            }
        }
        else
        {
            lblMessage.Text = "Enter Reports";
            lblMessage.Visible = true;
        }
    }
    protected void LBbacktofront_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
    }
    protected void DDlAllocatedcost(DropDownList DDobject)
    {
        DDobject.Items.Clear();
        DDobject.Items.Insert(0, new ListItem("Contest Registrations Report", "3"));
        DDobject.Items.Insert(0, new ListItem("Allocated Cost Report", "2"));
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || ((Session["RoleId"].ToString() == "37") && (hdnTechNational.Value == "Y")) || ((Session["RoleId"].ToString() == "38") && (hdnTechNational.Value == "Y")) || ((Session["RoleId"].ToString() == "37") && (hdnTechNational.Value == "Y")) || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "85")))
        {
            DDobject.Items.Insert(0, new ListItem("Add/Update AllocatedCost Table", "1"));
        }
        DDobject.Items.Insert(0, new ListItem("[Select AllocatedCost]", "0"));
    }
    protected void DDlContestYear(DropDownList DDobject)
    {
        object year = DateTime.Now.Year;
        int yearpre;
        int yearprevious;
        int yearprevious2;
        int yearprevious1;
        yearpre = Convert.ToInt16(year) - 1;
        yearprevious = Convert.ToInt16(year) - 2;
        yearprevious1 = Convert.ToInt16(year) - 3;
        yearprevious2 = Convert.ToInt16(year) - 4;
        DDobject.Items.Insert(0, new ListItem(yearprevious2.ToString()));
        DDobject.Items.Insert(0, new ListItem(yearprevious1.ToString()));
        DDobject.Items.Insert(0, new ListItem(yearprevious.ToString()));
        DDobject.Items.Insert(0, new ListItem(yearpre.ToString()));
        DDobject.Items.Insert(0, new ListItem(year.ToString()));
        DDobject.Items.Insert(0, new ListItem("[Select ContestYear]", "0"));
    }
    protected void Btncontinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (DDAllocatechoice.SelectedItem.Text != "[Select AllocatedCost]")
            {
                Lboption.Visible = false;
                if (DDAllocatechoice.SelectedItem.Text == "Add/Update AllocatedCost Table")
                {
                    Lballocated.Visible = true;
                    DDlContestYear(DDAddupdate);
                    Lbcontestyear.Visible = true;
                    DDAddupdate.Visible = true;
                    BtnAddupdate.Visible = true;
                    DDAddupdate.Visible = true;
                    BtnAddupdate.Visible = true;
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Allocated Cost Report")
                {
                    divallrep.Visible = false;
                    Lballocated.Visible = true;
                    divall.Visible = false;
                    divallrep.Visible = true;
                    Pnlfront.Visible = false;
                    ddevent.SelectedItem.Text = "Regionals";
                    ddevent.Enabled = false;
                    Pnldisp.Visible = true;
                    BtnAddupdate.Visible = false;
                    DDAddupdate.Visible = false;
                }
                else if (DDAllocatechoice.SelectedItem.Text == "Contest Registrations Report")
                {
                    divall.Visible = false;
                    Lballocated.Visible = true;
                    divcontestrep.Visible = true;
                    divall.Visible = false;
                    Pnlfront.Visible = false;
                    ddevent.SelectedItem.Text = "Regionals";
                    ddevent.Enabled = false;
                    Pnldisp.Visible = true;
                    BtnAddupdate.Visible = false;
                    DDAddupdate.Visible = false;
                }
                Btncontinue.Visible = false;
                DDAllocatechoice.Visible = false;
                lbReport.Visible = false;
            }
            else
            {
                lblMessage.Text = "Enter AllocatedCost";
                lblMessage.Visible = true;
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void BtnAddupdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (DDAddupdate.SelectedItem.Text != "[Select ContestYear]")
            {
                string upquery = "select distinct Contestyear from AllocatedCost where ContestYear='" + DDAddupdate.SelectedItem.Text + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, upquery);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    Panlvalid.Visible = true;
                }
                else
                {
                    AddUpdateCalculation();
                }
            }
            else
            {
                lblMessage.Text = "Enter ContestYear";
                lblMessage.Visible = true;
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void AddUpdateCalculation()
    {
        try
        {
            int yearreg = Convert.ToInt16(DDAddupdate.SelectedItem.Text);
            int yeratFy = yearreg + 1;
            string Qryselect = "select  A.ContestYear, A.ChapterID, COUNT(*) PaidReg,(select COUNT(*) paidreg from contestant where EventId =2 and ContestYear =A.ContestYear and PaymentReference is not null group by ContestYear ) as Total from Contestant A where A.EventId=2 and A.ContestYear='" + yearreg + "' and A.PaymentReference is not null group by A.contestyear, A.ChapterID";
            DataSet dsAddqry = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qryselect);
            DataTable dtAddqry = new DataTable("items");
            dtAddqry= dsAddqry.Tables[0];
            string qrygrant;
            string totalcalc;
            string totQry;
            string Qrymerch;
            string Qrymerchcond;
            string QrygrantsFy;
            string QrymerchFy;
            string totalcalcFy;
            string QrymerchcondFy;
            string qrygrantFy;
            string totQryFy;
            Qrygrants = "select Isnull (sum(Ej.ExpenseAmount),0)  as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + yearreg + "' and '12/31/" + yearreg + "' and Ej.ToChapterId=109 and ej.CheckNumber>0";
            Qrymerch = "select  Isnull (sum(Ej.ExpenseAmount),0)  as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + yearreg + "' and '12/31/" + yearreg + "' and Ej.ToChapterID=109   and ej.CheckNumber>0";
            totalcalc = "select Isnull (sum(Ej.ExpenseAmount),0)   as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '01/01/" + yearreg + "' and '12/31/" + yearreg + "' and  CheckNumber>0 and Ej.ToChapterID=109 and  Ej.ExpCatID is not null and Ej.ToChapterID is not null";
            QrygrantsFy = "select Isnull (sum(Ej.ExpenseAmount),0)  as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + yearreg + "' and '04/30/" + yeratFy + "' and Ej.ToChapterId=109 and ej.CheckNumber>0";
            QrymerchFy = "select Isnull (sum(Ej.ExpenseAmount),0)  as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + yearreg + "' and '04/30/" + yeratFy + "' and Ej.ToChapterID=109   and ej.CheckNumber>0";
            totalcalcFy = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '05/01/" + yearreg + "' and '04/30/" + yeratFy + "' and  CheckNumber>0 and Ej.ToChapterID=109 and  Ej.ExpCatID is not null and Ej.ToChapterID is not null";
            Qrymerchcond = Qrymerch + gengrantlinkWhereconditions();
            qrygrant = Qrygrants + gengrantlinkWhereconditions();
            totQry = totalcalc + gengrantlinkWhereconditions();
            QrymerchcondFy = QrymerchFy + gengrantlinkWhereconditions();
            qrygrantFy = QrygrantsFy + gengrantlinkWhereconditions();
            totQryFy = totalcalcFy + gengrantlinkWhereconditions();
            object dsnewmerge = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerchcond);
            object dsnew = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, qrygrant);
            Object dsnewtot = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, totQry);
            Object dsnewmergeFy = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, QrymerchcondFy);
            object dsnewFy = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, qrygrantFy);
            object dsnewtotFy = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, totQryFy);
            Label lblgrant = new Label();
            Label lbltotcalc = new Label();
            Label lblmerch = new Label();
            Label lblgrantFy = new Label();
            Label lbltotcalcFy = new Label();
            Label lblmerchFy = new Label();
            lbltotcalc.Text =Convert.ToString(dsnewtot);
            lblgrant.Text = Convert.ToString(dsnew);
            lblmerch.Text = Convert.ToString(dsnewmerge);
            lbltotcalcFy.Text = Convert.ToString(dsnewtotFy);
            lblgrantFy.Text = Convert.ToString(dsnewFy);
            lblmerchFy.Text = Convert.ToString(dsnewmergeFy);
            grantmerch = Convert.ToDouble(lblmerch.Text);
            grantcal = Convert.ToDouble(lblgrant.Text);
            grantcalFy = Convert.ToDouble(lblgrantFy.Text);
            grantmerchFy = Convert.ToDouble(lblmerchFy.Text);
            double valTot = Convert.ToDouble(lbltotcalc.Text);
            double grants = Convert.ToDouble(grantcal);
            double merchval = Convert.ToDouble(grantmerch);
            less = valTot - grants;
            merchless = less - merchval;
            double valTotFy = Convert.ToDouble(lbltotcalcFy.Text);
            double grantsFy = Convert.ToDouble(grantcalFy);
            double merchvalFy = Convert.ToDouble(grantmerchFy);
            lessFy = valTotFy - grantsFy;
            merchlessFy = lessFy - merchvalFy;
            dtAddqry.Columns.Add("AllocatedCostCY");
            dtAddqry.Columns.Add("AllocatedCostFy");
            for (int i = 0; i <= dtAddqry.Rows.Count - 1; i++)
            {
                double paidreg;
                double Total;
                double calc;
                double Homeoffice;
                double AllocatedCost;
                double calcFy;
                double HomeofficeFy;
                double AllocatedCostFy;
                paidreg = Convert.ToDouble(dtAddqry.Rows[i]["PaidReg"].ToString());
                Total = Convert.ToDouble(dtAddqry.Rows[i]["Total"].ToString());
                calc = paidreg / Total;
                Homeoffice = merchless;
                AllocatedCost = Homeoffice * calc;
                calcFy = paidreg / Total;
                HomeofficeFy = merchlessFy;
                AllocatedCostFy = HomeofficeFy * calcFy;
                dtAddqry.Rows[i]["AllocatedCostCY"] = AllocatedCost;
                dtAddqry.Rows[i]["AllocatedCostFy"] = AllocatedCostFy;
                DataTable dt = new DataTable();
            }
            DataTable dtg = new DataTable();
            dtg = dtAddqry;
            int createdby=int.Parse(Session["LoginID"].ToString());
            if (!Isupdate == true)
            {
                for (int i = 0; i < dtAddqry.Rows.Count; i++)
                {
                    string sqlstring = "INSERT INTO AllocatedCost (ContestYear,ChapterId,PaidReg,AllocatedCostCY,AllocatedCostFY,CreateDate,CreatedBy ) VALUES('" + dtAddqry.Rows[i][0].ToString() + "','" + dtAddqry.Rows[i][1].ToString() + "','" + dtAddqry.Rows[i][2].ToString() + "',CONVERT(DECIMAL(16,4),'" + dtAddqry.Rows[i][4].ToString() + "'),CONVERT(DECIMAL(16,4),'" + dtAddqry.Rows[i][5].ToString() + "'),'" + System.DateTime.Now + "','" + createdby + "')";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlstring);
                }
                Response.Write("<script>alert('Inserted successfully')</script>");
            }
            else
            {
                for (int i = 0; i < dtAddqry.Rows.Count; i++)
                {
                    string sqlstring = "Update  AllocatedCost set ContestYear='" + dtAddqry.Rows[i][0].ToString() + "',ChapterId='" + dtAddqry.Rows[i][1].ToString() + "',PaidReg='" + dtAddqry.Rows[i][2].ToString() + "',AllocatedCostCY=CONVERT(DECIMAL(16,4),'" + dtAddqry.Rows[i][4].ToString() + "'),AllocatedCostFY=CONVERT(DECIMAL(16,4),'" + dtAddqry.Rows[i][5].ToString() + "'),CreateDate='" + System.DateTime.Now + "',CreatedBy='" + createdby + "'  where ChapterID='" + dtAddqry.Rows[i][1].ToString() + "' and ContestYear='" + dtAddqry.Rows[i][0].ToString() + "'";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlstring);
                }
                Response.Write("<script>alert('Updated successfully')</script>");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        Panlvalid.Visible = false;
        Isupdate = true;
        AddUpdateCalculation();
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        Panlvalid.Visible = false;
        DDAddupdate.SelectedIndex = 0;
    }
    protected void AllocatedCostReport()
    {
        try
        {
            GRcategory.Visible = false;
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.[State],AC.ChapterID,ch.ChapterCode,AC.AllocatedCostCY as Allcost,CONVERT(varchar(50),AC.ContestYear)as [Year] from chapter ch inner join AllocatedCost AC on ch.ChapterID=AC.ChapterID";
            }
            else
            {
                Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.[State],AC.ChapterID,ch.ChapterCode,AC.AllocatedCostFY as Allcost,CONVERT(varchar(50),AC.ContestYear)as [Year] from chapter ch inner join AllocatedCost AC on ch.ChapterID=AC.ChapterID";
            }
            Qry = Qrycondition + genwherecondAllcost();
            DataSet Dsallcost = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtallcost = Dsallcost.Tables[0];
            DataRow dr = dtallcost.NewRow();
            dr[0] = "Total";
            for (int i = 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                dr[i] = 0;
                double sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }
                    }
                }
                dr[i] = sum;
            }
            dtallcost.Rows.Add(dr);
            for (int i = 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                foreach (DataRow drnw in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drnw[i]))
                    {
                        //double st;
                        double st1;
                        // st= Convert.ToDouble(drnw["total"]);
                        st1 = Convert.ToDouble(drnw[i]);
                        // drnw["total"] = String.Format("{0:#,###0}", st);
                        drnw[i] = Convert.ToString(drnw[i]);
                        drnw[i] = String.Format("{0:#,###0}", st1);
                        // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                    }
                }
            }
            dtallcost.Columns.Add("Total", typeof(String));
            for (int j = 0; j <= dtallcost.Rows.Count - 1; j++)
            {

                dtallcost.Rows[j].BeginEdit();
                double total = 0;
                string totvalue = "";
                for (int k = 1; k < dtallcost.Columns.Count - 1; k++)
                {
                    //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                    totvalue = dtallcost.Rows[j][k].ToString();
                    if (totvalue != "")
                    {
                        total += Convert.ToDouble(dtallcost.Rows[j][k].ToString());
                    }
                }
                dtallcost.Rows[j]["Total"] = total;

                dtallcost.Rows[j].EndEdit();
            }

            dtallcost.AcceptChanges();
            for (int i = dtallcost.Columns.Count - 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                foreach (DataRow drn in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drn))
                    {
                        string st;
                        double st1;
                        st1 = Convert.ToDouble(drn["Total"]);
                        st = String.Format("{0:#,###0}", st1);

                        //dr[i] = String.Format("{0:#,###0}", st1);
                        // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                        drn["Total"] = st.ToString();

                    }
                }
            }

            dtallcost.AcceptChanges();
            GridView1.DataSource = dtallcost;
            GridView1.DataBind();
            GridView1.Visible = true;
            Button2.Enabled = true;
            Session["AllocatedCost"] = dtallcost;
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void ContestReport()
    {
        try
        {
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.[State],AC.ChapterID,ch.ChapterCode,AC.PaidReg as PaidReg,CONVERT(varchar(50),AC.ContestYear)as [Year] from chapter ch inner join AllocatedCost AC on ch.ChapterID=AC.ChapterID";
            }
            else
            {
                Qrycondition = "with tbl as (select  TOP 100 PERCENT ch.[State],AC.ChapterID,ch.ChapterCode,AC.PaidReg as PaidReg,CONVERT(varchar(50),AC.ContestYear)as [Year] from chapter ch inner join AllocatedCost AC on ch.ChapterID=AC.ChapterID";
            }
            Qry = Qrycondition + genwherecondAllcost();
            DataSet Dsallcost = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
             dtallcost = Dsallcost.Tables[0];
            DataRow dr = dtallcost.NewRow();
            dr[0] = "Total";
            for (int i = 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                dr[i] = 0;
                double sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }
                    }
                }
                dr[i] = sum;
            }
            dtallcost.Rows.Add(dr);
            for (int i = 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                foreach (DataRow drnw in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drnw[i]))
                    {
                        //double st;
                        double st1;
                        // st= Convert.ToDouble(drnw["total"]);
                        st1 = Convert.ToDouble(drnw[i]);
                        // drnw["total"] = String.Format("{0:#,###0}", st);
                        drnw[i] = Convert.ToString(drnw[i]);
                        drnw[i] = String.Format("{0:#,###0}", st1);
                        // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);


                    }
                }
            }
            dtallcost.Columns.Add("Total", typeof(String));
            for (int j = 0; j <= dtallcost.Rows.Count - 1; j++)
            {

                dtallcost.Rows[j].BeginEdit();
                double total = 0;
                string totvalue = "";
                for (int k = 1; k < dtallcost.Columns.Count - 1; k++)
                {
                    //string str = dtnewgrid.Rows[j]["" + k + ""].ToString();
                    totvalue = dtallcost.Rows[j][k].ToString();
                    if (totvalue != "")
                    {
                        total += Convert.ToDouble(dtallcost.Rows[j][k].ToString());
                    }
                }
                dtallcost.Rows[j]["Total"] = total;

                dtallcost.Rows[j].EndEdit();
            }

            dtallcost.AcceptChanges();
            for (int i = dtallcost.Columns.Count - 1; i <= dtallcost.Columns.Count - 1; i++)
            {
                foreach (DataRow drn in dtallcost.Rows)
                {
                    if (!DBNull.Value.Equals(drn))
                    {
                        string st;
                        double st1;
                        st1 = Convert.ToDouble(drn["Total"]);
                        st = String.Format("{0:#,###0}", st1);

                        //dr[i] = String.Format("{0:#,###0}", st1);
                        // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["total"]);
                        drn["Total"] = st.ToString();

                    }
                }
            }

            dtallcost.AcceptChanges();
            GridView1.DataSource = dtallcost;
            Session["ContestantReport"] = dtallcost;
            GridView1.DataBind();
            GridView1.Visible = true;
            Button2.Enabled = true;
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string genwherecondAllcost()
    {
        string iCondtions = string.Empty;
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and AC.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        if (DDAllocatechoice.SelectedItem.Text != "Contest Registrations Report")
        {
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                return iCondtions + " group by AC.ChapterID,Ch.ChapterCode,AC.AllocatedCostCY,AC.ContestYear,ch.[state] order by ch.[state]) select ChapterCode " + am + " from tbl pivot(sum(Allcost) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
            }
            else
            {
                return iCondtions + " group by AC.ChapterID,Ch.ChapterCode,AC.AllocatedCostFY,AC.ContestYear,ch.[state] order by ch.[state]) select ChapterCode " + am + " from tbl pivot(sum(Allcost) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
            }
        }
        else
        {
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                return iCondtions + " group by AC.ChapterID,Ch.ChapterCode,AC.PaidReg,AC.ContestYear,ch.[state] order by ch.[state]) select ChapterCode " + am + " from tbl pivot(sum(PaidReg) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
            }
            else
            {
                return iCondtions + " group by AC.ChapterID,Ch.ChapterCode,AC.PaidReg,AC.ContestYear,ch.[state] order by ch.[state]) select ChapterCode " + am + " from tbl pivot(sum(PaidReg) for [Year] in (" + WCNT + "))As PVTTBL order by [State],ChapterCode";
            }
        }
    }
    protected void DDcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDFront.SelectedItem.Text == "Allocated Cost")
        {
            ddevent.Items.Insert(0, new ListItem("Regionals", "2"));
            ddevent.Enabled = false;
        }
    }
    protected void ddNoyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDFront.SelectedItem.Text == "Allocated Cost")
        {
            ddevent.Items.Clear();
            ddevent.Items.Insert(0, new ListItem("Regionals", "2"));
            ddevent.Enabled = false;
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (DDAllocatechoice.SelectedItem.Text != "Contest Registrations Report")
        {
            if (e.Row.RowIndex == -1) return;
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
                {
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                }
            }
        }
        else
        {
             if (e.Row.RowIndex == -1) return;
             for (int i = 1; i < e.Row.Cells.Count; i++)
             {
                 if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
                 {
                     e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                     e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                 }
             }
        }
    }
    protected void Lballocated_Click(object sender, EventArgs e)
    {
        Lboption.Visible = true;
        BtnAddupdate.Visible = false;
        divall.Visible = true;
        divallrep.Visible = false;
        Pnlfront.Visible = true;
        Pnldisp.Visible = false;
        divcontestrep.Visible = false;
        DDAddupdate.Visible = false;
        Lbcontestyear.Visible = false;
        DDAllocatechoice.Visible = true;
        Btncontinue.Visible = true;
        Lballocated.Visible = false;
    }
   
}



 
