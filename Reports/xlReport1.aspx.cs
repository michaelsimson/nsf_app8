using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_xlReport1 : System.Web.UI.Page
{
    int chapID;
    string Year = "";
    DataTable dt;
    DateTime[] day = { DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900") };

    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        Year = SqlHelper.ExecuteScalar(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(), CommandType.Text, "Select Max(ContestYear) From Contestant").ToString();
           // System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        lblChapter.Text = GetChapterName(chapID);
        GetData();
    }
    private void GetData()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        /*
        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contest.ContestCategoryID from Contestant "
            +" INNER JOIN Contest ON Contestant.ContestCode = Contest.ContestID "+
            " Where Contestant.PaymentReference IS NOT NULL AND Contestant.ChapterID=" +chapID+" AND Contestant.ContestYear='"+
            Year+"' ORDER BY Contestant.ChildNumber";
        */
        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Child.FIRST_NAME, " +
            "Child.MIDDLE_INITIAL, Child.LAST_NAME, Child.DATE_OF_BIRTH, Child.GRADE, ISNULL(Contestant.BadgeNumber,Contestant.ProductCode) as BadgeNumber,ISNULL(cp.TshirtSize,''),Case When I.DonorType='IND' Then ISNULL(I.AutoMemberID,'') Else ISNULL(I.Relationship,'') END as MemberID from Contestant " + //,Contestant.ParentID as MemberID 
            "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber LEFT JOIN dbo.ContestantPhoto cp ON cp.ChildNumber = Contestant.ChildNumber and cp.MemberId =Contestant.ParentID and cp.ContestYear = Contestant.ContestYear " +
            //Added on 11-07-2013 to show AutoMemberID of INDRecord
            "Left Join IndSpouse I on I.AutoMemberID = Contestant.ParentID " +
            //***********
            " Where (Contestant.PaymentReference IS NOT NULL OR Contestant.PaymentReference <> '') AND Contestant.ChapterID=" + chapID + " AND Contestant.ContestYear=" +
            Year + " ORDER BY Child.LAST_NAME, Child.FIRST_NAME";
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        DataSet dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        if (dsContestants.Tables[0].Rows.Count > 0)
            ProcessData(dsContestants);
        else
        {
            Response.Write("No records found!");
            btnSave.Visible = false;
        }

    }
    void ProcessData(DataSet ds)
    {
        DataSet dsContests = GetContests();
        dt = new DataTable();
        dt.Columns.Add("Ser#", typeof(int));
        dt.Columns.Add("FirstName", typeof(string));
        dt.Columns.Add("LastName", typeof(string));
        dt.Columns.Add("DOB", typeof(string));
        dt.Columns.Add("Gr", typeof(string));
        dt.Columns.Add("SpellingBee", typeof(string));
        dt.Columns.Add("Vocabulary", typeof(string));
        dt.Columns.Add("MathBee", typeof(string));
        dt.Columns.Add("Science", typeof(string));
        dt.Columns.Add("Geography", typeof(string));
        dt.Columns.Add("EssayWriting", typeof(string));
        dt.Columns.Add("PublicSpeaking", typeof(string));
        //For finals, we need Brain Bee
        dt.Columns.Add("BrainBee", typeof(string));
        dt.Columns.Add("Rank", typeof(string));
        dt.Columns.Add("Day1", typeof(string));//Type.GetType("System.Int32")
        dt.Columns.Add("Day2", typeof(string));
        dt.Columns.Add("Day3", typeof(string));
        dt.Columns.Add("TshirtSize", typeof(string));
        dt.Columns.Add("MemberID", typeof(int));
        
        string value = "";
        string prevChild = ds.Tables[0].Rows[0].ItemArray[0].ToString();
        int contestantNo = 1;
        DataRow dr = dt.NewRow();
        dr["Ser#"] = contestantNo.ToString();
        dr["FirstName"] = ds.Tables[0].Rows[0].ItemArray[2].ToString();
        dr["LastName"] = ds.Tables[0].Rows[0].ItemArray[4].ToString();
        value = ds.Tables[0].Rows[0].ItemArray[5].ToString();
        if (value != "")
            dr["DOB"] = Convert.ToDateTime(value).ToShortDateString();
        dr["Gr"] = ds.Tables[0].Rows[0].ItemArray[6].ToString();
        dr["MemberID"] = ds.Tables[0].Rows[0].ItemArray[9].ToString();//11-07-2013
        dr["Day1"] = 0;
        dr["Day2"] = 0;
        dr["Day3"] = 0;
        int i = 0;
        for (; i < ds.Tables[0].Rows.Count; i++)
        {

            if (prevChild == ds.Tables[0].Rows[i].ItemArray[0].ToString())
            {
                AddRow(ds, dsContests, dr, i, contestantNo);                
            }
            else
            {
                if (Convert.ToInt32(dr["Day1"]) == 0) dr["Day1"] = "";
                if (Convert.ToInt32(dr["Day2"]) == 0) dr["Day2"] = "";
                if (Convert.ToInt32(dr["Day3"]) == 0) dr["Day3"] = ""; 
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["Day1"] = 0;
                dr["Day2"] = 0;
                dr["Day3"] = 0;
                AddRow(ds, dsContests, dr, i, contestantNo + 1);
                contestantNo++;
                prevChild = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                dr["Ser#"] = contestantNo.ToString();
                dr["FirstName"] = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                dr["LastName"] = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                dr["DOB"] = Convert.ToDateTime(ds.Tables[0].Rows[i].ItemArray[5]).ToShortDateString();
                dr["Gr"] = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                dr["TshirtSize"] = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                dr["MemberID"] = ds.Tables[0].Rows[i].ItemArray[9].ToString();
            }
        }
        //AddRow(ds, dsContests, dr, i - 1, contestantNo);
        if (Convert.ToInt32(dr["Day1"]) == 0) dr["Day1"] = "";
        if (Convert.ToInt32(dr["Day2"]) == 0) dr["Day2"] = "";
        if (Convert.ToInt32(dr["Day3"]) == 0) dr["Day3"] = ""; 
        dt.Rows.Add(dr);
        DataGrid1.DataSource = dt;
        DataGrid1.DataBind();
        if (chapID==1)
            DataGrid1.Columns[17].Visible = true  ;
        else
            DataGrid1.Columns[17].Visible = false;
    }
    void AddRow(DataSet ds, DataSet dsContests, DataRow dr, int i, int contestantNo)
    {
        int catID = 0;
        string value = "";
        for (int j = 0; j < dsContests.Tables[0].Rows.Count; j++)
        {
            if (Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1].ToString().Trim()) ==   Convert.ToInt32(dsContests.Tables[0].Rows[j].ItemArray[0].ToString().Trim()))
            {
               // Response.Write("<br>" + dsContests.Tables[0].Rows[j].ItemArray[2].ToString().Trim() + ' '  + (ds.Tables[0].Rows[i].ItemArray[1].ToString().Trim()) + " " + (dsContests.Tables[0].Rows[j].ItemArray[0].ToString().Trim()));
                value = dsContests.Tables[0].Rows[j].ItemArray[2].ToString().Trim();
                if (value != "")
                    if (Convert.ToDateTime(value) == day[0])
                    {
                        dr["Day1"] = Convert.ToInt32(dr["Day1"]) + 1;
                    }
                    else if (Convert.ToDateTime(value) == day[1])
                    {
                        dr["Day2"] = Convert.ToInt32(dr["Day2"]) + 1; ;
                    }
                    else if (Convert.ToDateTime(value) == day[2])
                    {
                        dr["Day3"] = Convert.ToInt32(dr["Day3"]) + 1; ;
                    }
                value = dsContests.Tables[0].Rows[j].ItemArray[3].ToString().Trim();
            }
            if (value != "")
            // catID = Convert.ToInt32(value);
            {
                if (value == "SB")
                {
                    dr["SpellingBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "VB")
                {
                    dr["Vocabulary"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "MB")
                {
                    dr["MathBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "SC")
                {
                    dr["Science"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "GB")
                {
                    dr["Geography"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "EW")
                {
                    dr["EssayWriting"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "PS")
                {
                    dr["PublicSpeaking"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
                if (value == "BB")
                {
                    dr["BrainBee"] = ds.Tables[0].Rows[i].ItemArray[7].ToString().Trim();
                }
            }
        }        
    }
    DataSet GetContests()
    {
        DataSet ds = new DataSet();
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        string commandString = "Select ContestID, ContestCategoryID, ContestDate,ProductGroupCode FROM Contest" +
            " Where Contest_Year=" + Year + " AND NSFChapterID=" + chapID;
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        daContests.Fill(ds);

        // get distinct dates
        DataSet ds1 = new DataSet();
        string commandString1 = "select distinct(c.contestdate) from contest c  where c.contest_year = " + Year + " and c.nsfchapterid = " + chapID + " order by c.contestdate";
           
        SqlDataAdapter daContests1 = new SqlDataAdapter(commandString1, connection);
        daContests1.Fill(ds1);


        string value = "";
        value = ds1.Tables[0].Rows[0].ItemArray[0].ToString().Trim();
        if (value != "")
            day[0] = Convert.ToDateTime(value);
        for (int q = 1; q < ds1.Tables[0].Rows.Count; q++)
        {
            value = ds1.Tables[0].Rows[q].ItemArray[0].ToString().Trim();

            if (value != "")
            {
                int dayValue = Convert.ToDateTime(value).Day;

                if (dayValue != day[0].Day)
                {
                    if (day[1].Year == 1900)
                    {
                        day[1] = Convert.ToDateTime(value);
                    }
                    else if (dayValue != day[1].Day)
                    {
                        day[2] = Convert.ToDateTime(value);
                    }
                }
            }
        }
        return ds;
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        String strChapterName = GetChapterName(chapID);
        strChapterName = strChapterName.Replace(",","");
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=RegistrationMatrix_" + strChapterName + "_" + Year + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error

        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0) ;
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("../VolunteerFunctions.aspx");
    }
}
