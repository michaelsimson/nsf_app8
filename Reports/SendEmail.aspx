<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" ValidateRequest="false" CodeFile="SendEmail.aspx.vb" Inherits="SendEmail" title="Untitled Page" Debug="true" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table  width="100%">
    <tr><td colspan="3"><asp:Label ID="lblDebug" runat="server" Visible="false" ></asp:Label></td></tr>
    <tr>
    <td>
    <asp:Button ID="btnSend" runat="server" Text="Send Email" />
    </td>
    <td  style="width: 60%;" align="left">
    <asp:Button ID="btnIns" runat="server" Text="Insert Checked Contacts"  />
        <asp:Button ID="btnSendParents" runat="server" Text="Send Email to all Parents" /></td>
    <td style="width: 30%;" rowspan="6"><asp:Panel ID="pnlAddress" runat="server" GroupingText="Addresses"  ScrollBars="Vertical">
        <asp:dropdownlist ID="ddlTo" runat="server" AutoPostBack="true">
        <asp:ListItem Selected="true" Text="Parents - Main Contact" Value="PP"></asp:ListItem>
        <asp:ListItem Selected="false" Text="Parents - Both" Value="PB"></asp:ListItem>
        <asp:ListItem Selected="false" Text="Volunteers with Assigned Roles" Value="VR"></asp:ListItem>
        <asp:ListItem Selected="false" Text="Volunteers with Unassigned Roles" Value="VUR"></asp:ListItem>
       </asp:dropdownlist><br /><br />
        <asp:CheckBoxList id="lstAddress" runat="server" autopostback="false"   width="100%" height="100%" CssClass="SmallFont" EnableViewState="true" DataTextField="Email"  DataValueField="AutoMemberID" ></asp:CheckBoxList>
        </asp:Panel>    </td>
    </tr>
    <tr>
    <td> From: </td>
    <td style="width: 60%;"><asp:TextBox ID="txtFrom" runat="server" Width="99%" Text="contests@northsouth.org" ></asp:TextBox></td>
    </tr>
    <tr>
    <td> To: </td>
    <td style="width: 60%;"><asp:TextBox ID="txtTo" runat="server" Width="99%" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    
    <tr>
    <td> Subject: </td>
    <td style="width: 60%;"><asp:TextBox ID="txtSubject" runat="server" Width="99%"></asp:TextBox></td>
   </tr>
    
    <tr >
    <td> File to be Attached: </td>
    <td style="width: 60%;"><asp:FileUpload ID="AttachmentFile" runat="server"/>   </td>
    </tr>
    
    <tr >
    <td style="width: 60%;" colspan="2"><FTB:FreeTextBox id="txtEmailBodtText" runat="Server" Width="99%" /></td>
   </tr>
    <tr >
    <td style="width: 60%;" colspan="2"><asp:Label ID="lblEmailError" runat="server" Width="100%" ></asp:Label></td>
    </tr></table>
</asp:Content>


 

 
 
 