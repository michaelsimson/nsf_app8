﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="coachRegisReport.aspx.cs" Inherits="Reports_xlReportRegByProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
    <center>
        <h2>Coaching Registration</h2>
    </center>

    <%--   <asp:Button ID="btnBack" runat="server"
        Text="Back" OnClick="btnBack_Click"></asp:Button>--%>

    <center>
        <table border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td align="Left">Year
                </td>
                <td></td>
                <td>
                    <asp:DropDownList ID="ddlyear" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left">Semester</td>
                <td></td>
                <td align="left">
                    <asp:DropDownList Width="130px" ID="ddlSemester" runat="server" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Product Name</td>
                <td></td>
                <td>

                    <asp:ListBox ID="ddlprod" runat="server" Width="150px" Height="100px" SelectionMode="Multiple">
                        <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                    </asp:ListBox>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="3">
                    <div style="float: left;">
                        <asp:Button ID="btnsubmit" runat="server" Text="Generate Report" OnClick="btnsubmit_Click" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnSave" runat="server"
                            Text="Save File" OnClick="btnSave_Click"></asp:Button>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblChapter" runat="server" Width="144px"></asp:Label>

                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblError" runat="server" Width="144px" ForeColor="Red"></asp:Label>

                </td>
            </tr>

        </table>
    </center>
    <center>
        &nbsp;</center>
    <div style="width: 1260px; overflow-y: scroll;">
        <asp:DataGrid ID="grdCoaching" runat="server" AutoGenerateColumns="False" HeaderStyle-ForeColor="Green" Width="1250">
            <FooterStyle Wrap="False"></FooterStyle>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#ffffcc" Font-Bold="true"></HeaderStyle>
            <Columns>
                <asp:BoundColumn HeaderText="PaymentDate" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="Green" DataField="PaymentDate" DataFormatString="{0:d}" />
                <asp:BoundColumn HeaderText="Student Name" HeaderStyle-ForeColor="Green" DataField="ChildName" />
                <asp:BoundColumn HeaderText="Grade" HeaderStyle-ForeColor="Green" DataField="Grade" />
                <asp:BoundColumn HeaderText="Approved" HeaderStyle-ForeColor="Green" DataField="Approved" />
                <asp:BoundColumn HeaderText="FatherName" HeaderStyle-ForeColor="Green" DataField="FatherName" />
                <asp:BoundColumn HeaderText="Email" HeaderStyle-ForeColor="Green" DataField="Email" />
                <asp:BoundColumn HeaderText="HomePhone" HeaderStyle-ForeColor="Green" DataField="HPhone" />
                <asp:BoundColumn HeaderText="CPhone" HeaderStyle-ForeColor="Green" DataField="CPhone" />
                <asp:BoundColumn HeaderText="Address1" HeaderStyle-ForeColor="Green" DataField="Address1" />
                <asp:BoundColumn HeaderText="City" HeaderStyle-ForeColor="Green" DataField="City" />
                <asp:BoundColumn HeaderText="State" HeaderStyle-ForeColor="Green" DataField="State" />
                <asp:BoundColumn HeaderText="Zip" HeaderStyle-ForeColor="Green" DataField="Zip" />
                <asp:BoundColumn HeaderText="Chapter" HeaderStyle-ForeColor="Green" DataField="Chapter" />
                <asp:BoundColumn HeaderText="Product Name" HeaderStyle-ForeColor="Green" DataField="ProductName" />
                <asp:BoundColumn HeaderText="CoachName" HeaderStyle-ForeColor="Green" DataField="CoachName" />
                <asp:BoundColumn HeaderText="Level" HeaderStyle-ForeColor="Green" DataField="Level" />
                <asp:BoundColumn HeaderText="Semester" HeaderStyle-ForeColor="Green" DataField="Semester" />
                <asp:BoundColumn HeaderText="Session" HeaderStyle-ForeColor="Green" DataField="SessionNo" />
                 <asp:BoundColumn HeaderText="Type" HeaderStyle-ForeColor="Green" DataField="Type" />
                <asp:BoundColumn HeaderText="MaxCapacity" HeaderStyle-ForeColor="Green" DataField="MaxCapacity" />
                <asp:BoundColumn HeaderText="CoachDay" HeaderStyle-ForeColor="Green" DataField="Day" />
                <asp:BoundColumn HeaderText="Time" HeaderStyle-ForeColor="Green" DataField="Time" />
                <asp:BoundColumn HeaderText="PaymentReference" HeaderStyle-ForeColor="Green" DataField="PaymentReference" />
                <asp:BoundColumn HeaderText="Status" HeaderStyle-ForeColor="Green" DataField="Status" />
                <asp:BoundColumn HeaderText="ID" HeaderStyle-ForeColor="Green" DataField="ChildNumber" />
                <asp:BoundColumn HeaderText="MemberID" HeaderStyle-ForeColor="Green" DataField="MemberID" />
                <asp:BoundColumn HeaderText="CoachRegID" HeaderStyle-ForeColor="Green" DataField="CoachRegID" />
                <asp:BoundColumn HeaderText="SignUpID" HeaderStyle-ForeColor="Green" DataField="SignUpID" />

            </Columns>
            <PagerStyle Wrap="False"></PagerStyle>
        </asp:DataGrid>
    </div>
    <br />
    <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
    <table border="0" cellpadding="2" cellspacing="0" style="display: none;">
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
            </td>
            <td width="10px"></td>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="False">[Logout]</asp:HyperLink>
            </td>
        </tr>
    </table>





</asp:Content>
