Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports Microsoft.ApplicationBlocks.Data
Imports NativeExcel


Partial Class Reports_ReimbReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        spnMailStatus.InnerText = ""
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadchapter()
            If Session("RoleId").ToString() = "5" Then
                Dim i As Integer
                'dgExpense.Columns(1).Visible = False
                For i = 8 To 18
                    dgExpense.Columns(i).Visible = False
                Next
                DGRevenue.Columns(11).Visible = False
                DGRevenue.Columns(12).Visible = False
                For i = 15 To 17
                    DGRevenue.Columns(i).Visible = False
                Next
            End If
            If Len(Session("LoginChapterID")) > 0 And Val(Session("RoleId")) > 5 Then
                ddlChapter.SelectedValue = Session("LoginChapterID")
                ddlChapter.Enabled = False
                getreportdate()
            End If
            If ddlSelectType.SelectedValue = "1" Then
                trreportdate.Visible = False

            End If
            ddlSelectType.Items.Clear()
            Dim cmdText As String = String.Empty
            Dim ds As DataSet = New DataSet()
            cmdText = "select RoleID, ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and ( (RoleID=38 and [National]='Y') or (RoleID=37 and [National]='Y' ))"
            Dim isChaperExists As String = String.Empty

            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    isChaperExists = "1"
                Else
                    isChaperExists = ""
                End If
            End If
            Dim roleID As String = Session("RoleID").ToString()
            If roleID = "1" Or roleID = "2" Or isChaperExists = "1" Then
                ddlSelectType.Items.Insert(0, New ListItem("Chapter List", "1"))
                ddlSelectType.Items.Insert(1, New ListItem("Reimbursement Report", "2"))
            Else
                ddlSelectType.Items.Insert(0, New ListItem("Payment Status", "1"))
                ddlSelectType.Items.Insert(1, New ListItem("Reimbursement Report", "2"))

            End If
        End If
    End Sub
    Private Sub loadchapter()
        Dim dsEvents As New DataSet
        Dim liNull As New ListItem("Select A Chapter", "0")
        Dim strSQl As String = "select chapterId, Name, chapterCode from chapter order by state,chapterCode"
        Try
            dsEvents = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        Catch se As SqlException
            lblerr.Text = se.ToString()
            Return
        End Try
        If dsEvents.Tables.Count > 0 Then
            ddlChapter.Items.Clear()
            ddlChapter.DataSource = dsEvents.Tables(0)
            ddlChapter.DataTextField = dsEvents.Tables(0).Columns("chapterCode").ToString
            ddlChapter.DataValueField = dsEvents.Tables(0).Columns("ChapterId").ToString
            ddlChapter.DataBind()
            ddlChapter.Items.Insert(0, liNull)
            If (Session("RoleId").ToString() = "5") Then
                ddlChapter.Items.FindByValue(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select ChapterID from Volunteer where roleID=5 AND  memberid=" & Session("LoginID") & IIf(Session("SelChapterID") = "", "", " and ChapterID=" & Session("SelChapterID")))).Selected = True
                getreportdate()
                ddlChapter.Enabled = False
            Else
                ddlChapter.Items.FindByValue("0").Selected = True
            End If
        End If
    End Sub

    Private Sub getreportdate()
        Try
            Dim flag As Boolean = False
            Dim StrSql As String = "SELECT DISTINCT CAST(Reportdate AS date) ,CONVERT(VARCHAR(10), CAST(Reportdate AS date), 101) as ReportDate  FROM ExpJournal  WHERE (ChapterID = " & ddlChapter.SelectedValue & " and EventID=" & IIf(ddlChapter.SelectedValue = 1, 1, 2) & ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) GROUP By CAST(Reportdate AS date) Order by CAST(Reportdate AS date) DESC"
            Dim dsReportDate As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSql)
            If dsReportDate.Tables(0).Rows.Count > 0 Then
                lstReports.DataSource = dsReportDate
                lstReports.DataTextField = "ReportDate"
                lstReports.DataValueField = "ReportDate"
                lstReports.DataBind()
                lstReports.Items.Insert(0, New ListItem("Select Date", "-1"))
                lstReports.SelectedIndex = 0
                trreportdate.Visible = True
                btnReport.Enabled = True
                lblerr.Text = ""
            Else
                lblerr.Text = "No report Date for Selected Chapter"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clearlstReport()
        getreportdate()
    End Sub

    Protected Sub ddlSelectType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        spnReImpTitle.InnerText = ddlSelectType.SelectedItem.Text
        If (ddlSelectType.SelectedValue = "1") Then
            'btnImbursed.Visible = True
            'btnNotImbursed.Visible = True
        Else
            btnImbursed.Visible = False
            btnNotImbursed.Visible = False
        End If
    End Sub

    Private Sub clearlstReport()
        lstReports.Items.Clear()
        trreportdate.Visible = False
        btnReport.Enabled = False
        btnRevExport.Visible = False
        btnExport.Visible = False
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlSelectType.SelectedValue = "1" Then
            loadReImNotProcessedgrid(grdReImpNotProcessed, "")
            'loadReImProcessedgrid(grdReimpProcessed, "")
            dvReImpSection.Visible = False
            'dvChapterList.Visible = True
        Else
            dvReImpSection.Visible = True
            'dvChapterList.Visible = False
            loadgrid(dgExpense, "E")
            If dgExpense.Rows.Count > 0 Then
                lblExp.Text = "Expense Details :"
                btnExport.Visible = True
            Else
                btnExport.Visible = False
                lblExp.Text = "No Expense Record found for selected report Date " '& dgExpense.Rows.Count
            End If
            loadgrid(DGRevenue, "R")
            If DGRevenue.Rows.Count > 0 Then
                lblRev.Text = "Revenue Details :"
                btnRevExport.Visible = True
            Else
                btnRevExport.Visible = False
                lblRev.Text = "No Revenue Record found for selected report Date"
            End If
        End If
    End Sub

    Protected Sub dgExpense_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadgrid(dgExpense, "E")
        dgExpense.PageIndex = e.NewPageIndex
        dgExpense.DataBind()
    End Sub
    Protected Sub dgRevenue_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadgrid(DGRevenue, "R")
        DGRevenue.PageIndex = e.NewPageIndex
        DGRevenue.DataBind()
    End Sub

    Function reportdates() As String
        Dim selectedreportdates As String = ""
        Dim i As Integer
        For i = 1 To lstReports.Items.Count - 1
            If lstReports.Items(i).Selected = True Then
                'Response.Write(lstReports.Items(i).Text.ToString() & "<BR>")
                If selectedreportdates = "" Then
                    selectedreportdates = "'" & lstReports.Items(i).Text & "'"
                Else
                    selectedreportdates = selectedreportdates & ",'" & lstReports.Items(i).Text & "'"
                End If
            End If
        Next
        Return selectedreportdates
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=ReimbReportsExpense.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexportE As New GridView
        Try
            If reportdates() <> "" And reportdates() <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String
                If Session("RoleId").ToString() = "5" Then
                    StrSQL = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc ,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,Ev.Name As Event,Ch.ChapterCode as FromChapter, C.Chaptercode as ToChapter,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatCode FROM ExpJournal E INNER JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID INNER JOIN INDSPOUSE R ON R.AutoMemberID=E.ReimbMemberID INNER JOIN Treatment T ON T.TreatID=E.TreatID INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter C ON C.ChapterID = E.ToChapterID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType IN ('ChapterExp','FinalsExp') and E.ChapterID =" & ddlChapter.SelectedItem.Value & " and E.ReportDate in (" & reportdates() & ") order by E.reportdate"
                Else
                    StrSQL = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc ,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,E.ChapterApprover as ChApprover,E.NationalApprover as NatApprover,E.NationalApprovalFlag As NatApproval,E.Paid,E.DatePaid,E.ExpCatID,E.Account,E.ReimbMemberID,Ev.Name as Event,Ch.ChapterCode as FromChapter, C.Chaptercode as ToChapter,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatCode FROM ExpJournal E INNER JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID INNER JOIN INDSPOUSE R ON R.AutoMemberID=E.ReimbMemberID INNER JOIN Treatment T ON T.TreatID=E.TreatID INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter C ON C.ChapterID = E.ToChapterID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType IN ('ChapterExp','FinalsExp') and E.ChapterID =" & ddlChapter.SelectedItem.Value & " and E.ReportDate in (" & reportdates() & ") order by E.reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dgexportE.DataSource = dsExpJrnl
                dgexportE.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            lblerr.Text = reportdates()
            lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try

        dgexportE.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub btnExportRev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=ReimbReportsRevenue.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexportE As New GridView
        Dim reportdate As String = reportdates()
        Try

            If reportdate <> "" And reportdate <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String
                If Session("RoleId").ToString() = "5" Then
                    StrSQL = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" & ddlChapter.SelectedItem.Value & "and E.ReportDate in (" & reportdate & ") order by E.reportdate"
                    'StrSQL = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType='" & type & "' and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & "and ReportDate in (" & reportdate & ") order by reportdate"

                Else
                    StrSQL = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.PaymentMethod,E.CreatedBy,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.ChapterApprover as ChApprover,E.NationalApprovalFlag as NatApproval,E.NationalApprover as NationalApprover,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E  INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" & ddlChapter.SelectedItem.Value & "and E.ReportDate in (" & reportdate & ") order by E.reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dgexportE.DataSource = dsExpJrnl
                dgexportE.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            lblerr.Text = reportdate
            lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
        dgexportE.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub loadgrid(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()
        btnImbursed.Visible = False
        btnNotImbursed.Visible = False
        Dim reportdate As String = reportdates()
        Try
            If reportdate <> "" And reportdate <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String

                If type = "E" Then



                    StrSQL = "SELECT ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatDesc,E.Name as EventName,Ch.Chaptercode as FromChapter, C.Chaptercode as ToChapter, ExpJournal.ExpCatCode, T.TreatCode"
                    StrSQL = StrSQL & " FROM ExpJournal INNER JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID Inner Join IndSpouse R on R.AutoMemberID =ExpJournal.ReimbMemberID "
                    StrSQL = StrSQL & " INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID "
                    StrSQL = StrSQL & " INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID "
                    StrSQL = StrSQL & " INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID "
                    StrSQL = StrSQL & " INNER JOIN Chapter C on C.ChapterID =ExpJournal.ToChapterID "
                    StrSQL = StrSQL & " where ExpJournal.TransType IN ('ChapterExp','FinalsExp') and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                    'StrSQL = "SELECT ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName, T.TreatDesc,E.Name as EventName,Ch.Chaptercode FROM ExpJournal INNER JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType IN ('ChapterExp','FinalsExp') and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                Else
                    StrSQL = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dg.PageIndex = 0
                dg.DataSource = dsExpJrnl
                dg.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            'lblerr.Text = reportdate
            'lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub

    Protected Sub loadReImNotProcessedgrid(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()

        Try

            lblerr.Text = ""
            Dim dsExpJrnl As New DataSet
            Dim tblExpJrnl() As String = {"ExpJrnl"}
            Dim StrSQL As String
            Dim strRMemberIDs As String = String.Empty

            Dim strMemberID As String = String.Empty
            strMemberID = "select distinct ReimbMemberID from ExpJournal where  ToChapterID =" & hdnChapterID.Value & " and ReportDate='" & hdnReportDate.Value & "' and (Paid is null or Paid<>'Y') and TransType<>'Revenue'"
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strMemberID)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        strRMemberIDs &= dr("ReimbMemberID").ToString() & ","
                    Next
                End If
            End If
            strRMemberIDs = strRMemberIDs.TrimEnd(CChar(","))
            If strRMemberIDs = "" Then
                dvReimpNotErrorStatus.Visible = True
                spnReImpNotProcessed.InnerText = "Table 1:  Reimbursement Forms not Processed"
            Else
                dvReimpNotErrorStatus.Visible = False
                StrSQL = "SELECT '' as BankCode,'' as BankID, '' as CheckNumber, ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName,case when ExpJournal.DonorType='OWN' then OI.Organization_Name else R.FirstName + ' ' + R.Lastname end as ReimburseTo, T.TreatDesc,E.Name as EventName,Ch.Chaptercode as FromChapter, C.Chaptercode as ToChapter, ExpJournal.ExpCatCode, T.TreatCode"
                StrSQL = StrSQL & " FROM ExpJournal LEFT JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID left Join IndSpouse R on R.AutoMemberID =ExpJournal.ReimbMemberID "
                StrSQL = StrSQL & " LEFT JOIN Treatment T ON T.TreatID=ExpJournal.TreatID "
                StrSQL = StrSQL & " LEFT JOIN EVENT E ON E.EventID=ExpJournal.EventID "
                StrSQL = StrSQL & " LEFT JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID "
                StrSQL = StrSQL & " LEFT JOIN Chapter C on C.ChapterID =ExpJournal.ToChapterID "
                StrSQL = StrSQL & " left join organizationinfo OI on (ExpJournal.ReimbMemberID=OI.AutoMemberID)"
                StrSQL = StrSQL & " where ExpJournal.TransType <>'Revenue' and ExpJournal.ChapterID =" & hdnChapterID.Value & " and ReportDate in ('" & hdnReportDate.Value & "') and ReimbMemberID in (" & strRMemberIDs & ") order by C.State,C.ChapterCode"

                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dg.PageIndex = 0
                dg.DataSource = dsExpJrnl
                dg.DataBind()

                spnReImpNotProcessed.InnerText = "Table 1:  Reimbursement Forms not Processed"
                btnNotImbursed.Visible = True
                Dim sumAmt As String = String.Empty
                Dim amt As Double

                sumAmt = "select SUM(ExpenseAmount) as Amt from ExpJournal where ChapterID =" & hdnChapterID.Value & " and ReportDate = '" & hdnReportDate.Value & "' and TransType <>'Revenue' and ReimbMemberID in (" & strRMemberIDs & ")"
                ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, sumAmt)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        amt = Convert.ToDouble(ds.Tables(0).Rows(0)("Amt").ToString())
                        amt = Math.Round(amt, 2)
                        sumAmt = ds.Tables(0).Rows(0)("Amt").ToString()
                        spnAmount.InnerText = "Total amount of the check: " & String.Format("{0:F2}", amt) & ""
                    End If
                End If
            End If

        Catch ex As Exception
            ' lblerr.Text = reportdate
            'lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub

    Protected Sub loadReImProcessedgrid(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()

        Try

            lblerr.Text = ""
            Dim dsExpJrnl As New DataSet
            Dim tblExpJrnl() As String = {"ExpJrnl"}
            Dim StrSQL As String


            Dim strRMemberIDs As String = String.Empty
            Dim ds As DataSet = New DataSet()
            'Dim strMemberID As String = String.Empty
            'strMemberID = "select distinct ReimbMemberID from ExpJournal where  ChapterID =" & hdnChapterID.Value & "  and ReportDate ='" & hdnReportDate.Value & "' and Paid='Y' and TransType<>'Revenue' and ReImbMemberID is not null and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ToChapterID =" & hdnChapterID.Value & "  and ReportDate ='" & hdnReportDate.Value & "' and (Paid is null or Paid<>'Y') and TransType<>'Revenue')"
            'Dim ds As DataSet = New DataSet()
            'ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strMemberID)
            'If ds.Tables.Count > 0 Then
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        For Each dr As DataRow In ds.Tables(0).Rows
            '            strRMemberIDs &= dr("ReimbMemberID").ToString() & ","
            '        Next
            '    End If
            'End If
            strRMemberIDs = hdnReimbMemberID.Value
            If strRMemberIDs = "" Then
                strRMemberIDs = "is null"
            Else
                strRMemberIDs = "=" & hdnReimbMemberID.Value
            End If
            'dvReImpErrStatus.Visible = False
            StrSQL = "SELECT B.BankID,B.BankCode,ExpJournal.CheckNumber,ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName,case when ExpJournal.DonorType='OWN' then OI.Organization_Name else R.FirstName + ' ' + R.Lastname end as ReimburseTo, T.TreatDesc,E.Name as EventName,Ch.Chaptercode as FromChapter, C.Chaptercode as ToChapter, ExpJournal.ExpCatCode, T.TreatCode"
            StrSQL = StrSQL & " FROM ExpJournal LEFT JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID left Join IndSpouse R on R.AutoMemberID =ExpJournal.ReimbMemberID "
            StrSQL = StrSQL & " LEFT JOIN Treatment T ON T.TreatID=ExpJournal.TreatID "
            StrSQL = StrSQL & " LEFT JOIN EVENT E ON E.EventID=ExpJournal.EventID "
            StrSQL = StrSQL & " LEFT JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID "
            StrSQL = StrSQL & " LEFT JOIN Chapter C on C.ChapterID =ExpJournal.ToChapterID "
            StrSQL = StrSQL & " left join organizationinfo OI on (ExpJournal.ReimbMemberID=OI.AutoMemberID)"
            StrSQL = StrSQL & "left join Bank B on (B.BankID=ExpJournal.BankID)"

            StrSQL = StrSQL & " where ExpJournal.TransType <>'Revenue' and ExpJournal.ChapterID =" & hdnChapterID.Value & " and ReportDate = '" & hdnReportDate.Value & "' and ReimbMemberID " & strRMemberIDs & " and B.BankID=" & hdnBankID.Value & " and ExpJournal.CheckNumber=" & hdnCheckNumber.Value & "  order by C.State,C.ChapterCode"

            SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
            dg.PageIndex = 0
            dg.DataSource = dsExpJrnl
            dg.DataBind()
            'spnReImChapter.InnerText = "Chapter: " & ddlChapter.SelectedItem.Text

            'spnReImReportDate.InnerText = "Report Date: " & lstReports.SelectedItem.Text
            'spnReImpProcessed.InnerText = "Table 2:  Reimbursement Forms Processed in the last 15 months"
            btnImbursed.Visible = True

            Dim sumAmt As String = String.Empty
            Dim amt As Double

            sumAmt = "select SUM(ExpenseAmount) as Amt from ExpJournal where ChapterID =" & hdnChapterID.Value & " and ReportDate = '" & hdnReportDate.Value & "' and TransType <>'Revenue' and BankID=" & hdnBankID.Value & " and CheckNumber=" & hdnCheckNumber.Value & ""
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, sumAmt)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    amt = Convert.ToDouble(ds.Tables(0).Rows(0)("Amt").ToString())
                    amt = Math.Round(amt, 2)
                    sumAmt = ds.Tables(0).Rows(0)("Amt").ToString()
                    spnAmount.InnerText = "Total amount of the check: " & String.Format("{0:n}", amt) & ""
                End If
            End If




        Catch ex As Exception
            ' lblerr.Text = reportdate
            ' lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub


    Protected Sub btnNotImbursed_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            ExportExcel()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnImbursed_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            ExportExcelReImbursed()
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub loadReImPChapterList(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()

        Try
            Dim RoleID As String = Session("RoleID").ToString()
            lblerr.Text = ""
            Dim dsExpJrnl As New DataSet
            Dim tblExpJrnl() As String = {"ExpJrnl"}
            Dim StrSQL As String

            Dim ds As New DataSet()
            Dim cmdText As String = String.Empty
            cmdText = "select RoleID, ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and ( (RoleID=38 and [National]='Y') or (RoleID=37 and [National]='Y' ))"
            Dim isChaperExists As String = String.Empty
            Dim chapterID As String = String.Empty

            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    isChaperExists = "1"
                Else
                    isChaperExists = ""
                End If
            End If


            'StrSQL = "select distinct Ex.ChapterID, ReportDate,C.ChapterCOde,C.State,V.EventYear,V.MemberID,IP.EMail,IP.HPhone,IP.CPhone from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue')) and ReportDate >= DATEADD(month,-15,GetDate()) order by  ReportDate Desc, C.State, C.ChapterCode ASC"


            StrSQL = "select distinct Ex.ChapterID,Ex.ReimbMemberID,Ex.DonorType, ReportDate,C.ChapterCOde,C.State,Ex.BankID,B.BankCode,Ex.CheckNumber,(select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')) as reimbCount,"
            StrSQL &= "case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.EMAIL when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.Email when ( select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Ex.ChapterID =108 OR Ex.ChapterID=108))>1 then IP.EMail  when Ex.DonorType='OWN' then OI.Email else IP1.EMail end as email,"

            StrSQL &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.PHONE when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.HPhone when ( select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.HPhone when Ex.DonorType='OWN' then OI.PHONE else IP1.HPhone end as HPhone, "

            StrSQL &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then '' when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.CPhone when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.CPhone when Ex.DonorType='OWN' then '' else IP1.CPhone end as CPhone, "

            StrSQL &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.ORGANIZATION_NAME when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.FirstName+ ' ' +IP1.LastName when (select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.FirstName +' '+IP.LastName when Ex.DonorType='OWN' then OI.ORGANIZATION_NAME else IP1.FirstName +' ' +Ip1.LastName end as Name "

            StrSQL &= "  from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) left join IndSpouse IP1 on (Ex.ReimbMemberID=IP1.AutoMemberID) left join Bank B on B.BankID=Ex.BankID left join OrganizationInfo OI on OI.AutoMemberID=Ex.ReimbMemberID where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal  where Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue'  and ReportDate >= DATEADD(month,-15,GetDate()))) and ReportDate >= DATEADD(month,-15,GetDate()) "

            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                StrSQL &= "  or (ReimbMemberID is null and Paid='Y' and TransType<>'Revenue'  and ReportDate >= DATEADD(month,-15,GetDate()))"
            Else
                cmdText = "select ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and (RoleID=5 or (RoleID=37 and ChapterID is not null) or (RoleID=38 and ChapterID is not null))"
                ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            chapterID &= dr("ChapterID").ToString() & ","
                        Next

                    End If
                End If
                chapterID = chapterID.TrimEnd(CChar(","))
                StrSQL &= " and Ex.ChapterID in(" & chapterID & ")"
            End If

            StrSQL &= " order by  ReportDate Desc, Ex.BankID,Ex.CheckNumber"
            'order by C.State,C.ChapterCode ASC, ReportDate Desc
            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                Else
                    spnReimbResult.InnerText = "You don�t have access to this report."
                End If
            End If

            dg.PageIndex = 0
            dg.DataSource = dsExpJrnl
            dg.DataBind()
            If dsExpJrnl.Tables(0).Rows.Count > 0 Then
                btnImbursed.Visible = True
            Else
                spnReimbResult.InnerText = "No record found"
                btnImbursed.Visible = False
            End If


            spnImpChapterList.InnerText = "Table 2:  Reimbursement Forms Processed in the last 15 months"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub loadReImPNotChapterList(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()

        Try
            Dim RoleID As String = Session("RoleID").ToString()
            lblerr.Text = ""
            Dim dsExpJrnl As New DataSet
            Dim tblExpJrnl() As String = {"ExpJrnl"}
            Dim StrSQL As String

            Dim ds As New DataSet()
            Dim cmdText As String = String.Empty
            cmdText = "select RoleID, ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and ( (RoleID=38 and [National]='Y') or (RoleID=37 and [National]='Y' ))"
            Dim isChaperExists As String = String.Empty
            Dim chapterID As String = String.Empty

            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    isChaperExists = "1"
                Else
                    isChaperExists = ""
                End If
            End If

            'StrSQL = "select distinct Ex.ChapterID, ReportDate,C.ChapterCOde,C.State,V.EventYear,V.MemberID,IP.EMail,IP.HPhone,IP.CPhone from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID)  where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Paid is null or Paid<>'Y') order by ReportDate Desc, C.State, C.ChapterCode ASC"



            StrSQL = "select distinct Ex.ChapterID,Ex.EventID, ReportDate,C.ChapterCOde,C.State,(  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue') as count, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.EMail when Ex.DonorType='OWN' then OI.Email else IP1.EMail end as email, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.HPhone when Ex.DonorType='OWN' then OI.Phone else IP1.HPhone end as HPhone, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.CPhone when Ex.DonorType='OWN' then '' else IP1.CPhone end as CPhone, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.FirstName +' '+IP.LastName when Ex.DonorType='OWN' then OI.ORGANIZATION_NAME else IP1.FirstName +' ' +Ip1.LastName end as Name,  case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.AutoMemberID when Ex.DonorType='OWN' then OI.AutoMemberID else IP1.AutoMemberID  end as AutoMemberID  from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) left join IndSpouse IP1 on (Ex.ReimbMemberID=IP1.AutoMemberID) left join OrganizationInfo OI on OI.AutoMemberID=Ex.ReimbMemberID "
            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                spnNotImpChapterList.InnerText = "Table 1:  Reimbursement Forms not Processed"
                StrSQL &= " where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Paid is null or Paid<>'Y')"
            Else
                spnNotImpChapterList.InnerText = "Table 1:  Reimbursement Forms not Processed (for expense reports filed in the last 15 months)"
                StrSQL &= " where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue' and ReportDate >= DATEADD(month,-15,GetDate())) and (Paid is null or Paid<>'Y') and ReportDate >= DATEADD(month,-15,GetDate())"

                cmdText = "select ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and (RoleID=5 or (RoleID=37 and ChapterID is not null) or (RoleID=38 and ChapterID is not null))"
                ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            chapterID &= dr("ChapterID").ToString() & ","
                        Next

                    End If
                End If
                chapterID = chapterID.TrimEnd(CChar(","))
                StrSQL &= " and Ex.ChapterID in(" & chapterID & ") "
            End If
            StrSQL &= " order by ReportDate Desc, C.State, C.ChapterCode ASC"


            'order by C.State,C.ChapterCode ASC, ReportDate Desc
            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)

                Else
                    spnResult.InnerText = "You don�t have access to this report"
                End If

            End If

            dg.PageIndex = 0

            dg.DataSource = dsExpJrnl
            dg.DataBind()
            If dsExpJrnl.Tables(0).Rows.Count > 0 Then
                btnNotImbursed.Visible = True
            Else
                spnResult.InnerText = "No record found"
                btnNotImbursed.Visible = False
            End If




        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs)
        If ddlSelectType.SelectedValue = "1" Then


            loadReImPChapterList(grdImpChapterList, "")
            loadReImPNotChapterList(grdNotImpChapterList, "")
            dvGrdChapterListSection.Visible = True
        Else
            trReimburse.Visible = True
            dvGrdChapterListSection.Visible = False
            trChapter.Visible = True
            trButtons.Visible = True
            If ddlChapter.SelectedValue <> "0" Then
                trreportdate.Visible = True
            Else
                trreportdate.Visible = False
            End If


        End If

    End Sub
    Protected Sub grdNotImpChapterList_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If e.CommandName = "Select" Then
            Dim r As Integer
            For r = 0 To grdNotImpChapterList.Rows.Count - 1
                grdNotImpChapterList.Rows(r).BackColor = Nothing
            Next

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex

            Try

                Dim chapter As Integer = DirectCast(gvRow.FindControl("lblChapterID"), Label).Text

                Dim reportDate As String = DirectCast(gvRow.FindControl("lblReportDate"), Label).Text
                hdnChapterID.Value = chapter
                hdnReportDate.Value = reportDate
                loadReImNotProcessedgrid(grdReImpNotProcessed, "")
                grdReImpNotProcessed.Columns(12).Visible = False
                grdReImpNotProcessed.Columns(13).Visible = False
                grdReImpNotProcessed.Columns(14).Visible = False
                spnReImpNotProcessed.InnerText = "Table 1A"
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

            Catch ex As Exception
            End Try
        ElseIf e.CommandName = "Send" Then
            Dim r As Integer
            For r = 0 To grdNotImpChapterList.Rows.Count - 1
                grdNotImpChapterList.Rows(r).BackColor = Nothing
            Next

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex

            Try

                Dim chapter As Integer = DirectCast(gvRow.FindControl("lblChapter"), Label).Text

                Dim reportDate As String = DirectCast(gvRow.FindControl("lblRptDate"), Label).Text
                hdnChapterID.Value = chapter
                hdnReportDate.Value = reportDate
                Dim email As String = DirectCast(gvRow.FindControl("lblEmail"), Label).Text.Trim()
                Dim emailIDs As String = DirectCast(gvRow.FindControl("lblEmail"), Label).Text
                Dim chapterCode As String = DirectCast(gvRow.FindControl("lblChapterCode"), Label).Text
                Dim name As String = DirectCast(gvRow.FindControl("lblName"), Label).Text
                Dim toReimbMemberID As String = DirectCast(gvRow.FindControl("lblMemebrID"), Label).Text
                Dim eventID As String = DirectCast(gvRow.FindControl("lblEventID"), Label).Text
                Dim strSql As String = String.Empty
                strSql = "select distinct case when Ex.DonorType='OWN' then OI.EMAIL else IP.Email end as Email,case when Ex.DonorType='OWN' then OI.ORGANIZATION_NAME else IP.FirstName+' '+IP.LastName end as Name,Ex.ReportDate from ExpJournal Ex left join IndSpouse IP on (Ex.ReimbMemberID=Ip.AutoMemberID) left join OrganizationInfo OI on (Ex.ReimbMemberID=OI.AutoMemberID) where Ex.ChapterID=" & chapter & " and Ex.ReportDate='" & reportDate & "'"
                Dim ds As DataSet = New DataSet()
                'ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strSql)
                'If ds.Tables.Count > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                'For Each dr As DataRow In ds.Tables(0).Rows
                'email = dr("Email").ToString().Trim()
                'emailIDs &= dr("Email").ToString() & ";"
                Dim tblHtml As String = String.Empty
                tblHtml &= "<table>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"
                tblHtml &= "<span>Dear " & name & ",</span> </br>"
                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"

                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"
                tblHtml &= "<span>Thank you for filing expense report dated  <b>" & reportDate & "</b> for <b>" & chapterCode & "</b>. Please send receipt(s) so that we can mail you the reimbursement check.  To refresh your memory, please login to the NSF Website and click on Reimbursement Form to see what receipts you need to send for the report date mentioned above.  If you have any questions, please send an email to Thillai Rajendran at thillajen@yahoo.com</span> </br>"
                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"

                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"
                tblHtml &= "<span>Thank you for your support.</span> </br>"
                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"

                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "<tr>"
                tblHtml &= "<td>"
                tblHtml &= "<span>NSF Accounting </span> </br>"
                tblHtml &= "</td>"
                tblHtml &= "</tr>"
                tblHtml &= "</table>"

                Dim strSubject As String = String.Empty
                strSubject = "Thank you for filing expense report dated " & reportDate & ""
                ' email = "michael.simson@capestart.com"
                sentemail(strSubject, tblHtml, email)

                'Next
                spnMailStatus.InnerText = "Email has been sent to " & emailIDs & ""
                'End If
                'End If
                strSql = "insert into ReimbSentEmailLog(EventID,ChapterID,ChapterCode,LoginEmail,FromEmail,ToEmail,EmailSubject,RoleID,ToEmailMemberID,StartTime,Endtime,CreatedBy) values ('" & eventID & "'," & chapter & ", '" & chapterCode & "','" & Session("LoginEmail").ToString() & "','nsfcontests@gmail.com','" & emailIDs & "', 'Thank you for filing expense report dated " & reportDate & "', " & Session("RoleID").ToString() & ",'" & toReimbMemberID & "', GetDate(),GetDate()," & Session("LoginID").ToString() & ")"
                SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            Catch ex As Exception
            End Try
        End If
    End Sub
    Protected Sub grdImpChapterList_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If e.CommandName = "Select" Then
            Dim r As Integer
            For r = 0 To grdImpChapterList.Rows.Count - 1
                grdImpChapterList.Rows(r).BackColor = Nothing
            Next

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex

            Try
                grdReImpNotProcessed.Columns(12).Visible = True
                grdReImpNotProcessed.Columns(13).Visible = True
                grdReImpNotProcessed.Columns(14).Visible = True
                Dim chapter As Integer = DirectCast(gvRow.FindControl("lblChapterID"), Label).Text

                Dim reportDate As String = DirectCast(gvRow.FindControl("lblReportDate"), Label).Text
                Dim bankID As String = DirectCast(gvRow.FindControl("lblBankID"), Label).Text
                Dim checkNumber As String = DirectCast(gvRow.FindControl("lblCheckNumber"), Label).Text
                Dim reimbMemberID As String = DirectCast(gvRow.FindControl("lblReimbMemberID"), Label).Text
                hdnChapterID.Value = chapter
                hdnReportDate.Value = reportDate
                hdnBankID.Value = bankID
                hdnCheckNumber.Value = checkNumber
                hdnReimbMemberID.Value = reimbMemberID
                loadReImProcessedgrid(grdReImpNotProcessed, "")
                spnReImpNotProcessed.InnerText = "Table 2A"

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

            Catch ex As Exception
            End Try
        End If
    End Sub


    Public Sub ExportExcel()
        Try

            Dim cmdText As String = String.Empty
            Dim ds As DataSet = Nothing
            Dim RoleID As String = Session("RoleID").ToString()

            cmdText = "select RoleID, ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and ( (RoleID=38 and [National]='Y') or (RoleID=37 and [National]='Y' ))"
            Dim isChaperExists As String = String.Empty
            Dim chapterID As String = String.Empty

            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    isChaperExists = "1"
                Else
                    isChaperExists = ""
                End If
            End If

            'StrSQL = "select distinct Ex.ChapterID, ReportDate,C.ChapterCOde,C.State,V.EventYear,V.MemberID,IP.EMail,IP.HPhone,IP.CPhone from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID)  where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Paid is null or Paid<>'Y') order by ReportDate Desc, C.State, C.ChapterCode ASC"


            Dim sheetName As String = String.Empty
            cmdText = "select distinct Ex.ChapterID,Ex.EventID, ReportDate,C.ChapterCOde,C.State,(  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue') as count, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.EMail when Ex.DonorType='OWN' then OI.Email else IP1.EMail end as email, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.HPhone when Ex.DonorType='OWN' then OI.Phone else IP1.HPhone end as HPhone, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.CPhone when Ex.DonorType='OWN' then '' else IP1.CPhone end as CPhone, case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.FirstName +' '+IP.LastName when Ex.DonorType='OWN' then OI.ORGANIZATION_NAME else IP1.FirstName +' ' +Ip1.LastName end as Name,  case when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID and ReportDate=Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')>1 then IP.AutoMemberID when Ex.DonorType='OWN' then OI.AutoMemberID else IP1.AutoMemberID  end as AutoMemberID  from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) left join IndSpouse IP1 on (Ex.ReimbMemberID=IP1.AutoMemberID) left join OrganizationInfo OI on OI.AutoMemberID=Ex.ReimbMemberID where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Paid is null or Paid<>'Y')"
            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                sheetName = "Reimb Forms not Processed"
                cmdText &= " where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Paid is null or Paid<>'Y')"
            Else
                sheetName = "Reimb Forms not Processed (for expense reports filed in the last 15 months)"
                cmdText &= " where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue' and ReportDate >= DATEADD(month,-15,GetDate())) and (Paid is null or Paid<>'Y') and ReportDate >= DATEADD(month,-15,GetDate())"
                Dim cmdTextChap As String = "select ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and (RoleID=5 or (RoleID=37 and ChapterID is not null) or (RoleID=38 and ChapterID is not null))"
                ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdTextChap)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            chapterID &= dr("ChapterID").ToString() & ","
                        Next

                    End If
                End If
                chapterID = chapterID.TrimEnd(CChar(","))
                cmdText &= " and Ex.ChapterID in(" & chapterID & ")"
            End If
            cmdText &= " order by ReportDate Desc, C.State, C.ChapterCode ASC"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                    Dim oSheet As IWorksheet = Nothing
                    Dim oSheet1 As IWorksheet = Nothing
                    oSheet = oWorkbooks.Worksheets.Add()

                    oSheet.Name = "Reimbursement Forms not Processed"

                    oSheet.Range("A1:Q1").MergeCells = True
                    oSheet.Range("A1").Value = sheetName
                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A1").Font.Color = Color.Black


                    oSheet.Range("A3").Value = "Ser#"
                    oSheet.Range("A3").Font.Bold = True

                    oSheet.Range("B3").Value = "Chapter Name"
                    oSheet.Range("B3").Font.Bold = True

                    oSheet.Range("C3").Value = "Report Date"
                    oSheet.Range("C3").Font.Bold = True

                    oSheet.Range("D3").Value = "Email"
                    oSheet.Range("D3").Font.Bold = True

                    oSheet.Range("E3").Value = "Home Phone"
                    oSheet.Range("E3").Font.Bold = True

                    oSheet.Range("F3").Value = "Cell Phone"
                    oSheet.Range("F3").Font.Bold = True

                    oSheet.Range("G3").Value = "Name"
                    oSheet.Range("G3").Font.Bold = True

                    Dim iRowIndex As Integer = 4
                    Dim CRange As IRange = Nothing
                    Dim i As Integer = 0
                    For Each dr As DataRow In ds.Tables(0).Rows

                        CRange = oSheet.Range("A" + iRowIndex.ToString())
                        CRange.Value = i + 1

                        CRange = oSheet.Range("B" + iRowIndex.ToString())
                        CRange.Value = dr("ChapterCode")

                        CRange = oSheet.Range("C" + iRowIndex.ToString())
                        CRange.Value = dr("ReportDate")

                        CRange = oSheet.Range("D" + iRowIndex.ToString())
                        CRange.Value = dr("Email")

                        CRange = oSheet.Range("E" + iRowIndex.ToString())
                        CRange.Value = dr("HPhone")

                        CRange = oSheet.Range("F" + iRowIndex.ToString())
                        CRange.Value = dr("CPhone")

                        CRange = oSheet.Range("G" + iRowIndex.ToString())
                        CRange.Value = dr("Name")

                        iRowIndex = iRowIndex + 1
                        i = i + 1
                    Next


                    Dim dt As DateTime = DateTime.Now
                    Dim month As String = dt.ToString("MMM")
                    Dim day As String = dt.ToString("dd")
                    Dim year As String = dt.ToString("yyyy")
                    Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
                    Dim FileName__1 As String = "ReimbursementFormsnotProcessed"


                    Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & year) + ".xls"

                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
                    oWorkbooks.SaveAs(Response.OutputStream)
                    Response.[End]()
                End If

            End If
        Catch
        End Try
    End Sub

    Public Sub ExportExcelReImbursed()
        Try

            Dim cmdText As String = String.Empty
            Dim ds As DataSet = Nothing
            Dim RoleID As String = Session("RoleID").ToString()




            'Dim ds As New DataSet()
            'Dim cmdText As String = String.Empty
            cmdText = "select RoleID, ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and ( (RoleID=38 and [National]='Y') or (RoleID=37 and [National]='Y' ))"
            Dim isChaperExists As String = String.Empty
            Dim chapterID As String = String.Empty

            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    isChaperExists = "1"
                Else
                    isChaperExists = ""
                End If
            End If


            'StrSQL = "select distinct Ex.ChapterID, ReportDate,C.ChapterCOde,C.State,V.EventYear,V.MemberID,IP.EMail,IP.HPhone,IP.CPhone from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal where Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue')) and ReportDate >= DATEADD(month,-15,GetDate()) order by  ReportDate Desc, C.State, C.ChapterCode ASC"


            cmdText = "select distinct Ex.ChapterID,Ex.ReimbMemberID,Ex.DonorType, ReportDate,C.ChapterCOde,C.State,Ex.BankID,B.BankCode,Ex.CheckNumber,(select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue')) as reimbCount,"
            cmdText &= "case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.EMAIL when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.Email when ( select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue') and (Ex.ChapterID =108 OR Ex.ChapterID=108))>1 then IP.EMail  when Ex.DonorType='OWN' then OI.Email else IP1.EMail end as email,"

            cmdText &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.PHONE when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.HPhone when ( select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.HPhone when Ex.DonorType='OWN' then OI.PHONE else IP1.HPhone end as HPhone, "

            cmdText &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then '' when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.CPhone when (  select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.CPhone when Ex.DonorType='OWN' then '' else IP1.CPhone end as CPhone, "

            cmdText &= " case when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType='OWN' then OI.ORGANIZATION_NAME when (Ex.ChapterID=108 or Ex.ChapterID=109) and Ex.DonorType<>'OWN' then IP1.FirstName+ ' ' +IP1.LastName when (select count(distinct ReimbMemberID) from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where  ChapterID =C.ChapterID  and ReportDate =Ex.ReportDate and (Paid is null or Paid<>'Y') and TransType<>'Revenue'))>1 then IP.FirstName +' '+IP.LastName when Ex.DonorType='OWN' then OI.ORGANIZATION_NAME else IP1.FirstName +' ' +Ip1.LastName end as Name "

            cmdText &= "  from ExpJournal Ex inner join Chapter C on Ex.ChapterID=C.ChapterID left join Volunteer V on (V.ChapterID=Ex.ChapterID and V.RoleCode='ChapterC') left join Indspouse IP on (IP.AutoMemberID=V.MemberID) left join IndSpouse IP1 on (Ex.ReimbMemberID=IP1.AutoMemberID) left join Bank B on B.BankID=Ex.BankID left join OrganizationInfo OI on OI.AutoMemberID=Ex.ReimbMemberID where ReimbMemberID in (select distinct ReimbMemberID from ExpJournal  where Paid='Y' and TransType<>'Revenue' and ReimbMemberID not in(select distinct ReimbMemberID from ExpJournal where (Paid is null or Paid<>'Y') and TransType<>'Revenue'  and ReportDate >= DATEADD(month,-15,GetDate()))) and ReportDate >= DATEADD(month,-15,GetDate()) "

            If (RoleID = "1" Or RoleID = "2" Or isChaperExists = "1") Then
                cmdText &= "  or (ReimbMemberID is null and Paid='Y' and TransType<>'Revenue'  and ReportDate >= DATEADD(month,-15,GetDate()))"
            Else
                Dim cmdTextChap As String = "select ChapterID from Volunteer where MemberID=" & Session("LoginID").ToString() & " and (RoleID=5 or (RoleID=37 and ChapterID is not null) or (RoleID=38 and ChapterID is not null))"
                ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdTextChap)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            chapterID &= dr("ChapterID").ToString() & ","
                        Next

                    End If
                End If
                chapterID = chapterID.TrimEnd(CChar(","))
                cmdText &= " and Ex.ChapterID in(" & chapterID & ")"
            End If

            cmdText &= " order by  ReportDate Desc, Ex.BankID,Ex.CheckNumber"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                    Dim oSheet As IWorksheet = Nothing
                    Dim oSheet1 As IWorksheet = Nothing
                    oSheet = oWorkbooks.Worksheets.Add()

                    oSheet.Name = "Reimbursement Forms Processed"

                    oSheet.Range("A1:Q1").MergeCells = True
                    oSheet.Range("A1").Value = "Reimbursement Forms Processed in the last 15 months"

                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A1").Font.Color = Color.Black


                    oSheet.Range("A3").Value = "Ser#"
                    oSheet.Range("A3").Font.Bold = True

                    oSheet.Range("B3").Value = "Chapter Name"
                    oSheet.Range("B3").Font.Bold = True

                    oSheet.Range("C3").Value = "Report Date"
                    oSheet.Range("C3").Font.Bold = True

                    oSheet.Range("D3").Value = "Bank ID"
                    oSheet.Range("D3").Font.Bold = True

                    oSheet.Range("E3").Value = "Bank Name"
                    oSheet.Range("E3").Font.Bold = True

                    oSheet.Range("F3").Value = "Check Number"
                    oSheet.Range("F3").Font.Bold = True

                    oSheet.Range("G3").Value = "Email"
                    oSheet.Range("G3").Font.Bold = True

                    oSheet.Range("H3").Value = "Home Phone"
                    oSheet.Range("H3").Font.Bold = True

                    oSheet.Range("I3").Value = "Cell Phone"
                    oSheet.Range("I3").Font.Bold = True

                    oSheet.Range("J3").Value = "Name"
                    oSheet.Range("J3").Font.Bold = True


                    Dim iRowIndex As Integer = 4
                    Dim CRange As IRange = Nothing
                    Dim i As Integer = 0
                    For Each dr As DataRow In ds.Tables(0).Rows

                        CRange = oSheet.Range("A" + iRowIndex.ToString())
                        CRange.Value = i + 1

                        CRange = oSheet.Range("B" + iRowIndex.ToString())
                        CRange.Value = dr("ChapterCode")

                        CRange = oSheet.Range("C" + iRowIndex.ToString())
                        CRange.Value = dr("ReportDate")

                        CRange = oSheet.Range("D" + iRowIndex.ToString())
                        CRange.Value = dr("BankID")

                        CRange = oSheet.Range("E" + iRowIndex.ToString())
                        CRange.Value = dr("BankCode")

                        CRange = oSheet.Range("F" + iRowIndex.ToString())
                        CRange.Value = dr("CheckNumber")

                        CRange = oSheet.Range("G" + iRowIndex.ToString())
                        CRange.Value = dr("Email")

                        CRange = oSheet.Range("H" + iRowIndex.ToString())
                        CRange.Value = dr("HPhone")

                        CRange = oSheet.Range("I" + iRowIndex.ToString())
                        CRange.Value = dr("CPhone")

                        CRange = oSheet.Range("J" + iRowIndex.ToString())
                        CRange.Value = dr("Name")

                        iRowIndex = iRowIndex + 1
                        i = i + 1
                    Next


                    Dim dt As DateTime = DateTime.Now
                    Dim month As String = dt.ToString("MMM")
                    Dim day As String = dt.ToString("dd")
                    Dim year As String = dt.ToString("yyyy")
                    Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
                    Dim FileName__1 As String = "ReimbursementFormsProcessed"


                    Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & year) + ".xls"

                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
                    oWorkbooks.SaveAs(Response.OutputStream)
                    Response.[End]()
                End If

            End If
        Catch
        End Try
    End Sub
    Protected Sub grdNotImpChapterList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadReImPNotChapterList(grdNotImpChapterList, "")
        grdNotImpChapterList.PageIndex = e.NewPageIndex
        grdNotImpChapterList.DataBind()
    End Sub
    Protected Sub grdImpChapterList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadReImPChapterList(grdImpChapterList, "")
        grdImpChapterList.PageIndex = e.NewPageIndex
        grdImpChapterList.DataBind()
    End Sub

    Private Sub sentemail(sSubject As String, sBody As String, strMailTo As String)
        Try

            Dim mail As New MailMessage()
            mail.From = New MailAddress("nsfcontests@gmail.com")
            mail.Body = sBody
            mail.Subject = sSubject
            mail.CC.Add(New MailAddress("chitturi9@gmail.com"))

            Dim strEmailAddressList As [String]()
            Dim pattern As [String] = "^[a-zA-Z0-9][\w\.-\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim EmailAddressMatch As Match
            strEmailAddressList = strMailTo.Replace(",", ";").Split(";")
            For Each item As Object In strEmailAddressList
                EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern)
                If EmailAddressMatch.Success Then

                    mail.[To].Add(New MailAddress(item.ToString().Trim()))

                End If
            Next

            Dim client As New SmtpClient()
            ' client.Host = host;
            mail.IsBodyHtml = True
            Try
                client.Send(mail)
                'lblError.Text = ex.ToString();
            Catch ex As Exception
            End Try
        Catch
        End Try
    End Sub

End Class
