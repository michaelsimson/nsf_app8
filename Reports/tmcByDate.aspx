<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tmcByDate.aspx.cs" Inherits="Reports_tmcByDate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Medal Count By Date Page</title>
</head>
<body>
    <h3 align="center">List of chapters (by Contest Date)</h3>
		<h4>Please select one</h4>
		<form id="Form1" method="post" runat="server">
			<asp:DataGrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 144px; POSITION: absolute; TOP: 152px"
				runat="server" Height="272px" Width="496px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
				BackColor="White" CellPadding="3" GridLines="Vertical" AutoGenerateColumns="False">
				<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Chapter">
						<ItemTemplate>
							<asp:Label ID="Label1" runat="server" Text='<%# GetChapterName(Convert.ToInt32(DataBinder.Eval(Container, "DataItem.NSFChapterID"))) %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapID") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Contest Date">
						<ItemTemplate>
							<asp:Label ID="Label2" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.ContestDate")).ToShortDateString() %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDate") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Contest">
						<ItemTemplate>
							<asp:Label ID="Label3" runat="server" Text='<%# GetLabels(Convert.ToInt32(DataBinder.Eval(Container, "DataItem.ContestCategoryID"))) %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="TextBox3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestCategoryID") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="NSFChapterID" DataNavigateUrlFormatString="TotalMedalCount.aspx?Chap={0}"
						HeaderText="Get MedalCount" NavigateUrl="TotalMedalCount.aspx"></asp:HyperLinkColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="NSFChapterID" DataNavigateUrlFormatString="xlReport1.aspx?Chap={0}"
						HeaderText="Get Badges" NavigateUrl="xlReport1.aspx"></asp:HyperLinkColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:DataGrid>
			<asp:DropDownList id="ddlWeek" style="Z-INDEX: 102; LEFT: 168px; POSITION: absolute; TOP: 56px" runat="server"
				AutoPostBack="True" onselectedindexchanged="ddlWeek_SelectedIndexChanged">
				<asp:ListItem Value="1">Week 1</asp:ListItem>
				<asp:ListItem Value="2">Week 2</asp:ListItem>
				<asp:ListItem Value="3">Week 3</asp:ListItem>
				<asp:ListItem Value="4">Week 4</asp:ListItem>
				<asp:ListItem Value="5">Week 5</asp:ListItem>
				<asp:ListItem Value="6">Week 6</asp:ListItem>
				<asp:ListItem Value="7">Week 7</asp:ListItem>
				<asp:ListItem Value="8">Week 8</asp:ListItem>
			</asp:DropDownList>
            <asp:Label ID="lblTest" runat="server"></asp:Label>
		</form>
</body>
</html>


 
 
 