﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_xlReportRegPrepByChild : System.Web.UI.Page
{
    int chapID;
    DataSet ds;
    string Year = "";
    DataTable dt;
    DataSet dsVenue;
    DateTime[] day = { DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900"), DateTime.Parse("1/1/1900") };

    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        Year = SqlHelper.ExecuteScalar( System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select max(Eventyear) From registration_prepclub").ToString();
        // System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        lblChapter.Text = GetChapterName(chapID);
        GetData(chapID);
      //  GetVenueData(chapID);
    }
    private void GetData(int chapterid)
    
    {
        //string commandString = "select  distinct(p.ChildNumber),DENSE_RANK() OVER(ORDER BY c.ChildNumber) AS 'Ser#',First_Name+''+Last_Name as Name , " +

        //    " c.First_name,c.Last_name ,C.DATE_OF_BIRTH, C.GRADE ,p.MemberId from child c  " + //,registration_prepclub.ParentID as MemberID 

        //    " left join registration_prepclub p on p.childnumber=c.childnumber  left join chapter ch on ch.ChapterId=p.chapterid where ch.chapterid=" + chapterid + " and P.EventYear='" + System.DateTime.Now.Year.ToString() + "'";

        string commandString = "select  distinct(p.ChildNumber),DENSE_RANK() OVER(ORDER BY c.ChildNumber) AS 'Ser#', c.First_name,c.Last_name,CONVERT(VARCHAR(10),C.DATE_OF_BIRTH,101) as DOB, C.GRADE as Gr ,p.MemberId from child c  " + //,registration_prepclub.ParentID as MemberID 

           " left join registration_prepclub p on p.childnumber=c.childnumber  left join chapter ch on ch.ChapterId=p.chapterid where p.paymentreference is not null and  ch.chapterid=" + chapterid + " and P.EventYear='" + System.DateTime.Now.Year.ToString() + "'";


        ds = SqlHelper.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, commandString);
         GetVenueData(chapID);
         productGroup(chapID);
        

    }
    private void productGroup(int chapterid)
    {
        if (null == ds || ds.Tables.Count == 0) return;

        string[] productGroupColumnNames = { "SB", "VB","MB","SC","GB","EW","PS","BB"};
        foreach (string colName in productGroupColumnNames)
        {
            ds.Tables[0].Columns.Add(colName);
        }
        if (null == dsVenue || dsVenue.Tables.Count == 0) return;


        string strSQL = " select distinct Organization_Name,LEFT(organization_name,6) as short,o.automemberid,DENSE_RANK() OVER(ORDER BY organization_name) AS 'Ser#' " +
            "  from  Registration_PrepClub  Rp inner join organizationinfo o on  Rp.VenueID=o.automemberid where Rp.paymentreference is not null and Rp.ChapterId=" + chapterid + " and Rp.EventYear='" + System.DateTime.Now.Year.ToString() + "'";
        DataSet dsOrgDetails;
        dsOrgDetails = SqlHelper.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, strSQL);


        string commandString = " select RegID,ProductGroupCode,VenueID, ChildNumber,productcode from registration_prepclub where chapterid=" + chapID + "";

        DataSet dsproduct = SqlHelper.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, commandString);
       DataTable dtMaster = ds.Tables[0];
       DataTable dtRegProductDetails = dsproduct.Tables[0];
       DataTable dtOrganization = dsOrgDetails.Tables[0];
       Response.Write("<br> dfdf");
        for (int masterIndex = 0; masterIndex <= dtMaster.Rows.Count - 1; masterIndex++)
       {
        string childNumber= dtMaster.Rows[masterIndex]["ChildNumber"].ToString();

            DataRow[] drResults = dtRegProductDetails.Select("ChildNumber =" + childNumber  +" and venueid is not null");
             
                foreach(DataRow drReg in drResults)
                {                    
                    if (!DBNull.Value.Equals(drReg["VenueID"]))
                    {                      
                        string prdgrpcode = drReg["ProductGroupCode"].ToString();

                        DataRow[] drOrgRegults = dtOrganization.Select("automemberid=" +  Convert.ToInt32 (drReg["VenueID"]));
                        foreach (DataRow drOrg in drOrgRegults)
                        {
                            dtMaster.Rows[masterIndex]["" + prdgrpcode + ""] = drReg["ProductCode"].ToString() + "-" + drOrg["Ser#"].ToString();

                        }                    
                    }
                }
       }

       //for (int masterIndex = 0; masterIndex <= dtMaster.Rows.Count - 1; masterIndex++)
       //{
       //   string childNumber= dtMaster.Rows[masterIndex]["ChildNumber"].ToString();
       //    for (int productGroupIndex = 0; productGroupIndex <= dtRegProductDetails.Rows.Count - 1; productGroupIndex++)
       //    {
               
       //        if ((childNumber == dtRegProductDetails.Rows[productGroupIndex]["ChildNumber"].ToString()))
       //        {
       //            string ProductGroupCode = dtRegProductDetails.Rows[productGroupIndex]["ProductGroupCode"].ToString();

       //            Response.Write("Mas ind :" + masterIndex +":"+ ProductGroupCode + "<br><br>");

       //            for (int orgIndex = 0; orgIndex <= dtOrganization.Rows.Count - 1; orgIndex++)
       //            {
       //                if ((dtOrganization.Rows[orgIndex]["automemberid"].ToString() == dtRegProductDetails.Rows[productGroupIndex]["VenueID"].ToString()))
       //                {
       //                    dtMaster.Rows[masterIndex]["" + ProductGroupCode + ""] = dtRegProductDetails.Rows[productGroupIndex]["ProductCode"].ToString() + "-" + dtOrganization.Rows[orgIndex]["Ser#"].ToString();
       //                    break;
       //                }
       //            }                 
       //        }
       //    }
       //    //for (int g = 0; g <= dtmergeMain.Rows.Count - 1; g++)
       //    //{
       //    //Note: SB starts from 8th

       //    for (int venueColumnIndex = 8; venueColumnIndex <= 14; venueColumnIndex++)
       //    {
       //        if (!DBNull.Value.Equals(dtMaster.Rows[masterIndex][venueColumnIndex]))
       //        {
                 
       //            for (int v = 0; v <= dtOrganization.Rows.Count - 1; v++)
       //            {
       //                if ((dtOrganization.Rows[v]["Ser#"].ToString() == dtMaster.Rows[masterIndex][venueColumnIndex].ToString()))
       //                {
       //                    if (DBNull.Value.Equals(dtMaster.Rows[masterIndex][dtOrganization.Rows[v]["short"].ToString()]))

       //                        dtMaster.Rows[masterIndex][dtOrganization.Rows[v]["short"].ToString()] = 1;
       //                    else
       //                        dtMaster.Rows[masterIndex][dtOrganization.Rows[v]["short"].ToString()] = Convert.ToInt32(dtMaster.Rows[masterIndex][dtOrganization.Rows[v]["short"].ToString()]) + 1;

       //                }

       //            }
       //        }

       //    }
       //    //}

       
       //}
      
        Session["Report"] = dtMaster;
        if (dtMaster == null)
        {
            btnSave.Visible = false;
            GridViewforReport.Visible = false;
            spErrMsg.InnerText = "No Records to display.";
        }
        else
        {
            btnSave.Visible = true;
            GridViewforReport.Visible = true;
            GridViewforReport.DataSource = dtMaster;
            GridViewforReport.DataBind();
        }
    }
    private void GetVenueData(int chapterid)
    {

        string commandString = " select distinct Organization_Name,LEFT(organization_name,6) as short,o.automemberid,DENSE_RANK() OVER(ORDER BY organization_name) AS 'Ser#' " +

            "  from  Registration_PrepClub  Rp inner join organizationinfo o on  Rp.VenueID=o.automemberid where Rp.paymentreference is not null and Rp.ChapterId=" + chapterid + " and Rp.EventYear='" + System.DateTime.Now.Year.ToString() + "'";

        dsVenue = SqlHelper.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString().ToString(), CommandType.Text, commandString);
        DataTable dtmerge = new DataTable();
        GridVenueDetails.Visible = true;
        GridVenueDetails.DataSource = dsVenue.Tables[0];
       GridVenueDetails.DataBind();


    }   
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        GeneralExport((DataTable)Session["Report"], "ReportForPrepclub.xls");
    }


    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtdata.Columns.Count - 1) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'> Registration Details for PrepClub</td></tr>");
        Response.Write("<tr style='height:35px'><td><b>Chapter :</b></td><td colspan=" + (dtdata.Columns.Count - 2) + "> " + lblChapter.Text + "</td></tr>");

        if (dsVenue != null)
        {
            DataTable dtVenue = dsVenue.Tables[0];
            Response.Write("<tr><td colspan=" + (dtdata.Columns.Count - 1) + " style='font-weight:bold;text-align:right;'>Organization Name</td></tr>");
            foreach (DataRow dr in dtVenue.Rows)
            {
                Response.Write("<tr><td colspan=6></td><td colspan=" + (dtdata.Columns.Count - 7) + " style='text-align:center;'>" + dr[3].ToString() + " - " + dr[0].ToString() + "</td></tr>");
            }

        }

        if (dtdata != null)
        {
            Response.Write("<tr>");
            foreach (DataColumn dc in dtdata.Columns)
            {
                //hide ChildNumber column 
                if (dc.ColumnName == "ChildNumber") continue;
                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                Response.Write("<tr>");
                for (int i = 1; i < dtdata.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }



 //       Response.Write("\n\t\t\t\t Registration Details\n");

 //       Response.Write("\t\t <Label class='b'>Chapter :</Label>\t " + lblChapter.Text + "\n");
 //string style = @"<style> .text { mso-number-format:\@; } .b{font-weight:bold;}</style> ";
 //           Response.Write(style); 
        ////if (dtdata != null)
        ////{
        ////    foreach (DataColumn dc in dtdata.Columns)
        ////    {
        ////        Response.Write(dc.ColumnName + "\t");
                
        ////    }
        ////    Response.Write(System.Environment.NewLine);
        ////    foreach (DataRow dr in dtdata.Rows)
        ////    {
        ////        for (int i = 0; i < dtdata.Columns.Count; i++)
        ////        {
        ////            Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
        ////        }
        ////        Response.Write("\n");
        ////    }
           

        ////    Response.Flush();
        ////    HttpContext.Current.Response.Flush();
        ////    HttpContext.Current.Response.SuppressContent = true;
        ////    HttpContext.Current.ApplicationInstance.CompleteRequest();
        ////}
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.AppSettings["DBConnection"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void Gridview_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Visible = false;
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("workshopTmcList.aspx");
    } 
}