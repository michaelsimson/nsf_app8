Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI
Partial Class Reports_BankTransReports
    Inherits System.Web.UI.Page
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Or (Session("RoleId").ToString() = "38") Then
                    loadyear()
                    loadNewyear()
                    rbtnDaily.Checked = True
                    trdate.Visible = True
                    rbtnDate.Checked = True
                Else
                    Response.Redirect("..\VolunteerFunctions.aspx")
                End If
            Else
                Response.Redirect("..\Login.aspx")
            End If
            
        End If
    End Sub
    Private Sub loadyear()
        Dim LoadFY As New DataSet
        LoadFY = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct fiscalyear from BankFYDates order by fiscalyear")
        ddlFY.DataValueField = "fiscalyear"
        ddlFY.DataTextField = "fiscalyear"
        ddlFY.DataSource = LoadFY.Tables(0)
        ddlFY.DataBind()
        ddlCY.Items.Clear()
        Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select  Max(year(TransDate)) as Maxyear,Min(year(TransDate)) as MinYear from Banktrans")
        Dim MaxYear, MinYear As Integer
        While read1.Read()
            MaxYear = read1("MaxYear")
            MinYear = read1("MinYear")
        End While
        read1.Close()
        Dim year As Integer = MinYear
        Dim i As Integer = 0
        While year <= MaxYear
            ddlCY.Items.Insert(i, year)
            i = i + 1
            year = year + 1
        End While
    End Sub

    Protected Sub ddlFrequency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case ddlFrequency.SelectedValue
            Case 1
                'Daily 
                trdate.Visible = True
                tryear1.Visible = False
                rbtnFY.Checked = False
                rbtnCY.Checked = False
                tryear2.Visible = False
                rbtnDate.Checked = True
            Case 0
                lblErr.Text = "Please select Frequency"
            Case 2
                'Monthly 
                rbtnDate.Checked = False
                rbtnFY.Checked = True
                trdate.Visible = False
                tryear1.Visible = True
                tryear2.Visible = True
            Case 3
                'Yearly
                rbtnDate.Checked = False
                rbtnFY.Checked = True
                trdate.Visible = False
                tryear1.Visible = True
                tryear2.Visible = True
        End Select
    End Sub

    Protected Sub rbtnDaily_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbtnDaily.Checked = True Then
            'Daily Report
            trdate.Visible = True
            trfrequency.Visible = False
            tryear1.Visible = False
            tryear2.Visible = False
            rbtnDate.Checked = True
            btnAddFiscalYear.Visible = False
        Else
            'Report By Category
            trdate.Visible = False
            trfrequency.Visible = True
            tryear1.Visible = False
            tryear2.Visible = False
            rbtnFY.Checked = True
            rbtnDate.Checked = False
            btnAddFiscalYear.Visible = True
        End If
    End Sub
    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        '** Session("Frequency")
        '1 - Daily Without Summary
        '2 - Daily Summarized
        '3 - Monthly Fiscal Year
        '4 - Yearly Both Fiscal & Calendar
        '5 - Monthly Calendar year
        Dim StrSQl As String, wherebank As String = "", Wherecndtn As String = "", sel1 As String = "", selall As String = "", groupbyall As String = "", groupby1 As String = "", Orderby As String = ""
        Dim i As Integer
        Dim selectedyear As String = "", SQLString As String
        If ddlBank.SelectedValue > 0 And ddlBank.SelectedValue < 4 Then
            wherebank = " and BankID = " & ddlBank.SelectedValue & " "
        End If
        If rbtnDaily.Checked = True Then
            If IsDate(txtBeginDate.Text) = True And IsDate(txtEndDate.Text) = True Then
                StrSQl = "select BankTransID, BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description, AddInfo, DepSlipNumber, GLAccount from Banktrans where transdate between '" & txtBeginDate.Text & "' and '" & txtEndDate.Text & "' " & wherebank & "  order by transdate Desc"
                Session("StrSQl") = StrSQl
                Session("Frequency") = 1
                loadgrid(dgDailyReports, Session("StrSQl"), Session("Frequency"))
                dgDailyReports.CurrentPageIndex = 0
            Else
                lblErr.Text = "Please enter Begin Date and End Date in MM/DD/YYYY Format"
            End If
        ElseIf rbtnCategory.Checked = True And rbtnFY.Checked = False Then
            If ddlFrequency.SelectedValue = 1 Then
                'Daily
                If IsDate(txtBeginDate.Text) = True And IsDate(txtEndDate.Text) = True Then
                    Wherecndtn = " AND transdate between '" & txtBeginDate.Text & "' and '" & txtEndDate.Text & "' "
                Else
                    lblErr.Text = "Please enter Begin Date and End Date"
                    Exit Sub
                End If
                sel1 = " t1.MonthNo,t1.DayNo,t1.Name, "
                selall = "  month(TransDate) as MonthNo, Day(TransDate) as DayNo,convert(Varchar(2),day(TransDate)) + ' ' + datename(month, TransDate) + ' '+ convert(varchar(4), year(TransDate)) as Name, "
                groupbyall = " ,month(TransDate), Day(TransDate),convert(Varchar(2),day(TransDate)) + ' ' + datename(month, TransDate) + ' '+ convert(varchar(4), year(TransDate)) "
                groupby1 = " ,t1.MonthNo,t1.DayNo,t1.Name "
                Orderby = " ,2 ,3 "
                Session("Frequency") = 2
            ElseIf ddlFrequency.SelectedValue = 2 And rbtnCY.Checked = True Then
                'Monthly
                sel1 = " t1.MonthNo,t1.Name, "
                selall = " month(TransDate) as MonthNo, datename(month, TransDate) + ' ' + convert(varchar(4), year(TransDate)) as Name, "
                groupbyall = ", month(TransDate), datename(month, TransDate) + ' ' + convert(varchar(4), year(TransDate))"
                groupby1 = ",t1.MonthNo,t1.Name "
                Orderby = " ,2  "
                For i = 0 To ddlCY.Items.Count - 1
                    If ddlCY.Items(i).Selected = True Then
                        If selectedyear = "" Then
                            selectedyear = ddlCY.Items(i).Text
                        Else
                            selectedyear = selectedyear & "," & ddlCY.Items(i).Text
                        End If
                    End If
                Next
                If selectedyear <> "" Then
                    Wherecndtn = " and year(TransDate) in (" & selectedyear & ") "
                End If
                Session("Frequency") = 5
            ElseIf ddlFrequency.SelectedValue = 3 And rbtnCY.Checked = True Then
                'Yearly
                sel1 = " '' as Name, "
                Session("Frequency") = 4
            End If
            SQLString = "Select t1.year," & sel1 & "SUM(t1.Donation) as Donation,SUM(t1.Deposit) as Deposit,Sum(t1.VisaMC) as VisaMC,Sum(t1.Amex) as Amex,Sum(t1.Discover) as Discover,SUM(t1.Interest) as Interest,SUM(t1.[Check]) as [Check],SUM(t1.Shipping) as Shipping,SUM(t1.[Grant]) as [Grant],SUM(t1.[Deposit Return]) as [Deposit Return],SUM(t1.Fee) as Fee,"
            SQLString = SQLString + "SUM(t1.Donation)+SUM(t1.Deposit)+Sum(t1.VisaMC) + Sum(t1.Amex) + Sum(t1.Discover)+SUM(t1.Interest)+SUM(t1.[Check])+SUM(t1.Shipping)+SUM(t1.[Grant])+SUM(t1.[Deposit Return])+SUM(t1.Fee) as Total  from ("
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " sum(amount) as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Donation' " & wherebank & Wherecndtn & "   group by year(TransDate) " & groupbyall & ") Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,sum(amount) as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Deposit' " & wherebank & Wherecndtn & "  group by year(TransDate) " & groupbyall & " ) Union All"
            '**SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, sum(amount) as	[Credit Card],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='CreditCard' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, sum(amount) as [VisaMC],0 as [Amex],0 as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='CreditCard' and VendCust ='VisaMC' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],sum(amount) as [Amex],0 as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='CreditCard' and VendCust ='AmExp' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],sum(amount) as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='CreditCard' and VendCust ='Discover' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"

            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],sum(amount) as Interest,0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Interest' " & wherebank & Wherecndtn & "  group by year(TransDate) " & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, sum(amount) as [Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Check' " & wherebank & Wherecndtn & "  group by year(TransDate) " & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], sum(amount) as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Shipping' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], sum(amount) as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='Grant' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + "(select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], 0 as	[Grant], sum(amount) as	[Deposit Return], 0 as	Fee  from banktrans  where transcat='DepositReturn' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & " ) Union All"
            SQLString = SQLString + " select year(TransDate) as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], 0 as	[Grant], 0 as	[Deposit Return], sum(amount) as	Fee  from banktrans  where transcat='Fee' " & wherebank & Wherecndtn & "  group by year(TransDate)" & groupbyall & ") t1 Group by t1.year" & groupby1 & " order by 1 desc" & Orderby
            'lblErr.Text = SQLString
            Session("StrSQl") = SQLString
            loadgrid(dgReports, SQLString, Session("Frequency"))
            dgReports.CurrentPageIndex = 0
        ElseIf rbtnCategory.Checked = True And rbtnFY.Checked = True Then
            If ddlFrequency.SelectedValue = 2 And rbtnFY.Checked = True Then
                'Monthly
                sel1 = " t1.CYYear,t1.MonthNo,t1.Name, "
                selall = " Year(B.TransDate) as CYYear, month(B.TransDate) as MonthNo, datename(month, B.TransDate) + ' '+ convert(varchar(4), Year(B.TransDate)) as Name, "
                groupbyall = " , Year(B.TransDate), month(B.TransDate), datename(month, B.TransDate) + ' ' + convert(varchar(4), year(B.TransDate))"
                groupby1 = ",t1.CYYear,t1.MonthNo,t1.Name "
                Orderby = " ,2 ,3"
                selectedyear = ""
                For i = 0 To ddlFY.Items.Count - 1
                    If ddlFY.Items(i).Selected = True Then
                        If selectedyear = "" Then
                            selectedyear = ddlFY.Items(i).Text
                        Else
                            selectedyear = selectedyear & "," & ddlFY.Items(i).Text
                        End If
                    End If
                Next
                If selectedyear <> "" Then
                    Wherecndtn = " and FY.FiscalYear in (" & selectedyear & ") "
                End If
                Session("Frequency") = 3
            ElseIf ddlFrequency.SelectedValue = 3 And rbtnFY.Checked = True Then
                'Yearly
                sel1 = " '' as Name, "
                Session("Frequency") = 4
            End If

            SQLString = "Select t1.year," & sel1 & "SUM(t1.Donation) as Donation,SUM(t1.Deposit) as Deposit,Sum(t1.VisaMC) as VisaMC , Sum(t1.Amex) as Amex , Sum(t1.Discover) as Discover,SUM(t1.Interest) as Interest,SUM(t1.[Check]) as [Check],SUM(t1.Shipping) as Shipping,SUM(t1.[Grant]) as [Grant],SUM(t1.[Deposit Return]) as [Deposit Return],SUM(t1.Fee) as Fee,"
            SQLString = SQLString + " SUM(t1.Donation)+SUM(t1.Deposit)+Sum(t1.VisaMC) + Sum(t1.Amex) + Sum(t1.Discover)+SUM(t1.Interest)+SUM(t1.[Check])+SUM(t1.Shipping)+SUM(t1.[Grant])+SUM(t1.[Deposit Return])+SUM(t1.Fee) as Total from (("
            SQLString = SQLString + "  select  FY.FiscalYear as year," & selall & " sum(B.amount) as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Donation' " & wherebank & Wherecndtn & " and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear " & groupbyall & ") Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,sum(B.amount) as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Deposit' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            '**SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit, sum(B.amount) as	[Credit Card],0 as	Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='CreditCard' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear " & groupbyall & " ) Union All"
            SQLString = SQLString + "(select FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit, sum(B.amount) as [VisaMC],0 as [Amex],0 as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where B.transcat='CreditCard' and B.VendCust ='VisaMC' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear " & groupbyall & " ) Union All"
            SQLString = SQLString + "(select FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],sum(B.amount) as [Amex],0 as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where B.transcat='CreditCard' and B.VendCust ='AmExp' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear " & groupbyall & " ) Union All"
            SQLString = SQLString + "(select FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit, 0 as [VisaMC],0 as [Amex],sum(B.amount) as [Discover],0 as Interest, 0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where B.transcat='CreditCard' and B.VendCust ='Discover' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear " & groupbyall & " ) Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],sum(B.amount) as Interest,0 as	[Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Interest' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, sum(B.amount) as [Check], 0 as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Check' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], sum(B.amount) as	[Shipping],0 as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Shipping' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], sum(B.amount) as	[Grant], 0 as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='Grant' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            SQLString = SQLString + " (select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], 0 as	[Grant], sum(B.amount) as	[Deposit Return], 0 as	Fee  from banktrans B, BankFYDates FY  where transcat='DepositReturn' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") Union All"
            SQLString = SQLString + "  select  FY.FiscalYear as year," & selall & " 0 as Donation,0 as Deposit,  0 as [VisaMC],0 as [Amex],0 as [Discover],0 as	Interest, 0 as	[Check], 0 as	[Shipping], 0 as	[Grant], 0 as	[Deposit Return], sum(B.amount) as	Fee  from banktrans B, BankFYDates FY  where transcat='Fee' " & wherebank & Wherecndtn & "  and B.BankID=FY.BankNumber  and B.TransDate  Between FY.BeginDate and FY.EndDate  group by FY.FiscalYear  " & groupbyall & ") t1 Group by t1.year" & groupby1 & " order by 1 desc" & Orderby
            'lblErr.Text = SQLString
            Session("StrSQl") = SQLString
            loadgrid(dgReports, SQLString, Session("Frequency"))
            dgReports.CurrentPageIndex = 0
        End If
    End Sub
    Protected Sub loadgrid(ByVal dg As DataGrid, ByVal strsql As String, ByVal Freq As Integer)
        dgReports.DataSource = Nothing
        dgReports.DataBind()
        dgDailyReports.DataSource = Nothing
        dgDailyReports.DataBind()
        Dim dsBankTrans As New DataSet
        Dim tblBankTrans() As String = {"BankTrans"}
        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, strsql, dsBankTrans, tblBankTrans)
        Dim i, j As Integer
        Dim dr As DataRow
        dr = dsBankTrans.Tables("BankTrans").NewRow
        dr(0) = 0
        If Freq > 1 And Freq < 6 Then
            If Freq = 2 Then
                dsBankTrans.Tables("BankTrans").Columns.Remove("MonthNo")
                dsBankTrans.Tables("BankTrans").Columns.Remove("DayNo")
            ElseIf Freq = 3 Then
                dsBankTrans.Tables("BankTrans").Columns.Remove("CYYear")
                dsBankTrans.Tables("BankTrans").Columns.Remove("MonthNo")
            ElseIf Freq = 5 Then
                dsBankTrans.Tables("BankTrans").Columns.Remove("MonthNo")
            End If
            dr(1) = "Total"
            For i = 2 To dsBankTrans.Tables("BankTrans").Columns.Count - 1
                dr(i) = 0.0
                For j = 0 To dsBankTrans.Tables("BankTrans").Rows.Count - 1
                    dr(i) = dr(i) + dsBankTrans.Tables("BankTrans").Rows(j)(i)
                Next
            Next
            dsBankTrans.Tables("BankTrans").Rows.Add(dr)
        End If
        dg.DataSource = dsBankTrans
        dg.CurrentPageIndex = 0
        dg.DataBind()
        btnExport.Visible = True
    End Sub

    Protected Sub dgDailyReports_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        loadgrid(dgDailyReports, Session("StrSQl"), Session("Frequency"))
        dgDailyReports.CurrentPageIndex = e.NewPageIndex
        dgDailyReports.DataBind()
    End Sub

    Protected Sub dgReports_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        loadgrid(dgReports, Session("StrSQl"), Session("Frequency"))
        dgReports.CurrentPageIndex = e.NewPageIndex
        dgReports.DataBind()
    End Sub
    Private Sub loadNewyear()
        ddlNewYear.Items.Clear()
        Dim MaxYear As Integer
        MaxYear = Now.Year
        Dim year As Integer = MaxYear - 2
        Dim i As Integer = 0
        While year <= MaxYear
            ddlNewYear.Items.Insert(i, year)
            i = i + 1
            year = year + 1
        End While
    End Sub

    Protected Sub AddDate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlNewBank.SelectedValue < 1 Then
                lblNewErr.ForeColor = Drawing.Color.Red
                lblNewErr.Text = "Please select Bank"
            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from bankfydates where banknumber=" & ddlNewBank.SelectedValue & " and fiscalyear=" & ddlNewYear.SelectedValue & "") > 0 Then
                lblNewErr.ForeColor = Drawing.Color.Red
                lblNewErr.Text = "Selected bank has dates for selected Year"
            ElseIf IsDate(txtNewBeginDate.Text) = True And IsDate(txtNewEndDate.Text) = True Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankFYDates(BankNumber, BankName, FiscalYear, BeginDate, EndDate) values (" & ddlNewBank.SelectedItem.Value & ",'" & ddlNewBank.SelectedItem.Text & "'," & ddlNewYear.SelectedValue & ",'" & txtNewBeginDate.Text & "','" & txtNewEndDate.Text & "')")
                lblNewErr.ForeColor = Drawing.Color.Black
                lblNewErr.Text = "Dates Inseted Successfully"
            Else
                lblNewErr.ForeColor = Drawing.Color.Red
                lblNewErr.Text = "Please Enter Date In MM/DD/YYYY format"
            End If
        Catch ex As Exception
            lblNewErr.ForeColor = Drawing.Color.Red
            lblNewErr.Text = "Error in storing Data "
        End Try
    End Sub

    Protected Sub ClosePanel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PnlFYDates.Visible = False
    End Sub

    Protected Sub btnAddFiscalYear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PnlFYDates.Visible = True
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=BankTransReports.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        ''**If dgReports.Items.Count > 0 Then
        ''    dgReports.RenderControl(htmlWrite)
        ''Else
        ''    dgDailyReports.RenderControl(htmlWrite)
        ''End If
        Dim dgexport As New DataGrid
        loadgrid(dgexport, Session("StrSQl"), Session("Frequency"))
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub lbtnVolunteerFunctions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("StrSQL")
        Session.Remove("Frequency")
        Response.Redirect("~/VolunteerFunctions.aspx")
    End Sub
End Class
