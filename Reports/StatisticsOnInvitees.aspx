<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StatisticsOnInvitees.aspx.vb" Inherits="Reports_StatisticsOnInvitees" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Statistics On Invitees</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="left">
				<br />
				<table style="width :900px"><tr><td align="center">	<b>Statistics on National Invitees</b></td></tr></table>
			
				<br />
        <asp:LinkButton ID="Lbtn1" runat="server" PostBackUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:LinkButton>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button id="btnSave" runat="server" Text="Export Data" onclick="btnSave_Click" Width ="100px"></asp:Button>

	 </div> 
				<div align="center">
    
                <asp:datagrid id="dgStatistics"  runat="server" Width="784px" Height="304px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="3" GridLines="Vertical">				
                <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			    </asp:datagrid>
	
			
    
    </div>
    </form>
</body>
</html>
