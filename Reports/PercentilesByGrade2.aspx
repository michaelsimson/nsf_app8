<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PercentilesByGrade2.aspx.cs" Inherits="Reports_PercentilesByGrade2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Percentiles By Grade</title>
</head>
<body>
		<form id="Form1" method="post" runat="server">
			
<asp:datagrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 96px" runat="server"
				Width="784px" Height="304px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" BackColor="White"
				CellPadding="3" GridLines="Vertical">
				
<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>

				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
	<asp:dropdownlist id="ddlContest" style="Z-INDEX: 102; LEFT: 104px; POSITION: absolute; TOP: 24px"
				runat="server" AutoPostBack="True">
				<asp:ListItem Value="JSB">JSB</asp:ListItem>
				<asp:ListItem Value="SSB">SSB</asp:ListItem>
				<asp:ListItem Value="JVB">JVB</asp:ListItem>
				<asp:ListItem Value="IVB">IVB</asp:ListItem>
				<asp:ListItem Value="SVB">SVB</asp:ListItem>
				<asp:ListItem Value="MB1">MB1</asp:ListItem>
				<asp:ListItem Value="MB2">MB2</asp:ListItem>
				<asp:ListItem Value="MB3">MB3</asp:ListItem>
				<asp:ListItem Value="MB4">MB4</asp:ListItem>
				<asp:ListItem Value="JGB">JGB</asp:ListItem>
				<asp:ListItem Value="SGB">SGB</asp:ListItem>
				<asp:ListItem Value="EW1">EW1</asp:ListItem>
				<asp:ListItem Value="EW2">EW2</asp:ListItem>
				<asp:ListItem Value="EW3">EW3</asp:ListItem>
				<asp:ListItem Value="PS1">PS1</asp:ListItem>
				<asp:ListItem Value="PS3">PS3</asp:ListItem>
				<asp:ListItem Value="JSC">JSC</asp:ListItem>
				<asp:ListItem Value="ISC">ISC</asp:ListItem>
				<asp:ListItem Value="SSC">SSC</asp:ListItem>
			</asp:dropdownlist><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 208px; POSITION: absolute; TOP: 56px" runat="server"></asp:label>
			<asp:Button id="btnSave" style="Z-INDEX: 104; LEFT: 608px; POSITION: absolute; TOP: 48px" runat="server"
				Text="Save to Excel" onclick="btnSave_Click" Width ="100px"></asp:Button></form>
	</body>
</html>


 
 
 