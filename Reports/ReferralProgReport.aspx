﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReferralProgReport.aspx.cs" Inherits="Reports_ReferralProgReport" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 850px;
            top: 55%;
            left: 19%;
            margin-left: auto;
            margin-top: -200px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            margin-right: auto;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>
    <script src="../Scripts/jquery-1.9.1.js"></script>
    <script>
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }
        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvMemberDetails").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvMemberDetails").hide();
        }

    </script>
    <div style="width: 1000px; margin-left: auto; margin-right: auto; min-height: 400px;">
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            Referral Program Report

                     <br />
            <br />
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="float: right;">
            <asp:Button ID="btnExportToExcel" Visible="false" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
        </div>
        <div align="center">

            <div style="clear: both; margin-bottom: 5px;"></div>
            <div align="center"><span id="spnTable2Title" style="font-weight: bold;" runat="server" visible="true">Table 1: Parents responsible for getting others to join NSF</span></div>

            <div style="clear: both;"></div>

            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdReferralProgReport" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 850px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdReferralProgReport_RowCommand" AllowPaging="true" OnPageIndexChanging="grdReferralProgReport_PageIndexChanging" PageSize="20">
                <Columns>



                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            <div style="display: none;">
                                <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'></asp:Label>
                                <asp:Label ID="lblRefType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RefType") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:BoundField DataField="Chapter" HeaderText="ChapterName"></asp:BoundField>

                    <asp:BoundField DataField="FirstName" HeaderText="First Name" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                    <asp:ButtonField DataTextField="Recommended" HeaderText="Joined NSF" CommandName="MemberCount" ItemStyle-HorizontalAlign="Center"></asp:ButtonField>


                    <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                    <asp:BoundField DataField="RefType" HeaderText="Ref Type"></asp:BoundField>
                    <asp:BoundField DataField="AutoMemberID" HeaderText="AutoMemberID"></asp:BoundField>
                </Columns>

                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>

            <div style="clear: both;"></div>
            <div align="center">
                <span id="spnStatus" runat="server" style="color: red;" visible="false">No record found</span>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div id="output"></div>

        <div id="overlay" class="web_dialog_overlay"></div>
        <div id="dvMemberDetails" class="web_dialog">
            <div style="height: 25px; background-color: rgb(51, 102, 153); text-align: center;">
                <span style="text-align: center; color: white; font-weight: bold;">Member Details</span>
            </div>
            <center>

                <asp:GridView ID="grdMemberInfo" runat="server" AutoGenerateColumns="false" Width="800px" Style="margin-left: auto; margin-right: auto; margin-top: 10px;" AllowPaging="true" HeaderStyle-BackColor="#336699" BorderColor="#336699" OnPageIndexChanging="grdMemberInfo_PageIndexChanging" PageSize="10">
                    <Columns>

                        <asp:TemplateField HeaderStyle-ForeColor="White">
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="AutoMemberID" Visible="true" HeaderText="Member ID" HeaderStyle-ForeColor="White" />

                        <asp:BoundField DataField="Chapter" Visible="true" HeaderText="Chapter Name" HeaderStyle-ForeColor="White" />

                        <asp:BoundField DataField="FirstName" Visible="true" HeaderText="First Name" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="LastName" Visible="true" HeaderText="Last Name" HeaderStyle-ForeColor="White"></asp:BoundField>


                        <asp:BoundField DataField="Email" Visible="true" HeaderText="Email" HeaderStyle-ForeColor="White" />

                        <asp:BoundField DataField="HPhone" Visible="true" HeaderText="Home Phone" HeaderStyle-ForeColor="White"></asp:BoundField>
                        <asp:BoundField DataField="CPhone" Visible="true" HeaderText="Cell Phone" HeaderStyle-ForeColor="White"></asp:BoundField>
                        <asp:BoundField DataField="WPhone" Visible="true" HeaderText="Work Phone" HeaderStyle-ForeColor="White"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </center>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <input type="button" id="btnClose" onclick="HideDialog()" value="Close" style="margin-bottom: 10px;" />
            </center>
        </div>
    </div>
    <input type="hidden" id="hdnAutoMemberID" runat="server" value="" />
    <input type="hidden" id="hdnRefType" runat="server" value="" />
</asp:Content>
