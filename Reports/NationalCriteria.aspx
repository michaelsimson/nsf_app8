<%@ Page Language="VB" AutoEventWireup="true" CodeFile="NationalCriteria.aspx.vb"
    Inherits="Reports_NationalCriteria" Title="National Selection Criteria" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <link href="../css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="../css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../js/jquery.qtip.min.js"></script>
        <script src="../js/ezmodal.js"></script>
        <link href="../css/Hover.css" rel="stylesheet" />

        <script type="text/javascript">
            function confirmation() {
                if (confirm("Cutoff Scores already exist for the year 2018. Do you want to update the cutoff scores from the Percentile Application?")) {

                    document.getElementById("<%=btnUpdateCutOffScores.ClientID%>").click();
                } else {
                    document.getElementById("<%=btnUpdateCutOffScoresNo.ClientID%>").click();
                }
            }

            $(document).on("click", "#btnHelp", function (e) {
                var currentYear = (new Date).getFullYear();
                $("#spnCurYear").text(currentYear);
                $(".btnPopUP").trigger("click");
            });
        </script>
        <style type="text/css">
            .tblCell {
                border: 1px solid #999999;
                border-collapse: collapse;
            }
        </style>

        <style type="text/css">
            .custom_modal {
                width: auto !important;
            }

            .ac-wrapper {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(255,255,255,.6);
                z-index: 1001;
            }

            .popup {
                width: 480px;
                height: 250px;
                background: #FFFFFF;
                border: 2px solid #8CC403;
                border-radius: 15px;
                -moz-border-radius: 15px;
                -webkit-border-radius: 15px;
                box-shadow: #8CC403 0px 0px 3px 3px;
                -moz-box-shadow: #8CC403 0px 0px 3px 3px;
                -webkit-box-shadow: #8CC403 0px 0px 3px 3px;
                position: relative;
                top: 150px;
                left: 450px;
            }

            /*.qTipWidth {
            max-width: 900px !important;
        }*/
            p {
                font-weight: normal;
            }
        </style>
    </div>

    <%--  <%@ Page Language="VB" AutoEventWireup="false" CodeFile="NationalCriteria.aspx.vb" EnableEventValidation="false" Inherits="Reports_NationalCriteria" %>--%>


    <asp:Button ID="btnUpdateCutOffScores" runat="server" Text="Delete Meeting" Style="display: none;"
        OnClick="btnUpdateCutOffScores_Click" />
    <asp:Button ID="btnUpdateCutOffScoresNo" runat="server" Text="Delete Meeting" Style="display: none;" OnClick="btnUpdateCutOffScoresNo_Click" />
    <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:HyperLink>

    <center>
        <h2>Criteria for National Invitees by Contest and Grade
            <asp:Label ID="lblYear" runat="server"></asp:Label>
        </h2>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div style="float: right;">
        <input type="button" id="btnHelp" value="Help" />
    </div>
    <br />
    <center>
        <div style="width: 1000px; margin: auto;">
            <div style="float: left;"><b>Select Option</b></div>
            <div style="float: left; margin-left: 10px;">
                <asp:DropDownList ID="ddlOption" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Load Cutoff Scores from Percentiles Application
                    </asp:ListItem>
                    <asp:ListItem Value="2">Edit/Save Cutoff Scores Criteria
                    </asp:ListItem>
                    <asp:ListItem Value="3">Edit/Save New Chapters
                    </asp:ListItem>
                    <asp:ListItem Value="4">Calculate/View/Save Invitee Counts
                    </asp:ListItem>
                    <asp:ListItem Value="5">Year to Year Comparison
                    </asp:ListItem>
                    <asp:ListItem Value="6">Update Priority/Total Invite Columns
                    </asp:ListItem>
                    <asp:ListItem Value="7">Export Priority/Total Invitee Lists
                    </asp:ListItem>
                    <asp:ListItem Value="8">Set National Invitee Flag for Registration

                    </asp:ListItem>


                </asp:DropDownList>
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />
            </div>

            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnSaveScores" runat="server" Visible="false" Text="Save Scores" OnClick="btnSaveScores_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:DropDownList ID="DDLInviteOption" runat="server" Visible="False">
                    <asp:ListItem>Calculate</asp:ListItem>
                    <asp:ListItem>View</asp:ListItem>
                    <asp:ListItem>Save</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="BtnSubmitInvitees" runat="server" Visible="false" Text="Submit" OnClick="BtnSubmitInvitees_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnExportCommon" runat="server" Text="Export" Visible="false" OnClick="btnExportCommon_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:DropDownList ID="ddlInvite" runat="server" AutoPostBack="true" Width="125px" Style="margin-top: 0px" Visible="false">
                    <%-- <asp:ListItem Value="0">Select</asp:ListItem>--%>
                    <asp:ListItem Value="1">Use Priority List</asp:ListItem>
                    <asp:ListItem Value="2">Use Total List</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="BtnUpdatePriortyTotalColumn" runat="server" Visible="false" Text="Submit" OnClick="BtnUpdatePriortyTotalColumn_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:DropDownList ID="ddlNatInvite" runat="server" Width="125px" Style="margin-top: 0px" Visible="False">
                    <asp:ListItem Value="1">Use Priority List</asp:ListItem>
                    <asp:ListItem Value="2">Use Total List</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="BtnSubmitInviteFlag" runat="server" Visible="false" Text="Submit" OnClick="BtnSubmitInviteFlag_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnCalCulateInviteCount" runat="server" Visible="false" Text="Calculate" OnClick="btnCalCulateInviteCount_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnSaveInviteeCounts" runat="server" Visible="false" Text="Save" OnClick="btnSaveInviteeCounts_Click" />
            </div>
            <div runat="server" id="dvExport" visible="false" style="float: left; margin-left: 10px;">
                <asp:DropDownList ID="ddlExport" runat="server" Visible="True">
                    <%--<asp:ListItem Enabled="false">Export Criteria</asp:ListItem>
            <asp:ListItem Enabled="false">Export Invitee Count</asp:ListItem>--%>
                    <asp:ListItem>Export Total Invitee List</asp:ListItem>
                    <asp:ListItem>Export Priority Invitee List</asp:ListItem>
                    <%--<asp:ListItem Enabled="false">Export YearToYear</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:Button ID="BtnExport" runat="server" Text="Export" OnClick="BtnExport_Click" Width="100px" Visible="True" />
            </div>
        </div>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </center>

    <br />

    <asp:Label ID="lblPriority" runat="server"></asp:Label>
    <br />

    <asp:Label ID="lblUpdateMsg" runat="server" ForeColor="Red"></asp:Label>
    <br />

    <div style="display: none;">
        <asp:Button ID="btnSave" runat="server" Text="Edit/Save Criteria" OnClick="btnSave_Click" />
        <asp:Button ID="btnCalCulate" Text="View/Calc Invitee Counts" OnClick="btnCalculate_Click" runat="server" Visible="True" Width="157px" />
        <asp:Button ID="btnNewChapter" runat="server" OnClick="btnNewChapter_Click" Text="Edit/Save New Chapters" Width="160px" />

        <%--        <asp:Button ID="BtnUpdateCnst1" runat="server" Text="Set Priority Invitee Flags" OnClick="BtnUpdateCnst1_Click" Visible="false" Width="175px" /> 
        --%>
        <asp:Button ID="btnUpdateCnst" runat="server" Text="Update Total Invite/Priority Columns" OnClick="btnUpdateCnst_Click" Visible="false" Width="232px" />
        &nbsp; 
        <asp:Button ID="btnY2Y" runat="server" Text="YearToYear Comparison" OnClick="btnYear2Year_Click" Width="170px" Visible="True" />
        <asp:Button ID="btnTies" runat="server" Text="Solve for Ties" OnClick="btnTies_Click" Width="120px" Visible="False" />


        <%--    <asp:Button ID="btnExport" runat="server" Text="Export Invitee Counts" OnClick="BtnExport_Click" Width="160px" Visible="False"  /> 
        <asp:Button ID="btnExport1" runat="server" Text="Export Criteria" OnClick="BtnExport1_Click" Width="120px"  Visible="True"  /> 
        <asp:Button ID="btnExportInvitee" runat="server" Text="Export Total Invitee List" OnClick ="btnExportInvitee_Click" Width="153px"  Visible="False" /> 
        <asp:Button ID="btnExportInviteePriority" runat="server" Text="Export Priority Invitee List" OnClick ="btnExportInviteePriority_Click" Width="168px"  Visible="False" /> 
        <asp:Button ID="btnExportY2Y" runat="server" Text="Export YearToYear " OnClick="btnExportY2Y_Click" Width="150px"  Visible="False" /> 
        --%>
        <br />


        <asp:Button ID="btnNatInvite" runat="server" Text="Set Nat’l Invitee Flag to Register" Width="216px" Visible="False" />
        <asp:Label ID="lblerr" runat="server" Text="" ForeColor="Red"></asp:Label>
        <br />

        <asp:Button ID="btnLoadInviteeCount" runat="server" Text="Load Invitee Count" OnClick="btnLoadInviteeCount_Click" Width="130px" />
        <asp:Button ID="btnSaveInviteeCount" runat="server" Visible="false" Text="Save Invitee Count" OnClick="btnSaveInviteeCount_Click" Width="130px" />
        <br />
        <br />
        Notes: - 
             <ol style="color: Green">
                 <li>Year to Year will show up only after <b>Save the Invitee Count.</b></li>
                 <li><b>Save the Invitee Count</b> will show up after <b>View/Export Invitee Count.</b></li>
             </ol>

    </div>
    <asp:Panel ID="pnlNewChapter" runat="server" Visible="false">
        <table align="center" border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td colspan="3" style="text-align: center"><b>New Chapter Flag Set</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td align="center">Selected Chapters</td>
            </tr>
            <tr>
                <td>
                    <asp:ListBox ID="listAll" runat="server" Height="300px" Width="150px" SelectionMode="Multiple"></asp:ListBox>
                </td>
                <td style="vertical-align: middle; text-align: center">
                    <asp:Button ID="BtnAdd" runat="server" Text="(Move)>>" OnClick="BtnAdd_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnDelete" runat="server" Text="Delete" OnClick="BtnDelete_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnDeleteAll" runat="server" Text="Delete All" OnClick="BtnDeleteAll_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="BtnUpdate" runat="server" Text="Save New Chapters" OnClick="BtnUpdate_Click" Width="120px" /><br />
                    <br />
                    <asp:Button ID="btnClose" runat="server" Text="Close Panel" OnClick="btnClose_Click" Width="120px" />

                </td>
                <td>
                    <asp:ListBox ID="ListSelected" runat="server" Height="300px" Width="150px" SelectionMode="Multiple"></asp:ListBox></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <strong><span style="font-size: 12px; color: #ff3333; font-family: Calibri">* Always Press Update Chapters after making changes</span> </strong>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
    <table align="center">
        <tr runat="server" visible="false" id="trTotalPriorityList">
            <td>

                <div id="dvLoadCutOffScores" runat="server" visible="false">
                    <asp:Button ID="BtnLoadCutOffScores" runat="server" Text="Save Cutoff Scores" OnClick="BtnLoadCutOffScores_Click" Style="position: relative; left: 625px;" />
                </div>

                <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Total List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Width="200px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <%--<asp:BoundField DataField="Contest" HeaderText="Contest" HeaderStyle-ForeColor="white"  />--%>
                        <asp:TemplateField HeaderText="Contest" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtContest" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblContest" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimum Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Width="50px" Text='<%# Bind("MinScore") %>'></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rank1Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtRank1Score1" Width="50px" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRank1Score" runat="server" Width="50px" Text='<%# Bind("Rank1Score") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="New Chapter Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNewChScore1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewChScore" runat="server" Width="50px" Text='<%# Bind("NewChScore") %>'></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MaxScore" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMaxScore1" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMaxScore" runat="server" Text='<%# Bind("MaxScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="White" Height="2px" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" Font-Size="20px" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 50px"></td>
            <td>
                <asp:GridView ID="GVPriority" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Priority List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Width="200px" Style="position: relative; top: 0px;">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <%--<asp:BoundField DataField="Contest" HeaderText="Contest" HeaderStyle-ForeColor="white"  />--%>
                        <asp:TemplateField HeaderText="Contest" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtContest_pr" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblContest_pr" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGrade_pr" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGrade_pr" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimum Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2_pr" runat="server" Text='<%# Bind("MinScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rank1Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtRank1Score1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRank1Score_pr" runat="server" Text='<%# Bind("Rank1Score") %>' Width="50px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="New Chapter Score" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNewChScore1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewChScore_pr" runat="server" Text='<%# Bind("NewChScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MaxScore" HeaderStyle-ForeColor="white">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMaxScore1_pr" runat="server" Width="50px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMaxScore_pr" runat="server" Text='<%# Bind("MaxScore") %>' Width="50px"></asp:TextBox>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvOut" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Total List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Count" HeaderText="Count" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumCount" HeaderText="Cumulative" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Contestants" HeaderText="# of Contestants" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="Invited" HeaderText="% Invited" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="AddRank1" HeaderText="Incr. Rank 1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="nwchcount" HeaderText="Incr. New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumulWrank1" HeaderStyle-Font-Size="10px" HeaderText="Cum with Rank 1/New Chap" HeaderStyle-ForeColor="white" ControlStyle-Width="25px" />
                        <asp:BoundField DataField="AvgScore" HeaderText="Avg.Score" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 50px">
                <asp:GridView ID="gvRatio" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Ratio"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="Ratio1" HeaderText="Ratio1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Ratio2" HeaderText="Ratio2" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />

                </asp:GridView>
            </td>
            <td>
                <asp:GridView ID="gvOutPriority" runat="server" AutoGenerateColumns="False" BackColor="White" Caption="Priority List"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" Width="300px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Count" HeaderText="Count" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumCount" HeaderText="Cumulative" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Contestants" HeaderText="# of Contestants" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField="Invited" HeaderText="% Invited" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" />
                        <asp:BoundField DataField="AddRank1" HeaderText="Incr. Rank 1" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="nwchcount" HeaderText="Incr. New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="CumulWrank1" HeaderText="Cum with Rank 1/New Chap" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="AvgScore" HeaderText="Avg.Score" HeaderStyle-ForeColor="white" />
                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" Width="25px" Height="70px" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnY2YHelp" runat="server" Text="Help" Visible="false" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvY2Y" runat="server" AutoGenerateColumns="False" BackColor="White"
                    BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" Visible="False" Style="margin-top: 0px">
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="Invitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ExRegis" HeaderText="Exp_Reg" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ExpShows" HeaderText="Exp_Shows" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="lastMinScore" HeaderText="Last_Min_Score" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LasInvitees" HeaderText="Last_Invitees" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LastRegis" HeaderText="Last_Registrations" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="LastShows" HeaderText="Last_Shows" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="RegisPers" HeaderText="Registrations %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />
                        <asp:BoundField DataField="ShowsPers" HeaderText="Shows %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />

                    </Columns>
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                </asp:GridView>
            </td>
            <td align="left" width="50px"></td>
            <td>
                <%-- <asp:GridView ID="gcY2YPriority" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#3366CC" BorderStyle="Solid"  BorderWidth="1px" 
        CellPadding="4" Visible="False" style="margin-top: 0px">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <Columns>
                <asp:BoundField DataField="ProductCode" HeaderText="Contest" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="MinScore" HeaderText="Min Score" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Invitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ExRegis" HeaderText="Exp_Reg" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ExpShows" HeaderText="Exp_Shows" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="lastMinScore" HeaderText="Min_Score" HeaderStyle-ForeColor="white"  />
                <asp:BoundField DataField="LasInvitees" HeaderText="Invitees" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="LastRegis" HeaderText="Registrations" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="LastShows" HeaderText="Shows"  HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="RegisPers" HeaderText="Registrations %" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"  HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="ShowsPers" HeaderText="Shows %"  DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right" HeaderStyle-ForeColor="white" />

            </Columns>
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        </asp:GridView>--%>
            </td>
        </tr>

        <tr>
            <td>
                <br />
                <br />
                <asp:Label ID="lblTieBreaker" runat="server"></asp:Label>
                <br />
                <asp:GridView ID="GVtieBreaker" AutoGenerateColumns="false" runat="server" Caption="Rank1 Tie Contestants - Total List ">
                    <Columns>
                        <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                        <asp:BoundField DataField="contestant_id" HeaderText="contestant_id" />
                        <asp:BoundField DataField="TotalScore" HeaderText="TotalScore" />
                        <asp:BoundField DataField="Rank" HeaderText="Rank" />
                        <asp:BoundField DataField="GRADE" HeaderText="GRADE" />
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" />
                        <asp:BoundField DataField="ChapterID" HeaderText="ChapterID" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="left" width="50px"></td>
            <td>
                <br />
                <br />
                <asp:Label ID="lblTieBreakerRank" runat="server"></asp:Label>
                <br />
                <asp:GridView ID="GVTieBreakerRank" AutoGenerateColumns="false" runat="server" Caption="Rank1 Tie Contestants - Priority List ">
                    <Columns>
                        <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                        <asp:BoundField DataField="contestant_id" HeaderText="contestant_id" />
                        <asp:BoundField DataField="TotalScore" HeaderText="TotalScore" />
                        <asp:BoundField DataField="Rank" HeaderText="Rank" />
                        <asp:BoundField DataField="GRADE" HeaderText="GRADE" />
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" />
                        <asp:BoundField DataField="ChapterID" HeaderText="ChapterID" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblContestant" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblContestants_Priority" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblWaitContestants" runat="server" Text="" Visible="false"></asp:Label>
    <br />
    <input type="hidden" runat="server" id="hdnIsNationalCriteria" value="0" />

    <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>
    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Criteria for National Invitees by Contest and Grade <span id="spnCurYear">2018</span>
                    Help
                </span>
            </div>

            <div class="ezmodal-content" style="overflow-y: scroll; height: 500px; width: 800px;">
                <div>
                    <img src="../Images/N_Main.png" />
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div>

                        <b>Step 1: </b>
                        <p>Use <span style="font-weight: bold; color: blue;">“Percentiles for Regionals”</span> application to generate and save Cutoff scores in CutoffScores SQL table. This step should be carried out before using this application <span style="font-weight: bold; color: blue;">“Select Criteria for National Invites”</span>.</p>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div style="font-weight: bold; font-size: 18px;">Load Cutoff Scores from Percentiles Application:</div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>

                            <p>Cutoff scores from CutoffScores SQL table are used as the starting point and are to be loaded into the NationalCriteria SQL table. The user is allowed to modify these cutoff scores in the NationalCriteria SQL table based on experience and judgment. </p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Step 2: </b>
                            <p>Check if data exists for the current year in the SQL tables “CutoffScores” and “NationalCritera”.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Step 3: </b>
                            <p>If there is no data in both SQL tables “NationalCritera” and “CutoffScores”, the system will replicate last year’s data of the SQL table “NationalCriteria” into the current year in the SQL table “NationalCriteria” and show the error message to the user that “there is no record in the CutoffScores SQL table.”</p>
                            <div style="clear: both;"></div>
                            <p>In this case, user can modify the cutoff scores manually and proceed forward. Ideally the user should go back and follow Step 1 and load the cutoff scores from CutoffScores SQL table into the NationalCriteria SQL table.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Step 4: </b>
                            <p>If there is no data in the SQL table “NationalCritera” alone, it would replicate last year’s data of the SQL table “NationalCriteria” to the current year in the SQL table “NationalCriteria” and use the data of the SQL Table “CutoffScores” to updating the cutoff scores in the SQL table “NationalCriteria”.</p>

                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Step 5: </b>
                            <p>If there is a data in the SQL table “NationalCritera”, but not in the “CutoffScores” SQL table, an error message is given out “Cutoff scores are missing from the Cutoff scores SQL table. Use <span style="font-weight: bold; color: blue;">‘Percentiles for Regionals’</span> application to generate and save Cutoff scores in CutoffScores SQL table.”</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Step 6: </b>
                            <p>If there is data in both SQL tables “NationalCritera” and “CutoffScores”, then the system gives message “Do you want to overwrite the cutoffscores in the SQL table ‘NationlaCriteria’ by using the data from the SQL table ‘CutOffScores’?” </p>

                        </div>

                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Edit/Save Cutoff Scores Criteria:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <p>Cutoff scores can be modified manually through this option.</p>
                        </div>
                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Edit/Save New Chapters:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <p>In this option, all chapters (Status=’A’) and New chapters (NewChapterFlag=’Y’) are loaded from the SQL table “Chapter”, respectively, into two different lists, left and right.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <p>User can move a chapter from left to right or delete a chapter from the right.  The chapters on the right are treated as new.</p>

                        </div>
                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>

                        <div style="font-weight: bold; font-size: 18px;">Calculate/View/Save Invitee Counts:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <img src="../Images/N_View.png" />

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <b>Calculate: </b>
                            <p>Invitee counts are calculated based on the latest cutoff scores.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>View: </b>
                            <p>The option “Save” will be enabled after doing the Calculate option. The calculated invitee counts will be saved in the SQL table “IviteeCount”.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Save: </b>
                            <p>Saved invitee count would be visible in the Grid table at this option.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <p><b>Note 1: The Calculate option should only be pursued after completing the 1st option: Load Cutoff Scores from Percentiles Application.</b></p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <p><b>Note 2: One should carefully review this data carefully to make sure it is reasonable before proceeding to the 6th Option.</b></p>
                        </div>
                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Year to Year Comparison:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>

                            <p>Compares the invitee counts, Registrations and cutoff scores from last year to current year.</p>

                        </div>
                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Update Priority/Total Invite Columns:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <img src="../Images/N_Priority.png" />
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <p>The option can be used for updating the flags PriorityInivte and TotInvite to 1 in the SQL table “”Contestant”.</p>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <b>Priority List : </b>
                            <p>PriorityInvite flag would be set to 1 for the contestants who have met the minimum score specified in the CutoffScore SQL table under priority basis.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Total List: </b>
                            <p>TotInvite flag would be set to 1 for the contestants who have met the minimum score specified in the CutoffScore SQL table under waitlist basis.</p>

                        </div>
                        <div style="clear: both; margin-bottom: 5px;"></div>
                        <b>Note: This option should be pursued after completing the 4th option: Calculate/View/Save Invitee Counts.</b>
                    </div>

                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Export Priority/Total Invitee Lists:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <p>The option can be used for exporting the Priority, Total and Wait invitee list to an excel file.</p>
                        </div>
                    </div>
                    <div style="clear: both; margin-bottom: 15px;"></div>
                    <div>
                        <div style="font-weight: bold; font-size: 18px;">Set National Invitee Flag for Registration:</div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <img src="../Images/N_NationaInvitee.png" />
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <p>The option can be used to updating the flag “NationaInvite” to 1 in the SQL table “”Contestant”.</p>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <b>Priority List : </b>
                            <p>For priority list, NationalInvite flag would be set to 1 to the contestants who have the priorityInvite flag = 1. You should execute this option at the same time the registration is open for the priority list. This timing is critical.</p>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <b>Total List: </b>
                            <p>For Total list, NationalInvite flag would be set to 1 to the contestants who have the TotInvite flag = 1. You should execute this option when the priority registration deadline expires so that the wait list registration can begin simultaneously. This timing is critical.</p>

                        </div>

                    </div>


                </div>



                <div class="ezmodal-footer">

                    <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

                </div>

            </div>

        </div>
</asp:Content>

<%--      </form>
</body>

</html>--%>