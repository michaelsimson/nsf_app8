<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChapLogin.aspx.cs" Inherits="Reports_ChapLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
		<form id="Form1" method="post" runat="server">
			<table id="tblLogin" runat="server" width="60%">
				<tr>
					<td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h1>Sign In</h1>
					</td>
				</tr>
				<TR>
					<TD></TD>
					<TD class="ContentSubTitle" vAlign="top" noWrap align="center">Use exisitng NSF 
						login email address and password.</TD>
				</TR>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="left">Login ID</td>
					<td><asp:textbox id="txtUserId" runat="server" MaxLength="30" width="300" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Enter Login Id."
							ControlToValidate="txtUserId"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
							ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="left">Password
					</td>
					<td><asp:textbox id="txtPassword" runat="server" maxlength="30" width="150" CssClass="smFont" TextMode="Password"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Password." ControlToValidate="txtPassword"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td>
						<br>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="center"><asp:button id="btnLogin" runat="server" CssClass="FormButton" Text="Login" onclick="btnLogin_Click"></asp:button></td>
				</tr>
				<tr>
					<td colspan="2">
						&nbsp;<asp:label id="txtErrMsg" runat="server" ForeColor="red" Visible="False">Invalid Login Id or Password.</asp:label>
					</td>
				</tr>
			</table>
			<asp:Label id="lblErrorLogin" runat="server" Width="472px" Height="88px"></asp:Label>
		</form>
	</body>
</html>


 
 
 