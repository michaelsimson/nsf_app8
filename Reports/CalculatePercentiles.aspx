<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CalculatePercentiles.aspx.cs" Inherits="Reports_CalculatePercentiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Percentiles For Regionals</title>
</head>
<body>
		<form id="Form1" method="post" runat="server">
		    <div style="margin-left: 400px">
                <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
            </div>
			<asp:Button id="Button1" style="Z-INDEX: 101; LEFT: 672px; POSITION: absolute; TOP: 288px" runat="server"
				Text="Button" Visible="False" onclick="Button1_Click"></asp:Button>
			<br />
			<asp:DropDownList ID="ddlPercentile" runat="server" AutoPostBack="true"
                 
                style="Z-INDEX: 152; LEFT: 11px; POSITION: absolute; TOP: 37px; height: 23px; width: 221px;" 
                onselectedindexchanged="ddlPercentile_SelectedIndexChanged">
                <asp:ListItem Value="0">Select</asp:ListItem>
			<asp:ListItem Value="1">Calculate Percentiles By Contest</asp:ListItem>
			<asp:ListItem Value="2">View Percentiles By Contest</asp:ListItem>
			</asp:DropDownList>
			
			<br /><br />
			<asp:Label ID ="lblYear" runat="server" Text="Contest Year:"></asp:Label>&nbsp;&nbsp;
            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" 
              
                onselectedindexchanged="ddlYear_SelectedIndexChanged" >
              	<asp:ListItem Value="2013">2013</asp:ListItem>
				<asp:ListItem Value="2012">2012</asp:ListItem>
				<asp:ListItem Value="2011">2011</asp:ListItem>
				<asp:ListItem Value="2010">2010</asp:ListItem>
			    <asp:ListItem Value="2009">2009</asp:ListItem>
			</asp:DropDownList><br /><br />
			<asp:Label ID ="lblMessage" runat="server" Text="" ForeColor="Red" ></asp:Label>
           
			<br />
			<asp:LinkButton id="lnkDispPercentile" style="Z-INDEX: 203; LEFT: 254px; POSITION: absolute; TOP: 80px" runat="server" Visible="False">
				<a href="DisplayPercentiles.aspx">Click here to view percentiles</a>
			</asp:LinkButton>
            <br /><br />
            
            <br />	
        </form>
	</body>
</html>


 
 
 