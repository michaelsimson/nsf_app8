Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Reports_StatisticsOnInvitees
    Inherits System.Web.UI.Page
    Dim Year As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' connect to the peoducts database
        Year = Now.Year
        Dim commandString As String = "SELECT ContestCode as ProductCode from contestcategory where contestyear = " & Year & " and (NationalSelectionCriteria is null OR NationalSelectionCriteria ='I') and NationalFinalsStatus = 'Active' order by ContestCategoryID"
        Dim ContestCodes As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, commandString)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Contest", Type.GetType("System.String"))
        dt.Columns.Add("Invitees_" & year - 2 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("PaidReg_" & year - 2 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Paid_" & year - 2 & "", Type.GetType("System.Decimal"))
        dt.Columns.Add("Attend_" & year - 2 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Attend_" & year - 2 & "", Type.GetType("System.Decimal"))
        dt.Columns.Add("Invitees_" & year - 1 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("Paid_" & year - 1 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Paid_" & year - 1 & "", Type.GetType("System.Decimal"))
        dt.Columns.Add("Attend_" & year - 1 & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Attend_" & year - 1 & "", Type.GetType("System.Decimal"))
        dt.Columns.Add("Invitees_" & year & "", Type.GetType("System.Int32"))
        dt.Columns.Add("Paid_" & year & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Paid_" & year & "", Type.GetType("System.Decimal"))
        dt.Columns.Add("Attend_" & year & "", Type.GetType("System.Int32"))
        dt.Columns.Add("%Attend_" & year & "", Type.GetType("System.Decimal"))
        Dim SqlStr As String
        Dim Invitee1 As Integer = 0, Paid1 As Integer = 0, Attend1 As Integer = 0, Invitee2 As Integer = 0, Paid2 As Integer = 0, Attend2 As Integer = 0, Invitee3 As Integer = 0, Paid3 As Integer = 0, Attend3 As Integer = 0
        While ContestCodes.Read()
            SqlStr = "select t1.contestyear,sum(t1.invitee) as Invitee,sum(t1.paid) as paid,sum(t1.attend) as Attend from (("
            SqlStr = SqlStr & "select contestyear,count(*) as Invitee,0 as Paid,0 as attend from contestant where  Eventid=2 and PaymentReference is Not NUll and ProductCode='" & ContestCodes("ProductCode") & "' and  Nationalinvitee=1 and contestYear in (" & Year - 2 & "," & Year - 1 & "," & Year & ") Group by contestyear )"
            SqlStr = SqlStr & " Union(select contestyear,0 as Invitee,count(*) as Paid,0 as attend from contestant where  Eventid=1 and PaymentReference is Not NUll and ProductCode='" & ContestCodes("ProductCode") & "' and contestYear in (" & Year - 2 & "," & Year - 1 & "," & Year & ") Group by contestyear ) "
            SqlStr = SqlStr & "Union select contestyear,0 as Invitee, 0 as paid,count(*) as Attend from contestant where  Eventid=1 and (Score1+Score2+Score3)>0 and ProductCode='" & ContestCodes("ProductCode") & "' and contestYear in (" & Year - 2 & "," & Year - 1 & "," & Year & ") Group by contestyear) t1 Group by t1.ContestYear order by  t1.ContestYear"
            Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SqlStr)
            Dim i As Integer = 0
            dr = dt.NewRow()
            While Reader.Read()
                dr("Contest") = ContestCodes("ProductCode")
                If Reader("contestyear") = Year - 2 Then
                    dr("Invitees_" & Year - 2 & "") = Reader("Invitee")
                    Invitee1 = Invitee1 + Reader("Invitee")
                    dr("PaidReg_" & Year - 2 & "") = Reader("paid")
                    Paid1 = Paid1 + Reader("paid")
                    dr("%Paid_" & Year - 2 & "") = String.Format("{0:N2}", (Reader("Paid") / Reader("Invitee")) * 100)
                    dr("Attend_" & Year - 2 & "") = Reader("Attend")
                    Attend1 = Attend1 + Reader("Attend")
                    dr("%Attend_" & Year - 2 & "") = String.Format("{0:N2}", (Reader("Attend") / Reader("Invitee")) * 100)
                ElseIf Reader("contestyear") = Year - 1 Then
                    dr("Invitees_" & Year - 1 & "") = Reader("Invitee")
                    Invitee2 = Invitee2 + Reader("Invitee")
                    dr("Paid_" & Year - 1 & "") = Reader("paid")
                    Paid2 = Paid2 + Reader("paid")
                    dr("%Paid_" & Year - 1 & "") = String.Format("{0:N2}", (Reader("Paid") / Reader("Invitee")) * 100)
                    dr("Attend_" & Year - 1 & "") = Reader("Attend")
                    Attend2 = Attend2 + Reader("Attend")
                    dr("%Attend_" & Year - 1 & "") = String.Format("{0:N2}", (Reader("Attend") / Reader("Invitee")) * 100)
                ElseIf Reader("contestyear") = Year Then
                    dr("Invitees_" & Year & "") = Reader("Invitee")
                    Invitee3 = Invitee3 + Reader("Invitee")
                    dr("Paid_" & Year & "") = Reader("paid")
                    Paid3 = Paid3 + Reader("paid")
                    dr("%Paid_" & Year & "") = String.Format("{0:N2}", (Reader("Paid") / Reader("Invitee")) * 100)
                    dr("Attend_" & Year & "") = Reader("Attend")
                    Attend3 = Attend3 + Reader("Attend")
                    dr("%Attend_" & Year & "") = String.Format("{0:N2}", (Reader("Attend") / Reader("Invitee")) * 100)
                End If
            End While
            Reader.Close()
            dt.Rows.Add(dr)
        End While
        ContestCodes.Close()
        Try
            dr = dt.NewRow()
            dr("Contest") = "Total"
            dr("Invitees_" & Year - 2 & "") = Invitee1
            dr("PaidReg_" & Year - 2 & "") = Paid1
            dr("%Paid_" & Year - 2 & "") = String.Format("{0:N2}", (Paid1 / Invitee1) * 100)
            dr("Attend_" & Year - 2 & "") = Attend1
            dr("%Attend_" & Year - 2 & "") = String.Format("{0:N2}", (Attend1 / Invitee1) * 100)
            dr("Invitees_" & Year - 1 & "") = Invitee2
            dr("Paid_" & Year - 1 & "") = Paid2
            dr("%Paid_" & Year - 1 & "") = String.Format("{0:N2}", (Paid2 / Invitee2) * 100)
            dr("Attend_" & Year - 1 & "") = Attend2
            dr("%Attend_" & Year - 1 & "") = String.Format("{0:N2}", (Attend2 / Invitee2) * 100)
            dr("Invitees_" & Year & "") = Invitee3
            dr("Paid_" & Year & "") = Paid3
            dr("%Paid_" & Year & "") = String.Format("{0:N2}", (Paid3 / Invitee3) * 100)
            dr("Attend_" & Year & "") = Attend3
            dr("%Attend_" & Year & "") = String.Format("{0:N2}", (Attend3 / Invitee3) * 100)
            dt.Rows.Add(dr)
        Catch ex As Exception

        End Try
        dgStatistics.DataSource = dt
        dgStatistics.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=StstisticsOnInvitees.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dgStatistics.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
