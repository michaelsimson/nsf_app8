<%@ Page Language="C#" AutoEventWireup="true" CodeFile="xlReport2.aspx.cs" Inherits="Reports_xlReport2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="Form1" method="post" runat="server">
			<asp:DataGrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 80px; POSITION: absolute; TOP: 64px" runat="server"
				Height="248px" Width="432px" AutoGenerateColumns="False">
				<FooterStyle Wrap="False"></FooterStyle>
				<SelectedItemStyle Wrap="False"></SelectedItemStyle>
				<EditItemStyle Wrap="False"></EditItemStyle>
				<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
				<ItemStyle Wrap="False"></ItemStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Ser#" HeaderText="Ser#"></asp:BoundColumn>
					<asp:BoundColumn DataField="Rank" HeaderText="Rank"></asp:BoundColumn>
					<asp:BoundColumn DataField="FirstName" HeaderText="FirstName"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastName" HeaderText="LastName"></asp:BoundColumn>
					<asp:BoundColumn DataField="DOB" HeaderText="DOB"></asp:BoundColumn>
					<asp:BoundColumn DataField="Gr" HeaderText="Gr"></asp:BoundColumn>
					<asp:BoundColumn DataField="SpellingBee" HeaderText="SpBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="Vocabulary" HeaderText="VocBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="MathBee" HeaderText="MathBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="Geography" HeaderText="GeogBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="EssayWriting" HeaderText="EssayBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="PublicSpeaking" HeaderText="PSBee"></asp:BoundColumn>
					<asp:BoundColumn DataField="Day1" HeaderText="Day1"></asp:BoundColumn>
					<asp:BoundColumn DataField="Day2" HeaderText="Day2"></asp:BoundColumn>
				</Columns>
				<PagerStyle Wrap="False"></PagerStyle>
			</asp:DataGrid>
			<asp:Button id="btnSave" style="Z-INDEX: 102; LEFT: 568px; POSITION: absolute; TOP: 24px" runat="server"
				Text="Save File" onclick="btnSave_Click"></asp:Button>
			<asp:Label id="lblChapter" style="Z-INDEX: 103; LEFT: 208px; POSITION: absolute; TOP: 16px"
				runat="server" Width="144px"></asp:Label>
		</form>
</body>
</html>


 
 
 