<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tmcList.aspx.cs" Inherits="Reports_tmcList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <h3 align="center">Registrations by Chapter</h3>
		<h4>Please select one</h4>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id="DataGrid1" runat="server"
				AutoGenerateColumns="False" AllowSorting="true" Width="488px" BorderColor="#999999" BorderStyle="None" 
				BorderWidth="1px" BackColor="White" CellPadding="3" GridLines="Vertical" OnSortCommand="DataGrid1_Sort">
				<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="name" HeaderText="Chapter" SortExpression="name"></asp:BoundColumn>
					<asp:BoundColumn DataField="state" HeaderText="State"></asp:BoundColumn>
					<asp:BoundColumn DataField="date" HeaderText="Date" SortExpression="date"></asp:BoundColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="chapterid" DataNavigateUrlFormatString="xlReport1.aspx?Chap={0}"
						HeaderText="Registrations by Child (Matrix)" NavigateUrl="xlReport1.aspx">
						<HeaderStyle Width="130px"></HeaderStyle>
					</asp:HyperLinkColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="chapterid" DataNavigateUrlFormatString="xlReportRegByContest.aspx?Chap={0}"
						HeaderText="Registrations by Contest (Check-in List)" NavigateUrl="xlReportRegByContest.aspx">
						<HeaderStyle Width="130px"></HeaderStyle>
					</asp:HyperLinkColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			
			<asp:Button id="btnBack" runat="server" Text="Back" OnClick="btnBack_Click"  ></asp:Button>
		</form>
</body>
</html>


 
 
 