using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;

public partial class Reports_tmcByDate : System.Web.UI.Page
{
    string contestYear; 
    protected void Page_Load(object sender, EventArgs e)
    {
        contestYear =
            System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        GetList();
    }
    private void GetList()
    {
        // connect to the peoducts database
        string connectionString = 
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        DateTime startDate = DateTime.Parse("3/1/" + contestYear);

        while (startDate.DayOfWeek.ToString() != "Saturday")
            startDate = startDate.AddDays(1);
        startDate = startDate.AddDays(7);

        //lblTest.Text = startDate.ToShortDateString();
        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        
        // get records from the products table
        DateTime where1, where2;
        switch (ddlWeek.SelectedValue)
        {
            case "1":
                where1 = startDate.AddDays(7);
                where2 = startDate.AddDays(8);
                break;
            case "2":
                where1 = startDate.AddDays(14);
                where2 = startDate.AddDays(15);
                break;
            case "3":
                where1 = startDate.AddDays(21);
                where2 = startDate.AddDays(22);
                break;
            case "4":
                where1 = startDate.AddDays(28);
                where2 = startDate.AddDays(29);
                break;
            case "5":
                where1 = startDate.AddDays(35);
                where2 = startDate.AddDays(36);
                break;
            case "6":
                where1 = startDate.AddDays(42);
                where2 = startDate.AddDays(43);
                break;
            case "7":
                where1 = startDate.AddDays(49);
                where2 = startDate.AddDays(50);
                break;
            case "8":
                where1 = startDate.AddDays(56);
                where2 = startDate.AddDays(57);
                break;
            default:
                where1 = startDate.AddDays(63);
                where2 = startDate.AddDays(64);
                break;
        }
        string commandString = "Select NSFChapterID, ContestDate, ContestCategoryID from Contest " +
            " Where ContestDate is not null and NSFChapterID is not null and ContestDate >= '" + Convert.ToDateTime(where1) + "' AND ContestDate <= '" + Convert.ToDateTime(where2) + "' AND Contest_Year ="+contestYear+" ORDER BY NSFChapterID,ContestCategoryID, ContestDate";


        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        DataSet dsContests = new DataSet();
        daContests.Fill(dsContests);

        DataGrid1.DataSource = dsContests.Tables[0];
        DataGrid1.DataBind();
    }

    protected void ddlWeek_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        GetList();
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {

        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select City, State from Chapter where ChapterID = " + idNumber;
        SqlDataAdapter daChapters = new SqlDataAdapter(commandString, connection);
        DataSet dsChapters = new DataSet();
        daChapters.Fill(dsChapters);

        if (dsChapters.Tables[0].Rows.Count > 0)
            retValue = dsChapters.Tables[0].Rows[0].ItemArray[0].ToString() + ", " +
                dsChapters.Tables[0].Rows[0].ItemArray[1].ToString();
        return retValue;
    }
    public string GetLabels(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);

        string commandString2 = "Select ContestDesc from ContestCategory where ContestCategoryID = " + idNumber;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        string retValue;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        reader2.Read();
        retValue = reader2.GetString(0);
        // close connection return value
        connection.Close();
        //return retValue;
        return retValue;
    }
}
