using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_DisplayPercentiles : System.Web.UI.Page
{
    int Year = DateTime.Now.Year;//Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
    string contestType;
     DataSet dsContestants;
    DataTable dt2 = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        contestType = ddlContest.SelectedValue;
        ReadAllJSBScores();
    }
   
    void ReadAllJSBScores()
    {
        // connect to the peoducts database
        string connectionString =
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select ChildNumber, ContestCode, contestant_id, " +
            "Score1, Score2, Rank, BadgeNumber,ContestCategoryID  from Contestant " +
            " Where Score1 IS NOT NULL AND Score2 IS NOT NULL  AND ContestYear=" + ddlYear.SelectedValue + //  Year + 
           " AND (Score1>0 OR Score2>0) AND ProductCode = '" + contestType + "' And EventId=2 ORDER BY ChapterID";
        // create the command object and set its
       // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        if (dsContestants.Tables[0].Rows.Count > 29)
            //ProcessData(dsContestants);
            ProcessData();
        else
        {
            //Response.Write("No records found!");
            DataTable dt3 = new DataTable();
            dt3.Columns.Add("No data to display");
            //DataGrid1.Caption = contestType + ": Results can not be displayed as the sample size is too small!";
            //DataGrid1.DataSource = dt3;
            //DataGrid1.DataBind();
        }
    }
    //void ProcessData(DataSet ds)
    //{
    //    DataTable dt = new DataTable();
    //    DataRow dr;
    //    dt.Columns.Add("Contestant", typeof(int));
    //    dt.Columns.Add("TotalScore", typeof(int));
    //    //dt.Columns.Add("Percentile",typeof(float));
    //    //dt.Columns.Add("Count",typeof(int));
    //    int[] aryTscore = new int[ds.Tables[0].Rows.Count];
    //   for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //    {
    //        dr = dt.NewRow();
    //        dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
    //        dr["TotalScore"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
    //            Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
    //        aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
    //            Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
    //        dt.Rows.Add(dr);
    //    }

    //    Array.Sort(aryTscore);
    //    float pc;
    //    dt2 = new DataTable();
    //    dt2.Columns.Add("Score", typeof(int));
    //    dt2.Columns.Add("" + contestType + "-Percentile", typeof(string));
    //    dt2.Columns.Add("" + contestType + "-Count", typeof(int));
    //    dt2.Columns.Add("" + contestType + "-CumeCount", typeof(int));
    //    dr = dt2.NewRow();
    //    dr["Score"] = aryTscore[0];
    //    int cCount = 0, tCount = 0; //cumulative
    //    int lastScore = aryTscore[0];
    //    tCount = aryTscore.Length;
    //    for (int j = 0; j < aryTscore.Length; j++)
    //    {
    //        if (lastScore == aryTscore[j])
    //        {
    //            cCount++;
    //        }
    //        else
    //        {
    //            lastScore = aryTscore[j];
    //            dr["" + contestType + "-Count"] = cCount;
    //            dr["" + contestType + "-CumeCount"] = tCount;
    //            pc = (float)tCount / aryTscore.Length;
                
    //            dr["" + contestType + "-Percentile"] = String.Format("{0:F1}", 100 * Math.Round(pc, 3));
    //            dt2.Rows.Add(dr);
    //            dr = dt2.NewRow();
    //            dr["Score"] = aryTscore[j];
    //            tCount = tCount - cCount;
    //            cCount = 1;
                
    //        }
    //    }
    //    dr["" + contestType + "-Count"] = cCount;
    //    dr["" + contestType + "-CumeCount"] = tCount;
    //    pc = (float)tCount / aryTscore.Length;
    //    dr["" + contestType + "-Percentile"] = String.Format("{0:F1}", 100 * Math.Round(pc, 3));
    //    dt2.Rows.Add(dr);
    //    DataGrid1.Caption = contestType + " as of " + ddlYear.SelectedValue  ;//DateTime.Today.ToShortDateString();
    //    DataGrid1.DataSource = dt2;
    //    DataGrid1.DataBind();

    //}
    void ProcessData()
    {
        DataTable dt = new DataTable();
        DataSet dsPercentiles = new DataSet();


        string commandString = "Select TotalScore,TotalPercentile,Total_Count,Total_Cume_Count From PercentilesByGrade Where ContestYear=" + ddlYear.SelectedValue + //  Year + 
          " AND ProductCode = '" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (4) And EventId=2 ORDER BY TotalScore";
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daPercentiles = new SqlDataAdapter(commandString, Application["ConnectionString"].ToString());
        dsPercentiles = new DataSet();
        daPercentiles.Fill(dsPercentiles);

        DataGrid1.Caption = contestType + " as of " + ddlYear.SelectedValue;//DateTime.Today.ToShortDateString();
        DataGrid1.DataSource = dsPercentiles;
        DataGrid1.DataBind();


    
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=PercentRankOf" + contestType + "_" + ddlYear.SelectedValue + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
           ReadAllJSBScores();
        }
}
