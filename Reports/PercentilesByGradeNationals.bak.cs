using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;


public partial class Reports_PercentilesByGradeNationals : System.Web.UI.Page
{
    string Year;
    string contestType;
    DataSet dsContestants;
    DataTable dt2 = new DataTable();

    protected void Page_Load(object sender, System.EventArgs e)
    {
        // Put user code to initialize the page here
        Year = DateTime.Now.Year.ToString();   //System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        contestType = ddlContest.SelectedValue;
        ReadAllScores();

    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion

    void ReadAllScores()
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
            "Contestant.Score1, Contestant.Score2, Contestant.Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID, Child.GRADE  from Contestant " +
            "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber " +
            " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear='" +
            Year + "' AND (Contestant.Score1 >= 1 OR Contestant.Score2 >= 1) AND Contestant.BadgeNumber like '" + contestType + "%'  AND Contestant.ChapterID = 1 ORDER BY Contestant.ChapterID";

        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        //if(dsContestants.Tables[0].Rows.Count > 29)
        ProcessData(dsContestants);
        //else 
        //{
        //	Label1.Text = "%tile are not available because the number of contestants is less than 30.";
        //}
    }

    void ProcessData(DataSet ds)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Contestant", typeof(int));
        dt.Columns.Add("TotalScore", typeof(int));
        dt.Columns.Add("Grade", typeof(int));

        int[] aryTscore = new int[ds.Tables[0].Rows.Count];
        int[] grScore = new int[ds.Tables[0].Rows.Count];
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
            dr["TotalScore"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
            dr["Grade"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            grScore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
            dt.Rows.Add(dr);
        }

        SortArrays(aryTscore, grScore);

        dt2 = new DataTable();
        dt2.Columns.Add("TotalScore", typeof(int));
        switch (contestType)
        {
            case "MB1":
                dt2.Columns.Add("Gr1%", typeof(string));
                dt2.Columns.Add("Gr2%", typeof(string));
                dt2.Columns.Add("Gr1%tile", typeof(string));
                dt2.Columns.Add("Gr1cnt", typeof(string));
                dt2.Columns.Add("Gr1cum", typeof(string));
                dt2.Columns.Add("Gr2%tile", typeof(string));
                dt2.Columns.Add("Gr2cnt", typeof(string));
                dt2.Columns.Add("Gr2cum", typeof(string));
                break;
            case "MB2":
                dt2.Columns.Add("Gr3%", typeof(string));
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr3%tile", typeof(string));
                dt2.Columns.Add("Gr3cnt", typeof(string));
                dt2.Columns.Add("Gr3cum", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));
                break;
            case "MB3":
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                break;
            case "MB4":
                dt2.Columns.Add("Gr9%", typeof(string));
                dt2.Columns.Add("Gr10%", typeof(string));
                dt2.Columns.Add("Gr9%tile", typeof(string));
                dt2.Columns.Add("Gr9cnt", typeof(string));
                dt2.Columns.Add("Gr9cum", typeof(string));
                dt2.Columns.Add("Gr10%tile", typeof(string));
                dt2.Columns.Add("Gr10cnt", typeof(string));
                dt2.Columns.Add("Gr10cum", typeof(string));
                break;
            case "JSB":
                dt2.Columns.Add("Gr1%", typeof(string));
                dt2.Columns.Add("Gr2%", typeof(string));
                dt2.Columns.Add("Gr3%", typeof(string));
                dt2.Columns.Add("Gr1%tile", typeof(string));
                dt2.Columns.Add("Gr1cnt", typeof(string));
                dt2.Columns.Add("Gr1cum", typeof(string));
                dt2.Columns.Add("Gr2%tile", typeof(string));
                dt2.Columns.Add("Gr2cnt", typeof(string));
                dt2.Columns.Add("Gr2cum", typeof(string));
                dt2.Columns.Add("Gr3%tile", typeof(string));
                dt2.Columns.Add("Gr3cnt", typeof(string));
                dt2.Columns.Add("Gr3cum", typeof(string));
                break;
            case "SSB":
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                break;
            case "JVB":
            case "JSC":
                dt2.Columns.Add("Gr1%", typeof(string));
                dt2.Columns.Add("Gr2%", typeof(string));
                dt2.Columns.Add("Gr3%", typeof(string));
                dt2.Columns.Add("Gr1%tile", typeof(string));
                dt2.Columns.Add("Gr1cnt", typeof(string));
                dt2.Columns.Add("Gr1cum", typeof(string));
                dt2.Columns.Add("Gr2%tile", typeof(string));
                dt2.Columns.Add("Gr2cnt", typeof(string));
                dt2.Columns.Add("Gr2cum", typeof(string));
                dt2.Columns.Add("Gr3%tile", typeof(string));
                dt2.Columns.Add("Gr3cnt", typeof(string));
                dt2.Columns.Add("Gr3cum", typeof(string));
                break;
            case "ISC":
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));               
                break;
            case "IVB":
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                break;
            case "SVB":
            case "BB":
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr9%", typeof(string));
                dt2.Columns.Add("Gr10%", typeof(string));
                dt2.Columns.Add("Gr11%", typeof(string));
                dt2.Columns.Add("Gr12%", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                dt2.Columns.Add("Gr9%tile", typeof(string));
                dt2.Columns.Add("Gr9cnt", typeof(string));
                dt2.Columns.Add("Gr9cum", typeof(string));
                dt2.Columns.Add("Gr10%tile", typeof(string));
                dt2.Columns.Add("Gr10cnt", typeof(string));
                dt2.Columns.Add("Gr10cum", typeof(string));
                dt2.Columns.Add("Gr11%tile", typeof(string));
                dt2.Columns.Add("Gr11cnt", typeof(string));
                dt2.Columns.Add("Gr11cum", typeof(string));
                dt2.Columns.Add("Gr12%tile", typeof(string));
                dt2.Columns.Add("Gr12cnt", typeof(string));
                dt2.Columns.Add("Gr12cum", typeof(string));
                break;
            case "JGB":
                dt2.Columns.Add("Gr1%", typeof(string));
                dt2.Columns.Add("Gr2%", typeof(string));
                dt2.Columns.Add("Gr3%", typeof(string));
                dt2.Columns.Add("Gr1%tile", typeof(string));
                dt2.Columns.Add("Gr1cnt", typeof(string));
                dt2.Columns.Add("Gr1cum", typeof(string));
                dt2.Columns.Add("Gr2%tile", typeof(string));
                dt2.Columns.Add("Gr2cnt", typeof(string));
                dt2.Columns.Add("Gr2cum", typeof(string));
                dt2.Columns.Add("Gr3%tile", typeof(string));
                dt2.Columns.Add("Gr3cnt", typeof(string));
                dt2.Columns.Add("Gr3cum", typeof(string));
                break;
            case "SGB":
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                break;
            case "EW1":
                dt2.Columns.Add("Gr3%", typeof(string));
                dt2.Columns.Add("Gr4%", typeof(string));
                dt2.Columns.Add("Gr5%", typeof(string));
                dt2.Columns.Add("Gr3%tile", typeof(string));
                dt2.Columns.Add("Gr3cnt", typeof(string));
                dt2.Columns.Add("Gr3cum", typeof(string));
                dt2.Columns.Add("Gr4%tile", typeof(string));
                dt2.Columns.Add("Gr4cnt", typeof(string));
                dt2.Columns.Add("Gr4cum", typeof(string));
                dt2.Columns.Add("Gr5%tile", typeof(string));
                dt2.Columns.Add("Gr5cnt", typeof(string));
                dt2.Columns.Add("Gr5cum", typeof(string));
                break;
            case "EW2":
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                break;
            case "EW3":
                dt2.Columns.Add("Gr9%", typeof(string));
                dt2.Columns.Add("Gr10%", typeof(string));
                dt2.Columns.Add("Gr11%", typeof(string));
                dt2.Columns.Add("Gr12%", typeof(string));
                dt2.Columns.Add("Gr9%tile", typeof(string));
                dt2.Columns.Add("Gr9cnt", typeof(string));
                dt2.Columns.Add("Gr9cum", typeof(string));
                dt2.Columns.Add("Gr10%tile", typeof(string));
                dt2.Columns.Add("Gr10cnt", typeof(string));
                dt2.Columns.Add("Gr10cum", typeof(string));
                dt2.Columns.Add("Gr11%tile", typeof(string));
                dt2.Columns.Add("Gr11cnt", typeof(string));
                dt2.Columns.Add("Gr11cum", typeof(string));
                dt2.Columns.Add("Gr12%tile", typeof(string));
                dt2.Columns.Add("Gr12cnt", typeof(string));
                dt2.Columns.Add("Gr12cum", typeof(string));
                break;
            case "SSC":
            case "PS1":
                dt2.Columns.Add("Gr6%", typeof(string));
                dt2.Columns.Add("Gr7%", typeof(string));
                dt2.Columns.Add("Gr8%", typeof(string));
                dt2.Columns.Add("Gr6%tile", typeof(string));
                dt2.Columns.Add("Gr6cnt", typeof(string));
                dt2.Columns.Add("Gr6cum", typeof(string));
                dt2.Columns.Add("Gr7%tile", typeof(string));
                dt2.Columns.Add("Gr7cnt", typeof(string));
                dt2.Columns.Add("Gr7cum", typeof(string));
                dt2.Columns.Add("Gr8%tile", typeof(string));
                dt2.Columns.Add("Gr8cnt", typeof(string));
                dt2.Columns.Add("Gr8cum", typeof(string));
                break;
            case "PS3":
                dt2.Columns.Add("Gr9%", typeof(string));
                dt2.Columns.Add("Gr10%", typeof(string));
                dt2.Columns.Add("Gr11%", typeof(string));
                dt2.Columns.Add("Gr12%", typeof(string));
                dt2.Columns.Add("Gr9%tile", typeof(string));
                dt2.Columns.Add("Gr9cnt", typeof(string));
                dt2.Columns.Add("Gr9cum", typeof(string));
                dt2.Columns.Add("Gr10%tile", typeof(string));
                dt2.Columns.Add("Gr10cnt", typeof(string));
                dt2.Columns.Add("Gr10cum", typeof(string));
                dt2.Columns.Add("Gr11%tile", typeof(string));
                dt2.Columns.Add("Gr11cnt", typeof(string));
                dt2.Columns.Add("Gr11cum", typeof(string));
                dt2.Columns.Add("Gr12%tile", typeof(string));
                dt2.Columns.Add("Gr12cnt", typeof(string));
                dt2.Columns.Add("Gr12cum", typeof(string));
                break;
        }


        dr = dt2.NewRow();
        if (aryTscore.Length > 0)
            dr["TotalScore"] = aryTscore[0];

        int[] grCount = new int[13];
        int[] grCCount = new int[13];
        int[] grTCount = new int[13];
        float[] pc = new float[13];
        float[] pcile = new float[13];
        for (int k = 0; k < grScore.Length; k++)
        {
            switch (grScore[k])
            {
                case 0:
                case 1:
                    grTCount[1]++;
                    break;
                case 2:
                    grTCount[2]++;
                    break;
                case 3:
                    grTCount[3]++;
                    break;
                case 4:
                    grTCount[4]++;
                    break;
                case 5:
                    grTCount[5]++;
                    break;
                case 6:
                    grTCount[6]++;
                    break;
                case 7:
                    grTCount[7]++;
                    break;
                case 8:
                    grTCount[8]++;
                    break;
                case 9:
                    grTCount[9]++;
                    break;
                case 10:
                    grTCount[10]++;
                    break;
                case 11:
                    grTCount[11]++;
                    break;
                case 12:
                    grTCount[12]++;
                    break;
            }
        }
        int lastScore = 0;
        if (aryTscore.Length > 0)
            lastScore = aryTscore[0];

        int j;
        for (j = 0; j < aryTscore.Length; j++)
        {
            if (lastScore == aryTscore[j])
            {
                if ((grScore[j] == 1) || (grScore[j] == 0))
                {
                    grCount[1] = grCount[1] + 1;
                }
                if (grScore[j] == 2)
                {
                    grCount[2] = grCount[2] + 1;
                }
                if (grScore[j] == 3)
                {
                    grCount[3] = grCount[3] + 1;
                }
                if (grScore[j] == 4)
                {
                    grCount[4] = grCount[4] + 1;
                }
                if (grScore[j] == 5)
                {
                    grCount[5] = grCount[5] + 1;
                }
                if (grScore[j] == 6)
                {
                    grCount[6] = grCount[6] + 1;
                }
                if (grScore[j] == 7)
                {
                    grCount[7] = grCount[7] + 1;
                }
                if (grScore[j] == 8)
                {
                    grCount[8] = grCount[8] + 1;
                }
                if (grScore[j] == 9)
                {
                    grCount[9] = grCount[9] + 1;
                }
                if (grScore[j] == 10)
                {
                    grCount[10] = grCount[10] + 1;
                }
                if (grScore[j] == 11)
                {
                    grCount[11] = grCount[11] + 1;
                }
                if (grScore[j] == 12)
                {
                    grCount[12] = grCount[12] + 1;
                }
            }
            else
            {
                lastScore = aryTscore[j];

                switch (contestType)
                {
                    case "MB1":
                        pc[1] = (float)grCount[1] / grTCount[1];
                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                        grCCount[1] += grCount[1];
                        pcile[1] = (float)grCCount[1] / grTCount[1];
                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                        dr["Gr1cnt"] = grCount[1];
                        dr["Gr1cum"] = grCCount[1];
                        pc[2] = (float)grCount[2] / grTCount[2];
                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                        grCCount[2] += grCount[2];
                        pcile[2] = (float)grCCount[2] / grTCount[2];
                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                        dr["Gr2cnt"] = grCount[2];
                        dr["Gr2cum"] = grCCount[2];
                        break;
                    case "MB2":
                    case "EW1":
                        pc[3] = (float)grCount[3] / grTCount[3];
                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                        grCCount[3] += grCount[3];
                        pcile[3] = (float)grCCount[3] / grTCount[3];
                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                        dr["Gr3cnt"] = grCount[3];
                        dr["Gr3cum"] = grCCount[3];
                        pc[4] = (float)grCount[4] / grTCount[4];
                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                        grCCount[4] += grCount[4];
                        pcile[4] = (float)grCCount[4] / grTCount[4];
                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                        dr["Gr4cnt"] = grCount[4];
                        dr["Gr4cum"] = grCCount[4];
                        pc[5] = (float)grCount[5] / grTCount[5];
                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                        grCCount[5] += grCount[5];
                        pcile[5] = (float)grCCount[5] / grTCount[5];
                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                        dr["Gr5cnt"] = grCount[5];
                        dr["Gr5cum"] = grCCount[5];
                        break;
                    case "MB3":
                    case "EW2":
                    case "SSC":
                    case "PS1":
                        pc[6] = (float)grCount[6] / grTCount[6];
                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                        grCCount[6] += grCount[6];
                        pcile[6] = (float)grCCount[6] / grTCount[6];
                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                        dr["Gr6cnt"] = grCount[6];
                        dr["Gr6cum"] = grCCount[6];
                        pc[7] = (float)grCount[7] / grTCount[7];
                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                        grCCount[7] += grCount[7];
                        pcile[7] = (float)grCCount[7] / grTCount[7];
                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                        dr["Gr7cnt"] = grCount[7];
                        dr["Gr7cum"] = grCCount[7];
                        pc[8] = (float)grCount[8] / grTCount[8];
                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                        grCCount[8] += grCount[8];
                        pcile[8] = (float)grCCount[8] / grTCount[8];
                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                        dr["Gr8cnt"] = grCount[8];
                        dr["Gr8cum"] = grCCount[8];
                        break;
                    case "MB4":
                        pc[9] = (float)grCount[9] / grTCount[9];
                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                        grCCount[9] += grCount[9];
                        pcile[9] = (float)grCCount[9] / grTCount[9];
                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                        dr["Gr9cnt"] = grCount[9];
                        dr["Gr9cum"] = grCCount[9];
                        pc[10] = (float)grCount[10] / grTCount[10];
                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                        grCCount[10] += grCount[10];
                        pcile[10] = (float)grCCount[10] / grTCount[10];
                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                        dr["Gr10cnt"] = grCount[10];
                        dr["Gr10cum"] = grCCount[10];
                        break;

                    case "JSB":
                    case "JGB":
                    case "JSC":
                        pc[1] = (float)grCount[1] / grTCount[1];
                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                        grCCount[1] += grCount[1];
                        pcile[1] = (float)grCCount[1] / grTCount[1];
                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                        dr["Gr1cnt"] = grCount[1];
                        dr["Gr1cum"] = grCCount[1];
                        pc[2] = (float)grCount[2] / grTCount[2];
                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                        grCCount[2] += grCount[2];
                        pcile[2] = (float)grCCount[2] / grTCount[2];
                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                        dr["Gr2cnt"] = grCount[2];
                        dr["Gr2cum"] = grCCount[2];
                        pc[3] = (float)grCount[3] / grTCount[3];
                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                        grCCount[3] += grCount[3];
                        pcile[3] = (float)grCCount[3] / grTCount[3];
                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                        dr["Gr3cnt"] = grCount[3];
                        dr["Gr3cum"] = grCCount[3];
                        break;
                    case "SSB":
                    case "SGB":
                        pc[4] = (float)grCount[4] / grTCount[4];
                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                        grCCount[4] += grCount[4];
                        pcile[4] = (float)grCCount[4] / grTCount[4];
                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                        dr["Gr4cnt"] = grCount[4];
                        dr["Gr4cum"] = grCCount[4];
                        pc[5] = (float)grCount[5] / grTCount[5];
                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                        grCCount[5] += grCount[5];
                        pcile[5] = (float)grCCount[5] / grTCount[5];
                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                        dr["Gr5cnt"] = grCount[5];
                        dr["Gr5cum"] = grCCount[5];
                        pc[6] = (float)grCount[6] / grTCount[6];
                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                        grCCount[6] += grCount[6];
                        pcile[6] = (float)grCCount[6] / grTCount[6];
                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                        dr["Gr6cnt"] = grCount[6];
                        dr["Gr6cum"] = grCCount[6];
                        pc[7] = (float)grCount[7] / grTCount[7];
                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                        grCCount[7] += grCount[7];
                        pcile[7] = (float)grCCount[7] / grTCount[7];
                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                        dr["Gr7cnt"] = grCount[7];
                        dr["Gr7cum"] = grCCount[7];
                        pc[8] = (float)grCount[8] / grTCount[8];
                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                        grCCount[8] += grCount[8];
                        pcile[8] = (float)grCCount[8] / grTCount[8];
                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                        dr["Gr8cnt"] = grCount[8];
                        dr["Gr8cum"] = grCCount[8];
                        break;
                    case "JVB":
                        pc[1] = (float)grCount[1] / grTCount[1];
                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                        grCCount[1] += grCount[1];
                        pcile[1] = (float)grCCount[1] / grTCount[1];
                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                        dr["Gr1cnt"] = grCount[1];
                        dr["Gr1cum"] = grCCount[1];
                        pc[2] = (float)grCount[2] / grTCount[2];
                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                        grCCount[2] += grCount[2];
                        pcile[2] = (float)grCCount[2] / grTCount[2];
                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                        dr["Gr2cnt"] = grCount[2];
                        dr["Gr2cum"] = grCCount[2];
                        pc[3] = (float)grCount[3] / grTCount[3];
                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                        grCCount[3] += grCount[3];
                        pcile[3] = (float)grCCount[3] / grTCount[3];
                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                        dr["Gr3cnt"] = grCount[3];
                        dr["Gr3cum"] = grCCount[3];
                        break;
                    case "IVB":
                        pc[4] = (float)grCount[4] / grTCount[4];
                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                        grCCount[4] += grCount[4];
                        pcile[4] = (float)grCCount[4] / grTCount[4];
                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                        dr["Gr4cnt"] = grCount[4];
                        dr["Gr4cum"] = grCCount[4];
                        pc[5] = (float)grCount[5] / grTCount[5];
                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                        grCCount[5] += grCount[5];
                        pcile[5] = (float)grCCount[5] / grTCount[5];
                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                        dr["Gr5cnt"] = grCount[5];
                        dr["Gr5cum"] = grCCount[5];
                        pc[6] = (float)grCount[6] / grTCount[6];
                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                        grCCount[6] += grCount[6];
                        pcile[6] = (float)grCCount[6] / grTCount[6];
                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                        dr["Gr6cnt"] = grCount[6];
                        dr["Gr6cum"] = grCCount[6];
                        pc[7] = (float)grCount[7] / grTCount[7];
                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                        grCCount[7] += grCount[7];
                        pcile[7] = (float)grCCount[7] / grTCount[7];
                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                        dr["Gr7cnt"] = grCount[7];
                        dr["Gr7cum"] = grCCount[7];
                        break;
                    case "ISC":
                        pc[4] = (float)grCount[4] / grTCount[4];
                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                        grCCount[4] += grCount[4];
                        pcile[4] = (float)grCCount[4] / grTCount[4];
                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                        dr["Gr4cnt"] = grCount[4];
                        dr["Gr4cum"] = grCCount[4];
                        pc[5] = (float)grCount[5] / grTCount[5];
                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                        grCCount[5] += grCount[5];
                        pcile[5] = (float)grCCount[5] / grTCount[5];
                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                        dr["Gr5cnt"] = grCount[5];
                        dr["Gr5cum"] = grCCount[5];                       
                        break;
                    case "SVB":
                    case "BB":
                        pc[8] = (float)grCount[8] / grTCount[8];
                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                        grCCount[8] += grCount[8];
                        pcile[8] = (float)grCCount[8] / grTCount[8];
                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                        dr["Gr8cnt"] = grCount[8];
                        dr["Gr8cum"] = grCCount[8];
                        pc[9] = (float)grCount[9] / grTCount[9];
                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                        grCCount[9] += grCount[9];
                        pcile[9] = (float)grCCount[9] / grTCount[9];
                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                        dr["Gr9cnt"] = grCount[9];
                        dr["Gr9cum"] = grCCount[9];
                        pc[10] = (float)grCount[10] / grTCount[10];
                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                        grCCount[10] += grCount[10];
                        pcile[10] = (float)grCCount[10] / grTCount[10];
                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                        dr["Gr10cnt"] = grCount[10];
                        dr["Gr10cum"] = grCCount[10];
                        pc[11] = (float)grCount[11] / grTCount[11];
                        dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                        grCCount[11] += grCount[11];
                        pcile[11] = (float)grCCount[11] / grTCount[11];
                        dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                        dr["Gr11cnt"] = grCount[11];
                        dr["Gr11cum"] = grCCount[11];
                        if (grTCount[12] == 0)
                        {
                            pc[12] = 0;
                            dr["Gr12%"] = "";
                            dr["Gr12%tile"] = "";
                            dr["Gr12cnt"] = grCount[12];
                            dr["Gr12cum"] = grCCount[12];
                        }
                        else
                        {
                            pc[12] = (float)grCount[12] / grTCount[12];
                            dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                            grCCount[12] += grCount[12];
                            pcile[12] = (float)grCCount[12] / grTCount[12];
                            dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                            dr["Gr12cnt"] = grCount[12];
                            dr["Gr12cum"] = grCCount[12];
                        }
                        break;
                    case "EW3":
                    case "PS3":
                        pc[9] = (float)grCount[9] / grTCount[9];
                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                        grCCount[9] += grCount[9];
                        pcile[9] = (float)grCCount[9] / grTCount[9];
                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                        dr["Gr9cnt"] = grCount[9];
                        dr["Gr9cum"] = grCCount[9];
                        pc[10] = (float)grCount[10] / grTCount[10];
                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                        grCCount[10] += grCount[10];
                        pcile[10] = (float)grCCount[10] / grTCount[10];
                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                        dr["Gr10cnt"] = grCount[10];
                        dr["Gr10cum"] = grCCount[10];
                        pc[11] = (float)grCount[11] / grTCount[11];
                        dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                        grCCount[11] += grCount[11];
                        pcile[11] = (float)grCCount[11] / grTCount[11];
                        dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                        dr["Gr11cnt"] = grCount[11];
                        dr["Gr11cum"] = grCCount[11];
                        if (grTCount[12] == 0)
                        {
                            pc[12] = 0;
                            dr["Gr12%"] = "";
                            dr["Gr12%tile"] = "";
                        }
                        else
                        {
                            pc[12] = (float)grCount[12] / grTCount[12];
                            dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                            grCCount[12] += grCount[12];
                            pcile[12] = (float)grCCount[12] / grTCount[12];
                            dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                        }
                        dr["Gr12cnt"] = grCount[12];
                        dr["Gr12cum"] = grCCount[12];
                        break;
                }
                dt2.Rows.Add(dr);
                dr = dt2.NewRow();
                dr["TotalScore"] = aryTscore[j];
                for (int q = 0; q < 13; q++)
                    grCount[q] = 0;
                if ((grScore[j] == 1) || (grScore[j] == 0))
                {
                    grCount[1] = 1;
                }
                if (grScore[j] == 2)
                {
                    grCount[2] = 1;
                }
                if (grScore[j] == 3)
                {
                    grCount[3] = 1;
                }
                if (grScore[j] == 4)
                {
                    grCount[4] = 1;
                }
                if (grScore[j] == 5)
                {
                    grCount[5] = 1;
                }
                if (grScore[j] == 6)
                {
                    grCount[6] = 1;
                }
                if (grScore[j] == 7)
                {
                    grCount[7] = 1;
                }
                if (grScore[j] == 8)
                {
                    grCount[8] = 1;
                }
                if (grScore[j] == 9)
                {
                    grCount[9] = 1;
                }
                if (grScore[j] == 10)
                {
                    grCount[10] = 1;
                }
                if (grScore[j] == 11)
                {
                    grCount[11] = 1;
                }
                if (grScore[j] == 12)
                {
                    grCount[12] = 1;
                }
            }
        }

        switch (contestType)
        {
            case "MB1":
                pc[1] = (float)grCount[1] / grTCount[1];
                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                grCCount[1] += grCount[1];
                pcile[1] = (float)grCCount[1] / grTCount[1];
                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                dr["Gr1cnt"] = grCount[1];
                dr["Gr1cum"] = grCCount[1];
                pc[2] = (float)grCount[2] / grTCount[2];
                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                grCCount[2] += grCount[2];
                pcile[2] = (float)grCCount[2] / grTCount[2];
                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                dr["Gr2cnt"] = grCount[2];
                dr["Gr2cum"] = grCCount[2];
                break;
            case "MB2":
            case "EW1":
                pc[3] = (float)grCount[3] / grTCount[3];
                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                grCCount[3] += grCount[3];
                pcile[3] = (float)grCCount[3] / grTCount[3];
                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                dr["Gr3cnt"] = grCount[3];
                dr["Gr3cum"] = grCCount[3];
                pc[4] = (float)grCount[4] / grTCount[4];
                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                grCCount[4] += grCount[4];
                pcile[4] = (float)grCCount[4] / grTCount[4];
                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                dr["Gr4cnt"] = grCount[4];
                dr["Gr4cum"] = grCCount[4];
                pc[5] = (float)grCount[5] / grTCount[5];
                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                grCCount[5] += grCount[5];
                pcile[5] = (float)grCCount[5] / grTCount[5];
                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                dr["Gr5cnt"] = grCount[5];
                dr["Gr5cum"] = grCCount[5];
                break;
            case "MB3":
            case "EW2":
            case "SSC":
            case "PS1":
                pc[6] = (float)grCount[6] / grTCount[6];
                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                grCCount[6] += grCount[6];
                pcile[6] = (float)grCCount[6] / grTCount[6];
                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                dr["Gr6cnt"] = grCount[6];
                dr["Gr6cum"] = grCCount[6];
                pc[7] = (float)grCount[7] / grTCount[7];
                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                grCCount[7] += grCount[7];
                pcile[7] = (float)grCCount[7] / grTCount[7];
                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                dr["Gr7cnt"] = grCount[7];
                dr["Gr7cum"] = grCCount[7];
                pc[8] = (float)grCount[8] / grTCount[8];
                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                grCCount[8] += grCount[8];
                pcile[8] = (float)grCCount[8] / grTCount[8];
                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                dr["Gr8cnt"] = grCount[8];
                dr["Gr8cum"] = grCCount[8];
                break;
            case "MB4":
                pc[9] = (float)grCount[9] / grTCount[9];
                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                grCCount[9] += grCount[9];
                pcile[9] = (float)grCCount[9] / grTCount[9];
                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                dr["Gr9cnt"] = grCount[9];
                dr["Gr9cum"] = grCCount[9];
                pc[10] = (float)grCount[10] / grTCount[10];
                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                grCCount[10] += grCount[10];
                pcile[10] = (float)grCCount[10] / grTCount[10];
                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                dr["Gr10cnt"] = grCount[10];
                dr["Gr10cum"] = grCCount[10];
                break;

            case "JSB":
            case "JGB":
            case "JSC":
                pc[1] = (float)grCount[1] / grTCount[1];
                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                grCCount[1] += grCount[1];
                pcile[1] = (float)grCCount[1] / grTCount[1];
                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                dr["Gr1cnt"] = grCount[1];
                dr["Gr1cum"] = grCCount[1];
                pc[2] = (float)grCount[2] / grTCount[2];
                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                grCCount[2] += grCount[2];
                pcile[2] = (float)grCCount[2] / grTCount[2];
                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                dr["Gr2cnt"] = grCount[2];
                dr["Gr2cum"] = grCCount[2];
                pc[3] = (float)grCount[3] / grTCount[3];
                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                grCCount[3] += grCount[3];
                pcile[3] = (float)grCCount[3] / grTCount[3];
                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                dr["Gr3cnt"] = grCount[3];
                dr["Gr3cum"] = grCCount[3];
                break;
            case "SSB":
            case "SGB":
                pc[4] = (float)grCount[4] / grTCount[4];
                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                grCCount[4] += grCount[4];
                pcile[4] = (float)grCCount[4] / grTCount[4];
                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                dr["Gr4cnt"] = grCount[4];
                dr["Gr4cum"] = grCCount[4];
                pc[5] = (float)grCount[5] / grTCount[5];
                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                grCCount[5] += grCount[5];
                pcile[5] = (float)grCCount[5] / grTCount[5];
                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                dr["Gr5cnt"] = grCount[5];
                dr["Gr5cum"] = grCCount[5];
                pc[6] = (float)grCount[6] / grTCount[6];
                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                grCCount[6] += grCount[6];
                pcile[6] = (float)grCCount[6] / grTCount[6];
                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                dr["Gr6cnt"] = grCount[6];
                dr["Gr6cum"] = grCCount[6];
                pc[7] = (float)grCount[7] / grTCount[7];
                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                grCCount[7] += grCount[7];
                pcile[7] = (float)grCCount[7] / grTCount[7];
                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                dr["Gr7cnt"] = grCount[7];
                dr["Gr7cum"] = grCCount[7];
                pc[8] = (float)grCount[8] / grTCount[8];
                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                grCCount[8] += grCount[8];
                pcile[8] = (float)grCCount[8] / grTCount[8];
                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                dr["Gr8cnt"] = grCount[8];
                dr["Gr8cum"] = grCCount[8];
                break;
            case "ISC":
                pc[4] = (float)grCount[4] / grTCount[4];
                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                grCCount[4] += grCount[4];
                pcile[4] = (float)grCCount[4] / grTCount[4];
                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                dr["Gr4cnt"] = grCount[4];
                dr["Gr4cum"] = grCCount[4];
                pc[5] = (float)grCount[5] / grTCount[5];
                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                grCCount[5] += grCount[5];
                pcile[5] = (float)grCCount[5] / grTCount[5];
                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                dr["Gr5cnt"] = grCount[5];
                dr["Gr5cum"] = grCCount[5];               
                break;
            case "JVB":
                pc[1] = (float)grCount[1] / grTCount[1];
                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
                grCCount[1] += grCount[1];
                pcile[1] = (float)grCCount[1] / grTCount[1];
                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
                dr["Gr1cnt"] = grCount[1];
                dr["Gr1cum"] = grCCount[1];
                pc[2] = (float)grCount[2] / grTCount[2];
                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
                grCCount[2] += grCount[2];
                pcile[2] = (float)grCCount[2] / grTCount[2];
                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
                dr["Gr2cnt"] = grCount[2];
                dr["Gr2cum"] = grCCount[2];
                pc[3] = (float)grCount[3] / grTCount[3];
                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
                grCCount[3] += grCount[3];
                pcile[3] = (float)grCCount[3] / grTCount[3];
                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
                dr["Gr3cnt"] = grCount[3];
                dr["Gr3cum"] = grCCount[3];
                break;
            case "IVB":
                pc[4] = (float)grCount[4] / grTCount[4];
                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                grCCount[4] += grCount[4];
                pcile[4] = (float)grCCount[4] / grTCount[4];
                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                dr["Gr4cnt"] = grCount[4];
                dr["Gr4cum"] = grCCount[4];
                pc[5] = (float)grCount[5] / grTCount[5];
                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                grCCount[5] += grCount[5];
                pcile[5] = (float)grCCount[5] / grTCount[5];
                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                dr["Gr5cnt"] = grCount[5];
                dr["Gr5cum"] = grCCount[5];
                pc[6] = (float)grCount[6] / grTCount[6];
                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                grCCount[6] += grCount[6];
                pcile[6] = (float)grCCount[6] / grTCount[6];
                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                dr["Gr6cnt"] = grCount[6];
                dr["Gr6cum"] = grCCount[6];
                pc[7] = (float)grCount[7] / grTCount[7];
                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                grCCount[7] += grCount[7];
                pcile[7] = (float)grCCount[7] / grTCount[7];
                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                dr["Gr7cnt"] = grCount[7];
                dr["Gr7cum"] = grCCount[7];
                break;
            case "SVB":
            case "BB":
                pc[8] = (float)grCount[8] / grTCount[8];
                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                grCCount[8] += grCount[8];
                pcile[8] = (float)grCCount[8] / grTCount[8];
                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                dr["Gr8cnt"] = grCount[8];
                dr["Gr8cum"] = grCCount[8];
                pc[9] = (float)grCount[9] / grTCount[9];
                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                grCCount[9] += grCount[9];
                pcile[9] = (float)grCCount[9] / grTCount[9];
                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                dr["Gr9cnt"] = grCount[9];
                dr["Gr9cum"] = grCCount[9];
                pc[10] = (float)grCount[10] / grTCount[10];
                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                grCCount[10] += grCount[10];
                pcile[10] = (float)grCCount[10] / grTCount[10];
                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                dr["Gr10cnt"] = grCount[10];
                dr["Gr10cum"] = grCCount[10];
                pc[11] = (float)grCount[11] / grTCount[11];
                dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                grCCount[11] += grCount[11];
                pcile[11] = (float)grCCount[11] / grTCount[11];
                dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                dr["Gr11cnt"] = grCount[11];
                dr["Gr11cum"] = grCCount[11];
                if (grTCount[12] == 0)
                {
                    pc[12] = 0;
                    dr["Gr12%"] = "";
                    dr["Gr12%tile"] = "";
                }
                else
                {
                    pc[12] = (float)grCount[12] / grTCount[12];
                    dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                    grCCount[12] += grCount[12];
                    pcile[12] = (float)grCCount[12] / grTCount[12];
                    dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                }
                dr["Gr12cnt"] = grCount[12];
                dr["Gr12cum"] = grCCount[12];
                break;
            case "EW3":
            case "PS3":
                pc[9] = (float)grCount[9] / grTCount[9];
                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                grCCount[9] += grCount[9];
                pcile[9] = (float)grCCount[9] / grTCount[9];
                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                dr["Gr9cnt"] = grCount[9];
                dr["Gr9cum"] = grCCount[9];
                pc[10] = (float)grCount[10] / grTCount[10];
                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                grCCount[10] += grCount[10];
                pcile[10] = (float)grCCount[10] / grTCount[10];
                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                dr["Gr10cnt"] = grCount[10];
                dr["Gr10cum"] = grCCount[10];
                pc[11] = (float)grCount[11] / grTCount[11];
                dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                grCCount[11] += grCount[11];
                pcile[11] = (float)grCCount[11] / grTCount[11];
                dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                dr["Gr11cnt"] = grCount[11];
                dr["Gr11cum"] = grCCount[11];
                if (grTCount[12] == 0)
                {
                    pc[12] = 0;
                    dr["Gr12%"] = "";
                    dr["Gr12%tile"] = "";
                }
                else
                {
                    pc[12] = (float)grCount[12] / grTCount[12];
                    dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
                    grCCount[12] += grCount[12];
                    pcile[12] = (float)grCCount[12] / grTCount[12];
                    dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
                }
                dr["Gr12cnt"] = grCount[12];
                dr["Gr12cum"] = grCCount[12];
                break;
        }
        dt2.Rows.Add(dr);
        DataGrid1.Caption = contestType + " as of " + DateTime.Today.ToShortDateString();
        DataGrid1.DataSource = dt2;
        DataGrid1.DataBind();

    }

    void SortArrays(int[] aryTscore, int[] grScore)
    {
        int x = aryTscore.Length;
        int i;
        int j;
        int temp1, temp2;

        for (i = (x - 1); i >= 0; i--)
        {
            for (j = 1; j <= i; j++)
            {
                if (aryTscore[j - 1] > aryTscore[j])
                {
                    temp1 = aryTscore[j - 1];
                    aryTscore[j - 1] = aryTscore[j];
                    aryTscore[j] = temp1;
                    temp2 = grScore[j - 1];
                    grScore[j - 1] = grScore[j];
                    grScore[j] = temp2;
                }
            }
        }


    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=PercentRankOf" + contestType + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
}
