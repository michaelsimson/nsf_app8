﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;


public partial class Reports_OWkShopRegCounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            populateWkShopRegCount();
        }

    }

    public void populateWkShopRegCount()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            cmdText = "select RW.EventYear, RW.EventDate, RW.ProductGroupCode,RW.ProductCode,(select COUNT(*) from Registration_OnlineWkshop RW1 where RW1.EventYear=RW.EventYear and RW1.EventDate=RW.EventDate and RW1.ProductGroupCode=RW.ProductGroupCode and RW1.ProductCode=RW.ProductCode and RW1.Approved is not null) as PaidReg,(select COUNT(*) from Registration_OnlineWkshop RW1 where RW1.EventYear=RW.EventYear and RW1.EventDate=RW.EventDate and RW1.ProductGroupCode=RW.ProductGroupCode and RW1.ProductCode=RW.ProductCode and RW1.Approved is null) as PendingReg, case when RW.EventDate>=GetDate() then (select RegistrationDeadline from OnlineWSCal where EventYear=RW.EventYear aND ProductGroupId=RW.ProductGroupId AND ProductID=RW.ProductId and EventID=RW.EventId and Date=RW.EventDate) else null end as RegistrationDeadline, case when RW.EventDate>=GetDate() then (select LateFeeDeadLine from OnlineWSCal where EventYear=RW.EventYear aND ProductGroupId=RW.ProductGroupId AND ProductID=RW.ProductId and EventID=RW.EventId and Date=RW.EventDate) else null end as LateFeeDeadLine from Registration_OnlineWkshop RW where Approved is not null Group by EventYear, EventDate, ProductGroupId, ProductGroupCode, ProductId, ProductCode,EventId order by EventYear Desc, EventDate, ProductGroupId, ProductGroupCode, ProductId, ProductCode";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    spnStatus.Visible = false;
                    GrdWkShopRegCounts.DataSource = ds;
                    GrdWkShopRegCounts.DataBind();
                }
                else
                {
                    spnStatus.Visible = true;
                }
            }
            else
            {
                spnStatus.Visible = true;
            }
        }
        catch
        {
        }

    }
}