using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class Reports_xlReportRegByContest : System.Web.UI.Page
{
    int chapID = -1;
    string Year = "";
    int iYear = -1;

    DateTime day1 = DateTime.Parse("1/1/2020");
    DateTime day2 = DateTime.Parse("1/1/2000");

    protected void Page_Load(object sender, EventArgs e)
    {
       
        // Put user code to initialize the page here
        chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        if (!IsPostBack)
        {            
            int year = Convert.ToInt32(DateTime.Now.Year);
            ddlyear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
            ddlyear.SelectedIndex = 1;
            prod(chapID, year);

        }
        if (Session["LoggedIn"].ToString() == "True")
            HyperLink2.Visible = true;
        else
            HyperLink2.Visible = false;

    }
    private void prod(int chapID, int yy)
    {
        ddlprod.Items.Clear();
        ddlprod.Items.Add("All");
        ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("All"));
        int j = 0;
     string str = "Select distinct(e.productid), p.name from contest e, product p where e.productid = p.productid and e.contest_year = " + yy + " and e.NSFchapterid = " + chapID;
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.Connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListItem li = new ListItem();
                String af = reader["productid"].ToString();
                li.Value = af;
                li.Text = reader[1].ToString();
                ddlprod.Items.Add(li);
                j = 1;
               

            }
            cmd.Connection.Close();
            cmd.Connection.Dispose();
            if (j == 0)
            {
                ddlprod.Items.Clear();
                ddlprod.Items.Add("No Contest");
                ddlprod.SelectedIndex = ddlprod.Items.IndexOf(ddlprod.Items.FindByValue("No Contest"));
            }
        }
        catch (Exception a)
        {
            lblChapter.Text = a.ToString();
        }
    }
    private int GetData(int chapID, int contestYear)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@in_cont_chapterId", chapID);
            param[1] = new SqlParameter("@in_cont_year", contestYear);
            SqlCommand cmd = new SqlCommand("GetregistrationDetailsByContest", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("in_cont_chapterId", chapID);
            cmd.Parameters.AddWithValue("in_cont_year", contestYear);
            cmd.Connection.Open();
//            SqlDataSource dsResultsFromQuery = cmd.ExecuteReader();
            grdRegByContest.DataSource = cmd.ExecuteReader();
            grdRegByContest.DataBind();
            cmd.Connection.Close();
            cmd.Connection.Dispose();
        }
        catch (Exception e)
        {
            
        }
        return 0;
    }
    private int GetData(int chapID, int contestYear, int Proid)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@in_cont_chapterId", chapID);
            param[1] = new SqlParameter("@in_cont_year", contestYear);
            SqlCommand cmd = new SqlCommand("GetregistrationDetailsByContestProd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("in_cont_chapterId", chapID);
            cmd.Parameters.AddWithValue("in_cont_year", contestYear);
            cmd.Connection.Open();
            //            SqlDataSource dsResultsFromQuery = cmd.ExecuteReader();
            grdRegByContest.DataSource = cmd.ExecuteReader();
            grdRegByContest.DataBind();
            cmd.Connection.Close();
            cmd.Connection.Dispose();
        }
        catch (Exception e)
        {

        }
        return 0;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        String strChapterName = GetChapterName(chapID);
        strChapterName = strChapterName.Replace(",", "");

        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=CheckinList_" + strChapterName + "_" + Year + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        grdRegByContest.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
        
    }

    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
          Response.Redirect("../VolunteerFunctions.aspx");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ddlprod.SelectedItem.Text == "No Contest")
        { }
        else
        {

            if (ddlprod.SelectedItem.Text == "All")
            {
                //  Response.Write("ALL");
                Year = ddlyear.SelectedItem.Text;
                iYear = Convert.ToInt32(Year);
                lblChapter.Text = Year.ToString() + " ";
                lblChapter.Text = lblChapter.Text + GetChapterName(chapID);
                prod(chapID, iYear);
                GetData(chapID, iYear);

            }

            else
            {
                // Response.Write("YEAR AND PROD");
                Year = ddlyear.SelectedItem.Text;
                string prodi = "";

                foreach (ListItem i in ddlprod.Items)
                {
                    if (i.Selected)
                    {
                        prodi = i.Value;
                        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                        SqlCommand cmd = new SqlCommand("Wrkshoptemp", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("id", Convert.ToInt32(prodi));
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                        cmd.Connection.Dispose();
                    }
                }

                //  Response.Write (prodi);
                int prodid = Convert.ToInt32(ddlprod.SelectedItem.Value);

                iYear = Convert.ToInt32(Year);
                lblChapter.Text = Year.ToString() + " ";
                lblChapter.Text = lblChapter.Text + GetChapterName(chapID);
                //prod(chapID, iYear);
               GetData(chapID, iYear, prodid);


            }
        }
        if (chapID != 1)
        {
            grdRegByContest.Columns[24].Visible = false;
            grdRegByContest.Columns[25].Visible = false;
        }
    }
    protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Year = ddlyear.SelectedItem.Text;
        iYear = Convert.ToInt32(Year);
        prod(chapID, iYear);
    }
}
