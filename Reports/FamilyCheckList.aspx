﻿<%@ Page Language="VB"   EnableEventValidation="false"  %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        loadgrid(GVList)
    End Sub
    
    Private Sub loadgrid(ByVal gv As GridView)
        'GroupConcat Group ConCat in SQl Server
        Dim StrSQl As String = "select I1.AutoMemberID as Memberid ,I1 .FirstName as IndFname ,I1.LastName  as IndLName,I1.Gender as INDGender,I1.Hphone,I1.Cphone,I1.Email,Ch1.ChapterCode,I2.FirstName as SpFname ,I2.LastName  as SpLName,I2.Gender as SpGender,I1.City ,I1.State,LEFT(childName,LEN(childName) - 1)   AS childName  from IndSpouse I1 Inner Join Chapter Ch1 On Ch1.ChapterID = I1.ChapterID  Left Join IndSpouse I2 ON I2.Relationship = I1.AutoMemberID CROSS APPLY (SELECT Distinct C.FIRST_NAME + ' ' + C.LAST_NAME  + ', '"
        StrSQl = StrSQl & " FROM   Contestant AS intern Inner Join Child C ON intern.ChildNumber = C.ChildNumber  AND intern.EventId =1 and intern.ContestYear = YEAR(Getdate()) and PaymentReference is Not Null"
        StrSQl = StrSQl & " WHERE  I1.AutoMemberID  = intern.ParentID FOR XML PATH('')) pre_trimmed (childName)"
        StrSQl = StrSQl & " where I1.AutoMemberID in (select Distinct ParentID from contestant where ContestYear=Year(GetDate()) and EventId=1 and PaymentReference is Not Null) ORDER BY I1.LastName ,I1.FirstName "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
        dt = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Memberid", Type.GetType("System.Int32"))
        dt.Columns.Add("Member_FName", Type.GetType("System.String"))
        dt.Columns.Add("Member_LName", Type.GetType("System.String"))
        dt.Columns.Add("Gender", Type.GetType("System.String"))
        dt.Columns.Add("HPhone", Type.GetType("System.String"))
        dt.Columns.Add("CPhone", Type.GetType("System.String"))
        dt.Columns.Add("Email", Type.GetType("System.String"))
        dt.Columns.Add("ChapterCode", Type.GetType("System.String"))
        dt.Columns.Add("Spouse_FName", Type.GetType("System.String"))
        dt.Columns.Add("Spouse_LName", Type.GetType("System.String"))
        dt.Columns.Add("SpGender", Type.GetType("System.String"))
        dt.Columns.Add("City", Type.GetType("System.String"))
        dt.Columns.Add("State", Type.GetType("System.String"))
        dt.Columns.Add("Contestant1", Type.GetType("System.String"))
        dt.Columns.Add("Contestant2", Type.GetType("System.String"))
        dt.Columns.Add("Contestant3", Type.GetType("System.String"))
        Dim i As Integer
        For i = 0 To (ds.Tables(0).Rows.Count - 1)
            dr = dt.NewRow()
            dr("Memberid") = ds.Tables(0).Rows(i)("Memberid")
            dr("Member_FName") = ds.Tables(0).Rows(i)("IndFName")
            dr("Member_LName") = ds.Tables(0).Rows(i)("IndLName")
            dr("Gender") = ds.Tables(0).Rows(i)("IndGender")
            dr("Hphone") = ds.Tables(0).Rows(i)("Hphone")
            dr("CPhone") = ds.Tables(0).Rows(i)("CPhone")
            dr("Email") = ds.Tables(0).Rows(i)("Email")
            dr("ChapterCode") = ds.Tables(0).Rows(i)("ChapterCode")
            dr("Spouse_FName") = ds.Tables(0).Rows(i)("SpFName")
            dr("Spouse_LName") = ds.Tables(0).Rows(i)("SpLName")
            dr("SpGender") = ds.Tables(0).Rows(i)("SpGender")
            dr("City") = ds.Tables(0).Rows(i)("City")
            dr("State") = ds.Tables(0).Rows(i)("State")
            dr("SpGender") = ds.Tables(0).Rows(i)("SpGender")
            Dim Contestants As String() = ds.Tables(0).Rows(i)("childName").Split(New Char() {","c})
            If Contestants.Length = 1 Then
                dr("Contestant1") = Contestants(0)
                dr("Contestant2") = ""
                dr("Contestant3") = ""
            ElseIf Contestants.Length = 2 Then
                dr("Contestant1") = Contestants(0)
                dr("Contestant2") = Contestants(1)
                dr("Contestant3") = ""
            ElseIf Contestants.Length = 3 Then
                dr("Contestant1") = Contestants(0)
                dr("Contestant2") = Contestants(1)
                dr("Contestant3") = Contestants(2)
            ElseIf Contestants.Length = 4 Then
                dr("Contestant1") = Contestants(0)
                dr("Contestant2") = Contestants(1)
                dr("Contestant3") = Contestants(2) & " " & Contestants(3)
            End If
            dt.Rows.Add(dr)
        Next
        gv.DataSource = dt
        gv.DataBind()
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvtemp As New GridView
        loadgrid(gvtemp)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=FamilyCheckList_" & Now.Year.ToString() & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        gvtemp.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Family Check List</title>
</head>
<body>
    <form id="form1" runat="server">
    <center><h2>Family Check List for the Finals</h2></center>
    <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp;        <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="BtnExport_Click" Width="160px" />
<br /><br />
    <div align="center">
        <asp:GridView ID="GVList" runat="server" BackColor="White" AutoGenerateColumns = "false" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical">
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
             <Columns>
                <asp:BoundField  DataField="MemberID"   HeaderText="MemberID"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Member_LName"   HeaderText="Member LName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Member_FName"   HeaderText="Member FName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Gender"   HeaderText="Gender"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Hphone"   HeaderText="Hphone"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="CPhone"   HeaderText="CPhone"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Email"   HeaderText="Email"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="ChapterCode"   HeaderText="ChapterCode"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >

                <asp:BoundField  DataField="Spouse_LName"   HeaderText="Spouse LName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Spouse_FName"   HeaderText="Spouse FName"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="SpGender"   HeaderText="Gender"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
               <asp:BoundField  DataField="City"   HeaderText="City"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="State"   HeaderText="State"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
               <asp:BoundField  DataField="Contestant1"   HeaderText="Contestant1"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Contestant2"   HeaderText="Contestant2"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <asp:BoundField  DataField="Contestant3"   HeaderText="Contestant3"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
             </Columns> 
    
        </asp:GridView>
    </div>
    </form>
</body>
</html>
