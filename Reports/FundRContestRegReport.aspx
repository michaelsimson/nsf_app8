﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="FundRContestRegReport.aspx.cs" Inherits="Reports_FundRContestRegReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 

       <div align="center" id="dvCoachReport" style="font-size: 26px; width: 1300px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
           runat="server">
           Contest Registration Report
             <br />
           <br />
       </div>
    <div style="clear: both;"></div>

    <table style="margin-left: 450px;">
        <tr>

            <td align="left" nowrap="nowrap" style="font-weight: bold;">Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 140px;">
                <asp:DropDownList ID="ddlYear" runat="server" Width="100px" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Contest&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 120px;">
                <asp:ListBox ID="LstProducts" runat="server" Width="150px" Height="100px" SelectionMode="Multiple">
                    <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                </asp:ListBox>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="BtnGenerateReport" runat="server" Text="Generate Report" OnClick="BtnGenerateReport_Click" />
                <asp:Button ID="BtnExportToExcel" runat="server" Visible="false" Text="Export To Excel" OnClick="BtnExportToExcel_Click" />
            </td>

        </tr>
    </table>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" id="dvFundRContestReg" runat="server" visible="false">


        <div align="center" style="font-weight: bold;">Table 1: Contest Registration Report</div>


        <asp:GridView HorizontalAlign="Center" Width="1250px" RowStyle-HorizontalAlign="Left" ID="GrdFundRContestReg" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdFundRContestReg_PageIndexChanging" >

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventName" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="EventDate" HeaderText="Event Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="StartTime" HeaderText="Start Time"></asp:BoundField>
                <asp:BoundField DataField="EndTime" HeaderText="End Time"></asp:BoundField>
                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Contest"></asp:BoundField>
                <asp:BoundField DataField="First_Name" HeaderText="Child FName"></asp:BoundField>
                <asp:BoundField DataField="Last_Name" HeaderText="Child LName"></asp:BoundField>
                <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                <asp:BoundField DataField="DATE_OF_BIRTH" HeaderText="DOB" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                <asp:BoundField DataField="SchoolName" HeaderText="School Name"></asp:BoundField>

                <asp:BoundField DataField="FirstName" HeaderText="Father FName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Father LName"></asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Fathe E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="Father HPhone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="Father CPhone"></asp:BoundField>
                <asp:BoundField DataField="Approved" HeaderText="Approved"></asp:BoundField>

                <asp:BoundField DataField="MFFName" HeaderText="Mother FName"></asp:BoundField>
                <asp:BoundField DataField="MFLName" HeaderText="Mother LName"></asp:BoundField>
                <asp:BoundField DataField="MFEmail" HeaderText="Mother E-Mail"></asp:BoundField>
                <asp:BoundField DataField="MFCHPhone" HeaderText="Mother HPhone"></asp:BoundField>
                <asp:BoundField DataField="MFCPhone" HeaderText="Mother CPhone"></asp:BoundField>



            </Columns>
        </asp:GridView>


    </div>
</asp:Content>

