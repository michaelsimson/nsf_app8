<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="PercentilesByGrade.aspx.cs" Inherits="Reports_PercentilesByGrade" Title="Percentiles By Grade" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js"></script>
    <div style="text-align: left">
        <script language="javascript" type="text/javascript">
            $(document).on("click", "#btnCutOffScores", function (e) {

                //ShowCurrentTime();
            });
            function calculateScore() {

            }
            function ShowCurrentTime() {

                var year = $("<%=ddlYear.ClientID %>").val();

                var ProductCode = $("<%=ddlContest.ClientID%>").val();
                var contestPeriod = $("<%=ddlcontestdate.ClientID%>").val();
                var event = $("<%=ddlEvent.ClientID%>").val();
                $.ajax({
                    type: "POST",
                    url: "PercentilesByGrade.aspx/fillGrade",
                    data: JSON.stringify({ score: { Year: year, ProductCode: ProductCode, ContestPeriod: contestPeriod, Event: event } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        alert(JSON.stringify(data));
                    },
                    failure: function (response) {

                    }
                });
            }

            $(function (e) {


                var gradeMark1 = document.getElementById("<%=hdnGrade1mark.ClientID%>").value;
                var gradeMark2 = document.getElementById("<%=hdnGrade2mark.ClientID%>").value;
                var gradeMark3 = document.getElementById("<%=hdnGrade3mark.ClientID%>").value;
                var gradeMark4 = document.getElementById("<%=hdnGrade4mark.ClientID%>").value;
                var gradeMark5 = document.getElementById("<%=hdnGrade5mark.ClientID%>").value;
                var gradeMark6 = document.getElementById("<%=hdnGrade6mark.ClientID%>").value;

                var gradeMark7 = document.getElementById("<%=hdnGrade7mark.ClientID%>").value;
                var gradeMark8 = document.getElementById("<%=hdnGrade8mark.ClientID%>").value;
                var gradeMark9 = document.getElementById("<%=hdnGrade9mark.ClientID%>").value;
                var gradeMark10 = document.getElementById("<%=hdnGrade10mark.ClientID%>").value;
                var gradeMark11 = document.getElementById("<%=hdnGrade11mark.ClientID%>").value;
                var gradeMark12 = document.getElementById("<%=hdnGrade12mark.ClientID%>").value;
              

                var totalGrademark = document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value;
                var toatlListMark = document.getElementById("<%=hdnTotalList.ClientID%>").value;



                var grade1ID = document.getElementById("<%=hdnGrade1ID.ClientID%>").value;
                var grade2ID = document.getElementById("<%=hdnGrade2ID.ClientID%>").value;
                var grade3ID = document.getElementById("<%=hdnGrade3ID.ClientID%>").value;

                var totScore1ID = document.getElementById("<%=hdnTotalSocre1ID.ClientID%>").value;
                var totScore2ID = document.getElementById("<%=hdnTotalSocre2ID.ClientID%>").value;
                var totScore3ID = document.getElementById("<%=hdnTotalSocre3ID.ClientID%>").value;

                var totScore1 = document.getElementById("<%=hdnTotalScore1.ClientID%>").value;
                var totScore2 = document.getElementById("<%=hdnTotalScore2.ClientID%>").value;
                var totScore3 = document.getElementById("<%=hdnTotalScore3.ClientID%>").value;
                var totScore4 = document.getElementById("<%=hdnTotalScore4.ClientID%>").value;
                var totScore5 = document.getElementById("<%=hdnTotalScore5.ClientID%>").value;
                var totScore6 = document.getElementById("<%=hdnTotalScore6.ClientID%>").value;
                var totScore7 = document.getElementById("<%=hdnTotalScore7.ClientID%>").value;
                var totScore8 = document.getElementById("<%=hdnTotalScore8.ClientID%>").value;
                var totScore9 = document.getElementById("<%=hdnTotalScore9.ClientID%>").value;
                var totScore10 = document.getElementById("<%=hdnTotalScore10.ClientID%>").value;
                var totScore11 = document.getElementById("<%=hdnTotalScore11.ClientID%>").value;
                var totScore12 = document.getElementById("<%=hdnTotalScore12.ClientID%>").value;

                var prioritymark1 = "";
                var prioritymark2 = "";
                var prioritymark3 = "";
                var prioritymark4 = "";
                var prioritymark5 = "";
                var prioritymark6 = "";
                var prioritymark7 = "";
                var prioritymark8 = "";
                var prioritymark9 = "";
                var prioritymark10 = "";
                var prioritymark11 = "";
                var prioritymark12 = "";


                var totalmark1 = "";
                var totalmark2 = "";
                var totalmark3 = "";
                var totalmark4 = "";
                var totalmark5 = "";
                var totalmark6 = "";
                var totalmark7 = "";
                var totalmark8 = "";
                var totalmark9 = "";
                var totalmark10 = "";
                var totalmark11 = "";
                var totalmark12 = "";

                if (document.getElementById("<%=hdnIsSave.ClientID%>").value == "1") {
                    $("#btnSaveScore").css("display", "block");
                } else {
                    $("#btnSaveScore").css("display", "none");
                }

                if (gradeMark1 != "") {
                    prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    $(".clsGrade1").prop("checked", false);
                    $("#spnGr1mark").text(prioritymark1);
                    $("#spnGr1mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade1" + gradeMark1 + "").prop("checked", true);
                    $("#rbtnGrade1" + gradeMark1 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#spnTotalInvitees").text((totalGrademark == 0 ? "" : totalGrademark));
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList1").removeAttr("disabled");
                }

                if (gradeMark2 != "") {
                    prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    $(".clsGrade2").prop("checked", false);
                    $("#spnGr2mark").text(prioritymark2);
                    $("#spnGr2mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade2" + gradeMark2 + "").prop("checked", true);
                    $("#rbtnGrade2" + gradeMark2 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#spnTotalInvitees").text((totalGrademark == 0 ? "" : totalGrademark));
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList2").removeAttr("disabled");
                }
                if (gradeMark3 != "") {
                    prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    $(".clsGrade3").prop("checked", false);
                    $("#spnGr3mark").text(prioritymark3);
                    $("#spnGr3mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade3" + gradeMark3 + "").prop("checked", true);
                    $("#rbtnGrade3" + gradeMark3 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#spnTotalInvitees").text((totalGrademark == 0 ? "" : totalGrademark));
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList3").removeAttr("disabled");
                }
                if (totScore1 != "") {
                    totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    $(".clsTotal1").prop("checked", false);
                    $("#spnTotalScore1").text(totalmark1);
                    $("#spnTotalScore1").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal1" + totScore1 + "").prop("checked", true);
                    $("#rbtTotal1" + totScore1 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text((toatlListMark == 0 ? "" : toatlListMark));
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList1").removeAttr("disabled");
                }
                if (totScore2 != "") {
                    totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    $(".clsTotal2").prop("checked", false);
                    $("#spnTotalScore2").text(totalmark2);
                    $("#spnTotalScore2").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal2" + totScore2 + "").prop("checked", true);
                    $("#rbtTotal2" + totScore2 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text((toatlListMark == 0 ? "" : toatlListMark));
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList2").removeAttr("disabled");
                }
                if (totScore3 != "") {
                    totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    $(".clsTotal3").prop("checked", false);
                    $("#spnTotalScore3").text(totalmark3);
                    $("#spnTotalScore3").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal3" + totScore3 + "").prop("checked", true);
                    $("#rbtTotal3" + totScore3 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text((toatlListMark == 0 ? "" : toatlListMark));
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList3").removeAttr("disabled");
                }





                if (gradeMark4 != "") {
                    prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade4").prop("checked", false);
                    $("#spnGr4mark").text(prioritymark4);
                    $("#spnGr4mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade4" + gradeMark4 + "").prop("checked", true);
                    $("#rbtnGrade4" + gradeMark4 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);

                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList4").removeAttr("disabled");
                }
                if (gradeMark5 != "") {
                    prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade5").prop("checked", false);
                    $("#spnGr5mark").text(prioritymark5);
                    $("#spnGr5mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade5" + gradeMark5 + "").prop("checked", true);
                    $("#rbtnGrade5" + gradeMark5 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList5").removeAttr("disabled");
                }
                if (gradeMark6 != "") {
                    prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade6").prop("checked", false);
                    $("#spnGr6mark").text(prioritymark6);
                    $("#spnGr6mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade6" + gradeMark6 + "").prop("checked", true);
                    $("#rbtnGrade6" + gradeMark6 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList6").removeAttr("disabled");
                }
                if (gradeMark7 != "") {
                    prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade7").prop("checked", false);
                    $("#spnGr7mark").text(prioritymark7);
                    $("#spnGr7mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade7" + gradeMark7 + "").prop("checked", true);
                    $("#rbtnGrade7" + gradeMark7 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade7" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);
                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList7").removeAttr("disabled");
                }
                if (gradeMark8 != "") {
                    prioritymark8 = $("#rbtnGrade8" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade8").prop("checked", false);
                    $("#spnGr8mark").text(prioritymark8);
                    $("#spnGr8mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade8" + gradeMark8 + "").prop("checked", true);
                    $("#rbtnGrade8" + gradeMark8 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);

                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);

                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                   
                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList8").removeAttr("disabled");
                }

                if (gradeMark9 != "") {
                    prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade9").prop("checked", false);
                    $("#spnGr9mark").text(prioritymark9);
                    $("#spnGr9mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade9" + gradeMark9 + "").prop("checked", true);
                    $("#rbtnGrade9" + gradeMark9 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade8" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList9").removeAttr("disabled");
                }

                if (gradeMark10 != "") {
                    prioritymark10 = $("#rbtnGrade10" + gradeMark10 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade10").prop("checked", false);
                    $("#spnGr10mark").text(prioritymark10);
                    $("#spnGr10mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade10" + gradeMark10 + "").prop("checked", true);
                    $("#rbtnGrade10" + gradeMark10 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade8" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList10").removeAttr("disabled");
                }

                if (gradeMark11 != "") {
                    prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade11").prop("checked", false);
                    $("#spnGr11mark").text(prioritymark11);
                    $("#spnGr11mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade11" + gradeMark11 + "").prop("checked", true);
                    $("#rbtnGrade11" + gradeMark11 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade8" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);
                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark12 != "") {
                        prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark12 = (prioritymark12 == "" ? "0" : prioritymark12);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList11").removeAttr("disabled");
                }

                if (gradeMark12 != "") {
                    prioritymark12 = $("#rbtnGrade12" + gradeMark12 + "").parent().prev()[0].innerHTML;
                    $(".clsGrade12").prop("checked", false);
                    $("#spnGr12mark").text(prioritymark11);
                    $("#spnGr12mark").parent().css("background-color", "#DE1111");
                    $("#rbtnGrade12" + gradeMark11 + "").prop("checked", true);
                    $("#rbtnGrade12" + gradeMark11 + "").parent().prev().css("background-color", "#DE1111");
                    if (gradeMark2 != "") {
                        prioritymark2 = $("#rbtnGrade2" + gradeMark2 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark2 = (prioritymark2 == "" ? "0" : prioritymark2);
                    if (gradeMark3 != "") {
                        prioritymark3 = $("#rbtnGrade3" + gradeMark3 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark3 = (prioritymark3 == "" ? "0" : prioritymark3);
                    if (gradeMark1 != "") {
                        prioritymark1 = $("#rbtnGrade1" + gradeMark1 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark1 = (prioritymark1 == "" ? "0" : prioritymark1);
                    if (gradeMark4 != "") {
                        prioritymark4 = $("#rbtnGrade4" + gradeMark4 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark4 = (prioritymark4 == "" ? "0" : prioritymark4);
                    if (gradeMark5 != "") {
                        prioritymark5 = $("#rbtnGrade5" + gradeMark5 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark5 = (prioritymark5 == "" ? "0" : prioritymark5);
                    if (gradeMark6 != "") {
                        prioritymark6 = $("#rbtnGrade6" + gradeMark6 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark6 = (prioritymark6 == "" ? "0" : prioritymark6);
                    if (gradeMark7 != "") {
                        prioritymark7 = $("#rbtnGrade7" + gradeMark7 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark7 = (prioritymark7 == "" ? "0" : prioritymark7);
                    if (gradeMark8 != "") {
                        prioritymark8 = $("#rbtnGrade8" + gradeMark8 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark8 = (prioritymark8 == "" ? "0" : prioritymark8);
                    if (gradeMark9 != "") {
                        prioritymark9 = $("#rbtnGrade9" + gradeMark9 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark9 = (prioritymark9 == "" ? "0" : prioritymark9);
                    if (gradeMark10 != "") {
                        prioritymark10 = $("#rbtnGrade10" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark10 = (prioritymark10 == "" ? "0" : prioritymark10);
                    if (gradeMark11 != "") {
                        prioritymark11 = $("#rbtnGrade11" + gradeMark11 + "").parent().prev()[0].innerHTML;
                    }
                    prioritymark11 = (prioritymark11 == "" ? "0" : prioritymark11);

                    totalGrademark = parseInt(prioritymark1) + parseInt(prioritymark2) + parseInt(prioritymark3) + parseInt(prioritymark4) + parseInt(prioritymark5) + parseInt(prioritymark6) + parseInt(prioritymark7) + parseInt(prioritymark8) + parseInt(prioritymark9) + parseInt(prioritymark10) + parseInt(prioritymark11) + parseInt(prioritymark12);

                    $("#spnTotalInvitees").text(totalGrademark);
                    $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSavePriorityList12").removeAttr("disabled");
                }

                if (totScore4 != "") {
                    totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal4").prop("checked", false);
                    $("#spnTotalScore4").text(totalmark4);
                    $("#spnTotalScore4").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal4" + totScore4 + "").prop("checked", true);
                    $("#rbtTotal4" + totScore4 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList4").removeAttr("disabled");
                }
                if (totScore5 != "") {
                    totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal5").prop("checked", false);
                    $("#spnTotalScore5").text(totalmark5);
                    $("#spnTotalScore5").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal5" + totScore5 + "").prop("checked", true);
                    $("#rbtTotal5" + totScore5 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);

                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList5").removeAttr("disabled");
                }
                if (totScore6 != "") {
                    totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal6").prop("checked", false);
                    $("#spnTotalScore6").text(totalmark6);
                    $("#spnTotalScore6").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal6" + totScore6 + "").prop("checked", true);
                    $("#rbtTotal6" + totScore6 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList6").removeAttr("disabled");
                }
                if (totScore7 != "") {
                    totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal7").prop("checked", false);
                    $("#spnTotalScore7").text(totalmark7);
                    $("#spnTotalScore7").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal7" + totScore7 + "").prop("checked", true);
                    $("#rbtTotal7" + totScore7 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotal5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList7").removeAttr("disabled");
                }
                if (totScore8 != "") {
                    totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal8").prop("checked", false);
                    $("#spnTotalScore8").text(totalmark8);
                    $("#spnTotalScore8").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal8" + totScore8 + "").prop("checked", true);
                    $("#rbtTotal8" + totScore8 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotall5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList8").removeAttr("disabled");
                }

                if (totScore9 != "") {
                    totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal9").prop("checked", false);
                    $("#spnTotalScore9").text(totalmark9);
                    $("#spnTotalScore9").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal9" + totScore9 + "").prop("checked", true);
                    $("#rbtTotal9" + totScore9 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotall5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);

                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList9").removeAttr("disabled");
                }
                if (totScore10 != "") {
                    totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal10").prop("checked", false);
                    $("#spnTotalScore10").text(totalmark10);
                    $("#spnTotalScore10").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal10" + totScore10 + "").prop("checked", true);
                    $("#rbtTotal10" + totScore10 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotall5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);

                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList10").removeAttr("disabled");
                }

                if (totScore11 != "") {
                    totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal11").prop("checked", false);
                    $("#spnTotalScore11").text(totalmark11);
                    $("#spnTotalScore11").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal11" + totScore11 + "").prop("checked", true);
                    $("#rbtTotal11" + totScore11 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotall5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);

                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore12 != "") {
                        totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark12 = (totalmark12 == "" ? "0" : totalmark12);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList11").removeAttr("disabled");
                }
                if (totScore12 != "") {
                    totalmark12 = $("#rbtTotal12" + totScore12 + "").parent().prev()[0].innerHTML;
                    $(".clsTotal12").prop("checked", false);
                    $("#spnTotalScore12").text(totalmark10);
                    $("#spnTotalScore12").parent().css("background-color", "#03C8FA");
                    $("#rbtTotal12" + totScore12 + "").prop("checked", true);
                    $("#rbtTotal12" + totScore12 + "").parent().prev().css("background-color", "#03C8FA");
                    if (totScore1 != "") {
                        totalmark1 = $("#rbtTotal1" + totScore1 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark1 = (totalmark1 == "" ? "0" : totalmark1);
                    if (totScore2 != "") {
                        totalmark2 = $("#rbtTotal2" + totScore2 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark2 = (totalmark2 == "" ? "0" : totalmark2);
                    if (totScore3 != "") {
                        totalmark3 = $("#rbtTotal3" + totScore3 + "").parent().prev()[0].innerHTML;

                    }
                    totalmark3 = (totalmark3 == "" ? "0" : totalmark3);
                    if (totScore6 != "") {
                        totalmark6 = $("#rbtTotal6" + totScore6 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark6 = (totalmark6 == "" ? "0" : totalmark6);
                    if (totScore7 != "") {
                        totalmark7 = $("#rbtTotal7" + totScore7 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark7 = (totalmark7 == "" ? "0" : totalmark7);
                    if (totScore5 != "") {
                        totalmark5 = $("#rbtTotall5" + totScore5 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark5 = (totalmark5 == "" ? "0" : totalmark5);
                    if (totScore4 != "") {
                        totalmark4 = $("#rbtTotal4" + totScore4 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark4 = (totalmark4 == "" ? "0" : totalmark4);

                    if (totScore8 != "") {
                        totalmark8 = $("#rbtTotal8" + totScore8 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark8 = (totalmark8 == "" ? "0" : totalmark8);
                    if (totScore9 != "") {
                        totalmark9 = $("#rbtTotal9" + totScore9 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark9 = (totalmark9 == "" ? "0" : totalmark9);
                    if (totScore10 != "") {
                        totalmark10 = $("#rbtTotal10" + totScore10 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark10 = (totalmark10 == "" ? "0" : totalmark10);
                    if (totScore11 != "") {
                        totalmark11 = $("#rbtTotal11" + totScore11 + "").parent().prev()[0].innerHTML;
                    }
                    totalmark11 = (totalmark11 == "" ? "0" : totalmark11);


                    toatlListMark = parseInt(totalmark1) + parseInt(totalmark2) + parseInt(totalmark3) + parseInt(totalmark4) + parseInt(totalmark5) + parseInt(totalmark6) + parseInt(totalmark7) + parseInt(totalmark8) + parseInt(totalmark9) + parseInt(totalmark10) + parseInt(totalmark11) + parseInt(totalmark12);
                    $("#splTotalList").text(toatlListMark);
                    $("#splTotalList").parent().css("background-color", "#03C8FA");
                    $("#btnSaveScore").css("display", "block");
                    $("#btnSaveTotList12").removeAttr("disabled");
                }
            });



            $(document).on("click", ".clsGrade1", function (e) {



                var selectedText = $(this).parent().prev().html();



                $(".clsGrade1").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");
                var cellIndex = $(this).parent()[0].cellIndex;
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalPriorityScoreCS1.ClientID%>").value = totalPriorityScoresCS;

                $("#hdnGrade1mark").val(selectedText);
                document.getElementById("<%=hdnGrade1mark.ClientID%>").value = totalPriorityScoresCS;
                document.getElementById("<%=hdnGrade1Index.ClientID%>").value = cellIndex;
                document.getElementById("<%=hdnGrade1ID.ClientID%>").value = this.id;
                $("#spnGr1mark").text(selectedText);
                $("#spnGr1mark").parent().css("background-color", "#DE1111");
                $(".clsGrade1").prop("checked", false);
                $(this).prop("checked", true);

                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);

                $("#spnTotalInvitees").text(total);
                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");

                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                if (mark1 != "-1" && mark2 != "-1" && mark3 != "-1") {
                    $("#btnSaveScore").css("display", "block");
                }

                $("#btnSavePriorityList1").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade2", function (e) {


                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade2").parent().prev().css("background-color", "#ffffff");

                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalPriorityScoreCS2.ClientID%>").value = totalPriorityScoresCS;

                $(this).parent().prev().css("background-color", "#DE1111");
                document.getElementById("<%=hdnGrade2mark.ClientID%>").value = totalPriorityScoresCS;
                document.getElementById("<%=hdnGrade2Index.ClientID%>").value = cellIndex;
                document.getElementById("<%=hdnGrade2ID.ClientID%>").value = this.id;
                $("#spnGr2mark").text(selectedText);
                $("#spnGr2mark").parent().css("background-color", "#DE1111");
                $(".clsGrade2").prop("checked", false);
                $(this).prop("checked", true);
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;
                if (mark1 != "-1" && mark2 != "-1" && mark3 != "-1") {
                    $("#btnSaveScore").css("display", "block");
                }
                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList2").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade3", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade3").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");

                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalPriorityScoreCS3.ClientID%>").value = totalPriorityScoresCS;

                document.getElementById("<%=hdnGrade3mark.ClientID%>").value = totalPriorityScoresCS;
                document.getElementById("<%=hdnGrade3Index.ClientID%>").value = cellIndex;
                document.getElementById("<%=hdnGrade3ID.ClientID%>").value = this.id;
                $("#spnGr3mark").text(selectedText);
                $("#spnGr3mark").parent().css("background-color", "#DE1111");
                $(".clsGrade3").prop("checked", false);
                $(this).prop("checked", true);
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;
                if (mark1 != "-1" && mark2 != "-1" && mark3 != "-1") {
                    $("#btnSaveScore").css("display", "block");
                }
                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");

                $("#btnSavePriorityList3").removeAttr("disabled");
            });


            $(document).on("click", ".clsTotal1", function (e) {

                var selectedText = $(this).parent().prev().html();
                $("#rbtnGrade1" + selectedText + "").removeAttr("disabled");
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal1").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore1").text(selectedText);
                $(".clsTotal1").prop("checked", false);
                $(this).prop("checked", true);

                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScoreCS1.ClientID%>").value = totalListScoresCS;

                document.getElementById("<%=hdnTotalScore1.ClientID%>").value = totalListScoresCS;
                document.getElementById("<%=hdnTotalSocre1ID.ClientID%>").value = this.id;

                $("#spnTotalScore1").parent().css("background-color", "#03C8FA");

                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());

                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList1").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal2", function (e) {


                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal2").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore2").text(selectedText);

                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScoreCS2.ClientID%>").value = totalListScoresCS;

                $(".clsTotal2").prop("checked", false);
                $(this).prop("checked", true);
                document.getElementById("<%=hdnTotalScore2.ClientID%>").value = totalListScoresCS;
                document.getElementById("<%=hdnTotalSocre2ID.ClientID%>").value = this.id;
                $("#spnTotalScore2").parent().css("background-color", "#03C8FA");

                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());

                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList2").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal3", function (e) {


                var selectedText = $(this).parent().prev().html();


                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal3").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore3").text(selectedText);

                $("#spnTotalScore3").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScoreCS3.ClientID%>").value = totalListScoresCS;

                $(".clsTotal3").prop("checked", false);
                $(this).prop("checked", true);

                document.getElementById("<%=hdnTotalScore3.ClientID%>").value = totalListScoresCS;
                document.getElementById("<%=hdnTotalSocre3ID.ClientID%>").value = this.id;

                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());

                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);

                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList3").removeAttr("disabled");
            });

            $(document).on("click", ".clsGrade4", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade4").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr4mark").text(selectedText);
                $("#spnGr4mark").parent().css("background-color", "#DE1111");
                $(".clsGrade4").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade4mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList4").removeAttr("disabled");
            });

            $(document).on("click", ".clsGrade5", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade5").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr5mark").text(selectedText);
                $("#spnGr5mark").parent().css("background-color", "#DE1111");
                $(".clsGrade5").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade5mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList5").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade6", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade6").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr6mark").text(selectedText);
                $("#spnGr6mark").parent().css("background-color", "#DE1111");
                $(".clsGrade6").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade6mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);

                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList6").removeAttr("disabled");
            });

            $(document).on("click", ".clsGrade7", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade7").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr7mark").text(selectedText);
                $("#spnGr7mark").parent().css("background-color", "#DE1111");
                $(".clsGrade7").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade7mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());

                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList7").removeAttr("disabled");
            });

            $(document).on("click", ".clsGrade8", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade8").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr8mark").text(selectedText);
                $("#spnGr8mark").parent().css("background-color", "#DE1111");
                $(".clsGrade8").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade8mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList8").removeAttr("disabled");
            });

            $(document).on("click", ".clsGrade9", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade9").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr9mark").text(selectedText);
                $("#spnGr9mark").parent().css("background-color", "#DE1111");
                $(".clsGrade9").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade9mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList9").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade10", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade10").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr10mark").text(selectedText);
                $("#spnGr10mark").parent().css("background-color", "#DE1111");
                $(".clsGrade10").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade10mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList10").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade11", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade11").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr11mark").text(selectedText);
                $("#spnGr11mark").parent().css("background-color", "#DE1111");
                $(".clsGrade11").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade11mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList11").removeAttr("disabled");
            });
            $(document).on("click", ".clsGrade12", function (e) {

                var selectedText = $(this).parent().prev().html();
                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsGrade12").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#DE1111");


                $("#spnGr12mark").text(selectedText);
                $("#spnGr12mark").parent().css("background-color", "#DE1111");
                $(".clsGrade12").prop("checked", false);
                $(this).prop("checked", true);
                var totalPriorityScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnGrade12mark.ClientID%>").value = totalPriorityScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnGr1mark").text() == "" ? "0" : $("#spnGr1mark").text());
                var mark2 = ($("#spnGr2mark").text() == "" ? "0" : $("#spnGr2mark").text());
                var mark3 = ($("#spnGr3mark").text() == "" ? "0" : $("#spnGr3mark").text());
                var mark4 = ($("#spnGr4mark").text() == "" ? "0" : $("#spnGr4mark").text());
                var mark5 = ($("#spnGr5mark").text() == "" ? "0" : $("#spnGr5mark").text());
                var mark6 = ($("#spnGr6mark").text() == "" ? "0" : $("#spnGr6mark").text());
                var mark7 = ($("#spnGr7mark").text() == "" ? "0" : $("#spnGr7mark").text());
                var mark8 = ($("#spnGr8mark").text() == "" ? "0" : $("#spnGr8mark").text());
                var mark9 = ($("#spnGr9mark").text() == "" ? "0" : $("#spnGr9mark").text());
                var mark10 = ($("#spnGr10mark").text() == "" ? "0" : $("#spnGr10mark").text());
                var mark11 = ($("#spnGr11mark").text() == "" ? "0" : $("#spnGr11mark").text());
                var mark12 = ($("#spnGr12mark").text() == "" ? "0" : $("#spnGr12mark").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#spnTotalInvitees").text(total);
                document.getElementById("<%=hdnTotlaInvitees.ClientID%>").value = total;

                $("#spnTotalInvitees").parent().css("background-color", "#DE1111");
                $("#btnSavePriorityList12").removeAttr("disabled");
            });

            $(document).on("click", ".clsTotal4", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal4").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore4").text(selectedText);

                $("#spnTotalScore4").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal4").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore4.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());

                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());


                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList4").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal5", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal5").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore5").text(selectedText);

                $("#spnTotalScore5").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal5").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore5.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());


                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList5").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal6", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal6").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore6").text(selectedText);

                $("#spnTotalScore6").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal6").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore6.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());


                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList6").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal7", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal7").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore7").text(selectedText);

                $("#spnTotalScore7").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal7").prop("checked", false);
                $(this).prop("checked", true);
                $("#btnSaveScore").css("display", "block");
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore7.ClientID%>").value = totalListScoresCS;
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());


                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList7").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal8", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal8").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore8").text(selectedText);

                $("#spnTotalScore8").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal8").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore8.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());


                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList8").removeAttr("disabled");
            });

            $(document).on("click", ".clsTotal9", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal9").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore9").text(selectedText);

                $("#spnTotalScore9").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal9").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore9.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());
                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList9").removeAttr("disabled");
            });

            $(document).on("click", ".clsTotal10", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal10").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore10").text(selectedText);

                $("#spnTotalScore10").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal10").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore10.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());
                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList10").removeAttr("disabled");
            });
            $(document).on("click", ".clsTotal11", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal11").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore11").text(selectedText);

                $("#spnTotalScore11").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal11").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore11.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());
                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList11").removeAttr("disabled");
            });

            $(document).on("click", ".clsTotal12", function (e) {

                var selectedText = $(this).parent().prev().html();

                var cellIndex = $(this).parent()[0].cellIndex;
                $(".clsTotal12").parent().prev().css("background-color", "#ffffff");
                $(this).parent().prev().css("background-color", "#03C8FA");
                $("#spnTotalScore12").text(selectedText);

                $("#spnTotalScore12").parent().css("background-color", "#03C8FA");

                var totalListScoresCS = $(this).attr("attr-id");

                $(".clsTotal12").prop("checked", false);
                $(this).prop("checked", true);
                var totalListScoresCS = $(this).attr("attr-id");
                document.getElementById("<%=hdnTotalScore12.ClientID%>").value = totalListScoresCS;
                $("#btnSaveScore").css("display", "block");
                var mark1 = ($("#spnTotalScore1").text() == "" ? "0" : $("#spnTotalScore1").text());
                var mark2 = ($("#spnTotalScore2").text() == "" ? "0" : $("#spnTotalScore2").text());
                var mark3 = ($("#spnTotalScore3").text() == "" ? "0" : $("#spnTotalScore3").text());
                var mark4 = ($("#spnTotalScore4").text() == "" ? "0" : $("#spnTotalScore4").text());
                var mark5 = ($("#spnTotalScore5").text() == "" ? "0" : $("#spnTotalScore5").text());
                var mark6 = ($("#spnTotalScore6").text() == "" ? "0" : $("#spnTotalScore6").text());
                var mark7 = ($("#spnTotalScore7").text() == "" ? "0" : $("#spnTotalScore7").text());
                var mark8 = ($("#spnTotalScore8").text() == "" ? "0" : $("#spnTotalScore8").text());
                var mark9 = ($("#spnTotalScore9").text() == "" ? "0" : $("#spnTotalScore9").text());
                var mark10 = ($("#spnTotalScore10").text() == "" ? "0" : $("#spnTotalScore10").text());
                var mark11 = ($("#spnTotalScore11").text() == "" ? "0" : $("#spnTotalScore11").text());
                var mark12 = ($("#spnTotalScore12").text() == "" ? "0" : $("#spnTotalScore12").text());

                var total = parseInt(mark1) + parseInt(mark2) + parseInt(mark3) + parseInt(mark4) + parseInt(mark5) + parseInt(mark6) + parseInt(mark7) + parseInt(mark8) + parseInt(mark9) + parseInt(mark10) + parseInt(mark11) + parseInt(mark12);
                $("#splTotalList").text(total);
                $("#splTotalList").parent().css("background-color", "#03C8FA");
                document.getElementById("<%=hdnTotalList.ClientID%>").value = total;
                $("#btnSaveTotList12").removeAttr("disabled");
            });

            $(document).on("click", ".btnSaveTotalList", function (e) {
                var gradeVal = $(this).attr("attr-val");

                document.getElementById("<%=hdnGradeVal.ClientID%>").value = gradeVal;
                document.getElementById("<%=btnSaveTotalList.ClientID%>").click();
            });
            $(document).on("click", ".btnSavePriorityList", function (e) {
                var gradeVal = $(this).attr("attr-val");

                document.getElementById("<%=hdnGradeVal.ClientID%>").value = gradeVal;
                document.getElementById("<%=btnSavePriorityList.ClientID%>").click();
            });
            $(document).on("click", "#btnSaveScore", function (e) {

                document.getElementById("<%=btnSavePriorityList.ClientID%>").click();
            });

            function PopupPickerHelp(EventId) {
                var PopupWindow = null;
                settings = 'width=650,height=500,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                if (EventId == 1) {
                    settings = 'width=650,height=400,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                    PopupWindow = window.open('PercentilesByGradeHelpFinals.aspx', 'Percentiles by grade - Help', settings);
                }
                else
                    PopupWindow = window.open('PercentilesByGradeHelpReg.aspx', 'Percentiles by grade - Help', settings);
                PopupWindow.focus();
            }


        </script>
    </div>

    <div>
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <asp:Button ID="btnSavePriorityList" runat="server" Text="Save" Style="display: none;" OnClick="btnSavePriorityList_Click"></asp:Button>
        <asp:Button ID="btnSaveTotalList" runat="server" Text="Save" Style="display: none;" OnClick="btnSaveTotalList_Click"></asp:Button>
    </div>

    <div>
        <table align="center" width="90%">
            <tr align="center">
                <td align="center">
                    <asp:Label ID="lblPercentile" runat="server" Text="Percentiles by Contest/Grade and Cutoff Scores" Font-Size="Large" ForeColor="Green"></asp:Label><div style="float: right; clear: both;">
                        <asp:LinkButton ID="lnkHelp" runat="server" OnClick="lnkHelp_Click">Help</asp:LinkButton>
                    </div>
                    <br />
                    <br />
                    <asp:DropDownList ID="ddlPercentile" runat="server" AutoPostBack="true"
                        Height="20px" Width="264px"
                        OnSelectedIndexChanged="ddlPercentile_SelectedIndexChanged">
                        <%--onselectedindexchanged="ddlPercentile_SelectedIndexChanged"--%>
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Calculate Percentiles By Contest/Grade</asp:ListItem>
                        <asp:ListItem Value="2">View Percentiles By Contest/Grade</asp:ListItem>
                        <asp:ListItem Value="4">View Median Scores
                        </asp:ListItem>
                        <asp:ListItem Value="3">Cutoff Scores by Priority/Total Lists
                        </asp:ListItem>

                    </asp:DropDownList>

                </td>
            </tr>
        </table>
        <table width="1200px" runat="server">
            <tr align="right">
                <td>
                    <table>
                        <tr>

                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Save to Excel" OnClick="btnSave_Click" Enabled="false"></asp:Button>
                            </td>
                            <td>
                                <asp:Button ID="btnExport" runat="server" Text="Cut Off Scores By Percentile" OnClick="btnExport_Click" Visible="false"></asp:Button>
                                <input type="button" id="btnCutOffScores" value="Cut Off Scores By Percentile" style="display: none;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblNote" runat="server" Text="**Note: Press Continue before Exporting" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <tr align="left">
                <td>
                    <table border="1" style="float: left;">

                        <tr align="left">
                            <td colspan="1"><b>Event</b>:
                                <asp:DropDownList ID="ddlEvent" runat="server" Height="20px" AutoPostBack="true" Width="125px" Style="margin-bottom: 0px"></asp:DropDownList>
                            </td>
                            <td colspan="1" id="tdStart" runat="server" visible="false"><b>Starting Score</b>:
                                <asp:DropDownList ID="ddlStartingScore" runat="server" Height="20px" AutoPostBack="true" Width="62px" Style="margin-bottom: 0px"></asp:DropDownList>
                            </td>
                            <td colspan="1" id="tdEnd" runat="server" visible="false"><b>Ending Score</b>:
                                <asp:DropDownList ID="ddlEndingScore" runat="server" Height="20px" AutoPostBack="true" Width="62px" Style="margin-bottom: 0px"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--onselectedindexchanged="ddlEvent_SelectedIndexChanged"--%>
                        <tr align="center" id="trContest" runat="server" visible="true" style="width: 100%">
                            <td style="width: 35%">ContestYear:&nbsp;<asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" Width="60px"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" Height="20px">
                            </asp:DropDownList>&nbsp;&nbsp;
                            </td>
                            <td style="width: 30%">Contest:&nbsp;<asp:DropDownList ID="ddlContest" Visible="true"
                                runat="server" AutoPostBack="True" Height="20px" Width="60px" DataTextField="ProductCode" DataValueField="ProductID"
                                OnSelectedIndexChanged="ddlContest_SelectedIndexChanged">
                            </asp:DropDownList>
                            </td>
                            <td style="width: 50%">ContestDates:&nbsp;<asp:DropDownList ID="ddlcontestdate" AppendDataBoundItems="true"
                                runat="server" AutoPostBack="false" Width="95px" DataTextField="ContestDate" DataValueField="ContestPeriod"
                                OnSelectedIndexChanged="ddlcontestdate_SelectedIndexChanged">
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td colspan="2" align="center">
                                <br />
                                <asp:CheckBox ID="chkAllContest" Text="All Contests and Dates" AutoPostBack="true"
                                    runat="server" OnCheckedChanged="chkAllContest_CheckedChanged" Visible="false" />
                            </td>

                            <td colspan="2" align="right">
                                <br />
                                <asp:Button ID="BtnContinue" runat="server" Text="Continue"
                                    OnClick="BtnContinue_Click" />
                            </td>

                        </tr>
                    </table>
                    <div style="float: left; margin-left: 10px; margin-top: 35px; display: none;">
                        <asp:Label runat="server" ID="lblListType"><b>List :</b></asp:Label>
                        <asp:DropDownList ID="ddlListType" runat="server" Height="20px" AutoPostBack="true" Width="85px" Style="margin-bottom: 0px; margin-left: 10px;" Enabled="false">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Priority">Priority</asp:ListItem>
                            <asp:ListItem Value="Total">Total</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                </td>

            </tr>
            <tr align="center">
                <td colspan="3">
                    <asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="blue"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:DropDownList ID="ddlProductGroup" runat="server" Visible="false" Enabled="true"></asp:DropDownList>

                </td>
            </tr>
            <tr runat="server" align="center">
                <td>
                    <table id="tblDuplicate" runat="server" visible="false">
                        <tr>
                            <td>
                                <asp:Label ID="lblduplicates" runat="server" Text="Old records will be deleted and new records will be added at the end.  Please confirm if this is what you wish." ForeColor="Red" Font-Bold="false" Font-Size="Medium"></asp:Label>
                            </td>
                        </tr>
                        <%--"Data already exists, do you want to replace?"--%>
                        <tr>
                            <td align="center">
                                <br />
                                <asp:Button ID="BtnYes" runat="server" Text="Yes"
                                    OnClick="BtnYes_Click" />
                                &nbsp;
	                    <asp:Button ID="BtnNo" runat="server" Text="No"
                            OnClick="BtnNo_Click" />


                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="3" align="right">
                    <asp:Button ID="btnExporttoExcel" runat="server" Style="margin-right: 318px" Visible="false" Text="Export to Excel" OnClick="btnExporttoExcel_Click" />

                </td>
            </tr>

            <tr>
                <td>
                    <asp:DataGrid ID="DGPercentiles" runat="server" Visible="true" AutoGenerateColumns="false"
                        Width="945px" Height="304px" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" BackColor="White"
                        CellPadding="3" GridLines="Horizontal">
                        <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
                        <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ContestYear" HeaderText="ContestYear"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventID" HeaderText="EventID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductCode" HeaderText="ProductCode"></asp:BoundColumn>

                            <asp:BoundColumn DataField="TotalScore" HeaderText="TotalScore"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr1%" HeaderText="Gr1%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr1_Ptile" HeaderText="Gr1_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr1_cnt" HeaderText="Gr1_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr1_cum" HeaderText="Gr1_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr2%" HeaderText="Gr2%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr2_Ptile" HeaderText="Gr2_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr2_cnt" HeaderText="Gr2_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr2_cum" HeaderText="Gr2_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr3%" HeaderText="Gr3%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr3_Ptile" HeaderText="Gr3_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr3_cnt" HeaderText="Gr3_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr3_cum" HeaderText="Gr3_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr4%" HeaderText="Gr4%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr4_Ptile" HeaderText="Gr4_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr4_cnt" HeaderText="Gr4_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr4_cum" HeaderText="Gr4_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr5%" HeaderText="Gr5%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr5_Ptile" HeaderText="Gr5_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr5_cnt" HeaderText="Gr5_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr5_cum" HeaderText="Gr5_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr6%" HeaderText="Gr6%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr6_Ptile" HeaderText="Gr6_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr6_cnt" HeaderText="Gr6_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr6_cum" HeaderText="Gr6_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr7%" HeaderText="Gr7%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr7_Ptile" HeaderText="Gr7_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr7_cnt" HeaderText="Gr7_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr7_cum" HeaderText="Gr7_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr8%" HeaderText="Gr8%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr8_Ptile" HeaderText="Gr8_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr8_cnt" HeaderText="Gr8_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr8_cum" HeaderText="Gr8_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr9%" HeaderText="Gr9%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr9_Ptile" HeaderText="Gr9_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr9_cnt" HeaderText="Gr9_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr9_cum" HeaderText="Gr9_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr10%" HeaderText="Gr10%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr10_Ptile" HeaderText="Gr10_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr10_cnt" HeaderText="Gr10_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr10_cum" HeaderText="Gr10_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr11%" HeaderText="Gr11%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr11_Ptile" HeaderText="Gr11_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr11_cnt" HeaderText="Gr11_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr11_cum" HeaderText="Gr11_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Gr12%" HeaderText="Gr12%"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr12_Ptile" HeaderText="Gr12_Ptile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr12_cnt" HeaderText="Gr12_cnt"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Gr12_cum" HeaderText="Gr12_cum"></asp:BoundColumn>

                            <asp:BoundColumn DataField="TotalPercentile" HeaderText="TotalPercentile"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Total_count" HeaderText="Total_Count"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Total_Cume_Count" HeaderText="Total_Cume_Count"></asp:BoundColumn>

                        </Columns>
                    </asp:DataGrid>
                    <div style="clear: both;"></div>
                    <div style="width: 1200px;">
                        <asp:Button ID="btnSaveCutOffScores" runat="server" Text="Save Cut Off Scores" OnClick="btnSaveCutOffScores_Click" Style="display: none;" />
                        <input type="button" id="btnSaveScore" value="Save Cut Off Scores" style="display: none; float: right;" />
                        <asp:Button ID="btnExportToExcelAll" runat="server" Text="Save To Excel All" OnClick="btnExportToExcelAll_Click" Style="display: none;" />
                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div style="width: 1200px; overflow: scroll;" id="dvScheduleReportingDay2" runat="server" visible="false">
                        <asp:Literal ID="ltrSchedulePostingDay2" runat="server"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="width: 1100px; margin-left: auto; margin-right: auto;" id="dvMedian" runat="server">
            <div align="left"><span id="spnMedianTitle" runat="server" style="font-weight: bold;"></span></div>
            <div style="clear: both;"></div>
            <div style="float: right; width: 40%;">
                <asp:Button ID="BtnExportExcel" runat="server" Text="Export To Excel" Visible="false" OnClick="BtnExportExcel_Click" />
            </div>
            <div style="clear: both;"></div>
            <asp:Literal ID="ltrMedianScore" runat="server">
              
            </asp:Literal>

        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="width: 1100px; margin-left: auto; margin-right: auto;" id="dvStdDeviation" runat="server">
            <div align="left"><span id="spnTable2" runat="server" style="font-weight: bold;"></span></div>
            <div style="clear: both;"></div>
            <div style="float: right; width: 40%;">
                <asp:Button ID="BtnExportToExcel2" runat="server" Text="Export To Excel" Visible="false" OnClick="BtnExportToExcel2_Click" />
            </div>
            <div style="clear: both;"></div>
            <asp:Literal ID="ltrTable2" runat="server">

            </asp:Literal>
            <div style="clear: both; margin-bottom: 10px;"></div>
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="width: 1100px; margin-left: auto; margin-right: auto;" id="dvMean" runat="server">
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div align="left"><span id="spnTable3" runat="server" style="font-weight: bold;"></span></div>
            <div style="clear: both;"></div>
            <div style="float: right; width: 40%;">
                <asp:Button ID="BtnExportToExcel3" runat="server" Text="Export To Excel" Visible="false" OnClick="BtnExportToExcel3_Click" />
            </div>
            <div style="clear: both;"></div>
            <asp:Literal ID="ltrTable3" runat="server">

            </asp:Literal>

        </div>
    </div>


    <input type="hidden" id="hdnGrade1mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade2mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade3mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade4mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade5mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade6mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade7mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade8mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade9mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade10mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade11mark" value="" runat="server" />
    <input type="hidden" id="hdnGrade12mark" value="" runat="server" />



    <input type="hidden" id="hdnTotlaInvitees" value="" runat="server" />
    <input type="hidden" id="hdnIsCutOff" value="" runat="server" />
    <input type="hidden" id="hdnGrade1Index" value="" runat="server" />
    <input type="hidden" id="hdnGrade2Index" value="" runat="server" />
    <input type="hidden" id="hdnGrade3Index" value="" runat="server" />

    <input type="hidden" id="hdnGrade1ID" value="" runat="server" />
    <input type="hidden" id="hdnGrade2ID" value="" runat="server" />
    <input type="hidden" id="hdnGrade3ID" value="" runat="server" />

    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductGroup" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />
    <input type="hidden" id="hdnProduct" value="0" runat="server" />


    <input type="hidden" id="hdnTotalScore1" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore2" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore3" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore4" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore5" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore6" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore7" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore8" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore9" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore10" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore11" value="" runat="server" />
    <input type="hidden" id="hdnTotalScore12" value="" runat="server" />



    <input type="hidden" id="hdnTotalSocre1ID" value="" runat="server" />
    <input type="hidden" id="hdnTotalSocre2ID" value="" runat="server" />
    <input type="hidden" id="hdnTotalSocre3ID" value="" runat="server" />

    <input type="hidden" id="hdnTotalList" value="" runat="server" />

    <input type="hidden" id="hdnTotalScoreCS1" value="" runat="server" />
    <input type="hidden" id="hdnTotalScoreCS2" value="" runat="server" />
    <input type="hidden" id="hdnTotalScoreCS3" value="" runat="server" />

    <input type="hidden" id="hdnTotalPriorityScoreCS1" value="" runat="server" />
    <input type="hidden" id="hdnTotalPriorityScoreCS2" value="" runat="server" />
    <input type="hidden" id="hdnTotalPriorityScoreCS3" value="" runat="server" />

    <input type="hidden" id="hdnGradeVal" value="" runat="server" />
    <input type="hidden" id="hdnIsSave" value="" runat="server" />

    <input type="hidden" id="hdnStartGrade" value="" runat="server" />
    <input type="hidden" id="hdnEndGrade" value="" runat="server" />
    <input type="hidden" id="hdnContestDates" value="" runat="server" />
    <input type="hidden" id="hdnMaxScores" value="" runat="server" />
</asp:Content>


