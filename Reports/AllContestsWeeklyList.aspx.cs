using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Drawing;


public partial class Reports_AllContestsWeeklyList : System.Web.UI.Page
{
    DataSet dsMedals = new DataSet();
    DataTable dt = new DataTable();
    DataRow dr;
    DataRow drr;
    int[] category = new int[33];
    int tCount = 0;
    //int chapID;
    int prevChapterID;
   int retValue = 0;
    ArrayList al = new ArrayList();
    ArrayList cl = new ArrayList();
    int[] Grandtotal = new int[10];
     DataTable RankTrophy = new DataTable();

        

    protected void Page_Load(object sender, EventArgs e)
    {
     
        // Put user code to initialize the page here
        //if(Session["Admin"].ToString() != "Yes")
        //	Response.Redirect("Default.aspx");
        //chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        //chapID = 15;
        //Session["LoginID"] = 4240;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        lblChapter.Text = lblError.Text = "";

//35187
//select COUNT(*) from volunteer where roleid=63 and Finals = 'Y' and MemberID =35187
//select COUNT(*) from volunteer where roleid=63 and chapterId is Not Null and MemberID =35187

        if (!IsPostBack)
        {
       
            //LoadEvent();
            //if (Request.QueryString["id"].ToString() == "1")
            //{
            //    ddlEvent.SelectedValue = "2";
            //}
            //else
            //{
            //    ddlEvent.SelectedValue = "1";
            //}
        
            hdnFlag.Value = "N";
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2")))
                        hdnFlag.Value = "Y";
            else if ((Session["RoleId"] != null) && (Session["selChapterID"] != null) && (Request.QueryString["id"] != null) && (Request.QueryString["id"].ToString() == "1"))
                hdnFlag.Value = Session["selChapterID"].ToString ();
            else if ((Session["RoleId"] != null) && (Session["RoleId"].ToString() == "63"))
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from volunteer where roleid=63 and [National] = 'Y' and MemberID =" + Session["LoginId"].ToString() + "")) > 0)
                    hdnFlag.Value = "Y";
                else if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from volunteer where roleid=63 and  Finals = 'Y' and MemberID =" + Session["LoginId"].ToString() + "")) > 0)
                    hdnFlag.Value = "1";
                else
                {
                   SqlDataReader readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterId from volunteer where roleid=63 and chapterId is Not Null and MemberID ="+ Session["LoginId"].ToString() + "");
                    while (readr.Read())
                    {
                        if (hdnFlag.Value == "N")
                            hdnFlag.Value = readr["chapterId"].ToString();
                        else
                            hdnFlag.Value = hdnFlag.Value.ToString() + "," + readr["chapterId"].ToString();
                    }                   

                }
            }
            else
                Response.Redirect("~/VolunteerFunctions.aspx");

            int year = Convert.ToInt32(DateTime.Now.Year);
            ddlyear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
            ddlyear.SelectedIndex = ddlyear.Items.IndexOf(ddlyear.Items.FindByText(Convert.ToString(year)));
            //if (hdnFlag.Value != "Y")
            if (hdnFlag.Value == "N")
            {
                ddlWeek.Visible = false;
                ddlyear.Enabled = false;
                lblChapter.Visible = false;
            }
            loadWeeks();
            
        }
        dsMedals.Clear();
        al.Clear();
        cl.Clear();
        dt.Clear();
        if (ddlWeek.Items.Count > 0)
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                lblChapter.Text = ddlWeek.SelectedItem.ToString();
            }
          //      GetCounts();
            
        }
        else
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                Response.Write("No data exists");
            }
            lblChapter.Text = "";
            DataGrid1.Visible = false;
            btnSave.Enabled = false;
        }
        RankTrophy.Columns.Add("Year", typeof(string));
        RankTrophy.Columns.Add("EventID", typeof(string));
        RankTrophy.Columns.Add("ContestDate", typeof(string));

        RankTrophy.Columns.Add("ChapterID", typeof(string));
        RankTrophy.Columns.Add("ProductGroupID", typeof(string));
        RankTrophy.Columns.Add("ProductGroupCode", typeof(string));
        RankTrophy.Columns.Add("ProductID", typeof(string));
        RankTrophy.Columns.Add("ProductCode", typeof(string));
        RankTrophy.Columns.Add("Reg", typeof(string));
        RankTrophy.Columns.Add("First", typeof(string));
        RankTrophy.Columns.Add("Second", typeof(string));
        RankTrophy.Columns.Add("Third", typeof(string));


    }
    //public void LoadEvent()
    //{
    //    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select Distinct EventId,Name from Event Where EventId in (1,2)");
    //        if (ds.Tables[0].Rows.Count>0)
    //        {
    //            ddlEvent.DataSource =ds;
    //            ddlEvent.DataTextField ="Name";
    //            ddlEvent.DataValueField ="EventID";
    //            ddlEvent.DataBind();
            
    //        }
    //}
    public void loadWeeks()
    {
        ddlWeek.Items.Clear();
        //DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select convert(varchar(6), SatDay1, 7)+' - '+ convert(varchar(6), SunDay2, 7) as Weektext,weekid from weekcalendar where YEAR(SatDay1)=" + ddlyear.SelectedItem.Text  + "");
        String SQLstr = "SELECT  *,Row_number() over(order by temp.ContestDate1,temp.ContestDate2)as ContestPeriod FROM (";
        SQLstr = SQLstr + " SELECT  Distinct C.ContestDate as ContestDate1,C1.ContestDate as ContestDate2,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + ";
        SQLstr = SQLstr + " CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39) as ContestDates , Convert(Varchar(10),DATENAME(MM, C.ContestDate) ";
        SQLstr = SQLstr + " + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(10),DATENAME(MM, C1.ContestDate) + ' ' + ";
        SQLstr = SQLstr + " CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDate FROM Contest ";
        SQLstr = SQLstr + " C Inner join Contest C1 On C1.Contest_Year = C.Contest_Year Where C.Contest_Year=" +  ddlyear.SelectedValue + " and ";//c.eventid=" + ddlEvent.SelectedValue + " and ";
        SQLstr = SQLstr + " cast(DATEDIFF(Day,C.ContestDate,C1.ContestDate) as int) = 1) temp order by temp.ContestDate1,temp.ContestDate2";
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text,SQLstr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlWeek.DataSource = ds;
            ddlWeek.DataTextField = "ContestDate";
            ddlWeek.DataValueField = "ContestDates";
            ddlWeek.DataBind();
        }         
        ddlWeek.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("All", "All"));
        ddlWeek.Items.Insert(0, new ListItem("Select", "0"));
  
        //  ddlWeek.DataBind();

        if (ddlWeek.Items.Count > 0)
        {
            if (hdnFlag.Value != "N")
                ddlWeek.Visible = true;
            //if (ddlyear.SelectedItem.Text == "2011")
            //    ddlWeek.Items.Insert(ddlWeek.Items.Count, new ListItem("Apr 23 - Apr 24", "50"));
        }
        else
        {
            ddlWeek.Visible = false;
            lblChapter.Text = "";
        }

    }
    private void GetCounts()
    {
        dt.Columns.Add("Contest", typeof(string));
        dt.Columns.Add("PartCert", typeof(string));
        dt.Columns.Add("ExcelCert", typeof(string));
        dt.Columns.Add("Gold", typeof(string));
        dt.Columns.Add("Silver", typeof(string));
        dt.Columns.Add("Bronze", typeof(string));
        dt.Columns.Add("PartMed", typeof(string));
        dt.Columns.Add("VolCert", typeof(string));
        dt.Columns.Add("Ranks", typeof(String));
        dt.Columns.Add("UniqChild", typeof(String));
        GetChapterList();

        // Get session in Table
        DataTable PartMedTrophy = new DataTable();
        DataRow drr2;
        PartMedTrophy.Columns.Add("Year", typeof(string));
        PartMedTrophy.Columns.Add("EventID", typeof(string));
        PartMedTrophy.Columns.Add("ContestDate", typeof(string));
        PartMedTrophy.Columns.Add("ChapterID", typeof(string));
        PartMedTrophy.Columns.Add("Registrations", typeof(string));
        PartMedTrophy.Columns.Add("Unique", typeof(string));

        if (retValue > 0)
        {
            prevChapterID = Convert.ToInt32(cl[0]);
            string[] st = GetChapterName(prevChapterID);
            dr = dt.NewRow();
            dr["Contest"] = "Chapter: " + st[0]  ;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Coord: " + st[1]+ " " + st[2];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = st[3];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = st[4] + ", " + st[5] + ", " + st[6];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Tel:" + st[7];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Email:" + st[8];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Contest Date:" + st[9];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Registration Deadline:" + st[10];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dt.Rows.Add(dr);

            
            //PartMedTrophy.Columns.Add("Round", typeof(string));
            
           
                

            int RowCnt;
            for (int i = 0; i < cl.Count; i++)
            {
                drr2 = PartMedTrophy.NewRow();
                drr2["Year"] = ddlyear.SelectedValue;
                drr2["EventID"] = ViewState["event_id"];
                drr2["ContestDate"] = DateTime.Parse(st[9]).ToString("MM/dd/yyyy"); 

                if (i > 0)
                {
                    prevChapterID = Convert.ToInt32(cl[i]);
                    CtrlBrk();
                }
                GetContestCount(Convert.ToInt32(cl[i]));

                /*** Added to find GrandTotal for each Column *****/
                RowCnt = dt.Rows.Count - 1;
                Grandtotal[0] = Grandtotal[0] + Convert.ToInt32(dt.Rows[RowCnt]["PartCert"]);
                Grandtotal[1] = Grandtotal[1] + Convert.ToInt32(dt.Rows[RowCnt]["ExcelCert"]);
                Grandtotal[2] = Grandtotal[2] + Convert.ToInt32(dt.Rows[RowCnt]["Gold"]);
                Grandtotal[3] = Grandtotal[3] + Convert.ToInt32(dt.Rows[RowCnt]["Silver"]);
                Grandtotal[4] = Grandtotal[4] + Convert.ToInt32(dt.Rows[RowCnt]["Bronze"]);
                Grandtotal[5] = Grandtotal[5] + Convert.ToInt32(dt.Rows[RowCnt]["PartMed"]);
                Grandtotal[6] = Grandtotal[6] + Convert.ToInt32(dt.Rows[RowCnt]["VolCert"]);
                Grandtotal[7] = Grandtotal[7] + Convert.ToInt32(dt.Rows[RowCnt]["Ranks"]);
                Grandtotal[8] = Grandtotal[8] + Convert.ToInt32(dt.Rows[RowCnt]["UniqChild"]);

                drr2["ChapterID"] = Convert.ToInt32(cl[i]);

                drr2["Registrations"] = dt.Rows[RowCnt]["PartCert"];
                drr2["Unique"] = dt.Rows[RowCnt]["UniqChild"];
                PartMedTrophy.Rows.Add(drr2);
                /***  ***/
            }
            // Get session in Table
            Session["PartMedTrophy"] = PartMedTrophy;
            /*** Added to find GrandTotal for each Column ***/
            dr = dt.NewRow();
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Grand Total";
            dr["PartCert"] = Grandtotal[0];
            dr["ExcelCert"] = Grandtotal[1];
            dr["Gold"] = Grandtotal[2];
            dr["Silver"] = Grandtotal[3];
            dr["Bronze"] = Grandtotal[4];
            dr["PartMed"] = Grandtotal[5];
            dr["VolCert"] = Grandtotal[6];
            dr["Ranks"] = Grandtotal[7];
            dr["UniqChild"] = Grandtotal[8];
            dt.Rows.Add(dr);
            /*** ***/

            if (dt.Rows.Count < 1)
            {
                if (ddlWeek.SelectedIndex != 0)
                {
                    Response.Write("No data exists");
                }
                DataGrid1.Visible = false;
                btnSave.Enabled = false;
            }
            else
            {
                DataGrid1.Visible = true;
                DataGrid1.DataSource = dt;
                DataGrid1.DataBind();
                btnSave.Enabled = true;
               
            }
        }
    }
    void CtrlBrk()
    {

        dr = dt.NewRow();
        dt.Rows.Add(dr);
        string[] st = GetChapterName(prevChapterID);
        dr = dt.NewRow();
        dr["Contest"] = "Chapter: " + st[0] ;
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Coord: " + st[1] + " " + st[2];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = st[3];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = st[4] + ", " + st[5] + ", " + st[6];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Tel:" + st[7];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Email:" + st[8];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Contest Date:" + st[9];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Registration Deadline:" + st[10];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Contest";
        dr["PartCert"] = "PartCert";
        dr["ExcelCert"] = "ExcelCert";
        dr["Gold"] = "Gold";
        dr["Silver"] = "Silver";
        dr["Bronze"] = "Bronze";
        dr["PartMed"] = "PartMed";
        dr["VolCert"] = "VolCert";
        dr["Ranks"] = "Ranks";
        dr["UniqChild"] = "UniqChild";
        dt.Rows.Add(dr);
        al.RemoveRange(0, al.Count);

    }

    void GetChapterList()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // get records from the products table

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        //connection.Open();
        // get records from the products table
        string commandString;
      
            if ((hdnFlag.Value.ToString() != "Y") && (hdnFlag.Value.ToString() != "N") && (ddlWeek.SelectedItem.Text != "All"))
                commandString = "Select distinct C.NSFChapterID,Ch.State ,Ch.Name From Contest C Inner Join Chapter Ch On Ch.chapterId=C.NSFChapterID where Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ") and NSFChapterId in(" + hdnFlag.Value + ")";
            else if ((hdnFlag.Value.ToString() != "Y") && (hdnFlag.Value.ToString() != "N") &&(ddlWeek.SelectedItem.Text == "All"))
                commandString = "Select distinct C.NSFChapterID,Ch.State ,Ch.Name From Contest C Inner Join Chapter Ch On Ch.chapterId=C.NSFChapterID where Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and C.NSFChapterId in(" + hdnFlag.Value + ")";
            else if ((hdnFlag.Value.ToString() == "Y")&& (ddlWeek.SelectedItem.Text != "All"))
                commandString = "Select distinct C.NSFChapterID,Ch.State ,Ch.Name From Contest C,Chapter Ch where Ch.chapterId = C.NSFChapterID AND Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ") and NSFChapterId is not null ORDER BY Ch.State ,Ch.Name "; // WeekCalendar WC,
            else if ((hdnFlag.Value.ToString() == "Y") && (ddlWeek.SelectedItem.Text == "All"))
                commandString = "Select distinct C.NSFChapterID,Ch.State ,Ch.Name From Contest C,Chapter Ch where Ch.chapterId = C.NSFChapterID AND Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and EventID = 2 and NSFChapterId is not null ORDER BY Ch.State ,Ch.Name ";
            else
                return;


            //else if ((ddlWeek.SelectedValue == "50") && (hdnFlag.Value.ToString() == "Y"))
            //    commandString = "Select distinct Contest.NSFChapterID,c.State ,c.Name From Contest  Inner Join Chapter C On C.chapterId=Contest.NSFChapterID where Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and NSFChapterId is not null and ContestDate >= '04/23/2011' AND ContestDate <= '04/24/2011'  ORDER BY c.State ,c.Name ";
            //else if ((ddlWeek.SelectedItem.Text == "All") && (hdnFlag.Value.ToString() == "Y"))
            //        commandString = "Select distinct Contest.NSFChapterID,c.State ,c.Name From Contest, WeekCalendar WC,Chapter C where C.chapterId=Contest.NSFChapterID AND Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and NSFChapterId is not null and ContestDate >= WC.SatDay1 AND ContestDate <= WC.SunDay2 ORDER BY c.State ,c.Name ";
            //else if (hdnFlag.Value.ToString() == "Y")
            //    commandString = "Select distinct Contest.NSFChapterID,c.State ,c.Name From Contest, WeekCalendar WC,Chapter C where C.chapterId=Contest.NSFChapterID AND Contest_Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and NSFChapterId is not null and ContestDate >= WC.SatDay1 AND ContestDate <= WC.SunDay2 AND WC.WeekId='" + ddlWeek.SelectedValue + "' ORDER BY c.State ,c.Name ";
   
           
        SqlDataAdapter daChapters = new SqlDataAdapter(commandString, connection);
        DataSet dsChapters = new DataSet();
        retValue = daChapters.Fill(dsChapters);
        if (retValue > 0)
        {
            for (int i = 0; i < retValue; i++)
                if (cl.Contains(dsChapters.Tables[0].Rows[i].ItemArray[0]) == false)
                    cl.Add(dsChapters.Tables[0].Rows[i].ItemArray[0]);
        }
        else
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                lblError.Text = "No data exists";
            }
            DataGrid1.Visible = false;
            btnSave.Enabled = false;
        }
    }
    public void GetContestCount(int chap)
    {

        string[] catName = new string[33];
        int[] catCount = new int[33];
        int cCount = 0;
        int goldCount = 0, silverCount = 0, bronzeCount = 0, excelCount = 0;
        int partCount = 0;
        bool found;
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // create and open the connection object
        SqlConnection connection =
            new SqlConnection(connectionString);
        connection.Open();


        // get records from the products table
        string commandString = "Select Contest.ContestID, Contest.ContestCategoryID, Contest.ContestDate,Contestant.ChildNumber FROM Contest " +
            " INNER JOIN Contestant ON Contest.ContestID = Contestant.ContestCode WHERE Contest_Year='" + ddlyear.SelectedItem.Text + "' AND Contestant.PaymentReference IS NOT NULL " +
            " AND  Contest.NSFChapterID =" + chap + " ORDER BY Contest.ContestCategoryID, Contest.ContestID ";
        SqlCommand myCommand = new SqlCommand(commandString, connection);

        dsMedals.Clear();
        SqlDataAdapter daMedals = new SqlDataAdapter(commandString, connection);

        tCount = daMedals.Fill(dsMedals);
        //
        for (int j = 0; j < dsMedals.Tables[0].Rows.Count; j++)
        {
            found = false;
            for (int i = 0; i < cCount; i++)
            {
                if (catName[i] == dsMedals.Tables[0].Rows[j].ItemArray[0].ToString())
                {
                    catCount[i]++;
                    found = true;
                    //lblError.Text = dr["Contest"].ToString();// dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                }
            }
            if (found == false)
            {
                catName[cCount] = dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                catCount[cCount] = 1;
                cCount++;
            }
            partCount++;
        }

        for (int p = 0; p < dsMedals.Tables[0].Rows.Count; p++)
        {
            if (al.Contains(dsMedals.Tables[0].Rows[p].ItemArray[3].ToString()) == false)
                al.Add(dsMedals.Tables[0].Rows[p].ItemArray[3].ToString());
        }
       // Get session in Table

        
        

  

       

       
        //drr["Year"] = ddlyear.SelectedValue;
        


        for (int i = 0; i < cCount; i++)
        {
            // add product details into RankTrophy table to insert into table while click on 'Save to Server'

            drr = RankTrophy.NewRow();
            

            dr = dt.NewRow();
            dr["Contest"] = GetLabels(Convert.ToInt32(catName[i]), "ContestCategoryID");
           // drr["ChapterID"] = dr["Contest"];
            dr["PartCert"] = catCount[i];
            if (catCount[i] >= 10)
            {
                dr["Gold"] = 1;
                dr["Bronze"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 3;
                dr["Ranks"] = 3; //Gold + silver+ Bronze 
                goldCount++;
                silverCount++;
                bronzeCount++;
                excelCount += 3;
            }
            else if (catCount[i] >= 8)
            {
                dr["Gold"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 2;
                dr["Ranks"] = 2;    //Gold + silver+ Bronze 
                goldCount++;
                silverCount++;
                excelCount += 2;
            }
            else if (catCount[i] >= 5)
            {
                dr["Gold"] = 1;
                dr["ExcelCert"] = 1;
                dr["Ranks"] = 1;   //Gold + silver+ Bronze 
                goldCount++;
                excelCount += 1;
            }
             string prddt=ViewState["PrdDt"].ToString();
             string[] productdetails = prddt.Split(',');

             drr["ChapterID"] = productdetails[0];
             drr["Year"] = productdetails[1];
             drr["EventID"] = productdetails[2];
             drr["ContestDate"] = DateTime.Parse(productdetails[3]).ToString("MM/dd/yyyy");
        drr["ProductGroupID"] = productdetails[4];
        drr["ProductGroupCode"] = productdetails[5];
        drr["ProductID"] = productdetails[6];
        drr["ProductCode"] = productdetails[7];
        drr["Reg"] = dr["PartCert"];
        drr["First"] = dr["Gold"];
        drr["Second"] = dr["Silver"];
        drr["Third"] = dr["Bronze"];

        RankTrophy.Rows.Add(drr);
            //drr[@]=dr[@@];


            dt.Rows.Add(dr);

        }
        Session["RankTrophy"] = RankTrophy;
        int increment = 1;
        if (partCount > 50)
            increment = 2;
        else if (partCount > 100)
            increment = 3;
        else if (partCount > 150)
            increment = 4;
        int distRankMedals = (int)(Math.Round(.67 * (goldCount + .5), 0) + Math.Round(.67 * (silverCount + .5), 0) +
            Math.Round(.67 * (bronzeCount + .5), 0));
        dr = dt.NewRow();
        dr["PartCert"] = partCount;
        dr["ExcelCert"] = excelCount + increment;
        dr["Gold"] = goldCount;
        dr["Silver"] = silverCount;
        dr["Bronze"] = bronzeCount;
        //distinct contestants - distinct rank holders
        dr["PartMed"] = al.Count - distRankMedals + increment;
        if (partCount < 51)
            dr["VolCert"] = 8;
        else if (partCount < 100)
            dr["VolCert"] = 12;
        else if (partCount < 150)
            dr["VolCert"] = 15;
        else if (partCount < 200)
            dr["VolCert"] = 20;
        else dr["VolCert"] = 25;
        dr["Ranks"] = goldCount + silverCount + bronzeCount; //Gold + silver+ Bronze 
        dr["UniqChild"] = al.Count;// dsMedals.Tables[0].Rows.Count;
        dt.Rows.Add(dr);

    }
    public string GetLabels(int idNumber, string colName)
    {
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString1 = "Select ContestCategoryID,NSFChapterID,Contest_Year,EventID,ContestDate,ProductGroupID,ProductGroupCode,ProductId,ProductCode from Contest where ContestID = " + idNumber;


        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString1;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        reader.Read();
        int retVal;
        retVal = reader.GetInt32(0);
        //getting productgroup details for a contest to insert into ranktrophy table
       // ViewState["PrdDt"] = reader[1] + "," + reader.GetInt32(2).ToString() + "," + reader.GetString(3).ToString() + "," + reader.GetInt32(4).ToString() + "," + reader.GetString(5).ToString() + "," + reader.GetInt32(6).ToString() + "," + reader.GetString(7).ToString();
        ViewState["PrdDt"] = reader[1]+","+reader[2] + "," + reader[3] + "," + reader[4] + "," + reader[5] + "," + reader[6] + "," + reader[7] + "," + reader[8];
        connection.Close();
        string commandString2 = "Select ContestDesc from ContestCategory where " + colName + " = " + retVal;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        string retValue;
        reader2.Read();
        retValue = reader2.GetString(0);
        
        // close connection return value
        connection.Close();
        return retValue;
    }
    //Get chapter name instead of ID
    public string[] GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        //string retValue = "";	

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        DataSet dstemp = new DataSet();
        string tempCmd = "Select MemberID from Volunteer Where RoleCode='ChapterC' and TeamLead='Y' and ChapterID=" + idNumber;
        SqlDataAdapter datemp = new SqlDataAdapter(tempCmd, connection);
        int cntTemp = datemp.Fill(dstemp);
        int memID = 0;
        if (cntTemp>0)
            memID = Convert.ToInt32(dstemp.Tables[0].Rows[0].ItemArray[0]);
        else lblError.Text ="Member ID not found";

        // get records from the products table
        string commandString = "Select Chapter, FirstName, LastName, Address1, City, State,Zip, HPhone, Email from IndSpouse where AutoMemberID = " + memID;

        string[] st = new string[11];
        st[0] = GetChapterName2(idNumber);
        DataSet dsChapInfo = new DataSet();
        SqlDataAdapter daChapInfo = new SqlDataAdapter(commandString, connection);

        int cnt = daChapInfo.Fill(dsChapInfo);
        if (cnt > 0)
        {
            st[1] = dsChapInfo.Tables[0].Rows[0].ItemArray[1].ToString();
            st[2] = dsChapInfo.Tables[0].Rows[0].ItemArray[2].ToString();
            st[3] = dsChapInfo.Tables[0].Rows[0].ItemArray[3].ToString();
            st[4] = dsChapInfo.Tables[0].Rows[0].ItemArray[4].ToString();
            st[5] = dsChapInfo.Tables[0].Rows[0].ItemArray[5].ToString();
            st[6] = dsChapInfo.Tables[0].Rows[0].ItemArray[6].ToString();
            st[7] = dsChapInfo.Tables[0].Rows[0].ItemArray[7].ToString();
            st[8] = dsChapInfo.Tables[0].Rows[0].ItemArray[8].ToString();
        }
        else
            for (int t = 0; t < st.Length; t++)
                st[t] = "";
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        string cmdString = "Select Contest.ContestDate, Contest.RegistrationDeadline,Contest.EventID FROM Contest " +
            " WHERE Contest_Year='" + ddlyear.SelectedItem.Text + "' AND Contest.NSFChapterID =" + idNumber + " ORDER BY Contest.ContestDate ";

        command.CommandText = cmdString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        string contestString = "", deadlineString = "";
        DateTime firstContest = DateTime.Parse("1/1/2020");
        DateTime readContest = DateTime.Parse("1/1/1900");
        DateTime firstDeadline = DateTime.Parse("1/1/2020");
        DateTime readDeadline = DateTime.Parse("1/1/1900");
        
        while (reader.Read())
        {
            contestString = reader.GetValue(0).ToString();
            deadlineString = reader.GetValue(1).ToString();
            ViewState["event_id"] = reader.GetValue(2).ToString();
            if (contestString != "")
            {
                readContest = Convert.ToDateTime(contestString);
                if (readContest < firstContest && readContest > DateTime.Parse("1/1/1900"))
                    firstContest = readContest;
            }
            if (deadlineString != "")
            {
                readDeadline = Convert.ToDateTime(deadlineString);
                if (readDeadline < firstDeadline && readDeadline > DateTime.Parse("1/1/1900"))
                    firstDeadline = readDeadline;
            }
        }
        // close connection, return values
        connection.Close();
        if (readContest > DateTime.Parse("1/1/1900"))
            st[9] = firstContest.ToShortDateString();
        if (readDeadline > DateTime.Parse("1/1/1900"))
            st[10] = firstDeadline.ToShortDateString();

        // close connection, return values
        connection.Close();
        return st;

    }
    //Get chapter name instead of ID
    public string GetChapterName2(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
             System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=NSF-"+
            ddlyear.SelectedItem.Text + "-Reg-" + ddlWeek.SelectedItem.Text.Replace(',', '-').ToString() + ".xls"); //.Substring(2, 2)
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
    protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataGrid1.Visible = false;
        lblError.Text = "";
        lblChapter.Text = "";
        loadWeeks();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string strqryRoundNumber = "select max([Round]) as RoundNumber from RankTrophy where ContestDate in (" + ddlWeek.SelectedItem.Value + ") and Year=" + ddlyear.SelectedValue + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqryRoundNumber);
        if(ds.Tables[0].Rows[0]["RoundNumber"].ToString().Equals(""))  
        {
            ViewState["ClickCount"] = 1;
        }
        else
        {
            string rndNum = ds.Tables[0].Rows[0]["RoundNumber"].ToString();
            ViewState["ClickCount"] = Convert.ToInt32(rndNum) + 1;
           
        }
        GetCounts();

        string dateAppend = (ddlWeek.SelectedValue.ToString().Replace("'", "")).Replace("/", "-");
        string[] contestdates = dateAppend.Split(',');
       

        if (DateTime.Now.Date.CompareTo(Convert.ToDateTime(contestdates[0]))==0)
        {
            btnSaveOnServer.Enabled = false;
        }
        else if (DateTime.Now.Date > (Convert.ToDateTime(contestdates[0])))
        {
            btnSaveOnServer.Enabled = false;
        }
        else
        {
            btnSaveOnServer.Enabled = true;
        }

    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataGrid1.Visible = false;
        lblError.Text = "";
        lblChapter.Text = "";

        btnSaveOnServer.Enabled = false;

    }
    protected void btnSave_OnServer(object sender, EventArgs e)
    {
       
        // validate if the current date = latest create date for the same contest date in the drop down.  

        if ((Int32)ViewState["ClickCount"] == 1 || (Int32)ViewState["ClickCount"] == 2 || (Int32)ViewState["ClickCount"] == 3)
        {
            
                            insertintoTables();
           
        }
        else if ((Int32)ViewState["ClickCount"] >3)
        {
            string getlatestCreateDate = "select Top 1 CreatedDate from RankTrophy where Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ") order by CreatedDate Desc ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, getlatestCreateDate);
            //Response.Write("latest date: "+retVal);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DateTime retVal = (DateTime)ds.Tables[0].Rows[0][0];
                int result = DateTime.Compare(DateTime.Now.Date, retVal.Date);
                if (result == 0)
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>confirm('This is a duplicate. Do you want to replace?');</script>", true);
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "confirm", "confirm('This is a duplicate. Do you want to replace?');", true);
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtooverwrite();", true);


                }
                else
                {
                    lblError.ForeColor = Color.Red;
                    lblError.Text = "Round 4 is not allowed";
                    lblError.Visible = true;
                }
            }
        }
       

    }
    protected void insertintoTables()
    {
        DataTable dt = (DataTable)Session["RankTrophy"];
        string sql = "";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["First"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["First"] = "null";
            }
            if (dt.Rows[i]["Second"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Second"] = "null";
            }
            if (dt.Rows[i]["Third"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Third"] = "null";
            }
            if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Year"] = "null";
            }
            if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["EventID"] = "null";
            }
            if (dt.Rows[i]["ContestDate"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ContestDate"] = "null";
            }
            if (dt.Rows[i]["ChapterID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ChapterID"] = "null";
            }
            if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductGroupID"] = "null";
            }
            if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductGroupCode"] = "null";
            }
            if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductID"] = "null";
            }
            if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductCode"] = "null";
            }
            sql = sql + "insert into RankTrophy (Year,EventID,ContestDate,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Reg,First,Second,Third,Round,CreatedDate,CreatedBy) values("

                  + dt.Rows[i]["Year"].ToString().Trim() + ","
                  + dt.Rows[i]["EventID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ContestDate"].ToString().Trim() + "',"
                  + dt.Rows[i]["ChapterID"].ToString().Trim() + ","
                  + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                  + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ProductCode"].ToString().Trim() + "',"
                   + dt.Rows[i]["Reg"].ToString().Trim() + ","
                  + dt.Rows[i]["First"].ToString().Trim() + ","
                  + dt.Rows[i]["Second"].ToString().Trim() + ","
                  + dt.Rows[i]["Third"].ToString().Trim() + ","
                  + ViewState["ClickCount"].ToString() + ",getDate(),"
                  + Session["LoginID"] + ")";

        }

        DataTable dt1 = (DataTable)Session["PartMedTrophy"];
        string sqlpart = "";
        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            if (dt1.Rows[i]["Year"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["Year"] = "null";
            }
            if (dt1.Rows[i]["EventID"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["EventID"] = "null";
            }
            if (dt1.Rows[i]["ContestDate"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["ContestDate"] = "null";
            }
            if (dt1.Rows[i]["ChapterID"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["ChapterID"] = "null";
            }
            sqlpart = sqlpart + "insert into PartMedTrophy  (Year,EventID,ContestDate,ChapterID,Registrations,[Unique],Round,CreatedDate,CreatedBy) values("
                  + dt1.Rows[i]["Year"].ToString().Trim() + ","
                  + dt1.Rows[i]["EventID"].ToString().Trim() + ",'"
                  + dt1.Rows[i]["ContestDate"].ToString().Trim() + "',"
                  + dt1.Rows[i]["ChapterID"].ToString().Trim() + ","
                  + dt1.Rows[i]["Registrations"].ToString().Trim() + ","
                  + dt1.Rows[i]["Unique"].ToString().Trim() + ","
                  + ViewState["ClickCount"].ToString() + ",getDate(),"
                  + Session["LoginID"] + ")";
        }

        int k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlpart);
        int j = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
        ViewState["ClickCount"] = ((Int32)ViewState["ClickCount"]) + 1;
        if (j > 0 && k > 0)
        {
            lblError.Visible = true;
            lblError.ForeColor = Color.Blue;
            lblError.Text = "Saved Successfully";
        }
    }

    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Write("before update");
            string todelete = "delete from RankTrophy where Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ")  delete from PartMedTrophy where Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ")";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, todelete);
            ViewState["ClickCount"] = 1;
            insertintoTables();
        }
        catch (Exception ex)
        {

        }
    }
}
