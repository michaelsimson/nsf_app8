<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="AllContestsWeeklyList.aspx.cs" Inherits="Reports_AllContestsWeeklyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="server">
    <link href="~/css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1 {
            width: 152px;
        }

        .style2 {
            width: 92px;
        }
    </style>
    <script type="text/javascript">

        function ConfirmToSave(sender) {

            var btnlabel = sender.value;

            if (confirm("Are you sure want to save in the database?")) {

                return true;
            }
            else {

                return false;

            }
        }
        function confirmtooverwrite() {
            try {

                if (confirm("The data was saved already for today.  Do you want to replace?")) {
                    // alert(document.getElementById('<%= hiddenbtn.ClientID %>'));
                    document.getElementById('<%= hiddenbtn.ClientID %>').click();

                }
                else {

                }
            } catch (ex) { }
        }

    </script>

    <br />
    <br />
    <table cellpadding="5px" cellspacing="0" border="0">
        <tr>
            <td colspan="8" align="center" class="title02">North South Foundation
                    <br />
                Total Medal Count</td>
        </tr>
        <tr>
            <td colspan="8" align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>
            </td>
        </tr>

        <tr>

            <td align="left" class="style1">
                <asp:Label ID="lblChapter" runat="server"></asp:Label>
            </td>
            <%--<td> Event <asp:DropDownList ID="ddlEvent" runat="server"> </asp:DropDownList></td>
            --%>
            <td width="40px" align="right" valign="middle">Year :</td>
            <td align="left" class="style2">
                <asp:DropDownList ID="ddlyear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" style=" margin-top:1px; height:22px;"></asp:DropDownList>
            </td>
            <td align="left">Contest Dates:
                    <asp:DropDownList ID="ddlWeek" runat="server"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged" style=" margin-top:1px; height:22px;">
                    </asp:DropDownList>
                <%--DataTextField="Weektext" DataValueField="weekid"--%> 
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px"
                    OnClick="btnSubmit_Click" /></td>
            <td align="left">
                <asp:Button ID="btnSave" runat="server" Text="Save as Excel" Width="100px" OnClick="btnSave_Click"></asp:Button></td>
            <td align="left">
                <asp:Button ID="btnSaveOnServer" Enabled="false" runat="server" Text="Save on Server" OnClick="btnSave_OnServer" OnClientClick="return ConfirmToSave(this)"></asp:Button></td>

            <asp:Button ID="hiddenbtn" runat="server" Text="Button" OnClick="hiddenbtn_Click" Style="display: none;" />
        </tr>
        <tr>
            <td colspan="8" align="center">
                <asp:Label ID="lblError" runat="server"></asp:Label>
                <br />
                <asp:DataGrid ID="DataGrid1" BorderStyle="Solid" BorderColor="Black" ItemStyle-HorizontalAlign="Left" runat="server"></asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnFlag" runat="server" />


</asp:Content>



