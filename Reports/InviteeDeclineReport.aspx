﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InviteeDeclineReport.aspx.vb" Inherits="Invitee_Decline_Report" title="Invitee Decline Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invitee Decline Report</title>
</head>
<body>
    <form id="form1" runat="server">
<div align="left"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
<asp:Button id="btnExport" runat="server" Text="Export Data" onclick="btnExport_Click" Width ="100px"></asp:Button><br /></div>

    <div align="center">
      <H3>Invitee Decline Report - 
        <asp:Literal ID="ltlYear" runat="server"></asp:Literal></H3>
            Select Year : <asp:DropDownList ID="ddlyear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged">  </asp:DropDownList><br /><br />

        <asp:GridView ID="GVInvitee" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#3366CC" BorderStyle="Solid"  BorderWidth="1px" CellPadding="4" Visible ="true">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <Columns>
                <asp:BoundField DataField="ChapterCode" HeaderText="ChapterCode" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="GngtoIndia" HeaderText="Going to India" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="InconTime" HeaderText="Inconvenient time" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Inconvenue" HeaderText="Inconvenient venue" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="Expensive" HeaderText="Expensive to attend" HeaderStyle-ForeColor="white" />
                <asp:BoundField DataField="NoInterest" HeaderText="Not Interested" HeaderStyle-ForeColor="white" />
            </Columns>
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        </asp:GridView>
    </div>
        <table border="0" cellpadding = "2" cellspacing = "0" align="left" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

