<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContestsByDate.aspx.cs" Inherits="Reports_ContestsByDate" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
    <script type="text/javascript" language="javascript">
        function hide_table(var1) {
            document.getElementById(var1).style.display = "none";
        }
        function show_table(var1) {
            document.getElementById(var1).style.display = "block";
        }
        function hideshow_table(var1, var2) {
            document.getElementById(var1).style.display = "none";
            document.getElementById(var2).style.display = "block";
        }
    </script>
    <%--<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upcoming Contest </title>
</head>
<body>--%>

    <h3 align="center">List of Chapters (by Contest Type and Date)</h3>
    <h3><%--<asp:Button id="btnBack" runat="server" Text="Back" OnClick="btnBack_Click"  ></asp:Button>--%>
        <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink></h3>
    <table width="100%">
        <tr>
            <td style="width: 10%"></td>
            <td>
                <div style="text-align: center;">
                    <table style="width: 70%;">
                        <tr>
                            <td style="width: 20%"></td>
                            <td style="font-weight: bold; width: 30%">
                                <asp:Label ID="lblselect" runat="server" Text="Please select one"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="drpcontact" runat="server">
                                    <asp:ListItem Value="1">Upcoming Contest</asp:ListItem>
                                    <asp:ListItem Value="2">Contact details</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlContest" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged">
                                    <asp:ListItem Value="1">Spelling</asp:ListItem>
                                    <asp:ListItem Value="2">Vocabulary</asp:ListItem>
                                    <asp:ListItem Value="3">Math</asp:ListItem>
                                    <asp:ListItem Value="4">Geography</asp:ListItem>
                                    <asp:ListItem Value="5">Essay</asp:ListItem>
                                    <asp:ListItem Value="6">Public Speaking</asp:ListItem>
                                    <asp:ListItem Value="7">Science</asp:ListItem>
                                    <asp:ListItem Value="9">BrainBee</asp:ListItem>
                                    <asp:ListItem Value="8">All Contests</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td align="center">
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                                &nbsp;&nbsp;&nbsp;<asp:Button ID="btnExport" Visible="false" runat="server" Text="Export"
                                    OnClick="btnExport_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />

                <asp:Button ID="btnChapterList" runat="server" Text="Chapter List" OnClick="btnChapterList_Click" />

                <asp:DataGrid ID="DataGrid1" runat="server"
                    AutoGenerateColumns="False" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px"
                    BorderStyle="None" BorderColor="#999999" Width="760px">
                    <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
                    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                    <AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
                    <ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
                    <HeaderStyle Font-Bold="True" ForeColor="black" BackColor="Gray"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="Date" HeaderText="Contest Date">
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Chapter" HeaderText="Chapter">
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Contests" HeaderText="Contests">
                            <HeaderStyle Width="50%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Count" HeaderText="Total">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>

                </asp:DataGrid>

                <br />
                <asp:DataGrid ID="DataGrid2" runat="server"
                    AutoGenerateColumns="False" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px"
                    BorderStyle="None" BorderColor="#999999" Width="1139px">
                    <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
                    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                    <AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
                    <ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
                    <HeaderStyle Font-Bold="True" ForeColor="black" BackColor="Gray"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="Name" HeaderText="Name">
                            <HeaderStyle Width="12%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="hPhone" HeaderText="Home Phone">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="cPhone" HeaderText="Cell Phone">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="wPhone" HeaderText="Work Phone">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Email" HeaderText="Email">
                            <HeaderStyle Width="12%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Date" HeaderText="Contest Date">
                            <HeaderStyle Width="10%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Chapter" HeaderText="Chapter">
                            <HeaderStyle Width="12%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Contests" HeaderText="Contests">
                            <HeaderStyle Width="18%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Count" HeaderText="Total">
                            <HeaderStyle Width="4%"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>

                </asp:DataGrid>
                <asp:HiddenField ID="hdnTechCN" runat="server" />

            </td>
        </tr>
    </table>
    <div style="clear:both; margin-bottom:10px;"></div>
    <div style="width:800px; margin-left:auto;">
        <asp:Button ID="BtnExportAll" runat="server" Visible="false" OnClick="BtnExportAll_Click" Text="Export All" />
    </div>
    <div style="width:800px; margin-left:auto;">
        <div runat="server" id="dvTable1" visible="false" style="float: left;">
            <center>
                <asp:Label ID="lblTable1" Font-Bold="true" runat="server" Text="Table 1: Weekend March 17-18, 2018"></asp:Label></center>
            <div style="position: relative;">
                <asp:Button ID="BtnExportTable1" runat="server" Text="Export Excel" OnClick="BtnExportTable1_Click" />
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>

                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="dlData" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="margin-bottom: 10px; width:200px;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ChapterName" HeaderText="Chapter Name"></asp:BoundField>


                    </Columns>

                </asp:GridView>

                <%--  <asp:DataList ID="" runat="server" RepeatDirection="Horizontal"
                RepeatColumns="3" AlternatingItemStyle-BackColor="#EEEEEE" ItemStyle-BackColor="Gainsboro" BorderStyle="None" BorderColor="#999999">

                <ItemTemplate>
                    <table>
                        <tr>
                            <td><%#Eval("ChapterName") %></td>
                            <td>&nbsp;</td>
                        </tr>

                    </table>
                </ItemTemplate>
            </asp:DataList>--%>
            </center>
        </div>

        <div runat="server" id="dvTable2" visible="false" style="float: left; margin-left:50px;">
            <center>
                <asp:Label ID="lblTable2" Font-Bold="true" runat="server" Text="Table 2: Weekend March 17-18, 2018"></asp:Label></center>
            <div style="position: relative;">
                <asp:Button ID="BtnExportTable2" runat="server" Text="Export Excel" OnClick="BtnExportTable2_Click" />
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="ddlTable2" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="margin-bottom: 10px; width:200px;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ChapterName" HeaderText="Chapter Name"></asp:BoundField>


                    </Columns>

                </asp:GridView>
                <%--  <asp:DataList ID="" runat="server" RepeatDirection="Horizontal"
                RepeatColumns="3" AlternatingItemStyle-BackColor="#EEEEEE" ItemStyle-BackColor="Gainsboro" BorderStyle="None" BorderColor="#999999">

                <ItemTemplate>
                    <table>
                        <tr>
                            <td><%#Eval("ChapterName") %></td>
                            <td>&nbsp;</td>
                        </tr>

                    </table>
                </ItemTemplate>
            </asp:DataList>--%>
            </center>
        </div>

        <div runat="server" id="dvTable3" visible="false" style="float: left; margin-left:50px;">
            <center>
                <asp:Label ID="lblTable3" Font-Bold="true" runat="server" Text="Table 3: Weekend March 17-18, 2018"></asp:Label></center>
            <div style="position: relative;">
                <asp:Button ID="btnExportExcel3" runat="server" Text="Export Excel" OnClick="btnExportExcel3_Click" />
            </div>
            <center>

                <div style="clear: both; margin-bottom: 5px;"></div>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="ddlTable3" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="margin-bottom: 10px; width:200px;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ChapterName" HeaderText="Chapter Name"></asp:BoundField>


                    </Columns>

                </asp:GridView>
                <%-- <asp:DataList ID="" runat="server" RepeatDirection="Horizontal"
                RepeatColumns="3" AlternatingItemStyle-BackColor="#EEEEEE" ItemStyle-BackColor="Gainsboro" BorderStyle="None" BorderColor="#999999">

                <ItemTemplate>
                    <table>
                        <tr>
                            <td><%#Eval("ChapterName") %></td>
                            <td>&nbsp;</td>
                        </tr>

                    </table>
                </ItemTemplate>
            </asp:DataList>--%>
            </center>
        </div>
    </div>
    <asp:HiddenField ID="hdnDate1" runat="server" Value="" />
    <asp:HiddenField ID="hdnDate2" runat="server" Value="" />
    <asp:HiddenField ID="hdnDate3" runat="server" Value="" />

    <asp:HiddenField ID="hdntable1Title" runat="server" Value="" />
    <asp:HiddenField ID="hdntable2Title" runat="server" Value="" />
    <asp:HiddenField ID="hdntable3Title" runat="server" Value="" />
</asp:Content>
