<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InputNationalCriteria.aspx.cs" Inherits="Reports_InputNationalCriteria" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div><h2>
        Criteria for National Invitees by Contest and Grade<br /></h2>
        <asp:Label ID="lblMessage" runat="server"></asp:Label><br />
        <br />
        <br />
        <asp:Button ID="btnRecalc" runat="server" Text="Recalculate" OnClick="btnRecalc_Click" Visible="False" />&nbsp;<asp:Button
            ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" /><br />
        <br />
        <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <Columns>
                <asp:BoundField DataField="Contest" HeaderText="Contest" />
                <asp:TemplateField HeaderText="Grade">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Minimum Score">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MinScore") %>'></asp:TextBox>&nbsp;
                    </ItemTemplate>                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rank1Score">
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtRank1Score1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lblRank1Score" runat="server" Text='<%# Bind("Rank1Score") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="New Chapter Score">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNewChScore1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtNewChScore" runat="server" Text='<%# Bind("NewChScore") %>'></asp:TextBox>&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="MaxScore">
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtMaxScore1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtMaxScore" runat="server" Text='<%# Bind("MaxScore") %>'></asp:TextBox>&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        </asp:GridView>
        <asp:GridView ID="gvOut" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" Visible="False">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <Columns>
                <asp:BoundField DataField="Contest" HeaderText="Contest" />
                <asp:BoundField DataField="Grade" HeaderText="Grade" />
                <asp:BoundField DataField="MinScore" HeaderText="Min Score" />
                <asp:BoundField DataField="Count" HeaderText="Count" />
                <asp:BoundField DataField="TotalCount" HeaderText="Cumulative" />
                <asp:BoundField DataField="Contestants" HeaderText="# of Contestants" />
                <asp:BoundField DataField="Invited" HeaderText="% Invited" />
                <asp:BoundField DataField="AddRank1" HeaderText="Incr. Rank 1" />
                <asp:BoundField DataField="TotalWrank1" HeaderText="Total W/ Rank1" />
                <asp:BoundField DataField="CumulWrank1" HeaderText="Cumulative W/ Rank1" />
            </Columns>
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        </asp:GridView>
        &nbsp;
    </div>
    </form>
</body>
</html>


 
 
 