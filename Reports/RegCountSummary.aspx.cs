﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;


public partial class Reports_RegCountSummary : System.Web.UI.Page
{
    string lbdate;
    int newval;
    int Eventid;
    int seval;
    int sessval;
    int fiscyear;
    string Zoneqry;
    string StaYear = System.DateTime.Now.Year.ToString();
    string calc;
    int Start;
    int endall;
    int i;
    string Qrycontestid;
    string TblNmae;
    string Qryjoin;
    string Qry;
    string joinquery;
    string Qrycondition;
    DataTable dtnewgrid;
    DataTable Mergecopy = new DataTable();
    DataSet dsnew;
    string Qryvaluewhere = string.Empty;
    public Int32 ExpchapterID = 0;
    public Int32 ExpparentID = 0;
    public Int32 ExpeventID = 0;
    bool parentcondition = false;
    bool childcondition = false;
    bool chaptercondition = false;
    bool ischapcon = false;
    bool isparentevent = false;
    bool isworkshop = false;
    bool isprepclub = false;
    bool isgame = false;
    bool isfisc = false;
    string sqlStr;
    int Eventval;
    bool isprepclubval = false;
    bool isFiscEvent = false;
    string strnewcontest;
    bool iscontest= false;
    string WCNTQry = string.Empty;
    string WCNTQryreg = string.Empty;
    string WCNTQrycoachreg = string.Empty;
    string WCNTQryprepclub = string.Empty;
    string WCNTQrynew = string.Empty;
    string WCNTQrynew1 = string.Empty;
    string WCNTQrynew2 = string.Empty;
    string WCNTQrynew3 = string.Empty;
    string CommonQry = string.Empty;
    string CommonQryContest = string.Empty;
    string CommonQryReg = string.Empty;
    string CommonQrycoachreg = string.Empty;
    string CommonQryprepclup = string.Empty;
    string CommonQryContestchapter1 = string.Empty;
    string WCNTQryContest1 = string.Empty;
    string WCNTQrynewContest1 = string.Empty;
    string CommonQryGame = string.Empty;
    string WCNTQryGame = string.Empty;
    string WCNTQrynewGame = string.Empty;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Session["LoginID"] == null)
            //{
            //    Response.Redirect("~/Maintest.aspx");
            //}
            //else
            //{
            Session["RoleId"] = "2";
            Session["LoginID"] = "4240";
                if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || (Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5")))
                {
                    Iddonation.Visible = true;
                    DDlfrontchoice(DDchoice);
                    Event();
                    Yearscount();
                    years();
                    Pnldisp.Visible = false;
                    LBbacktofront.Visible = true;
                }
                if ((Session["RoleId"].ToString() == "5"))
                {
                    Chapter();
                    Cluster();
                    Event();
                    Yearscount();
                    years();
                }
                else if ((Session["RoleId"].ToString() == "3"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct zoneid from volunteer where MemberID='" + Session["LoginID"] + "' and TeamLead='" + Session["TL"] + "'  and zoneid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["zoneid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Zone();
                        Cluster();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have ZoneCode ";
                    }
                }
                else if ((Session["RoleId"].ToString() == "4"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct clusterid from volunteer where MemberID='" + Session["LoginID"] + "' and clusterid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["clusterid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Cluster();
                        Zone();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have Cluster ";
                    }
                }
                else
                {
                    Zone();
                    Event();
                    Yearscount();
                    years();
                }
            }
       // }
    }
    protected void DDlfrontchoice(DropDownList DDobject)
    {
        DDobject.Items.Clear();
        DDobject.Items.Insert(0, new ListItem("By Contest Group", "3"));
        DDobject.Items.Insert(0, new ListItem("By Contest", "4"));
        DDobject.Items.Insert(0, new ListItem("By Event", "2"));
        DDobject.Items.Insert(0, new ListItem("By Chapter", "1"));
        DDobject.Items.Insert(0, new ListItem("[Select Report]", "0"));
    }
    protected void Event()
    {
        ddevent.Items.Clear();
        if ((Session["RoleId"].ToString() == "1") || ((Session["RoleId"].ToString() == "2")))
        {
            ddevent.Items.Insert(0, new ListItem("Coaching", "13"));
            ddevent.Items.Insert(0, new ListItem("Game", "4"));
        }
        ddevent.Items.Insert(0, new ListItem("PrepClub", "19"));
        ddevent.Items.Insert(0, new ListItem("Workshop", "3"));
        ddevent.Items.Insert(0, new ListItem("Finals", "1"));
        ddevent.Items.Insert(0, new ListItem("Chapter", "2"));
        ddevent.Items.Insert(0, new ListItem("All", "-1"));
        ddevent.Items.Insert(0, new ListItem("[Select Event]", "0"));
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    #region Zone
    private string GetZoneQuery()
    {
        string ZoneQuery = "Select distinct Name,ZoneId from Zone where Status='A'";
        string ZoneFields = " Select ZoneCode,ZoneId from ";
        switch (Session["RoleId"].ToString())
        {
            case "5":
                return ZoneFields + " chapter where ChapterID='" + ddchapter.SelectedValue + "'";
            case "3":
                return ZoneFields + " Zone where  ZoneId='" + Txthidden.Text + "'  and ZoneCode is not null";
            case "4":
                return ZoneFields + " Cluster where ClusterId='" + Txthidden.Text + "'";
            case "1":
            case "2":
            case "84":
            case "85":
            default:
                return ZoneQuery;
        }
        return ZoneQuery;
    }
    private void InsertZoneDropDown(string Caption, string Value, bool IsEnabled)
    {
        ddZone.Items.Insert(0, new ListItem(Caption, Value));
        ddZone.Enabled = IsEnabled;
    }
    private void Zone()
    {
        try
        {
            ddZone.Items.Clear();
            switch (ddevent.SelectedItem.Text)
            {
                case "All":
                    InsertZoneDropDown("All", "-1", true);
                    ddCluster.Items.Insert(0, "All");
                    return;
                case "Finals":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "1", false);
                    return;
                case "Coaching":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "13", false);
                    return;
                case "Game":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "9", false);
                    return;

            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetZoneQuery());
            ddZone.DataSource = ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddZone.DataValueField = "ZoneId";
                ddZone.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddZone.Enabled = false;
                }
                else
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        ddZone.Items.Insert(0, "All");
                    }
                    else
                    {
                        ddZone.Items.Insert(0, "All");
                        ddZone.Items.Insert(0, new ListItem("[Select Zone]", "-1"));
                       
                    }
                    ddZone.Enabled = true;
                }
            }
            else
            {
                ddZone.Items.Insert(0,new ListItem("[Select Zone]","-1"));
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    #endregion
    protected void Cluster()
    {
        try
        {
            ddCluster.Enabled = true;
            ddCluster.Items.Clear();
            string ClusterQuery = "select ClusterId,clustercode from ";
            switch (Session["RoleId"].ToString())
            {
                case "5":
                    ClusterQuery = ClusterQuery + " chapter where ChapterID='" + ddchapter.SelectedValue + "'";
                    break;
                case "4":
                    ClusterQuery = ClusterQuery + " Cluster  where clusterId='" + Txthidden.Text + "'";
                    break;
                case "3":
                    ClusterQuery = ClusterQuery + " Cluster  where clusterId='" + Txthidden.Text + "'";
                    break;
                default:
                    if (ddZone.SelectedItem.Text == "All")
                    {
                        ClusterQuery = "select  distinct clustercode,clusterid from cluster where Status='A' ";
                    }
                    else
                    {
                        ClusterQuery = "select distinct Name as clustercode,ClusterID from Cluster where Status='A' and ZoneID='" + ddZone.SelectedValue + "'";
                    }
                    break;
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, ClusterQuery);
            ddCluster.DataSource = ds;
            ddCluster.DataTextField = "clustercode";
            ddCluster.DataValueField = "ClusterId";
            ddCluster.DataBind();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
            {
               
            }
            else
            {
                ddCluster.Enabled = true;
            }

            if (ddZone.SelectedItem.Text == "All")
            {

                ddCluster.Items.Insert(0, new ListItem("All", "-1"));
            }
            else if (ddevent.SelectedItem.Text == "Finals")
            {

                ddCluster.Items.Insert(0, new ListItem("Finals", "1"));
                ddCluster.Enabled = false;
            }
            else if (ddevent.SelectedItem.Text == "Coaching")
            {

                ddCluster.Items.Insert(0, new ListItem("Coaching", "13"));
                ddCluster.Enabled = false;
            }
            else if (ddevent.SelectedItem.Text == "Game")
            {

                ddCluster.Items.Insert(0, new ListItem("Game", "58"));
                ddCluster.Enabled = false;
            }
            else
            {
                 ddCluster.Items.Insert(0, "[Select Cluster]");

            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Chapter()
    {
        try
        {
            ddchapter.Enabled = true;
            if ((Session["RoleId"].ToString() == "5"))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from volunteer where  MemberID='" + Session["LoginID"] + "' and ChapterId is not null");
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddchapter.Items.Clear();
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;
                }
                else
                {
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode,[State] from chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        Cluster();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "All")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterid,chaptercode,[state] from chapter where clusterId='" + ddCluster.SelectedValue + "' and ChapterId is not null order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.Enabled = true;
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else
            {
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    if (ddCluster.SelectedItem.Text == "All")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, "All");
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Finals, US", "1"));
                        ddchapter.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text == "Coaching")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Coaching,US", "112"));
                        ddchapter.Enabled = false;

                    }
                    else if (ddevent.SelectedItem.Text == "Game")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Game,US", "117"));
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ChapterID,[state],chaptercode from Chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                        ddchapter.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddchapter.DataTextField = "Name";
                            ddchapter.DataValueField = "ChapterID";
                            ddchapter.DataBind();
                            ddchapter.Items.Insert(0, "All");
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                        else
                        {
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                    }
                }
                else
                {
                    ddchapter.Items.Clear();
                    ddchapter.Items.Insert(0, "[Select Chapter]");
                }
            }
            if (ddCluster.SelectedItem.Text == "All")
            {
                string ClusterFlqry;
                string WhereCluster;
                ClusterFlqry = "select distinct chapterid,chaptercode,[State] from chapter where  ChapterId is not null";
                WhereCluster = ClusterFlqry + Filterdropdown();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, WhereCluster);
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                ddchapter.Enabled = true;
                ddchapter.DataTextField = "chaptercode";
                ddchapter.DataValueField = "ChapterID";
                ddchapter.DataBind();
                ddchapter.Items.Insert(0, "All");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string Filterdropdown()
    {
        string iCondtions = string.Empty;
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ClusterId=" + ddCluster.SelectedValue;
            }
        }
        return iCondtions + "  order by [State],ChapterCode";
    }
    protected void DDchoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            IDchapter.Visible = true;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "3")
        {
            Divevent.Visible = false;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
            Divcontest.Visible = false;
            Divcontestgroup.Visible = true;
        }
          else if (DDchoice.SelectedItem.Value.ToString() == "4")
        {
            Divevent.Visible = false;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
            Divcontestgroup.Visible = false;
            Divcontest.Visible = true;
        }
        else
        {
            Divevent.Visible = true;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
        }
    }
    protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddevent.SelectedItem.Value.ToString() == "13")
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                Lbreport.Visible = true;
                DDReport.Enabled = true;
                DDReport.Visible = true;
            }
        }
        else
        {
            Lbreport.Visible = false;
            DDReport.Visible = false;
        }
        Zone();
        Cluster();
        Chapter();
    }
    protected void ddZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cluster();
        Chapter();
    }
    protected void ddCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        Chapter();
    }
    protected void Yearscount()
    {
        ddNoyear.Items.Clear();
        ddNoyear.Items.Add("[Select No of Years]");
       
        for (int i = 1; i <= 10; i++)
        {
            ddNoyear.Items.Add(i.ToString());
        }
       }
    protected void years()
    {
        DDyear.Items.Clear();
        DDyear.Items.Add("Year");
        DDyear.Items.Add("Calendar Year");
        DDyear.Items.Add("Fiscal Year");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if ((ddevent.SelectedValue.ToString() == "13"))
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (DDReport.SelectedItem.Text != "Select Report")
                {
                    buttonclick();
                }
                else
                {
                    lblall.Visible = true;
                    Gridcontestant.Visible = false;
                }
            }
            else
            {
                buttonclick();
            }

        }
        else
        {
            buttonclick();
        }
    }
    protected string genWhereConditons()
    {
        string iCondtions = string.Empty;
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (ddevent.SelectedItem.Text != "[Select Event]")
            {
                if (ddevent.SelectedItem.Text != "All")
                {
                    iCondtions += " and cs.EventID=" + ddevent.SelectedValue;
                }
            }
            if (ddZone.SelectedItem.Text != "[Select Zone]")
            {
                if (ddZone.SelectedItem.Text != "All")
                {
                    if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                    {
                        if (ddevent.SelectedItem.Text != "All")
                        {
                            iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                        }
                    }
                    else
                    {
                        if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                        {
                            if (ddevent.SelectedItem.Text != "All")
                            {
                                iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                            }
                        }
                    }
                }
            }
            if (ddCluster.SelectedItem.Text != "[Select Cluster]")
            {
                if (ddCluster.SelectedItem.Text != "All")
                {
                    if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                    {
                        if (ddevent.SelectedItem.Text != "All")
                        {
                            iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                        }
                    }
                    else
                    {
                        if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                        {
                            if (ddevent.SelectedItem.Text != "All")
                            {
                                iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                            }
                        }
                    }
                }
            }
            if (ddchapter.SelectedItem.Text != "[Select Chapter]")
            {
                if (ddchapter.SelectedItem.Text != "All")
                {
                    if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                    {
                        if (ddevent.SelectedItem.Text != "All")
                        {
                            iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                        }
                    }
                    else
                    {
                        if ((ddchapter.SelectedItem.Text != "Coaching,US") && ((ddchapter.SelectedItem.Text != "Game,US")))
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                if (ddchapter.SelectedItem.Value.ToString() != "1")
                                {
                                    iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                                }
                            }
                            else
                            {
                                if (ddevent.SelectedItem.Text != "All")
                                {
                                    iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                                }
                            }
                        }
                    }
                }
            }
        }
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        string Tot = string.Empty;
        string strtot = string.Empty;
        string Totwhere = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        Totwhere = "(" + Tot + ")!=0";
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (parentcondition == true)
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + ")as tblview group by  ChapterID,parentID,ParentName,year) select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else
                    {
                        if (isparentevent == true)
                        {
                            if ((Eventval == 1) || (Eventval == 2))
                            {
                                return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,cs.contestyear) select EventID,Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventID,Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                        else if (isworkshop == true)
                        {
                            return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventID,Chapterid,MemberID as ID ,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else if (isgame == true)
                        {
                            return iCondtions + "  select  EventID,MemberID as ParentID,ParentName" + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + ") select EventId,ParentID  ,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                return iCondtions + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,cs.contestyear) select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                        else
                        {
                            if ((Eventval == 1) || (Eventval == 2))
                            {
                                return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,cs.contestyear) select EventID,Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventID,Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3") || ((ddevent.SelectedItem.Value.ToString() == "19")))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,Chapterid,MemberId as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if (ddevent.SelectedItem.Value.ToString() == "13")
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            return iCondtions + " group by  cs.ChapterID,cs.CMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID as ID,CoachName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " group by  cs.ChapterID,cs.CMemberID,cs.PMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select CMemberID,ParentID as PMemberId,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        return iCondtions;
                    }
                }
            }
            else if (childcondition == true)
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + ")as tblview group by   ChapterID,parentid,ChildNumber,ChildName,grade,year) select Chapterid,ParentID  ,ChildName,grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else
                    {
                        if (ischapcon == true)
                        {
                            if (isprepclubval == true)
                            {
                                return iCondtions + " group by    cs.EventId,cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID  ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                if ((Eventid == 1) || (Eventid == 2))
                                {
                                    return iCondtions + " group by    cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,cs.contestyear) select Chapterid,ParentID  ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                                else
                                {
                                    return iCondtions + " group by    cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID  ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                            }
                        }
                        else
                        {
                            return iCondtions + ")as tblview group by   EventID,parentid,ChildNumber,ChildName,grade,year) select EventID,ParentID  ,ChildName,grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + " group by   cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,cs.contestyear) select Chapterid,ParentID  ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " group by   cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID  ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3") || ((ddevent.SelectedItem.Value.ToString() == "19")))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + " group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " group by   cs.EventID,cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Chapterid,ParentID ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if (DDReport.SelectedItem.Text == "Report by Parent")
                            {
                                return iCondtions + " group by   cs.PMemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.ProductGroupCode,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select ParentID ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " group by   cs.ChapterID,cs.CMemberID ,PMemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.ProductGroupCode,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select CoachID,PMemberID,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                        else
                        {
                            return iCondtions + " group by   cs.PMemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.ProductGroupCode,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select ParentID ,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "2")
                        {
                            return iCondtions + "  group by   cs.EventId,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))  select ParentID ,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + "  group by   cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select ParentID ,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        return iCondtions;
                    }
                }
            }
            else
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + " ) as newtbl group by  ChapterID,ChapterCode,year) select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        if (chaptercondition == true)
                        {
                            return iCondtions + " ) as newtbl group by EventId,ProductGroupCode,contest,ProductCode,year) select EventId,ProductGroupCode,contest,ProductCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " ) as newtbl group by contest,ProductGroupCode,year) select contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupCode asc";
                        }
                    }
                    else
                    {
                        if (chaptercondition == true)
                        {
                            return iCondtions + " ) select EventId,ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " ) as newtbl group by  EventId,EventCode,year) select EventId,EventCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                }
                else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            return iCondtions + " group by cs.PMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + "group by cs.CMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select ParentID as CMemberID,CoachName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        if (iscontest == true)
                        {
                            return iCondtions + " group by  cs.EventID, cs.ProductGroupCode,p.name, cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,ProductGroupCode,Name as Contest,productcode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "3")
                            {
                                return iCondtions + " group by  P.name,P.productgroupid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by productgroupid asc";
                            }
                            else
                            {
                                return iCondtions + " group by  P.name,P.Productid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Productid asc";
                        }
                        }
                    }
                    else
                    {
                        return iCondtions + " group by cs.Eventid,cs.PMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                }
                else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + " group by cs.MemberID,I.FirstName,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select MemberID as PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        if (iscontest == true)
                        {
                            return iCondtions + " group by  cs.EventID, cs.ProductGroupCode,p.name, cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,ProductGroupCode,Name as Contest,productcode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "3")
                            {
                                return iCondtions + " group by  P.name,P.ProductGroupid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupid asc";
                            }
                            else
                            {
                                return iCondtions + " group by  P.name,P.Productid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Productid asc";
                            }
                        }
                    }
                    else
                    {
                        return iCondtions + " group by cs.EventID,cs.MemberID,I.FirstName,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,MemberID as PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                }
                else
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        if ((ddevent.SelectedItem.Value.ToString() == "1") || ((ddevent.SelectedItem.Value.ToString() == "2")))
                        {
                            return iCondtions + " group by  cs.ChapterID,ch.ChapterCode,cs.contestyear) select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        if (chaptercondition == true)
                        {
                            if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                                {
                                    if ((ddevent.SelectedItem.Value.ToString() == "1") || ((ddevent.SelectedItem.Value.ToString() == "2")))
                                    {
                                        return iCondtions + " group by  cs.EventID, cs.ProductGroupCode,p.name, cs.ProductCode,cs.contestyear) select EventId,ProductGroupCode,Name as Contest,productcode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                    }
                                    else
                                    {
                                        return iCondtions + " group by  cs.EventID, cs.ProductGroupCode,p.name, cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,ProductGroupCode,Name as Contest,productcode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                    }
                                }
                              else
                            {
                            if ((seval == 1) || (seval == 2))
                            {
                                return iCondtions + " group by  cs.EventID,cs.chapterID,ch.Chaptercode,cs.contestyear) select EventId,ChapterId,Chaptercode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " group by  cs.EventID,cs.chapterID,ch.Chaptercode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,ChapterId,Chaptercode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            }
                        }
                        else
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                                {
                                     if (DDchoice.SelectedItem.Value.ToString() == "3")
                                     {
                                         return iCondtions + " group by  P.name,P.productgroupid,contestyear) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by productgroupid asc ";
                                     }
                                    else
                                     {
                                    return iCondtions + " group by  P.name,P.productid,contestyear) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by productid asc ";
                                }
                                }
                                else
                                {
                                    return iCondtions + " group by  cs.EventID,cs.EventCode,contestyear) select EventId,EventCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                            }
                            else
                            {
                                if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                                {
                                    if (DDchoice.SelectedItem.Value.ToString() == "3")
                                    {
                                        return iCondtions + " group by  P.name,P.productgroupid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by productgroupid asc";
                                    }
                                    else
                                    {
                                        return iCondtions + " group by  P.name,P.productid,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by productid asc";
                                    }
                                }
                                else
                                {
                                    return iCondtions + " group by  cs.EventID,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) select EventId,EventCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (parentcondition == true)
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + " ) as newtbl group by  ChapterID,ParentID,ParentName,year) select ChapterID,ParentId as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else
                    {
                        return iCondtions + "  select EventID,ChapterID,ParentId as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + "  select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + "  select EventID,Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3") || ((ddevent.SelectedItem.Value.ToString() == "19")))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + "  select Chapterid,ParentID as Id,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + "  select EventID,Chapterid,ParentID as Id,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            return iCondtions + "  select Chapterid,ParentID as ID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + "  select CMemberID,PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        return iCondtions;
                    }
                }
            }
            else if (childcondition == true)
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + " ) as newtbl group by  ChapterID,ParentID,ChildNumber,ChildName,Grade,year) select ChapterID,ParentId ,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else
                    {
                        return iCondtions + "  select EventID,ParentId ,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            return iCondtions + " select ParentID,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "1")
                            {
                                return iCondtions + " select coachId,PMemberID,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " select PMemberID,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + " select ParentID,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " select EventId,ParentID,ChildNumber,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + " select Chapterid,ParentID,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            return iCondtions + " select Chapterid,ParentID,ChildName,Grade " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                }
            }
            else
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        return iCondtions + " ) as newtbl group by  ChapterID,ChapterCode,year) select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                    }
                    else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        if (chaptercondition == true)
                        {
                            return iCondtions + " ) as newtbl group by  EventId,ProductGroupCode,contest,ProductCode,year) select  EventId,ProductGroupCode,contest,ProductCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "3") 
                            {
                                return iCondtions + " ) as newtbl group by  ProductGroupcode,Contest,year) select  Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupcode asc";
                            }
                            else
                            {
                                return iCondtions + " ) as newtbl group by  ProductGroupcode,Contest,year) select  Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupcode asc";
                        }
                        }
                    }
                    else
                    {
                        if (isfisc == true)
                        {
                            if (seval == 13)
                            {
                                return iCondtions + " select EventID,PMemberID as ParentId,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            if (seval == 4)
                            {
                                return iCondtions + " select EventID,MemberID as ParentId,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + " select EventId,ChapterId,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                        else
                        {
                            return iCondtions + " ) as newtbl group by  EventId,EventCode,year) select EventId,EventCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            if (chaptercondition == true)
                            {
                                return iCondtions + "  select EventID, ProductGroupCode,Name as Contest,ProductCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "3")
                                {
                                    return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupid asc";
                                }
                                else
                                {
                                    return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Productid asc";
                                }
                            }
                        }
                        else
                        {
                            if (DDReport.SelectedItem.Text == "Report by Parent")
                            {
                                return iCondtions + "  select  PMemberID,ParentName" + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "1")
                                {
                                    return iCondtions + "  select  CMemberID,CoachName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                                else
                                {
                                    return iCondtions + "  select  PMemberID,ParentName " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                                }
                            }
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + "  select  MemberID as PMemberID,ParentName" + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            if (chaptercondition == true)
                            {
                                return iCondtions + "  select EventID, ProductGroupCode,Name as Contest,ProductCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "3")
                                {
                                    return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupid asc";
                                }
                                else
                                {
                                return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Productid asc";
                            }
                            }
                        }
                        else
                        {
                            return iCondtions + "  select  MemberID as PMemberID,ParentName" + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                    }
                    else
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            return iCondtions + "  select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            if (chaptercondition == true)
                            {
                                return iCondtions + "  select EventID, ProductGroupCode,Name as Contest,ProductCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "3")
                                {
                                    return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by ProductGroupid asc";
                                }
                                else
                                {
                                    return iCondtions + "  select Contest " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Productid asc";
                            }
                            }
                        }
                        else
                        {
                            if (chaptercondition == true)
                            {
                                return iCondtions + "  select EventID,ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                            else
                            {
                                return iCondtions + "  select EventID,EventCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";
                            }
                        }
                    }
                }
            }
        }
    }
    protected void buttonclick()
    {
        try
        {
            int numval;
            chaptercondition = false;
            if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
            {
                lblall.Visible = false;
                if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
                {
                    calc = ddNoyear.SelectedItem.Text;
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
                }
              
                if (DDyear.SelectedItem.Text != "Fiscal Year")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        if (ddevent.SelectedItem.Text == "All")
                        {
                            if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                            {
                                Qryvaluewhere = genWhereConditons1();
                                string sqlStr = "with tbl as (select chapterid,chaptercode,year,SUM(amt) as amt from(select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" 
                                    + endall + "' and '" + Start + "' and cs.EventId=1 and cs.ChapterID=1 and  PaymentReference is not null" 
                                    + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.contestyear union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where    cs.contestyear   between '" 
                                    + endall + "' and '" + Start + "'  and cs.EventId=2 and cs.ChapterID!=1 and PaymentReference is not null" 
                                    + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.contestyear union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=3 "
                                    + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=19 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                Qrycondition = sqlStr + genWhereConditons();
                                Qry = Qrycondition;
                            }
                            else
                            {
                                Qryvaluewhere = genWhereConditons1();
                                string sqlqryforcoach = "select 117 as chapterid,'Game,US' as chaptercode,year,SUM(amt) from(select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and cs.EventId=4 and cs.ChapterID=117 and PaymentReference is not null " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year";
                                string sqlStr = "with tbl as (select chapterid,chaptercode,year,SUM(amt) as amt from(select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and cs.EventId=1 and cs.ChapterID=1 and  PaymentReference is not null" + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.contestyear union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "'  and cs.EventId=2 and cs.ChapterID!=1 and PaymentReference is not null" + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode, cs.contestyear  union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=3 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += " select 112 as chapterid,'Coaching,US' as chaptercode,year,SUM(amt) from( select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and cs.EventId=13  and PaymentReference is not null " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year  union all";
                                sqlStr += "  select 117 as chapterid,'Game,US' as chaptercode,year,SUM(amt) from(select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and cs.EventId=4 and PaymentReference is not null " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year  union all";
                                sqlStr += " select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=19 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                Qrycondition = sqlStr + genWhereConditons();
                                Qry = Qrycondition;
                            }
                        }
                        else
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "1"))
                            {
                                FunctionsForChapter("cs.EventId=1 and cs.chapterid=1");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                FunctionsForChapter("cs.EventId=2 and cs.chapterid!=1");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                            {
                                FunctionsForChapterOtherOption(" cs.ChapterID,ch.ChapterCode", "Count(*)as amt", "Registration", 
                                    "left join chapter ch on ch.ChapterID=CS.ChapterId");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                            {
                                if (DDReport.SelectedItem.Text == "Report by Parent")
                                {
                                    FunctionsForChapterOtherOption("cs.PMemberID ,(I.FirstName + ' ' + I.LastName) as ParentName", 
                                        "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt", "CoachReg", 
                                        "left join IndSpouse I on I.AutoMemberID=Cs.PMemberID   left join chapter ch on ch.ChapterID=CS.ChapterId");
                                }
                                else
                                {
                                    FunctionsForChapterOtherOption("cs.CMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as CoachName", 
                                        "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt", "CoachReg", 
                                        " left join IndSpouse I on I.AutoMemberID=Cs.CMemberID   left join chapter ch on ch.ChapterID=CS.ChapterId");
                                }
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                            {
                                FunctionsForChapterOtherOption("cs.ChapterID,ch.ChapterCode", "Count(*)as amt", "Registration_PrepClub", 
                                    " left join chapter ch on ch.ChapterID=CS.ChapterId");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                            {
                               
                                Qrycondition = "with tbl as (select   cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName,"+
                                " year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID"+
                                " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                                + endall + "' and '12/31/" + Start + "' and PaymentReference is not null ";
                            }
                            Qry = Qrycondition + genWhereConditons();
                        }
                    }
                    else if ((DDchoice.SelectedItem.Value.ToString() == "4") || ((DDchoice.SelectedItem.Value.ToString() == "3")))
                    {
                        if (ddevent.SelectedItem.Text == "All")
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "3")
                            {
                                Qrycontestid = "P.ProductGroupId";
                                TblNmae = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
                            }
                            else
                            {
                                TblNmae = "Product p on p.productid=cs.productid";
                                Qrycontestid = "P.productid";
                            }
                            Qryvaluewhere = genWhereConditons1();
                            string sqlStr = "with tbl as (select contest,ProductGroupCode,year,SUM(amt) as amt from(select     p.Name as Contest,P.ProductGroupCode,cs.contestyear as year ,Count(*)as amt from Contestant CS left join "
                                             +TblNmae+" left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear   between '" + endall + "' and '" + Start + "'  and  p.name is not null and PaymentReference is not null and cs.EventId=2 and cs.chapterid!=1" 
                                             + Qryvaluewhere + " group by  P.name,P.ProductGroupCode,cs.contestyear union all";
                            sqlStr += " select  p.Name  as Contest,P.ProductGroupCode,cs.contestyear as year ,Count(*)as amt from Contestant CS left join " + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear   between '"
                                       + endall + "' and '" + Start + "'  and  p.name is not null and PaymentReference is not null and cs.EventId=1 and cs.chapterid=1" + Qryvaluewhere + " group by  P.name,p.ProductGroupCode,cs.contestyear union all";
                            sqlStr += " select  p.Name  as Contest,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration CS left join " 
                                      + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                                       + endall + "' and '12/31/" + Start + "'  and  p.name is not null and PaymentReference is not null" + Qryvaluewhere + " group by  P.name,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                            sqlStr += " select     p.Name  as Contest,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from CoachReg CS left join " 
                                       + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "'  and  p.name is not null and PaymentReference is not null " 
                                       + Qryvaluewhere + " group by  P.name,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                            sqlStr += " select     p.Name  as Contest,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join " 
                                      + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                       + endall + "' and '12/31/" + Start + "'  and  p.name is not null and PaymentReference is not null  " + Qryvaluewhere + " group by  P.name,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                            sqlStr += " select     p.Name  as Contest,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join " 
                                + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                       + endall + "' and '12/31/" + Start + "'  and  p.name is not null and PaymentReference is not null " + Qryvaluewhere + " group by  P.name,P.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            Qrycondition = sqlStr + genWhereConditons();
                            Qry = Qrycondition;

                            }
                         else
                        {
                            switch (ddevent.SelectedItem.Value.ToString())
                            {
                                case "1":
                                    QryForCalendar("Contestant", "contestyear", endall, Start, "cs.EventId=1 and cs.chapterid=1");
                                    break;
                                case "2":
                                    QryForCalendar("Contestant", "contestyear", endall, Start, "p.EventId=2 and cs.chapterid!=1");
                                    break;
                                case "3":
                                    QryForCalendarOtherOption("Registration", endall, Start);
                                    break;
                                case "4":
                                    QryForCalendarOtherOption("Game", endall, Start);
                                    break;
                                case "13":
                                    QryForCalendarOtherOption("CoachReg", endall, Start);
                                    break;
                                case "19":
                                    QryForCalendarOtherOption("Registration_PrepClub", endall, Start);
                                    break;
                            }
                        Qrycondition = Qrycondition + genWhereConditons();
                        Qry = Qrycondition;
                            }
                    }
                    else
                    {
                        if (ddevent.SelectedItem.Text == "All")
                        {
                            Qryvaluewhere = genWhereConditons1();
                            if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                            {
                                string sqlqryforcoach = " select 117 as chapterid,'Game,US' as chaptercode,year,SUM(amt) from"+
                                    "(select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  "+
                                    "CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" + Start + "' and cs.EventId=4 and cs.ChapterID=117 and PaymentReference is not null " 
                                    + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year";
                                string sqlStr = "with tbl as (select EventId,EventCode,year,SUM(amt) as amt from(select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,"+
                                    "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" 
                                    + Start + "' and cs.EventId=1 and cs.ChapterID=1 and  PaymentReference is not null" + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,"+
                                    "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" + Start + "'  and cs.EventId=2 and cs.ChapterID!=1 and PaymentReference is not null" 
                                    + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration  CS"+
                                    "left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
                                    + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=3 " 
                                    + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,"+
                                    "Count(*)as amt from Registration_PrepClub  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                                    + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=19 " 
                                    + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                Qrycondition = sqlStr + genWhereConditons();
                            }
                            else
                            {
                                string sqlqryforcoach = " select 117 as chapterid,'Game,US' as chaptercode,year,SUM(amt) from(select   cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,"+
                                    " Count(*)as amt from Game  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '" 
                                    + endall + "' and '" + Start + "' and cs.EventId=4 and cs.ChapterID=117 and PaymentReference is not null " 
                                    + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year";
                                string sqlStr = "with tbl as (select EventId,EventCode,year,SUM(amt) as amt from(select   cs.EventId,cs.EventCode,cs.contestyear as year ,"+
                                    " Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where  cs.contestyear  between '"
                                    + endall + "' and '" + Start + "' and cs.EventId=1 and cs.ChapterID=1 and  PaymentReference is not null" 
                                    + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,cs.contestyear union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where cs.contestyear  between '"
                                    + endall + "' and '" + Start + "'  and cs.EventId=2 and cs.ChapterID!=1 and PaymentReference is not null" + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,cs.contestyear union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration  CS  left join chapter ch on ch.ChapterID=CS.ChapterId"+
                                    " where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and EventId=3 " 
                                    + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                                sqlStr += "  select   cs.EventId,E.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,"+
                                "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg  CS  left join Event E on E.EventId=cs.EventID  left join  chapter ch on ch.ChapterID=CS.ChapterId"+
                                " where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and cs.EventId=13  and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,E.EventCode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))   union all";
                                sqlStr += "  select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" 
                                    + Start + "' and cs.EventId=4 and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                                sqlStr += " select   cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub  CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" 
                                    + Start + "' and PaymentReference is not null and EventId=19 " + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                Qrycondition = sqlStr + genWhereConditons();
                            }
                            Qry = Qrycondition;
                        }
                        else
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                FunctionsForChapterEvent(" cs.EventId=2 and cs.chapterid!=1");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "1"))
                            {
                                FunctionsForChapterEvent("cs.EventId=1 and cs.chapterid=1");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                            {
                                FunctionsForChapterOtherOptionEvent("cs.EventId,cs.EventCode", "Count(*)as amt", "Registration", 
                                    "left join chapter ch on ch.ChapterID=CS.ChapterId", "cs.EventId=3");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                            {
                                FunctionsForChapterOtherOptionEvent("cs.EventId,cs.EventCode", "Count(*)as amt", 
                                    "Registration_PrepClub", "left join chapter ch on ch.ChapterID=CS.ChapterId", "cs.EventId=19");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                            {
                                FunctionsForChapterOtherOptionEvent("cs.EventId,cs.PMemberID ,(I.FirstName + ' ' + I.LastName) as ParentName", 
                                    "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt", "Registration_PrepClub", 
                                    "left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId", "cs.EventId=13");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                            {
                                FunctionsForChapterOtherOptionEvent("cs.EventId,cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName", "Count(*)  as amt", "Game",
                                    "left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId", "cs.EventId=4");
                            }
                            Qrycondition = Qrycondition + genWhereConditons();
                            Qry = Qrycondition;
                        }
                    }
                }
                else
                {
                    Qryvaluewhere = genWhereConditons1();
                    for (i = endall; i <= Start; i++)
                    {
                        fiscyear = i + 1;
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if (ddevent.SelectedItem.Text == "All")
                            {
                                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                                {
                                    CommonQryContest = "select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=1 and cs.Chapterid=1 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  ";
                                    CommonQryContestchapter1 = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=2 and cs.Chapterid!=1 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                                    CommonQryReg = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=3  " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryprepclup = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and eventID = 19 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                                else
                                {
                                    CommonQryContest = "select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=1 and cs.Chapterid=1 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  ";
                                    CommonQryContestchapter1 = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=2 and cs.Chapterid!=1 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                                    CommonQryReg = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=3  " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQrycoachreg = " select 112 as chapterid,'Coaching,US' as chaptercode,year,SUM(amt) from( select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  and cs.EventID=13  " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year ";
                                    CommonQryGame = "  select 117 as chapterid,'Game,US' as chaptercode,year,SUM(amt) from(select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Game CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  and cs.EventID=4  " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d group by year  ";
                                    CommonQryprepclup = "  select cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and eventID = 19 " + Qryvaluewhere + " group by  cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                            }
                            else
                            {
                                if ((ddevent.SelectedItem.Value.ToString() == "1"))
                                {
                                    FunctionsForChapterFical("cs.ChapterID,ch.ChapterCode", "cs.Eventid=1 and cs.chapterid=1", "cs.ChapterID,ch.ChapterCode");
                                }
                                else if ((ddevent.SelectedItem.Value.ToString() == "2"))
                                {
                                    FunctionsForChapterFical("cs.ChapterID,ch.ChapterCode", "cs.Eventid=2 and Cs.ChapterID!=1", "cs.ChapterID,ch.ChapterCode");
                                }
                                else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                                {
                                    FunctionsForChapterOtherOptionFiscal("cs.ChapterID,ch.ChapterCode","Count(*)as amt","Registration",
                                        "left join chapter ch on ch.ChapterID=CS.ChapterId","cs.Eventid=3","cs.ChapterID,ch.ChapterCode");
                                   
                                }
                                else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                                {
                                    if (DDReport.SelectedItem.Text == "Report by Parent")
                                    {
                                         FunctionsForChapterOtherOptionFiscal("cs.PMemberID ,(I.FirstName + ' ' + I.LastName) as ParentName",
                                             "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt",
                                             "CoachReg","left join IndSpouse I on I.AutoMemberID=Cs.PMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId",
                                             "cs.Eventid=13"," cs.PMemberID ,I.FirstName ,I.LastName,cs.productgroupcode");
                                    }
                                    else
                                    {
                                        FunctionsForChapterOtherOptionFiscal("cs.CMemberID ,(I.FirstName + ' ' + I.LastName) as CoachName", 
                                            "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt", "CoachReg", 
                                            "left join IndSpouse I on I.AutoMemberID=Cs.CMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId", 
                                            "cs.Eventid=13", " cs.CMemberID ,I.FirstName ,I.LastName,cs.productgroupcode");
                                    }
                                }
                                else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                                {
                                    FunctionsForChapterOtherOptionFiscal("cs.ChapterID,ch.ChapterCode",
                                         "Count(*)as amt", "Registration_PrepClub",
                                         "left join chapter ch on ch.ChapterID=CS.ChapterId",
                                         "cs.Eventid=19"," cs.ChapterID,ch.ChapterCode");
                                }
                                else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                                {
                                    FunctionsForChapterOtherOptionFiscal(" cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName",
                                      "Count(*)as amt", "Game",
                                      "left join IndSpouse I on I.AutoMemberID=Cs.CMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId",
                                      "cs.Eventid=4", "cs.MemberID ,I.FirstName ,I.LastName,cs.Amount");
                                }
                            }
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "4") || (DDchoice.SelectedItem.Value.ToString() == "3"))
                        {
                            if (ddevent.SelectedItem.Text == "All")
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "3")
                                {
                                    Qrycontestid = "P.ProductGroupId";
                                    TblNmae = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
                                }
                                else
                                {
                                    TblNmae = "Product p on p.productid=cs.productid";
                                    Qrycontestid = "P.productid";
                                }
                                CommonQryContest = "select  p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join "
                                                    + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and cs.chapterid=1 and p.name is not null and PaymentReference is not null "
                                                    + Qryvaluewhere + " group by  P.ProductGroupCode,p.Name ,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryContestchapter1 = "select p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join "
                                                    + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and Cs.ChapterID!=1 and  p.name is not null and PaymentReference is not null "
                                                    + Qryvaluewhere + " group by  P.ProductGroupCode,p.Name,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryprepclup = "select p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join "
                                                  + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and p.name is not null and PaymentReference is not null "
                                                   + Qryvaluewhere + " group by  P.ProductGroupCode,p.Name ,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryReg = "select p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join "
                                                  + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "'  and p.name is not null and PaymentReference is not null "
                                                  + Qryvaluewhere + " group by  P.ProductGroupCode,p.Name ,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQrycoachreg = "select  p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join "
                                                     + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=13  and PaymentReference is not null "
                                                     + Qryvaluewhere + " group by  P.ProductGroupCode,p.Name ,cs.productgroupcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryGame = "select  p.Name as Contest,P.ProductGroupCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year, Count(*)  as amt from game CS left join "
                                                + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=4  and p.name is not null and PaymentReference is not null "
                                                + Qryvaluewhere + " group by P.ProductGroupCode, p.Name ,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else
                            {
                                switch (ddevent.SelectedItem.Value.ToString())
                                {
                                    case "1":
                                        QryForCalendar("Contestant", "DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)) ", fiscyear, i, "cs.EventId=1 and cs.chapterid=1");
                                        break;
                                    case "2":
                                        QryForCalendar("Contestant", "DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)) ", fiscyear, i, "p.EventId=2 and cs.chapterid!=1");
                                        break;
                                    case "3":
                                        QryForCalendarOtherOption("Registration", fiscyear, i);
                                        break;
                                    case "4":
                                        QryForCalendarOtherOption("Game", fiscyear, i);
                                        break;
                                    case "13":
                                        QryForCalendarOtherOption("CoachReg", fiscyear, i);
                                        break;
                                    case "19":
                                        QryForCalendarOtherOption("Registration_PrepClub", fiscyear, i);
                                        break;
                                }
                            }
                        }
                        else
                        {
                            if (ddevent.SelectedItem.Text == "All")
                            {
                                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                                {
                                    isFiscEvent = true;
                                    CommonQryContest = "select  cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and cs.chapterid=1 and PaymentReference is not null " + Qryvaluewhere 
                                        + " group by  cs.EventId,cs.Eventcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryContestchapter1 = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and Cs.ChapterID!=1 and  PaymentReference is not null " + Qryvaluewhere 
                                        + " group by  cs.EventId,cs.Eventcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryprepclup = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryReg = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "'  and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                                else
                                {
                                    isFiscEvent = true;
                                    CommonQryContest = "select  cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and cs.chapterid=1 and PaymentReference is not null " + Qryvaluewhere
                                        + " group by  cs.EventId,cs.Eventcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryContestchapter1 = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and Cs.ChapterID!=1 and  PaymentReference is not null " 
                                        + Qryvaluewhere + " group by  cs.EventId,cs.Eventcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryprepclup = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null " + Qryvaluewhere 
                                        + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryReg = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "'  and  PaymentReference is not null " + Qryvaluewhere 
                                        + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQrycoachreg = "select  cs.EventId,E.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                    "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join Event E on E.EventId=cs.EventID left join IndSpouse I on I.AutoMemberID=Cs.PMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear 
                                    + "' and cs.Eventid=13  and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,E.EventCode,cs.productgroupcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                    CommonQryGame = "select  cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
                                        "Count(*)  as amt from game CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID    left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=4  and PaymentReference is not null " + Qryvaluewhere 
                                        + " group by  cs.EventId,cs.EventCode,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "1"))
                            {
                                FunctionsForChapterFical("cs.EventId,cs.EventCode", "cs.Eventid=1 and cs.chapterid=1", "cs.EventId,cs.EventCode");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                FunctionsForChapterFical("cs.EventId,cs.EventCode", "cs.Eventid=2 and Cs.ChapterID!=1", "cs.EventId,cs.EventCode");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                            {
                                CommonQry = "select cs.EventId,cs.EventCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,cs.EventCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                            {
                                FunctionsForChapterOtherOptionFiscal("cs.EventId,cs.EventCode", "Count(*)as amt", "Registration",
                                       "left join chapter ch on ch.ChapterID=CS.ChapterId", "cs.Eventid=3", "cs.EventId,cs.EventCode");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                            {
                                FunctionsForChapterOtherOptionFiscal("cs.PMemberID ,(I.FirstName + ' ' + I.LastName) as ParentName",
                                       "case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt", "CoachReg",
                                       "left join IndSpouse I on I.AutoMemberID=Cs.PMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId",
                                       "cs.Eventid=4", "cs.PMemberID ,I.FirstName ,I.LastName,cs.productgroupcode");
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                            {
                                FunctionsForChapterOtherOptionFiscal(" cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName",
                                         "Count(*)as amt", "Game",
                                         "left join IndSpouse I on I.AutoMemberID=Cs.CMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId",
                                         "cs.Eventid=4", "cs.MemberID ,I.FirstName ,I.LastName,cs.Amount");
                            }
                        }
                        FiscalyearCondition();
                    }

                    if (ddevent.SelectedItem.Text == "All")
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                            {
                                joinquery = "with tbl as (select chapterid,chaptercode,year,SUM(amt) as amt from(" + WCNTQry + " union all " + WCNTQryContest1 
                                    + "union all " + WCNTQryreg + "union all " + WCNTQryprepclub + genWhereConditons();
                            }
                            else
                            {
                                joinquery = "with tbl as (select chapterid,chaptercode,year,SUM(amt) as amt from(" + WCNTQry + " union all " 
                                    + WCNTQryContest1 + "union all  " + WCNTQryreg + " union all " + WCNTQrycoachreg + " union all " + WCNTQryGame + "union all " 
                                    + WCNTQryprepclub + genWhereConditons();
                            }
                        }
                        else
                        {
                            if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                            {
                                joinquery = "with tbl as (select EventID,EventCode,year,SUM(amt) as amt from(" + WCNTQry 
                                    + " union all " + WCNTQryContest1 + "union all " + WCNTQryreg + " union all " + WCNTQryprepclub + genWhereConditons();
                            }
                            else
                            {
                                if ((DDchoice.SelectedItem.Value.ToString() == "3") || ((DDchoice.SelectedItem.Value.ToString() == "4")))
                                {
                                    joinquery = "with tbl as (select Contest,ProductGroupCode,year,SUM(amt) as amt from(" + WCNTQry + " union all " 
                                        + WCNTQryContest1 + "union all " + WCNTQryreg + " union all " + WCNTQrycoachreg + " union all " 
                                        + WCNTQryGame + "union all  " + WCNTQryprepclub + genWhereConditons();
                                }
                                else
                                {
                                    joinquery = "with tbl as (select EventID,EventCode,year,SUM(amt) as amt from(" + WCNTQry + " union all " 
                                        + WCNTQryContest1 + "union all " + WCNTQryreg + " union all " + WCNTQrycoachreg + " union all " + WCNTQryGame 
                                        + "union all  " + WCNTQryprepclub + genWhereConditons();
                                }
                            }
                        }
                        Qrycondition = joinquery;
                    }
                    else
                    {
                        Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                    }
                    Qry = Qrycondition;
                    fiscyear = i + 1;
                }
                dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                DataTable dtmerge = new DataTable();
                dtmerge = dsnew.Tables[0];
                Mergecopy = dtmerge;

                if (Mergecopy.Rows.Count > 0)
                {
                    lbldisp.Visible = false;
                    Button2.Enabled = true;
                    dtnewgrid = Mergecopy;
                    if (ddevent.SelectedItem.Value.ToString() == "13")
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "2")
                        {
                            numval = 3;
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            numval = 1;
                        }
                        else
                        {
                            numval = 2;
                        }
                    }
                    else
                    {
                        if (ddevent.SelectedItem.Value.ToString() == "4")
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "2")
                            {
                                numval = 3;
                            }
                            else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                            {
                                numval = 1;
                            }
                            else
                            {
                                numval = 2;
                            }
                        }
                        else
                        {
                            if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                            {
                                numval = 1;
                            }
                            else
                            {
                                numval = 2;
                            }
                        }
                    }
                    DataRow dr = dtnewgrid.NewRow();
                    for (int i = numval; i <= dtnewgrid.Columns.Count - 1; i++)
                    {
                        dr[i] = 0;
                        double sum = 0;
                        string suma = "";
                        foreach (DataRow drnew in dtnewgrid.Rows)
                        {
                            if (!DBNull.Value.Equals(drnew[i]))
                            {
                                suma = drnew[i].ToString();
                                if (suma != "")
                                {
                                    sum += Convert.ToDouble(drnew[i]);
                                }
                            }
                        }
                        dr[i] = sum;
                    }
                    if (ddevent.SelectedItem.Value.ToString() == "13")
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "2")
                        {
                            dr[2] = "Total";
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            dr[0] = "Total";
                        }
                        else
                        {
                            dr[1] = "Total";
                        }
                    }
                    else
                    {
                        if (ddevent.SelectedItem.Value.ToString() == "4")
                        {
                            if (DDchoice.SelectedItem.Value.ToString() == "2")
                            {
                                dr[2] = "Total";
                            }
                            else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                            {
                                dr[0] = "Total";
                            }
                        }
                        else
                        {
                            if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                            {
                                dr[0] = "Total";
                            }
                            else
                            {
                                dr[1] = "Total";
                            }
                        }
                    }
                    dtnewgrid.Rows.Add(dr);
                    if ((ddevent.SelectedItem.Value.ToString() == "13") && (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        dr = dtnewgrid.NewRow();
                        DataRow[] drs = dtnewgrid.Select("Contest='SAT English'");

                        for (int i = numval; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            dr[i] = 0;
                            double sum = 0;
                            string suma = "";
                            for (int j = 0; j < dtnewgrid.Rows.Count - 1; j++)
                            {
                                DataRow drnew = dtnewgrid.Rows[j];
                                if (!DBNull.Value.Equals(drnew[i]))
                                {
                                    suma = drnew[i].ToString();
                                    if (suma != "")
                                    {
                                        sum += Convert.ToDouble(drnew[i]);
                                    }
                                }
                            }
                            if (!DBNull.Value.Equals(drs[0][i]))
                                dr[i] = sum - Convert.ToDouble(drs[0][i]);
                            else
                                dr[i] = sum;
                        }
                        if (DDchoice.SelectedItem.Value.ToString() == "2")
                        {
                            dr[2] = "Total less SAT English";
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            dr[0] = "Total less SAT English";
                        }
                        else
                        {
                            dr[1] = "Total less SAT English";
                        }
                        dtnewgrid.Rows.Add(dr);
                    }
                    for (int i = numval; i <= dtnewgrid.Columns.Count - 2; i++)
                    {
                        foreach (DataRow drnw in dtnewgrid.Rows)
                        {
                            if (!DBNull.Value.Equals(drnw[i]))
                            {
                                double st1;
                                st1 = Convert.ToDouble(drnw[i]);
                                drnw[i] = Convert.ToString(drnw[i]);
                                drnw[i] = String.Format("{0:#,###0}", st1);
                            }
                        }
                    }
                    dtnewgrid.Columns.Add("Total", typeof(String));
                    StringFormat();
                    dtnewgrid.Columns.Remove("total");
                    dtnewgrid.AcceptChanges();
                    Gridcontestant.Visible = true;
                    Grideventcontest.Visible = false;
                    Gridparent.Visible = false;
                    GridEventParentchapter.Visible = false;
                    Gridcoachparent.Visible = false;
                    GridEventparent.Visible = false;
                    Gridcoach.Visible = false;
                    if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if (DDReport.SelectedItem.Text == "Report by Parent")
                            {
                                Gridcoach.Visible = false;
                                Gridchild.Visible = false;
                                Gridcontestant.Visible = false;
                                GridEventparentdetail.Visible = false;
                                GridEvent.Visible = false;
                                GridViewcoachparentandchild.Visible = true;
                                GridViewcoachparentandchild.DataSource = dtnewgrid;
                                GridViewcoachparentandchild.DataBind();
                            }
                            else
                            {
                                Gridcontestant.Visible = false;
                                Gridchild.Visible = false;
                                GridViewcoachparentandchild.Visible = false;
                                GridEventparentdetail.Visible = false;
                                Gridcoach.Visible = true;
                                Gridcoach.DataSource = dtnewgrid;
                                Gridcoach.DataBind();
                            }
                        }
                       else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            Gridcontestdetail.Visible = false;
                            Gridnewcontest.Visible = true;
                            Gridnewcontest.DataSource = dtnewgrid;
                            Gridnewcontest.DataBind();
                        }
                        else
                        {
                            Gridcontestant.Visible = false;
                            Gridchild.Visible = false;
                            Gridcoach.Visible = false;
                            GridEventparentdetail.Visible = true;
                            GridEvent.Visible = false;
                            GridEventparentdetail.DataSource = dtnewgrid;
                            GridEventparentdetail.DataBind();
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "2")
                        {
                            Gridcontestant.Visible = false;
                            Gridchild.Visible = false;
                            Gridcoach.Visible = false;
                            GridEvent.Visible = false;
                            GridEventparentdetail.Visible = true;
                            GridEventparentdetail.DataSource = dtnewgrid;
                            GridEventparentdetail.DataBind();
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            Gridcontestdetail.Visible = false;
                            Gridnewcontest.Visible = true;
                            Gridnewcontest.DataSource = dtnewgrid;
                            Gridnewcontest.DataBind();
                        }
                        else
                        {
                            GridEvent.Visible = false;
                            Gridcoach.Visible = false;
                            Gridcontestant.Visible = false;
                            Gridchild.Visible = false;
                            GridViewcoachparentandchild.Visible = true;
                            GridViewcoachparentandchild.DataSource = dtnewgrid;
                            GridViewcoachparentandchild.DataBind();
                        }
                    }
                    else
                    {
                        GridViewcoachparentandchild.Visible = false;
                        Gridcoach.Visible = false;
                        Gridcontestant.Visible = true;
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            GridEventparentdetail.Visible = false;
                            Gridcontestant.DataSource = dtnewgrid;
                            Gridcontestant.DataBind();
                        }
                        else if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
                        {
                            Gridcontestdetail.Visible = false;
                            Gridnewcontest.Visible = true;
                            Gridnewcontest.DataSource = dtnewgrid;
                            Gridnewcontest.DataBind();
                        }
                        else
                        {
                            Gridcoach.Visible = false;
                            Gridcontestant.Visible = false;
                            Gridchild.Visible = false;
                            Gridcontestant.Visible = false;
                            GridViewcoachparentandchild.Visible = false;
                            GridEventparentdetail.Visible = false;
                            GridEvent.Visible = true;
                            GridEvent.DataSource = dtnewgrid;
                            GridEvent.DataBind();
                        }
                    }
                    Session["Gridcontestant"] = dtnewgrid;
                }
                else
                {
                    lbldisp.Visible = true;
                    Gridcontestant.Visible = false;
                    GridViewcoachparentandchild.Visible = false;
                    GridEventparentdetail.Visible = false;
                    GridEvent.Visible = false;
                    Gridcoach.Visible = false;
                    Gridnewcontest.Visible = false;
                }
            }
            else
            {
                Gridcontestant.Visible = false;
                lblall.Visible = true;
                lbldisp.Visible = false;
                Gridcontestant.Visible = false;
                GridViewcoachparentandchild.Visible = false;
                GridEventparentdetail.Visible = false;
                Gridcoach.Visible = false;
                GridEvent.Visible = false;
            }
        }
        catch (Exception err)
        {
        }
    }

    private void FunctionsForChapterFical(string Elements,string EventVAL,string Grouping)
    {
        CommonQry = "select "+Elements+",CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"+
            "Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" 
            + i + "' and '04/30/" + fiscyear + "' and " + EventVAL + " and PaymentReference is not null " + Qryvaluewhere + " group by  "+Grouping+",year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
    }
    private void FunctionsForChapterOtherOptionFiscal(string Elements, string Count, string TblName, string JoinCond, string EventVAL, string Grouping)
    {
           CommonQry = "select "+Elements+",CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,"
               + Count + " from " + TblName + " CS  " + JoinCond + " where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and " + EventVAL + "  and  PaymentReference is not null  " + Qryvaluewhere + " group by  "
               + Grouping + ",year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
    }
    private void FunctionsForChapterEvent(string EventVAL)
    {
        Qrycondition = "with tbl as (select    cs.EventId,cs.EventCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear   between '" 
            + endall + "' and '" + Start + "'  and PaymentReference is not null and " + EventVAL + "";
    }
    private void FunctionsForChapterOtherOptionEvent(string Elements, string Count, string TblName, string JoinCond, string EventVAL)
    {
        Qrycondition = "with tbl as (select     " + Elements + ",year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ," 
            + Count + " from " + TblName + "   CS  " + JoinCond + " where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" 
            + Start + "'  and PaymentReference is not null and " + EventVAL + "";
    }
    private void FunctionsForChapter(string EventVAL)
    {
        Qrycondition = "with tbl as (select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" 
            + endall + "' and '" + Start + "' and " + EventVAL + " and PaymentReference is not null ";
    }
    private void FunctionsForChapterOtherOption(string Elements,string Count,string TblName,string JoinCond)
    {
        Qrycondition = "with tbl as (select   " + Elements + ",year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ," 
            + Count + " from " + TblName + "  CS  "+JoinCond+" where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" 
            + endall + "' and '12/31/" + Start + "' and PaymentReference is not null ";
    }
   
    private void QryForCalendar(string strtable, string Yearcondition, int Yearend, int Yearstart, string Event)
   {
       if (DDchoice.SelectedItem.Value.ToString() == "3")
       {
           Qrycontestid = "P.ProductGroupId";
           TblNmae = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
       }
       else
       {
              TblNmae="Product p on p.productid=cs.productid";
           Qrycontestid ="P.productid";
       }
       if (DDyear.SelectedItem.Text != "Fiscal Year")
       {
           if (chaptercondition == false)
           {
               Qrycondition = "with tbl as (select     p.Name as Contest,"
                   + Qrycontestid + ",cs.contestyear as year ,Count(*)as amt from "
                   + strtable + " CS left join "
                   + TblNmae
                   + " left join chapter ch on ch.ChapterID=CS.ChapterId where   "
                   + Yearcondition + "   between '" + Yearend + "' and '"
                   + Yearstart + "'  and  p.name is not null and PaymentReference is not null and " + Event + "";
           }
           else
           {
               Qrycondition = "with tbl as (select     cs.EventID, cs.ProductGroupCode,p.Name,cs.ProductCode ,cs.contestyear as year ,Count(*)as amt from "
                  + strtable + " CS left join "
                  + TblNmae
                  + " left join chapter ch on ch.ChapterID=CS.ChapterId where   "
                  + Yearcondition + "   between '" + Yearend + "' and '"
                  + Yearstart + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null and " + Event + "";
           }
       }
       else
       {
           if (chaptercondition == false)
           {
               CommonQry = "select   p.Name as Contest," + Qrycontestid + ",CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + Yearend + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from "
                   + strtable + " CS  left join "
               + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  " + Yearcondition + "    between '05/01/" + Yearstart + "' and '04/30/"
               + fiscyear + "' and " + Event + " and p.name is not null and PaymentReference is not null " 
               + Qryvaluewhere + " group by   " + Qrycontestid + ",p.Name,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
           }
           else
           {
               CommonQry = "select  cs.EventID, cs.ProductGroupCode,p.Name,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + Yearend + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from "
                  + strtable + " CS  left join "
              + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  " 
              + Yearcondition + "    between '05/01/" + Yearstart + "' and '04/30/"
              + fiscyear + "' and " + Event + " and p.Name='" + Textdummy.Text + "' and PaymentReference is not null " 
              + Qryvaluewhere + " group by  cs.EventId,cs.ProductCode,cs.ProductGroupCode, " + Qrycontestid + ",p.Name,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
           }
       }
   }
    private void QryForCalendarOtherOption(string strtable, int Yearend, int Yearstart)
    {
        string QueryProdGroup = string.Empty;
        string TblName = string.Empty;
        if (DDchoice.SelectedItem.Value.ToString() == "3")
        {
            QueryProdGroup = "P.ProductGroupId";
            TblName = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
        }
        else
        {
            TblName = "Product p on p.productid=cs.productid";

            QueryProdGroup = "P.productid";
        }
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (chaptercondition == false)
            {
                Qrycondition = "with tbl as (select     p.Name  as Contest,"
                    + QueryProdGroup + ",year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from "
                    + strtable + " CS left join "
                    + TblName + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                    + Yearend + "' and '12/31/"
                    + Yearstart + "'  and  p.name is not null and PaymentReference is not null ";
            }
            else
            {
                Qrycondition = "with tbl as (select     cs.EventID, cs.ProductGroupCode,p.Name,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from "
                 + strtable + " CS left join "
                 + TblName + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                 + Yearend + "' and '12/31/"
                 + Yearstart + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null ";
            }
        }
        else
        {
            if (chaptercondition == false)
            {
                CommonQry = "select p.Name as Contest," + QueryProdGroup + ",CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + Yearend + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from " + strtable + " CS  left join "
                    + TblName + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" 
                    + Yearstart + "' and '04/30/" + Yearend + "' and p.name is not null and PaymentReference is not null " + Qryvaluewhere + " group by  " 
                    + QueryProdGroup + ",p.Name,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
            }
            else
            {
                CommonQry = "select cs.EventID, cs.ProductGroupCode,p.Name,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + Yearend + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from " + strtable + " CS  left join "
                  + TblName + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" 
                  + Yearstart + "' and '04/30/" + Yearend + "' and p.Name='" + Textdummy.Text + "' and PaymentReference is not null " 
                  + Qryvaluewhere + " group by   cs.EventId,cs.ProductCode,cs.ProductGroupCode," + QueryProdGroup + ",p.Name,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
            }
        }

    }
    protected string genWhereConditons1()
    {
        string iCondtions = string.Empty;

        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
                else
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
            }
            else
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddZone.SelectedItem.Text != "All")
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
                else
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
            }
            else
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddCluster.SelectedItem.Text != "All")
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    iCondtions += " and CS.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                }
                else
                {
                    if ((ddchapter.SelectedItem.Text != "Coaching,US") && ((ddchapter.SelectedItem.Text != "Game,US")))
                    {
                        iCondtions += " and CS.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                    }
                }
            }
            else
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddchapter.SelectedItem.Text != "All")
                    {
                        iCondtions += " and CS.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                    }
                }

            }
        }
        return iCondtions;
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            if ((ddevent.SelectedItem.Value.ToString() == "13"))
            {
                if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                {
                    HiddenField k = (HiddenField)Gridnewcontest.Rows[index].Cells[0].FindControl("Hdchapterid");
                    strnewcontest = k.Value;
                    Textdummy.Text = strnewcontest;
                }
                else
                {
                    HiddenField k = (HiddenField)Gridcoach.Rows[index].Cells[0].FindControl("Hdcoach");
                    ExpchapterID = Convert.ToInt32(k.Value);
                }
            }
            else
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    HiddenField k = (HiddenField)Gridcontestant.Rows[index].Cells[0].FindControl("Hdchapterid");
                    ExpchapterID = Convert.ToInt32(k.Value);
                }
                else
                {
                    if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        HiddenField k = (HiddenField)Gridnewcontest.Rows[index].Cells[0].FindControl("Hdchapterid");
                        strnewcontest = k.Value;
                        Textdummy.Text = strnewcontest;
                    }
                    else
                    {
                        if (GridEvent.Visible == true)
                        {
                            HiddenField k = (HiddenField)GridEvent.Rows[index].Cells[0].FindControl("Hdchapterid");
                            ExpchapterID = Convert.ToInt32(k.Value);
                        }
                        else
                        {
                            HiddenField kchapter = (HiddenField)Grideventcontest.Rows[index].Cells[0].FindControl("Hdchapterid");
                            HiddenField k = (HiddenField)Grideventcontest.Rows[index].Cells[0].FindControl("HidEventId");
                            ExpchapterID = Convert.ToInt32(kchapter.Value);
                            ExpparentID = Convert.ToInt32(k.Value);
                        }
                    }
                }
            }
            Session["chapterid"] = ExpchapterID;
            Session["Eventid"] = ExpparentID;
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                Parentdetails();
            }
            else
            {
                chapterdetails();
            }
        }
        catch (Exception err)
        {
        }
    }
    protected void chapterdetails()
    {
        chaptercondition = true;
        lbprevious.Visible = true;
        lblall.Visible = false;
        EnableDropdown(false);
        seval = Convert.ToInt32(Session["chapterid"]);
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        Qryvaluewhere = genWhereConditons1();
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (DDchoice.SelectedItem.Value.ToString() == "2")
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    Qryvaluewhere = genWhereConditons1();
                    if (seval == 2)
                    {
                        if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year , Count(*) as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" 
                                + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=2 " + Qryvaluewhere + "  group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.contestyear";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year , Count(*) as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" 
                                + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=2   group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.contestyear";
                        }
                    }
                    else if (seval == 1)
                    {
                        if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*) as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=1  " + Qryvaluewhere + "  group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.ProductGroupCode,cs.contestyear";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*) as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=1   group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.ProductGroupCode,cs.contestyear";
                        }
                    }
                    else if (seval == 13)
                    {
                        chaptercondition = false;
                        parentcondition = true;
                        isgame = false;
                        Qrycondition = "with tbl as ( select   cs.Eventid,cs.PMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=13   group by  cs.Eventid,cs.PMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                    }
                    else if (seval == 3)
                    {
                        if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year , Count(*)  as amt from registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=3 " + Qryvaluewhere + "  group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year , Count(*)  as amt from registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=3   group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                    }
                    else if (seval == 4)
                    {
                        chaptercondition = false;
                        parentcondition = true;
                        isparentevent = false;
                        isgame = true;
                        Qrycondition = "with tbl as (select   cs.EventID,cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game  CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and Eventid=4 group by  cs.Eventid,cs.MemberID,I.FirstName,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) ";
                    }
                    else if (seval == 19)
                    {
                        Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year , Count(*)  as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=19   group by  cs.ChapterID,cs.EventID,ch.chaptercode,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                }
                else
                {
                    if (seval == 2)
                    {
                        Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year , Count(*)  as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null ";
                    }
                    if (seval == 1)
                    {
                        Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,cs.contestyear as year , Count(*)  as amt from contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=1 ";
                    }
                    if (seval == 3)
                    {
                        Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year , Count(*)  as amt from registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=3 ";
                    }
                    if (seval == 19)
                    {
                        Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year , Count(*)  as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=19 ";
                    }
                }
                Qry = Qrycondition + genWhereConditons();
            }
            else if ((DDchoice.SelectedItem.Value.ToString() == "4") || ((DDchoice.SelectedItem.Value.ToString() == "3")))
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "3")
                    {
                        Qrycontestid = "P.ProductGroupId";
                        TblNmae = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
                    }
                    else
                    {
                        TblNmae = "Product p on p.productid=cs.productid";
                        Qrycontestid = "P.productid";
                    }

                    Qryvaluewhere = genWhereConditons1();
                    string sqlStr = "with tbl as (select EventID,ProductGroupCode,contest,ProductCode,year,SUM(amt) as amt from(select    cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,cs.contestyear as year ,Count(*)as amt from Contestant CS left join "
                                     + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear   between '" + endall + "' and '" + Start + "'  and p.Name='" + Textdummy.Text + "' and PaymentReference is not null and cs.EventId=2 and cs.chapterid!=1"
                                     + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,cs.contestyear union all";
                    sqlStr += " select  cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,cs.contestyear as year ,Count(*)as amt from Contestant CS left join " + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear   between '"
                               + endall + "' and '" + Start + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null and cs.EventId=1 and cs.chapterid=1" + Qryvaluewhere + " group by cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,cs.contestyear union all";
                    sqlStr += " select  cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration CS left join "
                              + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                               + endall + "' and '12/31/" + Start + "'  and p.Name='" + Textdummy.Text + "' and PaymentReference is not null" + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                    sqlStr += " select     cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from CoachReg CS left join "
                               + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                               + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                    sqlStr += " select     cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join "
                              + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                               + endall + "' and '12/31/" + Start + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null  " + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) union all";
                    sqlStr += " select     cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join " + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/"
                               + endall + "' and '12/31/" + Start + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    Qrycondition = sqlStr + genWhereConditons();
                    Qry = Qrycondition;

                }

                else
                {
                    switch (ddevent.SelectedItem.Value.ToString())
                    {
                        case "1":
                            QryForCalendar("Contestant", "contestyear", endall, Start, "cs.EventId=1 and cs.chapterid=1");
                            break;
                        case "2":
                            QryForCalendar("Contestant", "contestyear", endall, Start, "p.EventId=2 and cs.chapterid!=1");
                            break;
                        case "3":
                            QryForCalendarOtherOption("Registration", endall, Start);
                            break;
                        case "4":
                            QryForCalendarOtherOption("Game", endall, Start);
                            break;
                        case "13":
                            QryForCalendarOtherOption("CoachReg", endall, Start);
                            break;
                        case "19":
                            QryForCalendarOtherOption("Registration_PrepClub", endall, Start);
                            break;
                    }
                    Qrycondition = Qrycondition + genWhereConditons();
                    Qry = Qrycondition;
                }
            }
        }
            else
            {
                for (i = endall; i <= Start; i++)
                {
                    fiscyear = i + 1;
                    Qryvaluewhere = genWhereConditons1();
                     if ((DDchoice.SelectedItem.Value.ToString() == "4") || (DDchoice.SelectedItem.Value.ToString() == "3"))
                        {
                            if (ddevent.SelectedItem.Text == "All")
                            {
                                if (DDchoice.SelectedItem.Value.ToString() == "3")
                                {
                                    Qrycontestid = "P.ProductGroupId";
                                    TblNmae = "ProductGroup p on p.ProductGroupId=cs.ProductGroupId";
                                }
                                else
                                {
                                    TblNmae = "Product p on p.productid=cs.productid";
                                    Qrycontestid = "P.productid";
                                }
                                CommonQryContest = "select  cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join "
                                                    + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and cs.chapterid=1 and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                                                    + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryContestchapter1 = "select cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join "
                                                    + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and Cs.ChapterID!=1 and   p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                                                    + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryprepclup = "select cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join "
                                                  + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                                                   + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryReg = "select cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join "
                                                  + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "'  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                                                  + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQrycoachreg = "select  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join "
                                                     + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=13 and  p.Name='" + Textdummy.Text + "'  and PaymentReference is not null "
                                                     + Qryvaluewhere + " group by  cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryGame = "select  cs.EventID, cs.ProductGroupCode,p.Name as Contest,cs.ProductCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year, Count(*)  as amt from game CS left join "
                                                + TblNmae + " left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=4  and  p.Name='" + Textdummy.Text + "' and PaymentReference is not null "
                                                + Qryvaluewhere + " group by cs.EventID, cs.ProductGroupCode,p.Name ,cs.ProductCode ,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else
                            {
                                switch (ddevent.SelectedItem.Value.ToString())
                                {
                                    case "1":
                                        QryForCalendar("Contestant", "DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)) ", fiscyear, i, "cs.EventId=1 and cs.chapterid=1");
                                        break;
                                    case "2":
                                        QryForCalendar("Contestant", "DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)) ", fiscyear, i, "p.EventId=2 and cs.chapterid!=1");
                                        break;
                                    case "3":
                                        QryForCalendarOtherOption("Registration", fiscyear, i);
                                        break;
                                    case "4":
                                        QryForCalendarOtherOption("Game", fiscyear, i);
                                        break;
                                    case "13":
                                        QryForCalendarOtherOption("CoachReg", fiscyear, i);
                                        break;
                                    case "19":
                                        QryForCalendarOtherOption("Registration_PrepClub", fiscyear, i);
                                        break;
                                }
                            }
                        }
                    else
                    {
                        if (ddevent.SelectedItem.Text == "All")
                        {
                            isfisc = true;
                            if (seval == 2)
                            {
                                CommonQry = "select  cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and cs.chapterid!=1 and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 1)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and Cs.ChapterID=1 and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 19)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=19  and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 3)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=3  and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 13)
                            {
                                CommonQry = "select  cs.EventID,cs.PMemberID ,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID    left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=13  and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.PMemberID ,I.FirstName ,I.LastName,cs.productgroupcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 4)
                            {
                                CommonQry = "select  cs.EventID,cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Game CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID    left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=4  and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventId,cs.MemberID ,I.FirstName ,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                        }
                        else
                        {
                            if (seval == 2)
                            {
                                CommonQry = "select  cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=2 and cs.chapterid!=1 and PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 1)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=1 and Cs.ChapterID=1 and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 19)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=19  and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            if (seval == 3)
                            {
                                CommonQry = "select cs.EventID,cs.ChapterID,ch.ChapterCode,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and cs.Eventid=3  and  PaymentReference is not null " + Qryvaluewhere + " group by  cs.EventID,cs.ChapterID,ch.ChapterCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                        }
                    }

                     if (DDchoice.SelectedItem.Value.ToString() != "2")
                     {
                         FiscalyearCondition();
                     }

                     else
                     {
                         if (WCNTQry != string.Empty)
                         {
                             WCNTQrynew = CommonQry;
                             WCNTQry = WCNTQry + " union all " + WCNTQrynew;
                         }
                         else
                         {
                             WCNTQry = CommonQry;
                         }
                     }
                    
                }
                if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        joinquery = "with tbl as (select EventID,ProductGroupCode,contest,ProductCode,year,SUM(amt) as amt from(" + WCNTQry + " union all " + WCNTQryContest1 + "union all " + WCNTQryreg + " union all " + WCNTQrycoachreg + " union all " + WCNTQryGame + "union all  " + WCNTQryprepclub + genWhereConditons();
                        Qrycondition = joinquery;
                        Qry = Qrycondition;
                    }
                    else
                    {
                        Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                        Qry = Qrycondition;
                    }
                }
                else
                {
                    Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                    Qry = Qrycondition;
                }
            }
            dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtmerge = new DataTable();
            dtmerge = dsnew.Tables[0];
            Mergecopy = dtmerge;
            if (Mergecopy.Rows.Count > 0)
            {
                lbldisp.Visible = false;
                Button2.Enabled = true;
                dtnewgrid = Mergecopy;
                DataRow dr = dtnewgrid.NewRow();
                if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                {
                    newval = 4;
                }
                else
                {
                    newval = 3;
                }
                for (int i = newval; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    dr[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {
                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }

                        }
                    }
                    dr[i] = sum;
                }
                dr[2] = "Total";
                dtnewgrid.Rows.Add(dr);
                for (int i = newval; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    foreach (DataRow drnw in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnw[i]))
                        {
                            double st1;
                            st1 = Convert.ToDouble(drnw[i]);
                            drnw[i] = String.Format("{0:#,###0}", st1);
                        }
                    }
                }
                dtnewgrid.Columns.Add("Total", typeof(String));
                StringFormat();
                dtnewgrid.Columns.Remove("total");
                dtnewgrid.AcceptChanges();
                Session["Gridchapterevent"] = dtnewgrid;
                Gridcontestant.Visible = true;
                GridEvent.Visible = false;
                if ((seval == 13) || (seval == 4))
                {
                    Gridcontestant.Visible = false;
                    GridEventparent.Visible = true;
                    GridEventparent.DataSource = dtnewgrid;
                    GridEventparent.DataBind();
                }
                else
                {
                    if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
                    {
                        Gridnewcontest.Visible = false;
                        Gridcontestdetail.Visible = true;
                        Gridcontestdetail.DataSource = dtnewgrid;
                        Gridcontestdetail.DataBind();
                    }
                    else
                    {
                        Grideventcontest.Visible = true;
                        Gridcontestant.Visible = false;
                        Grideventcontest.DataSource = dtnewgrid;
                        Grideventcontest.DataBind();
                    }
                }
            }
        }
    protected void FiscalyearCondition()
    {

        if (ddevent.SelectedItem.Text == "All")
        {
            if (WCNTQry != string.Empty)
            {
                WCNTQrynew = CommonQryContest;
                WCNTQry = WCNTQry + " union all " + WCNTQrynew;
            }
            else
            {
                WCNTQry = CommonQryContest;
            }
            if (WCNTQryContest1 != string.Empty)
            {
                WCNTQrynewContest1 = CommonQryContestchapter1;
                WCNTQryContest1 = WCNTQryContest1 + " union all " + WCNTQrynewContest1;
            }
            else
            {
                WCNTQryContest1 = CommonQryContestchapter1;
            }
            if (WCNTQryGame != string.Empty)
            {
                WCNTQrynewGame = CommonQryGame;
                WCNTQryGame = WCNTQryGame + " union all " + WCNTQrynewGame;
            }
            else
            {
                WCNTQryGame = CommonQryGame;
            }
            if (WCNTQryreg != string.Empty)
            {
                WCNTQrynew1 = CommonQryReg;
                WCNTQryreg = WCNTQryreg + " union all " + WCNTQrynew1;
            }
            else
            {
                WCNTQryreg = CommonQryReg;
            }
            if (WCNTQrycoachreg != string.Empty)
            {
                WCNTQrynew2 = CommonQrycoachreg;
                WCNTQrycoachreg = WCNTQrycoachreg + " union all " + WCNTQrynew2;
            }
            else
            {
                WCNTQrycoachreg = CommonQrycoachreg;
            }
            if (WCNTQryprepclub != string.Empty)
            {
                WCNTQrynew3 = CommonQryprepclup;
                WCNTQryprepclub = WCNTQryprepclub + " union all " + WCNTQrynew3;
            }
            else
            {
                WCNTQryprepclub = CommonQryprepclup;
            }
        }

        else
        {
            if (WCNTQry != string.Empty)
            {
                WCNTQrynew = CommonQry;
                WCNTQry = WCNTQry + " union all " + WCNTQrynew;
            }
            else
            {
                WCNTQry = CommonQry;
            }
        }
    }
    protected void lnkViewparent_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if ((ddevent.SelectedItem.Value.ToString() == "13"))
                {
                    if (DDReport.SelectedItem.Text == "Report by Parent")
                    {
                        HiddenField k = (HiddenField)GridViewcoachparentandchild.Rows[index].Cells[0].FindControl("Hdparentchildid");
                        ExpchapterID = Convert.ToInt32(k.Value);
                    }
                    else
                    {
                        HiddenField kparent = (HiddenField)Gridcoachparent.Rows[index].Cells[0].FindControl("Hdcoachparent");
                        HiddenField k = (HiddenField)Gridcoachparent.Rows[index].Cells[0].FindControl("Hdparentid");
                        ExpchapterID = Convert.ToInt32(k.Value);
                        ExpparentID = Convert.ToInt32(kparent.Value);
                    }
                }
                else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                {
                    HiddenField k = (HiddenField)GridViewcoachparentandchild.Rows[index].Cells[0].FindControl("Hdparentchildid");
                    ExpchapterID = Convert.ToInt32(k.Value);
                }
                else
                {
                    HiddenField kparent = (HiddenField)Gridparent.Rows[index].Cells[0].FindControl("Hdparentid");
                    HiddenField k = (HiddenField)Gridparent.Rows[index].Cells[0].FindControl("Hdnchapterid");
                    ExpchapterID = Convert.ToInt32(k.Value);
                    ExpparentID = Convert.ToInt32(kparent.Value);
                }
            }
            else
            {
                if ((ddevent.SelectedItem.Value.ToString() == "13") || (ddevent.SelectedItem.Value.ToString() == "4"))
                {
                    HiddenField kparent = (HiddenField)GridEventparentdetail.Rows[index].Cells[0].FindControl("Hiddenparentidfield");
                    ExpparentID = Convert.ToInt32(kparent.Value);
                }
                else
                {
                    HiddenField kparent = (HiddenField)GridEventparent.Rows[index].Cells[0].FindControl("Hdparentchildid");
                    HiddenField k = (HiddenField)GridEventparent.Rows[index].Cells[0].FindControl("Hiddneventid");
                    ExpchapterID = Convert.ToInt32(k.Value);
                    ExpparentID = Convert.ToInt32(kparent.Value);
                }
            }
            Session["parentid"] = ExpparentID;
            Session["chapterid"] = ExpchapterID;
            Childdetails();
        }
        catch (Exception err)
        {
        }
    }
    protected void lnkViewchildchapter_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField kparent = (HiddenField)GridEventParentchapter.Rows[index].Cells[0].FindControl("Hdparentid");
            HiddenField k = (HiddenField)GridEventParentchapter.Rows[index].Cells[0].FindControl("Hdnchapterid");
            HiddenField kevent = (HiddenField)GridEventParentchapter.Rows[index].Cells[0].FindControl("Hiddeneventid");
            ExpchapterID = Convert.ToInt32(k.Value);
            ExpparentID = Convert.ToInt32(kparent.Value);
            ExpeventID = Convert.ToInt32(kevent.Value);
            Session["parentid"] = ExpparentID;
            Session["chapterid"] = ExpchapterID;
            Session["EventID"] = ExpeventID;
            Childdetails();
        }
        catch (Exception err)
        {
        }
    }
    protected void Parentdetails()
    {
        parentcondition = true;
        lblall.Visible = false;
        EnableDropdown(false);
        string sqlStr;
        int seval = Convert.ToInt32(Session["chapterid"]);
        Eventval = Convert.ToInt32(Session["Eventid"]);
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
      
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    Qryvaluewhere = genWhereConditons1();

                    if (seval == 112)
                    {
                        sqlStr = "with tbl as (select chapterid,ParentID,Parentname,year,SUM(amt) as amt from(select 112 as chapterID,ParentID,ParentName,year,amt from( select   cs.ChapterID,cs.PMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=13   group by  cs.ChapterID,cs.PMemberID,I.FirstName,I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ) as d";
                    }
                    else if (seval == 117)
                    {
                        sqlStr = "with tbl as (select chapterid,ParentID,Parentname,year,SUM(amt) as amt from(select 117 as chapterID,ParentID,ParentName,year,amt from(select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=4    group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))) as d  ";
                    }
                    else
                    {
                        if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                        {
                            sqlStr = "with tbl as (select chapterid,ParentID,Parentname,year,SUM(amt) as amt from(select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID=1 and cs.eventID = 1 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID!=1 and cs.eventID = 2 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=3 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.eventID = 19 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                        }
                        else
                        {
                            sqlStr = "with tbl as (select chapterid,ParentID,Parentname,year,SUM(amt) as amt from(select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.ChapterID=1 and cs.eventID = 1 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,cs.contestyear  union all";
                            sqlStr += " select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.ChapterID!=1 and cs.eventID = 2 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName,I.LastName,cs.contestyear  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=3 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=13 and  cs.ChapterID=117 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))   union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.eventID = 19 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName,I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                        }
                    }

                    Qrycondition = sqlStr + genWhereConditons();
                    Qry = Qrycondition;
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "1"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where  cs.contestyear   between '" + endall + "' and '" + Start + "' and Cs.ChapterID!=1  and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            Qrycondition = "with tbl as (select   cs.ChapterID,cs.CMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.PMemberID=" + Session["chapterid"] + " ";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select   cs.ChapterID,cs.CMemberID,cs.PMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.CMemberID=" + Session["chapterid"] + " ";

                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null   ";
                    }
                    Qry = Qrycondition + genWhereConditons();
                }
            }
            else
            {
                isparentevent = true;
                if (Eventval == 2)
                {
                    Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and Cs.ChapterID!=1 and cs.EventID=2  and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                }
                else if (Eventval == 1)
                {
                    Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,cs.contestyear as year ,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and Cs.ChapterID=1 and cs.EventID=1  and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                }
                else if (Eventval == 3)
                {
                    isworkshop = true;
                    isparentevent = false;
                    Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,cs.MemberID  ,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration  CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and  cs.EventID=3  and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                }
                else if (Eventval == 19)
                {
                    isworkshop = true;
                    isparentevent = false;
                    Qrycondition = "with tbl as (select   cs.EventID,cs.ChapterID,cs.MemberID ,(I.FirstName + ' ' + I.LastName) as ParentName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub  CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "'  and cs.EventID=19  and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " ";
                }

                Qry = Qrycondition + genWhereConditons();
            }
        }
        else
        {

            Qryvaluewhere = genWhereConditons1();


            for (i = endall; i <= Start; i++)
            {
                fiscyear = i + 1;
                if (ddevent.SelectedItem.Text == "All")
                {

                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        if (seval == 112)
                        {
                            CommonQryGame = "select 112 as chapterid,parentid,ParentName,year,amt from(select cs.ChapterID,cs.PMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  group by  cs.ChapterID,cs.PMemberID,I.FirstName, I.LastName,cs.productGroupcode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ) as d";
                        }
                        else if (seval == 117)
                        {
                            CommonQryGame = "select 117 as chapterid,parentid,ParentName,year,amt from(select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Game CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null   group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,cs.Amount,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ) as d";
                        }
                        else
                        {
                            if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                            {
                                CommonQryContest = "select cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=1 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryContestchapter1 = "select cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=2 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryReg = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryprepclup = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else
                            {
                                CommonQryContest = "select cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=1 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryContestchapter1 = "select cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventID=2 and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryReg = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                CommonQryprepclup = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                        }
                    }
                    else
                    {
                        if (Eventval == 1)
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and EventID=" + Eventval + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if (Eventval == 2)
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and EventID=" + Eventval + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if (Eventval == 3)
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if (Eventval == 19)
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }

                    }
                }
                else
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                        {
                            CommonQry = "select cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                        {
                            CommonQry = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                        {
                            if (DDReport.SelectedItem.Text == "Report by Parent")
                            {
                                CommonQry = "select cs.ChapterID,cs.PMemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  group by  cs.ChapterID,cs.PMemberID,I.FirstName, I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else
                            {
                                CommonQry = "select cs.ChapterID,cs.CMemberID,cs.PMemberID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join IndSpouse I on I.AutoMemberID=Cs.CMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  and cs.CMemberID=" + Session["chapterid"] + " group by  cs.ChapterID,cs.PMemberID,cs.CMemberID,I.FirstName, I.LastName,cs.ProductGroupCode,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                        }
                        else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                        {
                            CommonQry = "select cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  group by  cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                    }
                    else
                    {
                        if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and EventID=" + Eventval + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventID,cs.ChapterID,cs.ParentID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                        {
                            CommonQry = "select cs.EventID,cs.ChapterID,cs.MemberID as ParentID,(I.FirstName + ' ' + I.LastName) as ParentName,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null  group by  cs.EventID,cs.ChapterID,cs.MemberID,I.FirstName, I.LastName,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                    }
                }

                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    FiscalyearCondition();
                }
                else
                {
                    if (WCNTQry != string.Empty)
                    {
                        WCNTQrynew = CommonQry;
                        WCNTQry = WCNTQry + " union all " + WCNTQrynew;
                    }
                    else
                    {
                        WCNTQry = CommonQry;
                    }
                }
            }
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (seval == 112)
                    {
                        joinquery = "with tbl as (select chapterid,ParentID,ParentName,year,SUM(amt) as amt from(" + WCNTQryGame + genWhereConditons();

                    }
                    else if (seval == 117)
                    {
                        joinquery = "with tbl as (select chapterid,ParentID,ParentName,year,SUM(amt) as amt from(" + WCNTQryGame + genWhereConditons();
                    }
                    else
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            joinquery = "with tbl as (select chapterid,ParentID,ParentName,year,SUM(amt) as amt from(" + WCNTQry + " union all " + WCNTQryContest1 + "union all " + WCNTQryreg + "union all " + WCNTQryprepclub + genWhereConditons();
                        }
                        else
                        {
                            Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                            joinquery = Qrycondition;
                        }
                    }
                    Qrycondition = joinquery;
                }
                else
                {
                    Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                }
                Qry = Qrycondition;
                fiscyear = i + 1;
            }
            dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtmerge = new DataTable();
            dtmerge = dsnew.Tables[0];
            Mergecopy = dtmerge;
            if (Mergecopy.Rows.Count > 0)
            {
                dtnewgrid = Mergecopy;
                DataRow dr = dtnewgrid.NewRow();
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            newval = 3;
                        }
                        else
                        {
                            newval = 3;
                        }
                    }
                    else
                    {
                        newval = 4;
                    }
                }
                else
                {
                    newval = 4;
                }
                for (int i = newval; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    dr[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {
                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }

                        }
                    }
                    dr[i] = sum;
                }
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    dr[2] = "Total";
                }
                else
                {
                    dr[3] = "Total";
                }
                dtnewgrid.Rows.Add(dr);
                for (int i = newval; i <= dtnewgrid.Columns.Count - 2; i++)
                {
                    foreach (DataRow drnw in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnw[i]))
                        {
                            double st1;
                            st1 = Convert.ToDouble(drnw[i]);
                            drnw[i] = String.Format("{0:#,###0}", st1);
                        }
                    }
                }
                dtnewgrid.Columns.Add("Total", typeof(String));
                StringFormat();
                dtnewgrid.Columns.Remove("total");

                dtnewgrid.AcceptChanges();
                Gridcontestant.Visible = false;
                Gridcoach.Visible = false;
                Gridparent.Visible = true;
                GridEvent.Visible = false;
                if (DDchoice.SelectedItem.Value.ToString() == "2")
                {
                    Gridcoachparent.Visible = false;
                    Gridparent.Visible = false;
                    GridEventParentchapter.Visible = true;
                    GridEventparent.Visible = false;
                    Gridcontestant.Visible = false;
                    Gridcontestant.Visible = false;
                    Grideventcontest.Visible = false;
                    GridEventParentchapter.DataSource = dtnewgrid;
                    GridEventParentchapter.DataBind();
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        Gridcoachparent.Visible = true;
                        Gridparent.Visible = false;
                        Gridcoachparent.DataSource = dtnewgrid;
                        Gridcoachparent.DataBind();
                    }
                    else
                    {
                        Gridparent.Visible = true;
                        Gridparent.DataSource = dtnewgrid;
                        Gridparent.DataBind();
                    }
                }
            }
            Session["Gridcontestantparent"] = dtnewgrid;
        }
    
    protected void Childdetails()
    {
        GridEventParentchapter.Visible = false;
        GridEventparentdetail.Visible = false;
        childcondition = true;
        lblall.Visible = false;
        EnableDropdown(false);
        sessval = Convert.ToInt32(Session["parentid"]);
        int seval = Convert.ToInt32(Session["chapterid"]);
         Eventid = Convert.ToInt32(Session["Eventid"]);
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    Qryvaluewhere = genWhereConditons1();
                    if (seval == 117)
                    {
                        sqlStr = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select 117 as chapterid,parentid,ChildNumber,ChildName,Grade,year,amt from(select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.EventID=4 and cs.MemberID=" + Session["parentid"] + "  group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))as d";
                    }
                    else if (seval == 112)
                    {
                        sqlStr = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select 112 as chapterid,parentid,ChildNumber,ChildName,Grade,year,amt from(select   cs.ChapterID,cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.EventID=13 and cs.PMemberID=" + Session["parentid"] + "  group by   cs.ChapterID,cs.PMemberID ,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))as d";
                    }
                    else
                    {
                        if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                        {
                            sqlStr = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select   cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID=1 and cs.eventID = 1 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID!=1 and cs.eventID = 2 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and  cs.EventID=3 and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.eventID = 19  and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                        }
                        else
                        {
                            sqlStr = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select   cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID=1 and cs.eventID = 1 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.ChapterID!=1 and cs.eventID = 2 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and  cs.EventID=3 and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + "  group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))  union all";
                            sqlStr += " select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.eventID = 19  and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by   cs.ChapterID,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) ";
                        }
                    }
                    Qrycondition = sqlStr + genWhereConditons();
                    Qry = Qrycondition;
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,cs.contestyear as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";

                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        if (DDReport.SelectedItem.Text == "Report by Parent")
                        {
                            Qrycondition = "with tbl as (select   cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.PMemberID=" + Session["chapterid"] + "  ";
                        }
                        else
                        {
                            Qrycondition = "with tbl as (select   cs.CMemberID as CoachID,cs.PMemberID,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.PMemberID=" + Session["chapterid"] + "  and cs.CMemberID=" + Session["parentid"] + " ";
                        }
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                    {
                        Qrycondition = "with tbl as (select   cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        Qrycondition = "with tbl as (select   cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.MemberID=" + Session["chapterid"] + "  ";
                    }
                    Qry = Qrycondition + genWhereConditons();
                }
            }
            else
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if (seval == 13)
                    {
                        Qrycondition = "with tbl as (select EventId,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select   cs.EventId,cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.EventID=13 and cs.PMemberID=" + Session["parentid"] + "  group by   cs.EventId,cs.PMemberID ,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if (seval == 4)
                    {
                        Qrycondition = "with tbl as (select EventId,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt  from(select   cs.EventId,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,sum(Amount) as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.EventID=4 and cs.MemberID=" + Session["parentid"] + "  group by   cs.EventId,cs.MemberID ,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if (Eventid == 2)
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,cs.contestyear as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   contestyear  between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=2 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["ChapterId"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if (Eventid == 3)
                    {
                        ischapcon = true;
                        isprepclubval = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.MemberId as parentID,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.MemberId=" + Session["parentid"] + " and cs.chapterid=" + Session["ChapterId"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }

                    else if (Eventid == 1)
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,cs.contestyear as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '" + endall + "' and '" + Start + "' and PaymentReference is not null and cs.EventID=1 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["ChapterId"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if (Eventid == 19)
                    {
                        ischapcon = true;
                        isprepclubval = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.MemberId as parentID,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=19 and cs.MemberId=" + Session["parentid"] + " and cs.chapterid=" + Session["ChapterId"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=2 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["ChapterId"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "1"))
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.EventID=1 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.MemberId as parentId,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.MemberId=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                    {
                        ischapcon = true;
                        Qrycondition = "with tbl as (select   cs.EventId,cs.ChapterID,cs.MemberId as parentId,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,Count(*)as amt from Registration_PrepClub  CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.Eventid=19  and cs.MemberId=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        Qrycondition = "with tbl as (select   cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null and cs.PMemberID=" + Session["parentid"] + "  ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        Qrycondition = "with tbl as (select   cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) as year ,sum(Amount) as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber   left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and PaymentReference is not null  and cs.EventID=4 and cs.MemberID=" + Session["parentid"] + " ";
                        Qry = Qrycondition + genWhereConditons();
                    }
                }
            }
        }
        else
        {
            Qryvaluewhere = genWhereConditons1();
            for (i = endall; i <= Start; i++)
            {
                fiscyear = i + 1;
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        if (seval == 112)
                        {
                            CommonQryGame = " select 112 as chapterid,parentID,ChildNumber,ChildName,Grade,year,amt from(select cs.ChapterID,cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.PMemberID=" + Session["parentid"] + "  group by  cs.ChapterID,cs.PMemberID,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d";
                        }
                        else if (seval == 117)
                        {
                            CommonQryGame = "  select 117 as chapterid,parentID,ChildNumber,ChildName,Grade,year,amt from(select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + "  group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))) )as d";
                        }
                        else
                        {
                            CommonQryContest = "select cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=1 and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            CommonQryContestchapter1 = "select cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.EventId=2 and  cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            CommonQryReg = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            CommonQryprepclup = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                    }
                    else
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                            {
                                CommonQry = "select cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                            {
                                CommonQry = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                            {
                                if (DDReport.SelectedItem.Text == "Report by Parent")
                                {
                                    CommonQry = "select cs.ChapterID,cs.PMemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.PMemberID=" + Session["chapterid"] + "  group by  cs.ChapterID,cs.PMemberID,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                                else
                                {
                                    CommonQry = "select cs.CMemberID as CoachID,cs.PMemberID,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.CMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.CMemberID=" + Session["parentid"] + " and cs.PMemberID=" + Session["chapterid"] + "  group by  cs.PMemberID,cs.CMemberID,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                                }
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                            {
                                CommonQry = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Game CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["chapterid"] + "  group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                            else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                            {
                                CommonQry = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                            }
                        }
                    }
                }
                else
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        if (seval == 13)
                        {
                            CommonQry = "select cs.EventID,cs.PMemberID as ParentId,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.PMemberID=" + Session["parentid"] + "  group by  cs.EventID,cs.PMemberID,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        if (seval == 4)
                        {
                            CommonQry = "select cs.EventID,cs.MemberID as parentid,chil.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year, Count(*)  as amt from game CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + "  group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,cs.EventId,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.childnumber,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        if ((Eventid == 1) || ((Eventid == 2)))
                        {
                            CommonQry = "select cs.EventId,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,cs.EventID,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        if (Eventid == 3)
                        {
                            CommonQry = "select cs.EventId,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.EventId,cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                        if (Eventid == 19)
                        {
                            CommonQry = "select cs.EventId,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,cs.EventId,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                        }
                    }

                    if ((ddevent.SelectedItem.Value.ToString() == "1") || (ddevent.SelectedItem.Value.ToString() == "2"))
                    {
                        CommonQry = "select cs.ChapterID,cs.parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Contestant CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.ParentID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.parentid=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.parentid,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "3"))
                    {
                        CommonQry = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from registration CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "19"))
                    {
                        CommonQry = "select cs.ChapterID,cs.MemberID as parentid,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,Count(*)as amt from Registration_PrepClub CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + " and cs.chapterid=" + Session["chapterid"] + " group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "13"))
                    {
                        CommonQry = "select cs.ChapterID,cs.PMemberID ,cs.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,cs.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year,case when cs.ProductGroupCode='SAT' then (Count(*)/2) else Count(*) end as amt from CoachReg CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.PMemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.PMemberID=" + Session["parentid"] + "  group by  cs.ChapterID,cs.PMemberID,cs.ChildNumber,cs.ProductGroupCode,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                    else if ((ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        CommonQry = "select cs.EventID,cs.MemberID as parentid,chil.ChildNumber,(chil.FIRST_NAME+ ' ' + chil.LAST_NAME) as ChildName,chil.GRADE,CASE when (min(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))=" + fiscyear + ") then (max(year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))))-1  else min (CONVERT(varchar(50),year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))))) end as year, Count(*)  as amt from game CS left join child chil on chil.ChildNumber=Cs.ChildNumber  left join IndSpouse I on I.AutoMemberID=Cs.MemberID  left join chapter ch on ch.ChapterID=CS.ChapterId where   DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' and PaymentReference is not null and cs.MemberID=" + Session["parentid"] + "  group by  cs.ChapterID,cs.MemberID,cs.ChildNumber,cs.EventId,chil.FIRST_NAME+ ' ' + chil.LAST_NAME,cs.Amount,chil.childnumber,chil.Grade,year(DATEADD(dd, 0, DATEDIFF(dd, 0,cs.PaymentDate)))";
                    }
                }
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    FiscalyearCondition();
                }
                else
                {
                    if (WCNTQry != string.Empty)
                    {
                        WCNTQrynew = CommonQry;
                        WCNTQry = WCNTQry + " union all " + WCNTQrynew;
                    }
                    else
                    {
                        WCNTQry = CommonQry;
                    }
                }
            }
            if (ddevent.SelectedItem.Text == "All")
            {
                if (seval == 112)
                {
                    joinquery = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt from(" + WCNTQryGame + genWhereConditons();
                }
                else if (seval == 117)
                {
                    joinquery = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt from(" + WCNTQryGame + genWhereConditons();
                }
                else
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "2")
                    {
                        Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                        joinquery = Qrycondition;
                    }
                    else
                    {
                        joinquery = "with tbl as (select chapterid,ParentID,ChildNumber,ChildName,Grade,year,SUM(amt) as amt from(" + WCNTQry + " union all " + WCNTQryContest1 + "union all " + WCNTQryreg + "union all " + WCNTQryprepclub + genWhereConditons();
                    }
                }
                Qrycondition = joinquery;
            }
            else
            {
                Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
            }
            Qry = Qrycondition;
            fiscyear = i + 1;
          
        }
        dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
        DataTable dtmerge = new DataTable();
        dtmerge = dsnew.Tables[0];
        Mergecopy = dtmerge;
        if (Mergecopy.Rows.Count > 0)
        {
            dtnewgrid = Mergecopy;
            if ((ddevent.SelectedItem.Value.ToString() == "13"))
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if (DDReport.SelectedItem.Text == "Report by Parent")
                    {
                        newval = 3;
                    }
                    else
                    {
                        newval = 5;
                    }
                }
                else
                {

                    newval = 3;
                }
            }
            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
            {
                newval = 4;
            }
            else
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    newval = 5;
                }
                else
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        if (DDyear.SelectedItem.Text != "Fiscal Year")
                        {
                            newval = 4;
                        }
                        else
                        {
                            newval = 5;
                        }
                    }
                    else
                    {
                        newval = 4;
                    }
                }
            }
            DataRow dr = dtnewgrid.NewRow();
            for (int i = newval; i <= dtnewgrid.Columns.Count - 1; i++)
            {
                dr[i] = 0;
                double sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtnewgrid.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }

                    }
                }
                dr[i] = sum;
            }
            if ((ddevent.SelectedItem.Value.ToString() == "13"))
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if (DDReport.SelectedItem.Text == "Report by Parent")
                    {
                        Gridcoachparent.Visible = false;
                        dr[1] = "Total";
                    }
                    else
                    {
                        Gridcoachparent.Visible = false;
                        dr[3] = "Total";
                    }
                }
                else
                {
                    GridEventparentdetail.Visible = false;
                    dr[1] = "Total";
                }
            }
            else if ((ddevent.SelectedItem.Value.ToString() == "4"))
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    dr[2] = "Total";
                }
                else
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        dr[2] = "Total";
                    }
                    else
                    {
                        dr[3] = "Total";
                    }
                }
            }
            else
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if ((ddevent.SelectedItem.Text == "All") && (DDyear.SelectedItem.Text == "Fiscal Year"))
                    {
                        dr[3] = "Total";
                    }
                    else
                    {
                        dr[2] = "Total";
                    }
                }
                else
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        dr[2] = "Total";
                    }
                    else
                    {
                        if ((seval == 13) || (seval == 4) || (Eventid == 1) || (Eventid == 2) || (Eventid == 3) || (Eventid == 19))
                        {
                            if (DDyear.SelectedItem.Text == "Fiscal Year")
                            {

                                if ((DDchoice.SelectedItem.Value.ToString() == "2")  && (ddevent.SelectedItem.Text == "All"))
                                {
                                    dr[3] = "Total";
                                }
                                else
                                {
                                    dr[2] = "Total";
                                }
                            }
                            else
                            {
                                dr[3] = "Total";
                            }
                        }
                      
                        else
                        {
                            dr[2] = "Total";
                        }
                    }
                }
            }
            dtnewgrid.Rows.Add(dr);
            for (int i = newval; i <= dtnewgrid.Columns.Count - 2; i++)
            {
                foreach (DataRow drnw in dtnewgrid.Rows)
                {
                    if (!DBNull.Value.Equals(drnw[i]))
                    {
                        double st1;
                        st1 = Convert.ToDouble(drnw[i]);
                        drnw[i] = String.Format("{0:#,###0}", st1);
                    }
                }
            }
            dtnewgrid.Columns.Add("Total", typeof(String));
            StringFormat();
            dtnewgrid.Columns.Remove("total");
            dtnewgrid.AcceptChanges();
            Gridcontestant.Visible = false;
            Gridparent.Visible = false;
            Gridcoachparent.Visible = false;
            Gridcoachparent.Visible = false;
            Gridcoach.Visible = false;
            GridEventparent.Visible = false;
            GridViewcoachparentandchild.Visible = false;
            Gridchild.Visible = true;
            Gridchild.DataSource = dtnewgrid;
            Gridchild.DataBind();
        }
        Session["Gridcontestantchild"] = dtnewgrid;
    }
    protected void Gridcontestant_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int newno;
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            newno = 3;
        }
        else
        {
            newno = 4;
        }
        if (e.Row.RowIndex == -1) return;
        for (int i = newno; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdchapterid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }

    protected void StringFormat()
    {
        for (int i = dtnewgrid.Columns.Count - 1; i <= dtnewgrid.Columns.Count - 1; i++)
        {
            foreach (DataRow drn in dtnewgrid.Rows)
            {
                if (!DBNull.Value.Equals(drn))
                {
                    string st;
                    double st1;
                    st1 = Convert.ToDouble(drn["total"]);
                    st = String.Format("{0:#,###0}", drn["total"]);
                    drn["Total"] = st.ToString();
                }
            }
        }
    }
    protected void Gridcontestant_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Gridcontestant.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestant"];
            if (dtex == null)
            {
                buttonclick();
            }
            Gridcontestant.DataSource = (DataTable)Session["Gridcontestant"];
            Gridcontestant.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void Gridparent_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Gridparent.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestantparent"];
            Gridparent.DataSource = (DataTable)Session["Gridcontestantparent"];
            Gridparent.DataBind();
        }
        catch (Exception ex) { }

    }
    protected void Gridparent_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnchapterid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    private void EnableDropdown(bool IsEnable)
    {
        ddZone.Enabled = IsEnable;
        ddNoyear.Enabled = IsEnable;
        DDyear.Enabled = IsEnable;
        ddevent.Enabled = IsEnable;
        ddCluster.Enabled = IsEnable;
        ddchapter.Enabled = IsEnable;
        DDReport.Enabled = IsEnable;
    }
    protected void lbprevious_Click(object sender, EventArgs e)
    {
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            if ((ddevent.SelectedItem.Value.ToString() == "13"))
            {
                if (DDReport.SelectedItem.Text == "Report by Parent")
                {
                    GridViewcoachparentandchild.Visible = true;
                    lbprevious.Visible = false;
                    EnableDropdown(true);
                    Gridchild.Visible = false;
                }
                else
                {
                    if (Gridcoachparent.Visible == true)
                    {
                        Gridcoach.Visible = true;
                        lbprevious.Visible = false;
                        EnableDropdown(true);
                        Gridchild.Visible = false;
                        Gridcoachparent.Visible = false;
                    }
                    else if (Gridchild.Visible == true)
                    {
                        Gridcoachparent.Visible = true;
                        Gridcoach.Visible = false;
                        Gridchild.Visible = false;
                    }
                }
            }

            else
            {
                if (Gridparent.Visible == true)
                {
                    Gridcontestant.Visible = true;
                    Gridparent.Visible = false;
                    lbprevious.Visible = false;
                    EnableDropdown(true);
                }
                else if (Gridchild.Visible == true)
                {
                    if (ddevent.SelectedItem.Value.ToString() == "4")
                    {
                        GridViewcoachparentandchild.Visible = true;
                        lbprevious.Visible = true;
                        Gridchild.Visible = false;
                    }

                    else
                    {
                        Gridparent.Visible = true;
                        Gridcontestant.Visible = false;
                        Gridchild.Visible = false;
                        EnableDropdown(true);
                    }
                }
            }
        }
        else if ((DDchoice.SelectedItem.Value.ToString() == "3") || (DDchoice.SelectedItem.Value.ToString() == "4"))
        {
            lbprevious.Visible = false;
            EnableDropdown(true);
            Gridnewcontest.Visible = true;
            Gridcontestdetail.Visible = false;
        }
        else
        {
            if ((GridEventparent.Visible == true) || (Grideventcontest.Visible == true))
            {
                if ((ddevent.SelectedItem.Value.ToString() == "2"))
                {
                    if (Grideventcontest.Visible == true)
                    {
                        lbprevious.Visible = false;
                        EnableDropdown(true);
                        GridEvent.Visible = true;
                        Grideventcontest.Visible = false;
                    }
                    else
                    {
                        Grideventcontest.Visible = true;
                        GridEventParentchapter.Visible = false;
                    }
                }
                else
                {
                    lbprevious.Visible = false;
                    EnableDropdown(true);
                    if ((ddevent.SelectedItem.Value.ToString() == "13") || (ddevent.SelectedItem.Value.ToString() == "4"))
                    {
                        GridEventparentdetail.Visible = true;
                        Gridchild.Visible = false;
                    }
                    else
                    {
                        DDReport.Enabled = true;
                        GridEventparent.Visible = false;
                        GridEvent.Visible = true;
                        Gridcontestant.Visible = false;
                        GridEventparent.Visible = false;
                        Grideventcontest.Visible = false;
                        lbprevious.Visible = false;
                        GridEventParentchapter.Visible = false;
                    }
                }
            }
            else if (GridEventParentchapter.Visible == true)
            {
                Grideventcontest.Visible = true;
                GridEvent.Visible = false;
                GridEventParentchapter.Visible = false;
            }
            else if (Gridchild.Visible == true)
            {
                if ((ddevent.SelectedItem.Value.ToString() == "13") || (ddevent.SelectedItem.Value.ToString() == "4"))
                {
                    Gridchild.Visible = false;
                    EnableDropdown(true);
                    GridEventparentdetail.Visible = true;
                    lbprevious.Visible = false;
                }
                else
                {
                    Gridchild.Visible = false;
                    int f = Convert.ToInt32(Session["chapterid"]);
                    if (f == 13)
                    {
                        GridEventparent.Visible = true;
                    }
                    else
                    {
                        GridEventParentchapter.Visible = true;
                    }
                }
            }
            else if (GridEventparent.Visible == true)
            {
                GridEvent.Visible = true;
                GridEventparent.Visible = false;
            }
        }
    }
    protected void LBbacktofront_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        DateTime now = DateTime.Now;
        string yearshort;
        string month = now.ToString("MMM");
        string day = now.ToString("dd");
        string all = string.Concat(month, day);
        string date = all + "_" + StaYear;
        lbdate = date;
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            yearshort="CY";
        }
        else
        {
            yearshort = "FY";
        }
        if ((DDchoice.SelectedItem.Value.ToString() == "3")||(DDchoice.SelectedItem.Value.ToString() == "4"))
        {
            if (lbprevious.Visible == true)
            {
                GeneralExport((DataTable)Session["Gridchapterevent"], "RegCountByContest_" + yearshort + "_" + lbdate+ ".xls");
            }
            else
            {
                GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByContest_" + yearshort + "_" + lbdate + ".xls");
            }
        }
        else
        {
            if ((Gridcontestant.Visible == true) || (GridEvent.Visible == true) || (GridEventparentdetail.Visible == true))
            {
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if (GridEventparentdetail.Visible == true)
                    {
                        GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
                    }
                    else
                    {
                        GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByChapter_" + yearshort + "_" + lbdate + ".xls");
                    }
                }
                else
                {
                    if (Gridcontestant.Visible == true)
                    {
                        GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByChapter_" + yearshort + "_" + lbdate + ".xls");
                    }
                    else
                    {
                        if ((ddevent.SelectedItem.Value.ToString() == "13") || (ddevent.SelectedItem.Value.ToString() == "4"))
                        {
                            GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
                        }
                        else
                        {
                            GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByEvent_" + yearshort + "_" + lbdate + ".xls");
                        }
                    }
                }
            }
            else if (GridViewcoachparentandchild.Visible == true)
            {
                GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
            }
            else if (Gridcoach.Visible == true)
            {
                GeneralExport((DataTable)Session["Gridcontestant"], "RegCountByCoach_" + yearshort + "_" + lbdate + ".xls");
            }
            else if ((Grideventcontest.Visible == true) || (GridEventparent.Visible == true))
            {
                if (GridEventparent.Visible == true)
                {
                    GeneralExport((DataTable)Session["Gridchapterevent"], "RegCountByParent_" + yearshort + "_" + lbdate+ ".xls");
                }
                else
                {
                    GeneralExport((DataTable)Session["Gridchapterevent"], "RegCountByChapter_" + yearshort + "_" + lbdate + ".xls");
                }
            }

            else if (GridEventParentchapter.Visible == true)
            {
                GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
            }
            else if (Gridcoachparent.Visible == true)
            {
                GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
            }
            else if (Gridparent.Visible == true)
            {
                if ((ddevent.SelectedItem.Value.ToString() == "13"))
                {
                    if (DDReport.SelectedItem.Text == "Report by Parent")
                    {
                        GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
                    }
                    else
                    {
                        GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByCoach_" + yearshort + "_" + lbdate+ ".xls");
                    }
                }
                else
                {
                    GeneralExport((DataTable)Session["Gridcontestantparent"], "RegCountByParent_" + yearshort + "_" + lbdate + ".xls");
                }
            }
            else
            {
                GeneralExport((DataTable)Session["Gridcontestantchild"], "RegCountByChild_" + yearshort + "_" + lbdate + ".xls");
            }
        }
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {
                    Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                }
                Response.Write("\n");
            }
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void Gridchild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }
        if ((ddevent.SelectedItem.Value.ToString() == "13"))
        {
            if (DDReport.SelectedItem.Text == "Report by Parent")
            {
                newval = 3;
            }
            else
            {
                newval = 5;
            }
        }
        else
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                newval = 5;
            }
            else
            {
                newval = 4;
            }
        }
        for (int i = newval; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
    }
    protected void Gridcoach_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdcoach");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void Gridcoach_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Gridcoach.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestant"];
            Gridcoach.DataSource = (DataTable)Session["Gridcontestant"];
            Gridcoach.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GridViewcoachparentandchild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int newno;
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            newno = 3;
        }
        else
        {
            newno = 4;
        }
        if (e.Row.RowIndex == -1) return;
        for (int i = newno; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdparentchildid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }

    }
    protected void GridViewcoachparentandchild_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewcoachparentandchild.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestant"];
            if (dtex == null)
            {
                buttonclick();
            }
            GridViewcoachparentandchild.DataSource = (DataTable)Session["Gridcontestant"];
            GridViewcoachparentandchild.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void Gridcoachparent_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 5; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdparentid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void Gridcoachparent_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Gridcoachparent.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestantparent"];
            if (dtex == null)
            {
                buttonclick();
            }

            Gridcoachparent.DataSource = (DataTable)Session["Gridcontestantparent"];
            Gridcoachparent.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GridEvent_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }
        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdchapterid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void GridEventparent_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int newno;
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            newno = 3;
        }
        else
        {
            newno = 4;
        }
        if (e.Row.RowIndex == -1) return;
        for (int i = newno; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdparentchildid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void GridEventparent_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEventparent.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridchapterevent"];
            GridEventparent.DataSource = (DataTable)Session["Gridchapterevent"];
            GridEventparent.DataBind();
        }
        catch (Exception ex) { }

    }
    protected void Grideventcontest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grideventcontest.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridchapterevent"];
            Grideventcontest.DataSource = (DataTable)Session["Gridchapterevent"];
            Grideventcontest.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void Grideventcontest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdchapterid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void lnkViewevent_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField kchapter = (HiddenField)Grideventcontest.Rows[index].Cells[0].FindControl("Hdchapterid");
            HiddenField k = (HiddenField)Grideventcontest.Rows[index].Cells[0].FindControl("HidEventId");
            ExpchapterID = Convert.ToInt32(kchapter.Value);
            ExpparentID = Convert.ToInt32(k.Value);
            Session["chapterid"] = ExpchapterID;
            Session["Eventid"] = ExpparentID;
            Parentdetails();
        }
        catch (Exception err)
        {
        }
    }
    protected void GridEventParentchapter_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEventParentchapter.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestantparent"];
            if (dtex == null)
            {
                buttonclick();
            }
            GridEventParentchapter.DataSource = (DataTable)Session["Gridcontestantparent"];
            GridEventParentchapter.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GridEvent_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEvent.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestant"];
            if (dtex == null)
            {
                buttonclick();
            }
            GridEvent.DataSource = (DataTable)Session["Gridcontestant"];
            GridEvent.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GridEventParentchapter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 5; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdparentid");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void GridEventparentdetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEventparentdetail.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Gridcontestant"];
            if (dtex == null)
            {
                buttonclick();
            }

            GridEventparentdetail.DataSource = (DataTable)Session["Gridcontestant"];
            GridEventparentdetail.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GridEventparentdetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hiddenparentidfield");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void Gridnewcontest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 2; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnparentview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdchapterid");
            if ((hdnid.Value == "Total")||(hdnid.Value == "Total less SAT English"))
            {
                dpEmpdept.Visible = false;
            }
        }
    }
    protected void Gridnewcontest1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        }
        
    }
}
