using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_InputNationalCriteria : System.Web.UI.Page
{
    int Year;
    static DataTable dt;
    int grCCount, MB1Count, MB2Count, MB3Count, MB4Count, JSBCount, SSBCount,
        JVBCount, IVBCount, SVBCount, JGBCount, SGBCount, EW1Count, EW2Count, EW3Count,
        PS1Count, PS2Count, PS3Count, JSCCount, ISCCount, SSCCount;
    int MB1CumuCount, MB2CumuCount, MB3CumuCount, MB4CumuCount, JSBCumuCount, SSBCumuCount,
        JVBCumuCount, IVBCumuCount, SVBCumuCount, JGBCumuCount, SGBCumuCount, EW1CumuCount, EW2CumuCount, EW3CumuCount,
        PS1CumuCount, PS2CumuCount, PS3CumuCount;
    int with1Count, MB1min = 100, MB2min = 100, MB3min = 100, MB4min = 100, JSBmin = 100, SSBmin = 100,
        JVBmin = 100, IVBmin = 100, SVBmin = 100, JGBmin = 100, SGBmin = 100, EW1min = 100, EW2min = 100, EW3min = 100,
        PS1min = 100, PS2min = 100, PS3min = 100, JSCmin = 100, ISCmin = 100, SSCmin = 100;

    int MB1cntRank1, MB2cntRank1, MB3cntRank1, MB4cntRank1, JSBcntRank1, SSBcntRank1,
        JVBcntRank1, IVBcntRank1, SVBcntRank1, JGBcntRank1, SGBcntRank1, EW1cntRank1, EW2cntRank1, EW3cntRank1,
        PS1cntRank1, PS2cntRank1, PS3cntRank1, JSCcntRank1, ISCcntRank1, SSCcntRank1;

    int MB1Rank1Score = 100, MB2Rank1Score = 100, MB3Rank1Score = 100, MB4Rank1Score = 100, JSBRank1Score = 100,
        SSBRank1Score = 100, JVBRank1Score = 100, IVBRank1Score = 100, SVBRank1Score = 100, JGBRank1Score = 100,
        SGBRank1Score = 100, EW1Rank1Score = 100, EW2Rank1Score = 100, EW3Rank1Score = 100,
        PS1Rank1Score = 100, PS2Rank1Score = 100, PS3Rank1Score = 100, JSCRank1Score = 100, ISCRank1Score = 100, SSCRank1Score = 100;
   
    int MB1NwScore, MB2NwScore, MB3NwScore, MB4NwScore, JSBNwScore, SSBNwScore,
        JVBNwScore, IVBNwScore, SVBNwScore, JGBNwScore, SGBNwScore, EW1NwScore, EW2NwScore, EW3NwScore,
        PS1NwScore, PS2NwScore, PS3NwScore, JSCNwScore, ISCNwScore, SSCNwScore;

    int MB1MaxScore, MB2MaxScore, MB3MaxScore, MB4MaxScore, JSBMaxScore, SSBMaxScore,
        JVBMaxScore, IVBMaxScore, SVBMaxScore, JGBMaxScore, SGBMaxScore, EW1MaxScore, EW2MaxScore, EW3MaxScore,
        PS1MaxScore, PS2MaxScore, PS3MaxScore, JSCMaxScore, ISCMaxScore, SSCMaxScore;


    protected void Page_Load(object sender, EventArgs e)
    {
        //Year = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand("usp_GetOpenEventYear", connection);
        cmd.CommandType  = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@EventID", 2);
        Year = Convert.ToInt32(cmd.ExecuteScalar());
        connection.Close();
         if (!IsPostBack)
        {
            lblMessage.Text = "Please Update the scores and click Save button.";
            bool result = ReadDataBase();
            if(result == false)
            ReadContestCategories();
        }
    }
    private bool ReadDataBase()
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        string commandString = "SELECT N.Contest, N.Grade, N.MinScore,N.Rank1Score, N.NewChScore, N.MaxScore from NationalCriteria N Left Outer Join contestcategory C ON N.Contest = C.contestcode where contestyear = " + Year + " and NationalSelectionCriteria is null and NationalFinalsStatus = 'Active' order by C.ContestCategoryID";
        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        DataSet dsContests = new DataSet();
        int retCount = daContests.Fill(dsContests, "Contests");
        if (retCount > 0)
        {
            gvCriteria.DataSource = dsContests.Tables[0];
            gvCriteria.DataBind();
            dt = dsContests.Tables[0];
            return true;
        }
        return false;
    }
    private void ReadContestCategories()
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "SELECT ContestCode, GradeFrom, GradeTo From ContestCategory WHERE contestyear = " + Year + " and NationalSelectionCriteria is null and NationalFinalsStatus = 'Active' order by ContestCategoryID";

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        DataSet dsContests = new DataSet();
        daContests.Fill(dsContests, "Contests");
        dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Contest", typeof(string));
        dt.Columns.Add("Grade", typeof(int));
        dt.Columns.Add("MinScore", typeof(int));
        dt.Columns.Add("Rank1Score", typeof(int));
        dt.Columns.Add("NewChScore", typeof(int));
        dt.Columns.Add("MaxScore", typeof(int));
        //dt.Columns.Add("Count", typeof(int));
        string contest;
        int gradeFrom, gradeTo;
        for (int i = 0; i < dsContests.Tables["Contests"].Rows.Count; i++)
        {
            contest = dsContests.Tables["Contests"].Rows[i].ItemArray[0].ToString();
            gradeFrom = Convert.ToInt32(dsContests.Tables["Contests"].Rows[i].ItemArray[1].ToString());
            gradeTo = Convert.ToInt32(dsContests.Tables["Contests"].Rows[i].ItemArray[2].ToString());
            while (gradeFrom <= gradeTo)
            {
                dr = dt.NewRow();
                dr["Contest"] = contest;
                dr["Grade"] = gradeFrom;
                dr["MinScore"] = 0;
                dr["Rank1Score"] = 0;
                dr["NewChScore"] = 0;
                dr["MaxScore"] = 0;

                gradeFrom++;
                dt.Rows.Add(dr);
            }
        }
        gvCriteria.DataSource = dt;
        gvCriteria.DataBind();
    }
    private void GetMins()
    {
        for(int i=0; i < dt.Rows.Count; i++)
        {
        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) > 0)
            {
                switch (dt.Rows[i].ItemArray[0].ToString().Trim())
                {
                    case "JSB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JSBmin)
                            JSBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            JSBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            JSBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "SSB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SSBmin)
                            SSBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            SSBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            SSBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "JVB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JVBmin)
                            JVBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            JVBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            JVBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "IVB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < IVBmin)
                            IVBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            IVBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            IVBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "SVB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SVBmin)
                            SVBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            SVBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            SVBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "JGB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JGBmin)
                            JGBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            JGBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            JGBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "SGB":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SGBmin)
                            SGBmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            SGBNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            SGBMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "MB1":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB1min)
                            MB1min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            MB1NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            MB1MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "MB2":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB2min)
                            MB2min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            MB2NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            MB2MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "MB3":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB3min)
                            MB3min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            MB3NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            MB3MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "MB4":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB4min)
                            MB4min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            MB4NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            MB4MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "EW1":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW1min)
                            EW1min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            EW1NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            EW1MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "EW2":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW2min)
                            EW2min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            EW2NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            EW2MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "EW3":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW3min)
                            EW3min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            EW3NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            EW3MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "PS1":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS1min)
                            PS1min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            PS1NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            PS1MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "PS2":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS2min)
                            PS2min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            PS2NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            PS2MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "PS3":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS3min)
                            PS3min = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            PS3NwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            PS3MaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "JSC":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JSCmin)
                            JSCmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            JSCNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            JSCMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "ISC":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < ISCmin)
                            ISCmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            ISCNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            ISCMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                    case "SSC":
                        if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SSCmin)
                            SSCmin = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                            SSCNwScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
                            SSCMaxScore = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
                        break;
                }
           
            }
        }

    }
    protected void btnRecalc_Click(object sender, EventArgs e)
    {
        GetMins();
        DataTable dt2 = new DataTable();
        DataRow dr;
        int numCntestants, inviteCount, cntRank1;
        
        dt2.Columns.Add("Contest", typeof(string));
        dt2.Columns.Add("Grade", typeof(int));
        dt2.Columns.Add("MinScore", typeof(int));
        dt2.Columns.Add("Rank1Score", typeof(int));
        dt2.Columns.Add("NwScore", typeof(int));
        dt2.Columns.Add("MaxScore", typeof(int));
        dt2.Columns.Add("Count", typeof(int));
        dt2.Columns.Add("TotalCount", typeof(int));
        dt2.Columns.Add("Contestants", typeof(int));
        dt2.Columns.Add("Invited", typeof(string));
        dt2.Columns.Add("AddRank1", typeof(int));
        dt2.Columns.Add("TotalWrank1", typeof(int));
        dt2.Columns.Add("CumulWrank1", typeof(int));
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            numCntestants = 0;
            inviteCount = 0;
            cntRank1 = 0;
            dr = dt2.NewRow();
            dr["Contest"] = dt.Rows[i].ItemArray[0].ToString();
            dr["Grade"] = Convert.ToInt32(dt.Rows[i].ItemArray[1]);
            dr["MinScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
            dr["Rank1Score"] = Convert.ToInt32(((Label)gvCriteria.Rows[i].Cells[2].FindControl("lblRank1Score")).Text);
            dr["NwScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
            dr["MaxScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
            if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) > 0)
            {
                inviteCount = GetCount(dt.Rows[i].ItemArray[0].ToString().Trim(), Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text), Convert.ToInt32(((Label)gvCriteria.Rows[i].Cells[2].FindControl("Label1")).Text),ref numCntestants, ref cntRank1);
                dr["Count"] = inviteCount;
            }
            else dr["Count"] = 0;
                switch (dt.Rows[i].ItemArray[0].ToString().Trim())
                {
                    case "JSB":
                        dr["TotalCount"] = JSBCount;
                        dr["CumulWrank1"] = JSBCount + JSBcntRank1;
                        break;
                    case "SSB":
                        dr["TotalCount"] = SSBCount;
                        dr["CumulWrank1"] = SSBCount + SSBcntRank1;
                        break;
                    case "JVB":
                        dr["TotalCount"] = JVBCount;
                        dr["CumulWrank1"] = JVBCount + JVBcntRank1;
                        break;
                    case "IVB":
                        dr["TotalCount"] = IVBCount;
                        dr["CumulWrank1"] = IVBCount + IVBcntRank1;
                        break;
                    case "SVB":
                        dr["TotalCount"] = SVBCount;
                        dr["CumulWrank1"] = SVBCount + SVBcntRank1;
                        break;
                    case "JGB":
                        dr["TotalCount"] = JGBCount;
                        dr["CumulWrank1"] = JGBCount + JGBcntRank1;
                        break;
                    case "SGB":
                        dr["TotalCount"] = SGBCount;
                        dr["CumulWrank1"] = SGBCount + SGBcntRank1;
                        break;
                    case "MB1":
                        dr["TotalCount"] = MB1Count;
                        dr["CumulWrank1"] = MB1Count + MB1cntRank1;
                        break;
                    case "MB2":
                        dr["TotalCount"] = MB2Count;
                        dr["CumulWrank1"] = MB2Count + MB2cntRank1;
                        break;
                    case "MB3":
                        dr["TotalCount"] = MB3Count;
                        dr["CumulWrank1"] = MB3Count + MB3cntRank1;
                        break;
                    case "MB4":
                        dr["TotalCount"] = MB4Count;
                        dr["CumulWrank1"] = MB4Count + MB4cntRank1;
                        break;
                    case "EW1":
                        dr["TotalCount"] = EW1Count;
                        dr["CumulWrank1"] = EW1Count + EW1cntRank1;
                        break;
                    case "EW2":
                        dr["TotalCount"] = EW2Count;
                        dr["CumulWrank1"] = EW2Count + EW2cntRank1;
                        break;
                    case "EW3":
                        dr["TotalCount"] = EW3Count;
                        dr["CumulWrank1"] = EW3Count + EW3cntRank1;
                        break;
                    case "PS1":
                        dr["TotalCount"] = PS1Count;
                        dr["CumulWrank1"] = PS1Count + PS1cntRank1;
                        break;
                    case "PS2":
                        dr["TotalCount"] = PS2Count;
                        dr["CumulWrank1"] = PS2Count + PS2cntRank1;
                        break;
                    case "PS3":
                        dr["TotalCount"] = PS3Count;
                        dr["CumulWrank1"] = PS3Count + PS3cntRank1;
                        break;
                    case "JSC":
                        dr["TotalCount"] = JSCCount;
                        dr["CumulWrank1"] = JSCCount + JSCcntRank1;
                        break;
                    case "ISC":
                        dr["TotalCount"] = ISCCount;
                        dr["CumulWrank1"] = ISCCount + ISCcntRank1;
                        break;
                    case "SSC":
                        dr["TotalCount"] = SSCCount;
                        dr["CumulWrank1"] = SSCCount + SSCcntRank1;
                        break;
                }
                dr["Contestants"] = numCntestants;
                if (numCntestants > 0)
                    dr["Invited"] = String.Format("{0:N2}", ((float)inviteCount / numCntestants) * 100);
                else dr["Invited"] = "";
                dr["AddRank1"] = cntRank1;
            dr["TotalWrank1"] = inviteCount + cntRank1;
                
                dt2.Rows.Add(dr);
                with1Count += cntRank1;
        }
        gvOut.DataSource = dt2;
        gvOut.DataBind();
        gvOut.Visible = true;
        btnRecalc.Visible = false;
        lblMessage.Text = "";
        gvCriteria.Visible = false;
        lblMessage.Text = "Grand Total: " + grCCount.ToString() + " with addl rank1 holders total is: " + (with1Count+grCCount).ToString();
        SaveToDB(dt2);
    }
    private void SaveToDB(DataTable dt)
    {
        // Update rows
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        //get rid of old data
        string delCmdString = "Delete from NationalCriteria";
        SqlCommand delCmd = new SqlCommand(delCmdString, connection);
        delCmd.ExecuteNonQuery();

        // get records from the products table

        string commandString = "INSERT INTO NationalCriteria (SeqNo, Contest, Grade, MinScore,NewChScore,MaxScore,Rank1Score,CreatedDate) Values " +
            "(@seqno, @contest, @grade, @minscore,@newchscore,@maxscore,@Rank1Score,GetDate())";
        SqlCommand insCmd = new SqlCommand(commandString, connection);
        insCmd.Parameters.Add("@seqno", SqlDbType.Int);
        insCmd.Parameters.Add("@contest", SqlDbType.Char, 10);
        insCmd.Parameters.Add("@grade", SqlDbType.Int);
        insCmd.Parameters.Add("@minscore", SqlDbType.Int);
        insCmd.Parameters.Add("@newchscore", SqlDbType.Int);
        insCmd.Parameters.Add("@maxscore", SqlDbType.Int);
        insCmd.Parameters.Add("@Rank1Score", SqlDbType.Int);
        //DataRow dr;
        int i = 1;
        foreach (DataRow dr in dt.Rows)
        {
            insCmd.Parameters["@seqno"].Value = i;
            insCmd.Parameters["@contest"].Value = dr["Contest"];
            insCmd.Parameters["@grade"].Value = dr["Grade"];
            insCmd.Parameters["@minscore"].Value = dr["MinScore"];
            insCmd.Parameters["@NewChScore"].Value = dr["NwScore"];
            insCmd.Parameters["@MaxScore"].Value = dr["MaxScore"];
            insCmd.Parameters["@Rank1Score"].Value = dr["Rank1Score"];
            insCmd.ExecuteNonQuery();
            i++;
        }
        connection.Close();
    }
    private int GetCount(string contest, int score, int grade, ref int numCntestants, ref int cntRank1)
    {
        // connect to the peoducts database
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
            "Contestant.Score1, Contestant.Score2, Contestant.Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID, Child.GRADE  from Contestant " +
            "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber " +
            " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear=" +
            Year + " AND (Contestant.Score1>0 OR Contestant.Score2>0) AND Contestant.ProductCode = '" + contest + "' AND EventId=2 AND Child.GRADE=" + grade + " ORDER BY Contestant.ChapterID";

        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        DataSet dsContestants = new DataSet();
        daContestants.Fill(dsContestants);
        int cnt = ProcessData(dsContestants, contest, score, grade, ref numCntestants, ref cntRank1);
        return cnt;
    }
    int ProcessData(DataSet ds, string contest, int score, int grade,ref int numCntestants, ref int cntRank1)
    {
        //int[] aryTscore = new int[ds.Tables[0].Rows.Count];
        //int[] grScore = new int[ds.Tables[0].Rows.Count];
        Response.Write( " <br> Contest :" + contest + "Score :" + score + " Grade :" + grade + " NumContestant :" + numCntestants + "CntRank 1 :" + cntRank1 );
        int aScore = 0;
        int gCount = 0;
        cntRank1 = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            //grScore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
            aScore = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
            if (aScore >= score)
                gCount++;
            else {
              switch (contest.Trim())
                {
                    case "JSB":
                        if (aScore >= JSBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            JSBcntRank1++;
                            JSBCumuCount = cntRank1;
                        }
                        break;
                    case "SSB":
                        if (aScore >= SSBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            SSBcntRank1++;
                            SSBCumuCount = cntRank1;
                        }
                        break;
                    case "JVB":
                        if (aScore >= JVBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            JVBcntRank1++;
                            JVBCumuCount = cntRank1;
                        }
                        break;
                    case "IVB":
                        if (aScore >= IVBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            IVBcntRank1++;
                            IVBCumuCount = cntRank1;
                        }
                        break;
                    case "SVB":
                        if (aScore >= SVBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            SVBcntRank1++;
                            SVBCumuCount = cntRank1;
                        }
                        break;
                    case "JGB":
                        if (aScore >= JGBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            JGBcntRank1++;
                            JGBCumuCount = cntRank1;
                        }
                        break;
                    case "SGB":
                        if (aScore >= SGBmin && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            SGBcntRank1++;
                            SGBCumuCount = cntRank1;
                        }
                        break;
                    case "MB1":
                        if (aScore >= MB1min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            MB1cntRank1++;
                            MB1CumuCount = cntRank1;
                        }
                        break;
                    case "MB2":
                        if (aScore >= MB2min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            MB2cntRank1++;
                            MB2CumuCount = cntRank1;
                        }
                        break;
                    case "MB3":
                        if (aScore >= MB3min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            MB3cntRank1++;
                            MB3CumuCount = cntRank1;
                        }
                        break;
                    case "MB4":
                        if (aScore >= MB4min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            MB4cntRank1++;
                            MB4CumuCount = cntRank1;
                        }
                        break;
                    case "EW1":
                        if (aScore >= EW1min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            EW1cntRank1++;
                            EW1CumuCount = cntRank1;
                        }
                        break;
                    case "EW2":
                        if (aScore >= EW2min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            EW2cntRank1++;
                            EW2CumuCount = cntRank1;
                        }
                        break;
                    case "EW3":
                        if (aScore >= EW3min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            EW3cntRank1++;
                            EW3CumuCount = cntRank1;
                        }
                        break;
                    case "PS1":
                        if (aScore >= PS1min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            PS1cntRank1++;
                            PS1CumuCount = cntRank1;
                        }
                        break;
                    case "PS2":
                        if (aScore >= PS2min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            PS2cntRank1++;
                            PS2CumuCount = cntRank1;
                        }
                        break;
                    case "PS3":
                        if (aScore >= PS3min && Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[5]) == 1)
                        {
                            cntRank1++;
                            PS3cntRank1++;
                            PS3CumuCount = cntRank1;
                        }
                        break;
                }   
            }
                
                
            numCntestants++;
        }

              
        grCCount += gCount;
        switch (contest)
        {
            case "JSB":
                JSBCount += gCount;
                break;
            case "SSB":
                SSBCount += gCount;
                break;
            case "JVB":
                JVBCount += gCount;
                break;
            case "IVB":
                IVBCount += gCount;
                break;
            case "SVB":
                SVBCount += gCount;
                break;
            case "MB1":
                MB1Count += gCount;
                break;
            case "MB2":
                MB2Count += gCount;
                break;
            case "MB3":
                MB3Count += gCount;
                break;
            case "MB4":
                MB4Count += gCount;
                break;
            case "JGB":
                JGBCount += gCount;
                break;
            case "SGB":
                SGBCount += gCount;
                break;
            case "EW1":
                EW1Count += gCount;
                break;
            case "EW2":
                EW2Count += gCount;
                break;
            case "EW3":
                EW3Count += gCount;
                break;
            case "PS1":
                PS1Count += gCount;
                break;
            case "PS2":
                PS2Count += gCount;
                break;
            case "PS3":
                PS3Count += gCount;
                break;
        }
        return gCount;
    }

    void SortArrays(int[] aryTscore, int[] grScore)
    {
        int x = aryTscore.Length;
        int i;
        int j;
        int temp1, temp2;

        for (i = (x - 1); i >= 0; i--)
        {
            for (j = 1; j <= i; j++)
            {
                if (aryTscore[j - 1] > aryTscore[j])
                {
                    temp1 = aryTscore[j - 1];
                    aryTscore[j - 1] = aryTscore[j];
                    aryTscore[j] = temp1;
                    temp2 = grScore[j - 1];
                    grScore[j - 1] = grScore[j];
                    grScore[j] = temp2;
                }
            }
        }


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        GetMins();
        DataTable dt2 = new DataTable();
        DataRow dr;
        dt2.Columns.Add("Contest", typeof(string));
        dt2.Columns.Add("Grade", typeof(int));
        dt2.Columns.Add("MinScore", typeof(int));
        dt2.Columns.Add("NwScore", typeof(int));
        dt2.Columns.Add("MaxScore", typeof(int));
        dt2.Columns.Add("Rank1Score", typeof(int));

        for (int i=0;i<dt.Rows.Count;i++)
            switch (dt.Rows[i].ItemArray[0].ToString().Trim())
            {
                case "JSB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JSBRank1Score)
                    JSBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "SSB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SSBRank1Score)
                    SSBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "JVB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JVBRank1Score)
                    JVBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "IVB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < IVBRank1Score)
                    IVBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "SVB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SVBRank1Score)
                    SVBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "JGB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JGBRank1Score)
                    JGBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "SGB":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SGBRank1Score)
                        SGBRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "MB1":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB1Rank1Score)
                        MB1Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "MB2":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB2Rank1Score)
                        MB2Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "MB3":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB3Rank1Score)
                        MB3Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "MB4":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < MB4Rank1Score)
                        MB4Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "EW1":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW1Rank1Score)
                        EW1Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "EW2":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW2Rank1Score)
                        EW2Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "EW3":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < EW3Rank1Score)
                        EW3Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "PS1":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS1Rank1Score)
                        PS1Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "PS2":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS2Rank1Score)
                        PS2Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "PS3":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < PS3Rank1Score)
                        PS3Rank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "JSC":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < JSCRank1Score)
                        JSCRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "ISC":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < ISCRank1Score)
                        ISCRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
                case "SSC":
                    if (Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text) < SSCRank1Score)
                        SSCRank1Score = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
                    break;
            }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            dr = dt2.NewRow();
            dr["Contest"] = dt.Rows[i].ItemArray[0].ToString();
            dr["Grade"] = Convert.ToInt32(dt.Rows[i].ItemArray[1]);
            dr["MinScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("TextBox2")).Text);
            dr["NwScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtNewChScore")).Text);
            dr["MaxScore"] = Convert.ToInt32(((TextBox)gvCriteria.Rows[i].Cells[2].FindControl("txtMaxScore")).Text);
            switch (dt.Rows[i].ItemArray[0].ToString().Trim())
            {
                case "JSB":
                    dr["Rank1Score"] = JSBRank1Score;
                    break;
                case "SSB":
                    dr["Rank1Score"] = SSBRank1Score;
                    break;
                case "JVB":
                    dr["Rank1Score"] = JVBRank1Score;
                    break;
                case "IVB":
                    dr["Rank1Score"] = IVBRank1Score;
                    break;
                case "SVB":
                    dr["Rank1Score"] = SVBRank1Score;
                    break;
                case "JGB":
                    dr["Rank1Score"] = JGBRank1Score;
                    break;
                case "SGB":
                    dr["Rank1Score"] = SGBRank1Score;
                    break;
                case "MB1":
                    dr["Rank1Score"] = MB1Rank1Score;
                    break;
                case "MB2":
                    dr["Rank1Score"] = MB2Rank1Score;
                    break;
                case "MB3":
                    dr["Rank1Score"] = MB3Rank1Score;
                    break;
                case "MB4":
                    dr["Rank1Score"] = MB4Rank1Score;
                    break;
                case "EW1":
                    dr["Rank1Score"] = EW1Rank1Score;
                    break;
                case "EW2":
                    dr["Rank1Score"] = EW2Rank1Score;
                    break;
                case "EW3":
                    dr["Rank1Score"] = EW3Rank1Score;
                    break;
                case "PS1":
                    dr["Rank1Score"] = PS1Rank1Score;
                    break;
                case "PS2":
                    dr["Rank1Score"] = PS2Rank1Score;
                    break;
                case "PS3":
                    dr["Rank1Score"] = PS3Rank1Score;
                    break;
                case "JSC":
                    dr["Rank1Score"] = JSCRank1Score;
                    break;
                case "ISC":
                    dr["Rank1Score"] = ISCRank1Score;
                    break;
                case "SSC":
                    dr["Rank1Score"] = SSCRank1Score;
                    break;
            }
            dt2.Rows.Add(dr);
        }
        SaveToDB(dt2);
        bool result = ReadDataBase();
        btnRecalc.Visible = true;
    }
}
