﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using NativeExcel;
using System.Drawing;

public partial class xlReportRegByOnLineWorkshop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //Session["LoginID"] = 22214;
                //Session["RoleId"] = 1;
                if (Session["LoginID"] == null)
                {
                    Response.Redirect("~/Maintest.aspx");
                }

                if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
                {
                    Response.Redirect("~/login.aspx?entry=p");
                }
                if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97 && Convert.ToInt32(Session["RoleId"]) != 96)
                {
                    Response.Redirect("~/VolunteerFunctions.aspx");
                }

                int MaxYear = DateTime.Now.Year;
                //ArrayList list = new ArrayList();

                for (int i = MaxYear; i >= (DateTime.Now.Year - 3); i--)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }


                FillProductGroup();

                FillProduct();
                fillGVwsRegReport();
            }



        }
        catch (Exception ex)
        {
            //Response.Write("Err :" + ex.ToString());
        }
    }
    private void FillProductGroup()
    {
        //ddlProductGroup
        string ddlproductgroupqry;

        //if (Convert.ToInt32(Session["RoleId"]) == 89)
        //{
        //    ddlproductgroupqry = "select distinct p.Name,v.ProductGroupID,v.ProductGroupCode from volunteer v inner join ProductGroup p on v.ProductGroupID=p.ProductGroupID and  v.Eventid=20  where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and v.ProductId is not Null  Order by v.ProductGroupId";
        //}
        //else
        //{
        ddlproductgroupqry = "select  distinct p.ProductGroupCode,p.ProductGroupID,p.Name from  ProductGroup p inner join OnlineWsCal OW on (p.ProductgroupId=ow.productgroupid)  where p.eventid=20 and p.ProductGroupId is not Null and ow.EventYear=" + ddlYear.SelectedValue + " Order by p.ProductGroupId";
        // ddlproductgroupqry = "select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" + ddlEvent.SelectedValue + " and ProductId is not Null Order by ProductGroupId";
        //}
        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductgroupqry);

        ddlProductGroup.DataSource = dsstate;
        //ddlProductGroup.DataTextField = "Name";
        ddlProductGroup.DataValueField = "ProductGroupID";
        ddlProductGroup.DataBind();
        ddlProductGroup.Items.Insert(0, new ListItem("Select ProductGroup", "-1"));



    }
    private void FillProduct()
    {
        string ddlproductqry;
        try
        {
            //if (Convert.ToInt32(Session["RoleId"]) == 89)
            //{
            //    ddlproductqry = "select distinct p.Name,v.ProductID,v.ProductCode from CalSignup c inner join volunteer v on c.ProductID=v.ProductID inner join Product p on v.ProductID=p.ProductID where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and v.ProductGroupId=" + ddlProductGroup.SelectedValue + " and v.ProductId is not Null  Order by v.ProductId";
            //}
            //else
            //{
            ddlproductqry = "select  distinct p.ProductID,p.ProductCode,p.Name from  Product p inner join OnlineWsCal Ow on (p.ProductId=Ow.ProductId)  where p.eventid=20 and p.ProductID is not Null and Ow.EventYear=" + ddlYear.SelectedValue + "";
            if (ddlProductGroup.SelectedValue != "-1")
            {
                ddlproductqry += " and p.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }
            ddlproductqry += " Order by p.ProductID";
            //}
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            DataSet myDataSet = new DataSet();
            ddlProduct.DataSource = dsstate;
            //ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
            ddlProduct.Enabled = true;
            if (dsstate.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 1;
                ddlProduct.Enabled = false;
                FillWorkshopDate();
            }

        }
        catch (Exception e)
        { }
    }
    protected void FillWorkshopDate()
    {
        //string ddlproductqry;
        try
        {

            string ddlworkshopDateqry;
            ddlworkshopDateqry = "select distinct Convert(nvarchar(10),EventDate, 101) as EventDate from Registration_OnlineWkshop where ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and EventYear=" + ddlYear.SelectedValue + "";

            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlworkshopDateqry);
            DataSet myDataSet = new DataSet();
            ddlworkshopDate.DataSource = dsstate;
            //ddlProduct.DataTextField = "Name";
            ddlworkshopDate.DataValueField = "EventDate";
            ddlworkshopDate.DataBind();
            ddlworkshopDate.Items.Insert(0, new ListItem("Select Workshop Date", "-1"));
            ddlworkshopDate.Enabled = true;
            if (dsstate.Tables[0].Rows.Count == 1)
            {
                ddlworkshopDate.SelectedIndex = 1;
                ddlworkshopDate.Enabled = false;
            }

        }
        catch (Exception e)
        { }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlworkshopDate.Items.Clear();
        FillProduct();
        fillGVwsRegReport();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillProductGroup();
        FillProduct();
        fillGVwsRegReport();
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cmdText = " select ProductgroupId from product where productId=" + ddlProduct.SelectedValue + "";
        string productGroupid = string.Empty;
        try
        {
            productGroupid = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText).ToString();
            ddlProductGroup.SelectedValue = productGroupid;
        }
        catch
        {
        }

        FillWorkshopDate();
        fillGVwsRegReport();
    }
    protected void ddlworkshopDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillGVwsRegReport();
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        //if (ddlProductGroup.SelectedIndex == 0)
        //{
        //    lblErr.Text = "Select ProductGroup";

        //    gvOnlineWorkshopRegReport.Visible = false;

        //}
        //else if (ddlProduct.SelectedIndex == 0)
        //{
        //    lblErr.Text = "Select Product";

        //    gvOnlineWorkshopRegReport.Visible = false;
        //}
        ////else if(ddlworkshopDate.SelectedIndex==0)
        ////{
        ////    lblErr.Text = "Select Workshop Date";

        ////    gvOnlineWorkshopRegReport.Visible = false;
        ////}
        //else
        //{
        lblErr.Text = "";
        fillGVwsRegReport();
        //}
    }
    protected void fillGVwsRegReport()
    {
        try
        {
            string fillqry = "select Convert(nvarchar(10),isnull(r.paymentdate,r.CreateDate), 101) as RegistrationDate,Convert(nvarchar(10),ows.Date, 101) as WkshopDate,ows.ProductCode as Wkshop,c.last_name as ChildLastName,";
            fillqry = fillqry + "c.first_name as ChildFirstName,c.grade as Grade,Convert(nvarchar(10),c.date_of_birth, 101) as DOB,c.gender as GenderChild,i.cphone as CPhoneFather,";
            fillqry = fillqry + "i.hphone as HomePhone,i.lastName as LastNameFather,i.firstName as FirstNameFather,i.email as EmailFather,";
            fillqry = fillqry + "case r.Approved when 'Y' then 'Paid' else 'Not Paid' end as Status,c.SchoolName as SchoolName,r.productid as ProductID,";
            fillqry = fillqry + "isp.lastName as LastNameMother,isp.firstName as FirstNameMother,isp.Email as EmailMother,isp.cphone as CPhoneMother,";
            fillqry = fillqry + "p.Name as WkshopName, r.RegID as RegID,c.ChildNumber as ChildNumber,r.MemberId as MemberID,i.firstName+' '+i.lastName as Name,i.Address1 as Street,i.City+', '+i.State+' '+i.zip as CityLine,i.City,i.State,i.Zip, r.Approved, r.JoinURL from Registration_OnlineWkshop r inner join OnlineWSCal ows on ows.OnlineWSCalID=r.OnlineWSCalId ";
            fillqry = fillqry + "inner join Indspouse i on r.Memberid=i.AutoMemberid left join Indspouse isp on isp.relationship=i.automemberid ";
            fillqry = fillqry + "inner join Child c on (c.memberid=i.automemberid and c.ChildNumber=r.ChildNumber) inner join product p on p.ProductId=r.productid where r.EventYear=" + ddlYear.SelectedValue + "";

            if (ddlProductGroup.SelectedValue != "-1")
            {
                fillqry += " and r.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "-1")
            {
                fillqry += " and r.ProductId=" + ddlProduct.SelectedValue + "";
            }

            if (ddlworkshopDate.SelectedValue != "-1" && ddlworkshopDate.Items.Count > 0)
            {
                fillqry = fillqry + "   and r.EventDate='" + ddlworkshopDate.SelectedValue + "'";
            }
            if (DDLStatus.SelectedValue == "Paid")
            {
                fillqry = fillqry + "   and r.Approved='Y'";
            }
            else if (DDLStatus.SelectedValue == "Pending")
            {
                fillqry = fillqry + "   and r.Approved is null";
            }

            fillqry = fillqry + " order by Convert(nvarchar(10),isnull(r.paymentdate,r.CreateDate), 101) desc, C.Last_Name, C.First_Name ASC";
            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvOnlineWorkshopRegReport.DataSource = ds;
                    gvOnlineWorkshopRegReport.DataBind();
                    gvOnlineWorkshopRegReport.Visible = true;
                    excel.Enabled = true;
                    Session["Sessionno"] = ds.Tables[0];
                    dvCount.Visible = true;
                    lblNorecord.Visible = false;

                    for (int i = 0; i < gvOnlineWorkshopRegReport.Rows.Count; i++)
                    {
                        if (gvOnlineWorkshopRegReport.Rows[i].Cells[26].Text.Replace("&nbsp;", "") == "")
                        {
                            ((Button)gvOnlineWorkshopRegReport.Rows[i].FindControl("btnApproved") as Button).Visible = true;
                        }
                        else
                        {
                            ((Button)gvOnlineWorkshopRegReport.Rows[i].FindControl("btnApproved") as Button).Visible = false;
                        }
                    }
                }
                else
                {
                    gvOnlineWorkshopRegReport.DataSource = ds;
                    gvOnlineWorkshopRegReport.DataBind();
                    dvCount.Visible = false;
                    lblNorecord.Visible = true;
                }
            }
            else
            {
                dvCount.Visible = false;
            }



            fillqry = " select count(r.RegID) as TotCount from Registration_OnlineWkshop r inner join OnlineWSCal ows on ows.OnlineWSCalID=r.OnlineWSCalId ";
            fillqry = fillqry + "inner join Indspouse i on r.Memberid=i.AutoMemberid left join Indspouse isp on isp.relationship=i.automemberid ";
            fillqry = fillqry + "inner join Child c on (c.memberid=i.automemberid and c.ChildNumber=r.ChildNumber) inner join product p on p.ProductId=r.productid where r.EventYear=" + ddlYear.SelectedValue + "";

            if (ddlProductGroup.SelectedValue != "-1")
            {
                fillqry += " and r.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "-1")
            {
                fillqry += " and r.ProductId=" + ddlProduct.SelectedValue + "";
            }

            if (ddlworkshopDate.SelectedValue != "-1" && ddlworkshopDate.Items.Count > 0)
            {
                fillqry = fillqry + "   and r.EventDate='" + ddlworkshopDate.SelectedValue + "'";
            }
            if (DDLStatus.SelectedValue == "Paid")
            {
                fillqry = fillqry + "   and r.Approved='Y'";
            }
            else if (DDLStatus.SelectedValue == "Pending")
            {
                fillqry = fillqry + "   and r.Approved is null";
            }
            DataSet ds1 = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables.Count > 0)
            {
                spanTotRegCount.InnerText = ds1.Tables[0].Rows[0]["TotCount"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void excel_Click(object sender, EventArgs e)
    {
        // ExportToExcel();
        ExportToExcelNew();
    }

    public void ExportToExcelNew()
    {
        try
        {
            string fillqry = "select Convert(nvarchar(10),isnull(r.paymentdate,r.CreateDate), 101) as RegistrationDate,Convert(nvarchar(10),ows.Date, 101) as WkshopDate,ows.ProductCode as Wkshop,c.last_name as ChildLastName,";
            fillqry = fillqry + "c.first_name as ChildFirstName,c.grade as Grade,Convert(nvarchar(10),c.date_of_birth, 101) as DOB,c.gender as GenderChild,i.cphone as CPhoneFather,";
            fillqry = fillqry + "i.hphone as HomePhone,i.lastName as LastNameFather,i.firstName as FirstNameFather,i.email as EmailFather,";
            fillqry = fillqry + "case r.Approved when 'Y' then 'Paid' else 'Not Paid' end as Status,c.SchoolName as SchoolName,r.productid as ProductID,";
            fillqry = fillqry + "isp.lastName as LastNameMother,isp.firstName as FirstNameMother,isp.Email as EmailMother,isp.cphone as CPhoneMother,";
            fillqry = fillqry + "p.Name as WkshopName, r.RegID as RegID,c.ChildNumber as ChildNumber,r.MemberId as MemberID,i.firstName+' '+i.lastName as Name,i.Address1 as Street,i.City+', '+i.State+' '+i.zip as CityLine,i.City,i.State,i.Zip from Registration_OnlineWkshop r inner join OnlineWSCal ows on ows.OnlineWSCalID=r.OnlineWSCalId ";
            fillqry = fillqry + "inner join Indspouse i on r.Memberid=i.AutoMemberid left join Indspouse isp on isp.relationship=i.automemberid ";
            fillqry = fillqry + "inner join Child c on (c.memberid=i.automemberid and c.ChildNumber=r.ChildNumber) inner join product p on p.ProductId=r.productid where r.EventYear=" + ddlYear.SelectedValue + "";

            if (ddlProductGroup.SelectedValue != "-1")
            {
                fillqry += " and r.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "-1")
            {
                fillqry += " and r.ProductId=" + ddlProduct.SelectedValue + "";
            }

            if (ddlworkshopDate.SelectedValue != "-1" && ddlworkshopDate.Items.Count > 0)
            {
                fillqry = fillqry + "   and r.EventDate='" + ddlworkshopDate.SelectedValue + "'";
            }
            if (DDLStatus.SelectedValue == "Paid")
            {
                fillqry = fillqry + "   and r.Approved='Y'";
            }
            else if (DDLStatus.SelectedValue == "Pending")
            {
                fillqry = fillqry + "   and r.Approved is null";
            }

            fillqry = fillqry + " order by Convert(nvarchar(10),isnull(r.paymentdate,r.CreateDate), 101) desc, C.Last_Name, C.First_Name ASC";
            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);

            IWorkbook xlWorkBook = NativeExcel.Factory.CreateWorkbook();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorksheet Sheet1 = xlWorkBook.Worksheets.Add();
                    Sheet1.Name = ds.Tables[0].TableName;
                    Sheet1.Range["A1:F1"].MergeCells = true;
                    Sheet1.Range["A1"].Value = "Online Workshop Report";
                    Sheet1.Range["A1"].Font.Bold = true;

                    Sheet1.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;

                    int ColCount = ds.Tables[0].Rows.Count;
                    int i = 1;
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        Sheet1.Range[2, i].Value = dc.ColumnName;

                        Sheet1.Range[2, i].Font.Bold = true;
                        i = i + 1;
                    }

                    int j;
                    i = 3;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        for (j = 0; j < ds.Tables[0].Columns.Count - 1; j++)
                        {
                            Sheet1.Range[i, j + 1].Value = dr[j].ToString();
                        }
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "OnlineWorkshop_Report" + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    xlWorkBook.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();

                }

            }


        }
        catch (Exception ex)
        {

        }
    }

    protected void ExportToExcel()
    {
        try
        {
            DataTable dtWorkshop = (DataTable)Session["Sessionno"];

            Response.Clear();
            Response.AppendHeader("content-disposition", "attachment;filename=OnlineWorkshop_Report.xls");
            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtWorkshop.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Online Workshop Registration Report </td></tr>");
            //Response.Write("<tr style='height:35px'><td><b>Chapter :</b></td><td colspan=" + (dtWorkshop.Columns.Count - 2) + "> " + lblChapter.Text + "</td></tr>");


            //if (dtWorkshop != null)
            //{

            //    Response.Write("<tr><td colspan='4' style='font-weight:bold;text-align:left;'>Organization Name : </td></tr>");
            //    foreach (DataRow dr in dtWorkshop.Rows)
            //    {
            //        Response.Write("<tr><td colspan='4' style='text-align:center;'>" + dr[3].ToString() + " - " + dr[0].ToString() + "</td></tr>");
            //    }

            //}

            Response.Write("<tr>");
            foreach (DataColumn dc in dtWorkshop.Columns)
            {

                Response.Write("<th>" + dc.ColumnName + "</th>");
            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtWorkshop.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtWorkshop.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();

        }

        catch (Exception ex)
        {
        }
    }
    protected void DDLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillGVwsRegReport();
    }
    protected void gvOnlineWorkshopRegReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Approve")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                gvOnlineWorkshopRegReport.Rows[row.RowIndex].BackColor = Color.FromName("#EAEAEA");
                trApprove.Visible = true;
                hdnRegId.Value = ((Label)gvOnlineWorkshopRegReport.Rows[row.RowIndex].FindControl("lblRegID") as Label).Text;
            }
        }
        catch
        {
        }
    }

    protected void BtnApprove_Click(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showConfirmation();", true);

    }
    protected void btnConfApprove_Click(object sender, EventArgs e)
    {
        try
        {
            string Cmdtext = " Update Registration_onlineWkShop set Approved='Y' where RegId=" + hdnRegId.Value + "";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            lblErr.Text = "Approved successfully.";
            lblErr.ForeColor = Color.Green;
            trApprove.Visible = false;
            fillGVwsRegReport();
        }
        catch
        {
        }
    }
}