Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Data


Namespace VRegistration


    Partial Class SATtestStatus
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" Then
                Response.Write("../maintest.aspx")
            End If
             If Page.IsPostBack = False Then
            Dim TableStr As String = ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select CT.ChildNumber,Ch.First_Name+' '+Ch.Last_Name as Name from ChildTestAnswers CT INNER JOIN Child ch ON Ch.ChildNumber=CT.ChildNumber INNER JOIN IndSpouse I ON I.AutoMemberID=Ch.MemberID AND I.AutoMemberID=" & Session("CustIndID") & " Group By CT.ChildNumber,CT.ChildNumber,Ch.First_Name+' '+Ch.Last_Name Having COUNT(CT.ChildTestAnswersID)>124")
            Dim flag As Boolean = False
            Try
                If ds.Tables(0).Rows.Count > 0 Then
                    flag = True
                    ddlChildNumber.DataSource = ds
                    ddlChildNumber.DataBind()
                    GetAnswers()
                Else
                    ddlChildNumber.Visible = False
                End If
            Catch ex As Exception

            End Try
            If flag = False Then
                div1.Visible = True
                lblErr.Text = "No record to show"
            End If
            End If
        End Sub

        Protected Sub ddlChildNumber_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            GetAnswers()
        End Sub

        Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        End Sub

        Private Sub GetAnswers()


            Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center> Writing Skills</td><td align=Center> Critical Reading </td><td align=Center> Math </td></tr>"
            Dim flag As Boolean = False
            Dim reader As SqlDataReader
            Try
                reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT WSScaledScore,CRScaledScore,MathScaledScore   FROM ChildTestSummary  WHERE ChildNumber =" & ddlChildNumber.SelectedValue & "  AND Year=2007")
                While reader.Read()
                    flag = True
                    TableStr = TableStr & "<tr><td align=Center>" & reader("WSScaledScore") & "</td><td align=Center>" & reader("CRScaledScore") & "</td><td align=Center>" & reader("MathScaledScore") & "</td></TR>"
                End While
                TableStr = TableStr & "</table>"
                reader.Close()
            Catch ex As Exception

            End Try
            If flag = False Then
                TableStr = ""
            End If
            lbl.Text = TableStr
            GVAnswers.Visible = True
            BtnExport.Visible = True
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select CT.QuestionNumber,CT.Answer,CT.TestSectionID from ChildTestAnswers CT  WHERE CT.ChildNumber=" & ddlChildNumber.SelectedValue)
            GVAnswers.DataSource = ds
            GVAnswers.DataBind()
        End Sub


        Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=SATTestAnswers_" & ddlChildNumber.SelectedItem.Text & ".xls")
            Response.Charset = ""
            ' If you want the option to open the Excel file without saving then comment out the line below
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            'Dim dgexport As New GridView
            'loadgrid(dgexport)
            GVAnswers.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        End Sub

    End Class
   
End Namespace

