﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Reports_TopRanksListFinals
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Response.Redirect("../login.aspx?entry=" & Session("entryToken"))
        End If
        ' lblmessage.Text = ""
        'lblselected.Text = ""
        Dim roleid As Integer = Session("RoleID")
        'User code to initialize the page here
        'only Roleid = 1, 2, 3, 4, 5 Can access this page
        If Not Page.IsPostBack Then
            Session("Productid") = ""
            ' Load Year into ListBox
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year) + 1
            Dim YearCount As Integer = year - 2006
            For i As Integer = 0 To YearCount
                ddlYear.Items.Insert(i, Convert.ToString(year - i))
            Next
            ddlYear.Items.Insert(0, "ALL")
            ddlYear.Items(2).Selected = True
            Session("Year") = ddlYear.Items(1).Text
            LoadProductGroup()   ' method to  Load Product group ListBox
            LoadProductID()      ' method to  Load Product ID ListBox
            lblChapter.Text = (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ChapterCode from Chapter Where ChapterID=" & Session("loginChapterID"))).ToString.Replace(",", "_")
            If (Convert.ToInt32(Session("loginChapterID").ToString()) = 1) Then
                chkAllChapter.Checked = True
                chkAllChapter.Enabled = False
            End If
            If (Convert.ToInt32(Session("loginChapterID").ToString()) <> 1) Then
                chkAllChapter.Visible = False

            End If
        End If
    End Sub

    Private Sub LoadProductGroup()
        Dim strSql As String '= "Select ProductGroupID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
        'If Session("LoginChapterID") = "1" Then
        strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
        'Session("LoginChapterID") = "1"
        ' Else
        '    strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear = " & ddlYear.SelectedValue & " and c.RegionalStatus = 'Active' and P.EventId =2  order by P.ProductGroupID"
        ' End If
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()
        lstProductGroup.Items.Insert(0, "All")
        lstProductGroup.Items(0).Selected = True
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim eventid As String
        'If Session("LoginChapterID") = "1" Then
        eventid = 1
        ' Else
        ' eventid = 2
        ' End If
        Dim productgroup As String = ""
        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            lstProductid.Enabled = False
            Dim sbvalues As New StringBuilder
            Dim strSql1 As String '= "Select ProductGroupID as ID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
            ' strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            'If eventid = "1" Then
            strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
            ' ElseIf eventid = "2" Then
            '  strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear =" & ddlYear.SelectedValue & " and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            ' End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If

        Dim strSql As String '= "Select ProductID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        strSql = "Select ProductCode, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductid.DataSource = drproductid
        lstProductid.DataBind()
        lstProductid.Items.Insert(0, "All")
        lstProductid.Items(0).Selected = True
    End Sub
    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productid") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub btnShowList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowList.Click
        Dim wherecntn As String = ""
        Session("ChapterId") = Session("LoginChapterID")

        If (chkAllChapter.Checked) Then
            Dim strSQLChapter As String, sbChapterId As New StringBuilder
            strSQLChapter = "Select Chapterid from Chapter"
            Dim dsChapter As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQLChapter)
            If dsChapter.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsChapter.Tables(0).Rows.Count - 1
                    sbChapterId.Append("'")
                    sbChapterId.Append(dsChapter.Tables(0).Rows(i).Item("ChapterId").ToString)
                    sbChapterId.Append("'")
                    If i < dsChapter.Tables(0).Rows.Count - 1 Then
                        sbChapterId.Append(",")
                    End If
                Next
                Session("ChapterId") = sbChapterId.ToString
            End If
        End If

        If Not Session("ProductGroupId") = "" Then
            If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item in ProductId is ALL
                Dim sbvalues As New StringBuilder
                Dim eventid, StrSQL1 As String
                'If Session("LoginChapterID") = "1" Then
                eventid = 1
                'Else
                '    eventid = 2
                'End If
                StrSQL1 = "Select ProductCode as ID, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL1)
                If ds1.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds1.Tables(0).Rows.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(ds1.Tables(0).Rows(i).Item("ID").ToString)
                        sbvalues.Append("'")
                        If i < ds1.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Productid") = sbvalues.ToString
                End If
            End If
            wherecntn = " and a.Productcode in (" & Session("Productid") & ")"
        End If
        Dim StrSQl As String = "select b.ChapterID,b.Chapter as Chapter,c.ChildNumber, isnull(c.Schoolname,'') as SchoolName, isnull(c.Acheivements,'') as Achievements, isnull(c.Hobbies,'') as Hobbies, "

        If chkIncludeAchievements.Checked Then
            GVList.Columns(7).Visible = True
            GVList.Columns(8).Visible = True
            GVList.Columns(9).Visible = True
        Else
            GVList.Columns(7).Visible = False
            GVList.Columns(8).Visible = False
            GVList.Columns(9).Visible = False
        End If

        'StrSQl = StrSQl & " a.contestYear, a.productcode, a.rank,c.Last_Name as CLName , c.First_Name as CFname, a.Grade ,a.parentid, "
        'StrSQl = StrSQl & " CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.email else b.email END as emailFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.lastname else b.lastname END as LastNameFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.firstname else b.firstname END as FirstNameFather, b.hphone as HPhoneFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.cphone END as CPhoneFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.wphone END as WPhoneFather,"
        'StrSQl = StrSQl & " CASE isnull(d.gender,'Female') WHEN 'Female' then d.email ELSE b.email END as emailMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.lastname ELSE b.lastname END as LastNameMother, CASE isnull(d.gender,'Female') WHEN 'Female' then d.firstname ELSE b.firstname END as FirstNameMother, d.hphone as HPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.CPhone ELSE b.CPhone  END as CPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.wPhone ELSE b.wPhone END as wPhoneMother ,b.address1,"
        'StrSQl = StrSQl & " b.address2, b.city, b.state, b.zip  from contestant a " 'c.SchoolName, c.Acheivements, c.Hobbies,
        'StrSQl = StrSQl & " Inner Join contestant cc on cc.childnumber=a.childnumber and cc.ProductCode=a.ProductCode and cc.ContestYear=a.ContestYear"
        'StrSQl = StrSQl & " Inner Join indspouse b on b.automemberid = a.parentid and b.Chapterid=cc.Chapterid"
        'StrSQl = StrSQl & " Inner Join child c on c.childnumber=a.childnumber"
        'StrSQl = StrSQl & " Left Join indspouse d on d.Relationship = a.Parentid "
        'StrSQl = StrSQl & " where a.contestyear in (" & GetSelYear() & ") and a.eventid in (1) and cc.chapterID in (" & Session("ChapterId") & ") and a.PaymentReference <>'' " & wherecntn & " and (a.rank between 1 and " & ddlTopCount.SelectedValue & ") order by a.productid,a.contestYear desc, a.rank asc"

        StrSQl = StrSQl & " a.contestYear, a.productcode, a.rank,c.Last_Name as CLName , c.First_Name as CFname, a.Grade ,a.parentid, "
        StrSQl = StrSQl & " CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.email else b.email END as emailFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.lastname else b.lastname END as LastNameFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN d.firstname else b.firstname END as FirstNameFather, b.hphone as HPhoneFather, CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.cphone END as CPhoneFather,CASE isnull(b.gender,'Male') WHEN 'Male' THEN b.wphone END as WPhoneFather,"
        StrSQl = StrSQl & " CASE isnull(d.gender,'Female') WHEN 'Female' then d.email ELSE b.email END as emailMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.lastname ELSE b.lastname END as LastNameMother, CASE isnull(d.gender,'Female') WHEN 'Female' then d.firstname ELSE b.firstname END as FirstNameMother, d.hphone as HPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.CPhone ELSE b.CPhone  END as CPhoneMother,CASE isnull(d.gender,'Female') WHEN 'Female' then d.wPhone ELSE b.wPhone END as wPhoneMother ,b.address1,"
        StrSQl = StrSQl & " b.address2, b.city, b.state, b.zip  from contestant a " 'c.SchoolName, c.Acheivements, c.Hobbies,
        StrSQl = StrSQl & " Inner Join indspouse b on b.automemberid = a.parentid"
        StrSQl = StrSQl & " Inner Join child c on c.childnumber=a.childnumber"
        StrSQl = StrSQl & " Left Join indspouse d on d.Relationship = a.Parentid "
        StrSQl = StrSQl & " where a.contestyear in (" & GetSelYear() & ") and a.eventid in(1) and b.chapterID in (" & Session("ChapterId") & ") and a.PaymentReference <>'' " & wherecntn & " and (a.rank between 1 and " & ddlTopCount.SelectedValue & ") order by a.productid,a.contestYear desc, a.rank asc"

        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                GVList.Visible = True
                GVList.DataSource = ds
                GVList.DataBind()
                'For i As Integer = 0 To GVList.Rows.Count - 1
                '    GVList.Rows(i).Visible = True
                'Next
                'Dim ContestYear, ContestYear_1 As Integer
                'Dim ChildNumber, ChildNumber_1 As Integer
                'Dim Rank, Rank_1 As Integer

                'For i As Integer = 0 To ds.Tables(0).Rows.Count - 2 'GVList.Rows.Count - 2 
                '    ContestYear = ds.Tables(0).Rows(i)("ContestYear") 'GVList.Rows(i).Cells(1).Text
                '    ChildNumber = ds.Tables(0).Rows(i)("ChildNumber") 'GVList.Rows(i).Cells(23).Text 
                '    Rank = ds.Tables(0).Rows(i)("Rank") 'GVList.Rows(i).Cells(3).Text 

                '    For j As Integer = i + 1 To ds.Tables(0).Rows.Count - 1 'GVList.Rows.Count - 1
                '        ContestYear_1 = ds.Tables(0).Rows(j)("ContestYear")
                '        ChildNumber_1 = ds.Tables(0).Rows(j)("ChildNumber")
                '        Rank_1 = ds.Tables(0).Rows(j)("Rank")

                '        If ContestYear_1 = ContestYear And i <> j Then
                '            If ChildNumber_1 = ChildNumber Then 'If Same Child
                '                If Rank_1 = Rank Then '' if equal Ranks 
                '                    GVList.Rows(j).Visible = False
                '                ElseIf Rank_1 > Rank Then
                '                    GVList.Rows(j).Visible = False
                '                Else
                '                    GVList.Rows(i).Visible = False
                '                End If
                '            End If '
                '        ElseIf ContestYear_1 <> ContestYear And i <> j Then
                '            If ChildNumber_1 = ChildNumber Then 'If Same Child
                '                If Rank_1 = Rank Then '' if equal Ranks
                '                    GVList.Rows(j).Visible = False
                '                ElseIf Rank_1 > Rank Then
                '                    GVList.Rows(j).Visible = False
                '                Else
                '                    GVList.Rows(i).Visible = False
                '                End If
                '            End If
                '        End If
                '    Next
                'Next

                GVList.Columns(23).Visible = False
                GVList.Columns(24).Visible = False
                GVList.Columns(25).Visible = False

                btnExport.Visible = True
                lblErr.Text = ""
            Else
                GVList.Visible = False
                btnExport.Visible = False
                lblErr.Text = "Sorry No Record Found "
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
            ' Response.Write(StrSQl)
        End Try
    End Sub

    Function GetSelYear() As String
        Dim i As Integer
        Dim st As String = ""
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To ddlYear.Items.Count - 1
            If ddlYear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = ddlYear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            st = sbvalues.ToString
        End If
        If ddlYear.SelectedIndex = 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 1 To ddlYear.Items.Count - 1
                sbvalues.Append(ddlYear.Items(a).ToString)
                If a < ddlYear.Items.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            st = sbvalues.ToString
        End If

        Return st
    End Function
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=TopRanksList_" & ddlYear.SelectedValue & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GVList.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class
