using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Net.Mail;

public partial class UploadPhotos : System.Web.UI.Page
{
    Int32 LoginUserRoleId = -1;
    Int32 CustIndID = -1;
    Int32 m_oRoleId1 = 41; 
    Int32 m_oRoleId2 = 1; 
    Int32 m_oRoleId3 = 2; 

    string m_SortDirection = string.Empty;
    int m_PageIndex = 0;

    # region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        // Make sure that parent has logged in
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (Session["RoleId"] != null)
            LoginUserRoleId = int.Parse(Session["RoleId"].ToString());

        // Override the roleid value for testing purpose only!
        if (Request["RoleId"] != null)
            LoginUserRoleId = int.Parse(Request["RoleId"].ToString());

        if (!IsPostBack)
        {
            if (Session["CustIndID"] != null)
            {
                CustIndID = int.Parse(Session["CustIndID"].ToString());
            }
            lblMemberId.Text = CustIndID.ToString();
            lblRoleId.Text = LoginUserRoleId.ToString();
            lblFlag.Text = "flase";
            if ((Request["managephotos"] != null) && (Request["managephotos"].ToString() == "1"))
            {
                if (LoginUserRoleId == m_oRoleId1 || LoginUserRoleId == m_oRoleId2 || LoginUserRoleId == m_oRoleId3)
                {
                    lblManagePhotos.Text = Request["managephotos"].ToString();
                    lblFlag.Text = "true";
                    CustIndID = -1;
                    //*********** FERDINE SILVA **************//
                    //GetContestants(CustIndID, true);
                    rdoFilter.Visible = true;
                    btnContinue.Visible = true;
                    int year = Convert.ToInt32(DateTime.Now.Year);
                    ddlyear.Items.Insert(0, new ListItem(Convert.ToString(year + 1)));
                    ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
                    ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year - 1)));
                    ddlyear.Items.Insert(3, new ListItem(Convert.ToString(year - 2)));
                    ddlyear.Items.Insert(4, new ListItem(Convert.ToString(year - 3)));
                    ddlyear.Items.Insert(5, new ListItem(Convert.ToString(year - 4)));
                    ddlyear.SelectedIndex = 1;
                    ddlyear.Visible = true;
                    btnExport.Visible = true;
                    btnFsdPhotos.Visible = true;

                }
                else
                {
                    lblMessage.Text = "You do not have permission to view this page. Please contact Administrator!";

                }
                hlinkParentRegistration.Visible = false;
                hlinkVolunteerHome.Visible = true;
            }
            else
            {
                GetContestants(CustIndID, true);
                hlinkParentRegistration.Visible = true;
                hlinkVolunteerHome.Visible = false;

            }
            ViewState.Add("CustIndID", CustIndID);
        }
        else
        {
            CustIndID = int.Parse(ViewState["CustIndID"].ToString());
        }
    }

   protected void gvContestantPhotos_RowCommand(object sender, GridViewCommandEventArgs e)
   {
        try
        {
            int index = -1;
            int memberid = -1;
            int ChildNumber = -1;
            int ContestantPhotoId = -1;
            StringBuilder sb = new StringBuilder();
            CustIndID = int.Parse(ViewState["CustIndID"].ToString());
                          
            if (e.CommandArgument != null)
            {
                if (int.TryParse(e.CommandArgument.ToString(), out index))
                {
                    index = int.Parse((string)e.CommandArgument);
                    memberid = int.Parse(gvContestantPhotos.Rows[index].Cells[20].Text);//Need to change the index if the order of columns is changed in .aspx
                    ChildNumber = int.Parse(gvContestantPhotos.Rows[index].Cells[21].Text);//Need to change the index if the order of columns is changed in .aspx
                    if (gvContestantPhotos.Rows[index].Cells[22].Text != "&nbsp;")//Need to change the index if the order of columns is changed in .aspx
                    {
                        ContestantPhotoId = int.Parse(gvContestantPhotos.Rows[index].Cells[22].Text);//Need to change the index if the order of columns is changed in .aspx
                    }
                    EntityContestantPhoto objECP = GetEntityContestantPhoto(memberid,ChildNumber,ContestantPhotoId);
                    DropDownList ddlReviewerStatus = (DropDownList)gvContestantPhotos.Rows[index].FindControl("ReviewerStatus");
                    DropDownList ddlTshirtSize = (DropDownList)gvContestantPhotos.Rows[index].FindControl("TShirtPhoto");
                    if (e.CommandName == "Upload")
                    {
                        FileUpload fu = (FileUpload)gvContestantPhotos.Rows[index].FindControl("FileUpload1");
                        if (fu.HasFile)
                        {
                            objECP.PhotoFileExtn = fu.FileName.Split(new char[] { '.' })[1].ToString();
                            objECP.PhotoFileSize = fu.FileBytes.Length;
                            objECP.CreateDate = System.DateTime.Now;
                            objECP.CreatedBy = int.Parse(Session["LoginID"].ToString());
                            objECP.TshirtSize = ddlTshirtSize.SelectedItem.Text;
                            objECP.PhotoFileName = GetPhotoFileName(objECP);

                            if (ValidatePhotoFile(objECP))
                            {
                                try
                                {
                                    ContestantPhoto_Insert(objECP);
                                    //SaveFile(ConfigurationSettings.AppSettings["ContestantPhotoPath"], fu, objECP.PhotoFileName);
                                    lblMessage.Text = "File Uploaded: " + fu.FileName;
                                    GetContestants(CustIndID, true);
                                }
                                catch (Exception err)
                                {
                                   lblMessage.Text =  err.Message;
                                }
                            }

                        }
                        else
                        {
                            lblMessage.Text = "No File Uploaded.";
                        }
                    }
                    else if (e.CommandName == "Download")
                    {
                        objECP.PhotoFileFolderPath = Server.MapPath(ConfigurationSettings.AppSettings["ContestantPhotoPath"]);
                        if (objECP.PhotoFileName != string.Empty)
                        {
                            objECP.ReviewDate = System.DateTime.Now;
                            objECP.ReviewerComments = objECP.Comments;
                            objECP.ReviewerStatus = "Downloaded";
                            ContestantPhoto_Update(objECP);
                            GetContestants(CustIndID, true);
                            DownloadFile(objECP);
                        }
                        else
                        {
                            lblMessage.Text = "No Photo File Exist!";
                        }
                    }
                    else if (e.CommandName == "ChangeStatus")
                    {
                        TextBox tbxComments = (TextBox)gvContestantPhotos.Rows[index].FindControl("ReviewerComments");
                        objECP.ReviewerComments = tbxComments.Text;
                        objECP.ReviewerStatus = ddlReviewerStatus.SelectedItem.Value;
                        objECP.ReviewerId = int.Parse(Session["LoginID"].ToString());
                        objECP.ReviewDate = System.DateTime.Now;
                        ContestantPhoto_Update(objECP);
                        GetContestants(CustIndID, true);
                        lblMessage.Text = "Status Changed Successfully!";

                        sb.AppendFormat("Child Name: {0} {1}", objECP.FirstName, objECP.LastName);
                        sb.Append("<br/>");
                        sb.AppendFormat("Child Number: {0}", objECP.ChildNumber);
                        sb.Append("<br/>");
                        sb.AppendFormat("Photo status: {0}", objECP.ReviewerStatus);
                        sb.Append("<br/>");
                        sb.AppendFormat("Review Comments: {0}", objECP.ReviewerComments);
                        sb.Append("<br/>");

                        if (objECP.IndEmail != string.Empty)
                        {
                            // Comment the following lines of code if we do not need to send automatic email to the user
                            SendEmail("NSF BeeBook Photo Review Status", sb.ToString(), objECP.IndEmail);
                            lblMessage.Text += string.Format("<br>Confirmation email sent to {0}", objECP.IndEmail);
                        }

                        if (objECP.SpouseEmail != string.Empty)
                        {
                            // Comment the following lines of code if we do not need to send automatic email to the user
                            SendEmail("NSF BeeBook Photo Review Status", sb.ToString(), objECP.SpouseEmail);
                            lblMessage.Text += string.Format("<br>Confirmation email sent to {0}", objECP.SpouseEmail);
                        }
                    }
                }
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = string.Concat("Error: ",err.Message);
        }
    }

   public Boolean uploadEnable(int ChildNumber)
   {
       Boolean flag = false;
       SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
       //String Result = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select CASE WHEN COUNT(CS.Contestant_ID)> 0 then 'True' Else 'False' END from Contestant CS INNER JOIN contest C ON C.ContestID = CS.ContestID LEFT JOIN ExContestant Ex On Ex.ChapterID=C.NSFChapterID and Ex.ProductID=C.ProductId and Ex.ProductGroupID=C.ProductGroupId and Ex.ContestYear=C.Contest_Year and Ex.EventID=C.EventId and Ex.ChildNumber=CS.ChildNumber where DATEDIFF(d,Case when Ex.NewDeadline is Null then C.RegistrationDeadline Else Ex.NewDeadline end,Getdate()) < 11 and CS.EventID=1 and CS.contestyear=year(GETDate()) and CS.ChildNUmber = " + ChildNumber.ToString() + ""));
       String Result = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select CASE WHEN COUNT(CS.Contestant_ID)> 0 then 'True' Else 'False' END from Contestant CS INNER JOIN contest C ON C.ContestID = CS.ContestID Inner Join WeekCalendar W ON (C.ContestDate = W.SatDay1 or C.ContestDate = w.SunDay2) and W.EventID = 1  where DATEDIFF(d,W.PhotoDeadline,Getdate()) < 1 and CS.EventID=1 and CS.contestyear=year(GETDate()) and CS.ChildNUmber = " + ChildNumber.ToString() + ""));
       if (Result == "True")
           flag = true;
       return flag;
   } 

    protected void gvContestantPhotos_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FileUpload fu = (FileUpload)e.Row.FindControl("FileUpload1");
            Button oUploadButton = (Button)e.Row.Cells[19].Controls[0];//Need to change the index if the order of columns is changed in .aspx
            Button oDownloadButton = (Button)e.Row.Cells[2].Controls[0];//Need to change the index if the order of columns is changed in .aspx
            HyperLink olnkFullPhoto = (HyperLink)e.Row.FindControl("lnkFullPhoto"); //cell 6
            Image oImage = (Image)e.Row.FindControl("ChildImage"); //cell 6
            TextBox tbxComments = (TextBox)e.Row.FindControl("ReviewerComments");
            DropDownList ddlReviewerStatus = (DropDownList)e.Row.FindControl("ReviewerStatus");
            DropDownList ddlTshirtSize = (DropDownList)e.Row.FindControl("TShirtPhoto");
            //DataBinder.Eval(e.Row.DataItem, "ToCheckNotes")
            ddlTshirtSize.SelectedIndex = ddlTshirtSize.Items.IndexOf(ddlTshirtSize.Items.FindByText(DataBinder.Eval(e.Row.DataItem, "TshirtSize").ToString()));
            Button oChangeStatusButton = (Button)e.Row.Cells[6].Controls[0];//Need to change the index if the order of columns is changed in .aspx
            System.Web.UI.WebControls.CheckBox unwillingCheckbox = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("Unwilling");
            oUploadButton.Enabled = true;
            fu.Enabled = true;

            string ContestantPhotoName = e.Row.Cells[13].Text; //Need to change the index if the order of columns is changed in .aspx
            string ReviewStatus = e.Row.Cells[14].Text; //Need to change the index if the order of columns is changed in .aspx
            string PreviousComments = e.Row.Cells[15].Text; //Need to change the index if the order of columns is changed in .aspx
            // Show the status as Approved in the dropdownlist, if it is downloaded.
          
            if (ReviewStatus == "Downloaded")
                ReviewStatus = "Approved";
            else if (ReviewStatus == "Unwilling")
            {
                unwillingCheckbox.Checked = true;
                oUploadButton.Enabled = false;
                fu.Enabled = false;
            }
            else if (ReviewStatus == string.Empty)
                ReviewStatus = "Submitted";

            // Check if Photo name is available
            if (ContestantPhotoName != "&nbsp;")
            {
                // Photo available. Create contestant photo link and display the thumbnail
                StringBuilder sb = new StringBuilder();
                sb.Append(ConfigurationSettings.AppSettings["ContestantPhotoPath"]);
                sb.Append(ContestantPhotoName);
                
                olnkFullPhoto.NavigateUrl = sb.ToString();

                oImage.ImageUrl = string.Format("MakeThumbnail.aspx?file={0}",sb.ToString());

                oDownloadButton.Visible = true;
                oImage.Visible = true;
                tbxComments.Visible = true;
                ddlReviewerStatus.Visible = true;
                oChangeStatusButton.Visible = true;
                unwillingCheckbox.Visible = false;
            }
            else
            {
                // Photo not available. Don't show photo image, download button, Review Comments textbox, Status dropdownlist, and change status button
                oDownloadButton.Visible = false;
                oImage.Visible = false;
                tbxComments.Visible = false;
                ddlReviewerStatus.Visible = false;
                oChangeStatusButton.Visible = false;

            }

            // Don't show upload button (to the parent) if the status is Approved or Downloaded
            if (ReviewStatus == "Approved" || ReviewStatus == "Downloaded")
            {
                oUploadButton.Visible = false;
                fu.Visible = false;
            }
            else
            {
                oUploadButton.Visible = true;
                fu.Visible = true;
            }
            int ChildNumber = int.Parse(e.Row.Cells[21].Text);
            if (uploadEnable(ChildNumber)!= true)
            {
                oUploadButton.Visible = false;
                fu.Visible = false;
                unwillingCheckbox.Visible = false;
            }
            // If the photo is already reviewed populate status ddl and comments textbox
            if (ReviewStatus != "&nbsp;")
            {
                //Response.Write("ReviewStatus**" + ReviewStatus+"**");
                try
                {
                    ddlReviewerStatus.Items.FindByText(ReviewStatus).Selected = true;
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
            }

            if (PreviousComments != "&nbsp;")
            {
                tbxComments.Text = PreviousComments;
            }
        }

    }
    protected void OnUnwillingStatusChanged(object sender, EventArgs e)
    {

        int memberid = -1;
        int ChildNumber = -1;
        int ContestantPhotoId = -1;
        try
        {
            if (IsPostBack)
            {
                System.Web.UI.WebControls.CheckBox selectedCheckbox = (System.Web.UI.WebControls.CheckBox)sender;
                GridViewRow selectedRow = (GridViewRow)selectedCheckbox.NamingContainer;
                DropDownList ddlTshirtSize = (DropDownList)selectedRow.FindControl("TShirtSize");
               // DropDownList ddlTshirtSize = (DropDownList)gvContestantPhotos.SelectedRow.FindControl("TShirtSize");

                memberid = int.Parse(selectedRow.Cells[20].Text);//Need to change the index if the order of columns is changed in .aspx
                ChildNumber = int.Parse(selectedRow.Cells[21].Text);//Need to change the index if the order of columns is changed in .aspx
                if (selectedRow.Cells[22].Text != "&nbsp;")//Need to change the index if the order of columns is changed in .aspx
                {
                    ContestantPhotoId = int.Parse(selectedRow.Cells[22].Text);//Need to change the index if the order of columns is changed in .aspx
                }
                EntityContestantPhoto objECP = GetEntityContestantPhoto(memberid, ChildNumber, ContestantPhotoId);
                if (selectedCheckbox.Checked)
                {
                    objECP.CreateDate = System.DateTime.Now;
                    objECP.CreatedBy = int.Parse(Session["LoginID"].ToString());
                    objECP.TshirtSize = ddlTshirtSize.SelectedItem.Text;// selectedRow.Cells[23].Text;
                    objECP.StatusFlag = "Unwilling";
                    ContestantPhoto_Insert(objECP);
                    GetContestants(CustIndID, true);
                }
                else if (LoginUserRoleId == -1)
                {
                    ContestantPhoto_Delete(objECP);
                    GetContestants(CustIndID, true);
                }
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }


    }
    //protected void rdoFilter_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    GetContestants(CustIndID, false);
    //}

    protected void gvContestantPhotos_Sorting(Object sender, GridViewSortEventArgs e)
    {

        m_SortDirection = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
        Response.Write(string.Format(" (Sort Direction:{0})", m_SortDirection));
        GetContestants(CustIndID, false);
        
    }

    protected void gvContestantPhotos_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        m_PageIndex = e.NewPageIndex;
        GetContestants(CustIndID, false);
    }
    #endregion

    # region " Private Methods "

    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
       string newSortDirection = String.Empty;

       switch (sortDirection)
       {
          case SortDirection.Ascending:
             newSortDirection = "ASC";
             break;

          case SortDirection.Descending:
             newSortDirection = "DESC";
             break;
       }

       return newSortDirection;
    }

    private void HideOrShowGridColumnsFromParent()
    {
        if (lblFlag.Text == "true")
        {
            rdoFilter.Visible = true;
            //Need to change the following column index if the order of columns is changed in .aspx
            gvContestantPhotos.Columns[10].Visible = true; //EmailId
            gvContestantPhotos.Columns[11].Visible = true; //Spouse EmailId
            gvContestantPhotos.Columns[12].Visible = true; //ChapterId
            gvContestantPhotos.Columns[16].Visible = true; //Review Date
            gvContestantPhotos.Columns[17].Visible = true; //Reviewer Id
            gvContestantPhotos.Columns[18].Visible = false; //browse file
            gvContestantPhotos.Columns[19].Visible = false; //upload button
            gvContestantPhotos.Columns[2].Visible = true; //download button
            gvContestantPhotos.Columns[3].Visible = true; //thumbnail
            gvContestantPhotos.Columns[4].Visible = true; //Review comments textbox
            gvContestantPhotos.Columns[5].Visible = true; //Review status dropdownlist
            gvContestantPhotos.Columns[6].Visible = true; //Change Status button
        }
        else
        {
            rdoFilter.Visible = false;
            //Need to change the following column index if the order of columns is changed in .aspx
            gvContestantPhotos.Columns[10].Visible = false; //EmailId
            gvContestantPhotos.Columns[11].Visible = false; //Spouse EmailId
            gvContestantPhotos.Columns[12].Visible = false; //ChapterId
            gvContestantPhotos.Columns[16].Visible = false; //Review Date
            gvContestantPhotos.Columns[17].Visible = false; //Reviewer Id
            gvContestantPhotos.Columns[18].Visible = true; //browse file
            gvContestantPhotos.Columns[19].Visible = true; //upload button
            gvContestantPhotos.Columns[2].Visible = false; //download button
            gvContestantPhotos.Columns[3].Visible = true; //thumbnail
            gvContestantPhotos.Columns[4].Visible = false; //Review comments textbox
            gvContestantPhotos.Columns[5].Visible = false; //Review status dropdownlist
            gvContestantPhotos.Columns[6].Visible = false; //Change Status button
        }
    }
    private bool ValidatePhotoFile(EntityContestantPhoto objECP)
    {
        bool bResult = false;
        if (objECP.PhotoFileSize < 500 || objECP.PhotoFileSize > 2048000)
        {
            lblMessage.Text = "Photo size doesn't fall within the acceptable range.(500 Kb - 2 MB)";
            bResult = false;
        }
        else
            bResult = true;
        if (objECP.PhotoFileExtn.ToLower() == "jpg" || objECP.PhotoFileExtn.ToLower() == "tif" || objECP.PhotoFileExtn.ToLower() == "tiff")

            bResult = true;
        else
        {
            lblMessage.Text = "Photo file name is not of acceptable type.(jpg or tif)";
            bResult = false;
        }
        return bResult;
    }

    private void ContestantPhoto_Insert(EntityContestantPhoto objECP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "ContestantPhoto_Insert";
        SqlParameter[] param = new SqlParameter[19];
        param[0] = new SqlParameter("@ChildNumber", objECP.ChildNumber);
        param[1] = new SqlParameter("@ContestYear", objECP.ContestYear);
        param[2] = new SqlParameter("@FirstName", objECP.FirstName);
        param[3] = new SqlParameter("@LastName", objECP.LastName);
        param[4] = new SqlParameter("@MemberId", objECP.MemberId);
        param[5] = new SqlParameter("@Contests", objECP.Contests);
        param[6] = new SqlParameter("@City", objECP.City);
        param[7] = new SqlParameter("@State", objECP.State);
        param[8] = new SqlParameter("@ChapterId", objECP.ChapterId);
        param[9] = new SqlParameter("@PhotoFileName", objECP.PhotoFileName);
        param[10] = new SqlParameter("@StatusFlag", objECP.StatusFlag);
        param[11] = new SqlParameter("@IndEmail", objECP.IndEmail);
        param[12] = new SqlParameter("@SpouseEmail", objECP.SpouseEmail);
        param[13] = new SqlParameter("@Comments", objECP.Comments);
        param[14] = new SqlParameter("@ReviewerId", objECP.ReviewerId);
        param[15] = new SqlParameter("@ReviewDate", objECP.ReviewDate);
        param[16] = new SqlParameter("@CreateDate", objECP.CreateDate);
        param[17] = new SqlParameter("@CreatedBy", objECP.@CreatedBy);
        param[18] = new SqlParameter("@TshirtSize", objECP.TshirtSize);

        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
    }

    private void ContestantPhoto_Update(EntityContestantPhoto objECP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "ContestantPhoto_Update";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@ContestantPhotoId", objECP.ContestantPhotoId);
        param[1] = new SqlParameter("@StatusFlag", objECP.ReviewerStatus);
        param[2] = new SqlParameter("@Comments", objECP.ReviewerComments);
        param[3] = new SqlParameter("@ReviewerId", objECP.ReviewerId);
        param[4] = new SqlParameter("@ReviewDate", objECP.ReviewDate);
        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
    }

    private void ContestantPhoto_Delete(EntityContestantPhoto objECP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "ContestantPhoto_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ContestantPhotoId", objECP.ContestantPhotoId);
        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
    }

    private DataSet ContestantPhoto_Get(Int32 MemberId, int ChildNumber, int ContestantPhotoId)
    {
        SqlParameter[] param = new SqlParameter[3];
        if (lblFlag.Text == "true")
            MemberId = -1;
        param[0] = new SqlParameter("@memberid", MemberId);
        param[1] = new SqlParameter("@ChildNumber", ChildNumber);
        param[2] = new SqlParameter("@ContestantPhotoId", ContestantPhotoId);
        DataSet dscontestantphoto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), 
            CommandType.StoredProcedure, "usp_GetContestantsByParentId", param);
        return dscontestantphoto;
    }
    private EntityContestantPhoto GetEntityContestantPhoto(Int32 MemberId, int ChildNumber, int ContestantPhotoId)
    {
        EntityContestantPhoto objECP = new EntityContestantPhoto(MemberId);
        DataSet ds = ContestantPhoto_Get(MemberId, ChildNumber, ContestantPhotoId);

        objECP.ChapterId = int.Parse(ds.Tables[0].Rows[0]["ChapterId"].ToString());
        objECP.ChildNumber = int.Parse(ds.Tables[0].Rows[0]["ChildNumber"].ToString());
        objECP.City = ds.Tables[0].Rows[0]["City"].ToString();

        if (ds.Tables[0].Rows[0]["Comments"] != null)
        {
            objECP.Comments = ds.Tables[0].Rows[0]["Comments"].ToString();
        }

        if (ds.Tables[0].Rows[0]["ContestantPhotoId"].ToString() != string.Empty)
        {
            objECP.ContestantPhotoId = int.Parse(ds.Tables[0].Rows[0]["ContestantPhotoId"].ToString());
        }

        objECP.Contests = ds.Tables[0].Rows[0]["Contests"].ToString();
        objECP.FirstName = ds.Tables[0].Rows[0]["FIRST_NAME"].ToString();
        objECP.IndEmail = ds.Tables[0].Rows[0]["IndEmail"].ToString();
        objECP.LastName = ds.Tables[0].Rows[0]["LAST_NAME"].ToString();
        objECP.MemberId = int.Parse(ds.Tables[0].Rows[0]["MEMBERID"].ToString());

        if (ds.Tables[0].Rows[0]["PhotoFileName"].ToString() != string.Empty)
        {
            objECP.PhotoFileName = ds.Tables[0].Rows[0]["PhotoFileName"].ToString();
        }

        if (ds.Tables[0].Rows[0]["SpouseEmail"].ToString() != string.Empty)
        {
            objECP.SpouseEmail = ds.Tables[0].Rows[0]["SpouseEmail"].ToString();
        }

        objECP.State = ds.Tables[0].Rows[0]["State"].ToString();

        if (ds.Tables[0].Rows[0]["StatusFlag"].ToString() != string.Empty)
        {
            objECP.StatusFlag = ds.Tables[0].Rows[0]["StatusFlag"].ToString();
        }

        objECP.ContestYear = int.Parse(ds.Tables[0].Rows[0]["ContestYear"].ToString());
        objECP.TshirtSize = ds.Tables[0].Rows[0]["TshirtSize"].ToString();
        return objECP;

    }
    private void GetContestants(Int32 MemberId, bool blnReload)
    {
        GetContestants(MemberId, rdoFilter.SelectedValue, blnReload);
    }

    private void GetContestants(Int32 MemberId, string FilterValue, bool blnReload)
    {
        lbldebug.Text = string.Format("Memberid = {0}" , MemberId);

        DataSet dscontestantphoto = null;
        //Response.Write("<br> " + MemberId + " filter : " + FilterValue + " bool : " + blnReload);
        if (blnReload)
        {
            Session["photoDataSet"] = null;

            dscontestantphoto = ContestantPhoto_Get(MemberId, -1, -1);

            Session["photoDataSet"] = dscontestantphoto;
            m_PageIndex = 0;
        }
        else
        {
            if (Session["photoDataSet"] != null)
            {
                dscontestantphoto = (DataSet)Session["photoDataSet"];

                lbldebug.Text = "Using Session";
            }
            else
            {
                dscontestantphoto = ContestantPhoto_Get(MemberId, -1, -1);
            }
        }

        // Create a new dataview instance on the Customers table that was just created
        DataView dv = new DataView(dscontestantphoto.Tables[0]);

        if (FilterValue == "UnableToFind")
        {
            dv.RowFilter = "StatusFlag is null";
        }
        else if (FilterValue != "All")
        {
            dv.RowFilter = string.Format("StatusFlag='{0}'", FilterValue);
        }

        // Sort
        if (m_SortDirection != string.Empty)
        {
            dv.Sort = m_SortDirection;
        }

        HideOrShowGridColumnsFromParent();
        if (dv.Count != 0)
        {
            gvContestantPhotos.Visible = true;
            gvContestantPhotos.DataSource = dv;
            gvContestantPhotos.PageIndex = m_PageIndex;
            gvContestantPhotos.DataBind();
            lbldebug.Text += string.Format(" (Page:{0})", m_PageIndex);
            lblMessage.Text = string.Format("Total records found: {0}",dv.Count);
        }
        else
        {
            gvContestantPhotos.Visible = false;
            lblMessage.Text = "No Contestants Found!";
        }
    }

    private void SaveFile(string sVirtualPath, FileUpload objFileUpload, string PhotoFileName)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), PhotoFileName));
            //objFileUpload.SaveAs(sVirtualPath+PhotoFileName);
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength + ":" + string.Concat(Server.MapPath(sVirtualPath), PhotoFileName);
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(EntityContestantPhoto objECP)
    {
        try
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objECP.PhotoFileName);
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(string.Concat(objECP.PhotoFileFolderPath, @"\", objECP.PhotoFileName));
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }

    private string GetPhotoFileName(EntityContestantPhoto objECP)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(objECP.FirstName);
        sb.Append("_");
        sb.Append(objECP.LastName);
        sb.Append("_");
        sb.Append(objECP.ChildNumber);
        sb.Append("_");
        sb.Append(objECP.MemberId);
        sb.Append("_");
        sb.Append(objECP.City);
        sb.Append("_");
        sb.Append(objECP.State);
        sb.Append("_");
        sb.Append(objECP.Contests.Replace(",","_"));
        sb.Append(".");
        if (objECP.PhotoFileExtn.ToLower() == "tiff")
            objECP.PhotoFileExtn = "tif";
        sb.Append(objECP.PhotoFileExtn);
        return sb.ToString();
    }

    private void SendEmail(string sSubject, string sBody, string sMailTo)
    {
        string sFrom = System.Configuration.ConfigurationManager.AppSettings.Get("ContestEmail");
        MailMessage mail = new MailMessage(sFrom, sMailTo, sSubject, sBody);
        SmtpClient client = new SmtpClient();
        string host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost");
        client.Host = host;
        mail.IsBodyHtml = true;

        bool ok = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
            ok = false;
        }
    }
#endregion   
    protected void btnExport_Click(object sender, EventArgs e)
    {
        // export coding
        GridView Grd = new GridView();
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        String str = "Select Ch.First_Name , Ch.last_name,C.ChildNumber,I.Email,I.HPhone,I.CPhone,I.FirstName + ' ' + I.LastName as Parent_Name,C.ProductID,C.ProductCode,CP.PhotoFileName,Case when CP.StatusFlag='Approved' OR CP.StatusFlag='Submitted' THEN 1 else 2 End as Statusflag,I.City, I.State,CP.TshirtSize   from Contestant C inner join IndSpouse I ON C.ParentID = I.AutoMemberID  inner join child ch ON C.childnumber=Ch.ChildNumber Left Join contestantphoto CP ON CP.ChildNumber=C.ChildNumber AND C.ContestYear=CP.ContestYear  where C.eventid=1 and C.contestYear=" + ddlyear.SelectedItem.Text + " and C.PaymentReference is not null order by c.productid, cp.statusflag, ch.last_name, ch.first_name";
        Grd.DataSource = SqlHelper.ExecuteReader(conn, CommandType.Text, str);
        Grd.DataBind();
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=ContestantPhoto_" +ddlyear.SelectedItem.Text  + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        Grd.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
    protected void btnFsdPhotos_Click(object sender, EventArgs e)
    {
        // export coding
        GridView Grd = new GridView();
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        String str = "Select Ch.First_Name , Ch.last_name,C.ChildNumber,I.Email,I.HPhone,I.CPhone,I.FirstName + ' ' + I.LastName as Parent_Name,C.ProductID,C.ProductCode,'\\photos_badge\\'+ISNULL(replace(replace(CP.PhotoFileName,'.jpg','.tif'),'.gif','.tif'),'BEEE.JPG') as PhotoFilePathName,ISNULL(replace(replace(CP.PhotoFileName,'.jpg','.tif'),'.gif','.tif'),'') as PhotoFileName,Case when CP.StatusFlag='Approved' OR CP.StatusFlag='Submitted' THEN 1 else 2 End as Statusflag,I.City, I.State   from Contestant C inner join IndSpouse I ON C.ParentID = I.AutoMemberID  inner join child ch ON C.childnumber=Ch.ChildNumber Left Join contestantphoto CP ON CP.ChildNumber=C.ChildNumber AND C.ContestYear=CP.ContestYear  where C.eventid=1 and C.contestYear= Year(Getdate()) and C.PaymentReference is not null order by c.productid, cp.statusflag, ch.last_name, ch.first_name";
        DataSet DSCnstPhoto;
        DSCnstPhoto = SqlHelper.ExecuteDataset (conn, CommandType.Text, str);
        //Grd.DataSource = SqlHelper.ExecuteReader(conn, CommandType.Text, str);
        int i;
        String filepath;
        FileInfo file;
        for (i=0;i<DSCnstPhoto.Tables[0].Rows.Count;i++)
        {
            filepath = Server.MapPath(DSCnstPhoto.Tables[0].Rows[i]["PhotoFilePathName"].ToString());
            file = new FileInfo(filepath);
            if (!file.Exists)
            {
                DSCnstPhoto.Tables[0].Rows[i]["PhotoFilePathName"] = "";
                DSCnstPhoto.Tables[0].Rows[i]["PhotoFileName"] = "";
            }
        }
        DSCnstPhoto.Tables[0].Columns.RemoveAt(9);
        Grd.DataSource = DSCnstPhoto;
        Grd.DataBind();
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=Finished_Photos_List_" + DateTime.Now.Year .ToString () + ".xls");
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache); commented as this statement is causing "Internet Explorer cannot download ......error
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        Grd.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        GetContestants(CustIndID, true);
       // btnContinue.Visible = false;
    }
    protected void btnAddRegis_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        String str = " Select C.ChildNumber,C.contestant_id,CP.PhotoFileName,cp.ContestantPhotoId,Ch.FIRST_NAME +'_'+ ch.LAST_NAME+'_'+CAST(ch.ChildNumber as Varchar)+'_'+CAST(ch.MEMBERID as Varchar) +'_'+I.City +'_'+I.[State]+'_'+LEFT(contest,LEN(contest) - 1)+ SUBSTRING (CP.PhotoFileName,CHARINDEX('.',CP.PhotoFileName),4) as NewFilename, LEFT(contest,LEN(contest) - 1) as contests " +
                     " from Contestant C INNER JOIN IndSpouse I ON C.ParentID = I.AutoMemberID  " +
                     " INNER JOIN Child Ch ON C.ChildNumber = Ch.ChildNumber   " +
                     " Inner Join contestantphoto CP ON CP.ChildNumber=C.ChildNumber AND C.ContestYear=CP.ContestYear   " +
                     " CROSS APPLY (SELECT Distinct intern.ProductCode  + '_'  " +
                     " FROM   Contestant AS intern WHERE intern.EventId = C.EventId and intern.ContestYear = C.ContestYear and intern.PaymentReference is Not Null and intern.childnumber=c.childnumber FOR XML PATH('')) pre_trimmed (contest) " +
                     " where C.eventid=1 and C.contestYear= Year(Getdate()) and (C.photo_image is Null OR C.photo_image <> 'ok')  and C.PaymentReference is not null  order by c.productid ";
        DataSet DSCnstPhoto;
       
        DSCnstPhoto = SqlHelper.ExecuteDataset(conn, CommandType.Text, str);
        //Grd.DataSource = SqlHelper.ExecuteReader(conn, CommandType.Text, str);
        int i;
        String filepath;
        String Newfilepath;
        FileInfo file;
        for (i = 0; i < DSCnstPhoto.Tables[0].Rows.Count; i++)
        {
            filepath = Server.MapPath(ConfigurationSettings.AppSettings["ContestantPhotoPath"] + DSCnstPhoto.Tables[0].Rows[i]["PhotoFileName"].ToString());
            Newfilepath = Server.MapPath(ConfigurationSettings.AppSettings["ContestantPhotoPath"] + DSCnstPhoto.Tables[0].Rows[i]["NewFilename"].ToString());
            file = new FileInfo(filepath);
            if (file.Exists)
            {
                File.Move(filepath, Newfilepath);
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update contestant set photo_image ='ok' where ChildNumber=" + DSCnstPhoto.Tables[0].Rows[i]["ChildNumber"].ToString() + " AND ContestYear = YEAR(GETDATE()) AND EventId= 1 AND PaymentReference is Not NULL AND (photo_image is Null or photo_image <>'ok')");
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update ContestantPhoto set Contests ='" + DSCnstPhoto.Tables[0].Rows[i]["contests"].ToString().Replace("_", ", ") + "', PhotoFileName ='" + DSCnstPhoto.Tables[0].Rows[i]["NewFilename"].ToString() + "' where ContestantPhotoId=" + DSCnstPhoto.Tables[0].Rows[i]["ContestantPhotoId"].ToString() + "");
            }
        }
        lblMessage.Text = "All contestant and contestantphoto Records updated successfully";
    }

  
}


public class EntityContestantPhoto
{
    public Int32 ContestantPhotoId = -1;
    public Int32 PhotoId = -1;
    public Int32 ChildNumber = -1;
    public Int32 ContestYear = System.DateTime.Now.Year;
    public string FirstName = string.Empty;
    public string LastName = string.Empty;
    public Int32 MemberId = -1;
    public string Contests;
    public string City = string.Empty;
    public string State = string.Empty;
    public Int32 ChapterId = 1;
    public string PhotoFileName = string.Empty;
    public string PhotoFileFolderPath = string.Empty;
    public string StatusFlag = "Submitted";
    public string IndEmail = string.Empty;
    public string SpouseEmail = string.Empty;
    public string Comments = "Photo is not approved yet.";
    public Int32 ReviewerId = -1;
    public DateTime ReviewDate = new System.DateTime(1900, 1, 1);
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime ModifyDate = new System.DateTime(1900, 1, 1);
    public Int32 ModifiedBy = -1;
    public Int32 PhotoFileSize = -1;
    public string PhotoFileExtn = string.Empty;
    public string ReviewerComments = string.Empty;
    public string ReviewerStatus = string.Empty;
    public string TshirtSize = string.Empty;
    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
        ConfigurationSettings.AppSettings["FTPTestPapersPath"],TestFileName);
    }
    public EntityContestantPhoto(Int32 parentmemberid)
    {
        MemberId = parentmemberid;
    }
}
