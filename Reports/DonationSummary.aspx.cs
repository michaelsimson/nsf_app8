﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;

public partial class DonationSummary : System.Web.UI.Page
{
    
 int fiscyear;
 int neval;
 string st;
 Label lbdate = new Label();
 public Int32 ExpmemberID = 0;
 public Int32 Exyear = 0;
 public Int32 ExEventID = 0;
 String StrEvent;
 public Int32 Exchapterid = 0;
 public Int32 Exchapteridmember = 0;
 DataRow drExcel;
      int newvar;
 string Qrycondition;
 DataTable dtnewgrid;
 DataTable dtexcelview;
 DataTable Mergecopy = new DataTable();
 DataSet dsnew;
    string Zoneqry;
    string StaYear = System.DateTime.Now.Year.ToString();
    string calc;
    int Start;
    int endall;
    int i;
    string firstquert;
    string Qry;
    bool isviewexcel = false;
    string Qryvaluewhere = string.Empty;
    bool isviewexcelReport = false;
    DataRow dr;
    bool isall = false;


    protected void ddchaptercode(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        string Chaptercode = "select ChapterCode,[state] from chapter order by [state],ChapterCode";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Chaptercode);
        ddlTemp.DataSource = ds;
        DataTable dt = ds.Tables[0];
        ddlTemp.DataTextField = "ChapterCode";
        ddlTemp.DataBind();
        ddlTemp.Items.Insert(0, "[Select Chapter]");
        ddlTemp.SelectedIndex = 0;
    }

    protected void ddBankid(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;

        string Bank = "select distinct BankCode,bankid from Bank order by BankID";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Bank);
        ddlTemp.DataSource = ds;
        DataTable dt = ds.Tables[0];
        ddlTemp.DataTextField = "BankCode";
        ddlTemp.DataValueField = "bankid";
        ddlTemp.DataBind();
        ddlTemp.Items.Insert(0, "[Select Bank]");
        ddlTemp.SelectedIndex = 0;
    }

    protected void ddyear(object sender, System.EventArgs e)
    {
        int year;
        year = DateTime.Now.Year;
        int year1 = year - 1;
        int year2 = year1 - 1;
        int year3 = year2 - 1;
        int year4 = year3 - 1;
        int year5 = year4 - 1;
        int year6 = year5 - 1;
        int year7 = year6 - 1;
        int year8 = year7 - 1;
        int year9 = year8 - 1;
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        ddlTemp.Items.Insert(0, new ListItem(year.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year1.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year2.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year3.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year4.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year5.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year6.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year7.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year8.ToString()));
        ddlTemp.Items.Insert(0, new ListItem(year9.ToString()));
        ddlTemp.Items.Insert(0, "[Select year]");
        ddlTemp.SelectedIndex = 0;
    }
      
        
     protected void purpose(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        string Chaptercode = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Chaptercode);
        ddlTemp.DataSource = ds;
        DataTable dt = ds.Tables[0];
        ddlTemp.DataTextField ="PurposeDesc";
        ddlTemp.DataBind();
        ddlTemp.Items.Insert(0, "[Select Purpose]");
        ddlTemp.SelectedIndex = 0;
    }
    protected void Event(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        string Chaptercode = " select EventCode,Name from Event";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Chaptercode);
        ddlTemp.DataSource = ds;
        DataTable dt = ds.Tables[0];
        ddlTemp.DataTextField = "Name";
        ddlTemp.DataBind();
        ddlTemp.Items.Insert(0, "[Select Event]");
        ddlTemp.SelectedIndex = 0;
    }
  

     protected void donationtype(object sender, System.EventArgs e)
     {
         DropDownList ddlTemp = null;
         ddlTemp = (DropDownList)sender;
         ddlTemp.Items.Insert(0, "Unrestricted");
         ddlTemp.Items.Insert(0, "Temp Restricted");
         ddlTemp.Items.Insert(0, "Perm Restricted");
         ddlTemp.Items.Insert(0, "[Select Type]");
         ddlTemp.SelectedIndex = 0;
     }




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            else
            {
                if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || (Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5")))
                {
                    Iddonation.Visible = true;
                    DDlfrontchoice(DDchoice);
                    Event();
                    Yearscount();
                    years();
                    Pnldisp.Visible = false;
                    LBbacktofront.Visible = true;
                }
                if ((Session["RoleId"].ToString() == "5"))
                {
                    Chapter();
                    Cluster();
                    Event();
                    Yearscount();
                    years();
                }
                else if ((Session["RoleId"].ToString() == "3"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct zoneid from volunteer where MemberID='" + Session["LoginID"] + "' and TeamLead='" + Session["TL"] + "'  and zoneid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["zoneid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Zone();
                        Cluster();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have ZoneCode ";
                    }
                }
                else if ((Session["RoleId"].ToString() == "4"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct clusterid from volunteer where MemberID='" + Session["LoginID"] + "' and clusterid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["clusterid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Cluster();
                        Zone();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have Cluster ";
                    }
                }
                else
                {
                    Zone();
                    Event();
                    Yearscount();
                    years();
                }

            }
        }
    }
    protected void DDlfrontchoice(DropDownList DDobject)
    {
        DDobject.Items.Clear();
        DDobject.Items.Insert(0, new ListItem("ACH – By Event","4"));
        DDobject.Items.Insert(0, new ListItem("ACH – By Chapter","5"));
      
        DDobject.Items.Insert(0, new ListItem("ACH – By Donor","6"));

        DDobject.Items.Insert(0, new ListItem("Check/Credit Card – By Event", "3"));
        DDobject.Items.Insert(0, new ListItem("Check/Credit Card – By Chapter", "2"));
      
        DDobject.Items.Insert(0, new ListItem("Check/Credit Card – By Donor", "1"));
        DDobject.Items.Insert(0, new ListItem("[Select Report]", "0"));

       
    }
    protected void Event()
    {
        ddevent.Items.Clear();

       
        ddevent.Items.Insert(0, new ListItem("Tournament", "14"));
        ddevent.Items.Insert(0, new ListItem(" Marathon", "12"));
        ddevent.Items.Insert(0, new ListItem("Fund Raising", "9"));
        ddevent.Items.Insert(0, new ListItem("Excel-a-thon", "18"));
        ddevent.Items.Insert(0, new ListItem("Donation", "11"));
        //ddevent.Items.Insert(0, new ListItem("DAS", "6"));
        ddevent.Items.Insert(0, new ListItem("Corp Volunteering", "8"));
        ddevent.Items.Insert(0, new ListItem("Corp Matching", "7"));
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
        {
            ddevent.Items.Insert(0, new ListItem("Coaching Online", "13"));
        }
       
        ddevent.Items.Insert(0, new ListItem("Game", "4"));
        ddevent.Items.Insert(0, new ListItem("PrepClub", "19"));
        ddevent.Items.Insert(0, new ListItem("Workshop", "3"));
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
        {
            ddevent.Items.Insert(0, new ListItem("Finals", "1"));
        }
        
        ddevent.Items.Insert(0, new ListItem("Regional Contests", "2"));
        ddevent.Items.Insert(0, new ListItem("All", "-1"));
        ddevent.Items.Insert(0, new ListItem("[Select Event]", "0"));
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void Zone()
    {
        try
        {
            ddZone.Enabled = true;
            ddZone.Items.Clear();
            ddZone.Items.Insert(0, "[Select Zone]");
            if ((Session["RoleId"].ToString() == "5"))
            {
                Zoneqry = "select ZoneCode,ZoneId from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Zone where  ZoneId='" + Txthidden.Text + "'  and ZoneCode is not null";
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Cluster where ClusterId='" + Txthidden.Text + "'";
            }
            else
            {
                if (ddevent.SelectedItem.Text == "All")
                {
                    if ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") )
                    {
                        Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                    }
                }
                else if (ddevent.SelectedItem.Text == "All")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("All", "-1"));
                    ddCluster.Items.Insert(0, "All");
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Finals")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Finals", "1"));
                    ddZone.Enabled = false;
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Coaching Online")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddZone.Enabled = false;
                    return;
                }
                else
                {
                    Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                }
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Zoneqry);
            ddZone.DataSource = ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddZone.Items.Clear();
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    ddZone.Enabled = false;
                }
                else
                {
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        ddZone.Items.Insert(0, "[Select Zone]");
                        ddZone.Items.Insert(0, "All");
                    }
                    else
                    {
                        ddZone.Items.Insert(0, "All");
                        ddZone.Items.Insert(0, "[Select Zone]");
                    }
                }
            }
            else
            {
                ddZone.Items.Insert(0, "[Select Zone]");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Cluster()
    {
        try
        {
            ddCluster.Enabled = true;
            ddCluster.Items.Clear();
            ddCluster.Items.Insert(0, "[Select Cluster]");
            if ((Session["RoleId"].ToString() == "5"))
            {
                string clusqry = "select ClusterId,clustercode from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                string clusqry = "select clustercode,ClusterId from Cluster where clusterId='" + Txthidden.Text + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }

            else if ((Session["RoleId"].ToString() == "3"))
            {
                string clusqry = "select clustercode,clusterid from cluster where ZoneId='" + ddZone.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");
                }
            }
            else
            {
                if (ddZone.SelectedItem.Text != "[Select Zone]")
                {
                    if (ddZone.SelectedItem.Text == "All")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("All", "-1"));
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Finals", "1"));
                        ddCluster.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text =="Coaching Online")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddCluster.Enabled = false;
                    }
                    else
                    {
                        DataSet ds;
                        if (ddZone.SelectedItem.Text == "All")
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A'");
                        }
                        else
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A' and ZoneID='" + ddZone.SelectedValue + "'");
                        }
                        ddCluster.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddCluster.DataTextField = "Name";
                            ddCluster.DataValueField = "ClusterID";
                            ddCluster.DataBind();
                            ddCluster.Items.Insert(0, "All");
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                        else
                        {
                            ddCluster.Items.Clear();
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                    }
                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.Items.Insert(0, "[Select Cluster]");
                }
            }
            if (ddZone.SelectedItem.Text == "All")
            {
                string clusqry = "select  distinct clustercode,clusterid from cluster where Status='A' ";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;
                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");
                }
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Chapter()
    {
        try
        {
            ddchapter.Enabled = true;
            if ((Session["RoleId"].ToString() == "5"))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from volunteer where  MemberID='" + Session["LoginID"] + "' and ChapterId is not null");
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddchapter.Items.Clear();
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;
                }
                else
                {
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode,[State] from chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        Cluster();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "All")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterid,chaptercode,[state] from chapter where clusterId='" + ddCluster.SelectedValue + "' and ChapterId is not null order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.Enabled = true;
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else
            {
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    if (ddCluster.SelectedItem.Text == "All")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, "All");
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Finals, US", "1"));
                        ddchapter.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text == "Coaching Online")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Coaching,US", "112"));
                        ddchapter.Enabled = false;

                    }
                    else
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ChapterID,[state],chaptercode from Chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                        ddchapter.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddchapter.DataTextField = "Name";
                            ddchapter.DataValueField = "ChapterID";
                            ddchapter.DataBind();
                            ddchapter.Items.Insert(0, "All");
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                        else
                        {
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                    }
                }
                else
                {
                    ddchapter.Items.Clear();
                    ddchapter.Items.Insert(0, "[Select Chapter]");
                }
            }
            if (ddCluster.SelectedItem.Text == "All")
            {
                string ClusterFlqry;
                string WhereCluster;
                ClusterFlqry = "select distinct chapterid,chaptercode,[State] from chapter where  ChapterId is not null";
                WhereCluster = ClusterFlqry + Filterdropdown();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, WhereCluster);
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                ddchapter.Enabled = true;
                ddchapter.DataTextField = "chaptercode";
                ddchapter.DataValueField = "ChapterID";
                ddchapter.DataBind();
                ddchapter.Items.Insert(0, "All");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string Filterdropdown()
    {
        string iCondtions = string.Empty;
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ClusterId=" + ddCluster.SelectedValue;
            }
        }
        return iCondtions + "  order by [State],ChapterCode";
    }


    protected void DDchoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        Pnldisp.Visible = true;
        Divchoice.Visible = false;
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            Iddonation.Visible = false;
            Iddonor.Visible = true;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "3")
        {
            Iddonation.Visible = false;
            Iddonor.Visible = false;
            Idevent.Visible = true;
           
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "4")
        {
            Pnldisp.Visible = false;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "5")
        {
            Pnldisp.Visible = false;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "6")
        {
            Pnldisp.Visible = false;
        }
            
        else
        {
            Iddonation.Visible = false;
            Iddonor.Visible = false;
            Idevent.Visible = false;
            Idchapter.Visible = true;

        }



    }
    protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    {
        Zone();
        Cluster();
        Chapter();
    }
    protected void ddZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cluster();
        Chapter();
    }
    protected void ddCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        Chapter();
    }
    protected void Yearscount()
    {
        ddNoyear.Items.Clear();
        ddNoyear.Items.Add("[Select No of Years]");
        ddNoyear.Items.Add("1");
        ddNoyear.Items.Add("2");
        ddNoyear.Items.Add("3");
        ddNoyear.Items.Add("4");
        ddNoyear.Items.Add("5");
        ddNoyear.Items.Add("6");
        ddNoyear.Items.Add("7");
        ddNoyear.Items.Add("8");
        ddNoyear.Items.Add("9");
        ddNoyear.Items.Add("10");
    }
    protected void years()
    {
        DDyear.Items.Clear();
        DDyear.Items.Add("Year");
        DDyear.Items.Add("Calendar Year");
        DDyear.Items.Add("Fiscal Year");
    }
    protected void Amountcalc()
    {
        try
        {
            //Button2.Enabled = false;
            
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
            }
            int fiscyear = Start + 1;
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
               
                if (DDyear.SelectedItem.Text == "Fiscal Year")
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "Select CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,b.bankID,Di.Project ,DI.DonationType from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.MemberID=" + Session["MemberID"] + " ";
                    }
                    else
                    {
                        Qrycondition = "Select CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,b.bankID,Di.Project ,DI.DonationType from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.MemberID=" + Session["MemberID"] + " ";
                    }
                    Qrycondition = Qrycondition + genWhereConditons1() + " order by di.DonationDate desc";
                }
                
                    else
                    {
                        if (isviewexcelReport == true)
                        {
                            Qrycondition = "Select CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.MemberID=" + Session["MemberID"] + "  ";
                        }
                        else
                        {
                            Qrycondition = "Select CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.MemberID=" + Session["MemberID"] + "  ";
                        }
                        Qrycondition = Qrycondition + genWhereConditons1() + " order by di.DonationDate desc";
                    }
                }
            //}
            else if (DDchoice.SelectedItem.Value.ToString() == "3")
            {
                
                if (DDyear.SelectedItem.Text == "Fiscal Year")
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "Select Di.EVENT as EventName,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear  as EventYear,DI.event,E1.Name as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project,b.bankID ,DI.DonationType from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.Event='" + Session["Eventname"] + "' and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                    else
                    {
                        Qrycondition = "Select Di.EVENT as EventName,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear  as EventYear,DI.event,E1.Name as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project,b.bankID ,DI.DonationType from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.Event='" + Session["Eventname"] + "' and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                }
                else
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "Select Di.EVENT as EventName,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear  as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where  year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.Event='" + Session["Eventname"] + "' and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                    else
                    {
                        Qrycondition = "Select Di.EVENT as EventName,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear  as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode, b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where  year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.Event='" + Session["Eventname"] + "' and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                }
            }
            else
            {
               
                if (DDyear.SelectedItem.Text == "Fiscal Year")
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "Select Di.ChapterId,ch.Chaptercode,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode,b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join  chapter ch on ch.ChapterID =DI.ChapterId left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)) between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.ChapterId=" + Session["chapterID"] + " and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                    else
                    {
                        Qrycondition = "Select Di.ChapterId,ch.Chaptercode,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode,b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join  chapter ch on ch.ChapterID =DI.ChapterId left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)) between '05/01/" + endall + "'  and  '04/30/" + fiscyear + "' and DI.ChapterId=" + Session["chapterID"] + " and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                }
                else
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "Select Di.ChapterId,ch.Chaptercode,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode,b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join  chapter ch on ch.ChapterID =DI.ChapterId left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID  where  year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.ChapterId=" + Session["chapterID"] + " and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                    else
                    {
                        Qrycondition = "Select Di.ChapterId,ch.Chaptercode,CASE WHEN DI.DonorType='OWN' THEN Org.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + I.LastName, ' ', ' ') END as DonorName,DI.MEMBERID as MemberID,Di.TRANSACTION_NUMBER,DI.DonorType,di.DonationDate,(Select replace( convert( varchar(32), cast( '$' + cast( cast(DI.Amount as decimal(19,0)) AS varchar(32) ) AS money ), 1 ), '.00', '' )) as Amount,DI.EventYear as EventYear,DI.event,E1.Name  as EventID,convert (varchar(10),DI.DepositDate,101) as DepositDate ,DI.PURPOSE,ch.chaptercode,b.BankName,Di.Project ,DI.DonationType,b.bankID from DonationsInfo DI left join  chapter ch on ch.ChapterID =DI.ChapterId left join OrganizationInfo Org on Org.AutoMemberID =DI.MEMBERID left join IndSpouse I on I.AutoMemberID =DI.MEMBERID left join Event E1 on E1.EventId=di.EventId left join Bank b on b.BankID=di.BankID  where  year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))   between '" + endall + "' and '" + Start + "' and DI.ChapterId=" + Session["chapterID"] + " and Di.Memberid=" + Session["Memberid"] + " order by di.DonationDate desc ";
                    }
                }
            }
            dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            DataTable dtmerge = new DataTable();
            dtmerge = dsnew.Tables[0];

            dtnewgrid = dtmerge;
            DataRow dr = dtnewgrid.NewRow();

           
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (isviewexcelReport == true)
                {
                    neval = 13;
                }
                else
                {
                    neval = 5;
                }
                dr[0] = "Total (Sum)";
            }
            else if (DDchoice.SelectedItem.Value.ToString() == "3")
            {
                if (isviewexcelReport == true)
                {
                    neval = 14;
                }
                else
                {
                    neval = 6;

                }
                dr[1] = "Total (Sum)";
            }
            else
            {
                if (isviewexcelReport == true)
                {
                    neval = 15;
                }
                else
                {

                    neval = 7;
                }
                
                dr[2] = "Total (Sum)";
            }

            //dr[4] = 0;
            double sum = 0;
            string suma = "";
            foreach (DataRow drnew in dtnewgrid.Rows)
            {
                if (!DBNull.Value.Equals(drnew[neval]))
                {
                    suma = drnew[neval].ToString();
                    if (suma != "")
                    {
                        sum += Convert.ToDouble(drnew[neval]);
                    }
                }
            }

            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (isviewexcelReport == true)
                {
                    dr[13] = sum;
                }
                else
                {
                    dr[5] = sum;
                }
            }
            else if (DDchoice.SelectedItem.Value.ToString() == "2")
            {
                if (isviewexcelReport == true)
                {
                    dr[15] = sum;
                }
                else
                {
                    dr[7] = sum;
                }
            }
            else
            {
                if (isviewexcelReport == true)
                {
                    dr[14] = sum;
                }
                else
                {
                    dr[6] = sum;
                }
            }
            dtnewgrid.Rows.Add(dr);
          
            if (isviewexcelReport == true)
            {
                foreach (DataRow dr1 in dtnewgrid.Rows)
                {
                    if (!DBNull.Value.Equals(dr["Amount"]))
                    {
                        string st;
                        double st1;
                        st1 = Convert.ToDouble(dr1["Amount"]);
                        dr1["Amount"] = String.Format("{0:#,###0}", st1);


                    }
                }
            }
            Session["EXelEdit"] = dtnewgrid;
            GridDisplay.Visible = false;
            GridViewchapter.Visible = false;
            GVEventt.Visible = false;
            GVindividual.Visible = true;
            Session["pagingindi"] = dtnewgrid;
           
            GVindividual.DataSource = dtnewgrid;
            GVindividual.DataBind();
            isviewexcel = true;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;
            
           // Button2.Enabled = false;
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
  
    protected void buttonclick()
    {
        try
        {
            if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
            {

                GridDisplay.Visible = true;
                lblall.Visible = false;
                if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
                {
                    calc = ddNoyear.SelectedItem.Text;
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
                }
                bool Isfalse = false;
                //for (i = endall; i <= Start; i++)
                //{
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                            if (isviewexcelReport == true)
                            {
                                Qrycondition = "with tbl as (select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))     between '01/01/" + endall + "' and '12/31/" + Start + "' ";
                            }
                            else
                            {

                                Qrycondition = "with tbl as (select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' ";
                            }
                            
                        }
                        else if (DDchoice.SelectedItem.Value.ToString() == "3")
                        {
                            if (isviewexcelReport == true)
                            {
                                
                                    Qrycondition = "with tbl as (select  Di.EVENT as EventName,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))   between '01/01/" + endall + "' and '12/31/" + Start + "'";
                                

                            }
                            else
                            {
                                if (ddchapter.SelectedItem.Text == "All")
                                {
                                    Qrycondition = "with tbl as (select  Di.EVENT as EventName,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '01/01/" + endall + "' and '12/31/" + Start + "'";
                                }
                                else
                                {
                                    GridViewforall.Visible = false;
                                    Qrycondition = "with tbl as (select  Di.EVENT as EventName,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '01/01/" + endall + "' and '12/31/" + Start + "'";
                                }
                            }
                        }
                        else
                        {
                            if (isviewexcelReport == true)
                            {
                                Qrycondition = "with tbl as (select  Di.ChapterId,ch.ChapterCode,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '01/01/" + endall + "' and '12/31/" + Start + "' ";
                            }
                            else
                            {
                                if (ddchapter.SelectedItem.Text == "All")
                                {
                                    Qrycondition = "with tbl as (select  Di.ChapterId,ch.ChapterCode,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))    as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  (case when  Convert(nvarchar,di.[DepositDate],101) is NULL then Convert(nvarchar,di.DonationDate,101)  else  Convert(nvarchar,di.[DepositDate],101) end)  between '01/01/" + endall + "' and '12/31/" + Start + "' ";
                                }
                                else
                                {
                                    Gridchapterall.Visible = false;
                                    Qrycondition = "with tbl as (select  Di.ChapterId,ch.ChapterCode,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))    as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  (case when  Convert(nvarchar,di.[DepositDate],101) is NULL then Convert(nvarchar,di.DonationDate,101)  else  Convert(nvarchar,di.[DepositDate],101) end)  between '01/01/" + endall + "' and '12/31/" + Start + "' ";
                                }
                            }
                        }
                        Qry = Qrycondition + genWhereConditons();


                    }
                    else
                    {
                        Qryvaluewhere = genWhereConditons1();
                        string WCNTQry = string.Empty;
                        string WCNTQrynew = string.Empty;
                        string CommonQry = string.Empty;
                       
                        for (i = endall; i <= Start; i++)
                        {
                            fiscyear = i + 1;
                            if (DDchoice.SelectedItem.Value.ToString() == "1")
                            {
                                if (isviewexcelReport == true)
                                {
                                    CommonQry = "select  DI.MEMBERID,Di.donortype,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate), org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,DI.MEMBERID,di.DonorType";
                                }
                                else
                                {
                                    CommonQry = "select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType";
                                }
                            }
                            else if (DDchoice.SelectedItem.Value.ToString() == "3")
                            {
                                if (isviewexcelReport == true)
                                {
                                    CommonQry = "select  Di.EVENT as EventName,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName, org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,di.DonorType";
                                }
                                else
                                {
                                    if (ddchapter.SelectedItem.Text == "All")
                                    {
                                        GridViewforall.Visible = false;
                                        CommonQry = "select  Di.EVENT as EventName,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate";
                                    }
                                    else
                                    {
                                        CommonQry = "select  Di.EVENT as EventName,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType";
                                    }
                                }
                            }
                            else
                            {
                                if (isviewexcelReport == true)
                                {
                                    CommonQry = "select  Di.ChapterId,ch.ChapterCode,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName, org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,di.DonorType";
                                }
                                else
                                {
                                    if (ddchapter.SelectedItem.Text == "All")
                                    {
                                        CommonQry = "select  Di.ChapterId,ch.ChapterCode,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType";
                                    }
                                    else
                                    {
                                        Gridchapterall.Visible = false;
                                        CommonQry = "select  Di.ChapterId,ch.ChapterCode,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType";
                                    }
                                }
                            }
                            if (WCNTQry != string.Empty)
                            {
                                //WCNTQrynew = "select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(di.DonationDate))=1) then (max(year(di.DonationDate)))-1  else min (CONVERT(varchar(50),year(di.DonationDate))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where Di.[DonationDate]    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType";
                                WCNTQrynew = CommonQry;
                                WCNTQry = WCNTQry + " union all " + WCNTQrynew;
                               
                            }
                            else
                            {
                               // WCNTQry = "select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(di.DonationDate))=1) then (max(year(di.DonationDate)))-1  else min (CONVERT(varchar(50),year(di.DonationDate))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where Di.[DonationDate]    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType";
                                WCNTQry = CommonQry;
                               
                            }
                        }
                        Qrycondition = "with tbl as (" + WCNTQry + " )"+genWhereConditons()+"";
                        Qry = Qrycondition;
                        fiscyear = i + 1;

                    }
                   
                    dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                    DataTable dtmerge = new DataTable();
                    dtmerge = dsnew.Tables[0];
                    Mergecopy = dtmerge;
                   
                if (Mergecopy.Rows.Count > 0)
                {
                    Button2.Enabled = true;
                    if (isviewexcelReport == true)
                    {
                        dtexcelview = Mergecopy;
                       
                    }
                    else
                    {
                        dtnewgrid = Mergecopy;
                    }
                    lbldisp.Visible = false;
                    if (isviewexcelReport == true)
                    {
                         drExcel = dtexcelview.NewRow();
                    }
                    else
                    {
                         dr = dtnewgrid.NewRow();
                    }
                  
                   
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        
                        if (isviewexcelReport == true)
                        {
                            drExcel[1] = "Total";
                            newvar = 11;
                        }
                        else
                        {
                            dr[1] = "Total";
                            newvar = 3;
                        }
                    }
                    else if (DDchoice.SelectedItem.Value.ToString() == "3")
                    {
                       
                        if (isviewexcelReport == true)
                        {
                            drExcel[0] = "Total";
                            newvar = 12;
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                dr[0] = "Total";
                                newvar = 1;
                            }
                            else
                            {
                                dr[0] = "Total";
                                newvar = 4;
                            }
                        }
                    }
                    else
                    {
                       
                        if (isviewexcelReport == true)
                        {
                            drExcel[1] = "Total";
                            newvar = 13;
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                dr[1] = "Total";
                                newvar = 2;
                            }
                            else
                            {
                                dr[1] = "Total";
                                newvar = 5;
                            }
                        }
                    }
                    if (isviewexcelReport == true)
                    {
                        for (int i = newvar; i <= dtexcelview.Columns.Count - 1; i++)
                        {
                            drExcel[i] = 0;
                            double sum = 0;
                            string suma = "";
                            foreach (DataRow drnew in dtexcelview.Rows)
                            {
                                if (!DBNull.Value.Equals(drnew[i]))
                                {
                                    suma = drnew[i].ToString();
                                    if (suma != "")
                                    {
                                        sum += Convert.ToDouble(drnew[i]);
                                    }

                                }
                            }
                            drExcel[i] = sum;
                        }
                       
                    }
                    else
                    {
                        for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            dr[i] = 0;
                            double sum = 0;
                            string suma = "";
                            foreach (DataRow drnew in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drnew[i]))
                                {
                                    suma = drnew[i].ToString();
                                    if (suma != "")
                                    {
                                        sum += Convert.ToDouble(drnew[i]);
                                    }

                                }
                            }
                            dr[i] = sum;
                        }
                    
                        
                    }
                    if (isviewexcelReport == true)
                    {
                        if (DDchoice.SelectedItem.Value.ToString() == "1")
                        {
                                newvar = 11;
                           
                        }
                        else if (DDchoice.SelectedItem.Value.ToString() == "3")
                        {
                            newvar = 12;

                        }
                        else
                        {
                            newvar = 13;
                        }
                        
                        dtexcelview.Rows.Add(drExcel);
                        
                            dtexcelview.Columns.Add("total", typeof(String));
                            for (int i = newvar; i <= dtexcelview.Columns.Count - 1; i++)
                           {
                            foreach (DataRow dr in dtexcelview.Rows)
                            {
                                if (!DBNull.Value.Equals(dr[i]))
                                {
                                    string st;
                                    double st1;
                                    st1 = Convert.ToDouble(dr[i]);
                                    st = String.Format("{0:#,###0}", dr["Total"]);

                                    dr[i] = String.Format("{0:#,###0}", st1);
                                    // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["Total"]);
                                    dr["total"] = st.ToString();

                                }
                            }
                        }
                        dtexcelview.Columns.Remove("Total");
                        dtexcelview.AcceptChanges();
                    }
                    else
                    {
                        dtnewgrid.Rows.Add(dr);
                        dtnewgrid.Columns.Add("total", typeof(String));
                        for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            foreach (DataRow drn in dtnewgrid.Rows)
                            {
                                if (!DBNull.Value.Equals(drn[i]))
                                {
                                    string st;
                                    double st1;
                                    st1 = Convert.ToDouble(drn[i]);
                                    st = String.Format("{0:#,###0}", drn["Total"]);

                                    drn[i] = String.Format("{0:#,###0}", st1);
                                    // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["Total"]);
                                    drn["total"] = st.ToString();

                                }
                            }
                        }

                        dtnewgrid.Columns.Remove("Total");
                    }
                    

                    GVindividual.Visible = false;
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                        Session["Griddonor"] = dtnewgrid;
                       
                        if (isviewexcelReport == true)
                        {
                            Session["GriddonorExcel"] = dtexcelview;
                          
                        }
                        else
                        {
                            GridDisplay.DataSource = dtnewgrid;
                            GridDisplay.DataBind();
                        }

                    }
                    else if (DDchoice.SelectedItem.Value.ToString() == "3")
                    {
                        Session["GridEvent"] = dtnewgrid;
                        if (isviewexcelReport == true)
                        {
                            Session["GridEventExcel"] = dtexcelview;
                        }
                        else
                        {
                            if(ddchapter.SelectedItem.Text=="All")
                            {
                                GridViewforall.Visible = true;
                                Session["Pagingevent"] = dtnewgrid;
                                GridViewforall.DataSource = dtnewgrid;
                                GridViewforall.DataBind();
                            }
                            else
                            {
                            
                            GVEventt.DataSource = dtnewgrid;
                            GVEventt.DataBind();
                            }
                        }
                    }
                    else
                    {
                        Session["Gridchapter"] = dtnewgrid;
                        if (isviewexcelReport == true)
                        {
                            Session["GridchapterExcel"] = dtexcelview;
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {

                                Session["Pagingchapter"] = dtnewgrid;
                                Gridchapterall.DataSource = dtnewgrid;
                                Gridchapterall.DataBind();
                            }
                            else
                            {

                                GridViewchapter.DataSource = dtnewgrid;
                                GridViewchapter.DataBind();
                            }
                        }

                    }

                }
                else
                {
                    lbldisp.Visible = true;
                    GridDisplay.Visible = false;
                }
            }
            else
            {
                lblall.Visible = true;
                lbldisp.Visible = false;
                GridDisplay.Visible = false;
            }
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected void LBbacktofront_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
    }
    protected void Amountcalcallchoice()
    {
        try
        {
            double sum = 0;
            isall = true;
            lbprevious.Visible = true;
            //Button2.Enabled = false;
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
            }
            int fiscyear = Start + 1;


            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                
                 if (DDchoice.SelectedItem.Value.ToString() == "3")
                {
                    if (isviewexcelReport == true)
                    {

                        Qrycondition = "with tbl as (select  Di.EVENT as EventName,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))   between '01/01/" + endall + "' and '12/31/" + Start + "' and Di.Event= '" + Session["EventIdall"] + "'";

                    }
                    else
                    {

                        Qrycondition = "with tbl as (select  Di.EVENT as EventName,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as  year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '01/01/" + endall + "' and '12/31/" + Start + "'and Di.Event= '" + Session["EventIdall"] + "' ";
                       
                    }
                }
                else
                {
                    if (isviewexcelReport == true)
                    {
                        Qrycondition = "with tbl as (select  Di.ChapterId,ch.ChapterCode,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))  as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))    between '01/01/" + endall + "' and '12/31/" + Start + "' and  Di.ChapterId= " + Session["chapall"] + "";
                    }
                    else
                    {
                        Qrycondition = "with tbl as (select  Di.ChapterId,ch.ChapterCode,di.memberid,Di.donortype,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,year(DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate)))    as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where  (case when  Convert(nvarchar,di.[DepositDate],101) is NULL then Convert(nvarchar,di.DonationDate,101)  else  Convert(nvarchar,di.[DepositDate],101) end)  between '01/01/" + endall + "' and '12/31/" + Start + "' and  Di.ChapterId= " + Session["chapall"] + " ";
                    }
                }
                Qry = Qrycondition + genWhereConditons();


            }
            else
            {
                Qryvaluewhere = genWhereConditons1();
                string WCNTQry = string.Empty;
                string WCNTQrynew = string.Empty;
                string CommonQry = string.Empty;

                for (i = endall; i <= Start; i++)
                {
                    fiscyear = i + 1;
                  
                   
                     if (DDchoice.SelectedItem.Value.ToString() == "3")
                    {
                        if (isviewexcelReport == true)
                        {
                            CommonQry = "select  Di.EVENT as EventName,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' and Di.Event= '" + Session["EventIdall"] + "' group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName, org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,di.DonorType";
                        }
                        else
                        {

                            CommonQry = "select  Di.EVENT as EventName,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' and Di.Event= '" + Session["EventIdall"] + "'  group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType";
                            
                        }
                    }
                    else
                    {
                        if (isviewexcelReport == true)
                        {
                            CommonQry = "select  Di.ChapterId,ch.ChapterCode,PubName=Case when DI.DonorType='OWN' then org.PubName else I.PubName  end,hPHONE=Case when DI.DonorType='OWN' then org.PHONE else I.HPhone  end,PHONE=Case when DI.DonorType='OWN' then org.PHONE else I.CPhone  end,eMAIL=Case when DI.DonorType='OWN' then org.EMAIL else I.Email  end,ADDRESS1=Case when DI.DonorType='OWN' then org.ADDRESS1 else I.Address1  end,CITY=Case when DI.DonorType='OWN' then org.CITY else I.City  end,STATE1=Case when DI.DonorType='OWN' then org.STATE else I.State  end,Zip=Case when DI.DonorType='OWN' then org.ZIP else I.Zip  end,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' and di.chapterid=" + Session["chapall"] + " group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName, org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,di.DonorType";
                        }
                        else
                        {
                            CommonQry = "select  Di.ChapterId,ch.ChapterCode,di.memberid,Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,Di.DonorType,sum(dI.AMOUNT)as amt,CASE when (min(year(Convert(nvarchar,di.DonationDate,101)))=" + fiscyear + ") then (max(year(Convert(nvarchar,di.DonationDate,101))))-1  else min (CONVERT(varchar(50),year(Convert(nvarchar,di.[DonationDate],101)))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID  left join chapter ch on ch.ChapterID=Di.ChapterId where DATEADD(dd, 0, DATEDIFF(dd, 0,di.DonationDate))  between '05/01/" + i + "' and '04/30/" + fiscyear + "' and di.chapterid=" + Session["chapall"] + " group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType";
                        }
                    }
                    if (WCNTQry != string.Empty)
                    {
                        //WCNTQrynew = "select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(di.DonationDate))=1) then (max(year(di.DonationDate)))-1  else min (CONVERT(varchar(50),year(di.DonationDate))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where Di.[DonationDate]    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType";
                        WCNTQrynew = CommonQry;
                        WCNTQry = WCNTQry + " union all " + WCNTQrynew;

                    }
                    else
                    {
                        // WCNTQry = "select  DI.MEMBERID,Di.donortype, Donorname=Case when DI.DonorType='OWN' then org.ORGANIZATION_NAME else (I.FirstName + ' ' + I.LastName)  end,sum(dI.AMOUNT)as amt,CASE when (min(year(di.DonationDate))=1) then (max(year(di.DonationDate)))-1  else min (CONVERT(varchar(50),year(di.DonationDate))) end as year from DonationsInfo DI Left join IndSpouse I on i.AutoMemberID=di.MEMBERID left join OrganizationInfo org on org.AutoMemberID=DI.MEMBERID left join chapter ch on ch.ChapterID=Di.ChapterId where Di.[DonationDate]    between '05/01/" + i + "' and '04/30/" + fiscyear + "' " + Qryvaluewhere + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType";
                        WCNTQry = CommonQry;

                    }
                }
                Qrycondition = "with tbl as (" + WCNTQry + " )" + genWhereConditons() + "";
                Qry = Qrycondition;
                fiscyear = i + 1;

            }


            dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtmerge = new DataTable();
            dtmerge = dsnew.Tables[0];

            dtnewgrid = dtmerge;
            DataRow dr = dtnewgrid.NewRow();

            if (DDchoice.SelectedItem.Value.ToString() == "3")
                    {
                       
                        if (isviewexcelReport == true)
                        {
                            dr[0] = "Total";
                            newvar = 12;
                        }
                        else
                        {
                           
                                dr[0] = "Total";
                                newvar = 4;
                            
                        }
                    }
                    else
                    {
                       
                        if (isviewexcelReport == true)
                        {
                            dr[1] = "Total";
                            newvar = 13;
                        }
                        else
                        {
                            dr[1] = "Total";
                            newvar = 5;
                        }
                    }

            //dr[4] = 0;
            
                //double sum = 0;
                for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    dr[i] = 0;
                    sum = 0;
                   
                    foreach (DataRow drnew in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {
                            string suma = "";
                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }

                        }
                    }
                    dr[i] = sum;
                }
           

           
          
            dtnewgrid.Rows.Add(dr);
            if (isviewexcelReport == true)
            {

                dtnewgrid.Columns.Add("total", typeof(String));
                for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                {
                    foreach (DataRow dr1 in dtnewgrid.Rows)
                    {
                        if (!DBNull.Value.Equals(dr1[i]))
                        {
                            string st;
                            double st1;
                            st1 = Convert.ToDouble(dr1[i]);
                            st = String.Format("{0:#,###0}", dr1["Total"]);

                            dr1[i] = String.Format("{0:#,###0}", st1);
                            // st = String.Format(CultureInfo.CreateSpecificCulture("da-DK"), "{0:00.000}", dr["Total"]);
                            dr1["total"] = st.ToString();

                        }
                    }
                }
                dtnewgrid.Columns.Remove("Total");
                dtnewgrid.AcceptChanges();
            }

           
           // Session["EXelEdit"] = dtnewgrid;
            GridDisplay.Visible = false;
             Gridchapterall.Visible = false;
            GridViewforall.Visible = false;
            GVindividual.Visible = true;
           // Session["pagingindi"] = dtnewgrid;
            if (DDchoice.SelectedItem.Value.ToString() == "2")
            {
                GVindividual.Visible = false;
                GridViewchapter.Visible = true;
                Session["allachap"] = dtnewgrid;
                GridViewchapter.DataSource = dtnewgrid;
                GridViewchapter.DataBind();
            }
            else
            {
                GVEventt.Visible = true;
                GVindividual.Visible = false;
                Session["allEvent"] = dtnewgrid;
                GVEventt.DataSource = dtnewgrid;
                GVEventt.DataBind();
            }
            isviewexcel = true;
            ddZone.Enabled = false;
            ddNoyear.Enabled = false;
            DDyear.Enabled = false;
            ddevent.Enabled = false;
            ddCluster.Enabled = false;
            ddchapter.Enabled = false;

            // Button2.Enabled = false;
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected string genWhereConditons()
    {
        string iCondtions = string.Empty;
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
            if (ddevent.SelectedItem.Text != "[Select Event]")
            {
                if (ddevent.SelectedItem.Text != "All")
                {
                    iCondtions += " and DI.EventID=" + ddevent.SelectedValue;
                }
            }
            if (ddZone.SelectedItem.Text != "[Select Zone]")
            {
                if (ddZone.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
            }
            if (ddCluster.SelectedItem.Text != "[Select Cluster]")
            {
                if (ddCluster.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
            }
            if (ddchapter.SelectedItem.Text != "[Select Chapter]")
            {
                if (ddchapter.SelectedItem.Text != "All")
                {
                    iCondtions += " and DI.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                }
            }
        }
        
        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }
        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        string Tot = string.Empty;
        string strtot = string.Empty;
        string Totwhere = string.Empty;
        for (int i = endall; i <= sta; i++)
        {
            
            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }
        }
        Totwhere = "(" + Tot + ")!=0";
       
                if (DDchoice.SelectedItem.Value.ToString() == "1")
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,  org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType) select MEMBERID,Donorname,donortype,PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            return iCondtions + " group by  year(Di.DepositDate),Di.DepositDate,di.DonationDate,Di.donortype,org.ORGANIZATION_NAME,I.FirstName + ' ' + I.LastName,year(DI.DonationDate),DI.MEMBERID,di.DonorType) select MEMBERID,Donorname,donortype " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                    }
                    else
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + "  select MEMBERID,Donorname, PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip,donortype " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            return iCondtions + "  select MEMBERID,Donorname,donortype " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                    }
                   
                }
                else if (DDchoice.SelectedItem.Value.ToString() == "3")
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + "group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName, org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City,org.PubName,I.PubName,org.ZIP,i.Zip,di.DonorType) select EventName,MEMBERID,Donorname,DonorType,PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                if (isall == true)
                                {
                                    return iCondtions + "group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType) select EventName,MEMBERID,Donorname,DonorType " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                                else
                                {
                                    return iCondtions + "group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate) select EventName " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                            }
                            else
                            {
                                return iCondtions + "group by Di.EVENT,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType) select EventName,MEMBERID,Donorname,DonorType " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                            }
                        }
                    }
                    else
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + " select EventName,MEMBERID,Donorname,DonorType,PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                if (isall == true)
                                {
                                    return iCondtions + " select EventName,MEMBERID,Donorname,DonorType " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                                else
                                {
                                    return iCondtions + " select EventName " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                            }
                            else
                            {
                                return iCondtions + " select EventName,MEMBERID,Donorname,DonorType " + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                            }
                        }
                    }
                }
                else
                {
                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + "group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), org.PubName,I.PubName,org.ZIP,i.Zip,org.PHONE,org.STATE,i.State,org.ADDRESS1,i.Address1,org.EMAIL,i.Email,i.HPhone,i.CPhone,org.CITY,i.City, year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType) select ChapterId,ChapterCode,MEMBERID,Donorname,DonorType,PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                if (isall == true)
                                {
                                    return iCondtions + "group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType) select ChapterId,ChapterCode,MEMBERID,Donorname,DonorType" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                                else
                                {
                                    return iCondtions + "group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate) select ChapterId,ChapterCode" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                            }
                            else
                            {

                                return iCondtions + "group by Di.ChapterId,ch.ChapterCode,year(DI.DonationDate), year(Di.DepositDate),Di.DepositDate,di.DonationDate,DI.MEMBERID,org.ORGANIZATION_NAME,i.FirstName,i.LastName,di.DonorType) select ChapterId,ChapterCode,MEMBERID,Donorname,DonorType" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                            }
                        }
                    }
                    else
                    {
                        if (isviewexcelReport == true)
                        {
                            return iCondtions + " select ChapterId,ChapterCode,MEMBERID,Donorname,PubName,hPHONE,PHONE,eMAIL,ADDRESS1,CITY,STATE1,Zip,DonorType" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                        }
                        else
                        {
                            if (ddchapter.SelectedItem.Text == "All")
                            {
                                if (isall == true)
                                {
                                    return iCondtions + " select ChapterId,ChapterCode,MEMBERID,Donorname,DonorType" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                                else
                                {
                                    return iCondtions + " select ChapterId,ChapterCode" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                                }
                            }
                            else
                            {
                                return iCondtions + " select ChapterId,ChapterCode,MEMBERID,Donorname,DonorType" + am + ",'Total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by Total desc";
                            }
                        }
                    }

                }
      
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
          
            buttonclick();
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
        
    }
    protected string genWhereConditons1()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and DI.EventID=" + ddevent.SelectedValue;
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and DI.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }
       
            return iCondtions ;
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField k = (HiddenField)GridDisplay.Rows[index].Cells[0].FindControl("Hdnmemberid");
            ExpmemberID = Convert.ToInt32(k.Value);
            Session["MemberID"] = ExpmemberID;
            Amountcalc();
            isviewexcel = true;
           // Button2.Enabled = false;
           
            
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected void lnkviewEvent_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField k1 = (HiddenField)GVEventt.Rows[index].Cells[0].FindControl("Hdnmember");
            HiddenField k = (HiddenField)GVEventt.Rows[index].Cells[0].FindControl("HdnEvent");
            ExpmemberID = Convert.ToInt32(k1.Value);

            Session["MemberID"] = ExpmemberID;
            StrEvent = k.Value;
            //ExEventID = Convert.ToInt32(k.Value);
            Session["Eventname"] = StrEvent;
            Amountcalc();
            isviewexcel = true;


        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected void lnkviewEventall_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
             if (DDchoice.SelectedItem.Value.ToString() == "3")
            {
            HiddenField k1 = (HiddenField)GridViewforall.Rows[index].Cells[0].FindControl("HdnEventidall");
            //HiddenField k = (HiddenField)GVEventt.Rows[index].Cells[0].FindControl("HdnEventidall");
            //ExEventID = Convert.ToInt32(k.Value);
            StrEvent = k1.Value;
            //ExEventID = Convert.ToInt32(k1.Value);

            Session["EventIdall"] = StrEvent;
            }
            else
            {
                HiddenField k1 = (HiddenField)Gridchapterall.Rows[index].Cells[0].FindControl("Hdnchaptidall");
                //HiddenField k = (HiddenField)GVEventt.Rows[index].Cells[0].FindControl("HdnEventidall");
                ExpmemberID = Convert.ToInt32(k1.Value);

                Session["chapall"] = ExpmemberID;
            }
           // StrEvent = k.Value;
            //ExEventID = Convert.ToInt32(k.Value);
            ///Session["Eventname"] = StrEvent;
            Amountcalcallchoice();

            //isviewexcel = true;
            isall = true;
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected void lnkviewChapter_Click(object sender, EventArgs e)
    {
        try
        {
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField k = (HiddenField)GridViewchapter.Rows[index].Cells[0].FindControl("Hdnchapterid");
            HiddenField k1 = (HiddenField)GridViewchapter.Rows[index].Cells[0].FindControl("Hdnmember");
            Exchapteridmember = Convert.ToInt32(k1.Value);
            Exchapterid = Convert.ToInt32(k.Value);
            Session["chapterID"] = Exchapterid;
            Session["Memberid"] = Exchapteridmember;
            Amountcalc();
           
        }
        catch (Exception err)
        {
            // lblMessage.Text = err.Message;
        }
    }
    protected void GridDisplay_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;

        for (int i = 4; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                // e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnview");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnmemberid");
            if (hdnid.Value == "") 
            {
                dpEmpdept.Visible = false;
            }
        }
       
    }
    protected void GVindividual_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowIndex == -1) return;
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                neval = 5;

            }
            else if (DDchoice.SelectedItem.Value.ToString() == "3")
            {
                neval = 6;

            }
            else
            {
                neval = 7;

            }
            string st = Convert.ToString(((DataRowView)(e.Row.DataItem))[neval]);
            object y = ((DataRowView)(e.Row.DataItem))[neval];
            if ((!string.IsNullOrEmpty(st) && (((DataRowView)(e.Row.DataItem))[5] != "&nbsp;")))
            {
                st = String.Format("{0:#,###0}", Convert.ToDouble(st)).ToString();
                e.Row.Cells[6].Text = st;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button dpEmpdept1= (Button)e.Row.FindControl("btnEdit");
                HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnmemberdedit");
                if (hdnid.Value == "")
                {
                    dpEmpdept1.Visible = false;
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void lbprevious_Click(object sender, EventArgs e)
    {
        ddZone.Enabled = true;
        ddNoyear.Enabled = true;
        DDyear.Enabled = true;
        ddevent.Enabled = true;
        ddCluster.Enabled = true;
        ddchapter.Enabled = true;
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            GVindividual.Visible = false;
            GridDisplay.Visible = true;
            lbprevious.Visible = false;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "3")
        {
            if (ddchapter.SelectedItem.Text == "All")
            {
                if (GVEventt.Visible == true)
                {
                    GVEventt.Visible = false;
                    GridViewforall.Visible = true;
                    lbprevious.Visible = false;
                }
                else
                {
                    GVEventt.Visible = true;
                    GVindividual.Visible = false;

                    ddZone.Enabled = false;
                    ddNoyear.Enabled = false;
                    DDyear.Enabled = false;
                    ddevent.Enabled = false;
                    ddCluster.Enabled = false;
                    ddchapter.Enabled = false;

                }
            }
            else
            {
               
                    GVindividual.Visible = false;
                    GVEventt.Visible = true;
                    lbprevious.Visible = false;
               
            }
        }
        else
        {
            if (ddchapter.SelectedItem.Text == "All")
            {
                if (GridViewchapter.Visible == true)
                {
                    lbprevious.Visible = true;
                    GridViewchapter.Visible = false;
                    Gridchapterall.Visible = true;
                    lbprevious.Visible = false;
                }
                else
                {
                    GridViewchapter.Visible = true;
                    GVindividual.Visible = false;

                    ddZone.Enabled = false;
                    ddNoyear.Enabled = false;
                    DDyear.Enabled = false;
                    ddevent.Enabled = false;
                    ddCluster.Enabled = false;
                    ddchapter.Enabled = false;

                }
            }
            else
            {
                GVindividual.Visible = false;
                GridViewchapter.Visible = true;
                lbprevious.Visible = false;
            }
        }
    }
    protected void GVEventt_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;

        for (int i = 5; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                // e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnviewEvent");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnmember");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }

    }
    protected void GridViewchapter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        //string st = e.Row.Cells[6].Text;
        for (int i = 6; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                // e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnviewchapter");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnmember");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }

    }
    protected void GridDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridDisplay.PageIndex = e.NewPageIndex;
            //if (isviewexcelReport == true)
            //{
            //    isviewexcelReport = false;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Griddonor"];
            if (dtex == null)
            {
                buttonclick();
            }
            
            GridDisplay.DataSource = (DataTable)Session["Griddonor"];
            GridDisplay.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GVindividual_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GVindividual.PageIndex = e.NewPageIndex;
            GVindividual.DataSource = (DataTable)Session["pagingindi"];
            GVindividual.DataBind();
        }
        catch (Exception ex) { }

    }
    protected void GVEventt_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GVEventt.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            if (ddchapter.SelectedItem.Text == "All")
            {

                dtex = (DataTable)Session["allEvent"];
                if (dtex == null)
                {
                    buttonclick();
                }
                GVEventt.DataSource = (DataTable)Session["allEvent"];
            }
            else
            {
                dtex = (DataTable)Session["GridEvent"];
                if (dtex == null)
                {
                    buttonclick();
                }
                GVEventt.DataSource = (DataTable)Session["GridEvent"];
            }
            GVEventt.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GVindividual_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GVindividual.EditIndex = e.NewEditIndex;
     
       Amountcalc();
       // bindgrid();
    }
    protected void GVindividual_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GVindividual.EditIndex = -1;
        Amountcalc();
       // bindgrid();
    }
    protected void GridViewchapter_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewchapter.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            if (ddchapter.SelectedItem.Text == "All")
            {
                dtex = (DataTable)Session["allachap"];
                if (dtex == null)
                {
                    Amountcalcallchoice();
                }
                GridViewchapter.DataSource = (DataTable)Session["allachap"];

            }
            else
            {
                dtex = (DataTable)Session["Gridchapter"];
                if (dtex == null)
                {
                    buttonclick();
                }
                GridViewchapter.DataSource = (DataTable)Session["Gridchapter"];
            }
            GridViewchapter.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void GVindividual_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {

            string Qrychapter;
            string chcode;
            string frmchcode;
            string bank;
            string chapter;
            string purpose;
            string year;
            string Event;
            string s = GVindividual.DataKeys[e.RowIndex].Value.ToString();
           // TextBox member = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("lblmembid");
            TextBox Txtdonationtype = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("Txtdonationtype");
            TextBox Txtbank = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("Txtbankdep");
            TextBox TxtfrmChaptercode = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("TxtfrmChaptercode");
            TextBox Txtpurpose = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("Txtpurpose");
            TextBox TxtEddyear = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("TxtEddyear");
            TextBox TxtEvente = (TextBox)GVindividual.Rows[e.RowIndex].FindControl("TxtEvente");
            
            DropDownList ddyear = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("ddyear");
            DropDownList DDEvent1 = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("DDEvent1");
            DropDownList DDpurpose = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("DDpurpose");
            DropDownList DDfrmchaptercode = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("DDfrmchaptercode");
            DropDownList DDfbank = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("DDfbank");
            DropDownList dddonationtype = (DropDownList)GVindividual.Rows[e.RowIndex].FindControl("DDdonationtype");
            if (dddonationtype.SelectedIndex == 0)
            {
                frmchcode = Txtdonationtype.Text;
            }
            else
            {
                frmchcode = dddonationtype.SelectedItem.Text;
            }
            if (DDfbank.SelectedIndex == 0)
            {
                bank = Txtbank.Text;
            }
            else
            {
                bank = DDfbank.SelectedValue;
            }

            if (DDfrmchaptercode.SelectedIndex == 0)
            {
                chapter = TxtfrmChaptercode.Text;
            }
            else
            {
                chapter = DDfrmchaptercode.SelectedItem.Text;
            }
            Qrychapter = "Select ChapterID from chapter where chaptercode='" + chapter.ToString() + "'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrychapter);
            DataTable dt = ds.Tables[0];
            Txthidden.Text = dt.Rows[0]["ChapterID"].ToString();
            if (DDpurpose.SelectedIndex == 0)
            {
                purpose = TxtfrmChaptercode.Text;
            }
            else
            {
                purpose = DDpurpose.SelectedItem.Text;
            }
            if (ddyear.SelectedIndex == 0)
            {
                year = TxtEddyear.Text;
            }
            else
            {
                year = ddyear.SelectedItem.Text;
            }

            if (DDEvent1.SelectedIndex == 0)
            {
                Event = TxtEvente.Text;
            }
            else
            {
                Event = DDEvent1.SelectedItem.Text;
            }

            if (bank == "")
            {
                bank = "0";
            }

            string qry = "update DonationsInfo set DonationType='" + frmchcode + "', BankID='" + bank + "',PURPOSE='" + purpose + "',EVENT='" + Event + "',EventYear=" + year + ",chapterid=" + Txthidden.Text + "  where TRANSACTION_NUMBER='" + s + "'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qry);
            Response.Write("<script>alert('Updated successfully')</script>");
            GVindividual.EditIndex = -1;
            Amountcalc();
            GVindividual.Visible = true;
        }
        catch (Exception err)
        {
           // lblMessage.Text = err.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            isviewexcelReport = true;
            DateTime now = DateTime.Now;
            string yearshort;
            string month = now.ToString("MMM");
            string day = now.ToString("dd");
            string all = string.Concat(month, day);
            // string date = all+StaYear;
            string date = all + "_" + StaYear;
            lbdate.Text = date;
            if (DDyear.SelectedItem.Text != "Fiscal Year")
            {
                yearshort = "CY";
            }
            else
            {
                yearshort = "FY";
            }
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (lbprevious.Visible == true)
                {

                    Amountcalc();
                    GeneralExport((DataTable)Session["EXelEdit"], "DonorByIndividual_" + yearshort + "_" + lbdate.Text + ".xls");
                }
                else
                {

                    buttonclick();
                    GeneralExport((DataTable)Session["GriddonorExcel"], "DonorList_" + yearshort + "_" + lbdate.Text + ".xls");
                }
            
               
            }
            else if (DDchoice.SelectedItem.Value.ToString() == "2")
            {
                if (lbprevious.Visible == true)
                {
                    if ((ddchapter.SelectedItem.Text == "All") )
                    {
                       
                        
                            if (GVindividual.Visible == true)
                            {
                                Amountcalc();
                                GeneralExport((DataTable)Session["EXelEdit"], "ChaptersByIndividual_" + yearshort + "_" + lbdate.Text + ".xls");
                             
                            }
                       
                        
                        else
                        {
                            Amountcalcallchoice();
                            GeneralExport((DataTable)Session["allachap"], "ChaptersByDonorList_" + yearshort + "_" + lbdate.Text + ".xls");
                            
                        }
                    }
                    else
                    {
                        Amountcalc();
                        GeneralExport((DataTable)Session["EXelEdit"], "ChaptersByIndividual_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                }
                else
                {
                    buttonclick();
                    if (ddchapter.SelectedItem.Text == "All")
                    {
                        GeneralExport((DataTable)Session["Pagingchapter"], "ChaptersList_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                    else
                    {
                        // dtnewgrid = (DataTable)Session["GridchapterExcel"];
                        GeneralExport((DataTable)Session["GridchapterExcel"], "ChaptersList_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                    //GeneralExport(dtnewgrid, "ChaptersList.xls");

                }

            }
            else
            {
                if (lbprevious.Visible == true)
                {
                    if ((ddchapter.SelectedItem.Text == "All") )
                    {

                        if (GVindividual.Visible == true)
                        {
                            Amountcalc();
                            GeneralExport((DataTable)Session["EXelEdit"], "EventByIndividual_" + yearshort + "_" + lbdate.Text + ".xls");
                        }
                        else
                        {

                            Amountcalcallchoice();
                            GeneralExport((DataTable)Session["allEvent"], "EventByDonorList_" + yearshort + "_" + lbdate.Text + ".xls");
                        }
                        
                    }
                    else
                    {
                        Amountcalc();
                        GeneralExport((DataTable)Session["EXelEdit"], "EventByIndividual_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                }
                else
                {
                    buttonclick();
                    if (ddchapter.SelectedItem.Text == "All")
                    {
                        GeneralExport((DataTable)Session["Pagingevent"], "EventList_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                    else
                    {
                        GeneralExport((DataTable)Session["GridEventExcel"], "EventList_" + yearshort + "_" + lbdate.Text + ".xls");
                    }
                   // dtnewgrid = (DataTable)Session["GridEventExcel"];
                }
                GeneralExport(dtnewgrid, "EventList_" + yearshort + "_" + lbdate.Text + ".xls");
            }
        }
        catch(Exception err)
        {
            Response.Write(err);
        }
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
                //sep = ";";
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {

                    Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                }
                Response.Write("\n");
            }
         
           // Response.Write(sw.ToString());
            //response.End()                           //       Thread was being aborted.
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void GridViewforall_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;

        for (int i = 2; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                // e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnviewEventall");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("HdnEventidall");
            if (hdnid.Value == "Total")
            {
                dpEmpdept.Visible = false;
            }
        }

    }
    protected void Gridchapterall_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Gridchapterall.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Pagingchapter"];
            if (dtex == null)
            {
                buttonclick();
            }
            Gridchapterall.DataSource = (DataTable)Session["Pagingchapter"];
            Gridchapterall.DataBind();
        }
        catch (Exception ex) { }

    }
    protected void Gridchapterall_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;

        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[i].Text = String.Format("{0:#,###0}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
                // e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button dpEmpdept = (Button)e.Row.FindControl("btnviewchapttall");
            HiddenField hdnid = (HiddenField)e.Row.FindControl("Hdnchaptidall");
            if (hdnid.Value == "")
            {
                dpEmpdept.Visible = false;
            }
        }

    }
    protected void GridViewforall_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewforall.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["Pagingevent"];
            if (dtex == null)
            {
                buttonclick();
            }
            GridViewforall.DataSource = (DataTable)Session["Pagingevent"];
            GridViewforall.DataBind();
        }
        catch (Exception ex) { }

    }
}
