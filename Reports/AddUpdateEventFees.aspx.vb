﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports VRegistration
Imports System.Globalization

Partial Class AddUpdateEventFees
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoginId") = 4240
        'Session("entry") = "v"
        'Session("LoggedIn") = "true"

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            'Dim year As Integer = 0
            'year = Convert.ToInt32(DateTime.Now.Year) + 1
            'For i As Integer = 0 To 6
            '    ddlEventYear.Items.Insert(i, New ListItem(year, year))
            '    ddlPropYear.Items.Insert(i, New ListItem(year, year))
            '    year = year - 1
            'Next
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year) + 1
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))
            ddlPropYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))
            For i As Integer = 0 To 6
                ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))
                ddlPropYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))
            Next
            ddlPropYear.Items(0).Selected = True
            ddlEventYear.Items(1).Selected = True

            LoadEvent("3, 4, 13, 19,20") ' Workshop-3, Game-4 , Coaching-13 ,PrepClub-19
        End If
    End Sub
    Private Sub LoadEvent(ByVal EventIDs As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventID,EventCode,Name From Event Where EventID in (" & EventIDs & ")")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, "Select Event")
    End Sub
    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function
    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function
    Function getEventcode(ByVal EventID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Eventcode from Event where EventID=" & EventID & "")
    End Function
    Private Sub LoadProductGroup()
        If Not ddlEvent.SelectedItem.Text = "Select Event" Then
            ddlProduct.Items.Clear()
            Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup WHERE EventId=" & ddlEvent.SelectedValue & "  order by ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()
            If ddlProductGroup.Items.Count < 1 Then
                lblErr.Text = "No Product is opened."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, New ListItem("Select Product Group", "0"))
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
            End If
        End If
    End Sub

    Private Sub LoadClasse()
        ddClasses.Items.Insert(0, New ListItem("Select", 0))
        For i As Integer = 1 To 20
            ddClasses.Items.Insert(i, New ListItem(i.ToString(), i))
        Next
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try
                strSql = "Select ProductID, Name from Product where EventID=" & ddlEvent.SelectedValue & " and ProductGroupID =" & ddlProductGroup.SelectedValue & " order by ProductID"
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product"))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                End If
            Catch ex As Exception
                lblErr.Text = ex.ToString
            End Try
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProduct.Items.Clear()
        LoadProductID()
        lblErr.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Add new Event
        lblError.ForeColor = Color.Green
        lblError.Text = ""
        lblErr.Text = ""
        If ddlProductGroup.SelectedValue = "0" Then
            lblErr.Text = "Please Select a Product Group"
            Exit Sub
        ElseIf ddlProduct.SelectedValue = "0" Then
            lblErr.Text = "Please Select a Product"
            Exit Sub
        ElseIf ddlGradeTo.SelectedValue = "0" Then
            lblErr.Text = "Please Select Valid Grade for Grade to"
            Exit Sub
        ElseIf Val(txtRegFee.Text) < 1 Then
            lblErr.Text = "Please Enter Valid Reg. Fee"
            Exit Sub
        ElseIf Val(txtTax.Text) < 1 Then
            lblErr.Text = "Please Enter Tax Deduction Amount"
            Exit Sub
        ElseIf txtDeadLineDate.Text = "" And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Tax Deadline Date"
            Exit Sub
        ElseIf txtLateRegDLineDate.Text = "" And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Late Reg Deadline Date"
            Exit Sub
        ElseIf txtChangeCoachDL.Text = "" And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Change Coach Deadline Date"
            Exit Sub
        ElseIf (txtCapacity.Text = "0" Or txtCapacity.Text = "") And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Valid Capacity"
            Exit Sub
        ElseIf ddlDuration.SelectedIndex = 0 And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Duration"
            Exit Sub
        ElseIf ddlFrequency.SelectedIndex = 0 And ddlEvent.SelectedValue = "13" Then
            lblErr.Text = "Please Enter Frequency"
            Exit Sub
        Else
            lblErr.Text = ""
        End If
        Dim sqlstr As String = ""
        Try
            If btnAdd.Text = "Add" Then
                'Add New
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from EventFees where EventYear=" & ddlEventYear.Text & " AND EventID=" & ddlEvent.SelectedValue & " AND ProductGroupID =" & ddlProductGroup.SelectedValue & " AND ProductID = " & ddlProduct.SelectedValue & "") < 1 Then
                    sqlstr = "INSERT INTO EVENTFEES(EventID, EventCode, EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo, DeadlineDate, LateRegDLDate ,ChangeCoachDL,RegFee, LateFee, TaxDedRegional,ProductLevelPricing, Semester, CoachSelCriteria,DiscountAmt,Capacity,Duration,Frequency, CreateDate, CreatedBy,Classes,Licenses) VALUES("
                    sqlstr = sqlstr & ddlEvent.SelectedValue & ",'" & getEventcode(ddlEvent.SelectedValue) & "'," & ddlEventYear.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "'," & ddlGradeFrom.SelectedValue & "," & ddlGradeTo.SelectedValue & "," & IIf(TrDlineDate.Visible = False, "NULL", "'" & txtDeadLineDate.Text & "'") & "," & IIf(TrLateDlineDate.Visible = False, "NULL", "'" & txtLateRegDLineDate.Text & "'") & "," & IIf(TrChangeCoachDL.Visible = False, "NULL", "'" & txtChangeCoachDL.Text & "'") & "," & txtRegFee.Text & "," & txtlateFee.Text & "," & txtTax.Text & ",'" & ddlPricing.SelectedValue & "','" & ddlSemester.SelectedValue & "','" & ddlGradeTest.SelectedValue & "'," & IIf(txtDiscount.Text = "", "NULL", txtDiscount.Text) & "," & IIf(TrCapacity.Visible = True, txtCapacity.Text, "NULL") & "," & IIf(TrDuration.Visible = True, ddlDuration.SelectedValue, "NULL") & "," & IIf(TrFrequency.Visible = True, "'" & ddlFrequency.SelectedValue & "'", "NULL") & ",GetDate()," & Session("LoginID") & "," & IIf(ddClasses.SelectedValue = 0, "NULL", ddClasses.SelectedValue) & ", " & IIf(ddlLicenses.SelectedValue = 0, "NULL", ddlLicenses.SelectedValue) & ")"
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr) > 0 Then
                        lblError.Text = "Added Sucessfully"
                    End If
                    clear()
                    traddUpdate.Visible = False
                    traddUpdate1.Visible = False
                    divAddUpdate.Visible = False
                    loadgrid()
                Else
                    lblErr.Text = "This record already Exist"
                End If
            ElseIf lblEventFeesID.Text.Length > 0 Then
                'Update Event
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from EventFees where EventYear=" & ddlEventYear.Text & " AND EventID=" & ddlEvent.SelectedValue & " AND ProductGroupID =" & ddlProductGroup.SelectedValue & " AND ProductID = " & ddlProduct.SelectedValue & " AND EventFeesID not in (" & lblEventFeesID.Text & ")") < 1 Then
                    Dim iCnt As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from CoachingDateCal where EventYear=" & ddlEventYear.Text & " AND ProductId=" & ddlProduct.SelectedValue & " and EndDate>GetDate() and Semester <>'" & ddlSemester.SelectedItem.Text & "'")
                    If iCnt > 0 Then
                        Dim semester As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select top 1 isnull(semester,'') Semester  from CoachingDateCal where EventYear=" & ddlEventYear.Text & " AND ProductId=" & ddlProduct.SelectedValue & " and EndDate>GetDate() and Semester <>'" & ddlSemester.SelectedItem.Text & "'")
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ValidateSemester('" & ddlSemester.SelectedValue & "','" & ddlProduct.SelectedItem.Text & "','" & semester & "');", True)
                    Else
                        btnUpdate_Click(btnUpdate, New EventArgs)
                    End If
                Else
                    lblErr.Text = "This record already Exist, So you can't update"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim sqlstr As String = "Update EVENTFEES SET ProductGroupID =" & ddlProductGroup.SelectedValue & ", ProductGroupCode='" & getProductGroupcode(ddlProductGroup.SelectedValue) & "', ProductID=" & ddlProduct.SelectedValue & ", ProductCode='" & getProductcode(ddlProduct.SelectedValue) & "', GradeFrom=" & ddlGradeFrom.SelectedValue & ", GradeTo=" & ddlGradeTo.SelectedValue & ",DeadlineDate=" & IIf(TrDlineDate.Visible = False, "NULL", "'" & txtDeadLineDate.Text & "'") & ", LateRegDLDate=" & IIf(TrLateDlineDate.Visible = False, "NULL", "'" & txtLateRegDLineDate.Text & "'") & ", ChangeCoachDL=" & IIf(TrChangeCoachDL.Visible = False, "NULL", "'" & txtChangeCoachDL.Text & "'") & ", RegFee=" & txtRegFee.Text & ""
        sqlstr = sqlstr & ", LateFee=" & txtlateFee.Text & ", TaxDedRegional=" & txtTax.Text & ",ProductLevelPricing='" & ddlPricing.SelectedValue & "', Semester='" & ddlSemester.SelectedValue & "',CoachSelCriteria='" & ddlGradeTest.SelectedValue & "',DiscountAmt=" & txtDiscount.Text & IIf(TrCapacity.Visible = True, ",Capacity= " & txtCapacity.Text, "") & IIf(TrDuration.Visible = True, ",Duration= " & ddlDuration.SelectedValue, "") & IIf(TrFrequency.Visible = True, ",Frequency= '" & ddlFrequency.SelectedValue & "'", "") & ",ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Classes=" & IIf(ddClasses.SelectedValue = 0, "NULL", ddClasses.SelectedValue) & ", Licenses=" & IIf(ddlLicenses.SelectedValue = 0, "NULL", ddlLicenses.SelectedValue) & "  WHERE EventFeesID = " & lblEventFeesID.Text
        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr) > 0 Then
            lblError.Visible = True
            lblError.Text = "Updated Sucessfully"
        End If
        clear()
        traddUpdate.Visible = False
        traddUpdate1.Visible = False
        divAddUpdate.Visible = False
        loadgrid()
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If Not ddlEvent.SelectedItem.Text = "Select Event" Then
            traddUpdate.Visible = False
            traddUpdate1.Visible = False
            divAddUpdate.Visible = False
            If ddlEvent.SelectedValue = "13" Then
                Dim yr As String = Convert.ToString(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString())
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(yr))
                ddlPropYear.SelectedIndex = ddlPropYear.Items.IndexOf(ddlPropYear.Items.FindByValue(yr + 1))
            End If
            loadgrid()
            LoadProductGroup()

        End If
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlEvent.SelectedItem.Text = "Select Event" Then
            loadgrid()
        End If
    End Sub

    Private Sub DGEventFees_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGEventFees.ItemCreated
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.FindControl("lbtnRemove"), LinkButton) '(e.Item.Cells(1).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub DGEventFees_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim EventFeesID As Integer = CInt(e.Item.Cells(3).Text)
        lblError.Text = ""
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from EventFees Where EventFeesID=" & EventFeesID & "")
                loadgrid()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            btnAdd.Text = "Update"
            lblEventFeesID.Text = EventFeesID.ToString()
            LoadClasse()
            loadforUpdate(EventFeesID)
        End If
    End Sub

    Private Sub loadforUpdate(ByVal EventFeesID As Integer)
        ' For update 
        If ddlEvent.SelectedValue = "13" Then 'Coaching
            TrDlineDate.Visible = True
            TrLateDlineDate.Visible = True
            TrChangeCoachDL.Visible = True
            TrCapacity.Visible = True
            TrDuration.Visible = True
            TrFrequency.Visible = True
        Else
            TrDlineDate.Visible = False
            TrLateDlineDate.Visible = False
            TrChangeCoachDL.Visible = False
            TrCapacity.Visible = False
            TrDuration.Visible = False
            TrFrequency.Visible = False
        End If
        traddUpdate.Visible = True
        traddUpdate1.Visible = True
        divAddUpdate.Visible = True
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * from EventFees Where EventFeesID=" & EventFeesID & "")
        ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(ds.Tables(0).Rows(0)("ProductGroupID")))
        LoadProductID()
        ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(ds.Tables(0).Rows(0)("ProductID")))
        ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByValue(ds.Tables(0).Rows(0)("GradeFrom")))
        ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByValue(ds.Tables(0).Rows(0)("GradeTo")))
        ddlPricing.SelectedIndex = ddlPricing.Items.IndexOf(ddlPricing.Items.FindByValue(ds.Tables(0).Rows(0)("ProductLevelPricing")))
        If Not IsDBNull(ds.Tables(0).Rows(0)("CoachSelCriteria")) Then
            ddlGradeTest.SelectedIndex = ddlGradeTest.Items.IndexOf(ddlGradeTest.Items.FindByValue(ds.Tables(0).Rows(0)("CoachSelCriteria")))
        Else
            ddlGradeTest.SelectedIndex = ddlGradeTest.Items.IndexOf(ddlGradeTest.Items.FindByValue("O"))
        End If
        txtDeadLineDate.Text = IIf(ds.Tables(0).Rows(0)("DeadlineDate") Is DBNull.Value, "", ds.Tables(0).Rows(0)("DeadlineDate"))
        txtLateRegDLineDate.Text = IIf(ds.Tables(0).Rows(0)("LateRegDLDate") Is DBNull.Value, "", ds.Tables(0).Rows(0)("LateRegDLDate"))
        txtChangeCoachDL.Text = IIf(ds.Tables(0).Rows(0)("ChangeCoachDL") Is DBNull.Value, "", ds.Tables(0).Rows(0)("ChangeCoachDL"))
        txtRegFee.Text = Math.Round(Convert.ToDecimal(ds.Tables(0).Rows(0)("RegFee")), 2)
        txtlateFee.Text = Math.Round(Convert.ToDecimal(ds.Tables(0).Rows(0)("LateFee")), 2)
        txtTax.Text = Math.Round(Convert.ToDecimal(ds.Tables(0).Rows(0)("TaxDedRegional")), 2)
        txtDiscount.Text = Math.Round(Convert.ToDecimal(IIf(ds.Tables(0).Rows(0)("DiscountAmt") Is DBNull.Value, "0.00", ds.Tables(0).Rows(0)("DiscountAmt"))), 2)
        txtCapacity.Text = Math.Round(Convert.ToDecimal(IIf(ds.Tables(0).Rows(0)("Capacity") Is DBNull.Value, "0", ds.Tables(0).Rows(0)("Capacity"))), 2)
        ddlDuration.SelectedIndex = ddlDuration.Items.IndexOf(ddlDuration.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Duration") Is DBNull.Value, "0.0", ds.Tables(0).Rows(0)("Duration"))))
        ddlFrequency.SelectedIndex = ddlFrequency.Items.IndexOf(ddlFrequency.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Frequency") Is DBNull.Value, "0.0", ds.Tables(0).Rows(0)("Frequency"))))

        ddClasses.SelectedIndex = ddClasses.Items.IndexOf(ddClasses.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Classes") Is DBNull.Value, "0.0", ds.Tables(0).Rows(0)("Classes"))))

        ddlSemester.SelectedIndex = ddlSemester.Items.IndexOf(ddlSemester.Items.FindByValue(ds.Tables(0).Rows(0)("Semester")))
    End Sub

    Private Sub loadgrid()
        Dim strSQL As String = "Select EF.EventFeesID, EF.EventID,E.Name as EventName,Ef.Classes, EF.EventYear,EF.CoachSelCriteria, EF.ProductGroupID,PG.Name as ProductGroupName , EF.ProductID, PG.ProductGroupCode as ProductGroupCode,P.ProductCode as ProductCode, EF.GradeFrom, EF.GradeTo,CONVERT(VARCHAR(10), EF.DeadlineDate, 101) AS DeadlineDate,CONVERT(VARCHAR(10), EF.LateRegDLDate, 101) AS LateRegDLDate,EF.RegFee, EF.LateFee,EF.TaxDedRegional,EF.DiscountAmt,EF.ProductLevelPricing,EF.Semester,CONVERT(VARCHAR(10), EF.ChangeCoachDL, 101) AS ChangeCoachDL,IsNull(EF.Capacity,0) as Capacity,IsNull(EF.Duration,0.0) as Duration,IsNull(EF.Frequency,'') as Frequency,EF.Licenses from EventFees EF INNER JOIN Event E ON E.EventId = EF.EventID INNER JOIN ProductGroup PG ON PG.ProductGroupId = EF.ProductGroupID Inner Join Product P ON P.ProductId  = EF.ProductID WHERE EF.EventID= " & ddlEvent.SelectedValue & " and EF.EventYear = " & ddlEventYear.SelectedValue & " order by P.ProductID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        DGEventFees.DataSource = dt
        DGEventFees.DataBind()
        btnConfirmUpgrade.Visible = False
        If (dt.Rows.Count > 0) Then
            btnConfirmUpgrade.Visible = True
        End If
        If ddlEvent.SelectedValue = "13" Then
            DGEventFees.Columns(13).Visible = True
            DGEventFees.Columns(14).Visible = True
            DGEventFees.Columns(15).Visible = True
            DGEventFees.Columns(16).Visible = True
            DGEventFees.Columns(17).Visible = True
            DGEventFees.Columns(18).Visible = True
            DGEventFees.Columns(19).Visible = True
            DGEventFees.Columns(20).Visible = True
        Else
            DGEventFees.Columns(13).Visible = False
            DGEventFees.Columns(14).Visible = False
            DGEventFees.Columns(15).Visible = False
            DGEventFees.Columns(16).Visible = False
            DGEventFees.Columns(18).Visible = False
            DGEventFees.Columns(19).Visible = False
            DGEventFees.Columns(20).Visible = True
        End If
        If (Count < 1) Then
            lblErr.Text = "No record available."
        Else
            lblErr.Text = ""
            lblNotemsg.Visible = True
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        'add new button
        clear()
        LoadClasse()
        If ddlEvent.SelectedItem.Text = "Select Event" Then
            traddUpdate.Visible = False
            traddUpdate1.Visible = False
            divAddUpdate.Visible = False
            lblError.Text = "Please select Event"
        Else
            lblError.Text = ""
            traddUpdate.Visible = True
            traddUpdate1.Visible = True
            divAddUpdate.Visible = True
        End If
        If ddlEvent.SelectedValue = "13" Then 'Coaching
            TrDlineDate.Visible = True
            TrLateDlineDate.Visible = True
            TrChangeCoachDL.Visible = True
            TrCapacity.Visible = True
            TrDuration.Visible = True
            TrFrequency.Visible = True
        Else
            TrDlineDate.Visible = False
            TrLateDlineDate.Visible = False
            TrChangeCoachDL.Visible = False
            TrCapacity.Visible = False
            TrDuration.Visible = False
            TrFrequency.Visible = False
        End If
    End Sub

    Private Sub clear()
        btnAdd.Text = "Add"
        lblEventFeesID.Text = ""
        ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByValue("0"))
        ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByValue("0"))
        ddlPricing.SelectedIndex = ddlPricing.Items.IndexOf(ddlPricing.Items.FindByValue("0"))
        ddlGradeTest.SelectedIndex = ddlGradeTest.Items.IndexOf(ddlGradeTest.Items.FindByValue("O"))
        ddlDuration.SelectedIndex = ddlDuration.Items.IndexOf(ddlDuration.Items.FindByValue("0"))
        ddlFrequency.SelectedIndex = ddlFrequency.Items.IndexOf(ddlFrequency.Items.FindByValue("0"))

        txtDeadLineDate.Text = String.Empty
        txtLateRegDLineDate.Text = String.Empty
        txtChangeCoachDL.Text = String.Empty
        txtlateFee.Text = "0.00"
        txtRegFee.Text = "0.00"
        txtDiscount.Text = "0.00"
        txtCapacity.Text = "0"
        ddlProduct.Items.Clear()
        ddClasses.SelectedValue = "0"
        ddlLicenses.SelectedValue = "0"
        LoadProductGroup()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub

    Protected Sub btnRelpicateSel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRelpicateSel.Click
        Dim item As DataGridItem
        Dim flag As Boolean = False
        Dim EventIds As String = "0"
        Dim EventFeesID As Integer
        Dim sqlstr As String
        Dim chk As CheckBox
        Dim RepFlag As Boolean = False
        For Each item In DGEventFees.Items
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                EventFeesID = CInt(item.Cells(3).Text)
                EventIds = EventIds & "," & EventFeesID.ToString
                flag = True
            End If
        Next
        If flag = True Then
            'Insert coding
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT EventID, EventCode, " & ddlPropYear.SelectedValue & "  as EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo, DeadlineDate, LateRegDLDate,ChangeCoachDL ,RegFee, LateFee,TaxDedRegional,ProductLevelPricing,Semester,DiscountAmt,Capacity,Duration,Classes,Licenses,Frequency,GETDATE()," & Session("LoginID") & " FROM EVENTFEES WHERE EVENTFEESID in (" & EventIds & ")")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from EventFees where EventYear=" & ds.Tables(0).Rows(i)("EventYear") & " AND EventID=" & ds.Tables(0).Rows(i)("EventID") & " AND ProductGroupID =" & ds.Tables(0).Rows(i)("ProductGroupID") & " AND ProductID = " & ds.Tables(0).Rows(i)("ProductID") & "") < 1 Then
                        sqlstr = "INSERT INTO EVENTFEES(EventID, EventCode, EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo, DeadlineDate, LateRegDLDate ,ChangeCoachDL,RegFee, LateFee, TaxDedRegional,ProductLevelPricing,Semester,DiscountAmt,Capacity,Duration,Frequency,classes,Licenses, CreateDate, CreatedBy) VALUES("
                        sqlstr = sqlstr & ds.Tables(0).Rows(i)("EventID") & ",'" & ds.Tables(0).Rows(i)("Eventcode") & "'," & ds.Tables(0).Rows(i)("EventYear") & "," & ds.Tables(0).Rows(i)("ProductGroupID") & ",'" & ds.Tables(0).Rows(i)("ProductGroupcode") & "'," & ds.Tables(0).Rows(i)("ProductID") & ",'" & ds.Tables(0).Rows(i)("Productcode") & "'," & ds.Tables(0).Rows(i)("GradeFrom") & "," & ds.Tables(0).Rows(i)("GradeTo") & "," & IIf(ds.Tables(0).Rows(i)("DeadlineDate") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("DeadlineDate") & "'") & "," & IIf(ds.Tables(0).Rows(i)("LateRegDLDate") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("LateRegDLDate") & "'") & "," & IIf(ds.Tables(0).Rows(i)("ChangeCoachDL") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("ChangeCoachDL") & "'") & "," & ds.Tables(0).Rows(i)("RegFee") & "," & ds.Tables(0).Rows(i)("lateFee") & "," & ds.Tables(0).Rows(i)("TaxDedRegional") & "," & IIf(ds.Tables(0).Rows(i)("ProductLevelPricing") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("ProductLevelPricing") & "'") & ",'" & ds.Tables(0).Rows(i)("Semester") & "'," & IIf(ds.Tables(0).Rows(i)("DiscountAmt") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DiscountAmt")) & "," & IIf(ds.Tables(0).Rows(i)("Capacity") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Capacity")) & "," & IIf(ds.Tables(0).Rows(i)("Duration") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Duration")) & "," & IIf(ds.Tables(0).Rows(i)("Frequency") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("Frequency") & "'") & "," & IIf(ds.Tables(0).Rows(i)("classes") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("classes")) & "," & IIf(ds.Tables(0).Rows(i)("Licenses") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Licenses")) & ",GetDate()," & Session("LoginID") & ")"
                        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr) > 0 Then
                            RepFlag = True
                        End If
                    End If
                Next
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(ddlPropYear.SelectedValue))
                loadgrid()
                If RepFlag = True Then
                    lblError.Text = "selected Item replicated successfully"
                End If
            End If
        Else
            lblError.Text = "No record is selected"
        End If
    End Sub

    Protected Sub btnRelpicateAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRelpicateAll.Click
        Dim item As DataGridItem
        Dim flag As Boolean = False
        Dim EventIds As String = "0"
        Dim EventFeesID As Integer
        Dim sqlstr As String
        Dim RepFlag As Boolean = False
        For Each item In DGEventFees.Items
            EventFeesID = CInt(item.Cells(3).Text)
            EventIds = EventIds & "," & EventFeesID.ToString
        Next
        'Insert coding
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT EventID, EventCode, " & ddlPropYear.SelectedValue & " as EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo,  DeadlineDate, LateRegDLDate ,ChangeCoachDL, RegFee, LateFee,TaxDedRegional,ProductLevelPricing,Semester,DiscountAmt,Capacity,Duration,Frequency,classes,Licenses,GETDATE()," & Session("LoginID") & " FROM EVENTFEES WHERE EVENTFEESID in (" & EventIds & ")")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from EventFees where EventYear=" & ds.Tables(0).Rows(i)("EventYear") & " AND EventID=" & ds.Tables(0).Rows(i)("EventID") & " AND ProductGroupID =" & ds.Tables(0).Rows(i)("ProductGroupID") & " AND ProductID = " & ds.Tables(0).Rows(i)("ProductID") & "") < 1 Then
                        sqlstr = "INSERT INTO EVENTFEES(EventID, EventCode, EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, GradeFrom, GradeTo, DeadlineDate, LateRegDLDate ,ChangeCoachDL,RegFee, LateFee, TaxDedRegional,ProductLevelPricing,Semester,DiscountAmt, Capacity,Duration,Frequency,classes,Licenses,CreateDate, CreatedBy) VALUES("
                        sqlstr = sqlstr & ds.Tables(0).Rows(i)("EventID") & ",'" & ds.Tables(0).Rows(i)("Eventcode") & "'," & ds.Tables(0).Rows(i)("EventYear") & "," & ds.Tables(0).Rows(i)("ProductGroupID") & ",'" & ds.Tables(0).Rows(i)("ProductGroupcode") & "'," & ds.Tables(0).Rows(i)("ProductID") & ",'" & ds.Tables(0).Rows(i)("Productcode") & "'," & ds.Tables(0).Rows(i)("GradeFrom") & "," & ds.Tables(0).Rows(i)("GradeTo") & "," & IIf(ds.Tables(0).Rows(i)("DeadlineDate") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("DeadlineDate") & "'") & "," & IIf(ds.Tables(0).Rows(i)("LateRegDLDate") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("LateRegDLDate") & "'") & "," & IIf(ds.Tables(0).Rows(i)("ChangeCoachDL") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("ChangeCoachDL") & "'") & "," & ds.Tables(0).Rows(i)("RegFee") & "," & ds.Tables(0).Rows(i)("lateFee") & "," & ds.Tables(0).Rows(i)("TaxDedRegional") & "," & IIf(ds.Tables(0).Rows(i)("ProductLevelPricing") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("ProductLevelPricing") & "'") & ",'" & ds.Tables(0).Rows(i)("Semester") & "'," & IIf(ds.Tables(0).Rows(i)("DiscountAmt") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DiscountAmt")) & "," & IIf(ds.Tables(0).Rows(i)("Capacity") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Capacity")) & "," & IIf(ds.Tables(0).Rows(i)("Duration") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Duration")) & "," & IIf(ds.Tables(0).Rows(i)("Frequency") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("Frequency") & "'") & "," & IIf(ds.Tables(0).Rows(i)("Classes") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("Classes") & "'") & "," & IIf(ds.Tables(0).Rows(i)("Licenses") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("Licenses") & "'") & ",GetDate()," & Session("LoginID") & ")"
                        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr) > 0 Then
                            RepFlag = True
                        End If
                    End If
                Next
                If RepFlag = True Then
                    lblError.ForeColor = Color.Green
                    lblError.Text = "Replicated successfully"
                End If
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(ddlPropYear.SelectedValue))
                loadgrid()
            End If
        Catch ex As Exception
            '  Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub txtRegFee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRegFee.TextChanged
        Dim ProductFlag As Boolean = False
        If ddlPricing.SelectedValue = "No" Then
            ProductFlag = True
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs)
        lblErr.Text = ""
    End Sub

    Protected Sub ddlDuration_SelectedIndexChanged(sender As Object, e As EventArgs)
        lblErr.Text = ""
    End Sub
     
    Protected Sub btnConfirmUpgrade_Click(sender As Object, e As EventArgs) Handles btnConfirmUpgrade.Click
        Dim CurrMonth As String = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Today.Month).ToString().Substring(0, 3)
        Dim c As New Common
        hfSemName.Value = c.GetSemesterNameByMonth(CurrMonth)
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from EventFees where EventYear=" & ddlEventYear.Text & " AND EventID=" & ddlEvent.SelectedValue) > 0 Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "UpgradeConfirmation('" & hfSemName.Value & "');", True)
        End If
    End Sub

    Protected Sub btnUpgrade_Click(sender As Object, e As EventArgs) Handles btnUpgrade.Click

        Dim str As String = "UPDATE EVENTFEES SET Semester='" & hfSemName.Value & "' WHERE EventYear=" & ddlEventYear.Text & " and EventId=" & ddlEvent.SelectedValue

        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, str)
        lblError.ForeColor = Color.Green
        lblError.Text = "Semester upgraded sucessfully for all products"
        loadgrid()
    End Sub
End Class

