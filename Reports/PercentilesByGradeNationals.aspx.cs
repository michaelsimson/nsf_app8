using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using Microsoft.ApplicationBlocks.Data;
using NativeExcel;

public partial class Reports_PercentilesByGradeNationals : System.Web.UI.Page
{
    string Year;
    string contestType;
    DataSet dsContestants;
    DataTable dt2 = new DataTable();

   
    int ProductId, ProductGroupID, ContestPeriod;
    String Product_Code, ProductGroupcode, ContestDays;
    int StartGrade, EndGrade;
    String SQLExecString = "";
    Boolean DuplicateFlag;
   

    protected void Page_Load(object sender, System.EventArgs e)
    {
        // Put user code to initialize the page here
        /**** Commented on 21-02-2013 to redesign the module*****/
        //Year = DateTime.Now.Year.ToString();   //System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        //contestType = ddlContest.SelectedValue;
        //ReadAllScores();
        /************/

        if (!this.IsPostBack)
        {
            //DuplicateFlag = false;
            LoadContestYear();
            LoadProduct();
        }

    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion

    void LoadContestYear()
    {
        int year = 0;
        //  'year = Convert.ToInt32(DateTime.Now.Year)
        int first_year = 2008;

        year = Convert.ToInt32(DateTime.Now.Year) + 1;
        int count = year - first_year;
        int i;
        for (i = 0; i < count; i++) //first_year To Convert.ToInt32(DateTime.Now.Year)
        {
            ddlYear.Items.Insert(i, new ListItem(Convert.ToString(year - (i + 1)), Convert.ToString(year - (i + 1))));
        }
        ddlYear.Items.Insert(0, "Select");
        ddlYear.SelectedIndex = 0;
    }
    void LoadProduct()
    {
        String SQLStr = "SELECT Distinct ProductId,ProductCode FROM Product WHERE EventID=1 and Status='O'";
        DataSet ds =  SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlContest.DataSource = ds;
            ddlContest.DataTextField = "ProductCode";
            ddlContest.DataValueField = "ProductId";
            ddlContest.DataBind();
        }
        ddlContest.Items.Insert(0, "Select");
        ddlContest.SelectedIndex = 0;
    }
    void ReadAllScores(String ProductCode,String Contest_Day,int Contest_Period)
    {
        try
        {
            lblSuccess.Text = "";
            //String[] Contestdates;
            //String ContestDate = "";

            ContestDays = Contest_Day;
            ContestPeriod = Contest_Period;
            // connect to the database
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(Application["ConnectionString"].ToString());
            connection.Open();
            // get records from the Contestant table
            contestType = ProductCode;// ddlContest.SelectedValue;

            //Product_Code = ProductCode;
            DataSet ds_product = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ProductId,ProductCode,ProductGroupId,ProductgroupCode FROM Product Where ProductCode ='" + contestType + "' and eventId =1");
            if (ds_product.Tables[0].Rows.Count > 0)
            {
                ProductId = Convert.ToInt32(ds_product.Tables[0].Rows[0]["ProductID"]);
                ProductGroupID = Convert.ToInt32(ds_product.Tables[0].Rows[0]["ProductGroupId"]);
                ProductGroupcode = ds_product.Tables[0].Rows[0]["ProductGroupcode"].ToString();
            }

            //if (Contest_Day != "All") //(ddlcontestdate.SelectedItem.Text != "All")
            //{
            //    Contestdates = Contest_Day.Split(','); ;// ddlcontestdate.SelectedItem.Text.Split(',');
            //    ContestDate = "'" + Contestdates[0] + "," + ddlYear.SelectedValue + "','" + Contestdates[1] + "," + ddlYear.SelectedValue + "'";
            //}

            // connect to the peoducts database
            //  string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 
            // create and open the connection object
            //System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString);

            //connection.Open();

            // get records from the Contestant table

            string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
                "Contestant.Score1, Contestant.Score2, Contestant.Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID, Child.GRADE  from Contestant " +
                "INNER JOIN Child ON Contestant.ChildNumber = Child.ChildNumber " +
                " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear='" + ddlYear.SelectedValue   // Year 
                + "' AND (Contestant.Score1 >= 1 OR Contestant.Score2 >= 1) AND Contestant.BadgeNumber like '" + contestType + "%'  AND Contestant.ChapterID = 1 ORDER BY Contestant.GRADE";

            // create the command object and set its
            // command string and connection
           
            SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
            dsContestants = new DataSet();
            daContestants.Fill(dsContestants);

            //if(dsContestants.Tables[0].Rows.Count > 29)
            ProcessData(dsContestants);
            //else 
            //{
            //	Label1.Text = "%tile are not available because the number of contestants is less than 30.";
            //}
        }
        catch (Exception ex)
        {
           // Response.Write(ex.ToString());
        }
			
    }
    void ProcessData(DataSet ds)
    {
       try
       {
			DataTable dt = new DataTable();
			DataRow dr;
			dt.Columns.Add("Contestant",typeof(int));
			dt.Columns.Add("TotalScore",typeof(double));
			dt.Columns.Add("Grade", typeof(int));
            int curGrade = -1;
            int count = -1;
            int[] aryCCount = new int[5];
            double[] aryTscore = new double[ds.Tables[0].Rows.Count];
            double[] grScore = new double[ds.Tables[0].Rows.Count];

           // if (ds.Tables[0].Rows.Count > 0)
           // {
                //ProductId = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductID"]);
                //ProductGroupID = Convert.ToInt32(ds.Tables[0].Rows[0]["ProductGroupId"]);
                //ProductGroupcode = ds.Tables[0].Rows[0]["ProductGroupcode"].ToString();
          //  }

           	for(int i =0; i< ds.Tables[0].Rows.Count;i++)
			{
				    dr = dt.NewRow();
				    dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]); 
				    dr["TotalScore"]= Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
					    Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
				    dr["Grade"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);

                    if ((curGrade == Convert.ToInt32(dr["Grade"])) || ((curGrade == 0) || (curGrade == 1)) && ((Convert.ToInt32(dr["Grade"]) == 0) || (Convert.ToInt32(dr["Grade"]) == 1)))
                    {
                        aryCCount[count] = aryCCount[count] + 1;
                    }
                    else
                    {
                        curGrade = Convert.ToInt32(dr["Grade"]);
                        aryCCount[++count] = 1;
                    }

				    grScore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
				    aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
					    Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
				    dt.Rows.Add(dr);
			}
            
			SortArrays(aryTscore, grScore);
			
			dt2 = new DataTable();
			dt2.Columns.Add("TotalScore",typeof(double));
			switch(contestType)
			{
				case "MB1":
					dt2.Columns.Add("Gr1%",typeof(string));
					dt2.Columns.Add("Gr2%",typeof(string));
					dt2.Columns.Add("Gr1%tile",typeof(string));
					dt2.Columns.Add("Gr1cnt",typeof(string));
					dt2.Columns.Add("Gr1cum",typeof(string));
					dt2.Columns.Add("Gr2%tile",typeof(string));
					dt2.Columns.Add("Gr2cnt",typeof(string));
					dt2.Columns.Add("Gr2cum",typeof(string));
                    StartGrade = 1;
                    EndGrade = 2;
					break;
				case "MB2":
					dt2.Columns.Add("Gr3%",typeof(string));
					dt2.Columns.Add("Gr4%",typeof(string));
					dt2.Columns.Add("Gr5%",typeof(string));
					dt2.Columns.Add("Gr3%tile",typeof(string));
					dt2.Columns.Add("Gr3cnt",typeof(string));
					dt2.Columns.Add("Gr3cum",typeof(string));
					dt2.Columns.Add("Gr4%tile",typeof(string));
					dt2.Columns.Add("Gr4cnt",typeof(string));
					dt2.Columns.Add("Gr4cum",typeof(string));
					dt2.Columns.Add("Gr5%tile",typeof(string));
					dt2.Columns.Add("Gr5cnt",typeof(string));
					dt2.Columns.Add("Gr5cum",typeof(string));
                    StartGrade = 3;
                    EndGrade = 5;
					break;
				case "MB3":
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
					break;
				case "MB4":
					dt2.Columns.Add("Gr9%",typeof(string));
					dt2.Columns.Add("Gr10%",typeof(string));
					dt2.Columns.Add("Gr9%tile",typeof(string));
					dt2.Columns.Add("Gr9cnt",typeof(string));
					dt2.Columns.Add("Gr9cum",typeof(string));
					dt2.Columns.Add("Gr10%tile",typeof(string));
					dt2.Columns.Add("Gr10cnt",typeof(string));
					dt2.Columns.Add("Gr10cum",typeof(string));
                    StartGrade = 9;
                    EndGrade = 10;
					break;
				case "JSB":
                case "JSC":
					dt2.Columns.Add("Gr1%",typeof(string));
					dt2.Columns.Add("Gr2%",typeof(string));
					dt2.Columns.Add("Gr3%",typeof(string));
					dt2.Columns.Add("Gr1%tile",typeof(string));
					dt2.Columns.Add("Gr1cnt",typeof(string));
					dt2.Columns.Add("Gr1cum",typeof(string));
					dt2.Columns.Add("Gr2%tile",typeof(string));
					dt2.Columns.Add("Gr2cnt",typeof(string));
					dt2.Columns.Add("Gr2cum",typeof(string));
					dt2.Columns.Add("Gr3%tile",typeof(string));
					dt2.Columns.Add("Gr3cnt",typeof(string));
					dt2.Columns.Add("Gr3cum",typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
					break;
                case "ISC":
                    dt2.Columns.Add("Gr4%", typeof(string));
                    dt2.Columns.Add("Gr5%", typeof(string));
                    dt2.Columns.Add("Gr4%tile", typeof(string));
                    dt2.Columns.Add("Gr4cnt", typeof(string));
                    dt2.Columns.Add("Gr4cum", typeof(string));
                    dt2.Columns.Add("Gr5%tile", typeof(string));
                    dt2.Columns.Add("Gr5cnt", typeof(string));
                    dt2.Columns.Add("Gr5cum", typeof(string));
                    StartGrade = 4;
                    EndGrade = 5;
                    break;
                case "SSC":
                    dt2.Columns.Add("Gr6%", typeof(string));
                    dt2.Columns.Add("Gr7%", typeof(string));
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr6%tile", typeof(string));
                    dt2.Columns.Add("Gr6cnt", typeof(string));
                    dt2.Columns.Add("Gr6cum", typeof(string));
                    dt2.Columns.Add("Gr7%tile", typeof(string));
                    dt2.Columns.Add("Gr7cnt", typeof(string));
                    dt2.Columns.Add("Gr7cum", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
                    break;
				case "SSB":
					dt2.Columns.Add("Gr4%",typeof(string));
					dt2.Columns.Add("Gr5%",typeof(string));
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr4%tile",typeof(string));
					dt2.Columns.Add("Gr4cnt",typeof(string));
					dt2.Columns.Add("Gr4cum",typeof(string));
					dt2.Columns.Add("Gr5%tile",typeof(string));
					dt2.Columns.Add("Gr5cnt",typeof(string));
					dt2.Columns.Add("Gr5cum",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
					break;
				case "JVB":
					dt2.Columns.Add("Gr1%",typeof(string));
					dt2.Columns.Add("Gr2%",typeof(string));
					dt2.Columns.Add("Gr3%",typeof(string));
					dt2.Columns.Add("Gr1%tile",typeof(string));
					dt2.Columns.Add("Gr1cnt",typeof(string));
					dt2.Columns.Add("Gr1cum",typeof(string));
					dt2.Columns.Add("Gr2%tile",typeof(string));
					dt2.Columns.Add("Gr2cnt",typeof(string));
					dt2.Columns.Add("Gr2cum",typeof(string));
					dt2.Columns.Add("Gr3%tile",typeof(string));
					dt2.Columns.Add("Gr3cnt",typeof(string));
					dt2.Columns.Add("Gr3cum",typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
					break;
				case "IVB":
					dt2.Columns.Add("Gr4%",typeof(string));
					dt2.Columns.Add("Gr5%",typeof(string));
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr4%tile",typeof(string));
					dt2.Columns.Add("Gr4cnt",typeof(string));
					dt2.Columns.Add("Gr4cum",typeof(string));
					dt2.Columns.Add("Gr5%tile",typeof(string));
					dt2.Columns.Add("Gr5cnt",typeof(string));
					dt2.Columns.Add("Gr5cum",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
					break;
				case "SVB":
					dt2.Columns.Add("Gr9%",typeof(string));
					dt2.Columns.Add("Gr10%",typeof(string));
					dt2.Columns.Add("Gr11%",typeof(string));
					dt2.Columns.Add("Gr12%",typeof(string));
					dt2.Columns.Add("Gr9%tile",typeof(string));
					dt2.Columns.Add("Gr9cnt",typeof(string));
					dt2.Columns.Add("Gr9cum",typeof(string));
					dt2.Columns.Add("Gr10%tile",typeof(string));
					dt2.Columns.Add("Gr10cnt",typeof(string));
					dt2.Columns.Add("Gr10cum",typeof(string));
					dt2.Columns.Add("Gr11%tile",typeof(string));
					dt2.Columns.Add("Gr11cnt",typeof(string));
					dt2.Columns.Add("Gr11cum",typeof(string));
					dt2.Columns.Add("Gr12%tile",typeof(string));
					dt2.Columns.Add("Gr12cnt",typeof(string));
					dt2.Columns.Add("Gr12cum",typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
					break;
				case "JGB":
					dt2.Columns.Add("Gr1%",typeof(string));
					dt2.Columns.Add("Gr2%",typeof(string));
					dt2.Columns.Add("Gr3%",typeof(string));
					dt2.Columns.Add("Gr1%tile",typeof(string));
					dt2.Columns.Add("Gr1cnt",typeof(string));
					dt2.Columns.Add("Gr1cum",typeof(string));
					dt2.Columns.Add("Gr2%tile",typeof(string));
					dt2.Columns.Add("Gr2cnt",typeof(string));
					dt2.Columns.Add("Gr2cum",typeof(string));
					dt2.Columns.Add("Gr3%tile",typeof(string));
					dt2.Columns.Add("Gr3cnt",typeof(string));
					dt2.Columns.Add("Gr3cum",typeof(string));
                    StartGrade = 1;
                    EndGrade = 3;
					break;
				case "SGB":
					dt2.Columns.Add("Gr4%",typeof(string));
					dt2.Columns.Add("Gr5%",typeof(string));
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr4%tile",typeof(string));
					dt2.Columns.Add("Gr4cnt",typeof(string));
					dt2.Columns.Add("Gr4cum",typeof(string));
					dt2.Columns.Add("Gr5%tile",typeof(string));
					dt2.Columns.Add("Gr5cnt",typeof(string));
					dt2.Columns.Add("Gr5cum",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 4;
                    EndGrade = 8;
					break;
				case "EW1":
					dt2.Columns.Add("Gr3%",typeof(string));
					dt2.Columns.Add("Gr4%",typeof(string));
					dt2.Columns.Add("Gr5%",typeof(string));
					dt2.Columns.Add("Gr3%tile",typeof(string));
					dt2.Columns.Add("Gr3cnt",typeof(string));
					dt2.Columns.Add("Gr3cum",typeof(string));
					dt2.Columns.Add("Gr4%tile",typeof(string));
					dt2.Columns.Add("Gr4cnt",typeof(string));
					dt2.Columns.Add("Gr4cum",typeof(string));
					dt2.Columns.Add("Gr5%tile",typeof(string));
					dt2.Columns.Add("Gr5cnt",typeof(string));
					dt2.Columns.Add("Gr5cum",typeof(string));
                    StartGrade = 3;
                    EndGrade = 5;
					break;
				case "EW2":
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
					break;
				case "EW3":
					dt2.Columns.Add("Gr9%",typeof(string));
					dt2.Columns.Add("Gr10%",typeof(string));
					dt2.Columns.Add("Gr11%",typeof(string));
					dt2.Columns.Add("Gr12%",typeof(string));
					dt2.Columns.Add("Gr9%tile",typeof(string));
					dt2.Columns.Add("Gr9cnt",typeof(string));
					dt2.Columns.Add("Gr9cum",typeof(string));
					dt2.Columns.Add("Gr10%tile",typeof(string));
					dt2.Columns.Add("Gr10cnt",typeof(string));
					dt2.Columns.Add("Gr10cum",typeof(string));
					dt2.Columns.Add("Gr11%tile",typeof(string));
					dt2.Columns.Add("Gr11cnt",typeof(string));
					dt2.Columns.Add("Gr11cum",typeof(string));
					dt2.Columns.Add("Gr12%tile",typeof(string));
					dt2.Columns.Add("Gr12cnt",typeof(string));
					dt2.Columns.Add("Gr12cum",typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
					break;
				case "PS1":
					dt2.Columns.Add("Gr6%",typeof(string));
					dt2.Columns.Add("Gr7%",typeof(string));
					dt2.Columns.Add("Gr8%",typeof(string));
					dt2.Columns.Add("Gr6%tile",typeof(string));
					dt2.Columns.Add("Gr6cnt",typeof(string));
					dt2.Columns.Add("Gr6cum",typeof(string));
					dt2.Columns.Add("Gr7%tile",typeof(string));
					dt2.Columns.Add("Gr7cnt",typeof(string));
					dt2.Columns.Add("Gr7cum",typeof(string));
					dt2.Columns.Add("Gr8%tile",typeof(string));
					dt2.Columns.Add("Gr8cnt",typeof(string));
					dt2.Columns.Add("Gr8cum",typeof(string));
                    StartGrade = 6;
                    EndGrade = 8;
					break;
				case "PS3":
					dt2.Columns.Add("Gr9%",typeof(string));
					dt2.Columns.Add("Gr10%",typeof(string));
					dt2.Columns.Add("Gr11%",typeof(string));
					dt2.Columns.Add("Gr12%",typeof(string));
					dt2.Columns.Add("Gr9%tile",typeof(string));
					dt2.Columns.Add("Gr9cnt",typeof(string));
					dt2.Columns.Add("Gr9cum",typeof(string));
					dt2.Columns.Add("Gr10%tile",typeof(string));
					dt2.Columns.Add("Gr10cnt",typeof(string));
					dt2.Columns.Add("Gr10cum",typeof(string));
					dt2.Columns.Add("Gr11%tile",typeof(string));
					dt2.Columns.Add("Gr11cnt",typeof(string));
					dt2.Columns.Add("Gr11cum",typeof(string));
					dt2.Columns.Add("Gr12%tile",typeof(string));
					dt2.Columns.Add("Gr12cnt",typeof(string));
					dt2.Columns.Add("Gr12cum",typeof(string));
                    StartGrade = 9;
                    EndGrade = 12;
					break;
                case "BB":
                    dt2.Columns.Add("Gr8%", typeof(string));
                    dt2.Columns.Add("Gr9%", typeof(string));
                    dt2.Columns.Add("Gr10%", typeof(string));
                    dt2.Columns.Add("Gr11%", typeof(string));
                    dt2.Columns.Add("Gr8%tile", typeof(string));
                    dt2.Columns.Add("Gr8cnt", typeof(string));
                    dt2.Columns.Add("Gr8cum", typeof(string));
                    dt2.Columns.Add("Gr9%tile", typeof(string));
                    dt2.Columns.Add("Gr9cnt", typeof(string));
                    dt2.Columns.Add("Gr9cum", typeof(string));
                    dt2.Columns.Add("Gr10%tile", typeof(string));
                    dt2.Columns.Add("Gr10cnt", typeof(string));
                    dt2.Columns.Add("Gr10cum", typeof(string));
                    dt2.Columns.Add("Gr11%tile", typeof(string));
                    dt2.Columns.Add("Gr11cnt", typeof(string));
                    dt2.Columns.Add("Gr11cum", typeof(string));
                    StartGrade = 8;
                    EndGrade = 11;
                    break;
			}
            dt2.Columns.Add("TotalPercentile", typeof(string));
            dt2.Columns.Add("TotalCount", typeof(double));
            dt2.Columns.Add("TotalCumeCount",typeof(double));
			
			dr = dt2.NewRow();

			if(aryTscore.Length>0)
			dr["TotalScore"] = aryTscore[0];
						
			int[] grCount = new int[13];
			int[] grCCount = new int[13];
			int[] grTCount = new int[13];
			float[] pc = new float[13];
			float[] pcile = new float[13];
			for(int k=0; k < grScore.Length; k++)
			{
                    if (grScore[k] >= -0.1 && grScore[k] <= 1.5)
                        grTCount[1]++;
                    else if (grScore[k] > 1.5 && grScore[k] < 2.5)
                        grTCount[2]++;
                    else if (grScore[k] > 2.5 && grScore[k] < 3.5)
                        grTCount[3]++;
                    else if (grScore[k] > 3.5 && grScore[k] < 4.5)
                        grTCount[4]++;
                    else if (grScore[k] > 4.5 && grScore[k] <= 5.5)
                        grTCount[5]++;
                    else if (grScore[k] > 5.5 && grScore[k] < 6.5)
                        grTCount[6]++;
                    else if (grScore[k] > 6.5 && grScore[k] < 7.5)
                        grTCount[7]++;
                    else if (grScore[k] > 7.5 && grScore[k] < 8.5)
                        grTCount[8]++;
                    else if (grScore[k] > 8.5 && grScore[k] < 9.5)
                        grTCount[9]++;
                    else if (grScore[k] > 9.5 && grScore[k] < 10.5)
                        grTCount[10]++;
                    else if (grScore[k] > 10.5 && grScore[k] < 11.5)
                        grTCount[11]++;
                    else if (grScore[k] > 11.5)
                        grTCount[12]++;
			}
			double lastScore = 0;
			if(aryTscore.Length>0)
			lastScore = aryTscore[0];

            String SQLInsertCutOff = "";
            String SQLInsertCutOffVal = "";
            Boolean Contest_Flag = false;
            int j;

            int totalCumCount = 0,totalCount=0, tScoreCount=0,cCount = 0;
            double totalPercentile = 0.0;
          
            totalCount = aryTscore.Length;

           	for(j=0; j< aryTscore.Length;j++)
			{
                if(lastScore == aryTscore[j])
				{
                    if((grScore[j] == 1) || (grScore[j] == 0))
					{
						grCount[1] = grCount[1] + 1;
					}
					 if(grScore[j] == 2)
					{
						grCount[2] = grCount[2] + 1;
					}
					 if(grScore[j] == 3)
					{
						grCount[3] = grCount[3] + 1;
					}
					 if(grScore[j] == 4)
					{
						grCount[4] = grCount[4] + 1;
					}
					 if(grScore[j] == 5)
					{
						grCount[5] = grCount[5] + 1;
					}
					if(grScore[j] == 6)
					{
						grCount[6] = grCount[6] + 1;
					}
					if(grScore[j] == 7)
					{
						grCount[7] = grCount[7] + 1;
					}
					if(grScore[j] == 8)
					{
						grCount[8] = grCount[8] + 1;
					}
					if(grScore[j] == 9)
					{
						grCount[9] = grCount[9] + 1;
					}
					if(grScore[j] == 10)
					{
						grCount[10] = grCount[10] + 1;
					}
					if(grScore[j] == 11)
					{
						grCount[11] = grCount[11] + 1;
					}
					if(grScore[j] == 12)
					{
						grCount[12] = grCount[12] + 1;
					}

                    cCount++;
				}
				else 
				{
                    lastScore = aryTscore[j];
                    
                    SQLInsertCutOff = "Insert into CutOffScoresByPercentile ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
                    SQLInsertCutOffVal = ") Values (" + ddlYear.SelectedValue  + ",1,"+ ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + aryTscore[j-1] + ""; //Year

					switch(contestType)
					{
						case "MB1":
							pc[1] = (float)grCount[1]/grTCount[1];
							dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
							//grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
							pcile[1] = (float)grCCount[1]/grTCount[1];
							dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
							dr["Gr1cnt"] = grCount[1];
							dr["Gr1cum"] = grCCount[1];
							pc[2] = (float)grCount[2]/grTCount[2];
							dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
							//grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
							pcile[2] = (float)grCCount[2]/grTCount[2];
							dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));

							dr["Gr2cnt"] = grCount[2];
							dr["Gr2cum"] = grCCount[2];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];
							break;
						case "MB2":
						case "EW1":
						 	pc[3] = (float)grCount[3]/grTCount[3];
							dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
							//grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[0];
                            aryCCount[0] -= grCount[3];
							pcile[3] = (float)grCCount[3]/grTCount[3];
							dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
							dr["Gr3cnt"] = grCount[3];
							dr["Gr3cum"] = grCCount[3];
							pc[4] = (float)grCount[4]/grTCount[4];
							dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
							//grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[1];
                            aryCCount[1] -= grCount[4];
							pcile[4] = (float)grCCount[4]/grTCount[4];
							dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
							dr["Gr4cnt"] = grCount[4];
							dr["Gr4cum"] = grCCount[4];
							pc[5] = (float)grCount[5]/grTCount[5];
							dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
							//grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[2];
                            aryCCount[2] -= grCount[5];
							pcile[5] = (float)grCCount[5]/grTCount[5];
							dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
							dr["Gr5cnt"] = grCount[5];
							dr["Gr5cum"] = grCCount[5];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

							break;
						case "MB3":
						case "EW2":
							case "PS1":
							pc[6] = (float)grCount[6]/grTCount[6];
							dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
							//grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[0];
                            aryCCount[0] -= grCount[6];
							pcile[6] = (float)grCCount[6]/grTCount[6];
							dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
							dr["Gr6cnt"] = grCount[6];
							dr["Gr6cum"] = grCCount[6];
							pc[7] = (float)grCount[7]/grTCount[7];
							dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
							//grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[1];
                            aryCCount[1] -= grCount[7];
							pcile[7] = (float)grCCount[7]/grTCount[7];
							dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
							dr["Gr7cnt"] = grCount[7];
							dr["Gr7cum"] = grCCount[7];
							pc[8] = (float)grCount[8]/grTCount[8];
							dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
							//grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[2];
                            aryCCount[2] -= grCount[8];
							pcile[8] = (float)grCCount[8]/grTCount[8];
							dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
							dr["Gr8cnt"] = grCount[8];
							dr["Gr8cum"] = grCCount[8];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

							break;
						case "MB4":
							pc[9] = (float)grCount[9]/grTCount[9];
							dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
							//grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
							pcile[9] = (float)grCCount[9]/grTCount[9];
							dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
							dr["Gr9cnt"] = grCount[9];
							dr["Gr9cum"] = grCCount[9];
							pc[10] = (float)grCount[10]/grTCount[10];
							dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
							//grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[2];
                            aryCCount[2] -= grCount[10];
							pcile[10] = (float)grCCount[10]/grTCount[10];
							dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
							dr["Gr10cnt"] = grCount[10];
							dr["Gr10cum"] = grCCount[10];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

							break;

						case "JSB":
                        case "JSC":
						case "JGB":
                           
                            pc[1] = (float)grCount[1]/grTCount[1];
							dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
							//grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
							pcile[1] = (float)grCCount[1]/grTCount[1];
							dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
							dr["Gr1cnt"] = grCount[1];
							dr["Gr1cum"] = grCCount[1];
							pc[2] = (float)grCount[2]/grTCount[2];
							dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
							//grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
							pcile[2] = (float)grCCount[2]/grTCount[2];
							dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));
							dr["Gr2cnt"] = grCount[2];
							dr["Gr2cum"] = grCCount[2];
							pc[3] = (float)grCount[3]/grTCount[3];
							dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
							//grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[2];
                            aryCCount[2] -= grCount[3];
							pcile[3] = (float)grCCount[3]/grTCount[3];
							dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
							dr["Gr3cnt"] = grCount[3];
							dr["Gr3cum"] = grCCount[3];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];
				
							break;
                        case "ISC":
                            pc[4] = (float)grCount[4] / grTCount[4];
                            dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                            //grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
                            pcile[4] = (float)grCCount[4] / grTCount[4];
                            dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                            dr["Gr4cnt"] = grCount[4];
                            dr["Gr4cum"] = grCCount[4];
                            pc[5] = (float)grCount[5] / grTCount[5];
                            dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                            //grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
                            pcile[5] = (float)grCCount[5] / grTCount[5];
                            dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                            dr["Gr5cnt"] = grCount[5];
                            dr["Gr5cum"] = grCCount[5];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
                            break;
                        case "SSC":
                            pc[6] = (float)grCount[6] / grTCount[6];
                            dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                            //grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[0];
                            aryCCount[0] -= grCount[6];
                            pcile[6] = (float)grCCount[6] / grTCount[6];
                            dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                            dr["Gr6cnt"] = grCount[6];
                            dr["Gr6cum"] = grCCount[6];
                            pc[7] = (float)grCount[7] / grTCount[7];
                            dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                            //grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[1];
                            aryCCount[1] -= grCount[7];
                            pcile[7] = (float)grCCount[7] / grTCount[7];
                            dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                            dr["Gr7cnt"] = grCount[7];
                            dr["Gr7cum"] = grCCount[7];
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[2];
                            aryCCount[2] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];
                            break;
						case "SSB":
						case "SGB":
							pc[4] = (float)grCount[4]/grTCount[4];
							dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
							//grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
							pcile[4] = (float)grCCount[4]/grTCount[4];
							dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
							dr["Gr4cnt"] = grCount[4];
							dr["Gr4cum"] = grCCount[4];
							pc[5] = (float)grCount[5]/grTCount[5];
							dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
							//grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
							pcile[5] = (float)grCCount[5]/grTCount[5];
							dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
							dr["Gr5cnt"] = grCount[5];
							dr["Gr5cum"] = grCCount[5];
							pc[6] = (float)grCount[6]/grTCount[6];
							dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
							//grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[2];
                            aryCCount[2] -= grCount[6];
							pcile[6] = (float)grCCount[6]/grTCount[6];
							dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
							dr["Gr6cnt"] = grCount[6];
							dr["Gr6cum"] = grCCount[6];
							pc[7] = (float)grCount[7]/grTCount[7];
							dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
							//grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[3];
                            aryCCount[3] -= grCount[7];
							pcile[7] = (float)grCCount[7]/grTCount[7];
							dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
							dr["Gr7cnt"] = grCount[7];
							dr["Gr7cum"] = grCCount[7];
							pc[8] = (float)grCount[8]/grTCount[8];
							dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
							//grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[4];
                            aryCCount[4] -= grCount[8];
							pcile[8] = (float)grCCount[8]/grTCount[8];
							dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
							dr["Gr8cnt"] = grCount[8];
							dr["Gr8cum"] = grCCount[8];
                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

							break;
						case "JVB":
							pc[1] = (float)grCount[1]/grTCount[1];
							dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
							//grCCount[1] += grCount[1];
                            grCCount[1] = aryCCount[0];
                            aryCCount[0] -= grCount[1];
							pcile[1] = (float)grCCount[1]/grTCount[1];
							dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
							dr["Gr1cnt"] = grCount[1];
							dr["Gr1cum"] = grCCount[1];
							pc[2] = (float)grCount[2]/grTCount[2];
							dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
							//grCCount[2] += grCount[2];
                            grCCount[2] = aryCCount[1];
                            aryCCount[1] -= grCount[2];
							pcile[2] = (float)grCCount[2]/grTCount[2];
							dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));
							dr["Gr2cnt"] = grCount[2];
							dr["Gr2cum"] = grCCount[2];
							pc[3] = (float)grCount[3]/grTCount[3];
							dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
							//grCCount[3] += grCount[3];
                            grCCount[3] = aryCCount[2];
                            aryCCount[2] -= grCount[3];
							pcile[3] = (float)grCCount[3]/grTCount[3];
							dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
							dr["Gr3cnt"] = grCount[3];
							dr["Gr3cum"] = grCCount[3];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

							break;
						case "IVB":
							pc[4] = (float)grCount[4]/grTCount[4];
							dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
							//grCCount[4] += grCount[4];
                            grCCount[4] = aryCCount[0];
                            aryCCount[0] -= grCount[4];
							pcile[4] = (float)grCCount[4]/grTCount[4];
							dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
							dr["Gr4cnt"] = grCount[4];
							dr["Gr4cum"] = grCCount[4];
							pc[5] = (float)grCount[5]/grTCount[5];
							dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
							//grCCount[5] += grCount[5];
                            grCCount[5] = aryCCount[1];
                            aryCCount[1] -= grCount[5];
							pcile[5] = (float)grCCount[5]/grTCount[5];
							dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
							dr["Gr5cnt"] = grCount[5];
							dr["Gr5cum"] = grCCount[5];
							pc[6] = (float)grCount[6]/grTCount[6];
							dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
							//grCCount[6] += grCount[6];
                            grCCount[6] = aryCCount[2];
                            aryCCount[2] -= grCount[6];
							pcile[6] = (float)grCCount[6]/grTCount[6];
							dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
							dr["Gr6cnt"] = grCount[6];
							dr["Gr6cum"] = grCCount[6];
							pc[7] = (float)grCount[7]/grTCount[7];
							dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
							//grCCount[7] += grCount[7];
                            grCCount[7] = aryCCount[3];
                            aryCCount[3] -= grCount[7];
							pcile[7] = (float)grCCount[7]/grTCount[7];
							dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
							dr["Gr7cnt"] = grCount[7];
							dr["Gr7cum"] = grCCount[7];
							pc[8] = (float)grCount[8]/grTCount[8];
							dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
							//grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[4];
                            aryCCount[4] -= grCount[8];
							pcile[8] = (float)grCCount[8]/grTCount[8];
							dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
							dr["Gr8cnt"] = grCount[8];
							dr["Gr8cum"] = grCCount[8];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

							break;
						case "SVB":
							pc[9] = (float)grCount[9]/grTCount[9];
							dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
							//grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
							pcile[9] = (float)grCCount[9]/grTCount[9];
							dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
							dr["Gr9cnt"] = grCount[9];
							dr["Gr9cum"] = grCCount[9];
							pc[10] = (float)grCount[10]/grTCount[10];
							dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
							//grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[1];
                            aryCCount[1] -= grCount[10];
							pcile[10] = (float)grCCount[10]/grTCount[10];
							dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
							dr["Gr10cnt"] = grCount[10];
							dr["Gr10cum"] = grCCount[10];
							pc[11] = (float)grCount[11]/grTCount[11];
							dr["Gr11%"] =String.Format("{0:P1}",Math.Round(pc[11],3));
							//grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[2];
                            aryCCount[2] -= grCount[11];
							pcile[11] = (float)grCCount[11]/grTCount[11];
							dr["Gr11%tile"] =String.Format("{0:P1}",Math.Round(pcile[11],3));
							dr["Gr11cnt"] = grCount[11];
							dr["Gr11cum"] = grCCount[11];
							if(grTCount[12] == 0)
							{
								pc[12] = 0;
								dr["Gr12%"] = "";
								dr["Gr12%tile"] = "";
								dr["Gr12cnt"] = grCount[12];
								dr["Gr12cum"] = grCCount[12];
							}
							else
							{
								pc[12] = (float)grCount[12]/grTCount[12];
								dr["Gr12%"] =String.Format("{0:P1}",Math.Round(pc[12],3));
								//grCCount[12] += grCount[12];
                                grCCount[12] = aryCCount[3];
                                aryCCount[3] -= grCount[12];
								pcile[12] = (float)grCCount[12]/grTCount[12];
								dr["Gr12%tile"] =String.Format("{0:P1}",Math.Round(pcile[12],3));
								dr["Gr12cnt"] = grCount[12];
								dr["Gr12cum"] = grCCount[12];
							}

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

							break;
						case "EW3":
						case "PS3":
							pc[9] = (float)grCount[9]/grTCount[9];
							dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
							//grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[0];
                            aryCCount[0] -= grCount[9];
							pcile[9] = (float)grCCount[9]/grTCount[9];
							dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
							dr["Gr9cnt"] = grCount[9];
							dr["Gr9cum"] = grCCount[9];
							pc[10] = (float)grCount[10]/grTCount[10];
							dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
							//grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[1];
                            aryCCount[1] -= grCount[10];
							pcile[10] = (float)grCCount[10]/grTCount[10];
							dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
							dr["Gr10cnt"] = grCount[10];
							dr["Gr10cum"] = grCCount[10];
							pc[11] = (float)grCount[11]/grTCount[11];
							dr["Gr11%"] =String.Format("{0:P1}",Math.Round(pc[11],3));
							//grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[2];
                            aryCCount[2] -= grCount[11];
							pcile[11] = (float)grCCount[11]/grTCount[11];
							dr["Gr11%tile"] =String.Format("{0:P1}",Math.Round(pcile[11],3));
							dr["Gr11cnt"] = grCount[11];
							dr["Gr11cum"] = grCCount[11];
							if(grTCount[12] == 0)
							{
								pc[12] = 0;
								dr["Gr12%"] = "";
								dr["Gr12%tile"] = "";
							}
							else
							{
								pc[12] = (float)grCount[12]/grTCount[12];
								dr["Gr12%"] =String.Format("{0:P1}",Math.Round(pc[12],3));
								//grCCount[12] += grCount[12];
                                grCCount[12] = aryCCount[3];
                                aryCCount[3] -= grCount[12];
								pcile[12] = (float)grCCount[12]/grTCount[12];
								dr["Gr12%tile"] =String.Format("{0:P1}",Math.Round(pcile[12],3));
							}
							dr["Gr12cnt"] = grCount[12];
							dr["Gr12cum"] = grCCount[12];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

							break;
                        case "BB":
                            pc[8] = (float)grCount[8] / grTCount[8];
                            dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                            //grCCount[8] += grCount[8];
                            grCCount[8] = aryCCount[0];
                            aryCCount[0] -= grCount[8];
                            pcile[8] = (float)grCCount[8] / grTCount[8];
                            dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                            dr["Gr8cnt"] = grCount[8];
                            dr["Gr8cum"] = grCCount[8];
                            pc[9] = (float)grCount[9] / grTCount[9];
                            dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                            //grCCount[9] += grCount[9];
                            grCCount[9] = aryCCount[1];
                            aryCCount[1] -= grCount[9];
                            pcile[9] = (float)grCCount[9] / grTCount[9];
                            dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                            dr["Gr9cnt"] = grCount[9];
                            dr["Gr9cum"] = grCCount[9];
                            pc[10] = (float)grCount[10] / grTCount[10];
                            dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                            //grCCount[10] += grCount[10];
                            grCCount[10] = aryCCount[2];
                            aryCCount[2] -= grCount[10];
                            pcile[10] = (float)grCCount[10] / grTCount[10];
                            dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                            dr["Gr10cnt"] = grCount[10];
                            dr["Gr10cum"] = grCCount[10];
                            pc[11] = (float)grCount[11] / grTCount[11];
                            dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                            //grCCount[11] += grCount[11];
                            grCCount[11] = aryCCount[3];
                            aryCCount[3] -= grCount[11];
                            pcile[11] = (float)grCCount[11] / grTCount[11];
                            dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                            dr["Gr11cnt"] = grCount[11];
                            dr["Gr11cum"] = grCCount[11];

                            SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum";
                            SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"] + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"];
                            break;
			
					}
					dt2.Rows.Add(dr);

                    totalCumCount = (grCCount[1] + grCCount[2] + grCCount[3] + grCCount[4] + grCCount[5] + grCCount[6] + grCCount[7] + grCCount[8] + grCCount[9] + grCCount[10] + grCCount[11] + grCCount[12]);
                    tScoreCount = (grCount[1] + grCount[2] + grCount[3] + grCount[4] + grCount[5] + grCount[6] + grCount[7] + grCount[8] + grCount[9] + grCount[10] + grCount[11] + grCount[12]);

                    totalPercentile = (float) totalCumCount / totalCount;// aryTscore.Length);

                    dr["TotalPercentile"] = String.Format("{0:P1}", Math.Round(totalPercentile, 3));// String.Format("{0:F1}", 100 * Math.Round(totalPercentile, 3));
                    dr["TotalCount"] = tScoreCount;
                    dr["TotalCumeCount"] = totalCumCount;

					dr = dt2.NewRow();
					dr["TotalScore"] = aryTscore[j];

                    SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,ContestPeriod,ContestDate,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + tScoreCount + "," + totalCumCount + ",'" + ContestPeriod + "','" + ContestDays + "','" + Session["LoginID"] + "', GETDATE())";

                    //SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + Session["LoginID"] + "', GETDATE())";
                    SQLInsertCutOff = "";
                    SQLInsertCutOffVal = "";

                    totalCumCount = totalCumCount - cCount;
                    cCount = 1;

					for(int q=0; q < 13; q++)
						grCount[q] = 0;
					if((grScore[j] == 1) || (grScore[j] == 0))
					{
						grCount[1] =  1;
					}
					if(grScore[j] == 2)
					{
						grCount[2] =  1;
					}
					if(grScore[j] == 3)
					{
						grCount[3] = 1;
					}
					if(grScore[j] == 4)
					{
						grCount[4] =  1;
					}
					if(grScore[j] == 5)
					{
						grCount[5] =  1;
					}
					if(grScore[j] == 6)
					{
						grCount[6] =  1;
					}
					if(grScore[j] == 7)
					{
						grCount[7] =  1;
					}
					if(grScore[j] == 8)
					{
						grCount[8] =  1;
					}
					if(grScore[j] == 9)
					{
						grCount[9] =  1;
					}
					if(grScore[j] == 10)
					{
						grCount[10] =  1;
					}
					if(grScore[j] == 11)
					{
						grCount[11] =  1;
					}
					if(grScore[j] == 12)
					{
						grCount[12] = 1;
					}
                }
  			}
  
           SQLInsertCutOff = "Insert into CutOffScoresByPercentile ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
           SQLInsertCutOffVal = ")Values(" + ddlYear.SelectedValue + ",1,"+ ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + lastScore + ""; // Year
          
			switch(contestType)
			{
                case "MB1":
					pc[1] = (float)grCount[1]/grTCount[1];
					dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
					//grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
					pcile[1] = (float)grCCount[1]/grTCount[1];
					dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
					dr["Gr1cnt"] = grCount[1];
					dr["Gr1cum"] = grCCount[1];
					pc[2] = (float)grCount[2]/grTCount[2];
					dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
					//grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
					pcile[2] = (float)grCCount[2]/grTCount[2];
					dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));
					dr["Gr2cnt"] = grCount[2];
					dr["Gr2cum"] = grCCount[2];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];

					break;
				case "MB2":
				case "EW1":
					pc[3] = (float)grCount[3]/grTCount[3];
					dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
					//grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[0];
                    aryCCount[0] -= grCount[3];
					pcile[3] = (float)grCCount[3]/grTCount[3];
					dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
					dr["Gr3cnt"] = grCount[3];
					dr["Gr3cum"] = grCCount[3];
					pc[4] = (float)grCount[4]/grTCount[4];
					dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
					//grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[1];
                    aryCCount[1] -= grCount[4];
					pcile[4] = (float)grCCount[4]/grTCount[4];
					dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
					dr["Gr4cnt"] = grCount[4];
					dr["Gr4cum"] = grCCount[4];
					pc[5] = (float)grCount[5]/grTCount[5];
					dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
					//grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[2];
                    aryCCount[2] -= grCount[5];
					pcile[5] = (float)grCCount[5]/grTCount[5];
					dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
					dr["Gr5cnt"] = grCount[5];
					dr["Gr5cum"] = grCCount[5];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

					break;
				case "MB3":
				case "EW2":
				case "PS1":
					pc[6] = (float)grCount[6]/grTCount[6];
					dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
					//grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[0];
                    aryCCount[0] -= grCount[6];
					pcile[6] = (float)grCCount[6]/grTCount[6];
					dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
					dr["Gr6cnt"] = grCount[6];
					dr["Gr6cum"] = grCCount[6];
					pc[7] = (float)grCount[7]/grTCount[7];
					dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
					//grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[1];
                    aryCCount[1] -= grCount[7];
					pcile[7] = (float)grCCount[7]/grTCount[7];
					dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
					dr["Gr7cnt"] = grCount[7];
					dr["Gr7cum"] = grCCount[7];
					pc[8] = (float)grCount[8]/grTCount[8];
					dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
					//grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[2];
                    aryCCount[2] -= grCount[8];
					pcile[8] = (float)grCCount[8]/grTCount[8];
					dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
					dr["Gr8cnt"] = grCount[8];
					dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

					break;
				case "MB4":
					pc[9] = (float)grCount[9]/grTCount[9];
					dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
					//grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
					pcile[9] = (float)grCCount[9]/grTCount[9];
					dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
					dr["Gr9cnt"] = grCount[9];
					dr["Gr9cum"] = grCCount[9];
					pc[10] = (float)grCount[10]/grTCount[10];
					dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
					//grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
					pcile[10] = (float)grCCount[10]/grTCount[10];
					dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
					dr["Gr10cnt"] = grCount[10];
					dr["Gr10cum"] = grCCount[10];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

					break;

				case "JSB":
                case "JSC":
				case "JGB":
					pc[1] = (float)grCount[1]/grTCount[1];
					dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
					//grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
					pcile[1] = (float)grCCount[1]/grTCount[1];
					dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
					dr["Gr1cnt"] = grCount[1];
					dr["Gr1cum"] = grCCount[1];
					pc[2] = (float)grCount[2]/grTCount[2];
					dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
					//grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
					pcile[2] = (float)grCCount[2]/grTCount[2];
					dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));
					dr["Gr2cnt"] = grCount[2];
					dr["Gr2cum"] = grCCount[2];
					pc[3] = (float)grCount[3]/grTCount[3];
					dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
					//grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[2];
                    aryCCount[2] -= grCount[3];
					pcile[3] = (float)grCCount[3]/grTCount[3];
					dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
					dr["Gr3cnt"] = grCount[3];
					dr["Gr3cum"] = grCCount[3];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

					break;
                case "ISC":
                    pc[4] = (float)grCount[4] / grTCount[4];
                    dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
                    //grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
                    pcile[4] = (float)grCCount[4] / grTCount[4];
                    dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
                    dr["Gr4cnt"] = grCount[4];
                    dr["Gr4cum"] = grCCount[4];
                    pc[5] = (float)grCount[5] / grTCount[5];
                    dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
                    //grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
                    pcile[5] = (float)grCCount[5] / grTCount[5];
                    dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
                    dr["Gr5cnt"] = grCount[5];
                    dr["Gr5cum"] = grCCount[5];
                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
                    break;
                case "SSC":
                    pc[6] = (float)grCount[6] / grTCount[6];
                    dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
                    //grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[0];
                    aryCCount[0] -= grCount[6];
                    pcile[6] = (float)grCCount[6] / grTCount[6];
                    dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
                    dr["Gr6cnt"] = grCount[6];
                    dr["Gr6cum"] = grCCount[6];
                    pc[7] = (float)grCount[7] / grTCount[7];
                    dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
                    //grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[1];
                    aryCCount[1] -= grCount[7];
                    pcile[7] = (float)grCCount[7] / grTCount[7];
                    dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
                    dr["Gr7cnt"] = grCount[7];
                    dr["Gr7cum"] = grCCount[7];
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[2];
                    aryCCount[2] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];
                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];
                      break;
				case "SSB":
				case "SGB":
					pc[4] = (float)grCount[4]/grTCount[4];
					dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
					//grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
					pcile[4] = (float)grCCount[4]/grTCount[4];
					dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
					dr["Gr4cnt"] = grCount[4];
					dr["Gr4cum"] = grCCount[4];
					pc[5] = (float)grCount[5]/grTCount[5];
					dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
					//grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
					pcile[5] = (float)grCCount[5]/grTCount[5];
					dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
					dr["Gr5cnt"] = grCount[5];
					dr["Gr5cum"] = grCCount[5];
					pc[6] = (float)grCount[6]/grTCount[6];
					dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
					//grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[2];
                    aryCCount[2] -= grCount[6];
					pcile[6] = (float)grCCount[6]/grTCount[6];
					dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
					dr["Gr6cnt"] = grCount[6];
					dr["Gr6cum"] = grCCount[6];
					pc[7] = (float)grCount[7]/grTCount[7];
					dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
					//grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[3];
                    aryCCount[3] -= grCount[7];
					pcile[7] = (float)grCCount[7]/grTCount[7];
					dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
					dr["Gr7cnt"] = grCount[7];
					dr["Gr7cum"] = grCCount[7];
					pc[8] = (float)grCount[8]/grTCount[8];
					dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
					//grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[4];
                    aryCCount[4] -= grCount[8];
					pcile[8] = (float)grCCount[8]/grTCount[8];
					dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
					dr["Gr8cnt"] = grCount[8];
					dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

					break;
				case "JVB":
					pc[1] = (float)grCount[1]/grTCount[1];
					dr["Gr1%"] =String.Format("{0:P1}",Math.Round(pc[1],3));
					//grCCount[1] += grCount[1];
                    grCCount[1] = aryCCount[0];
                    aryCCount[0] -= grCount[1];
					pcile[1] = (float)grCCount[1]/grTCount[1];
					dr["Gr1%tile"] =String.Format("{0:P1}",Math.Round(pcile[1],3));
					dr["Gr1cnt"] = grCount[1];
					dr["Gr1cum"] = grCCount[1];
					pc[2] = (float)grCount[2]/grTCount[2];
					dr["Gr2%"] =String.Format("{0:P1}",Math.Round(pc[2],3));
					//grCCount[2] += grCount[2];
                    grCCount[2] = aryCCount[1];
                    aryCCount[1] -= grCount[2];
					pcile[2] = (float)grCCount[2]/grTCount[2];
					dr["Gr2%tile"] =String.Format("{0:P1}",Math.Round(pcile[2],3));
					dr["Gr2cnt"] = grCount[2];
					dr["Gr2cum"] = grCCount[2];
					pc[3] = (float)grCount[3]/grTCount[3];
					dr["Gr3%"] =String.Format("{0:P1}",Math.Round(pc[3],3));
					//grCCount[3] += grCount[3];
                    grCCount[3] = aryCCount[2];
                    aryCCount[2] -= grCount[3];
					pcile[3] = (float)grCCount[3]/grTCount[3];
					dr["Gr3%tile"] =String.Format("{0:P1}",Math.Round(pcile[3],3));
					dr["Gr3cnt"] = grCount[3];
					dr["Gr3cum"] = grCCount[3];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

					break;
				case "IVB":
					pc[4] = (float)grCount[4]/grTCount[4];
					dr["Gr4%"] =String.Format("{0:P1}",Math.Round(pc[4],3));
					//grCCount[4] += grCount[4];
                    grCCount[4] = aryCCount[0];
                    aryCCount[0] -= grCount[4];
					pcile[4] = (float)grCCount[4]/grTCount[4];
					dr["Gr4%tile"] =String.Format("{0:P1}",Math.Round(pcile[4],3));
					dr["Gr4cnt"] = grCount[4];
					dr["Gr4cum"] = grCCount[4];
					pc[5] = (float)grCount[5]/grTCount[5];
					dr["Gr5%"] =String.Format("{0:P1}",Math.Round(pc[5],3));
					//grCCount[5] += grCount[5];
                    grCCount[5] = aryCCount[1];
                    aryCCount[1] -= grCount[5];
					pcile[5] = (float)grCCount[5]/grTCount[5];
					dr["Gr5%tile"] =String.Format("{0:P1}",Math.Round(pcile[5],3));
					dr["Gr5cnt"] = grCount[5];
					dr["Gr5cum"] = grCCount[5];
					pc[6] = (float)grCount[6]/grTCount[6];
					dr["Gr6%"] =String.Format("{0:P1}",Math.Round(pc[6],3));
					//grCCount[6] += grCount[6];
                    grCCount[6] = aryCCount[2];
                    aryCCount[2] -= grCount[6];
					pcile[6] = (float)grCCount[6]/grTCount[6];
					dr["Gr6%tile"] =String.Format("{0:P1}",Math.Round(pcile[6],3));
					dr["Gr6cnt"] = grCount[6];
					dr["Gr6cum"] = grCCount[6];
					pc[7] = (float)grCount[7]/grTCount[7];
					dr["Gr7%"] =String.Format("{0:P1}",Math.Round(pc[7],3));
					//grCCount[7] += grCount[7];
                    grCCount[7] = aryCCount[3];
                    aryCCount[3] -= grCount[7];
					pcile[7] = (float)grCCount[7]/grTCount[7];
					dr["Gr7%tile"] =String.Format("{0:P1}",Math.Round(pcile[7],3));
					dr["Gr7cnt"] = grCount[7];
					dr["Gr7cum"] = grCCount[7];
					pc[8] = (float)grCount[8]/grTCount[8];
					dr["Gr8%"] =String.Format("{0:P1}",Math.Round(pc[8],3));
					//grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[4];
                    aryCCount[4] -= grCount[8];
					pcile[8] = (float)grCCount[8]/grTCount[8];
					dr["Gr8%tile"] =String.Format("{0:P1}",Math.Round(pcile[8],3));
					dr["Gr8cnt"] = grCount[8];
					dr["Gr8cum"] = grCCount[8];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

					break;
				case "SVB":
					pc[9] = (float)grCount[9]/grTCount[9];
					dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
					//grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
					pcile[9] = (float)grCCount[9]/grTCount[9];
					dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
					dr["Gr9cnt"] = grCount[9];
					dr["Gr9cum"] = grCCount[9];
					pc[10] = (float)grCount[10]/grTCount[10];
					dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
					//grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
					pcile[10] = (float)grCCount[10]/grTCount[10];
					dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
					dr["Gr10cnt"] = grCount[10];
					dr["Gr10cum"] = grCCount[10];
					pc[11] = (float)grCount[11]/grTCount[11];
					dr["Gr11%"] =String.Format("{0:P1}",Math.Round(pc[11],3));
					//grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[2];
                    aryCCount[2] -= grCount[11];
					pcile[11] = (float)grCCount[11]/grTCount[11];
					dr["Gr11%tile"] =String.Format("{0:P1}",Math.Round(pcile[11],3));
					dr["Gr11cnt"] = grCount[11];
					dr["Gr11cum"] = grCCount[11];
					if(grTCount[12] == 0)
					{
						pc[12] = 0;
						dr["Gr12%"] = "";
						dr["Gr12%tile"] = "";
					}
					else
					{
						pc[12] = (float)grCount[12]/grTCount[12];
						dr["Gr12%"] =String.Format("{0:P1}",Math.Round(pc[12],3));
						//grCCount[12] += grCount[12];
                        grCCount[12] = aryCCount[3];
                        aryCCount[3] -= grCount[12];
						pcile[12] = (float)grCCount[12]/grTCount[12];
						dr["Gr12%tile"] =String.Format("{0:P1}",Math.Round(pcile[12],3));
					}
					dr["Gr12cnt"] = grCount[12];
					dr["Gr12cum"] = grCCount[12];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

					break;
				case "EW3":
				case "PS3":
					pc[9] = (float)grCount[9]/grTCount[9];
					dr["Gr9%"] =String.Format("{0:P1}",Math.Round(pc[9],3));
					//grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[0];
                    aryCCount[0] -= grCount[9];
					pcile[9] = (float)grCCount[9]/grTCount[9];
					dr["Gr9%tile"] =String.Format("{0:P1}",Math.Round(pcile[9],3));
					dr["Gr9cnt"] = grCount[9];
					dr["Gr9cum"] = grCCount[9];
					pc[10] = (float)grCount[10]/grTCount[10];
					dr["Gr10%"] =String.Format("{0:P1}",Math.Round(pc[10],3));
					//grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[1];
                    aryCCount[1] -= grCount[10];
					pcile[10] = (float)grCCount[10]/grTCount[10];
					dr["Gr10%tile"] =String.Format("{0:P1}",Math.Round(pcile[10],3));
					dr["Gr10cnt"] = grCount[10];
					dr["Gr10cum"] = grCCount[10];
					pc[11] = (float)grCount[11]/grTCount[11];
					dr["Gr11%"] =String.Format("{0:P1}",Math.Round(pc[11],3));
					//grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[2];
                    aryCCount[2] -= grCount[11];
					pcile[11] = (float)grCCount[11]/grTCount[11];
					dr["Gr11%tile"] =String.Format("{0:P1}",Math.Round(pcile[11],3));
					dr["Gr11cnt"] = grCount[11];
					dr["Gr11cum"] = grCCount[11];
					if(grTCount[12] == 0)
					{
						pc[12] = 0;
						dr["Gr12%"] = "";
						dr["Gr12%tile"] = "";
					}
					else
					{
						pc[12] = (float)grCount[12]/grTCount[12];
						dr["Gr12%"] =String.Format("{0:P1}",Math.Round(pc[12],3));
						//grCCount[12] += grCount[12];
                        grCCount[12] = aryCCount[3];
                        aryCCount[3] -= grCount[12];
						pcile[12] = (float)grCCount[12]/grTCount[12];
						dr["Gr12%tile"] =String.Format("{0:P1}",Math.Round(pcile[12],3));
					}
					dr["Gr12cnt"] = grCount[12];
					dr["Gr12cum"] = grCCount[12];


                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];
                    break;

                case "BB":
                    pc[8] = (float)grCount[8] / grTCount[8];
                    dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
                    //grCCount[8] += grCount[8];
                    grCCount[8] = aryCCount[0];
                    aryCCount[0] -= grCount[8];
                    pcile[8] = (float)grCCount[8] / grTCount[8];
                    dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
                    dr["Gr8cnt"] = grCount[8];
                    dr["Gr8cum"] = grCCount[8];
                    pc[9] = (float)grCount[9] / grTCount[9];
                    dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
                    //grCCount[9] += grCount[9];
                    grCCount[9] = aryCCount[1];
                    aryCCount[1] -= grCount[9];
                    pcile[9] = (float)grCCount[9] / grTCount[9];
                    dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
                    dr["Gr9cnt"] = grCount[9];
                    dr["Gr9cum"] = grCCount[9];
                    pc[10] = (float)grCount[10] / grTCount[10];
                    dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
                    //grCCount[10] += grCount[10];
                    grCCount[10] = aryCCount[2];
                    aryCCount[2] -= grCount[10];
                    pcile[10] = (float)grCCount[10] / grTCount[10];
                    dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
                    dr["Gr10cnt"] = grCount[10];
                    dr["Gr10cum"] = grCCount[10];
                    pc[11] = (float)grCount[11] / grTCount[11];
                    dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
                    //grCCount[11] += grCount[11];
                    grCCount[11] = aryCCount[3];
                    aryCCount[3] -= grCount[11];
                    pcile[11] = (float)grCCount[11] / grTCount[11];
                    dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
                    dr["Gr11cnt"] = grCount[11];
                    dr["Gr11cum"] = grCCount[11];

                    SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum";
                    SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"] + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] ;
		    		break;
			}

            totalPercentile = (float)totalCumCount / aryTscore.Length;
            dr["TotalPercentile"] = String.Format("{0:P1}", Math.Round(totalPercentile, 3));
            dr["TotalCount"] = cCount;
            dr["TotalCumeCount"] = totalCumCount;

            SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,ContestPeriod,ContestDate,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + cCount + "," + totalCumCount + ",'" + ContestPeriod +"','"+ ContestDays + "','" + Session["LoginID"] + "', GETDATE())";
            // SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + cCount + "," + totalCumCount + ",'" + Session["LoginID"] + "', GETDATE())";                                                                                                                                                                                                                              ] = ;

           dt2.Rows.Add(dr);
            //DataGrid1.Caption = contestType + " as of " + ddlYear.SelectedValue;// DateTime.Today.ToShortDateString();
            //DataGrid1.DataSource = dt2;
            //DataGrid1.DataBind();
           // DataGrid1.Visible = true;
            //for (int s = StartGrade; s <= EndGrade; s++)
            //{
            //    if (dr["Gr" + s + "%tile"].ToString() == "NaN")
            //    {
            //        Contest_Flag = true;
            //    }
            //}
     
            if (Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, SQLExecString)) > 0)
            {
                    lblSuccess.Text = "Percentiles Calculation done for the Year " + ddlYear.SelectedValue;
                    SQLExecString = "";
            }
       }
       catch (Exception ex)
       {
               //Response.Write(ex.ToString());
       }
    }

   // void ProcessData(DataSet ds)
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        DataRow dr;
    //        dt.Columns.Add("Contestant", typeof(int));
    //        dt.Columns.Add("TotalScore", typeof(int));
    //        dt.Columns.Add("Grade", typeof(int));
    //        int curGrade = -1;
    //        int count = -1;
    //        int[] aryCCount = new int[5];
    //        int[] aryTscore = new int[ds.Tables[0].Rows.Count];
    //        int[] grScore = new int[ds.Tables[0].Rows.Count];
    //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //        {
    //            dr = dt.NewRow();
    //            dr["Contestant"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
    //            dr["TotalScore"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
    //                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
    //            dr["Grade"] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);

    //            //if ((curGrade == Convert.ToInt32(dr["Grade"]) || ((curGrade == 0) || (curGrade == 1)) && (Convert.ToInt32(dr["Grade"]) == 0) || (Convert.ToInt32(dr["Grade"]) == 1)))
    //            //{
    //            //    aryCCount[count] = aryCCount[count] + 1;
    //            //}
    //            //else
    //            //{
    //            //    curGrade = Convert.ToInt32(dr["Grade"]);
    //            //    aryCCount[++count] = 1;
    //            //}
              
    //            grScore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[8]);
    //            aryTscore[i] = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3]) +
    //                Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[4]);
    //            dt.Rows.Add(dr);
    //        }

    //        SortArrays(aryTscore, grScore);

    //        dt2 = new DataTable();
    //        dt2.Columns.Add("TotalScore", typeof(int));
    //        switch (contestType)
    //        {
    //            case "MB1":
    //                dt2.Columns.Add("Gr1%", typeof(string));
    //                dt2.Columns.Add("Gr2%", typeof(string));
    //                dt2.Columns.Add("Gr1%tile", typeof(string));
    //                dt2.Columns.Add("Gr1cnt", typeof(string));
    //                dt2.Columns.Add("Gr1cum", typeof(string));
    //                dt2.Columns.Add("Gr2%tile", typeof(string));
    //                dt2.Columns.Add("Gr2cnt", typeof(string));
    //                dt2.Columns.Add("Gr2cum", typeof(string));
    //                StartGrade = 1;
    //                EndGrade = 2;
    //                break;
    //            case "MB2":
    //                dt2.Columns.Add("Gr3%", typeof(string));
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr3%tile", typeof(string));
    //                dt2.Columns.Add("Gr3cnt", typeof(string));
    //                dt2.Columns.Add("Gr3cum", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                StartGrade = 3;
    //                EndGrade = 5;
    //                break;
    //            case "MB3":
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                StartGrade = 6;
    //                EndGrade = 8;
    //                break;
    //            case "MB4":
    //                dt2.Columns.Add("Gr9%", typeof(string));
    //                dt2.Columns.Add("Gr10%", typeof(string));
    //                dt2.Columns.Add("Gr9%tile", typeof(string));
    //                dt2.Columns.Add("Gr9cnt", typeof(string));
    //                dt2.Columns.Add("Gr9cum", typeof(string));
    //                dt2.Columns.Add("Gr10%tile", typeof(string));
    //                dt2.Columns.Add("Gr10cnt", typeof(string));
    //                dt2.Columns.Add("Gr10cum", typeof(string));
    //                StartGrade = 9;
    //                EndGrade = 10;
    //                break;
    //            case "JSB":
    //                dt2.Columns.Add("Gr1%", typeof(string));
    //                dt2.Columns.Add("Gr2%", typeof(string));
    //                dt2.Columns.Add("Gr3%", typeof(string));
    //                dt2.Columns.Add("Gr1%tile", typeof(string));
    //                dt2.Columns.Add("Gr1cnt", typeof(string));
    //                dt2.Columns.Add("Gr1cum", typeof(string));
    //                dt2.Columns.Add("Gr2%tile", typeof(string));
    //                dt2.Columns.Add("Gr2cnt", typeof(string));
    //                dt2.Columns.Add("Gr2cum", typeof(string));
    //                dt2.Columns.Add("Gr3%tile", typeof(string));
    //                dt2.Columns.Add("Gr3cnt", typeof(string));
    //                dt2.Columns.Add("Gr3cum", typeof(string));
    //                StartGrade = 1;
    //                EndGrade = 3;
    //                break;
    //            case "SSB":
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                StartGrade = 4;
    //                EndGrade = 8;
    //                break;
    //            case "JVB":
    //            case "JSC":
    //                dt2.Columns.Add("Gr1%", typeof(string));
    //                dt2.Columns.Add("Gr2%", typeof(string));
    //                dt2.Columns.Add("Gr3%", typeof(string));
    //                dt2.Columns.Add("Gr1%tile", typeof(string));
    //                dt2.Columns.Add("Gr1cnt", typeof(string));
    //                dt2.Columns.Add("Gr1cum", typeof(string));
    //                dt2.Columns.Add("Gr2%tile", typeof(string));
    //                dt2.Columns.Add("Gr2cnt", typeof(string));
    //                dt2.Columns.Add("Gr2cum", typeof(string));
    //                dt2.Columns.Add("Gr3%tile", typeof(string));
    //                dt2.Columns.Add("Gr3cnt", typeof(string));
    //                dt2.Columns.Add("Gr3cum", typeof(string));
    //                StartGrade = 1;
    //                EndGrade = 3;
    //                break;
    //            case "ISC":
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                StartGrade = 4;
    //                EndGrade = 5;
    //                break;
    //            case "IVB":
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                StartGrade = 4;
    //                EndGrade = 7;
    //                break;
    //            case "SVB":
    //            case "BB":
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr9%", typeof(string));
    //                dt2.Columns.Add("Gr10%", typeof(string));
    //                dt2.Columns.Add("Gr11%", typeof(string));
    //                dt2.Columns.Add("Gr12%", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                dt2.Columns.Add("Gr9%tile", typeof(string));
    //                dt2.Columns.Add("Gr9cnt", typeof(string));
    //                dt2.Columns.Add("Gr9cum", typeof(string));
    //                dt2.Columns.Add("Gr10%tile", typeof(string));
    //                dt2.Columns.Add("Gr10cnt", typeof(string));
    //                dt2.Columns.Add("Gr10cum", typeof(string));
    //                dt2.Columns.Add("Gr11%tile", typeof(string));
    //                dt2.Columns.Add("Gr11cnt", typeof(string));
    //                dt2.Columns.Add("Gr11cum", typeof(string));
    //                dt2.Columns.Add("Gr12%tile", typeof(string));
    //                dt2.Columns.Add("Gr12cnt", typeof(string));
    //                dt2.Columns.Add("Gr12cum", typeof(string));
    //                StartGrade = 8;
    //                EndGrade = 12;
    //                break;
    //            case "JGB":
    //                dt2.Columns.Add("Gr1%", typeof(string));
    //                dt2.Columns.Add("Gr2%", typeof(string));
    //                dt2.Columns.Add("Gr3%", typeof(string));
    //                dt2.Columns.Add("Gr1%tile", typeof(string));
    //                dt2.Columns.Add("Gr1cnt", typeof(string));
    //                dt2.Columns.Add("Gr1cum", typeof(string));
    //                dt2.Columns.Add("Gr2%tile", typeof(string));
    //                dt2.Columns.Add("Gr2cnt", typeof(string));
    //                dt2.Columns.Add("Gr2cum", typeof(string));
    //                dt2.Columns.Add("Gr3%tile", typeof(string));
    //                dt2.Columns.Add("Gr3cnt", typeof(string));
    //                dt2.Columns.Add("Gr3cum", typeof(string));
    //                StartGrade = 1;
    //                EndGrade = 3;
    //                break;
    //            case "SGB":
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                StartGrade =4;
    //                EndGrade = 8;
    //                break;
    //            case "EW1":
    //                dt2.Columns.Add("Gr3%", typeof(string));
    //                dt2.Columns.Add("Gr4%", typeof(string));
    //                dt2.Columns.Add("Gr5%", typeof(string));
    //                dt2.Columns.Add("Gr3%tile", typeof(string));
    //                dt2.Columns.Add("Gr3cnt", typeof(string));
    //                dt2.Columns.Add("Gr3cum", typeof(string));
    //                dt2.Columns.Add("Gr4%tile", typeof(string));
    //                dt2.Columns.Add("Gr4cnt", typeof(string));
    //                dt2.Columns.Add("Gr4cum", typeof(string));
    //                dt2.Columns.Add("Gr5%tile", typeof(string));
    //                dt2.Columns.Add("Gr5cnt", typeof(string));
    //                dt2.Columns.Add("Gr5cum", typeof(string));
    //                StartGrade = 3;
    //                EndGrade = 5;
    //                break;
    //            case "EW2":
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                StartGrade = 6;
    //                EndGrade = 8;
    //                break;
    //            case "EW3":
    //                dt2.Columns.Add("Gr9%", typeof(string));
    //                dt2.Columns.Add("Gr10%", typeof(string));
    //                dt2.Columns.Add("Gr11%", typeof(string));
    //                dt2.Columns.Add("Gr12%", typeof(string));
    //                dt2.Columns.Add("Gr9%tile", typeof(string));
    //                dt2.Columns.Add("Gr9cnt", typeof(string));
    //                dt2.Columns.Add("Gr9cum", typeof(string));
    //                dt2.Columns.Add("Gr10%tile", typeof(string));
    //                dt2.Columns.Add("Gr10cnt", typeof(string));
    //                dt2.Columns.Add("Gr10cum", typeof(string));
    //                dt2.Columns.Add("Gr11%tile", typeof(string));
    //                dt2.Columns.Add("Gr11cnt", typeof(string));
    //                dt2.Columns.Add("Gr11cum", typeof(string));
    //                dt2.Columns.Add("Gr12%tile", typeof(string));
    //                dt2.Columns.Add("Gr12cnt", typeof(string));
    //                dt2.Columns.Add("Gr12cum", typeof(string));
    //                StartGrade = 9;
    //                EndGrade = 12;
    //                break;
    //            case "SSC":
    //            case "PS1":
    //                dt2.Columns.Add("Gr6%", typeof(string));
    //                dt2.Columns.Add("Gr7%", typeof(string));
    //                dt2.Columns.Add("Gr8%", typeof(string));
    //                dt2.Columns.Add("Gr6%tile", typeof(string));
    //                dt2.Columns.Add("Gr6cnt", typeof(string));
    //                dt2.Columns.Add("Gr6cum", typeof(string));
    //                dt2.Columns.Add("Gr7%tile", typeof(string));
    //                dt2.Columns.Add("Gr7cnt", typeof(string));
    //                dt2.Columns.Add("Gr7cum", typeof(string));
    //                dt2.Columns.Add("Gr8%tile", typeof(string));
    //                dt2.Columns.Add("Gr8cnt", typeof(string));
    //                dt2.Columns.Add("Gr8cum", typeof(string));
    //                StartGrade = 6;
    //                EndGrade = 8;
    //                break;
    //            case "PS3":
    //                dt2.Columns.Add("Gr9%", typeof(string));
    //                dt2.Columns.Add("Gr10%", typeof(string));
    //                dt2.Columns.Add("Gr11%", typeof(string));
    //                dt2.Columns.Add("Gr12%", typeof(string));
    //                dt2.Columns.Add("Gr9%tile", typeof(string));
    //                dt2.Columns.Add("Gr9cnt", typeof(string));
    //                dt2.Columns.Add("Gr9cum", typeof(string));
    //                dt2.Columns.Add("Gr10%tile", typeof(string));
    //                dt2.Columns.Add("Gr10cnt", typeof(string));
    //                dt2.Columns.Add("Gr10cum", typeof(string));
    //                dt2.Columns.Add("Gr11%tile", typeof(string));
    //                dt2.Columns.Add("Gr11cnt", typeof(string));
    //                dt2.Columns.Add("Gr11cum", typeof(string));
    //                dt2.Columns.Add("Gr12%tile", typeof(string));
    //                dt2.Columns.Add("Gr12cnt", typeof(string));
    //                dt2.Columns.Add("Gr12cum", typeof(string));
    //                StartGrade = 9;
    //                EndGrade = 12;
    //                break;
    //        }

    //        dt2.Columns.Add("TotalPercentile", typeof(string));
    //        dt2.Columns.Add("TotalCount", typeof(double));
    //        dt2.Columns.Add("TotalCumeCount", typeof(double));
		

    //        dr = dt2.NewRow();
    //        if (aryTscore.Length > 0)
    //            dr["TotalScore"] = aryTscore[0];

    //        int[] grCount = new int[13];
    //        int[] grCCount = new int[13];
    //        int[] grTCount = new int[13];
    //        float[] pc = new float[13];
    //        float[] pcile = new float[13];
    //        for (int k = 0; k < grScore.Length; k++)
    //        {
    //            switch (grScore[k])
    //            {
    //                case 0:
    //                case 1:
    //                    grTCount[1]++;
    //                    break;
    //                case 2:
    //                    grTCount[2]++;
    //                    break;
    //                case 3:
    //                    grTCount[3]++;
    //                    break;
    //                case 4:
    //                    grTCount[4]++;
    //                    break;
    //                case 5:
    //                    grTCount[5]++;
    //                    break;
    //                case 6:
    //                    grTCount[6]++;
    //                    break;
    //                case 7:
    //                    grTCount[7]++;
    //                    break;
    //                case 8:
    //                    grTCount[8]++;
    //                    break;
    //                case 9:
    //                    grTCount[9]++;
    //                    break;
    //                case 10:
    //                    grTCount[10]++;
    //                    break;
    //                case 11:
    //                    grTCount[11]++;
    //                    break;
    //                case 12:
    //                    grTCount[12]++;
    //                    break;
    //            }
    //        }
    //        int lastScore = 0;
    //        if (aryTscore.Length > 0)
    //            lastScore = aryTscore[0];

    //        String SQLInsertCutOff = "";
    //        String SQLInsertCutOffVal = "";
    //        Boolean Contest_Flag = false;
    //        int j;

    //        int totalCumCount = 0, totalCount = 0, tScoreCount = 0, cCount = 0;
    //        double totalPercentile = 0.0;

    //        totalCount = aryTscore.Length;
           
    //        for (j = 0; j < aryTscore.Length; j++)
    //        {
    //            if (lastScore == aryTscore[j])
    //            {
    //                if ((grScore[j] == 1) || (grScore[j] == 0))
    //                {
    //                    grCount[1] = grCount[1] + 1;
    //                }
    //                if (grScore[j] == 2)
    //                {
    //                    grCount[2] = grCount[2] + 1;
    //                }
    //                if (grScore[j] == 3)
    //                {
    //                    grCount[3] = grCount[3] + 1;
    //                }
    //                if (grScore[j] == 4)
    //                {
    //                    grCount[4] = grCount[4] + 1;
    //                }
    //                if (grScore[j] == 5)
    //                {
    //                    grCount[5] = grCount[5] + 1;
    //                }
    //                if (grScore[j] == 6)
    //                {
    //                    grCount[6] = grCount[6] + 1;
    //                }
    //                if (grScore[j] == 7)
    //                {
    //                    grCount[7] = grCount[7] + 1;
    //                }
    //                if (grScore[j] == 8)
    //                {
    //                    grCount[8] = grCount[8] + 1;
    //                }
    //                if (grScore[j] == 9)
    //                {
    //                    grCount[9] = grCount[9] + 1;
    //                }
    //                if (grScore[j] == 10)
    //                {
    //                    grCount[10] = grCount[10] + 1;
    //                }
    //                if (grScore[j] == 11)
    //                {
    //                    grCount[11] = grCount[11] + 1;
    //                }
    //                if (grScore[j] == 12)
    //                {
    //                    grCount[12] = grCount[12] + 1;
    //                }
    //            }
    //            else
    //            {
    //                lastScore = aryTscore[j];

    //                SQLInsertCutOff = "Insert into CutOffScoresByPercentile ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
    //                SQLInsertCutOffVal = ") Values (" + ddlYear.SelectedValue + ",2," + ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + aryTscore[j - 1] + ""; //Year

    //                switch (contestType)
    //                {
    //                    case "MB1":
    //                        pc[1] = (float)grCount[1] / grTCount[1];
    //                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                        grCCount[1] += grCount[1];
    //                        pcile[1] = (float)grCCount[1] / grTCount[1];
    //                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                        dr["Gr1cnt"] = grCount[1];
    //                        dr["Gr1cum"] = grCCount[1];
    //                        pc[2] = (float)grCount[2] / grTCount[2];
    //                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                        grCCount[2] += grCount[2];
    //                        pcile[2] = (float)grCCount[2] / grTCount[2];
    //                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                        dr["Gr2cnt"] = grCount[2];
    //                        dr["Gr2cum"] = grCCount[2];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];
	
    //                        break;
    //                    case "MB2":
    //                    case "EW1":
    //                        pc[3] = (float)grCount[3] / grTCount[3];
    //                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                        grCCount[3] += grCount[3];
    //                        pcile[3] = (float)grCCount[3] / grTCount[3];
    //                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                        dr["Gr3cnt"] = grCount[3];
    //                        dr["Gr3cum"] = grCCount[3];
    //                        pc[4] = (float)grCount[4] / grTCount[4];
    //                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                        grCCount[4] += grCount[4];
    //                        pcile[4] = (float)grCCount[4] / grTCount[4];
    //                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                        dr["Gr4cnt"] = grCount[4];
    //                        dr["Gr4cum"] = grCCount[4];
    //                        pc[5] = (float)grCount[5] / grTCount[5];
    //                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                        grCCount[5] += grCount[5];
    //                        pcile[5] = (float)grCCount[5] / grTCount[5];
    //                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                        dr["Gr5cnt"] = grCount[5];
    //                        dr["Gr5cum"] = grCCount[5];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

    //                        break;
    //                    case "MB3":
    //                    case "EW2":
    //                    case "SSC":
    //                    case "PS1":
    //                        pc[6] = (float)grCount[6] / grTCount[6];
    //                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                        grCCount[6] += grCount[6];
    //                        pcile[6] = (float)grCCount[6] / grTCount[6];
    //                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                        dr["Gr6cnt"] = grCount[6];
    //                        dr["Gr6cum"] = grCCount[6];
    //                        pc[7] = (float)grCount[7] / grTCount[7];
    //                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                        grCCount[7] += grCount[7];
    //                        pcile[7] = (float)grCCount[7] / grTCount[7];
    //                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                        dr["Gr7cnt"] = grCount[7];
    //                        dr["Gr7cum"] = grCCount[7];
    //                        pc[8] = (float)grCount[8] / grTCount[8];
    //                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                        grCCount[8] += grCount[8];
    //                        pcile[8] = (float)grCCount[8] / grTCount[8];
    //                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                        dr["Gr8cnt"] = grCount[8];
    //                        dr["Gr8cum"] = grCCount[8];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

    //                        break;
    //                    case "MB4":
    //                        pc[9] = (float)grCount[9] / grTCount[9];
    //                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                        grCCount[9] += grCount[9];
    //                        pcile[9] = (float)grCCount[9] / grTCount[9];
    //                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                        dr["Gr9cnt"] = grCount[9];
    //                        dr["Gr9cum"] = grCCount[9];
    //                        pc[10] = (float)grCount[10] / grTCount[10];
    //                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                        grCCount[10] += grCount[10];
    //                        pcile[10] = (float)grCCount[10] / grTCount[10];
    //                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                        dr["Gr10cnt"] = grCount[10];
    //                        dr["Gr10cum"] = grCCount[10];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

    //                        break;

    //                    case "JSB":
    //                    case "JGB":
    //                    case "JSC":
    //                        pc[1] = (float)grCount[1] / grTCount[1];
    //                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                        grCCount[1] += grCount[1];
    //                        pcile[1] = (float)grCCount[1] / grTCount[1];
    //                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                        dr["Gr1cnt"] = grCount[1];
    //                        dr["Gr1cum"] = grCCount[1];
    //                        pc[2] = (float)grCount[2] / grTCount[2];
    //                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                        grCCount[2] += grCount[2];
    //                        pcile[2] = (float)grCCount[2] / grTCount[2];
    //                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                        dr["Gr2cnt"] = grCount[2];
    //                        dr["Gr2cum"] = grCCount[2];
    //                        pc[3] = (float)grCount[3] / grTCount[3];
    //                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                        grCCount[3] += grCount[3];
    //                        pcile[3] = (float)grCCount[3] / grTCount[3];
    //                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                        dr["Gr3cnt"] = grCount[3];
    //                        dr["Gr3cum"] = grCCount[3];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

    //                        break;
    //                    case "SSB":
    //                    case "SGB":
    //                        pc[4] = (float)grCount[4] / grTCount[4];
    //                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                        grCCount[4] += grCount[4];
    //                        pcile[4] = (float)grCCount[4] / grTCount[4];
    //                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                        dr["Gr4cnt"] = grCount[4];
    //                        dr["Gr4cum"] = grCCount[4];
    //                        pc[5] = (float)grCount[5] / grTCount[5];
    //                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                        grCCount[5] += grCount[5];
    //                        pcile[5] = (float)grCCount[5] / grTCount[5];
    //                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                        dr["Gr5cnt"] = grCount[5];
    //                        dr["Gr5cum"] = grCCount[5];
    //                        pc[6] = (float)grCount[6] / grTCount[6];
    //                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                        grCCount[6] += grCount[6];
    //                        pcile[6] = (float)grCCount[6] / grTCount[6];
    //                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                        dr["Gr6cnt"] = grCount[6];
    //                        dr["Gr6cum"] = grCCount[6];
    //                        pc[7] = (float)grCount[7] / grTCount[7];
    //                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                        grCCount[7] += grCount[7];
    //                        pcile[7] = (float)grCCount[7] / grTCount[7];
    //                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                        dr["Gr7cnt"] = grCount[7];
    //                        dr["Gr7cum"] = grCCount[7];
    //                        pc[8] = (float)grCount[8] / grTCount[8];
    //                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                        grCCount[8] += grCount[8];
    //                        pcile[8] = (float)grCCount[8] / grTCount[8];
    //                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                        dr["Gr8cnt"] = grCount[8];
    //                        dr["Gr8cum"] = grCCount[8];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

    //                        break;
    //                    case "JVB":
    //                        pc[1] = (float)grCount[1] / grTCount[1];
    //                        dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                        grCCount[1] += grCount[1];
    //                        pcile[1] = (float)grCCount[1] / grTCount[1];
    //                        dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                        dr["Gr1cnt"] = grCount[1];
    //                        dr["Gr1cum"] = grCCount[1];
    //                        pc[2] = (float)grCount[2] / grTCount[2];
    //                        dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                        grCCount[2] += grCount[2];
    //                        pcile[2] = (float)grCCount[2] / grTCount[2];
    //                        dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                        dr["Gr2cnt"] = grCount[2];
    //                        dr["Gr2cum"] = grCCount[2];
    //                        pc[3] = (float)grCount[3] / grTCount[3];
    //                        dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                        grCCount[3] += grCount[3];
    //                        pcile[3] = (float)grCCount[3] / grTCount[3];
    //                        dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                        dr["Gr3cnt"] = grCount[3];
    //                        dr["Gr3cum"] = grCCount[3];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

    //                        break;
    //                    case "IVB":
    //                        pc[4] = (float)grCount[4] / grTCount[4];
    //                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                        grCCount[4] += grCount[4];
    //                        pcile[4] = (float)grCCount[4] / grTCount[4];
    //                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                        dr["Gr4cnt"] = grCount[4];
    //                        dr["Gr4cum"] = grCCount[4];
    //                        pc[5] = (float)grCount[5] / grTCount[5];
    //                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                        grCCount[5] += grCount[5];
    //                        pcile[5] = (float)grCCount[5] / grTCount[5];
    //                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                        dr["Gr5cnt"] = grCount[5];
    //                        dr["Gr5cum"] = grCCount[5];
    //                        pc[6] = (float)grCount[6] / grTCount[6];
    //                        dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                        grCCount[6] += grCount[6];
    //                        pcile[6] = (float)grCCount[6] / grTCount[6];
    //                        dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                        dr["Gr6cnt"] = grCount[6];
    //                        dr["Gr6cum"] = grCCount[6];
    //                        pc[7] = (float)grCount[7] / grTCount[7];
    //                        dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                        grCCount[7] += grCount[7];
    //                        pcile[7] = (float)grCCount[7] / grTCount[7];
    //                        dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                        dr["Gr7cnt"] = grCount[7];
    //                        dr["Gr7cum"] = grCCount[7];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] ;

    //                        break;
    //                    case "ISC":
    //                        pc[4] = (float)grCount[4] / grTCount[4];
    //                        dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                        grCCount[4] += grCount[4];
    //                        pcile[4] = (float)grCCount[4] / grTCount[4];
    //                        dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                        dr["Gr4cnt"] = grCount[4];
    //                        dr["Gr4cum"] = grCCount[4];
    //                        pc[5] = (float)grCount[5] / grTCount[5];
    //                        dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                        grCCount[5] += grCount[5];
    //                        pcile[5] = (float)grCCount[5] / grTCount[5];
    //                        dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                        dr["Gr5cnt"] = grCount[5];
    //                        dr["Gr5cum"] = grCCount[5];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
   
    //                        break;
    //                    case "SVB":
    //                    case "BB":
    //                        pc[8] = (float)grCount[8] / grTCount[8];
    //                        dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                        grCCount[8] += grCount[8];
    //                        pcile[8] = (float)grCCount[8] / grTCount[8];
    //                        dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                        dr["Gr8cnt"] = grCount[8];
    //                        dr["Gr8cum"] = grCCount[8];
    //                        pc[9] = (float)grCount[9] / grTCount[9];
    //                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                        grCCount[9] += grCount[9];
    //                        pcile[9] = (float)grCCount[9] / grTCount[9];
    //                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                        dr["Gr9cnt"] = grCount[9];
    //                        dr["Gr9cum"] = grCCount[9];
    //                        pc[10] = (float)grCount[10] / grTCount[10];
    //                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                        grCCount[10] += grCount[10];
    //                        pcile[10] = (float)grCCount[10] / grTCount[10];
    //                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                        dr["Gr10cnt"] = grCount[10];
    //                        dr["Gr10cum"] = grCCount[10];
    //                        pc[11] = (float)grCount[11] / grTCount[11];
    //                        dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
    //                        grCCount[11] += grCount[11];
    //                        pcile[11] = (float)grCCount[11] / grTCount[11];
    //                        dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
    //                        dr["Gr11cnt"] = grCount[11];
    //                        dr["Gr11cum"] = grCCount[11];
    //                        if (grTCount[12] == 0)
    //                        {
    //                            pc[12] = 0;
    //                            dr["Gr12%"] = "";
    //                            dr["Gr12%tile"] = "";
    //                            dr["Gr12cnt"] = grCount[12];
    //                            dr["Gr12cum"] = grCCount[12];
    //                        }
    //                        else
    //                        {
    //                            pc[12] = (float)grCount[12] / grTCount[12];
    //                            dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
    //                            grCCount[12] += grCount[12];
    //                            pcile[12] = (float)grCCount[12] / grTCount[12];
    //                            dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
    //                            dr["Gr12cnt"] = grCount[12];
    //                            dr["Gr12cum"] = grCCount[12];
    //                        }

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"]+ ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

    //                        break;
    //                    case "EW3":
    //                    case "PS3":
    //                        pc[9] = (float)grCount[9] / grTCount[9];
    //                        dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                        grCCount[9] += grCount[9];
    //                        pcile[9] = (float)grCCount[9] / grTCount[9];
    //                        dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                        dr["Gr9cnt"] = grCount[9];
    //                        dr["Gr9cum"] = grCCount[9];
    //                        pc[10] = (float)grCount[10] / grTCount[10];
    //                        dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                        grCCount[10] += grCount[10];
    //                        pcile[10] = (float)grCCount[10] / grTCount[10];
    //                        dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                        dr["Gr10cnt"] = grCount[10];
    //                        dr["Gr10cum"] = grCCount[10];
    //                        pc[11] = (float)grCount[11] / grTCount[11];
    //                        dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
    //                        grCCount[11] += grCount[11];
    //                        pcile[11] = (float)grCCount[11] / grTCount[11];
    //                        dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
    //                        dr["Gr11cnt"] = grCount[11];
    //                        dr["Gr11cum"] = grCCount[11];
    //                        if (grTCount[12] == 0)
    //                        {
    //                            pc[12] = 0;
    //                            dr["Gr12%"] = "";
    //                            dr["Gr12%tile"] = "";
    //                        }
    //                        else
    //                        {
    //                            pc[12] = (float)grCount[12] / grTCount[12];
    //                            dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
    //                            grCCount[12] += grCount[12];
    //                            pcile[12] = (float)grCCount[12] / grTCount[12];
    //                            dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
    //                        }
    //                        dr["Gr12cnt"] = grCount[12];
    //                        dr["Gr12cum"] = grCCount[12];

    //                        SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
    //                        SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

    //                        break;
    //                }
    //                dt2.Rows.Add(dr);
    //                dr = dt2.NewRow();
    //                dr["TotalScore"] = aryTscore[j];
    //                for (int q = 0; q < 13; q++)
    //                    grCount[q] = 0;
    //                if ((grScore[j] == 1) || (grScore[j] == 0))
    //                {
    //                    grCount[1] = 1;
    //                }
    //                if (grScore[j] == 2)
    //                {
    //                    grCount[2] = 1;
    //                }
    //                if (grScore[j] == 3)
    //                {
    //                    grCount[3] = 1;
    //                }
    //                if (grScore[j] == 4)
    //                {
    //                    grCount[4] = 1;
    //                }
    //                if (grScore[j] == 5)
    //                {
    //                    grCount[5] = 1;
    //                }
    //                if (grScore[j] == 6)
    //                {
    //                    grCount[6] = 1;
    //                }
    //                if (grScore[j] == 7)
    //                {
    //                    grCount[7] = 1;
    //                }
    //                if (grScore[j] == 8)
    //                {
    //                    grCount[8] = 1;
    //                }
    //                if (grScore[j] == 9)
    //                {
    //                    grCount[9] = 1;
    //                }
    //                if (grScore[j] == 10)
    //                {
    //                    grCount[10] = 1;
    //                }
    //                if (grScore[j] == 11)
    //                {
    //                    grCount[11] = 1;
    //                }
    //                if (grScore[j] == 12)
    //                {
    //                    grCount[12] = 1;
    //                }
    //            }
    //        }
       
    //        SQLInsertCutOff = "Insert into CutOffScoresByPercentile ( " + " ContestYear,EventID,ProductID,ProductCode,ProductGroupID,ProductGroupCode,TotalScore";
    //        SQLInsertCutOffVal = ")Values(" + ddlYear.SelectedValue + ",1," + ProductId + ",'" + contestType + "'," + ProductGroupID + ",'" + ProductGroupcode + "'," + lastScore + ""; // Year

    //        switch (contestType)
    //        {
    //            case "MB1":
    //                pc[1] = (float)grCount[1] / grTCount[1];
    //                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                grCCount[1] += grCount[1];
    //                pcile[1] = (float)grCCount[1] / grTCount[1];
    //                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                dr["Gr1cnt"] = grCount[1];
    //                dr["Gr1cum"] = grCCount[1];
    //                pc[2] = (float)grCount[2] / grTCount[2];
    //                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                grCCount[2] += grCount[2];
    //                pcile[2] = (float)grCCount[2] / grTCount[2];
    //                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                dr["Gr2cnt"] = grCount[2];
    //                dr["Gr2cum"] = grCCount[2];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"];
    //                break;
    //            case "MB2":
    //            case "EW1":
    //                pc[3] = (float)grCount[3] / grTCount[3];
    //                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                grCCount[3] += grCount[3];
    //                pcile[3] = (float)grCCount[3] / grTCount[3];
    //                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                dr["Gr3cnt"] = grCount[3];
    //                dr["Gr3cum"] = grCCount[3];
    //                pc[4] = (float)grCount[4] / grTCount[4];
    //                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                grCCount[4] += grCount[4];
    //                pcile[4] = (float)grCCount[4] / grTCount[4];
    //                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                dr["Gr4cnt"] = grCount[4];
    //                dr["Gr4cum"] = grCCount[4];
    //                pc[5] = (float)grCount[5] / grTCount[5];
    //                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                grCCount[5] += grCount[5];
    //                pcile[5] = (float)grCCount[5] / grTCount[5];
    //                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                dr["Gr5cnt"] = grCount[5];
    //                dr["Gr5cum"] = grCCount[5];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum,[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"] + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];

    //                break;
    //            case "MB3":
    //            case "EW2":
    //            case "SSC":
    //            case "PS1":
    //                pc[6] = (float)grCount[6] / grTCount[6];
    //                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                grCCount[6] += grCount[6];
    //                pcile[6] = (float)grCCount[6] / grTCount[6];
    //                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                dr["Gr6cnt"] = grCount[6];
    //                dr["Gr6cum"] = grCCount[6];
    //                pc[7] = (float)grCount[7] / grTCount[7];
    //                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                grCCount[7] += grCount[7];
    //                pcile[7] = (float)grCCount[7] / grTCount[7];
    //                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                dr["Gr7cnt"] = grCount[7];
    //                dr["Gr7cum"] = grCCount[7];
    //                pc[8] = (float)grCount[8] / grTCount[8];
    //                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                grCCount[8] += grCount[8];
    //                pcile[8] = (float)grCCount[8] / grTCount[8];
    //                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                dr["Gr8cnt"] = grCount[8];
    //                dr["Gr8cum"] = grCCount[8];
    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

    //                break;
    //            case "MB4":
    //                pc[9] = (float)grCount[9] / grTCount[9];
    //                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                grCCount[9] += grCount[9];
    //                pcile[9] = (float)grCCount[9] / grTCount[9];
    //                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                dr["Gr9cnt"] = grCount[9];
    //                dr["Gr9cum"] = grCCount[9];
    //                pc[10] = (float)grCount[10] / grTCount[10];
    //                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                grCCount[10] += grCount[10];
    //                pcile[10] = (float)grCCount[10] / grTCount[10];
    //                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                dr["Gr10cnt"] = grCount[10];
    //                dr["Gr10cum"] = grCCount[10];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"];

    //                break;

    //            case "JSB":
    //            case "JGB":
    //            case "JSC":
    //                pc[1] = (float)grCount[1] / grTCount[1];
    //                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                grCCount[1] += grCount[1];
    //                pcile[1] = (float)grCCount[1] / grTCount[1];
    //                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                dr["Gr1cnt"] = grCount[1];
    //                dr["Gr1cum"] = grCCount[1];
    //                pc[2] = (float)grCount[2] / grTCount[2];
    //                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                grCCount[2] += grCount[2];
    //                pcile[2] = (float)grCCount[2] / grTCount[2];
    //                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                dr["Gr2cnt"] = grCount[2];
    //                dr["Gr2cum"] = grCCount[2];
    //                pc[3] = (float)grCount[3] / grTCount[3];
    //                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                grCCount[3] += grCount[3];
    //                pcile[3] = (float)grCCount[3] / grTCount[3];
    //                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                dr["Gr3cnt"] = grCount[3];
    //                dr["Gr3cum"] = grCCount[3];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

    //                break;
    //            case "SSB":
    //            case "SGB":
    //                pc[4] = (float)grCount[4] / grTCount[4];
    //                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                grCCount[4] += grCount[4];
    //                pcile[4] = (float)grCCount[4] / grTCount[4];
    //                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                dr["Gr4cnt"] = grCount[4];
    //                dr["Gr4cum"] = grCCount[4];
    //                pc[5] = (float)grCount[5] / grTCount[5];
    //                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                grCCount[5] += grCount[5];
    //                pcile[5] = (float)grCCount[5] / grTCount[5];
    //                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                dr["Gr5cnt"] = grCount[5];
    //                dr["Gr5cum"] = grCCount[5];
    //                pc[6] = (float)grCount[6] / grTCount[6];
    //                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                grCCount[6] += grCount[6];
    //                pcile[6] = (float)grCCount[6] / grTCount[6];
    //                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                dr["Gr6cnt"] = grCount[6];
    //                dr["Gr6cum"] = grCCount[6];
    //                pc[7] = (float)grCount[7] / grTCount[7];
    //                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                grCCount[7] += grCount[7];
    //                pcile[7] = (float)grCCount[7] / grTCount[7];
    //                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                dr["Gr7cnt"] = grCount[7];
    //                dr["Gr7cum"] = grCCount[7];
    //                pc[8] = (float)grCount[8] / grTCount[8];
    //                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                grCCount[8] += grCount[8];
    //                pcile[8] = (float)grCCount[8] / grTCount[8];
    //                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                dr["Gr8cnt"] = grCount[8];
    //                dr["Gr8cum"] = grCCount[8];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum,[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"];

    //                break;
    //            case "ISC":
    //                pc[4] = (float)grCount[4] / grTCount[4];
    //                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                grCCount[4] += grCount[4];
    //                pcile[4] = (float)grCCount[4] / grTCount[4];
    //                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                dr["Gr4cnt"] = grCount[4];
    //                dr["Gr4cum"] = grCCount[4];
    //                pc[5] = (float)grCount[5] / grTCount[5];
    //                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                grCCount[5] += grCount[5];
    //                pcile[5] = (float)grCCount[5] / grTCount[5];
    //                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                dr["Gr5cnt"] = grCount[5];
    //                dr["Gr5cum"] = grCCount[5];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"];
 
    //                break;
    //            case "JVB":
    //                pc[1] = (float)grCount[1] / grTCount[1];
    //                dr["Gr1%"] = String.Format("{0:P1}", Math.Round(pc[1], 3));
    //                grCCount[1] += grCount[1];
    //                pcile[1] = (float)grCCount[1] / grTCount[1];
    //                dr["Gr1%tile"] = String.Format("{0:P1}", Math.Round(pcile[1], 3));
    //                dr["Gr1cnt"] = grCount[1];
    //                dr["Gr1cum"] = grCCount[1];
    //                pc[2] = (float)grCount[2] / grTCount[2];
    //                dr["Gr2%"] = String.Format("{0:P1}", Math.Round(pc[2], 3));
    //                grCCount[2] += grCount[2];
    //                pcile[2] = (float)grCCount[2] / grTCount[2];
    //                dr["Gr2%tile"] = String.Format("{0:P1}", Math.Round(pcile[2], 3));
    //                dr["Gr2cnt"] = grCount[2];
    //                dr["Gr2cum"] = grCCount[2];
    //                pc[3] = (float)grCount[3] / grTCount[3];
    //                dr["Gr3%"] = String.Format("{0:P1}", Math.Round(pc[3], 3));
    //                grCCount[3] += grCount[3];
    //                pcile[3] = (float)grCCount[3] / grTCount[3];
    //                dr["Gr3%tile"] = String.Format("{0:P1}", Math.Round(pcile[3], 3));
    //                dr["Gr3cnt"] = grCount[3];
    //                dr["Gr3cum"] = grCCount[3];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr1%],Gr1_Ptile,Gr1_cnt,Gr1_cum,[Gr2%],Gr2_Ptile,Gr2_cnt,Gr2_cum,[Gr3%],Gr3_Ptile,Gr3_cnt,Gr3_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr1%"] + "','" + dr["Gr1%tile"] + "'," + dr["Gr1cnt"] + "," + dr["Gr1cum"] + ",'" + dr["Gr2%"] + "','" + dr["Gr2%tile"] + "'," + dr["Gr2cnt"] + "," + dr["Gr2cum"] + ",'" + dr["Gr3%"] + "','" + dr["Gr3%tile"] + "'," + dr["Gr3cnt"] + "," + dr["Gr3cum"];

    //                break;
    //            case "IVB":
    //                pc[4] = (float)grCount[4] / grTCount[4];
    //                dr["Gr4%"] = String.Format("{0:P1}", Math.Round(pc[4], 3));
    //                grCCount[4] += grCount[4];
    //                pcile[4] = (float)grCCount[4] / grTCount[4];
    //                dr["Gr4%tile"] = String.Format("{0:P1}", Math.Round(pcile[4], 3));
    //                dr["Gr4cnt"] = grCount[4];
    //                dr["Gr4cum"] = grCCount[4];
    //                pc[5] = (float)grCount[5] / grTCount[5];
    //                dr["Gr5%"] = String.Format("{0:P1}", Math.Round(pc[5], 3));
    //                grCCount[5] += grCount[5];
    //                pcile[5] = (float)grCCount[5] / grTCount[5];
    //                dr["Gr5%tile"] = String.Format("{0:P1}", Math.Round(pcile[5], 3));
    //                dr["Gr5cnt"] = grCount[5];
    //                dr["Gr5cum"] = grCCount[5];
    //                pc[6] = (float)grCount[6] / grTCount[6];
    //                dr["Gr6%"] = String.Format("{0:P1}", Math.Round(pc[6], 3));
    //                grCCount[6] += grCount[6];
    //                pcile[6] = (float)grCCount[6] / grTCount[6];
    //                dr["Gr6%tile"] = String.Format("{0:P1}", Math.Round(pcile[6], 3));
    //                dr["Gr6cnt"] = grCount[6];
    //                dr["Gr6cum"] = grCCount[6];
    //                pc[7] = (float)grCount[7] / grTCount[7];
    //                dr["Gr7%"] = String.Format("{0:P1}", Math.Round(pc[7], 3));
    //                grCCount[7] += grCount[7];
    //                pcile[7] = (float)grCCount[7] / grTCount[7];
    //                dr["Gr7%tile"] = String.Format("{0:P1}", Math.Round(pcile[7], 3));
    //                dr["Gr7cnt"] = grCount[7];
    //                dr["Gr7cum"] = grCCount[7];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr4%],Gr4_Ptile,Gr4_cnt,Gr4_cum,[Gr5%],Gr5_Ptile,Gr5_cnt,Gr5_cum,[Gr6%],Gr6_Ptile,Gr6_cnt,Gr6_cum,[Gr7%],Gr7_Ptile,Gr7_cnt,Gr7_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr4%"] + "','" + dr["Gr4%tile"] + "'," + dr["Gr4cnt"] + "," + dr["Gr4cum"] + ",'" + dr["Gr5%"] + "','" + dr["Gr5%tile"] + "'," + dr["Gr5cnt"] + "," + dr["Gr5cum"] + ",'" + dr["Gr6%"] + "','" + dr["Gr6%tile"] + "'," + dr["Gr6cnt"] + "," + dr["Gr6cum"] + ",'" + dr["Gr7%"] + "','" + dr["Gr7%tile"] + "'," + dr["Gr7cnt"] + "," + dr["Gr7cum"] ;

    //                break;
    //            case "SVB":
    //            case "BB":
    //                pc[8] = (float)grCount[8] / grTCount[8];
    //                dr["Gr8%"] = String.Format("{0:P1}", Math.Round(pc[8], 3));
    //                grCCount[8] += grCount[8];
    //                pcile[8] = (float)grCCount[8] / grTCount[8];
    //                dr["Gr8%tile"] = String.Format("{0:P1}", Math.Round(pcile[8], 3));
    //                dr["Gr8cnt"] = grCount[8];
    //                dr["Gr8cum"] = grCCount[8];
    //                pc[9] = (float)grCount[9] / grTCount[9];
    //                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                grCCount[9] += grCount[9];
    //                pcile[9] = (float)grCCount[9] / grTCount[9];
    //                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                dr["Gr9cnt"] = grCount[9];
    //                dr["Gr9cum"] = grCCount[9];
    //                pc[10] = (float)grCount[10] / grTCount[10];
    //                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                grCCount[10] += grCount[10];
    //                pcile[10] = (float)grCCount[10] / grTCount[10];
    //                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                dr["Gr10cnt"] = grCount[10];
    //                dr["Gr10cum"] = grCCount[10];
    //                pc[11] = (float)grCount[11] / grTCount[11];
    //                dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
    //                grCCount[11] += grCount[11];
    //                pcile[11] = (float)grCCount[11] / grTCount[11];
    //                dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
    //                dr["Gr11cnt"] = grCount[11];
    //                dr["Gr11cum"] = grCCount[11];
    //                if (grTCount[12] == 0)
    //                {
    //                    pc[12] = 0;
    //                    dr["Gr12%"] = "";
    //                    dr["Gr12%tile"] = "";
    //                }
    //                else
    //                {
    //                    pc[12] = (float)grCount[12] / grTCount[12];
    //                    dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
    //                    grCCount[12] += grCount[12];
    //                    pcile[12] = (float)grCCount[12] / grTCount[12];
    //                    dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
    //                }
    //                dr["Gr12cnt"] = grCount[12];
    //                dr["Gr12cum"] = grCCount[12];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr8%],Gr8_Ptile,Gr8_cnt,Gr8_cum,[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr8%"] + "','" + dr["Gr8%tile"] + "'," + dr["Gr8cnt"] + "," + dr["Gr8cum"]+ ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

    //                break;
    //            case "EW3":
    //            case "PS3":
    //                pc[9] = (float)grCount[9] / grTCount[9];
    //                dr["Gr9%"] = String.Format("{0:P1}", Math.Round(pc[9], 3));
    //                grCCount[9] += grCount[9];
    //                pcile[9] = (float)grCCount[9] / grTCount[9];
    //                dr["Gr9%tile"] = String.Format("{0:P1}", Math.Round(pcile[9], 3));
    //                dr["Gr9cnt"] = grCount[9];
    //                dr["Gr9cum"] = grCCount[9];
    //                pc[10] = (float)grCount[10] / grTCount[10];
    //                dr["Gr10%"] = String.Format("{0:P1}", Math.Round(pc[10], 3));
    //                grCCount[10] += grCount[10];
    //                pcile[10] = (float)grCCount[10] / grTCount[10];
    //                dr["Gr10%tile"] = String.Format("{0:P1}", Math.Round(pcile[10], 3));
    //                dr["Gr10cnt"] = grCount[10];
    //                dr["Gr10cum"] = grCCount[10];
    //                pc[11] = (float)grCount[11] / grTCount[11];
    //                dr["Gr11%"] = String.Format("{0:P1}", Math.Round(pc[11], 3));
    //                grCCount[11] += grCount[11];
    //                pcile[11] = (float)grCCount[11] / grTCount[11];
    //                dr["Gr11%tile"] = String.Format("{0:P1}", Math.Round(pcile[11], 3));
    //                dr["Gr11cnt"] = grCount[11];
    //                dr["Gr11cum"] = grCCount[11];
    //                if (grTCount[12] == 0)
    //                {
    //                    pc[12] = 0;
    //                    dr["Gr12%"] = "";
    //                    dr["Gr12%tile"] = "";
    //                }
    //                else
    //                {
    //                    pc[12] = (float)grCount[12] / grTCount[12];
    //                    dr["Gr12%"] = String.Format("{0:P1}", Math.Round(pc[12], 3));
    //                    grCCount[12] += grCount[12];
    //                    pcile[12] = (float)grCCount[12] / grTCount[12];
    //                    dr["Gr12%tile"] = String.Format("{0:P1}", Math.Round(pcile[12], 3));
    //                }
    //                dr["Gr12cnt"] = grCount[12];
    //                dr["Gr12cum"] = grCCount[12];

    //                SQLInsertCutOff = SQLInsertCutOff + ",[Gr9%],Gr9_Ptile,Gr9_cnt,Gr9_cum,[Gr10%],Gr10_Ptile,Gr10_cnt,Gr10_cum,[Gr11%],Gr11_Ptile,Gr11_cnt,Gr11_cum,[Gr12%],Gr12_Ptile,Gr12_cnt,Gr12_cum";
    //                SQLInsertCutOffVal = SQLInsertCutOffVal + ",'" + dr["Gr9%"] + "','" + dr["Gr9%tile"] + "'," + dr["Gr9cnt"] + "," + dr["Gr9cum"] + ",'" + dr["Gr10%"] + "','" + dr["Gr10%tile"] + "'," + dr["Gr10cnt"] + "," + dr["Gr10cum"] + ",'" + dr["Gr11%"] + "','" + dr["Gr11%tile"] + "'," + dr["Gr11cnt"] + "," + dr["Gr11cum"] + ",'" + dr["Gr12%"] + "','" + dr["Gr12%tile"] + "'," + dr["Gr12cnt"] + "," + dr["Gr12cum"];

    //                break;
    //        }


    //        totalPercentile = (float)totalCumCount / aryTscore.Length;
    //        dr["TotalPercentile"] = String.Format("{0:P1}", Math.Round(totalPercentile, 3));
    //        dr["TotalCount"] = cCount;
    //        dr["TotalCumeCount"] = totalCumCount;

    //        SQLExecString = SQLExecString + SQLInsertCutOff + ",Start_Grade,End_Grade,TotalPercentile,Total_Count,Total_Cume_Count,ContestPeriod,ContestDate,CreatedBy,CreatedDate" + SQLInsertCutOffVal + "," + StartGrade + "," + EndGrade + ",'" + String.Format("{0:P1}", Math.Round(totalPercentile, 3)) + "'," + cCount + "," + totalCumCount + ",'" + ContestPeriod + "','" + ContestDays + "','" + Session["LoginID"] + "', GETDATE())";



    //        dt2.Rows.Add(dr);
    //        //DataGrid1.Caption = contestType + " as of " + DateTime.Today.ToShortDateString();
    //        //DataGrid1.DataSource = dt2;
    //        //DataGrid1.DataBind();

    //        Response.Write(SQLExecString);


    //        if (Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, SQLExecString)) > 0)
    //        {
    //            lblSuccess.Text = "Percentiles Calculation done for the Year " + ddlYear.SelectedValue;
    //            SQLExecString = "";
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //         Response.Write(ex.ToString());
    //    }
			
    //}

    void SortArrays(double[] aryTscore, double[] grScore)
    {
        int x = aryTscore.Length;
        int i;
        int j;
        double temp1, temp2;

        for (i = (x - 1); i >= 0; i--)
        {
            for (j = 1; j <= i; j++)
            {
                if (aryTscore[j - 1] > aryTscore[j])
                {
                    temp1 = aryTscore[j - 1];
                    aryTscore[j - 1] = aryTscore[j];
                    aryTscore[j] = temp1;
                    temp2 = grScore[j - 1];
                    grScore[j - 1] = grScore[j];
                    grScore[j] = temp2;
                }
            }
        }


    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=PercentRankOf" + contestType + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DGPercentiles.RenderControl(hw);
      //  DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }



     protected void btnExport_Click(object sender, EventArgs e)
    {
           
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select distinct cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId from CutOffScoresByPercentile cs Inner Join Product p On cs.ProductCode = p.ProductCode and cs.EventID = p.EventId where cs.ProductCode is not null and ContestYear=" + ddlYear.SelectedValue + " and EventID=1 Group By cs.ProductCode,cs.Start_Grade,cs.End_Grade,p.ProductId Order by p.ProductId "); //Year

            IWorkbook xlWorkBook  = NativeExcel.Factory.OpenWorkbook(Server.MapPath("CutOffScoreSheet\\CutoffScoresByPercentiles.xls"));
            IWorksheet Sheet1 ;
            Sheet1 = xlWorkBook.Worksheets[1];

            double x,y;
            double Ptile_Val1, Ptile_Val2, Ptile_1, Ptile_2 = 0.0,Ptile_Sheet, Ptile_Sheet1;
            double diff = 0.0, diff1 = 0.0;

            int Sht_Col = 2;
            int Sht_Row =11;
            int Start_Row = 10;
            int s,Col1 , Col2,TScore,p_flag ,flag_ptile;
            int k = 7;
          
            String TScoreValues, Grade_count, Contest_type;
           
            try
            {
                for (int q = 0; q < ds1.Tables[0].Rows.Count; q++)
                {
                    ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select * From CutOffScoresByPercentile Where ContestYear=" + ddlYear.SelectedValue + "and ProductCode='" + ds1.Tables[0].Rows[q]["ProductCode"].ToString() + "' and EventID=1 Order by TotalScore desc"); //Year
                    Contest_type= ds1.Tables[0].Rows[q]["ProductCode"].ToString();
                    Sheet1.Range[Start_Row, 1].Value = ds1.Tables[0].Rows[q]["ProductCode"].ToString();
                    Sheet1.Range[Start_Row, 2].Value = "Total";
                    Grade_count = "";
                    TScoreValues = "0";
                    TScore = 0;
                    StartGrade = Convert.ToInt32(ds1.Tables[0].Rows[q]["Start_Grade"].ToString());
                    EndGrade = Convert.ToInt32(ds1.Tables[0].Rows[q]["End_Grade"].ToString());
                    for (int row = StartGrade; row <= EndGrade; row++)
                    {
                        Grade_count = Grade_count + "Gr" + row + "_cum +";
                    }
                    Grade_count = Grade_count.TrimEnd('+');

                    for (int st = StartGrade; st <= EndGrade; st++)
                    {
                        Sheet1.Range[Sht_Row, Sht_Col].Value = st;

                        Col1 = 3;
                        Col2 = 4;
                        p_flag = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            //   j = i + 1;

                            Ptile_Val1 = (Convert.ToDouble(ds.Tables[0].Rows[i]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));

                            if (Ptile_Val1 <= 30)
                            {
                                //if (j < ds.Tables[0].Rows.Count)
                                //{
                                if (Ptile_Val1 > 0.0 && p_flag < 1)//&& Ptile_Val1 < 1 // To Find the First Value nearest to Percentile value 1
                                {
                                    Ptile_Val2 = (Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));

                                    if (Ptile_Val2 >= 1)
                                    {
                                        if (Ptile_Val1 != Ptile_Val2)
                                        {
                                            x = Math.Abs(1 - Ptile_Val1);//(Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'))));
                                            y = Math.Abs(1 - Ptile_Val2);//(Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr1_Ptile"].ToString().TrimEnd('%'))));

                                            if (x < y)
                                            {
                                                //   Sheet1.Range[k, Col1].Value = Ptile_Val1;// Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'));
                                                //  Sheet1.Range[k, Col2].Value = Ptile_Val1;
                                                Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
                                                Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
                                            }
                                            else if (y < x)
                                            {
                                                // Sheet1.Range[k, Col1].Value = Ptile_Val2;// Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr1_Ptile"].ToString().TrimEnd('%'));
                                                // Sheet1.Range[k, Col2].Value = Ptile_Val2;
                                                Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i + 1]["TotalScore"];
                                                Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i + 1]["Gr" + st + "_cum"];
                                                i = i + 1;
                                            }
                                            else
                                            {
                                                //  Sheet1.Range[k, Col1].Value = Ptile_Val1;// Convert.ToDouble(ds.Tables[0].Rows[i]["Gr1_Ptile"].ToString().TrimEnd('%'));
                                                //   Sheet1.Range[k, Col2].Value = Ptile_Val1;
                                                Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i + 1]["TotalScore"];
                                                Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i + 1]["Gr" + st + "_cum"];
                                                i = i + 1;
                                            }

                                            TScore = Convert.ToInt32(Sheet1.Range[Sht_Row, Col1].Value);

                                            p_flag = p_flag + 1;
                                        }
                                    }
                                }

                                else if (p_flag == 1)
                                {
                                    TScore = TScore - 1;
                                    Col1 = Col1 + 2;
                                    Col2 = Col2 + 2;
                                    flag_ptile = 0;
                                  
                                    if (TScore > Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
                                    {
                                        if (SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From CutOffScoresByPercentile Where ProductCode='" + Contest_type + "' and ContestYear= " + ddlYear.SelectedValue + " and EventID=1 and TotalScore= " + TScore + "") != "1")//Year
                                        {
                                             TScore = TScore - 1;
                                        }
                                    }

                                    TScoreValues = TScoreValues + "," + Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString());
                                    
                                    for (int l = 1; l < 12; l++)
                                    {
                                        if (TScore == Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
                                        {
                                            Ptile_1 = (Convert.ToDouble(ds.Tables[0].Rows[i]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));//Math.Round
                                            if (i < ds.Tables[0].Rows.Count-1)
                                            {
                                               Ptile_2 = (Convert.ToDouble(ds.Tables[0].Rows[i + 1]["Gr" + st + "_Ptile"].ToString().TrimEnd('%')));//Math.Round
                                            }
                                            Ptile_Sheet = (Convert.ToDouble(Sheet1.Range[k, Col1].Value));//Math.Round
                                            Ptile_Sheet1 = (Convert.ToDouble(Sheet1.Range[k, Col1 + 2].Value));// Math.Round

                                            if (Ptile_1 != Ptile_2)
                                            {
                                                if (Ptile_Sheet <= Ptile_1 && Ptile_1 <= Ptile_Sheet1)
                                                {
                                                    diff = Math.Abs(Ptile_Sheet - Ptile_1);
                                                    diff1 = Math.Abs(Ptile_Sheet1 - Ptile_1);

                                                    if (diff <= diff1)
                                                    {
                                                        //if (Sheet1.Range[Sht_Row, Col1].Value != "" && Sheet1.Range[Sht_Row, Col2].Value !="")
                                                        //{

                                                        Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
                                                        Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
                                                        //  }
                                                        // else
                                                        // {
                                                        //     Col1 = Col1 + 2;
                                                        //     Col2 = Col2 + 2;
                                                        //     Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
                                                        //    Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];

                                                        //}
                                                        flag_ptile = 1;
                                                        //goto breakfor;
                                                    }
                                                    else if (diff > diff1)
                                                    {
                                                        Col1 = Col1 + 2;
                                                        Col2 = Col2 + 2;
                                                        Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
                                                        Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
                                                        flag_ptile = 1;
                                                        //goto breakfor;
                                                    }

                                                    goto breakfor;
                                                }
                                                // }
                                                else if (Ptile_1 <= Ptile_Sheet)
                                                {
                                                    Sheet1.Range[Sht_Row, Col1].Value = ds.Tables[0].Rows[i]["TotalScore"];
                                                    Sheet1.Range[Sht_Row, Col2].Value = ds.Tables[0].Rows[i]["Gr" + st + "_cum"];
                                                    goto breakfor;
                                                }

                                                else
                                                {
                                                    Col1 = Col1 + 2;
                                                    Col2 = Col2 + 2;
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        //else if (TScore > Convert.ToInt32(ds.Tables[0].Rows[i]["TotalScore"].ToString()))
                                        //{
                                        //    if (SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From CutOffScoresByPercentile Where TotalScore= " + TScore + "") == 0)
                                        //    {
                                        //        TScore = TScore - 1;
                                        //    }
                                        //}
                                    }
                                breakfor:
                                    flag_ptile = 0;
                                    // Col1 = Col1 + 2; //Col1;//
                                    // Col2 = Col2 + 2;//Col2;//

                                }

                                //}

                            }

                        }
                        Sht_Row++;
                    }

                    ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select (" + Grade_count + ") as Count,TotalScore from  [CutOffScoresByPercentile] Where ContestYear= " + ddlYear.SelectedValue + " and ProductCode= '" + ds1.Tables[0].Rows[q]["ProductCode"].ToString() + "' and TotalScore in (" + TScoreValues + ")  and EventID=1 Order by TotalScore desc");
                    s = 0;
                    
                        for (int m = 3; m <= 26; m = m + 2)
                        {
                            if (s < ds2.Tables[0].Rows.Count )
                            {
                                Sheet1.Range[Start_Row, m].Value = ds2.Tables[0].Rows[s]["TotalScore"];
                                Sheet1.Range[Start_Row, m + 1].Value = ds2.Tables[0].Rows[s]["Count"];
                                s = s + 1;
                            }
                        }
                    Start_Row = Sht_Row++;

                }
            }
            catch (Exception ex)
            {
              // Response.Write(ex.ToString());
            }

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CutOffScoresByPercentile_" +ddlYear.SelectedValue +".xls" );
            xlWorkBook.SaveAs(Response.OutputStream);
            Response.End();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e) 
    {
            lblSuccess.Text = "";
            LoadContestDates();
             
    }
    void LoadContestDates()
    {
            String SQLstr = "SELECT  *,Row_number() over(order by temp.ContestDate1,temp.ContestDate2)as ContestPeriod FROM (";
            SQLstr =SQLstr +" SELECT  Distinct C.ContestDate as ContestDate1,C1.ContestDate as ContestDate2,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + ";
            SQLstr =SQLstr + " CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39) as ContestDates , Convert(Varchar(10),DATENAME(MM, C.ContestDate) ";
            SQLstr =SQLstr + " + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(10),DATENAME(MM, C1.ContestDate) + ' ' + ";
            SQLstr =SQLstr + " CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDate FROM Contest ";
            SQLstr =SQLstr + " C Inner join Contest C1 On C1.Contest_Year = C.Contest_Year Where C.Contest_Year=2012 and c.eventid=1 and ";
            SQLstr = SQLstr + " cast(DATEDIFF(Day,C.ContestDate,C1.ContestDate) as int) = 1) temp order by temp.ContestDate1,temp.ContestDate2";
            
            //String SQLstr = "SELECT Distinct C.ContestDate,C1.ContestDate,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39)  as ContestDates , ";
            //SQLstr = SQLstr + " Convert(Varchar(10),DATENAME(MM, C.ContestDate) + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(10),DATENAME(MM, C1.ContestDate) + ' ' + CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDays,";
            //SQLstr = SQLstr + " DATEDIFF(Day,C.ContestDate,C1.ContestDate) FROM Contest C Inner join Contest C1 On C1.Contest_Year = C.Contest_Year Where C.Contest_Year=" + ddlYear.SelectedValue + " and c.eventid=1 and cast(DATEDIFF(Day,C.ContestDate,C1.ContestDate) as int) = 1 order by C.ContestDate,C1.ContestDate";
           
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr);
            ddlcontestdate.Items.Clear();

            int Count = ds.Tables[0].Rows.Count;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcontestdate.DataSource = ds;
                ddlcontestdate.DataTextField = "ContestDate".ToString();
                ddlcontestdate.DataValueField = "ContestPeriod".ToString();
               //   ddlcontestdate.Items.Insert(0, new ListItem("All", (Convert.ToInt32(ds.Tables[0].Rows[Count - 1]["ContestPeriod"]) + 1).ToString()));
                ddlcontestdate.DataBind();
            }
    }      

    protected void ddlPercentile_SelectedIndexChanged(object sender, EventArgs e)
    {
            lblSuccess.Text = "";
            if (ddlPercentile.SelectedValue == "1")
            {
                chkAllContest.Visible = true; //trContest.Visible = true;
                btnSave.Enabled = false;
                btnExport.Enabled = false;
            }
            else
            {
                chkAllContest.Visible = false;
                btnSave.Enabled = true;
                btnExport.Enabled = true;
                ddlContest.Enabled = true;
                ddlcontestdate.Enabled = true;

            }
    }
      
    protected void BtnContinue_Click(object sender, EventArgs e)
    {

            lblSuccess.Text = "";
            if (ddlYear.SelectedIndex == 0)
            {
                lblSuccess.Text = "Please select Chapter";
                return;
            }
           
            if (ddlPercentile.SelectedValue == "1")
            {
               if (chkAllContest.Checked == true)
                {
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From CutOffScoresByPercentile Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (1,2,3,4)  and EventID=1")) > 0)
                    {
                        DuplicateFlag = true;
                        tblDuplicate.Visible = true;
                    }
                    else 
                    {
                        tblDuplicate.Visible = false;
                        CalculatePercentiles();
                    }
                }
                else 
                {
                    if (ddlContest.SelectedIndex ==0)
                    {
                        lblSuccess.Text = "Please select Contest";
                        return;
                    }
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) From CutOffScoresByPercentile Where ContestYear=" + ddlYear.SelectedValue + " and ProductCode in ('" + ddlContest.SelectedItem.Text + "')and ContestPeriod in (" + ddlcontestdate.SelectedValue + ")  and EventID=1 ")) > 0)
                    {
                        DuplicateFlag = true;
                        tblDuplicate.Visible = true;
                    }
                    else
                    {
                        tblDuplicate.Visible = false;
                        CalculatePercentiles();
                    }
                                   
                    //ReadAllScores(ddlContest.SelectedItem.Text,ddlcontestdate.SelectedItem.Text,Convert.ToInt32(ddlcontestdate.SelectedItem.Value));
                }

                DGPercentiles.Visible = false;

            }
            else if (ddlPercentile.SelectedValue == "2")
            {
                LoadDGPercentiles();
            }

           Session["DuplicateFlag"] = DuplicateFlag ;
    }
    private void LoadDGPercentiles()
    {
            int StartGrade, EndGrade;
            String ptile;
            lblSuccess.Text = "";
            String StrSQL = "Select * FROM CutOffscoresByPercentile Where Contestyear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ")  and EventID=1 ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DGPercentiles.DataSource = ds;
                DGPercentiles.DataBind();
                DGPercentiles.Visible = true;
                StartGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["Start_Grade"]);
                EndGrade = Convert.ToInt32(ds.Tables[0].Rows[0]["End_Grade"]);

                foreach (DataGridColumn col in DGPercentiles.Columns)
                {
                    ptile = col.HeaderText;
                    if (ds.Tables[0].Rows[0][ptile] is DBNull)
                    {
                        col.Visible = false;
                    }
                    else
                    {
                        col.Visible = true;
                    }
                }
            }
            else
            {
                lblSuccess.Text = "No data exists for the options selected. Please use calculate option.";
                DGPercentiles.Visible = false;
            }
    }
    protected void ddlcontestdate_SelectedIndexChanged(object sender, EventArgs e)
    {
            lblSuccess.Text = "";
    }
    protected void BtnYes_Click(object sender, EventArgs e)
    {
            //string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

            lblSuccess.Text = "";
            DuplicateFlag = Convert.ToBoolean (Session["DuplicateFlag"]) ;
            if (DuplicateFlag == true && chkAllContest.Checked == true)
            {
               SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Delete From CutOffScoresByPercentile Where ContestYear=" + ddlYear.SelectedValue + " and ContestPeriod in (1,2,3,4) and EventID=1");
            }
            else if (DuplicateFlag == true && chkAllContest.Checked == false)
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Delete From CutOffScoresByPercentile Where ContestYear=" + ddlYear.SelectedValue + " and ProductCode='" + ddlContest.SelectedItem.Text + "' and ContestPeriod in (" + ddlcontestdate.SelectedValue + ") and EventID=1");
            }
            CalculatePercentiles();
            tblDuplicate.Visible = false;
    }
    protected void BtnNo_Click(object sender, EventArgs e)
    {
            tblDuplicate.Visible = false;
            lblSuccess.Text = "No Update Done";
    }
    protected void ddlContest_SelectedIndexChanged(object sender, EventArgs e)
    {
            lblSuccess.Text = "";
    }
   
    protected void chkAllContest_CheckedChanged(object sender, EventArgs e)
    {
            if (chkAllContest.Checked == true)
            {
                ddlContest.Enabled = false;
                ddlcontestdate.SelectedIndex = 0;
                ddlcontestdate.Enabled = false;
            }
            else
            {
                ddlContest.Enabled = true;
                ddlcontestdate.Enabled = true;
            }
    }

    private void CalculatePercentiles()
    {
            if (chkAllContest.Checked == true)
            {
                for (int i = 1; i < ddlContest.Items.Count; i++)
                {
                    for (int j = 0; j < ddlcontestdate.Items.Count; j++)
                    {
                        ReadAllScores(ddlContest.Items[i].Text, ddlcontestdate.Items[j].Text, Convert.ToInt32(ddlcontestdate.Items[j].Value));
                    }
                }
            }
            else 
            {
                ReadAllScores(ddlContest.SelectedItem.Text, ddlcontestdate.SelectedItem.Text, Convert.ToInt32(ddlcontestdate.SelectedItem.Value));
            }
    }


}
