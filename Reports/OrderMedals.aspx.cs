using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Drawing;
using System.Text;


public partial class Reports_OrderMedals: System.Web.UI.Page
{
    DataSet dsMedals = new DataSet();
    DataTable dt = new DataTable();
    DataRow dr;
    DataRow drr;
    int[] category = new int[33];
    int tCount = 0;
    //int chapID;
    int prevChapterID;
   int retValue = 0;
    ArrayList al = new ArrayList();
    ArrayList cl = new ArrayList();
    int[] Grandtotal = new int[10];
    

        

    protected void Page_Load(object sender, EventArgs e)
    {
     
        // Put user code to initialize the page here
        //if(Session["Admin"].ToString() != "Yes")
        //	Response.Redirect("Default.aspx");
        //chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        //chapID = 15;
        //Session["LoginID"] = 4240;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        lblChapter.Text = lblError.Text = "";

//35187
//select COUNT(*) from volunteer where roleid=63 and Finals = 'Y' and MemberID =35187
//select COUNT(*) from volunteer where roleid=63 and chapterId is Not Null and MemberID =35187

        if (!IsPostBack)
        {
       
                
            hdnFlag.Value = "N";
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2")))
                        hdnFlag.Value = "Y";
            else if ((Session["RoleId"] != null) && (Session["selChapterID"] != null) && (Request.QueryString["id"] != null) && (Request.QueryString["id"].ToString() == "1"))
                hdnFlag.Value = Session["selChapterID"].ToString ();
            else if ((Session["RoleId"] != null) && (Session["RoleId"].ToString() == "63"))
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from volunteer where roleid=63 and [National] = 'Y' and MemberID =" + Session["LoginId"].ToString() + "")) > 0)
                    hdnFlag.Value = "Y";
                else if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(*) from volunteer where roleid=63 and  Finals = 'Y' and MemberID =" + Session["LoginId"].ToString() + "")) > 0)
                    hdnFlag.Value = "1";
                else
                {
                   SqlDataReader readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterId from volunteer where roleid=63 and chapterId is Not Null and MemberID ="+ Session["LoginId"].ToString() + "");
                    while (readr.Read())
                    {
                        if (hdnFlag.Value == "N")
                            hdnFlag.Value = readr["chapterId"].ToString();
                        else
                            hdnFlag.Value = hdnFlag.Value.ToString() + "," + readr["chapterId"].ToString();
                    }                   

                }
            }
            else
                Response.Redirect("~/VolunteerFunctions.aspx");

            int year = Convert.ToInt32(DateTime.Now.Year);
            ddlyear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
            ddlyear.SelectedIndex = ddlyear.Items.IndexOf(ddlyear.Items.FindByText(Convert.ToString(year)));
            //if (hdnFlag.Value != "Y")
            if (hdnFlag.Value == "N")
            {
                ddlWeek.Visible = false;
                ddlyear.Enabled = false;
                lblChapter.Visible = false;
            }
            loadWeeks();
            
        }
        dsMedals.Clear();
        al.Clear();
        cl.Clear();
        dt.Clear();
        if (ddlWeek.Items.Count > 0)
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                lblChapter.Text = ddlWeek.SelectedItem.ToString();
            }
          //      GetCounts();
            
        }
        else
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                Response.Write("No data exists");
            }
            lblChapter.Text = "";
            DataGrid1.Visible = false;
           // btnSave.Enabled = false;
        }
       


    }
  
    public void loadWeeks()
    {
        ddlWeek.Items.Clear();
        //DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select convert(varchar(6), SatDay1, 7)+' - '+ convert(varchar(6), SunDay2, 7) as Weektext,weekid from weekcalendar where YEAR(SatDay1)=" + ddlyear.SelectedItem.Text  + "");
        String SQLstr = "SELECT  *,Row_number() over(order by temp.ContestDate1,temp.ContestDate2)as ContestPeriod FROM (";
        SQLstr = SQLstr + " SELECT  Distinct C.ContestDate as ContestDate1,C1.ContestDate as ContestDate2,CHAR(39)+ CONVERT(VARCHAR(10), C.ContestDate, 101)+CHAR(39) + ',' + ";
        SQLstr = SQLstr + " CHAR(39)+CONVERT(VARCHAR(10), C1.ContestDate, 101)+CHAR(39) as ContestDates , Convert(Varchar(10),DATENAME(MM, C.ContestDate) ";
        SQLstr = SQLstr + " + ' ' + CAST(DAY(C.ContestDate) AS VARCHAR(2)))+','+ Convert(Varchar(10),DATENAME(MM, C1.ContestDate) + ' ' + ";
        SQLstr = SQLstr + " CAST(DAY(C1.ContestDate) AS VARCHAR(2))) as ContestDate FROM RankTrophy ";
        SQLstr = SQLstr + " C Inner join RankTrophy C1 On C1.Year = C.Year Where C.Year=" + ddlyear.SelectedValue + " and ";//c.eventid=" + ddlEvent.SelectedValue + " and ";
        SQLstr = SQLstr + " cast(DATEDIFF(Day,C.ContestD    ate,C1.ContestDate) as int) = 1) temp order by temp.ContestDate1,temp.ContestDate2";
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text,SQLstr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlWeek.DataSource = ds;
            ddlWeek.DataTextField = "ContestDate";
            ddlWeek.DataValueField = "ContestDates";
            ddlWeek.DataBind();
        }         
        //ddlWeek.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("All", "All"));
        ddlWeek.Items.Insert(0, new ListItem("Select", "0"));
  
        //  ddlWeek.DataBind();

        if (ddlWeek.Items.Count > 0)
        {
            if (hdnFlag.Value != "N")
                ddlWeek.Visible = true;
            //if (ddlyear.SelectedItem.Text == "2011")
            //    ddlWeek.Items.Insert(ddlWeek.Items.Count, new ListItem("Apr 23 - Apr 24", "50"));
        }
        else
        {
            ddlWeek.Visible = false;
            lblChapter.Text = "";
        }

    }
    private void GetCounts()
    {
    
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "IDDataKey";
        column.AutoIncrement = true;
        dt.Columns.Add(column);

        dt.Columns.Add("RankTrophyID", typeof(int));
        dt.Columns.Add("PartMedTrophyID", typeof(int));
        dt.Columns.Add("Contest", typeof(string));
        dt.Columns.Add("PartCert", typeof(string));
  
        dt.Columns.Add("Gold", typeof(string));
        dt.Columns.Add("Silver", typeof(string));
        dt.Columns.Add("Bronze", typeof(string));

        dt.Columns.Add("UniqChild", typeof(String));

        GetChapterList();

        // Get session in Table
       

        if (retValue > 0)
        {
            prevChapterID = Convert.ToInt32(cl[0]);
            string[] st = GetChapterName(prevChapterID);
            dr = dt.NewRow();
            dr["Contest"] = "Chapter: " + st[0]  ;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Coord: " + st[1]+ " " + st[2];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = st[3];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = st[4] + ", " + st[5] + ", " + st[6];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Tel:" + st[7];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Email:" + st[8];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Contest Date:" + st[9];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Registration Deadline:" + st[10];
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dt.Rows.Add(dr);

            
            //PartMedTrophy.Columns.Add("Round", typeof(string));
            
           
                

            int RowCnt;
            for (int i = 0; i < cl.Count; i++)
            {
                

                if (i > 0)
                {
                    prevChapterID = Convert.ToInt32(cl[i]);
                    CtrlBrk();
                }
                GetContestCount(Convert.ToInt32(cl[i]));

                /*** Added to find GrandTotal for each Column *****/
                RowCnt = dt.Rows.Count - 1;


                Grandtotal[0] = Grandtotal[0] + Convert.ToInt32(dt.Rows[RowCnt]["PartCert"]);
                Grandtotal[1] = Grandtotal[1] + Convert.ToInt32(dt.Rows[RowCnt]["Gold"]);
                Grandtotal[2] = Grandtotal[2] + Convert.ToInt32(dt.Rows[RowCnt]["Silver"]);
                Grandtotal[3] = Grandtotal[3] + Convert.ToInt32(dt.Rows[RowCnt]["Bronze"]);
                Grandtotal[4] = Grandtotal[4] + Convert.ToInt32(dt.Rows[RowCnt]["UniqChild"]);

               
                /***  ***/
            }
            // Get session in Table
        
            /*** Added to find GrandTotal for each Column ***/
            dr = dt.NewRow();
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Grand Total";
            dr["PartCert"] = Grandtotal[0];
            dr["Gold"] = Grandtotal[1];
            dr["Silver"] = Grandtotal[2];
            dr["Bronze"] = Grandtotal[3];
            dr["UniqChild"] = Grandtotal[4];
            dt.Rows.Add(dr);
            /*** ***/

            if (dt.Rows.Count < 1)
            {
                if (ddlWeek.SelectedIndex != 0)
                {
                    Response.Write("No data exists");
                }
                DataGrid1.Visible = false;
                //btnSave.Enabled = false;
            }
            else
            {
                DataGrid1.Visible = true;
                DataGrid1.DataSource = dt;
            
                DataGrid1.DataBind();


                dt.Columns.RemoveAt(0);
                dt.Columns.RemoveAt(0);
                dt.Columns.RemoveAt(0);
                dt.Columns.RemoveAt(1);
                DataRow workRow;
                workRow = dt.NewRow();
                workRow[1] = "Gold";
                workRow[2] = "Silver";
                workRow[3] = "Bronze";
                workRow[4] = "UniqChild";
                dt.Rows.InsertAt(workRow, 8);

                Session["ExportDataTable"] = dt;
                //btnSave.Enabled = true;
               
            }
        }
    }
    void CtrlBrk()
    {

        dr = dt.NewRow();
        dt.Rows.Add(dr);
        string[] st = GetChapterName(prevChapterID);
        dr = dt.NewRow();
        dr["Contest"] = "Chapter: " + st[0] ;
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Coord: " + st[1] + " " + st[2];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = st[3];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = st[4] + ", " + st[5] + ", " + st[6];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Tel:" + st[7];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Email:" + st[8];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Contest Date:" + st[9];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Registration Deadline:" + st[10];
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Contest";
        dr["PartCert"] = "PartCert";
        dr["Gold"] = "Gold";
        dr["Silver"] = "Silver";
        dr["Bronze"] = "Bronze";
        dr["UniqChild"] = "UniqChild";
        dt.Rows.Add(dr);
        al.RemoveRange(0, al.Count);

    }

    void GetChapterList()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // get records from the products table

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        //connection.Open();
        // get records from the products table
        string commandString;
      
            if ((hdnFlag.Value.ToString() != "Y") && (hdnFlag.Value.ToString() != "N") && (ddlWeek.SelectedItem.Text != "All"))
                commandString = "Select distinct C.ChapterID,Ch.State ,Ch.Name From PartMedTrophy C Inner Join Chapter Ch On Ch.chapterId=C.ChapterID where C.Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ") and C.ChapterId in(" + hdnFlag.Value + ")";
            else if ((hdnFlag.Value.ToString() != "Y") && (hdnFlag.Value.ToString() != "N") &&(ddlWeek.SelectedItem.Text == "All"))
                commandString = "Select distinct C.ChapterID,Ch.State ,Ch.Name From PartMedTrophy C Inner Join Chapter Ch On Ch.chapterId=C.ChapterID where C.Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and C.ChapterId in(" + hdnFlag.Value + ")";
            else if ((hdnFlag.Value.ToString() == "Y")&& (ddlWeek.SelectedItem.Text != "All"))
                commandString = "Select distinct C.ChapterID,Ch.State ,Ch.Name From PartMedTrophy C,Chapter Ch where Ch.chapterId = C.ChapterID AND C.Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and ContestDate in (" + ddlWeek.SelectedItem.Value + ") and C.ChapterId is not null ORDER BY Ch.State ,Ch.Name "; // WeekCalendar WC,
            else if ((hdnFlag.Value.ToString() == "Y") && (ddlWeek.SelectedItem.Text == "All"))
                commandString = "Select distinct C.ChapterID,Ch.State ,Ch.Name From PartMedTrophy C,Chapter Ch where Ch.chapterId = C.ChapterID AND C.Year='" + ddlyear.SelectedItem.Text + "' AND ContestDate is not null and EventID = 2 and C.ChapterId is not null ORDER BY Ch.State ,Ch.Name ";
            else
                return;
                   
        SqlDataAdapter daChapters = new SqlDataAdapter(commandString, connection);
        DataSet dsChapters = new DataSet();
        retValue = daChapters.Fill(dsChapters);
        if (retValue > 0)
        {
            for (int i = 0; i < retValue; i++)
                if (cl.Contains(dsChapters.Tables[0].Rows[i].ItemArray[0]) == false)
                    cl.Add(dsChapters.Tables[0].Rows[i].ItemArray[0]);
        }
        else
        {
            if (ddlWeek.SelectedIndex != 0)
            {
                lblError.Text = "No data exists";
            }
            DataGrid1.Visible = false;
            //btnSave.Enabled = false;
        }
    }
    public void GetContestCount(int chap)
    {
       

      
        // Get session in Table

        //drr["Year"] = ddlyear.SelectedValue;

        //string strqry = "select RankTrophyID,PartTrophyID,rt.ProductGroupID,rt.ProductID,rt.Reg,rt.First,rt.Second,rt.Third,pt.[Unique] from RankTrophy rt inner join PartMedTrophy pt on rt.ChapterID=pt.ChapterID where pt.ChapterID=" + chap + " and rt.Year='" + ddlyear.SelectedItem.Text + "' AND rt.ContestDate is not null and rt.ContestDate in (" + ddlWeek.SelectedItem.Value + ") and rt.Round=" + ddlRound.SelectedValue + " and pt.Round=" + ddlRound.SelectedValue + "";
        string strqry = "select r.ranktrophyid, p.name,r.Reg,r.First,r.Second,r.Third,rp.[Unique] from ranktrophy r inner join product p on p.productid= r.productid inner join partMedTrophy rp on rp.ChapterID=r.ChapterID and rp.round=r.round and r.year=rp.year where r.chapterid=" + chap + " and r.[contestdate] in (" + ddlWeek.SelectedItem.Value + ") and r.year='" + ddlyear.SelectedItem.Text + "' and r.round=" + ddlRound.SelectedValue + " and rp.round=" + ddlRound.SelectedValue + " order by ranktrophyid";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(),CommandType.Text,strqry);
            int PartCertCount = 0;
            int Goldcount = 0;
            int SilverCount = 0;
            int BronzeCount = 0;
            //int UniqueCount = 0;

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                // add product details into RankTrophy table to insert into table while click on 'Save to Server'

                dr = dt.NewRow();

                dr["RankTrophyID"] = ds.Tables[0].Rows[i]["ranktrophyid"].ToString();
                dr["Contest"] = ds.Tables[0].Rows[i]["name"].ToString();
                dr["PartCert"] = ds.Tables[0].Rows[i]["Reg"].ToString();
                PartCertCount =PartCertCount+ Convert.ToInt32(ds.Tables[0].Rows[i]["Reg"].ToString());
                dr["Gold"] = ds.Tables[0].Rows[i]["First"].ToString();
                if (ds.Tables[0].Rows[i]["First"].ToString().Equals(""))
                {
                    ds.Tables[0].Rows[i]["First"] = 0;
                }
                Goldcount = Goldcount + Convert.ToInt32(ds.Tables[0].Rows[i]["First"].ToString());
                dr["Silver"] = ds.Tables[0].Rows[i]["Second"].ToString();
                if (ds.Tables[0].Rows[i]["Second"].ToString().Equals(""))
                {
                    ds.Tables[0].Rows[i]["Second"] = 0;
                }
                SilverCount = SilverCount + Convert.ToInt32(ds.Tables[0].Rows[i]["Second"].ToString());
                dr["Bronze"] = ds.Tables[0].Rows[i]["Third"].ToString();
                if (ds.Tables[0].Rows[i]["Third"].ToString().Equals(""))
                {
                    ds.Tables[0].Rows[i]["Third"] = 0;
                }
                BronzeCount = BronzeCount + Convert.ToInt32(ds.Tables[0].Rows[i]["Third"].ToString());

                dt.Rows.Add(dr);
            }
           
            dr = dt.NewRow();
            dr["PartCert"] = PartCertCount;
            dr["Gold"] = Goldcount;
            dr["Silver"] = SilverCount;
            dr["Bronze"] = BronzeCount;
            string strqryUnique = "select distinct rp.[Unique],rp.PartTrophyID from partMedTrophy rp  where rp.chapterid=" + chap + " and rp.[contestdate] in (" + ddlWeek.SelectedItem.Value + ") and rp.year='" + ddlyear.SelectedItem.Text + "' and rp.round=" + ddlRound.SelectedValue + "";
            DataSet dsUnique = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqryUnique);
            if (dsUnique.Tables[0].Rows.Count > 0)
            {
                dr["UniqChild"] = dsUnique.Tables[0].Rows[0]["Unique"].ToString();
            }
            else
            {
                //dr["UniqChild"] = null;
            }
            // dsMedals.Tables[0].Rows.Count;
            dr["PartMedTrophyID"] = dsUnique.Tables[0].Rows[0]["PartTrophyID"].ToString();
            dt.Rows.Add(dr);

           

    }
    public string GetLabels(int idNumber, string colName)
    {
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString1 = "Select ContestCategoryID,NSFChapterID,Contest_Year,EventID,ContestDate,ProductGroupID,ProductGroupCode,ProductId,ProductCode from Contest where ContestID = " + idNumber;


        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString1;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        reader.Read();
        int retVal;
        retVal = reader.GetInt32(0);
        //getting productgroup details for a contest to insert into ranktrophy table
       // ViewState["PrdDt"] = reader[1] + "," + reader.GetInt32(2).ToString() + "," + reader.GetString(3).ToString() + "," + reader.GetInt32(4).ToString() + "," + reader.GetString(5).ToString() + "," + reader.GetInt32(6).ToString() + "," + reader.GetString(7).ToString();
        ViewState["PrdDt"] = reader[1]+","+reader[2] + "," + reader[3] + "," + reader[4] + "," + reader[5] + "," + reader[6] + "," + reader[7] + "," + reader[8];
        connection.Close();
        string commandString2 = "Select ContestDesc from ContestCategory where " + colName + " = " + retVal;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        string retValue;
        reader2.Read();
        retValue = reader2.GetString(0);
        
        // close connection return value
        connection.Close();
        return retValue;
    }
    //Get chapter name instead of ID
    public string[] GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        //string retValue = "";	

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();
        DataSet dstemp = new DataSet();
        string tempCmd = "Select MemberID from Volunteer Where RoleCode='ChapterC' and TeamLead='Y' and ChapterID=" + idNumber;
        SqlDataAdapter datemp = new SqlDataAdapter(tempCmd, connection);
        int cntTemp = datemp.Fill(dstemp);
        int memID = 0;
        if (cntTemp>0)
            memID = Convert.ToInt32(dstemp.Tables[0].Rows[0].ItemArray[0]);
        else lblError.Text ="Member ID not found";

        // get records from the products table
        string commandString = "Select Chapter, FirstName, LastName, Address1, City, State,Zip, HPhone, Email from IndSpouse where AutoMemberID = " + memID;

        string[] st = new string[11];
        st[0] = GetChapterName2(idNumber);
        DataSet dsChapInfo = new DataSet();
        SqlDataAdapter daChapInfo = new SqlDataAdapter(commandString, connection);

        int cnt = daChapInfo.Fill(dsChapInfo);
        if (cnt > 0)
        {
            st[1] = dsChapInfo.Tables[0].Rows[0].ItemArray[1].ToString();
            st[2] = dsChapInfo.Tables[0].Rows[0].ItemArray[2].ToString();
            st[3] = dsChapInfo.Tables[0].Rows[0].ItemArray[3].ToString();
            st[4] = dsChapInfo.Tables[0].Rows[0].ItemArray[4].ToString();
            st[5] = dsChapInfo.Tables[0].Rows[0].ItemArray[5].ToString();
            st[6] = dsChapInfo.Tables[0].Rows[0].ItemArray[6].ToString();
            st[7] = dsChapInfo.Tables[0].Rows[0].ItemArray[7].ToString();
            st[8] = dsChapInfo.Tables[0].Rows[0].ItemArray[8].ToString();
        }
        else
            for (int t = 0; t < st.Length; t++)
                st[t] = "";
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        string cmdString = "Select Contest.ContestDate, Contest.RegistrationDeadline,Contest.EventID FROM Contest " +
            " WHERE Contest_Year='" + ddlyear.SelectedItem.Text + "' AND Contest.NSFChapterID =" + idNumber + " ORDER BY Contest.ContestDate ";

        command.CommandText = cmdString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        string contestString = "", deadlineString = "";
        DateTime firstContest = DateTime.Parse("1/1/2020");
        DateTime readContest = DateTime.Parse("1/1/1900");
        DateTime firstDeadline = DateTime.Parse("1/1/2020");
        DateTime readDeadline = DateTime.Parse("1/1/1900");
        
        while (reader.Read())
        {
            contestString = reader.GetValue(0).ToString();
            deadlineString = reader.GetValue(1).ToString();
            ViewState["event_id"] = reader.GetValue(2).ToString();
            if (contestString != "")
            {
                readContest = Convert.ToDateTime(contestString);
                if (readContest < firstContest && readContest > DateTime.Parse("1/1/1900"))
                    firstContest = readContest;
            }
            if (deadlineString != "")
            {
                readDeadline = Convert.ToDateTime(deadlineString);
                if (readDeadline < firstDeadline && readDeadline > DateTime.Parse("1/1/1900"))
                    firstDeadline = readDeadline;
            }
        }
        // close connection, return values
        connection.Close();
        if (readContest > DateTime.Parse("1/1/1900"))
            st[9] = firstContest.ToShortDateString();
        if (readDeadline > DateTime.Parse("1/1/1900"))
            st[10] = firstDeadline.ToShortDateString();

        // close connection, return values
        connection.Close();
        return st;

    }
    //Get chapter name instead of ID
    public string GetChapterName2(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
             System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterCode from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0);
        // close connection, return values
        connection.Close();
        return retValue;

    }
    
    protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataGrid1.Visible = false;
        lblError.Text = "";
        lblChapter.Text = "";
        loadWeeks();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ViewState["ClickCount"] = 1;
        GetCounts();
        btnExportOrder.Enabled = true;

    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataGrid1.Visible = false;
        lblError.Text = "";
        lblChapter.Text = "";
        btnExportOrder.Enabled = false;
        loadRound();
    }
    protected void loadRound()
    {
        string sqlstr = "select distinct Round from RankTrophy where Year=" + ddlyear.SelectedValue + " and ContestDate in (" + ddlWeek.SelectedItem.Value + ")";
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, sqlstr);
        if (ds.Tables[0].Rows.Count > 0)
        {
          
            ddlRound.DataSource = ds;
            ddlRound.DataTextField = "Round";
            ddlRound.DataValueField = "Round";
            ddlRound.DataBind();
            ddlRound.Enabled = true;
            ddlRound.SelectedIndex = 0;

        }
        if (ds.Tables[0].Rows.Count == 1)
        {
            ddlRound.SelectedIndex = 0;
            ddlRound.Enabled = false;
        }
    }
    
    protected void insertintoTables()
    {
        DataTable dt = (DataTable)Session["RankTrophy"];
        string sql = "";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["First"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["First"] = "null";
            }
            if (dt.Rows[i]["Second"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Second"] = "null";
            }
            if (dt.Rows[i]["Third"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Third"] = "null";
            }
            if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["Year"] = "null";
            }
            if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["EventID"] = "null";
            }
            if (dt.Rows[i]["ContestDate"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ContestDate"] = "null";
            }
            if (dt.Rows[i]["ChapterID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ChapterID"] = "null";
            }
            if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductGroupID"] = "null";
            }
            if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductGroupCode"] = "null";
            }
            if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductID"] = "null";
            }
            if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
            {
                dt.Rows[i]["ProductCode"] = "null";
            }
            sql = sql + "insert into RankTrophy (Year,EventID,ContestDate,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Reg,First,Second,Third,Round,CreatedDate,CreatedBy) values("

                  + dt.Rows[i]["Year"].ToString().Trim() + ","
                  + dt.Rows[i]["EventID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ContestDate"].ToString().Trim() + "',"
                  + dt.Rows[i]["ChapterID"].ToString().Trim() + ","
                  + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                  + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                  + dt.Rows[i]["ProductCode"].ToString().Trim() + "',"
                   + dt.Rows[i]["Reg"].ToString().Trim() + ","
                  + dt.Rows[i]["First"].ToString().Trim() + ","
                  + dt.Rows[i]["Second"].ToString().Trim() + ","
                  + dt.Rows[i]["Third"].ToString().Trim() + ","
                  + ViewState["ClickCount"].ToString() + ",getDate(),"
                  + Session["LoginID"] + ")";

        }

        DataTable dt1 = (DataTable)Session["PartMedTrophy"];
        string sqlpart = "";
        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            if (dt1.Rows[i]["Year"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["Year"] = "null";
            }
            if (dt1.Rows[i]["EventID"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["EventID"] = "null";
            }
            if (dt1.Rows[i]["ContestDate"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["ContestDate"] = "null";
            }
            if (dt1.Rows[i]["ChapterID"].ToString().Equals(string.Empty))
            {
                dt1.Rows[i]["ChapterID"] = "null";
            }
            sqlpart = sqlpart + "insert into PartMedTrophy  (Year,EventID,ContestDate,ChapterID,Registrations,[Unique],Round,CreatedDate,CreatedBy) values("
                  + dt1.Rows[i]["Year"].ToString().Trim() + ","
                  + dt1.Rows[i]["EventID"].ToString().Trim() + ",'"
                  + dt1.Rows[i]["ContestDate"].ToString().Trim() + "',"
                  + dt1.Rows[i]["ChapterID"].ToString().Trim() + ","
                  + dt1.Rows[i]["Registrations"].ToString().Trim() + ","
                  + dt1.Rows[i]["Unique"].ToString().Trim() + ","
                  + ViewState["ClickCount"].ToString() + ",getDate(),"
                  + Session["LoginID"] + ")";
        }

        int k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlpart);
        int j = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
        ViewState["ClickCount"] = ((Int32)ViewState["ClickCount"]) + 1;
        if (j > 0 && k > 0)
        {
            lblError.Visible = true;
            lblError.ForeColor = Color.Blue;
            lblError.Text = "Saved Successfully";
        }
    }

    
    protected void btnExportOrder_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    protected void ExportToExcel()
    {
        try
        {

            DataTable dtTable = (DataTable)Session["ExportDataTable"];
            Response.Clear();
            Response.AppendHeader("content-disposition", "attachment;filename=NSF-" + ddlyear.SelectedItem.Text + "-Reg-" + ddlWeek.SelectedItem.Text.Replace(',', '-').ToString() + ".xls");
            Response.Charset = "";
            //Response.ContentType = "application/xls";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtTable.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Engraving for Trophies</td></tr>");
            Response.Write("\n<center><tr style='height:35px'><td colspan=" + (dtTable.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>" + ddlyear.SelectedValue + " Regional Contests</td></tr>");

            //foreach (DataColumn dc in dtTable.Columns)
            //{

            //    Response.Write("<th>" + dc.ColumnName + "</th>");

            //}
            //Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtTable.Rows)
            {
                if (dr[0].ToString().Contains("Chapter:"))
                {
                    Response.Write("<tr bgcolor=#C0C0C0 >");
                }
                else
                {
                    Response.Write("<tr>");
                }
                for (int i = 0; i < dtTable.Columns.Count; i++)
                {
                    if (dr[i].ToString().Contains("Chapter:"))
                    {
                        Response.Write("<td><b><h3>" + dr[i].ToString().Replace("\t", " ") + "</h3></b></td>");
                    }
                    else if (dr[i].ToString().Equals("Contest"))
                    {
                        Response.Write("<td><b></b></td>");
                    }
                    else if (dr[i].ToString().Contains("Gold"))
                    {
                        Response.Write("<td bgcolor=#C0C0C0><b>TRG8IN</b></td>");
                    }
                    else if (dr[i].ToString().Contains("Silver"))
                    {
                        Response.Write("<td bgcolor=#C0C0C0><b>TRG6IN</b></td>");
                    }
                    else if (dr[i].ToString().Contains("Bronze"))
                    {
                        Response.Write("<td bgcolor=#C0C0C0><b>TRG4IN</b></td>");
                    }
                    else if (dr[i].ToString().Contains("UniqChild"))
                    {
                        Response.Write("<td bgcolor=#C0C0C0><b>Participant Medals</b></td>");
                    }
                    else

                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");


            Response.Flush();
            Response.End();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();


            //DataSet dsExport = (DataSet)Session["ExportDataTable"];
            //DataTable dt = (DataTable)Session["ExportDataTable"];
            //dt.Columns.RemoveAt(0);
            //dt.Columns.RemoveAt(0);
            //dt.Columns.RemoveAt(0);
            ////dt.Columns[12].
            //Response.Clear();
            //Response.Buffer = true;
            //Response.AppendHeader("content-disposition", "attachment;filename=NSF-" +
            //ddlyear.SelectedItem.Text + "-Reg-" + ddlWeek.SelectedItem.Text.Replace(',', '-').ToString() + ".xls");
            //Response.Charset = "";
            //Response.ContentType = "application/text";
            //StringBuilder sb = new StringBuilder();
            //sb.Append("Engraving for Trophies \n "+ddlyear.SelectedValue+" Regional Contests \n");
            //for (int k = 0; k < dt.Columns.Count; k++)
            //{
            //    sb.Append(dt.Columns[k].ColumnName + ',');
            //}
            //sb.Append("\r\n");
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    for (int k = 0; k < dt.Columns.Count; k++)
            //    {
            //        sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
            //    }

            //    sb.Append("\r\n");
            //}
            //Response.Output.Write(sb.ToString());
            //Response.Flush();
            //Response.End();
        }

        catch (Exception ex)
        {
        }
    }
    protected void DataGrid1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        DataGrid1.EditIndex = e.NewEditIndex;
        GetCounts();

        //Label lblUniqChild = (Label)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("UniqChild");
        //string UniqChildValue = lblUniqChild.Text.ToString();

        TextBox tbUniqChild = (TextBox)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("txtUniqChild");
        DropDownList ddlGold = (DropDownList)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("ddlGold");
        DropDownList ddlSilver = (DropDownList)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("ddlSilver");
        DropDownList ddlBronze = (DropDownList)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("ddlBronze");
        //ddlGold.SelectedIndex=ddlGold.
        Label lblGold = (Label)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("lblGold");
        Label lblSilver = (Label)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("lblSilver");
        Label lblBronze = (Label)DataGrid1.Rows[DataGrid1.EditIndex].FindControl("lblBronze");
        if (tbUniqChild.Text.Equals(""))
        {
            tbUniqChild.Visible = false;
            
        }
        else
        {
            ddlGold.Visible = false;
            ddlSilver.Visible = false;
            ddlBronze.Visible = false;
            lblGold.Visible = true;
            lblSilver.Visible = true;
            lblBronze.Visible = true;
        }
    }
    protected void ddlSelectOptions(object sender, System.EventArgs e)
    {
        //if (string.IsNullOrEmpty(TxtRecType1.Text))
        //{
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ddlTemp.Items.Insert(0, new ListItem("1", "1"));
            ddlTemp.Items.Insert(0, new ListItem("None", "NULL"));
            //ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
           
           

            //ddlTemp.Items.Insert(0, new ListItem("Select RecType", "0"));
        //}
    }
    protected void DataGrid1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hfRankTrophy = (HiddenField)DataGrid1.Rows[e.RowIndex].FindControl("hfRankTrophyID");
        string hfRankTrophyValue = hfRankTrophy.Value;
        //try
        //{

       

        if (hfRankTrophyValue != "")
        {

            DropDownList ddlGold = (DropDownList)DataGrid1.Rows[e.RowIndex].FindControl("ddlGold");
            string txtGoldValue = ddlGold.SelectedValue;

            DropDownList ddlSilver = (DropDownList)DataGrid1.Rows[e.RowIndex].FindControl("ddlSilver");
            string txtSilverValue = ddlSilver.SelectedValue;

            DropDownList ddlBronze = (DropDownList)DataGrid1.Rows[e.RowIndex].FindControl("ddlBronze");
            string txtBronzeValue = ddlBronze.SelectedValue;

            if (ddlGold.SelectedIndex == 0 && ddlSilver.SelectedIndex == 1 && ddlBronze.SelectedIndex == 1)
            {
                lblError.ForeColor = Color.Red;
                lblError.Visible = true;
                lblError.Text = "Not Allowed to Update";
            }
            else if (ddlGold.SelectedIndex == 0 && ddlSilver.SelectedIndex == 0 && ddlBronze.SelectedIndex == 1)
            {
                lblError.ForeColor = Color.Red;
                lblError.Visible = true;
                lblError.Text = "Not Allowed to Update";
            }
            else if (ddlGold.SelectedIndex == 0 && ddlSilver.SelectedIndex == 1 && ddlBronze.SelectedIndex == 0)
            {
                lblError.ForeColor = Color.Red;
                lblError.Visible = true;
                lblError.Text = "Not Allowed to Update";
            }
            else if (ddlGold.SelectedIndex == 1 && ddlSilver.SelectedIndex == 0 && ddlBronze.SelectedIndex == 1)
            {
                lblError.ForeColor = Color.Red;
                lblError.Visible = true;
                lblError.Text = "Not Allowed to Update";
            }
            else
            {

                lblError.Visible = false;
                string qryUpdate = "update RankTrophy set First=" + txtGoldValue + ",Second=" + txtSilverValue + ",Third=" + txtBronzeValue + ",ModifiedDate=getDate(),ModifiedBy="+Session["LoginID"]+"  where RankTrophyID=" + hfRankTrophyValue + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
                //Response.Write("<script>alert('Updated successfully')</script>");
                lblError.ForeColor = Color.Blue;
                lblError.Visible = true;
                lblError.Text = "Updated Successfully";
                DataGrid1.EditIndex = -1;
                GetCounts();
            }
        }
        else
        {
            HiddenField hfPartMedTrophyID = (HiddenField)DataGrid1.Rows[e.RowIndex].FindControl("hfPartMedTrophyID");
            string hfPartMedTrophyIDValue = hfPartMedTrophyID.Value;
            TextBox tbUniqChild = (TextBox)DataGrid1.Rows[e.RowIndex].FindControl("txtUniqChild");
            string strUniqChild = tbUniqChild.Text.ToString();
            Label lblPartCert = (Label)DataGrid1.Rows[e.RowIndex].FindControl("lblPartCert");
            string strlblPartCert=lblPartCert.Text;
            if(Convert.ToInt32(strUniqChild)>Convert.ToInt32(strlblPartCert))
            {
             lblError.ForeColor = Color.Red;
                lblError.Visible = true;
                lblError.Text = "Unique Children cannot be greater than registrations ";
            }
            else
            {
                string qryUpdate = "update PartMedTrophy set [Unique]=" + Convert.ToInt32(strUniqChild) + ",ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where PartTrophyID=" + hfPartMedTrophyIDValue + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
                //Response.Write("<script>alert('Updated successfully')</script>");
                lblError.ForeColor = Color.Blue;
                lblError.Visible = true;
                lblError.Text = "Updated Successfully";
                DataGrid1.EditIndex = -1;
                GetCounts();
            }
                

        }
        
        //}
        //catch(Exception ex)
        //{ Response.Write ("Err :" + ex.ToString());}


    }
    protected void DataGrid1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        DataGrid1.EditIndex = -1;
        GetCounts();
    }
    protected void DataGrid1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            int p_ID = Int32.Parse(DataGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
            Session["productID"] = p_ID.ToString();
            HiddenField hfRankTrophyID = (HiddenField)e.Row.FindControl("hfRankTrophyID"); //DataGrid1.Rows[e.Row.RowIndex].FindControl("hfRankTrophyID");
            string hfRankTrophyValue = hfRankTrophyID.Value;
            LinkButton lnkbtnEdit = (LinkButton)e.Row.FindControl("lnkbtnEdit");

            HiddenField hfPartMedTrophyID = (HiddenField)e.Row.FindControl("hfPartMedTrophyID"); //DataGrid1.Rows[e.Row.RowIndex].FindControl("hfRankTrophyID");
            string hfPartMedTrophyValue = hfPartMedTrophyID.Value;

            if (!hfPartMedTrophyValue.Equals(""))
            {
                //lnkbtnEdit.Visible = true;
            }
            else if (hfRankTrophyValue.Equals(""))
            {

                lnkbtnEdit.Visible = false;

            }
            //LinkButton lnkbtnEdit1 = (LinkButton)e.Row.FindControl("lnkbtnEdit");



            //else
            //{

            //    lnkbtnEdit.Visible = true;
            //}
        }
        //else if(e.Row.RowType==)
        //{ 
          
        //}
        //}
        //catch(Exception ex)
        //{ Response.Write ("Err :" + ex.ToString());}

    }
    protected void DataGrid1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
    }

    //public string NULL { get; set; }
}
