<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ccreports.aspx.vb" Inherits="Reports_ccreports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Credit Card Revenues</title>
</head>
<body>
    <form id="form1" runat="server">
     
    <div style="text-align:left">
    <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" OnClick="lbtnVolunteerFunctions_Click" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp;<br />
    <center>
    <table border ="0" cellpadding = "3" cellspacing = "0" width="600px">
    <tr><td align="center" colspan ="5"><b>Credit Card Revenues</b></td>
       
    </tr>
    <tr><td align="left" style="width:130px">Event  </td>
    <td style="width:5px"></td><td  align="left" style="width:130px">
        <asp:ListBox ID="lstEvent" runat="server" Width ="130px" Rows="5" SelectionMode="Multiple"></asp:ListBox>            
       </td>
        <td align="left" style="width:130px">
        </td>
        <td align="left" style="width:130px">
        </td>
    </tr>
    <tr><td align="left">Revenue Type</td><td></td><td  align="left">
        <asp:ListBox ID="lstRevenueType" runat="server" Width ="130px" Rows="3" SelectionMode="Multiple" >
        </asp:ListBox>          
       </td>
        <td align="left">
        </td>
        <td align="left">
        </td>
    </tr>    
        <tr><td align="left">Frequency </td><td></td><td  align="left">
        <asp:DropDownList ID="DdlFreq" runat="server" Width ="130px" AutoPostBack="true" OnSelectedIndexChanged="DdlFreq_SelectedIndexChanged">
            <asp:ListItem Value="0">Daily</asp:ListItem>
            <asp:ListItem Value="1" Selected="true">Monthly</asp:ListItem>
            <asp:ListItem Value="2">Annual</asp:ListItem>
        </asp:DropDownList>             
       </td>
            <td align="left">
            </td>
            <td align="left">
            </td>
        </tr>
      <tr runat="server" id="trdate" visible="false"><td align="left">Begin Date : <br /> <asp:Label ID="Label2" runat="server" ForeColor="red" Font-Size="Small" CssClass="Smallfont" Text="mm/dd/yyyy"></asp:Label></td><td></td><td  align="left"> <asp:TextBox ID="txtBeginDate" runat="server" Width="75px"></asp:TextBox></td><td  align="left">
       End Date :<br />
          <asp:Label ID="Label1" runat="server" CssClass="Smallfont" ForeColor="red" Font-Size="Small" Text="mm/dd/yyyy"></asp:Label></td> <td  align="left"><asp:TextBox ID="txtEndDate" runat="server"  Width="75px"></asp:TextBox></td></tr>
  
    <tr id="trMonthly" runat = "server"  visible="true" ><td align="left">Beginning Month/Year</td><td></td><td  align="left">
        <asp:DropDownList ID="DdlMonth1" runat="server" Width="60px">
            <asp:ListItem Value="1">Jan</asp:ListItem>
            <asp:ListItem Value="2">Feb</asp:ListItem>
            <asp:ListItem Value="3">Mar</asp:ListItem>
            <asp:ListItem Value="4">Apr</asp:ListItem>
            <asp:ListItem Value="5">May</asp:ListItem>
            <asp:ListItem Value="6">Jun</asp:ListItem>
            <asp:ListItem Value="7">July</asp:ListItem>
            <asp:ListItem Value="8">Aug</asp:ListItem>
            <asp:ListItem Value="9">Sept</asp:ListItem>
            <asp:ListItem Value="10">Oct</asp:ListItem>
            <asp:ListItem Value="11">Nov</asp:ListItem>
            <asp:ListItem Value="12">Dec</asp:ListItem></asp:DropDownList> 
        <asp:DropDownList ID="ddlYear1" runat="server" Width ="60px" >
        </asp:DropDownList>    
        
              
       </td>
        <td align="left">End Month/Year </td>
        <td align="left">
        <asp:DropDownList ID="ddlMonth2" runat="server" Width="60px">
            <asp:ListItem Value="1">Jan</asp:ListItem>
            <asp:ListItem Value="2">Feb</asp:ListItem>
            <asp:ListItem Value="3">Mar</asp:ListItem>
            <asp:ListItem Value="4">Apr</asp:ListItem>
            <asp:ListItem Value="5">May</asp:ListItem>
            <asp:ListItem Value="6">Jun</asp:ListItem>
            <asp:ListItem Value="7">July</asp:ListItem>
            <asp:ListItem Value="8">Aug</asp:ListItem>
            <asp:ListItem Value="9">Sept</asp:ListItem>
            <asp:ListItem Value="10">Oct</asp:ListItem>
            <asp:ListItem Value="11">Nov</asp:ListItem>
            <asp:ListItem Value="12">Dec</asp:ListItem></asp:DropDownList> 
        <asp:DropDownList ID="ddlYear2" runat="server" Width ="60px" >
        </asp:DropDownList>   
        </td>
    </tr>
 <tr  id="trAnnual" runat = "server" visible="false" ><td align="left"  >Type of  Year </td><td></td><td  align="left">
        <asp:DropDownList ID="ddlyeartype" runat="server" Width ="120px">
            <asp:ListItem Value="0">Fiscal</asp:ListItem>
            <asp:ListItem Value="1">Calendar</asp:ListItem>
        </asp:DropDownList> 
       
                  
       </td>
     <td align="left"></td>
     <td align="left"></td>
 </tr>
  <tr>
            <td align="center" colspan="5">
                <asp:Label ID="lblErr" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td> 
        </tr>
        <tr>
            <td align="center" colspan="5">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" Width ="100px" /> 
            <asp:Button id="btnExport" runat="server" Text="Export Data" onclick="btnExport_Click" Width ="100px"></asp:Button>
            </td> 
        </tr>
    </table> 
         
        <ASP:DATAGRID id="dgReports" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" OnPageIndexChanged="dgReports_PageIndexChanged" AllowPaging ="true"  PageSize="50" >
					<HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter" ItemStyle-HorizontalAlign="Left" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Jan" HeaderText="Jan" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Feb" HeaderText="Feb"  DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Mar" HeaderText="Mar" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Apr" HeaderText="Apr" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="May" HeaderText="May" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Jun" HeaderText="Jun" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="July" HeaderText="July" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Aug" HeaderText="Aug" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Sept" HeaderText="Sept" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Oct" HeaderText="Oct" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Nov" HeaderText="Nov" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Dec" HeaderText="Dec" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Total" HeaderText="Total" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                </COLUMNS>                
            <ItemStyle HorizontalAlign="Right" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
            </ASP:DATAGRID> 
            
             <ASP:DATAGRID id="DGfiscal" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" OnPageIndexChanged="dgfiscal_PageIndexChanged" AllowPaging ="true"  PageSize="50" >
					<HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter" ItemStyle-HorizontalAlign="Left" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="May" HeaderText="May" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Jun" HeaderText="Jun" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="July" HeaderText="July" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Aug" HeaderText="Aug" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Sept" HeaderText="Sept" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Oct" HeaderText="Oct" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Nov" HeaderText="Nov" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Dec" HeaderText="Dec" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Jan" HeaderText="Jan" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Feb" HeaderText="Feb"  DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Mar" HeaderText="Mar" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Apr" HeaderText="Apr" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
            
                <asp:BOUNDCOLUMN DataField="Total" HeaderText="Total" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                </COLUMNS>                
            <ItemStyle HorizontalAlign="Right" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
            </ASP:DATAGRID> 
            
            <br />
             <ASP:DATAGRID id="dgYearly" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" OnPageIndexChanged="dgYearly_PageIndexChanged" AllowPaging ="true"  PageSize="50" >
					<HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
					
                <COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter" ItemStyle-HorizontalAlign="Left" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="year1" HeaderText="Dec" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="year2" HeaderText="Jan" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="year3" HeaderText="Feb" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="year4" HeaderText="Mar" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="year5" HeaderText="Apr" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Total" HeaderText="Total" DataFormatString="{0:c}"  > </asp:BOUNDCOLUMN>
                </COLUMNS> 
                <ItemStyle HorizontalAlign="Right" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
            </ASP:DATAGRID> 
            
            <br /> 
        <ASP:DATAGRID id="DGDaily" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" OnPageIndexChanged="dgDaily_PageIndexChanged" AllowPaging ="true"  PageSize="50" >
					<HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>					
                <COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChargeRecID" HeaderText="ChargeRecID" ItemStyle-HorizontalAlign="Left" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_Date" HeaderText="PP_Date" DataFormatString="{0:d}"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_OrderNumber" HeaderText="PP_OrderNumber" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_CN1" HeaderText="PP_CN1"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_CN2" HeaderText="PP_CN2"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_Amount" HeaderText="PP_Amount" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_Approval" HeaderText="PP_Approval" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MS_TransDate" HeaderText="MS_TransDate" DataFormatString="{0:d}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MS_Amount" HeaderText="MS_Amount" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MS_CN1" HeaderText="MS_CN1"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MS_CN3" HeaderText="MS_CN3"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="PP_MS_Match" HeaderText="PP_MS_Match"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Brand" HeaderText="Brand"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Reference" HeaderText="Reference"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Comments" HeaderText="Comments"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="NFG_PaymentDate" HeaderText="NFG_PaymentDate" DataFormatString="{0:d}" > </asp:BOUNDCOLUMN> 
                </COLUMNS> 
                <ItemStyle HorizontalAlign="left" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
            </ASP:DATAGRID>
            </center>
    </div>
   <br />
          <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
 </form>
</body>
</html>
