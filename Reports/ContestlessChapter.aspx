﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContestlessChapter.aspx.vb" Inherits="Reports_ContestlessChapter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapters without a calendar</title>
</head>
<body>
    <form id="form1" runat="server">
         <div style="text-align:left">
    <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" OnClick="lbtnVolunteerFunctions_Click" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <asp:Button id="btnExport" runat="server" Text="Export Data" onclick="btnExport_Click" Width ="100px"></asp:Button>
</div>
    <div style="text-align:center">
    <h3>Chapters without a Calendar - 
        <asp:Literal ID="ltlYear" runat="server"></asp:Literal></h3> 
        Select Year : <asp:DropDownList ID="ddlyear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged">  </asp:DropDownList><br />
          <asp:Label ID="lblErr" runat="server" ForeColor="Red"  ></asp:Label>
        <br />
         <ASP:DATAGRID id="dgChapter" runat="server" CssClass="mediumwording"  AutoGenerateColumns="False" AllowSorting="false" HorizontalAlign = "Center"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" >
					<HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
					
                <COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChapterCode" HeaderText="Chapter" ItemStyle-HorizontalAlign="Left" > </asp:BOUNDCOLUMN>
                </COLUMNS> 
                <ItemStyle HorizontalAlign="left" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
            </ASP:DATAGRID>       
    </div>
    <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td style="width:10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
