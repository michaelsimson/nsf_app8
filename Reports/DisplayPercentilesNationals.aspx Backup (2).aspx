<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DisplayPercentilesNationals.aspx.cs" Inherits="Reports_DisplayPercentilesNationals" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
		<title>DisplayPercentiles</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<h3 align="center" style='TEXT-ALIGN:center'>Percentile values for each Contest</h3>
		<p style=" LEFT: -65px; POSITION: absolute; TOP: 34px; width: 174px; margin-left: 80px;"><asp:HyperLink ID="hybback"  runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
	        </p>
		<h3 align="center" style='TEXT-ALIGN:center' color='BLUE'>Percentiles are not available for contests that had < 30 contestants</h3>		
		<form id="Form1" method="post" runat="server">
		 <asp:Button ID="Button1" runat="server" Text="Export to Excel" onclick="Button1_Click" />
		 
		 	<asp:DataGrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 48px; POSITION: absolute; TOP: 140px"
				runat="server" Height="320px" Width="200px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
				BackColor="White" CellPadding="3" GridLines="Vertical">
				<FooterStyle ForeColor="Black" Width="10%" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle Wrap="False" Width="10%" BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle Wrap="False" ForeColor="Black" Width="10%" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Width="10%" BackColor="#000084"></HeaderStyle>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:DataGrid>
			<asp:Label id="Label1" style="Z-INDEX: 102; LEFT: 248px; POSITION: absolute; TOP: 128px" runat="server"
				Font-Size="Large"></asp:Label>
		   	
		</form>
	</body>
</html>


 
 
 