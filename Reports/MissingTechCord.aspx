<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MissingTechCord.aspx.cs" Inherits="Reports_MissingTechCord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Missing Tech Coordinators by chapter
</title>
   <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

   <div class="title02" align="center">Missing Tech Coordinators by chapter</div> 
   <h3> <%--<asp:Button id="btnBack" runat="server" Text="Back" OnClick="btnBack_Click"  ></asp:Button>--%>
   <asp:HyperLink ID="hybback" runat="server" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink></h3> 
    <%--<h4>Please select one</h4> --%>
    <form id="Form1" method="post" runat="server">
    <table style="width:60%;">
    <tr>
    <td style="font-weight:bold; width:30% ">
    <asp:Label ID="lblselect" runat="server" Text="Please select one"></asp:Label>
    </td>
    <td>
    <asp:dropdownlist id="drpContestDate" Width="160px"  DataTextField="ContestDates" DataValueField="WeekID" runat="server">   			
			</asp:dropdownlist>
    &nbsp;<asp:DropDownList ID="ddlyear" runat="server" Width="150px" 
            AutoPostBack="True" onselectedindexchanged="ddlyear_SelectedIndexChanged" >
            
        </asp:DropDownList></td>
    <td>
        &nbsp;</td>
    </tr>
    <tr><td></td></tr>
    <tr>
    <td></td>
    <td align="left">
    <asp:Button ID="btnsubmit" runat="server"  Text="Submit" OnClick="btnsubmit_Click" />
        &nbsp;&nbsp;&nbsp;<asp:Button ID="btnExport" Visible="false"  runat="server" Text="Export" 
            onclick="btnExport_Click" /><br />
        <asp:Label ID="lblerr" runat="server" ForeColor = "Red" ></asp:Label>
    </td>
    </tr>
   </table>
   	<br />
		<br />	
		
			<asp:datagrid id="DataGrid1" runat="server"
				AutoGenerateColumns="False" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px"
				BorderStyle="None" BorderColor="#999999" Width="760px">
				<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="ChapterCode" HeaderText="Chapter">
						<HeaderStyle Width="20%"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="ProductGroupCode" HeaderText="ProductGroupCode">
						<HeaderStyle Width="20%"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="ProductCode" HeaderText="ProductCode">
						<HeaderStyle Width="20%"></HeaderStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="ContestDate" DataFormatString="{0:d}" HeaderText="ContestDate">
						<HeaderStyle Width="20%"></HeaderStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			
			</asp:datagrid>
			<br />
			
</form>
</body>
</html>


 
 
 