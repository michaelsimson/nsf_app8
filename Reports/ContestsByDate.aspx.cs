using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;
using NativeExcel;
using System.Drawing;

public partial class Reports_ContestsByDate : System.Web.UI.Page
{
    DataSet dsContests;
    DataSet dsContests3;
    DataTable dt;
    DataTable dt1;
    DataTable dt3;
    string Year = "";
    String retValue;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["RoleID"] = 1; 
        // Put user code to initialize the page here
        //	if(this.IsPostBack == false)
        //		ddlContest.SelectedIndex = 0;

        // Year = DateTime.Now.Year.ToString();//System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        Year = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(contestyear) from contestcategory").ToString();

        if (!Page.IsPostBack)
        {

            hdnTechCN.Value = "N";
            if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "hide_table", "<script language='javascript'>document.getElementById('" + DataGrid2.ClientID + "').style.display = 'none';</script>");
                GetList();
            }
            else if ((Convert.ToInt16(Session["RoleID"]) == 9) && (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=9 and [National]='Y' and MemberID = " + Session["LoginID"])) > 0))
            {
                hdnTechCN.Value = "Y";
                ClientScript.RegisterStartupScript(this.GetType(), "hide_table", "<script language='javascript'>document.getElementById('" + DataGrid2.ClientID + "').style.display = 'none';</script>");
                GetList();
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "hide_table", "<script language='javascript'>document.getElementById('" + drpcontact.ClientID + "').style.display = 'none';</script>");
                DataGrid2.Visible = false;
                btnsubmit.Visible = false;
                GetList();
            }
        }
        else
            if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32) || (hdnTechCN.Value == "Y"))
        {
            if (Convert.ToInt16(drpcontact.SelectedValue) == 2)
            {
                String newPanel1 = "<script type=\'text/javascript\' language=\'javascript\'>hideshow_table('" + DataGrid1.ClientID + "','" + DataGrid2.ClientID + "');</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideshow_table", newPanel1);

                //   ClientScript.RegisterStartupScript(this.GetType(), "hide_table", "<script language='javascript'>document.getElementById('" + DataGrid2.ClientID + "').style.display = 'none';</script>");
            }
            else
            {
                String newPanel1 = "<script type=\'text/javascript\' language=\'javascript\'>hideshow_table('" + DataGrid2.ClientID + "','" + DataGrid1.ClientID + "');</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideshow_table", newPanel1);

            }
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(), "hide_table", "<script language='javascript'>document.getElementById('" + drpcontact.ClientID + "').style.display = 'none';</script>");
            DataGrid2.Visible = false;
            btnsubmit.Visible = false;

        }
    }

    private void GetList()
    {

        string where2 = "";
        switch (ddlContest.SelectedValue)
        {
            case "1":
                where2 = " Contest.ProductGroupCode='SB'";
                GetTable(where2);
                break;
            case "2":
                where2 = " Contest.ProductGroupCode='VB'";
                GetTable(where2);
                break;
            case "3":
                where2 = " Contest.ProductGroupCode='MB'";
                GetTable(where2);
                break;
            case "4":
                where2 = " Contest.ProductGroupCode='GB'";
                GetTable(where2);
                break;
            case "5":
                where2 = " Contest.ProductGroupCode='EW'";
                GetTable(where2);
                break;
            case "6":
                where2 = " Contest.ProductGroupCode='PS'";
                GetTable(where2);
                break;
            case "7":
                where2 = " Contest.ProductGroupCode='SC'";
                GetTable(where2);
                break;
            case "8":
                AggregateReport();
                break;
            case "9":
                where2 = " Contest.ProductGroupCode='BB'";
                GetTable(where2);
                break;
            default:
                AggregateReport();
                break;
        }
        if (((DataGrid1.Items.Count > 0) && (drpcontact.SelectedItem.Text == "Upcoming Contest")) || ((DataGrid2.Items.Count > 0) && (drpcontact.SelectedItem.Text == "Contact details")))
            btnExport.Visible = true;
        else
            btnExport.Visible = false;

    }

    public void GetTable(string where2)
    {

        DataGrid1.DataSource = "";
        DataGrid1.DataBind();
        DataGrid2.DataSource = "";
        DataGrid2.DataBind();

        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);

        // get records from the products table

        DateTime where1 = DateTime.Today;


        string commandString = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
            " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate >= '" + Convert.ToDateTime(where1) + "' AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State, Contest.NSFChapterID, Contest.ProductID";

        string commandString2 = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
            " Where Contest.Contest_Year = '" + Year + "' AND (Contest.ContestDate < '" + Convert.ToDateTime(where1) + "' OR Contest.Contestdate IS NULL) AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State";

        //string commandString = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
        //    " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate >= '04/17/2018' AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State, Contest.NSFChapterID, Contest.ProductID";

        //string commandString2 = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
        //    " Where Contest.Contest_Year = '" + Year + "' AND (Contest.ContestDate < '04/17/2018' OR Contest.Contestdate IS NULL) AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State";

        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        dsContests = new DataSet();
        int count = daContests.Fill(dsContests);
        dt = new DataTable();
        dt.Columns.Add("Date", typeof(string));
        dt.Columns.Add("Chapter", typeof(string));
        dt.Columns.Add("Contests", typeof(string));
        dt.Columns.Add("Count", typeof(string));
        if (count > 0)
        {
            MakeTable();
        }
        SqlDataAdapter daContests2 = new SqlDataAdapter(commandString2, connection);

        //dsContests = new DataSet();
        if (dsContests.Tables[0].Rows.Count > 0)
            dsContests.Clear();
        int count2 = daContests2.Fill(dsContests);
        if (count2 > 0)
        {
            MakeTable();
        }
        if (count > 0 || count2 > 0)
        {
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();

        }

        //**** For contact details
        if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32) || (hdnTechCN.Value == "Y"))
        {
            if (Convert.ToInt16(drpcontact.SelectedValue) == 2)
            {

                string commandString3 = "Select i.firstname  + ' ' + i.LastName Name, i.email, i.hphone,i.cphone, i.wphone, contest.ProductCode, chapter.city, chapter.state, Contest.ContestDate, Contest.NSFChapterID from Contest INNER JOIN indspouse i On i.automemberid = contest.ExamRecID inner join chapter on Chapter.ChapterID=Contest.NSFChapterID " +
                    " where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate >= '" + Convert.ToDateTime(where1) + "' AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State, Chapter.ChapterCode, Name";

                string commandString4 = "Select i.firstname  + ' ' + i.LastName Name, i.email, i.hphone,i.cphone, i.wphone, contest.ProductCode, chapter.city, chapter.state, Contest.ContestDate, Contest.NSFChapterID from Contest INNER JOIN indspouse i On i.automemberid = contest.ExamRecID inner join chapter on Chapter.ChapterID=Contest.NSFChapterID " +
                    " where Contest.Contest_Year = '" + Year + "' AND ( Contest.ContestDate < '" + Convert.ToDateTime(where1) + "' OR Contest.Contestdate IS NULL) AND (" + where2 + ") ORDER BY Contest.ContestDate, Chapter.State, Chapter.ChapterCode, Name";

                SqlDataAdapter daContests3 = new SqlDataAdapter(commandString3, connection);
                dsContests3 = new DataSet();
                int count3 = daContests3.Fill(dsContests3);
                dt3 = new DataTable();
                dt3.Columns.Add("Name", typeof(string));
                dt3.Columns.Add("Email", typeof(string));
                dt3.Columns.Add("Hphone", typeof(string));
                dt3.Columns.Add("Cphone", typeof(string));
                dt3.Columns.Add("Wphone", typeof(string));
                dt3.Columns.Add("Date", typeof(string));
                dt3.Columns.Add("Chapter", typeof(string));
                dt3.Columns.Add("Contests", typeof(string));
                dt3.Columns.Add("Count", typeof(string));
                if (count3 > 0)
                {
                    MakeTable2();
                }

                SqlDataAdapter daContests4 = new SqlDataAdapter(commandString4, connection);

                //dsContests = new DataSet();
                if (dsContests3.Tables[0].Rows.Count > 0)
                    dsContests3.Clear();
                int count4 = daContests4.Fill(dsContests3);
                if (count4 > 0)
                {
                    MakeTable2();
                }
                if (count3 > 0 || count4 > 0)
                {
                    DataGrid2.DataSource = dt3;
                    DataGrid2.DataBind();
                }
            }
        }
    }

    public void AggregateReport()
    {
        DateTime where1 = DateTime.Today;
        DataGrid1.DataSource = "";
        DataGrid1.DataBind();
        DataGrid2.DataSource = "";
        DataGrid2.DataBind();

        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        string commandString = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
            " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate >= '" + Convert.ToDateTime(where1) + "' ORDER BY Contest.ContestDate, Chapter.State, Contest.NSFChapterID, Contest.ProductID";

        string commandString2 = "Select Contest.NSFChapterID, Contest.ContestDate, Contest.ProductID, Chapter.City, Chapter.State, Contest.ProductCode from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
        " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate < '" + Convert.ToDateTime(where1) + "' ORDER BY Contest.ContestDate, Chapter.State, Contest.NSFChapterID, Contest.ProductID";

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

        dsContests = new DataSet();
        int count = daContests.Fill(dsContests);
        dt = new DataTable();
        dt.Columns.Add("Date", typeof(string));
        dt.Columns.Add("Chapter", typeof(string));
        dt.Columns.Add("Contests", typeof(string));
        dt.Columns.Add("Count", typeof(string));
        if (count > 0)
        {
            MakeTable();
        }
        SqlDataAdapter daContests2 = new SqlDataAdapter(commandString2, connection);
        if (dsContests.Tables[0].Rows.Count > 0)
            dsContests.Clear();
        int count2 = daContests2.Fill(dsContests);
        if (count2 > 0)
        {
            MakeTable();
        }
        if (count > 0 || count2 > 0)
        {
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();

        }

        //**** For contact details
        if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32) || (hdnTechCN.Value == "Y"))
        {
            if (Convert.ToInt16(drpcontact.SelectedValue) == 2)
            {

                string commandString3 = "Select i.firstname  + ' ' + i.LastName Name, i.email, i.hphone,i.cphone, i.wphone, contest.ProductCode, chapter.city, chapter.state, Contest.ContestDate, Contest.NSFChapterID from Contest INNER JOIN indspouse i On i.automemberid = contest.ExamRecID inner join chapter on Chapter.ChapterID=Contest.NSFChapterID " +
                    " where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate >= '" + Convert.ToDateTime(where1) + "' ORDER BY Contest.ContestDate, Chapter.State, Chapter.ChapterCode, Name";

                string commandString4 = "Select i.firstname  + ' ' + i.LastName Name, i.email, i.hphone,i.cphone, i.wphone, contest.ProductCode, chapter.city, chapter.state, Contest.ContestDate, Contest.NSFChapterID from Contest INNER JOIN indspouse i On i.automemberid = contest.ExamRecID inner join chapter on Chapter.ChapterID=Contest.NSFChapterID " +
                    " where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate < '" + Convert.ToDateTime(where1) + "' ORDER BY Contest.ContestDate, Chapter.State, Chapter.ChapterCode, Name";

                SqlDataAdapter daContests3 = new SqlDataAdapter(commandString3, connection);
                dsContests3 = new DataSet();
                int count3 = daContests3.Fill(dsContests3);
                dt3 = new DataTable();
                dt3.Columns.Add("Name", typeof(string));
                dt3.Columns.Add("Email", typeof(string));
                dt3.Columns.Add("Hphone", typeof(string));
                dt3.Columns.Add("Cphone", typeof(string));
                dt3.Columns.Add("Wphone", typeof(string));
                dt3.Columns.Add("Date", typeof(string));
                dt3.Columns.Add("Chapter", typeof(string));
                dt3.Columns.Add("Contests", typeof(string));
                dt3.Columns.Add("Count", typeof(string));
                if (count3 > 0)
                {
                    MakeTable2();
                }

                SqlDataAdapter daContests4 = new SqlDataAdapter(commandString4, connection);

                //dsContests = new DataSet();
                if (dsContests3.Tables[0].Rows.Count > 0)
                    dsContests3.Clear();
                int count4 = daContests4.Fill(dsContests3);
                if (count4 > 0)
                {
                    MakeTable2();
                }
                if (count3 > 0 || count4 > 0)
                {
                    DataGrid2.DataSource = dt3;
                    DataGrid2.DataBind();

                }
            }
        }

    }

    protected void ddlWeek_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        // For contact deatils
        if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32) || (hdnTechCN.Value == "Y"))
        {
            //if (Convert.ToInt16(drpcontact.SelectedValue) == 2)
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "show_table", "<script language='javascript'>document.getElementById('" + DataGrid2.ClientID + "').style.display = 'block';</script>");
            //}
            //else
            //{
            //    String newPanel1 = "<script type=\'text/javascript\' language=\'javascript\'>hideshow_table('" + DataGrid2.ClientID + "','" + DataGrid1.ClientID + "');</script>";
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideshow_table", newPanel1);
            //}
        }
        else
        {
            GetList();
        }
    }

    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select City, State from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0) + ", " + reader.GetString(1);
        // close connection, return values
        connection.Close();
        return retValue;

    }

    /* Mail SQL is modifies such a way that ProductCode is retrieved in the same query itself. so that we don't have to execute the below query.
     * public string GetLabels(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);

        string commandString2 = "Select ContestCode from ContestCategory where ContestCategoryID = " + idNumber;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        string retValue;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        reader2.Read();
        retValue = reader2.GetString(0);
        // close connection return value
        connection.Close();
        //return retValue;
        return retValue;
    }*/

    public string GetChildCount(int iChapterId, String strprodCode)
    {
        // connect to the contestant table
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);

        string commandString2 = "Select Count(ChildNumber) from Contestant a where a.PaymentReference<>'' and a.ContestYear =" + Convert.ToInt32(Year) + " and a.Chapterid =" + iChapterId + " and a.ProductCode = '" + strprodCode + "'";
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;

        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        reader2.Read();
        retValue = Convert.ToString(reader2.GetInt32(0));
        // close connection return value
        connection.Close();
        //return retValue;
        return retValue;
    }

    public void MakeTable()
    {
        DataRow dr = dt.NewRow();
        string value = dsContests.Tables[0].Rows[0].ItemArray[1].ToString();
        if (value != "")
            dr["Date"] = Convert.ToDateTime(value).ToShortDateString();
        dr["Chapter"] = dsContests.Tables[0].Rows[0].ItemArray[3].ToString() + ", " +
            dsContests.Tables[0].Rows[0].ItemArray[4].ToString();
        DateTime prevDate = DateTime.Today;
        value = dsContests.Tables[0].Rows[0].ItemArray[1].ToString();
        if (value != "")
            prevDate = Convert.ToDateTime(value);
        int prevChapter = Convert.ToInt32(dsContests.Tables[0].Rows[0].ItemArray[0]);
        string contestList = "";
        String strContestCode = "";
        int count = 0;
        for (int i = 0; i < dsContests.Tables[0].Rows.Count; i++)
        {
            value = dsContests.Tables[0].Rows[i].ItemArray[1].ToString();
            int iChapterId = Convert.ToInt32(dsContests.Tables[0].Rows[i].ItemArray[0]);
            if ((value == "" || prevDate == Convert.ToDateTime(value)) &&
                prevChapter == iChapterId)
            {
                if (i != 0)
                    contestList += ", ";
                strContestCode = Convert.ToString(dsContests.Tables[0].Rows[i].ItemArray[5]);
                strContestCode = strContestCode + "-" + GetChildCount(iChapterId, strContestCode);
                contestList += strContestCode;
                count += Convert.ToInt16(retValue);
            }
            else
            {
                dr["Contests"] = contestList;
                dr["Count"] = count;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                value = dsContests.Tables[0].Rows[i].ItemArray[1].ToString();
                if (value != "")
                    prevDate = Convert.ToDateTime(value);
                prevChapter = iChapterId;
                contestList = "";
                count = 0;
                value = dsContests.Tables[0].Rows[i].ItemArray[1].ToString();
                if (value != "")
                    dr["Date"] = Convert.ToDateTime(value).ToShortDateString();
                dr["Chapter"] = dsContests.Tables[0].Rows[i].ItemArray[3].ToString() + ", " +
                    dsContests.Tables[0].Rows[i].ItemArray[4].ToString();
                strContestCode = Convert.ToString(dsContests.Tables[0].Rows[i].ItemArray[5]);
                strContestCode = strContestCode + "-" + GetChildCount(iChapterId, strContestCode);
                contestList += strContestCode;
                count += Convert.ToInt16(retValue);

            }
        }
        dr["Contests"] = contestList;
        dr["Count"] = count;
        dt.Rows.Add(dr);
    }

    public void MakeTable2()
    {
        DataRow dr = dt3.NewRow();
        string name = dsContests3.Tables[0].Rows[0].ItemArray[0].ToString();
        if (name != "")
            dr["Date"] = Convert.ToDateTime(dsContests3.Tables[0].Rows[0].ItemArray[8].ToString()).ToShortDateString();
        dr["Chapter"] = dsContests3.Tables[0].Rows[0].ItemArray[6].ToString() + ", " +
            dsContests3.Tables[0].Rows[0].ItemArray[7].ToString();
        dr["Name"] = dsContests3.Tables[0].Rows[0].ItemArray[0].ToString();
        //dr["Name"] = name.ToString();
        dr["Email"] = dsContests3.Tables[0].Rows[0].ItemArray[1].ToString();
        dr["Hphone"] = dsContests3.Tables[0].Rows[0].ItemArray[2].ToString();
        dr["Cphone"] = dsContests3.Tables[0].Rows[0].ItemArray[3].ToString();
        dr["Wphone"] = dsContests3.Tables[0].Rows[0].ItemArray[4].ToString();
        string prevname = "";
        if (name != "")
            prevname = name;
        int prevChapter = Convert.ToInt32(dsContests3.Tables[0].Rows[0].ItemArray[9]);
        string contestList = "";
        int count = 0;
        String strContestCode = "";
        for (int i = 0; i < dsContests3.Tables[0].Rows.Count; i++)
        {
            name = dsContests3.Tables[0].Rows[i].ItemArray[0].ToString();
            int iChapterId = Convert.ToInt32(dsContests3.Tables[0].Rows[i].ItemArray[9]);
            if ((name == "" || prevname == name) &&
                prevChapter == iChapterId)
            {
                if (i != 0)
                    contestList += ", ";
                strContestCode = Convert.ToString(dsContests3.Tables[0].Rows[i].ItemArray[5]);
                strContestCode = strContestCode + "-" + GetChildCount(iChapterId, strContestCode);
                contestList += strContestCode;
                count += Convert.ToInt16(retValue);
            }
            else
            {
                dr["Contests"] = contestList;
                dr["Count"] = count;
                dt3.Rows.Add(dr);
                dr = dt3.NewRow();
                name = dsContests3.Tables[0].Rows[i].ItemArray[0].ToString();
                if (name != "")
                    prevname = name;

                prevChapter = iChapterId;
                contestList = "";
                count = 0;
                name = dsContests3.Tables[0].Rows[i].ItemArray[0].ToString();
                if (name != "")
                    dr["Date"] = Convert.ToDateTime(dsContests3.Tables[0].Rows[i].ItemArray[8].ToString()).ToShortDateString();
                dr["Chapter"] = dsContests3.Tables[0].Rows[i].ItemArray[6].ToString() + ", " +
                dsContests3.Tables[0].Rows[i].ItemArray[7].ToString();
                dr["Name"] = dsContests3.Tables[0].Rows[i].ItemArray[0].ToString();
                // dr["Name"] = name.ToString();
                dr["Email"] = dsContests3.Tables[0].Rows[i].ItemArray[1].ToString();
                dr["Hphone"] = dsContests3.Tables[0].Rows[i].ItemArray[2].ToString();
                dr["Cphone"] = dsContests3.Tables[0].Rows[i].ItemArray[3].ToString();
                dr["Wphone"] = dsContests3.Tables[0].Rows[i].ItemArray[4].ToString();
                strContestCode = Convert.ToString(dsContests3.Tables[0].Rows[i].ItemArray[5]);
                strContestCode = strContestCode + "-" + GetChildCount(iChapterId, strContestCode);
                contestList += strContestCode;
                count += Convert.ToInt16(retValue);
            }
        }
        dr["Count"] = count;
        dr["Contests"] = contestList;
        dt3.Rows.Add(dr);
    }

    ////protected void btnBack_Click(object sender, EventArgs e)
    ////{
    ////    Response.Redirect("../VolunteerFunctions.aspx");
    ////}

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if ((Convert.ToInt16(Session["RoleID"]) == 1) || (Convert.ToInt16(Session["RoleID"]) == 2) || (Convert.ToInt16(Session["RoleID"]) == 96) || (Convert.ToInt16(Session["RoleID"]) == 32) || (hdnTechCN.Value == "Y"))
        {
            if (Convert.ToInt16(drpcontact.SelectedValue) == 2)
            {
                // ClientScript.RegisterStartupScript(this.GetType(), "show_table", "<script language='javascript'>document.getElementById('" + DataGrid2.ClientID + "').style.display = 'block';</script>");
                String newPanel1 = "<script type=\'text/javascript\' language=\'javascript\'>hideshow_table('" + DataGrid1.ClientID + "','" + DataGrid2.ClientID + "');</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideshow_table", newPanel1);
                GetList();
            }
            else
            {
                String newPanel1 = "<script type=\'text/javascript\' language=\'javascript\'>hideshow_table('" + DataGrid2.ClientID + "','" + DataGrid1.ClientID + "');</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideshow_table", newPanel1);
                GetList();
            }

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        Response.Clear();
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        string Contest = "ALL";
        if (ddlContest.SelectedItem.Value.ToString() != "8")
        {
            Contest = ddlContest.SelectedItem.Text.Replace(" ", "");
        }
        Contest = Contest + "_" + DateTime.Now.ToShortDateString();
        Contest = Contest.Replace("-", "_");
        if ((DataGrid1.Items.Count > 0) && (drpcontact.SelectedItem.Text == "Upcoming Contest"))
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Upcoming_Contest_Details_" + Contest + ".xls");
            DataGrid1.RenderControl(hw);
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Contact_Details_" + Contest + ".xls");
            DataGrid2.RenderControl(hw);
        }
        Response.Write(sw.ToString());
        Response.End();
    }

    public void GetChapterList()
    {
        try
        {


            //ArrayList lstDate = new ArrayList();
            //lstDate.Clear();

            //for (int i = 0; i < DataGrid1.Items.Count; i++)
            //{
            //    string date = DataGrid1.Items[i].Cells[0].Text;
            //    DateTime dtDate = Convert.ToDateTime(date);
            //    string today = Convert.ToDateTime(DateTime.Today).ToString("dd/MM/yyyy");
            //    DateTime dtToday = Convert.ToDateTime(today);
            //    if (dtDate >= dtToday)
            //    {
            //        if (lstDate.IndexOf(date) < 0)
            //        {
            //            lstDate.Add(date);
            //        }

            //    }
            //}

            string connectionString =
               System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);
            // get records from the products table

            string cmdDateText = "select * from WeekCalendar where EventId=2";
            SqlDataAdapter daDates = new SqlDataAdapter(cmdDateText, connection);

            DataSet dsDates = new DataSet();
            daDates.Fill(dsDates);
            if (dsDates.Tables[0].Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow dr in dsDates.Tables[0].Rows)
                {
                    string date = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MM/dd/yyyy");
                    string date1 = Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("MM/dd/yyyy");

                    string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                  " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in ('" + date + "', '" + date1 + "') ORDER BY Chapter.State, Chapter.Name Asc";

                    SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

                    dsContests = new DataSet();
                    daContests.Fill(dsContests);
                    if (i == 0)
                    {

                        if (dsContests.Tables[0].Rows.Count > 0)
                        {
                            dlData.DataSource = dsContests;
                            dlData.DataBind();
                            string month = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MMMM");
                            string day = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("dd");
                            string day2 = Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("dd");
                            string year = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("yyyy");
                            string title = "Table 1: Weekend " + month + " " + day + "-" + day2 + "," + year + "";
                            lblTable1.Text = title;
                            dvTable1.Visible = true;
                            hdnDate1.Value = "'" + Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("MM/dd/yyyy") + "'";
                            hdntable1Title.Value = month + " " + day + "-" + day2 + "," + year;
                        }
                        else
                        {
                            dvTable1.Visible = false;
                        }
                    }
                    else if (i == 1)
                    {


                        if (dsContests.Tables[0].Rows.Count > 0)
                        {
                            BtnExportAll.Visible = true;
                            ddlTable2.DataSource = dsContests;
                            ddlTable2.DataBind();
                            string month = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MMMM");
                            string day = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("dd");
                            string day2 = Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("dd");
                            string year = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("yyyy");
                            string title = "Table 2: Weekend " + month + " " + day + "-" + day2 + "," + year + "";
                            lblTable2.Text = title;
                            dvTable2.Visible = true;
                            //  hdnDate2.Value = date;
                            hdnDate2.Value = "'" + Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("MM/dd/yyyy") + "'";
                            hdntable2Title.Value = month + " " + day + "-" + day2 + "," + year;
                        }
                        else
                        {
                            dvTable2.Visible = false;
                        }
                    }
                    else if (i == 2)
                    {

                        if (dsContests.Tables[0].Rows.Count > 0)
                        {
                            BtnExportAll.Visible = true;
                            ddlTable3.DataSource = dsContests;
                            ddlTable3.DataBind();
                            string month = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MMMM");
                            string day = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("dd");
                            string day2 = Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("dd");
                            string year = Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("yyyy");
                            string title = "Table 3: Weekend " + month + " " + day + "-" + day2 + "," + year + "";
                            lblTable3.Text = title;
                            dvTable3.Visible = true;
                            //  hdnDate3.Value = date;
                            hdnDate3.Value = "'" + Convert.ToDateTime(dr["SatDay1"].ToString()).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(dr["SunDay2"].ToString()).ToString("MM/dd/yyyy") + "'";
                            hdntable3Title.Value = month + " " + day + "-" + day2 + "," + year;
                        }
                        else
                        {
                            dvTable3.Visible = false;
                        }
                    }
                    i++;
                }
            }

            //if (lstDate.Count > 0)
            //{
            //    for (int i = 0; i < lstDate.Count; i++)
            //    {
            //        string date = lstDate[i].ToString();

            //        string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
            //      " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate = '" + date + "' ORDER BY Chapter.State, Chapter.Name Asc";

            //        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

            //        dsContests = new DataSet();
            //        daContests.Fill(dsContests);
            //        if (i == 0)
            //        {

            //            if (dsContests.Tables[0].Rows.Count > 0)
            //            {
            //                dlData.DataSource = dsContests;
            //                dlData.DataBind();
            //                string month = Convert.ToDateTime(date).ToString("MMMM");
            //                string day = Convert.ToDateTime(date).ToString("dd");
            //                string year = Convert.ToDateTime(date).ToString("yyyy");
            //                string title = "Table 1:Weekend " + month + " " + day + "," + year + "";
            //                lblTable1.Text = title;
            //                dvTable1.Visible = true;
            //                hdnDate1.Value = date;
            //            }
            //            else
            //            {
            //                dvTable1.Visible = false;
            //            }
            //        }
            //        else if (i == 1)
            //        {


            //            if (dsContests.Tables[0].Rows.Count > 0)
            //            {
            //                ddlTable2.DataSource = dsContests;
            //                ddlTable2.DataBind();
            //                string month = Convert.ToDateTime(date).ToString("MMMM");
            //                string day = Convert.ToDateTime(date).ToString("dd");
            //                string year = Convert.ToDateTime(date).ToString("yyyy");
            //                string title = "Table 2: Weekend " + month + " " + day + "," + year + "";
            //                lblTable2.Text = title;
            //                dvTable2.Visible = true;
            //                hdnDate2.Value = date;
            //            }
            //            else
            //            {
            //                dvTable2.Visible = false;
            //            }
            //        }
            //        else if (i == 2)
            //        {

            //            if (dsContests.Tables[0].Rows.Count > 0)
            //            {
            //                ddlTable3.DataSource = dsContests;
            //                ddlTable3.DataBind();
            //                string month = Convert.ToDateTime(date).ToString("MMMM");
            //                string day = Convert.ToDateTime(date).ToString("dd");
            //                string year = Convert.ToDateTime(date).ToString("yyyy");
            //                string title = "Table 3: Weekend " + month + " " + day + "," + year + "";
            //                lblTable3.Text = title;
            //                dvTable3.Visible = true;
            //                hdnDate3.Value = date;
            //            }
            //            else
            //            {
            //                dvTable3.Visible = false;
            //            }
            //        }
            //    }


            //}
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, " Upcoming Contest(s)");
        }
    }
    protected void btnChapterList_Click(object sender, EventArgs e)
    {
        GetChapterList();
    }
    protected void BtnExportTable1_Click(object sender, EventArgs e)
    {
        ExportToExcelTable1();
    }
    protected void BtnExportTable2_Click(object sender, EventArgs e)
    {
        ExportToExcelTable2();
    }
    protected void btnExportExcel3_Click(object sender, EventArgs e)
    {
        ExportToExcelTable3();
    }
    protected void BtnExportAll_Click(object sender, EventArgs e)
    {
        ExportToExcelAll();
    }

    public void ExportToExcelTable1()
    {
        try
        {


            string connectionString =
              System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);

            string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                 " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate1.Value + ") ORDER BY Chapter.State, Chapter.Name Asc";

            SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

            dsContests = new DataSet();
            daContests.Fill(dsContests);
            if (dsContests.Tables[0].Rows.Count > 0)
            {
                IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                IWorksheet oSheet = default(IWorksheet);

                oSheet = oWorkbooks.Worksheets.Add();

                oSheet.Name = "Chapter List - " + hdntable1Title.Value + "";
                oSheet.Range["A1:AA1"].MergeCells = true;
                oSheet.Range["A1"].Value = "Chapter List - " + hdntable1Title.Value + "";
                oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet.Range["A1"].Font.Bold = true;
                oSheet.Range["A1"].Font.Color = Color.Black;



                oSheet.Range["A3"].Value = "Ser#";
                oSheet.Range["A3"].Font.Bold = true;

                oSheet.Range["B3"].Value = "Chapter Name";
                oSheet.Range["B3"].Font.Bold = true;

                int iRowIndex = 4;
                IRange CRange = default(IRange);
                int i = 0;

                foreach (DataRow dr in dsContests.Tables[0].Rows)
                {
                    CRange = oSheet.Range["A" + iRowIndex.ToString()];
                    CRange.Value = i + 1;

                    CRange = oSheet.Range["B" + iRowIndex.ToString()];
                    CRange.Value = dr["ChapterName"].ToString();


                    iRowIndex = iRowIndex + 1;
                    i = i + 1;
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ChapterList" + "_" + monthDay + "_" + year + ".xls";

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                Response.End();
            }
        }
        catch
        {

        }
    }

    public void ExportToExcelTable2()
    {
        try
        {

            string connectionString =
              System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);

            string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                 " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate2.Value + ") ORDER BY Chapter.State, Chapter.Name Asc";

            SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

            dsContests = new DataSet();
            daContests.Fill(dsContests);
            if (dsContests.Tables[0].Rows.Count > 0)
            {
                IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                IWorksheet oSheet = default(IWorksheet);

                oSheet = oWorkbooks.Worksheets.Add();

                oSheet.Name = "Chapter List - " + hdntable2Title.Value + "";
                oSheet.Range["A1:AA1"].MergeCells = true;
                oSheet.Range["A1"].Value = "Chapter List - " + hdntable2Title.Value + "";
                oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet.Range["A1"].Font.Bold = true;
                oSheet.Range["A1"].Font.Color = Color.Black;


                oSheet.Range["A3"].Value = "Ser#";
                oSheet.Range["A3"].Font.Bold = true;

                oSheet.Range["B3"].Value = "Chapter Name";
                oSheet.Range["B3"].Font.Bold = true;

                int iRowIndex = 4;
                IRange CRange = default(IRange);
                int i = 0;

                foreach (DataRow dr in dsContests.Tables[0].Rows)
                {
                    CRange = oSheet.Range["A" + iRowIndex.ToString()];
                    CRange.Value = i + 1;

                    CRange = oSheet.Range["B" + iRowIndex.ToString()];
                    CRange.Value = dr["ChapterName"].ToString();


                    iRowIndex = iRowIndex + 1;
                    i = i + 1;
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ChapterList" + "_" + monthDay + "_" + year + ".xls";

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                Response.End();
            }
        }
        catch
        {

        }
    }

    public void ExportToExcelTable3()
    {
        try
        {

            string connectionString =
              System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);

            string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                 " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate3.Value + ") ORDER BY Chapter.State, Chapter.Name Asc";

            SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

            dsContests = new DataSet();
            daContests.Fill(dsContests);
            if (dsContests.Tables[0].Rows.Count > 0)
            {
                IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                IWorksheet oSheet = default(IWorksheet);

                oSheet = oWorkbooks.Worksheets.Add();

                oSheet.Name = "Chapter List - " + hdntable3Title.Value + "";
                oSheet.Range["A1:AA1"].MergeCells = true;
                oSheet.Range["A1"].Value = "Chapter List - " + hdntable3Title.Value + "";
                oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet.Range["A1"].Font.Bold = true;
                oSheet.Range["A1"].Font.Color = Color.Black;


                oSheet.Range["A3"].Value = "Ser#";
                oSheet.Range["A3"].Font.Bold = true;

                oSheet.Range["B3"].Value = "Chapter Name";
                oSheet.Range["B3"].Font.Bold = true;

                int iRowIndex = 4;
                IRange CRange = default(IRange);
                int i = 0;

                foreach (DataRow dr in dsContests.Tables[0].Rows)
                {
                    CRange = oSheet.Range["A" + iRowIndex.ToString()];
                    CRange.Value = i + 1;

                    CRange = oSheet.Range["B" + iRowIndex.ToString()];
                    CRange.Value = dr["ChapterName"].ToString();


                    iRowIndex = iRowIndex + 1;
                    i = i + 1;
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ChapterList" + "_" + monthDay + "_" + year + ".xls";

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                Response.End();
            }
        }
        catch
        {

        }
    }

    public void ExportToExcelAll()
    {

        try
        {


            string connectionString =
              System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);

            string commandString = "Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                 " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate1.Value + ") ORDER BY Chapter.State, Chapter.Name Asc; ";
            if (hdnDate2.Value != "")
            {
                commandString += " Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                     " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate2.Value + ") ORDER BY Chapter.State, Chapter.Name Asc;";
            }
            if (hdnDate3.Value != "")
            {
                commandString += " Select distinct Contest.NSFChapterID,Chapter.Name,Chapter.State, Chapter.Name +', '+Chapter.State as ChapterName from Contest INNER JOIN Chapter On Chapter.ChapterID=Contest.NSFChapterID " +
                    " Where Contest.Contest_Year = '" + Year + "' AND Contest.ContestDate in(" + hdnDate3.Value + ") ORDER BY Chapter.State, Chapter.Name Asc;";
            }
            SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);

            dsContests = new DataSet();
            daContests.Fill(dsContests);
            if (dsContests.Tables[0].Rows.Count > 0)
            {
                IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                IWorksheet oSheet = default(IWorksheet);
                IWorksheet oSheet2 = default(IWorksheet);
                IWorksheet oSheet3 = default(IWorksheet);

                oSheet = oWorkbooks.Worksheets.Add();
                if (hdnDate2.Value != "")
                {
                    oSheet2 = oWorkbooks.Worksheets.Add();
                }
                if (hdnDate3.Value != "")
                {
                    oSheet3 = oWorkbooks.Worksheets.Add();
                }


                oSheet.Name = "Chapter List - " + hdntable1Title.Value + "";
                oSheet.Range["A1:AA1"].MergeCells = true;
                oSheet.Range["A1"].Value = "Chapter List - " + hdntable1Title.Value + "";
                oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet.Range["A1"].Font.Bold = true;
                oSheet.Range["A1"].Font.Color = Color.Black;

                if (hdnDate2.Value != "")
                {
                    oSheet2.Name = "Chapter List - " + hdntable2Title.Value + "";
                    oSheet2.Range["A1:AA1"].MergeCells = true;
                    oSheet2.Range["A1"].Value = "Chapter List - " + hdntable2Title.Value + "";
                    oSheet2.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet2.Range["A1"].Font.Bold = true;
                    oSheet2.Range["A1"].Font.Color = Color.Black;
                }
                if (hdnDate3.Value != "")
                {
                    oSheet3.Name = "Chapter List - " + hdntable3Title.Value + "";
                    oSheet3.Range["A1:AA1"].MergeCells = true;
                    oSheet3.Range["A1"].Value = "Chapter List - " + hdntable3Title.Value + "";
                    oSheet3.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet3.Range["A1"].Font.Bold = true;
                    oSheet3.Range["A1"].Font.Color = Color.Black;
                }

                oSheet.Range["A3"].Value = "Ser#";
                oSheet.Range["A3"].Font.Bold = true;

                oSheet.Range["B3"].Value = "Chapter Name";
                oSheet.Range["B3"].Font.Bold = true;
                if (hdnDate2.Value != "")
                {
                    oSheet2.Range["A3"].Value = "Ser#";
                    oSheet2.Range["A3"].Font.Bold = true;

                    oSheet2.Range["B3"].Value = "Chapter Name";
                    oSheet2.Range["B3"].Font.Bold = true;
                }
                if (hdnDate3.Value != "")
                {
                    oSheet3.Range["A3"].Value = "Ser#";
                    oSheet3.Range["A3"].Font.Bold = true;

                    oSheet3.Range["B3"].Value = "Chapter Name";
                    oSheet3.Range["B3"].Font.Bold = true;
                }
                int iRowIndex = 4;
                IRange CRange = default(IRange);
                int i = 0;

                foreach (DataRow dr in dsContests.Tables[0].Rows)
                {
                    CRange = oSheet.Range["A" + iRowIndex.ToString()];
                    CRange.Value = i + 1;

                    CRange = oSheet.Range["B" + iRowIndex.ToString()];
                    CRange.Value = dr["ChapterName"].ToString();


                    iRowIndex = iRowIndex + 1;
                    i = i + 1;
                }
                if (hdnDate2.Value != "")
                {
                    iRowIndex = 4;
                    i = 0;
                    foreach (DataRow dr in dsContests.Tables[1].Rows)
                    {
                        CRange = oSheet2.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet2.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterName"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                }
                if (hdnDate3.Value != "")
                {
                    iRowIndex = 4;
                    i = 0;
                    foreach (DataRow dr in dsContests.Tables[2].Rows)
                    {
                        CRange = oSheet3.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet3.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterName"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                }
                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ChapterList" + "_" + monthDay + "_" + year + ".xls";

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                Response.End();
            }
        }
        catch
        {

        }

    }
}
