﻿<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="CoachClassCalReport.aspx.vb" Inherits="CoachClassCalReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">


    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td colspan="2">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                <br />
                <br />
            </td>
        </tr>
        <tr>

            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <h2>Coach Class Calendar Report</h2>
            </td>
        </tr>

        <tr>
            <td colspan="2" align="center">

                <asp:RadioButtonList ID="rdoCalendar" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="0">Class Calendar by Coach</asp:ListItem>
                    <asp:ListItem Value="1">Missing Calendar Report</asp:ListItem>
                    <asp:ListItem Value="2">Cancelled Class Report
                    </asp:ListItem>
                    <asp:ListItem Value="3">Substitute Coach Report
                    </asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <br />
                <asp:Label ID="lblCoachClassCalendar" runat="server" CssClass="ContentSubTitle"></asp:Label><br />
                <br />

                <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold" runat="server" id="tblCoach" visible="false">

                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>Semester</td>
                        <td>
                            <asp:DropDownList ID="ddlSemester" runat="server" Width="105px" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" Width="125px">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="ProductCode" DataValueField="ProductId" Width="110px">
                            </asp:DropDownList>
                        </td>

                        <td>CoachName</td>
                        <td>
                            <asp:DropDownList ID="ddlCoach" runat="server" DataTextField="CoachName" DataValueField="MemberId" Width="105px">
                            </asp:DropDownList>
                        </td>

                    </tr>


                    <tr>
                        <td colspan="12">
                            <center>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Style="text-align: center; height: 26px;" /></center>
                        </td>
                    </tr>

                    <tr class="ContentSubTitle">
                        <td colspan="12">
                            <asp:Repeater ID="rptCoachClass" runat="server">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="rep_hdWeekCnt" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hdProductGrp" runat="server" Value='<%# Eval("ProductGroupId")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hdProduct" runat="server" Value='<%# Eval("ProductId") %>'></asp:HiddenField>

                                    <table width="90%">
                                        <tr>
                                            <td style="text-align: right; width: 200px"><b>Table
                                                <asp:Label ID="lblTableCnt" runat="server"></asp:Label>
                                                :</b></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td><b>Product Group: </b>
                                                <asp:Label ID="lblPrdGrp" runat="server" Text='<%# Eval("ProductGroupCode")%>'></asp:Label>
                                            </td>
                                            <td><b>Product : </b>
                                                <asp:Label ID="lblProduct" runat="server" Text='<%# Eval("ProductCode")%>'></asp:Label></td>
                                            <td><b>Semester : </b>
                                                <asp:Label ID="lblSemester" runat="server" Text='<%# Eval("Semester")%>'></asp:Label></td>
                                            <td><b>Level : </b>
                                                <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Level")%>'></asp:Label></td>
                                            <td><b>Session :</b><asp:Label ID="lblSessionNo" runat="server" Text='<%# Eval("SessionNo")%>'></asp:Label></td>
                                        </tr>
                                    </table>



                                    <asp:GridView ID="rpt_grdCoachClassCal" runat="server" DataKeyNames="SignUpID" AutoGenerateColumns="False" EnableViewState="true" HeaderStyle-BackColor="#ffffcc"
                                        Width="100%">
                                        <Columns>
                                            <%--  <asp:TemplateField ItemStyle-Width="70px" >
                      <ItemTemplate>                               
                               <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                                <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="UpdateCoach" Visible="false" />
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="CancelCoach" Visible="false" />  
                            </ItemTemplate>                            
                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "date", "{0:MM/dd/yyyy}")%>'></asp:Label>


                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Day">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'></asp:Label>
                                                </ItemTemplate>


                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Duration">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ser#">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSerial" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SerNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Week#">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWeek" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Class Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblClassType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ClassType")%>'></asp:Label>
                                                    <asp:HiddenField ID="hdClassType" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Status")%>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                                                    <asp:HiddenField ID="hdStatus" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Status")%>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Substitute">
                                                <ItemTemplate>
                                                    <%--<asp:HiddenField ID="hdSubstitute" runat="server" value='<%#DataBinder.Eval(Container.DataItem, "Substitute")%>'/>--%>
                                                    <asp:Label ID="lblSubstitute" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SubstituteName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reason">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReason" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Reason")%>'></asp:Label>
                                                    <%--<asp:HiddenField ID="hdReason" runat="server" value='<%#DataBinder.Eval(Container.DataItem, "Reason")%>'/>
                                <asp:DropDownList ID="ddlReason" runat="server" Enabled="false">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="Illness">Illness</asp:ListItem>
                                    <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                                    <asp:ListItem Value ="Business Trip">Business Trip</asp:ListItem>
                                    <asp:ListItem Value="Work Related">Work Related</asp:ListItem>
                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                </asp:DropDownList>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Homework">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblHWRelDate" runat="server" EnableViewState="true" Text='<%#DataBinder.Eval(Container.DataItem, "qreleasedate", "{0:MM/dd/yyyy}")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="HW Due Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblHWDueDate" runat="server" EnableViewState="true" Text='<%#DataBinder.Eval(Container.DataItem, "qdeadlinedate", "{0:MM/dd/yyyy}")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Answer">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblARelDate" runat="server" EnableViewState="true" Text='<%#DataBinder.Eval(Container.DataItem, "areleasedate", "{0:MM/dd/yyyy}")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="UserID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pwd" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                        <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                                    </asp:GridView>

                                </ItemTemplate>

                            </asp:Repeater>
                        </td>

                    </tr>

                </table>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div id="dvSearchSignups" visible="false" style="border: 1px solid grey; height: 120px; margin: auto; width: 900px;"
                    runat="server">
                    <div style="margin-top: 10px; margin-left: 20px;">
                        <center>
                            <h3>
                                <asp:Label ID="LblReportTitle" runat="server"></asp:Label></h3>

                        </center>
                        <div style="float: left;">
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <asp:DropDownList ID="ddlEventyearFilter" runat="server" Width="100">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 25px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <asp:DropDownList ID="ddlPhaseFilter" runat="server" OnSelectedIndexChanged="ddlPhaseFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="Fall" Selected="True">Fall</asp:ListItem>
                                        <asp:ListItem Value="Spring">Spring</asp:ListItem>
                                        <asp:ListItem Value="Summer">Summer</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 25px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <asp:DropDownList ID="ddlProductGroupFilter" OnSelectedIndexChanged="ddlProductGroupFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 25px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <asp:DropDownList ID="ddlProductFilter" OnSelectedIndexChanged="ddlProductFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div style="float: left;">

                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 35px;">
                                    <asp:DropDownList ID="ddlLevelFilter" runat="server" OnSelectedIndexChanged="ddlLevelFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div style="float: left; margin-left: 26px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Session#"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 13px;">
                                    <asp:DropDownList ID="ddlSessionFilter" runat="server" OnSelectedIndexChanged="ddlSessionFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div style="float: left; margin-left: 27px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label6" runat="server" Font-Bold="true" Text="Coach"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 54px;">
                                    <asp:DropDownList ID="ddlCoachFilter" runat="server" Width="100px" OnSelectedIndexChanged="ddlCoachFilter_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div style="float: left; margin-left: 25px;">
                                <div style="float: left;">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="true">Day </asp:Label>
                                </div>
                                <div style="float: left; margin-left: 34px;">
                                    <asp:DropDownList ID="ddlDayFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDayFilter_SelectedIndexChanged1" Width="100">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                        <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                        <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                        <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                        <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                        <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                        <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <div style="float: left;">
                                    <asp:Button ID="btnSearchFilter" runat="server" Visible="false" Text="Search" OnClick="btnSearchFilter_Click" />
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <asp:Button ID="btnClearFilter" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="float: left; margin-left: 5px;">
                    <asp:Button ID="BtnExportMissedCalReport" runat="server" Text="Export to Excel" OnClick="BtnExportMissedCalReport_Click" Visible="false" />
                </div>

                <div style="clear: both; margin-bottom: 5px;"></div>

                <asp:GridView ID="gvCoachClassCal" runat="server" Width="1100" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Year" Visible="false" DataField="EventYear" />
                        <asp:BoundField HeaderText="Event" Visible="false" DataField="EventCode" />
                        <asp:BoundField HeaderText="Product Group" DataField="ProductGroupCode" />
                        <asp:BoundField HeaderText="Product" DataField="ProductCode" />
                        <asp:BoundField HeaderText="Semester" DataField="Semester" />
                        <asp:BoundField HeaderText="Date" DataField="date" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:BoundField HeaderText="Day" DataField="day" />
                        <asp:BoundField HeaderText="Time" DataField="Time" />
                        <asp:BoundField HeaderText="Duration" DataField="Duration" />
                        <asp:BoundField HeaderText="Ser#" DataField="SerNo" />
                        <asp:BoundField HeaderText="Week#" DataField="WeekNo" />
                        <asp:BoundField HeaderText="Coach" DataField="Coach" />
                        <asp:BoundField HeaderText="Coach Email" DataField="Coachemail" />
                        <asp:BoundField HeaderText="Status" DataField="Status" />

                        <asp:BoundField HeaderText="Substitute" DataField="SubstituteName" />
                        <asp:BoundField HeaderText="Reason" DataField="Reason" />
                        <asp:BoundField HeaderText="Home Work" DataField="qreleasedate" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:BoundField HeaderText="HW Due Date" DataField="qdeadlinedate" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:BoundField HeaderText="Answers" DataField="areleasedate" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:BoundField HeaderText="UserID" DataField="UserID" />

                        <asp:BoundField HeaderText="PWD" DataField="PWD" Visible="false" />
                    </Columns>

                </asp:GridView>


            </td>
        </tr>
    </table>
</asp:Content>
