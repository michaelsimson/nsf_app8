﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Reports_FinalsRegReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadyear()
            loadcount(Now.Year, GVFinals)
            ltlYear.Text = Now.Year.ToString()
        End If
    End Sub
    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ltlYear.Text = ddlyear.SelectedValue
        loadcount(ddlyear.SelectedValue, GVFinals)
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 4 Step -1
            ddlyear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub
    Private Sub loadcount(ByVal year As Integer, ByVal GV As GridView)
        Dim dsFinalReg As New DataSet
        Dim tblFinalReg() As String = {"FinalReg"}
        Dim i As Integer
        Dim invitee, paid, pending As Integer
        invitee = 0
        paid = 0
        pending = 0
        Dim StrSQL As String = " select Distinct C.childnumber,C.ChapterId,C.productcode  into #tmpCnstchapter from contestant C inner join contestcategory CC ON CC.ContestYear=C.ContestYear and CC.ContestCode = C.ProductCode where C.contestyear=" & year & " and C.EventID = CASE WHEN CC.NationalSelectionCriteria = 'O' THEN 1 Else 2 END and  ((NationalInvitee=1 and Eventid=2) OR (Eventid=1)) order by childnumber;"
        StrSQL = StrSQL & " select Sum(t1.Invitee) as Invitee,Sum(t1.paid) as Paid,Sum(t1.pending) as Pending,t1.State,t1.ChapterCode from"
        StrSQL = StrSQL & " ((select count(C.contestant_ID) as Invitee,0 as Paid,0 as Pending,Ch.State,ch.ChapterCode from contestant C,Chapter Ch where C.ChapterID=Ch.ChapterID and C.contestyear=" & year & " and C.NationalInvitee=1 and C.Eventid=2 Group by Ch.State,Ch.ChapterCode) Union All"
        StrSQL = StrSQL & " (select 0 as Invitee,count(C.contestant_ID) as Paid,0 as Pending,Ch.State,ch.ChapterCode from contestant C,#tmpCnstchapter tmp,Chapter Ch where Ch.ChapterID=tmp.ChapterID and tmp.ProductCode=c.ProductCode and tmp.ChildNumber=C.ChildNumber and C.contestyear=" & year & " and C.PaymentReference is Not Null and C.Eventid=1 Group by Ch.State,Ch.ChapterCode)Union All"
        StrSQL = StrSQL & " select 0 as Invitee,0 as Paid,count(C.contestant_ID) as Pending,Ch.State,ch.ChapterCode from contestant C,#tmpCnstchapter tmp,Chapter Ch where Ch.ChapterID=tmp.ChapterID and tmp.ProductCode=c.ProductCode and tmp.ChildNumber=C.ChildNumber and C.contestyear=" & year & " and C.PaymentReference is  Null and C.Eventid=1 Group by Ch.State,Ch.ChapterCode) as t1"
        StrSQL = StrSQL & " Group by t1.State,t1.ChapterCode order by t1.state,t1.chaptercode;"
        StrSQL = StrSQL & " drop table #tmpCnstchapter"
        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsFinalReg, tblFinalReg)
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("ChapterCode", Type.GetType("System.String"))
        dt.Columns.Add("Invitee", Type.GetType("System.Int32"))
        dt.Columns.Add("Paid", Type.GetType("System.Int32"))
        dt.Columns.Add("Pending", Type.GetType("System.Int32"))
        dt.Columns.Add("PaidPercent", Type.GetType("System.String"))
        dt.Columns.Add("PendingPercent", Type.GetType("System.String"))
        For i = 0 To dsFinalReg.Tables("FinalReg").Rows.Count - 1
            dr = dt.NewRow()
            dr("ChapterCode") = dsFinalReg.Tables("FinalReg").Rows(i)("ChapterCode")
            dr("Invitee") = dsFinalReg.Tables("FinalReg").Rows(i)("Invitee")
            invitee = invitee + dr("Invitee")
            dr("Paid") = dsFinalReg.Tables("FinalReg").Rows(i)("Paid")
            paid = paid + dr("Paid")
            dr("Pending") = dsFinalReg.Tables("FinalReg").Rows(i)("Pending")
            pending = pending + dr("Pending")
            dr("PaidPercent") = String.Format("{0:N2}", (dsFinalReg.Tables("FinalReg").Rows(i)("Paid") / dr("Invitee")) * 100)
            dr("PendingPercent") = String.Format("{0:N2}", (dsFinalReg.Tables("FinalReg").Rows(i)("Pending") / dr("Invitee")) * 100)
            dt.Rows.Add(dr)
        Next
        dr = dt.NewRow()
        dr("ChapterCode") = "Total"
        dr("Invitee") = invitee
        dr("Paid") = paid
        dr("Pending") = pending
        dr("PaidPercent") = String.Format("{0:N2}", (paid / dr("Invitee")) * 100)
        dr("PendingPercent") = String.Format("{0:N2}", (pending / dr("Invitee")) * 100)
        dt.Rows.Add(dr)
        GV.DataSource = dt
        GV.DataBind()
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=FinalRegReport_" & ddlyear.SelectedValue & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New GridView
        loadcount(ddlyear.SelectedValue, dgexport)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.Write("Report does not include EW3, PS3 and BB")
        Response.[End]()
    End Sub
End Class
