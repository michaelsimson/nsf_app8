﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="ReviewExpTrans.aspx.cs" Inherits="ReviewExpTrans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="Scripts/jquery-1.9.1.js"></script>

    <div style="text-align: left">
        <script language="javascript" type="text/javascript">
            function transDelete() {
                if (confirm("Are you sure you want to delete this Transaction?")) {
                    document.getElementById('<%= btnConfirmDelete.ClientID%>').click();
                }
            }
        </script>
    </div>

    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    
      <asp:Button ID="btnConfirmDelete" Style="display: none;" runat="server" OnClick="btnConfirmDelete_Click" />
    <table id="tblContestSchedule" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Review Expense Transactions</h2>
                </center>
            </td>
        </tr>

        <tr>
            <td>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <table style="margin-left: auto; margin-right: auto;">
                    <tr>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Review By </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddReviewBy" runat="server" Width="140px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddReviewBy_SelectedIndexChanged">

                                <asp:ListItem Value="Payee">Payee</asp:ListItem>
                                <asp:ListItem Value="Check Number">Check Number</asp:ListItem>
                                <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                <asp:ListItem Value="Expense Category">Expense Category</asp:ListItem>
                                <asp:ListItem Value="Transaction Type">Transaction Type</asp:ListItem>
                                <asp:ListItem Value="Restriction Type">Restriction Type</asp:ListItem>
                                <asp:ListItem Value="Event">Event</asp:ListItem>
                                <asp:ListItem Value="Chapter">Chapter</asp:ListItem>
                                <asp:ListItem Value="Checks Not Issued">Checks Not Issued</asp:ListItem>
                                <asp:ListItem Value="Voided/Not Cashed">Voided/Not Cashed</asp:ListItem>
                            </asp:DropDownList>

                        </td>

                        <td id="tdReviewTitle" runat="server" visible="false" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblReviewTitle" Style="font-weight: bold;" runat="server"></asp:Label>
                        </td>
                        <td id="tdReviewDContent" runat="server" visible="false" style="width: 100px" align="left">
                            <asp:DropDownList ID="ddReviewContent" runat="server" Width="130px" AutoPostBack="True" Visible="false">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtReviewContent" runat="server" Visible="false"></asp:TextBox>
                        </td>

                        <td id="tdorgCheck" runat="server" visible="false" align="left" nowrap="nowrap">
                            <asp:RadioButton ID="rbtOrgCheck" GroupName="Donor" AutoPostBack="true" runat="server" OnCheckedChanged="rbtOrgCheck_CheckedChanged" /></td>
                        <td id="tdOrgTitle" runat="server" visible="false" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label1" Style="font-weight: bold;" runat="server">Organization</asp:Label>
                        </td>
                        <td id="tdOrgContent" runat="server" visible="false" style="width: 100px" align="left">
                            <asp:DropDownList ID="ddlOrganization" runat="server" Enabled="false" Width="130px" AutoPostBack="True">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>

                        </td>
                        <td id="tdIndvidualCheck" runat="server" visible="false" align="left" nowrap="nowrap">
                            <asp:RadioButton ID="rbtIndividualCheck" AutoPostBack="true" GroupName="Donor" runat="server" OnCheckedChanged="rbtIndividualCheck_CheckedChanged" /></td>
                        <td id="tdIndividualTitle" runat="server" visible="false" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" Style="font-weight: bold;" runat="server">Individual</asp:Label>
                        </td>
                        <td id="tdIndividualContent" runat="server" visible="false" style="width: 100px" align="left">
                            <asp:DropDownList ID="ddlIndividualContent" Enabled="false" runat="server" Width="130px" AutoPostBack="True">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>

                        </td>
                        <td id="tdSubmit" runat="server" align="left">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                        <td id="tdExport" runat="server" visible="false" align="left">

                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" OnClick="btnExportToExcel_Click" />

                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="1200px" style="margin-left: 40px; margin-right: auto; font-weight: bold; background-color: #ffffcc;" id="tblUpdateTransactions" runat="server" visible="false">
                    <tr class="ContentSubTitle" align="center">
                        <td style="text-align: right">Payee</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlpayee" runat="server" AutoPostBack="True" Width="125px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Amount</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtAmount" runat="server" Width="80px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">Check Date</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtCheckDate" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">Check No</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtCheckNo" runat="server" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">Bank</td>
                        <td>
                            <asp:DropDownList ID="ddlbank" runat="server" Width="125px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Exp Cat</td>
                        <td>
                            <asp:DropDownList ID="ddlExpCat" runat="server" Width="86px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Trans Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTransType" runat="server" AutoPostBack="True" Width="106px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Rest Type</td>
                        <td>
                            <asp:DropDownList ID="ddlRestType" runat="server" AutoPostBack="True" Width="106px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">Event
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True" Width="85px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Chapter
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlChapter" runat="server" AutoPostBack="True" Width="85px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">To Bank
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTobank" runat="server" AutoPostBack="True" Width="106px">
                                <asp:ListItem Value="NULL">Select</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Year</td>
                        <td>
                            <asp:TextBox ID="txtEventYear" runat="server" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">Account
                        </td>
                        <td>

                            <asp:TextBox ID="txtAccount" runat="server" Width="80px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">&nbsp;Report Date
                        </td>
                        <td>

                            <asp:TextBox ID="txtReportDate" runat="server" Width="80px"></asp:TextBox>
                        </td>

                    </tr>

                    <tr>
                        <td style="text-align: center" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Style="text-align: center" OnClick="btnUpdate_Click" />
                            &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>


                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="text-align: center">
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                </div>
                <div style="text-align: center">
                    <asp:Label ID="lblTransTitle" Style="font-weight: bold;" runat="server" Visible="false"> Table : Expense Transactions</asp:Label>
                </div>

                <asp:GridView ID="grdReviewExpenseTran" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-right: auto; margin-top: 10px; table-layout: fixed;" HeaderStyle-BackColor="#ffffcc" PageSize="100" AllowPaging="true" OnPageIndexChanging="grdReviewExpenseTran_PageIndexChanging" OnRowCommand="grdReviewExpenseTran_RowCommand" OnRowDeleting="grdReviewExpenseTran_RowDeleting">
                    <Columns>


                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="63px">

                            <ItemTemplate>
                                <asp:Button ID="btnModifyTrans" runat="server" Text="Modify" CommandName="Modify" />
                                <asp:Button ID="btnDeleteTrans" runat="server" Text="Delete" CommandName="Delete" />
                                <div style="display: none;">
                                    <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DonorType") %>'>'></asp:Label>
                                    <asp:Label ID="lblbankIDFill" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BanKID") %>'>'></asp:Label>
                                    <asp:Label ID="lblToBankIDFill" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ToBankID") %>'>'></asp:Label>
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                    <asp:Label ID="lblToChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ToChapterID") %>'>'></asp:Label>
                                    <asp:Label ID="lblExpID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExpCatID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTransTypeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TransType") %>'>'></asp:Label>
                                    <asp:Label ID="lblRestTypeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RestTypeFrom") %>'>'></asp:Label>
                                    <asp:Label ID="lblReImbID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ReimbMemberID") %>'>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TransID" HeaderStyle-Width="50px">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTransaID" Text='<%#DataBinder.Eval(Container.DataItem,"TransactionID") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="50px">

                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblStatus" Text='<%#DataBinder.Eval(Container.DataItem,"TransStatus") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cashed Date" HeaderStyle-Width="70px">

                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblCashedDate" Text='<%#DataBinder.Eval(Container.DataItem,"StatusDate","{0:MM/dd/yyyy}") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payee" HeaderStyle-Width="100px">

                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblPayee" Text='<%#DataBinder.Eval(Container.DataItem,"ReImbName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="80px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblAmount" Text='<%#DataBinder.Eval(Container.DataItem,"ExpenseAmount","{0:n2}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Check Date" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label Style="word-wrap: break-word;" runat="server" ID="lblCheckDate" Text='<%#DataBinder.Eval(Container.DataItem,"DatePaid","{0:MM/dd/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <%--  <EditItemTemplate>

                                    <asp:DropDownList ID="ddlPhase1" runat="server">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>

                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Check No" HeaderStyle-Width="75px">
                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblCheckNumber" Text='<%#DataBinder.Eval(Container.DataItem,"CheckNumber") %>'></asp:Label>
                            </ItemTemplate>
                            <%--    <EditItemTemplate>

                                    <asp:DropDownList ID="ddlPhase2" runat="server">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>

                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BankID" HeaderStyle-Width="110px">

                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblBankID" Text='<%#DataBinder.Eval(Container.DataItem,"BankCode") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exp Cat" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblExpCat" Text='<%#DataBinder.Eval(Container.DataItem,"ExpCatDesc") %>'></asp:Label>
                            </ItemTemplate>
                            <%--   <EditItemTemplate>

                                    <asp:DropDownList ID="ddlPhase3" runat="server">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>

                                    </asp:DropDownList>
                                </EditItemTemplate>--%>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Trans Type" HeaderStyle-Width="80px">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTranstype" Style="word-wrap: break-word;" Text='<%#DataBinder.Eval(Container.DataItem,"TransType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rest Type" HeaderStyle-Width="80px">

                            <ItemTemplate>

                                <asp:Label runat="server" ID="lblRestType" Style="word-wrap: break-word;" Text='<%# DataBinder.Eval(Container.DataItem,"RestTypeFrom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event" HeaderStyle-Width="80px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblEvent" Text='<%# DataBinder.Eval(Container.DataItem,"Event") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Chapter" HeaderStyle-Width="80px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblChapter" Text='<%# DataBinder.Eval(Container.DataItem,"Chapter") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ToBankID" HeaderStyle-Width="110px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblToBankID" Text='<%# DataBinder.Eval(Container.DataItem,"ToBankCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Year" HeaderStyle-Width="50px">

                            <ItemTemplate>

                                <asp:Label runat="server" ID="lblYear" Text='<%# DataBinder.Eval(Container.DataItem,"Year") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account" HeaderStyle-Width="50px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblAccount" Text='<%# DataBinder.Eval(Container.DataItem,"Account") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Report Date" HeaderStyle-Width="70px">

                            <ItemTemplate>

                                <asp:Label runat="server" Style="word-wrap: break-word;" ID="lblReportDate" Text='<%# DataBinder.Eval(Container.DataItem,"ReportDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


                <div style="clear: both; margin-top: 10px;">
                </div>
                <div style="text-align: center">
                    <asp:Label ID="lblVoidedChecksTitle" Style="font-weight: bold;" runat="server" Visible="true"> </asp:Label>
                </div>
                <asp:GridView ID="grdVoidedChecks" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1000px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="100" OnPageIndexChanging="grdVoidedChecks_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="TransStatus" Visible="true" HeaderText="Status" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="CashedDate" DataFormatString="{0:MM/dd/yyyy}" Visible="true" HeaderText="Cashed Date" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="CheckDate" DataFormatString="{0:MM/dd/yyyy}" Visible="true" HeaderText="Check Date" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="bankID" Visible="true" HeaderText="BankID" HeaderStyle-Width="200px"></asp:BoundField>

                        <asp:BoundField DataField="CheckNumber" Visible="true" HeaderText="Check Number" HeaderStyle-Width="100px" />

                        <asp:BoundField DataField="Amount" DataFormatString="{0:n2}" Visible="true" HeaderText="Amount" HeaderStyle-Width="100px"></asp:BoundField>
                        <asp:BoundField DataField="Payee" Visible="true" HeaderText="Payee" HeaderStyle-Width="200px"></asp:BoundField>
                        <asp:BoundField DataField="DonorType" Visible="true" HeaderText="Donor Type" HeaderStyle-Width="100px"></asp:BoundField>
                    </Columns>
                </asp:GridView>
                <div style="clear: both; margin-top: 10px;">
                </div>
                <div style="text-align: center">
                    <asp:Label ID="lblNotChashedTitle" Style="font-weight: bold;" runat="server" Visible="true"> </asp:Label>
                </div>
                <asp:GridView ID="grdNotCashed" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1000px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="100" OnPageIndexChanging="grdNotCashed_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="TransStatus" Visible="true" HeaderText="Status" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="StatusDate" DataFormatString="{0:MM/dd/yyyy}" Visible="true" HeaderText="Cashed Date" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="Datepaid" DataFormatString="{0:MM/dd/yyyy}" Visible="true" HeaderText="Check Date" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="BankCode" Visible="true" HeaderText="BankID" HeaderStyle-Width="200px"></asp:BoundField>

                        <asp:BoundField DataField="CheckNumber" Visible="true" HeaderText="Check Number" HeaderStyle-Width="100px" />

                        <asp:BoundField DataField="Amount" DataFormatString="{0:n2}" Visible="true" HeaderText="Status" HeaderStyle-Width="100px"></asp:BoundField>
                        <asp:BoundField DataField="ReImbName" Visible="true" HeaderText="Payee" HeaderStyle-Width="200px"></asp:BoundField>
                        <asp:BoundField DataField="DonorType" Visible="true" HeaderText="Donor Type" HeaderStyle-Width="100px"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>



        </tr>
    </table>
    <asp:HiddenField ID="hdnTrnsactionID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnDonorType" Value="" runat="server" />
</asp:Content>
