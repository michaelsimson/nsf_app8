﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class CoachClassCalStudentsRpt
    Inherits System.Web.UI.Page


    Dim cmdText As String
    Dim iChildNumber As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Session("EntryToken") = "student"
        'Session("RoleId") = 1 '89
        'Session("LoginID") = 45750     '  46278  
        'Session("LoggedIn") = "true"
        'Session("StudentId") = 28942

        Try

            If LCase(Session("LoggedIn")) <> "true" Or Session("LoginID").ToString = String.Empty Then
                Response.Redirect("~/maintest.aspx")
            End If
            'If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "96") Then
            ChangeLinkLabel()
            If Page.IsPostBack = False Then
                If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
                    LoadChild(Session("LoginID"), "Y", False)
                ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    LoadChild(Session("LoginID"), "Y", True)
                    ' tblChild.Visible = False
                    ' btnSubmit_Click(btnSubmit, New EventArgs)
                End If

                Dim year As Integer = 0
                year = Convert.ToInt32(DateTime.Now.Year)
                Dim i, j As Integer
                j = 0
                For i = 0 To 4
                    ddlEventYear.Items.Insert(i, Convert.ToString(year - j))
                    j = j + 1
                Next

                ' ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
                cmdText = "select max(eventyear) from calsignup"
                Dim iYear As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(iYear)))
                If ddlEventYear.SelectedIndex = -1 Then
                    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
                End If
                'Else
                '    Server.Transfer("maintest.aspx")
            End If
        Catch ex As Exception
            '  Response.Write(ex.ToString())
        End Try


    End Sub

    Private Sub LoadChild(ByVal ParentID As Integer, ByVal status As String, IsChild As Boolean)

        Try

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim strSql As String = "Select distinct First_name+' '+Last_name as Name, ChildNumber From Child where memberid in ( " & ParentID & "," & Session("CustIndID") & ") "
            If IsChild = True Then
                strSql = strSql & " and ChildNumber=" & Session("StudentId")
            End If
            Dim drChild As SqlDataReader
            drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlChild.DataSource = drChild
            ddlChild.DataBind()
            ddlChild.Enabled = True
            If status = "Y" Then
                If drChild.HasRows Then
                    If ddlChild.Items.Count = 1 Then
                        ddlChild.Enabled = False
                    Else
                        ddlChild.Items.Insert(0, New ListItem("Select Child", "-1"))
                    End If
                Else
                    ddlChild.Items.Insert(0, New ListItem("No Child", "-2"))
                End If
                ddlChild.Items(0).Selected = True
            End If

        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try

    End Sub

    Private Sub ChangeLinkLabel()

        If (Session("entryToken").ToString().ToUpper() = "PARENT") Then

            hlnkMainMenu.Text = "Back to Parent Functions"
            hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
            'LoadChild(Session("LoginID"), "Y")

        ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then

            hlnkMainMenu.Text = "Back to Student Functions"
            hlnkMainMenu.NavigateUrl = "~/StudentFunctions.aspx"
            ' tdChild.Visible = False
        Else

            hlnkMainMenu.Text = "Back to Volunteer Functions"
            hlnkMainMenu.NavigateUrl = "~/VolunteerFunctions.aspx"
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            CoachList.Visible = False
            If tblChild.Visible = True Then
                If ddlChild.SelectedItem.Value = "No Child" Then
                    lblMsg.Text = "There is no child to select"
                    Exit Sub
                ElseIf ddlChild.SelectedItem.Value = "-1" Then
                    lblMsg.Text = "Select Child"
                    Exit Sub
                End If
            End If

            cmdText = "  select distinct cc.EventYear, 'Coaching' EventCode, cc.ProductGroup ProductGroupCode,cc.ProductGroupId,(select name from productgroup where productgroupid=cc.ProductGroupId) ProductGroupName, cc.Product ProductCode, cc.ProductId ,(select name from product where productid=cc.productid) as ProductName, cc.Semester, cc.Level ,cc.SessionNo, IP.FirstName+' '+IP.LastName as CoachName "
            cmdText = cmdText + " from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo and cp.Semester=cc.Semester "
            cmdText = cmdText + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId "
            cmdText = cmdText + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
            cmdText = cmdText + " left join coachreg reg on reg.CMemberId= cs.MemberId and reg.EventID=13 and reg.EventYear=cs.EventYear and reg.ProductGroupId=cs.productgroupid and reg.productid = cs.productid and reg.Semester=cs.Semester and reg.level=cs.level and reg.sessionno=cs.sessionno and approved='y' "

            Dim iChildNumber As Integer
            If tblChild.Visible = True Then
                iChildNumber = ddlChild.SelectedItem.Value
            Else
                iChildNumber = Convert.ToInt32(Session("StudentId"))
            End If
            cmdText = cmdText + " left join child ch on ch.childnumber= reg.childnumber and ch.childnumber=" & iChildNumber & " inner join Indspouse IP on (IP.AutoMemberId=cc.memberId) where cc.[date] is not null and cc.EventYear=" & ddlEventYear.SelectedItem.Value & " and cc.EventId=13 and reg.childnumber=" & iChildNumber & " and reg.Approved='Y' and reg.Eventyear=" & ddlEventYear.SelectedItem.Value & "" '(cc.[date]>=getdate() and cc.[date]<= dateadd(dd,32, getdate())) and cc.[date] is not null "

            cmdText = cmdText + " order by cc.productgroupID, cc.ProductID, cc.Semester, cc.Level, cc.SessionNo"
            hdTableCnt.Value = "1"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            rptCoachClass.DataSource = ds.Tables(0)
            rptCoachClass.DataBind()
            If rptCoachClass.Items.Count = 0 Then
                lblMsg.Text = "No classes"
                lblMsg.Visible = True
            End If

            cmdText = "select ch.first_name+ ' ' + ch.last_name as childname,cr.EventYear, 'Coaching' EventCode, cr.ProductGroupCode ProductGroupCode,(select name from productgroup where productgroupid=cr.ProductGroupId) ProductGroupName, cr.ProductCode ProductCode,(select name from product where productid=cr.productid) as ProductName, cr.Semester, cr.Level ,cr.SessionNo "
            cmdText = cmdText + ",(select I1.Firstname + ' '+ I1.LastName   from indspouse I1 where I1.automemberid=cr.cMemberId) as Coach"
            cmdText = cmdText + " from coachreg cr left join calsignup cs on cs.MemberId=cr.PMemberId and cs.EventId=13 and cs.EventYear=cr.EventYear and cs.ProductGroupId= cr.ProductGroupId and cs.ProductId=cr.ProductId and cs.Semester=cr.Semester and cs.level=cr.level and cs.sessionno=cr.sessionno and cs.Accepted='y'"
            cmdText = cmdText + " inner join child ch on cr.childnumber=ch.childnumber"
            cmdText = cmdText + " where cr.eventyear=" & ddlEventYear.SelectedItem.Value & " and cr.childnumber=" & iChildNumber
            If Not Session("MissedProductList") Is Nothing Then
                cmdText = cmdText + " and CR.ProductId not in ( " & Session("MissedProductList") & " )"
            End If
            Dim dsUnReg As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If dsUnReg.Tables(0).Rows.Count > 0 Then
                CoachList.Visible = True
                gvCoachReg.DataSource = dsUnReg.Tables(0)
                gvCoachReg.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptCoachClass_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCoachClass.ItemDataBound

        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim lblTableCnt As Label = DirectCast(e.Item.FindControl("lblTableCnt"), Label)
            lblTableCnt.Text = hdTableCnt.Value
            hdTableCnt.Value = CInt(hdTableCnt.Value) + 1
            Dim hdProductGrp As HiddenField = DirectCast(e.Item.FindControl("hdProductGrp"), HiddenField)
            Dim hdProduct As HiddenField = DirectCast(e.Item.FindControl("hdProduct"), HiddenField)
            Dim lblSemester As Label = DirectCast(e.Item.FindControl("lblSemester"), Label)
            Dim lblLevel As Label = DirectCast(e.Item.FindControl("lblLevel"), Label)

            Dim lblSessionNo As Label = DirectCast(e.Item.FindControl("lblSessionNo"), Label)
            Dim grid As GridView = DirectCast(e.Item.FindControl("gvCoachClassCal"), GridView)

            cmdText = "  select ch.first_name+ ' ' + ch.last_name as childname,cc.EventYear, 'Coaching' EventCode, cc.ProductGroup ProductGroupCode, cc.Product ProductCode , cc.Semester, cc.[Date] [date], cc.[Day],cc.[Time], cc.Duration, cc.SerNo, cc.WeekNo,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1"
            cmdText = cmdText + " where I1.automemberid=cc.MemberId) as Coach, cc.[Status], (select I2.FirstName + ' ' + I2.LastName from indspouse I2 where I2.automemberid=cc.Substitute) as SubstituteName, (select I3.FirstName + ' ' + I3.LastName from indspouse I3 where I3.automemberid=cc.Makeup) as MakeupName, Reason,ClassType, "
            cmdText = cmdText + " rel.QReleaseDate,rel.QDeadlineDate, rel.AReleaseDate , cs.UserId, cs.PWD from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo and cp.Semester=cc.Semester "
            cmdText = cmdText + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.Semester=cc.Semester and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId "
            cmdText = cmdText + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.Semester=cc.Semester and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
            cmdText = cmdText + " left join coachreg reg on reg.CMemberId= cs.MemberId and reg.EventID=13 and reg.EventYear=cs.EventYear and reg.ProductGroupId=cs.productgroupid and reg.productid = cs.productid and reg.Semester=cs.Semester and reg.level=cs.level and reg.sessionno=cs.sessionno and approved='y'"
            cmdText = cmdText + " left join child ch on ch.childnumber= reg.childnumber  "

            If tblChild.Visible = True Then
                iChildNumber = ddlChild.SelectedItem.Value
            Else
                iChildNumber = Convert.ToInt32(Session("StudentId"))
            End If

            cmdText = cmdText + " where cc.EventYear=" & ddlEventYear.SelectedItem.Value & "  and cc.[date] is not null and ch.ChildNumber=" & iChildNumber & " and cc.EventID=13  and  cc.Status='On'" '(cc.[date]>=getdate() and cc.[date]<= dateadd(dd,32, getdate()))

            cmdText = cmdText + " and cc.ProductGroupId=" & hdProductGrp.Value & " and cc.ProductId=" & hdProduct.Value & " and cc.Semester='" & lblSemester.Text & "' and cc.Level='" & lblLevel.Text & "' and cc.SessionNo=" & lblSessionNo.Text & ""
            cmdText = cmdText + " order by childname, cc.productgroupID, cc.ProductID, cc.Semester, cc.Date, cc.Time"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count = 0 Then
                hdTableCnt.Value = CInt(hdTableCnt.Value) - 1
                e.Item.Visible = False
            Else
                If Not Session("MissedProductList") Is Nothing Then
                    Session("MissedProductList") = Session("MissedProductList") & "," & hdProduct.Value
                Else
                    Session("MissedProductList") = hdProduct.Value
                End If
                lblMsg.Visible = False
                grid.DataSource = ds.Tables(0)
                grid.DataBind()
            End If
        End If

    End Sub

End Class
