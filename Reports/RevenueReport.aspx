<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RevenueReport.aspx.vb" Inherits="Reports_RevenueReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
      <style type="text/css">
	._ctl0_NavLinks_0 { background-color:white;visibility:hidden;display:none;position:absolute;left:0px;top:0px; }
	._ctl0_NavLinks_1 { text-decoration:white; }
	._ctl0_NavLinks_2 {  }

</style>
</head>
<body>

    <form id="form1" runat="server">
    <table  style="color: White;font-size: 10pt;"   width ="100%" border ="1" cellpadding = "10" >
        <tr>
            <td align="center" style="height: 21px; width: 131px;"><a href ="RevenueReport.aspx?Param=ByMonth">Revenue by Month</a></td>
            <!--<td style="width: 118px; height: 21px" align="center"><a href ="RevenueReport.aspx?Param=ByDate">Revenue by Day</a></td>-->
            <!--<td style="width: 165px; height: 21px" align="center"><a href ="RevenueReport.aspx?Param=ByContest">Revenue by Contest</a></td>-->
            <td align="center" style="width: 165px; height: 21px" ><a href ="RevenueReport.aspx?Param=ByChapter">Revenue by Chapter</a></td>
        </tr>
    </table>
    <asp:Panel ID="PnlByMonth" runat="server" Visible="false" >
    <div>
        <table style="width: 900px" border="0">
            <tr>
                <td  style="width: 723px" align="center">
                    <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                        <tr>
                                        <td style="height: 19px; background-color: white;" colspan="2">
                                         <asp:Label ID="lblHeader" runat="server" Text="Revenue by Month" Width="192px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                Select Month</td>
                            <td style="height: 19px">
                                <asp:DropDownList ID="ddMonth" runat="server">
                                    <asp:ListItem Value="0">--------------------</asp:ListItem>
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">December</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 82px" class="ItemLabel">
                                Select Year</td>
                            <td>
                                <asp:DropDownList ID="ddYear" runat="server" Width="105px">
                                    <asp:ListItem Value="0">--------------------</asp:ListItem>
                                    <asp:ListItem Value="2006">2006</asp:ListItem>
                                    <asp:ListItem Value="2007">2007</asp:ListItem>
                                    <asp:ListItem Value="2008">2008</asp:ListItem>
                                    <asp:ListItem Value="2009">2009</asp:ListItem>
                                    <asp:ListItem Value="2010">2010</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 82px">
                            </td>
                            <td>
                                <asp:Button ID="btnFind" runat="server" Text="Find" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 723px; height: 1px">
                    <asp:Button ID="btnExcel" runat="server" Text="ExportToExcel" />&nbsp;<br />
                    <br />
                    <span class="Apple-style-span" style="word-spacing: 0px; font: 13px arial; text-transform: none;
                        color: rgb(0,0,0); text-indent: 0px; white-space: normal; letter-spacing: normal;
                        border-collapse: collapse; orphans: 2; widows: 2; webkit-border-horizontal-spacing: 0px;
                        webkit-border-vertical-spacing: 0px; webkit-text-decorations-in-effect: none;
                        webkit-text-size-adjust: auto; webkit-text-stroke-width: 0"><strong style="color: blue">
                            Revenues:&nbsp; Online Registration Fees and Donations</strong></span></td>
            </tr>
            <tr>
                <td style="width: 723px; height: 129px;">
                    <asp:gridview ID="dvRevenue" runat="server" Width="800px"></asp:gridview>
                    <asp:Label ID="lblNoData" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 723px; height: 18px;">
                </td>
            </tr>
        </table>
    </div>
    </asp:Panel> 
    
    <asp:Panel ID="PnlByDate" runat="server" Visible="false" >
    <div>
        <table style="width: 900px" border="0">
					<tr width="100%" >
					<td >
					    <table style="width: 806px">
                                    <tr>
                                        <td style="width: 723px" align="center">
                                            <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                                                <tr>
                                                                <td style="height: 19px" bgcolor="white" colspan="2">
                                                                    <asp:Label ID="lblDayHeader" runat="server" Text="Revenue by Day" Width="192px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                                        Select Month</td>
                                                    <td style="height: 19px">
                                                        <asp:DropDownList ID="ddlMonth" runat="server">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="1">January</asp:ListItem>
                                                            <asp:ListItem Value="2">February</asp:ListItem>
                                                            <asp:ListItem Value="3">March</asp:ListItem>
                                                            <asp:ListItem Value="4">April</asp:ListItem>
                                                            <asp:ListItem Value="5">May</asp:ListItem>
                                                            <asp:ListItem Value="6">June</asp:ListItem>
                                                            <asp:ListItem Value="7">July</asp:ListItem>
                                                            <asp:ListItem Value="8">August</asp:ListItem>
                                                            <asp:ListItem Value="9">September</asp:ListItem>
                                                            <asp:ListItem Value="10">October</asp:ListItem>
                                                            <asp:ListItem Value="11">November</asp:ListItem>
                                                            <asp:ListItem Value="12">December</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px" class="ItemLabel">
                                                        Select Year</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlYear" runat="server" Width="105px">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="2006">2006</asp:ListItem>
                                                            <asp:ListItem Value="2007">2007</asp:ListItem>
                                                            <asp:ListItem Value="2008">2008</asp:ListItem>
                                                            <asp:ListItem Value="2009">2009</asp:ListItem>
                                                            <asp:ListItem Value="2010">2010</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px">
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnDayFind" runat="server" Text="Find" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 1px">
                                            <asp:Button ID="btndayExcel" runat="server" Text="ExportToExcel" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 129px;">
                                        <asp:gridview ID="dvRevenuebyday" runat="server" Width="790px">
                                        </asp:gridview>
                                            <asp:Label ID="Label2" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 18px;">
                                        </td>
                                    </tr>
                                </table>
					    
                                
                          </td>
					</tr>
					
				</table>
					

    </div>
    
    </asp:Panel> 
    
    <asp:Panel ID="pnlByContest" runat="server" Visible="false" >
    <div>
        <table style="width: 900px" border="0">
					<tr>
					<td >
					    <table style="width: 806px">
                                    <tr>
                                        <td style="width: 723px" align="center">
                                            <table style="width: 260px; height: 65px; background-color: silver;" border="1">
                                                <tr>
                                                                <td style="height: 19px" bgcolor="white" colspan="2">
                                                                    <asp:Label ID="lblContestHeader" runat="server"></asp:Label></td>                                                                    
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                                                        Select Month</td>
                                                    <td style="height: 19px">
                                                        <asp:DropDownList ID="ddlContestMonth" runat="server">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="1">January</asp:ListItem>
                                                            <asp:ListItem Value="2">February</asp:ListItem>
                                                            <asp:ListItem Value="3">March</asp:ListItem>
                                                            <asp:ListItem Value="4">April</asp:ListItem>
                                                            <asp:ListItem Value="5">May</asp:ListItem>
                                                            <asp:ListItem Value="6">June</asp:ListItem>
                                                            <asp:ListItem Value="7">July</asp:ListItem>
                                                            <asp:ListItem Value="8">August</asp:ListItem>
                                                            <asp:ListItem Value="9">September</asp:ListItem>
                                                            <asp:ListItem Value="10">October</asp:ListItem>
                                                            <asp:ListItem Value="11">November</asp:ListItem>
                                                            <asp:ListItem Value="12">December</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px" class="ItemLabel">
                                                        Select Year</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlContestYear" runat="server" Width="105px" AutoPostBack="true">
                                                            <asp:ListItem Value="0">--------------------</asp:ListItem>
                                                            <asp:ListItem Value="2006">2006</asp:ListItem>
                                                            <asp:ListItem Value="2007" Selected="True">2007</asp:ListItem>
                                                            <asp:ListItem Value="2008">2008</asp:ListItem>
                                                            <asp:ListItem Value="2009">2009</asp:ListItem>
                                                            <asp:ListItem Value="2010">2010</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 82px">
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnContestFind" runat="server" Text="Find" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 1px">
                                            <asp:Button ID="btnContestExcel" runat="server" Text="ExportToExcel" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 129px;">
                                        <asp:gridview ID="dvRevenuecontest" runat="server" Width="790px">
                                        </asp:gridview>
                                            <asp:Label ID="Label3" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 723px; height: 14px;">
                                        </td>
                                    </tr>
                                </table>                                
                          </td>
					</tr>
					
				</table>
    </div>
    </asp:Panel> 
    
    <asp:Panel ID="pnlByChapter" runat="server" Visible="false">
    <div>
        <table style="width: 900px" border="0">
        <tr>
            <td style="width: 668px" align="center">
                <table style="width: 223px; height: 65px; background-color: silver;" border="1">
                    <tr>
                        <td style="height: 19px" bgcolor="white" colspan="2">
                            <asp:Label ID="lblChapterHeader" runat="server" Text="Revenue by Chapter" Width="192px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 19px;" class="ItemLabel" nowrap>
                            Select Month</td>
                        <td style="height: 19px; width: 171px;">
                            <asp:DropDownList ID="ddlChapterMonth" runat="server">
                                <asp:ListItem Value="0">--------------------</asp:ListItem>
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 26px;" class="ItemLabel">
                            Select Year</td>
                        <td style="width: 171px; height: 26px">
                            <asp:DropDownList ID="ddlChapterYear" runat="server" Width="105px">
                                <asp:ListItem Value="0">--------------------</asp:ListItem>
                                <asp:ListItem Value="2006">2006</asp:ListItem>
                                <asp:ListItem Value="2007">2007</asp:ListItem>
                                <asp:ListItem Value="2008">2008</asp:ListItem>
                                <asp:ListItem Value="2009">2009</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 82px">
                        </td>
                        <td style="width: 171px">
                            <asp:Button ID="btnChapterFind" runat="server" Text="Find" /></td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td style="width: 668px; height: 1px">
                <asp:Button ID="btnChapterExcel" runat="server" Text="ExportToExcel" /><br />
                <br />
                <strong><span style="font-size: 9pt; color: #0000ff">Revenues:&nbsp; Online Registration
                    Fees and Donations</span></strong></td>
        </tr>
        <tr>
            <td style="width: 668px; height: 129px;">
                <asp:gridview ID="dvRevenuechapter" runat="server" Width="880px"></asp:gridview>
                <asp:Label ID="Label4" runat="server" CssClass="announcement_text" Width="601px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 668px; height: 14px;">
            </td>
        </tr>
        </table>
    </div>
    </asp:Panel> 
    </form>
</body>

</html>
