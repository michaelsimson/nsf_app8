<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DisplayPercentiles.aspx.cs" Inherits="Reports_DisplayPercentiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Display Percentile</title>
</head>
<body>
		<form id="Form1" method="post" runat="server">
		<h3 align="center" style='TEXT-ALIGN:center'>Percentile values for Selected Contest</h3>
		 <p style=" RIGHT: 300px; POSITION: absolute; TOP: 34px; width: 174px; margin-left: 80px;"><asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
	        </p>
			<asp:datagrid id="DataGrid1" 
            
            style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 109px; height: 310px; width: 799px;" 
            runat="server" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" BackColor="White"
				CellPadding="3" GridLines="Vertical">
				
<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>

				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="Blue"></HeaderStyle>
				<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			<asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" style="Z-INDEX: 102; LEFT: 50px; POSITION: absolute; TOP: 24px" onselectedindexchanged="ddlYear_SelectedIndexChanged">
			    <asp:ListItem Value="2013">2013</asp:ListItem>
				<asp:ListItem Value="2012">2012</asp:ListItem>
				<asp:ListItem Value="2011">2011</asp:ListItem>
				<asp:ListItem Value="2010">2010</asp:ListItem>
			    <asp:ListItem Value="2009">2009</asp:ListItem>
			</asp:DropDownList>
            
			<asp:dropdownlist id="ddlContest" style="Z-INDEX: 102; LEFT: 150px; POSITION: absolute; TOP: 24px"
				runat="server" AutoPostBack="True">
				<asp:ListItem Value="JSB">JSB</asp:ListItem>
				<asp:ListItem Value="SSB">SSB</asp:ListItem>
				<asp:ListItem Value="JVB">JVB</asp:ListItem>
				<asp:ListItem Value="IVB">IVB</asp:ListItem>
				<asp:ListItem Value="SVB">SVB</asp:ListItem>
				<asp:ListItem Value="MB1">MB1</asp:ListItem>
				<asp:ListItem Value="MB2">MB2</asp:ListItem>
				<asp:ListItem Value="MB3">MB3</asp:ListItem>
				<asp:ListItem Value="MB4">MB4</asp:ListItem>
				<asp:ListItem Value="JGB">JGB</asp:ListItem>
				<asp:ListItem Value="SGB">SGB</asp:ListItem>
				<asp:ListItem Value="EW1">EW1</asp:ListItem>
				<asp:ListItem Value="EW2">EW2</asp:ListItem>
				<asp:ListItem Value="EW3">EW3</asp:ListItem>
				<asp:ListItem Value="PS1">PS1</asp:ListItem>
				<asp:ListItem Value="PS3">PS3</asp:ListItem>
				<asp:ListItem Value="JSC">JSC</asp:ListItem>
				<asp:ListItem Value="ISC">ISC</asp:ListItem>
				<asp:ListItem Value="SSC">SSC</asp:ListItem>
			</asp:dropdownlist>
			
			<asp:Button ID="Button1" runat="server" Text="Show CutOff Scores" onclick="Button1_Click" />
		   
		<br />
		   
		</form>
	</body>
</html>


 
 
 