<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="BankTransReports.aspx.vb" Inherits="Reports_BankTransReports" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Bank Transaction Reports</title>
</head>
<body>
    <form id="form1" runat="server">
     
    <div style="text-align:left">
    <br />
          <asp:LinkButton ID="lbtnVolunteerFunctions" OnClick="lbtnVolunteerFunctions_Click" runat="server">Back to Volunteer Functions</asp:LinkButton>
&nbsp;&nbsp;&nbsp;<br />
   
    <center>
    <table border ="0" cellpadding = "5" cellspacing = "0" width="400px">
    <tr><td align="center" colspan ="3"><b>Bank Trans Reports</b></td></tr>
     <tr>
            <td align="left" colspan="3">
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtnDaily" runat="server" GroupName="Reportby" Text="Daily Transactions Report" AutoPostBack="true"  OnCheckedChanged="rbtnDaily_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan ="3">
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtnCategory" runat="server" GroupName="Reportby" Text="Report by Tran Category" AutoPostBack="true"  OnCheckedChanged="rbtnDaily_CheckedChanged" />
            </td>
        </tr>
         <tr runat="server" id="trfrequency" visible="false"><td align="left">Frequency</td><td> </td><td align="left">
            <asp:DropDownList ID="ddlFrequency" runat="server"  Width ="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged">
            <asp:ListItem Value="1">Daily</asp:ListItem>
            <asp:ListItem Value="2">Monthly</asp:ListItem>
            <asp:ListItem Value="3">Annual</asp:ListItem>
            <asp:ListItem Value="0" Selected="True">Select Frequency</asp:ListItem>
            </asp:DropDownList></td></tr>
    <tr runat="server" id="tryear1" visible="false"><td align="left"><asp:RadioButton ID="rbtnFY" runat="server" GroupName="timelimit" Text="Fiscal Year" /></td><td></td><td  align="left">
        <asp:ListBox ID="ddlFY" runat="server"  Width ="120px" Rows="3" SelectionMode="Multiple"></asp:ListBox>
       </td></tr>
    <tr runat="server" id="tryear2" visible="false"><td align="left"><asp:RadioButton ID="rbtnCY" runat="server" GroupName="timelimit" Text="Calendar Year" /></td><td></td><td  align="left">
               <asp:ListBox ID="ddlCY" runat="server"  Width ="120px" Rows="3" SelectionMode="Multiple"></asp:ListBox>
       </td></tr>
   <tr runat="server" id="trdate" visible="false"><td align="left"><asp:RadioButton ID="rbtnDate" runat="server" GroupName="timelimit" Text="Begin Date : " /></td><td> </td><td>
       <asp:TextBox ID="txtBeginDate" runat="server" Width="75px"></asp:TextBox> &nbsp;&nbsp; End Date :  &nbsp;<asp:TextBox ID="txtEndDate" runat="server"  Width="75px"></asp:TextBox></td></tr>
   <tr><td align="left">Bank</td><td> </td><td align="left">
       <asp:DropDownList ID="ddlBank" runat="server" Width ="150px">
           <asp:ListItem Value="1">Chase - General</asp:ListItem>
           <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
           <asp:ListItem Value="3">HBT - SAT</asp:ListItem>
           <asp:ListItem Value="4" Selected="True">All Banks</asp:ListItem>
       </asp:DropDownList></td></tr>
       
       <tr><td align="center" colspan ="3">
           <asp:Button ID="BtnReport" runat="server" Text="Generate Report" OnClick ="BtnReport_Click" />&nbsp;<asp:Button
               ID="btnAddFiscalYear" runat="server" Visible="false" Text="Add Fiscal Year" OnClick = "btnAddFiscalYear_Click" /><br />
           <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
       </td></tr></table>
       <asp:Panel ID="PnlFYDates" Visible="false" runat="server">
        <table border ="0" cellpadding = "3" cellspacing="0" width ="350" >
        <tr><td align = "center"  colspan="3">Add Fiscal Year</td></tr>
        <tr><td align = "left" >Select Bank</td><td align = "left" ></td>
        <td align = "left" ><asp:DropDownList ID="ddlNewBank" runat="server">
    <asp:ListItem Value="1">Chase - General</asp:ListItem>
    <asp:ListItem Value="2">HBT - Spelling</asp:ListItem>
    <asp:ListItem Value="3">HBT - SAT</asp:ListItem>
    <asp:ListItem Selected="True" Value="0">Select Bank</asp:ListItem></asp:DropDownList></td></tr>
        <tr><td align = "left" >Select Fiscal Year</td><td align = "left" >
            </td><td align = "left" >
            <asp:DropDownList ID="ddlNewYear" runat="server">
            </asp:DropDownList></td></tr>
        <tr><td align = "left" >Begin Date</td><td align = "left" ></td><td align = "left" >
            <asp:TextBox ID="txtNewBeginDate" runat="server"></asp:TextBox></td></tr>
        <tr><td align = "left" >End Date</td><td align = "left" ></td><td align = "left" > <asp:TextBox ID="txtNewEndDate" runat="server"></asp:TextBox></td></tr>
        <tr><td align ="right" >
            <asp:Button ID="AddDate" runat="server" OnClick ="AddDate_Click" Text="Add Date" /></td><td></td><td align ="left" >
                <asp:Button ID="ClosePanel" OnClick="ClosePanel_Click" runat="server" Text="Close Panel" /></td></tr>
                  <tr><td align = "center"  colspan="3">
                      <asp:Label ID="lblNewErr" runat="server" ForeColor="Red"></asp:Label></td></tr>
        </table>
        </asp:Panel>
        <asp:Button ID="btnExport" runat="server" Text="Export Report"  OnClick="btnExport_Click"  Visible="false"  />
        <ASP:DATAGRID id="dgDailyReports" runat="server" OnPageIndexChanged ="dgDailyReports_PageIndexChanged" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" AllowPaging ="true"  PageSize="50">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="#EEEEEE" ForeColor="Black"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
                <COLUMNS>
                <asp:BOUNDCOLUMN DataField="BankTransID" HeaderText="BankTransID" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="BankID" HeaderText="BankID" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TransDate" HeaderText="TransDate"  DataFormatString="{0:d}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TransType" HeaderText="TransType" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TransCat" HeaderText="TransCat" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="CheckNumber" HeaderText="CheckNumber" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="DepositNumber" HeaderText="DepositNumber" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Right" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="VendCust" HeaderText="VendCust" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Reason" HeaderText="Reason" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Description" HeaderText="Description" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="AddInfo" HeaderText="AddInfo" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="DepSlipNumber" HeaderText="DepSlipNumber" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="GLAccount" HeaderText="GLAccount" >                   
                </asp:BOUNDCOLUMN>
                </COLUMNS> 
                <ItemStyle HorizontalAlign="Left" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="#DCDCDC" />
            </ASP:DATAGRID>  
             <ASP:DATAGRID id="dgReports" runat="server" OnPageIndexChanged ="dgReports_PageIndexChanged" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" 
					Height="14px" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
					BorderColor="#999999" Font-Bold="True" AllowPaging ="true"  PageSize="50">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="#EEEEEE" ForeColor="Black"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="#000084" ForeColor="White" ></HeaderStyle>
					<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
                <COLUMNS>
                <asp:BOUNDCOLUMN DataField="Year" HeaderText="Year" ItemStyle-HorizontalAlign="left"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Name" HeaderText="Date/Month" ItemStyle-HorizontalAlign="Left"  > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Donation" HeaderText="Donation"  DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Deposit" HeaderText="Deposit"  DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
<%--                <asp:BOUNDCOLUMN DataField="Credit Card" HeaderText="Credit Card"  DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
--%>                
                <asp:BOUNDCOLUMN DataField="VisaMC" HeaderText="VisaMC"  DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Amex" HeaderText="Amex"  DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Discover" HeaderText="Discover"  DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>

                <asp:BOUNDCOLUMN DataField="Interest" HeaderText="Interest"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Check" HeaderText="Check"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Shipping" HeaderText="Shipping" DataFormatString="{0:c}" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Grant" HeaderText="Grant"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Deposit Return" HeaderText="Deposit Return"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Fee" HeaderText="Fee"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Total" HeaderText="Total"   DataFormatString="{0:c}"> </asp:BOUNDCOLUMN>                                
 </COLUMNS> 
 <ItemStyle HorizontalAlign="Right" />
             <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="#DCDCDC" />
            </ASP:DATAGRID>
                      
			
			
    </center>
    </div>
   <br />
          <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
 </form>
</body>
</html>
