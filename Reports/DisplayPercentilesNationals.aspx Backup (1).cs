using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_DisplayPercentilesNationals : System.Web.UI.Page
{
    string contest;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			contest = Request.QueryString["contest"];
		
			if(contest == null)
				ShowAll();
			else ShowATable();
		}

		private void ShowAll()
		{
			// Put user code to initialize the page here
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 
			
			// create and open the connection object
			System.Data.SqlClient.SqlConnection connection =
				new System.Data.SqlClient.SqlConnection(connectionString);
			connection.Open();

			// get records from the products table
            string commandString = "Select Score, [JSB-Percentile],[JSB-Count],[JSB-TotalCount],Score,[SSB-Percentile],[SSB-Count], [SSB-TotalCount],Score,[JVB-Percentile],[JVB-Count], [JVB-TotalCount],Score,[IVB-Percentile],[IVB-Count], [IVB-TotalCount],Score,[MB1-Percentile],[MB1-Count], [MB1-TotalCount],Score,[MB2-Percentile],[MB2-Count], [MB2-TotalCount],Score,[MB3-Percentile],[MB3-Count], [MB3-TotalCount],Score,[JGB-Percentile],[JGB-Count], [JGB-TotalCount],Score,[SGB-Percentile],[SGB-Count], [SGB-TotalCount],Score,[JSC-Percentile],[JSC-Count], [JSC-TotalCount],Score,[ISC-Percentile],[ISC-Count], [ISC-TotalCount],Score,[SSC-Percentile],[SSC-Count], [SSC-TotalCount]   from PercentileRankNationals where score < 46 order by Score";			
			
			// command string and connection
			
			SqlDataAdapter daRanks = new SqlDataAdapter(commandString,connection);
			DataSet dsRanks = new DataSet();
			daRanks.Fill(dsRanks);
			
			DataGrid1.DataSource = dsRanks.Tables[0];
			DataGrid1.DataBind();
		}
		private void ShowATable()
		{
			// Put user code to initialize the page here
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(); 
			
			// create and open the connection object
			System.Data.SqlClient.SqlConnection connection =
				new System.Data.SqlClient.SqlConnection(connectionString);
			connection.Open();

			// get records from the products table
			string commandString = "Select Score, [" + contest + "-Percentile], [" + contest+"-Count], [" +
				contest+"-TotalCount] from PercentileRankNationals WHERE [" + contest + "-Percentile] IS NOT NULL";
			
			// command string and connection
			
			SqlDataAdapter daRanks = new SqlDataAdapter(commandString,connection);
			DataSet dsRanks = new DataSet();
			daRanks.Fill(dsRanks);
			if(dsRanks.Tables[0].Rows.Count <1)
			{
				Label1.Text = "Percentiles are not available because the number of contestants is less than 30.";
				DataGrid1.Visible = false;
			}
			else
			{
				DataGrid1.DataSource = dsRanks.Tables[0];
				DataGrid1.DataBind();
			}
			
			// DataGrid1.Columns[0].ItemStyle.Width = new Unit(10,UnitType.Percentage);
			// DataGrid1.Columns[1].ItemStyle.Width = new Unit(10,UnitType.Percentage); 
			 
			
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AppendHeader("content-disposition", "attachment;filename=Score Percentiles.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
            DataGrid1.RenderControl(hw);
            Response.Write(sw.ToString());
            Response.End();
        }

	}