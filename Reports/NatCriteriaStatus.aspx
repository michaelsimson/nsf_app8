<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NatCriteriaStatus.aspx.vb" Inherits="NatCriteriaStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NatCriteriaStatus</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <style type="text/css">
        <!--
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .style1 {
            font-family: Verdana;
            font-size: 30px;
            color: #FFFFFF;
        }

        .style2 {
            font-family: Arial;
            font-size: 12pt;
        }

        .style3 {
            color: #FFFFFF
        }

        a:link {
            color: #FFFFFF;
            text-decoration: none;
        }

        a:visited {
            text-decoration: none;
            color: #FFFFFF;
        }

        a:hover {
            text-decoration: underline;
            color: #999999;
        }

        a:active {
            text-decoration: none;
            color: #FFFFFF;
        }

        -->
    </style>
    <script type="text/javascript">
        var currentYear = (new Date).getFullYear();
        $(function (e) {
            $("#spnYear").text(currentYear);
        })
    </script>
</head>
<body bgcolor="#333333">
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 600px" bgcolor="#FFFFAE">
                <tr>
                    <td align="center" bgcolor="#3366CC" style="vertical-align: middle; height: 60px">
                        <span class="style1">North South Foundation</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="style2">
                        <br />
                        National Invitee Status</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Literal ID="Literal1" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td bgcolor="#3366CC" align="right" style="padding-right: 25px; padding-bottom: 3px; padding-top: 3px"><span class="style3">� 1993-<span id="spnYear"></span> North South Foundation. All worldwide rights reserved.
        <a href="/public/main/privacy.aspx" target="_blank">Copyright</a>
                    </span></td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
