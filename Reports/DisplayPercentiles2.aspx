<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DisplayPercentiles.aspx.cs" Inherits="Reports_DisplayPercentiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
		<h3 align="center" style='TEXT-ALIGN:center'>Percentile values for each Contest</h3>
		<h4>NOTE: These percentiles are based on the data available to date ONLY. As 
			additional centers report contest scores, these percentiles would change. So 
			you should periodically visit this page to get the latest results.</h4>
		<form id="Form1" method="post" runat="server">
			<asp:DataGrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 48px; POSITION: absolute; TOP: 120px"
				runat="server" Height="320px" Width="200px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
				BackColor="White" CellPadding="3" GridLines="Vertical">
				<FooterStyle ForeColor="Black" Width="10%" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle Wrap="False" Width="10%" BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle Wrap="False" ForeColor="Black" Width="10%" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" Width="10%" BackColor="#000084"></HeaderStyle>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:DataGrid>
			<asp:Label id="Label1" style="Z-INDEX: 102; LEFT: 248px; POSITION: absolute; TOP: 128px" runat="server"
				Font-Size="Large"></asp:Label>
		</form>
	</body>
</html>


 
 
 