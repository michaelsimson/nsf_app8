using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_CalculatePercentiles : System.Web.UI.Page
{
    int Year = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
    string contestType;
    DataSet dsContestants;
    DataTable dt2 = new DataTable();
    Boolean updateFlag = false;
    public void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        SavePercentilesData();
    }
    void SavePercentilesData()
    {
        string[] contType = new string[16];
        contType[0] = "JSB";
        contType[1] = "SSB";
        contType[2] = "JVB";
        contType[3] = "IVB";
        contType[4] = "SVB";
        contType[5] = "MB1";
        contType[6] = "MB2";
        contType[7] = "MB3";
        contType[8] = "MB4";
        contType[9] = "JGB";
        contType[10] = "SGB";
        contType[11] = "EW1";
        contType[12] = "EW2";
        contType[13] = "EW3";
        contType[14] = "PS1";
        contType[15] = "PS3";
        AddRecords();
        for (int i = 0; i < contType.Length; i++)
        {
            contestType = contType[i];
            CleanUpdate();
            ReadAllJSBScores();
            if (dsContestants.Tables[0].Rows.Count > 29)
                SaveData();
        }
       // LinkButton1.Visible = true;
        if (updateFlag == true)
        {
            lblMessage.Text = "Percentiles Updated for the year" + ddlYear.SelectedValue;
        }
        else
        {
            lblMessage.Text = "";//No Scores Present for the Year" + ddlYear.SelectedValue;
        }
    }
    void ReadAllJSBScores()
    {
        // connect to the peoducts database
        string connectionString =
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table

        string commandString = "Select Contestant.ChildNumber, Contestant.ContestCode, Contestant.contestant_id, " +
            "Contestant.Score1, Contestant.Score2, Contestant.Rank, Contestant.BadgeNumber,Contestant.ContestCategoryID,Contestant.ContestYear  from Contestant " +
            " Where Contestant.Score1 IS NOT NULL AND Contestant.Score2 IS NOT NULL  AND Contestant.ContestYear=" + ddlYear.SelectedValue + //  Year + 
            " AND (Contestant.Score1>0 OR Contestant.Score2>0) AND Contestant.ProductCode = '" + contestType + "' And EventId=2 ORDER BY Contestant.ChapterID";
        //AND (Contestant.ChapterID=15 OR Contestant.ChapterID=67)
        //
        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContestants = new SqlDataAdapter(commandString, connection);
        dsContestants = new DataSet();
        daContestants.Fill(dsContestants);

        if (dsContestants.Tables[0].Rows.Count > 29)
            ProcessData(dsContestants);
        else
        {
            //Response.Write("No records found!");
            DataTable dt3 = new DataTable();
            dt3.Columns.Add("No data to display");
            //DataGrid1.Caption = contestType + ": Results can not be displayed as the sample size is too small!";
            //DataGrid1.DataSource = dt3;
            //DataGrid1.DataBind();
        }
    }
    void ProcessData(DataSet ds)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Contestant", typeof(string));
        dt.Columns.Add("TotalScore", typeof(double));
        //dt.Columns.Add("ContestYear", typeof(int));

        //dt.Columns.Add("Percentile",typeof(float));
        //dt.Columns.Add("Count",typeof(int));
        double[] aryTscore = new double[ds.Tables[0].Rows.Count];
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["Contestant"] = ds.Tables[0].Rows[i].ItemArray[2].ToString();//Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]);
            dr["TotalScore"] = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[4]);
            aryTscore[i] = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[3]) +
                Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[4]);
            //dr["ContestYear"] = ds.Tables[0].Rows[i].ItemArray[8].ToString();
            dt.Rows.Add(dr);
        }

        Array.Sort(aryTscore);
        float pc;
        dt2 = new DataTable();
        dt2.Columns.Add("TotalScore", typeof(double));
        dt2.Columns.Add("Percentile", typeof(double));
        dt2.Columns.Add("Count", typeof(int));
        dt2.Columns.Add("TotalCount", typeof(int));
        dt2.Columns.Add("ContestYear", typeof(int));

        dr = dt2.NewRow();
        dr["TotalScore"] = aryTscore[0];
        int cCount = 0, tCount = 0; //cumulative
        double lastScore = aryTscore[0];
        for (int j = 0; j < aryTscore.Length; j++)
        {
            if (lastScore == aryTscore[j])
            {
                cCount++;
                tCount++;
            }
            else
            {
                lastScore = aryTscore[j];
                dr["Count"] = cCount;
                dr["TotalCount"] = tCount;
                pc = (float)tCount / aryTscore.Length;
                dr["Percentile"] = Math.Round(pc, 3);//String.Format("{0:P1}", Math.Round(pc, 3));
                dt2.Rows.Add(dr);
                dr = dt2.NewRow();
                dr["TotalScore"] = aryTscore[j];
                cCount = 1;
                tCount++;
            }
            dr["ContestYear"] = ddlYear.SelectedValue; 
        }
        dr["Count"] = cCount;
        dr["TotalCount"] = tCount;
        pc = (float)tCount / aryTscore.Length;
        dr["Percentile"] = Math.Round(pc, 3);// String.Format("{0:P1}", Math.Round(pc, 3));
      
        dt2.Rows.Add(dr);
        //DataGrid1.Caption = contestType + " as of " + DateTime.Today.ToShortDateString();
        //DataGrid1.DataSource = dt2;
        //DataGrid1.DataBind();

    }

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        // Update rows
        //string connectionString =
        //    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        //// create and open the connection object
        //System.Data.SqlClient.SqlConnection connection =
        //    new System.Data.SqlClient.SqlConnection(connectionString);
        //connection.Open();

        //// get records from the products table

        //string commandString = "UPDATE PercentileRank SET " + contestType + " = @PRV WHERE Score=@Score";
        //SqlCommand insCmd = new SqlCommand(commandString, connection);
        //insCmd.Parameters.Add("@PRV", SqlDbType.Decimal);
        //insCmd.Parameters.Add("@Score", SqlDbType.Int);
        ////DataRow dr;
        //foreach (DataRow dr in dt2.Rows)
        //{
        //    insCmd.Parameters["@PRV"].Value = dr["Percentile"];
        //    insCmd.Parameters["@Score"].Value = dr["TotalScore"];
        //    insCmd.ExecuteNonQuery();
        //}
        //connection.Close();
       
    }
    private void AddRecords()
    {
        // Just to add rows
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        //Delete everything /** Commented on 06-02-2013 to calculate and update percentiles on a yearly basis**/
        //string delCommandString = "Delete from PercentileRank Where ContestYear=" + ddlYear.SelectedValue; ;
        //SqlCommand delCommand = new SqlCommand(delCommandString, connection);
        //delCommand.ExecuteNonQuery();

        string commandStringTest = "SELECT Count(*) FROM PercentileRank  WHERE Score=@Score and ContestYear= @ContestYear";
        SqlCommand testCmd = new SqlCommand(commandStringTest, connection);
        testCmd.Parameters.Add("@Score", SqlDbType.Int);
        testCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

         //SET IDENTITY_INSERT PercentileRankNationals ON
        string commandString = "INSERT INTO PercentileRank (Score,ContestYear) VALUES (@Score,@ContestYear)";
        SqlCommand insCmd = new SqlCommand(commandString, connection);
        insCmd.Parameters.Add("@Score", SqlDbType.Int);
        insCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

            for (int i = 0; i < 50; i++)
            {
                insCmd.Parameters["@Score"].Value = i;
                insCmd.Parameters["@ContestYear"].Value = ddlYear.SelectedValue;

                testCmd.Parameters["@Score"].Value = i;
                testCmd.Parameters["@ContestYear"].Value = ddlYear.SelectedValue;

                if (Convert.ToInt32(testCmd.ExecuteScalar()) <= 0)
                {
                    insCmd.ExecuteNonQuery();
                }
            }
            connection.Close();
        }

        private void CleanUpdate()
        {
            // Just to update existing rows
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);
            connection.Open();

            string commandStringTest = "SELECT Count(*) FROM PercentileRank  WHERE Score=@Score and ContestYear= @ContestYear";
            SqlCommand testCmd = new SqlCommand(commandStringTest, connection);
            testCmd.Parameters.Add("@Score", SqlDbType.Int);
            testCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

            string commandStringUpdate = "UPDATE PercentileRank SET [" + contestType + "-Percentile] = Null, [" + contestType +
             "-Count] = Null, [" + contestType + "-TotalCount] = Null WHERE Score=@Score and ContestYear= @ContestYear";// +ddlYear.SelectedValue;
            SqlCommand updateCmd = new SqlCommand(commandStringUpdate, connection);
            updateCmd.Parameters.Add("@Score", SqlDbType.Int);
            updateCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

            //SET IDENTITY_INSERT PercentileRankNationals ON
         
            foreach (DataRow dr in dt2.Rows)
            {
                testCmd.Parameters["@Score"].Value = dr["TotalScore"];
                testCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];

                updateCmd.Parameters["@Score"].Value = dr["TotalScore"];
                updateCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];

                if (testCmd.ExecuteNonQuery() > 0)
                {
                    updateCmd.ExecuteNonQuery();
                }
               
            }
            connection.Close();
        }


        private void SaveData()
        {
            // Update rows
            string connectionString =
             System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

            // create and open the connection object
            System.Data.SqlClient.SqlConnection connection =
                new System.Data.SqlClient.SqlConnection(connectionString);
            connection.Open();
             // get records from the products table

            string commandString = "UPDATE PercentileRank SET [" + contestType + "-Percentile] = @PRV, [" + contestType +
                "-Count] = @Count, [" + contestType + "-TotalCount] = @TCount WHERE Score=@Score and ContestYear= @ContestYear";// +ddlYear.SelectedValue;
            SqlCommand insCmd = new SqlCommand(commandString, connection);
            insCmd.Parameters.Add("@PRV", SqlDbType.Decimal);
            insCmd.Parameters.Add("@Count", SqlDbType.Int);
            insCmd.Parameters.Add("@TCount", SqlDbType.Int);
            insCmd.Parameters.Add("@Score", SqlDbType.Decimal);
            insCmd.Parameters.Add("@ContestYear", SqlDbType.Int);

            //DataRow dr = dt2.NewRow();
            foreach (DataRow dr in dt2.Rows)
            {
                insCmd.Parameters["@PRV"].Value = dr["Percentile"];
                insCmd.Parameters["@Count"].Value = dr["Count"];
                insCmd.Parameters["@TCount"].Value = dr["TotalCount"];
                insCmd.Parameters["@Score"].Value = dr["TotalScore"];
                insCmd.Parameters["@ContestYear"].Value = dr["ContestYear"];
                if (insCmd.ExecuteNonQuery() > 0)
                {
                    updateFlag = true;
                }
            }
            connection.Close();
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            SavePercentilesData();
        }
        protected void ddlPercentile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPercentile.SelectedValue == "1")
            {
                lnkDispPercentile.Visible = true;
            }
            else if(ddlPercentile.SelectedValue == "2")
            {
            Response.Redirect("PercentilesByGrade.aspx");
            }

        }
}
