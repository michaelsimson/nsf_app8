using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;


public partial class Reports_MissingTechCord : System.Web.UI.Page
{
    DataSet dsContests;
    DataSet dsContests3;
    DataTable dt;
    DataTable dt1;
    DataTable dt3;
    string Year = "";
    String retValue;

    protected void Page_Load(object sender, EventArgs e)
    {
           
       if (!Page.IsPostBack)
        {

            int year = Convert.ToInt32(DateTime.Now.Year);
            ddlyear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            ddlyear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            ddlyear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
            ddlyear.SelectedIndex = 1;
            GetList();
                
        }
      
    }

    private void GetList()
    {
         DataSet dsContestDt = new DataSet();
         dsContestDt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select convert(varchar, SatDay1, 101)+ ', '+ convert(varchar, SunDay2, 101) as ContestDates,WeekID  from WeekCalendar WHERE Year(SatDay1) = " + ddlyear.SelectedValue  + ""); //
         drpContestDate.DataSource = dsContestDt;
         drpContestDate.DataBind();
         if (drpContestDate.Items.Count == 0)
         {
             lblerr.Text = "No Contest Date to show";
             return;
         }
         else
             lblerr.Text = "";
    }

      
   protected void btnExport_Click(object sender, EventArgs e)
   {
       Response.Clear();
       System.IO.StringWriter sw = new System.IO.StringWriter();
       System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
       Response.Charset = "";
       Response.ContentType = "application/vnd.xls";
       Response.AppendHeader("content-disposition", "attachment;filename=Missing_Tech_Coordinators_by_chapter" + DateTime.Now.Year.ToString() + ".xls");
       DataGrid1.RenderControl(hw);
      
       Response.Write(sw.ToString());
       Response.End();
   }
   protected void btnsubmit_Click(object sender, EventArgs e)
   {
       if (drpContestDate.Items.Count == 0)
       {
           lblerr.Text = "No Contest Date to show";
           return;
       }


       DataSet dsContestDt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select C.NSFChapterID,Ch.ChapterCode ,C.ProductGroupCode,C.ProductCode,C.ContestDate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID   where C.Contest_Year=" + ddlyear.SelectedValue.ToString() + "and C.ExamRecID is null  and C.ContestDate in ('" + drpContestDate.SelectedItem.Text.Replace(",", "','") + "') order by NSFChapterID"); //
       if (dsContestDt.Tables[0].Rows.Count == 0)
       {
           btnExport.Visible = false;
           DataGrid1.Visible = false;
           lblerr.Text = "No Record to Show";
       }
       else
       {
           DataGrid1.DataSource = dsContestDt;
           DataGrid1.DataBind();
           DataGrid1.Visible = true; 
           btnExport.Visible = true;
           lblerr.Text = "";
       }
   }
   protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
   {
       GetList();
   }
}
