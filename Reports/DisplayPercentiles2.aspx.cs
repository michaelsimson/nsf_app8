using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_DisplayPercentiles : System.Web.UI.Page
{
    string contest;
    protected void Page_Load(object sender, EventArgs e)
    {
        contest = Request.QueryString["contest"];

        if (contest == null)
            ShowAll();
        else ShowATable();
    }
    private void ShowAll()
    {
        // Put user code to initialize the page here
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table	
        string commandString = "Select Score, [JSB-Percentile],[JSB-Count],[JSB-TotalCount],Score,[SSB-Percentile],[SSB-Count], [SSB-TotalCount],Score,[JVB-Percentile],[JVB-Count], [JVB-TotalCount],Score,[IVB-Percentile],[IVB-Count], [IVB-TotalCount],Score,[SVB-Percentile],[SVB-Count], [SVB-TotalCount],Score,[MB1-Percentile],[MB1-Count], [MB1-TotalCount],Score,[MB2-Percentile],[MB2-Count], [MB2-TotalCount],Score,[MB3-Percentile],[MB3-Count], [MB3-TotalCount],Score,[JGB-Percentile],[JGB-Count], [JGB-TotalCount],Score,[SGB-Percentile],[SGB-Count], [SGB-TotalCount],Score,[EW1-Percentile],[EW1-Count], [EW1-TotalCount],Score,[EW2-Percentile],[EW2-Count], [EW2-TotalCount],Score,[EW3-Percentile],[EW3-Count], [EW3-TotalCount],Score,[PS1-Percentile],[PS1-Count], [PS1-TotalCount],Score,[PS3-Percentile],[PS3-Count], [PS3-TotalCount] from PercentileRank where score < 36 order by Score";

        // command string and connection

        SqlDataAdapter daRanks = new SqlDataAdapter(commandString, connection);
        DataSet dsRanks = new DataSet();
        daRanks.Fill(dsRanks);

        DataGrid1.DataSource = dsRanks.Tables[0];
        DataGrid1.DataBind();
    }
    private void ShowATable()
    {
        // Put user code to initialize the page here
        string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select Score, [" + contest + "-Percentile], [" + contest + "-Count], [" +
            contest + "-TotalCount] from PercentileRank  where score < 36 order by Score ";// WHERE [" + contest + "-Percentile] IS NOT NULL";

        // command string and connection

        SqlDataAdapter daRanks = new SqlDataAdapter(commandString, connection);
        DataSet dsRanks = new DataSet();
        daRanks.Fill(dsRanks);
        if (dsRanks.Tables[0].Rows.Count < 1)
        {
            Label1.Text = "Percentiles are not available because the number of contestants is less than 30.";
            DataGrid1.Visible = false;
        }
        else
        {
            DataGrid1.DataSource = dsRanks.Tables[0];
            DataGrid1.DataBind();
        }

        // DataGrid1.Columns[0].ItemStyle.Width = new Unit(10,UnitType.Percentage);
        // DataGrid1.Columns[1].ItemStyle.Width = new Unit(10,UnitType.Percentage); 


    }
}
