<%@Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="PercentilesByGrade.aspx.cs" Inherits="Reports_PercentilesByGrade" title="Percentiles By Grade"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
      <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
 
<table align="center"><tr align="center"><td align="center" colspan="3"><asp:Label ID="Label1" runat="server" Text="Percentiles by Grade"  Font-Size="Large" ForeColor="Green"></asp:Label><br /><br /></td></tr></table> 
<table width="100%" runat="server">
    <tr><td>
        <table border="0" width="90%">
            <tr align="center">
                        <td align="right" colspan="2">Select Percentiles Option</td>&nbsp;
                        <td align="left" >
			                    <asp:DropDownList ID="ddlPercentile" runat="server" AutoPostBack="true"  
                                    Height="20px" Width="264px" 
                                    onselectedindexchanged="ddlPercentile_SelectedIndexChanged"> <%--onselectedindexchanged="ddlPercentile_SelectedIndexChanged"--%>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
			                            <asp:ListItem Value="1">Calculate Percentiles By Contest</asp:ListItem>
			                            <asp:ListItem Value="2">View Percentiles By Contest</asp:ListItem>
			                    </asp:DropDownList><br /><br />
			            </td></tr>
			<tr align="center" id="trContest" runat="server" visible="true"><td>Contest Year: 
			              <asp:DropDownList ID="ddlYear" runat="server"  AutoPostBack="true" width="70px"
                                 OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"
                    Height="16px"> <%--  style="Z-INDEX: 102; LEFT: 113px; POSITION: absolute; TOP: 35px;  height: 24px; width: 55px;" --%>
			                  <%--  <asp:ListItem Value="0">Select</asp:ListItem>
			                    <asp:ListItem Value="2013">2013</asp:ListItem>
			                    <asp:ListItem Value="2012">2012</asp:ListItem>
			                    <asp:ListItem Value="2011">2011</asp:ListItem>
			                    <asp:ListItem Value="2010">2010</asp:ListItem>
			                    <asp:ListItem Value="2009">2009</asp:ListItem>
			                    <asp:ListItem Value="2008">2008</asp:ListItem>--%>
			               </asp:DropDownList>&nbsp;&nbsp;
			             </td><td>
			            Contest: &nbsp;&nbsp;<asp:dropdownlist id="ddlContest"  Visible="true"
				            runat="server" AutoPostBack="True" Height="20px" Width="85px"  DataTextField="ProductCode" DataValueField ="ProductID"
                            onselectedindexchanged="ddlContest_SelectedIndexChanged">
				            <%--<asp:ListItem Value="">Select</asp:ListItem>
				            <asp:ListItem Value="JSB">JSB</asp:ListItem>  
				            <asp:ListItem Value="SSB">SSB</asp:ListItem>
				            <asp:ListItem Value="JVB">JVB</asp:ListItem>
				            <asp:ListItem Value="IVB">IVB</asp:ListItem>
				            <asp:ListItem Value="SVB">SVB</asp:ListItem>
				            <asp:ListItem Value="MB1">MB1</asp:ListItem>
				            <asp:ListItem Value="MB2">MB2</asp:ListItem>
				            <asp:ListItem Value="MB3">MB3</asp:ListItem>
				            <asp:ListItem Value="MB4">MB4</asp:ListItem>
				            <asp:ListItem Value="JGB">JGB</asp:ListItem>
				            <asp:ListItem Value="SGB">SGB</asp:ListItem>
				            <asp:ListItem Value="EW1">EW1</asp:ListItem>
				            <asp:ListItem Value="EW2">EW2</asp:ListItem>
				            <asp:ListItem Value="EW3">EW3</asp:ListItem>
				            <asp:ListItem Value="PS1">PS1</asp:ListItem>
				            <asp:ListItem Value="PS3">PS3</asp:ListItem>
				            <asp:ListItem Value="JSC">JSC</asp:ListItem>
				            <asp:ListItem Value="ISC">ISC</asp:ListItem>
				            <asp:ListItem Value="SSC">SSC</asp:ListItem>--%>
			            </asp:dropdownlist>
			            </td><td>Contest Dates: 
			                <asp:dropdownlist id="ddlcontestdate"  AppendDataBoundItems="true" 
				                runat="server" AutoPostBack="false" Width="105px" Datatextfield="ContestDate" DataValueField="ContestPeriod"
                        onselectedindexchanged="ddlcontestdate_SelectedIndexChanged">   
		            	<%-- 	<asp:ListItem Selected="True">ALL</asp:ListItem>
				           <asp:ListItem Value="'03-31-2012','04-01-2012'">March 31-April 1</asp:ListItem>
				            <asp:ListItem Value="'04-21-2012','04-22-2012'">Apil 21-22, 2012</asp:ListItem>
				            <asp:ListItem Value="'05-12-2012','05-13-2012'"> May 12-13, 2012</asp:ListItem>
	            --%>		</asp:dropdownlist>
			              </td>
			</tr>
			<tr style=" height:35px;">
			 <td colspan="2" align="center"><br />
			 			 <asp:CheckBox ID="chkAllContest" Text="All Contests and Dates"  AutoPostBack="true" 
                     runat="server" oncheckedchanged="chkAllContest_CheckedChanged"  Visible="false"/>
			 </td>

			 <td colspan="2" align="right"><br />
			<asp:Button ID="BtnContinue" runat="server" Text="Continue" 
                     onclick="BtnContinue_Click" />
			</td>

			</tr>
           <tr><td colspan="3"><asp:Label Id="lblSuccess" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
           </table>
        </td>
        <td>
            <table><tr>
                       
                        <td><asp:Button id="btnSave" runat="server" Text="Save to Excel" onclick="btnSave_Click"></asp:Button></td>
		                <td><asp:Button id="btnExport" runat="server" Text="Cut Off Scores By Percentile" onclick="btnExport_Click"></asp:Button></td>
	                </tr>
	        </table>
        </td>
	</tr>
<tr runat="server" align="center"><td>
 <table id="tblDuplicate" runat="server" visible="false">
    <tr><td>
        <asp:Label ID ="lblduplicates" runat="server" Text="Data already exists, do you want to replace?" ForeColor="Red" Font-Bold="false" Font-Size="Medium"></asp:Label></td></tr>
	<tr><td align="center" ><br /><asp:Button ID="BtnYes" runat="server" Text="Yes" 
            onclick="BtnYes_Click" />&nbsp;
	                    <asp:Button ID="BtnNo" runat="server" Text="No" 
            onclick="BtnNo_Click" />
	                   
	
	</td></tr></table>             
	</td></tr>
	
 <tr><td colspan="3"></td></tr>
 <tr><td>
	                &nbsp;</td>
             </tr>
             
             <tr>
             <td>  <asp:datagrid id="DGPercentiles"  runat="server" Visible="true" AutoGenerateColumns="false" 
		                    Width="945px" Height="304px" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" BackColor="White"
		                    CellPadding="3" GridLines="Horizontal">
		                    <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
		                    <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
		                    <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
		                    <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
		                    <HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
		                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		                    <Columns>
		                    <asp:BoundColumn DataField="TotalScore" HeaderText="TotalScore"></asp:BoundColumn>
		                    
		                   <asp:BoundColumn DataField="Gr1%" HeaderText="Gr1%"></asp:BoundColumn>
		                   	<asp:BoundColumn DataField="Gr1_Ptile" HeaderText="Gr1_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr1_cnt" HeaderText="Gr1_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr1_cum" HeaderText="Gr1_cum"></asp:BoundColumn>
		                              
		                   <asp:BoundColumn DataField="Gr2%" HeaderText="Gr2%"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr2_Ptile" HeaderText="Gr2_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr2_cnt" HeaderText="Gr2_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr2_cum" HeaderText="Gr2_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr3%" HeaderText="Gr3%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr3_Ptile" HeaderText="Gr3_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr3_cnt" HeaderText="Gr3_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr3_cum" HeaderText="Gr3_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr4%" HeaderText="Gr4%"></asp:BoundColumn>
		                       <asp:BoundColumn DataField="Gr4_Ptile" HeaderText="Gr4_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr4_cnt" HeaderText="Gr4_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr4_cum" HeaderText="Gr4_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr5%" HeaderText="Gr5%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr5_Ptile" HeaderText="Gr5_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr5_cnt" HeaderText="Gr5_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr5_cum" HeaderText="Gr5_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr6%" HeaderText="Gr6%"></asp:BoundColumn>
		                      <asp:BoundColumn DataField="Gr6_Ptile" HeaderText="Gr6_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr6_cnt" HeaderText="Gr6_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr6_cum" HeaderText="Gr6_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr7%" HeaderText="Gr7%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr7_Ptile" HeaderText="Gr7_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr7_cnt" HeaderText="Gr7_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr7_cum" HeaderText="Gr7_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr8%" HeaderText="Gr8%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr8_Ptile" HeaderText="Gr8_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr8_cnt" HeaderText="Gr8_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr8_cum" HeaderText="Gr8_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr9%" HeaderText="Gr9%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr9_Ptile" HeaderText="Gr9_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr9_cnt" HeaderText="Gr9_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr9_cum" HeaderText="Gr9_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr10%" HeaderText="Gr10%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr10_Ptile" HeaderText="Gr10_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr10_cnt" HeaderText="Gr10_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr10_cum" HeaderText="Gr10_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr11%" HeaderText="Gr11%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr11_Ptile" HeaderText="Gr11_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr11_cnt" HeaderText="Gr11_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr11_cum" HeaderText="Gr11_cum"></asp:BoundColumn>
		                                 
		                   <asp:BoundColumn DataField="Gr12%" HeaderText="Gr12%"></asp:BoundColumn>
		                   <asp:BoundColumn DataField="Gr12_Ptile" HeaderText="Gr12_Ptile"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr12_cnt" HeaderText="Gr12_cnt"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Gr12_cum" HeaderText="Gr12_cum"></asp:BoundColumn>
		                                 
		                      <asp:BoundColumn DataField="TotalPercentile" HeaderText="TotalPercentile"></asp:BoundColumn>
		                  	 <asp:BoundColumn DataField="Total_count" HeaderText="Total_Count"></asp:BoundColumn>
		                     <asp:BoundColumn DataField="Total_Cume_Count" HeaderText="Total_Cume_Count"></asp:BoundColumn>
		                     
		                    
		                    </Columns>
		                    
		                    
		                    
		                    
		                    
		                    
		                    
		                    
		                    
		                    
		                    
		                    
	                </asp:datagrid></td></tr>
</table>
</asp:Content>

 
 
 