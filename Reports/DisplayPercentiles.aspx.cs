using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using Microsoft.ApplicationBlocks.Data;
public partial class DisplayPercentiles : System.Web.UI.Page
{
        int Year ;//= DateTime.Now.Year;//Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
        String ProductCode ;
        int ContestPeriod;
        string contestType;
        DataSet dsContestants;
        DataTable dt2 = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Year = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select distinct top 1 Contestyear from PercentilesByGrade WHERE EventID=2 order by ContestYear desc"));
                if (Request.QueryString["Contest"] == null)
                {
                    ProductCode = "JSB";
                }
                else if (Request.QueryString["Contest"] != null)
                {
                    ProductCode = Request.QueryString["Contest"];
                }
                ContestPeriod = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select distinct top 1 ContestPeriod from PercentilesByGrade WHERE ProductCode='" + ProductCode + "' and EventID=2 order by ContestPeriod desc"));
              
                ReadAllJSBScores();
            }
        }
        void ReadAllJSBScores()
        {
              // connect to the products database
                string connectionString =// Application["ConnectionString"].ToString();
              System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

                 //create and open the connection object
                System.Data.SqlClient.SqlConnection connection =
                    new System.Data.SqlClient.SqlConnection(connectionString);
                connection.Open();
 
                DataSet dsPercentiles = new DataSet();
                try
                {
                    string commandString = "Select Distinct TotalScore,TotalPercentile as " + ProductCode + "_Percentile,Total_Count as " + ProductCode + "_Count,Total_Cume_Count as " + ProductCode + "_Cume_Count From PercentilesByGrade Where ContestYear=" + Year + //  Year + 
                      " AND ProductCode = '" + ProductCode + "' and ContestPeriod in (" + ContestPeriod + " ) And EventId=2 ORDER BY TotalScore";
                    // create the command object and set its
                    //  command string and connection
                    SqlDataAdapter daPercentiles = new SqlDataAdapter(commandString, Application["ConnectionString"].ToString());
                    dsPercentiles = new DataSet();
                    daPercentiles.Fill(dsPercentiles);

                    String ProductName = Convert.ToString(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select Distinct Name from Product where ProductCode='" + ProductCode + "'"));
                    if (Year != 0)
                    {
                        DGShowPercentiles.Caption = ProductName + " as of " + Year;
                        DGShowPercentiles.Font.Bold = true;
                    }

                    if (dsPercentiles.Tables[0].Rows.Count > 0)
                    {
                        DGShowPercentiles.Style.Add("Width", "100%");
                        DGShowPercentiles.Style.Add("Height", "100%");
                        DGShowPercentiles.Visible = true;
                        // btnExport.Enabled = true;
                        //DGShowPercentiles.Caption = ddlContest.SelectedItem.Text + " as of " + ddlYear.SelectedValue;//DateTime.Today.ToShortDateString();
                        DGShowPercentiles.DataSource = dsPercentiles;
                        DGShowPercentiles.DataBind();
                    }
                    else
                    {
                        DGShowPercentiles.Visible = true;
                        // btnExport.Enabled = false;
                        DataTable dt = new DataTable();
                        dt.Columns.Add("No data to display");
                        //DGShowPercentiles.Caption = ProductName + ": Results can not be displayed as the sample size is too small!";
                      
                        DGShowPercentiles.Style.Add("Width", "50%px");
                        DGShowPercentiles.Style.Add("Height", "10%");

                        DGShowPercentiles.DataSource = dt;
                        DGShowPercentiles.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = "No Percentile Calculations done";
                }
        }
}
