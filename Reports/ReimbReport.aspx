<%@ Page Language="VB" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="ReimbReport.aspx.vb" Inherits="Reports_ReimbReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 1120px;
            max-width: 1120px;
            top: 55%;
            left: 8.1%;
            margin-left: auto;
            margin-top: -200px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            margin-right: auto;
            max-height: 400px;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>
    <script src="../Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }
        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvChapterList").fadeIn(300);
            //$("#dvChapterList").show();

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvChapterList").hide();
        }

    </script>
    <div style="text-align: left">
        <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;&nbsp;<br />
    </div>
    <div>
        <table border="0" cellpadding="0" width="900px" cellspacing="0">
            <tr>
                <td align="center">

                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Select</td>
                            <td>
                                <asp:DropDownList ID="ddlSelectType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectType_SelectedIndexChanged">

                                    <asp:ListItem Value="1">Chapter List
                                    </asp:ListItem>
                                    <asp:ListItem Value="2">Reimbursement Report

                                    </asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />
                                <asp:Button ID="btnNotImbursed" runat="server" Text="Export Table1" OnClick="btnNotImbursed_Click" Visible="false" />
                                <asp:Button ID="btnImbursed" runat="server" Text="Export Table 2" OnClick="btnImbursed_Click" Visible="false" />
                            </td>
                        </tr>
                        <tr id="trReimburse" runat="server" visible="false">
                            <td colspan="2" align="center"><b><span id="spnReImpTitle" runat="server">Reimbursement Report</span></b> </td>
                        </tr>
                        <tr id="trChapter" runat="server" visible="false">
                            <td>Chapter</td>
                            <td>
                                <asp:DropDownList ID="ddlChapter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChapter_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="trreportdate" runat="server" visible="false">
                            <td>Report Date</td>
                            <td>
                                <asp:ListBox ID="lstReports" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
                        </tr>
                        <tr id="trButtons" runat="server" visible="false">
                            <td colspan="2" align="center">
                                <asp:Button ID="btnReport" runat="server" Text="Generate Report" OnClick="btnReport_Click" Enabled="false" />
                                <asp:Button ID="btnExport" runat="server" Text="Export Expense Report" OnClick="btnExport_Click" Visible="false" />
                                <asp:Button ID="btnRevExport" runat="server" Text="Export Revenue Report" OnClick="btnExportRev_Click" Visible="false" />



                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <asp:Label ID="lblerr" runat="server"></asp:Label>
        <br />
        <div id="dvReImpSection" runat="server" visible="false">
            <asp:Label ID="lblExp" runat="server" Text="" Font-Bold="true"></asp:Label>
            <br />
            <asp:GridView CssClass="SmallFont" ForeColor="Black" ID="dgExpense" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False" GridLines="Both" CellPadding="2" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" OnPageIndexChanging="dgExpense_PageIndexChanging" AllowPaging="true" PageSize="20" PagerStyle-Mode="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small" AllowSorting="True">
                <Columns>
                    <asp:BoundField DataField="IncName" HeaderText="IncurredBy" />
                    <asp:BoundField DataField="ExpCatCode" HeaderText="ExpCatCode" />
                    <asp:BoundField DataField="TreatDesc" HeaderText="TreatDesc" />
                    <asp:BoundField DataField="ReportDate" HeaderText="ReportDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Providername" HeaderText="Vendor Name" />
                    <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval" />
                    <asp:BoundField DataField="Comments" HeaderText="Comments" />
                    <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover" />
                    <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover" />
                    <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval" />
                    <asp:BoundField DataField="Paid" HeaderText="Paid" />
                    <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="ExpCatID" HeaderText="ExpCatID" />
                    <asp:BoundField DataField="Account" HeaderText="Account" />
                    <asp:BoundField DataField="TreatID" HeaderText="TreatID" />
                    <asp:BoundField DataField="ReimbMemberID" HeaderText="ReMemberID" />
                    <asp:BoundField DataField="EventName" HeaderText="Event" />
                    <asp:BoundField DataField="FromChapter" HeaderText="From Chapter" />
                    <asp:BoundField DataField="ToChapter" HeaderText="ToChapter" />
                    <asp:BoundField DataField="ReimburseTo" HeaderText="ReimburseTo" />
                    <asp:BoundField DataField="TreatCode" HeaderText="TreatCode" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:Label ID="lblRev" runat="server" Text="" Font-Bold="true"></asp:Label>

            <asp:GridView ID="DGRevenue" CssClass="SmallFont" ForeColor="Black" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" OnPageIndexChanging="DGRevenue_PageIndexChanging" AllowPaging="true" PageSize="20" PagerStyle-Mode="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small" AllowSorting="True">
                <Columns>
                    <asp:BoundField DataField="RevSource" HeaderText="RevSource" />
                    <asp:BoundField DataField="RevCatCode" HeaderText="From" />
                    <asp:BoundField DataField="SponsorCode" HeaderText="Given For" />
                    <asp:BoundField DataField="ReportDate" HeaderText="ReportDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="DonPurposeCode" HeaderText="DonPurpose" />
                    <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="FeesType" HeaderText="Fee Type" />
                    <asp:BoundField DataField="Comments" HeaderText="Comments" />
                    <asp:BoundField DataField="ProviderName" HeaderText="Giver's Name" />
                    <asp:BoundField DataField="SalesCatCode" HeaderText="SalesCat" />
                    <asp:BoundField DataField="PaymentMethod" HeaderText="Payment Method" />
                    <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval" />
                    <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover" />
                    <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval" />
                    <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover" />
                    <asp:BoundField DataField="Account" HeaderText="Account" />
                    <asp:BoundField DataField="EventName" HeaderText="Event" />
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>

    <div id="dvGrdChapterListSection" runat="server">
        <div align="center"><span style="font-weight: bold;" id="spnNotImpChapterList" runat="server"></span></div>
        <div style="clear: both;"></div>
        <div align="center"><span style="font-weight: bold; color: blue;" id="spnMailStatus" runat="server"></span></div>
          <div align="center"><span style="font-weight: bold; color: red;" id="spnResult" runat="server"></span></div>
        <asp:GridView ID="grdNotImpChapterList" runat="server" AutoGenerateColumns="False" EnableViewState="true" HorizontalAlign="Center" Width="85%" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdNotImpChapterList_RowCommand" OnPageIndexChanging="grdNotImpChapterList_PageIndexChanging" AllowPaging="true" PageSize="20">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        Action
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                        <div style="display: none">
                            <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChapterID")%>'></asp:Label>
                            <asp:Label ID="lblReportDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDate", "{0:yyyy/MM/dd}")%>'></asp:Label>

                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        Email
                    </HeaderTemplate>
                    <ItemTemplate>

                        <asp:Button ID="btnSendEMail" runat="server" CommandName="Send" Text="Send" />
                        <div style="display: none">
                            <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChapterID")%>'></asp:Label>
                            <asp:Label ID="lblRptDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                            <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label>
                            <asp:Label ID="lblChapterCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChapterCode")%>'></asp:Label>
                            <asp:Label ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Label>
                            <asp:Label ID="lblMemebrID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AutoMemberID")%>'></asp:Label>
                            <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EventID")%>'></asp:Label>

                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ChapterCode" Visible="True" HeaderText="ChapterCode"></asp:BoundField>
                <asp:BoundField DataField="ReportDate" Visible="true" HeaderText="Report Date" DataFormatString="{0:d}"></asp:BoundField>
                <asp:BoundField DataField="Email" Visible="True" HeaderText="Email Address"></asp:BoundField>
                <asp:BoundField DataField="HPhone" Visible="True" HeaderText="Home Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" Visible="True" HeaderText="Cell Phone"></asp:BoundField>
                <asp:BoundField DataField="Name" Visible="True" HeaderText="Name"></asp:BoundField>

            </Columns>
        </asp:GridView>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center"><span style="font-weight: bold;" id="spnImpChapterList" runat="server"></span></div>
        <div style="clear: both;"></div>
        <div align="center"><span style="font-weight: bold; color: red;" id="spnReimbResult" runat="server"></span></div>
        <div style="clear: both;"></div>
        <asp:GridView ID="grdImpChapterList" runat="server" AutoGenerateColumns="False" EnableViewState="true" HorizontalAlign="Center" Width="1100px" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdImpChapterList_RowCommand" OnPageIndexChanging="grdImpChapterList_PageIndexChanging" AllowPaging="true" PageSize="20">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        Action
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                        <div style="display: none">
                            <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChapterID")%>'></asp:Label>
                            <asp:Label ID="lblReportDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDate", "{0:yyyy/MM/dd}")%> '></asp:Label>
                            <asp:Label ID="lblBankID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "BankID")%> '></asp:Label>
                            <asp:Label ID="lblCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CheckNumber")%> '></asp:Label>
                            <asp:Label ID="lblReimbMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReimbMemberID")%> '></asp:Label>

                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ChapterCode" Visible="True" HeaderText="ChapterCode"></asp:BoundField>
                <asp:BoundField DataField="ReportDate" Visible="true" HeaderText="Report Date" DataFormatString="{0:d}"></asp:BoundField>
                <asp:BoundField DataField="BankID" Visible="True" HeaderText="BankID"></asp:BoundField>
                <asp:BoundField DataField="BankCode" Visible="True" HeaderText="Bank Code"></asp:BoundField>
                <asp:BoundField DataField="CheckNumber" Visible="True" HeaderText="Check Number"></asp:BoundField>
                <asp:BoundField DataField="Email" Visible="True" HeaderText="Email Address"></asp:BoundField>
                <asp:BoundField DataField="HPhone" Visible="True" HeaderText="Home Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" Visible="True" HeaderText="Cell Phone"></asp:BoundField>
                <asp:BoundField DataField="Name" Visible="True" HeaderText="Name"></asp:BoundField>


            </Columns>
        </asp:GridView>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dvChapterList" class="web_dialog">
        <div id="dvReImpNotProcessed" runat="server" style="width: 1100px; margin-left: 10px; margin-bottom: 10px; margin-top: 10px; height: 350px; overflow: scroll;">
            <div align="center"><span style="font-weight: bold;" id="spnReImpNotProcessed" runat="server"></span></div>
            <div style="float: left; position: relative;"><span id="spnAmount" runat="server" style="font-weight: bold;"></span></div>
            <div style="clear: both;"></div>

            <div style="clear: both; margin-bottom: 10px;" id="dvReimpNotErrorStatus" align="center" runat="server" visible="false">
                <span id="Span1" runat="server" style="color: red;">No record found for selected report Date</span>
            </div>
            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdReImpNotProcessed" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
                <Columns>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="IncName" HeaderText="IncurredBy" />
                    <asp:BoundField DataField="ExpCatCode" HeaderText="ExpCatCode" />
                    <asp:BoundField DataField="TreatDesc" HeaderText="TreatDesc" />
                    <asp:BoundField DataField="ReportDate" HeaderText="ReportDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:n}" />
                    <asp:BoundField DataField="Providername" HeaderText="Vendor Name" />
                    <%--   <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval" />--%>
                    <asp:BoundField DataField="Comments" HeaderText="Comments" />
                    <%--   <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover" />--%>
                    <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover" />
                    <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval" />
                    <asp:BoundField DataField="Paid" HeaderText="Paid" />
                    <asp:BoundField DataField="BankID" HeaderText="BankID" />
                    <asp:BoundField DataField="BankCode" HeaderText="BankCode" />
                    <asp:BoundField DataField="CheckNumber" HeaderText="Check#" />
                    <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="ExpCatID" HeaderText="ExpCatID" />
                    <asp:BoundField DataField="Account" HeaderText="Account" />
                    <asp:BoundField DataField="TreatID" HeaderText="TreatID" />
                    <asp:BoundField DataField="ReimbMemberID" HeaderText="ReMemberID" />
                    <asp:BoundField DataField="EventName" HeaderText="Event" />
                    <asp:BoundField DataField="FromChapter" HeaderText="From Chapter" />
                    <asp:BoundField DataField="ToChapter" HeaderText="ToChapter" />
                    <asp:BoundField DataField="ReimburseTo" HeaderText="ReimburseTo" />
                    <asp:BoundField DataField="TreatCode" HeaderText="TreatCode" />
                </Columns>
                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>

        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <input type="button" id="btnClose" onclick="HideDialog()" value="Close" style="margin-bottom: 10px;" />
        </center>
    </div>

    <br />
    <input type="hidden" runat="server" id="hdnChapterID" />
    <input type="hidden" runat="server" id="hdnReportDate" />
    <input type="hidden" runat="server" id="hdnBankID" />
    <input type="hidden" runat="server" id="hdnCheckNumber" />
    <input type="hidden" runat="server" id="hdnReimbMemberID" />
    <input type="hidden" runat="server" id="hdnEmail" />
    <table border="0" cellpadding="2" cellspacing="0" runat="server" visible="false">
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
            </td>
            <td width="10px"></td>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

