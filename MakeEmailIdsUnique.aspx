<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="MakeEmailIdsUnique.aspx.vb" Inherits="VRegistration.MakeEmailIdsUnique"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <asp:hyperlink id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
    &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink><table border="0" cellpadding="0" style="width:100%">
      <tr><td align="left">
        <table width="1000" cellpadding="0" cellspacing ="0" border="0">
			<tr><td class="Heading" alignMaking Email Unique between Spouses </b></td>
			</tr>
			<tr><td align="center">
                <asp:Panel ID="PnlNew" runat="server">
                    <table cellpadding = "4" cellspacing = "0" border="0" style ="width:720px">
                        <tr><td  align="center" colspan = "3">Note: A volunteer should have a unique email address, separate from the spouse.<br /><br /></td></tr>		
				        <tr><td align="left" style ="width:120px;font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900">Current</td>
				        <td align="left" style ="width:300px;font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spouse 1</td><td align="left" style ="width:300px;font-family: Georgia,serif;font-size: 16px; font-weight:bold; color:#009900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spouse 2</td></tr>						
				        <tr><td align="left">Name</td><td align="left"><asp:Label ID="lblIndName" runat="server" ></asp:Label></td><td  align="left"><asp:Label ID="lblIndSpName" runat="server" ></asp:Label> </td></tr>	
				        <tr><td align="left">Email </td><td align="left"><asp:Label ID="lblIndEmail" runat="server" ></asp:Label> </td><td  align="left"><asp:Label ID="LblIndSpEmail" runat="server" ></asp:Label> </td></tr>	
				         <tr id="TrChange" runat="server" visible="true"><td align="left">Change </td><td align="left">
				                <asp:DropDownList ID="ddlChange" AutoPostBack="true" runat="server" >
				                     <asp:ListItem Value="0" Text="No">No</asp:ListItem>
				                     <asp:ListItem Value="1" Text="Yes">Yes</asp:ListItem>
    				            </asp:DropDownList> </td><td  align="left">
    				             
    				            <asp:DropDownList ID="ddlChange1" AutoPostBack="true" runat="server" >
				                     <asp:ListItem Value="0" Text="No">No</asp:ListItem>
				                     <asp:ListItem Value="1" Text="Yes">Yes</asp:ListItem>
    				            </asp:DropDownList> </td><td  align="left">	</td></tr>
				       
				        <tr><th align="left" colspan ="3" style ="font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900"> To make it unique, replace with:</th></tr>
				          	
                        <tr><td align="left">New Email </td><td align="left"> 
                            <asp:TextBox ID="txtIndEmail" runat="server" Enabled="false" Width="175px"></asp:TextBox> <br />
                                <asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
                                    ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtIndEmail"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </td><td  align="left"><asp:TextBox ID="txtIndSpEmail" runat="server" Enabled="false" Width="175px"></asp:TextBox><br />
                                     <asp:RegularExpressionValidator
                                ID="RegularExpressionValidator2" runat="server" CssClass="smFont" Display="Dynamic"
                                ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtIndSpEmail"
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                        </tr>
				        <tr><td align="left">Confirm New Email </td><td align="left">
				                <asp:TextBox ID="txtIndCEmail" runat="server" Enabled="false" Width="175px"></asp:TextBox><br />
				                    <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndEmail"
                                            ControlToValidate="txtIndCEmail" ErrorMessage="ReType New Email Incorrect"></asp:CompareValidator></td><td style="width: 162px" align="left">
                                <asp:TextBox ID="txtIndSpCEmail" runat="server" Enabled="false" Width="175px"></asp:TextBox><br />
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndSpEmail"
                                            ControlToValidate="txtIndSpCEmail" ErrorMessage="ReType New Email Incorrect"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr><td align="left">Password </td><td align="left"><asp:TextBox ID="txtIndPwd" runat="server" TextMode="Password" Enabled="false" Width="175px"></asp:TextBox><br />
				              </td><td style="width: 162px" align="left"><asp:TextBox ID="txtIndSpPwd" runat="server" TextMode="Password" Enabled="false" Width="175px"></asp:TextBox><br />
				                </td></tr>
                        <tr id="TrConfirmPwd" runat="server" visible="True"><td align="left">ConfirmPassword </td><td align="left"><asp:TextBox ID="txtIndCPwd" runat="server" TextMode="Password" Enabled="false" Width="175px"></asp:TextBox><br />
                             <asp:CompareValidator ID="CompareValidator5" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndPwd"
                                            ControlToValidate="txtIndCPwd" ErrorMessage="ReType Password Incorrect"></asp:CompareValidator>  
                             </td><td style="width: 162px" align="left"><asp:TextBox ID="txtIndCSpPwd" runat="server" TextMode="Password" Enabled="false" Width="175px"></asp:TextBox><br /> 
                             <asp:CompareValidator ID="CompareValidator6" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndSpPwd"
                                            ControlToValidate="txtIndCSpPwd" ErrorMessage="ReType Password Incorrect"></asp:CompareValidator></td><td style="width: 162px" align="left">  
                              </td>
                        </tr>  
                          <tr><td align="center" colspan="3"><asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td></tr>
				        <tr><td align="center" colspan="3" align="center"><asp:Label ID="lblSuccess" runat="server" ForeColor="Red" Text=""></asp:Label></td></tr>
				        <tr><td colspan="3" align="center" style="height: 32px"><asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></td></tr>
                    </table>
                </asp:Panel>
             </td></tr>
             <tr><td align="center">
                <table width="500" border="0" cellspacing="0" cellpadding="0">
                    <tr runat="Server" id="trContinue"  visible="false"><td align="left"><p align="justify"  style="font-size:13px; font-family:Georgia, Times, serif; ">The email address you gave already exists in multiple places.  
                        Please mail to nsfcontests@gmail.com for investigation. </p></td></tr>
                    <tr id="Tr1" runat="Server"><td><center><asp:Button ID="btncontinue" runat="server" Text="Continue" Visible ="false" /></center></td></tr>
             </table>
            </td></tr>
        </table></td></tr>
        <tr id="TrSwapEmails" align="center" runat="server" visible="False"><td>
            <table id="tblSwapEmails" runat="server" visible="true" >
                <tr><td><asp:Label ID="lblSwap" Text="Do you want to swap emails between Spouse 1 and Spouse 2?" Font-Bold="true"  Font-Size= "Medium" runat="server"></asp:Label></td></tr>
                <tr align="center"><td><asp:RadioButton ID="radYes" runat="server" AutoPostBack="true" Checked="false" />Yes</td></tr>
                <tr align="center"><td><asp:RadioButton ID="radNo" runat="server" AutoPostBack="true" Checked="false"/>  No</td></tr> 
            </table>
        </td></tr>
        <tr align="center"><td> 
        <asp:Label ID="lblSwapMsg" runat="server" Text="" ForeColor="Red"></asp:Label> 
        </td></tr>
  </table>
</asp:Content>
 

 
 
 
