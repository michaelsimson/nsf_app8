﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VolChangeEmail.aspx.vb" Inherits="VolChangeEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>NSF-Volunteer Change Email</title>
    <script language="javascript" type="text/javascript" >
        function closewindow()
		{			
			//window.opener.location.reload();
			window.close();
			return false;
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <a href="#" onclick="return closewindow();"> click here to close this</a>
    <div align="center">
        <table ID="tblChange" runat="server"><tr><td>
            <asp:Label runat="server" ID="lblMessage"></asp:Label> <br /></td></tr>
            <tr id="trChange" runat="server" align ="center"><td>
       <asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click" /> &nbsp;&nbsp;    <input type = "button" value="No" onclick="return closewindow();" />
       </td></tr></table>
    </div>
    </form>
</body>
</html>