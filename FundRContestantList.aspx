﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="FundRContestantList.aspx.cs" Inherits="FundRContestantList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Contestant List         
        <br />
        <br />
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center" style="width: 1100px;">
        <tr>

            <td align="left" nowrap="nowrap" style="font-weight: bold;">Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 140px;">
                <asp:DropDownList ID="DDlYear" runat="server" Width="100px">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="2016">2016</asp:ListItem>
                    <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                    <asp:ListItem Value="2014">2014</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Event&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="ddEvent" Enabled="false" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="9" Selected="True">FundRD</asp:ListItem>

                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Check-in List&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="DDlCheckInList" runat="server">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">List Type&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="DDLListType" runat="server">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="Paid">Paid</asp:ListItem>
                    <asp:ListItem Value="Pending">Pending</asp:ListItem>

                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Product Group&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="ddlProductGroup" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Product&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="ddlProduct" runat="server">
                </asp:DropDownList>
                <div style="margin-bottom: 10px;"></div>
            </td>


        </tr>
        <tr>
            <td colspan="13" align="center">
                <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" />
            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <asp:Label ID="LblErrMsg" runat="server" ForeColor="red"></asp:Label>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">

        <center>
            <b>Table 1: Contestant List</b>
        </center>
        <div style="clear: both;"></div>
        <div align="left" id="dvExcel" runat="server" visible="false">
            <asp:Button ID="BtnExcel" runat="server"
                Text="Export To Excel" OnClick="BtnExcel_Click" />
        </div>
        <div style="clear: both;"></div>
        <center>

            <asp:Literal ID="LtrContestantList" runat="server"></asp:Literal>
        </center>
    </div>

</asp:Content>
