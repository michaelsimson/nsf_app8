<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>



<!--#include file="scholarships_home_header.aspx"-->



<div class="txt01_strong" align="justify">
<center><b>Table 1: Designated Scholarships for College</b></center>
</div>
<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr bgcolor="#42c01e">
        <td width="45%" height="24" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Donor</td>
		<td width="5%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Number</td>
        <td width="10%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Tenure</td>
        <td width="40%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Geographic Region</td>
		
    </tr>
	
<%
    Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
    
    cn.Open()
    Try
       
        strquery = "SELECT D.Donortype,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') END as DonorName,D.Number,D.Tenure,D.GeographicRegion From DonorDesig D Left Join IndSpouse I on (I.AutoMemberID =D.MemberID  AND D.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = D.MemberID AND D.DonorType = 'OWN') where D.Designation='College' order by I.lastname,I.Firstname,O.ORGANIZATION_NAME"
        
    Catch ex As Exception
    
    End Try
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
    While rs.Read()
%>
	<tr>
		<td bgcolor="#FFFFFF" ><%=rs(1)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs(2)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs(3)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs(4)%></td>
	
	</tr>
<%
	End While
	cn.Close()
%>
</table>
<div class="txt01_strong" align="justify">
<center><b>Table 2: Designated Scholarships for High School</b></center>
</div>
<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr bgcolor="#42c01e">
        <td width="45%" height="24" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Donor</td>
		<td width="5%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center" >Number</td>
        <td width="10%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Tenure</td>
        <td width="40%" class="btn_06" style=" font-family: Arial; color:#FFFFFF; font-size:10pt; font-weight:bold" align="center">Geographic Region</td>
		
    </tr>
	
<%
    Dim cn1 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd1 As SqlCommand
    Dim rs1 As SqlDataReader
    Dim strquery1 As String
    
    cn1.Open()
    Try
       
        strquery1 = "SELECT D.Donortype,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') END as DonorName,D.Number,D.Tenure,D.GeographicRegion From DonorDesig D Left Join IndSpouse I on (I.AutoMemberID =D.MemberID  AND D.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = D.MemberID AND D.DonorType = 'OWN') where D.Designation='HighSchool' order by I.lastname,I.Firstname,O.ORGANIZATION_NAME"
        
    Catch ex As Exception
       
    End Try
    cmd1 = New SqlCommand(strquery1, cn1)
    rs1 = cmd1.ExecuteReader()
    While rs1.Read()
%>
	<tr>
		<td bgcolor="#FFFFFF" ><%=rs1(1)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs1(2)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs1(3)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs1(4)%></td>
	
	</tr>
<%
	End While
cn1.Close()
%>
</table>

	
<!--#include file="scholarships_home_footer.aspx"-->
