﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Ionic.Zip;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("~/zipfolder"));
            FileInfo[] filesInFolder = directory.GetFiles("*.*", SearchOption.AllDirectories);
            foreach (FileInfo fileInfo in filesInFolder)
            {
                checkBoxList.Items.Add(fileInfo.Name);
            }
        }

    }
    protected void btnDownloadZipFile_Click(object sender, EventArgs e)
    {
        Ionic.Zip.ZipFile multipleFilesAsZipFile = new Ionic.Zip.ZipFile();
        Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedZipFile.zip");
        Response.ContentType = "application/zip";
        foreach (ListItem fileName in checkBoxList.Items)
        {
            if (fileName.Selected)
            {
                string filePath = Server.MapPath("~/zipfolder/" + fileName.Value);
                multipleFilesAsZipFile.AddFile(filePath,string.Empty);
            }
        }
        multipleFilesAsZipFile.Save(Response.OutputStream); 
    }
}
