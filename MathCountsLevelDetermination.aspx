﻿<!--#include file="../uscontests_header.aspx"-->
<script type="text/javascript">
    document.getElementById("dvMathsCoaching").style.display = "block";

</script>
<div class="title02">NSF Mathcounts Requirements</div>

<div align="justify" class="txt01">



    <p>
        Students should be in 6th, 7th or 8th grade in order to be in NSF Mathcounts coaching. Please note, this is a hard requirement. Please don’t ask for exceptions. There are three different levels Beginner, Intermediate and Advanced. There is guidance and sample questions are given for each level. Your are responsible for evaluating and picking the right level for your child. Once registered, you will likely not able to change the level or timings as the space available is limited. You should be able to see the availabiliy when you login to northsouth.org (“Register for Coaching” from the Parent Functions page). If there is no space available, sorry we can’t help. It is first come, first serve, so it is important that you register early to ensure a space for your child. 
    </p>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvBeginner">
        <div class="title02">NSF Mathcounts Coaching - Beginner Level</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <b>Goal: </b>Introduce MATHCOUNTS problems, teach underlying concepts of the problems.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet the following criteria.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Students must be in 6th grade or higher.</li>
            <li>Please find the sample problems attached. Beginner level starts with this level of complexity. The complexity increases as the class progresses.</li>
        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Parents: </b>You are the best judge for your child. Please ask your child to do the sample problems given for this level and register if this is the right level.
        <div style="clear: both; margin-bottom: 30px;"></div>
        <img src="/public/images/MathWarmUp1.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWarmUp2.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut1.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut2.png" />

    </div>
    <div style="clear: both; margin-bottom: 20px; border: 1px solid; width: 100%;"></div>


    <div id="dvIntermediate">
        <div class="title02">NSF Mathcounts Coaching - Intermediate Level</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <b>Goal: </b>To help students be successful at MATHCOUNTS Chapter level.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet one of the first two criteria and the third one. 
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Students must be at least in Algebra 1 at school.</li>
            <li>Students who participated in NSF Mathcounts coaching Beginner Level in the past.</li>
            <li>Should be able to complete Warm-Up 5, 6 and Workout 3 (given below) in 45 minutes and should score 80%. If they either take longer time to finish or score less than 80%, they are not ready for this level. </li>
        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Parents: </b>You are the best judge for your child. Even though if your child does not meet above criteria and if you believe that your child is mature and has the skills to be in intermediate level, you can register them. The pace of the class will not slow down if your child can’t keep up. The basics of what is expected to know in intermediate level will not be covered in the class. Secondly and most important if your child struggles that will severely hurt their self-esteem. Please keep this in mind when you register.

         <div style="clear: both; margin-bottom: 30px;"></div>
        <img src="/public/images/MathWarmUp3.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWarmUp4.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut3.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut4.png" />

    </div>

    <div style="clear: both; margin-bottom: 20px; border: 1px solid; width: 100%;"></div>

    <div id="dvAdvanced">
        <div class="title02">NSF Mathcounts Coaching - Advanced Level</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <b>Goal: </b>To help/motivate the students to be successful in State and National round. Speed is an important factor to be successful in MATHCOUNTS. Challenge the kids with faster pace. Students are expected to know most concepts.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet one of the first two criteria and the third one.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Students must be at least in Geometry level at school.</li>
            <li>Qualified for previous year MATHCOUNTS State round.</li>
            <li>Should be able to complete Warm-Up 11, 12 and Workout 6 (given below) in 45 minutes and should score 80% or above. If they either take longer time to finish or score less than 80%, they are not ready for this level. </li>
        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Parents: </b>You are the best judge for your child. Even though your child does not meet above criteria and if you believe that your child is mature enough and has the skills to be in the Advanced class, you can register them. The pace of the class will not slow down if your child can’t keep up. Focus here is not to teach basics but teaching alternate methods and ways to solve problems in a faster way. Secondly and most important if your child struggles at this level, that will severely hurt their self-esteem. Please keep this in mind when you register.

          <div style="clear: both; margin-bottom: 30px;"></div>
        <img src="/public/images/MathWarmUp5.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWarmUp6.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut5.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>
        <img src="/public/images/MathWorkOut6.png" />

    </div>
</div>

<!--#include file="../uscontests_footer.aspx"-->
