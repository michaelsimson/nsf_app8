<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewContestTeam.aspx.vb" Inherits="ViewContestTeam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<div align="left">
        <asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>

<table id="MainTable" runat="server" align="left" border="0">
      <tr><td align="center">Contest Team Schedule </td></tr> 
      <tr><td>  
            <table border="1" runat="server" align="left"><tr>  
                      <td align="left" width="100px">ContestYear </td>
                          <td> <asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px"> Event</td>
                          <td><asp:DropDownList ID="ddlEvent" DataTextField="Name" DataValueField="EventId" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px">Chapter</td>
                          <td><asp:DropDownList ID="ddlChapter" DataTextField="ChapterCode" DataValueField="ChapterId" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px">Purpose</td>
                          <td><asp:DropDownList ID="ddlPurpose" DataTextField="Purpose" DataValueField="Purpose" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      </tr>
            </table> 
      </td></tr>
      <tr><td> 
            <table border="0" runat="server" align="center"><tr>
               <td>
                 <table border="1" runat="server">
                   <tr id="TrDay" runat="server">   
                           <td align="left" width="100px"> Date </td><td> 
                           <asp:DropDownList ID="ddlDate" DataTextField="Date" DataValueField="Date" runat="server" Width="125px" visible="true"> </asp:DropDownList></td>
                    </tr><tr id="TrProductGroup" runat="server">
                           <td align="left" width="100px">ProductGroup </td><td>
                           <asp:DropDownList ID="ddlProductGroup" DataTextField="ProductGroupCode" DataValueField="ProductGroupId"  AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td> 
                    </tr><tr id="TrProduct" runat="server"> 
                           <td width="100px"> Product</td><td> 
                           <asp:DropDownList ID="ddlProduct"  DataTextField="ProductCode" DataValueField="ProductId" AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td>
                    </tr><tr id ="TrPhase" runat="server"> 
                            <td width="100px">Phase</td><td> 
                            <asp:DropDownList ID="ddlPhase"  AutoPostBack="true" DataTextField="Phase" DataValueField="Phase" runat="server" width="125px">
                                  <%--  <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                                    <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                                    <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                                    <asp:ListItem Text="3" Value="3">3</asp:ListItem>--%>
                            </asp:DropDownList> </td> 
                    </tr><tr id="TrBldg" runat="server"><td>BldgName</td>
                           <td><asp:DropDownList ID="ddlBldgID" runat="server" AutoPostBack="true" DataTextField="BldgID" DataValueField="BldgID" visible="true" Width="125px"></asp:DropDownList></td>
                    </tr><tr id="TrRoomNo" runat="server"><td>RoomNumber</td>
                           <td><asp:DropDownList ID="ddlRoomNo" runat="server" AutoPostBack="true" DataTextField="RoomNumber" DataValueField="RoomNumber" Enabled="false" visible="true" Width="125px"></asp:DropDownList></td>
                    </tr>
                 </table>
              </td>
              <td align="center" runat="server">
                  &nbsp;</td></tr>
         </table> 
   </td></tr>
   <tr align="center"><td align="center"> 
           <asp:Button ID="btnView" runat="server" Width="100px" Text="View Details"> </asp:Button>
           <asp:Button ID="BtnCancel"  OnClick="BtnCancel_Click" runat="server"  Text="Cancel" />
   </td></tr>
   <tr align="center"><td><asp:Label ID="lblShowContest" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr> 
   </table>

<table align="left"><tr><td>
 <asp:DataGrid ID="DGRoomSchedule" runat="server" DataKeyField="RoomSchID" AutoGenerateColumns="False" CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
                 <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                        <ItemStyle BackColor="White" />
     <COLUMNS>
         <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Chapter"  HeaderText="Chapter" />
           <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear"/>
             <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Event" HeaderText="Event"/>
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID"/>
                <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="SeqNo" HeaderText="SequenceNo"/>
                  <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="RoomNumber" HeaderText="RoomNumber"/>
            <asp:TemplateColumn HeaderText="Capacity"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblCapacity" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Capacity") %>'></asp:Label>
		            </ItemTemplate>           
            </asp:TemplateColumn>        
         <%--<asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>--%>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Date" DataFormatString="{0:d}" HeaderText="Date"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"/>
                <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"/>
                  <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase" HeaderText="Phase"/>
           <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="StartTime" HeaderText="StartTime" />
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndTime" HeaderText="EndTime"/>
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="StartBadgeNo" HeaderText="StartBadgeNo"/>
                 <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndBadgeNo" HeaderText="EndBadgeNo"/>
                                 
                <asp:TemplateColumn HeaderText="RoomMc"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomMc" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="Pronouncer"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblPronouncer" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="ChiefJudge"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblChiefJudge" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="AssociateJudge"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblAssociateJudge" runat="server"></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="LaptopJudge"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblLaptopJudge" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="DictHandler"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblDictHandler" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="Grading"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrading" runat="server"></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 <asp:TemplateColumn HeaderText="Proctor"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblProctor" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Timer"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblTimer" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="RoomGuide_1"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_1" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="RoomGuide_2"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_2" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 
                  <asp:TemplateColumn HeaderText="RoomGuide_3"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_3" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_4"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_4" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_5"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_5" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_6"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_6" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_7"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_7" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_8"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_8" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_9"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_9" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="RoomGuide_10"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblRoomG_10" runat="server" ></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 
                 
                 
                 
                 
                 
                 
                 
                                                                     
     </COLUMNS>           
               <HeaderStyle BackColor="White" />
 </asp:DataGrid>
   
    </td></tr></table>
</asp:Content>

