<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddupEvent.aspx.vb" Inherits="AddupEvent" title="Add/Update Event" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
  <asp:hyperlink id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink><br />
  <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004px"><tr><td align = "center"> 
         <center>
    <table cellpadding = "4" cellspacing = "0"  border="0" width="650px">
        <tr>
            <td align="center" colspan="4">
                <h2>
                ADD/UPDATE EVENT</h2>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
            <td align="center" Width = " 200px">&nbsp;
            </td>
        </tr>
    <tr><td align = "left">Event Year</td><td></td><td align="left"><asp:DropDownList ID="ddlyear" runat="server" Width="155px" Height="23px">
    </asp:DropDownList></td>
        <td align="Center">
            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Text="Update Year(For all Events)" /></td>
    </tr>    
        <tr>
            <td align="left">
                Event Code</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtEveCode" runat="server" Width="150px" Height="18px"></asp:TextBox>
                 <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Red" Text=" * Use Unique Code"></asp:Label>
                </td>
            <td align="Right">
                <asp:Label ID="lblold" runat="server" Text="Old Year :" Visible="False"></asp:Label>
                <asp:TextBox ID="txtOld" runat="server" Visible="False" Width="60px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                Event Name</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtEveName" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
            <td align="right">
                <asp:Label ID="lblnew" runat="server" Text="New Year :" Visible="False"></asp:Label>
                <asp:TextBox ID="txtNew" runat="server" Visible="False" Width="60px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                Status</td>
            <td>
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlStatus" runat="server" Width="155px" Height="23px">
                    <asp:ListItem Value="O">Open</asp:ListItem>
                    <asp:ListItem Value="N">Not Open</asp:ListItem>
                    <asp:ListItem>Select Status</asp:ListItem>
                </asp:DropDownList></td>
            <td align="Center">
             <asp:Button ID="btnUpYear" runat="server" Text="Update Year" Visible="False" />
            </td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
            </td>
            <td align="center" colspan="1">
                <asp:Label ID="lblyear" runat="server" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="panel3"   Visible="False">

    <center> <asp:Label ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>    </center>
     <br />
     <br />

    <asp:GridView ID="GridView1" runat="server" DataKeyNames="EventId" AutoGenerateColumns="False"  OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="EventId"  HeaderText="EventID" />
            <asp:BoundField DataField="EventCode" HeaderText="Event Code"  />
            <asp:BoundField DataField="Name" HeaderText="Event Name"   ItemStyle-HorizontalAlign="Left"    />
            <asp:BoundField DataField="EventYear" HeaderText="Event Year" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="CreateDate" HeaderText="Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
            <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By"/>           
        </Columns>
    </asp:GridView>


</asp:Panel>
    </center>
    </td></tr></table>
</asp:Content>

