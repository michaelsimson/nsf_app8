﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="TeamSchedule.aspx.cs" Inherits="TeamPlanning_TeamSchedule" %>

<%@ Register Assembly="ScheduleControl" Namespace="EventCalendar" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">


    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
       <asp:LinkButton ID="LinkButton1" CausesValidation="false" PostBackUrl="~/TeamPlanning.aspx" runat="server">Back to Team Planning</asp:LinkButton>
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
    <table id="tblBeeBookSchedule" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Team Schedule</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                <div style="text-align: center">
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                </div>
                <br />
                <center>
                    <table style="margin-left: 0px;">

                        <tr>
                            <td style="width: 70px"></td>
                            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                            <td align="left" nowrap="nowrap">
                                <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                                    AutoPostBack="True">
                                    <asp:ListItem Value="0">Select year</asp:ListItem>
                                    <asp:ListItem Value="-1">All</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                </asp:DropDownList>

                            </td>

                            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                            <td style="width: 100px" align="left">
                                <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                            <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                                <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                            </td>
                            <td style="width: 125px;" align="left">
                                <asp:DropDownList ID="ddTeams" DataTextField="TeamName" DataValueField="TeamID" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddTeams_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                            <td>

                                <asp:Button ID="btnExportToExcel" runat="server" Visible="false" Text="Export To Excel" OnClick="btnExportToExcel_Click" />
                            </td>
                        </tr>

                    </table>
                </center>
                <br />
                <div style="text-align: center">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
                <br />

                <table id="tblTeamScheduleChart" border="0" cellpadding="3" cellspacing="0" width="1250px" runat="server" style="display: none;">


                    <tr>
                        <td colspan="2">

                            <div>
                                <center>
                                    <div runat="server" id="div1">
                                        <cc1:ScheduleControl ID="EventCalendarControl1" runat="server" Height="91px" Width="300px" />
                                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    </div>
                                </center>
                            </div>



                        </td>

                    </tr>
                </table>
                <br />







            </td>
        </tr>
    </table>

</asp:Content>
