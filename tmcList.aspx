<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tmcList.aspx.cs" Inherits="Reports_tmcList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <h3 align="center">List of chapters (by Chapter)</h3>
		<h4>Please select one</h4>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id="DataGrid1" style="Z-INDEX: 101; LEFT: 72px; POSITION: absolute; TOP: 80px" runat="server"
				AutoGenerateColumns="False" Width="488px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
				BackColor="White" CellPadding="3" GridLines="Vertical">
				<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="City" HeaderText="Chapter"></asp:BoundColumn>
					<asp:BoundColumn DataField="State" HeaderText="ChapterState"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Date">
						<ItemTemplate>
							<asp:Label ID="Label1" runat="server" Text='<%# GetDate(Convert.ToInt32(DataBinder.Eval(Container, "DataItem.ChapterID")))  %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterID") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="ChapterID" DataNavigateUrlFormatString="TotalMedalCount.aspx?Chap={0}"
						HeaderText="Get MedalCounts" NavigateUrl="TotalMedalCount.aspx">
						<HeaderStyle Width="30px"></HeaderStyle>
					</asp:HyperLinkColumn>
					<asp:HyperLinkColumn Text="Select" DataNavigateUrlField="ChapterID" DataNavigateUrlFormatString="xlReport1.aspx?Chap={0}"
						HeaderText="Get Badges" NavigateUrl="xlReport1.aspx">
						<HeaderStyle Width="30px"></HeaderStyle>
					</asp:HyperLinkColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			</asp:datagrid></form>
</body>
</html>

 

 
 
 