Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports Microsoft.ApplicationBlocks.Data
<Serializable()> Public Class VolunteerAssignRoles
    Inherits System.Web.UI.Page

    Protected isProductGroupSelectionChanged As Boolean
    Dim isDebug As Boolean = False
    Dim conn As SqlConnection

    Dim volRoleDataReader As SqlDataReader
    Dim isInEditMode As Boolean = False
    Dim editIndex As Integer = -1
    Dim loginId As Integer = 0
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'todo: uncomment before uploading
        If (Session("LoggedIn") Is Nothing) Or (Session("LoggedIn") <> "True") Then
            If (Not Session("entryToken") Is Nothing) Then
                Response.Redirect("Login.aspx?entry=" + Session("entryToken").ToString.Substring(1, 1))
            Else
                'showTimeOutMsg()
            End If
        End If

        Dim loginChapterId As Integer = 0
        Dim loginZoneId As Integer = 0
        Dim loginClusterId As Integer = 0
        Dim loginVolunteerId As Integer = 0
        Dim loginRoleId As Integer = 0
        Dim loginName As String = ""
        Dim loginTeamLead As String = ""
        Dim loginEventId As Integer = 0
        Dim loginProdGroupId As Integer = 0
        Dim loginProdId As Integer = 0

        conn = New SqlConnection(Application("ConnectionString"))
        If (Session("LoginID") = Nothing) Then
            showTimeOutMsg()
            Return
        End If
        If (Session("RoleId") = Nothing) Then
            showTimeOutMsg()
            Return
        End If

        If (Not Page.IsPostBack) Then
            Dim nsfMaster As VRegistration.NSFMasterPage = Me.Master
            nsfMaster.addBackMenuItem("volunteerfunctions.aspx")
            loginId = CType(Session("LoginId"), Decimal)
            loginRoleId = CType(Session("RoleId"), Decimal)

            Dim sb As StringBuilder = New StringBuilder
            sb.Append("select firstname,lastname,v.volunteerid ,isnull(v.chapterId,0) as chapterid ,isnull(v.clusterid,0) as clusterid ,isnull(v.zoneid,0) as zoneid , isnull(v.teamlead,'N') as teamlead, isnull(v.eventid,0) as eventid , isnull(productGroupId,0) as productGroupId, isnull(productId,0) as productId")
            sb.Append(" from volunteer v ,indspouse i where v.memberid = i.automemberid and v.memberid= ")
            sb.Append(CType(loginId, String))
            sb.Append(" and roleid =")
            sb.Append(CType(loginRoleId, String))
            If (loginRoleId > 5) Then  'must be a TeamLead
                sb.Append(" and TeamLead='Y'")
            End If

            Dim dr As SqlDataReader
            Try
                dr = SqlHelper.ExecuteReader(Application("connectionString"), CommandType.Text, sb.ToString)
                ' Iterate through DataReader, should only be one 
                While (dr.Read())
                    If Not dr.IsDBNull(0) Then
                        loginName = CStr(dr("firstName")) + " " + CStr(dr("lastname"))
                        loginVolunteerId = CInt(dr("volunteerid"))
                        loginChapterId = CInt(dr("chapterid"))
                        loginZoneId = CInt(dr("ZoneId"))
                        loginClusterId = CInt(dr("ClusterId"))
                        loginTeamLead = CStr(dr("teamLead"))
                        loginEventId = CInt(dr("eventid"))
                        loginProdGroupId = CInt(dr("productGroupId"))
                        loginProdId = CInt(dr("productId"))
                    End If
                End While
                If Not dr Is Nothing Then dr.Close()
            Finally
                dr = Nothing
            End Try

            If ((loginRoleId = Nothing) Or (loginRoleId = 0)) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is no role assigned to this login id."
                Return
            End If
            If (Not isAllowedToContinue(loginRoleId, loginTeamLead)) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is a mismatch in the assignment of roles.  Please contact the National Coordinator"
                Return
            End If

            Dim errMsg As String = Nothing
            If (loginRoleId = 3 And loginZoneId = 0) Then
                errMsg = "The login Zonal Coordinator's zone information missing"
            ElseIf (loginRoleId = 4 And loginClusterId = 0) Then
                errMsg = "The login Cluster coordinator's cluster information missing"
            ElseIf (loginRoleId = 5 And loginChapterId = 0) Then
                errMsg = "The login Chapter Coordinator's chapter information missing"
            End If
            If (Not errMsg Is Nothing) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = errMsg
                Return
            End If

            Session("loginChapterId") = loginChapterId
            Session("loginRoleId") = loginRoleId
            Session("loginZoneId") = loginZoneId
            Session("loginClusterId") = loginClusterId
            Session("loginVolunteerId") = loginVolunteerId
            Session("loginName") = loginName
            Session("loginTeamLead") = loginTeamLead
            Session("loginEventId") = loginEventId
            Session("loginRoleCategory") = getLoginRoleCategory(loginVolunteerId)
            Session("loginProdGroupId") = loginProdGroupId
            Session("loginProdId") = loginProdId

            loadSearchControls(loginVolunteerId, loginRoleId, loginZoneId, loginClusterId, loginChapterId, loginTeamLead)

        End If
        Page.MaintainScrollPositionOnPostBack = True
        clearMessages()
        If (isSessionValid()) Then
            showTimeOutMsg()
            Return
        End If
    End Sub
    Private Function isAllowedToContinue(ByVal loginRoleId As Integer, ByVal loginTeamLead As String) As Boolean
        If ((loginRoleId > 0 And loginRoleId < 6) Or (loginRoleId = 36 And loginTeamLead = "Y") Or loginTeamLead = "Y") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub showTimeOutMsg()
        Response.Redirect("timeoutError.aspx")
    End Sub
    Private Function isSessionValid() As Boolean
        If (Session("LoginID") Is Nothing Or Session("loginRoleId") Is Nothing Or Session("loginVolunteerId") Is Nothing) Then
            Return True
        Else : Return False
        End If
    End Function
    Protected Function getLoginRoleId() As Integer
        Dim loginRoleId As Integer = 0
        Try
            If (Not Session("loginRoleId") Is Nothing) Then
                loginRoleId = CInt(Session("loginRoleId").ToString())
            End If
        Catch

        End Try
        Return loginRoleId
    End Function
    Private Function getLoginProdGroupId() As Integer
        Dim loginProdGroupId As Integer = 0
        Try
            If (Not Session("loginProdGroupId") Is Nothing) Then
                loginProdGroupId = CInt(Session("loginProdGroupId").ToString())
            End If
        Catch

        End Try
        Return loginProdGroupId
    End Function
    Private Function getLoginProductId() As Integer
        Dim loginProdId As Integer = 0
        Try
            If (Not Session("loginProdId") Is Nothing) Then
                loginProdId = CInt(Session("loginProdId").ToString())
            End If
        Catch

        End Try
        Return loginProdId
    End Function
    Protected Function getLoginEventId() As Integer
        Dim loginEventId As Integer = 0
        Try
            If (Not Session("loginEventId") Is Nothing) Then
                loginEventId = CInt(Session("loginEventId").ToString())
            End If
        Catch

        End Try
        Return loginEventId
    End Function

    Protected Function getLoginTeamLead() As String
        Dim loginTeamLead As String = 0
        Try
            If (Not Session("loginTeamLead") Is Nothing) Then
                loginTeamLead = Session("loginTeamLead").ToString()
            End If
        Catch

        End Try
        Return loginTeamLead
    End Function
    Protected Function getRoleSearchCategory() As String
        If (Not Session("searchRoleCategory") Is Nothing) Then
            Return Session("searchRoleCategory").ToString
        Else
            showTimeOutMsg()
            Return Nothing
        End If
    End Function
    Public Sub loadSearchControls(ByVal volId As Integer, ByVal roleId As Integer, ByVal zoneId As Integer, ByVal clusterId As Integer, ByVal chapterId As Integer, ByVal loginTeamLead As String)
        If (Not roleId = Nothing) Then
            lblLoginName.Text = "<u>Name</u> : " + Session("loginName")
            loadRoleCatDropDown(roleId)
            lblLoginRole.Text = " You are logged in as "
            Select Case roleId
                Case Is < 3 'admin, NationalC
                    If (roleId = 1) Then
                        lblLoginRole.Text += " 'Admin'"
                    ElseIf (roleId = 2) Then
                        lblLoginRole.Text += "'National Coordinator'."
                    End If

                Case 3 'ZonalC
                    lblLoginRole.Text += "'Zonal Coordinator'"

                Case 4 'ClusterC
                    lblLoginRole.Text += "'Cluster Coordinator'"

                Case 5 'ChapterC
                    lblLoginRole.Text += "'Chapter Coordinator'"

                Case 36 'FinalCoreT
                    lblLoginRole.Text += "'Finals Convenor'"
                Case Else
                    lblLoginRole.Text += "'" + getRoleName(roleId) + "'"

            End Select

            If (loginTeamLead = "Y") Then
                lblLoginRole.Text += "- Team Lead"
            End If

            If (getLoginEventId() > 0) Then
                lblLoginRole.Text += "<br><u>Event</u> : " + getEventName(getLoginEventId().ToString)
            ElseIf (roleId = 36) Then
                lblLoginRole.Text += "<br><u>Event</u> : Finals"
            End If

            If (getLoginProdGroupId() > 0) Then
                lblLoginRole.Text += "&nbsp;&nbsp;&nbsp;<u>Product Group</u> : " + getProductGroupName(getLoginProdGroupId())
            End If
            If (getLoginProductId() > 0) Then
                lblLoginRole.Text += "&nbsp;&nbsp;&nbsp;<u>Product</u> : " + getProductName(getLoginProductId())
            End If
        End If
    End Sub
    Private Sub loadRoleCatDropDown(ByVal loginRoleId As Integer)
        If (getSessionSelectionID("zone") > 0) Then 'use login user's selection (set by volunteerfunctions.aspx) to determine the display items
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Zonal"
            loadRolesDropDown("Zonal")
            loadList(loginRoleId, "Zonal")
        ElseIf (getSessionSelectionID("cluster") > 0) Then
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Cluster"
            loadRolesDropDown("Cluster")
            loadList(loginRoleId, "Cluster")
        ElseIf (getSessionSelectionID("chapter") > 0) Then
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Chapter"
            loadRolesDropDown("Chapter")
            'Else
            If ((loginRoleId = 1 Or loginRoleId = 2 Or loginRoleId = 3 Or loginRoleId = 5 Or loginRoleId = 52) And (Request.QueryString("ID") = 1)) Then
                Try
                    If (loginRoleId = 5 Or loginRoleId = 3) Or (loginRoleId = 52 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From Volunteer Where EventId = 2 and ChapterID > 1 and ChapterID=" & getSessionSelectionID("chapter") & " and MemberID=" & Session("LoginID") & "") > 0) Then 'Finals = "Y") ThengetSessionSelectionID("chapter") > 1) Then ' Chapter = "Y"
                        'ddlRoleCatSch.Items.Add("Chapter")
                        ddlRoleCatSch.Visible = False
                        lblRoleCat.Visible = True
                        lblRoleCat.Text = "Chapter"
                        loadRolesDropDown("Chapter")
                        ddlRoleSch.Visible = True
                    ElseIf (loginRoleId = 52 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1 and MemberID=" & Session("LoginID") & " and RoleID=" & Session("RoleID") & "") > 0) Then   'Finals = "Y") Then ' and ChapterID=" & getSessionSelectionID("chapter") & "
                        'ddlRoleCatSch.Items.Add("Finals")
                        ddlRoleCatSch.Visible = False
                        lblRoleCat.Visible = True
                        lblRoleCat.Text = "Finals"
                        loadRolesDropDown("Finals")
                    ElseIf (loginRoleId = 1 Or loginRoleId = 2) And getSessionSelectionID("chapter") = 1 Then
                        ddlRoleCatSch.Visible = False
                        lblRoleCat.Visible = True
                        lblRoleCat.Text = "Finals"
                        loadRolesDropDown("Finals")
                    ElseIf (loginRoleId = 1 Or loginRoleId = 2) And getSessionSelectionID("chapter") > 1 Then
                        ddlRoleCatSch.Items.Add("Chapter")
                        ddlRoleCatSch.Visible = False
                        lblRoleCat.Visible = True
                        lblRoleCat.Text = "Chapter"
                        loadRolesDropDown("Chapter")
                    Else 'If getSessionSelectionID("chapter") > 1 Then
                        ddlRoleCatSch.Items.Add("Chapter")
                        ddlRoleCatSch.Items.Add("Finals")
                    End If
                Catch ex As Exception
                    'Response.Write(ex.ToString)
                End Try
            End If
        Else  'else use login role id to determine the display items
            If (loginRoleId > 0 And loginRoleId < 3) Then
                If (Request.QueryString("ID") = 1) Then
                    ddlRoleCatSch.Items.Add("Finals")
                    ddlRoleCatSch.Items.Add("Chapter")
                Else
                    ddlRoleCatSch.Items.Add("National")
                    ddlRoleCatSch.Items.Add("Zonal")
                    ddlRoleCatSch.Items.Add("Cluster")
                    ddlRoleCatSch.Items.Add("Finals")
                    ddlRoleCatSch.Items.Add("Chapter")
                    ddlRoleCatSch.Items.Add("IndiaChapter")
                End If
            ElseIf (loginRoleId = 3) Then 'ZonalC
                If (Request.QueryString("ID") = 1) Then
                    ddlRoleCatSch.Items.Add("Chapter")
                Else
                    ddlRoleCatSch.Items.Add("Zonal")
                    ddlRoleCatSch.Items.Add("Chapter")
                End If
            ElseIf (loginRoleId = 4) Then  'clusterC
                ddlRoleCatSch.Items.Add("Cluster")
                ddlRoleCatSch.Items.Add("Chapter")
            ElseIf (loginRoleId = 5) Then 'ChapterC
                ddlRoleCatSch.Visible = False
                lblRoleCat.Visible = True
                lblRoleCat.Text = "Chapter"
                loadRolesDropDown("Chapter")
            ElseIf (loginRoleId = 36) Then 'FinalsConvenor
                ddlRoleCatSch.Visible = False
                lblRoleCat.Visible = True
                lblRoleCat.Text = "Finals"
                loadRolesDropDown("Finals")
            ElseIf (loginRoleId = 52) Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1 and MemberID=" & Session("LoginID") & " and RoleId=" & loginRoleId & "") > 0 Then
                    ddlRoleCatSch.Visible = False
                    lblRoleCat.Visible = True
                    lblRoleCat.Text = "Finals"
                    loadRolesDropDown("Finals")
                End If
            Else  'must be TeamLead
                'Load only the items relavant to this role
                'Dim dr As SqlDataReader
                'Try
                '    dr = SqlHelper.ExecuteReader(Application("connectionString"), CommandType.StoredProcedure, "usp_getroleCategories ", New SqlParameter("@roleid", CStr(loginRoleId)))
                '    ' Iterate through DataReader, should only be one 
                '    While (dr.Read())
                '        If Not dr.IsDBNull(0) Then
                '            ddlRoleCatSch.Items.Add(dr("category"))
                '        End If
                '    End While
                '    If Not dr Is Nothing Then dr.Close()
                'Finally
                '    dr = Nothing
                'End Try
                lblRoleCat.Text = Session("loginRoleCategory")
                lblRole.Text = getRoleName(loginRoleId)
                lblRoleCat.Visible = True
                lblRole.Visible = True
                ddlRoleCatSch.Visible = False
                ddlRoleSch.Visible = False
                End If
        End If
    End Sub

    Private Sub loadDropDownBox(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal storedProc As String, ByVal name As String, ByVal textCol As String, ByVal valueCol As String, ByVal nullItemText As String, ByVal selectedValue As Integer)

        Dim ds As New DataSet
        Dim liEvent As New ListItem(nullItemText, "0")
        ddl.Items.Clear()
        ds = Cache.Get(name)

        If (ds Is Nothing) Then
            Try
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, storedProc)
            Catch se As SqlException
                lblError.Visible = True
                lblError.Text = se.Message
                tblMessage.Visible = True
                Return
            End Try
            Cache.Insert(name, ds)
        End If

        If ds.Tables.Count > 0 Then
            ddl.DataSource = ds.Tables(0)
            ddl.DataTextField = ds.Tables(0).Columns(textCol).ToString
            ddl.DataValueField = ds.Tables(0).Columns(valueCol).ToString
            ddl.DataBind()
            ddl.Items.Insert(0, liEvent)
            If (selectedValue > 0) Then
                ddl.Items.FindByValue(selectedValue).Selected = True
                If Session("RoleId") > 4 Then
                    ddl.Enabled = False
                End If
            Else
                ddl.Items.FindByValue("0").Selected = True
            End If

        End If

    End Sub
    Private Sub loadChapterDropDown(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal zoneId As Integer, ByVal clusterid As Integer, ByVal selectedValue As Integer)
        Dim dsEvents As New DataSet
        Dim liNull As New ListItem("Select A Chapter", "0")
        Dim sb As New StringBuilder

        sb.Append(" select chapterId, Name, chapterCode from chapter ")

        If (zoneId > 0 And clusterid > 0) Then
            sb.Append(" where zoneid=")
            sb.Append(CStr(zoneId))
            sb.Append(" and clusterid= ")
            sb.Append(CStr(clusterid))
        ElseIf (zoneId > 0) Then
            sb.Append(" where zoneid = ")
            sb.Append(CStr(zoneId))
        ElseIf (clusterid > 0) Then
            sb.Append(" where clusterid = ")
            sb.Append(CStr(clusterid))
        End If
        sb.Append(" order by state,chapterCode ")

        Try
            dsEvents = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString)
        Catch se As SqlException
            displayErrorMessage(se.Message)
            Return
        End Try

        If dsEvents.Tables.Count > 0 Then
            ddl.Items.Clear()
            ddl.DataSource = dsEvents.Tables(0)
            ddl.DataTextField = dsEvents.Tables(0).Columns("chapterCode").ToString
            ddl.DataValueField = dsEvents.Tables(0).Columns("ChapterId").ToString
            ddl.DataBind()
            ddl.Items.Insert(0, liNull)
            If (selectedValue > 0) Then
                ddl.Items.FindByValue(selectedValue).Selected = True
                If Session("RoleId") > 4 Then
                    ddl.Enabled = False
                End If
            Else
                ddl.Items.FindByValue("0").Selected = True
            End If

        End If

    End Sub
    Public Sub loadRolesDropDown(ByVal category As String)

        Dim strSql As String = "Select roleid, selection from Role where "
        Dim loginRoleId As Integer = getLoginRoleId()

        If (category <> "0") Then
            strSql = strSql + getCategorySql(category)
            If (loginRoleId > 0 And loginRoleId <= 5) Then  'not to let Chapter Coor to select ChapterC
                strSql = strSql + " and roleid >=" + CStr(loginRoleId)
            ElseIf (loginRoleId <> 36 And getLoginTeamLead() = "Y") Then
                strSql = strSql + " and roleid =" + CStr(loginRoleId) 'TeamLead can assign only in his role
            End If
            strSql = strSql + " order by selection "
            If Request.QueryString("ID") = "1" Then
                strSql = "Select roleid, selection from Role where roleid in(15,16,17,18,19,91,86,20,21,23)"
            End If
            Dim ds As DataSet
            Try
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            Catch se As SqlException
                displayErrorMessage(strSql)
                Return
            End Try
            If (ds.Tables.Count > 0) Then
                ddlRoleSch.DataSource = ds.Tables(0)
                ddlRoleSch.DataBind()
                ddlRoleSch.Items.Add("Select a Role")
                ddlRoleSch.Items.FindByText("Select a Role").Selected = True
                ddlRoleSch.Items.FindByText("Select a Role").Value = 0
            End If
        Else
            ddlRoleSch.Items.Clear()
            ddlRoleSch.Items.Add("Select a Role")
            ddlRoleSch.Items.FindByText("Select a Role").Selected = True
            ddlRoleSch.Items.FindByText("Select a Role").Value = 0
        End If

    End Sub
    Private Function getCategorySql(ByVal category As String) As String
        Dim strSql As String = ""

        If (category <> Nothing) Then
            Select Case (category)
                Case "Zonal"
                    strSql = " [zonal]='Y'"
                Case "Cluster"
                    strSql = " [Cluster]='Y'"
                Case "Chapter"
                    strSql = " [chapter]='Y'"
                Case "Finals"
                    strSql = " [Finals]='Y'"
                Case "IndiaChapter"
                    strSql = " [IndiaChapter]='Y'"
                Case "National"
                    strSql = " [National]='Y'"
            End Select
        End If
        Return strSql
    End Function
    Private Sub displayErrorMessage(ByVal msg As String)
        lblError.Visible = True
        lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = msg
        tblMessage.Visible = True
        TblSearchString.Visible = True
        'txtSearchCriteria.Text = ""
    End Sub
    Private Sub clearMessages()
        lblError.Visible = False
        lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = ""
        lblSessionTimeout.Text = ""
        lblSessionTimeout.Visible = False
        lblUpdateError.Text = ""
    End Sub
    Private Sub clearAllPanels()
        tblIndSearch.Visible = False
        tblGrdVolRoles.Visible = False
        tblVolRoleResults.Visible = False
        grdVolResults.Visible = False
        grdVolRoles.Visible = False
        tblGrdIndResults.Visible = False
        grdIndResults.Visible = False
        tblAddRole.Visible = False
        lblIndError.Text = ""
    End Sub
    Private Function getLoginTypeID(ByVal type As String) As Integer
        Dim retId As Integer = 0
        If (type.ToLower = "zone") Then
            If (Not Session("loginZoneId") Is Nothing) Then
                retId = CInt(Session("loginZoneId"))
            End If
        ElseIf (type.ToLower = "cluster") Then
            If (Not Session("loginClusterId") Is Nothing) Then
                retId = CInt(Session("loginClusterId"))
            End If
        ElseIf (type.ToLower = "chapter") Then
            If (Not Session("loginChapterId") Is Nothing) Then
                retId = CInt(Session("loginChapterId"))
            End If
        End If
        Return retId
    End Function
    Private Function getSessionSelectionID(ByVal type As String) As Integer
        Try
            Dim selChapterId As Integer = 0  'sel*ID are values selected at VolunteerFunctions.aspx page
            Dim selClusterId As Integer = 0
            Dim selZoneId As Integer = 0
            Dim retId As Integer = 0
            If (Not Session("selZoneID") Is Nothing) Then
                selZoneId = CInt(Session("selZoneID"))
            End If
            If (Not Session("selClusterID") Is Nothing) Then
                selClusterId = CInt(Session("selClusterID"))
            End If
            If (Not Session("selChapterID") Is Nothing) Then
                selChapterId = CInt(Session("selChapterID"))
            End If
            Select Case type.ToLower()
                Case "zone"
                    retId = selZoneId
                Case "cluster"
                    retId = selClusterId
                Case "chapter"
                    retId = selChapterId
            End Select
            Return retId
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Function
    Public Sub ddlRoleCat_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoleCatSch.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        Dim selectText As String = ddl.SelectedValue
        Dim loginRoleId As Integer = 0

        clearAllPanels()
        Session("volRoleSearchCriteria") = ""
        Session("volRoleSearchSql") = ""
        hideSearchCriteria()

        ddlList.Visible = False
        ddlList.SelectedIndex = -1

        loadRolesDropDown(selectText)
        loginRoleId = getLoginRoleId() 'CInt(Session("loginRoleId").ToString
        loadList(loginRoleId, selectText)

    End Sub
    Private Sub loadList(ByVal loginRoleId As Integer, ByVal selectText As String)
        Dim loginZoneId As Integer = 0
        Dim loginClusterId As Integer = 0
        Select Case loginRoleId
            Case 0
                Return
            Case Is < 3  'admin or nationalC
                Select Case selectText
                    Case "Zonal"
                        ddlList.Visible = True
                        loadDropDownBox(ddlList, "usp_GetZones", "Zones", "ZoneCode", "ZoneID", "[Select a Zone]", getSessionSelectionID("zone"))
                    Case "Cluster"
                        ddlList.Visible = True
                        loadDropDownBox(ddlList, "usp_GetClusters", "Clusters", "ClusterCode", "ClusterID", "[Select a Cluster]", getSessionSelectionID("cluster"))
                    Case "Chapter"
                        ddlList.Visible = True
                        loadDropDownBox(ddlList, "usp_GetChapters", "Chapter", "ChapterCode", "ChapterID", "[Select a Chapter]", getSessionSelectionID("chapter"))
                End Select

            Case 3 'zonal
                loginZoneId = getSessionSelectionID("Zone")

                Select Case selectText
                    Case "Chapter"
                        ddlList.Visible = True
                        ddlList.SelectedIndex = -1
                        'loadChapterDropDown(ddlList, loginZoneId, 0) 'only chapters that belong to the login zone
                        loadChapterDropDown(ddlList, loginZoneId, 0, getSessionSelectionID("chapter"))
                End Select


            Case 4 'cluster
                loginClusterId = getSessionSelectionID("Cluster")

                Select Case selectText
                    Case "Chapter"
                        ddlList.Visible = True
                        ddlList.SelectedIndex = -1
                        loadChapterDropDown(ddlList, 0, loginClusterId, getSessionSelectionID("chapter")) 'only clusters that belong to the login zone
                End Select

            Case 5 'chapter
        End Select
    End Sub



    Protected Sub Find_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Find.Click

        ' strSql.Append("select i.memberid,v.volunteerid,FirstName+' '+LastName as Name,v.RoleId,v.RoleCode,v.TeamLead,v.EventYear,v.EventId,v.EventCode,v.Chapterid,v.ChapterCode,[National],Zoneid,Finals,ClusterId,ProductGroupId From volunteer v,Indspouse i where v.memberid = i.automemberId ")
        Dim sb As StringBuilder = New StringBuilder()
        Dim sbSch As StringBuilder = New StringBuilder()
        Dim bCheck As Boolean = False
        Dim bFirst As Boolean = False
        Dim txtRole As String = ddlRoleSch.SelectedValue
        Dim txtRole2 As String = lblRole.Text
        Dim txtRoleCat As String = ddlRoleCatSch.SelectedValue
        Dim txtRoleCat2 As String = lblRoleCat.Text
        Dim txtList As String = ddlList.SelectedValue
        Dim blnFirst As Boolean = True
        Dim loginRoleId As Integer = getLoginRoleId()
        Dim bError As Boolean = False
        Dim strErrMsg As String = ""

        grdVolResults.Visible = False
        grdVolResults.Controls.Clear()
        Session("volRoleSearchCriteria") = ""
        Session("volRoleSearchSql") = ""
        hideSearchCriteria()

        ' sbSch.Append("<UL><b>You searched for:</B></ul> <br>")

        If (txtRoleCat <> "0" Or txtRoleCat2 <> Nothing) Then
            Dim roleCategory As String

            If (txtRoleCat <> Nothing And txtRoleCat <> "0") Then
                roleCategory = txtRoleCat
            Else
                roleCategory = txtRoleCat2
            End If

            If (loginRoleId < 3) Then
                If (roleCategory = "Zonal" And txtList = "0") Then
                    bError = True
                    strErrMsg = "Please Select a Zone"
                ElseIf (roleCategory = "Cluster" And txtList = "0") Then
                    bError = True
                    strErrMsg = "Please select a Cluster"
                ElseIf (roleCategory = "Chapter" And (txtList = "0")) Then
                    bError = True
                    strErrMsg = "Please select a chapter"
                End If
            End If

            'If (txtRole = "0") Then
            '    bError = True
            '    strErrMsg = "Please select a Role."
            'End If

            If (bError = False) Then
                Dim roleText As String
                Dim roleId As Integer

                sbSch.Append("<font color=green> Role Category = </font>" + roleCategory + "<br>")
                Session("searchRoleCategory") = roleCategory
                If (txtRole <> Nothing And txtRole <> "0") Then
                    roleText = txtRole
                    roleId = ddlRoleSch.SelectedValue
                Else
                    roleText = txtRole2
                    roleId = getLoginRoleId()  'must be a teamlead
                End If

                If (roleText <> "0" And roleText <> "" And roleText.Length() > 0) Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" RoleId=" + CType(roleId, String))
                    sbSch.Append("<font color=green>Role = </font>" + roleText + "<br>")
                    Session("searchRoleId") = CInt(roleId)
                    Session("searchRoleCode") = roleText
                Else  'all roles in the dropdown
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    'Get all the roles from the drop down
                    Dim item As ListItem
                    Dim sbRoles As StringBuilder = New StringBuilder
                    Dim bStart As Boolean = True

                    For Each item In ddlRoleSch.Items
                        If (bStart = True) Then
                            bStart = False
                            sbRoles.Append(" roleid in (")
                        Else
                            sbRoles.Append(",")
                        End If
                        sbRoles.Append(item.Value)
                    Next
                    If (sbRoles.Length > 0) Then
                        sbRoles.Append(")")
                    End If
                    'sb.Append(" RoleId in ( select roleid from role where " + getCategorySql(roleCategory) + ")")
                    sb.Append(sbRoles.ToString())
                    sbSch.Append("<font color=green>Role =  </font>All Roles <br>")
                    Session("searchRoleCode") = "All Roles"
                    Session("searchRoleId") = 0
                End If

                If (roleCategory = "Zonal") Then
                    Dim zId As Integer
                    If (txtList <> "0" And txtList <> "") Then
                        zId = CInt(txtList)
                    Else
                        ' zId = getLoginTypeId("Zone")
                        zId = getSessionSelectionID("zone")
                    End If
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" zoneId=" + CStr(zId))
                    sbSch.Append("<font color=green>Zone = </font>" + getZoneName(zId) + "<br>")
                    Session("searchZoneId") = zId
                ElseIf (roleCategory = "Cluster") Then
                    Dim clId As Integer
                    If (txtList <> "0" And txtList <> "") Then
                        clId = CInt(txtList)
                    Else
                        ' clId = getLoginTypeId("Cluster")
                        clId = getSessionSelectionID("cluster")
                    End If
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" clusterId=" + CStr(clId))
                    sbSch.Append("<font color=green>Cluster = </font>" + getClusterName(clId) + "<br>")
                    Session("searchClusterId") = clId

                ElseIf (roleCategory = "Chapter") Then
                    Dim chId As Integer
                    If (txtList <> "0" And txtList <> "") Then
                        chId = CInt(txtList)
                    Else
                        If (loginRoleId > 4 And loginRoleId <> 36) Then
                            chId = getLoginTypeID("chapter")
                        Else
                            chId = getSessionSelectionID("chapter")
                        End If
                    End If
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" chapterId=" + CStr(chId))
                    sbSch.Append("<font color=green>Chapter = </font>" + getChapterName(chId) + "<br>")
                    Session("searchChapterId") = chId
                ElseIf (roleCategory = "National") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" [national]='Y'")
                    sbSch.Append("<font color=green>National = </font> Yes" + "<br>")
                ElseIf (roleCategory = "Finals") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" [finals]='Y'")
                    sbSch.Append("<font color=green>Finals = </font> Yes" + "<br>")
                ElseIf (roleCategory = "IndiaChapter") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" [indiachapter]='Y'")
                    sbSch.Append("<font color=green>India Chapter = </font> Yes" + "<br>")
                End If

                If (loginRoleId <= 5 And loginRoleId >= 1 And loginRoleId <> 36) Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" roleid >= " + CStr(loginRoleId)) ' Do not show records below the given level.
                End If

                If (getLoginTeamLead().ToLower = "y" And getLoginRoleId() > 5 And getLoginRoleId() <> 36) Then 'teamLead - don't show the login volunteer's record
                    'exclude the volunteer's own record
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" volunteerid <> " + Session("loginVolunteerId").ToString())
                    'Product group code
                    If (getLoginProdGroupId() > 0) Then
                        If (blnFirst = True) Then
                            blnFirst = False
                        Else
                            sb.Append(" and ")
                        End If
                        sb.Append(" productGroupId = " + getLoginProdGroupId().ToString())

                        If (getLoginProductId() > 0) Then
                            sb.Append(" and productId = " + getLoginProductId().ToString)
                        End If
                    End If
                End If

                Dim whereStr As String = ""

                If (sb.Length() > 0) Then
                    whereStr = sb.ToString()
                End If

                Session("volRoleSearchSql") = whereStr
                Session("volRoleSearchCriteria") = sbSch.ToString
                'displaySearchCriteria()
                clearGrid()
                loadGrid(True)
            End If
        Else
            bError = True
            strErrMsg = "Please select an option"
        End If

        If (bError = True) Then
            displayErrorMessage(strErrMsg)
        End If

        tblIndSearch.Visible = False
        tblGrdVolRoles.Visible = False
        grdVolRoles.Visible = False
        tblGrdIndResults.Visible = False
        grdIndResults.Visible = False
        tblAddRole.Visible = False
        lblIndError.Text = ""
    End Sub
    Public Function getLoginRoleCategory(ByVal volunteerId As Int16) As String
        Dim category As String = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "dbo.usp_getVolunteerRoleCategory", New SqlParameter("@volunteerid", volunteerId))
        Return category
    End Function

    Public Function getChapterName(ByVal chapterId As Int16) As String
        Dim chapName As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from chapter where chapterid = " + CStr(chapterId))
        Return chapName
    End Function
    Public Function getChapterCode(ByVal chapterId As Int16) As String
        Dim chapCode As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select chaptercode from chapter where chapterid = " + CStr(chapterId))
        Return chapCode
    End Function
    Public Function getClusterName(ByVal clusterId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from cluster where clusterid = " + CStr(clusterId))
        Return name
    End Function
    Public Function getClusterCode(ByVal clusterId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select clustercode from cluster where clusterid = " + CStr(clusterId))
        Return name
    End Function
    Public Function getZoneName(ByVal zoneId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from zone where zoneid = " + CStr(zoneId))
        Return name
    End Function
    Public Function getZoneCode(ByVal zoneId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select zonecode from zone where zoneid = " + CStr(zoneId))
        Return name
    End Function
    Public Function getRoleName(ByVal roleId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from role where roleid = " + CStr(roleId))
        Return name
    End Function
    Public Function getRoleCode(ByVal roleId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select rolecode from role where roleid = " + CStr(roleId))
        Return name
    End Function
    Public Function getEventName(ByVal eventId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from event where eventid = " + CStr(eventId))
        Return name
    End Function
    Public Function getEventCode(ByVal eventId As Int16) As String
        Dim code As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select eventcode from event where eventid = " + CStr(eventId))
        Return code
    End Function
    Public Function getProductGroupCode(ByVal pgId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select productgroupcode from productgroup where productGroupid = " + CStr(pgId))
        Return name
    End Function
    Public Function getProductGroupName(ByVal pgId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from productgroup where productGroupid = " + CStr(pgId))
        Return name
    End Function
    Public Function getProductName(ByVal pId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from product where productid = " + CStr(pId))
        Return name
    End Function
    Public Function getProductCode(ByVal pId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select productcode from product where productid = " + CStr(pId))
        Return name
    End Function
    Public Sub displaySearchCriteria()
        Dim str As String = Session("volRoleSearchCriteria").ToString
        'Dim str As String = Session("volRoleSearchSql").ToString ''for debugging
        TblSearchString.Visible = True
        txtSearchCriteria.Visible = True
        txtSearchCriteria.Text = str
        hLinkAddNewVolunteer.Visible = True

    End Sub
    Public Sub hideSearchCriteria()
        TblSearchString.Visible = False
        txtSearchCriteria.Visible = False
        txtSearchCriteria.Text = False
    End Sub
    Public Sub clearGrid()
        grdVolResults.EditItemIndex = -1
        grdVolResults.DataSource = Nothing
        grdVolResults.DataBind()
    End Sub

    Public Sub loadGrid(ByVal blnReload As Boolean)
        If (isDebug = True) Then
            Response.Write("<br>loadGrid")
        End If
        Dim ds As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        cmd.CommandText = " " + Session("volRoleSearchSql").ToString
        'ferdine silva
        'lblSessionTimeout.Visible = True
        'lblSessionTimeout.Text = Session("volRoleSearchSql")
        If (blnReload = True) Then
            'Response.Write(cmd.CommandText)
            Session("volDataSet") = Nothing
            Try
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.usp_SelectWhereVolunteer2", New SqlParameter("@wherecondition", Session("volRoleSearchSql").ToString()))

                ' ######## Ferdine Silvaa Found To Be Serialization error ########
                Session("volDataSet") = ds
                grdVolResults.CurrentPageIndex = 0
            Catch se As SqlException
                displayErrorMessage(cmd.CommandText + " " + se.ToString())
                'displayErrorMessage("There is an error while trying to execute the query.")
                Return
            End Try
        Else
            If (Not Session("volDataSet") Is Nothing) Then
                ds = CType(Session("volDataSet"), DataSet)
            End If
        End If

        If (ds Is Nothing Or ds.Tables.Count < 1) Then
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            lblGrdVolResultsHdg.Text = ""
        ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            lblGrdVolResultsHdg.Text = ""
        Else
            ''grdVolResults_ShowColumn()
            Dim i As Integer = ds.Tables(0).Rows.Count
            grdVolResults.Visible = True
            grdVolResults.DataSource = ds.Tables(0)
            grdVolResults.DataBind()
            ''grdVolResults_hideColumn()
            lblError.Visible = False
            tblMessage.Visible = True
            tblVolRoleResults.Visible = True
            lblGrdVolResultsHdg.Text = "2. List of <u>'" + Session("searchRoleCode") + "'</u>"
        End If
        hLinkAddNewVolunteer.Visible = True
        displaySearchCriteria()

    End Sub

    ''Private Sub grdVolResults_ShowColumn()
    ''    Dim i As Integer
    ''    For i = 17 To 24
    ''        grdVolResults.Columns(i).Visible = True
    ''    Next
    ''End Sub
    ''Private Sub grdVolResults_hideColumn()
    ''    Dim i As Integer
    ''    For i = 17 To 24
    ''        grdVolResults.Columns(i).Visible = False
    ''    Next
    ''End Sub
    Protected Sub grdVolResults_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdVolResults.PreRender

    End Sub
    Protected Function getEventId(ByVal strEventCode As String) As Integer
        Dim eventId As Integer

        If (strEventCode = "Chapter") Then
            eventId = 2
        ElseIf (strEventCode = "Finals") Then
            eventId = 1
        ElseIf (strEventCode = "WkShop") Then
            eventId = 3
        End If
        Return eventId
    End Function
    Protected Sub grdVolResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdVolResults.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim volunteerId As Integer
            Dim automemberid As Integer
            volunteerId = CInt(e.Item.Cells(2).Text)
            automemberid = CInt(e.Item.Cells(3).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "INSERT INTO DeletedVolunteer (MemberID, RoleId, RoleCode, TeamLead, EventYear, EventId, EventCode, ChapterId, ChapterCode,  [National], ZoneId, ZoneCode, Finals, ClusterID, ClusterCode, IndiaChapter, ProductGroupID, ProductGroupCode, ProductID,  ProductCode, IndiaChapterName, AgentFlag, WriteAccess, [Authorization], Yahoogroup, CreateDate, VolunteerID,CreatedBy) SELECT     MemberID, RoleId, RoleCode, TeamLead, EventYear, EventId, EventCode, ChapterId, ChapterCode, [National], ZoneId, ZoneCode, Finals, ClusterID, ClusterCode, IndiaChapter, ProductGroupID, ProductGroupCode, ProductID, ProductCode, IndiaChapterName, AgentFlag, WriteAccess, [Authorization], Yahoogroup, GETDATE(), VolunteerID," & Session("LoginID") & " FROM Volunteer WHERE VolunteerID = " & CStr(volunteerId) & "; DELETE FROM volunteer WHERE VolunteerID =" + CStr(volunteerId))
            Catch se As SqlException
                displayErrorMessage("Error: while deleting the record")
                Return
            End Try
            loadGrid(True)
            If (tblGrdVolRoles.Visible) Then
                tblGrdVolRoles.Visible = False
            End If
            grdVolResults.EditItemIndex = -1
        End If
    End Sub
    Protected Sub grdVolResults_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdVolResults.ItemDataBound
        'ferdine 
        If e.Item.Cells(5).Text = "RoleID" Then
        Else
            If Val(Trim(e.Item.Cells(5).Text)) <= getLoginRoleId() Then
                e.Item.Cells(1).Enabled = False
            End If

            If Val(Trim(e.Item.Cells(5).Text)) = 5 And getLoginRoleId() > 4 Then
                e.Item.Cells(1).Enabled = False
                e.Item.Cells(0).Enabled = False
            End If
        End If

    End Sub
    Protected Sub grdVolResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdVolResults.EditCommand
        If (isDebug = True) Then
            Response.Write("<br>edit command")
        End If
        grdVolResults.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        If (vDS Is Nothing) Then
            showTimeOutMsg()
            Return
        End If
        Dim page As Integer = grdVolResults.CurrentPageIndex
        Dim pageSize As Integer = grdVolResults.PageSize

        Dim currentRowIndex As Integer

        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr
        loadGrid(False)
    End Sub
    Private Sub grdVolResults_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdVolResults.ItemCreated
        If (isDebug = True) Then
            Response.Write("<br>itemcreated")
        End If
        Dim loginRoleId As Integer
        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        If (roleCat Is Nothing) Then
            Return
        End If

        loginRoleId = getLoginRoleId()

        If (loginRoleId > 0) Then
            If (loginRoleId > 2) Then
                'grdVolResults.Columns(6).Visible = False 'TeamLead
                grdVolResults.Columns(13).Visible = False 'AgentFlag
                grdVolResults.Columns(14).Visible = False 'WriteAccess
                grdVolResults.Columns(15).Visible = False 'Authorization
                grdVolResults.Columns(16).Visible = False 'IndiaChapterName
            End If
            grdVolResults.Columns(8).Visible = True 'EventYear
            grdVolResults.Columns(7).Visible = False
            Select Case roleCat
                Case "Finals"
                    grdVolResults.Columns(7).Visible = True 'eventcode                   

                Case "Zonal"
                    grdVolResults.Columns(7).Visible = False

                Case "Cluster"
                    grdVolResults.Columns(7).Visible = False

                Case "Chapter"
                    grdVolResults.Columns(7).Visible = True 'eventcode

                Case "IndiaChapter"
                    grdVolResults.Columns(7).Visible = False 'eventcode
                Case Nothing
                    showTimeOutMsg()
                    Return
            End Select
        End If

        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub grdVolResults_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdVolResults.UpdateCommand
        If (isDebug = True) Then
            Response.Write("<br>update command")
        End If
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim volId As Integer, memberId As Integer, roleId As Integer, eventYear As Integer
        Dim productGroupId As Integer = Nothing
        Dim productId As Integer = Nothing
        Dim indiaChapterName As String = Nothing
        Dim yahooGroup As String = Nothing
        Dim agentFlag As String = Nothing
        Dim writeAccess As String = Nothing
        Dim authorization As String = Nothing
        Dim teamLead As String = Nothing
        Dim indiaChapter As String = Nothing
        Dim chapterId As Integer = Nothing
        Dim clusterId As Integer = Nothing
        Dim zoneId As Integer = Nothing
        Dim eventId As Integer = Nothing

        Dim temp As String
        volId = CInt(e.Item.Cells(2).Text)
        memberId = CInt(e.Item.Cells(3).Text)
        Dim national As String = (e.Item.Cells(18).Text)
        Dim finals As String = (e.Item.Cells(19).Text)

        temp = (e.Item.Cells(20).Text)
        If (temp <> "" And temp <> "&nbsp;") Then
            zoneId = CInt(temp)
        End If
        temp = (e.Item.Cells(21).Text)
        If (temp <> "" And temp <> "&nbsp;") Then
            clusterId = CInt(temp)
        End If
        temp = (e.Item.Cells(22).Text)
        If (temp <> "" And temp <> "&nbsp;") Then
            chapterId = CInt(temp)
        End If
        If (e.Item.FindControl("ddlEventCode").Visible = True) Then
            Dim s As String = CType(e.Item.FindControl("ddlEventCode"), DropDownList).SelectedValue
            If (s <> "") Then
                eventId = CInt(s)
            End If
        Else
            temp = (e.Item.Cells(23).Text)
            If (temp <> "" And temp <> "&nbsp;") Then
                eventId = CInt(temp)
            End If
        End If

        temp = (e.Item.Cells(24).Text)
        If (temp <> "" And temp <> "&nbsp;") Then
            indiaChapter = temp
        End If

        roleId = CInt(CType(e.Item.FindControl("ddlRoleCode"), DropDownList).SelectedValue)
        eventYear = CInt(CType(e.Item.FindControl("ddlEventYear"), DropDownList).SelectedValue)

        If (CType(e.Item.FindControl("ddlProductGroup"), DropDownList).SelectedIndex > 0) Then
            productGroupId = CInt(CType(e.Item.FindControl("ddlProductGroup"), DropDownList).SelectedValue)
        End If

        If (CType(e.Item.FindControl("ddlProduct"), DropDownList).SelectedIndex > 0) Then
            productId = CInt(CType(e.Item.FindControl("ddlProduct"), DropDownList).SelectedValue)
        End If

        If (CStr(CType(e.Item.FindControl("ddlIndiaChapter"), DropDownList).SelectedValue) <> "0") Then
            indiaChapterName = CStr(CType(e.Item.FindControl("ddlIndiaChapter"), DropDownList).SelectedValue)
        End If
        yahooGroup = CType(e.Item.FindControl("txtYahooGroup"), TextBox).Text

        If (getLoginRoleId() < 3 Or getLoginRoleId() = 36) Then
            agentFlag = CType(e.Item.FindControl("ddlAgentFlag"), DropDownList).SelectedValue
            writeAccess = CType(e.Item.FindControl("ddlWriteAccess"), DropDownList).SelectedValue
            authorization = CType(e.Item.FindControl("ddlAuthorization"), DropDownList).SelectedValue
            teamLead = CType(e.Item.FindControl("ddlTeamLead"), DropDownList).SelectedValue
            If (CType(e.Item.FindControl("ddlIndiaChapter"), DropDownList).SelectedValue <> "0") Then
                indiaChapterName = CType(e.Item.FindControl("ddlIndiaChapter"), DropDownList).SelectedValue
            End If
        Else
            If (Not e.Item.FindControl("agentFlag") Is Nothing) Then
                agentFlag = CType(e.Item.FindControl("agentFlag"), Label).Text
            End If
            If (Not e.Item.FindControl("WriteAccess") Is Nothing) Then
                writeAccess = CType(e.Item.FindControl("writeAccess"), Label).Text
            End If
            If (Not e.Item.FindControl("authorization") Is Nothing) Then
                authorization = CType(e.Item.FindControl("authorization"), Label).Text
            End If
            If (Not e.Item.FindControl("TeamLead") Is Nothing) Then
                teamLead = CType(e.Item.FindControl("teamLead"), Label).Text
            End If
            If (Not e.Item.FindControl("indiaChapter") Is Nothing) Then
                indiaChapterName = CType(e.Item.FindControl("indiaChapter"), Label).Text
            End If
        End If
        If (getLoginRoleId() = 5) Then
            teamLead = CType(e.Item.FindControl("ddlTeamLead"), DropDownList).SelectedValue
        End If

        Dim isDuplicate As Boolean
        isDuplicate = isDuplicateRole(volId, memberId, roleId, eventId, clusterId, zoneId, chapterId, productGroupId, productId, national, finals, eventYear)
        If (isDuplicate = True) Then
            lblUpdateError.Visible = True
            lblUpdateError.Text = "Error: Duplicate role for the volunteer."
            Return
        End If

        If (Not teamLead Is Nothing And teamLead = "Y") Then
            isDuplicate = isDuplicateTeamLead(volId, teamLead, roleId, eventId, clusterId, zoneId, chapterId, productGroupId, productId, national, finals, eventYear)
            If (isDuplicate = True) Then
                lblUpdateError.Visible = True
                lblUpdateError.Text = "Error: Duplicate TeamLead."
                Return
            End If
        End If

        'call the update stored proc
        Try
            Dim param(21) As SqlParameter
            param(0) = New SqlParameter("@VolId", volId)
            param(1) = New SqlParameter("@RoleId", roleId)
            param(2) = New SqlParameter("@eventYear", eventYear)
            param(3) = New SqlParameter("@eventId", eventId)
            param(4) = New SqlParameter("@productGroupId", productGroupId)
            param(5) = New SqlParameter("@ProductId", productId)
            param(6) = New SqlParameter("@agentFlag", agentFlag)
            param(7) = New SqlParameter("@writeAccess", writeAccess)
            param(8) = New SqlParameter("@yahooGroup", yahooGroup)
            param(9) = New SqlParameter("@authorization", authorization)
            param(10) = New SqlParameter("@teamLead", teamLead)
            param(11) = New SqlParameter("@indiaChapterName", indiaChapterName)
            param(12) = New SqlParameter("@ModifyDate", Now)
            param(13) = New SqlParameter("@ModifiedBy", Session("LoginID"))
            param(14) = New SqlParameter("@memberId", memberId)
            param(15) = New SqlParameter("@national", national)
            param(16) = New SqlParameter("@finals", finals)
            If (clusterId = 0) Then
                param(17) = New SqlParameter("@clusterId", DBNull.Value)
            Else
                param(17) = New SqlParameter("@clusterId", clusterId)
            End If
            If (chapterId = 0) Then
                param(18) = New SqlParameter("@chapterId", DBNull.Value)
            Else
                param(18) = New SqlParameter("@chapterId", chapterId)
            End If
            If (zoneId = 0) Then
                param(19) = New SqlParameter("@zoneId", DBNull.Value)
            Else
                param(19) = New SqlParameter("@zoneId", zoneId)
            End If
            If (indiaChapter = Nothing) Then
                param(20) = New SqlParameter("@IndiaChapter", DBNull.Value)
            Else
                param(20) = New SqlParameter("@IndiaChapter", indiaChapter)
            End If


            Dim ret As Integer
            ret = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_updateVolunteer", param)
        Catch ex As SqlException
            'ex.message
            lblError.Visible = True
            lblError.Text = "Error:updating the record" + ex.ToString
            Return
        End Try
        grdVolResults.EditItemIndex = -1
        loadGrid(True)
    End Sub

    Protected Sub grdVolResults_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdVolResults.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = grdVolResults.CurrentPageIndex
            Dim pageSize As Integer = grdVolResults.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If

            'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            grdVolResults.EditItemIndex = -1
            loadGrid(False)
        Else
            showTimeOutMsg()
            Return
        End If
    End Sub
    Protected Sub grdVolResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdVolResults.PageIndexChanged
        grdVolResults.CurrentPageIndex = e.NewPageIndex
        grdVolResults.EditItemIndex = -1
        loadGrid(False)
    End Sub
    Private Sub LoadRoleDropDownList(ByRef ddl As DropDownList)
        Dim dsTarget As New DataSet
        Dim whereStr As String = ""
        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        Dim loginRoleId As Integer = getLoginRoleId()

        If (Not roleCat Is Nothing) Then
            Select Case roleCat
                Case "Admin"
                    whereStr = " where [National]='Y'"
                Case "National"
                    whereStr = " where [National]='Y' "
                Case "Zonal"
                    whereStr = " where Zonal='Y'"
                Case "Cluster"
                    whereStr = " where Cluster='Y'"
                Case "Chapter"
                    whereStr = " where Chapter='Y'"
                Case "Finals"
                    whereStr = " where Finals='Y'"
            End Select

            If (loginRoleId <= 5) Then
                whereStr = whereStr + " and roleid >" + CStr(loginRoleId)
            ElseIf (loginRoleId <> 36 And getLoginTeamLead() = "Y") Then
                whereStr = whereStr + " and roleid =" + CStr(loginRoleId)
            ElseIf (loginRoleId = 36) Then
                'whereStr = whereStr + " and roleid >" + CStr(loginRoleId) + " and roleid <> 36 "
                whereStr = whereStr + " and roleid > 5 " ' + " and roleid <> 36 "
            End If

            dsTarget = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_getRolesWhere", New SqlParameter("@whereCondition", whereStr))
            If Not ddl Is Nothing Then
                ddl.DataValueField = "RoleId"
                ddl.DataTextField = "Selection"
                ddl.DataSource = dsTarget.Tables(0)
                ddl.DataBind()
            End If
        Else : Return

        End If
    End Sub
    Private Sub LoadProductGroupDropDownList(ByRef ddl As DropDownList, ByVal eventId As Integer, ByVal productGroupId As Integer)
        If (Not Session("volRoleId") Is Nothing) Then
            Dim loginRole As Integer = CInt(Session("volRoleId").ToString)
            If (loginRole < 0) Then
                ddl.Enabled = False
            Else
                ddl.Enabled = True
            End If
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_getProductGroupByEvent", New SqlParameter("@eventId", eventId))
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "ProductGroupId"
            ddl.DataTextField = "Name"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()

            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Insert(0, blankListItem)

            'TeamLead customization.
            If ((getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes") And getLoginRoleId() <> 36 And getLoginRoleId() > 5) Then
                productGroupId = getLoginProdGroupId()
                If (productGroupId > 0) Then
                    ddl.SelectedValue = productGroupId
                    ddl.Enabled = False
                    'load products
                    ' LoadProductDropDownList(ddlARProd, productGroupId, eventId, 0)
                Else
                    ddl.Enabled = False
                    ddl.SelectedIndex = -1
                    ' ddlARProd.Enabled = False
                End If
            Else
                If (productGroupId > 0) Then
                    ddl.SelectedValue = productGroupId
                Else
                    ddl.SelectedIndex = -1
                End If
            End If

        End If
    End Sub

    Private Sub LoadProductDropDownList(ByRef ddl As DropDownList, ByVal productGroupId As Integer, ByVal eventId As Integer, ByVal productId As Integer)
        If (Not Session("volRoleId") Is Nothing) Then
            Dim loginRole As Integer = CInt(Session("volRoleId").ToString)
            If (loginRole < 0) Then
                ddl.Enabled = False
            End If
        End If
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ProductGroupId", productGroupId)
        params(1) = New SqlParameter("@EventId", eventId)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_getProductByGroupAndEvent", params)
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "ProductId"
            ddl.DataTextField = "Name"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()
            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Insert(0, blankListItem)

            'TeamLead customization
            If ((getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes") And getLoginRoleId() <> 36 And getLoginRoleId() > 5) Then
                productGroupId = getLoginProductId()
                If (productGroupId > 0) Then
                    ddl.SelectedValue = productGroupId
                    ddl.Enabled = False
                Else
                    ddl.SelectedIndex = -1
                    ddl.Enabled = False
                End If
            Else
                If (productId > 0) Then
                    ddl.SelectedItem.Value = productId
                Else
                    ddl.SelectedValue = -1
                End If
            End If
        End If
    End Sub
    Private Sub LoadEventCodeDropDownList(ByRef ddl As DropDownList, ByVal eventId As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select eventid,eventcode from event")
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "eventId"
            ddl.DataTextField = "eventCode"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()
            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Add(blankListItem)
            If (eventId > 0) Then
                ddl.SelectedItem.Value = eventId
            Else
                ddl.SelectedValue = -1
            End If
        End If
    End Sub
    Public Sub EventCode_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>event_code_oninit")
        End If
        Dim searchRoleCategory As String
        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            Dim rowEventId As Integer
            Dim rowEventCode As String = ""
            ' Dim dr As DataRow = CType(Session("editRow"), DataRow)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("EventId") Is DBNull.Value) Then
                    rowEventId = dr.Item("EventId")
                End If
            End If

            If (searchRoleCategory = "Chapter") Then
                LoadEventCodeDropDownList(sender, rowEventId)
            Else
                Dim ddl As DropDownList = CType(sender, DropDownList)
                Dim item As ListItem = New ListItem
                item.Value = rowEventId
                item.Text = rowEventCode
                ddl.Enabled = False
                ddl.Items.Insert(0, item)
            End If
        Else : Return
        End If

    End Sub
    Public Sub Role_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadRoleDropDownList(sender)
    End Sub
    Public Sub Role_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>role_prerender")
        End If
        Dim rowRoleCode As String = ""
        Dim rowRoleId As Integer
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            'If (Not dr.Item("Selection") Is DBNull.Value) Then
            '    rowRoleCode = dr.Item("Selection")
            'End If
            If (Not dr.Item("roleId") Is DBNull.Value) Then
                rowRoleCode = dr.Item("Selection")
                rowRoleId = dr.Item("roleId")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowRoleCode")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRoleId))
            'mRowRoleId = ddlTemp.SelectedValue
            'Session("rowRoleId") = ddlTemp.SelectedValue
            If (getLoginRoleId() > 5 And getLoginRoleId() <> 36 And (getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes")) Then
                ddlTemp.Enabled = False 'role non-editable for teamlead
            End If
        Else
            showTimeOutMsg()
            Return
        End If
    End Sub
    Public Sub Role_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList

        ddlTemp = sender
        'Session("rowRoleId") = ddlTemp.SelectedValue
        'Session("rowRoleCode") = ddlTemp.SelectedItem.Text
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("roleId") = ddlTemp.SelectedValue
            dr.Item("selection") = ddlTemp.SelectedItem.Text
            'Session("editRow") = dr
            Cache("editRow") = dr
            Dim roleId = CInt(ddlTemp.SelectedValue)
            If (roleId = 36 And getLoginRoleId() = 36) Then
                'freeze team lead to 'N'
            End If
        Else
            Return
        End If

    End Sub
    Public Sub lblEventCode2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim searchRoleCategory As String
        Dim lbl As Label

        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            lbl = CType(sender, Label)
            If (searchRoleCategory = "Chapter") Then
                lbl.Visible = False
            Else
                lbl.Visible = True
            End If
        Else : Return
        End If

    End Sub
    Public Sub EventCode_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>event_code_prerender")
        End If
        Dim searchRoleCategory As String
        Dim rowEventId As Integer = 0
        Dim rowEventCode As String = ""


        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            'Dim dr As DataRow = CType(Session("editRow"), DataRow)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("EventId") Is DBNull.Value) Then
                    rowEventId = dr.Item("EventId")
                End If
            End If
            If (searchRoleCategory = "Chapter") Then
                LoadEventCodeDropDownList(sender, rowEventId)

                'for TeamLead, event should not be editable
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender

                If (getLoginRoleId() > 5 And getLoginRoleId() <> 36 And (getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes")) Then
                    ddlTemp.Enabled = False 'role non-editable for teamlead
                End If
            Else
                Dim ddl As DropDownList = CType(sender, DropDownList)
                ddl.Visible = False
            End If
        Else : Return
        End If
    End Sub
    Public Sub EventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>event_year prerender")
        End If
        Dim rowEventYear As Integer = 0
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventYear") Is DBNull.Value) Then
                rowEventYear = dr.Item("EventYear")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowEventYear")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowEventYear))
        Else
            Return
        End If
    End Sub
    Public Sub ProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>PG prerender")
        End If
        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0

        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("RoleId") Is DBNull.Value) Then
            rowRoleId = dr.Item("RoleId")
        End If
        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (ddlTemp.Items.Count = 0) Then
            LoadProductGroupDropDownList(ddlTemp, rowEventId, 0)  'if empty, load
        End If
        ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowProductGroup")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowProdGroupCode))
        'mProductGroupId = ddlTemp.SelectedValue
        'Session("rowProductGroup") = ddlTemp.SelectedValue

        'If (Session("rowRoleId") < 8) Then
        If (rowRoleId < 8) Or (getLoginRoleId() <> 36 And getLoginTeamLead().ToLower = "y") Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
    End Sub
    Public Sub ProductGroup_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Public Sub ProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>PG selectedIndexChanged")
        End If
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""

        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductGroupCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductGroupId") = ddlTemp.SelectedItem.Value
        'Session("editRow") = dr
        Cache("editRow") = dr

        isProductGroupSelectionChanged = True

    End Sub
    Public Sub Product_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>product prerender")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0
        Dim rowProductCode As String = ""

        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("RoleId") Is DBNull.Value) Then
            rowRoleId = dr.Item("RoleId")
        End If
        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If
        If (Not dr.Item("ProductCode") Is DBNull.Value) Then
            rowProductCode = dr.Item("productCode")
        End If

        If (rowProdGroupId > 0) Then
            LoadProductDropDownList(ddlTemp, rowProdGroupId, rowEventId, 0)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowProductCode))
        Else
            ddlTemp.Items.Clear()
        End If
        Dim prodId As Integer = getLoginProductId()
        Dim prodGroupId As Integer = getLoginProdGroupId()

        If (rowRoleId < 8) Or (getLoginRoleId() <> 36 And getLoginTeamLead().ToLower = "y" And ((prodId > 0 Or prodGroupId <= 0) Or (prodGroupId <= 0 And prodId <= 0))) Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
    End Sub
    Public Sub IndiaChapter_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>IC Prerender")
        End If
        Dim indiaChapterName As String = ""
        ' Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("IndiaChapterName") Is DBNull.Value) Then
            indiaChapterName = dr.Item("IndiaChapterName")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowIndiaChapter")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(indiaChapterName))
    End Sub

    Public Sub AgentFlag_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>AgentFlag Prerender")
        End If
        Dim rowAgentFlag As String = ""
        ' Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("AgentFlag") Is DBNull.Value) Then
            rowAgentFlag = dr.Item("AgentFlag")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowAgentFlag")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowAgentFlag))
    End Sub
    Public Sub WriteAccess_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>WriteAccess Prerender")
        End If
        Dim rowWriteAccess As String = ""
        ' Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("WriteAccess") Is DBNull.Value) Then
            rowWriteAccess = dr.Item("WriteAccess")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowWriteAccess")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowWriteAccess))
    End Sub
    Public Sub Authorization_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>Authorization Prerender")
        End If
        Dim rowAuthorization As String = ""
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("authorization") Is DBNull.Value) Then
            rowAuthorization = dr.Item("Authorization")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowAuthorization")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowAuthorization))
    End Sub
    Public Sub TeamLead_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>Team Lead Prerender")
        End If

        Dim rowTeamLead As String = "N"
        Dim rowRoleId As Integer

        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If

        If (Not dr.Item("TeamLead") Is DBNull.Value) Then
            rowTeamLead = dr.Item("TeamLead")
            rowRoleId = dr.Item("roleId")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (rowTeamLead.Trim = "") Then
            rowTeamLead = "N"
        End If
        If (rowRoleId = 36 And getLoginRoleId() = 36) Then
            ddlTemp.SelectedValue = "N" 'freeze it to N for roleid 36 and row role id = 36
            ddlTemp.Enabled = False
        Else
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowTeamLead))
            ddlTemp.Enabled = True
        End If

        If (getLoginRoleId() > 5 And getLoginRoleId() <> 36 And (getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes")) Then
            ddlTemp.Enabled = False 'TeamLead non-editable for teamlead
        End If
    End Sub
    Sub PagerButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim arg As String = sender.CommandArgument
        Select Case arg
            Case "Next"
                If (grdVolResults.CurrentPageIndex < (grdVolResults.PageCount - 1)) Then
                    grdVolResults.CurrentPageIndex += 1
                End If
            Case "Prev"
                If (grdVolResults.CurrentPageIndex > 0) Then
                    grdVolResults.CurrentPageIndex -= 1
                End If
            Case "Last"
                grdVolResults.CurrentPageIndex = (grdVolResults.PageCount - 1)
            Case Else
                grdVolResults.CurrentPageIndex = Convert.ToInt32(arg)
        End Select
        loadGrid(False)
    End Sub
    Private Sub LoadStateDropDownList()
        Dim dsTarget As New DataSet

        dsTarget = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " select stateCode,stateCode +'('+ltrim(rtrim(name))+')' as [Name] from stateMaster")
        Dim lItem As New ListItem
        lItem.Text = ""
        lItem.Value = "0"

        ddlState.DataValueField = "StateCode"
        ddlState.DataTextField = "Name"
        ddlState.DataSource = dsTarget.Tables(0)
        ddlState.DataBind()
        ddlState.Items.Insert(0, lItem)
        ddlState.Items.FindByValue("0").Selected = True
    End Sub
    Public Sub hLinkAddNew_Onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        tblIndSearch.Visible = True
        'srchTitle.Visible = True
        LoadStateDropDownList()

        Dim loginRoleId As Integer = 0
        Dim loginZoneId As Integer = 0
        Dim loginClusterId As Integer = 0
        Dim loginChapterId As Integer = 0

        loginRoleId = getLoginRoleId()

        Select Case loginRoleId
            Case 0
                Return
            Case Is < 3
                loadDropDownBox(ddlIndSearchChapter, "usp_GetChapters", "Chapter", "ChapterCode", "ChapterID", "", getSessionSelectionID("chapter"))

            Case 3
                If (Not Session("loginZoneId") Is Nothing) Then
                    loginZoneId = CInt(Session("loginZoneId").ToString)
                End If
                loadChapterDropDown(ddlIndSearchChapter, loginZoneId, 0, getSessionSelectionID("chapter"))

            Case 4
                If (Not Session("loginClusterId") Is Nothing) Then
                    loginClusterId = CInt(Session("loginClusterId").ToString)
                End If
                loadChapterDropDown(ddlIndSearchChapter, 0, loginClusterId, getSessionSelectionID("chapter"))
            Case 5
                If (Not Session("loginChapterId") Is Nothing) Then
                    loginChapterId = CInt(Session("loginChapterId").ToString)
                End If
                Dim lItem As New ListItem
                lItem.Text = getChapterName(loginChapterId)
                lItem.Value = loginChapterId
                ddlIndSearchChapter.Items.Clear()
                ddlIndSearchChapter.Items.Add(lItem)
                ddlIndSearchChapter.SelectedIndex = -1
            Case 36
                loadDropDownBox(ddlIndSearchChapter, "usp_GetChapters", "Chapter", "ChapterCode", "ChapterID", "", getSessionSelectionID("chapter"))
            Case Else 'must be a teamlead
                loadDropDownBox(ddlIndSearchChapter, "usp_GetChapters", "Chapter", "ChapterCode", "ChapterID", "", Session("loginChapterId").ToString)
        End Select
    End Sub
    Public Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Public Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Public Sub btnSearch_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim email As String
        Dim firstName As String
        Dim lastName As String
        Dim street As String
        Dim city As String
        Dim stateCode As String = ""
        Dim homePhone As String
        Dim zip As String
        Dim referredBy As String
        Dim liaisonPerson As String
        Dim nsfChapterId As Integer
        Dim ds As DataSet = New DataSet
        Dim strSql As StringBuilder = New StringBuilder()

        lblGrdVolRolesMsg.Visible = False
        lblGrdVolRolesHdg.Text = ""
        grdVolRoles.Visible = False
        tblAddRole.Visible = False

        email = txtEmail.Text
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        street = txtStreet.Text
        city = txtCity.Text
        homePhone = txtHomePhone.Text
        zip = txtZip.Text
        referredBy = txtReferredBy.Text
        liaisonPerson = txtLiaison.Text

        If (ddlState.SelectedValue <> "0") Then
            stateCode = ddlState.SelectedValue.ToString
        End If
        If (ddlIndSearchChapter.SelectedValue <> "0") Then
            nsfChapterId = CInt(ddlIndSearchChapter.SelectedValue.ToString)
        End If
        strSql.Append(" DeletedFlag <> 'yes' and email is not null and len(email) > 0 ")
        If (email.Length > 0) Then
            strSql.Append(" and EMAIL like '%" + email + "%'")
        End If
        If (firstName.Length > 0) Then
            strSql.Append(" and firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            strSql.Append(" and lastName like '%" + lastName + "%'")
        End If
        If (street.Length > 0) Then
            strSql.Append(" and address1 like '%" + street + "%'")
        End If
        If (city.Length > 0) Then
            strSql.Append(" and city like '%" + city + "%'")
        End If
        If (stateCode.Length > 0) Then
            strSql.Append(" and state like '%" + stateCode + "%'")
        End If
        If (zip.Length > 0) Then
            strSql.Append(" and zip like '%" + zip + "%'")
        End If
        If (homePhone.Length > 0) Then
            strSql.Append(" and hPhone like '%" + homePhone + "%'")
        End If
        If lblRoleCat.Text = "Chapter" Then
            strSql.Append(" and chapterId  in (" + getchaptersinCluster(CStr(nsfChapterId)) & ")")
        ElseIf (nsfChapterId > 0) Then
            strSql.Append(" and chapterId =" + CStr(nsfChapterId))
        End If
        If (referredBy.Length > 0) Then
            strSql.Append(" and referredBy like '%" + referredBy + "%'")
        End If
        If (liaisonPerson.Length > 0) Then
            strSql.Append(" and liasonPerson like '%" + liaisonPerson + "%'")
        End If
        strSql.Append(" order by lastname,firstname")
        Session("indSpouseSearchSql") = strSql.ToString()
        loadIndSpouseResultsGrid(True)

        grdVolRoles.Visible = False
        tblAddRole.Visible = False
        lblGrdVolRolesMsg.Visible = False

    End Sub
    Private Function getchaptersinCluster(ByVal Chapterid As Integer) As String
        Try
            Dim Chaptersid As String = ""
            Dim i As Integer = 0
            Dim resultset As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Distinct chapterid from Chapter where Clusterid=(select clusterid from Chapter where chapterid=" & Chapterid & ")")
            While resultset.Read
                If i = 0 Then
                    Chaptersid = Chaptersid & resultset("ChapterID").ToString
                Else
                    Chaptersid = Chaptersid & "," & resultset("ChapterID").ToString
                End If
                i = i + 1
            End While
            resultset.Close()
            Return Chaptersid
        Catch ex As Exception
            Return Chapterid.ToString()
        End Try
    End Function
    Private Sub loadIndSpouseResultsGrid(ByVal reLoad As Boolean)

        Dim ds As DataSet = New DataSet
        Dim strSql As String
        If (Session("indSpouseSearchSql") Is Nothing) Then
            Return
        End If
        strSql = Session("indSpouseSearchSql").ToString
        'lblLoginName.Text = lblLoginName.Text & "<br> " & strSql
        If reLoad = True Then
            Try
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", New SqlParameter("@wherecondition", strSql))
                Session("indSpouseDataSet") = ds
                grdIndResults.CurrentPageIndex = 0
            Catch se As SqlException
                tblIndMessage.Visible = True
                lblIndError.Text = "Error while searching for the records."
            End Try
        Else
            If (Not Session("indSpouseDataSet") Is Nothing) Then
                ds = CType(Session("indSpouseDataSet"), DataSet)
            End If
        End If
        If (ds Is Nothing Or ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0) Then
            tblIndMessage.Visible = True
            lblIndError.Text = "No records matching the search criteria"
            lblIndError.Visible = True
            lblIndMsg.Text = strSql.ToString()
            grdIndResults.DataSource = Nothing
            grdIndResults.Visible = False
            tblGrdIndResults.Visible = False
            grdIndResults.CurrentPageIndex = 0
        Else
            grdIndResults.Visible = True
            grdIndResults.DataSource = ds
            grdIndResults.DataBind()
            lblGrdIndResultsHdg.Text = "4. List of NSF Members"
            tblGrdIndResults.Visible = True
            lblIndError.Visible = False
            lblIndError.Text = ""
            btnGrdIndResultsClose.Visible = True
        End If
        btnGrdVolRolesClose.Visible = False
    End Sub
    Protected Sub grdIndResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdIndResults.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        Dim r As Integer = e.Item.ItemIndex
        Dim lblAddRole As LinkButton = e.Item.FindControl("lbtnAddRole")
        Dim autoMemberId As Integer
        Dim firstName As String = ""
        Dim lastName As String = ""
        Dim email As String = ""
        Dim email_count As Integer = 0
        lblGrdVolRolesMsg.Visible = False
        lblGrdVolRolesHdg.Text = ""
        grdVolRoles.Visible = False
        lblErr.Text = ""
        lblAddRolesHdg.Text = ""
        tblAddRole.Visible = False
        lblAddRoleError.Visible = False
        lblAddRoleError.Text = ""

        If (cmdName = "AddRole" Or cmdName = "ViewRoles") Then
            autoMemberId = CInt(e.Item.Cells(2).Text)
            firstName = e.Item.Cells(3).Text
            lastName = e.Item.Cells(4).Text
            email = e.Item.Cells(5).Text
            Session("grdIndMemberId") = autoMemberId
            If (cmdName = "AddRole") Then
                email_count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From IndSpouse where email='" & email & "'")
                If email_count <= 0 Then
                    tblAddRole.Visible = False
                    lblErr.Text = "Email address doesn't exist for this volunteer."
                    btnGrdVolRolesClose.Visible = False
                    lblAddRole.Enabled = False
                    Exit Sub
                ElseIf email_count > 1 Then
                    tblAddRole.Visible = False
                    lblErr.Text = email & "  -  Email address is not unique for this volunteer"
                    btnGrdVolRolesClose.Visible = False
                    lblAddRole.Enabled = False
                    Exit Sub
                ElseIf email_count = 1 Then
                    lblAddRole.Enabled = True
                    lblErr.Text = ""
                    tblAddRole.Visible = True
                    addNewVolunteer(autoMemberId)
                    lblAddRolesHdg.Text = "5. Add New Role for: " + firstName + " " + lastName
                    btnGrdVolRolesClose.Visible = False
                End If
            ElseIf (cmdName = "ViewRoles") Then
                viewCurrentRoles(autoMemberId)
                tblGrdVolRoles.Visible = True
                lblGrdVolRolesHdg.Text = "6. Current Roles For: <font color=DarkSeaGreen>" + firstName + " " + lastName + "</font>"
                lblAddRolesHdg.Text = ""
            End If
        End If
    End Sub
    Private Function addNewVolunteer(ByVal memberId As Integer) As Boolean

        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        Dim zoneId As Integer = 0
        Dim zoneCode As String = ""
        Dim clusterId As Integer = 0
        Dim clusterCode As String = ""
        Dim chapterId As Integer = 0
        Dim chapterCode As String = ""
        Dim national As String = "No"
        Dim firstName As String = Nothing
        Dim lastName As String = Nothing
        Dim finals As String = "No"
        Dim indiaChapter As String = "No"
        Dim roleSelect As String = ""

        If (roleCat = "National") Then
            national = "Yes"
            roleSelect = " [national]='Y'"
        ElseIf (roleCat = "Finals") Then
            finals = "Yes"
            roleSelect = " finals='Y'"
        ElseIf (roleCat = "IndiaChapter") Then
            indiaChapter = "Yes"
            roleSelect = " indiaChapter='Y'"
        ElseIf (roleCat = "Zonal") Then
            zoneId = CInt(Session("searchZoneId").ToString())
            If (zoneId > 0) Then
                zoneCode = getZoneName(zoneId)
            End If
            roleSelect = " zonal='Y'"
        ElseIf (roleCat = "Cluster") Then
            clusterId = CInt(Session("searchClusterId").ToString())
            If (clusterId > 0) Then
                clusterCode = getClusterName(clusterId)
            End If
            roleSelect = " cluster='Y'"
        ElseIf (roleCat = "Chapter") Then
            chapterId = CInt(Session("searchChapterId").ToString())
            If (chapterId > 0) Then
                chapterCode = getChapterCode(chapterId)
            End If
            If chapterId = 1 Then
                finals = "Yes"
                roleSelect = " Finals='Y'"
            Else
                roleSelect = " Chapter='Y'"
            End If
        End If

        lblARMemberId.Text = memberId

        Dim ds As DataSet = New DataSet
        Try
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstname,lastname from indspouse where automemberId=" + CStr(memberId))
            If (ds.Tables(0).Rows.Count > 0) Then
                firstName = ds.Tables(0).Rows(0)("FirstName")
                lastName = ds.Tables(0).Rows(0)("LastName")
            End If
        Catch
        End Try

        'roles
        If (Not Session("searchRoleCode") Is Nothing) Then
            If (Session("searchRoleCode").ToString = "All Roles") Then
                Dim roleSql As String
                roleSql = "select roleId,selection from role where roleId >="
                'load roles dropdown. 
                Dim startingAllowedRoleId As Integer = 10000
                Select Case getLoginRoleId()
                    Case 1 'admin
                        startingAllowedRoleId = 1
                    Case 2 'national
                        startingAllowedRoleId = 3
                    Case 3 'zonal
                        startingAllowedRoleId = 5
                    Case 4 'cluster
                        startingAllowedRoleId = 5
                    Case 5 'chapterC
                        startingAllowedRoleId = 6
                    Case 36 'FinalCoreT
                        startingAllowedRoleId = 3 'same as NationalC
                    Case Else 'must be TeamLead
                        If (getLoginTeamLead() = "Y") Then
                            roleSql = "select roleId,selection from role where roleId ="
                            startingAllowedRoleId = getLoginRoleId()
                        End If
                End Select

                roleSql = roleSql + CStr(startingAllowedRoleId)
                If (roleSelect.Trim().Length() > 0) Then
                    roleSql = roleSql + " and " + roleSelect
                End If
                roleSql = roleSql + " order by selection"

                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, roleSql)  '"select roleId,selection from role where roleid >= " + CStr(startingAllowedRoleId))
                    If (ds.Tables(0).Rows.Count > 0) Then
                        ddlARRole.DataSource = ds.Tables(0)
                        ddlARRole.DataBind()

                        Dim liItem As New ListItem
                        liItem.Text = "Select A Role"
                        liItem.Value = "0"
                        ddlARRole.Items.Insert(0, liItem)
                    End If
                Catch
                    'error handling
                End Try
                lblARRole.Visible = False
                ddlARRole.Visible = True
            Else
                lblARRole.Text = Session("searchRoleCode").ToString
                lblARRole1.Text = getRoleName(lblARRole.Text)
                ddlARRole.Visible = False
                lblARRole1.Visible = True
            End If
        End If

            'clear prod drop downs
            'ddlARProdGroup.Items.Clear()
            'ddlARProd.Items.Clear()

        'Event(s)

        If (roleCat = "Chapter") Then
            If (getLoginRoleId() > 4 And getLoginTeamLead().ToUpper = "Y") Then  'if TeamLead, then only display login user's event id
                ddlAREvent.Enabled = False
                ddlAREvent.Visible = False
                lblAREvent.Visible = True
                'lblAREvent.Text = "Chapter"
                lblAREvent.Text = getEventName(getLoginEventId()) 'login user's event id
                ' LoadProductGroupDropDownList(ddlARProdGroup, getLoginEventId(), 0)
            Else
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select eventId,eventCode from event where eventid <> ''") 'finals ' 13/08/2012
                    If (ds.Tables(0).Rows.Count > 0) Then
                        ddlAREvent.DataSource = ds.Tables(0)
                        ddlAREvent.DataBind()
                        Dim liItem As New ListItem
                        liItem.Text = "Select An Event"
                        liItem.Value = "0"
                        ddlAREvent.Items.Insert(0, liItem)
                        If chapterId > 1 Then
                            ddlAREvent.SelectedIndex = ddlAREvent.Items.IndexOf(ddlAREvent.Items.FindByText("Chapter"))
                        ElseIf chapterId = 1 Then
                            ddlAREvent.SelectedIndex = ddlAREvent.Items.IndexOf(ddlAREvent.Items.FindByText("Finals"))
                        End If
                        ddlAREvent.Enabled = False
                        ddlAREvent.Visible = True
                        ddlAREvent.Enabled = True
                        lblAREvent.Visible = False
                    End If
                Catch
                    'error handling
                End Try
            End If
        ElseIf (roleCat = "Finals") Then
            ddlAREvent.Enabled = False
            ddlAREvent.Visible = False
            lblAREvent.Visible = True
            lblAREvent.Text = "Finals"
            ' LoadProductGroupDropDownList(ddlARProdGroup, 1, 0)  'product groups for finals
        Else
            ddlAREvent.Enabled = False
            ddlAREvent.Visible = False
        End If

            ''load product drop downs
            'If (getLoginRoleId() < 8) Then
            '    rowProduct.Visible = False
            'Else 
            '   rowProduct.Visible = True
            'End If
            'writeaccess, authorization,agentflag rows
            If (getLoginRoleId() = 1) Then
                ' rowAdmin.Visible = True
                'rowAdmin2.Visible = True
            Else
                rowAdmin.Visible = False
                rowAdmin2.Visible = False
            End If

            'eventYear
            If (roleCat = "Finals" Or roleCat = "Zonal" Or roleCat = "Cluster" Or roleCat = "Chapter") Then
                rowEventYear.Visible = True
            Else
                rowEventYear.Visible = False
            End If

            'TeamLead
            If (getLoginRoleId() > 5 And getLoginRoleId() <> 36 And (getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes")) Then
                lblARTeamLead.Text = "No"
                lblARTeamLead.Visible = True
                ddlARTeamLead.Visible = False
            Else
                lblARTeamLead.Visible = False
            End If

            tblAddRole.Visible = True
            Page.SetFocus(tblAddRole)
            lblARFirst.Text = firstName
            lblARLast.Text = lastName
            lblARZone.Text = zoneCode
            lblARChapter.Text = chapterCode
            lblARCluster.Text = clusterCode
            lblARNational.Text = national
            lblARFinals.Text = finals
            lblARIndiaChapter.Text = indiaChapter
    End Function
    Protected Sub ddlAREvent_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAREvent.SelectedIndexChanged
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (ddlTemp.SelectedValue <> "0") Then
            Dim eventId As Integer = CInt(ddlTemp.SelectedItem.Value)
            ' LoadProductGroupDropDownList(ddlARProdGroup, eventId, 0)
        End If
    End Sub
    Protected Sub ddlARRole_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (ddlTemp.SelectedValue <> "0") Then
            Dim roleId As Integer = CInt(ddlARRole.SelectedItem.Value)
            If (roleId = 36 And getLoginRoleId() = 36) Then
                ddlARTeamLead.SelectedValue = "N"
                ddlARTeamLead.Enabled = False
            Else
                ddlARTeamLead.Enabled = True
            End If
        End If
    End Sub
    Protected Sub ddlARProdGroup_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (ddlTemp.SelectedValue <> "0") Then
            Dim prodGroupId As Integer = CInt(ddlTemp.SelectedItem.Value)
            Dim eventId As Integer = CInt(ddlAREvent.SelectedItem.Value)
            If (eventId = 0) Then
                If (getLoginRoleId() = 36) Then
                    eventId = 1
                End If
            End If
            If (eventId = 0) Then
                lblAddRoleError.Text = "Please select the Event code before selecting the product Group"
                Return
            End If
            LoadProductDropDownList(ddlARProd, prodGroupId, eventId, 0)
        End If

    End Sub
    Private Sub viewCurrentRoles(ByVal memberId As Integer)
        'get the list of roles from volunteer table for the given member
        btnGrdVolRolesClose.Visible = False
        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_getCurrentRoles", New SqlParameter("@memberId", memberId))
            If (ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0) Then
                Dim hdg As String = "6. Current Roles for " + ds.Tables(0).Rows(0)("firstName").ToString + " " + ds.Tables(0).Rows(0)("lastName").ToString
                grdVolRoles.DataSource = ds.Tables(0)
                grdVolRoles.DataBind()
                grdVolRoles.Visible = True
                tblGrdVolRoles.Visible = True
                lblGrdVolRolesMsg.Text = ""
                lblGrdVolRolesHdg.Text = hdg
                lblGrdVolRolesHdg.Visible = True
                btnGrdVolRolesClose.Visible = True

            Else
                tblGrdVolRoles.Visible = True
                grdVolRoles.Visible = False
                lblGrdVolRolesHdg.Text = ""
                lblGrdVolRolesMsg.Text = "Currently there are no roles for this member"
                lblGrdVolRolesMsg.ForeColor = Color.Red
                lblGrdVolRolesMsg.Visible = True
            End If
        Catch ex As Exception
            lblGrdVolRolesMsg.Text = "An error while accessing the record." + ex.ToString
            lblGrdVolRolesMsg.Visible = True
            lblGrdVolRolesMsg.ForeColor = Color.Red
        End Try
    End Sub
    Protected Sub grdIndResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdIndResults.EditCommand
        grdIndResults.EditItemIndex = CInt(e.Item.ItemIndex)
        Dim automemberId As Integer
        Dim name As String = e.CommandName

        automemberId = e.Item.Cells(1).Text
        Session("rowAutoMemberId") = automemberId
        loadIndSpouseResultsGrid(False)
        'ClientScript.RegisterStartupScript(GetType(String), "SetFocus", "<script language=""JavaScript"">document.getElementByID(""" + SelectedRecord + """).focus();</script>")
        'ClientScript.RegisterStartupScript(GetType(String), "SetFocus", "<script type=""text/javascript"">WebForm_AutoFocus('_ctl0_Content_main_grdIndResults');document.getElementByID('_ctl0_Content_main_grdIndResults').focus();</script>")
        grdIndResults.Focus()
    End Sub
    Protected Sub grdIndResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdIndResults.PageIndexChanged
        grdIndResults.CurrentPageIndex = e.NewPageIndex
        grdIndResults.EditItemIndex = -1
        loadIndSpouseResultsGrid(False)
    End Sub
    Protected Sub grdVolRoles_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdVolRoles.PageIndexChanged
        grdVolRoles.CurrentPageIndex = e.NewPageIndex
        grdVolRoles.EditItemIndex = -1
        Dim memberId As Integer = CInt(Session("grdIndMemberId").ToString)
        viewCurrentRoles(memberId)
        grdVolRoles.Controls(0).Focus()
    End Sub
    Protected Sub grdVolRoles_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdVolRoles.PreRender

    End Sub
    Protected Sub btnAddRole_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim memberId As Integer = CInt(lblARMemberId.Text)
        Dim firstName As String = lblARFirst.Text
        Dim lastName As String = lblARLast.Text
        Dim national As String = lblARNational.Text.Substring(0, 1)
        Dim finals As String = lblARFinals.Text.Substring(0, 1)
        Dim indiaChapter As String = lblARIndiaChapter.Text.Substring(0, 1)
        Dim teamLead As String = ddlARTeamLead.SelectedValue
        Dim teamLead2 As String = lblARTeamLead.Text
        Dim zoneid As Integer = 0
        Dim zonecode As String = Nothing
        Dim clusterId As Integer = 0
        Dim clustercode As String = Nothing
        Dim chapterId As Integer = 0
        Dim chaptercode As String = Nothing
        Dim eventId As Integer = CInt(ddlAREvent.SelectedValue)
        Dim eventcode As String = Nothing
        'Dim roleId As Integer = CInt(ddlARRole.SelectedValue)
        Dim roleId As Integer = 0
        Dim rolecode As String = Nothing 'Session("searchRoleCode").ToString
        Dim eventYear As Integer = CInt(ddlAREventYear.SelectedValue)
        Dim prodid As Integer = 0
        Dim prodGroupId As Integer = 0
        Dim prodCode As String = Nothing
        Dim prodGroupCode As String = Nothing
        Dim writeAccess As String = Nothing
        Dim authorization As String = Nothing
        Dim agentFlag As String = Nothing
        Dim yahooGroup As String = Nothing
        Dim indiaChapterName As String = Nothing
        Dim bErr As Boolean = False 'for session timeout flag

        If ((teamLead = Nothing Or teamLead = "") And (Not teamLead2 Is Nothing)) Then
            teamLead = lblARTeamLead.Text.Substring(0, 1)
            'teamLead = teamLead2
        Else
            teamLead = teamLead.Substring(0, 1)
        End If

        lblAddRoleError.Visible = False
        lblAddRoleError.Text = ""

        If (Not Session("searchRoleCode") Is Nothing) Then
            rolecode = Session("searchRoleCode").ToString
        Else
            bErr = True
        End If
        If (bErr = True) Then
            showTimeOutMsg()
            Return
        End If

        If (rolecode = "All Roles") Then
        	roleId = ddlARRole.SelectedItem.Value
        Else
        	roleId = CInt(Session("searchRoleId").ToString)
        End If

        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        If (roleCat Is Nothing) Then
            Return
        End If

        If (roleId < 1) Then
            lblAddRoleError.Visible = True
            lblAddRoleError.Text = "Please make sure you have selected a Role for the volunteer"
            Return
        ElseIf (roleCat = "Chapter" And eventId < 1 And getLoginTeamLead() <> "Y") Then
            lblAddRoleError.Visible = True
            lblAddRoleError.Text = "Please make sure you have selected an Event."
            Return
        Else
            lblAddRoleError.Visible = False
            lblAddRoleError.Text = ""
        End If

        eventcode = ddlAREvent.SelectedItem.Text
        'rolecode = ddlARRole.SelectedItem.Text
        If (roleCat = "Zonal") Then
            zoneid = CInt(Session("searchZoneId").ToString())
            If (zoneid > 0) Then
                zonecode = getZoneCode(zoneid)
            End If
        ElseIf (roleCat = "Cluster") Then
            clusterId = CInt(Session("searchClusterId").ToString())
            If (clusterId > 0) Then
                clustercode = getClusterCode(clusterId)
            End If
        ElseIf (roleCat = "Chapter") Then
            chapterId = CInt(Session("searchChapterId").ToString())
            If (chapterId > 0) Then
                chaptercode = getChapterCode(chapterId)
            End If
            '** Ferdine Needs Attension 26/03/2010
            If (getLoginTeamLead() = "Y" And getLoginRoleId() <> 36 And getLoginRoleId() >= 5) Then
                eventId = getLoginEventId()
                eventcode = getEventCode(eventId)
            Else
                eventcode = ddlAREvent.SelectedItem.Text
                eventId = CInt(ddlAREvent.SelectedValue)
            End If
        ElseIf (roleCat = "Finals") Then
            eventcode = "Finals"
            eventId = 1
            chapterId = 1
            chaptercode = getChapterName(chapterId)
        End If
        '
        If (zoneid = 0 And chapterId = 0 And clusterId = 0 And national = "N" And finals = "N" And indiaChapter = "n") Then
            lblAddRoleError.Visible = True
            lblAddRoleError.Text = "zone /chapter /cluster /national / indiaChapter - one of them should be supplied"
            Return
        End If
        
        'If (getLoginRoleId() = 1) Then
        '    writeAccess = ddlARWriteAccess.SelectedValue.Substring(0, 1)
        '    authorization = ddlARAuthorization.SelectedValue.Substring(0, 1)
        '    agentFlag = ddlARAgentFlag.SelectedValue.Substring(0, 1)
        'End If

        'If (getLoginRoleId() < 8 Or getLoginTeamLead().ToLower = "y" Or getLoginTeamLead().ToLower = "yes") Then '????should this be available for teamlead and finalsCoreT
        'If (ddlARProd.SelectedIndex > 0) Then
        '    prodid = ddlARProd.SelectedValue
        '    prodCode = getProductCode(prodid)
        'End If

        'If (ddlARProdGroup.SelectedIndex > 0) Then
        '    prodGroupId = ddlARProdGroup.SelectedValue
        '    prodGroupCode = getProductGroupCode(prodGroupId)
        'End If
        'End If




        'Dim isDuplicate As Boolean


        'isDuplicate = isDuplicateRole(0, memberId, roleId, eventId, clusterId, zoneid, chapterId, prodGroupId, prodid, national, finals, eventYear)
        'If (isDuplicate = True) Then
        '    lblAddRoleError.Visible = True
        '    lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
        '    Return
        'End If

        'If (Not teamLead Is Nothing And teamLead = "Y") Then
        '    isDuplicate = isDuplicateTeamLead(0, teamLead, roleId, eventId, clusterId, zoneid, chapterId, prodGroupId, prodid, national, finals, eventYear)
        '    If (isDuplicate = True) Then
        '        lblAddRoleError.Visible = True
        '        lblAddRoleError.Text = "Error: Duplicate TeamLead."
        '        Return
        '    End If
        'End If
        '** ferdine Silva
        If (roleCat = "Zonal") Then
            If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from volunteer where roleid=" & roleId & " and MemberId=" & memberId & " and Zoneid=" & zoneid & "") > 0) Then
                lblAddRoleError.Visible = True
                lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                Return
            Else
                lblAddRoleError.Visible = False
                lblAddRoleError.Text = ""
            End If
        ElseIf (roleCat = "Cluster") Then
            If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from volunteer where roleid=" & roleId & " and MemberId=" & memberId & " and clusterid=" & clusterId & "") > 0) Then
                lblAddRoleError.Visible = True
                lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                Return
            Else
                lblAddRoleError.Visible = False
                lblAddRoleError.Text = ""
            End If
        ElseIf (roleCat = "Chapter") Then
            If roleId = 5 Then
                If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from volunteer where roleid=" & roleId & " and chapterid=" & chapterId & "") > 0) Then
                    lblAddRoleError.Visible = True
                    lblAddRoleError.Text = "Error: Duplicate Coordinator for the Chapter."
                    Return
                End If
            ElseIf (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from volunteer where roleid=" & roleId & " and chapterid=" & chapterId & " and MemberId=" & memberId & "") > 0) Then
                lblAddRoleError.Visible = True
                lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                Return
            Else
                lblAddRoleError.Visible = False
                lblAddRoleError.Text = ""
            End If
            ElseIf (roleCat = "Finals") Then
                If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "SELECT count(*)  FROM  Volunteer where Finals = 'Y' AND roleid=" & roleId & " AND MemberId = " & memberId & "") > 0) Then
                    lblAddRoleError.Visible = True
                    lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                    Return
                Else
                    lblAddRoleError.Visible = False
                    lblAddRoleError.Text = ""
                End If
            ElseIf (roleCat = "National") Then
                If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "SELECT count(*)  FROM  Volunteer where [National] = 'Y' AND roleid=" & roleId & " AND MemberId = " & memberId & "") > 0) Then
                    lblAddRoleError.Visible = True
                    lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                    Return
                Else
                    lblAddRoleError.Visible = False
                    lblAddRoleError.Text = ""
                End If
            ElseIf (roleCat = "IndiaChapter") Then
                If (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "SELECT count(*)  FROM  Volunteer where IndiaChapter = 'Y' AND roleid=" & roleId & " AND MemberId = " & memberId & "") > 0) Then
                    lblAddRoleError.Visible = True
                    lblAddRoleError.Text = "Error: Duplicate role for the volunteer."
                    Return
                Else
                    lblAddRoleError.Visible = False
                    lblAddRoleError.Text = ""
                End If
            End If


        'Insert Coding for Detecting Duplicates

        'insert the record
        Dim param(25) As SqlParameter
        param(0) = New SqlParameter("@memberId", memberId)
        param(1) = New SqlParameter("@roleid", roleId)
        param(2) = New SqlParameter("@teamlead", teamLead)
        param(3) = New SqlParameter("@eventyear", eventYear)
        If (eventId > 0) Then
            param(4) = New SqlParameter("@eventid", eventId)
            param(5) = New SqlParameter("@eventcode", eventcode)
        Else
            param(4) = New SqlParameter("@eventid", DBNull.Value)
            param(5) = New SqlParameter("@eventcode", DBNull.Value)
        End If

        If (chapterId > 0) Then
            param(6) = New SqlParameter("@chapterid", chapterId)
            param(7) = New SqlParameter("@chaptercode", chaptercode)
        Else
            param(6) = New SqlParameter("@chapterid", DBNull.Value)
            param(7) = New SqlParameter("@chaptercode", DBNull.Value)
        End If
        If (Not national Is Nothing And national = "Y") Then
            param(8) = New SqlParameter("@national", national)
        Else
            param(8) = New SqlParameter("@national", DBNull.Value)
        End If

        If (zoneid > 0) Then
            param(9) = New SqlParameter("@zoneid", zoneid)
            param(10) = New SqlParameter("@zonecode", zonecode)
        Else
            param(9) = New SqlParameter("@zoneid", DBNull.Value)
            param(10) = New SqlParameter("@zonecode", DBNull.Value)
        End If
        If (clusterId > 0) Then
            param(11) = New SqlParameter("@clusterid", clusterId)
            param(12) = New SqlParameter("@clustercode", clustercode)
        Else
            param(11) = New SqlParameter("@clusterid", DBNull.Value)
            param(12) = New SqlParameter("@clustercode", DBNull.Value)
        End If

        If (Not finals Is Nothing And finals = "Y") Then
            param(13) = New SqlParameter("@finals", finals)
        Else
            param(13) = New SqlParameter("@finals", DBNull.Value)
        End If
        If (Not indiaChapter Is Nothing And indiaChapter = "Y") Then
            param(14) = New SqlParameter("@indiaChapter", indiaChapter)
        Else
            param(14) = New SqlParameter("@indiaChapter", DBNull.Value)
        End If

        If (prodGroupId > 0) Then
            param(15) = New SqlParameter("@productGroupId", prodGroupId)
            param(16) = New SqlParameter("@productGroupCode", prodGroupCode)
        Else
            param(15) = New SqlParameter("@productGroupId", DBNull.Value)
            param(16) = New SqlParameter("@productGroupCode", DBNull.Value)
        End If

        If (prodid > 0) Then
            param(17) = New SqlParameter("@productid", prodid)
            param(18) = New SqlParameter("@productCode", prodCode)
        Else
            param(17) = New SqlParameter("@productid", DBNull.Value)
            param(18) = New SqlParameter("@productCode", DBNull.Value)
        End If

        If (Not agentFlag Is Nothing And agentFlag = "Y") Then
            param(19) = New SqlParameter("@agentFlag", agentFlag)
        Else
            param(19) = New SqlParameter("@agentFlag", DBNull.Value)
        End If

        If (Not writeAccess Is Nothing And writeAccess = "Y") Then
            param(20) = New SqlParameter("@writeAccess", writeAccess)
        Else
            param(20) = New SqlParameter("@writeAccess", DBNull.Value)
        End If

        If (Not authorization Is Nothing And authorization = "Y") Then
            param(21) = New SqlParameter("@authorization", authorization)
        Else
            param(21) = New SqlParameter("@authorization", DBNull.Value)
        End If

        If (Not yahooGroup Is Nothing And yahooGroup = "Y") Then
            param(22) = New SqlParameter("@yahoogroup", yahooGroup)
        Else
            param(22) = New SqlParameter("@yahoogroup", DBNull.Value)
        End If

        If (Not indiaChapterName Is Nothing) Then
            param(23) = New SqlParameter("@indiachapterName", indiaChapterName)
        Else
            param(23) = New SqlParameter("@indiachapterName", indiaChapterName)
        End If
        param(24) = New SqlParameter("@createdBy", CInt(Session("LoginId").ToString()))

        Dim ret As Integer = Nothing
        Try
            ret = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "dbo.usp_insertVolunteer", param)
        Catch ex As Exception
            ' MsgBox(ex.ToString())
            Response.Write(ex.ToString)
            lblAddRoleError.Visible = True
            lblAddRoleError.Text = "Error while adding the new role."
            Return
        End Try
        loadGrid(True)
        tblAddRole.Visible = False
        'grdVolRoles.Visible = True
        lblGrdVolRolesHdg.Text = ret
        viewCurrentRoles(memberId)
        Me.SetFocus(grdVolResults)
        'grdVolResults.Focus()
        'display the vol id
    End Sub
    Protected Sub btnCancel_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        tblAddRole.Visible = False
        grdVolRoles.Focus()
    End Sub

    Private Function isDuplicateRole(ByVal volId As Integer, ByVal memberid As Integer, ByVal roleId As Integer, ByVal eventId As Integer, ByVal clusterId As Integer, ByVal zoneId As Integer, ByVal chapterId As Integer, ByVal prodGroupId As Integer, ByVal prodId As Integer, ByVal national As String, ByVal finals As String, ByVal eventYear As Integer) As Boolean
        'in the update mode volId will have the id of the rec that is being updated
        Dim ret As Boolean = True

        Dim sb As New StringBuilder
        sb.Append(" memberid=" + CStr(memberid))
        sb.Append(" and roleid=" + CStr(roleId))
        sb.Append(" and eventid=" + CStr(eventId))
        If (clusterId > 0) Then
            sb.Append(" and clusterid=" + CStr(clusterId))
        End If
        If (zoneId > 0) Then
            sb.Append(" and zoneid=" + CStr(zoneId))
        End If
        If (chapterId > 0) Then
            sb.Append(" and chapterid=" + CStr(chapterId))
        End If
        If (prodGroupId > 0) Then
            sb.Append(" and productGroupId=" + CStr(prodGroupId))
        End If
        If (prodId > 0) Then
            sb.Append(" and productId=" + CStr(prodId))
        End If

        If (Not national Is Nothing And national = "Y") Then
            sb.Append(" and [national]='" + national + "'")
        End If
        If (Not finals Is Nothing And finals = "Y") Then
            sb.Append(" and finals='" + finals + "'")
        End If
        If (eventYear > 0) Then
            sb.Append(" and eventYear=" + CStr(eventYear))
        End If
        If (volId > 0) Then
            sb.Append(" and volunteerid <> " + CStr(volId))
        End If

        Dim strSql As String = sb.ToString()

        Dim ds As New DataSet
        Try
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_SelectWhereVolunteer", New SqlParameter("@wherecondition", sb.ToString()))
        Catch se As SqlException
            lblAddRoleError.Visible = True
            lblAddRoleError.Text = sb.ToString()
        End Try
        If (ds Is Nothing Or ds.Tables.Count < 1) Then
            ret = False
        ElseIf ds.Tables(0).Rows.Count = 0 Then  'hack for empty table with zero rows
            ret = False
        ElseIf (ds.Tables(0).Rows.Count > 0) Then
            ret = True
        End If

        Return ret
    End Function

    Private Function isDuplicateTeamLead(ByVal volId As Integer, ByVal teamLead As String, ByVal roleId As Integer, ByVal eventId As Integer, ByVal clusterId As Integer, ByVal zoneId As Integer, ByVal chapterId As Integer, ByVal prodGroupId As Integer, ByVal prodId As Integer, ByVal national As String, ByVal finals As String, ByVal eventYear As Integer) As Boolean
        'in the update mode volId will have the id of the rec that is being updated

        Dim ret As Boolean = True
        Dim sb As New StringBuilder
        If (Not teamLead Is Nothing And teamLead = "Y") Then
            sb.Append(" teamlead='" + teamLead + "'")
            sb.Append(" and roleid=" + CStr(roleId))
            sb.Append(" and eventid=" + CStr(eventId))
            If (clusterId > 0) Then
                sb.Append(" and clusterid=" + CStr(clusterId))
            End If
            If (zoneId > 0) Then
                sb.Append(" and zoneid=" + CStr(zoneId))
            End If
            If (chapterId > 0) Then
                sb.Append(" and chapterid=" + CStr(chapterId))
            End If
            If (prodGroupId > 0) Then
                sb.Append(" and productGroupId=" + CStr(prodGroupId))
            End If
            If (prodId > 0) Then
                sb.Append(" and productId=" + CStr(prodId))
            End If

            If (Not national Is Nothing And national = "Y") Then
                sb.Append(" and [national]='" + national + "'")
            End If
            If (Not finals Is Nothing And finals = "Y") Then
                sb.Append(" and finals='" + finals + "'")
            End If
            If (eventYear > 0) Then
                sb.Append(" and eventYear=" + CStr(eventYear))
            End If
            If (volId > 0) Then
                sb.Append(" and volunteerid <> " + CStr(volId))
            End If
            Dim strSql As String = sb.ToString()
            Dim ds As New DataSet
            Try
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_SelectWhereVolunteer", New SqlParameter("@wherecondition", sb.ToString()))
            Catch se As SqlException
                lblAddRoleError.Visible = True
                lblAddRoleError.Text = sb.ToString()
            End Try
            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                ret = False
            ElseIf ds.Tables(0).Rows.Count = 0 Then 'hack for empty table with zero rows
                ret = False
            ElseIf (ds.Tables(0).Rows.Count > 0) Then
                ret = True
            End If
        Else
            ret = False 'no need to check if TeamLead is 'N'
        End If
        Return ret
    End Function
    Protected Sub btnGrdIndResultsClose_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        grdIndResults.Visible = False
        btnGrdIndResultsClose.Visible = False
        lblGrdIndResultsHdg.Text = ""
        lblIndError.Text = ""
    End Sub
    Protected Sub btnGrdVolRolesClose_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        grdVolRoles.Visible = False
        btnGrdVolRolesClose.Visible = False
        lblGrdVolRolesHdg.Text = ""
    End Sub
    Protected Sub btnIndClose_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        tblIndSearch.Visible = False
    End Sub

    Protected Sub grdVolResults_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdVolResults.SelectedIndexChanged

    End Sub
End Class



