Imports System
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports NorthSouth.BAL
Imports System.Net.Mail
Imports nsf.Data.SqlClient


Namespace VRegistration


Partial Class reg_Contestants
    Inherits System.Web.UI.Page
        'Private us As CultureInfo = New CultureInfo("en-US")
    Protected Hyperlink1 As System.Web.UI.WebControls.HyperLink
    Public nRegFee As Decimal = 0

        Dim cnTemp As SqlConnection

        ' Dim objEntChapter As New nsf.Entities.Chapter
        Dim objEntContestant As New nsf.Entities.Contestant
        Dim objEntContest As New nsf.Entities.Contest
        ' Dim objSqlChapterProviderBase As New SqlChapterProviderBase
        Dim objSqlContestProviderBase As New SqlContestProviderBase
        Dim objSqlContestantProviderBase As New SqlContestantProviderBase
        ' Dim objSqlIndSpouseProviderBase As New SqlIndSpouseProviderBase
        ' Dim objSqlOrgInfoProviderBase As New SqlOrganizationInfoProviderBase


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ''Debug 
            'Session("ParentID") = 4


            If Session("LoggedIn") <> "LoggedIn" Then
                Server.Transfer("login.aspx")
            End If

            cnTemp = New SqlConnection(Application("ConnectionString"))

            If (Page.IsPostBack = False) Then
                DisplayChildren()
                DisplayContests()
                If Application("ContestType") = 1 Then 'National
                    Dim redirectURL As String

                    If Request.ServerVariables("Server_Name") <> "localhost" Then
                        redirectURL = "/app6/Parents/RegistrationGuestChaperone.aspx"
                    Else
                        redirectURL = "/VRegistration/Parents/RegistrationGuestChaperone.aspx"
                    End If
                    hlPayNow.NavigateUrl = redirectURL
                    hlinkParent.Visible = True
                End If
            End If

            'old code   
            '    If Session("LoggedIn") <> "LoggedIn" Then
            '        Server.Transfer("login.aspx")
            '    End If
            'Session("ContestDate1") = Nothing
            'Session("ContestDate2") = Nothing

            'If Not (Session("IndID") = Nothing) Then
            '    Session("ParentID") = Session("IndID")
            'End If
            '' Put user code to initialize the page here
            'If (Page.IsPostBack = False) Then
            '    Session("LATEFEE") = 0
            '    'Response.Write("<script>window.parent.frames[0].location='menu.aspx';</script>");
            '    DisplayChildren()
            '    DisplayContests()
            '    If Application("ContestType") = 1 Then
            '        Dim redirectURL As String

            '        If Request.ServerVariables("Server_Name") <> "localhost" Then
            '            redirectURL = "/app6/Parents/RegistrationGuestChaperone.aspx"
            '        Else
            '            redirectURL = "/VRegistration/Parents/RegistrationGuestChaperone.aspx"
            '        End If
            '        hlPayNow.NavigateUrl = redirectURL
            '        hlinkParent.Visible = True
            '    End If
            'End If
    End Sub

    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        '
        ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
        '
        InitializeComponent()
        MyBase.OnInit(e)
    End Sub

    ' <summary>
    ' Required method for Designer support - do not modify
    ' the contents of this method with the code editor.
    ' </summary>
    Private Sub InitializeComponent()

    End Sub

        Private Sub DisplayChildren_Old()

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            Dim str As String

            str = "MemberId='" & Session("IndID") & "'"
            If Not Session("SpouseId") = "0" Then
                str = str & " and SpouseID='" & Session("SpouseId") & "'"
            End If

            Dim objChild As New Child
            Dim dsChild As New DataSet

            objChild.SearchChildWhere(Application("ConnectionString"), dsChild, str)

            If dsChild.Tables.Count > 0 Then
                dgChildren.DataSource = dsChild.Tables(0)
                dgChildren.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
            End If

            cmd = Nothing
            conn = Nothing
        End Sub

        Private Sub DisplayChildren()
            Dim objChild As New Child
            Dim dsChild As New DataSet
            Dim strWhere As String

            strWhere = "MemberId='" & Session("ParentID") & "' or SpouseID='" & Session("ParentId") & "'"
            objChild.SearchChildWhere(cnTemp.ConnectionString, dsChild, strWhere)

            If dsChild.Tables.Count > 0 Then
                dgChildren.DataSource = dsChild.Tables(0)
                dgChildren.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
            Else
                dgChildren.Visible = False
            End If
        End Sub

    Private Sub dgChildren_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildren.ItemDataBound
        Dim lbtn1 As LinkButton = CType(e.Item.FindControl("lbDisplayContest"), LinkButton)
        Dim lblDisplayText As Label = CType(e.Item.FindControl("lblDisplayContestText"), Label)
        Dim hl2 As HyperLink = CType(e.Item.FindControl("hlUpdateInfo"), HyperLink)
        Dim lblUpdateText As Label = CType(e.Item.FindControl("lblUpdateInfoText"), Label)
        If ((Not (lbtn1) Is Nothing) _
                    AndAlso ((Not (hl2) Is Nothing) _
                    AndAlso ((Not (lblDisplayText) Is Nothing) _
                    AndAlso (Not (lblUpdateText) Is Nothing)))) Then
            Dim Grade As Integer = -1
            Dim dob As DateTime = Convert.ToDateTime("1/1/1900")
            Dim ChildNumber As Integer

            dob = CType(DataBinder.Eval(e.Item.DataItem, "Date_of_Birth"), DateTime)
            Grade = CType(DataBinder.Eval(e.Item.DataItem, "Grade"), Integer)
            ChildNumber = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
            If (Grade >= 0) Then
                lbtn1.CommandArgument = (dob.ToShortDateString + ("," _
                            + (Grade.ToString + ("," + ChildNumber.ToString))))
                hl2.Visible = False
                lblUpdateText.Visible = False
            Else
                hl2.NavigateUrl = (hl2.NavigateUrl + ("?ChildNumber=" + ChildNumber.ToString))
                lbtn1.Visible = False
                lblDisplayText.Visible = False
            End If
        End If
    End Sub

        Private Sub DisplayContests()
            Panel2.Visible = True
            Panel3.Visible = False
            lblMessage.Text = ""

            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}

            Dim prmArray(3) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ParentID"
            prmArray(0).Value = Session("ParentID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = Application("ContestYear")
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter
            prmArray(2).ParameterName = "@LateFeeDate"
            prmArray(2).Value = IIf(Application("ContestType") = "1", Application("LateRegistrationDate"), DBNull.Value)
            prmArray(2).Direction = ParameterDirection.Input

            Select Case Application("ContestType").ToString
                Case 1
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)
                Case 2
                    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)
            End Select
            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If
            Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now
            dvSelectedContes = dsContestant.Tables(0).DefaultView
            dvSelectedContes.RowFilter = " RegistrationDeadline <> '1900/01/01' AND RegistrationDeadline > " & "'" & strToday.ToShortDateString & "'"

            If dsContestant.Tables.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                Session("ContestsSelected") = ""
                dgSelectedContests.DataBind()
                dgSelectedContests.Visible = (dsContestant.Tables(0).Rows.Count > 0)
                'ViewState("SelectedContests") = New ArrayList
            End If

            RegFee.Text = "None"
            Dim hlPaynow As HyperLink = CType(FindControl("hlPayNow"), HyperLink)
            Session("RegFee") = CType(0, Decimal)
            Session("Donation") = CType(0, Decimal)
            If (Not (hlPaynow) Is Nothing) Then
                hlPaynow.Visible = False
                If (nRegFee > 0) Then
                    RegFee.Text = nRegFee.ToString("c", "us")
                    Session("RegFee") = CType(nRegFee, Decimal)
                    hlPaynow.Visible = True
                End If
            End If
            connContest = Nothing
        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
                    Dim pdt As Object = DataBinder.Eval(e.Item.DataItem, "PaymentDate")
                    'If Not lbtn Is Nothing Then
                    'hide the remove option for every entry
                    CType(e.Item.FindControl("lbRemoveContest"), LinkButton).Visible = False
                    If Application("ContestType") = "1" Then
                        CType(e.Item.FindControl("lblContact"), Label).Text = Application("NationalFinalsCity")
                    Else
                        CType(e.Item.FindControl("lblContact"), Label).Text = e.Item.DataItem("City") & ", " & _
                                                                                e.Item.DataItem("State")
                        '& _
                        '                                                                               "<BR> Coordinator: " & e.Item.DataItem("CoordinatorName")
                    End If
                    'if payment date is not available it is considered as unpaid
                    If IsDBNull(pdt) Then
                        'enable the remove contest option for unpaid entries
                        CType(e.Item.FindControl("lbRemoveContest"), LinkButton).Visible = True
                        Dim contestCode As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestID"), String)
                        Dim ContestAbbr As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestAbbr"), String)
                        Dim ChildNumber As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
                        Dim contestCategoryID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ContestCategoryID"), Integer)
                        If ViewState("SelectedContests") Is Nothing Then
                            ViewState("SelectedContests") = New ArrayList
                        End If
                        CType(ViewState("SelectedContests"), ArrayList).Add(ChildNumber.ToString & contestCategoryID.ToString)
                        If e.Item.DataItem("CreateDate") > e.Item.DataItem("RegistrationDeadline") Then
                            If Application("ContestType") = 1 Then
                                If DateDiff(DateInterval.Day, Today, Convert.ToDateTime(Application("LateRegistrationDate"))) > 0 Then
                                    lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & Application("LateFee")
                                Else
                                    lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & "0.0"
                                End If
                            End If
                        Else
                            lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & "0.0"
                        End If
                        Dim fee As Decimal = CType(DataBinder.Eval(e.Item.DataItem, "Fee"), Decimal)
                        'acumulate fee total
                        nRegFee = (nRegFee + fee)
                        'gather all the contest codes so this is treated as item list in the payment via credit card
                        Session("ContestsSelected") = (Session("ContestsSelected") _
                                    + (ContestAbbr + ("(" _
                                    + (ChildNumber.ToString + ") "))))
                    Else
                        If Application("ContestType") = 1 Then
                            '*****************************************
                            '*** Make sure the Contestant Photo is available  to enable the download link
                            '*****************************************
                            If Not IsDBNull(e.Item.DataItem("photo_image")) Then
                                If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                                    Dim linkURL As String = "/app6/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                                    Dim linkDOCURL As String = "/app6/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                                    Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                                    Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                                    If ((Not (hlnk) Is Nothing) _
                                                AndAlso (linkURL <> "")) Then
                                        hlnk.Visible = True
                                        hlnk.NavigateUrl = linkURL
                                        hlnk.Target = "downloadmateriallink"
                                        hDOClnk.Visible = True
                                        hDOClnk.NavigateUrl = linkDOCURL
                                        hDOClnk.Target = "downloadmateriallink"
                                    End If
                                End If
                            End If
                        Else
                            If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                                Dim linkURL As String = "/app6/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                                Dim linkDOCURL As String = "/app6/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                                Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                                Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                                If ((Not (hlnk) Is Nothing) _
                                            AndAlso (linkURL <> "")) Then
                                    hlnk.Visible = True
                                    hlnk.NavigateUrl = linkURL
                                    hlnk.Target = "downloadmateriallink"
                                    hDOClnk.Visible = True
                                    hDOClnk.NavigateUrl = linkDOCURL
                                    hDOClnk.Target = "downloadmateriallink"
                                End If
                            End If
                        End If
                        Dim score1 As Double = 0
                        Dim score2 As Double = 0
                        Dim score3 As Double = 0
                        Dim rank As Integer = 0
                        Dim finalscore1 As Double = 0
                        Dim finalscore2 As Double = 0
                        Dim finalpercentile As Decimal = 0
                        Dim finalrank As Integer = 0
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score1")) Then
                            score1 = CType(DataBinder.Eval(e.Item.DataItem, "Score1"), Double)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score2")) Then
                            score2 = CType(DataBinder.Eval(e.Item.DataItem, "Score2"), Double)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score3")) Then
                            score3 = CType(DataBinder.Eval(e.Item.DataItem, "Score3"), Double)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Rank")) Then
                            rank = CType(DataBinder.Eval(e.Item.DataItem, "Rank"), Integer)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1")) Then
                            finalscore1 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1"), Double)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2")) Then
                            finalscore2 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2"), Double)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Rank")) Then
                            finalrank = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Rank"), Integer)
                        End If
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile")) Then
                            finalpercentile = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile"), Decimal)
                        End If
                        Dim lblScore As Label = CType(e.Item.FindControl("lblScore"), Label)
                        If (Not (lblScore) Is Nothing) Then
                            If (score1 _
                                        + (score2 _
                                        + (score3 > 0))) Then
                                Dim sb As StringBuilder = New StringBuilder
                                sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
                                sb.Append("<tr bgcolor=lightblue forecolor=white>")
                                sb.Append("<td width=20%><b>Scores</b></td>")
                                sb.Append("<td width=20%>Phase 1</td>")
                                sb.Append("<td width=20%>Phase 2</td>")
                                sb.Append("<td width=20%>Phase 3</td>")
                                sb.Append("<td width=20%>Total</td>")
                                sb.Append("<td width=20%>Percentile</td>")
                                sb.Append("<td width=20%>Rank</td>")
                                sb.Append("</tr>")
                                sb.Append("<tr>")
                                sb.Append("<td width=20%>Regionals</td>")
                                If (score1 > 0) Then
                                    sb.Append(("<td width=20%>" _
                                                    + (score1.ToString + "</td>")))
                                Else
                                    sb.Append("<td width=20%>na</td>")
                                End If
                                If (score2 > 0) Then
                                    sb.Append(("<td width=20%>" _
                                                    + (score2.ToString + "</td>")))
                                Else
                                    sb.Append("<td width=20%>na</td>")
                                End If
                                If (score3 > 0) Then
                                    sb.Append(("<td width=20%>" _
                                                    + (score3.ToString + "</td>")))
                                Else
                                    sb.Append("<td width=20%>na</td>")
                                End If
                                If (score1 _
                                            + (score2 _
                                            + (score3 > 0))) Then
                                    sb.Append(("<td width=20%>" _
                                                    + ((score1 _
                                                    + (score2 + score3)).ToString + "</td>")))
                                Else
                                    sb.Append("<td width=20%>na</td>")
                                End If
                                sb.Append("<td width=20%>na</td>")
                                'no percentile for regionals
                                If (rank > 0) Then
                                    sb.Append(("<td width=20%>" _
                                                    + (rank.ToString + "</td>")))
                                Else
                                    sb.Append("<td width=20%>na</td>")
                                End If
                                sb.Append("</tr>")
                                If (finalscore1 _
                                            + (finalscore2 > 0)) Then
                                    sb.Append("<tr>")
                                    sb.Append("<td width=20%>Finals</td>")
                                    If (finalscore1 > 0) Then
                                        sb.Append(("<td width=20%>" _
                                                        + (finalscore1.ToString + "</td>")))
                                    Else
                                        sb.Append("<td width=20%>na</td>")
                                    End If
                                    If (finalscore2 > 0) Then
                                        sb.Append(("<td width=20%>" _
                                                        + (finalscore2.ToString + "</td>")))
                                    Else
                                        sb.Append("<td width=20%>na</td>")
                                    End If
                                    sb.Append("<td width=20%>na</td>")
                                    If (finalscore1 _
                                                + (finalscore2 > 0)) Then
                                        sb.Append(("<td width=20%>" _
                                                        + ((finalscore1 + finalscore2).ToString + "</td>")))
                                    Else
                                        sb.Append("<td width=20%>na</td>")
                                    End If
                                    If (finalpercentile > 0) Then
                                        sb.Append(("<td width=20%>" _
                                                        + (finalpercentile.ToString + "</td>")))
                                    Else
                                        sb.Append("<td width=20%>na</td>")
                                    End If
                                    If (finalrank > 0) Then
                                        sb.Append(("<td width=20%>" _
                                                        + (finalrank.ToString + "</td>")))
                                    Else
                                        sb.Append("<td width=20%>na</td>")
                                    End If
                                    sb.Append("</tr>")
                                End If
                                sb.Append("</table>")
                                lblScore.Text = sb.ToString
                            End If
                        End If
                    End If
                    If Session("ContestDate1") Is Nothing Then
                        Try
                            Session("ContestDate1") = Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString
                        Catch ex As Exception
                            Exit Try
                        End Try
                    Else
                        If Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString <> Session("ContestDate1") Then
                            If Session("ContestDate2") Is Nothing Then
                                Session("ContestDate2") = Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString
                            End If
                        End If
                    End If
            End Select
        End Sub

        Private Sub OldRemoveContest_Click(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.ItemCommand
            Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
            If (Not (lbtn) Is Nothing) Then
                Dim info As String = lbtn.CommandArgument
                Dim arInfo() As String = New String((2) - 1) {}
                ' define which character is seperating fields

                arInfo = Split(info, ",", , CompareMethod.Text)
                CType(ViewState("SelectedContests"), ArrayList).Remove(arInfo(1).ToString & arInfo(2).ToString)

                Dim prmArray(6) As SqlParameter

                prmArray(0) = New SqlParameter
                prmArray(0).ParameterName = "@ParentID"
                prmArray(0).Value = Convert.ToInt32(Session("ParentID").ToString)
                prmArray(0).Direction = ParameterDirection.Input

                prmArray(1) = New SqlParameter
                prmArray(1).ParameterName = "@ChildNumber"
                prmArray(1).Value = arInfo(1)
                prmArray(1).Direction = ParameterDirection.Input

                prmArray(2) = New SqlParameter
                prmArray(2).ParameterName = "@ContestCode"
                prmArray(2).Value = arInfo(0)
                prmArray(2).Direction = ParameterDirection.Input

                If Application("ContestType") = 1 Then
                    If Not arInfo(3) Is Nothing And Convert.ToDecimal(arInfo(3)) > 0.0 Then
                        Session("LATEFEE") -= Convert.ToDecimal(arInfo(3))
                    End If
                End If


                Dim conn As New SqlConnection(Application("ConnectionString"))
                Try
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_DeleteContestant", prmArray)
                Catch se As SqlException
                    lblMessage.Text = se.Message
                    lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                    Return
                Finally
                    conn = Nothing
                End Try

                DisplayChildren()
                DisplayContests()

            End If
        End Sub

        Private Sub RemoveContest_Click(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.ItemCommand
            Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
            If (Not (lbtn) Is Nothing) Then
                Dim info As String = lbtn.CommandArgument
                Dim arInfo() As String = New String((2) - 1) {}
                ' define which character is seperating fields

                arInfo = Split(info, ",", , CompareMethod.Text)
                CType(ViewState("SelectedContests"), ArrayList).Remove(arInfo(1).ToString & arInfo(2).ToString)

                If Application("ContestType") = 1 Then
                    If Not arInfo(3) Is Nothing And Convert.ToDecimal(arInfo(3)) > 0.0 Then
                        Session("LATEFEE") -= Convert.ToDecimal(arInfo(3))
                    End If
                End If

                Try
                    'delete routine
                    objSqlContestantProviderBase.Delete(arInfo(3))
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                    lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                    Return
                End Try

                DisplayChildren()
                DisplayContests()

            End If



        End Sub

    Private Sub AddChildToContest_Click(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEligibleContests.ItemCommand
        Dim lbtnSelect As LinkButton = CType(e.Item.FindControl("lbtnSelect"), LinkButton)
        Dim conn As New SqlConnection(Application("ConnectionString"))

        lblMessage.Text = ""
        If (Not (lbtnSelect) Is Nothing) Then
            Dim info As String = lbtnSelect.CommandArgument
            Dim arInfo() As String = New String((2) - 1) {}
            ' define which character is seperating fields
            arInfo = Split(info, ",", , CompareMethod.Text)
            If ViewState("SelectedContests") Is Nothing Then
                ViewState("SelectedContests") = New ArrayList
            End If
            If CType(ViewState("SelectedContests"), ArrayList).IndexOf(arInfo(3).ToString & arInfo(5).ToString) = -1 Then
                Dim prmArray(8) As SqlParameter

                prmArray(0) = New SqlParameter
                prmArray(0).ParameterName = "@ChapterID"
                prmArray(0).Value = arInfo(0)
                prmArray(0).Direction = ParameterDirection.Input

                prmArray(1) = New SqlParameter
                prmArray(1).ParameterName = "@ContestCode"
                prmArray(1).Value = arInfo(1)
                prmArray(1).Direction = ParameterDirection.Input

                prmArray(2) = New SqlParameter
                prmArray(2).ParameterName = "@ContestYear"
                prmArray(2).Value = arInfo(2)
                prmArray(2).Direction = ParameterDirection.Input

                prmArray(3) = New SqlParameter
                prmArray(3).ParameterName = "@ParentID"
                prmArray(3).Value = Convert.ToInt32(Session("ParentID").ToString)
                prmArray(3).Direction = ParameterDirection.Input

                prmArray(4) = New SqlParameter
                prmArray(4).ParameterName = "@ChildNumber"
                prmArray(4).Value = arInfo(3)
                prmArray(4).Direction = ParameterDirection.Input

                prmArray(5) = New SqlParameter
                prmArray(5).ParameterName = "@Fee"
                prmArray(5).Value = Convert.ToDecimal(arInfo(4))
                prmArray(5).Direction = ParameterDirection.Input

                prmArray(6) = New SqlParameter
                prmArray(6).ParameterName = "@ContestCategoryID"
                prmArray(6).Value = Convert.ToInt32(arInfo(5))
                prmArray(6).Direction = ParameterDirection.Input


                prmArray(7) = New SqlParameter
                prmArray(7).ParameterName = "@RetValue"
                prmArray(7).SqlDbType = SqlDbType.Int
                prmArray(7).Direction = ParameterDirection.Output
                If Application("ContestType") = 1 Then
                    If Not arInfo(6) Is Nothing And Convert.ToDecimal(arInfo(6)) > 0.0 Then
                        Session("LATEFEE") += Convert.ToDecimal(arInfo(6))
                    End If
                End If

                Try
                    If Application("ContestType") = "1" Then
                        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertContestantNational", prmArray)
                    Else
                        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertContestant", prmArray)
                    End If
                Catch se As SqlException
                    lblMessage.Text = se.Message
                    lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                    Return
                Finally
                    conn = Nothing
                End Try
                If prmArray(7).Value = 0 Then
                    lblMessage.Text = "This Contest is already selected. Please Select a Different one."
                    lblMessage.Font.Bold = True
                    lblMessage.ForeColor = Color.Red
                    lblMessage.Visible = True
                    Exit Sub
                End If
            Else
                lblMessage.Text = "Same Contest from different cluster is already Selected. Same Contest from multiple Chapters can not be Selected"
                lblMessage.Font.Bold = True
                lblMessage.ForeColor = Color.Red
                lblMessage.Visible = True
                Exit Sub
            End If
            DisplayChildren()
            DisplayContests()
        End If
    End Sub

    Private Sub DisplayEligibleContests_Click(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgChildren.ItemCommand
        Panel2.Visible = False
        Panel3.Visible = True
        lblMessage.Text = ""
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbDisplayContest"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim info As String = lbtn.CommandArgument
            Dim arInfo(3) As String

            arInfo = Split(info, ",", 3, CompareMethod.Text)
            Dim dob As String = arInfo(0)
            Dim grade As String = arInfo(1)
            Dim childnumber As String = arInfo(2)

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim dsEligibleContests As DataSet = New DataSet
            Dim tblEligibleContests() As String = {"EligibleContests"}
            Dim prmArray(4) As SqlParameter

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ChildNumber"
            prmArray(0).Value = childnumber
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
                prmArray(1).Value = Application("ContestYear")
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter
            prmArray(2).ParameterName = "@ContestTypeID"
                prmArray(2).Value = Application("ContestType")
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter
            prmArray(3).ParameterName = "@IndID"
                prmArray(3).Value = Session("IndID")
            prmArray(3).Direction = ParameterDirection.Input

            SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetEligibleContests", dsEligibleContests, tblEligibleContests, prmArray)

                Dim strChildName As String
                strChildName = ""
                If Not dsEligibleContests Is Nothing Then
                    If dsEligibleContests.Tables.Count > 0 Then
                        dgEligibleContests.SelectedIndex = -1
                        dgEligibleContests.DataSource = dsEligibleContests.Tables(0).DefaultView
                        dgEligibleContests.DataBind()
                        Panel3.Visible = True
                        If dsEligibleContests.Tables(0).Rows.Count > 0 Then
                            'get child name for display
                            strChildName = dsEligibleContests.Tables(0).Rows(0).Item("First_Name") & " " & dsEligibleContests.Tables(0).Rows(0).Item("Last_Name")
                        Else
                        End If
                    End If
                End If

                conn = Nothing
                If strChildName = "" Then
                    lblGridHeading.Text = "Your child is not eligible for any contests."
                Else
                    lblGridHeading.Text = strChildName & " is eligible for following contests."
                End If
                DisplayChildren()
            End If
    End Sub

    Private Sub dgEligibleContests_ItemDatabound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEligibleContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lbtnSelect As LinkButton = CType(e.Item.FindControl("lbtnSelect"), LinkButton)
                If (Not (lbtnSelect) Is Nothing) Then
                    Dim chapterID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChapterID"), Integer)
                    Dim contestCode As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestID"), String)
                    Dim contestYear As Integer = CType(DataBinder.Eval(e.Item.DataItem, "Contest_Year"), Integer)
                    Dim ChildNumber As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
                    Dim ContestCategoryID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ContestCategoryID"), Integer)
                    Dim fee As Decimal = CType(DataBinder.Eval(e.Item.DataItem, "Fee"), Decimal)
                    Dim SelectedContestCode As String = CType(DataBinder.Eval(e.Item.DataItem, "SelectedContestCode"), String)
                    If (SelectedContestCode > 0) Then
                        lbtnSelect.Text = "Already Selected"
                        lbtnSelect.Enabled = False
                    ElseIf IsDate(e.Item.DataItem("RegistrationDeadline")) Then
                        If Convert.ToDateTime((DataBinder.Eval(e.Item.DataItem, "RegistrationDeadline").ToString)) > Convert.ToDateTime("1900/01/01") Then
                                If Convert.ToDateTime((DataBinder.Eval(e.Item.DataItem, "RegistrationDeadline").ToString)).Date < Today.Date Then
                                    If Application("ContestType").ToString = "1" Then
                                        If Convert.ToDateTime(e.Item.DataItem("RegistrationDeadline")) < Today.Date Then
                                            lbtnSelect.Text = ("Last date for registration was " + e.Item.DataItem("RegistrationDeadline").ToString.Replace(" 12:00:00 AM", ""))
                                            lbtnSelect.Enabled = False
                                        Else
                                            lbtnSelect.Text = "Late Fee:" + Application("LateFee") + " Select "
                                            lbtnSelect.CommandArgument = chapterID.ToString & "," _
                                                & contestCode & "," _
                                                & contestYear.ToString & "," _
                                                & ChildNumber.ToString & "," _
                                                & fee.ToString & "," _
                                                & ContestCategoryID.ToString & "," _
                                                & Application("LateFee")
                                        End If
                                    Else
                                        lbtnSelect.Text = ("Last date for registration was " + DataBinder.Eval(e.Item.DataItem, "RegistrationDeadline").ToString.Replace(" 12:00:00 AM", ""))
                                        lbtnSelect.Enabled = False
                                    End If
                                Else
                                    lbtnSelect.CommandArgument = chapterID.ToString & "," _
                                                & contestCode & "," _
                                                & contestYear.ToString & "," _
                                                & ChildNumber.ToString & "," _
                                                & fee.ToString & "," _
                                                & ContestCategoryID.ToString & "," _
                                                & "0.0"
                                End If
                        Else
                            lbtnSelect.CommandArgument = chapterID.ToString & "," _
                                            & contestCode & "," _
                                            & contestYear.ToString & "," _
                                            & ChildNumber.ToString & "," _
                                            & fee.ToString & "," _
                                            & ContestCategoryID.ToString & "," _
                                            & "0.0"

                        End If
                    Else
                        lbtnSelect.CommandArgument = (chapterID.ToString + ("," _
                                    + (contestCode + ("," _
                                    + (contestYear.ToString + ("," _
                                    + (ChildNumber.ToString + ("," + fee.ToString + "," + ContestCategoryID.ToString))))))))
                    End If
                    If Application("ContestType") = "2" Then   'Regional Contact Information
                            CType(e.Item.FindControl("lblContactInfo"), Label).Text = e.Item.DataItem("City") & ", " & e.Item.DataItem("State") & "<BR>" ' & e.Item.DataItem("CoordinatorName")
                        If Not Session("ParentsChapterID") Is Nothing Then
                            If chapterID = Session("ParentsChapterID") Then
                                CType(e.Item.FindControl("lblContactInfo"), Label).Font.Bold = True
                                CType(e.Item.FindControl("lblContactInfo"), Label).ForeColor = Color.Green
                            End If
                        End If
                    Else
                        CType(e.Item.FindControl("lblContactInfo"), Label).Text = Application("NationalFinalsCity")
                    End If
                End If
        End Select
        dgEligibleContests.Visible = True
        Panel3.Visible = True
    End Sub

    Private Sub lnkClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkClose.Click
        DisplayChildren()
        DisplayContests()
    End Sub
    Private Sub TestContestEmail()
        Dim sb As New StringBuilder

        Dim connContest As New SqlConnection(Application("ConnectionString"))
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("ParentID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)


        Dim emailBody As String = ""
        Dim screenConfirmText As String = ""
        Dim subMail As String = "Confirmation received for NSF Regional Contests"
        Dim re As StreamReader
        Dim rowcount As Int32 = 0

        sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
        sb.Append("<tr bgcolor=lightblue forecolor=white>")
        sb.Append("<td width=20%><b>Contestant Name</b></td>")
        sb.Append("<td width=20%>Contest Desc</td>")
        sb.Append("<td width=20%>Contest Location</td>")
        sb.Append("<td width=20%>Contest Date Time</td>")
        sb.Append("<td width=20%>Payment Info</td>")
        sb.Append("</tr>")
        If dsContestant.Tables.Count > 0 Then
            If dsContestant.Tables(0).Rows.Count > 0 Then
                For rowcount = 0 To dsContestant.Tables(0).Rows.Count - 1
                    sb.Append("<tr>")
                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestantName").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestDesc").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterCity").ToString() + ", ")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterState").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("CoordinatorName").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestDate").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestTime").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(FormatCurrency(dsContestant.Tables(0).Rows(rowcount).Item("Fee")).ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentDate").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentReference").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentMode").ToString() + "</td></tr>")
                Next
            End If
        End If
        sb.Append("</table>")

        re = File.OpenText(Server.MapPath("success_email.htm"))
        screenConfirmText = re.ReadToEnd
        screenConfirmText = screenConfirmText.Replace("[DATAGRID]", sb.ToString)
        screenConfirmText = screenConfirmText.Replace("[AMNT]", nRegFee)

        emailBody = screenConfirmText
        re.Close()
        Response.Write(emailBody.ToString)
        SendEmail(subMail, emailBody, Session("LoginEmail").ToString)
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean

        'leave blank to use default SMTP server
        Dim ok As Boolean = True


            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("contests@northsouth.org")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = "<strong>Hello Steve!</strong>"

            'Send Email
            Dim client As New SmtpClient()

            client.Host = ""

            Try
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
            Return ok

    End Function

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload

    End Sub

        Private Function InsertContests(ByVal GridItem As Integer) As Boolean
            Dim ChildNumber As Int32, ContestID As Int32, ContestCode As String, Fee As Decimal
            Dim ContestCategoryID As Int32, ChapterID As Int32
            Dim EventID As Int32, EventCode As String
            Dim ProductGroupID As Int32, ProductGroupCode As String
            Dim ProductID As Int32, ProductCode As String
            Dim myTableCell As TableCell
            Dim param(18) As SqlParameter


            'ChildNumber
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(1)
            ChildNumber = CInt(myTableCell.Text)

            'ContestID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(2)
            ContestID = CInt(myTableCell.Text)

            'Fee
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(3)
            Fee = Convert.ToDecimal(Mid(myTableCell.Text, 2))

            'ContestCategoryID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(9)
            ContestCategoryID = CInt(myTableCell.Text)

            'EventID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(10)
            EventID = CInt(myTableCell.Text)

            'EventCode
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(11)
            EventCode = CStr(myTableCell.Text)

            'ProductGroupID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(12)
            ProductGroupID = CInt(myTableCell.Text)

            'ProductGroupCode
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(13)
            ProductGroupCode = CStr(myTableCell.Text)

            'ProductID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(14)
            ProductID = CInt(myTableCell.Text)

            'ProductCode
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(15)
            ProductCode = CStr(myTableCell.Text)

            'ChapterID
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(16)
            ChapterID = CStr(myTableCell.Text)

            'ContestCode
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(17)
            ContestCode = CStr(myTableCell.Text)

            Try
                param(0) = New SqlParameter("@ChapterID", ChapterID)
                param(1) = New SqlParameter("@ContestID", ContestID)
                param(2) = New SqlParameter("@ContestYear", Application("ContestYear"))
                param(3) = New SqlParameter("@ParentID", Session("ParentID"))
                param(4) = New SqlParameter("@ChildNumber", ChildNumber)
                param(5) = New SqlParameter("@Fee", Fee)
                param(6) = New SqlParameter("@ContestCategoryID", ContestCategoryID)
                param(7) = New SqlParameter("@EventId", EventID)
                param(8) = New SqlParameter("@EventCode", EventCode)
                param(9) = New SqlParameter("@ProductGroupId", ProductGroupID)
                param(10) = New SqlParameter("@ProductGroupCode", ProductGroupCode)
                param(11) = New SqlParameter("@ProductId", ProductID)
                param(12) = New SqlParameter("@ProductCode", ProductCode)
                param(13) = New SqlParameter("@CreateDate", Now)
                param(14) = New SqlParameter("@CreatedBy", Session("ParentID"))
                param(15) = New SqlParameter("@ContestCode", ContestCode)
                param(16) = New SqlParameter("@RetValue", SqlDbType.Int)
                param(16).Direction = ParameterDirection.Output

                If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
                SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertContestant", param)
            Catch ex As SqlException
                lblMessage.Text = lblMessage.Text & " " & ex.Message
                Return False
            End Try
            Return True
        End Function

        Protected Sub btnAddContests_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddContests.Click
            Dim intCtr As Integer, intAddCtr As Integer
            For intCtr = 0 To dgEligibleContests.Items.Count - 1
                'disenable delete and copy buttons
                Dim lnkBtn As LinkButton = dgEligibleContests.Items.SyncRoot.Item(intCtr).Cells(8).Controls(1)
                If LCase(lnkBtn.Text) <> LCase("Already Selected") Then
                    Dim intcell As Integer = dgEligibleContests.Columns.Count - 1
                    Dim myTableCell As TableCell
                    myTableCell = dgEligibleContests.Items.SyncRoot.Item(intCtr).Cells(intcell)
                    Dim chkSelected As CheckBox = myTableCell.Controls(1)
                    If chkSelected.Checked = True Then
                        If InsertContests(intCtr) = False Then
                            lblMessage.Text = "Problems inserting records. " & lblMessage.Text
                        Else
                            lblMessage.Text = "Selected Contests were added successfully."
                        End If
                        lblMessage.Visible = True
                        intAddCtr = intAddCtr + 1
                    End If
                End If
            Next

            DisplayChildren()
            DisplayContests()

        End Sub
    End Class

End Namespace


