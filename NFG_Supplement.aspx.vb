Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.StringSplitOptions
Imports System.String

Partial Class NFG_Supplement
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            Try
                ViewSupp_Dates()
            Catch ex As Exception
                Response.Redirect("VolunteerFunctions.aspx")
            End Try
        End If

    End Sub
    Private Sub ViewSupp_Dates()
        Response.Write("<script language='javascript'>")
        Response.Write(" window.open('NFG_SuppStatus.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
        Response.Write("</script>")
    End Sub
    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim SQLUpdate As String = ""
        Dim NRows As Integer = 0

        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        ElseIf CheckSupp_dates() = True Then
            lblErr.Text = "Records already exist.Please Verify the Dates"
            ViewSupp_Dates()
            Exit Sub
        Else
            'Flag 'Y'
            SQLUpdate = SQLUpdate & "UPDATE NFG SET NFG.MatchedStatus = 'Y' From NFG_Transactions NFG  INNER JOIN Contestant as Con ON Con.PaymentReference = NFG.asp_Session_Id Where  NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "';"
            SQLUpdate = SQLUpdate & "UPDATE NFG SET NFG.MatchedStatus = 'Y' From NFG_Transactions NFG INNER JOIN Registration as Reg ON Reg.PaymentReference = NFG.asp_session_id Where  NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "';"
            SQLUpdate = SQLUpdate & "UPDATE NFG SET NFG.MatchedStatus = 'Y' From NFG_Transactions NFG INNER JOIN DuplicateContestantReg as Dup ON Dup.PaymentReference = NFG.asp_session_id Where NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "';"
            SQLUpdate = SQLUpdate & "UPDATE NFG SET NFG.MatchedStatus = 'Y' From NFG_Transactions NFG INNER JOIN CoachReg as CR ON CR.PaymentReference = NFG.asp_session_id Where NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "';"
            'Flag 'R'
            SQLUpdate = SQLUpdate & "Update NFG_Transactions Set MatchedStatus='R' Where MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' AND TotalPayment < 0;"
            'Flag 'O'
            SQLUpdate = SQLUpdate & "Update NFG_Transactions SET MatchedStatus='O' Where EventId in (4,5,6,10,11,18) and MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' and MatchedStatus is null;"
            'Flag 'N'
            SQLUpdate = SQLUpdate & ";Update NFG_Transactions SET MatchedStatus='N' Where asp_session_id in(SELECT asp_session_id From(SELECT N.asp_Session_Id, SUM(N.TFee) as TotFee,N.Fee,N.MS_DT, CASE WHEN SUM(N.TFee)= N.Fee THEN 'Y' ELSE 'N' END as NFG_Status From"
            SQLUpdate = SQLUpdate & "(SELECT NFG.asp_Session_Id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) as MS_DT  ,SUM(Con.Fee) as TFee,'Contest' as Event  From NFG_Transactions NFG  INNER JOIN Contestant as Con ON Con.PaymentReference = NFG.asp_Session_Id Where  NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Group By  NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101)  UNION ALL "
            SQLUpdate = SQLUpdate & " SELECT NFG.asp_Session_Id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) as MS_DT ,SUM(Reg.Fee) as TFee,'WorkShop'  From NFG_Transactions NFG INNER JOIN Registration as Reg ON Reg.PaymentReference = NFG.asp_session_id Where  NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Group By  NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) UNION ALL "
            SQLUpdate = SQLUpdate & "SELECT NFG.asp_Session_Id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) as MS_DT ,SUM(Dup.Fee) as TFee,'Duplicate' as Event From NFG_Transactions NFG INNER JOIN DuplicateContestantReg as Dup ON Dup.PaymentReference = NFG.asp_session_id Where NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Group By  NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) UNION ALL "
            SQLUpdate = SQLUpdate & "SELECT NFG.asp_Session_Id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) as MS_DT ,SUM(CR.Fee) as TFee,'Coaching' as Event From NFG_Transactions NFG INNER JOIN CoachReg as CR ON CR.PaymentReference = NFG.asp_session_id Where NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Group by NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) "
            'SQLUpdate = SQLUpdate & "SELECT NFG.asp_Session_Id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) as MS_DT ,SUM(Rf.Na_Amount) as TFee,'Refund' as Event From NFG_Transactions NFG INNER JOIN Refund as Rf ON Rf.Na_OrderNumber = NFG.asp_session_id Where NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Group by NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101)"
            SQLUpdate = SQLUpdate & ") as N  Group by N.asp_Session_id,N.Fee,N.MS_DT having SUM(N.TFee)<> N.Fee) as M) and Fee > 0 and MS_TransDate Between  '" & TxtFrom.Text & "' and '" & txtTo.Text & "';"

            SQLUpdate = SQLUpdate & "Update NFG_Transactions set MatchedStatus='N' Where asp_session_id in (SELECT NFG.asp_Session_Id  From NFG_Transactions NFG  INNER JOIN Contestant as Con ON Con.PaymentReference = NFG.asp_Session_Id Where  NFG.Fee > 0 and NFG.MS_TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' and MatchedStatus ='Y' Group By  NFG.asp_Session_id,NFG.Fee, CONVERT(VARCHAR(10), NFG.MS_TransDate, 101) having NFG.Fee <> SUM(Con.Fee))"

            NRows = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLUpdate)
            If NRows > 0 Then
                lblErr.Text = NRows & " Rows are Updated Successfully"
            End If
        End If
    End Sub
    Public Function CheckSupp_dates()
        Dim ErrorFlag As Boolean = False
        Dim ds_Date As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT Distinct FromDate,ToDate From NFG_Supp order by Todate Desc;")
        If ds_Date.Tables(0).Rows.Count > 0 Then
           
            For i As Integer = 0 To ds_Date.Tables(0).Rows.Count - 1
                If ds_Date.Tables(0).Rows(i)("FromDate") <= Convert.ToDateTime(TxtFrom.Text) And Convert.ToDateTime(TxtFrom.Text) <= ds_Date.Tables(0).Rows(i)("ToDate") Then
                    ErrorFlag = True
                End If
                If ds_Date.Tables(0).Rows(i)("FromDate") <= Convert.ToDateTime(txtTo.Text) And Convert.ToDateTime(txtTo.Text) <= ds_Date.Tables(0).Rows(i)("ToDate") Then
                    ErrorFlag = True
                End If
                If Convert.ToDateTime(TxtFrom.Text) <= ds_Date.Tables(0).Rows(i)("FromDate") And ds_Date.Tables(0).Rows(i)("FromDate") <= Convert.ToDateTime(txtTo.Text) And Convert.ToDateTime(txtTo.Text) < ds_Date.Tables(0).Rows(i)("ToDate") Then
                    ErrorFlag = True
                End If
            Next
        Else
            ErrorFlag = False
        End If
        Return ErrorFlag
    End Function
    Protected Sub btnGenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        ElseIf CheckSupp_dates() = True Then
            lblErr.Text = "Records already exist.Please Verify the Dates"
            ViewSupp_Dates()
            Exit Sub
        Else
            ParsePaymentNotes()
        End If
    End Sub
    Public Sub ParsePaymentNotes()
        Dim match As Match
        Dim capture As Capture
        Dim ProductCode As String = ""
        Dim MatchString As String = ""
        Dim SQL_NFG As String = ""
        Dim SQL_NFGValue As String = ""
        Dim EventID As Integer
        Dim Matches As MatchCollection
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim PaymentMatch As String
        Dim pattern As String = ""
        Dim CheckFlag As Boolean = False
        Dim SQLExec As String = ""
        Dim Nxt_PrdMatch As String = ""
        Dim Prd_index As Integer = 0
        Dim ProductCode_R As String = ""

        Dim ds_Product As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "SELECT Distinct ProductCode,ProductId From Product Order by ProductId")
        For j As Integer = 0 To ds_Product.Tables(0).Rows.Count - 1
            pattern = pattern & ds_Product.Tables(0).Rows(j)("ProductCode") & "|"
        Next

        pattern = pattern.TrimEnd("|")

        Dim SQLStr As String = "SELECT PaymentNotes,asp_session_id,MemberID,EventYear,EventID,Fee,[Payment Date],MS_TransDate,ChapterID,MatchedStatus from NFG_Transactions Where (MatchedStatus is null Or MatchedStatus='R' Or MatchedStatus='N') and MS_Transdate between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' order by ChapterId" '
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
        If ds.Tables(0).Rows.Count > 0 Then
            Try
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    PaymentMatch = IIf(ds.Tables(0).Rows(i)("PaymentNotes") Is DBNull.Value, "", ds.Tables(0).Rows(i)("PaymentNotes"))
                    Matches = Regex.Matches(PaymentMatch, pattern)

                    Nxt_PrdMatch = PaymentMatch

                    For Each match In Matches
                        Dim j As Integer = 0
                        For Each capture In match.Captures
                            ProductCode = capture.Value
                        Next

                        Nxt_PrdMatch = (Nxt_PrdMatch.Substring(Nxt_PrdMatch.IndexOf(ProductCode) + Len(ProductCode)))
                        Dim m As Match = Regex.Match(Nxt_PrdMatch, pattern)

                        If (m.Success) Then
                            ProductCode_R = m.Value
                        Else
                            ProductCode_R = ""
                        End If

                        'MatchString = PaymentMatch.Substring((PaymentMatch.IndexOf(ProductCode) + Len(ProductCode)), IIf(ProductCode_R <> "", PaymentMatch.IndexOf(ProductCode_R), Len(PaymentMatch)) - (PaymentMatch.IndexOf(ProductCode) + Len(ProductCode)))
                        MatchString = Nxt_PrdMatch.Substring(0, IIf(ProductCode_R <> "", Nxt_PrdMatch.IndexOf(ProductCode_R), Len(Nxt_PrdMatch))) ' - (PaymentMatch.IndexOf(ProductCode) + Len(ProductCode)))

                        'Dim substrings() As String = Regex.Split(PaymentMatch, pattern)
                        'For Each match1 As String In substrings
                        '    MatchString = (match1.ToString()) '.Value).Replace("(", "").Replace(")", ""))
                        'Next
                        CheckFlag = True
                        EventID = ds.Tables(0).Rows(i)("EventId")
                        SQL_NFG = "Insert into NFG_supp(MemberID,EventId,EventYear,ProductGroupId,ProductGroupCode,ProductID,ProductCode,Fee_NFG,PaymentReference,PaymentDate,PaymentNotes,MS_TransDate,MatchedStatus,FromDate,ToDate"
                        SQL_NFGValue = ") Values(" & ds.Tables(0).Rows(i)("MemberID") & "," & ds.Tables(0).Rows(i)("EventId") & "," & ds.Tables(0).Rows(i)("EventYear") & "," & "(SELECT ProductGroupId From Product Where EventId=" & EventID & " and ProductCode='" & ProductCode & "')," & "(SELECT ProductGroupCode From Product Where EventId=" & EventID & " and ProductCode='" & ProductCode & "')," & "(SELECT ProductId From Product Where EventId=" & EventID & " and ProductCode='" & ProductCode & "'),'" & ProductCode & "',"
                        SQL_NFGValue = SQL_NFGValue & ds.Tables(0).Rows(i)("Fee") & ",'" & ds.Tables(0).Rows(i)("asp_session_id") & "','" & ds.Tables(0).Rows(i)("Payment Date") & "','" & ds.Tables(0).Rows(i)("PaymentNotes") & "','" & ds.Tables(0).Rows(i)("MS_TransDate") & "'," & IIf(ds.Tables(0).Rows(i)("MatchedStatus") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("MatchedStatus") & "'") & ",'" & TxtFrom.Text & "','" & txtTo.Text & "'"

                        CheckPaymentNotes(ProductCode, MatchString, ds.Tables(0).Rows(i)("asp_session_id"), SQL_NFG, SQL_NFGValue)
                    Next
                    If CheckFlag = False Then
                        SQL_NFG = "Insert into NFG_supp(MemberID,EventId,EventYear,ProductGroupId,ProductGroupCode,ProductID,ProductCode,Fee_NFG,PaymentReference,PaymentDate,PaymentNotes,MS_TransDate,MatchedStatus,FromDate,ToDate,ChildNumber,ChapterId,Amount_pars,CreateDate"
                        SQL_NFGValue = ") Values(" & ds.Tables(0).Rows(i)("MemberID") & "," & ds.Tables(0).Rows(i)("EventId") & "," & ds.Tables(0).Rows(i)("EventYear") & "," & "NULL,NULL,NULL,NULL,"
                        SQL_NFGValue = SQL_NFGValue & ds.Tables(0).Rows(i)("Fee") & ",'" & ds.Tables(0).Rows(i)("asp_session_id") & "','" & ds.Tables(0).Rows(i)("Payment Date") & "','" & ds.Tables(0).Rows(i)("PaymentNotes") & "','" & ds.Tables(0).Rows(i)("MS_TransDate") & "', " & IIf(ds.Tables(0).Rows(i)("MatchedStatus") Is DBNull.Value, "NULL", "'" & ds.Tables(0).Rows(i)("MatchedStatus") & "'") & ",'" & TxtFrom.Text & "','" & txtTo.Text & "',NULL," & ds.Tables(0).Rows(i)("ChapterID") & ",NULL,GETDATE())"
                        SQLExec = SQLExec & SQL_NFG & SQL_NFGValue
                        'CheckPaymentNotes("", "", ds.Tables(0).Rows(i)("asp_session_id"), "", "")
                    End If

                Next

                If CheckFlag = False Then
                    If SqlHelper.ExecuteNonQuery(conn, CommandType.Text, SQLExec) > 0 Then
                        lblErr.Text = "Records Inserted Successfully"
                    End If
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        Else
            lblErr.Text = "No Records Present in NFG_Transactions matching the criteria"
        End If
    End Sub
    Public Function CheckPaymentNotes(ByVal ProductCode As String, ByVal PaymentMatch As String, ByVal asp_session_id As String, ByVal SQL_NFG As String, ByVal SQL_NFGValue As String) As Boolean
        'Get the amount in it and Check
        Dim match As Match
        Dim capture As Capture
        Dim Amt As Decimal = 0
        Dim Totamt As Decimal = 0
        Dim Mealamt As Decimal = 0
        Dim Donamt As Decimal = 0
        Dim ChapterId As Integer
        Dim ChildNumber As String = ""
        Dim MatchFlag As Boolean = False
        Dim pattern As String
        Dim Matches As MatchCollection
        Dim SQLExec As String = ""
        'Dim SQL_NFG As String = "Insert into TempNFG_Trans (asp_session_id,ChapterID,Amount,MealsAmount,Donation,CreateDate) Values ('" & asp_session_id & "'"

        pattern = (ProductCode & "[(][0-9]{1,5}[)]")
        Matches = Regex.Matches(ProductCode & PaymentMatch, pattern)

        For Each match In Matches
            For Each capture In match.Captures
                ChildNumber = (capture.Value.Replace(ProductCode, "").Replace("(", "").Replace(")", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & ",ChildNumber"
        SQL_NFGValue = SQL_NFGValue & "," & IIf(MatchFlag = True, ChildNumber, "NULL")
        pattern = ("[)][(][0-9]{1,3}[)]")
        Matches = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                ChapterId = Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & ",ChapterId"
        SQL_NFGValue = SQL_NFGValue & "," & IIf(MatchFlag = True, ChapterId, "NULL")
        MatchFlag = False

        pattern = ("[(][-]*[0-9]+[.][0-9]+[)]")
        Matches = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Amt = Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & ",Amount_pars"
        SQL_NFGValue = SQL_NFGValue & "," & IIf(MatchFlag = True, Amt, "NULL")
        MatchFlag = False

        SQL_NFG = SQL_NFG & " ,CreateDate"
        SQL_NFGValue = SQL_NFGValue & ",GETDATE())"
        Dim conn As New SqlConnection(Application("ConnectionString"))

        SQLExec = SQL_NFG & SQL_NFGValue
 
        If SqlHelper.ExecuteNonQuery(conn, CommandType.Text, SQLExec) > 0 Then
            lblErr.Text = "Successfully inserted"
        Else
            lblErr.Text = "Insertion Failed"
        End If
        Return True
    End Function
End Class
