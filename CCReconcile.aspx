<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CCReconcile.aspx.vb" Inherits="VRegistration.CCReconcile" title="Credit Card Charges Reconciliation"  EnableEventValidation = "false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="javascript" type="text/javascript">
            function PopupPicker(ctl) {
                      var PopupWindow = null;
                      settings = 'width=600,height=175,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                      PopupWindow = window.open('CCReconcileHelp.aspx?ID=' + ctl, 'CCReconcile Help', settings);
                      PopupWindow.focus();
                    }
	</script>
   <asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>
   &nbsp;&nbsp;&nbsp;<br />
   <table cellpadding="3" cellspacing="0" style="width:1000px">
      <tr>
         <td align="center">
            <table cellpadding="3" cellspacing="0" bgcolor="#99CC33">
               <tr>
                  <td>
                     <table border ="0" cellpadding ="2px" cellspacing ="0" bgcolor="white">
                        <tr>
                           <td colspan ="3" align = "center" class="head" bgcolor="#3366cc">Credit Card Charges Reconciliation</td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnUploadFile" Text="Upload Input Files" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlCSVUpload" runat="server">
                                 <asp:ListItem Value="1">Payment Processor - PP</asp:ListItem>
                                 <asp:ListItem Value="2">Merchant Services - MS</asp:ListItem>
                                <%-- <asp:ListItem Value="3">Email Data - EM</asp:ListItem>--%>
                                 <asp:ListItem Selected="True" Value="0">Select File Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left">
                              <asp:FileUpLoad id="FileUpLoad1"  runat="server" />
                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPostData" Text="Post Data from Input Files (from Temp to Charge)" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlPostData" runat="server">
                                 <asp:ListItem Value="1">Payment Processor - PP</asp:ListItem>
                                 <asp:ListItem Value="2">Merchant Services - MS</asp:ListItem>
                                <%-- <asp:ListItem Value="3">Email Data - EM</asp:ListItem>--%>
                                 <asp:ListItem Selected="True" Value="0">Select Data Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"></td>
                        </tr>
                        <tr id="TrMsg" runat = "server" visible = "false">
                           <td colspan = "3" align ="center" >
                              <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                              <br />
                              <asp:Button ID="BtnYes" runat="server" Text="Yes" Width="50px" />
                              &nbsp;
                              <asp:Button ID="BtnNo"
                                 runat="server" Text="No"  Width="50px"  />
                           </td>
                        </tr>
                      <%--  <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnEMNSF" Text="Reconcile Email Data (EM) with NSF data (EMCharge with NFG_Transactions)" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>--%>
                        <%--<tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVwMsdNSF" Text="View Payments missed by NSF" GroupName="Uploadfiles" runat="server" />
                           </td>
                            <td align="left">
                              <asp:DropDownList ID="ddlVwMsdNSF" runat="server" AutoPostBack="true">
                                 <asp:ListItem Value="1">Reconciled Data</asp:ListItem>
                                 <asp:ListItem Value="2">Un-Reconciled Data</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Data Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"> 
                              <asp:DropDownList ID="ddlUnRecVwMsdNSF" runat="server"  Enabled ="false">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="Brand">Brand</asp:ListItem>
                                    <asp:ListItem Value="Reference">Reference</asp:ListItem>
                                    <asp:ListItem Value="Comments">Comments</asp:ListItem>
                                    <asp:ListItem Value="MemberID">MemberID</asp:ListItem>
                                    <asp:ListItem Value="NFG_PaymentDate">NFG_PaymentDate</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           
                           
                        </tr>--%>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPPMS" Text="Reconcile PP with MS & add records to ChargeRec table" GroupName="Uploadfiles" runat="server" /> <%--Reconcile LinkPoint (PP) with Merchant Services (MS)--%>
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVwPPMS" Text="View PP/MS Data" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="DdlVwPPMS" runat="server">
                                 <asp:ListItem Value="1">Reconciled Data</asp:ListItem>
                                 <asp:ListItem Value="2">Un-Reconciled Data</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Data Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"></td>
                        </tr>
                       <%-- <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnSDEM" Text="Reconcile ChargeRec Data (SD) with Email Data (EM)" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>--%>
                       <%-- <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVwSDEM" Text="View ChargeRec Data/EM Data" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="DdlVwSDEM" runat="server" AutoPostBack="true">
                                 <asp:ListItem Value="1">Reconciled Data</asp:ListItem>
                                 <asp:ListItem Value="2">Un-Reconciled Data</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Data Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left"> 
                              <asp:DropDownList ID="ddlUnRecSDEM" runat="server"  Enabled ="false">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="Brand">Brand</asp:ListItem>
                                    <asp:ListItem Value="Reference">Reference</asp:ListItem>
                                    <asp:ListItem Value="Comments">Comments</asp:ListItem>
                                    <asp:ListItem Value="MemberID">MemberID</asp:ListItem>
                                    <asp:ListItem Value="NFG_UniqueID">NFG_UniqueID</asp:ListItem>
                                    <asp:ListItem Value="NFG_PaymentDate">NFG_PaymentDate</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                        </tr>--%>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnSDNSF" Text="Reconcile ChargeRec Data (SD) with NSF Data" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVwSDNSF" Text="View ChargeRec Data/NSF Data" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="DdlVwSDNSF" runat="server" AutoPostBack="true">
                                 <asp:ListItem Value="1">Reconciled Data</asp:ListItem>
                                 <asp:ListItem Value="2">Un-Reconciled Data</asp:ListItem>
                                 <asp:ListItem Selected="True" Value="0">Select Data Type</asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td align="left">
                              <asp:DropDownList ID="ddlUnRecSDNSF" runat="server" Enabled ="false">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="Brand">Brand</asp:ListItem>
                                    <asp:ListItem Value="Reference">Reference</asp:ListItem>
                                    <asp:ListItem Value="Comments">Comments</asp:ListItem>
                                    <asp:ListItem Value="MemberID">MemberID</asp:ListItem>
                                    <asp:ListItem Value="NFG_UniqueID">NFG_UniqueID</asp:ListItem>
                                    <asp:ListItem Value="NFG_PaymentDate">NFG_PaymentDate</asp:ListItem>
                              </asp:DropDownList>

                           </td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPostNSFTrans" Text="Post MS_TransDate and CC Brand to NFG_Trans table" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnViewNFGData" Text=" View missing Brand and MS_Transdate in NFG_Trans table" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left">From Date: <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox></td>
                           <td align="left">To Date: <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox></td>
                        </tr>
                       
                        <tr>
                           <td align="left">
                              <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPostNFGSupp" Text="Reconcile NFG_Trans with component tables" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left"></td>
                           <td align="left"></td>
                        </tr>
                        <tr>
                            <td align="left">
                                        <asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnViewUnRecData" Text="View Reconciled/Un-Reconciled data From NFG_Trans[MatchedStatus]" GroupName="Uploadfiles" runat="server" />
                           </td>
                           <td align="left" colspan="2">
                                    <asp:DropDownList ID="ddlMatchedStatus" runat="server">
                                            <asp:ListItem Value="Null">Null</asp:ListItem> 
                                            <asp:ListItem Value="N">N</asp:ListItem> 
                                            <asp:ListItem Value="R">R</asp:ListItem> 
                                    </asp:DropDownList><%--</td><td align="left">--%> 
                           <a href="javascript:PopupPicker('UnRecData');">Help</a></asp:AccessDataSource><%--</td>
                         <td align="left">--%>
                         From Date: <asp:TextBox ID="txtDateFrom" runat="server"></asp:TextBox>
                         To Date: <asp:TextBox ID="txtDateTo" runat="server"></asp:TextBox></td>
                        
                        
                        
                        </tr>
                        <tr>
                           <td colspan = "3" align ="center" >
                              <asp:Button id="Button1" Text="Continue" OnClick="Button1_Click" runat="server" />
                           </td>
                        </tr>
                        <tr>
                           <td colspan = "3" align ="center" >
                              <asp:Label id="Label1" runat="server" ForeColor="Red" />
                           </td>
                        </tr>
                       <%-- <tr>
                           <td colspan = "3" align ="left" >
                              Note: 
                              <UL>
                                 <li>Please generate Email Transaction Records from Outlook using the option "Comma Seperated Values(Windows)" format.</li>
                              </UL>
                           </td>
                        </tr>--%>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
   <%--<asp:Panel ID="PnlVwMsdNSF" runat="server" Visible="false">
      <br />
      <asp:Label ID="lblVwMsdNSF" CssClass="keytext" runat="server" Text="EMCharge Data Missed by NSF"></asp:Label>
      <br />
      <asp:GridView Visible="true" ID="GrdVwMsdNSF" runat="server" DataKeyNames="EMChargeid" AutoGenerateColumns="False" AllowPaging="True" PagerSettings-PageButtonCount="30"   PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="EMChargeid"  HeaderText="EMChargeid" />
            <asp:BoundField DataField="Type" HeaderText="Type"/>
            <asp:BoundField DataField="OrderNumber" HeaderText="OrderNumber"/>
            <asp:BoundField DataField="Date" HeaderText="Date"  DataFormatString="{0:d}" />
            <asp:BoundField DataField="Brand" HeaderText="Brand" />
            <asp:BoundField DataField="Approved" HeaderText="Approved" />
            <asp:BoundField DataField="Name" HeaderText="Name"/>
            <asp:BoundField DataField="Comments" HeaderText="Comments"/>
            <asp:BoundField DataField="Amount" HeaderText="Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="MemberID" HeaderText="MemberID"/>
            <asp:BoundField DataField="StatusFlag" HeaderText="StatusFlag"  />
            <asp:BoundField DataField="Address1" HeaderText="Address1"/>
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="State" HeaderText="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" />
            <asp:BoundField DataField="Reference" HeaderText="Reference" />
         </Columns>
      </asp:GridView>
   </asp:Panel>--%>
   <asp:Panel ID="pnlChargeRec" runat="server" Visible="false">
      <br />
      <asp:Label ID="lblChargeRec" CssClass="keytext" runat="server" Text="Charge Rec Data"></asp:Label>
      <br />
      <asp:GridView Visible="true" ID="GrdChargeRec" runat="server" DataKeyNames="ChargeRecID" AutoGenerateColumns="False" AllowPaging="True" PagerSettings-PageButtonCount="30"   PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="ChargeRecID"	  HeaderText="ChargeRecID"/>
            <asp:BoundField DataField="PP_Date"  HeaderText="PP_Date"/>
            <asp:BoundField DataField="PP_OrderNumber"  HeaderText="PP_OrderNumber"/>
            <asp:BoundField DataField="PP_CN1"  HeaderText="PP_CN1"/>
            <asp:BoundField DataField="PP_CN2"  HeaderText="PP_CN2"/>
            <asp:BoundField DataField="PP_Amount"  HeaderText="PP_Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="PP_Approval"  HeaderText="PP_Approval"/>
            <asp:BoundField DataField="MS_TransDate"  HeaderText="MS_TransDate" DataFormatString="{0:d}" />
            <asp:BoundField DataField="MS_Amount"  HeaderText="MS_Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="MS_CN1"  HeaderText="MS_CN1"/>
            <asp:BoundField DataField="MS_CN2"  HeaderText="MS_CN2"/>
            <asp:BoundField DataField="MS_CN3"  HeaderText="MS_CN3"/>
            <asp:BoundField DataField="PP_MS_Match"  HeaderText="PP_MS_Match"/>
            <asp:BoundField DataField="Brand"  HeaderText="Brand"/>
            <asp:BoundField DataField="Reference"  HeaderText="Reference"/>
            <asp:BoundField DataField="Comments"  HeaderText="Comments"/>
            <asp:BoundField DataField="MemberID"  HeaderText="MemberID"/>
            <asp:BoundField DataField="EMChargeID"  HeaderText="EMChargeID"/>
            <asp:BoundField DataField="NFG_UniqueID"  HeaderText="NFG_UniqueID"/>
            <asp:BoundField DataField="NFG_PaymentDate"  HeaderText="NFG_PaymentDate" DataFormatString="{0:d}"	 />
            <asp:BoundField DataField="PPChargeID"  HeaderText="PPChargeID"/>
            <asp:BoundField DataField="MSChargeID"  HeaderText="MSChargeID"/>
            <asp:BoundField DataField="CreateDate"  HeaderText="CreateDate" DataFormatString="{0:d}" />
            <asp:BoundField DataField="CreatedBy"  HeaderText="CreatedBy"/>
            <asp:BoundField DataField="ModifiedDate"  HeaderText="ModifiedDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="ModifiedBy"  HeaderText=	"ModifiedBy"/>
         </Columns>
      </asp:GridView>
   </asp:Panel>
   <asp:Panel ID="PnlVwPPMS" runat="server" Visible="false">
      <br />
      <asp:Label ID="lblVwPP" CssClass="keytext" runat="server" Text="UnRec PPCharge"></asp:Label>
      <br />
      <asp:GridView Visible="true" ID="grdVwPP" runat="server" DataKeyNames="PPChargeID" AutoGenerateColumns="False" AllowPaging="True" PagerSettings-PageButtonCount="30"   PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="PPChargeID"  HeaderText="PPChargeID" />
            <asp:BoundField DataField="PP_OrderNumber"  HeaderText="PP_OrderNumber" />
            <asp:BoundField DataField="PP_Date"  HeaderText="PP_Date"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="PP_CN1"  HeaderText="PP_CN1" />
            <asp:BoundField DataField="PP_CN2"  HeaderText="PP_CN2" />
            <asp:BoundField DataField="PP_Amount"  HeaderText="PP_Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="PP_Approval"  HeaderText="PP_Approval" />
         </Columns>
      </asp:GridView>
      <asp:Label ID="lblMS" CssClass="keytext" runat="server" Text="UnRec MSCharge"></asp:Label>
      <asp:GridView Visible="true" ID="grdVwMS" runat="server" DataKeyNames="MSChargeID" AutoGenerateColumns="False" AllowPaging="True" PagerSettings-PageButtonCount="30"   PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="MSChargeID"  HeaderText="	MSChargeID" />
            <asp:BoundField DataField="MS_TransDate"  HeaderText="MS_TransDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="MS_Amount"  HeaderText="MS_Amount" DataFormatString="{0:c}" />
            <asp:BoundField DataField="MS_CN1"  HeaderText="MS_CN1" />
            <asp:BoundField DataField="MS_CN2"  HeaderText="MS_CN2" />
            <asp:BoundField DataField="MS_CN3"  HeaderText="MS_CN3" />
         </Columns>
      </asp:GridView>
   </asp:Panel>
   <asp:Panel ID="pnlVwSDEM" runat="server" Visible="false">
      <br />
      <asp:Label ID="lblVwSDEM" CssClass="keytext" runat="server" Text="View ChargeRec Data/EM Data(Un-reconciled: EM Charge data is missing)"></asp:Label>
      <br />
      <asp:GridView Visible="true" ID="GrdVwSDEM" runat="server" DataKeyNames="ChargeRecID" AutoGenerateColumns="False" AllowPaging="True" PagerSettings-PageButtonCount="30"   PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="ChargeRecID"  HeaderText="ChargeRecID" />
            <asp:BoundField DataField="EMChargeid"  HeaderText="EMChargeid" />
            <asp:BoundField DataField="PP_OrderNumber"  HeaderText="PP_OrderNumber" />
            <asp:BoundField DataField="PP_Date"  HeaderText="PP_Date" 	DataFormatString="{0:d}" />
            <asp:BoundField DataField="PP_Amount"  HeaderText="PP_Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="MS_TransDate"  HeaderText="MS_TransDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="Brand"  HeaderText="Brand" />
            <asp:BoundField DataField="Comments"  HeaderText="Comments" />
            <asp:BoundField DataField="MemberID"  HeaderText="MemberID" />
            <asp:BoundField DataField="PP_CN1"  HeaderText="PP_CN1" />
            <asp:BoundField DataField="PP_CN2"  HeaderText="PP_CN2" />
            <asp:BoundField DataField="Reference"  HeaderText="Reference" />
         </Columns>
      </asp:GridView>
   </asp:Panel>
   <asp:Panel ID="pnlVwSDNSF" runat="server" Visible="false">
      <br />
      <asp:Label ID="lblVwSDNSF" CssClass="keytext" runat="server" Text="View ChargeRec Data/NSF Data (Un-reconciled: NFG_Trans data is missing)"></asp:Label>
      <br />
      <asp:GridView Visible="true" ID="GrdVwSDNSF" runat="server" DataKeyNames="ChargeRecID" AutoGenerateColumns="False" AllowPaging="True"  PagerSettings-PageButtonCount="30" PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="ChargeRecID"  HeaderText="ChargeRecID" />
            <asp:BoundField DataField="PP_Date"  HeaderText="PP_Date"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="PP_OrderNumber"  HeaderText=	"PP_OrderNumber" />
            <asp:BoundField DataField="PP_CN1"  HeaderText="PP_CN1" />
            <asp:BoundField DataField="PP_CN2"  HeaderText="PP_CN2" />
            <asp:BoundField DataField="PP_Amount"  HeaderText="PP_Amount"  DataFormatString="{0:c}"  />
            <asp:BoundField DataField="PP_Approval"  HeaderText="PP_Approval" />
            <asp:BoundField DataField="NFG_UniqueID"  HeaderText="NFG_UniqueID" />
            <asp:BoundField DataField="NFG_PaymentDate"  HeaderText="NFG_PaymentDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="CreateDate"  HeaderText="CreateDate"	 />
            <asp:BoundField DataField="CreatedBy"  HeaderText="CreatedBy" />
            <asp:BoundField DataField="ModifiedDate"  HeaderText="ModifiedDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="ModifiedBy"  HeaderText="ModifiedBy" />
         </Columns>
      </asp:GridView>
   </asp:Panel>
   <asp:Panel ID="pnlVwNFGData" runat="server" Visible="false">
      <table runat="server"><tr><td>
      <br />
      <asp:Label ID="lblVwNFGData" CssClass="announcement_text" runat="server" Text=""></asp:Label><%--NFG_Trans with records with selected Matched Status flag--%>
      
      <br /></td></tr>
      <tr><td align="right"><asp:Button ID="btnExport" runat="server" Text="Export to Excel" /></td></tr>
      <tr><td>
      <asp:GridView Visible="true" ID="GrdVwNFGData" runat="server" DataKeyNames="Unique_ID" AutoGenerateColumns="False" AllowPaging="True"  PagerSettings-PageButtonCount="30" PageSize="50"  >
         <Columns>
            <asp:BoundField DataField="Unique_ID"  HeaderText="Unique_ID" />
            <asp:BoundField DataField="MemberID"  HeaderText="MemberID"  />
            <asp:BoundField DataField="First Name"  HeaderText="First Name" />
            <asp:BoundField DataField="Last Name"  HeaderText="Last Name" />
            <asp:BoundField DataField="DonorType"  HeaderText="DonorType" />
            <asp:BoundField DataField="Email (ok to contact)"  HeaderText="Email" />
            <asp:BoundField DataField="EventYear"  HeaderText="EventYear" />
            <asp:BoundField DataField="Event_for"  HeaderText="Event" />
            <asp:BoundField DataField="ChapterID"  HeaderText="ChapterID" />
            <asp:BoundField DataField="MS_TransDate"  HeaderText="MS_TransDate" /><%--	DataFormatString="{0:d}--%>
            <asp:BoundField DataField="asp_session_id"  HeaderText=	"OrderNumber" />
            <asp:BoundField DataField="Approval_status"  HeaderText="Approval" />
            <asp:BoundField DataField="Payment Date"  HeaderText="PaymentDate"	DataFormatString="{0:d}" />
            <asp:BoundField DataField="TotalPayment"  HeaderText="TotalPayment"	 DataFormatString="{0:c}" />
            <asp:BoundField DataField="Brand"  HeaderText="Brand"	 />
            <asp:BoundField DataField="MatchedStatus"  HeaderText="MatchedStatus" />
            <asp:BoundField DataField="Status"  HeaderText="Status" />
            
         </Columns>
      </asp:GridView>
      </td></tr></table>
   </asp:Panel>
</asp:Content>

