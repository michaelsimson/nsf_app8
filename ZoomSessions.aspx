﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZoomSessions.aspx.cs" Inherits="ZoomSessions" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>

        <style type="text/css">
            .web_dialog_overlay {
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                height: 100%;
                width: 100%;
                margin: 0;
                padding: 0;
                background: #000000;
                opacity: .15;
                filter: alpha(opacity=15);
                -moz-opacity: .15;
                z-index: 101;
                display: none;
            }

            .web_dialog {
                display: none;
                position: fixed;
                width: 520px;
                height: 200px;
                top: 55%;
                left: 50%;
                margin-left: -190px;
                margin-top: -100px;
                background-color: #ffffff;
                border: 2px solid #336699;
                padding: 0px;
                z-index: 102;
                font-family: Verdana;
                font-size: 10pt;
            }

            .web_dialog_title {
                border-bottom: solid 2px #336699;
                background-color: #336699;
                padding: 4px;
                color: White;
                font-weight: bold;
            }

                .web_dialog_title a {
                    color: White;
                    text-decoration: none;
                }

            .align_right {
                text-align: right;
            }
        </style>



        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>

        <script src="js/ezmodal.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />

        <script src="js/jquery.toast.js"></script>
        <link href="css/jquery.toast.css" rel="stylesheet" />

        <script src="Scripts/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

        <script type="text/javascript">
            function JoinMeeting() {

                var url = document.getElementById("<%=hdnHostURL.ClientID%>").value;

                window.open(url, '_blank');

            }


            function showAlert() {
                alert("Meeting attendees can only join their class up to 30 minutes before class time");
            }
            function DeleteMeeting() {
                if (confirm("Are you sure want to delete the attendee?")) {
                    document.getElementById("<%=BtnDelete.ClientID%>").click();
                }
            }

          <%--  function JoinMeeting() {

                var url = document.getElementById("<%=hdnHostURL.ClientID%>").value;

                window.open(url, '_blank');

            }--%>


            function showmsg() {
                alert("You can only start/join the meeting up to 15 minutes before start time.");
            }

            $(document).on("click", ".btnViewAttendee", function (e) {
                var sessionKey = $(this).attr("attr-SessionKey");
                $("#spnMemberTitle").text("View Attendee")
                $(".dvSendEMailContent").hide();
                $(".dvGridTableContent").show();


                populateAttendees(sessionKey);
                populateCoHost(sessionKey);

            });

            function populateAttendees(sessionKey) {
                var jsonData = JSON.stringify({ SessionKey: sessionKey });
                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/GetAttendees",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var tblHtml = "";
                        var date = "";
                        var time = "";
                        var name = "";
                        var email = "";
                        var duration = "";
                        var sessionType = "";
                        var attendeeId = "";
                        var sessionkey = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Action</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Name</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Email</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Date</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Time</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Duration</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>SessionType</th>";

                        tblHtml += "</tr>";
                        tblHtml += "</thead>";

                        if (JSON.stringify(data.d.length) > 0) {
                            $.each(data.d, function (index, value) {
                                date = value.Date;
                                time = value.Time;
                                name = value.AttendeeName;
                                email = value.AttendeeEmail;
                                duration = value.Duration;
                                sessionType = value.SessionType;
                                attendeeId = value.AttendeeId;
                                sessionkey = value.SessionKey;
                                tblHtml += "<tbody>";
                                tblHtml += "<tr>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'><input type='button' class='btnDelete' attr-AttendeeId=" + attendeeId + " attr-sessionkey=" + sessionkey + " value='Delete'></td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + name + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + email + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + date + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + time + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + duration + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + sessionType + "</td>";

                                tblHtml += "</tr>";
                                tblHtml += "</tbody>";
                            });

                        } else {
                            tblHtml += "<tbody>";
                            tblHtml += "<tr>";
                            tblHtml += "<td colspan='7' align='center' style='color:red;'>No record exists.</td>";
                            tblHtml += "</tr>";
                            tblHtml += "</tbody>";
                        }

                        $("#tblAttendees").html(tblHtml);

                        $(".btnPopUP").trigger("click");
                    }
                });
            }

            function populateCoHost(sessionKey) {
                var jsonData = JSON.stringify({ SessionKey: sessionKey });
                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/GetCoHost",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var tblHtml = "";
                        var date = "";
                        var time = "";
                        var name = "";
                        var email = "";

                        var coHostId = "";
                        var sessionkey = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Action</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Name</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Email</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Date</th>";
                        tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Time</th>";
                        //tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>Duration</th>";
                        //tblHtml += "<th style='border:1px solid green; border-collapse:collapse;'>SessionType</th>";

                        tblHtml += "</tr>";
                        tblHtml += "</thead>";

                        if (JSON.stringify(data.d.length) > 0) {
                            $.each(data.d, function (index, value) {
                                date = value.Date;
                                time = value.Time;
                                name = value.CoHostName;
                                email = value.CoHostEmail;
                                //duration = value.Duration;
                                //sessionType = value.SessionType;
                                coHostId = value.CoHostId;
                                sessionkey = value.SessionKey;
                                tblHtml += "<tbody>";
                                tblHtml += "<tr>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'><input type='button' class='btnDeleteCoHost' attr-AttendeeId=" + coHostId + " attr-sessionkey=" + sessionKey + " value='Delete'></td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + name + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + email + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + date + "</td>";
                                tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + time + "</td>";
                                //tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + duration + "</td>";
                                //tblHtml += "<td style='border:1px solid green; border-collapse:collapse;'>" + sessionType + "</td>";

                                tblHtml += "</tr>";
                                tblHtml += "</tbody>";
                            });

                        } else {
                            tblHtml += "<tbody>";
                            tblHtml += "<tr>";
                            tblHtml += "<td colspan='5' align='center' style='color:red;'>No record exists.</td>";
                            tblHtml += "</tr>";
                            tblHtml += "</tbody>";
                        }

                        $("#tblCoHost").html(tblHtml);

                        $(".btnPopUP").trigger("click");
                    }
                });
            }

            $(document).on("click", ".btnDelete", function (e) {
                var attendeeId = $(this).attr("attr-AttendeeId");
                var sessionkey = $(this).attr("attr-Sessionkey");
                if (confirm("Are you sure want to delete?")) {
                    deleteAttendee(attendeeId, sessionkey);
                }
            });
              $(document).on("click", ".btnDeleteCoHost", function (e) {
                var coHostId = $(this).attr("attr-AttendeeId");
                var sessionkey = $(this).attr("attr-Sessionkey");
                if (confirm("Are you sure want to delete?")) {
                    deleteCoHost(coHostId, sessionkey);
                }
            });
            function deleteAttendee(attendeeId, sessionkey) {

                var jsonData = JSON.stringify({ AttendeeId: attendeeId });

                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/DeleetAteendees",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $(".spnErrMsg").text("Attendee deleted successfully.");
                        populateAttendees(sessionkey);
                    }
                });
            }
            function deleteCoHost(coHostId, sessionkey) {

                var jsonData = JSON.stringify({ CoHostId: coHostId });

                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/DeleteCoHosts",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $(".spnErrMsg").text("CoHost deleted successfully.");
                        populateCoHost(sessionkey);
                    }
                });
            }
            $(document).on("click", ".btnInviteAttendee", function (e) {

                var sessionKey = $(this).attr("attr-SessionKey");
                var hostId = $(this).attr("attr-HostId");
                var fromEmail = document.getElementById("<%=hdnLoginEmail.ClientID%>").value;
                var toEmail = document.getElementById("<%=hdnLoginOrgEmail.ClientID%>").value;
                $(".spnMemberTitle").text("Send Email");
                $(".dvSendEMailContent").show();
                $(".dvGridTableContent").hide();
                $(".btnSenEmail").show();
                $(".txFrom").val(fromEmail);
                $(".txtTo").val("");
                $(".txtCC").val(toEmail);
                $(".txtSubject").val("Zoom Invite");
                $(".txtMailBody").val("");
                //  $(".txtMailBody").val(joinURl);
                $(".spnErrMsg").text("");
                $("#spnMemberTitle").text("Send Email");

                var jsonData = JSON.stringify({ SessionKey: sessionKey, HostId: hostId });

                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/GetMeetingInfo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var password = "";
                        var topic = "";
                        var timeZone = "";
                        var joinURl = "";
                        var date = "";
                        var time = "";
                        var coachName = "";
                        $.each(data.d, function (index, value) {
                            password = value.Password;
                            topic = value.Topic;
                            joinURl = value.JoinURL;
                            timeZone = value.TimeZone;
                            date = value.Date;
                            time = value.Time;
                            coachName = value.CoachName;
                        });
                        var content = "";
                        content = "Hi there, "
                        content += "\n\n";
                        content += "" + coachName + " is inviting you to a scheduled Zoom meeting";
                        content += "\n\n"
                        content += "Topic: " + topic + "";
                        content += "\n"
                        content += "Time: " + date + " " + time + " " + timeZone + "(US and Canada)";
                        content += "\n\n"
                        content += "Join from PC, Mac, Linux, iOS or Android: " + joinURl + "";
                        content += "\n"
                        content += "Password: " + password + "";
                        content += "\n\n"
                        content += "Or iPhone one-tap :";
                        content += "\n"
                        content += "US: +16465588656,,550101276# or +16699006833,,550101276# ";
                        content += "\n"
                        content += "Or Telephone:";
                        content += "\n"
                        content += "Dial(for higher quality, dial a number based on your current location): ";
                        content += "\n"
                        content += "US: +1 646 558 8656 or +1 669 900 6833";
                        content += "\n"
                        content += "Meeting ID: " + sessionKey + "";
                        content += "\n"
                        content += "International numbers available: https://zoom.us/u/bg1A7g1o";

                        $(".txtMailBody").val(content);
                        //$(".txtMailBody").val(replace($(".txtMailBody").val(), "<br>", "&#10;"));
                        $(".btnPopUP").trigger("click");
                    }
                });


            });
            $(document).on("click", ".btnSenEmail", function (e) {

                senEmailToCoachAndparents();
            });
            function senEmailToCoachAndparents() {

                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
                var from = $(".txFrom").val();
                var to = $(".txtTo").val();

                var cc = $(".txtCC").val();
                var subject = $(".txtSubject").val();

                var body = $(".txtMailBody").val().replace(/\n/gi, '<br />');

                var jsonData = JSON.stringify({ FromEmail: from, ToEmail: to, Subject: subject, Body: body, CC: cc });

                var url = "SetupZoomSessions.aspx/SendEmailsCoachesAndParents";

                if (validateEmaul() == 1) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                $(".spnErrMsg").text("Email has been sent successfully");
                                $("#fountainTextG").css("display", "none");
                                $("#overlay").css("display", "none");
                                $().toastmessage('showToast', {
                                    text: 'Emails sent successfully',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });
                }
            }
            function validateEmaul() {
                var retVal = 1;

                var from = $(".txFrom").val();
                var to = $(".txtTo").val();
                var cc = $(".txtCC").val();
                var subject = $(".txtSubject").val();
                var body = $(".txtMailBody").val();

                if (from == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter From email");
                } else if (to == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter To email");
                } else if (subject == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter Subject");
                } else if (body == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter Content");
                }
                return retVal;
            }
        </script>
    </div>


    <asp:Button ID="BtnDelete" runat="server" Text="Delete" OnClick="BtnDelete_Click" Style="display: none;" />
    <table id="tblLogin" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none;" runat="server" align="center" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>
        </tr>
        <tr>

            <td>
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Zoom Sessions</strong>
                </div>
            </td>
        </tr>

    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:Label ID="lblErr" runat="server"></asp:Label>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvAttendee" runat="server" style="width: 390px; margin-left: auto; background-color: #FFFFCC; height: 25px; padding: 10px; margin-right: auto;" visible="false">
        <div style="width: 250px; float: left;">
            <div style="width: 90px; float: left;">
                <span style="font-weight: bold;" id="spnAttendeeTitle" runat="server">Attendee</span>
            </div>
            <div style="width: 150px; float: left;">
                <asp:DropDownList ID="ddlAttendee" Style="width: 140px;" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; margin-left: 10px;">
            <asp:Button ID="btnAddAttendee" runat="server" Text="Add" OnClick="btnAddAttendee_Click" />
        </div>
        <div style="float: left; margin-left: 10px;">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <center>
        <div id="dvHostSection" runat="server" visible="false">
            <center>
                <asp:Label ID="Label2" runat="server" Font-Bold="true">Table: Zoom Sessions (Host)</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblHostNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="dvHostNoRecord" runat="server">
                <asp:GridView ID="GrdZoomSessionsHost" runat="server" Width="100%" OnRowCommand="GrdZoomSessionsHost_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                        <asp:BoundField DataField="Time" HeaderText="Time" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType" />
                        <asp:BoundField DataField="RecurringType" HeaderText="Recurring Type" />
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                        <asp:BoundField DataField="Topic" HeaderText="Topic" />
                        <asp:BoundField DataField="SessionKey" HeaderText="SessionKey" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>

                                <div style="display: none;">

                                    <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>
                                    <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberId") %>'></asp:Label>
                                    <asp:Label ID="lblAttEventId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventId") %>'></asp:Label>
                                    <asp:Label ID="lblAttDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                                    <asp:Label ID="lblAttTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                    <asp:Label ID="lblAttSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'></asp:Label>
                                    <asp:Label ID="lblhostId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostId") %>'></asp:Label>
                                    <asp:Label ID="lblHostURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostURL") %>'></asp:Label>
                                     <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                                </div>

                                <asp:Button ID="BtnAddAttendee" runat="server" Text="Add" CommandName="AddAttendee" />
                                <%-- <asp:Button ID="BtnViewAttendee" runat="server" Text="View" CommandName="View" />--%>
                                <input type="button" class="btnViewAttendee" value="View" attr-sessionkey='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>' />
                                <input type="button" class="btnInviteAttendee" value="Invite" attr-sessionkey='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>' attr-hostid='<%#DataBinder.Eval(Container.DataItem,"HostId") %>' />
                                <asp:Button ID="btnJoinMeeting" runat="server" Text="Join" CommandName="Join" />
                                <asp:Button ID="BtnAddCoHost" runat="server" Text="Add Co-Host" CommandName="AddCoHost" />

                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </center>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <center>
        <div id="dvCoHost" runat="server" visible="false">
            <center>
                <asp:Label ID="lblCoHost" runat="server" Font-Bold="true">Table: Zoom Sessions (Co-Host)</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblCoHostNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="Div2" runat="server">
                <asp:GridView ID="GrdCoHost" runat="server" Width="100%" OnRowCommand="GrdCoHost_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                        <asp:BoundField DataField="Time" HeaderText="Time" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType" />
                        <asp:BoundField DataField="RecurringType" HeaderText="Recurring Type" />
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                        <asp:BoundField DataField="Topic" HeaderText="Topic" />
                        <asp:BoundField DataField="SessionKey" HeaderText="SessionKey" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>

                                <div style="display: none;">

                                    <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>
                                    <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberId") %>'></asp:Label>
                                    <asp:Label ID="lblAttEventId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventId") %>'></asp:Label>
                                    <asp:Label ID="lblAttDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                                    <asp:Label ID="lblAttTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                    <asp:Label ID="lblAttSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'></asp:Label>
                                    <asp:Label ID="lblhostId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostId") %>'></asp:Label>
                                </div>

                                <asp:Button ID="BtnAddAttendee" runat="server" Text="Add" CommandName="AddAttendee" />
                                <%-- <asp:Button ID="BtnViewAttendee" runat="server" Text="View" CommandName="View" />--%>
                                <input type="button" class="btnViewAttendee" value="View" attr-sessionkey='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>' />
                                <input type="button" class="btnInviteAttendee" value="Invite" attr-sessionkey='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>' attr-hostid='<%#DataBinder.Eval(Container.DataItem,"HostId") %>' />
                                <asp:Button ID="btnJoinMeeting" runat="server" Text="Join" CommandName="Join" />

                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </center>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <center>
        <div id="dvAttendeesSection" runat="server" visible="false">
            <center>
                <asp:Label ID="lblAttendees" runat="server" Font-Bold="true">Table: Attendees</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="LblAttendeesNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="dvAttendees" runat="server">
                <asp:GridView ID="GrdAttendee" runat="server" Width="80%" OnRowCommand="GrdAttendee_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Join URL">
                            <ItemTemplate>

                                <div style="display: none;">

                                    <asp:Label ID="lblAttendeeId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeId") %>'></asp:Label>

                                </div>
                                <asp:Button ID="btnDeleteMeeting" runat="server" Text="Delete" CommandName="DeleteMeeting" />


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="Year" HeaderText="Year" Visible="false" />
                        <asp:BoundField DataField="Name" HeaderText="Attendee Name" />
                        <asp:BoundField DataField="Email" HeaderText="Attendee Email" />
                        <asp:BoundField DataField="HostName" Visible="false" HeaderText="Host Name" />
                        <asp:BoundField DataField="HostEmail" Visible="false" HeaderText="Host Email" />
                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                        <asp:BoundField DataField="Time" HeaderText="Time" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType" />




                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </center>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <center>
        <div id="dvZoomSection" runat="server" visible="false">
            <center>
                <asp:Label ID="Label1" runat="server" Font-Bold="true">Table: Zoom Sessions (Attendee)</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblAttendeeNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="dvAttendeeGrid" runat="server">
                <asp:GridView ID="gvAttendee" runat="server" Width="80%" OnRowCommand="gvAttendee_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="Year" Visible="false" HeaderText="Year" />
                        <asp:BoundField DataField="Name" Visible="false" HeaderText="Attendee Name" />
                        <asp:BoundField DataField="Email" Visible="false" HeaderText="Attendee Email" />
                        <asp:BoundField DataField="HostName" HeaderText="Host Name" />
                        <asp:BoundField DataField="HostEmail" HeaderText="Host Email" />
                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                        <asp:BoundField DataField="Time" HeaderText="Time" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration" />
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType" />
                        <asp:BoundField DataField="Topic" HeaderText="Topic" />

                        <asp:TemplateField HeaderText="Join URL">
                            <ItemTemplate>

                                <div style="display: none;">

                                    <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>
                                    <%-- <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>--%>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                </div>
                                <asp:Button ID="btnJoinMeeting" runat="server" Text="Join" CommandName="Join" />


                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </center>

    <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>

    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Send Email</span>
            </div>

            <div class="ezmodal-content">

                <div class="dvSendEMailContent" style="display: none;">
                    <div align="center"><span class="spnErrMsg" style="color: red;"></span></div>
                    <div style="clear: both; margin-bottom: 5px;"></div>
                    <div>
                        <div class="from">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">From </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txFrom" style="width: 400px;" />
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="To">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">To </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txtTo" style="width: 400px;" />
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="CC">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">CC </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtCC" style="width: 600px; height: 18px;"></textarea>

                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="subjext">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Subject </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtSubject" style="width: 600px; height: 18px;"></textarea>
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="body">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Body </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <div>
                                <asp:TextBox ID="txtPP" runat="server" Style="display: none;"></asp:TextBox>
                                <textarea class="txtMailBody" rows="10" cols="10" style="width: 800px; height: 150px;"></textarea>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="dvGridTableContent" style="display: none;">
                    <center><span style="color: black; font-weight: bold;">Table: Attendee</span></center>
                    <table id="tblAttendees" style='border: 1px solid green; border-collapse: collapse;'>
                    </table>
                    <div style="clear: both; margin-bottom: 20px;"></div>
                    <center><span style="color: black; font-weight: bold;">Table: Co-Host</span></center>
                    <center>
                        <table id="tblCoHost" style='border: 1px solid green; border-collapse: collapse;'>
                        </table>
                    </center>
                </div>

            </div>



            <div class="ezmodal-footer">
                <button id="Button1" type="button" class="btnSenEmail" style="display: none;">Send Email</button>
                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

    <asp:HiddenField ID="hdnZoomURL" runat="server" Value="" />
    <asp:HiddenField ID="hdnSessionKey" runat="server" Value="" />
    <asp:HiddenField ID="hdnJoinURl" runat="server" Value="" />
    <asp:HiddenField ID="hdnDate" runat="server" Value="" />
    <asp:HiddenField ID="hdnTime" runat="server" Value="" />
    <asp:HiddenField ID="hdnMemberId" runat="server" Value="" />
    <asp:HiddenField ID="hdnEventId" runat="server" Value="" />
    <asp:HiddenField ID="hdnAttendeeId" runat="server" Value="" />
    <asp:HiddenField ID="hdnHostURL" runat="server" Value="" />
    <asp:HiddenField ID="hdnLoginEmail" runat="server" Value="" />
    <asp:HiddenField ID="hdnLoginOrgEmail" runat="server" Value="" />
    <asp:HiddenField ID="hdnCoHostId" runat="server" Value="" />
    
    <asp:HiddenField ID="hdnDuration" runat="server" Value="" />
</asp:Content>
