<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowParticipantCertificate.aspx.vb" Inherits="ShowParticipantCertificate" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>  
        <style type="text/css" media="screen" >
        <!-- 
        p.MsoNormal, li.MsoNormal, div.MsoNormal
	    {
	        margin:0in;
	        margin-bottom:.0001pt;
	        text-autospace:none;
	        font-size:10.0pt;
	        font-family:"Times New Roman","serif";
	    }  
        @page Section1
	        {
	            size:11.0in 8.5in;
	            margin:.5in 1.0in .5in 1.0in;
	        }
        div.Section1
	        {
	            page:Section1;
	        }
        -->
        </style>
        <script language="javascript" type="text/javascript">
        function printdoc()
        {
            document.getElementById('btnPrint').style.visibility="hidden";
            document.getElementById('hlnkMainMenu').style.visibility="hidden";            
            window.print();
            document.getElementById('btnPrint').style.visibility="visible";
            document.getElementById('hlnkMainMenu').style.visibility="visible";            
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Section1">
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
             
               <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0" >                
                    <tr>
                        
                        <td class="ItemCenter" align="center" rowspan="2">
                            <asp:Image runat="server" ID="imgThinkingMan" ImageUrl="Images/image002.gif" />                            
                        </td>        
                        <td class="ItemCenter" colspan="2" align="center" valign="top" >
                            <asp:Image runat="server" ID="imgHeader"  ImageUrl="Images/image008.gif" Height="62px" Width="90%" /><br />
                        </td>
                        <td class="ItemCenter" colspan="2" align="center" valign="top" >
                            
                           
                            
                             <asp:Image runat="server" ID="imgCertificate"  ImageUrl="Images/image003.png" Height="62px"/>
                           
                            <br />
                        </td> 
                       </tr>
                       <tr>
                       <td class="ItemCenter" colspan="2" align="center" valign="top" >                      
                       <asp:Image runat="server" ID="Image1"  ImageUrl="Images/image007.gif" Width="90%"/>
                       </td>
                       <td class="ItemCenter" colspan="2" align="center" valign="top" >
                            <asp:Image runat="server" ID="imgBee"  ImageUrl="http://www.northsouth.org/app9/Images/image006.gif" />
                       </td>
                    </tr>       
                            
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle" ForeColor="brown" Text="Certificate of Participation" Font-Bold="true" Font-Size="28"></asp:Label>
                        </td>
                    </tr>                   
                       
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="Label1" ForeColor="brown" Text="awarded to" Font-Bold="true" Font-Size="15"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>  
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblName" ForeColor="Blue" Font-Bold="true" Font-Size="22" Text='<%# DataBinder.Eval(Container,"DataItem.First_Name").ToString() & " " & DataBinder.Eval(Container,"DataItem.Last_Name").ToString() %>'></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>  
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            <b>
                            <font face="Arial">for successfully participating in  
                              <% If Session("SelChapterID") = 1 Then%>
                               <asp:Label runat="server" ID="Label2" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ExamName") %>'/>
                              <%Else%>
                               <asp:Label runat="server" ID="lblProduct" Font-Bold="true" Text='<%# GetProductName(DataBinder.Eval(Container,"DataItem.ChildNumber")) %>'/>
                              <%End If%>
                           
                            during the
                            <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.contestyear") %>'></asp:Label>
                            
                            <% If Session("SelChapterID") = 1 Then%>
                            National Championship Finals held on  <asp:Label runat="server" ID="Label3"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ContestDate") %>'></asp:Label> at
                            <%Else%>
                             regional contests held at the
                            <%End If%>
                           
                            <asp:Label runat="server" ID="lblLocation" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label> Chapter.
                            </font>
                            </b>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>  
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            
                            <font face="Arial Narrow"  size="3" style="font-style: italic;" > North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.</font>
                        </td>
                    </tr>
                     
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                  
                    <tr>
                        <td colspan="8" >
                             <table cellspacing="0" cellpadding="0" width="98%"  align="left" border="0" >                
                                <tr >
                                    <td colspan="3"  align="left" width="40%" height="60" >
                                    
                                    <hr  />
                                    </td> 
                                
                                    <td colspan="2" align="center">
                                        &nbsp;
                                    </td> 
                                
                                    <td colspan="3" align="left" width="40%" height="60" > 
                                    
                                   
                                        <hr />
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="3" align="left">
                                    <% If Session("SelChapterID") <> 1 Then%>
                                    <asp:Label runat="server" ID="Label11" Font-Names="Arial" Font-Size="12" Font-Bold="true"  Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.LeftTitle"),Session("LeftTitle")) %>'></asp:Label>&nbsp;
                                    <asp:Label runat="server" ID="Label12" Font-Names="Arial" Font-Size="12" Font-Bold="true" Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.LeftSignatureName"),Session("LeftSignatureName")) %>'></asp:Label>                                        
                                    <%--
                                    <asp:Label runat="server" ID="lblLeftTitle" Font-Names="Arial" Font-Size="12" Font-Bold="true"  Text='<%# Session("LeftTitle") %>'></asp:Label>&nbsp;
                                    <asp:Label runat="server" ID="lblLeftSignature" Font-Names="Arial" Font-Size="12" Font-Bold="true" Text='<%# Session("LeftSignatureName") %>'></asp:Label>                                        
                                   --%>
                                         
                                    <%else %>  
                                        <asp:Label runat="server" ID="Label4" Font-Names="Arial" Font-Size="12" Font-Bold="true"  Text='<%# DataBinder.Eval(Container,"DataItem.LeftTitle") %>'></asp:Label>&nbsp;
                                        <asp:Label runat="server" ID="Label5" Font-Names="Arial" Font-Size="12" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.LeftSignatureName") %>'></asp:Label>                                        
                                     <%End If%>   
                                    </td>
                                    <td colspan="2" align="center" >
                                        <asp:Label runat="server" ID="lblFooter" ForeColor="brown"  Text="www.northsouth.org" Font-Bold="true" Font-Size="15" ></asp:Label>
                                    </td>
                                    <td  colspan="3" align="left">
                                     <% If Session("SelChapterID") <> 1 Then%>
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="12"  Font-Names="Arial" Font-Bold="true" Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.RightTitle"),Session("RightTitle")) %>'></asp:Label>&nbsp;
                                        <asp:Label runat="server" ID="lblRightSignature" Font-Names="Arial" Font-Size="12"  Font-Bold="true" Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.RightSignatureName"),Session("RightSignatureName")) %>'></asp:Label>
                                       <%else %>   
                                        <asp:Label runat="server" ID="Label6" Font-Size="12"  Font-Names="Arial" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.RightTitle") %>'></asp:Label>&nbsp;
                                        <asp:Label runat="server" ID="Label7" Font-Names="Arial" Font-Size="12"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.RightSignatureName") %>'></asp:Label>
                                       <%End If%>   
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="3" align="left">
                                    <% If Session("SelChapterID") <> 1 Then%>                                  
                                    <asp:Label runat="server" Font-Names="Arial"  ID="lblSigTitle" Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.LeftSignatureTitle"),Session("LeftSignatureTitle")) %>'></asp:Label>
                                    <%else %>   
                                    <asp:Label runat="server" Font-Names="Arial"  ID="Label8" Text='<%# DataBinder.Eval(Container,"DataItem.LeftSignatureTitle") %>'></asp:Label>
                                     <%End If%>   
                                    </td>
                                    <td colspan="2" align="left">&nbsp;</td>
                                    <td colspan="3" align="left">
                                    <% If Session("SelChapterID") <> 1 Then%>
                                    <asp:Label runat="server" ID="lblRightSigTitle" Font-Names="Arial"  Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),DataBinder.Eval(Container,"DataItem.RightSignatureTitle"),Session("RightSignatureTitle")) %>'></asp:Label>
                                     <%else %>   
                                    <asp:Label runat="server" ID="Label9" Font-Names="Arial"  Text='<%# DataBinder.Eval(Container,"DataItem.RightSignatureTitle") %>'></asp:Label>
                                    <%End If%>   
                                    </td>
                                </tr>
                            </table>
                        </td> 
                    </tr>                   
		       </table> 
	</div> 
		      
		
    </form>
</body>
</html>


 
 
 