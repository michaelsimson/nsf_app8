Imports System
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Specialized
Imports System.Data.OleDb
Imports NativeExcel

Namespace VRegistration
    Partial Class CCReconcile
        Inherits System.Web.UI.Page

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If
            'Put user code to initialize the page here
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Try
                        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Or (Session("RoleID") = "38") Then
                            'for Volunteer more than roleid 1,84,85  
                            Response.Write("<script language='javascript'>")
                            Response.Write(" window.open('ccreconcilestatus.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
                            Response.Write("</script>")
                        Else
                            Response.Redirect("VolunteerFunctions.aspx")
                        End If
                    Catch ex As Exception
                        Response.Redirect("VolunteerFunctions.aspx")
                    End Try
                End If
            End If
        End Sub

        Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Label1.Text = ""
            closepanel()
            If RbtnUploadFile.Checked = True Then
                uploadfile() 'Uploads data to temp[PPTemp,MSTemp,EmTemp and from EMTemp to EMTemp2] tables
            ElseIf RbtnPostData.Checked = True Then
                If ddlPostData.SelectedValue > 0 Then
                    postdata() 'Moves data from Temp tables to PPCharge ,MSCharge,EMcharge respectively
                Else
                    Label1.Text = "Please select the File type to post data"
                End If
                
            ElseIf RbtnPPMS.Checked = True Then
                Dim StrPPCharge_Update As String = ""
                Dim StrMSCharge_Update As String = ""
                Dim i As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec")
                Dim j As Integer = 0
                Dim records As SqlDataReader
                'Reconcile Charge Data (Reconcile LinkPoint with Merchant Services) and inserting Values into ChargeRec         
                Dim cmdText As String = " Update PP set PP.ModifiedDate=GetDate(), PP.ModifiedBy=" & Session("LoginID") & ", PP.MS_Match='Y',PP.MSChargeID = MS.MSChargeId "
                cmdText = cmdText & " FROM PPCharge PP INNER JOIN MSCharge MS on MS.MS_Amount = PP.PP_Amount and MS.MS_CN1 = PP.PP_CN1 and MS.MS_CN3 = PP.PP_CN2 and  Datediff(d,PP.PP_Date,MS.MS_TransDate) >= 0 and "
                cmdText = cmdText & " Datediff(d,PP.PP_Date,MS.MS_TransDate) <= 3 and PP.MSChargeID is null and MS.PPChargeID is null and (MS.PP_Match is Null or MS.PP_Match <>'Y')"
                cmdText = cmdText & " where (PP.MS_Match is Null or PP.MS_Match <>'Y')  and "
                cmdText = cmdText & " not exists (Select Distinct PPChargeid from MSCharge where PPChargeid is not null and PPChargeid=PP.PPChargeid)"

                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    cmdText = " Update MS set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",PP_Match='Y',MS.PPChargeID = PP.PPChargeID from MSCharge MS"
                    cmdText = cmdText & " Inner join ppcharge PP on PP.PP_Amount= MS.MS_Amount and PP.MSChargeID=MS.MSChargeId"
                    cmdText = cmdText & " where MS.PPChargeId is null and (MS.PP_Match is Null or MS.PP_Match <>'Y')"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                Catch ex As Exception
                End Try
                Dim BeginRecID, EndRecID As Integer
                Try
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull(max(ChargeRecID),0) from ChargeRec Where ChargeRecid is not null") + 1
                Catch ex As Exception
                    BeginRecID = 0
                End Try
                'Inserting Records into Charge Rec
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into ChargeRec(PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, PPChargeID, MSChargeID, PP_MS_Match, CreateDate,CreatedBy,Brand,MemberID) Select PP.PP_Date, PP.PP_OrderNumber, PP.PP_CN1, PP.PP_CN2, PP.PP_Amount, PP.PP_Approval, MS.MS_TransDate, MS.MS_Amount, MS.MS_CN1, Ms.MS_CN2, Ms.MS_CN3,PP.PPChargeID, MS.MSChargeID,'Y',GetDate()," & Session("LoginID") & ",Case when PP.PP_Type<>'Sale' Then PP.PP_Type Else NULL End,PP.MemberID From MSCharge MS,PPCharge PP where  PP.PPChargeid = MS.PPChargeID and PP.ChargeRecID is Null  and PP.PPChargeID Not in (Select Distinct PPChargeid from ChargeRec where PPChargeid is not null) order by PP.PPChargeID")
                Dim k As Integer = 0
                Try
                    '** Reconciled for refunded data to fix the Payment and Refund amount with the MSCharge in credit cards
                    Dim CmdString As String = "select Distinct P.PPchargeID AS PPchargeID1,PP.PPchargeID AS PPchargeID2, P.PP_Amount as PP_Amount1, PP.PP_Amount as PP_Amount2, (P.PP_Amount+PP.PP_Amount) AS Amount,P.PP_CN1 as CN1, P.PP_CN2 as CN2,P.PP_Date,MS.MSChargeID,MS.MS_Amount,MS.MS_TransDate from PPcharge P "
                    CmdString = CmdString & "INNER JOIN PPCharge PP ON P.PP_CN1=PP.PP_CN1 AND P.PP_CN2=PP.PP_CN2 and DateDiff(d,P.PP_date,PP.PP_Date)=0 " 'P.PP_OrderNumber = PP.PP_OrderNumber AND
                    CmdString = CmdString & "INNER JOIN MSCharge MS ON MS.MS_CN1=PP.PP_CN1 AND MS.MS_CN3=PP.PP_CN2  "
                    CmdString = CmdString & " where(P.MSchargeId Is null And PP.MSchargeId Is Null And MS.PPchargeId Is Null And P.PPChargeID <> PP.PPChargeID And (P.PP_Amount + PP.PP_Amount) > 0 And MS.MS_Amount = (P.PP_Amount + PP.PP_Amount) And DateDiff(d, PP.PP_Date, MS.MS_TransDate) >= 0 And DateDiff(d, PP.PP_Date, MS.MS_TransDate) < 3 And PP.PP_Amount < 0 And P.PP_Amount > 0 And ABS(PP.PP_Amount) <= P.PP_Amount)"
                    records = SqlHelper.ExecuteReader(Application("connectionstring"), CommandType.Text, CmdString)
                    While records.Read()
                        k = k + 2
                        cmdText = " Update PPCharge set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", MS_Match='Y',MSChargeID = " & records("MSChargeID") & "  where PP_CN1 ='" & records("CN1") & "' and PP_CN2='" & records("CN2") & "' and MSChargeId is null and (MS_Match is Null or MS_Match <>'Y') and PPChargeID in (" & records("PPChargeID1") & "," & records("PPChargeID2") & ") and PPChargeid not in (Select Distinct PPChargeid from MSCharge where PPChargeid is not null) "
                        cmdText = cmdText & " Update MSCharge set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",PP_Match='Y',PPChargeID = " & records("PPChargeID1") & " from MSCharge MS where PPChargeId is null and (PP_Match is Null or PP_Match <>'Y') and MSChargeID=" & records("MSChargeID") & " "
                        cmdText = cmdText & " Insert into ChargeRec(PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, PPChargeID, MSChargeID, PP_MS_Match, CreateDate,CreatedBy,Brand,MemberID) Select PP.PP_Date, PP.PP_OrderNumber, PP.PP_CN1, PP.PP_CN2, PP.PP_Amount, PP.PP_Approval, MS.MS_TransDate, PP.PP_Amount, MS.MS_CN1, Ms.MS_CN2, Ms.MS_CN3,PP.PPChargeID, MS.MSChargeID,'Y',GetDate()," & Session("LoginID") & ",Case when PP.PP_Type<>'Sale' Then PP.PP_Type Else NULL End,PP.MemberID From PPCharge PP Left Join MSCharge MS ON PP.MSChargeid = MS.MSChargeID  where  PP.PPChargeID Not in (Select Distinct PPChargeid from ChargeRec where PPChargeid is not null) AND PP.PPChargeid in (" & records("PPChargeID1") & "," & records("PPChargeID2") & ")  order by PP.PPChargeID"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    End While
                    records.Close()
                Catch ex As Exception
                    Label1.Text = "Error In Second Phase of Reconciling " & ex.ToString()
                End Try
                'Posting into CCPostinglog file
                Try
                    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                Catch ex As Exception
                    EndRecID = 0
                End Try

                Dim nodataflag As Boolean = False
                If BeginRecID = 0 And EndRecID > 0 Then
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                    nodataflag = True
                End If
                If EndRecID < BeginRecID And nodataflag = False Then
                    BeginRecID = 0
                    EndRecID = 0
                ElseIf EndRecID = BeginRecID And nodataflag = True Then
                    BeginRecID = 0
                    EndRecID = 0
                End If
                Dim BeginDate, EndDate As String
                If BeginRecID > 0 Then
                    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(PP_Date) from ChargeRec where PP_date is Not Null and ChargeRecID >=" & BeginRecID & "") & "'"
                Else
                    BeginDate = "NULL"
                End If
                If EndRecID > 0 Then
                    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(PP_Date) from ChargeRec where PP_date is Not Null") & "'"
                Else
                    EndDate = "Null"
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values(0," & BeginRecID & "," & EndRecID & ",0," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
                'Updating The Populating PPCharge and MSCharge with ChargeRecID's
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update PPCharge set ChargeRecID = C.ChargeRecID from ChargeRec C,PPCharge PP where C.PPChargeid=PP.PPChargeID and PP.ChargeRecID is Null and PP.MS_Match is Not Null;update MSCharge set ChargeRecID = C.ChargeRecID from ChargeRec C,MSCharge MS where C.MSChargeid=MS.MSChargeID and MS.ChargeRecID is Null and MS.PP_Match is Not Null")
                Label1.Text = " Total Rows Reconciled & Inserted Into Charge Rec : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChargeRec") - i
            ElseIf RbtnVwPPMS.Checked = True Then
                If DdlVwPPMS.SelectedValue = 1 Then
                    GetVwChargeRec()
                ElseIf DdlVwPPMS.SelectedValue = 2 Then
                    GetVwPP()
                    GetVwMS()
                Else
                    Label1.Text = "Please Select data type to View PP/MS Data"
                End If
            ElseIf RbtnSDNSF.Checked = True Then
                'Reconcile ChargeRec Data/NSF - NFG_Transactions table
                Dim intialcount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec where NFG_uniqueid is not null")
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND (ChargeRec.PP_OrderNumber = b.asp_session_id) AND (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5)))  AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL)")
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND (ChargeRec.PP_OrderNumber = b.asp_session_id) AND (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5)))  AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL)")

                '************* Added on 09-07-2014 to update NFG_UniqueID in ChargeREc using CCsubmitLog table values.[Matching CCSubmitLog.RefundReference = ChargeRec.PP_OrderNumber and CCSubmitLog.PaymentRefernce=NFG_Transactions.asp_session_id]
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=4240,NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b  Inner join CCSubmitLog CC on CC.RefundReference =PP_OrderNumber and CC.PaymentReference =b.asp_session_id and CC.Amount =PP_Amount and CC.Amount =b.TotalPayment and CC.PaymentDate =b.[Payment Date]  WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND  (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5))) AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL) and CC.AspxPage ='Refund'") ' and b.MS_TransDate = ChargeRec.MS_TransDate ")
                '******************************************************************************************************************************************************************************************'

                '************* Added on 07-07-2014 to update Reference/Comments from NFG_Transactions table if the values are not updated from EMCharge table ****************************'
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", Reference= (Select top 1 Case When EventID in (1,2,3,13,19) then case When CharIndex('Chapter',Name)>0 then Replace(Name,'Chapter','') Else Name End + ' Registration' Else Name END  From Event Where EventId=b.EventID) , Comments=b.PaymentNotes , MemberID=b.MemberID FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE ChargeRec.PP_OrderNumber = b.asp_session_id AND ChargeRec.PP_Amount = b.TotalPayment AND ChargeRec.NFG_UniqueID = b.Unique_ID AND ChargeRec.NFG_PaymentDate =b.[Payment Date] and (ChargeRec.Reference is null Or ChargeRec.Comments is null)")
                '******************************************************************************************************************************************************************************************'

                Label1.Text = "Rows Reconciled :" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec where NFG_uniqueid is not null") - intialcount
            ElseIf RbtnVwSDNSF.Checked = True Then
                ' View Charge Rec Data/NSF
                If DdlVwSDNSF.SelectedValue > 0 Then
                    VwSDNSF()
                Else
                    Label1.Text = "Please Select data type to View Charge Rec Data/NSF Data"
                End If
            ElseIf RbtnPostNSFTrans.Checked = True Then
                Dim intialcount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "UPDATE_MSTransDate_Brand")
                Label1.Text = "" & intialcount & " rows updated in NFG_Transactions Table"
            ElseIf RbtnViewNFGData.Checked = True Then
                If Not IsDate(txtFromDate.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Not IsDate(txtToDate.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
                    Label1.Text = "From date is greater than To Date.Please verify"
                    Exit Sub
                Else
                    VwNFGData(txtFromDate.Text, txtToDate.Text)
                End If
                ' VwNFGData()
            ElseIf RbtnPostNFGSupp.Checked = True Then
                Response.Redirect("NFG_Supplement.aspx")
            ElseIf RbtnViewUnRecData.Checked = True Then
                If Not IsDate(txtDateFrom.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Not IsDate(txtDateTo.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtDateFrom.Text) > Convert.ToDateTime(txtDateTo.Text) Then
                    Label1.Text = "From date is greater than To Date.Please verify"
                    Exit Sub
                Else
                    VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
                End If
                'VwNFGMatchedData()
            Else
                Label1.Text = "Please Choose the option"
            End If
            clearddl()
        End Sub
        Sub uploadfile()
            'Moving data from CSV file to Temp Tables
            If FileUpLoad1.HasFile And ddlCSVUpload.SelectedValue > 0 Then
                Dim fileExt, filename As String
                Dim filecount As Integer = 0
                fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName).ToLower
                filename = Left(FileUpLoad1.FileName, 2)
                If (fileExt = ".csv") Then
                    Try
                        Dim strfilename, FileType As String
                        Select Case ddlCSVUpload.SelectedValue
                            Case 1
                                FileType = "PP"
                            Case 2
                                FileType = "MS"
                            Case 3
                                FileType = "EM"
                        End Select
                        If filename.ToUpper = FileType Then
                            strfilename = FileType & "file" & Now.Date.ToString("ddMMyyyy")
                            'FileCount is the count of file name in database
                            filecount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from fileupload where fileType='" & FileType & "' AND DateDiff(d,createDate,GetDate())=0")
                            If filecount <> 0 Then
                                strfilename = strfilename & filecount.ToString()
                            End If
                            strfilename = strfilename & ".csv"
                            FileUpLoad1.PostedFile.SaveAs(Server.MapPath("UploadFiles" & "/" & strfilename))

                            Label1.Text = "Received " & strfilename & " Content Type " & FileUpLoad1.PostedFile.ContentType & " Length " & FileUpLoad1.PostedFile.ContentLength
                            Dim flag As Integer = 0
                            'Upload Files 
                            Dim dataCount As Integer = 0
                            Select Case ddlCSVUpload.SelectedValue
                                Case 1
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from PPTemp")
                                    If dataCount = 0 Then
                                        ImportPPCSV_Database(Server.MapPath("UploadFiles/" & strfilename))
                                        If Not validateData() Then
                                            'MsgBox("Not ok")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table PPTemp")
                                            Label1.Text = "Error Exist in the Data. No Records Uploaded"
                                        Else
                                            '**Have to down up if he no need to store the wrong data. 
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & strfilename & "','" & FileType & "',GetDate()," & Session("LoginID") & ")")
                                        End If
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                                Case 2
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSTemp")
                                    If dataCount = 0 Then
                                        ImportMSCSV_Database(Server.MapPath("UploadFiles/" & strfilename))
                                        If Not validateData() Then
                                            'MsgBox("Not ok")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table MSTemp")
                                            Label1.Text = "Error Exist in the Data. No Records Uploaded"
                                        Else
                                            '**Have to down up if he no need to store the wrong data. 
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & strfilename & "','" & FileType & "',GetDate()," & Session("LoginID") & ")")
                                        End If
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                                Case 3
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from EMTemp2")
                                    If dataCount = 0 Then
                                        ImportEMCSV_Database(Server.MapPath("UploadFiles/" & strfilename))
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_ConvertEMTemp2")
                                        If Not validateData() Then
                                            'MsgBox("Not ok")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table EMTemp2")
                                            Label1.Text = "Error Exist in the Data. No Records Uploaded"
                                        Else
                                            '**Have to down up if he no need to store the wrong data. 
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & strfilename & "','" & FileType & "',GetDate()," & Session("LoginID") & ")")
                                        End If
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                            End Select
                            ddlCSVUpload.SelectedIndex = ddlCSVUpload.Items.IndexOf(ddlCSVUpload.Items.FindByValue(0))
                        Else
                            Label1.Text = "File type selected Mismatch with Filename."
                        End If
                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                        'Response.Write(ex.ToString())
                    End Try
                Else
                    Label1.Text = "Not a Csv file"
                End If

            Else
                Label1.Text = "No uploaded file / File Type Not Selected"
            End If
        End Sub

        Function validateData() As Boolean
            Dim recordset As SqlDataReader
            Select Case ddlCSVUpload.SelectedValue
                Case 1
                    'PP
                    Try
                        recordset = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT Top 1  OrderNo, PDate, UserID, Type, PayerAuth,  CardRouteNo, ExpDate, Approval, Amount FROM PPTemp") ' StoreID,InvoiceNo, PONo, TransID,
                        While recordset.Read()
                            Dim OrderNo As String = recordset("OrderNo")
                            Dim PDate As Date = recordset("PDate")
                            Dim Type As String = recordset("Type")
                            Dim CardRouteNo As String = recordset("CardRouteNo")
                            Dim Approval As String = recordset("Approval")
                            Dim Amount As Integer = recordset("Amount")
                            If Amount = 0 Then
                                Return False
                            End If
                        End While
                        Return True
                    Catch ex As Exception
                        Return False
                    End Try
                Case 2
                    'MS
                    Try
                        recordset = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT Top 1   TransactionDate, case when SUBSTRING(Amount, 1, 1) = '(' then '-' + SUBSTRING(Amount, 2, 5) else SUBSTRING(Amount, 1, 5) END AS Amount, CardHolderNumber FROM MSTemp   order by TransactionDate")
                        While recordset.Read()
                            Dim TransactionDate As Date = recordset("TransactionDate")
                            Dim Amount As Integer = recordset("Amount")
                            Dim CardHolderNumber As String = recordset("CardHolderNumber")
                        End While
                        Return True
                    Catch ex As Exception
                        'MsgBox(ex.ToString())
                        Return False
                    End Try
                Case 3
                    'EM
                    Try
                        recordset = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT  Top 1    Type, OrderNumber, Tdate, Brand, Approved, Name, Address, city, Zip, State, Reference, Comments, Amount FROM EMTemp2")
                        Dim flag As Boolean = False
                        While recordset.Read()
                            flag = True
                            Dim Type As String = recordset("Type")
                            Dim OrderNumber As String = recordset("OrderNumber")
                            Dim Tdate As String = recordset("Tdate")
                            Dim Brand As String = recordset("Brand")
                            Dim Approved As String = recordset("Approved")
                            Dim Name As String = recordset("Name")
                            Dim Address As String = recordset("Address")
                            Dim city As String = recordset("city")
                            Dim Zip As String = recordset("Zip")
                            Dim State As String = recordset("State")
                            Dim Reference As String = recordset("Reference")
                            Dim Comments As String = recordset("Comments")
                            Dim Amount As Integer = recordset("Amount")
                            If Amount = 0 Then
                                Return False
                            End If
                        End While
                        If flag = True Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
            End Select

        End Function


        Private Sub ImportMSCSV_Database(ByVal TransFileWithPath As String)

            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            Try

                'open imported file as dataset
                Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
                Dim dsFile As DataSet = OpenFileConnection(TransFile)

                Dim RowsImported As Integer = 0

                For Each TranRow As DataRow In dsFile.Tables(0).Rows
                    If TranRow(0).ToString.Trim <> "" And TranRow(0).ToString.Trim.Contains("Transaction Date") = False Then
                        Dim TransDate As String = TranRow(10).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim TransAmount As String = TranRow(13).ToString.Replace("""", "").Replace(",", "").Replace("$", "").Trim
                        Dim TransCardNum As String = TranRow(12).ToString.Replace("""", "").Replace(",", "").Replace("$", "").Trim
                        Dim TransID As String = TranRow(5).ToString.Replace("""", "").Replace(",", "").Replace("$", "").Trim
                        Dim PP_Date As String = TranRow(2).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim CardType As String = TranRow(11).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim AuthCode As String = TranRow(18).ToString.Replace("""", "").Replace(",", "").Trim

                        'create insert statement
                        Dim SqlInsert As String = "INSERT INTO MSTemp(TransactionDate, Amount, CardHolderNumber,TerminalID,PP_Date,CardType,AuthCode) VALUES("
                        SqlInsert = SqlInsert & "'" & TransDate & "','" & TransAmount & "','" & TransCardNum & "','" & TransID & "','" & PP_Date & "','" & CardType & "','" & AuthCode & "')"

                        ' Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    End If
                Next
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update MStemp set Tdate = convert(datetime,TransactionDate,101) where Tdate is NUll")

                'display number of rows imported
                Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & RowsImported
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
            End Try
        End Sub

        Private Sub ImportPPCSV_Database(ByVal TransFileWithPath As String)

            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            Try
                'open imported file as dataset
                Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
                Dim dsFile As DataSet = OpenFileConnection(TransFile)

                Dim RowsImported As Integer = 0
                'If Not dsFile.Tables(0).Rows(0)(22) Is DBNull.Value Then
                For Each TranRow As DataRow In dsFile.Tables(0).Rows
                    If TranRow(0).ToString.Trim <> "" Then 'And TranRow(22).ToString.Trim.Contains("OrderNo") = False Then 'TranRow(0).ToString.Trim.Contains("Store ID") = False Then
                        'Dim StoreID As String = TranRow(0).ToString
                        '& "-" & TranRow(13).ToString.Trim 'TranRow(1).ToString.Trim
                        Dim PDate As String = TranRow(7).ToString.Trim '2
                        Dim UserID As String = TranRow(19).ToString '3
                        Dim CardType As String = TranRow(4).ToString
                        Dim Type As String = TranRow(17).ToString
                        'Dim PayerAuth As String = TranRow(6).ToString '5
                        'Dim InvoiceNO As String = TranRow(6).ToString
                        'Dim PONO As String = TranRow(7).ToString
                        'Dim TransID As String = TranRow(8).ToString
                        Dim CardRouteNo As String = TranRow(16).ToString '8
                        Dim ExpDate As String = TranRow(10).ToString '9
                        Dim Approval As String = TranRow(5).ToString.Trim '10
                        Dim Amount As String = TranRow(14).ToString '11
                        Dim Tag As String = TranRow(13).ToString
                        Dim OrderNo As String = "" 'TranRow(22).ToString.Trim
                        Dim Name As String = TranRow(0).ToString
                        Dim AuthCode As String = TranRow(6).ToString.Trim
                        If AuthCode.Length() > 0 And AuthCode.Length < 6 Then
                            For i As Integer = AuthCode.Length To 5
                                AuthCode = "0" + AuthCode
                            Next
                        End If

                        Dim PayerAuth As String
                        Dim OrderNo_Sub As String
                        'If OrderNo.Length() > 0 Then
                        '    PayerAuth = OrderNo.Substring(0, OrderNo.IndexOf("-")) 'TranRow(6).ToString
                        '    OrderNo_Sub = OrderNo.Substring(OrderNo.IndexOf("-"), OrderNo.Length() - OrderNo.IndexOf("-"))
                        '    If PayerAuth.Length() > 0 And PayerAuth.Length < 6 Then
                        '        For i As Integer = PayerAuth.Length To 5
                        '            PayerAuth = "0" + PayerAuth
                        '        Next
                        '    End If
                        '    OrderNo = PayerAuth + OrderNo_Sub 'OrderNo.Substring(OrderNo.IndexOf("-"), OrderNo.Length)
                        '    PayerAuth = "'" & PayerAuth & "'"
                        '    OrderNo = "'" & OrderNo & "'"
                        'Else
                        OrderNo = "NULL"
                        PayerAuth = "NULL"
                        'End If

                        Dim Month_Name As String = MonthName(ExpDate.Substring(0, ExpDate.Length - 2))
                        Dim Expirydate As String = ExpDate.Substring(ExpDate.Length - 2) & "-" & Month_Name.Substring(0, 3) 'strDate.ToString("MMM")
                        ''create insert statement

                        'Dim SqlInsert As String = "INSERT INTO PPTemp(StoreID, OrderNo, PDate, UserID, Type, PayerAuth, InvoiceNo, PONo,  CardRouteNo, ExpDate, Approval, Amount) VALUES(" 'TransID,
                        'SqlInsert = SqlInsert & "'" & StoreID & "','" & OrderNo & "','" & PDate & "','" & UserID & "','" & Type & "','" & PayerAuth & "','" & InvoiceNO & "','" & PONO & "','" & CardRouteNo & "','" & ExpDate & "','" & Approval & "','" & Amount & "')" ','" & TransID & "'
                        Dim SqlInsert As String = "INSERT INTO PPTemp(OrderNo, PDate, UserID, CardType,Type, PayerAuth,   CardRouteNo, ExpDate, Approval, Amount,Tag,Name,AuthCode) VALUES(" 'TransID,
                        SqlInsert = SqlInsert & "'" & AuthCode & "-" & Tag & "','" & PDate & "','" & UserID & "','" & CardType & "','" & Type & "'," & PayerAuth & ",'" & CardRouteNo & "','" & Expirydate & "','" & Approval & "','" & Amount & "','" & Tag & "','" & Name & "','" & AuthCode & "')" ','" & TransID & "'

                        'Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    End If
                Next
                ' Else
                'Label1.Text = "Please add OrderNo to the file and Proceed.<br /> (Note *: OrderNo = Concatenate([Auth No],'-',[Tag]) at the end of Columns)"
                'Exit Sub
                'End If

                'update PPtemp set Tdate=convert(datetime,PDate,101) where Tdate is NUll
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update PPtemp set Tdate=convert(datetime,PDate,101) where Tdate is NUll")
                'display number of rows imported
                Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & RowsImported

            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                'Response.Write(ex.ToString())
            End Try
        End Sub

        Private Sub ImportEMCSV_Database(ByVal TransFileWithPath As String)
            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            Try
                'open imported file as dataset
                Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
                Dim dsFile As DataSet = OpenFileConnection(TransFile)
                Dim RowsImported As Integer = 0
                For Each TranRow As DataRow In dsFile.Tables(0).Rows
                    If TranRow(0).ToString.Trim <> "" And TranRow(0).ToString.Trim.Contains("Subject") = False Then
                        Dim Subject As String = TranRow(0).ToString.Trim
                        Dim Body As String = TranRow(1).ToString.Replace("""", "").Replace("'", "").Trim
                        Dim FromName As String = TranRow(2).ToString.Trim
                        Dim FromAddress As String = TranRow(3).ToString.Trim
                        Dim FromType As String = TranRow(4).ToString.Trim
                        Dim ToName As String = TranRow(5).ToString.Trim
                        Dim ToAddress As String = TranRow(6).ToString.Trim
                        Dim ToType As String = TranRow(7).ToString.Trim

                        'create insert statement
                        Dim SqlInsert As String = "INSERT INTO EMTemp(Subject, Body, FromName, FromAddress, FromType, ToName, ToAddress, ToType) VALUES("
                        SqlInsert = SqlInsert & "'" & Subject & "','" & Body & "','" & FromName & "','" & FromAddress & "','" & FromType & "','" & ToName & "','" & ToAddress & "','" & ToType & "')"
                        'Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    End If
                Next
                'display number of rows imported
                Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & RowsImported
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                'Response.Write("<br> ERROR in ImportCSV_Database: " & ex.Message.ToString())
            End Try
        End Sub

        Private Function OpenFileConnection(ByVal TransFileTable As FileInfo) As DataSet
            Dim dsFile As New DataSet
            Try
                Dim ConnCSV As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=Yes;';Data Source=" & TransFileTable.DirectoryName.ToString()
                Dim sqlSelect As String
                Dim objOleDBConn As OleDbConnection
                Dim objOleDBDa As OleDbDataAdapter
                objOleDBConn = New OleDbConnection(ConnCSV)
                objOleDBConn.Open()

                'sqlSelect = "select [Store ID],[Order #],[Date],[User ID],Type,[PayerAuth],[Invoice #],[PO #],[Trans ID],[Card/Route Number],[Exp Date],Approval,Amount from [" + TransFileTable.Name.ToString() + "]"
                'sqlSelect = "Select [Card Type],[Status],[Auth No] ,[Time],[Expiry]	,[Tag],[Amount],[Card Number],[User ID] from [" + TransFileTable.Name.ToString() + "]"

                sqlSelect = "select * from [" + TransFileTable.Name.ToString() + "]"
                objOleDBDa = New OleDbDataAdapter(sqlSelect, objOleDBConn)
                objOleDBDa.Fill(dsFile)
                objOleDBConn.Close()

            Catch ex As Exception
                Label1.Text = Label1.Text & "<br> ERROR in OpenFileConnection: " & ex.Message.ToString()
            End Try
            Return dsFile
        End Function

        'Private Function OpenFileConnectionTest(ByVal TransFileTable As FileInfo) As Boolean
        '    Dim dsFile As New DataSet
        '    Try
        '        Dim ConnCSV As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=Yes;FMT=Delimited';Data Source=" & TransFileTable.DirectoryName.ToString()
        '        Dim sqlSelect As String
        '        Dim objOleDBConn As OleDbConnection
        '        Dim objOleDBDa As OleDbDataAdapter
        '        objOleDBConn = New OleDbConnection(ConnCSV)
        '        objOleDBConn.Open()
        '        sqlSelect = "select [Transaction Date],[Amount],[CardHolder Number],[Terminal ID] from [" + TransFileTable.Name.ToString() + "]"
        '        'sqlSelect = "select * from [" + TransFileTable.Name.ToString() + "]"
        '        objOleDBDa = New OleDbDataAdapter(sqlSelect, objOleDBConn)
        '        objOleDBDa.Fill(dsFile)
        '        objOleDBConn.Close()
        '        Return True
        '    Catch ex As Exception
        '        Label1.Text = Label1.Text & "<br> ERROR in OpenFileConnection: " & ex.Message.ToString()
        '        Return False
        '    End Try

        'End Function

        Sub postdata()
            Dim datediff As Integer = -1
            Dim ChargeCount, TempCount As Integer
            'Moving data from temp table to Original Table
            Select Case ddlPostData.SelectedValue
                Case 1
                    ' Coding for PP
                    TempCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from pptemp")
                    ChargeCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ppCharge")
                    If TempCount > 0 And ChargeCount > 0 Then

                        Dim FirstPPTempDate, LastPPChargeDate As Date
                        'Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select max(PP.PP_Date) AS LastPPChargeDate,MIn(P.TDate) AS FirstPPTempDate from PPCharge PP, PPTemp p")
                        'While Readr.Read
                        FirstPPTempDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Min(TDate) AS FirstPPTempDate from PPTemp")
                        LastPPChargeDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select max(PP_Date) AS LastPPChargeDate from PPCharge")
                        'End While
                        'Readr.Close()
                        datediff = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select datediff(d,(select max(PP_Date) from PPCharge),(select MIN(TDate) from PPTemp))")
                        If datediff > 1 Then
                            Literal1.Text = "The last date in PPCharge table is " & LastPPChargeDate & ".  The first date in the CSV file is " & FirstPPTempDate & ". So there is a gap in dates. The gap in dates may imply a missing CSV file.  Is this gap ok?"
                            TrMsg.Visible = True
                        ElseIf datediff <= 0 Then
                            Literal1.Text = "The last date in PPCharge table is " & LastPPChargeDate & ".  The first date in the CSV file is " & FirstPPTempDate & ". So there is an overlap in dates. The overlapped dates in the CSV file will be ignored.  Do you approve?"
                            TrMsg.Visible = True
                        Else
                            'continue process
                            PPPostData()
                        End If
                    ElseIf TempCount > 0 Then
                        PPPostData()
                    Else
                        Label1.Text = "Sorry! No data In temp Table to post"
                    End If
                Case 2
                    ' Coding for MS
                    TempCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MStemp")
                    ChargeCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSCharge")
                    If TempCount > 0 And ChargeCount > 0 Then
                        Dim FirstMSTempDate, LastMSChargeDate As Date
                        'Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select max(MS.MS_TransDate) AS LastMSChargeDate,MIN(M.TDate) AS FirstMSTempDate from MSCharge MS, MSTemp M")
                        'While Readr.Read
                        FirstMSTempDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MIN(TDate) AS FirstMSTempDate from MSTemp")
                        LastMSChargeDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select max(MS_TransDate) AS LastMSChargeDate from MSCharge")
                        'End While
                        'Readr.Close()
                        datediff = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Datediff(d,(Select max(MS_TransDate) from MSCharge),(Select MIN(TDate) from MSTemp))")
                        If datediff > 1 Then
                            Literal1.Text = "The last date in MSCharge table is " & LastMSChargeDate & ".  The first date in the CSV file is " & FirstMSTempDate & ". So there is a gap in dates. The gap in dates may imply a missing CSV file.  Is this gap ok?"
                            TrMsg.Visible = True
                        ElseIf datediff <= 0 Then
                            Literal1.Text = "The last date in MSCharge table is " & LastMSChargeDate & ".  The first date in the CSV file is " & FirstMSTempDate & ". So there is an overlap in dates. The overlapped dates in the CSV file will be ignored.  Do you approve?"
                            TrMsg.Visible = True
                        Else
                            'continue process
                            MSPostData()
                        End If
                    ElseIf TempCount > 0 Then
                        MSPostData()
                    Else
                        Label1.Text = "Sorry! No data In temp Table to post"
                    End If

                Case 3
                    ' Coding for EM
                    'EMPostData()
                Case Else
                    Label1.Text = "Please select The type to be posted"
            End Select
        End Sub
        Sub PPPostData()
            '** updated Count Query 
            Dim TempRecRead, BeginRecID, EndRecID, TempRecRej As Integer
            Dim count As Integer = 0
            Dim Records As Integer = 0
            Dim wherecontn As String = " "
            TempRecRead = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from PPTemp")
            Try
                BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(PPChargeID) from PPCharge Where PPChargeid is not null") + 1
            Catch ex As Exception
                BeginRecID = 0
            End Try
            Label1.Text = "Number of Records Read :" & TempRecRead
            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from PPCharge")
            If count > 0 Then
                wherecontn = " AND pdate > (Select max(PP_Date) from PPCharge) "
            End If
            Records = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(OrderNo) FROM PPTemp where Approval='Approved' and AuthCode is not null " & wherecontn & "")   ';like 'Y%' " 
            Label1.Text = Label1.Text & "<br> Number of Records Posted Successfully: " & Records
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into  PPCharge (PP_OrderNumber, PP_Date,PP_Type, PP_CN1, PP_CN2,  PP_Approval,PP_Amount, CreateDate) SELECT   OrderNo,convert(datetime,pdate, 101),Type, REPLICATE('0', (4 - LEN(SUBSTRING(CardRouteNo, 1, 4)))) + SUBSTRING(CardRouteNo, 1, 4), SUBSTRING(CardRouteNo, 6, 4)+ REPLICATE('0', (4 - LEN(SUBSTRING(CardRouteNo, 6, 4)))) , Approval, Amount,GetDate() FROM PPTemp where Approval like 'Y%' " & wherecontn & " Group By  OrderNo,Pdate,Type,CardRouteNo,Approval,Amount; Delete PPtemp;")
            '**********Commented and updated on 19-06-2014 to update PPCharge from PPTemp and CCsubmitLog tables *****************'
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into  PPCharge (PP_OrderNumber, PP_Date,PP_Type, PP_CN1, PP_CN2,  PP_Approval,PP_Amount, CreateDate,CreatedBy)  SELECT   OrderNo,Tdate,Type, REPLICATE('0', (4 - LEN(SUBSTRING(CardRouteNo, 1, 4)))) + SUBSTRING(CardRouteNo, 1, 4), SUBSTRING(CardRouteNo, 6, 4)+ REPLICATE('0', (4 - LEN(SUBSTRING(CardRouteNo, 6, 4)))) , Approval, case when Type like 'R%' then '-' + Amount else Amount END ,GetDate()," & Session("LoginID") & "  FROM PPTemp where Approval like 'Y%' " & wherecontn & " Group By  OrderNo,Tdate,Type,CardRouteNo,Approval,Amount order by Tdate; Truncate Table PPtemp;")
            Dim StrPPCharge_Insert As String = " Insert into PPCharge (PP_OrderNumber, PP_Date,PP_Type, PP_CN1, PP_CN2, MemberID, PP_Approval,PP_Amount,Name,CardType,AuthCode,Tag, CreateDate,CreatedBy)    SELECT   P.OrderNo,P.Tdate,P.CardType, C.CCF4 ,C.CCL4, C.MemberID,P.Approval, case when P.Type like '%Refund%' then '-' + P.Amount else P.Amount END , P.Name,P.CardType,P.AuthCode,P.Tag ,GetDate(),4240  FROM PPTemp P "
            StrPPCharge_Insert = StrPPCharge_Insert & " Left join CCSubmitLog C on CAST(C.PaymentDate as date) = CAST(P.PDate as date) and P.OrderNo =case when P.Type like '%Refund%' then C.RefundReference else C.PaymentReference END where P.Approval ='Approved' and P.AuthCode is not null AND P.pdate > (Select max(PP_Date) from PPCharge) Group By  OrderNo,Tdate,Type,CardType,CardRouteNo,Approval,P.Amount,P.Name,P.CardType,P.AuthCode,P.Tag,C.CCF4 ,C.CCL4,C.MemberID order by Tdate; Truncate Table PPtemp; " '  and Cast(P.TDate as DATE)=Cast(C.PaymentDate as DATE)  ' on OrderNo = C.PaymentReference 
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrPPCharge_Insert)

            'Response.Write(StrPPCharge_Insert)

            'Posting log file
            Try
                EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(PPChargeID) from PPCharge Where PPChargeid is not null")
            Catch ex As Exception
                EndRecID = 0
            End Try
            Dim nodataflag As Boolean = False
            If BeginRecID = 0 And EndRecID > 0 Then
                BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(PPChargeID) from PPCharge Where PPChargeid is not null")
                nodataflag = True
            End If
            TempRecRej = TempRecRead - (EndRecID - (BeginRecID - 1))
            If EndRecID < BeginRecID And nodataflag = False Then
                TempRecRej = TempRecRead
                BeginRecID = 0
                EndRecID = 0
            ElseIf EndRecID = BeginRecID And nodataflag = True Then
                TempRecRej = TempRecRead
                BeginRecID = 0
                EndRecID = 0
            ElseIf EndRecID = 0 Or BeginRecID = 0 Then
                TempRecRej = TempRecRead
            End If
            Dim BeginDate, EndDate As String
            If BeginRecID > 0 Then
                BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(PP_Date) from PPCharge where PP_date is Not Null and PPChargeID >=" & BeginRecID & "") & "'"
            Else
                BeginDate = "NULL"
            End If
            If EndRecID > 0 Then
                EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(PP_Date) from PPcharge where PP_date is Not Null") & "'"
            Else
                EndDate = "Null"
            End If
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(InputFileName,TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values('" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select top 1 filename from  fileupload where FileType='PP' And filename is not null order by fileid desc") & "'," & TempRecRead & "," & BeginRecID & "," & EndRecID & "," & TempRecRej & "," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
            ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
        End Sub
        Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
            If RbtnPostData.Checked = True Then
                Select Case ddlPostData.SelectedValue
                    Case 1
                        PPPostData()
                    Case 2
                        MSPostData()
                End Select
            End If
            TrMsg.Visible = False
        End Sub
        Sub MSPostData()
            Try
                Dim count As Integer = 0
                Dim Records As Integer = 0
                Dim wherecontn As String = " "
                Dim TempRecRead, BeginRecID, EndRecID, TempRecRej As Integer
                TempRecRead = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSTemp")
                Try
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(MSChargeID) from MSCharge Where MSChargeid is not null") + 1
                Catch ex As Exception
                    BeginRecID = 0
                End Try
                Label1.Text = "Number of Records Read :" & TempRecRead
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSCharge")
                If count > 0 Then
                    wherecontn = " Where TransactionDate >(Select max(MS_TransDate) from MSCharge) "
                End If
                'This is to find duplicate but ommits some needed data also so No need Group BY
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into  MSCharge (MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, CreateDate) SELECT  TransactionDate, case when SUBSTRING(Amount, 1, 1) = '(' then SUBSTRING(Amount, 2, 5) else SUBSTRING(Amount, 1, 5) END , SUBSTRING(CardHolderNumber, 1, 4), SUBSTRING(CardHolderNumber, 5, 2), SUBSTRING(CardHolderNumber, 13, 4), Getdate() FROM MSTemp " & wherecontn & " Group By TransactionDate,CardHolderNumber,Amount; Delete MStemp")
                'With Out Refund Values as Minus
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into  MSCharge (MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, CreateDate) SELECT  TransactionDate, case when SUBSTRING(Amount, 1, 1) = '(' then  SUBSTRING(Amount, 2, 5) else SUBSTRING(Amount, 1, 5) END , SUBSTRING(CardHolderNumber, 1, 4), SUBSTRING(CardHolderNumber, 5, 2), SUBSTRING(CardHolderNumber, 13, 4), Getdate() FROM MSTemp " & wherecontn & " ; Delete MStemp")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into  MSCharge (MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3,PP_Date, CardType, AuthCode, CreateDate,CreatedBy) SELECT  TDate, case when SUBSTRING(Amount, 1, 1) = '(' then '-' + SUBSTRING(Amount, 2, 5) else SUBSTRING(Amount, 1, 5) END , SUBSTRING(CardHolderNumber, 1, 4), SUBSTRING(CardHolderNumber, 5, 2), SUBSTRING(CardHolderNumber, 13, 4),PP_Date, CardType, AuthCode, Getdate()," & Session("LoginID") & " FROM MSTemp " & wherecontn & "  order by TDate; Truncate Table MStemp")
                ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
                Records = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSCharge") - count
                Label1.Text = Label1.Text & " <br> Number of Records Posted Successfully: " & Records
                'Posting log file
                Try
                    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(MSChargeID) from MSCharge Where MSChargeid is not null")
                Catch ex As Exception
                    EndRecID = 0
                End Try
                Dim nodataflag As Boolean = False
                If BeginRecID = 0 And EndRecID > 0 Then
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(MSChargeID) from MSCharge Where MSChargeid is not null")
                    nodataflag = True
                End If
                TempRecRej = TempRecRead - (EndRecID - (BeginRecID - 1))
                If EndRecID < BeginRecID And nodataflag = False Then
                    TempRecRej = TempRecRead
                    BeginRecID = 0
                    EndRecID = 0
                ElseIf EndRecID = BeginRecID And nodataflag = True Then
                    TempRecRej = TempRecRead
                    BeginRecID = 0
                    EndRecID = 0
                ElseIf EndRecID = 0 Or BeginRecID = 0 Then
                    TempRecRej = TempRecRead
                End If
                Dim BeginDate, EndDate As String
                If BeginRecID > 0 Then
                    'select MS_Transdate from MScharge where MSChargeID=1 
                    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(MS_transdate) from MSCharge where MS_Transdate is Not Null and MSChargeID>=" & BeginRecID & "") & "'"
                Else
                    BeginDate = "NULL"
                End If
                If EndRecID > 0 Then
                    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(MS_Transdate) from MScharge where MS_Transdate is Not Null") & "'"
                Else
                    EndDate = "Null"
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(InputFileName,TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values('" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select top 1 filename from  fileupload where FileType='MS' And filename is not null order by fileid desc") & "'," & TempRecRead & "," & BeginRecID & "," & EndRecID & "," & TempRecRej & "," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
            Catch ex As Exception
                Label1.Text = ex.ToString()
            End Try
        End Sub
        'Sub EMPostData()
        '    Dim count As Integer = 0
        '    Dim Records As Integer = 0
        '    Dim TempRecRead, BeginRecID, EndRecID, TempRecRej As Integer
        '    TempRecRead = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from EMTemp2")
        '    Try
        '        BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(EMChargeID) from EMCharge Where EMChargeid is not null") + 1
        '    Catch ex As Exception
        '        BeginRecID = 0
        '    End Try
        '    Label1.Text = "Number of Valid Records Read :" & TempRecRead
        '    Label1.Text = Label1.Text + " <br> Number of Invalid Records Unread in EMTemp :" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from EMTemp")
        '    count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(EMChargeID) from EMCharge")

        '    'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_ConvertEMTemp", New SqlParameter("@Createdby", Session("LoginID")))
        '    Try

        '        Dim conn As New SqlConnection
        '        conn.ConnectionString = Application("ConnectionString")
        '        Dim cmd As SqlCommand = New SqlCommand("usp_ConvertEMTemp", conn)
        '        conn.Open()
        '        cmd.CommandTimeout = 1200
        '        cmd.CommandType = CommandType.StoredProcedure
        '        cmd.Parameters.AddWithValue("@Createdby", Session("LoginID"))
        '        cmd.ExecuteNonQuery()
        '        conn.Close()
        '        ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
        '        Records = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(EMChargeID) from EMCharge") - count
        '        Label1.Text = Label1.Text + "<br>Number of  Records Posted Successfully: " & Records
        '    Catch ex As Exception
        '        Label1.Text = "Error in Posting Data " & ex.ToString()
        '        'MsgBox(ex.ToString())
        '    End Try
        '    'Posting log file
        '    Try
        '        EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(EMChargeID) from EMCharge Where EMChargeid is not null")
        '    Catch ex As Exception
        '        EndRecID = 0
        '    End Try
        '    Dim nodataflag As Boolean = False
        '    If BeginRecID = 0 And EndRecID > 0 Then
        '        BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(EMChargeID) from EMCharge Where EMChargeid is not null")
        '        nodataflag = True
        '    End If
        '    TempRecRej = TempRecRead - (EndRecID - (BeginRecID - 1))
        '    If EndRecID < BeginRecID And nodataflag = False Then
        '        TempRecRej = TempRecRead
        '        BeginRecID = 0
        '        EndRecID = 0
        '    ElseIf EndRecID = BeginRecID And nodataflag = True Then
        '        TempRecRej = TempRecRead
        '        BeginRecID = 0
        '        EndRecID = 0
        '    ElseIf EndRecID = 0 Or BeginRecID = 0 Then
        '        TempRecRej = TempRecRead
        '    End If
        '    Dim BeginDate, EndDate As String
        '    If BeginRecID > 0 Then
        '        BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(Date) from EMcharge where date is Not Null AND EMChargeID>=" & BeginRecID & "") & "'"
        '    Else
        '        BeginDate = "NULL"
        '    End If
        '    If EndRecID > 0 Then
        '        EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(Date) from EMcharge where date is Not Null") & "'"
        '    Else
        '        EndDate = "Null"
        '    End If
        '    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(InputFileName,TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values('" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select top 1 filename from  fileupload where FileType='EM' And filename is not null order by fileid desc") & "'," & TempRecRead & "," & BeginRecID & "," & EndRecID & "," & TempRecRej & "," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
        '    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update a SET a.Brand=b.Brand,a.Reference=B.Reference,a.Comments=b.Comments,a.MemberID=b.MemberID from EMCharge a,EMCharge b where a.Comments is NULL and a.Type='R' and a.OrderNumber=b.OrderNumber and b.Type='S'")
        'End Sub

        'Private Sub GetVwMsdNSF()
        '    Dim str As String
        '    str = " is null"
        '    If ddlVwMsdNSF.SelectedValue <= 1 Then
        '        str = "  is  not null "
        '    ElseIf ddlVwMsdNSF.SelectedValue = 2 Then
        '        If ddlUnRecVwMsdNSF.SelectedValue = "Brand" Then
        '            str = str & " and Brand is null "
        '        ElseIf ddlUnRecVwMsdNSF.SelectedValue = "Reference" Then
        '            str = str & " and Reference is null  "
        '        ElseIf ddlUnRecVwMsdNSF.SelectedValue = "Comments" Then
        '            str = str & " and Comments is null  "
        '        ElseIf ddlUnRecVwMsdNSF.SelectedValue = "MemberID" Then
        '            str = str & " and MemberID is null  "
        '        ElseIf ddlUnRecVwMsdNSF.SelectedValue = "NFG_PaymentDate" Then
        '            str = str & " and Date is null "
        '        End If
        '    End If
        '    Dim dsRecords As New DataSet
        '    dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select EMChargeid, Type, OrderNumber, Date, Brand, Approved, Name, Comments, Amount, MemberID, StatusFlag, Address1, City, State, Zip, Reference  from EMCharge where NFG_UniqueID " & str & " order By date desc")
        '    Dim dt As DataTable = dsRecords.Tables(0)
        '    GrdVwMsdNSF.DataSource = dt
        '    GrdVwMsdNSF.DataBind()
        '    If (dt.Rows.Count = 0) Then
        '        Label1.Text = "No Payments missed by NSF"
        '        PnlVwMsdNSF.Visible = False
        '    Else
        '        Label1.Text = ""
        '        PnlVwMsdNSF.Visible = True
        '    End If
        'End Sub
        Private Sub GetVwChargeRec()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from ChargeRec  order By  ChargeRecID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdChargeRec.DataSource = dt
            GrdChargeRec.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = "No Data"
                pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                pnlChargeRec.Visible = True
            End If
        End Sub
        Private Sub GetVwPP()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from PPCharge where MS_Match<>'Y' or MS_Match is Null  order By  PPChargeID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            grdVwPP.DataSource = dt
            grdVwPP.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = "No PP Data"
                'pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                PnlVwPPMS.Visible = True
            End If
        End Sub
        Protected Sub grdVwPP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVwPP.PageIndexChanging
            GetVwPP()
            grdVwPP.PageIndex = e.NewPageIndex
            grdVwPP.DataBind()
        End Sub
        Protected Sub GrdVwNFGData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwNFGData.PageIndexChanging
            If RbtnViewNFGData.Checked = True Then
                VwNFGData(txtFromDate.Text, txtToDate.Text)
            Else
                VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
            End If
            GrdVwNFGData.PageIndex = e.NewPageIndex
            GrdVwNFGData.DataBind()

        End Sub

        Private Sub GetVwMS()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from MSCharge where PP_Match<>'Y' or PP_Match is Null  order By  MSChargeID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            grdVwMS.DataSource = dt
            grdVwMS.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No MS Data"
                'pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                PnlVwPPMS.Visible = True
            End If
        End Sub

        'Private Sub VwSDEM()
        '    Dim dsRecords As New DataSet
        '    Dim str As String = "is Null"
        '    If DdlVwSDEM.SelectedValue = 1 Then
        '        str = "is Not Null"
        '    ElseIf DdlVwSDEM.SelectedValue = 2 Then
        '        If ddlUnRecSDEM.SelectedValue = "Brand" Then
        '            str = str & " and Brand is null "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "Reference" Then
        '            str = str & " and Reference is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "Comments" Then
        '            str = str & " and Comments is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "MemberID" Then
        '            str = str & " and MemberID is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "NFG_UniqueID" Then
        '            str = str & " and NFG_UniqueID is null   "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "NFG_PaymentDate" Then
        '            str = str & " and NFG_PaymentDate is null "
        '        End If
        '    End If

        '    dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select ChargeRecID, EMChargeid, PP_OrderNumber, PP_Date, PP_Amount, MS_TransDate, Brand, Comments, MemberID,  PP_CN1, PP_CN2, Reference  from ChargeRec where EMChargeid " & str & "  order by ChargeRecID Desc")
        '    Dim dt As DataTable = dsRecords.Tables(0)
        '    GrdVwSDEM.DataSource = dt
        '    GrdVwSDEM.DataBind()
        '    If (dt.Rows.Count = 0) Then
        '        Label1.Text = Label1.Text & "No SDEM Data"
        '        pnlVwSDEM.Visible = False
        '    Else
        '        Label1.Text = ""
        '        pnlVwSDEM.Visible = True
        '    End If
        'End Sub
        Private Sub VwSDNSF()
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"
            If DdlVwSDNSF.SelectedValue = 1 Then
                str = "is Not Null"
                'ElseIf DdlVwSDEM.SelectedValue = 2 Then
                '    If ddlUnRecSDNSF.SelectedValue = "Brand" Then
                '        str = str & " and Brand is null "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "Reference" Then
                '        str = str & " and Reference is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "Comments" Then
                '        str = str & " and Comments is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "MemberID" Then
                '        str = str & " and MemberID is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "NFG_UniqueID" Then
                '        str = str & " and NFG_UniqueID is null   "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "NFG_PaymentDate" Then
                '        str = str & " and NFG_PaymentDate is null "
                '    End If
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select ChargeRecID, PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, NFG_UniqueID, NFG_PaymentDate, CreateDate, CreatedBy, ModifiedDate, ModifiedBy  from ChargeRec  where NFG_UniqueID " & str & " order by ChargeRecID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwSDNSF.DataSource = dt
            GrdVwSDNSF.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No ChargeRec Data/NSF Data"
                pnlVwSDNSF.Visible = False
            Else
                Label1.Text = ""
                pnlVwSDNSF.Visible = True
            End If
        End Sub
        Private Sub VwNFGData(ByVal Frmdate As DateTime, ByVal Todate As DateTime)
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"

            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select * From NFG_Transactions where Brand is null and MS_Transdate is null and [Payment Date] between '" & Frmdate & "' and '" & Todate & "' order by unique_id desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwNFGData.DataSource = dt
            GrdVwNFGData.DataBind()
            Session("ds_NFGData") = dsRecords
            lblVwNFGData.Text = "View missing Brand and MS_Transdate in NFG_Trans table"
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No NFG Transactions Data"
                pnlVwNFGData.Visible = False
            Else
                Label1.Text = ""
                pnlVwNFGData.Visible = True
            End If
        End Sub

        Private Sub VwNFGMatchedData(ByVal dateFrm As DateTime, ByVal dateTo As DateTime)
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"

            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select * From NFG_Transactions where MatchedStatus " & IIf(ddlMatchedStatus.SelectedIndex = 0, " is null", " ='" & ddlMatchedStatus.SelectedValue & "'") & " and [Payment Date] between '" & dateFrm & "' and '" & dateTo & "' order by unique_id desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwNFGData.DataSource = dt
            GrdVwNFGData.DataBind()
            Session("ds_NFGData") = dsRecords

            lblVwNFGData.Text = "NFG_Trans with records with selected Matched Status flag"
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No NFG Transactions Data"
                pnlVwNFGData.Visible = False
            Else
                Label1.Text = ""
                pnlVwNFGData.Visible = True
            End If
        End Sub

        Protected Sub GrdVwSDNSF_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwSDNSF.PageIndexChanging
            VwSDNSF()
            GrdVwSDNSF.PageIndex = e.NewPageIndex
            GrdVwSDNSF.DataBind()
        End Sub
        Protected Sub GrdVwSDEM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwSDEM.PageIndexChanging
            'VwSDEM()
            GrdVwSDEM.PageIndex = e.NewPageIndex
            GrdVwSDEM.DataBind()
        End Sub
        Protected Sub grdVwMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVwMS.PageIndexChanging
            GetVwMS()
            grdVwMS.PageIndex = e.NewPageIndex
            grdVwMS.DataBind()
        End Sub

        Protected Sub GrdChargeRec_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdChargeRec.PageIndexChanging
            GetVwChargeRec()
            GrdChargeRec.PageIndex = e.NewPageIndex
            GrdChargeRec.DataBind()
        End Sub

        'Protected Sub GrdVwMsdNSF_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwMsdNSF.PageIndexChanging
        '    GetVwMsdNSF()
        '    GrdVwMsdNSF.PageIndex = e.NewPageIndex
        '    GrdVwMsdNSF.DataBind()
        'End Sub
        Sub closepanel()
            pnlVwSDNSF.Visible = False
            PnlVwPPMS.Visible = False
            pnlChargeRec.Visible = False
            'PnlVwMsdNSF.Visible = False
            pnlVwSDEM.Visible = False
        End Sub
        Sub clearddl()
            ' ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
            ddlCSVUpload.SelectedIndex = ddlCSVUpload.Items.IndexOf(ddlCSVUpload.Items.FindByValue(0))
            If Not RbtnVwSDNSF.Checked Then DdlVwSDNSF.SelectedIndex = DdlVwSDNSF.Items.IndexOf(DdlVwSDNSF.Items.FindByValue(0))
            If Not RbtnVwPPMS.Checked Then DdlVwPPMS.SelectedIndex = DdlVwPPMS.Items.IndexOf(DdlVwPPMS.Items.FindByValue(0))
            'If Not RbtnVwSDEM.Checked Then DdlVwSDEM.SelectedIndex = DdlVwSDEM.Items.IndexOf(DdlVwSDEM.Items.FindByValue(0))
        End Sub
        Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
            TrMsg.Visible = False
            ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
        End Sub
        Sub ExecuteQuery(ByVal StrSQL As String)
            Dim conn As New SqlConnection
            conn.ConnectionString = Application("ConnectionString")
            Dim cmd As SqlCommand = New SqlCommand(StrSQL, conn)
            conn.Open()
            cmd.CommandTimeout = 600
            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            conn.Close()
        End Sub

        'Protected Sub DdlVwSDEM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlVwSDEM.SelectedIndexChanged
        '    If DdlVwSDEM.SelectedValue <= 1 Then
        '        ddlUnRecSDEM.SelectedIndex = 0
        '        ddlUnRecSDEM.Enabled = False
        '    Else
        '        ddlUnRecSDEM.Enabled = True
        '    End If
        'End Sub

        Protected Sub DdlVwSDNSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlVwSDNSF.SelectedIndexChanged
            If DdlVwSDNSF.SelectedValue <= 1 Then
                ddlUnRecSDNSF.SelectedIndex = 0
                ddlUnRecSDNSF.Enabled = False
            Else
                ddlUnRecSDNSF.Enabled = True
            End If
        End Sub

        'Protected Sub ddlVwMsdNSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVwMsdNSF.SelectedIndexChanged
        '    If ddlVwMsdNSF.SelectedValue <= 1 Then
        '        ddlUnRecVwMsdNSF.SelectedIndex = 0
        '        ddlUnRecVwMsdNSF.Enabled = False
        '    Else
        '        ddlUnRecVwMsdNSF.Enabled = True
        '    End If
        'End Sub

        Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        End Sub
        Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try

          
                Dim TempGrid As New GridView
                Dim Filename As String = lblVwNFGData.Text.Replace(" ", "_")
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=" & Filename & ".xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                'If RbtnViewNFGData.Checked = True Then
                '    VwNFGData(txtFromDate.Text, txtToDate.Text)
                'Else
                '    VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
                'End If
                ' If Session("ds_NFGData") <> "" Then
                Dim ds As DataSet = Session("ds_NFGData")
                TempGrid.DataSource = ds
                TempGrid.DataBind()
                'End If
                TempGrid.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End Sub

    End Class
End Namespace