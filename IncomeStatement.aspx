<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="IncomeStatement.aspx.vb" Inherits="IncomeStatement"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

 <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
     <tr bgcolor="#FFFFFF" ><td align="left">
            <asp:LinkButton ID="hlnkMainPage" OnClick="hlnkMainPage_Click"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:LinkButton>&nbsp; &nbsp;
            <asp:LinkButton ID="hlnkBackPage" OnClick="hlnkBackPage_Click"  CssClass="btn_02" runat="server">Back To Trial Balance</asp:LinkButton></td>
     </tr>
     <tr bgcolor="#FFFFFF" ><td colspan="2" align="center" class="Heading">North South Foundation</td></tr>
     <tr bgcolor="#FFFFFF" ><td colspan="2" align="center" class="txt01"><b>Statement Of Activities</b><br /><asp:Label ID="LblMessage" runat="server"></asp:Label><br /><asp:Label ID="lblIncomeStmt_ID" runat="server" ></asp:Label></td></tr>
     <tr bgcolor="#FFFFFF" ><td colspan="2" align="center" class="txt01">&nbsp;</td></tr>
     <tr runat="server" id="TrDetailView"  bgcolor="#FFFFFF" ><td  align="center"> &nbsp;</td> </tr> 
     <tr><td><table><tr><td>Operation</td>
                          <td><asp:DropDownList ID="ddlOperation" runat="server"  AutoPostBack="True" DataTextField="Operation" DataValueField="Operation" Width="70px">
                                    <asp:ListItem Value="1"> Title </asp:ListItem>
                                    <asp:ListItem Value="2"> Header </asp:ListItem>
                                    <asp:ListItem Value="3"> I </asp:ListItem>
                                    <asp:ListItem Value="4"> Sum </asp:ListItem>
                                    <asp:ListItem Value="5"> Blank </asp:ListItem>
                                    <asp:ListItem Value="6"> Calc </asp:ListItem>
                              </asp:DropDownList></td>
                         <td>Intensity</td>
                         <td><asp:DropDownList ID="ddlIntensity" runat="server" DataTextField="Intensity" DataValueField="Intensity" Width="70px">
                                        <asp:ListItem Value="Bold">Bold</asp:ListItem>
                                        <asp:ListItem Value="Normal" Text=""> </asp:ListItem>
                              </asp:DropDownList></td>
                         <td>LineNo</td><td><asp:DropDownList ID="ddlLineNo" runat="server" DataTextField="ddlLineNo" DataValueField="LineNo" Width="40px"></asp:DropDownList></td>
                         <td>Description</td><td><asp:DropDownList ID="ddlDescription" runat="server" DataTextField="Description" DataValueField="Description" Width="150px" AutoPostBack="true"></asp:DropDownList></td>
                         <td>UnRestricted:$</td><td><asp:TextBox ID="txtUnRestricted" runat="server" Width="70px" Enabled="False"></asp:TextBox></td>
                         <td>TempRestricted:$</td><td><asp:TextBox ID="txtTempRestricted" runat="server" Width="70px" Enabled="False"></asp:TextBox></td>
                         <td>PermRestricted:$</td><td><asp:TextBox ID="txtPermRestricted" runat="server" Width="70px" Enabled="False"></asp:TextBox></td>
                         <td>Total:$</td><td><asp:TextBox ID="txtTotal" runat="server" Width="70px" Enabled="False"></asp:TextBox></td>
         </tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td>Description</td><td><asp:TextBox ID="txtDescription" runat="server" Width="150px" Enabled="true"></asp:TextBox></td>
         </tr>
         
         </table></td> 
     </tr>
          <tr><td align="center"><asp:Button ID="BtnAddUpdate" runat="server" Text="Add" /> &nbsp;&nbsp;
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" /></td>
     </tr>
     <tr><td><table align="center"><tr><td>
        <asp:DataGrid ID="DGIncomeStmt" runat="server" AutoGenerateColumns="False"  OnItemCommand="DGIncomeStmt_ItemCommand"  Width="1000px" BorderWidth="1px" CellPadding="4"  AllowSorting="True" >					            							            
	       <Columns>                                        
             <asp:TemplateColumn>
                        <ItemTemplate><asp:LinkButton id="lbtnRemove" runat="server" CommandName="Delete" Text="Delete" Width="75px"></asp:LinkButton>
		                </ItemTemplate>
		            </asp:TemplateColumn>
               <asp:TemplateColumn>
                        <ItemTemplate><asp:LinkButton id="lbtnEdit" runat="server" CommandName="Edit" Text="Edit" Width="75px"></asp:LinkButton>
		                </ItemTemplate>
		           </asp:TemplateColumn>
		         <asp:Boundcolumn DataField="IS_ID"  HeaderText="IS_ID" Visible="false" />
                  <asp:Boundcolumn HeaderStyle-Font-Bold="true" ItemStyle-Width="75px" DataField="Operation"  HeaderText="Operation" />
                   <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Intensity" HeaderText="Intensity"/>
                    <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="LineNo" HeaderText="LineNo"/>
                     <asp:Boundcolumn HeaderStyle-Font-Bold="true" ItemStyle-Width="175px" DataField="Description" HeaderText="Description"/>
                      <asp:Boundcolumn HeaderStyle-Font-Bold="true" ItemStyle-Width="75px" DataField="URAmount" DataFormatString="{0:c}" HeaderText="UnRestricted"/>
                       <asp:Boundcolumn HeaderStyle-Font-Bold="true" ItemStyle-Width="75px" DataField="TRAmount" DataFormatString="{0:c}" HeaderText="TemporaryRestricted"/>
                        <asp:Boundcolumn HeaderStyle-Font-Bold="true" ItemStyle-Width="75px" DataField="PRAmount" DataFormatString="{0:c}" HeaderText="PermanentlyRestricted"/>
                         <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Total" DataFormatString="{0:c}" HeaderText="Total"/>      
            </Columns>   
        </asp:DataGrid></td></tr></table></td>
     </tr>
     <tr><td align="center"><asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
  </table> 
</asp:Content>

