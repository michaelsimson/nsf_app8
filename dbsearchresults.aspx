<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="dbsearchresults.aspx.vb" Inherits="dbsearchresults"  EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="server">
    <div  runat="server">
     <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" class="Heading">Search Results</td>
     </tr>
     <tr bgcolor="#FFFFFF" >
        <td colspan="2"><asp:HyperLink runat="server" ID="hlnkMainPage" NavigateUrl="~/dbsearch.aspx" >Back To Search</asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <asp:LinkButton ID="HyperLink1" runat="server">Add an Individual</asp:LinkButton>
        
        </td>
     </tr>
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="Heading"><asp:Label ID="LblMessage" runat="server" ></asp:Label><br />
        </td>
     </tr>
     <tr><td align="right"><asp:Button ID="btnExport" runat="server" Text ="Export to Excel" /></td></tr>
    <tr bgcolor="#FFFFFF"  runat="server">
    <td  runat="server">        
        &nbsp; &nbsp;
        <asp:Label ID="lblgvMsg" runat="server" Text="NOTE: *** Donations are applied to Donor Type = 'IND'" Visible="false" ForeColor="Brown" Font-Bold="true"></asp:Label>
        <asp:GridView  ID="gvIndSpouse"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None"  Caption="Individual / Spouse" CssClass="GridStyle" CaptionAlign="Top" PageSize="5" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
            <Columns>                
                <asp:BoundField DataField ="AutoMemberID" HeaderText="Member ID" />
                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/Registration.aspx?Id={0}"  HeaderText="Update Profile" Text="Update Profile" />                               
                <asp:TemplateField HeaderText="Donations">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="hlnkDonations" Text="Add/Update Donations" NavigateUrl="~/ViewDonations.aspx?Id={0}&type=IND"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:HyperLinkField  DataNavigateUrlFields="CustINDID" DataNavigateUrlFormatString="~/MainChild.aspx?Id={0}"  HeaderText="Add/Update Child Info" Text="Add/Update Child" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
                <asp:BoundField DataField="ReferredBy" HeaderText="ReferredBy" />                
                <asp:BoundField DataField="Chapter" HeaderText="Chapter" />
                <asp:BoundField DataField="DonorType" HeaderText="DonorType" />
            </Columns>
            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        </asp:GridView>
        &nbsp; &nbsp;&nbsp;
        <br /><br />
        </td>
        </tr>
             <tr><td align="right"><asp:Button ID="BtnExport_Org" runat="server" Text ="Export to Excel" /></td></tr>

        <tr>
            <td><asp:HyperLink runat="server" ID="HyperLink2" NavigateUrl="~/addorganization.aspx" >Add an Organization</asp:HyperLink></td>
        </tr>
        <tr bgcolor="#FFFFFF">
        <td align="center" >
        <asp:GridView ID="gvOrg" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" Caption="Organization" CssClass="GridStyle"  CaptionAlign="Top" PageSize="5" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" CellPadding="3" Height="133px" EnableSortingAndPagingCallbacks="false">
            <Columns>
                <asp:BoundField DataField="AutoMemberID" HeaderText="Member ID" />
                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/UpdateOrganization.aspx?MemberId={0}"  HeaderText="Update Profile" Text="Update Profile" />
                <asp:TemplateField HeaderText="Donations">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="hlnkOrgDonations" Text="Add/Update Donations" NavigateUrl="~/ViewDonations.aspx?Id={0}&type=OWN"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>                
                <asp:BoundField DataField="ORGANIZATION_NAME" HeaderText="Organization" />
                <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name" />
                <asp:BoundField DataField="MIDDLE_INITIAL" HeaderText="Initial" />
                <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name" />                
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                 <asp:BoundField DataField="Phone" HeaderText="Phone" />
                 <asp:BoundField DataField="Referred_By" HeaderText="Referred By" />
                 <asp:BoundField DataField="NSF_Chapter" HeaderText="NSF Chapter" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        &nbsp;
        </td>
        </tr>
</table>    
    </div>
</asp:Content>



 
 