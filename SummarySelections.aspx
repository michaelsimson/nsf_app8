<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SummarySelections.aspx.vb" Inherits="SummarySelections"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
	<table width="100%" border="0">
				<tr>
					<td class="Heading"  align="center" colspan="2">
                         Summary of Your Workshop Selections
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<%--<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>--%>
							<tr>
								<td><asp:label id="lblStateZip" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">Child/Children Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px"><asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="ECalendarID"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblChildId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildId") %>' CssClass="SmallFont" Visible="false">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="WorkShops Selected" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblWorkShop" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblProductId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>' CssClass="SmallFont" Visible ="false" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Workshop Date" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>' CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>' CssClass="SmallFont"></asp:Label>&nbsp;
										<asp:Label id="lblChapterId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterId") %>' CssClass="SmallFont" Visible ="false" ></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblLocation" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>' CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
						</td>
                        
				</tr>
				
								<tr><td class="SmallFont" style="width:340px" align="right">
								Total Amount to be paid :
                        <asp:Label ID="lblTotalAmt" style="color:Red" runat="server" Text="Label" Width="118px"></asp:Label>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
				<tr>
				<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td   colspan="2" class="SmallFont"  style="color:Red">
					Please verify your selections for each child along with the event date, chapter and location. 
					</td>
				</tr>
				<tr>
				<td   colspan="2" class="SmallFont" style="color:Red">
				The date and location are subject to change.  Please visit www.northsouth.org for the latest information.  Once paid, your payment is non-refundable
				</td>
				</tr>
			</table>
			<table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left" colspan="2">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
				</tr>
			</table>
</asp:Content>

