<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="GradeCard.aspx.vb" Inherits="GradeCard" title="GradeCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table cellpadding = "2" cellspacing = "0" border="0" width="1250px">
        <tr><td  class="btn_02" align="center"><asp:Label ID="lblmsg" runat="server" ForeColor="Green" Text="Grade Card"></asp:Label><br /></td></tr>
          <tr><td align="center"> <asp:Label CssClass="btn02" ID="lblCoachPaper" runat="server" Text="" ForeColor="Brown" Font-Bold="true"></asp:Label></td></tr>
             
</table>
 <table id="tblDGCoachPaper" runat="server" align="center" cellpadding = "2" cellspacing = "0" border="0" visible="false">
        <tr><td  colspan="8" align="center">
                <asp:DataGrid  ID="DGGradeCard" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4" 
                                 BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black">
                           <FooterStyle BackColor="#CCCCCC" />
                           <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                           <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                           <HeaderStyle  BackColor="#BE7C00" ForeColor="Blue"/>
                           <ItemStyle BackColor="White" />
                  <Columns>
                       <%--   <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="RowNo" HeaderText="ChildName"></asp:BoundColumn>   
                       --%>    <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="Ser#"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.RowNo") %>'></asp:Label>
                                                </ItemTemplate>
                             </asp:TemplateColumn>
                          <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ChildNumber" HeaderText="ChildNumber" Visible="false"></asp:BoundColumn>
                           <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventCode" HeaderText="Event"></asp:BoundColumn>
                             <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundColumn><%--Code--%>
                              <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Product" HeaderText="Product"></asp:BoundColumn><%--Code--%>
                               <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Phase" HeaderText="Phase"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SessionNo" HeaderText="Session"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                                  <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="StartDate" HeaderText="Date"></asp:BoundColumn>
                                   <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Day" HeaderText="Day"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Time" HeaderText="Time"></asp:BoundColumn>
                                     <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                                      <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="PaperID"></asp:BoundColumn>
                                       <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="MaxQ"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10" runat="server" ID="lblMaxQ" Text='<%# DataBinder.Eval(Container, "DataItem.MaxQ") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="Correct"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblCorrect" Text='<%# DataBinder.Eval(Container, "DataItem.Correct") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
                                             <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="Percent"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblPercent" Text='<%# DataBinder.Eval(Container, "DataItem.Percent") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
                                         
                                         
                                         <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="Max#of Papers"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblMax_Count" Text='<%# DataBinder.Eval(Container, "DataItem.Max_Count") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
                                     <%--    <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="#of Papers Uploaded[By children]"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblCount" Text='<%# DataBinder.Eval(Container, "DataItem.Count") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>--%>
                                         <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="#of Top Papers"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblTop_Count" Text='<%# DataBinder.Eval(Container, "DataItem.Top_Count") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
                                         
                                         
                                    <%--     
                                          <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Max_Count" HeaderText="Max#of Papers"></asp:BoundColumn>
                                     <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Count" HeaderText="No.of Children"></asp:BoundColumn>
                                      <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Top_Count" HeaderText="#of Top Papers"></asp:BoundColumn>
                                   --%>
                                          <asp:TemplateColumn ItemStyle-Width="150px"  HeaderText="Grade"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblGrade" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'></asp:Label>
                                                </ItemTemplate>
                                         </asp:TemplateColumn>
<%--                                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Gradeint" HeaderText="Grade" Visible="false"></asp:BoundColumn> 
--%>                    </Columns>
                </asp:DataGrid ></td></tr>
 <tr><td></td></tr>
  
        <tr><td  colspan="8" align="center"><asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
  </table>        
</asp:Content>




