﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Partial Class AvailableGames
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection
    Dim ChildNumCount As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadData()
            hlbl.Text = "N"
        End If
    End Sub
    Private Sub LoadData()
        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                ' lblCity.Text = drIndSpouse.Item("City")
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If

        Dim objChild As New Child
        Dim dsChild As New DataSet

        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")

        If dsChild.Tables.Count > 0 Then
            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Rows.Count
            ViewState("ChildInfo") = dsChild
        End If
    End Sub

    Protected Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound

        ''''''''''''''''''''''
        Dim SelCount As Integer = 0
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                'Dim dsContest As DataSet
                Try

                    Dim SQLStr As String = ""
                    Dim dsInvitees As New DataSet
                    Dim tblEligible() As String = {"EligibleCoach"}
                    SQLStr = SQLStr & " Select Distinct P.Name as ProductName,EF.LateRegDLDate,P.Status,P.ProductID ,P.ProductCode, P.ProductGroupId ,P.ProductGroupCode,P.EventId,P.EventCode,I.ChapterID ,I.Chapter from Child Ch"
                    SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear >=2010" '& Now.Year()
                    SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID and P.Status ='O' "
                    SQLStr = SQLStr & "inner Join IndSpouse I on I.AutoMemberID  =ch.MEMBERID "
                    SQLStr = SQLStr & " where ch.ChildNumber = " & e.Item.DataItem("ChildNumber") & " and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo "
                    SQLStr = SQLStr & " Order by P.ProductID"

                    SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, SQLStr, dsInvitees, tblEligible)
                    If dsInvitees.Tables(0).Rows.Count <= 0 Then
                        ChildNumCount = ChildNumCount + 1
                        lblContestInfo.Text = "There is no Games available to match your grade level."
                        lblContestInfo.Visible = True
                    Else
                        hlbl.Text = "Y"
                        lblContestInfo.Visible = False
                    End If
                    If hlbl.Text = "Y" Then
                        lblContestInfo.Visible = False
                    End If
                    Dim sbContest As New StringBuilder, strContestDesc As String
                    strContestDesc = ""
                    sbContest.Append("<ul>")
                    Dim sblevel As New StringBuilder
                    sblevel.Append("<ul>")
                    Dim i As Integer = 0

                    For Each dr As DataRow In dsInvitees.Tables(0).Rows
                        If strContestDesc <> dr.Item("ProductName") Then
                            sbContest.Append("<li>" & dr.Item("ProductName"))
                            sbContest.Append("</li>")
                            strContestDesc = dr.Item("ProductName")
                        End If
                        If dsInvitees.Tables(0).Rows.Count = SelCount Then
                            ChildNumCount = ChildNumCount + 1
                        End If
                    Next
                    If ChildNumCount = Session("ChildCount") Then
                        btnRegister.Enabled = False
                    Else
                        btnRegister.Enabled = True
                    End If

                    sbContest.Append("</ul>")
                    sblevel.Append("</ul>")
                    CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString



                Catch ex As Exception
                    lblContestInfo.Visible = True
                    lblContestInfo.Text = lblContestInfo.Text & "<br> " & ex.ToString()
                End Try
               
        End Select
    End Sub
    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Response.Redirect("~/GameRegistration.aspx")
    End Sub
    Protected Sub dgChildList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dgChildList.SelectedIndexChanged

    End Sub
    
End Class
