﻿
Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports NativeExcel
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class IncomeStatement
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                Dim FrmDate As DateTime = Request.QueryString("From")
                Dim Todate As DateTime = Request.QueryString("To")
                Response.Write(FrmDate & Todate)
                LblMessage.Text = Request.QueryString("From") & " - " & Request.QueryString("To")
                LoadLineNumbers()
                LoadDescription()
                LoadGrid()
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub


    Protected Sub hlnkBackPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkBackPage.Click
        Response.Redirect("TrialBalance.aspx")
    End Sub
    Private Sub LoadLineNumbers()
        For i As Integer = 1 To 35
            ddlLineNo.Items.Add(i)
        Next
    End Sub
    Private Sub LoadDescription()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT Distinct Description FROM NSFAccounts Where AccType in ('I')")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlDescription.DataSource = ds
            ddlDescription.DataBind()
        End If
        ddlDescription.Items.Insert(0, "SELECT")
    End Sub
    Protected Sub DGIncomeStmt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim IncomeStmt_ID As Integer = CInt(e.Item.Cells(2).Text)
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from IncomeStmt Where IS_ID=" & IncomeStmt_ID & "")
                LoadGrid()
            Catch ex As Exception
                lblSuccess.Text = (lblSuccess.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            BtnAddUpdate.Text = "Update"
            lblIncomeStmt_ID.Text = IncomeStmt_ID.ToString()
            loadforUpdate(IncomeStmt_ID)
        End If
    End Sub
    Private Sub loadforUpdate(ByVal IncomeStmt_ID As Integer)
        ' For update 
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * from IncomeStmt Where IS_ID=" & IncomeStmt_ID & "")
        ddlOperation.SelectedIndex = ddlOperation.Items.IndexOf(ddlOperation.Items.FindByValue(ds.Tables(0).Rows(0)("Operation")))
        ddlIntensity.SelectedIndex = ddlIntensity.Items.IndexOf(ddlIntensity.Items.FindByValue(ds.Tables(0).Rows(0)("Intensity")))
        ddlLineNo.SelectedIndex = ddlLineNo.Items.IndexOf(ddlLineNo.Items.FindByValue(ds.Tables(0).Rows(0)("LineNo")))
        If Not IsDBNull(ds.Tables(0).Rows(0)("Description")) Then
            If ddlDescription.Items.IndexOf(ddlDescription.Items.FindByValue(ds.Tables(0).Rows(0)("Description"))) = -1 Then
                txtDescription.Text = ds.Tables(0).Rows(0)("Description")
                ddlDescription.SelectedIndex = 0
            Else
                ddlDescription.SelectedIndex = ddlDescription.Items.IndexOf(ddlDescription.Items.FindByValue(ds.Tables(0).Rows(0)("Description")))
                txtDescription.Text = ""
            End If
        Else
            ddlDescription.SelectedIndex = ddlDescription.Items.IndexOf(ddlDescription.Items.FindByValue("0"))
            txtDescription.Text = ""
        End If
            txtUnRestricted.Text = IIf(ds.Tables(0).Rows(0)("URAmount") Is DBNull.Value, "", ds.Tables(0).Rows(0)("URAmount"))
            txtTempRestricted.Text = IIf(ds.Tables(0).Rows(0)("TRAmount") Is DBNull.Value, "", ds.Tables(0).Rows(0)("TRAmount"))
            txtPermRestricted.Text = IIf(ds.Tables(0).Rows(0)("PRAmount") Is DBNull.Value, "", ds.Tables(0).Rows(0)("PRAmount"))
            txtTotal.Text = IIf(ds.Tables(0).Rows(0)("PRAmount") Is DBNull.Value, "", ds.Tables(0).Rows(0)("Total"))
    End Sub
    Protected Sub ddlOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperation.SelectedIndexChanged
        ddlDescription.Enabled = True
        If ddlOperation.SelectedItem.Text.Trim = "I" Then
            txtDescription.Enabled = "False"
        ElseIf ddlOperation.SelectedItem.Text.Trim = "Blank" Then
            txtDescription.Enabled = "False"
            ddlDescription.SelectedItem.Text = "SELECT"
            ddlDescription.Enabled = False
        Else
            ddlDescription.SelectedItem.Text = "SELECT"
            txtDescription.Enabled = "True"
        End If
    End Sub
    Private Sub LoadGrid()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT * FROM IncomeStmt Order by [LineNo]")
        If ds.Tables(0).Rows.Count > 0 Then
            DGIncomeStmt.DataSource = ds
            DGIncomeStmt.DataBind()
        End If


        For i As Integer = 1 To DGIncomeStmt.Items.Count
            If DGIncomeStmt.Columns(4).ToString = "" Then

            End If

            Response.Write(i)
        Next

    End Sub

    Protected Sub ddlDescription_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDescription.SelectedIndexChanged

        txtUnRestricted.Text = ""
        txtTempRestricted.Text = ""
        txtPermRestricted.Text = ""

        Dim Sqlstr As String = ""
        Dim UnRest As Double = 0.0
        Dim TempRest As Double = 0.0
        Dim PermRest As Double = 0.0
        Dim Total As Double = 0.0
        Sqlstr = Sqlstr & " Select N.AccNo,NA.AccNo as HeaderAccNo,NA.Description as HeaderDesc,N.Description,ISNULL(N.RestrictionType,'Unrestricted') as RestrictionType, -SUM(CASE WHEN N.RestrictionType= 'Perm Restricted'  "
        Sqlstr = Sqlstr & " THEN G.Amount Else 0 END) as PRAmount,-SUM(CASE WHEN N.RestrictionType= 'Temp Restricted' THEN G.Amount Else 0 END) as TRAmount,-SUM(CASE WHEN N.RestrictionType= 'Unrestricted' OR N.RestrictionType IS NULL THEN G.Amount Else 0 END) as URAmount,SUM(G.Amount),AccountType  from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account INNER JOIN NSFAccounts NA ON NA.AccNo = Convert(int,LEFT(N.AccNo,LEN(N.AccNo) -3) + '000') "
        Sqlstr = Sqlstr & " where   N.AccType IN ('I') AND NA.Description='" & ddlDescription.SelectedItem.Text & "' AND"
        Sqlstr = Sqlstr & " G.BankDate is Not Null and G.BankDate Between '" & Convert.ToDateTime(Request.QueryString("From")) & "' AND '" & Convert.ToDateTime(Request.QueryString("To")) & " ' "
        Sqlstr = Sqlstr & " Group By N.AccNo,N.Description,N.RestrictionType,NA.Description,AccountType,NA.AccNo order By N.AccNo,NA.AccNo "
        'Response.Write(Sqlstr)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, Sqlstr)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If ds.Tables(0).Rows(i)("RestrictionType") = "Unrestricted" Then
                    UnRest = UnRest + ds.Tables(0).Rows(i)("URAmount")
                ElseIf ds.Tables(0).Rows(i)("RestrictionType") = "Temp Restricted" Then
                    TempRest = TempRest + ds.Tables(0).Rows(i)("TRAmount")
                ElseIf ds.Tables(0).Rows(i)("RestrictionType") = "Perm Restricted" Then
                    PermRest = PermRest + ds.Tables(0).Rows(i)("PRAmount")
                End If
            Next
            txtUnRestricted.Text = UnRest
            txtTempRestricted.Text = TempRest
            txtPermRestricted.Text = PermRest
            Total = Total + UnRest + TempRest + PermRest
            txtTotal.Text = Total
        Else
            'txtUnRestricted.Text = 0
            'txtTempRestricted.Text = 0
            'txtPermRestricted.Text = 0
        End If
    End Sub

    Protected Sub BtnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddUpdate.Click
        Dim SQLInsert As String = ""
        Dim SQLInsertVal As String = ""
        Dim SQLUpdate As String = ""
        Dim SQLLineNoStr As String = ""

        Try
            SQLInsert = SQLInsert & "Insert into IncomeStmt (Operation,Intensity,[LineNo],"
            SQLInsertVal = SQLInsertVal & "'" & ddlOperation.SelectedItem.Text & "','" & ddlIntensity.SelectedItem.Text & "'," & ddlLineNo.SelectedValue & ","
            SQLUpdate = SQLUpdate & "Update IncomeStmt set Operation='" & ddlOperation.SelectedItem.Text & "',Intensity='" & ddlIntensity.SelectedItem.Text & "',[LineNo]=" & ddlLineNo.SelectedValue & ","

            If ddlDescription.SelectedItem.Text.Trim <> "SELECT" Then
                SQLInsert = SQLInsert & "Description,"
                SQLInsertVal = SQLInsertVal & "'" & ddlDescription.SelectedItem.Text & "',"
                SQLUpdate = SQLUpdate & "Description='" & ddlDescription.SelectedItem.Text & "',"
            ElseIf txtDescription.Enabled = True And txtDescription.Text <> "" Then
                SQLInsert = SQLInsert & "Description,"
                SQLInsertVal = SQLInsertVal & "'" & txtDescription.Text & "',"
                SQLUpdate = SQLUpdate & "Description='" & txtDescription.Text & "',"
            End If
            If txtUnRestricted.Text <> "" Then
                SQLInsert = SQLInsert & "URAmount,"
                SQLInsertVal = SQLInsertVal & Convert.ToDouble(txtUnRestricted.Text.ToString) & ","
                SQLUpdate = SQLUpdate & "URAmount=" & Convert.ToDouble(txtUnRestricted.Text.ToString) & ","
            End If
            If txtTempRestricted.Text <> "" Then
                SQLInsert = SQLInsert & "TRAmount,"
                SQLInsertVal = SQLInsertVal & Convert.ToDouble(txtTempRestricted.Text.ToString) & ","
                SQLUpdate = SQLUpdate & "TRAmount=" & Convert.ToDouble(txtTempRestricted.Text.ToString) & ","
            End If
            If txtPermRestricted.Text <> "" Then
                SQLInsert = SQLInsert & "PRAmount,"
                SQLInsertVal = SQLInsertVal & Convert.ToDouble(txtPermRestricted.Text.ToString) & ","
                SQLUpdate = SQLUpdate & "PRAmount=" & Convert.ToDouble(txtPermRestricted.Text.ToString) & ","
            End If
            If txtTotal.Text <> "" Then
                SQLInsert = SQLInsert & "Total,"
                SQLInsertVal = SQLInsertVal & Convert.ToDouble(txtTotal.Text.ToString) & ","
                SQLUpdate = SQLUpdate & "Total=" & Convert.ToDouble(txtTotal.Text.ToString) & ","
            End If
            SQLInsert = SQLInsert & "CreatedBy,CreatedDate) Values("
            SQLInsertVal = SQLInsertVal & Session("LoginID") & ",GETDATE())"
            SQLUpdate = SQLUpdate & "ModifiedBy=" & Session("LoginID") & " ,ModifiedDate=GETDATE() Where IS_ID=" & lblIncomeStmt_ID.Text

            If BtnAddUpdate.Text = "Update" Then
                If SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, SQLUpdate) > 0 Then
                    lblSuccess.Text = "Updated Scuccessfully"
                    BtnAddUpdate.Text = "Add"
                End If
            Else
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From IncomeStmt Where [LineNo]=" & ddlLineNo.SelectedValue) > 0 Then
                    SQLLineNoStr = SQLLineNoStr & "Drop Table TempIS;WITH T AS (SELECT *,DENSE_RANK() OVER (ORDER BY [LineNo]) - [LineNo] AS Grp FROM IncomeStmt) SELECT MIN([LineNo]) AS LineStart,  MAX([LineNo]) AS LineEnd into TempIS  FROM T GROUP BY Grp ORDER BY MIN([LineNo]); SELECT * FROM TempIS Where " & ddlLineNo.SelectedValue & " Between LineStart and LineEnd"
                    Dim dsLineNo As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLLineNoStr)
                    If dsLineNo.Tables(0).Rows.Count > 0 Then
                        SQLLineNoStr = "Update IncomeStmt set [LineNo] = [LineNo]+1 FROM IncomeStmt I Inner Join TempIS T ON I.[LineNo] Between T.LineStart and T.LineEnd Where I.[LineNo]>=" & ddlLineNo.SelectedValue & " and " & ddlLineNo.SelectedValue & " Between T.LineStart and T.LineEnd  "
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLLineNoStr) > 0 Then
                            lblSuccess.Text = "Line Numbers Updated"
                        End If
                    End If
                    If SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, SQLInsert & SQLInsertVal) > 0 Then
                        lblSuccess.Text = "Insertion done Scuccessfully with Line Number Updates"
                    End If
                Else
                    If SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, SQLInsert & SQLInsertVal) > 0 Then
                        lblSuccess.Text = "Insertion done Scuccessfully"
                    End If
                End If

            End If
            LoadGrid()
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try

    End Sub

End Class
