﻿<%@ Page Language="VB" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace ="System.Security.Cryptography" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            
            'Open a file for reading
            Dim FILENAME As String = Server.MapPath(txtFileName.Text.Replace("/", "\"))
            'Get a StreamReader class that can be used to read the file
            Dim objStreamReader As StreamReader
            objStreamReader = File.OpenText(FILENAME)
            'File.Copy(FILENAME, Server.MapPath("DonorReceipts\temp.txt"))
            'Now, read the entire file into a string
            Dim contents As String = objStreamReader.ReadToEnd()
            'Set the text of the file to a Web control
            lbltxt.Text = contents
            'We may wish to replace carriage returns with <br>s
            lbltxt.Text = contents.Replace(vbCrLf, "<br>")
            objStreamReader.Close()
        Catch ex As Exception
            lbltxt.Text = "file not found <br>" & ex.ToString()
        End Try
    End Sub

    Protected Sub btnEnCrypt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As String = getMd5Hash(txtEnCrypt.Text)

        lbltxt.Text = "The MD5 hash of " + txtEnCrypt.Text + " is: " + hash + "."

        
        If verifyMd5Hash(txtEnCrypt.Text, hash) Then
            Console.WriteLine("The hashes are the same.")
        Else
            Console.WriteLine("The hashes are not same.")
        End If
    End Sub
    
    
    Function getMd5Hash(ByVal input As String) As String
        ' Create a new instance of the MD5 object.
        Dim md5Hasher As MD5 = MD5.Create()

        ' Convert the input string to a byte array and compute the hash.
        Dim data As Byte() = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function


    ' Verify a hash against a string.
    Function verifyMd5Hash(ByVal input As String, ByVal hash As String) As Boolean
        ' Hash the input.
        Dim hashOfInput As String = getMd5Hash(input)
        Response.Write(hashOfInput)
        ' Create a StringComparer an compare the hashes.
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

        If 0 = comparer.Compare(hashOfInput, hash) Then
            Return True
        Else
            Return False
        End If

    End Function

    
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            
            'Open a file for reading
            Dim FILENAME As String = Server.MapPath(txtFileName.Text.Replace("/", "\"))
            'Get a StreamReader class that can be used to read the file
            'Dim objStreamReader As StreamReader
            'objStreamReader = File.OpenText(FILENAME)
            'File.Copy(FILENAME, txtDest.Text & "temp" & Now.Year & Now.Month & Now.Day & Now.Millisecond & ".txt")
            File.Copy(FILENAME, txtDest.Text, True)
            'Now, read the entire file into a string
            'Dim contents As String = objStreamReader.ReadToEnd()
            'Set the text of the file to a Web control
            ' lbltxt.Text = contents
            'We may wish to replace carriage returns with <br>s
            'lbltxt.Text = contents.Replace(vbCrLf, "<br>")
            ' objStreamReader.Close()
        Catch ex As Exception
            lbltxt.Text = "Cannot Copy <br>" & ex.ToString()
        End Try
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim FILENAME As String = Server.MapPath(txtFileName.Text.Replace("/", "\"))
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(FILENAME)
        If file.Exists Then 'set appropriate headers
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End() 'if file does not exist
        Else
            Response.Write("This file does not exist.")
        End If
    End Sub

    Protected Sub btnCopyfolder_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CopyDirectory(Server.MapPath(txtFileName.Text.Replace("/", "\")), txtDest.Text, True)
    End Sub
    Private Sub CopyDirectory(ByVal sourcePath As String, ByVal destPath As String, ByVal overwrite As Boolean)
        Dim sourceDir As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(sourcePath)
        Dim destDir As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(destPath)
        'the source directory must exist for our code to run
        If (sourceDir.Exists) Then
            'if the destination folder does not exist, create it
            If Not (destDir.Exists) Then
                destDir.Create()
            End If
            'loop through all the files of the current directory
            'and copy them if overwrite=true or they do not exist
            Dim file As System.IO.FileInfo
            For Each file In sourceDir.GetFiles()
                If (overwrite) Then
                    file.CopyTo(System.IO.Path.Combine(destDir.FullName, file.Name), True)
                Else
                    If ((System.IO.File.Exists(System.IO.Path.Combine(destDir.FullName, file.Name))) = False) Then
                        file.CopyTo(System.IO.Path.Combine(destDir.FullName, file.Name), False)
                    End If
                End If
                : Next
            'loop through all the subfolders and call this method recursively
            Dim dir As System.IO.DirectoryInfo
            For Each dir In sourceDir.GetDirectories()
                If (dir.FullName <> txtDest.Text) Then
                    CopyDirectory(dir.FullName, System.IO.Path.Combine(destDir.FullName, dir.Name), overwrite)
                End If
            Next
            lbltxt.Text = "Folder copied successfully"

            'if source directory does not exist
        Else
            lbltxt.Text = "Error: source folder does not exist!"

        End If
        
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim FILENAME As String = Server.MapPath(txtFileName.Text.Replace("/", "\"))
        File.Delete(FILENAME)
        lbltxt.Text = "deleted Successfully"
         End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If FileUpload1.HasFile Then
            FileUpload1.PostedFile.SaveAs(Server.MapPath("/Signs/") & FileUpload1.FileName.Replace(" ", ""))
            lbltxt.Text = "uploaded Successfully"
        End If
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ssss
        
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Testing Pages</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style ="text-align :center">
        Source File Name : <asp:TextBox ID="txtFileName" Text="\App9\Spelling_Game\" runat="server" Width = "300px"></asp:TextBox><br />
        Destination File Name : <asp:TextBox ID="txtDest" Text="d:\inetpub\wwwroot\northsouth\App8\Spelling_Game"  Width = "300px" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button1"
            runat="server" Text="view" onclick="Button1_Click" />&nbsp;
            <asp:Button ID="Button2" runat="server" Text="Copy" onclick="Button2_Click" />&nbsp;
             <asp:Button ID="Button3" runat="server" Text="Download" onclick="Button3_Click" />
            
        &nbsp;<asp:Button ID="btnCopyfolder" runat="server" Text="Copy folder" 
            onclick="btnCopyfolder_Click" />
        <asp:Button ID="Button4" runat="server" Text="Delete" onclick="Button4_Click" />
            
        <br />
        <asp:FileUpload ID="FileUpload1" runat="server" />
&nbsp;<asp:Button ID="Button5" runat="server" onclick="Button5_Click" Text="Button" />
            
        <br />
             <asp:Label ID="lbltxt" runat="server" Text=""></asp:Label>
       
    </div>
    
     <div style ="text-align:center">
     Encrypt/DeCrypt : <asp:TextBox ID="txtEnCrypt" runat="server"></asp:TextBox><br />
         <asp:Button ID="btnEnCrypt"  onclick="btnEnCrypt_Click" runat="server" Text="EnCrypr/DeCrypt" />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Button ID="Button6" runat="server" onclick="Button6_Click" Text="Button" />
     </div> 
        
    </form>
</body>
</html>
