﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="CKEditorSendEmail.aspx.cs" Inherits="CKEditorSendEmail" %>
<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    
    <div>
        <table border="0">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;<asp:Label ID="lblError" runat="server" Text="" Visible="false"></asp:Label></td>
                <td style="width: 167px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <%--<asp:Label ID="Label1" runat="server" Text="Event"></asp:Label>--%>

                </td>
                <td>
                   <%-- <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem value="1">Finals</asp:ListItem>
                        <asp:ListItem value="2">Regionals</asp:ListItem>
                    </asp:DropDownList>--%>

                </td>
                <td style="width: 167px"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Chapter"></asp:Label>

                </td>
                <td>
                    <asp:DropDownList ID="ddlChapter" runat="server">
                       
                    </asp:DropDownList>&nbsp;&nbsp;<asp:Button ID="loadTomaillist" runat="server" Text="Load To Email list" OnClick="loadTomaillist_Click" />

                </td>
                <td style="width: 167px"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="fromEmail" runat="server" Text="From"></asp:Label></td>
                <td>
                    <asp:TextBox ID="tbMailFrom" runat="server"></asp:TextBox>
                    <%--<input ID="tbMailFrom" type="email" runat="server" />--%>

                CC: &nbsp&nbsp;<asp:TextBox ID="ccemaillist" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                <td style="width: 167px"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="To"></asp:Label></td>
                <td>
                    <asp:TextBox ID="tbMailTo" runat="server" TextMode="MultiLine" Width="567px"></asp:TextBox></td>
                <td valign="middle" style="width: 167px">
                    &nbsp;&nbsp;<asp:Button ID="cleartolist" runat="server" Text="Clear" OnClick="cleartolist_Click" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Subject"></asp:Label></td>
                <td>
                    <asp:TextBox ID="tbEmailSubject" runat="server" Width="456px" ></asp:TextBox></td>
                <td style="width: 167px"></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label6" runat="server" Text="Body" Font-Bold="true"></asp:Label>
                    </td>
                <td valign="bottom" colspan="2" align="right">
                    <asp:FileUpLoad id="FileUpLoad1"   runat="server" style="margin-bottom: 7px" />
                <asp:Button ID="insertFiletoeditor" runat="server" Text="Insert File to Editor" OnClick="ChooseFileUpload_Click"  /></td>
            </tr>
            
        </table>
 <CKEditor:CKEditorControl ID="txtEmailBody" runat="server" Width="800px"></CKEditor:CKEditorControl>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Send" OnClick="Button1_Click" /><asp:Label ID="lbEmail" runat="server" Text="" Visible="false"></asp:Label>
    </div>
   
    </div>
   
    </asp:Content>

