<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="NotYetAcceptedCoachList.aspx.vb" Inherits="NotYetAcceptedCoachList" title="NotYetAcceptedCoachList" EnableEventValidation = "false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server"  EnableViewState="false">
  
    <script language="javascript" type="text/javascript">
            function PopupPicker(ctl) {
                      var PopupWindow = null;
                      settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                      PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CoachingClassesReport Help', settings);
                      PopupWindow.focus();
                    }
	</script>
    <div>
        <table border="0" cellpadding ="3" cellspacing = "0" width ="980">
                <tr><td align="left">
                  <asp:hyperlink CssClass="btn_02" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>&nbsp;&nbsp;
                    </td></tr>
        </table>
        
        <asp:Panel ID="hiddenPanel" runat ="server" Visible="False">
        
        <table border="1" cellpadding="3" cellspacing="0" align="center" visible ="false">
            <tr><td align="center" colspan="2" style="font-size:16px; font-weight:bold ; font-family:Calibri">Calendar Signup</td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Name</td>
                  <td style="width: 133px">
                    <asp:TextBox ID="txtName" runat="server" style="margin-left: 0px" Width="150px" Enabled="false" Visible="false"></asp:TextBox>
                    <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server"  AutoPostBack="True" Height="20px" Width="150px" Visible="false"></asp:DropDownList>
                      <asp:HiddenField Id="hdnMemberID" runat="server" />
                        </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                <asp:Button ID="btnClear" runat="server" Text="Clear" Height="25px" Width="50px"/>&nbsp;
                <asp:Button Id="btnSearch" runat="server" Text="Search" Height="25px" Width="72px" Visible="false"/></td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Event Year</td>
                  <td align="left" style="width: 133px">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" style="width: 107px">Event</td>
                <td align="left" style="width: 133px">
                    &nbsp;</td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Phase</td>
                 <td align="left" style="width: 133px">
                        &nbsp;
                            </td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Product Group</td>
                 <td align="left" style="width: 133px">&nbsp;</td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Product</td>
                 <td align="left" style="width: 150px">&nbsp;</td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Level</td>
                 <td align="left" style="width: 133px">
                     &nbsp;</td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Session #</td>
                 <td align="left" style="width: 133px">
                   <asp:DropDownList ID="ddlSession" runat="server" Width="100px" Height ="20px">
                       <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                         <asp:ListItem Value="2">2</asp:ListItem>
                          <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                              <asp:ListItem Value="5">5</asp:ListItem>               
                          </asp:DropDownList>&nbsp;
                            <a href="javascript:PopupPicker('Session');">Help</a>
                        </td>
            </tr> 
            <tr>
               <td align="left" style="width: 107px">Day</td>
                  <td style="width: 133px"><asp:DropDownList ID="ddlWeekDays" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Time</td>
                 <td align="left" style="width: 133px"><asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="120px"></asp:DropDownList> 
                   <asp:Label ID="lblESTTime" runat="server" Text="EST"></asp:Label>
                   </td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Preferences</td>
                 <td style="width: 133px">
                    <asp:Dropdownlist ID="ddlPref" runat="server" Height="20px" Width="100px">
                      <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                          <asp:ListItem Value="3">3</asp:ListItem>
                        </asp:Dropdownlist>&nbsp;
                        <a href="javascript:PopupPicker('Pref');">Help</a>
                </td>
            </tr>
            <tr>
               <td align="left" style="width: 107px">Maximum Capacity </td>
                 <td align="left" style="width: 133px"><asp:DropDownList ID="ddlMaxCapacity" runat="server" AutoPostback="true" Height="20px" Width="150px"></asp:DropDownList></td>
            </tr>
            <tr>
               <td align="center" colspan="2">&nbsp;</td></tr>
            <tr>
               <td align="center" colspan="2">
                 <asp:Button ID="btnSubmit" runat="server" Text="Add/Update" />&nbsp;
                   <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                     </td>
            </tr>
            <tr>
               <td align="center" colspan="2">
                 <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                   <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                     </td>
            </tr>
            <tr>
               <td align="left" colspan="2">
                 <asp:Label  CssClass="btn_02" ID="lblMesg" runat="server" Text="(Please signup for at least 3 alternate day/times, just in case)"></asp:Label>
                   </td>
            </tr>
            <tr>
               <td align="center" colspan="2">
                 <asp:Label ID="lblError" runat="server" ForeColor = "Red"></asp:Label>
                   </td>
            </tr>
        </table>
        </asp:Panel>
        <div align ="center" style="font-size:26px; font-weight:bold ; font-family:Calibri">
        Scheduled Classes Report
        
            <br />
            <br />
        
            <br />

        </div>
        <div align="center">
            <table style="width: 100%">
                <tr>
                    <td style="width: 67px; text-align: left; font-weight: 700;" 
                        bgcolor="Honeydew">
                        Event Year</td>
                    <td bgcolor="Honeydew" style="text-align: left; width: 77px; font-weight: 700;">
                        Event</td>
                    <td bgcolor="Honeydew" style="width: 82px; text-align: left; font-weight: 700;">
                        Phase</td>
                    <td bgcolor="Honeydew" style="width: 132px">
                        Product Group</td>
                    <td bgcolor="Honeydew" style="width: 118px">
                        Product</td>
                    <td bgcolor="Honeydew" style="width: 124px">
                        Level</td>
                    <td bgcolor="Honeydew" style="width: 63px">
                        Accepted</td>
                    <td bgcolor="Honeydew" style="width: 58px">
                        &nbsp;</td>
                    <td bgcolor="Honeydew" style="width: 107px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 67px; text-align: left;">
                        <asp:DropDownList ID="ddlEventYear" 
                            OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" 
                            runat="server" Height="20px"  Width="80px"></asp:DropDownList>
                    </td>
                    <td style="width: 77px">
                    <asp:DropDownList ID="ddlEvent" 
                            OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"  
                            DataTextField="EventCode"  DataValueField="EventID" AutoPostBack="true" 
                            runat="server" Height="20px" Width="80px" style="text-align: left">
                       </asp:DropDownList>
                               </td>
                    <td style="width: 82px; text-align: left;">
                        <asp:DropDownList ID="ddlPhase" 
                            OnSelectedIndexChanged ="ddlPhase_SelectedIndexChanged"  Width="80px" 
                            Height ="20px" runat="server" style="text-align: left">
                              <asp:ListItem Value="1">One</asp:ListItem>
                                <asp:ListItem Value = "2">Two</asp:ListItem>
                                   <asp:ListItem Value = "3">Three</asp:ListItem>
                          </asp:DropDownList>                            
                        </td>
                    <td style="text-align: left; width: 132px">
                        <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID"  OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
                    <td style="width: 118px">
                        <asp:DropDownList ID="ddlProduct" DataTextField="Name" 
                        DataValueField="ProductID" AutoPostBack="true" 
                        OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false" 
                        runat="server" Height="20px" Width="150px"></asp:DropDownList>
                    </td>
                    <td style="width: 124px">
                   <asp:DropDownList ID="ddlLevel" runat="server" Width="120px" Height="20px">
                        </asp:DropDownList> 
                       </td>
                    <td style="width: 63px">
                        
                        <asp:DropDownList ID="ddlAccepted" runat="server" 
                            Width="70px">
                            <asp:ListItem Value="1">Y</asp:ListItem>
                            <asp:ListItem Value="2">All</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                    <td bgcolor="White" style="width: 58px">
                        
                        <asp:Button ID="bttnSubmit" runat="server" Text="Submit" />
                        
                    </td>
                    <td style="width: 107px">
                        
                        <asp:Button ID="bttnExport" runat="server" Enabled="False" 
                            Text="Export to Excel" />
                    </td>
                    <td>
                        
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblerr" 
                       runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 132px">
                        &nbsp;</td>
                    <td style="width: 118px">
                        &nbsp;</td>
                    <td style="width: 124px">
                        &nbsp;</td>
                    <td colspan="4">
                        
                        &nbsp;</td>
                </tr>
            </table>
        </div>
        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
        <b> Search NSF member</b>   
        <div align = "center">       
            <table border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
                <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			        <td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	              </tr>
    	        <tr>
                   <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			         <td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	               </tr>
    	        <tr>
                   <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			         <td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	              </tr>
    	        <tr>
    	           <td align="right"><asp:Button ID="Button1" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" /></td>
    	              <td align="left"><asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/></td>
    	                </tr>		
	            </table>
	        <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
        </div> 
        <br />
        <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
        <b> Search Result</b>
         <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
            <Columns>
                    <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                     <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                      <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                       <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                        <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                         <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                          <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                           <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                            <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                             <asp:BoundField DataField="chapter" headerText="Chapter" ></asp:BoundField>
                  </Columns> 
           </asp:GridView>    
        </asp:Panel>
 </asp:Panel>
        <table>
            <tr><td align="center">
                    <asp:DataGrid ID="DGCoach" runat="server" datakeyfield="SignUpID" 
                    AutoGenerateColumns="False" Height="14px" CellPadding="2" BackColor="Navy" 
                    BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True" Font-Italic="False" 
                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
			    <HeaderStyle Font-Size="X-Small" ForeColor="White" Font-Names="Verdana" 
                        Font-Bold="True" BackColor="Navy" Font-Italic="False" Font-Overline="False" 
                        Font-Strikeout="False" Font-Underline="False" ></HeaderStyle>
			    <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                     <asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" >
<ItemStyle ForeColor="Blue"></ItemStyle>
                     </asp:EditCommandColumn> 
                     <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete" >
<ItemStyle ForeColor="Blue"></ItemStyle>
                     </asp:buttoncolumn>
                     <asp:BoundColumn DataField="SignUpID" ItemStyle-Width="50px"   HeaderText="SignUp ID"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " readonly=true Visible="true" >
<HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
                     </asp:BoundColumn>
                     <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Volunteer Name">
                      <HEADERSTYLE Width="130px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                      <ItemTemplate><asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>
                      </ItemTemplate>      
                      <EditItemTemplate><asp:DropDownList id="ddlDGMember" runat="server" 
                              DataTextField="Name" DataValueField="MemberId" 
                              OnPreRender="ddlDGMember_PreRender" 
                              OnSelectedIndexChanged="ddlDGMember_SelectedIndexChanged" 
                              AutoPostBack="true"></asp:DropDownList>   
                      </EditItemTemplate>                                                                  

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn > 
                      <asp:TemplateColumn HeaderText="First Name">
                          <EditItemTemplate>
                              <asp:DropDownList ID="ddlFname" runat="server" AutoPostBack="True" 
                                  DataTextField="Firstname" DataValueField="MemberId" Enabled="False" 
                                  onprerender="ddlFname_PreRender" 
                                  onselectedindexchanged="ddlFname_SelectedIndexChanged">
                              </asp:DropDownList>
                          </EditItemTemplate>
                          <ItemTemplate>
                              <asp:Label ID="lblFirstName" runat="server" 
                                  Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                          </ItemTemplate>
                      <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                      </asp:TemplateColumn>                      
                      <asp:TemplateColumn HeaderText="Last Name">
                          <EditItemTemplate>
                              <asp:DropDownList ID="ddlLname" runat="server" AutoPostBack="True" 
                                  DataTextField="Lastname" DataValueField="MemberId" Enabled="False" 
                                  OnPreRender="ddlLname_PreRender" 
                                  OnSelectedIndexChanged="ddlLname_SelectedIndexChanged">
                              </asp:DropDownList>
                          </EditItemTemplate>
                          <ItemTemplate>
                              <asp:Label ID="lblLastName" runat="server" 
                                  Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                          </ItemTemplate>
                      <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                      </asp:TemplateColumn>
                                           
                      <asp:TemplateColumn HeaderText="Event Year">
                           <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                             <ItemTemplate><asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                               </ItemTemplate>  
                                 <EditItemTemplate><asp:DropDownList id="ddlDGEventYear" runat="server" 
                                         DataTextField="EventYear" DataValueField="EventYear" 
                                         OnPreRender="ddlDGEventYear_PreRender" AutoPostBack="false" 
                                         Enabled="False"></asp:DropDownList>                                       
                                   </EditItemTemplate>                                 
                       </asp:TemplateColumn >
                       <asp:TemplateColumn HeaderText="EventCode">
                            <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate><asp:Label ID="lblEvent" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                   <asp:HiddenField ID="hfEventId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.EventId")%>' />

                                </ItemTemplate>      
                                  <EditItemTemplate>                                  
                                      <asp:DropDownList id="ddlDGEvent" runat="server" DataTextField="EventCode" 
                                          DataValueField="EventID" OnPreRender="ddlDGEvent_PreRender" 
                                          AutoPostBack="false" Enabled="false">  
                                         </asp:DropDownList>  
                                            </EditItemTemplate>
                       </asp:TemplateColumn> 
                       <asp:TemplateColumn HeaderText="Phase">
                             <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                               <ItemTemplate><asp:Label ID="lblPhase" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Phase")%>'></asp:Label>
                                 </ItemTemplate>      
                                   <EditItemTemplate>                                  
                                     <asp:DropDownList id="ddlDGPhase" runat="server" 
                                           OnPreRender="ddlDGPhase_PreRender" AutoPostBack="true" Enabled="false">  
                                       <asp:ListItem Value="1">One</asp:ListItem>
                                         <asp:ListItem Value="2">Two</asp:ListItem>
                                           <asp:ListItem Value="3">Three</asp:ListItem>
                                             </asp:DropDownList>  
                                               </EditItemTemplate>
                       </asp:TemplateColumn> 
                       <asp:TemplateColumn HeaderText="ProductGroupCode" Visible="true" >
                             <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                               <ItemTemplate>
                                  <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                    <asp:HiddenField ID="hfProductGroupId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>' />
                                     </ItemTemplate>  
                                       <EditItemTemplate>
                                          <asp:DropDownList id="ddlDGProductGroup" runat="server" DataTextField="Name" 
                                               DataValueField="ProductGroupId" Enabled="false" 
                                               OnPreRender="ddlDGProductGroup_PreRender">  
                                            </asp:DropDownList> 
                                               </EditItemTemplate>                                
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Product">
                             <HEADERSTYLE Width="80px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                               <ItemTemplate >
                                 <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                   <asp:HiddenField ID="hfProductId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>' />
                                    </ItemTemplate>  
                                      <EditItemTemplate>
                                       <asp:DropDownList id="ddlDGProduct" runat="server" DataTextField="Name" 
                                              DataValueField="ProductId" OnPreRender="ddlDGProduct_PreRender" 
                                              OnSelectedIndexChanged="ddlDGProduct_SelectedIndexChanged" 
                                              AutoPostBack="true">
                                        </asp:DropDownList> 
                                         </EditItemTemplate>                                 
                         </asp:TemplateColumn > 
                        <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Level" >
                             <HEADERSTYLE Width="100px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                               <ItemTemplate>
                                 <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                    </ItemTemplate>         
                                       <EditItemTemplate>
                                          <asp:DropDownList id="ddlDGLevel" runat="server" 
                                               OnPreRender="ddlDGLevel_PreRender" AutoPostBack="false">
                                                  </asp:DropDownList>
                                                    </EditItemTemplate>                                                                  

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn>  
                       <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Session" >
                            <HEADERSTYLE Width="100px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                 <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionNo")%>'></asp:Label>
                                   </ItemTemplate>         
                                     <EditItemTemplate>
                                        <asp:DropDownList id="ddlDGSessionNo" OnPreRender="ddlDGSessionNo_PreRender" 
                                             runat="server" AutoPostBack="false">
                                             <asp:ListItem Value="1">1</asp:ListItem>
                                               <asp:ListItem Value="2">2</asp:ListItem>
                                                 <asp:ListItem Value="3">3</asp:ListItem>
                                                   <asp:ListItem Value="4">4</asp:ListItem>
                                                     <asp:ListItem Value="5">5</asp:ListItem>               
                                                       </asp:DropDownList>
                                                        </EditItemTemplate>                                                                  

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn > 
                       <asp:TemplateColumn HeaderText="Day">
                            <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate >
                                <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Day")%>'></asp:Label>
                                 </ItemTemplate>      
                                   <EditItemTemplate>                                  
                                     <asp:DropDownList id="ddlDGDay" runat="server" OnPreRender="ddlDGDay_PreRender" 
                                           AutoPostBack="false"></asp:DropDownList>    
                                       </EditItemTemplate>
                       </asp:TemplateColumn> 
                       <asp:TemplateColumn  ItemStyle-HorizontalAlign="left" HeaderText="Time">
                            <HEADERSTYLE  Width="400px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                  </ItemTemplate>      
                                    <EditItemTemplate>
                                       <asp:DropDownList id="ddlDGTime" runat="server"  
                                            OnPreRender="ddlDGTime_PreRender" AutoPostBack="false"></asp:DropDownList>    
                                         </EditItemTemplate>                                                                  

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn > 
                         <asp:TemplateColumn  ItemStyle-HorizontalAlign="left" HeaderText="Accepted" Visible ="true" >
                            <HEADERSTYLE  Width="400px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                <asp:Label ID="lblAccepted" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'></asp:Label>
                                  </ItemTemplate>      
                                    <EditItemTemplate>
                                       <asp:DropDownList id="ddlDGAccepted" runat="server"  
                                            OnPreRender="ddlDGAccepted_PreRender" AutoPostBack="false">   
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                               <asp:ListItem Value="Y">Y</asp:ListItem>
                                               </asp:DropDownList> 
                                         </EditItemTemplate>        
                                                                                                   
<ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                                   
                       </asp:TemplateColumn >
                       
                       <asp:TemplateColumn HeaderText="Preferences">
                            <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                <asp:Label ID="lblPreferences" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Preference")%>'></asp:Label>
                                  </ItemTemplate>      
                                    <EditItemTemplate>                                  
                                      <asp:DropDownList id="ddlDGPreferences" runat="server" 
                                            OnPreRender="ddlDGPreferences_PreRender" AutoPostBack="false">
                                          <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                              <asp:ListItem Value="2">3</asp:ListItem>
                                                </asp:DropDownList>    
                                                   </EditItemTemplate>                                                               
                       </asp:TemplateColumn > 
                       <asp:TemplateColumn HeaderText="Max Cap">
                            <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate >
                                <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCapacity")%>'></asp:Label>
                                 </ItemTemplate>      
                                  <EditItemTemplate>                                  
                                    <asp:DropDownList id="ddlDGMaxCapacity" runat="server" 
                                          OnPreRender="ddlDGMaxCapacity_PreRender" AutoPostBack="false"></asp:DropDownList> <%--OnSelectedIndexChanged="ddlDGCapacity_SelectedIndexChanged"--%>   
                                     </EditItemTemplate>                                                               
                       </asp:TemplateColumn > 
                     
                     
                     
          
                                                 
                     <asp:TemplateColumn HeaderText="Email">
                         <ItemTemplate>
                             <asp:Label ID="lblEmail" runat="server" 
                                 Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Home">
                         <ItemTemplate>
                             <asp:Label ID="lblHphone" runat="server" 
                                 Text='<%# DataBinder.Eval(Container, "DataItem.Hphone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Cell">
                         <ItemTemplate>
                             <asp:Label ID="lblCphone" runat="server" 
                                 Text='<%# DataBinder.Eval(Container, "DataItem.Cphone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Office">
                         <ItemTemplate>
                             <asp:Label ID="lblWphone" runat="server" 
                                 Text='<%# DataBinder.Eval(Container, "DataItem.Wphone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateColumn>
                     
                     
                     
          
                                                 
                    </Columns> 
         
           	        <PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
                    <AlternatingItemStyle BackColor="LightBlue" />         
                </asp:DataGrid>
              
            </td></tr>
        </table>
    </div>

</asp:Content>
