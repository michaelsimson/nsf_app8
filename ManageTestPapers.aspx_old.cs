using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

#region " Class ManageTestPapers "
public partial class ManageTestPapers : System.Web.UI.Page
{
    #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion
    // **************** Roles Allowed to Access in this Page
    //  Role 1,  Role 9 National,  Role 31 and 32 have full access.
    //  Role 9 chapter and  Role 33 can only download test papers for the current year.  
    
    #region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
            Response.Redirect("login.aspx?entry=v");

        if (!IsPostBack)
        {
            gvSortDirection = "ASC";
            gvSortExpression = "ProductCode";

            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "9") || (Session["RoleID"].ToString()== "93")))
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=" + Session["RoleId"]+ " and [National]='Y' and MemberID = " + Session["LoginID"])) > 0)
                    hdnTechNational.Value = "Y";
                else
                    hdnTechNational.Value = "N";
            }


            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "31") || (Session["RoleId"].ToString() == "32") || ((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "Y")) ||((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y"))))
            {
                GetDropDownChoice(dllfileChoice, true);
                dllfileChoice.Visible = true;
                if (Session["RoleId"].ToString() == "93")
                {
                    dllfileChoice.Items.RemoveAt(1);
                    //dllfileChoice.Enabled = false;
                }

            }
            else if ((Session["RoleId"] != null) && (((Session["RoleId"].ToString() == "9")||(Session["RoleId"].ToString() == "93")) && (hdnTechNational.Value == "N"))) //|| (Session["RoleId"].ToString() == "33")
            {
                if (Request.QueryString["id"] != null)
                {
                    SqlDataReader Reader   = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString (), CommandType.Text, "select V.ChapterID, Ch.ChapterCode from Volunteer V inner Join Chapter Ch ON V.ChapterID= Ch.ChapterID  where  V. MemberID = " + Session["LoginID"] + " AND V.VolunteerId =" + Request.QueryString["id"].ToString() + "");
                    while(Reader.Read())
                    {
                        lblChapter.Text = Reader["ChapterCode"].ToString ();
                        lblChapter.Visible = true;
                        hdnChapterID.Value = Reader["ChapterID"].ToString ();
                    } 
                }

                if (hdnChapterID.Value.Length > 0)
                {
                    gvTestPapers.Columns[10].Visible = true;
                    int weekid = Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                    int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);
                    if (Count == 0)
                    {
                        //LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
                    }
                    else
                    {
                        LblexamRecErr.Text = "";
                    }
                    gvTestPapers.Columns[10].Visible = false;
                    Panel3.Visible = true;  
                    
                }
                else
                     LoadSearchAndDownloadPanels(false);
            }
            else
            {
                Panel2.Visible = false;
                Panel3.Visible = false;
                lblNoPermission.Visible = true;
                lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
            }
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
           
            LoadSearchAndDownloadPanels(true);
            //Loads the Grid.....
             //GetTestPapers(m_objETP);
            if ((Convert.ToInt32(Session["RoleID"])==9) ||(Convert.ToInt32(Session["RoleID"])==93))
            {
                 int weekid = Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                 int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);
               
                 if (Count == 0)
                 {
                         LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
                 }
                 else
                 {
                         LblexamRecErr.Text = "";
                         gvTestPapers.Columns[10].Visible = false;
                         Panel3.Visible = true;
                 }
            }
            else
            {
                         GetTestPapers(m_objETP);
            }

        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            LoadUploadPanel();
            //GetTestPapers(m_objETP);
            if ((Convert.ToInt32(Session["RoleID"]) == 9) || (Convert.ToInt32(Session["RoleID"]) == 93))
            {

                int weekid = Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);

                if (Count == 0)
                {
                    LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
                }
                else
                {
                    LblexamRecErr.Text = "";
                    gvTestPapers.Columns[10].Visible = false;
                    Panel3.Visible = true;
                }
            }
            else
            {
                GetTestPapers(m_objETP);
            }
            
        }
    }

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        int value = 0;
        
        if (FileUpLoad1.HasFile)
        {
            
            EntityTestPaper objETP = new EntityTestPaper();

            objETP.ProductId = int.Parse(ddlProduct.SelectedValue);
            objETP.ProductCode = ddlProduct.SelectedItem.Text;
            objETP.ProductGroupId = int.Parse(ddlProductGroup.SelectedValue);
            objETP.ProductGroupCode = GetProductGroupCode(ddlProductGroup.SelectedItem.Text);
            objETP.EventCode = GetEventCode(ddlProductGroup.SelectedItem.Text);
            objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
            objETP.SetNum = int.Parse(ddlSet.SelectedValue);
            objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
            objETP.ContestYear = ddlContestYear.SelectedItem.Text;
            objETP.DocType = ddlDocType.SelectedItem.Value;
            objETP.TestFileName = FileUpLoad1.FileName;
            objETP.Description = txtDescription.Text;
            objETP.Password = TxtPassword.Text;
            objETP.CreateDate = System.DateTime.Now;
            objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
           

            if (ValidateFileName(objETP))
            {
                try
                {
                   value =  TestPapers_Insert(objETP);
                    //string ftpPath = String.Format("{0}/{1}",
                    //    ConfigurationSettings.AppSettings["FTPTestPapersPath"],
                    //    FileUpLoad1.FileName);

                    //uploadFileUsingFTP(ftpPath, FileUpLoad1.FileContent);
                   if (value != 0)
                   {
                       SaveFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], FileUpLoad1);

                       lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                       GetTestPapers(m_objETP);
                   }
                   else
                   {
                       //lblMessage.Text = "You are inserting duplicate record";
                       FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString ()),"Temp_"+ FileUpLoad1.FileName.ToString ()));
                       hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                       Panel1.Visible = false;
                       Panel5.Visible = true; 
                       dllfileChoice.Enabled = false ;
                   }
                }
                catch(Exception err)
                {
                    lblMessage.Text = err.Message;
                }
            }
        }
        else
        {
            lblMessage.Text = "No File Uploaded.";
        }
    }

     protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (hdnTempFileName.Value.Length > 0)
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update testpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate()  where  testFileName='" + hdnTempFileName.Value.Replace("Temp_", "").ToString() + "'");
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), true);
            Panel5.Visible = false;
            Panel1.Visible = true;
            lblMessage.Text = "File Replaced : " + hdnTempFileName.Value.Replace("Temp_", "").ToString();
        }
        else
        {
            Panel5.Visible = false;
            Panel1.Visible = true;
        }

    }

     protected void btnNo_Click(object sender, EventArgs e)
     {
         dllfileChoice.Enabled = true;
         if (hdnTempFileName.Value.Length > 0)
                 File.Delete (string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value));
         Panel5.Visible = false;
         Panel1.Visible = true;
     }
    //public void gvTestPapers_RowDataBound(object sender, GridViewRowEventArgs e) 
    //{
 
    //}
  
    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                objETP.TestPaperId = int.Parse(gvTestPapers.Rows[index].Cells[0].Text);
                objETP.TestFileName = gvTestPapers.Rows[index].Cells[6].Text;
                objETP.DocType = gvTestPapers.Rows[index].Cells[10].Text;
                if (e.CommandName == "DeleteTestPaper")
                {
                    DeleteTestPaper(objETP);
                    //lblMessage.Text = DeleteTestPaperFromFTPSite(objETP);
                    DeleteFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                    GetTestPapers(m_objETP);
                }
                else if (e.CommandName == "Download")
                {
                     //DownloadFileFromFTP(objETP);
                    //lblMessage.Text = string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"]), objETP.TestFileName);

                    if ((Session["RoleId"] != null) && (((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "N")) || (Session["RoleId"].ToString() == "33")))
                    {
                        try
                        {
                           if ((objETP.DocType != "Instr"))
                            {
                                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(c.contestID) from contest C Inner Join TestPapers T ON C.ProductId = T.ProductId AND C.Contest_Year = T.ContestYear where C.InstrDownFlag is Not Null and  C.ExamRecID =  " + int.Parse(Session["LoginID"].ToString()) + " and C.NSFChapterID = " + hdnChapterID.Value + " and T.TestPaperId = " + objETP.TestPaperId + "")) > 0)
                                {
                                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                                }
                                else
                                    LblexamRecErr.Text = "Please download Instruction file before downloading test Papers";
                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "update C set C.InstrDownFlag = 1  from contest C Inner Join TestPapers T ON C.ProductGroupId = T.ProductGroupId AND C.Contest_Year = T.ContestYear where C.InstrDownFlag is  Null and C.ExamRecID =  " + int.Parse(Session["LoginID"].ToString()) + " and C.NSFChapterID = " + hdnChapterID.Value + " and T.TestPaperId = " + objETP.TestPaperId + "");
                                DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.ToString());
                        }

                    }
                        
                    else
                        DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                        
                    }
                
            }
        }
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        if (e.SortExpression == "CreateDate")
        {
            if (this.gvSortDirection == "ASC")
            {
                e.SortExpression = "YEAR DESC, MONTH DESC, DAY DESC";
                this.gvSortDirection = "DESC";
            }
            else
            {
                e.SortExpression = "YEAR ASC, MONTH ASC, DAY ASC";
                this.gvSortDirection = "ASC";
            }
        }

        else if (e.SortExpression == "ProductId")
        {
            if(this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {

                this.gvSortDirection = "ASC";
            }
            
            this.gvSortExpression = "ProductId";

        }
        else
        {
            if (this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {
                this.gvSortDirection = "ASC";
            }
        }
        if ((Session["RoleId"] != null) && ((((Session["RoleId"].ToString() == "9")|| (Session["RoleId"].ToString() == "93"))&& (hdnTechNational.Value == "N")) || (Session["RoleId"].ToString() == "33")))
        {
            LoadRecordsForExamRcvr();
        }
        else
        {
            GetTestPapers(m_objETP);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
        m_objETP.ProductId = int.Parse(ddlFlrProduct.SelectedValue);
        m_objETP.ProductGroupId = int.Parse(ddlFlrProductGroup.SelectedValue);
        m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedItem.Value);
        m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
        m_objETP.NoOfContestants = int.Parse(ddlFlrNoOfContestants.SelectedValue);
        m_objETP.Description = tbxFlrDescription.Text;
        m_objETP.TestFileName = tbxFlrTestFileName.Text;
        GetTestPapers(m_objETP);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
        ddlFlrProduct.SelectedIndex = 0;
        ddlFlrProductGroup.SelectedIndex = 0;
        ddlFlrWeek.SelectedIndex = 0;
        ddlFlrSet.SelectedIndex = 0;
        ddlFlrNoOfContestants.SelectedIndex = 0;
        tbxFlrDescription.Text = String.Empty;
        tbxFlrTestFileName.Text = string.Empty;
       // GetTestPapers(m_objETP);

        if ((Convert.ToInt32(Session["RoleID"]) == 9) || (Convert.ToInt32(Session["RoleID"]) == 93))
        {
            int weekid = Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
            int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);

            if (Count == 0)
            {
                LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
            }
            else
            {
                LblexamRecErr.Text = "";
                gvTestPapers.Columns[10].Visible = false;
                Panel3.Visible = true;
            }
        }
        else
        {
            GetTestPapers(m_objETP);
        }
    }

    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
    }

    #endregion

    #region " Private Methods - Data Access Layer "
    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Insert";
        SqlParameter[] param = new SqlParameter[17];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@EventCode", objETP.EventCode);
        param[5] = new SqlParameter("@WeekId", objETP.WeekId);
        param[6] = new SqlParameter("@SetNum", objETP.SetNum);
        param[7] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants == -1 ? 0 : objETP.NoOfContestants);
        param[8] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[9] = new SqlParameter("@Description", objETP.Description);
        param[10] = new SqlParameter("@Password", objETP.Password);
        param[11] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[12] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[13] = new SqlParameter("@ModifyDate", objETP.ModifyDate);
        param[14] = new SqlParameter("@ModifiedBy", objETP.ModifiedBy);
        param[15] = new SqlParameter("@DocType", objETP.DocType);
        param[16] = new SqlParameter("@ContestYear", objETP.ContestYear);
       
        value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
        if(value == null)
        {
            return -1;
           // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsproductgroup ;
            if (Convert.ToInt32(Session["RoleId"]) == 93)
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") Order by A.EventCode");
            }
            else
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active') AND ContestYear = " + System.DateTime.Now.Year.ToString() + ") Order by A.EventCode");
            }

            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetFlrContextYear()
    {
        ddlFlrYear.Items.Clear();
        if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "32"))
        {
            int Minyear;
            try
            {
                Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from Testpapers"));
                int year = DateTime.Now.Year;
                int j = 0;
                for (int i = Minyear; i <= year; i++)
                {
                    ddlFlrYear.Items.Insert(j, new ListItem(i.ToString()));
                    j = j + 1;
                }
                ddlFlrYear.SelectedIndex = ddlFlrYear.Items.IndexOf(ddlFlrYear.Items.FindByText(year.ToString()));
            }
            catch (SqlException se)
            {
                lblMessage.Text = se.Message;
                return;
            }

        }
        else
        {
            ddlFlrYear.Items.Insert(0, new ListItem(DateTime.Now.Year.ToString()));
            ddlFlrYear.Enabled = false;
        }
    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {
        object year;
        try
        {
          
            year =  SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct EventYear from Event");
         if (blnCreateEmptyItem)
            {
       
             ddlContestYear.Items.Insert(0, new ListItem(year.ToString()));
           
            }
         }

        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;

        }


    }

    private void GetWeek(DropDownList ddlWeek, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select WeekID, (Convert(varchar(14),SatDay1,106) +  ' - ' + Convert(varchar(14),sunDay2,106)) as Week  from weekCalendar Where Year(SatDay1)=Year(GETDATE()) Or Year(sunDay2)= YEAR(GETDATE())");
            ddlWeek.DataSource = dsWeek;
            ddlWeek.DataTextField = "Week";
            ddlWeek.DataValueField = "WeekID";
            ddlWeek.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlWeek.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlWeek.SelectedIndex = 0;
        }
        catch(SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            if (ProductGroupId != -1)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ProductGroupId", ProductGroupId);
                DataSet dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "Product_GetByProductGroupId", param);
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "ProductCode";
                ddlObject.DataValueField = "ProductId";
                ddlObject.DataBind();
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 5, 10, 15, 20, 25, 30, 35 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select #Of Children]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }


    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "UploadScreen", "DownloadScreen" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;



    }
    private void GetTestPapers(EntityTestPaper objETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        if (ddlFlrYear.SelectedValue == DateTime.Now.Year.ToString ())
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        else
            param[4] = new SqlParameter("@WeekId", -1);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@ContestYear", ddlFlrYear.SelectedValue);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
            lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
        else
            lblSearchErr.Text = string.Empty;
        DataView dv = new DataView(dt);
        //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

        gvTestPapers.DataSource = dv;
        gvTestPapers.DataBind();

        //if (Convert.ToInt32(Session["RoleID"]) == 93) //Added to Show only the eancbled productgroups for NatTechT(93)
        //{
        //    DataSet dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") ");
        //    String ProductGroups = "";

        //    for (int j = 0; j < dsproductgroup.Tables[0].Rows.Count; j++)
        //    {
        //        ProductGroups = ProductGroups + dsproductgroup.Tables[0].Rows[j]["ProductGroupCode"] + ",";
        //    }
        //    ProductGroups = ProductGroups.TrimEnd(',');
        //    if (gvTestPapers.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < gvTestPapers.Rows.Count; i++)
        //        {
        //            //for (int j = 0; j < dsproductgroup.Tables[0].Rows.Count;j++ )
        //            if (!ProductGroups.Contains(gvTestPapers.Rows[i].Cells[2].Text))
        //            {
        //                gvTestPapers.Rows[i].Visible = false;
        //            }
        //        }
        //    }
        //}
               
    }
    private int GetTestPapers(int memberid, int roleid, string contestYear, int weekID)
    {
        
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetTestPapersforDownload";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@memberid", memberid);
        param[1] = new SqlParameter("@contestyear", contestYear);
        param[2] = new SqlParameter("@WeekId", weekID);
        param[3] = new SqlParameter("@NoOfDaysBeforeContestDate", 10);
        param[4] = new SqlParameter("@Chapter", hdnChapterID.Value);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];
        DataTable dtNew = dt.Clone();
        String CurrTestFileName;
        String CurrTestFilePrefix;
        String PrevTestFilePrefix = "";
        try
        {
            //Following loop is to filter out the Test Paprers, which belong to the same product code and same set but with 
            //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
            //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                {
                    PrevTestFilePrefix = CurrTestFilePrefix;
                    DataRow dr = dt.Rows[i];
                    dtNew.ImportRow(dr);
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        DataView dv = new DataView(dtNew);
        //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

        gvTestPapers.DataSource = dv;
        gvTestPapers.DataBind();

        return dtNew.Rows.Count;
    }
    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TestPaperId", objDelETP.TestPaperId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }
    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion
    
    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");
            //Session["DOWNLOAD_FILE_PATH"] = filePath;
            //Response.Redirect("~/downloader.aspx");
            

            //The following code calls Response.End which results in bad thread errors
            //so, trying to avoid it with above code
            /*
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                FileInfo finfo = new FileInfo(filePath);
                long FileInBytes = finfo.Length;
                Response.AddHeader("Content-Length", FileInBytes.ToString());
            }
            Response.Clear();
            //Response.Write("File Path: " + filePath);
            Response.WriteFile(@filePath);
            Response.Flush();
            Response.End();
            */
        }
        catch (Exception ex)
        {
           lblMessage.Text = ex.ToString();
        }
    }

    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {
       
        string ProductGroupCode = EventCodeAndProductGroupCode.Substring(EventCodeAndProductGroupCode.Length - 2);
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = EventCodeAndProductGroupCode.Substring(0, EventCodeAndProductGroupCode.Length - 5);
        return EventCode;
    }


    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {

        Panel1.Visible = false;
        Panel2.Visible = searchPanel;
        Panel3.Visible = true;
        if (searchPanel == false)
        {
            Panel4.Visible = true;
            GetWeek(ddlFlrWeekForExamReceiver, true);
        }
        GetProductGroupCodes(ddlFlrProductGroup, true);
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
        GetWeek(ddlFlrWeek, true);
        GetFlrContextYear();
        PopulateContestants(ddlFlrNoOfContestants, true);
        PopulateSets(ddlFlrSet, true);
        //GetTestPapers(int.Parse(Session["RoleId"].ToString()));
    }

    private void LoadUploadPanel()
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
        Panel3.Visible = true;
        GetProductGroupCodes(ddlProductGroup, true);
        GetContextYear(ddlContestYear, true);
        GetWeek(ddlWeek, true);
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
        // GetWeeks(ddlWeek, true);
        PopulateContestants(ddlNoOfContestants, true);
        PopulateSets(ddlSet, true);
        ddlDocType.Items.Insert(0, new ListItem("[Select Doc Type]", "-1"));
        //GetTestPapers(m_objETP);
    }
    #endregion

    #region " Private Methods - Validations "
    private bool ValidateFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode;
        ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        string NoOfContestants = objETP.NoOfContestants.ToString();
        bool IsValidFileName = false;
        // Correct number of contestants if the selected value is "5"
        if (NoOfContestants == "5")
        {
            NoOfContestants = "05";
        }

        // Any product code other than MB, EW, and PS should have number of contestants
        if (ProductGroupCode == "MB" || ProductGroupCode == "EW" || ProductGroupCode == "PS")
        { 
            // No need to do anything here
        }
        else if (objETP.NoOfContestants == -1)
        {
            lblMessage.Text = "No. of Contestants is a required field.";
            return false;         
        }

        // Get the initial part of the file name
        if (objETP.NoOfContestants != -1)
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}_{4}_{5}", objETP.SetNum, System.DateTime.Now.Year, objETP.ProductGroupCode, objETP.ProductCode, objETP.DocType, NoOfContestants); //Set1_SB_05_JSB_2007.pdf or .doc
        }
        else
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}", objETP.SetNum,System.DateTime.Now.Year,objETP.ProductGroupCode,objETP.ProductCode,NoOfContestants); //Set1_SB_JSB_2007.pdf or .doc
        }

        // For MB the file name can have Q or A as suffix
        if (ProductGroupCode == "MB")
        {
            if (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip"))
                //||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_q", ".doc")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".pdf")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".doc")))
            {
                IsValidFileName = true;
            }
        }
        else if ((objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")) || (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")))
        {
            IsValidFileName = true;
        }

        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString().ToLower() + ".zip'") ;
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion


    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }

    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
        int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), selectedWeekId);
        if (Count == 0)
        {
            lblNoPermission.Visible = true;
            lblNoPermission.Text = "There are no records";
        }
    }

   
}
#endregion

#region "Enums" 
public enum ScreenChoice
{
    DownloadScreen,
    UploadScreen

}

#endregion

#region " Class EntityTestPaper (ETP) "
public class EntityTestPaper
{
    public Int32 TestPaperId = -1;
    public Int32 ProductId = -1;
    public string ProductCode = string.Empty;
    public Int32 ProductGroupId = -1;
    public string ProductGroupCode = string.Empty;
    public string EventCode = string.Empty;
    public Int32 WeekId = -1;
    public Int32 SetNum = -1;
    public Int32 NoOfContestants = -1;
    public string TestFileName = string.Empty;
    public string Description = string.Empty;
    public string Password = string.Empty;
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime? ModifyDate = null;
    public Int32? ModifiedBy = null;
    public string ContestYear = string.Empty;
     public string DocType = string.Empty;
    public string WeekOf = string.Empty;
    public string ReceivedBy = string.Empty;
    public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
   
   

    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
                  System.Configuration.ConfigurationManager.AppSettings["FTPTestPapersPath"], TestFileName);
    }
    public EntityTestPaper()
    {
    }

}
#endregion

 