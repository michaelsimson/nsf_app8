﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Don_athon_custom.aspx.vb" Inherits="Don_athon_custom" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
       </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"  language="jscript">
    function invisible() {
        document.getElementById("Divselect").style.visibility = "visible";
        document.getElementById("Divselect").style.display = "inline";
     }
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	text-align:center;
	width:100%;
	background-color:#FFF;
}
.style2 {
	font-size: 12px;
	font-family: Calibri;
	color: #FFFFFF;
}
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center" style="width :100%" >
    <table  border="1"  cellpadding = "0" cellspacing = "0" bordercolor="#189A2C" style="width:1000px; text-align:center">
    <tr>
    <td>
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:1000px; text-align:center">
    <tr><td> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="right"  >
                      <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                        <map name="Map" id="Map">
                          <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                        </map></td>
                      <td width="76%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="51%"  background="images_new/topbg_1.jpg"><img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                              <td width="49%" valign="top" background="images_new/img_03.jpg">
                              <table width="89%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="30%">&nbsp;</td>         
                                  <td width="70%" valign="top" >
                                  <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td valign="middle" style="padding-left:10px; padding-top:5px">
                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                        <Items>                    
                                       
                                        </Items>     
                                        </asp:Menu>
                                        </td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr><td colspan ="2" align="center" ><br />
                                    <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server" ></asp:Label>
                                    </td></tr>
                              </table>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="35" valign="middle" background="images_new/menubg.jpg">
<table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    <tr><td style="text-align:center; width :10px" >  </td><td style="vertical-align:middle; text-align:center;">
        <asp:Label ID="lblPageCaption" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" runat="server" ></asp:Label>
        <asp:Literal ID="ltlEvents" runat="server"></asp:Literal> </td></tr> 
    </table>
</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
    </td>
    </tr>
  
      
        <tr><td align="left" style="height :1px">
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" Visible="false" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerFunctions" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink>
            
            </td></tr>
        <tr><td align="center">        
         <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:100%; text-align:center">
    <tr><td valign = "top" align="center">
    <table border = "0" cellpadding = "3" cellspacing = "0" >
     <tr>
      <td colspan="2"  style="vertical-align:middle; text-align:center; padding-top:20px" >
          <asp:Image ID="img1" ImageUrl="~/walkmarathon/testimg.jpg"
            BorderColor="1"   runat="server" BorderStyle="Solid" 
            BorderWidth="1px" /></td></tr>
     <tr >
       <td height="10px" colspan="2" style="vertical-align:middle; text-align:left;" >&nbsp;
            </td>     </tr> 
       <tr>
       <td style="vertical-align:middle; text-align:left;" >
           Fundraising target        </td> 
       <td style="vertical-align:middle; text-align:left;" > : 
        <asp:Label ID="lblTargetamt" runat="server"></asp:Label>    </td>
    
   
 
     </tr> 
    <tr><td style="vertical-align:middle; text-align:left;" >
        So far I have raised
    </td> <td style="vertical-align:middle; text-align:left;" >:
        <asp:Label ID="lblRaisedamt" runat="server"></asp:Label>
    </td></tr>
    
    <tr runat="server" id="trChapt" >
    <td width="50%" style="vertical-align:middle; text-align:left;" >
        For Chapter
    </td> <td style="vertical-align:middle; text-align:left;" >:
        <asp:Label Font-Bold="true"   ID="lblChapter" runat="server"></asp:Label>
    </td></tr>
   
    <%--<tr>
      <td colspan="4" style="vertical-align:middle; text-align:left; font-weight :normal ;" >
     <b>My Personal Message :</b>
      <Div align="justify">
          <asp:Label ID="lblCustomMessage" runat="server"></asp:Label>     
      </Div> 
      
      </td>
     
    </tr>--%>
    <tr><td colspan="2"  style="vertical-align:middle; text-align:center; padding:5px" >
         <asp:Button ID="btnSponsor" runat="server" Text="Sponsor me now" OnClick="btnSponsor_Click" /><br />
        <asp:Label ID="lblErr" runat="server" ForeColor="Red"  ></asp:Label>
        </td></tr><tr>
      <td colspan="2"  style="vertical-align:middle; text-align:center;" >
    <b>Fundraising Progress</b><br />
      <iframe height ="100px" scrolling="no" marginheight="0px" marginwidth="0px" frameborder="0" src="https://chart.googleapis.com/chart?cht=gom&chs=200x100&chd=t:<%= i %>&chxt=x,y&chxl=0:| |1:|0%|50%|100%&chma=0,0,0,0|0,0"  width ="200px" ></iframe>
      
      
     </td></tr>
                <tr><td colspan="2"  style="vertical-align:middle; text-align:center;" >
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4c7ba56958893624" class="addthis_button_compact" style="font-family:Calibri; font-size:13px;">
    Share</a>
<span class="addthis_separator">|</span>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_myspace"></a>
<a class="addthis_button_google"></a>
<a class="addthis_button_twitter"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c7ba56958893624"></script>
<!-- AddThis Button END -->
     </td></tr>
    
    </table> </td> <td width="75%" style = "padding :5px">
     <asp:Literal ID="Literal1" runat="server"></asp:Literal><br />
    
              <asp:Literal ID="ltlCustomMsg" runat="server"></asp:Literal>
<div align="left" runat="server" id="divchldname"  style="font-family:Calibri; font-size:14px;">
    Sincerely,&nbsp;&nbsp;&nbsp;&nbsp;<br />
    <asp:Label ID="lblName" runat="server"></asp:Label>
</div>
<div align="left" runat="server" id="divWalkathon" visible = "false" style="font-family:Calibri; font-size:12px;">
   <center>   <asp:Button ID="btnSponsor1" runat="server" Text="Sponsor me now" OnClick="btnSponsor_Click" /></center><br /><br />
  <b>  Note: Please click on &#39;Sponsor me now&#39; button to continue. Here are the 
    options for payment after you click to continue:</b>
<br /><br />
    1. Secure Online Credit Card payment: Your card information is not stored in our 
    records. If you were a previous user/donor, you can use that login. Otherwise, 
    check New User.<br />
    2. Cash and Check payment: Please provide payment to the participant. If you 
    were a previous user/donor, you can use that login. Otherwise, check New User.

</div>
       

    </td> </tr> </table> 
    
    
        </td> </tr>
          <tr runat="server" id="trExcelethon" visible="true" >
          <td align="center" style="padding-left:5px; padding-right:5px">
           

</td> </tr>
         
           <tr><td align="center" style="font-family:Calibri; font-size:14px;">
           <div style="width:80%; text-align:justify;font-family:Tahoma; font-size:12px; font-weight :normal " runat ="server" id="divbelow">
            <b>Note: (for Payment Please press &quot;Sponsor Me now&quot; button)</b><br />

               1. Credit Card payment needs Login. If you were a previous user, you can use 
               that login here.<br />
               2. Cash and Check payment needs to register, you can enter the amount and 
               Pay/Give the check/Cash to the Participant.<br /><br />


                </div>
           </td> </tr> 
            <tr><td align="center">
            <asp:Label ID="lblSponsor" Font-Bold="true" runat="server" Text="Thanks to the following donors for their support!!"></asp:Label>
         <br />
    <asp:GridView  Width="600px" ID="GVwList" AutoGenerateColumns="False" runat="server" 
                   BackColor="White" BorderColor="#337816" BorderWidth="1px" 
                   CellPadding="3" GridLines="Horizontal">
                   <RowStyle BackColor="#FFFFFF" ForeColor="#4A3C8C" />
                   <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                   <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                   <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                   <HeaderStyle BackColor="#47a81d" Font-Bold="True" ForeColor="#F7F7F7" />
                   <AlternatingRowStyle BackColor="#88c800" />
                   <Columns >
                    <asp:BoundField DataField="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign ="Left"    HeaderText="Name"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <%--<asp:BoundField  DataField="LastName"   HeaderText="Last Name"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >--%>
                <asp:BoundField  DataField="CreateDate" HeaderStyle-HorizontalAlign="Left"  ItemStyle-HorizontalAlign ="Left"    HeaderText="Date" Visible="false" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataFormatString = "{0:d}"></asp:BoundField>
                <asp:BoundField  DataField="PledgeAmount" HeaderStyle-HorizontalAlign="Left"   ItemStyle-HorizontalAlign ="Left"   HeaderText="Amount"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataFormatString = "{0:c}"></asp:BoundField >
                <asp:BoundField  DataField="Comment" HeaderStyle-HorizontalAlign="Left"   ItemStyle-HorizontalAlign ="Left"   HeaderText="Comment"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                </Columns> 
                </asp:GridView>
               
            </td> </tr> 
             <tr><td height="5px" align="center"></td> </tr> 
            <tr><td align="right"  bgcolor="#99CC33" style="height:25px; vertical-align:middle ;" class="style2">
                © North South Foundation. All worldwide rights reserved.
<a class="btn_01" href="/public/main/privacy.aspx">Copyright</a>&nbsp;&nbsp;&nbsp;
</td> 
            </tr>
    </table>
   
    <asp:HiddenField ID="HlblWMmemberid" runat="server" />
        <asp:HiddenField ID="HlblWalkMarathonID" runat="server" />
         <asp:HiddenField ID="hdnEventID" runat="server" />
        </div>
    </form>
</body>
</html>
