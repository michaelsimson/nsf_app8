﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using System.Collections;
using VRegistration;
using System.Data.OleDb;

using NativeExcel;

public partial class VocabExcel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btnUpload_Click(object sender, EventArgs e)
    //{
    //    //Coneection String by default empty  
    //    string ConStr = "";
    //    //Extantion of the file upload control saving into ext because   
    //    //there are two types of extation .xls and .xlsx of Excel   
    //    string ext = Path.GetExtension(FileUpload1.FileName).ToLower();
    //    //getting the path of the file   
    //    string path = Server.MapPath("~/Uploads/" + FileUpload1.FileName);
    //    //saving the file inside the MyFolder of the server  
    //    FileUpload1.SaveAs(path);
    //    Label1.Text = FileUpload1.FileName + "\'s Data showing into the GridView";
    //    //checking that extantion is .xls or .xlsx  
    //    if (ext.Trim() == ".xls")
    //    {
    //        //connection string for that file which extantion is .xls  
    //        ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
    //    }
    //    else if (ext.Trim() == ".xlsx")
    //    {
    //        //connection string for that file which extantion is .xlsx  
    //        ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
    //    }
    //    //making query  
    //    string query = "SELECT * FROM [New$] select * from [Original$]";
    //    //Providing connection  
    //    OleDbConnection conn = new OleDbConnection(ConStr);
    //    //checking that connection state is closed or not if closed the   
    //    //open the connection  
    //    if (conn.State == ConnectionState.Closed)
    //    {
    //        conn.Open();
    //    }
    //    //create command object  
    //    OleDbCommand cmd = new OleDbCommand(query, conn);
    //    // create a data adapter and get the data into dataadapter  
    //    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
    //    DataSet ds = new DataSet();
    //    //fill the Excel data to data set  
    //    da.Fill(ds);
    //    //set data source of the grid view  
    //    gvExcelFile.DataSource = ds.Tables[0];
    //    //binding the gridview  
    //    gvExcelFile.DataBind();
    //    //close the connection  
    //    conn.Close();
    //}


    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //if File is not selected then return  
        string ConStr = "";
        //Extantion of the file upload control saving into ext because   
        //there are two types of extation .xls and .xlsx of Excel   
        string ext = Path.GetExtension(FileUpload1.FileName).ToLower();
        //getting the path of the file   
        string path = Server.MapPath("~/Uploads/" + FileUpload1.FileName);
        //saving the file inside the MyFolder of the server  
        FileUpload1.SaveAs(path);
        Label1.Text = FileUpload1.FileName + "\'s Data showing into the GridView";
        //checking that extantion is .xls or .xlsx  
        if (ext.Trim() == ".xls")
        {
            //connection string for that file which extantion is .xls  
            ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
        }
        else if (ext.Trim() == ".xlsx")
        {
            //connection string for that file which extantion is .xlsx  
            ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
        }

        BindData(ConStr);

        //Delete the excel file from the server  
        File.Delete(path);
    }

    private void BindData(string strConn)
    {
        OleDbConnection objConn = new OleDbConnection(strConn);
        objConn.Open();

        // Get the data table containg the schema guid.  
        DataTable dt = null;
        dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        objConn.Close();

        if (dt.Rows.Count > 0)
        {
            int i = 0;

            // Bind the sheets to the Grids  
            //foreach (DataRow row in dt.Rows)
            //{
            DataTable dtNew = null;
            DataTable dtOriginal = null;
            dtNew = getSheetData(strConn, "New$");


            dtOriginal = getSheetData(strConn, "Original$");

            string cmdtext = "";
            cmdtext += " drop table Tmp_VocabExcel; CREATE TABLE Tmp_VocabExcel(Source varchar(500),Level varchar(500),Sublevel varchar(100), Word varchar(max), Type varchar(500), Action varchar(500), Reason varchar(Max), PartsOfSpeech varchar(500), A varchar(500), B varchar(500), C varchar(500), D varchar(500), E varchar(500), Answer varchar(Max), AnswerText varchar(Max), Meaning varchar(Max)); ";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            SqlCommand cmd;
            try
            {
                foreach (DataRow dr in dtOriginal.Rows)
                {
                    cmdtext = "insert into Tmp_VocabExcel (Source, Level, Sublevel, Word, Type, Action, Reason, PartsOfSpeech,A, B, C, D, E,Answer, AnswerText, Meaning) values(@Source,@Level,@Sublevel,@Word,@Type,@Action,@Reason,@PartsOfSpeech,@A,@B,@C,@D,@E,@Answer,@AnswerText,@Meaning);";
                    cmd = new SqlCommand(cmdtext);
                    cmd.Parameters.AddWithValue("@Source", dr["Source"].ToString());
                    cmd.Parameters.AddWithValue("@Level", dr["Level"].ToString());
                    cmd.Parameters.AddWithValue("@Sublevel", dr["Sub-level"].ToString());
                    cmd.Parameters.AddWithValue("@Word", dr["Word"].ToString());
                    cmd.Parameters.AddWithValue("@Type", dr["Type"].ToString());
                    cmd.Parameters.AddWithValue("@Action", dr["Action"].ToString());
                    cmd.Parameters.AddWithValue("@Reason", dr["Reason"].ToString());
                    cmd.Parameters.AddWithValue("@PartsOfSpeech", dr["Parts Of Speech"].ToString());
                    cmd.Parameters.AddWithValue("@A", dr["A"].ToString());
                    cmd.Parameters.AddWithValue("@B", dr["B"].ToString());
                    cmd.Parameters.AddWithValue("@C", dr["C"].ToString());
                    cmd.Parameters.AddWithValue("@D", dr["D"].ToString());
                    cmd.Parameters.AddWithValue("@E", dr["E"].ToString());
                    cmd.Parameters.AddWithValue("@Answer", dr["Answer"].ToString());
                    cmd.Parameters.AddWithValue("@AnswerText", dr["Answer_Text"].ToString());
                    cmd.Parameters.AddWithValue("@Meaning", dr["Meaning"].ToString());

                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {

                    }
                }
            }
            catch
            {

            }
            try
            {
                foreach (DataRow dr in dtNew.Rows)
                {

                    cmdtext = "insert into Tmp_VocabExcel (Source, Level, Sublevel, Word, Type, Action, Reason, PartsOfSpeech,A, B, C, D, E,Answer, AnswerText, Meaning) values(@Source,@Level,@Sublevel,@Word,@Type,@Action,@Reason,@PartsOfSpeech,@A,@B,@C,@D,@E,@Answer,@AnswerText,@Meaning);";
                    cmd = new SqlCommand(cmdtext);
                    cmd.Parameters.AddWithValue("@Source", dr["Source"].ToString());
                    cmd.Parameters.AddWithValue("@Level", dr["Level"].ToString());
                    cmd.Parameters.AddWithValue("@Sublevel", dr["Sub-level"].ToString());
                    cmd.Parameters.AddWithValue("@Word", dr["Word"].ToString());
                    cmd.Parameters.AddWithValue("@Type", dr["Type"].ToString());
                    cmd.Parameters.AddWithValue("@Action", dr["Action"].ToString());
                    cmd.Parameters.AddWithValue("@Reason", dr["Reason"].ToString());
                    cmd.Parameters.AddWithValue("@PartsOfSpeech", dr["Parts Of Speech"].ToString());
                    cmd.Parameters.AddWithValue("@A", dr["A"].ToString());
                    cmd.Parameters.AddWithValue("@B", dr["B"].ToString());
                    cmd.Parameters.AddWithValue("@C", dr["C"].ToString());
                    cmd.Parameters.AddWithValue("@D", dr["D"].ToString());
                    cmd.Parameters.AddWithValue("@E", dr["E"].ToString());
                    cmd.Parameters.AddWithValue("@Answer", dr["Answer"].ToString());
                    cmd.Parameters.AddWithValue("@AnswerText", dr["Answer_Text"].ToString());
                    cmd.Parameters.AddWithValue("@Meaning", dr["Meaning"].ToString());

                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {

                    }
                }
                BtnExport.Visible = true;
                Label1.Text = "Excel file imported successfully. Please click on the Export button.";
            }
            catch
            {

            }
            //  SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            //   CompareData(dtNew, dtOriginal);




            // dt_sheet = getSheetData(strConn, row["TABLE_NAME"].ToString());

            //}
        }
    }
    private DataTable getSheetData(string strConn, string sheet)
    {
        string query = "select * from [" + sheet + "] where Source <>''";
        OleDbConnection objConn;
        OleDbDataAdapter oleDA;
        DataTable dt = new DataTable();
        objConn = new OleDbConnection(strConn);
        objConn.Open();
        oleDA = new OleDbDataAdapter(query, objConn);
        oleDA.Fill(dt);
        objConn.Close();
        oleDA.Dispose();
        objConn.Dispose();
        return dt;
    }

    public void CompareData()
    {
        string CmdText = "select * from Tmp_VocabExcel order by Word, Type DEsc";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
            IWorksheet oSheet = default(IWorksheet);

            oSheet = oWorkbooks.Worksheets.Add();

            oSheet.Name = "Vocab Excel";
            oSheet.Range["A1:AA1"].MergeCells = true;
            oSheet.Range["A1"].Value = "Vocab Excel";
            oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            oSheet.Range["A1"].Font.Bold = true;
            oSheet.Range["A1"].Font.Color = Color.Black;


            oSheet.Range["A3"].Value = "Ser#";
            oSheet.Range["A3"].Font.Bold = true;



            oSheet.Range["B3"].Value = "Source";
            oSheet.Range["B3"].Font.Bold = true;

            oSheet.Range["C3"].Value = "Level";
            oSheet.Range["C3"].Font.Bold = true;

            oSheet.Range["D3"].Value = "Sub-Level";
            oSheet.Range["D3"].Font.Bold = true;

            oSheet.Range["E3"].Value = "Word";
            oSheet.Range["E3"].Font.Bold = true;

            oSheet.Range["F3"].Value = "Type";
            oSheet.Range["F3"].Font.Bold = true;

            oSheet.Range["G3"].Value = "Action";
            oSheet.Range["G3"].Font.Bold = true;

            oSheet.Range["H3"].Value = "Reason";
            oSheet.Range["H3"].Font.Bold = true;

            oSheet.Range["I3"].Value = "Parts Of Speech";
            oSheet.Range["I3"].Font.Bold = true;

            oSheet.Range["J3"].Value = "A";
            oSheet.Range["J3"].Font.Bold = true;

            oSheet.Range["K3"].Value = "B";
            oSheet.Range["K3"].Font.Bold = true;

            oSheet.Range["L3"].Value = "C";
            oSheet.Range["L3"].Font.Bold = true;

            oSheet.Range["M3"].Value = "D";
            oSheet.Range["M3"].Font.Bold = true;

            oSheet.Range["N3"].Value = "E";
            oSheet.Range["N3"].Font.Bold = true;

            oSheet.Range["O3"].Value = "Answer";
            oSheet.Range["O3"].Font.Bold = true;

            oSheet.Range["P3"].Value = "Answer_Text";
            oSheet.Range["P3"].Font.Bold = true;

            oSheet.Range["Q3"].Value = "Meaning";
            oSheet.Range["Q3"].Font.Bold = true;



            int iRowIndex = 4;
            IRange CRange = default(IRange);
            int i = 0;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string Action = string.Empty;
                if (dr["Type"].ToString() == "New")
                {
                    if (dr["Source"].ToString() != ds.Tables[0].Rows[i - 1]["Source"].ToString())
                    {
                        oSheet.Range["B" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Level"].ToString() != ds.Tables[0].Rows[i - 1]["Level"].ToString())
                    {
                        oSheet.Range["C" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["SubLevel"].ToString() != ds.Tables[0].Rows[i - 1]["SubLevel"].ToString())
                    {
                        oSheet.Range["D" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Word"].ToString() != ds.Tables[0].Rows[i - 1]["Word"].ToString())
                    {
                        oSheet.Range["E" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Type"].ToString() != ds.Tables[0].Rows[i - 1]["Type"].ToString())
                    {
                        oSheet.Range["F" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Action"].ToString() != ds.Tables[0].Rows[i - 1]["Action"].ToString())
                    {
                        oSheet.Range["G" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Reason"].ToString() != ds.Tables[0].Rows[i - 1]["Reason"].ToString())
                    {
                        oSheet.Range["H" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["PartsOfSpeech"].ToString() != ds.Tables[0].Rows[i - 1]["PartsOfSpeech"].ToString())
                    {
                        oSheet.Range["I" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["A"].ToString() != ds.Tables[0].Rows[i - 1]["A"].ToString())
                    {
                        oSheet.Range["J" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["B"].ToString() != ds.Tables[0].Rows[i - 1]["B"].ToString())
                    {
                        oSheet.Range["K" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["C"].ToString() != ds.Tables[0].Rows[i - 1]["C"].ToString())
                    {
                        oSheet.Range["L" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["D"].ToString() != ds.Tables[0].Rows[i - 1]["D"].ToString())
                    {
                        oSheet.Range["M" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["E"].ToString() != ds.Tables[0].Rows[i - 1]["E"].ToString())
                    {
                        oSheet.Range["N" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Answer"].ToString() != ds.Tables[0].Rows[i - 1]["Answer"].ToString())
                    {
                        oSheet.Range["O" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["AnswerText"].ToString() != ds.Tables[0].Rows[i - 1]["AnswerText"].ToString())
                    {
                        oSheet.Range["P" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                    if (dr["Meaning"].ToString() != ds.Tables[0].Rows[i - 1]["Meaning"].ToString())
                    {
                        oSheet.Range["Q" + iRowIndex.ToString()].Interior.Color = Color.Yellow;
                        Action = "Update";
                    }
                }

                CRange = oSheet.Range["A" + iRowIndex.ToString()];
                CRange.Value = i + 1;

                CRange = oSheet.Range["B" + iRowIndex.ToString()];
                CRange.Value = dr["Source"].ToString();

                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                CRange.Value = Convert.ToInt32(dr["Level"].ToString().Replace("&nbsp;", " "));

                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                CRange.Value = Convert.ToInt32(dr["SubLevel"].ToString().Replace("&nbsp;", " "));

                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                CRange.Value = dr["Word"].ToString();

                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                CRange.Value = dr["Type"].ToString();

                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                CRange.Value = (dr["Action"].ToString() == "Delete" ? "Delete" : Action);

                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                CRange.Value = dr["Reason"].ToString();

                CRange = oSheet.Range["I" + iRowIndex.ToString()];
                CRange.Value = dr["PartsOfSpeech"].ToString();

                CRange = oSheet.Range["J" + iRowIndex.ToString()];
                CRange.Value = dr["A"].ToString();

                CRange = oSheet.Range["K" + iRowIndex.ToString()];
                CRange.Value = dr["B"].ToString();

                CRange = oSheet.Range["L" + iRowIndex.ToString()];
                CRange.Value = dr["C"].ToString();

                CRange = oSheet.Range["M" + iRowIndex.ToString()];
                CRange.Value = dr["D"].ToString();

                CRange = oSheet.Range["N" + iRowIndex.ToString()];
                CRange.Value = dr["E"].ToString();

                CRange = oSheet.Range["O" + iRowIndex.ToString()];
                CRange.Value = dr["Answer"].ToString();

                CRange = oSheet.Range["P" + iRowIndex.ToString()];
                CRange.Value = dr["AnswerText"].ToString();

                CRange = oSheet.Range["Q" + iRowIndex.ToString()];
                CRange.Value = dr["Meaning"].ToString();


                iRowIndex = iRowIndex + 1;
                i = i + 1;
            }

            DateTime dt = DateTime.Now;
            string month = dt.ToString("MMM");
            string day = dt.ToString("dd");
            string year = dt.ToString("yyyy");
            string monthDay = month + "" + day;

            string filename = "VoacbExcel" + "_" + monthDay + "_" + year + ".xls";

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
            oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
            Response.End();
        }
    }
    protected void BtnExport_Click(object sender, EventArgs e)
    {
        CompareData();
    }
}