Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class ContRegSearch
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)

            drpyear.Items.Add(DateTime.Now.Year.ToString())
            drpyear.Items.Add(Convert.ToString(year - 1))
            drpyear.Items.Add(Convert.ToString(year - 2))
            drpyear.Items.Add(Convert.ToString(year - 3))
            drpyear.Items.Add(Convert.ToString(year - 4))
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If (txtpname.Text = "" And txtemail.Text = "" And txtphone.Text = "" And txtname.Text = "" And txtsname.Text = "") Then
            lblerror.Text = "Please Enter atleast one search box"
        Else

            Dim ParentName As String = txtpname.Text
            Dim email As String = txtemail.Text
            Dim contestantname As String = txtname.Text
            Dim phone As String = txtphone.Text
            Dim contestyear As Integer = drpyear.SelectedItem.Value
            Dim SpouseName As String = txtsname.Text
            Session("ParentName") = ParentName
            Session("email") = email
            Session("contestantname") = contestantname
            Session("phone") = phone
            Session("contestyear") = contestyear
            Session("spousename") = SpouseName

            Response.Redirect("ContRegSearchResult.aspx")
        End If
    End Sub
End Class
