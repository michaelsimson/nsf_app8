﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class DonVolRecAwardsList : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            Yearscount(FromYear);
            Yearscount(ToYear);
        }
    }
    protected void Yearscount(DropDownList DDYear)
    {
        try
        {
            DDYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = 1993; i <= DateTime.Now.Year; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DDYear.DataSource = list;
            DDYear.DataTextField = "Text";
            DDYear.DataValueField = "Value";
            DDYear.DataBind();
            DDYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //SessionExp();
        }

    }
    
    protected void ReportQuery(string StrRectype,GridView GridData)
    {
        string QueryVal = "select D.[Year],case when D.Spouse='Y' and I.Gender='Male' then (I2.FirstName+' and '+I.FirstName+' '+I.LastName) when D.Spouse='Y' and I.Gender='female'  then (I.FirstName+' and '+I2.FirstName+' '+I2.LastName)"+
        " when  D.Anon='Y' then 'Anonymous' else (I.FirstName+' '+I.LastName) end as Name,D.AwardsType,case when D.Anon='Y' then  'Anonymous'  else I.City end as City,I.State,case when D.Anon='Y' then  'Anonymous'  else D.Chapter end as Chapter " +
        "from DonVolAwards  D  left join IndSpouse I on I.AutoMemberID=D.MemberID LEFT JOIN IndSpouse I2 ON ((I.DonorType='IND' AND I.AutoMemberID =   I2.Relationship) OR (I.DonorType='SPOUSE' AND  I.Relationship = I2.AutoMemberID)) where " +
        " D.RecType='" + StrRectype + "' and  Year  between " + FromYear.SelectedValue + " and " + ToYear.SelectedValue + " order by D.year desc,D.AwardsType,I.LastName,I.FirstName ";

        DataSet dsQuery = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, QueryVal);
     
        if (dsQuery.Tables[0].Rows.Count > 0)
        {
            lblError.Visible = false;
            lblError.Text = "";
            GridData.DataSource = dsQuery.Tables[0];
            GridData.DataBind();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnsearchChild0_Click1(object sender, EventArgs e)
    {
        if ((FromYear.SelectedItem.Text != "Select Year") && (ToYear.SelectedItem.Text != "Select Year"))
        {
            if (Convert.ToInt32(ToYear.SelectedValue) >= Convert.ToInt32(FromYear.SelectedValue))
            {
                lblError.Text = "";
                lblError.Visible = false;
                ReportQuery("Donor", GridDonor);
                ReportQuery("Volunteer", GridVOlunteer);
                if ((GridDonor.Rows.Count > 0) || (GridVOlunteer.Rows.Count > 0))
                {
                    lblError.Visible = false;
                    lblError.Text ="";
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Donor/VolunteerAwards List.doc");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-word ";
                    StringWriter sw = new StringWriter();

                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                  
                    if (GridDonor.Rows.Count > 0)
                    {
                        Label1.Text = "Table 1: Donor Recognition Awards";
                    TblDonorName.RenderControl(hw);
                    }
                    Label1.Visible = true;
                    GridDonor.DataBind();
                    GridDonor.RenderControl(hw);
                     if (GridVOlunteer.Rows.Count > 0)
                     {

                         Label2.Text = "Table 2: Volunteer Recognition Awards";
                    TblVolunteer.RenderControl(hw);
                     }
                    Label2.Visible = true;
                    GridVOlunteer.DataBind();
                    GridVOlunteer.RenderControl(hw);
                    //TblBorder.RenderControl(hw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Records not found";
                  
                }
            }
            else
            {
                Response.Write("<script>alert('To year should be greater than From Year')</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('Please Select From year and To Year')</script>");
        }
       
    }
    protected void LoadGrid()
    {
        if ((FromYear.SelectedItem.Text != "Select Year") && (ToYear.SelectedItem.Text != "Select Year"))
        {
            if (Convert.ToInt32(ToYear.SelectedValue) >= Convert.ToInt32(FromYear.SelectedValue))
            {
                lblError.Text = "";
                lblError.Visible = false;
                ReportQuery("Donor", GridDonor);
                ReportQuery("Volunteer", GridVOlunteer);
                if ((GridDonor.Rows.Count > 0) || (GridVOlunteer.Rows.Count > 0))
                {
                    lblError.Visible = false;
                    lblError.Text = "";
                    if (GridDonor.Rows.Count > 0)
                    {
                        Label1.Text = "Table 1: Donor Recognition Awards";
                        GridDonor.Visible = true;
                    }
                    else
                    {
                        LablnoDonor.Text = "No Records found";
                        LablnoDonor.Visible = true;
                    }

                    GridDonor.DataBind();

                    if (GridVOlunteer.Rows.Count > 0)
                    {
                        Label2.Text = "Table 2: Volunteer Recognition Awards";
                        GridVOlunteer.Visible = true;
                    }
                    else
                    {
                        lbnovol.Text = "No Records found";
                        lbnovol.Visible = true;
                    }

                    GridVOlunteer.DataBind();

                    //TblBorder.RenderControl(hw);

                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Records not found";

                }
            }
            else
            {
                Response.Write("<script>alert('To year should be greater than From Year')</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('Please Select From year and To Year')</script>");
        }
       
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        LoadGrid();

    }
}