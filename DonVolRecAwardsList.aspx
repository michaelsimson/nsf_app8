﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="DonVolRecAwardsList.aspx.cs" Inherits="DonVolRecAwardsList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>

 <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Donor/Volunteer Awards List
    </strong>
             <br />
        <br />
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblFrmYear" runat="server" Text="From Year"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="FromYear" runat="server" Width="150px">
    </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                 <asp:Label ID="LbToyear" runat="server" Text="To Year"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="ToYear" runat="server" Width="150px">
    </asp:DropDownList>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" 
         />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnsearchChild0" runat="server" Text="Generate Report" 
        OnClick="btnsearchChild0_Click1"  />
        <br />
        <b> <asp:Label ID="lblError" runat="server"  ForeColor="Red"   Text="" Visible="false"></asp:Label></b>
        <br />
        <br />
    <table runat="server" id="TblDonorName" align="center" ><tr><td style="width: 55px"></td><td> <b> <asp:Label ID="Label1" runat="server"  Font-Size="Large"   Font-Bold="true" Text=""></asp:Label></b></td></tr>
    <tr><td></td></tr>
     <tr><td></td></tr>
    </table>
  
    <div>
                      <asp:GridView ID="GridDonor" runat="server" align="center"    
                EnableViewState="true"
                >
           
            </asp:GridView>
                  </div>


                      <table runat="server" id="Table1" align="center" ><tr><td style="width: 55px"></td><td> <b> <asp:Label ID="LablnoDonor" runat="server"  ForeColor="Red"   Text="" Visible="false"></asp:Label></b></td></tr>
    <tr><td></td></tr>
     <tr><td></td></tr>
    </table>

                  
                  <br />
                  <table runat="server" id="TblVolunteer" align="center">  <tr><td></td></tr><tr><td style="width: 80px"></td><td> <b> <asp:Label ID="Label2" Font-Size="Large" Font-Bold="true"   runat="server" Text=""></asp:Label></b></td></tr>
                  <tr><td></td></tr>
                   <tr><td></td></tr>
                  </table>
                    <div>
                      <asp:GridView ID="GridVOlunteer" runat="server" align="center"    
                EnableViewState="true" 
                >
           
            </asp:GridView>
                  </div>


                  
                      <table runat="server" id="Table2" align="center" ><tr><td style="width: 55px"></td><td>  <b> <asp:Label ID="lbnovol" runat="server"  ForeColor="Red"   Text="" Visible="false"></asp:Label></b></td></tr>
    <tr><td></td></tr>
     <tr><td></td></tr>
    </table>
                   
</asp:Content>

