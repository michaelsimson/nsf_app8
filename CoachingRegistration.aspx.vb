Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System
Imports System.Net.Mail

Partial Class CoachingRegistration
    Inherits System.Web.UI.Page
    Dim yr As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        Page.MaintainScrollPositionOnPostBack = True
        yr = Session("EventYear")
        btnYear.Text = Session("CoachSeasonHeader")
        If Not Page.IsPostBack Then
            Session("Checklog") = 0
            Session("RegFee") = Nothing
            Session("MealCharges") = Nothing
            Session("LATEFEE") = Nothing
            Session("SaleAmt") = Nothing
            Session("FundRAmt") = Nothing
            Session("Donation") = Nothing
            Session("Discount") = Nothing
            Loadchild(Session("CustIndID"))
            LoadSelectedCoaching()
            If Not Session("Discount") Is Nothing Then
                If (Session("Discount") - Session("TotalFee")) = 0 Or Session("UpdateRegInChildAccess") = "true" Then
                    btnDonate.Visible = False
                End If
            End If

            If Session("HasAdult") = "Y" Then 
                If Session("HasAdultChild") = "Y" Then
                Else
                    If ddlProduct.Items.Count = 2 Then
                        ddlProduct.SelectedIndex = 1
                        ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
                        ddlProduct.Enabled = False
                        If ddlProductLevel.Items.Count > 0 Then
                            Dim i As Integer = 0
                            Dim ind As Integer = 0, highVal As Integer = -1
                            For i = 0 To ddlProductLevel.Items.Count - 1
                                If (highVal < ddlProductLevel.Items(i).Value) Then
                                    highVal = ddlProductLevel.Items(i).Value
                                    ind = i
                                End If
                            Next
                            ddlProductLevel.SelectedIndex = ind
                            ddlProductLevel_SelectedIndexChanged(ddlProductLevel, New EventArgs)
                            ddlProductLevel.Enabled = False
                        End If
                    End If
                End If
            End If
        End If
        If HlblBtnDonate.Text = "Y" Then
            btnDonate.Visible = True
        End If
        If Session("UpdateRegInChildAccess") = "true" Or Session("FullWaive") = "true" Then
            btnDonate.Visible = False
        End If
        lblNote.Text = " Note:Product Level Pricing flag 'N' Calculates the Reg fee only once for the Product group as a whole. <br />"
        If Session("HasAdult") = "Y" Then
            lblChildLabel.Text = "Parent"
            If Session("HasAdultChild") = "Y" Then
                lblChildLabel.Text = "Parent/Child"
            End If
        End If
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        conn.Open()

        'Dim strSql As String = " Select First_name+' '+Last_name as Name, cast(ChildNumber as varchar) ChildNumber  "
        'strSql = strSql + "From dbo.Child Where  memberid=" & ParentID & " and (  "
        'strSql = strSql + " select count(*) from EventFees EF inner join Event E on EF.EventId=E.eventId and EF.EventYear=E.EventYear and EF.GradeFrom<>13  "
        'strSql = strSql + " and EF.Semester=E.Semester inner join Product P on P.ProductId=EF.ProductId and P.Status='O' and P.EventId=13  where EF.Eventid=13 and E.Status='O') >0"

        'strSql = strSql + " union "
        'strSql = strSql + " select FirstName+' '+LastName as Name, cast(Automemberid as varchar) +'A' ChildNumber    "
        'strSql = strSql + " from indspouse Where (automemberid=" & ParentID & " or relationship=" & ParentID & "  ) and   "
        'strSql = strSql + " (  "
        'strSql = strSql + " select count(*) from EventFees EF inner join Event E on EF.EventId=E.eventId and EF.EventYear=E.EventYear and EF.Adult='Y'  "
        'strSql = strSql + " and EF.Semester=E.Semester inner join Product P on P.ProductId=EF.ProductId and P.Status='O' and P.EventId=13  where EF.Eventid=13 and E.Status='O') >0    "
 
        'Dim strSql As String = "select First_name+' '+Last_name as Name, cast(ChildNumber as varchar) ChildNumber From Child where memberid=" & ParentID
        'If Session("HasAdult") = "Y" Then
        '    strSql = strSql + " UNION select FirstName+' '+LastName as Name, cast(Automemberid as varchar) +'A' ChildNumber from IndSpouse where Automemberid in (" & Session("IndID") & "," & Session("SpouseID") & ")"
        '                End If
        Dim drChild As SqlDataReader
        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter("@MemberID", Session("IndID"))
        prmArray(1) = New SqlParameter("@Year", Session("EventYear"))
        drChild = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetParentChildInfo", prmArray)
        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        ddlChild.Enabled = True
        conn.Close()
        If ddlChild.SelectedValue.Contains("A") Then
            ddlType.SelectedIndex = 0 'ddlType.Items.IndexOf(ddlType.Items.FindByText("Parent"))
        Else
            ddlType.SelectedIndex = 1
        End If
        LoadProduct()
        LoadGrade()
    End Sub

    Private Sub LoadCoaching()
        lblerr.Text = ""
        TrDeadlineDate.Visible = False
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If Not ddlProduct.SelectedIndex = 0 Then
            conn.Open()
            If SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(CR.CoachRegID) from CoachReg CR  Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo inner join EventFees EF on EF.ProductId=C.Productid and EF.EventYear=C.EventYear and EF.EventId=13 and EF.Semester=C.Semester where " & IIf(ddlChild.SelectedValue.Contains("A"), " CR.AdultId = " & ddlChild.SelectedValue.Replace("A", ""), " CR.ChildNumber=" & ddlChild.SelectedValue) & " and CR.ProductID = " & ddlProduct.SelectedValue & "  and CR.EventYear=" & yr) < 1 Then
                Dim SQLStr As String
                SQLStr = " SELECT SignUpid,productname,RegFee,LateFee,ProductLevelPricing, CoachName,Level, MaxCapacity,Day,[Time], StartDate,EndDate,convert(varchar(20),LateRegDLDate) LateRegDLDate,City, State, LevelId,"
                SQLStr = SQLStr & " TimeZone,Email,Duration,ApprovedCount, ProductId,LastName,FirstName  from ("
                SQLStr = SQLStr & " select C.SignUpID,P.Name as ProductName,EF.RegFee,Case WHEN GETDATE() > dbo.ufn_GetDeadLine(EF.DeadlineDate) THEN  EF.LateFee ELSE '' End as LateFee,EF.ProductLevelPricing,I.FirstName + ' ' + I.LastName as CoachName,"
                SQLStr = SQLStr & "C.Level,"
                SQLStr = SQLStr & "C.MaxCapacity,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) + ' EST' as Time,C.StartDate,C.Enddate,EF.LateRegDLDate,I.City,I.State,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone,I.Email"
                SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END + ' hr(s)' as Duration, "
                SQLStr = SQLStr & "(select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level AND CR1.EventYear=C1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,C.ProductId,I.LastName,I.FirstName "
                If ddlChild.SelectedValue.Contains("A") Then
                    SQLStr = SQLStr & " from IndSpouse Ad"
                    SQLStr = SQLStr & " Inner Join EventFees EF ON EF.EventID=" & Session("EventID") & " and Ef.EventYear >=" & yr & " and GETDATE()<DateAdd(dd, 1, EF.LateRegDLDate)"
                    SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
                    SQLStr = SQLStr & " Inner Join CalSignUp C ON C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
                    SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                    SQLStr = SQLStr & " where Ad.AutoMemberId = " & ddlChild.SelectedValue.Replace("A", "") & " and C.Accepted='Y' "
                Else
                    SQLStr = SQLStr & " from  Child Ch"
                    SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear >=" & yr & " and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)"
                    SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
                    SQLStr = SQLStr & " Inner Join CalSignUp C  ON  C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
                    SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                    SQLStr = SQLStr & " Left Join ChildCoachLevel CL ON  CL.Level =C.Level and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'"
                    SQLStr = SQLStr & " where ch.ChildNumber = " & ddlChild.SelectedValue & " and C.Accepted='Y' and C.Confirm='Y' and (Ch.GRADE Between EF.GradeFrom and EF.GradeTo OR P.ProductGroupCode='UV')"
                End If

                Dim strProd As String = "", strCond As String = ""
                Try
                    Select Case Convert.ToInt32(ddlProduct.SelectedValue.ToString())
                        Case 106 To 107
                            Select Case Convert.ToInt32(ddlGrade.SelectedValue.ToString())
                                Case 6 To 8
                                    strProd = strProd & " and C.Level in ('Junior')"
                                Case Else
                                    strProd = strProd & " and C.Level in ('Senior')"
                            End Select
                        Case 109 To 111
                            Select Case Convert.ToInt32(ddlGrade.SelectedValue.ToString())
                                Case 1 To 3
                                    strProd = strProd & " and C.Level in ('Junior')"
                                Case 4 To 8
                                    strProd = strProd & " and C.Level in ('Intermediate')"
                                Case 9 To 12
                                    strProd = strProd & " and C.Level in ('Senior')"
                                Case Else
                            End Select
                        Case Else
                            strProd = strProd & " and C.Level in ('" & ddlProductLevel.SelectedItem.Text & "')"
                    End Select
                Catch ex As Exception
                End Try
                strCond = " AND ((EF.CoachSelCriteria IS NULL OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' "
                If ddlChild.SelectedValue.Contains("A") = False Then
                    strCond = strCond & " AND CL.Eligibility IS NOT NULL"
                End If
                strCond = strCond & ")) "

                SQLStr = SQLStr & strProd & strCond
                Dim sqlEx As String = " select C.SignUpID,P.Name as ProductName,EF.RegFee,Case WHEN GETDATE() > dbo.ufn_GetDeadLine(EF.DeadlineDate)  THEN  EF.LateFee ELSE '' End as LateFee,EF.ProductLevelPricing, "
                sqlEx = sqlEx & " I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) + ' EST' as Time,C.StartDate,C.Enddate, "
                sqlEx = sqlEx & " Ex.NewDeadLine,I.City,I.State,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone,I.Email "
                sqlEx = sqlEx & " ,Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case "
                sqlEx = sqlEx & " when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END + ' hr(s)' as Duration, "
                sqlEx = sqlEx & " (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level AND "
                sqlEx = sqlEx & " CR1.EventYear=C1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,C.ProductId,I.LastName,I.FirstName "
                If ddlChild.SelectedValue.Contains("A") Then
                    sqlEx = sqlEx & " from IndSpouse Ad "
                    sqlEx = sqlEx & " inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear>=" & yr
                    sqlEx = sqlEx & " Inner Join Product P ON P.ProductId = EF.ProductID "
                    sqlEx = sqlEx & " Inner Join CalSignUp C  ON  C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
                    sqlEx = sqlEx & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                    sqlEx = sqlEx & " Inner JOIN EXCoaching Ex On Ex.EventId=" & Session("EventID") & " and Ex.ProductID=P.ProductId and Ex.ProductGroupID=P.ProductGroupId and Ex.EventYear=" & yr
                    sqlEx = sqlEx & " and Ex.AdultId= Ad.AutoMemberId and Ex.CMemberId=I.AutoMemberId and GETDATE() < dbo.ufn_GetDeadLine(Ex.NewDeadLine)  "
                    ' sqlEx = sqlEx & " Left Join ChildCoachLevel CL ON  CL.Level =C.Level and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y' "
                    sqlEx = sqlEx & " where Ad.AutoMemberId = " & ddlChild.SelectedValue.Replace("A", "") & " and C.Accepted='Y'   and C.MemberID=Ex.CMemberId "

                Else
                    sqlEx = sqlEx & " from  Child Ch "
                    sqlEx = sqlEx & " inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear>=" & yr
                    sqlEx = sqlEx & " Inner Join Product P ON P.ProductId = EF.ProductID "
                    sqlEx = sqlEx & " Inner Join CalSignUp C  ON  C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
                    sqlEx = sqlEx & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                    sqlEx = sqlEx & " Inner JOIN EXCoaching Ex On Ex.EventId=" & Session("EventID") & " and Ex.ProductID=P.ProductId and Ex.ProductGroupID=P.ProductGroupId and Ex.EventYear=" & yr
                    sqlEx = sqlEx & " and Ex.ChildNumber= ch.ChildNumber and Ex.CMemberId=I.AutoMemberId and GETDATE() < dbo.ufn_GetDeadLine(Ex.NewDeadLine)  "
                    sqlEx = sqlEx & " Left Join ChildCoachLevel CL ON  CL.Level =C.Level and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y' "
                    sqlEx = sqlEx & " where ch.ChildNumber =  " & ddlChild.SelectedValue & " and C.Accepted='Y' and C.Confirm='Y' and (Ch.GRADE Between EF.GradeFrom and EF.GradeTo OR P.ProductGroupCode='UV') and C.MemberID=Ex.CMemberId "

                End If

                sqlEx = sqlEx & strProd & strCond

                SQLStr = SQLStr & " UNION " & sqlEx
                SQLStr = SQLStr & " ) CoachTest Order BY ProductId,LevelID,CASE Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end,CONVERT(VARCHAR(10),TIME), LastName, FirstName"

                Dim drCoaching As SqlDataReader
                Try
                    drCoaching = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
                Catch ex As Exception

                End Try
                dgCoachSelection.DataSource = drCoaching
                dgCoachSelection.DataBind()

                conn.Close()
                pnlCoachSelection.Visible = True
                If dgCoachSelection.Items.Count > 0 Then
                    btnSubmit.Visible = True
                    dgCoachSelection.Visible = True
                    lblCoachSelection.Text = ""
                Else
                    dgCoachSelection.Visible = False
                    lblCoachSelection.ForeColor = Color.Red
                    lblCoachSelection.Text = "No Eligible Coaching session Available"
                    btnSubmit.Visible = False
                End If
            Else
                If IsSubmitClicked = False Then
                    lblerr.Text = "Sorry, you had selected this product already"
                    pnlCoachSelection.Visible = False
                    'Validate product level pricing
                    ChkAllProductsRegistered()
                End If
            End If
            Dim dsEvent As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select isnull(convert(varchar, DeadlineDate,101) ,'') DeadlineDate,isnull(convert(varchar,LateRegDLDate,101) ,'') LateRegDLDate From EventFees Where RegOpen='Y' and EventID=" & Session("EventID") & "  and EventYear >=" & yr & " and ProductID= " & ddlProduct.SelectedValue & "")       ' and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)")
            If dsEvent.Tables(0).Rows.Count > 0 Then
                TrDeadlineDate.Visible = True
                'TrLateDLDate.Visible.Visible = True
                lblDLDate.Text = Convert.ToString(dsEvent.Tables(0).Rows(0)("DeadlineDate"))
                lblLateDLDate.Text = Convert.ToString(dsEvent.Tables(0).Rows(0)("LateRegDLDate"))
            Else
                TrDeadlineDate.Visible = False
            End If
            conn.Close()
        Else
            pnlCoachSelection.Visible = False
        End If
        LoadSelectedCoaching()
    End Sub

    Sub ChkAllProductsRegistered()
        Dim SQLChild As String, SQLProdGroup As String
        Dim SQLProductChk, SQLProduct
        Dim conn As New SqlConnection(Application("ConnectionString"))
        SQLChild = SQLChild & "Select Distinct ChildNumber, AdultId, Grade From CoachReg Where PMemberID=" & Session("CustIndID") & " and EventYear=" & yr & " and EventID=" & Session("EventID") '& " and ChildNumber=" & ddlChild.SelectedValue
        conn.Open()
        Dim ds_Child As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLChild)
        Dim missedToRegProduct As String = ""
        Dim registrationREF As String
        If ds_Child.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds_Child.Tables(0).Rows.Count - 1
                If ds_Child.Tables(0).Rows(i)("ChildNumber") Is DBNull.Value Then
                    registrationREF = " CR.AdultId = " & ds_Child.Tables(0).Rows(i)("AdultId")
                Else
                    registrationREF = " CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber")
                End If

                SQLProdGroup = "Select Distinct CR.ProductGroupID,CR.ProductGroupCode from CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID and EF.RegOpen='Y' and EF.ProductLevelPricing ='N' Where CR.PMemberID = " & Session("CustIndID") & " And CR.EventYear = " & yr & " And CR.EventID = " & Session("EventID") & "  And "
                SQLProdGroup = SQLProdGroup & registrationREF & " And Grade =" & ds_Child.Tables(0).Rows(i)("Grade")
                Dim ds_ProdGroup As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLProdGroup)
                If ds_ProdGroup.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To ds_ProdGroup.Tables(0).Rows.Count - 1
                        SQLProduct = "Select Distinct CR.ProductId,CR.ProductCode From CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID  and EF.ProductLevelPricing ='N' and EF.Semester=CR.Semester Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & yr & " and CR.EventID=" & Session("EventID") & " and " & registrationREF & " and Grade= " & ds_Child.Tables(0).Rows(i)("Grade") & " and CR.ProductGroupID=" & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID") & ""
                        SQLProductChk = "Select Distinct P.ProductID,P.ProductCode from Product P Inner Join EventFees EF on EF.ProductGroupId = EF.ProductGroupID  and  P.ProductId=EF.ProductID and Ef.EventID=P.EventID Where EF.EventID= " & Session("EventID") & " and EF.EventYear=" & yr & " and P.Status ='O' and EF.ProductLevelPricing='N' and EF.ProductGroupID = " & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID")
                        Dim dsRegProList As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLProduct)
                        Dim dsReqProList As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLProductChk)
                        Dim isRegistered As Boolean
                        For Each dr As DataRow In dsReqProList.Tables(0).Rows
                            isRegistered = False
                            For Each dr2 As DataRow In dsRegProList.Tables(0).Rows
                                If (dr("ProductCode") = dr2("ProductCode")) Then
                                    isRegistered = True
                                End If
                            Next
                            If isRegistered = False Then
                                If missedToRegProduct.Length = 0 Then
                                    missedToRegProduct = dr("ProductCode")
                                Else
                                    missedToRegProduct = missedToRegProduct & "," & dr("ProductCode")
                                End If
                            End If
                        Next
                    Next
                End If
            Next
        End If
        conn.Close()
        If missedToRegProduct.Length <> 0 Then  'Row_cnt > 0 And Product_Flag = False Then
            lblerr.Text = "Please select " & missedToRegProduct & " class before clicking Pay "
        End If
    End Sub

    Private Sub LoadProductLevel()
        Dim SQLStr As String = ""
        Try
            SQLStr = SQLStr & " Select Distinct LevelId, LevelCode from ProdLevel "
            SQLStr = SQLStr & " where EventID=" & Session("EventID") & " and  EventYear >=" & Session("Year") & " and ProductId=" & ddlProduct.SelectedValue 
            If ddlChild.SelectedValue.Contains("A") = False Then
                '1=case when mandatory is null then 1  else case when 7 between gradefrom and gradeto then 1 else 0 END END
                SQLStr = SQLStr & " AND 1=CASE WHEN mandatory IS NULL THEN 1 ELSE CASE WHEN " & ddlGrade.SelectedValue & " BETWEEN GradeFrom AND GradeTo THEN 1 ELSE 0 END END"
            End If
            SQLStr = SQLStr & " Order by LevelId"
            ddlProductLevel.Enabled = True
            ddlProductLevel.Items.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlProductLevel.DataSource = ds.Tables(0)
                ddlProductLevel.DataBind()
                If ds.Tables(0).Rows.Count = 1 Then
                    ddlProductLevel.Enabled = False
                    ddlProductLevel_SelectedIndexChanged(ddlProductLevel, New EventArgs)
                Else
                    ddlProductLevel.Items.Insert(0, New ListItem("Select Level", -1))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadProduct()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String = ""
        Dim tblEligible() As String = {"EligibleCoach"}
        If ddlChild.SelectedValue.Contains("A") Then
            SQLStr = SQLStr & " Select Distinct P.Name as ProductName,EF.LateRegDLDate,P.Status,P.ProductID from IndSpouse Ad"
            SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & " and Ef.EventYear >=" & Session("Year") & " and EF.Adult='Y'"
            SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
            SQLStr = SQLStr & " Inner Join CalSignUp C ON C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear"
            SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
            SQLStr = SQLStr & " where Ad.AutoMemberId = " & ddlChild.SelectedValue.Replace("A", "")
            SQLStr = SQLStr & " AND ((EF.CoachSelCriteria IS NULL  OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I')) and EF.RegOpen='Y'"
            SQLStr = SQLStr & " Order by P.ProductID"
        Else
            SQLStr = SQLStr & " Select Distinct P.Name as ProductName,EF.LateRegDLDate,P.Status,P.ProductID from Child Ch"
            SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & " and Ef.EventYear >=" & Session("Year")
            SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
            SQLStr = SQLStr & " Inner Join CalSignUp C  ON  C.Semester=EF.Semester and P.ProductId = C.ProductID and EF.EventYear=C.EventYear"
            SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
            SQLStr = SQLStr & " Left Join ChildCoachLevel CL ON CL.Level =C.Level and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'"
            SQLStr = SQLStr & " where ch.ChildNumber = " & ddlChild.SelectedValue & " and (Ch.GRADE Between EF.GradeFrom and Ef.GradeTo or P.ProductGroupCode='UV')"
            SQLStr = SQLStr & " AND ((EF.CoachSelCriteria IS NULL  OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' AND CL.Eligibility IS NOT NULL  )) and EF.RegOpen='Y'"
            SQLStr = SQLStr & " Order by P.ProductID"
        End If


        Dim drProduct As DataTable
        Dim dsInvitees As New DataSet
        Try
            conn.Open()
            SqlHelper.FillDataset(conn, CommandType.Text, SQLStr, dsInvitees, tblEligible)
            drProduct = dsInvitees.Tables(0).Clone()
            conn.Close()

            For Each dr As DataRow In dsInvitees.Tables(0).Rows
                Dim SQLStr1 As String = ""
                If dr.Item("ProductName") = "Universal Values 201" Then
                    'Commented and updated on 11-09-2014  '******************UV201 was taken before: (Criteria to take UV301)*************************'
                    SQLStr1 = "Select Count(CoachRegID) as count from CoachReg where EventYear<" & yr & " and ProductCode = 'UV101' and Approved='Y' and childnumber =  " & ddlChild.SelectedValue & ""
                    conn.Open()
                    Dim drContestCategory As SqlDataReader
                    drContestCategory = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr1)
                    If drContestCategory.Read() Then
                        If drContestCategory("count") = 0 Then
                            dr.Item("ProductName") = ""
                        End If
                    End If
                    conn.Close()
                ElseIf dr.Item("ProductName") = "Universal Values 301" Then
                    'Commented and updated on 11-09-2014  '******************UV101 was taken before: (Criteria to take UV201)*************************'
                    SQLStr1 = " Select Count(CoachRegID) as count from CoachReg where EventYear<" & yr & " and ProductCode = 'UV201' and Approved='Y' and childnumber =  " & ddlChild.SelectedValue & ""
                    conn.Open()
                    Dim drContestCategory As SqlDataReader
                    drContestCategory = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr1)
                    If drContestCategory.Read() Then
                        If drContestCategory("count") = 0 Then
                            dr.Item("ProductName") = ""
                        End If
                    End If
                    conn.Close()
                End If
                If dr.Item("ProductName") <> "" Then
                    drProduct.ImportRow(dr)
                End If
            Next
            conn.Close()
            ddlProduct.DataSource = drProduct
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 0 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
                lblerr.Text = ""
            Else
                lblerr.ForeColor = Color.Red
                lblerr.Text = "No eligible coaching is available. Select another child."
            End If
        Catch ex As Exception
            lblerr.Text = "<br>" & ex.ToString() 'SQLStr & 
        End Try
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
        LoadProduct()
        LoadGrade()
        If ddlChild.SelectedValue.Contains("A") Then
            ddlType.SelectedIndex = 0
        Else
            ddlType.SelectedIndex = 1
        End If
        ddlProductLevel.Items.Clear()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        'load level
        LoadProductLevel()
    End Sub
    Private Sub clear()
        pnlCoachSelection.Visible = False
        ddlProduct.Enabled = False
        TrDeadlineDate.Visible = False
    End Sub
    Private Sub LoadGrade()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        If ddlChild.SelectedValue.Contains("A") Then
            strSql = "select 0 as Grade"
        Else
            strSql = "select Distinct Grade From Child where ChildNumber=" & ddlChild.SelectedValue
        End If

        conn.Open()
        Dim drGrade As SqlDataReader
        drGrade = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlGrade.DataSource = drGrade
        ddlGrade.DataBind()
        ddlGrade.Enabled = False
        conn.Close()
    End Sub
    Private IsSubmitClicked As Boolean = False

    Private Function IsValidNIO(PId As String, ChapterId As String) As Boolean
        Dim strMembers As String = ""
        strMembers = " select Convert(varchar,ind.automemberid) +','+ convert(varchar,case when ind.DonorType='IND' then isnull(sp.automemberid,'') else isnull(ind.relationship,'') end) from indspouse ind"
        strMembers = strMembers & " left join indspouse sp on ind.automemberid=sp.relationship  where ind.automemberid =" & Session("LoginID")
        strMembers = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strMembers)
        Try
            Dim iCOIValid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from IndSpouse where CountryOfOrigin='IN' and AutoMemberId in (" & strMembers & ")")
            If iCOIValid = 0 Then
                ' kk = #of all paid registrations for the particular Year, Product, chapter, Payment reference is not null
                'nn= #of Non-Indian origin paid registrations for particular Year, Product, chapter, Payment reference is not null and COI='N'
                'Limit = max [5, Integer(NIOP*kk/100)], kk=#of all paid registrations  
                Dim TotalPaid As Long = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "Select count(*) from CoachReg where EventYear=" & Session("EventYear") & " and EventId=" & Session("EventId") & " and ProductId=" & PId & " and paymentreference is not null")

                Dim strText As String = "Select count(*) from CoachReg C where EventYear=" & Session("EventYear") & " and EventId=13 and ProductId=" & PId & " and paymentreference is not null and exists(Select * from IndSpouse where CountryOfOrigin <> 'IN' and automemberid =C.PMemberId)"
                strText = strText & " and exists(Select * from IndSpouse where CountryOfOrigin <> 'IN' and relationship =C.PMemberId)"
                Dim TotalNIOPaid As Long = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, strText)

                Dim limit As Decimal = 5
                Dim NIOPVal As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select isnull(NIOP,0) from Chapter where ChapterId='Coaching, US'")
                Dim limitTo As Long = Convert.ToInt64(NIOPVal * (TotalPaid / 100))
                If (limit < limitTo) Then
                    limit = limitTo
                End If
                If TotalNIOPaid >= limit Then
                    Return False
                End If
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
        Return True
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim intCtr As Integer
            lblNotReg.Text = "Unable To Register for the following Coaching since Capacity is Full"
            lblNotReg.Visible = False
            lblRegErr.Text = " You cannot select following coach(s) since you already have a coach for the same Product Group."
            lblRegErr.Visible = False
            Dim myTableCell As TableCell, chkSelected As CheckBox, SignUpID As Integer
            If dgCoachSelection.Items.Count <= 0 Then Exit Sub
            Dim conn As New SqlConnection(Application("ConnectionString"))

            For intCtr = 0 To dgCoachSelection.Items.Count - 1
                myTableCell = dgCoachSelection.Items.SyncRoot.Item(intCtr).Cells(1)
                chkSelected = myTableCell.Controls(1)
                If chkSelected.Checked = True Then
                    myTableCell = dgCoachSelection.Items.SyncRoot.Item(intCtr).Cells(0)
                    SignUpID = CInt(myTableCell.Text)
                    Dim dsPDetail As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select ProductId,ProductCode from CalSignUp where SignUpID = " & SignUpID & "")

                    Dim PId As String = dsPDetail.Tables(0).Rows(0)("ProductId").ToString()
                    Dim pName As String = dsPDetail.Tables(0).Rows(0)("ProductCode").ToString()

                    If IsValidNIO(PId, Session("CustIndChapterID")) = False Then
                        'Display Warning message. 
                        lblRegErr.Text = lblRegErr.Text & " <br> " & "Capacity is reached for " & pName & "."
                        lblRegErr.Visible = True
                        GoTo lblEnd
                    End If

                    conn.Open()
                    Dim result As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select CASE WHEN count(CR.CoachRegID)<(select MaxCapacity from CalSignUp where SignUpID=" & SignUpID & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level  and CR.EventYear=C.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo where CR.Approved='Y' AND C.SignUpID=" & SignUpID & "") '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) 'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                    conn.Close()
                    If result.ToLower = "true" Then
                        InsertChild(SignUpID)
                    Else
                        lblNotReg.Visible = True
                        lblNotReg.ForeColor = Color.Red
                        lblNotReg.Text = lblNotReg.Text & " <br>   " & SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + ' - Level : ' + C.Level + ' - Semester : ' + C.Semester from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & "")
                    End If
                End If
            Next
            pnlCoachSelection.Visible = False
            lblerr.Text = ""
            IsSubmitClicked = True
            LoadCoaching()
            IsSubmitClicked = False
            PayError.Text = ""
lblEnd:
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub chkEvent_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim index1, index2 As Integer
        Dim i As Integer = dgCoachSelection.SelectedIndex
        lblGridError.Text = ""
        For Each dgItem In dgCoachSelection.Items
            selectRadioButton = dgItem.FindControl("chkCoachSelect")
            If selectRadioButton.Checked = True Then
                index1 = dgItem.ItemIndex
            End If
            For Each dgItem1 In dgCoachSelection.Items
                selectRadioButton1 = dgItem1.FindControl("chkCoachSelect")
                If selectRadioButton1.Checked = True Then
                    index2 = dgItem1.ItemIndex
                End If

                If index1 <> index2 Then
                    If selectRadioButton1.Checked = True And selectRadioButton.Checked = True Then
                        If selectRadioButton1.Checked = True Then 'Session("ChkIndex") = index1 And
                            selectRadioButton.Checked = False
                        ElseIf selectRadioButton.Checked = True Then 'Session("ChkIndex") = index2 And
                            selectRadioButton1.Checked = False
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Private Sub InsertChild(ByVal SignUpID As Integer)
        Dim cmdText As String = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        conn.Open()
        Dim SqlCnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp CC ON CC.ProductGroupID  = CR.ProductGroupID and CC.EventYear=CR.EventYear AND CC.Semester = CR.Semester and CC.SessionNo = CR.SessionNo   where " & IIf(ddlChild.SelectedValue.Contains("A"), "CR.AdultId=" & ddlChild.SelectedValue.Replace("A", ""), " CR.childnumber=" & ddlChild.SelectedValue) & " and CC.SignUpID=" & SignUpID & "")
        Dim SQlPrd_Pricing As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select ProductLevelPricing From EventFees Where EventID=" & Session("EventID") & "  and EventYear >=" & yr & " and ProductID= " & ddlProduct.SelectedValue & "")       ' and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)")
        If CheckChildData(SignUpID) = True Then
            Exit Sub
        End If
        If SqlCnt < 1 Then
            Try
                cmdText = "Insert Into CoachReg(CMemberID, PMemberID, " & IIf(ddlChild.SelectedValue.Contains("A"), " AdultId", " ChildNumber")
                cmdText = cmdText & " , EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Semester,SessionNo,Fee) "
                cmdText = cmdText & " select C.MemberID," & Session("CustIndID") & "," & IIf(ddlChild.SelectedValue.Contains("A"), ddlChild.SelectedValue.Replace("A", ""), ddlChild.SelectedValue) & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N',"
                cmdText = cmdText & IIf(ddlChild.SelectedValue.Contains("A"), "0", " (Select Grade from child where Childnumber=" & ddlChild.SelectedValue & ")") & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Semester,C.SessionNo, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID and C.Semester=EF.Semester where C.SignUpID=" & SignUpID
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, cmdText)
                lblCoachSelection.Text = "Inserted Successfully"
            Catch ex As Exception

            End Try
        ElseIf SqlCnt >= 1 And SQlPrd_Pricing = "N" Then
            Try
                cmdText = "Insert Into CoachReg(CMemberID, PMemberID, " & IIf(ddlChild.SelectedValue.Contains("A"), " AdultId", " ChildNumber")
                cmdText = cmdText & " , EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Semester,SessionNo,Fee) "
                cmdText = cmdText & " select C.MemberID," & Session("CustIndID") & "," & IIf(ddlChild.SelectedValue.Contains("A"), ddlChild.SelectedValue.Replace("A", ""), ddlChild.SelectedValue) & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N',"
                cmdText = cmdText & IIf(ddlChild.SelectedValue.Contains("A"), "0", " (Select Grade from child where Childnumber=" & ddlChild.SelectedValue & ")") & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Semester,C.SessionNo, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID and C.Semester=Ef.Semester where C.SignUpID=" & SignUpID
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, cmdText)
                '"Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Semester,SessionNo,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N'," & childGrade(ddlChild.SelectedValue) & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Semester,C.SessionNo, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID and C.Semester=Ef.Semester where C.SignUpID=" & SignUpID & "") ' Case When GETDATE()> DateAdd((dd, 1,EF.DeadlineDate) Then EF.RegFee+Ef.LateFee Else
                lblCoachSelection.Text = "Inserted Successfully"
            Catch ex As Exception
            End Try
        Else
            Dim strtxt As String = "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + case when C.Level is null then '' else + ' - Level : ' + C.level end as Level + ' - Semester : '+ C.Semester from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & ""
            lblRegErr.Text = lblRegErr.Text & " <br> " & SqlHelper.ExecuteScalar(conn, CommandType.Text, strtxt)
            lblRegErr.Visible = True
        End If
        conn.Close()
    End Sub
    Public Function CheckChildData(ByVal SignUpId As Integer) As Boolean
        Dim dgItem As DataGridItem
        Dim Lbl1 As Label
        Dim LblDay As Label
        Dim LblTime As Label
        Dim RetString As Boolean = False
        lblGridError.Text = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        conn.Open()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select C.Day,CAST(C.TIME AS TIME) as Time,EF.duration,C.Semester From CalSignUp C inner join EventFees EF on EF.EventId=13 and EF.EventYear=C.EventYear and EF.ProductId=C.ProductId Where SignUpId=" & SignUpId)
        If ds.Tables(0).Rows.Count > 0 Then

            For Each dgItem In dgselected.Items
                Dim SignUpID_1 As Integer = dgItem.Cells(4).Text
                Lbl1 = dgItem.FindControl("lblChildName")
                LblDay = dgItem.FindControl("lblCoachDay")
                LblTime = dgItem.FindControl("lblTime")
                Dim Duration1, Duration2 As Decimal
                Dim Time1, Time2 As DateTime
                If ddlChild.SelectedItem.Text = Lbl1.Text Then
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select C.Day,CAST(C.TIME AS TIME) as Time,EF.duration From CalSignUp C inner join EventFees EF on EF.EventId=13 and EF.EventYear=C.EventYear and EF.ProductId=C.ProductId and EF.Semester=C.Semester Where SignUpId=" & SignUpID_1 & " and C.Semester ='" & ds.Tables(0).Rows(0)("Semester") & "'")
                    If ds1.Tables(0).Rows.Count > 0 Then
                        If LblDay.Text = ds.Tables(0).Rows(0)("Day") Then
                            Time1 = Convert.ToDateTime(ds.Tables(0).Rows(0)("Time").ToString())
                            Time2 = Convert.ToDateTime(ds1.Tables(0).Rows(0)("Time").ToString())
                            Dim checkCloseItem As Boolean = False
                            If (Time1 < Time2) Then
                                Duration1 = ds.Tables(0).Rows(0)("Duration")
                                Dim minute As Integer = 0
                                If Duration1.ToString().Contains(".5") = True Then
                                    minute = 30
                                    Duration1 = Duration1.ToString().Substring(0, Duration1.ToString().IndexOf("."))
                                End If

                                Dim t1 As DateTime = Convert.ToDateTime(Time1.AddHours(Duration1).AddMinutes(minute + 30))
                                If t1 > Time2 Then
                                    checkCloseItem = True
                                End If

                            ElseIf (Time2 < Time1) Then
                                Duration2 = ds1.Tables(0).Rows(0)("Duration")
                                Dim minute As Integer = 0
                                If Duration2.ToString().Contains(".5") = True Then
                                    minute = 30
                                    Duration2 = Duration2.ToString().Substring(0, Duration2.ToString().IndexOf("."))
                                End If

                                Dim t2 As DateTime = Convert.ToDateTime(Time2.AddHours(Duration2).AddMinutes(minute + 30))
                                If t2 > Time1 Then
                                    checkCloseItem = True
                                End If
                            End If

                            If checkCloseItem = True Then
                                lblGridError.Text = "The selected item is too close to the other selections already made for the child."
                                lblGridError.Visible = True
                                RetString = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
        End If
        conn.Close()
        Return RetString
    End Function
    Function childGrade(ByVal childnumber As Integer) As Integer
        Dim Grade As Integer = 0
        Dim conn As New SqlConnection(Application("ConnectionString"))
        conn.Open()
        Grade = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select Grade from child where Childnumber=" & childnumber & "")
        conn.Close()
        Return Grade
    End Function

    Protected Sub dgCoachSelection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCoachSelection.ItemDataBound
        Dim conn As New SqlConnection(Application("ConnectionString"))
        conn.Open()
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim SignUpID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "SignUpID"), Integer)
                If SqlHelper.ExecuteScalar(conn, CommandType.Text, "  Select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp CC ON CC.MemberID = CR.CMemberID and CR.Level =CC.Level and CC.ProductID=CR.ProductID and CR.EventYear=CC.EventYear AND CC.Semester = CR.Semester and CC.SessionNo = CR.SessionNo  where " & IIf(ddlChild.SelectedValue.Contains("A"), " CR.AdultId = " & ddlChild.SelectedValue.Replace("A", ""), " CR.childnumber=" & ddlChild.SelectedValue) & " and CR.EventYear=" & yr & " and CC.SignUpID=" & SignUpID & "") > 0 Then '((Case when CC.ProductGroupCode not in('UV') then CR.Level end)=CC.Level Or (Case when CC.ProductGroupCode in('UV') then CR.Level end)Is null) ' Commented on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                    CType(e.Item.FindControl("chkCoachSelect"), CheckBox).Visible = False
                    Dim lbl As Label = CType(e.Item.FindControl("lblStatus"), Label)
                    lbl.Text = "Selected"
                    lbl.Visible = True
                Else
                    btnSubmit.Visible = True
                End If
        End Select
        conn.Close()
    End Sub

    Private Sub LoadSelectedCoaching()
        Try
            Session("ChildNumbers") = Nothing
            Session("UpdateRegInChildAccess") = "false"
            Session("FullWaive") = "false"
            PayError.Text = ""
            sp.Visible = False
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            Dim ProductFee As Double = 0.0
            Dim ProductGroupFee As Double = 0.0
            SQLStr = " select * from (select CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.ProductCode as ProductName,P.Name,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,"
            SQLStr = SQLStr & " C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+ ' EST' as Time, C.StartDate,C.Enddate,EF.LateRegDLDate,EF.RegFee,Case When GETDATE()>dbo.ufn_GetDeadLine(EF.DeadlineDate) Then EF.LateFee Else '' End As LateFee,EF.ProductLevelPricing,I.City,I.State,Case when CR.Approved ='N' then 'Pending' Else Case when CR.PaymentReference IS NULL THEN 'Approved' ELSE 'Paid' End End as Status,CR.Approved,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level and  C1.EventYear = CR1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,I.Email "
            SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END +' hr(s)' as Duration ,P.Productid,I.FirstName,I.LastName"
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join  EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID and  EF.Semester=CR.Semester and EF.RegOpen='Y' Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & ""
            SQLStr = SQLStr & " and CR.EventYear=" & yr & " and C.Accepted='Y' "
            SQLStr = SQLStr & " UNION "
            SQLStr = SQLStr & "select CR.CoachRegID,C.SignUpID,Ch.FIRSTNAME + ' ' + Ch.LASTNAME AS ChildName, P.ProductCode as ProductName,P.Name,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,"
            SQLStr = SQLStr & " C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+ ' EST' as Time, C.StartDate,C.Enddate,EF.LateRegDLDate,EF.RegFee,Case When GETDATE()>dbo.ufn_GetDeadLine(EF.DeadlineDate) Then EF.LateFee Else '' End As LateFee,EF.ProductLevelPricing,I.City,I.State,Case when CR.Approved ='N' then 'Pending' Else Case when CR.PaymentReference IS NULL THEN 'Approved' ELSE 'Paid' End End as Status,CR.Approved,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level and  C1.EventYear = CR1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,I.Email "
            SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END +' hr(s)' as Duration ,P.Productid,I.FirstName,I.LastName"
            SQLStr = SQLStr & " from Coachreg CR Inner Join Indspouse Ch ON CR.AdultId=Ch.Automemberid Inner Join  EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID and EF.Adult='Y' and  EF.Semester=CR.Semester and EF.RegOpen='Y' Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & ""
            SQLStr = SQLStr & " and CR.EventYear=" & yr & " and C.Accepted='Y' "
            SQLStr = SQLStr & " ) coach ORDER BY ChildNumber,ProductID,LevelID,CASE Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end, CAST(TIME AS varchar), LastName, FirstName"

            pnlSeleted.Visible = True
            conn.Open()
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            conn.Close()

            If dgselected.Items.Count > 0 Then
                dgselected.Visible = True
                Dim Total_Amount As Double = 0.0
                Dim Amt_Product As Double
                Dim Amt_ProductGroup As Double
                Dim RegFee, LateFee As Double
                Dim DiscntProds As String = ""
                Dim DiscountAmt As Double = 0.0
                Dim DiscountFlag As Boolean = False
                Dim registrationREF As String
                conn.Open()
                Dim dsChild As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select Distinct Childnumber,AdultId from CoachReg Where PMemberid=" & Session("CustIndID") & " and EventYear >=" & yr & " and EventID=" & Session("EventID"))
                For i As Integer = 0 To dsChild.Tables(0).Rows.Count - 1 ' Done for Each child
                    If dsChild.Tables(0).Rows(i)("ChildNumber") Is DBNull.Value Then
                        registrationREF = " CR.AdultId = " & dsChild.Tables(0).Rows(i)("AdultId")
                    Else
                        registrationREF = " CR.ChildNumber = " & dsChild.Tables(0).Rows(i)("ChildNumber")
                    End If
                    'SP A ProductLevelPricing 'Y'
                    '*****************Sum of UpPaid(PaymentReference is Null) Products under ProductLevelPricing 'N'**************'
                    Dim SqlProdGroupY As String = ""
                    SqlProdGroupY = "Select Distinct CR.ProductGroupID,CR.ProductId From CoachReg CR  Inner Join EventFees EF on EF.EventYear = CR.EventYear and EF.EventId = CR.EventID and EF.ProductGroupID=CR.ProductGroupID "
                    SqlProdGroupY = SqlProdGroupY & " Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & yr & " and CR.EventID=" & Session("EventID") & " and EF.ProductLevelPricing ='Y' and " & registrationREF ' CR.ChildNumber=" & dsChild.Tables(0).Rows(i)("ChildNumber") & ""
                    SqlProdGroupY = SqlProdGroupY & " and CR.PaymentReference is Null and CR.Approved='N'" ''Added on 28-09-2013 to include only UnPaid Transactions
                    Dim dsPrGroup1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SqlProdGroupY)
                    For j As Integer = 0 To dsPrGroup1.Tables(0).Rows.Count - 1
                        Try
                            Dim SQLProduct As String = "Select EF.RegFee, Case When GETDATE()> dbo.ufn_GetDeadLine(EF.DeadlineDate)  Then EF.LateFee Else 0.00 End as LateFee from CoachReg CR Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID and Ef.Semester=CR.Semester and EF.ProductLevelPricing='Y' where CR.PMemberid=" & Session("CustIndID") & " and " & registrationREF & " and CR.ProductGroupID=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & " and CR.ProductID=" & dsPrGroup1.Tables(0).Rows(j)("ProductID") & " and CR.PaymentReference is Null  and CR.Approved='N' and C.Accepted='Y' and CR.EventYear>=" & yr
                            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLProduct)
                            If ds1.Tables(0).Rows.Count > 0 Then
                                RegFee = RegFee + ds1.Tables(0).Rows(0)("RegFee")
                                LateFee = LateFee + ds1.Tables(0).Rows(0)("LateFee")
                                Amt_Product = Amt_Product + ds1.Tables(0).Rows(0)("RegFee") + ds1.Tables(0).Rows(0)("LateFee") 'SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLProduct)
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                    'SP B ProductLevelPricing 'Y' & 'N'
                    ''''Sum of UpPaid(PaymentReference is Null) Products under ProductLevelPricing 'N'
                    Dim SqlProdGroupN As String = ""
                    SqlProdGroupN = SqlProdGroupN & " Select Distinct CR.ProductGroupID,CR.ProductID From CoachReg CR  Inner Join EventFees EF on EF.EventYear = CR.EventYear and EF.EventId = CR.EventID and EF.ProductGroupID=CR.ProductGroupID "
                    SqlProdGroupN = SqlProdGroupN & " Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & yr & " and CR.EventID=" & Session("EventID") & " and EF.ProductLevelPricing ='N' and " & registrationREF 'CR.ChildNumber=" & dsChild.Tables(0).Rows(i)("ChildNumber") & ""
                    SqlProdGroupN = SqlProdGroupN & " and CR.PaymentReference is Null  and CR.Approved='N'" ''Added on 28-09-2013 to include only UnPaid Transactions
                    Dim ProdGroupCount As Double = 0.0
                    Dim prodRegFee As Double = 0.0
                    Dim dsPrGroup2 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SqlProdGroupN)
                    Try
                        For k As Integer = 0 To dsPrGroup2.Tables(0).Rows.Count - 1
                            Try
                                Dim SQLPrdGp_Amt As String = ""
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " select EF.RegFee , Case When GETDATE()> dbo.ufn_GetDeadLine(EF.DeadlineDate)  Then EF.LateFee Else 0.00 End as LateFee from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductGroupID=C.ProductGroupID AND  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductGroupID = Ef.ProductGroupID and CR.Semester=Ef.Semester and EF.ProductLevelPricing='N' "
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " where CR.PMemberid=" & Session("CustIndID") & " and " & registrationREF & " and CR.ProductID=" & dsPrGroup2.Tables(0).Rows(k)("ProductID") & "  and CR.PaymentReference is Null "
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " and CR.Approved='N' and C.Accepted='Y' and CR.EventYear>=" & yr
                                Dim ds2 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLPrdGp_Amt)
                                If ds2.Tables(0).Rows.Count > 0 Then
                                    ProdGroupCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From EventFees Where EventID=" & Session("EventID") & " and EventYear=" & yr & " and ProductGroupID=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & " and ProductLevelPricing ='N'")
                                    If ProdGroupCount > 0 Then
                                        prodRegFee = Format$((ds2.Tables(0).Rows(0)("RegFee") / ProdGroupCount), "Currency")
                                    Else
                                        prodRegFee = ds2.Tables(0).Rows(0)("RegFee")
                                    End If
                                    RegFee = RegFee + prodRegFee
                                    LateFee = LateFee + ds2.Tables(0).Rows(0)("LateFee")
                                    Amt_ProductGroup = Amt_ProductGroup + prodRegFee + ds2.Tables(0).Rows(0)("LateFee") ' SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLPrdGp_Amt)
                                End If
                            Catch ex As Exception
                            End Try
                        Next
                    Catch ex As Exception
                    End Try
                Next
                Total_Amount = Total_Amount + Amt_Product + Amt_ProductGroup

                Session("TotalFee") = Total_Amount
                Session("RegFee") = RegFee
                Session("LateFee") = LateFee
                Dim RemainingCredit As Double
                Dim IsCoach As Boolean = False
                Dim strCmd As String                '[usp_GetCoachDiscount]
                strCmd = "[usp_GetCoachDiscount]"
                Dim ParamDisc(1) As SqlParameter
                ParamDisc(0) = New SqlParameter("@MemberID", Session("CustIndID"))
                Dim PaidAmount As Double
                PaidAmount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, strCmd, ParamDisc)
                lblDiscountGiven.Text = "Discount given earlier : " & Format$(PaidAmount, "Currency")
                If Total_Amount > 0 Then
                    'Get Discount amount of Coach
                    Dim param(1) As SqlParameter, inx As Integer, dr As DataRow
                    ', disAmount As Double = 0
                    Dim BalanceToPay As Double
                    param(0) = New SqlParameter("@MemberID", Session("CustIndID"))
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_GetCoachProductDiscount]", param)
                    For inx = 0 To ds.Tables(0).Rows.Count - 1
                        dr = ds.Tables(0).Rows(inx)
                        If dr("ProductLevelPricing") = "N" Then
                            DiscountAmt = DiscountAmt + (CInt(dr("RegFee")) / 2) '(CInt(dr("RegFee")) / 2) CInt(dr("DiscountAmt"))
                        Else
                            DiscountAmt = DiscountAmt + CInt(dr("RegFee"))
                        End If
                        DiscntProds = DiscntProds & "," & dr("ProductGroupCode") & ""
                    Next

                    RemainingCredit = DiscountAmt - PaidAmount
                    If DiscountAmt > 0 Then
                        lblDiscount.Text = DiscountAmt
                        lblDiscount.Visible = True
                    Else
                        lblDiscount.Text = ""
                        lblDiscount.Visible = False
                    End If
                    Session("Discount") = 0

                    If RemainingCredit > 0 Then
                        IsCoach = True
                        BalanceToPay = Total_Amount - RemainingCredit
                        Session("Discount") = RemainingCredit
                        If BalanceToPay <= 0 Then
                            Session("Discount") = Total_Amount
                            btnDonate.Visible = False
                            HlblBtnDonate.Text = ""
                            lblSelected.Text = "Gross Amount : " & Format$(Total_Amount, "Currency")
                            lblDiscount.Text = "Current Discount : " & Format$(Total_Amount, "Currency") & " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net to be paid :" & Format$(0, "Currency")
                            If IsExistsProductPricing() = True Then
                                Exit Sub
                            End If
                            sp.Visible = True
                            'Get full discount
                            'Check the student has onlineemailid  
                            Session("FullWaive") = "true"
                            'Send mail to Coach
                            ApprovedChildNotification()
                            ' update coachreg status and skip payment process
                            strCmd = "UPDATE CR SET CR.Approved='Y',CR.ModifyDate=GetDate(),ModifiedBy=" & Session("CustIndID") & " FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level = C.Level AND C.EventYear = CR.EventYear "
                            strCmd = strCmd & " AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo inner join EventFees EF on EF.EventId=13 and EF.EventYear=C.EventYear and EF.ProductId=C.ProductId and EF.Semester=C.Semester WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" & Session("CustIndID") & " AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & yr  '& Year(Today())
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strCmd)

                            SQLStr = "select CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.ProductCode as ProductName,P.Name,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,"
                            SQLStr = SQLStr & " C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+ ' EST' as Time, C.StartDate,C.Enddate,EF.LateRegDLDate,EF.RegFee,Case When GETDATE()> dbo.ufn_GetDeadLine(EF.DeadlineDate)  Then EF.LateFee Else '' End As LateFee,EF.ProductLevelPricing,I.City,I.State,Case when CR.Approved ='N' then 'Pending' Else Case when CR.PaymentReference IS NULL THEN 'Approved' ELSE 'Paid' End End as Status,CR.Approved,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level and  C1.EventYear = CR1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,I.Email "
                            SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END +' hr(s)' as Duration "
                            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join  EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID and Ef.Semester= CR.Semester Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
                            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & ""
                            SQLStr = SQLStr & " and CR.EventYear=" & yr & " and C.Accepted='Y'"
                            SQLStr = SQLStr & " UNION "
                            SQLStr = "select CR.CoachRegID,C.SignUpID,Ch.FIRSTNAME + ' ' + Ch.LASTNAME AS ChildName, P.ProductCode as ProductName,P.Name,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,"
                            SQLStr = SQLStr & " C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+ ' EST' as Time, C.StartDate,C.Enddate,EF.LateRegDLDate,EF.RegFee,Case When GETDATE()> dbo.ufn_GetDeadLine(EF.DeadlineDate)  Then EF.LateFee Else '' End As LateFee,EF.ProductLevelPricing,I.City,I.State,Case when CR.Approved ='N' then 'Pending' Else Case when CR.PaymentReference IS NULL THEN 'Approved' ELSE 'Paid' End End as Status,CR.Approved,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.Level =C1.Level and  C1.EventYear = CR1.EventYear AND C1.Semester = CR1.Semester AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,I.Email "
                            SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END +' hr(s)' as Duration "
                            SQLStr = SQLStr & " from Coachreg CR Inner Join Indspouse Ch ON CR.AdultId=Ch.Automemberid Inner Join  EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and EF.Adult='Y' and CR.ProductID = Ef.ProductID and Ef.Semester= CR.Semester Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
                            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & ""
                            SQLStr = SQLStr & " and CR.EventYear=" & yr & " and C.Accepted='Y'"
                            SQLStr = SQLStr & " ORDER BY CR.ChildNumber,CR.ProductID,LevelID,CASE C.Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end, CAST(C.TIME AS TIME), I.LastName, I.FirstName"

                            drCoaching = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr)
                            dgselected.DataSource = drCoaching
                            dgselected.DataBind()
                            Exit Sub
                        End If
                    Else
                        DiscountAmt = 0
                    End If
                    If (RemainingCredit > 0 And BalanceToPay > 0) Then
                        Total_Amount = Total_Amount - RemainingCredit
                    End If
                End If
                '  Gross Amount:  $280      Discount:  $151      Net to be paid: $129
                If IsCoach = True Then
                    lblSelected.Text = "Gross Amount : " & Format$(Total_Amount + RemainingCredit, "Currency")
                    lblDiscount.Text = "Current Discount : " & Format$(RemainingCredit, "Currency") & " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net to be paid :" & Format$(Total_Amount, "Currency")
                Else
                    lblSelected.Text = "Total Amount to be paid : " & Format$(Total_Amount, "Currency")
                    lblDiscount.Text = "Discount Given: " & Format$(DiscountAmt, "Currency")
                End If
                conn.Close()
            Else
                Session("RegFee") = Nothing

                dgselected.Visible = False
                lblSelected.ForeColor = Color.Red
                lblSelected.Text = "No Coaching was selected"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub ApprovedChildNotification()
        Try
            Dim CoachEmail As String = ""
            If (Not Session("CoachEmail") Is Nothing) Then
                CoachEmail = Session("CoachEmail").ToString
            End If
            Dim eventType As String
            If (Session("EventID") = 1) Then
                eventType = "Finals"
            Else
                eventType = "Regional"
            End If
            Dim conn As New SqlConnection(Application("ConnectionString"))
            conn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select I.Email,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,CR.Level,CR.ProductID FROM CoachReg CR INNER JOIN CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo   Inner JOIN Product P ON C.ProductID=P.ProductID  Inner JOIN  IndSpouse I ON I.AutoMemberID = C.MemberID INNER JOIN Child Ch ON Ch.ChildNumber=CR.ChildNumber INNER JOIN IndSpouse I2 ON Ch.MEMBERID = I2.AutoMemberID  WHERE  CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" & Session("CustIndID") & " AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & yr & " and C.Accepted='Y'") ''** and C.StartDate < GETDATE ()") '((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            Dim Emailid, Mailbody As String
            While reader.Read()
                Emailid = reader("Email")
                CoachEmail = Emailid
                Session("ProductCodeCoach") = reader("ProductCode")
                Mailbody = "Dear Coach, <br><br>Note : Do not reply to the email above.  "
                Mailbody = Mailbody & "<br><br>New student: " & reader("ChildName") & ", Parent Name: " & reader("ParentName") & ", Email: " & reader("ParentEMail") & ", " & reader("ProductCode") & ", Coach: " & Emailid & ", Level: " & reader("Level") & "."
                SendEmail("A New student is joining ", Mailbody, Emailid)
            End While
            conn.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
        'Build Email Message
        Dim email As New MailMessage

        email.From = New MailAddress(System.Configuration.ConfigurationManager.AppSettings("CoachingSupport").ToString())


        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Dim ok As Boolean = True
        Try
            email.To.Add(sMailTo)
            client.Send(email)
        Catch e As Exception
            ok = False
        End Try
        Return ok
    End Function

    Function IsExistsProductPricing() As Boolean
        lblerr.Text = ""
        ' do validation rule if Productlevelpricing is N
        Dim Product_Flag As Boolean = False
        Dim Flag_Count As Integer = 0
        Dim Row_cnt As Integer = 0
        Dim ProdGroupErr As String = ""

        Dim SQLChild As String = ""
        Dim SQLProdGroup As String = ""
        Dim SQLProduct As String = ""
        Dim SQLProuctChk As String = ""

        SQLChild = SQLChild & "Select Distinct ChildNumber,Grade From CoachReg Where PMemberID=" & Session("CustIndID") & " and EventYear=" & yr & " and EventID=" & Session("EventID") & ""
        Dim ds_Child As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLChild)
        If ds_Child.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds_Child.Tables(0).Rows.Count - 1
                SQLProdGroup = "Select Distinct CR.ProductGroupID,CR.ProductGroupCode from CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID and EF.ProductLevelPricing ='N' Where CR.PMemberID = " & Session("CustIndID") & " And CR.EventYear = " & yr & " And CR.EventID = " & Session("EventID") & "  And CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber") & " And Grade =" & ds_Child.Tables(0).Rows(i)("Grade")
                Dim ds_ProdGroup As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLProdGroup)
                If ds_ProdGroup.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To ds_ProdGroup.Tables(0).Rows.Count - 1
                        SQLProduct = "Select Count(Distinct CR.ProductID) From CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID  and EF.ProductLevelPricing ='N' Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & yr & " and CR.EventID=" & Session("EventID") & " and CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber") & " and Grade= " & ds_Child.Tables(0).Rows(i)("Grade") & " and CR.ProductGroupID=" & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID") & ""
                        Dim ProductID_Count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProduct)
                        SQLProuctChk = "Select Count(Distinct P.ProductID) from Product P Inner Join EventFees EF on EF.ProductGroupId = EF.ProductGroupID  and  P.ProductId=EF.ProductID and Ef.EventID=P.EventID Where EF.EventID= " & Session("EventID") & " and EF.EventYear=" & yr & " and P.Status ='O' and EF.ProductLevelPricing='N' and EF.ProductGroupID = " & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID")
                        Dim ProductIDChk_Count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProuctChk)
                        If ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count <> ProductIDChk_Count Then
                            ProdGroupErr = ProdGroupErr & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupCode") & ","
                        ElseIf ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count = ProductIDChk_Count Then
                            ProdGroupErr = ProdGroupErr & ""
                        End If
                    Next
                End If
            Next
        End If
        If ProdGroupErr <> "" Then  'Row_cnt > 0 And Product_Flag = False Then
            lblerr.Visible = True
            lblerr.Text = "Please Select all the Products for the ProductGroup(s) " & ProdGroupErr.TrimEnd(",")
            Return True
        End If
        Return False
    End Function
    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgselected.ItemCommand
        lblPendingErr.Text = ""
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If (Not (lbtn) Is Nothing) Then
            Dim CoachRegID As Integer = CInt(e.Item.Cells(0).Text)
            Try
                conn.Open()
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "Delete from CoachReg Where CoachRegID=" & CoachRegID & " and Approved='N' and pmemberid=" & Session("CustIndID") & "")
                conn.Close()
                LoadCoaching()

                PayError.Text = ""
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Remove failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub dgselected_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgselected.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Status As String = CType(DataBinder.Eval(e.Item.DataItem, "status"), String)
                Dim Approved As String = CType(DataBinder.Eval(e.Item.DataItem, "Approved"), String)
                If Status.Trim = "Paid" Then
                    CType(e.Item.FindControl("lbtnRemove"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lblStatus1"), Label).Text = "Paid"
                ElseIf Approved.Trim = "Y" Then
                    CType(e.Item.FindControl("lbtnRemove"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lblStatus1"), Label).Text = "Approved"
                Else
                    HlblBtnDonate.Text = "Y"
                    If Session("UpdateRegInChildAccess") = "true" Then
                        btnDonate.Visible = False
                    Else : btnDonate.Visible = True
                    End If
                    Dim lbl As Label = CType(e.Item.FindControl("lblStatus1"), Label)
                End If
        End Select
    End Sub

    Protected Sub btnDonate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDonate.Click
        Dim Product_Flag As Boolean = False
        Dim Flag_Count As Integer = 0
        Dim Row_cnt As Integer = 0
        Dim ProdGroupErr As String = ""

        Dim SQLChild As String = ""
        Dim SQLProdGroup As String = ""
        Dim SQLProduct As String = ""
        Dim SQLProuctChk As String = ""

        Dim conn As New SqlConnection(Application("ConnectionString"))
        SQLChild = SQLChild & "Select Distinct ChildNumber,AdultId,Grade From CoachReg Where PMemberID=" & Session("CustIndID") & " and EventYear=" & yr & " and EventID=" & Session("EventID") & ""
        conn.Open()
        Dim ds_Child As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLChild)
        Dim registrationREF As String
        If ds_Child.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds_Child.Tables(0).Rows.Count - 1
                If ds_Child.Tables(0).Rows(i)("ChildNumber") Is DBNull.Value Then
                    registrationREF = " CR.AdultId = " & ds_Child.Tables(0).Rows(i)("AdultId")
                Else
                    registrationREF = " CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber")
                End If
                SQLProdGroup = "Select Distinct CR.ProductGroupID,CR.ProductGroupCode from CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID and EF.ProductLevelPricing ='N' Where CR.PMemberID = " & Session("CustIndID") & " And CR.EventYear = " & yr & " And CR.EventID = " & Session("EventID") & "  And " & registrationREF & " And Grade =" & ds_Child.Tables(0).Rows(i)("Grade")
                Dim ds_ProdGroup As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLProdGroup)
                If ds_ProdGroup.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To ds_ProdGroup.Tables(0).Rows.Count - 1
                        SQLProduct = "Select Count(Distinct CR.ProductID) From CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID  and EF.ProductLevelPricing ='N' Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & yr & " and CR.EventID=" & Session("EventID") & " and " & registrationREF & " and Grade= " & ds_Child.Tables(0).Rows(i)("Grade") & " and CR.ProductGroupID=" & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID") & ""
                        Dim ProductID_Count As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLProduct)
                        SQLProuctChk = "Select Count(Distinct P.ProductID) from Product P Inner Join EventFees EF on EF.ProductGroupId = EF.ProductGroupID  and  P.ProductId=EF.ProductID and Ef.EventID=P.EventID Where EF.EventID= " & Session("EventID") & " and EF.EventYear=" & yr & " and P.Status ='O' and EF.ProductLevelPricing='N' and EF.ProductGroupID = " & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID")
                        Dim ProductIDChk_Count As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLProuctChk)
                        If ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count <> ProductIDChk_Count Then
                            ProdGroupErr = ProdGroupErr & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupCode") & ","
                        ElseIf ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count = ProductIDChk_Count Then
                            ProdGroupErr = ProdGroupErr & ""
                        End If
                    Next
                End If
            Next
        End If
        conn.Close()
        If ProdGroupErr <> "" Then  'Row_cnt > 0 And Product_Flag = False Then
            Product_Flag = False
            ChkAllProductsRegistered()
            PayError.Text = "Please Select all the Products for the ProductGroup(s) " & ProdGroupErr.TrimEnd(",")
            Exit Sub
        Else
            Product_Flag = True
        End If

        If Session("RegFee") < 1 Then
            lblerr.Text = "No coaching is Pending to be paid"
        ElseIf chkcapacity() And Product_Flag = True Then
            Response.Redirect("CoachingSelectionSum.aspx")
        Else
            lblerr.Text = "Please Change the Coach"
        End If
    End Sub
    Function chkcapacity() As Boolean
        Dim intCtr As Integer
        Dim flag As Boolean = True
        lblNotReg.Text = "There is no more room for following coach.  Please change coach."
        lblNotReg.Visible = False
        Dim myTableCell As TableCell, SignUpID As Integer, status As String
        Try
            If dgselected.Items.Count <= 0 Then Exit Function
            Dim conn As New SqlConnection(Application("ConnectionString"))
            conn.Open()
            For intCtr = 0 To dgselected.Items.Count - 1
                myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(6)
                Session("CoachEmail") = myTableCell.Text

                myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(1)
                status = myTableCell.Text

                If status = "Pending" Then
                    Dim LateRegDate As Date
                    Dim ProductCode As String

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(3)
                    ProductCode = myTableCell.Text.ToString
                    Session("ProductCodeCoach") = ProductCode

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(4)
                    SignUpID = CInt(myTableCell.Text)

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(5)
                    LateRegDate = Date.Parse(myTableCell.Text)

                    Dim currentcount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level AND C.EventYear=CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo where CR.paymentreference is NULL and CR.PMemberID = " & Session("CustIndID") & " AND C.SignUpID=" & SignUpID & "") ' ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) 'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                    Dim result As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select CASE WHEN count(CR.CoachRegID)+" & currentcount & "<=(select MaxCapacity from CalSignUp where SignUpID=" & SignUpID & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear=CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo where CR.Approved='Y' AND C.SignUpID=" & SignUpID & "") '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                    If result.ToLower = "false" Then
                        flag = False
                        lblNotReg.Visible = True
                        lblNotReg.ForeColor = Color.Red
                        lblNotReg.Text = lblNotReg.Text & " <br> " & SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + ' - Level : ' + C.Level + ' - Semester : ' + C.Semester from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & "")
                    End If

                    If SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from ExCoaching Ex inner join CoachReg CR on Ex.ChildNumber=CR.ChildNumber and Ex.EventYear=CR.EventYear and Ex.ProductId=CR.ProductId and Ex.CMemberId=CR.CMemberId Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level AND C.EventYear=CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo where CR.paymentreference is NULL and CR.PMemberID = " & Session("CustIndID") & " AND C.SignUpID=" & SignUpID & "") > 0 Then
                        LateRegDate = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select NewDeadLine from ExCoaching Ex inner join CoachReg CR on Ex.ChildNumber=CR.ChildNumber and Ex.EventYear=CR.EventYear and Ex.ProductId=CR.ProductId and Ex.CMemberId=CR.CMemberId Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level AND C.EventYear=CR.EventYear and  C.Semester = CR.Semester and C.SessionNo = CR.SessionNo where CR.paymentreference is NULL and CR.PMemberID = " & Session("CustIndID") & " AND C.SignUpID=" & SignUpID & "")
                    End If

                    If Now.Date() > LateRegDate Then
                        lblPendingErr.Text = lblPendingErr.Text & " <br> Sorry Registration Deadline Passed for " & ProductCode & ". Please delete it to proceed."
                        lblPendingErr.Visible = True
                        flag = False
                        Exit For
                    End If
                End If
            Next
            conn.Close()
        Catch ex As Exception
        End Try
        Return flag
    End Function

    Protected Sub ddlProductLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductLevel.SelectedIndexChanged
        If ddlProductLevel.SelectedValue <> -1 Then
            LoadCoaching()
        End If
    End Sub

End Class
