﻿Imports Microsoft.ApplicationBlocks.Data

Partial Class ContestSettings
    Inherits System.Web.UI.Page
    Dim cmdText As String
    Dim i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 1 '89
        'Session("LoginID") = 4240 '22214       
        'Session("LoggedIn") = "true"

        Try

            If Session("LoginID") Is Nothing Then
                Response.Redirect("~/maintest.aspx")
            End If
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then
                    FillEvent()
                    FillContestYear()
                    FillDetailsInDropDown()
                    BindContestSettings()
                Else
                    Response.Redirect("~/maintest.aspx")
                End If
            End If
        Catch ex As Exception
            Response.Redirect("~/maintest.aspx")
        End Try
    End Sub
    Private Sub FillContestYear()
        ddlYear.Items.Clear()
        ddlProYear.Items.Clear()
        Dim strCond As String = ""
        If ddlEvent.SelectedIndex > 0 Then
            strCond = "Where EventId=" & ddlEvent.SelectedValue
        End If
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year + 2)
        Dim i, j As Integer
        j = 0
        For i = 0 To 5
            ddlYear.Items.Insert(i, Convert.ToString(year - j))
            ddlProYear.Items.Insert(i, Convert.ToString(year - (j - 1)))
            j = j + 1
        Next
        cmdText = "select isnull(max(ContestYear),0) FROM ContestSettings " & strCond
        Dim iYr As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
        If iYr = 0 Then
            iYr = DateTime.Now.Year
        End If
        If ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(Convert.ToInt32(iYr)))) <> -1 Then
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(Convert.ToInt32(iYr))))
        Else
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(Convert.ToInt32(DateTime.Now.Year))))
        End If

        ddlProYear.SelectedIndex = ddlYear.SelectedIndex
        ddlYear_SelectedIndexChanged(ddlYear, New EventArgs)

    End Sub
    Private Sub FillEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        If ddlEvent.Items.Count > 0 Then
            ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
            ddlEvent.SelectedIndex = 1
            ddlEvent_SelectedIndexChanged(ddlEvent, New EventArgs)
        End If
    End Sub
    Private Sub FillDetailsInDropDown()
        ' Fill Phase
        ddlPhase.Items.Add(New ListItem("Select", -1))
        For i = 1 To 3
            ddlPhase.Items.Add(i)
        Next
        ' Fill Type
        ddlType.Items.Add(New ListItem("Select Type", -1))
        ddlType.Items.Add(New ListItem("Oral-Written (OW)", "OW"))
        ddlType.Items.Add(New ListItem("Oral-Oral (OO)", "OO"))
        ddlType.Items.Add(New ListItem("Written (W)", "W"))
        ' Fill Rounds 0 to 10, 99
        ddlRounds.Items.Add(New ListItem("Select", "NULL"))
        For i = 0 To 10
            ddlRounds.Items.Add(i)
        Next
        ddlRounds.Items.Add(New ListItem(30, 30))
        ' Fill Duration 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 75, 90, 105, 120, 150, 180
        ddlDuration.Items.Add(New ListItem("Select", -1))
        For i = 15 To 60 Step 5
            ddlDuration.Items.Add(i)
        Next
        For i = 75 To 120 Step 15
            ddlDuration.Items.Add(i)
        Next
        ddlDuration.Items.Add(150)
        ddlDuration.Items.Add(180)
        '1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 35, 40
        For i = 1 To 10
            ddlQuestion.Items.Add(i)
        Next
        For i = 15 To 40 Step 5
            ddlQuestion.Items.Add(i)
        Next
        ddlQTime.Items.Add(New ListItem("Select", "NULL"))
        For i = 10 To 60 Step 5
            ddlQTime.Items.Add(i)
        Next
        ddlMaxCap.Items.Add(New ListItem("Select", "NULL"))
        For i = 0 To 50 Step 5
            If i = 0 Then
                ddlMaxCap.Items.Add(1)
            Else
                ddlMaxCap.Items.Add(i)
            End If
        Next
        ddlRecCap.Items.Add(New ListItem("Select", "NULL"))
        For i = 10 To 30 Step 5
            ddlRecCap.Items.Add(i)
        Next

        'at the end 150, 175, 200, 250, 300, 350
        ddlRoomCap.Items.Add(New ListItem("Select", "NULL"))
        For i = 30 To 120 Step 5
            ddlRoomCap.Items.Add(i)
        Next
        ddlRoomCap.Items.Add(150)
        ddlRoomCap.Items.Add(175)
        ddlRoomCap.Items.Add(200)
        ddlRoomCap.Items.Add(250)
        ddlRoomCap.Items.Add(300)
        ddlRoomCap.Items.Add(350)


        ' Published
        ddlPub.Items.Add(New ListItem("Select", "NULL"))
        For i = 1 To 10
            ddlPub.Items.Add(i)
        Next
        For i = 15 To 30 Step 5
            ddlPub.Items.Add(i)
        Next

    End Sub
    Private Sub ProductGroup()
        cmdText = "SELECT  Distinct P.ProductGroupID, P.Name ProductGroup from ProductGroup P where  P.EventId =" & ddlEvent.SelectedValue & "  order by ProductGroupID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ddlProductGroup.DataSource = ds.Tables(0)
        ddlProductGroup.DataBind()
        ddlProductGroup.Items.Insert(0, "Select Product Group")
        ddlProductGroup.Items(0).Selected = True

        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
    End Sub
    Private Sub Product()
        If ddlProductGroup.SelectedIndex <> 0 Then
            ' will load depending on Selected item in Productgroup
            cmdText = "SELECT  Distinct P.ProductID, P.Name Product from Product P INNER JOIN ProductGroup PG on P.ProductGroupId=PG.ProductGroupId where PG.ProductGroupId=" & ddlProductGroup.SelectedValue & " and P.EventId =" & ddlEvent.SelectedValue & " order by  ProductID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlProduct.DataSource = ds.Tables(0)
            ddlProduct.DataBind()
            ddlProduct.Items.Insert(0, "Select Product")
            ddlProduct.Items(0).Selected = True
        End If
    End Sub
    Private Sub BindContestSettings()
        Dim strCond As String = ""
        If ddlEvent.SelectedIndex > 0 Then
            cmdText = "select ContSetID,ContestYear,cs.EventID, e.Name EventName,cs.ProductGroupID, pg.Name ProductGroup,cs.ProductID,p.Name Product,Phase,Type,Rounds,Duration,[Questions],Pub,isnull([MultiChoice],'N') MultiChoice,isnull([Projector],'N') Projector,[QTime],isnull([EmptySeats],0) EmptySeats,[RecCap],[MaxCap],isnull([Parents],'N') Parents,[EmptyRows],[RoomCap],cs.[CreateDate],cs.[CreatedBy],cs.[ModifyDate],cs.[ModifiedBy]"
            cmdText = cmdText + " from contestsettings cs inner join ProductGroup pg on cs.ProductGroupId= pg.ProductGroupId inner join Product p on p.ProductGroupId= pg.ProductGroupId inner join Event e on e.EventId= cs.EventID and cs.ProductId=p.ProductId "
            cmdText = cmdText + " where cs.EventID = " & ddlEvent.SelectedValue & " And CS.ContestYear = " & ddlYear.SelectedValue & " order by ContestYear, EventID, ProductGroupID, ProductID, Phase"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            grdContSet.DataSource = ds.Tables(0)
            grdContSet.DataBind()
        Else
            grdContSet.DataSource = Nothing
        End If
    End Sub
    Protected Sub grdContSet_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdContSet.RowCommand
        If e.CommandName = "Modify" Then
            Dim r As Integer
            For r = 0 To grdContSet.Rows.Count - 1
                grdContSet.Rows(r).BackColor = Nothing
            Next
            lblMsg.Text = ""

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex
            hfRowIndex.Value = RowIndex
            Try
                hfContSetID.Value = gvRow.Cells(1).Text
                Dim productGroupId As Integer = DirectCast(gvRow.FindControl("lblPgId"), Label).Text
                ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(productGroupId))
                ddlProduct.Items.Clear()
                ddlCopyToPrd.Items.Clear()
                Product()
                Dim productId As Integer = DirectCast(gvRow.FindControl("lblPId"), Label).Text
                ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(productId))
                ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByText(gvRow.Cells(3).Text))
                ddlPhase.SelectedIndex = ddlPhase.Items.IndexOf(ddlPhase.Items.FindByValue(gvRow.Cells(6).Text))
                ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByValue(gvRow.Cells(7).Text))
                ddlRounds.SelectedIndex = ddlRounds.Items.IndexOf(ddlRounds.Items.FindByValue(gvRow.Cells(8).Text))
                ddlDuration.SelectedIndex = ddlDuration.Items.IndexOf(ddlDuration.Items.FindByValue(gvRow.Cells(9).Text))
                ddlQuestion.SelectedIndex = ddlQuestion.Items.IndexOf(ddlQuestion.Items.FindByValue(gvRow.Cells(10).Text))
                ddlPub.SelectedIndex = ddlPub.Items.IndexOf(ddlPub.Items.FindByValue(gvRow.Cells(11).Text))
                ddlMultiChoice.SelectedIndex = ddlMultiChoice.Items.IndexOf(ddlMultiChoice.Items.FindByText(gvRow.Cells(12).Text))
                ddlProjector.SelectedIndex = ddlProjector.Items.IndexOf(ddlProjector.Items.FindByText(gvRow.Cells(13).Text))
                ddlQTime.SelectedIndex = ddlQTime.Items.IndexOf(ddlQTime.Items.FindByValue(gvRow.Cells(14).Text))
                ddlEmptySeats.SelectedIndex = ddlEmptySeats.Items.IndexOf(ddlEmptySeats.Items.FindByText(gvRow.Cells(15).Text))
                ddlEmptyRows.SelectedIndex = ddlEmptyRows.Items.IndexOf(ddlEmptyRows.Items.FindByValue(gvRow.Cells(16).Text))

                ddlParents.SelectedIndex = ddlParents.Items.IndexOf(ddlParents.Items.FindByText(gvRow.Cells(17).Text))
                ddlRecCap.SelectedIndex = ddlRecCap.Items.IndexOf(ddlRecCap.Items.FindByValue(gvRow.Cells(18).Text))
                ddlMaxCap.SelectedIndex = ddlMaxCap.Items.IndexOf(ddlMaxCap.Items.FindByValue(gvRow.Cells(19).Text))

                ddlRoomCap.SelectedIndex = ddlRoomCap.Items.IndexOf(ddlRoomCap.Items.FindByValue(gvRow.Cells(20).Text))
                btnAddUpdate.Text = "Update"
                tdCopyTo.Style("visibility") = "visible"
                ddlCopyToPrd.Enabled = False
                btnSubmit.Enabled = False
                tblAddUpd.Visible = True
            Catch ex As Exception
            End Try
        End If
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEvent.SelectedIndexChanged
        FillContestYear()
       
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        ddlProduct.Items.Clear()
        Product()
    End Sub
    Protected Sub btnAddUpdate_Click(sender As Object, e As EventArgs) Handles btnAddUpdate.Click
        Try
            ' Validate
        lblMsg.ForeColor = Color.Red
        If ddlProductGroup.SelectedIndex = 0 Then
            lblMsg.Text = "Select Product Group"
        ElseIf ddlProduct.SelectedIndex = 0 Then
            lblMsg.Text = "Select Product"
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblMsg.Text = "Select Phase"
        ElseIf ddlType.SelectedIndex = 0 Then
            lblMsg.Text = "Select Type"
            ElseIf ddlDuration.SelectedIndex = 0 Then
                lblMsg.Text = "Select Duration"
        Else
            If btnAddUpdate.Text = "Add" Then
                cmdText = " select count(*) from ContestSettings WHERE ContestYear=" & ddlYear.SelectedValue & "  and EventID=" & ddlEvent.SelectedValue & " and ProductGroupID= " & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue
                If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
                    lblMsg.Text = "Duplicate Record Exists!"
                    Exit Sub
                Else
                        cmdText = "insert into ContestSettings (ContestYear,EventID, ProductGroup, ProductGroupID, Product, ProductID, Phase, Type, Rounds, Duration, "
                        cmdText = cmdText & " Questions,Pub, MultiChoice, Projector, QTime, EmptySeats, RecCap, MaxCap, Parents, EmptyRows, RoomCap, CreateDate, CreatedBy ) values"
                    cmdText = cmdText & " ( " & ddlYear.SelectedValue & " ," & ddlEvent.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlProduct.SelectedValue
                    cmdText = cmdText & " , " & ddlPhase.SelectedValue & ",'" & ddlType.SelectedValue & "'," & ddlRounds.SelectedValue & "," & ddlDuration.SelectedValue
                        cmdText = cmdText & " , " & ddlQuestion.SelectedValue & "," & ddlPub.SelectedValue & "," & ddlMultiChoice.SelectedValue & ", " & ddlProjector.SelectedValue & "," & ddlQTime.SelectedValue
                        cmdText = cmdText & ", " & ddlEmptySeats.SelectedValue & "," & ddlRecCap.SelectedValue & "," & ddlMaxCap.SelectedValue & "," & ddlParents.SelectedValue & ", " & ddlEmptyRows.SelectedValue & "," & ddlRoomCap.SelectedValue & ", GETDATE(), " & Session("LoginId") & ")"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    lblMsg.ForeColor = Color.Blue
                    lblMsg.Text = "Record Inserted Successfully."

                End If
            Else
                cmdText = " select count(*) from ContestSettings WHERE ContSetID<>" & hfContSetID.Value & " and  ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ProductGroupID= " & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue
                If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
                    lblMsg.Text = "Duplicate Record Exists!"
                    Exit Sub
                Else
                    cmdText = "UPDATE ContestSettings SET  ProductGroup ='" & ddlProductGroup.SelectedItem.Text & "' ,  ProductGroupID=" & ddlProductGroup.SelectedValue & ", Product ='" & ddlProduct.SelectedItem.Text & "', ProductID =" & ddlProduct.SelectedValue & ", Phase=" & ddlPhase.SelectedValue & ", Type='" & ddlType.SelectedValue & "', Rounds=" & ddlRounds.SelectedValue & ", Duration=" & ddlDuration.SelectedValue
                        cmdText = cmdText & ", Questions=" & ddlQuestion.SelectedValue & ",Pub= " & ddlPub.SelectedValue & ", MultiChoice=" & ddlMultiChoice.SelectedValue & ", Projector=" & ddlProjector.SelectedValue & ", QTime=" & ddlQTime.SelectedValue
                        cmdText = cmdText & " , EmptySeats= " & ddlEmptySeats.SelectedValue & ", RecCap=" & ddlRecCap.SelectedValue & ", MaxCap=" & ddlMaxCap.SelectedValue & ", Parents=" & ddlParents.SelectedValue & ", EmptyRows= " & ddlEmptyRows.SelectedValue & ", RoomCap=" & ddlRoomCap.SelectedValue & ", ModifyDate=GetDate(), ModifiedBy=" & Session("LoginId") & "  WHERE ContSetID =" & hfContSetID.Value & " and ContestYear =" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue
                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    lblMsg.ForeColor = Color.Blue
                    lblMsg.Text = "Record Updated Successfully."
                End If
            End If
            BindContestSettings()
            tblAddUpd.Visible = False
            btnAddUpdate.Text = "Add"
            End If
        Catch ex As Exception
            ' Response.Write("Error on " & btnAddUpdate.Text & " :" & ex.ToString)
        End Try
    End Sub
    Protected Sub btnAddNew_Click(sender As Object, e As EventArgs) Handles btnAddNew.Click
        tblAddUpd.Visible = True
        btnAddUpdate.Text = "Add"
        lblMsg.Text = ""
        Reset()

    End Sub

    Private Sub Reset()
        ddlProductGroup.SelectedIndex = 0
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
        ddlPhase.SelectedIndex = 0
        ddlType.SelectedIndex = 0
        ddlRounds.SelectedIndex = 0
        ddlDuration.SelectedIndex = 0
        ddlQuestion.SelectedIndex = 0
        ddlMultiChoice.SelectedIndex = 1
        ddlProjector.SelectedIndex = 0
        ddlQTime.SelectedIndex = 0
        ddlEmptySeats.SelectedIndex = 0
        ddlEmptyRows.SelectedIndex = 0
        ddlParents.SelectedIndex = 1
        ddlRecCap.SelectedIndex = 0
        ddlMaxCap.SelectedIndex = 0
        ddlRoomCap.SelectedIndex = 0
        Dim r As Integer
        For r = 0 To grdContSet.Rows.Count - 1
            grdContSet.Rows(r).BackColor = Nothing
        Next
        tdCopyTo.Style("visibility") = "hidden"

    End Sub

    Protected Sub btnReplicate_Click(sender As Object, e As EventArgs) Handles btnReplicate.Click
        Try
            cmdText = "select count(*) from ContestSettings where ContestYear=" & ddlProYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue
            If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmMsgToOverwrite();", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CnfmMsg", "ConfirmMsgToReplicate(" & ddlYear.SelectedValue & "," & ddlProYear.SelectedValue & ");", True)
            End If
        Catch ex As Exception
            '  Response.Write("Error on " & btnReplicate.Text & " :" & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        BindContestSettings()
        tblAddUpd.Visible = False
        btnAddUpdate.Text = "Add"
        lblMsg.Text = ""
        Dim r As Integer
        For r = 0 To grdContSet.Rows.Count - 1
            grdContSet.Rows(r).BackColor = Nothing
        Next
    End Sub
    Protected Sub btnConfirmOverwrite_Click(sender As Object, e As EventArgs) Handles btnConfirmOverwrite.Click
        cmdText = "Delete From ContestSettings Where ContestYear=" & ddlProYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue
        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        ReplicateAll()
    End Sub

    Protected Sub btnConfirmReplicate_Click(sender As Object, e As EventArgs) Handles btnConfirmReplicate.Click
        ReplicateAll()
    End Sub
    Private Sub ReplicateAll()

        cmdText = "INSERT INTO ContestSettings (ContestYear, EventId, ProductGroup,ProductGroupID,Product,ProductID,Phase,Type,Rounds,Duration,Questions, MultiChoice, Projector, QTime, EmptySeats, RecCap, MaxCap, Parents, EmptyRows, RoomCap,CreateDate,CreatedBy,ModifyDate,ModifiedBy)"
        cmdText = cmdText & " SELECT " & ddlProYear.SelectedValue & ", EventId, ProductGroup,ProductGroupID,Product,ProductID,Phase,Type,Rounds,Duration,Questions, MultiChoice, Projector, QTime, EmptySeats, RecCap, MaxCap, Parents, EmptyRows, RoomCap,CreateDate,CreatedBy,ModifyDate,ModifiedBy from ContestSettings where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue
        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        lblMsg.ForeColor = Color.Blue
        lblMsg.Text = "Data replicated from " & ddlYear.SelectedValue & " to " & ddlProYear.SelectedValue
    End Sub
 
    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        lblMsg.Text = ""
        ProductGroup()
        BindContestSettings()
        ddlProYear.SelectedIndex = ddlYear.SelectedIndex
    End Sub
    Protected Sub grdContSet_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdContSet.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.BackColor = Nothing
        End If
    End Sub
    Protected Sub btnCopyTo_Click(sender As Object, e As EventArgs) Handles btnCopyTo.Click
        If ddlProductGroup.SelectedIndex <> 0 Then
            ddlCopyToPrd.Enabled = True
            btnSubmit.Enabled = True
      
            ' will load depending on Selected item in Productgroup
            cmdText = "SELECT  Distinct P.ProductID, P.Name Product from Product P INNER JOIN ProductGroup PG on P.ProductGroupId=PG.ProductGroupId where PG.ProductGroupId=" & ddlProductGroup.SelectedValue & " and P.EventId =" & ddlEvent.SelectedValue & " order by  ProductID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlCopyToPrd.DataSource = ds.Tables(0)
            ddlCopyToPrd.DataBind()
            ddlCopyToPrd.Items.Insert(0, "Select Product")
            ddlCopyToPrd.Items(0).Selected = True
        End If
    End Sub



    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        ' Validate
        lblMsg.ForeColor = Color.Red
        If ddlProductGroup.SelectedIndex = 0 Then
            lblMsg.Text = "Select Product Group"
        ElseIf ddlCopyToPrd.SelectedIndex = 0 Then
            lblMsg.Text = "Select Product"
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblMsg.Text = "Select Phase"
        ElseIf ddlType.SelectedIndex = 0 Then
            lblMsg.Text = "Select Type"
        ElseIf ddlDuration.SelectedIndex = 0 Then
            lblMsg.Text = "Select Duration"
        Else
            cmdText = " select count(*) from ContestSettings WHERE ContestYear=" & ddlYear.SelectedValue & "  and EventID=" & ddlEvent.SelectedValue & " and ProductGroupID= " & ddlProductGroup.SelectedValue & " and ProductID=" & ddlCopyToPrd.SelectedValue & " and Phase=" & ddlPhase.SelectedValue
            If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
                lblMsg.Text = "Failed to Copy. Duplicate Record Exists!"
                Exit Sub
            Else
                cmdText = "insert into ContestSettings (ContestYear,EventID, ProductGroup, ProductGroupID, Product, ProductID, Phase, Type, Rounds, Duration, "
                cmdText = cmdText & " Questions, MultiChoice, Projector, QTime, EmptySeats, RecCap, MaxCap, Parents, EmptyRows, RoomCap, CreateDate, CreatedBy ) values"
                cmdText = cmdText & " ( " & ddlYear.SelectedValue & " ," & ddlEvent.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlCopyToPrd.SelectedItem.Text & "'," & ddlCopyToPrd.SelectedValue
                cmdText = cmdText & " , " & ddlPhase.SelectedValue & ",'" & ddlType.SelectedValue & "'," & ddlRounds.SelectedValue & "," & ddlDuration.SelectedValue
                cmdText = cmdText & " , " & ddlQuestion.SelectedValue & "," & ddlMultiChoice.SelectedValue & ", " & ddlProjector.SelectedValue & "," & ddlQTime.SelectedValue
                cmdText = cmdText & ", " & ddlEmptySeats.SelectedValue & "," & ddlRecCap.SelectedValue & "," & ddlMaxCap.SelectedValue & "," & ddlParents.SelectedValue & ", " & ddlEmptyRows.SelectedValue & "," & ddlRoomCap.SelectedValue & ", GETDATE(), " & Session("LoginId") & ")"
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                lblMsg.ForeColor = Color.Blue
                lblMsg.Text = "Successfully Copied."

                BindContestSettings()
                grdContSet.Rows(hfRowIndex.Value).BackColor = ColorTranslator.FromHtml("#EAEAEA")

            End If
        End If
    End Sub


End Class


