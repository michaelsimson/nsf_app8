﻿<%@ Page Language="VB" %>
<%@ import Namespace="Microsoft.ApplicationBlocks.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If Page.IsPostBack = False Then
            If Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("Maintest.aspx")
            Else
                LoadChapter()
            End If
        End If
    End Sub

    Protected Sub btnUnsubscribe_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ' validatr the from email both in dropdown and in textbox depending on the source
        Dim emailAddressMatch As Match = Regex.Match(txtUserId.Text, pattern)
        
        If txtUserId.Text.Length < 3 Then
            lblerr.ForeColor = Color.Red
            lblerr.Text = "Please enter  EMail"
        ElseIf Not emailAddressMatch.Success Then
            lblerr.ForeColor = Color.Red
            lblerr.Text = "Please enter valid EMail"
        Else
            lblerr.Text = ""
            Dim i As Integer = 0
            i = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from newsletter_subscribers  where email_address ='" & txtUserId.Text & "'")
            If i > 0 Then
                lblerr.Text = "Email Successfully Unsubscribed"
                txtUserId.Text = String.Empty
            Else
                lblerr.ForeColor = Color.Red
                lblerr.Text = "Email Address not found"
            End If
        End If
    End Sub
    Private Sub LoadChapter()
        Dim strSql As String
        strSql = "select chapterId, Name, chapterCode from chapter order by state,chapterCode"
        Dim drchapter As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        ddlChapter.DataSource = drchapter
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, "None")
        ddlChapter.Items(0).Selected = True
    End Sub

    Protected Sub btnsubscribe_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ' validatr the from email both in dropdown and in textbox depending on the source
        Dim emailAddressMatch As Match = Regex.Match(txtEmail.Text, pattern)
        If txtName.Text.Length < 2 Then
            lblError.ForeColor = Color.Red
            lblError.Text = "Please Enter Name"
            Exit Sub
        ElseIf txtEmail.Text.Length < 3 Then
            lblError.ForeColor = Color.Red
            lblError.Text = "Please Enter EMail"
            Exit Sub
        ElseIf Not emailAddressMatch.Success Then
            lblError.ForeColor = Color.Red
            lblError.Text = "You must enter an Correct email address"
            Exit Sub
        ElseIf ddlChapter.SelectedItem.Text = "None" Then
            lblError.ForeColor = Color.Red
            lblError.Text = "Please Select a Chapter"
            Exit Sub
        Else
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO newsletter_subscribers (subscriber_name, email_address, chapter, date_added, active_status, pref_format) VALUES     ('" & txtName.Text & "','" & txtEmail.Text & "', '" & ddlChapter.SelectedValue & "', GETDATE(), 'Yes', 'HTML')")
            txtEmail.Text = String.Empty
            txtName.Text = String.Empty
            lblError.Text = "Inserted Successfully"
            ddlChapter.SelectedIndex = 0
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Unsubscribe from Newsletter Subscribers</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border="0" cellpadding ="0" cellspacing = "0" width ="1000">
    <tr><td align="center" colspan="2" >
   <img src="images/trilogo.gif" width="980px" alt ="" />
	</td></tr>
  <tr>
  <td align="left" colspan="2"> &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
    </td> </tr>
        <tr>
    <td align="center" valign="top" >
   
   <table border="0" cellpadding="3" cellspacing="0">
   <tr>
   <td align="center" ><h4>Unsubscribe from Newsletter Subscribers</h4></td>
   </tr> 
   <tr><td align = "center"  > 
     EMail ID <asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="175" MaxLength="50"></asp:textbox>
						</td></tr>
							
							 <tr><td align = "center" > 
                                 <asp:Button ID="btnUnsubscribe" runat="server" Text="Unsubscribe" 
                                     onclick="btnUnsubscribe_Click" /></td></tr> 
							
							 <tr><td align = "center" > 
                                 <asp:Label ID="lblerr" runat="server" ></asp:Label>
                                 </td></tr> 
   </table> 
   </td> 
      <td align="left">
    <table border="0" cellpadding="3" cellspacing="0">
   <tr>
   <td align="center" colspan="2" ><h4>Add Newsletter Subscriber</h4></td>
   </tr> 
    <tr><td align = "left" > Name </td> <td align = "left" >
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox> </td> </tr> 
   <tr><td align = "left" > Enter EMailID </td> <td align = "left" > 
      <asp:textbox id="txtEmail" runat="server" CssClass="SmallFont" width="175" MaxLength="50"></asp:textbox><br>
						</td></tr>
<tr><td align = "left"> Chapter </td> <td align = "left" >
                                <asp:DropDownList ID="ddlChapter" Width="150px" DataTextField="ChapterCode" DataValueField ="ChapterCode" runat="server">
                                </asp:DropDownList>
                                </td> </tr> 
							 <tr><td colspan="2" align = "center" > 
                                 <asp:Button ID="btnsubscribe" runat="server" Text="Subscribe" 
                                     onclick="btnsubscribe_Click" /></td></tr> 
							 
							 <tr><td align = "center" colspan="2" > 
                                 <asp:Label ID="lblError" runat="server" ></asp:Label>
                                 </td></tr> 
   </table>
    </td> </tr> 
   </table> 
    </div>
    <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
