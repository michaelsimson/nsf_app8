<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master"  AutoEventWireup="false" CodeFile="UpdateOrganization.aspx.vb" Inherits="UpdateOrgnization" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
        <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
			    <tr bgcolor="#FFFFFF"  align="center" >
					<td  class="Heading" align="center">Update Organization</td>
				</tr>
				<tr bgcolor="#FFFFFF"  >
				    <td colspan="2">
				        <asp:hyperlink CssClass="btn_02" id="hMainMenu" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				        <asp:hyperlink CssClass="btn_02" id="hlnkOrgList" runat="server" NavigateUrl="~/vieworganizations.aspx">Back to Organization List</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				        <asp:hyperlink CssClass="btn_02" id="hlnkSearch" runat="server" NavigateUrl="~/dbsearchresults.aspx">Back to Search Results</asp:hyperlink>
				    </td>
				</tr>
				<tr>
					<td valign="top" width="30%">
				<table id="tblIndividual" align="center">
				<tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Organization Name</td>
			        <td  ><asp:TextBox runat="server" CssClass="inputBox" ID="txtOrgName" MaxLength="75" Width="300px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOrgName"
                            ErrorMessage="Enter Valid Organization Name"></asp:RequiredFieldValidator></td>
			    </tr>			    
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Contact Person Title</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlTitle" CssClass="inputBox">
			                <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
			                <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
			                <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
			                <asp:ListItem Text="Dr." Value="Dr."></asp:ListItem>
			                <asp:ListItem Text="Prof." Value="Prof."></asp:ListItem>
			            </asp:DropDownList>
			        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >First Name</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtFirstName" CssClass="inputBox"></asp:TextBox>
                        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF"  id="trinv1" runat = "server" visible = "false" >
			        <td  valign="top" align="left" >Middle Initial</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtInitial" CssClass="inputBox"></asp:TextBox>
			        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Last Name</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtLastName" CssClass="inputBox"></asp:TextBox>
                        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Address1</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtAddress1" CssClass="inputBox"></asp:TextBox>
			        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF"  id="trinv2" runat = "server" visible = "false" >
			        <td  valign="top" align="left" >Address2</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtAddress2" CssClass="inputBox"></asp:TextBox>
			        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >City</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtCity" CssClass="inputBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select Valid City" ControlToValidate="txtCity"></asp:RequiredFieldValidator></td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >State</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlState" CssClass="inputBox">
			            </asp:DropDownList>
                        <asp:RequiredFieldValidator ControlToValidate="ddlState" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select Valid State"></asp:RequiredFieldValidator></td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Zip Code</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtZipCode" CssClass="inputBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtZipCode" runat="server" ErrorMessage="Enter Valid ZIP Code"></asp:RequiredFieldValidator></td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Country</td>
			        <td ><asp:dropdownlist AutoPostBack="true" id="ddlCountry" runat="server" CssClass="inputBox">
							<asp:ListItem Value="" Selected="True">Select Country</asp:ListItem>
							<asp:ListItem Value="United States">United States</asp:ListItem>							
							<asp:ListItem Value="Canada">Canada</asp:ListItem>	
							<asp:ListItem Value="India">India</asp:ListItem>							
						</asp:dropdownlist>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="ddlCountry" runat="server" ErrorMessage="Select Valid Country"></asp:RequiredFieldValidator></td>
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Phone Number</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtPhone" CssClass="inputBox"></asp:TextBox>
                        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Fax Number</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtFax" CssClass="inputBox"></asp:TextBox>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Email</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtEmail" CssClass="inputBox"></asp:TextBox>
                        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Secondary Email</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtEmail2" CssClass="inputBox"></asp:TextBox>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Matching Gift</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlMatchingGift" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Send E-Mail</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlSendEmail" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >NSF Chapter</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlNSFChapter" CssClass="inputBox">			                
			            </asp:DropDownList>
			            
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="ddlNSFChapter" runat="server" ErrorMessage="Select Valid Chapter" InitialValue="0"></asp:RequiredFieldValidator></td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Send News Letter</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlSendNewsLetter" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Generate Mailing Label</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlMailingLabel" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Send Receipt</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlSendReceipt" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Referred By</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtReferredBy" CssClass="inputBox"></asp:TextBox>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Liaison Person</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtLiaisonPerson" CssClass="inputBox"></asp:TextBox>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >IRS Cat</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlIRSCat" CssClass="inputBox">
			                <asp:ListItem Text="Profit" Value="Profit"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,501(c)(3)" Value="Non-proft,501(c)(3)"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,PAC" Value="Non-proft,PAC"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,Other" Value="Non-proft,Other"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			    <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Organization Type</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlBusinessType" CssClass="inputBox">
			            					   <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>

			                <asp:ListItem Text="NRI,Business" Value="NRI,Business"></asp:ListItem>
			                <asp:ListItem Text="NRI, Ethnic" Value="NRI, Ethnic"></asp:ListItem>
			                <asp:ListItem Text="NRI, Professional" Value="NRI, Professional"></asp:ListItem>
			                <asp:ListItem Text="NRI, Other" Value="NRI, Other"></asp:ListItem>
			                <asp:ListItem Text="Press, India" Value="Press, India"></asp:ListItem>
			                <asp:ListItem Text="Press, NRI" Value="Press, NRI"></asp:ListItem>
			                <asp:ListItem Text="Press, US" Value="Press, US"></asp:ListItem>
			                <asp:ListItem Text="Religious, Temples" Value="Religious, Temples"></asp:ListItem>
			                <asp:ListItem Text="Religious, Other" Value="Religious, Other"></asp:ListItem>
					    <asp:ListItem Text="University_College_School" Value="University_College_School"></asp:ListItem>
					    <asp:ListItem Text="Corporation_LLC" Value="Corporation_LLC"></asp:ListItem>
					    <asp:ListItem Text="United Way" Value="United Way"></asp:ListItem>
					    <asp:ListItem Text="Other Non-Profit" Value="Other Non-Profit"></asp:ListItem>
			                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
			            </asp:DropDownList>
			              &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                            ControlToValidate="ddlBusinessType" ErrorMessage="Select Organization Type" 
                            InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator></td>			
			       		        
			    </tr>
			     <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Venue</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlVenue" CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			     <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Sponsor</td>
			        <td  >
			            <asp:DropDownList runat="server" ID="ddlSponsor"  CssClass="inputBox">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No"></asp:ListItem>
			            </asp:DropDownList>
			        </td>			        
			    </tr>
			     <tr bgcolor="#FFFFFF">
			        <td  valign="top" align="left" >Website</td>
			        <td  >
			            <asp:TextBox runat="server" ID="txtWebSite" CssClass="inputBox"></asp:TextBox>
			        </td>			        
			    </tr>
			    	<tr bgcolor="#FFFFFF" id="TrGiftURL" runat="server">
					<td  valign="top" align="left">Matching_Gift_URL:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiFtURL" runat="server" CssClass="inputBox" MaxLength="75" Width="400px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftAccNo" runat="server">
					<td  valign="top" align="left" >Matching_Gift_Account_Number:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftAccNo" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftUserID" runat="server">
					<td  valign="top" align="left" >Matching_Gift_User_Id:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftUserId" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftPwd" runat="server">
					<td  valign="top" align="left">Matching_Gift_Password:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftPwd" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
			    
			     <tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center" colSpan="2">
					   <asp:button id="btnUpdate" runat="server" CssClass="FormButton" Text="Update"></asp:button>    <br />
                        <asp:Label ID="lblErr" runat="server"  ForeColor="Red" ></asp:Label>                    
					</td>
				</tr>
				<tr><td><asp:Label ID="lblError" runat="server"></asp:Label></td></tr>
			</table>
				</td> 
			</tr> 
			</table> 
    <asp:HiddenField ID="HdnChapterID" runat="server" />
</asp:Content>



 
 
 