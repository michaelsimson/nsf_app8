Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Collections
Imports System.Globalization

Partial Class CoachingClassesReport
    Inherits System.Web.UI.Page
    Dim editFlag As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))

            ''Options for Volunteer Name Selection
            If Session("RoleID") = 88 Then 'Coach can schedule for his own Sessions.
                txtName.Visible = True
                btnSearch.Visible = False
                btnClear.Visible = False
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID from IndSpouse I inner join Volunteer V On V.MemberID = I.AutoMemberID where V.RoleId in (" & Session("RoleID") & ") and V.MemberID=" & Session("LoginID"))
                If ds.Tables(0).Rows.Count > 0 Then
                    txtName.Text = ds.Tables(0).Rows(0)("Name")
                    hdnMemberID.Value = ds.Tables(0).Rows(0)("MemberID")
                End If
            ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
                If Request.QueryString("Role") = "Coach" Then
                    txtName.Visible = False
                    btnSearch.Visible = False
                    btnClear.Visible = False
                Else
                    txtName.Visible = True
                    btnSearch.Visible = True
                    btnClear.Visible = True
                End If
                ddlVolName.Visible = True
                ddlVolName.Visible = True

                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.LastName,I.FirstName")
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVolName.DataSource = ds
                    ddlVolName.DataBind()
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlVolName.Items.Insert(0, "Select Volunteer")
                        ddlVolName.SelectedIndex = 0
                    End If
                Else
                    lblerr.Text = "No Volunteers present for Calendar Sign up"
                End If

            Else
                txtName.Visible = True
                ddlVolName.Visible = False
                btnSearch.Visible = True
                btnClear.Visible = True
            End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            'If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
            'LoadProductGroup()
            'Else
            If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                    'more than one 
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim i As Integer
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If prd.Length = 0 Then
                            prd = ds.Tables(0).Rows(i)(1).ToString()
                        Else
                            prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                        End If

                        If Prdgrp.Length = 0 Then
                            Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                        Else
                            Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                        End If
                    Next
                    lblPrd.Text = prd
                    lblPrdGrp.Text = Prdgrp
                    'LoadProductGroup()
                Else
                    'only one
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    If ds.Tables(0).Rows.Count > 0 Then
                        prd = ds.Tables(0).Rows(0)(1).ToString()
                        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                    End If
                    'LoadProductGroup()
                End If
            End If

            LoadEvent(ddlEvent)
            LoadWeekdays(ddlWeekDays)
            LoadDisplayTime(ddlDisplayTime, False)
            LoadCapacity(ddlMaxCapacity)
            ' LoadGrid()
            'lblerr.Text = ""
        End If
    End Sub
    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                ddlObject.Enabled = True
            Else
                ddlObject.Enabled = False
                If ddlProductGroup.Items.Count < 1 Then
                    LoadProductGroup()
                End If
            End If
            ddlObject.SelectedIndex = 0
        Else
            lblerr.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Public Sub LoadDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "8:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadWeekdays(ByVal ddlObject As DropDownList)
        'For i As Integer = 0 To 6
        'items.Add(new ListItem("Item 2", "Value 2"));
        ddlObject.Items.Add(New ListItem("Monday", "0"))
        ddlObject.Items.Add(New ListItem("Tuesday", "1"))
        ddlObject.Items.Add(New ListItem("Wednesday", "2"))
        ddlObject.Items.Add(New ListItem("Thursday", "3"))
        ddlObject.Items.Add(New ListItem("Friday", "4"))
        ddlObject.Items.Add(New ListItem("Saturday", "5"))
        ddlObject.Items.Add(New ListItem("Sunday", "6"))

        'ddlObject.Items.Add(New ListItem(WeekdayName(i + 1), i))
        ' ddlWeekDays.Items.Add(WeekdayName(i))
        'Next
    End Sub

    Private Sub LoadCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 10 To 100 'Step 5
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
        ddltemp.SelectedIndex = 2
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblerr.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
                LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & "  order by P.ProductID " 'and P.Status='O'
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    LoadGrid()
                End If
            End If
        Catch ex As Exception
            lblerr.Text = lblerr.Text & "<br>" & ex.ToString
        End Try
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String)
        ddlObject.Items.Clear()
        ddlObject.Enabled = True
        If ProductGroup.Contains("SAT") Then ' ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("Junior", 1))
            ddlObject.Items.Insert(2, New ListItem("Senior", 2))
        ElseIf ProductGroup.Contains("Universal Values") Then
            ddlObject.Enabled = False
        Else
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("Beginner", 1))
            ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
            ddlObject.Items.Insert(3, New ListItem("Advanced", 3))
        End If
    End Sub

    Function CheckVolunteer() As Boolean
        lblerr.Text = ""
        If ddlVolName.Visible = False And txtName.Visible = True And txtName.Text = "" Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = False And ddlVolName.Visible = True And ddlVolName.SelectedIndex = 0 Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = True And ddlVolName.Visible = True Then
            If ddlVolName.SelectedIndex = 0 And txtName.Text.Trim.ToString() = "" Then
                lblerr.Text = "Please select Volunteer "
            ElseIf ddlVolName.SelectedIndex > 0 And txtName.Text <> "" Then
                lblerr.Text = "Please select one of the options for Volunteer"
            Else
                lblerr.Text = ""
            End If
        End If

        If lblerr.Text = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            If CheckVolunteer() = False Then
                lblerr.Text = lblerr.Text '"Please select Volunteer"
            ElseIf ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblerr.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblerr.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblerr.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblerr.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblerr.Text = "Please select Product"
                'ElseIf ddlLevel.Enabled = True And ddlLevel.SelectedItem.Text = "Select" Then
                'If ddlLevel.SelectedItem.Text = "Select" Then
                'lblerr.Text = "Please select Level"
                'End If
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Enter Coaching time"
            ElseIf ddlMaxCapacity.SelectedValue < 2 Then
                lblerr.Text = "Please select Maximum Capacity"
            Else
                lblerr.Text = ""
                Dim strSQL As String
                Dim Level As String = ""
                If ddlLevel.Enabled = True Then
                    If ddlLevel.SelectedItem.Text = "Select" Then
                        lblerr.Text = "Please select Level"
                        Exit Sub
                    End If
                    Level = ddlLevel.SelectedItem.Text
                End If
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "Select Count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Eventyear=" & ddlEventYear.SelectedValue & "and EventID =" & ddlEvent.SelectedValue & " and Day='" & ddlWeekDays.SelectedValue & "' and Time='" & ddlDisplayTime.SelectedItem.Text & "'") > 0 Then
                    lblerr.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
                ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue & IIf(Level <> "", " and Level = '" & Level & "'", "") & " AND Phase=" & ddlPhase.SelectedValue & " AND SessionNo=" & ddlSession.SelectedValue) < 3 Then '< 1 Then
                    strSQL = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Phase,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES ("
                    strSQL = strSQL & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & "," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlPhase.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "'," & IIf(Level <> "", "'" & Level & "'", "NULL") & "," & ddlSession.SelectedValue & ",'" & ddlWeekDays.SelectedItem.Text & "','" & ddlDisplayTime.SelectedValue & "'," & ddlPref.SelectedValue & "," & ddlMaxCapacity.SelectedValue & "," & Session("loginID") & ",Getdate())"
                    If SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strSQL) > 0 Then
                        'clear()
                        LoadGrid()
                        lblerr.Text = "Inserted Successfully"
                    End If
                Else
                    lblerr.Text = "Coach with Same Product, Phase,Level and Session already found"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub LoadGrid()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            StrSql = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, I.FirstName, I.LastName, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo,Case When C.Day  ='Saturday' Then '1' Else Case When C.Day  ='Sunday' then '2' Else Case When C.Day  ='Monday' then '3' Else Case When C.Day  ='Tuesday' then '4' Else Case When C.Day  ='Wednesday' then '5' Else Case When C.Day  ='Thursday' then '6' Else '7' end end end end  End  end as D, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time, CONVERT(varchar(15),CAST(TIME AS TIME),108) as T1,C.Accepted,C.Preference, I.Email, I.Hphone,I.Cphone,I.Wphone FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedIndex > 0, " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
            StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")

            If Request.QueryString("Role") = "Coach" Then
                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
            End If

            If ddlAccepted.SelectedValue = 1 Then
                StrSql = StrSql & " and C.Accepted = 'y'"
            End If

            If ddlLevel.SelectedIndex > 0 Then
                StrSql = StrSql & " and C.Level='" & ddlLevel.SelectedItem.Text & "'"
            End If

            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then

                StrSql = StrSql & " order by D, C.Time, C.ProductGroupID, C.ProductID, I.LastName, I.FirstName " 'order by I.LastName,I.FirstName"

                'Response.Write(StrSql)
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds

                If ds.Tables(0).Rows.Count > 0 Then
                    lblerr.Text = ""
                    DGCoach.Visible = True
                    DGCoach.DataSource = ds
                    DGCoach.DataBind()
                    DGCoach.CurrentPageIndex = 0
                    DGCoach.Visible = True
                    bttnExport.Enabled = True
                    'DGCoach.Columns(13).Visible = False
                    'DGCoach.Columns(16).Visible = False
                    'DGCoach.Columns(20).Visible = False
                    'DGCoach.Columns(21).Visible = False
                    'DGCoach.Columns(22).Visible = False
                    'DGCoach.Columns(23).Visible = False

                    If Session("RoleId").ToString = "88" Then
                        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from volunteer where RoleId = 88 and TeamLead ='Y' and [National]='Y' and MemberID =" & Session("LoginId").ToString) > 0) Then
                            DGCoach.Columns(0).Visible = False
                            DGCoach.Columns(1).Visible = False
                        End If
                    End If
                    DGCoach.Columns(3).Visible = False
                    'If Request.QueryString("Role") = "Admin" Then
                    '    DGCoach.Columns(15).Visible = True
                    'Else
                    '    DGCoach.Columns(15).Visible = False
                    'End If
                Else
                    bttnExport.Enabled = False
                    DGCoach.Visible = False
                    If Page.IsPostBack = True Then
                        lblerr.Text = "No schedule available"

                    End If
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    'Public Sub loadGrid(ByVal blnReload As Boolean)
    '    'lblerr.Text = ""
    '    Dim ds As DataSet
    '    Try
    '        Dim StrSql As String = ""
    '        StrSql = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day, Time,C.Accepted,C.Preference FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedIndex > 0, " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")

    '        If blnReload = True Then
    '            Try
    '                If (ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0) Or (ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0) Then
    '                    StrSql = StrSql & " and C.MemberID= " & IIf(Request.QueryString("Role") = "Coach", IIf(ddlVolName.Visible = True, ddlVolName.SelectedValue, Session("LoginID")), IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue)) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ElseIf ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
    '                    StrSql = StrSql & " AND C.EventID=" & ddlEvent.SelectedValue & " AND C.Phase=" & ddlPhase.SelectedValue & " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue & IIf(ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "") & IIf(Request.QueryString("Role") = "Coach", " and C.MemberID=" & Session("LoginID"), "") & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                End If

    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds

    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If

    '            Catch se As SqlException
    '                lblerr.Text = StrSql 'se.ToString()
    '                Return
    '            End Try
    '        Else
    '            If Request.QueryString("Role") = "Coach" Or Request.QueryString("Role") = "Admin" Then
    '                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If
    '            Else
    '                ds = CType(Session("volDataSet"), DataSet)
    '                DGCoach.DataSource = Nothing 'ds
    '                DGCoach.DataBind()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.ToString())
    '    End Try
    'End Sub

    Protected Sub DGCoach_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim SignUpID As Integer
            SignUpID = CInt(e.Item.Cells(2).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM CalSignUp WHERE SignUpID =" + CStr(SignUpID))
                If rowsAffected > 0 Then
                    lblerr.Text = "Deleted Successfully."
                End If
            Catch se As SqlException
                lblerr.Text = "Error: while deleting the record"
                Return
            End Try

            LoadGrid()

            DGCoach.EditItemIndex = -1
        End If
    End Sub

    Protected Sub DGCoach_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.EditCommand
        DGCoach.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        Dim page As Integer = DGCoach.CurrentPageIndex
        Dim pageSize As Integer = DGCoach.PageSize
        Dim currentRowIndex As Integer
        lblerr.Text = ""
        lblError.Text = ""
        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If
        'DGCoach.Columns(3).Visible = True
        'DGCoach.Columns(4).Visible = False
        'DGCoach.Columns(5).Visible = False
        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr
        editFlag = True
        LoadGrid()
    End Sub

    Private Sub DGCoach_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGCoach.ItemCreated
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub DGCoach_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.UpdateCommand
        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim SignUpID As Integer
        Dim MemberID As Integer
        Dim EventID As Integer
        Dim EventYear As Integer
        Dim Capacity As Integer
        Dim SessionNo As Integer
        Dim ProductID As Integer
        Dim Level As String
        Dim Day As String
        Dim Time As String
        Dim Preference As Integer
        Dim Accepted As String
        Try
            SignUpID = CInt(e.Item.Cells(2).Text)
            'MemberID = CInt(CType(e.Item.FindControl("txtMemberID"), TextBox).Text)
            EventYear = CInt(CType(e.Item.FindControl("ddlDGEventYear"), DropDownList).SelectedValue)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                ProductID = dr.Item("productID")
                MemberID = dr.Item("MemberID")
                EventID = dr.Item("EventID")
            End If
            If CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Enabled = True Then
                Level = CStr(CType(e.Item.FindControl("ddlDGLevel"), DropDownList).SelectedItem.Text)
            Else
                Level = ""
            End If
            SessionNo = CInt(CType(e.Item.FindControl("ddlDGSessionNo"), DropDownList).SelectedItem.Value)
            Capacity = CInt(CType(e.Item.FindControl("ddlDGMaxCapacity"), DropDownList).SelectedItem.Value)
            Time = CStr(CType(e.Item.FindControl("ddlDGTime"), DropDownList).SelectedItem.Text)
            Day = CStr(CType(e.Item.FindControl("ddlDGDay"), DropDownList).SelectedItem.Text) 'Value)
            Preference = CInt(CType(e.Item.FindControl("ddlDGPreferences"), DropDownList).SelectedItem.Value)
            Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)

            ',ProductGroupID = " & ProductGroupID & ",ProductGroupCode = '" & getProductGroupcode(ProductGroupID) & "'
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and Eventyear=" & EventYear & " and EventID=" & EventID & " and Day='" & Day & "' and Time='" & Time & "' and  SignUpID not in (" & SignUpID & ")") > 0 Then
                lblerr.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
            ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ")") < 3 Then
                sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "'" & IIf(DGCoach.Columns(13).Visible = True, ",Accepted='" & Accepted & "'", "") & " ,Preference=" & Preference & ",MaxCapacity =" & Capacity & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & SignUpID
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr) > 0 Then
                    lblerr.Text = "Updated Successfully"
                End If
                DGCoach.EditItemIndex = -1
                LoadGrid()

            Else
                lblerr.Text = "Coach with same product, Phase, Level and Session are already found"
            End If
        Catch ex As SqlException
            'ex.message
            'Response.Write(ex.ToString())
            lblerr.Text = "<br> Error:updating the record" + ex.ToString
            Return
        End Try

    End Sub

    Protected Sub DGCoach_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        lblerr.Text = ""
        lblError.Text = ""
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGCoach.CurrentPageIndex
            Dim pageSize As Integer = DGCoach.PageSize
            'DGCoach.Columns(9).Visible = False
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            'DGCoach.Columns(3).Visible = False
            'DGCoach.Columns(4).Visible = True
            'DGCoach.Columns(5).Visible = True
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DGCoach.EditItemIndex = -1

            LoadGrid()
        Else
            Return
        End If
    End Sub
    Protected Sub DGCoach_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGCoach.PageIndexChanged
        DGCoach.CurrentPageIndex = e.NewPageIndex
        DGCoach.EditItemIndex = -1
        LoadGrid()
    End Sub

    Public Sub ddlDGEventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            ddlTemp.Items.Insert(0, Convert.ToString(Now.Year() + 1))
            ddlTemp.Items.Insert(1, Convert.ToString(Now.Year()))
            ddlTemp.Items.Insert(2, Convert.ToString(Now.Year() - 1))
            '999
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("EventYear")))
        End If

    End Sub

    Public Sub ddlDGMember_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.FirstName"
        Else
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName"
        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False
        End If
    End Sub

    Public Sub ddlDGMember_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub
    Public Sub ddlDGEvent_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim EventCode As String = ""
        Dim EventId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventCode") Is DBNull.Value) Then
                EventCode = dr.Item("EventCode")
                EventId = dr.Item("EventId")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadEvent(ddlTemp)
            Dim rowEventID As Integer = 0
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(EventId))
        Else
            Return
        End If

    End Sub
    Public Sub ddlDGPhase_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Phase As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Phase") Is DBNull.Value) Then
                Phase = dr.Item("Phase")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Phase))
        End If
    End Sub

    Public Sub ddlDGProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProductGroupID As Integer = 0
        Dim rowEventID As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Try
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If

            Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=" & rowEventID & "  order by ProductGroupID"
            Dim drproductGroupid As SqlDataReader
            drproductGroupid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductGroupid
            ddlTemp.DataBind()
            If (Not dr.Item("productGroupID") Is DBNull.Value) Then
                rowProductGroupID = dr.Item("productGroupID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductGroupID))
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub ddlDGProduct_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProdGroupId As Integer = 0
        Dim rowEventID As Integer = 0

        Try
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
                rowProdGroupId = dr.Item("productGroupId")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            Dim strSql As String = "Select ProductID, Name from Product where EventID in (" & rowEventID & ") and ProductGroupID =" & rowProdGroupId & " order by ProductID"
            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductid
            ddlTemp.DataBind()
            Dim rowProductID As Integer = 0
            If (Not dr.Item("productID") Is DBNull.Value) Then
                rowProductID = dr.Item("productID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("productID") = ddlTemp.SelectedValue
            dr.Item("ProductCode") = getProductcode(ddlTemp.SelectedValue)
            'Session("editRow") = dr
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim level As String = ""
        Dim ProductGroupCode As String = ""

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Level") Is DBNull.Value) Then
                level = dr.Item("Level")
            End If
            If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
                ProductGroupCode = dr.Item("ProductGroupCode")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadLevel(ddlTemp, ProductGroupCode)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(level))
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGSessionNo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SessionNo As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("SessionNo") Is DBNull.Value) Then
                SessionNo = dr.Item("SessionNo")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(SessionNo))
        Else
            Return
        End If
    End Sub
    Public Sub ddlDGDay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Day As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If
            LoadWeekdays(ddlTemp)
            'For i As Integer = 1 To 7
            '    ddlTemp.Items.Add(WeekdayName(i))
            'Next
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Day))
    End Sub

    Public Sub ddlDGTime_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            LoadDisplayTime(ddlTemp, True)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
        End If
    End Sub

    Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Accepted As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Accepted") Is DBNull.Value) Then
                Accepted = dr.Item("Accepted")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGPreferences_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Preference As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Preference") Is DBNull.Value) Then
                Preference = dr.Item("Preference")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Preference))
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGMaxCapacity_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            Dim li As ListItem
            Dim i As Integer
            For i = 10 To 100 'Step 5
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("MaxCapacity")))
        End If
    End Sub
    Private Sub clear()
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProductGroup.Enabled = False
        ddlProduct.Items.Clear()
        ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlPhase.SelectedIndex = 0
        ddlSession.SelectedIndex = 0
        ddlWeekDays.SelectedIndex = 0
        ddlDisplayTime.SelectedIndex = 0

        If ddlLevel.Enabled = True Then
            ddlLevel.SelectedIndex = ddlLevel.Items.IndexOf(ddlLevel.Items.FindByValue(0))
        End If
        ddlMaxCapacity.SelectedIndex = ddlMaxCapacity.Items.IndexOf(ddlMaxCapacity.Items.FindByText("20"))
        ddlPref.SelectedIndex = 0

    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlLevel.Items.Clear()
        DGCoach.Visible = False
        LoadEvent(ddlEvent)

        'If ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex <> 0 Then
        'LoadProductGroup()
        'Else
        '   LoadProductGroup()
        'End If
        'If ddlVolName.SelectedIndex <> 0 Then
        '    LoadGrid()
        'End If

        'LoadGrid()

    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        'lblerr.Text = ""
        'lblError.Text = ""
        'If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
        '    lblerr.Text = "Please select Valid Product"
        'Else
        '    LoadGrid()
        'End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadProductID()
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
            ' LoadGrid()
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblerr.Text = "Please select Valid Product"
        Else
            '   LoadGrid()
        End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        LoadGrid()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  I.lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_SelectCalSignupVolunteers]", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub
    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtName.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            hdnMemberID.Value = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
            lblIndSearch.Visible = False
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignup Where MemberID=" & hdnMemberID.Value & "") > 0 Then
                lblerr.Text = "The member already exists for Calendar Signup"
            End If
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIndClose.Click
        pIndSearch.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtName.Text = ""
        If ddlVolName.Items.Count > 0 Then
            ddlVolName.SelectedIndex = 0
        End If
    End Sub

    'Protected Sub ddlAccepted_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccepted.SelectedIndexChanged
    '    lblerr.Text = ""
    '    lblError.Text = ""
    '    LoadGrid()
    'End Sub


    Protected Sub ddlAccepted_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccepted.SelectedIndexChanged
        'lblerr.Text = ""
        'lblError.Text = ""
        'LoadGrid()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        'lblerr.Text = ""
        'lblError.Text = ""
        'LoadGrid()
    End Sub



    Protected Sub bttnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bttnSubmit.Click
        lblerr.Text = ""

        LoadGrid()
        'ddlProduct.Enabled = False
    End Sub

    Protected Sub ddlFname_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Firstname , I.AutoMemberId as MemberID,I.FirstName as MemberID from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.FirstName"
        Else
            strSql = "select Distinct I.Firstname, I.AutoMemberId as MemberID, I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName"
        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False
        End If
    End Sub

    Protected Sub ddlFname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Protected Sub ddlLname_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Lastname, I.AutoMemberId as MemberID from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.Lastname"
        Else
            strSql = "select Distinct I.LastName, I.AutoMemberId as MemberID from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.Lastname"
        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False

        End If
    End Sub


    Protected Sub ddlLname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub


    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub
    Protected Sub ExportToExcel()
        Try
           
            Dim StrSql As String = ""
            StrSql = "SELECT C.SignUpID,C.MemberID,I.FirstName, I.LastName, C.EventYear,C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase, PG.Name as ProductGroupCode,P.ProductCode as ProductCode, C.[Level],C.SessionNo,Case When C.Day  ='Saturday' Then '1' Else Case When C.Day  ='Sunday' then '2' Else Case When C.Day  ='Monday' then '3' Else Case When C.Day  ='Tuesday' then '4' Else Case When C.Day  ='Wednesday' then '5' Else Case When C.Day  ='Thursday' then '6' Else '7' end end end end  End  end as D,  C.Day as Day ,CONVERT(varchar(15),CAST(TIME AS TIME),108) as Time,  C.Vroom, C.UserID, C.Pwd, C.Accepted,C.Preference,C.MaxCapacity, I.Email, I.Hphone as Home,I.Cphone as Cell,I.Wphone as Office FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedIndex > 0, " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
            StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")

            If Request.QueryString("Role") = "Coach" Then
                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
            End If

            If ddlAccepted.SelectedValue = 1 Then
                StrSql = StrSql & " and C.Accepted = 'y'"
            End If

            If ddlLevel.SelectedIndex > 0 Then
                StrSql = StrSql & " and C.Level='" & ddlLevel.SelectedItem.Text & "'"
            End If

            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                StrSql = StrSql & " order by D, C.Time, C.ProductGroupID, C.ProductID, I.LastName, I.FirstName " 'order by I.LastName,I.FirstName"
            End If
            Dim ds1 As DataSet
            'Response.Write(StrSql)
            ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
            'Dim ds1 As DataSet = Session("volDataSet")
            Dim dt As DataTable = ds1.Tables(0)
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", _
                    "attachment;filename=CoachingClassReport.csv")
            Response.Charset = ""
            Response.ContentType = "application/text"

            Dim sb As New StringBuilder()
            For k As Integer = 0 To dt.Columns.Count - 1
                'add separator
                sb.Append(dt.Columns(k).ColumnName + ","c)
            Next
            'append new line
            sb.Append(vbCr & vbLf)
            For i As Integer = 0 To dt.Rows.Count - 1
                For k As Integer = 0 To dt.Columns.Count - 1
                    'add separator
                    sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
                Next
                'append new line
                sb.Append(vbCr & vbLf)
            Next
            Response.Output.Write(sb.ToString())
            Response.Flush()
            Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub bttnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bttnExport.Click
        ExportToExcel()
    End Sub

    Protected Sub DGCoach_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGCoach.SelectedIndexChanged

    End Sub

    Protected Sub DGCoach_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGCoach.PreRender
    End Sub
End Class
