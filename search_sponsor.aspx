<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="search_sponsor.aspx.vb" Inherits="search_sponcer" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table cellspacing="1" cellpadding="2" width="90%"  align="center" border="0" >
        <tr>
            <td colspan="4" >
                              
                <asp:LinkButton ID="hplback" CssClass="btn_02" OnClick="hplback_Click"  runat="server">Back To Main Menu</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;    
                
                <asp:HyperLink ID="hypl1" runat="server"  CssClass="btn_02"  NavigateUrl="~/Registration.aspx?Type=Individual">Add Sponsor-Individual</asp:HyperLink>&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink2" runat="server"  CssClass="btn_02"  NavigateUrl="~/addorganization.aspx">Add Sponsor-Organization</asp:HyperLink>&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink3" runat="server" CssClass="btn_02"  NavigateUrl="~/addorganization.aspx">Add Venue</asp:HyperLink>&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" class="Heading" colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td class="Heading" colspan="4" align="center" >Search for Sponsor/Venue</td>
        </tr>
        <tr>
            <td align="center" colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center" >Search by one or more attributes given below.  Only enter the data you know for sure.  Avoid creating duplicates.</td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td>
                Email:</td>
            <td>
                <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>    
            </td>
            <td>First Name/Organization:</td>
            <td><asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox>    </td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td>
                <asp:TextBox ID="txtlastname" runat="server" ></asp:TextBox>            
            </td>
            <td>
                Street:</td>
            <td><asp:TextBox ID="txtstreet" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                City:</td>
            <td><asp:TextBox ID="txtcity" runat="server"></asp:TextBox></td>
            <td>
                State:</td>
            <td><asp:DropDownList ID="ddlstate" runat="server">
                
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Zip:</td>
            <td><asp:TextBox ID="txtzip" runat="server"></asp:TextBox></td>
             <td>NSF Chapter:</td>
            <td><asp:DropDownList ID="ddlchapter" runat="server" >
        
            </asp:DropDownList></td>
        </tr>           
        <tr align="center">
            <td colspan="4" align="center" >
            <asp:GridView ID="gvIndSpouse" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None"  Caption="Individual / Spouse" CssClass="GridStyle" CaptionAlign="Top" PageSize="5" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:BoundField DataField ="AutoMemberID" HeaderText="Member ID" />
                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/Registration.aspx?Id={0}"  HeaderText="Update Profile" Text="Update Profile" />                
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
                <asp:BoundField DataField="ReferredBy" HeaderText="ReferredBy" />                
                <asp:BoundField DataField="Chapter" HeaderText="Chapter" />
            </Columns>
            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        </asp:GridView>
                <asp:Button CssClass="FormButton" ID="btnsearch" runat="server" Text="Search" />
            </td>
        </tr>
    </table>
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
        <tr>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:GridView ID="gvOrg" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" Caption="Organization" CssClass="GridStyle"  CaptionAlign="Top" PageSize="5" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" CellPadding="3" Height="133px">
            <Columns>
                <asp:BoundField DataField="AutoMemberID" HeaderText="Member ID" />
                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/UpdateOrganization.aspx?MemberId={0}"  HeaderText="Update Profile" Text="Update Profile" />
                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/ViewDonations.aspx?Id={0}&amp;Type=OWN"  HeaderText="Add/Update Donations" Text="Add/Update Donations" />                
                <asp:BoundField DataField="ORGANIZATION_NAME" HeaderText="Organization" />
                <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name" />
                <asp:BoundField DataField="MIDDLE_INITIAL" HeaderText="Initial" />
                <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name" />                
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                 <asp:BoundField DataField="Phone" HeaderText="Phone" />
                 <asp:BoundField DataField="Referred_By" HeaderText="Referred By" />
                 <asp:BoundField DataField="NSF_Chapter" HeaderText="NSF Chapter" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" ForeColor="White" />
        </asp:GridView>
            </td>
        </tr>
        <tr>
            <td  class="Heading" colspan="4"><asp:Label runat="server" ID="lblMessage"></asp:Label></td>
        </tr>
    </table>    
    
</asp:Content>



 
 
 