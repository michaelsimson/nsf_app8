'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/Registration.aspx  
'   Author    : NSF
'   Contact   : NSF
'
'	Desc:	This page enables the user to Register them as Volunteer  
'           to the NSF.
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetVolunteer
'           2) NSF\usp_GetVolunteerSpouse
'           3) NSF\usp_Insert_Volunteer
'           4) NSF\
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Web.SessionState
Imports nsf.Entities
Imports nsf.Data.SqlClient

Namespace VRegistration


Partial Class Registration
        Inherits System.Web.UI.Page
        'Implements System.Web.UI.ICallbackEventHandler
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtSecEmailSp As System.Web.UI.WebControls.TextBox
    Protected WithEvents ckbAddressSp As System.Web.UI.WebControls.CheckBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
        Dim clientData As String
        ' Dim SessobjIndSpouse As IndSpouse10
        '  Dim SessobjSpouse As IndSpouse10
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
                Server.Transfer("login.aspx")
            End If
            If Session("LoginEmail") Is Nothing Then
                Server.Transfer("login.aspx")
            End If
            lblMsg.Text = String.Empty
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If

            Dim objIndSpouse As New IndSpouse10
            Dim objSpouse As IndSpouse10
            Dim nextPage As Boolean

            'ClientScript.RegisterOnSubmitStatement([GetType], "Form1", "if (this.submitted) return false; this.submitted = true; return true;")
            If Session("nextPageName") Is Nothing Then
                Session("nextPageName") = "MainChild.aspx"
            End If

            hide1.Visible = False
            hide2.Visible = False

            If Page.IsPostBack = False Then
                If Session("entryToken").ToString.ToUpper() = "DONOR" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    rfvHomePhoneInd.Enabled = False
                    hlnkMainMenu.NavigateUrl = "DonorFunctions.aspx"
                ElseIf Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    hlnkMainMenu.NavigateUrl = "VolunteerFunctions.aspx"
                ElseIf Session("entryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    hlnkMainMenu.NavigateUrl = "UserFunctions.aspx"
                End If
                Page.MaintainScrollPositionOnPostBack = True
                '*** Populate State DropDown
                Dim dsStates As DataSet

                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlStateInd.DataSource = dsStates.Tables(0)
                    ddlStateInd.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlStateInd.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlStateInd.DataBind()
                    ddlStateInd.Items.Insert(0, New ListItem("Select State", String.Empty))
                    ddlStateSp.DataSource = dsStates.Tables(0)
                    ddlStateSp.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlStateSp.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlStateSp.DataBind()
                    ddlStateSp.Items.Insert(0, New ListItem("Select State", String.Empty))
                End If
                ddlStateInd.SelectedIndex = 0
                ddlStateSp.SelectedIndex = 0
                '*** Populate Chapter DropDown
                '*** Populate Chapter Names List
                Dim objChapters As New NorthSouth.BAL.Chapter
                Dim dsChapters As New DataSet
                objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

                If dsChapters.Tables.Count > 0 Then
                    ddlChapterInd.DataSource = dsChapters.Tables(0)
                    ddlChapterInd.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                    ddlChapterInd.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                    ddlChapterInd.DataBind()
                    ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                    If Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                        If Len(Session("LoginChapterID")) > 0 Then
                            ddlChapterInd.SelectedValue = Session("LoginChapterID")
                            ddlChapterInd.Enabled = False
                        End If
                    End If
                    If Session("RoleID") = "5" Then
                        ddlChapterInd.Items.Clear()
                        Dim strSql As String
                        strSql = "Select chapterid, chaptercode, state from chapter "
                        strSql = strSql & " where clusterid in (Select clusterid from "
                        strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                        strSql = strSql & " order by state, chaptercode"
                        MsgBox(strSql)
                        Dim con As New SqlConnection(Application("ConnectionString"))
                        Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                        While (drNSFChapters.Read())
                            ddlChapterInd.Items.Add(New ListItem(drNSFChapters(1).ToString(), drNSFChapters(0).ToString()))
                        End While
                        ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                        'ddlChapterInd.Enabled = True
                    End If
                End If
                setSpouseValidators(False)

                If Not Request.QueryString("Type") Is Nothing Then
                    If Request.QueryString("Type") = "Individual" Then
                        If Session("NavPath") = "Sponsor" Then
                            hlnkPrevPage.Visible = True
                            hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        ElseIf Session("NavPath") = "Search" Then
                            hlnkPrevPage.Visible = True
                            hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        Else
                            hlnkPrevPage.Visible = False
                        End If
                    Else
                        Exit Sub
                    End If
                    If Session("NavPath") = "Search" Then
                        rfvEmail.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                    ElseIf Session("NavPath") = "Sponsor" Then
                        rfvEmail.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                    End If
                Else
                    'Get IndID and SpouseID for the givn Logon Person
                    Dim StrIndSpouse As String = ""
                    Dim intIndID As Integer = 0
                    Dim dsIndSpouse As New DataSet
                    If Request.QueryString("Id") Is Nothing Then
                        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
                        hlnkPrevPage.Visible = True

                        If Session("NavPath") = "Sponsor" Then
                            hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        ElseIf Session("NavPath") = "Search" Then
                            hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        Else
                            hlnkPrevPage.Visible = False
                        End If
                    Else
                        StrIndSpouse = "AutoMemberID=" & Request.QueryString("ID")
                    End If

                    If Session("NavPath") = "Search" Then
                        rfvEmail.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        hlnkPrevPage.Visible = True
                    ElseIf Session("NavPath") = "Sponsor" Then
                        rfvEmail.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        hlnkPrevPage.Visible = True
                    End If
                    'revSecondaryEMail.Enabled = False                    

                    objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

                    If dsIndSpouse.Tables.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                            If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                                End If
                            Else
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                Else
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                                End If
                            End If
                        Else
                            txtPrimaryEmailInd.Text = Session("LoginEmail")
                            txtPrimaryEmailInd.Enabled = False
                            Try
                                ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                            Catch
                                ddlChapterInd.Items.FindByValue(String.Empty).Selected = True
                            End Try
                        End If
                    Else
                        txtPrimaryEmailInd.Text = Session("LoginEmail")
                        txtPrimaryEmailInd.Enabled = False
                        ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                    End If

                    If intIndID > 0 Then

                        'Start Grade locking code
                        '*******************************************
                        '**** Code Added By FERDINE Jan 05 2010 ****
                        '*******************************************

                        If (Not (Session("RoleId") = Nothing)) Then

                            If (((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5"))) Then

                            Else
                                lockchapter(intIndID)

                            End If
                        Else
                            lockchapter(intIndID)
                        End If
                        'End Grade Locking code
                        'check that child has already registered.
                        'Dim objContestant As nsf.Data.SqlClient.SqlContestantProvider
                        'objContestant = New nsf.Data.SqlClient.SqlContestantProvider(Application("ConnectionString"), True, "SqlNetTiersProvider")

                        'Dim regContestants As nsf.Entities.TList(Of Contestant) = objContestant.Find("ContestYear='" + Application("ContestYear") + "' AND ParentID='" + intIndID.ToString + "'")

                        'If regContestants.Count > 0 Then
                        '    ddlChapterInd.Enabled = False
                        'End If
                        'get spouse
                        Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
                        objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), intIndID)
                        If objIndSpouseExt.Id > 0 Then
                            objIndSpouse = objIndSpouseExt
                        End If
                        Me.btnSubmit.Text = "Continue"
                        nextPage = True
                    End If


                    'set spouse validators to false.

                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailSp.ReadOnly = False
                    ddlStateOfOriginInd.Visible = True
                    ddlStateOfOriginSp.Visible = True
                    'txtStateOfOriginInd.Visible = False
                    'txtStateOfOriginSp.Visible = False

                    loadStates(Me.ddlStateOfOriginInd, "IN")
                    loadStates(Me.ddlStateOfOriginSp, "IN")

                    If Not objIndSpouse Is Nothing Then
                        If objIndSpouse.Id > 0 Then
                            'Found Session Ferdine
                            Session("GetIndSpouseID") = objIndSpouse.Id
                            With objIndSpouse
                                Session("CustIndID") = .Id
                                If Trim(.Title.Length) > 0 Then
                                    Try
                                        ddlTitleInd.Items.FindByValue(.Title).Selected = True
                                    Catch
                                    End Try
                                End If
                                txtFirstNameInd.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                'txtLastNameInd.Text = .LastName.Substring(0, 1).ToUpper + .LastName.Substring(1)
                                txtLastNameInd.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                txtAddress1Ind.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                txtAddress2Ind.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                txtCityInd.Text = StrConv(.City, VbStrConv.ProperCase)
                                If Trim(.State.Length) > 0 Then
                                    ddlStateInd.SelectedValue = .State
                                End If
                                txtZipInd.Text = .Zip
                                If Trim(.Country.Length) > 0 Then
                                    ddlCountryInd.SelectedValue = .Country
                                End If
                                If Trim(.Gender.Length) > 0 Then
                                    ddlGenderInd.SelectedValue = .Gender
                                End If
                                txtHomePhoneInd.Text = .HPhone
                                txtWorkPhoneInd.Text = .WPhone
                                txtWorkFaxInd.Text = .WFax
                                txtCellPhoneInd.Text = .CPhone
                                txtPrimaryEmailInd.Text = .Email
                                txtPrimaryEmailInd.Enabled = False
                                txtSecondaryEmailInd.Text = .SecondaryEmail
                                If Trim(.Education.Length) > 0 Then
                                    ddlEducationalInd.SelectedValue = .Education
                                End If
                                If Trim(.Career.Length) > 0 Then
                                    ddlCareerInd.SelectedValue = .Career
                                End If
                                txtEmployerInd.Text = StrConv(.Employer, VbStrConv.ProperCase)
                                If Trim(.CountryOfOrigin.Length) > 0 Then
                                    ddlCountryOfOriginInd.SelectedValue = .CountryOfOrigin
                                End If
                                If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
                                    If Trim(.StateOfOrigin.Length) > 0 Then
                                        ddlStateOfOriginInd.SelectedValue = .StateOfOrigin
                                    End If
                                    'txtStateOfOriginInd.Visible = False
                                Else
                                    txtStateOfOriginInd.Visible = True
                                    txtStateOfOriginInd.Text = .StateOfOrigin
                                    'ddlStateOfOriginInd.Visible = False
                                End If

                                If .VolunteerFlag = "Yes" Or .VolunteerFlag = "No" Then
                                    rbVolunteerInd.SelectedValue = .VolunteerFlag
                                Else
                                    rbVolunteerInd.Items(0).Selected = False
                                    rbVolunteerInd.Items(1).Selected = False
                                End If

                                If .Liaison = "Yes" Or .Liaison = "No" Then
                                    rbLiaisonInd.SelectedValue = .Liaison
                                Else
                                    rbLiaisonInd.Items(0).Selected = False
                                    rbLiaisonInd.Items(1).Selected = False
                                End If

                                If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                    rbSponsorInd.SelectedValue = .Sponsor
                                Else
                                    rbSponsorInd.Items(0).Selected = False
                                    rbSponsorInd.Items(1).Selected = False
                                End If

                                ddlMaritalStatusInd.SelectedValue = .MaritalStatus

                                If Trim(.Chapter.Length) > 0 Then
                                    Try
                                        ddlChapterInd.Items.FindByValue(.ChapterID).Selected = True
                                    Catch
                                    End Try

                                End If

                                If Trim(.ReferredBy.Length) >= 4 And Not IsDBNull(.ReferredBy) Then
                                    ddlReferredByInd.Items.FindByValue(.ReferredBy).Selected = True
                                End If


                                Dim StrSpouse As String = ""
                                Dim intSpouseID As Integer = 0
                                Dim dsSpouse As New DataSet
                                StrSpouse = "Relationship='" & Session("CustIndID") & "'"


                                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                                If dsSpouse.Tables.Count > 0 Then
                                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    End If
                                End If

                                If intSpouseID > 0 Then
                                    'If objSpouse Is Nothing Then
                                    objSpouse = New IndSpouse10
                                    objSpouse.GetIndSpouseByID(Application("ConnectionString"), intSpouseID)
                                    'End If
                                    'Found Session Ferdine
                                    ' SessobjSpouse = objSpouse
                                    Session("GetSpouseID") = objSpouse.Id

                                    If Not objSpouse Is Nothing Then
                                        With objSpouse
                                            Session("CustSpouseID") = .Id

                                           
                                            If Trim(.Title.Length) > 0 Then
                                                Try
                                                    ddlTitleSp.Items.FindByValue(.Title).Selected = True
                                                Catch
                                                End Try
                                            End If

                                            txtFirstNameSp.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                            txtLastNameSp.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                            txtAddress1Sp.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                            txtAddress2Sp.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                            txtCitySp.Text = StrConv(.City, VbStrConv.ProperCase)

                                            If Trim(.State.Length) > 0 Then
                                                ddlStateSp.SelectedValue = .State
                                            End If

                                            txtZipSp.Text = .Zip

                                            If Trim(.Country.Length) > 0 Then
                                                ddlCountrySp.SelectedValue = .Country
                                            End If

                                            If Trim(.Gender.Length) > 0 Then
                                                ddlGenderSp.SelectedValue = .Gender
                                            End If

                                            txtHomePhoneSp.Text = .HPhone
                                            txtWorkPhoneSp.Text = .WPhone
                                            txtWorkFaxSp.Text = .WFax
                                            txtCellPhoneSp.Text = .CPhone

                                            If Trim(.Email.Length) = 0 Then
                                                'sg 01/26/07 txtPrimaryEmailSp.Text = txtPrimaryEmailInd.Text
                                                txtPrimaryEmailSp.Enabled = True
                                            Else
                                                'sg 01/26/07 If .Email = Session("LoginEmail") Then
                                                txtPrimaryEmailSp.Text = .Email
                                                txtPrimaryEmailSp.Enabled = False
                                                'Else
                                                'txtPrimaryEmailSp.Text = .Email
                                                'txtPrimaryEmailSp.Enabled = False
                                                'End If
                                            End If

                                            txtSecondaryEmailSp.Text = .SecondaryEmail

                                            If Trim(.Education.Length) > 0 Then
                                                ddlEducationSp.SelectedValue = .Education
                                            End If

                                            If Trim(.Career.Length) > 0 Then
                                                ddlCareerSp.SelectedValue = .Career
                                            End If

                                            txtEmployerSp.Text = StrConv(.Employer, VbStrConv.ProperCase)

                                            If Trim(.CountryOfOrigin.Length) > 0 Then
                                                ddlCountryOfOriginSp.SelectedValue = .CountryOfOrigin
                                            End If

                                            If loadStates(Me.ddlStateOfOriginSp, Me.ddlCountryOfOriginSp.SelectedValue) Then
                                                If Trim(.StateOfOrigin.Length) > 0 Then
                                                    ddlStateOfOriginSp.SelectedValue = .StateOfOrigin
                                                End If
                                                'txtStateOfOriginSp.Visible = False
                                            Else
                                                txtStateOfOriginSp.Visible = True
                                                txtStateOfOriginSp.Text = .StateOfOrigin
                                                'ddlStateOfOriginSp.Visible = False
                                            End If

                                            txtStateOfOriginSp.Text = .StateOfOrigin

                                            If .VolunteerFlag = "Yes" Or .VolunteerFlag = "No" Then
                                                rbVolunteerSp.SelectedValue = .VolunteerFlag
                                            Else
                                                rbVolunteerSp.Items(0).Selected = False
                                                rbVolunteerSp.Items(1).Selected = False
                                            End If

                                            If .Liaison = "Yes" Or .Liaison = "No" Then
                                                rbLiaisonSp.SelectedValue = .Liaison
                                            Else
                                                rbLiaisonSp.Items(0).Selected = False
                                                rbLiaisonSp.Items(1).Selected = False
                                            End If

                                            If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                                rbSponsorSp.SelectedValue = .Sponsor
                                            Else
                                                rbSponsorSp.Items(0).Selected = False
                                                rbSponsorSp.Items(1).Selected = False
                                            End If

                                            ddlMaritalStatusSp.SelectedValue = .MaritalStatus

                                            If .PrimaryContact = "Y" Then
                                                Me.ckbPrimaryContact.Checked = True
                                            End If
                                        End With
                                        setSpouseValidators(True)
                                    End If
                                End If
                            End With
                        End If
                        ' Commented by Ferdine
                        'If checkContestantRegistered(Session("CustIndID"), Session("CustSpouseID"), Application("ContestYear")) Then
                        '    Me.ddlChapterInd.Enabled = False
                        'Else
                        '    Me.ddlChapterInd.Enabled = True
                        'End If
                    End If
                End If
            End If
            If Session("RoleID") = "1" Or Session("RoleID") = "2" Or Session("RoleID") = "37" Or Session("RoleID") = "38" Then
                txtPrimaryEmailInd.ReadOnly = False
                txtPrimaryEmailInd.Enabled = True
                txtPrimaryEmailSp.Enabled = True
                txtPrimaryEmailSp.ReadOnly = False
                'setSpouseValidators(False)
                Me.rfvPrimaryEmailSp.Enabled = False
            End If
            If Session("RoleID") = "37" And Session("LoginChapterID") = "48" Then
                If Len(txtPrimaryEmailInd.Text) > 0 Then
                    txtPrimaryEmailInd.ReadOnly = True
                    txtPrimaryEmailInd.Enabled = False
                Else
                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailInd.Enabled = True
                End If
                If Len(txtPrimaryEmailSp.Text) > 0 Then
                    txtPrimaryEmailSp.Enabled = False
                    txtPrimaryEmailSp.ReadOnly = True
                    'setSpouseValidators(False)
                    Me.rfvPrimaryEmailSp.Enabled = False
                Else
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailSp.ReadOnly = False
                    setSpouseValidators(True)
                    Me.rfvPrimaryEmailSp.Enabled = False
                End If
            ElseIf Session("RoleID") = "38" And Session("LoginChapterID") = "48" Then
                If Len(txtPrimaryEmailInd.Text) > 0 Then
                    txtPrimaryEmailInd.ReadOnly = True
                    txtPrimaryEmailInd.Enabled = False
                Else
                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailInd.Enabled = True
                End If
                If Len(txtPrimaryEmailSp.Text) > 0 Then
                    txtPrimaryEmailSp.Enabled = False
                    txtPrimaryEmailSp.ReadOnly = True
                    'setSpouseValidators(False)
                    Me.rfvPrimaryEmailSp.Enabled = False
                Else
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailSp.ReadOnly = False
                    setSpouseValidators(True)
                    Me.rfvPrimaryEmailSp.Enabled = False
                End If
            End If
            'ddlCountryOfOriginInd.Attributes.Add("onchange", "GetStateDetails(this.name+ '-' + this.options[this.selectedIndex].value , 'ddlCountryOfOriginInd');")
            'Dim callBackStateName As String
            'callBackStateName = Page.ClientScript.GetCallbackEventReference(Me, "arg", "StateNameClientCallback", "context")
            'Dim StateNameclientFunction As String
            'StateNameclientFunction = "function GetStateDetails(arg, context){ " + callBackStateName + "; }"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GetStateDetails", StateNameclientFunction, True)

            'ddlCountryOfOriginSp.Attributes.Add("onchange", "GetStateDetails(this.name+ '-' + this.options[this.selectedIndex].value , 'ddlCountryOfOriginSp');")
            'callBackStateName = Page.ClientScript.GetCallbackEventReference(Me, "arg", "StateNameClientCallback", "context")
            'StateNameclientFunction = "function GetStateDetails(arg, context){ " + callBackStateName + "; }"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GetStateDetails", StateNameclientFunction, True)

        End Sub
    Private Sub GetSpouseData()

    End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim indId As Integer
            Dim spId As Integer
            Dim strSql As String
            If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                setSpouseValidators(True)
                'If Session("Entrytoken").ToString.ToUpper() <> "VOLUNTEER" Then
                If rbVolunteerSp.SelectedValue = "Yes" And Session("NavPath") <> "Search" Then
                    If Len(txtPrimaryEmailSp.Text) <= 0 Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Primary Email Address Should not be blank."
                        lblMsg.ForeColor = Color.Red
                        Exit Sub
                    End If
                End If
                'End If
            End If
            If Page.IsValid Then
                If ckbPrimaryContact.Checked And txtPrimaryEmailSp.Text.Length < 5 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Primary Email should not be blank."
                    lblMsg.ForeColor = Color.Red
                    Exit Sub
                End If
                indId = UpdateIndividualInfo()
                If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                    If Session("LoginID") Is Nothing Then
                        Session("LoginID") = indId
                    End If
                End If
                If lblDuplicateMsg.Visible = False Then
                    If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                        If Val(indId) > 0 Then
                            spId = UpdateSpouseInfo(indId)
                        End If
                        'set spouseid for individual.
                        If spId > 0 Then
                            'Dim objIndividual As New IndSpouse10
                            'objIndividual = Session("IndRegInfo")
                            'objIndividual.PrimaryIndSpouseID = spId
                            'objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                            strSql = "UPDATE INDSPOUSE SET PrimaryIndSpouseID=" & spId
                            If Not Session("LoginID") Is Nothing Then
                                strSql = strSql & ",CreatedBy=" & Session("LoginID")
                            Else
                                strSql = strSql & ",CreatedBy=" & indId
                            End If
                            strSql = strSql & " WHERE AutomemberID=" & indId
                            strSql = strSql & " AND DonorType='IND'"

                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                            strSql = "UPDATE INDSPOUSE SET "
                            If Not Session("LoginID") Is Nothing Then
                                strSql = strSql & "CreatedBy=" & Session("LoginID")
                            Else
                                strSql = strSql & "CreatedBy=" & indId
                            End If
                            strSql = strSql & " WHERE AutomemberID=" & spId
                            strSql = strSql & " AND DonorType='SPOUSE'"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                        End If
                    Else
                        Dim objIndividual As New IndSpouse10
                        'objIndividual = SessobjIndSpouse
                        objIndividual.GetIndSpouseByID(Application("ConnectionString"), Convert.ToInt32(Session("GetIndSpouseID")))

                        objIndividual.PrimaryIndSpouseID = Nothing
                        objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                    End If
                End If
                'set session vars for following screens.
                Session("CustIndID") = indId
                Session("CustSpouseID") = spId

                'cleanup...
                'SessobjIndSpouse = Nothing
                Session("GetIndSpouseID") = Nothing
                Session("GetSpouseID") = Nothing

                If Session("ParentsChapterID") Is Nothing Then
                    Session("ParentsChapterID") = ddlChapterInd.SelectedItem.Value
                End If
                If lblDuplicateMsg.Visible = False Then
                    If rbVolunteerInd.SelectedValue = "Yes" Then
                        Session("VolunteerInd") = True
                    Else
                        Session("VolunteerInd") = False
                    End If
                    If rbVolunteerSp.SelectedValue = "Yes" Then
                        Session("VolunteerSP") = True
                    Else
                        Session("VolunteerSP") = False
                    End If
                    If rbVolunteerInd.SelectedValue = "Yes" Or rbVolunteerSp.SelectedValue = "Yes" Or rbVolunteerSp.SelectedValue Is Nothing Then
                        If Request.QueryString("Id") Is Nothing Then
                            Response.Redirect("VolunteerRole.aspx")
                        Else
                            Response.Redirect("VolunteerRole.aspx?Id=" & Request.QueryString("Id"))
                        End If
                    ElseIf rbVolunteerInd.SelectedValue = "No" Or rbVolunteerSp.SelectedValue = "No" Or rbVolunteerSp.SelectedValue Is Nothing Then
                        If Session("entryToken").ToString.ToUpper = "DONOR" Then
                            Response.Redirect("DonorFunctions.aspx")
                        ElseIf Session("entryToken").ToString.ToUpper = "PARENT" Then
                            Response.Redirect("MainChild.aspx")
                        Else
                            If Session("NavPath") = "Sponsor" Then
                                Response.Redirect("search_sponsor.aspx")
                            ElseIf Session("NavPath") = "Search" Then
                                Response.Redirect("dbSearchResults.aspx")
                            Else
                                btnSubmit.Enabled = False
                                lblDuplicateMsg.Visible = True
                                lblDuplicateMsg.ForeColor = Color.Red
                                lblDuplicateMsg.Text = "Your profile was saved successfully.  Please click on Home to log out."
                            End If
                        End If
                    Else
                        Select Case Session("EventID")
                            Case 1, 2
                                Response.Redirect("MainChild.aspx")
                            Case 3
                                If Request.QueryString("Id") Is Nothing Then
                                    Response.Redirect("VolunteerRole.aspx")
                                Else
                                    Response.Redirect("VolunteerRole.aspx?Id=" & Request.QueryString("Id"))
                                End If
                        End Select
                    End If

                    If Not Request("Action") Is Nothing Then
                        If Request("Action") = "ChapterFunction" Then
                            Response.Redirect("RegistrationList.aspx")
                        End If
                    Else
                        Select Case Session("EventID")
                            Case 1, 2
                                If Request("ParentUpdate") = "True" Then
                                    Response.Redirect("UserFunctions.aspx")
                                End If
                        End Select
                    End If
                End If
            Else

                'If lblDuplicateMsg.Visible = True Then
                'lblDuplicateMsg.Text = "Duplicate Record Found."
                'End If
                lblDuplicateMsg.Visible = False
            End If
        End Sub
        Private Function UpdateIndividualInfo() As Integer

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim objIndividual As New IndSpouse10
            Dim indID As Integer

            If Not Session("GetIndSpouseID") Is Nothing Then
                'objIndividual = SessobjIndSpouse
                objIndividual.GetIndSpouseByID(Application("ConnectionString"), Session("GetIndSpouseID"))


            End If
            Dim bIsNewRecord As Boolean
            lblDuplicateMsg.Visible = False
            bIsNewRecord = False
            With objIndividual
                .Title = IIf(ddlTitleInd.SelectedItem.Value = "", DBNull.Value, ddlTitleInd.SelectedItem.Value)
                .FirstName = StrConv(txtFirstNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtFirstNameInd.Text.Substring(1)), VbStrConv.ProperCase)
                .LastName = StrConv(txtLastNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtLastNameInd.Text.Substring(1)), VbStrConv.ProperCase)
                .DonorType = "IND"
                If Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                    If Request.QueryString("Type") Is Nothing Then
                        If Request.QueryString("Id") Is Nothing Then
                            .MemberID = Session("LoginID")
                            If Session("LoginID") Is Nothing Then
                                bIsNewRecord = True
                            End If
                        ElseIf Not Request.QueryString("Id") Is Nothing Then
                            .MemberID = Request.QueryString("Id")
                        End If
                    Else
                        .MemberID = String.Empty
                        bIsNewRecord = True
                    End If
                Else
                    If Session("LoginID") Is Nothing Then
                        bIsNewRecord = True
                    Else
                        bIsNewRecord = False
                        .MemberID = Session("LoginID")
                    End If
                End If

                'If Session("LoginID") Is Nothing Then
                'bIsNewRecord = True
                'Else
                'bIsNewRecord = False
                'End If
                'set old attributes.
                If Not Session("GetIndSpouseID") Is Nothing Then
                    If .Address1 <> Server.HtmlEncode(txtAddress1Ind.Text) Or _
                        .Address2 <> Server.HtmlEncode(txtAddress2Ind.Text) Or _
                        .City <> Server.HtmlEncode(txtCityInd.Text) Then
                        .Address1_Old = .Address1
                        .Address2_Old = .Address2
                        .City_old = .City
                        .State_Old = .State
                        .ZipCode_Old = .Zip
                        .PrimaryEmail_Old = .Email
                        .HomePhone_Old = .HPhone
                        .CellPhone_Old = .CPhone

                    End If
                Else
                    .CreateDate = Now
                    .MemberSince = Now
                End If

                .Address1 = StrConv(Server.HtmlEncode(txtAddress1Ind.Text), VbStrConv.ProperCase)
                .Address2 = StrConv(Server.HtmlEncode(txtAddress2Ind.Text), VbStrConv.ProperCase)
                .City = StrConv(Server.HtmlEncode(txtCityInd.Text), VbStrConv.ProperCase)
                .State = ddlStateInd.SelectedValue
                .Zip = Server.HtmlEncode(txtZipInd.Text)
                'sg 01/27/2007.Email = Session("LoginEmail")
                If Len(txtPrimaryEmailInd.Text) = 0 Then
                    .Email = ""
                Else
                    .Email = Server.HtmlEncode(txtPrimaryEmailInd.Text)
                End If
                .HPhone = Server.HtmlEncode(txtHomePhoneInd.Text)
                .CPhone = Server.HtmlEncode(txtCellPhoneInd.Text)
                .Country = ddlCountryInd.SelectedValue
                .Gender = ddlGenderInd.SelectedValue
                .WPhone = Server.HtmlEncode(txtWorkPhoneInd.Text)
                .WFax = Server.HtmlEncode(txtWorkFaxInd.Text)
                .SecondaryEmail = Server.HtmlEncode(txtSecondaryEmailInd.Text)
                .Education = Server.HtmlEncode(ddlEducationalInd.SelectedValue)
                .Career = Server.HtmlEncode(ddlCareerInd.SelectedValue)
                .Employer = StrConv(Server.HtmlEncode(txtEmployerInd.Text), VbStrConv.ProperCase)
                .CountryOfOrigin = ddlCountryOfOriginInd.SelectedValue
                If .CountryOfOrigin = "IN" Or .CountryOfOrigin = "US" Then
                    .StateOfOrigin = Me.ddlStateOfOriginInd.SelectedValue
                Else
                    .StateOfOrigin = Server.HtmlEncode(txtStateOfOriginInd.Text)
                End If

                .VolunteerFlag = rbVolunteerInd.SelectedValue
                .ReferredBy = Server.HtmlEncode(ddlReferredByInd.SelectedValue)

                .ChapterID = ddlChapterInd.SelectedValue
                .Chapter = ddlChapterInd.SelectedItem.Text

                .MaritalStatus = ddlMaritalStatusInd.SelectedValue
                .Liaison = rbLiaisonInd.SelectedValue
                .Sponsor = rbSponsorInd.SelectedValue

                '.NewsLetter = rblSendNewsletterInd.SelectedValue
                '.MailingLabel = rblMailingLabelInd.SelectedValue
                '.SendReceipt = rblSendReceiptInd.SelectedValue
                '.LiasonPerson = Server.HtmlEncode(txtLiaisonInd.Text)
                If rbVolunteerInd.SelectedValue = "No" Then
                    .VolunteerRole1 = Nothing
                    .VolunteerRole2 = Nothing
                    .VolunteerRole3 = Nothing
                End If
                If .CreateDate = Nothing Then
                    .CreateDate = Now
                End If
                If .MemberSince = Nothing Then
                    .MemberSince = Now
                End If
                Dim sqlDateTime As SqlTypes.SqlDateTime
                sqlDateTime = SqlTypes.SqlDateTime.Null

                If bIsNewRecord = True Then
                    .ModifyDate = sqlDateTime
                    If Session("LoginID") Is Nothing Then
                        .CreatedBy = Nothing
                    Else
                        .CreatedBy = Session("LoginID")
                    End If
                    .ModifiedBy = Nothing
                Else
                    .ModifyDate = Now
                    If Session("LoginID") Is Nothing Then
                        .ModifiedBy = Nothing
                    Else
                        .ModifiedBy = Session("LoginID")
                    End If
                End If
                .PrimaryContact = "No"
                Session("ParentLName") = .LastName
                Session("ParentFName") = .FirstName
            End With

            'If Not objIndividual.MemberID Is Nothing And Not objIndividual.MemberID.Trim() = "" Then
            If Trim(objIndividual.MemberID) <> String.Empty Then
                objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                indID = objIndividual.Id()
                bIsNewRecord = False
            End If
            If bIsNewRecord = True Then
                Dim objInd As New IndSpouseExt
                Dim cnt As Integer = 0
                'If objInd.CheckIndSpouseDuplicateEmail(Application("ConnectionString"), objIndividual.Email, objIndividual.LastName, objIndividual.Address1, objIndividual.HPhone) = 0 Then
                Dim emails As String = ""
                Dim strSql As String
                Dim conn, Conn1 As New SqlConnection(Application("ConnectionString"))
                Dim drDuplicates As SqlDataReader
                Dim bIsDuplicateFound As Boolean
                Dim iDuplicateID As Double
                bIsDuplicateFound = False
                Dim AutoMemberID, Count As Integer
                emails = emails & " UPPER(EMAIL) IN('" & Trim(txtPrimaryEmailInd.Text.ToUpper()) & "'"
                If Len(Trim(txtPrimaryEmailSp.Text)) > 0 Then
                    emails = emails & ",'" & Trim(txtPrimaryEmailSp.Text.ToUpper()) & "'"
                End If
                cnt = SqlHelper.ExecuteScalar(conn1, CommandType.Text, "select Count(*) from IndSpouse where " & emails & ")")
                If cnt > 0 Then
                    AutoMemberID = SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "select AutoMemberID from IndSpouse where Email='" & txtPrimaryEmailSp.Text & "'")
                    bIsDuplicateFound = True
                Else
                    strSql = "SELECT AutoMemberID FROM IndSpouse WHERE "
                    strSql = strSql & " UPPER(FirstName) ='" & Trim(txtFirstNameInd.Text.ToUpper()) & "' AND "
                    strSql = strSql & " UPPER(LastName) ='" & Trim(txtLastNameInd.Text.ToUpper()) & "'"
                    'End If
                    strSql = strSql & " AND DonorType='IND'"
                    drDuplicates = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                    Dim bIsDuplicateFoundflag As Integer = 1
                    Dim NotDuplicateFlag As Integer = 1

                    'Ferdine Silva * To Find the duplicate of Many Ind with Same Name 
                    While drDuplicates.Read()
                        AutoMemberID = Convert.ToInt32(drDuplicates(0))
                        Dim StrDup As String = "SELECT COUNT(DonorType) FROM IndSpouse WHERE DonorType = 'SPOUSE' AND Relationship =" & AutoMemberID                        Count = Convert.ToString(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrDup))
                        If Count > 0 Then
                            Dim drDuplicates1 As SqlDataReader
                            If Trim(Me.txtLastNameSp.Text).Length > 0 And Trim(Me.txtFirstNameSp.Text).Length > 0 Then
                                Dim strSql1 As String
                                strSql1 = "SELECT AutoMemberID FROM IndSpouse WHERE "
                                strSql1 = strSql1 & " UPPER(FirstName) ='" & Trim(txtFirstNameSp.Text.ToUpper()) & "' AND "
                                strSql1 = strSql1 & " UPPER(LastName) ='" & Trim(txtLastNameSp.Text.ToUpper()) & "'"
                                strSql1 = strSql1 & " AND DonorType='SPOUSE' AND Relationship=" & AutoMemberID
                                drDuplicates1 = SqlHelper.ExecuteReader(Conn1, CommandType.Text, strSql1)
                                If drDuplicates1.Read() Then
                                    bIsDuplicateFoundflag = 0
                                Else
                                    NotDuplicateFlag = 0
                                End If
                                Conn1.Close()
                            End If
                        End If
                        bIsDuplicateFound = True
                    End While
                    'Ferdine Silva * testing closes
                    conn.Close()
                    If bIsDuplicateFoundflag = 1 And NotDuplicateFlag = 0 Then
                        bIsDuplicateFound = False
                    End If
                End If
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    If Session("RoleID") = "1" Or Session("RoleID") = "2" Or Session("RoleID") = "37" Or Session("RoleID") = "38" Then
                        bIsDuplicateFound = False
                        lblDuplicateMsg.Visible = False
                        Dim strNational As String
                        strNational = ""
                        If Not Session("LoginID") Is Nothing Then
                            strSql = "SELECT ""NATIONAL"" FROM VOLUNTEER WHERE MEMBERID=" & Session("LoginID")
                            strNational = Convert.ToString(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql))
                        End If
                        If Trim(UCase(strNational)) <> "Y" Then
                            bIsDuplicateFound = True
                            lblDuplicateMsg.Visible = True
                            lblDuplicateMsg.Text = "Duplicate Record Found."
                        End If
                    Else
                        If AutoMemberID > 0 Then
                            bIsDuplicateFound = True
                            lblDuplicateMsg.Visible = True
                            lblDuplicateMsg.Text = "Duplicate Record Found."
                        End If
                    End If
                    'If bIsDuplicateFound = True Then
                    'lblDuplicateMsg.Visible = True
                    'lblDuplicateMsg.Text = "Duplicate Record Found."
                    'End If
                End If
                Dim iDuplicateSID As Double = 0
                If bIsDuplicateFound = False Then
                    lblDuplicateMsg.Visible = False
                    indID = objIndividual.AddIndSpouse(Application("ConnectionString"))
                Else
                    'Inserting into IndDuplicate Table
                    'If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                    If bIsDuplicateFound = True Then
                        lblDuplicateMsg.Visible = True
                        lblDuplicateMsg.Text = "Duplicate Record Found."
                        Session("GetSpouseID") = Nothing
                        If Len(AutoMemberID) > 0 Then
                            If Len(txtFirstNameInd.Text) > 0 Then
                                strSql = "INSERT INTO INDDUPLICATE VALUES("
                                strSql = strSql & "'" & Session("EntryToken").ToString.ToUpper & "',"
                                strSql = strSql & "'Pending',"
                                strSql = strSql & AutoMemberID & ","
                                strSql = strSql & "'IND',"
                                strSql = strSql & "NULL," & ddlChapterInd.SelectedItem.Value & ",'" & ddlTitleInd.SelectedItem.Text & "',"
                                strSql = strSql & "'" & txtFirstNameInd.Text & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & txtLastNameInd.Text & "',"
                                strSql = strSql & "'" & txtAddress1Ind.Text & "',"
                                strSql = strSql & "'" & txtAddress2Ind.Text & "',"
                                strSql = strSql & "'" & txtCityInd.Text & "',"
                                strSql = strSql & "'" & ddlStateInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtZipInd.Text & "',"
                                strSql = strSql & "'" & ddlCountryInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlGenderInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtHomePhoneInd.Text & "',"
                                strSql = strSql & "'" & txtCellPhoneInd.Text & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & txtWorkPhoneInd.Text & "',"
                                strSql = strSql & "'" & txtWorkFaxInd.Text & "',"
                                strSql = strSql & "'" & txtPrimaryEmailInd.Text & "',"
                                strSql = strSql & "'" & txtSecondaryEmailInd.Text & "',"
                                strSql = strSql & "'" & ddlEducationalInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlCareerInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtEmployerInd.Text & "',"
                                strSql = strSql & "'" & ddlCountryOfOriginInd.SelectedItem.Value & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & ddlMaritalStatusInd.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlChapterInd.SelectedItem.Text & "',"
                                strSql = strSql & "'" & rbVolunteerInd.SelectedValue & "',"
                                strSql = strSql & "'" & ddlReferredByInd.SelectedItem.Value & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & rbLiaisonInd.SelectedValue & "',"
                                strSql = strSql & "'" & rbSponsorInd.SelectedValue & "',"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "'" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "',"
                                If Session("LoginID") Is Nothing Then
                                    strSql = strSql & "0,"
                                Else
                                    strSql = strSql & Session("LoginID") & ","
                                End If
                                strSql = strSql & "'',"
                                strSql = strSql & "Null,"
                                strSql = strSql & "NULL,NULL,NULL,NULL)"
                                strSql = strSql & "Select Scope_Identity()"
                                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                                iDuplicateID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
                                'iDuplicateID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(IndDupID) from  INDDUPLICATE")
                            End If

                            If Len(txtFirstNameSp.Text) > 0 Then
                                strSql = "INSERT INTO INDDUPLICATE VALUES("
                                strSql = strSql & "'" & Session("EntryToken").ToString.ToUpper & "',"
                                strSql = strSql & "'Pending',"
                                strSql = strSql & AutoMemberID & ","
                                strSql = strSql & "'SPOUSE',"
                                strSql = strSql & "NULL," & ddlChapterInd.SelectedItem.Value & ",'" & ddlTitleSp.SelectedItem.Text & "',"
                                strSql = strSql & "'" & txtFirstNameSp.Text & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & txtLastNameSp.Text & "',"
                                strSql = strSql & "'" & txtAddress1Sp.Text & "',"
                                strSql = strSql & "'" & txtAddress2Sp.Text & "',"
                                strSql = strSql & "'" & txtCitySp.Text & "',"
                                strSql = strSql & "'" & ddlStateSp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtZipSp.Text & "',"
                                strSql = strSql & "'" & ddlCountrySp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlGenderSp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtHomePhoneSp.Text & "',"
                                strSql = strSql & "'" & txtCellPhoneSp.Text & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & txtWorkPhoneSp.Text & "',"
                                strSql = strSql & "'" & txtWorkFaxSp.Text & "',"
                                strSql = strSql & "'" & txtPrimaryEmailSp.Text & "',"
                                strSql = strSql & "'" & txtSecondaryEmailSp.Text & "',"
                                strSql = strSql & "'" & ddlEducationSp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlCareerSp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & txtEmployerSp.Text & "',"
                                strSql = strSql & "'" & ddlCountryOfOriginSp.SelectedItem.Value & "',"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & ddlMaritalStatusSp.SelectedItem.Value & "',"
                                strSql = strSql & "'" & ddlChapterInd.SelectedItem.Text & "',"
                                strSql = strSql & "'" & rbVolunteerSp.SelectedValue & "',"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "'',"
                                strSql = strSql & "'" & rbLiaisonSp.SelectedValue & "',"
                                strSql = strSql & "'" & rbSponsorSp.SelectedValue & "',"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "'" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "',"
                                strSql = strSql & "NULL,"
                                strSql = strSql & "'',"
                                strSql = strSql & "Null,"
                                strSql = strSql & "NULL,NULL,NULL," & iDuplicateID & ");"
                                strSql = strSql & "Select Scope_Identity()"
                                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                                iDuplicateSID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
                            End If
                        End If
                        If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                            If iDuplicateSID > 0 Then
                                Response.Redirect("DuplicateReg.aspx?MemberID=" & iDuplicateID & "&MemberSID=" & iDuplicateSID)
                            Else
                                Response.Redirect("DuplicateReg.aspx?MemberID=" & iDuplicateID)
                            End If
                        End If
                    End If

                End If
            Else

            End If

            If objIndividual.Gender.Equals("Male", StringComparison.CurrentCultureIgnoreCase) Then
                Session("FatherID") = objIndividual.Id
            Else
                Session("MotherID") = objIndividual.Id
            End If
            'SessobjIndSpouse = objIndividual
            Session("GetIndSpouseID") = objIndividual.MemberID
            Return indID
        End Function
        Private Function UpdateSpouseInfo(ByVal p_IndID As Integer) As Integer
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim objSpouse As New IndSpouse10
            Dim spouseID As Integer
            Dim tAddress1 As String = ""
            Dim tAddress2 As String = ""
            Dim tCity As String = ""

            If Not Session("GetSpouseID") Is Nothing Then
                'objSpouse = SessobjSpouse
                objSpouse.GetIndSpouseByID(Application("ConnectionString"), Session("GetSpouseID"))
            End If
            Dim bIsNewRecord As Boolean
            With objSpouse
                objSpouse.PrimaryIndSpouseID = p_IndID
                .Title = IIf(ddlTitleSp.SelectedItem.Value = "", DBNull.Value, ddlTitleSp.SelectedItem.Value)
                .FirstName = StrConv(txtFirstNameSp.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtFirstNameSp.Text.Substring(1)), VbStrConv.ProperCase)
                .LastName = StrConv(txtLastNameSp.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtLastNameSp.Text.Substring(1)), VbStrConv.ProperCase)
                .DonorType = "SPOUSE"
                .Relationship = p_IndID

                If Me.ckbPrimaryContact.Checked Then
                    .PrimaryContact = "Yes"
                Else
                    .PrimaryContact = "No"
                End If

                'set old attributes.
                If Not Session("GetSpouseID") Is Nothing Then
                    If .Address1 <> tAddress1.ToString Or _
                        .Address2 <> tAddress2.ToString Or _
                        .City <> tCity Then
                        .Address1_Old = .Address1
                        .Address2_Old = .Address2
                        .City_old = .City
                        .State_Old = .State
                        .ZipCode_Old = .Zip
                        .PrimaryEmail_Old = .Email
                        .HomePhone_Old = .HPhone
                        .CellPhone_Old = .CPhone
                    End If
                Else
                    .CreateDate = Now
                    .MemberSince = Now
                    bIsNewRecord = True
                End If

                .Address1 = StrConv(Server.HtmlEncode(txtAddress1Sp.Text), VbStrConv.ProperCase)
                .Address2 = StrConv(Server.HtmlEncode(txtAddress2Sp.Text), VbStrConv.ProperCase)
                .City = StrConv(Server.HtmlEncode(txtCitySp.Text), VbStrConv.ProperCase)
                .State = ddlStateSp.SelectedValue
                .Zip = Server.HtmlEncode(txtZipSp.Text)
                .Country = ddlCountrySp.SelectedValue

                .Gender = ddlGenderSp.SelectedValue
                .HPhone = Server.HtmlEncode(txtHomePhoneSp.Text)
                .CPhone = Server.HtmlEncode(txtCellPhoneSp.Text)
                .WPhone = Server.HtmlEncode(txtWorkPhoneSp.Text)
                .WFax = Server.HtmlEncode(txtWorkFaxSp.Text)
                'If txtPrimaryEmailSp.Text.Equals(txtPrimaryEmailInd.Text) Then
                '.Email = String.Empty
                ' Else
                .Email = Server.HtmlEncode(txtPrimaryEmailSp.Text)
                'End If
                .SecondaryEmail = Server.HtmlEncode(txtSecondaryEmailSp.Text)
                .Education = Server.HtmlEncode(ddlEducationSp.SelectedValue)
                .Career = Server.HtmlEncode(ddlCareerSp.SelectedValue)
                .Employer = StrConv(Server.HtmlEncode(txtEmployerSp.Text), VbStrConv.ProperCase)
                .CountryOfOrigin = ddlCountryOfOriginSp.SelectedValue
                If .CountryOfOrigin = "IN" Or .CountryOfOrigin = "US" Then
                    .StateOfOrigin = Me.ddlStateOfOriginSp.SelectedValue
                Else
                    .StateOfOrigin = Server.HtmlEncode(txtStateOfOriginSp.Text)
                End If

                .VolunteerFlag = rbVolunteerSp.SelectedValue
                .MaritalStatus = ddlMaritalStatusSp.SelectedValue
                .Liaison = rbLiaisonSp.SelectedValue
                .Sponsor = rbSponsorSp.SelectedValue
                .ChapterID = ddlChapterInd.SelectedValue
                .Chapter = ddlChapterInd.SelectedItem.Text

                'If rbVolunteerSp.SelectedValue = "Yes" Then
                '.VolunteerRole1 = Nothing
                '.VolunteerRole2 = Nothing
                '.VolunteerRole3 = Nothing

                'Else
                '    .VolunteerRole1 = ddlVolunteerRole1Sp.Items.FindByText("None").Value
                '    .VolunteerRole2 = ddlVolunteerRole2Sp.Items.FindByText("None").Value
                '    .VolunteerRole3 = ddlVolunteerRole3Sp.Items.FindByText("None").Value
                'End If
                If rbVolunteerSp.SelectedValue = "No" Then
                    .VolunteerRole1 = Nothing
                    .VolunteerRole2 = Nothing
                    .VolunteerRole3 = Nothing
                End If
                .CreatedBy = Session("LoginID")
                Dim sqlDateTime As SqlTypes.SqlDateTime
                sqlDateTime = SqlTypes.SqlDateTime.Null
                .ModifyDate = sqlDateTime.Null
                .ModifiedBy = Nothing
                If .CreateDate = Nothing Then
                    .CreateDate = Now
                End If

                If .MemberSince = Nothing Then
                    .MemberSince = Now
                End If
                If bIsNewRecord = False Then
                    .ModifyDate = Now
                    .ModifiedBy = Session("LoginID")
                End If
            End With

            If Not Session("GetSpouseID") Is Nothing Then
                objSpouse.UpdateIndSpouse(Application("ConnectionString"))
                spouseID = objSpouse.Id()
            Else
                Dim objInd As New IndSpouseExt
                spouseID = objSpouse.AddIndSpouse(Application("ConnectionString"))

                'If objInd.CheckIndSpouseDuplicateEmail(Application("ConnectionString"), objSpouse.Email, objSpouse.LastName, objSpouse.Address1) = 0 Then
                'Else
                '    Response.Redirect("DuplicateReg.aspx?dup=Primary")
                'End If
            End If

            If objSpouse.Gender.Equals("Male", StringComparison.CurrentCultureIgnoreCase) Then
                Session("FatherID") = objSpouse.Id
            Else
                Session("MotherID") = objSpouse.Id
            End If
            Return spouseID

        End Function


    Private Sub txtFirstNameSp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFirstNameSp.TextChanged
        If Trim(Me.txtFirstNameSp.Text).Length > 0 Then
            setSpouseValidators(True)
        Else
            setSpouseValidators(False)
        End If
    End Sub

    Private Sub setSpouseValidators(ByVal p_Flag As Boolean)
        Me.rfvTitleSp.Enabled = p_Flag
        Me.rfvFirstNameSp.Enabled = p_Flag
        Me.rfvLastNameSp.Enabled = p_Flag
            setSpouseAddressValidators(p_Flag)
            If rbVolunteerSp.SelectedValue = "Yes" Then
                Me.rfvPrimaryEmailSp.Enabled = p_Flag
            Else
                Me.rfvPrimaryEmailSp.Enabled = False
            End If
            Me.rfvCountryOfOriginSp.Enabled = p_Flag
            Me.rfvZipSp.Enabled = p_Flag
            Me.rfvGenderSp.Enabled = p_Flag
            rfvVolSp.Enabled = p_Flag
        End Sub

    Private Sub setSpouseAddressValidators(ByVal p_flag As Boolean)
        Me.rfvAddress1Sp.Enabled = p_flag
        Me.rfvCitySp.Enabled = p_flag
        Me.rfvStateSp.Enabled = p_flag
        Me.rfvCountrySp.Enabled = p_flag
            ' Me.rfvHomePhoneSp.Enabled = p_flag
        Me.rfvZipSp.Enabled = p_flag
    End Sub

    Private Sub ckbAddressSp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAddressSp.CheckedChanged
        If Me.ckbAddressSp.Checked Then
            setSpouseAddressValidators(False)
        Else
            setSpouseAddressValidators(True)
        End If
    End Sub

    Private Sub txtLastNameSp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastNameSp.TextChanged
        If Trim(Me.txtLastNameSp.Text).Length > 0 Then
            setSpouseValidators(True)
        Else
            setSpouseValidators(False)
        End If
    End Sub

    Private Sub ddlCountryOfOriginInd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountryOfOriginInd.SelectedIndexChanged
        If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
            txtStateOfOriginInd.Visible = False
        Else
            Me.ddlStateOfOriginInd.Visible = False
            Me.txtStateOfOriginInd.Visible = True
        End If
    End Sub
    Private Function loadStates(ByRef ddlControl As DropDownList, ByVal p_country As String) As Boolean
        Dim rtnValue As Boolean
        If p_country = "IN" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet

            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
            If dsIndiaStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsIndiaStates.Tables(0)
                ddlControl.DataTextField = dsIndiaStates.Tables(0).Columns("StateName").ToString
                ddlControl.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        ElseIf p_country = "US" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsStates As DataSet

            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsStates.Tables(0)
                ddlControl.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlControl.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        End If
        Return rtnValue
    End Function



    Private Sub ddlCountryOfOriginSp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountryOfOriginSp.SelectedIndexChanged
        If loadStates(Me.ddlStateOfOriginSp, Me.ddlCountryOfOriginSp.SelectedValue) Then
            txtStateOfOriginSp.Visible = False
        Else
            Me.ddlStateOfOriginSp.Visible = False
            Me.txtStateOfOriginSp.Visible = True
        End If
    End Sub
   Private Function checkContestantRegistered(ByVal indId As Integer, ByVal spouseId As Integer, ByVal contestYear As Integer) As Boolean
        Dim prmArray(2) As SqlParameter
        Dim rtnValue As Boolean
        Dim count As Integer
        
        prmArray(0) = New SqlParameter("@IndId", DbType.UInt32)
        prmArray(0).Value = indId

        prmArray(1) = New SqlParameter("@SpouseId", DbType.UInt32)
        prmArray(1).Value = spouseId

        prmArray(2) = New SqlParameter("@ContestYear", DbType.UInt32)
        prmArray(2).Value = contestYear

        count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_CheckContestantRegistered", prmArray)

        If count > 0 Then
            rtnValue = True
        End If

        Return rtnValue
    End Function

        Protected Sub ckbPrimaryContact_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            If txtPrimaryEmailSp.Text.Length < 3 Then
                lblMsg.Text = "Primary contact needs valid email."
            End If
        End Sub
        'Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        '    Return clientData
        'End Function
        'Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        '    Dim eventArgumentArray() As String
        '    eventArgumentArray = eventArgument.Split("-")
        '    Dim strArgName As String = eventArgumentArray(0)
        '    Dim strArgValue As String = eventArgumentArray(1)
        '    Dim sb As New StringBuilder()
        '    If strArgValue = "IN" Then
        '        Dim dsIndiaStates As DataSet
        '        dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
        '        Dim i As Integer
        '        For i = 0 To dsIndiaStates.Tables(0).Rows.Count - 1
        '            sb.Append(dsIndiaStates.Tables(0).Rows(i).Item("StateName").ToString())
        '            sb.Append("^")
        '            sb.Append(dsIndiaStates.Tables(0).Rows(i).Item("StateCode").ToString())
        '            sb.Append("|")
        '        Next
        '        clientData = sb.ToString()
        '    ElseIf strArgValue = "US" Then
        '        Dim dsStates As DataSet
        '        dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
        '        Dim i As Integer
        '        For i = 0 To dsStates.Tables(0).Rows.Count - 1
        '            sb.Append(dsStates.Tables(0).Rows(i).Item("Name").ToString())
        '            sb.Append("^")
        '            sb.Append(dsStates.Tables(0).Rows(i).Item("StateCode").ToString())
        '            sb.Append("|")
        '        Next
        '        clientData = sb.ToString()
        '    Else
        '        clientData = ""
        '    End If
        'End Sub
        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody

            Dim client As New SmtpClient()
            Dim ok As Boolean = True

            Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

            client.Host = host
            client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            Try
                client.Send(email)
            Catch e As Exception
                ok = False
            End Try
        End Sub
        Private Sub lockchapter(ByVal MemId As Integer)
            Dim chkDate As String
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim EvntYear As Integer = 0
            Dim CurrentYear As Integer = Now.Year()
            EvntYear = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT ContestYear FROM Contestant where contestant_id = (select MAX(contestant_id) FROM  Contestant where ParentID=" & MemId & ")")
            If EvntYear < CInt(CurrentYear) Then
            Else
                chkDate = "09/14/" & EvntYear.ToString()
                If EvntYear = CInt(CurrentYear) And (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then

                Else
                    ddlChapterInd.Enabled = False
                End If
            End If

        End Sub
    End Class
End Namespace

