Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization

Partial Class BadgeNumbers_Gen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim ContestFlag As Boolean = False
        Dim ContestCount As Integer = 0
        Dim ContestcodeAlt As String = ""
        Dim ContestCodeOrg As String = ""
        Dim UpdateSQL As String = ""
        lblmsg.Text = "Badge numbers were generated for " + Request.QueryString("contestcode")

        Dim ContestCode As String = Request.QueryString("contestcode")
        Dim ChapterID As Integer = Request.QueryString("cid")
        hlnkContinue.NavigateUrl = "BadgeNumbers.aspx"

        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim prmArray(3) As SqlParameter
        Dim prmArrayAlt(3) As SqlParameter
        Dim ds As DataSet = Nothing
        Dim ds1 As DataSet = Nothing
        If ContestCode = "JSB/JVB" Then
            ContestCodeOrg = "JSB"
            ContestcodeAlt = "JVB"
        ElseIf ContestCode = "SSB/IVB" Then
            ContestCodeOrg = "SSB"
            ContestcodeAlt = "IVB"
        End If

        Try

            If ContestCode <> "JSB/JVB" And ContestCode <> "SSB/IVB" Then
                prmArray(0) = New SqlParameter("@ChapterID", Request.QueryString("cid"))
                prmArray(1) = New SqlParameter("@ContestCode", Request.QueryString("contestcode"))

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GenerateBadgeNumbers", prmArray)

            ElseIf (ContestCode = "JSB/JVB" Or ContestCode = "SSB/IVB") Then 'JVB" Or ContestCode = "IVB" Or ContestCode = "SSB") And ChapterID = 1 Then

                'If ContestFlag = False Then '''JSB ,SSB Generation 
                prmArray(0) = New SqlParameter("@ChapterID", Request.QueryString("cid"))
                prmArray(1) = New SqlParameter("@ContestCode", ContestCodeOrg) ' Request.QueryString("contestcode"))
                prmArray(2) = New SqlParameter("@ContestCodeAlt", ContestcodeAlt) ' Request.QueryString("contestcode"))

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GenerateBadgeNumbers_Finals_Combined", prmArray)
                ContestCount = ds.Tables(0).Rows(0)(0)

                prmArray(3) = New SqlParameter("@ContestCount", ContestCount)
                'ContestFlag = True

                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GenerateBadgeNumbers_Finals", prmArray)
                tblFinalCount.Visible = True
                lblFinCount.Text = ContestCodeOrg & " Only </br>" & ds1.Tables(0).Rows(0)(0) & "</br>"

                'End If

                'If ContestFlag = True Then ''' JVB,IVB Generation
                prmArrayAlt(0) = New SqlParameter("@ChapterID", Request.QueryString("cid"))
                prmArrayAlt(1) = New SqlParameter("@ContestCode", ContestcodeAlt) ' Request.QueryString("contestcode"))
                prmArrayAlt(2) = New SqlParameter("@ContestCodeAlt", ContestCodeOrg) ' Request.QueryString("contestcode"))

                UpdateSQL = "Update a set a.BadgeNumber = REPLACE(b.BadgeNumber,b.ProductCode,a.ProductCode) from Contestant a Inner Join Contestant b on a.ChildNumber=b.ChildNumber and a.ContestYear =b.ContestYear and a.EventId = b.EventId where a.ContestYear=" & Now.Year() & " and b.ProductCode ='" & ContestCodeOrg & "' and b.BadgeNumber is not null and a.EventId=1 and a.PaymentReference is not null and  a.ProductCode='" & ContestcodeAlt & "' "

                'ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, UpdateSQL) ' "GenerateBadgeNumbers_Finals_Combined", prmArrayAlt)
                'ContestCount = ds.Tables(0).Rows(0)(0)
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, UpdateSQL)

                prmArrayAlt(3) = New SqlParameter("@ContestCount", ContestCount)

                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GenerateBadgeNumbers_Finals", prmArrayAlt)
                lblFinRowCount1.Text = ContestcodeAlt & " Only </br>" & ds1.Tables(0).Rows(0)(0) & "</br>"
                'End If

            End If

            If (ds.Tables.Count > 0) Then
                GrdGenbadge.DataSource = ds
                GrdGenbadge.DataBind()
            End If

        Catch se As SqlException
            lblmsg.Text = se.Message
            Return
        End Try
    End Sub
End Class
