﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Net.Mail
Partial Class CoachingRegistrationUpdate
    Inherits System.Web.UI.Page
    Dim Year As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblErr.Text = ""
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Page.IsPostBack = False Then

            Dim roleID As String = String.Empty
            Try


                roleID = Session("RoleID").ToString()
                hdnRoleID.Value = roleID
            Catch ex As Exception
                roleID = 0
            End Try

            If Now.Month <= 5 Then
                Session("Year") = Now.Year - 1
            Else
                Session("Year") = Now.Year
            End If
            If Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then
                If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                        'more than one 
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        'LoadProductGroup()
                    Else
                        'only one
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                            lblPrd.Text = prd
                            lblPrdGrp.Text = Prdgrp
                        End If
                        'LoadProductGroup()
                    End If
                End If
                If (Request.QueryString("memberID") Is Nothing) Then
                Else
                    Dim memberID As String = Request.QueryString("memberID").ToString()
                    hdnmemberID.Value = memberID
                    LoadCoaching(memberID.Trim)
                End If
            Else
                Response.Redirect("maintest.aspx")
            End If
        End If
    End Sub

    Public Sub getEmailByMemberID(ByVal MemberId As String)

        Try


            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmdtext As String = "select I IP.EMail as ParentEmail from indspouse IP where IP.AutoMemberID=" & MemberId & ""
            Dim drCoaching As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, cmdtext)

            If drCoaching.Tables(0).Rows.Count > 0 Then
                txtUserId.Text = drCoaching.Tables(0).Rows(0)("EMail").ToString()
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtUserId.Text = ""

        LoadCoaching()

    End Sub

    Private Sub LoadCoaching()
        Try
            clear()
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String

            SQLStr = "select distinct EventYear, Semester from EventFees where EventID=13 and(RegOpen='Y' or CalsignupOpen='Y')"
            Dim dsYear As DataSet = New DataSet()
            dsYear = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, SQLStr)
            Dim EventYear As String = ""
            Dim Semester As String = ""
            If (dsYear.Tables(0).Rows.Count > 0) Then
                For Each dr As DataRow In dsYear.Tables(0).Rows
                    EventYear = EventYear + dr("EventYear").ToString() + ","
                    Semester = Semester + "'" & dr("Semester").ToString() & "'" + ","

                Next
            End If
            EventYear = EventYear.TrimEnd(",")
            Semester = Semester.TrimEnd(",")

            SQLStr = "select CR.CoachRegID,CR.Semester, CR.EventYear,C.SignUpID, Case when CR.ChildNumber is null then IP.FirstName+ ' ' +IP.LastName else Ch.FIRST_NAME + ' ' + Ch.LAST_NAME end AS ChildName, Case when P.CoachName is null then P.Name Else P.CoachName End as ProductName,P.ProductID,CR.ProductGroupId,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName, I.Email as CoachEMail,C.Level,C.MaxCapacity, Case when CR.ChildNumber is null then CR.AdultId else CR.ChildNumber end as ChildNumber ,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email,I1.HPhone,I1.CPhone,I1.City,I1.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo,(select count(CR1.CoachRegID),CR.PMemberID, CR.CMemberID from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and   ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null) and C1.EventYear=CR1.EventYear and C1.Semester=CR1.Semester where CR1.Approved='Y' and C1.Accepted='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount, C.MeetingKey, Case when CR.ChildNumber is null then 'Adult' else 'Child' end as Type  "
            SQLStr = SQLStr & " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level " ' ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) "''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
            SQLStr = SQLStr & "  AND C.EventYear=CR.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.EventYear in (" & EventYear & ") and C.Accepted='Y' and C.Semester in (" & Semester & ") " 'C.Enddate>Getdate() and
            If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
                If lblPrdGrp.Text <> "" Then
                    SQLStr = SQLStr & " and P.ProductGroupid in (" & lblPrdGrp.Text & ")"
                End If
                If lblPrd.Text <> "" Then
                    SQLStr = SQLStr & " and P.ProductID in (" & lblPrd.Text & ")"
                End If
            End If
            SQLStr = SQLStr & " ORDER BY Ch.LAST_NAME, Ch.FIRST_NAME "
            'Response.Write(SQLStr)
            Dim drCoaching As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
            Session("drCoaching") = drCoaching
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                lblErr.Text = ""
                dgselected.Visible = True
            Else
                dgselected.Visible = False
                lblErr.Text = "No Coaching found " 'for selected Child"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getMemberid()
        Try
            Dim memberid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
            hdnmemberID.Value = memberid
            If memberid > 0 Then
                LoadCoaching(memberid)

            Else
                lblErr.Text = "Sorry EMail Doesn't Match with our database"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnLoadChild_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtUserId.Text = "" Then
            lblErr.Text = "Enter loginId"
        Else
            getMemberid()
        End If

    End Sub
    Private Sub LoadCoaching(ByVal Memberid As String)
        clear()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select max(EventYEar) from CoachREG where PmemberId=" & Memberid & ""

        SQLStr = "select distinct EventYear, Semester from EventFees where EventID=13 and(RegOpen='Y' or CalsignupOpen='Y')"
        Dim dsYear As DataSet = New DataSet()
        dsYear = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, SQLStr)
        Dim EventYear As String = ""
        Dim Semester As String = ""
        If (dsYear.Tables(0).Rows.Count > 0) Then
            For Each dr As DataRow In dsYear.Tables(0).Rows
                EventYear = EventYear + dr("EventYear").ToString() + ","
                Semester = Semester + "'" & dr("Semester").ToString() & "'" + ","

            Next
        End If
        EventYear = EventYear.TrimEnd(",")
        Semester = Semester.TrimEnd(",")

        hdnEventYears.Value = EventYear
        hdnSemesters.Value = Semester


        SQLStr = "select CR.CoachRegID, CR.Semester, CR.EventYear,C.SignUpID,Case when CR.ChildNumber is null then IP.FirstName+ ' ' +IP.LastName else Ch.FIRST_NAME + ' ' + Ch.LAST_NAME end AS ChildName, Case when P.CoachName is null then P.Name Else P.CoachName End as ProductName,P.ProductID,CR.ProductGroupId,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName, I.Email as CoachEMail,C.Level,C.MaxCapacity,Case when CR.ChildNumber is null then CR.AdultId else CR.ChildNumber end as ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time , C.StartDate,C.Enddate,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email,I1.HPhone,I1.CPhone,I1.City,I1.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo,(select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and   ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null) and C1.EventYear=CR1.EventYear and C1.Semester=CR1.Semester  AND C1.SessionNo=CR1.SessionNo where CR1.Approved='Y' AND C1.Accepted='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount, CR.PMemberID, CR.CMemberID, C.Meetingkey, Case when CR.ChildNumber is null then 'Adult' else 'Child' end as Type "
        SQLStr = SQLStr & " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear=CR.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberID=" & Memberid & " and CR.EventYear in (" & EventYear & ") and C.Accepted='Y' and C.Semester in (" & Semester & ") " 'C.Enddate>Getdate() and '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT

        If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
            If lblPrdGrp.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductGroupid in (" & lblPrdGrp.Text & ")"
            End If
            If lblPrd.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductID in (" & lblPrd.Text & ")"
            End If
        End If
        SQLStr = SQLStr & " ORDER BY Ch.LAST_NAME, Ch.FIRST_NAME "

        Try
            Dim drCoaching As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
            Session("drCoaching") = drCoaching
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                lblErr.Text = ""
                dgselected.Visible = True
            Else
                dgselected.Visible = False
                lblErr.Text = "No Coaching registration record found given Member Email"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadProduct(ByVal ChildNumber As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim SQLStr As String
        SQLStr = "select Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductID from  CoachReg CR"
        SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=13  and Ef.EventYear in(" & hdnEventyear.Value & ")"
        SQLStr = SQLStr & " Inner Join Product P On P.ProductId = EF.ProductID  "
        SQLStr = SQLStr & " Inner Join CalSignUp C  On P.ProductId = C.ProductID And EF.EventYear=C.EventYear"
        SQLStr = SQLStr & " Inner Join IndSpouse I On C.MemberID=I.AutoMemberID "
        If (hdnRegType.Value = "Adult") Then
            SQLStr = SQLStr & " where cr.AdultId = " & ChildNumber '& " And Ch.GRADE Between EF.GradeFrom And Ef.GradeTo " ' and GETDATE()< C.Enddate"
        Else
            SQLStr = SQLStr & " where cr.ChildNumber = " & ChildNumber '& " and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo " ' and GETDATE()< C.Enddate"
        End If

        If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
            If lblPrdGrp.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductGroupid in (" & lblPrdGrp.Text & ")"
            End If
            If lblPrd.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductID in (" & lblPrd.Text & ")"
            End If
        End If
        SQLStr = SQLStr & " Group by P.CoachName,P.ProductCode,P.ProductID"


        Dim drProduct As SqlDataReader
        Try
            drProduct = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            ddlProduct.DataSource = drProduct
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 0 Then
                ddlProduct.Enabled = True
                lblErr.Text = ""
            Else
                lblErr.ForeColor = Color.Red
                lblErr.Text = "No Eligible Coaching Available"
            End If
        Catch ex As Exception
            lblErr.Text = SQLStr
        End Try
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim CoachRegID As Integer = CInt(e.Item.Cells(0).Text)
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM coachreg WHERE CoachRegID =" & CoachRegID.ToString() & "")
            If txtUserId.Text.Length > 3 Then
                getMemberid()
            Else
                LoadCoaching()
            End If
            clear()
        Else


            Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnEdit"), LinkButton)
            If (Not (lbtn) Is Nothing) Then



                Dim CoachRegID As Integer = CInt(e.Item.Cells(0).Text)
                Dim SignUpID As Integer = CInt(e.Item.Cells(3).Text)
                Dim ProductID As Integer = CInt(e.Item.Cells(4).Text)
                Dim childNumber As Integer = CInt(e.Item.Cells(1).Text)
                lblChildNumber.Text = childNumber.ToString()
                hdnChildNumber.Value = childNumber
                Dim approved As Label = CType(e.Item.FindControl("lblApproved"), Label)
                Dim semester As Label = CType(e.Item.FindControl("lblSemester"), Label)
                Dim RegType As Label = CType(e.Item.FindControl("lblRegType"), Label)
                hdnRegType.Value = RegType.Text
                hdnSemester.Value = semester.Text
                Dim level As String = String.Empty
                Dim eventYear As String = String.Empty
                level = CType(e.Item.FindControl("lblLevel"), Label).Text
                eventYear = CType(e.Item.FindControl("lblEventyear"), Label).Text
                hdnLevel.Value = level
                hdnEventyear.Value = eventYear
                Session("Approved") = approved.Text
                LoadProduct(childNumber)

                ddlApproved.SelectedIndex = ddlApproved.Items.IndexOf(ddlApproved.Items.FindByValue(Trim(approved.Text)))
                ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(ProductID))
                If ddlProduct.Items.Count > 0 Then
                    LoadLevel()
                    ddlLevel.SelectedValue = level
                    LoadCoach()
                    ddlCoachName.SelectedIndex = ddlCoachName.Items.IndexOf(ddlCoachName.Items.FindByValue(SignUpID))
                End If
                trEdit.Visible = True
                lblCoachRegID.Text = CoachRegID
                Session("SignUpID") = SignUpID
                If Trim(approved.Text) = "Y" Then
                    lblSignUpID.Text = SignUpID
                End If
                'load product
            End If
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadLevel()
        LoadCoach()
    End Sub

    Private Sub LoadCoach()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select  C.SignUpID,P.ProductCode as ProductName,EF.RegFee,I.FirstName + ' ' + I.LastName + ' : ' + C.Level + ' : '+ CONVERT(CHAR,C.SessionNo)+ ' '+ LEFT(c.Day,3) +', '+ CONVERT(varchar(15),CAST(TIME AS TIME),100) +' ,' + 'Max Capacity: '+ convert(varchar, C.MaxCapacity) +' ,' + 'Approved Count: '+ convert(varchar,(select count(*) from CoachReg where EventYear=C.EventYear and Semester=C.Semester and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and Level=C.Level and SessionNo=C.SessionNo and Approved='Y' and CMemberID=C.MemberID)) as CoachName from  CoachReg CR"
        SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=13  and Ef.EventYear >=" & hdnEventyear.Value
        SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
        SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
        SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
        If (hdnRegType.Value = "Adult") Then
            SQLStr = SQLStr & " where C.Accepted='Y' and C.Semester='" & hdnSemester.Value & "' and cr.AdultId = " & lblChildNumber.Text ' & " and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo " 'and GETDATE()< C.Enddate "
        Else
            SQLStr = SQLStr & " where C.Accepted='Y' and C.Semester='" & hdnSemester.Value & "' and cr.ChildNumber = " & lblChildNumber.Text ' & " and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo " 'and GETDATE()< C.Enddate "
        End If

        If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
            If lblPrdGrp.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductGroupid in (" & lblPrdGrp.Text & ")"
            End If
            If lblPrd.Text <> "" Then
                SQLStr = SQLStr & " and P.ProductID in (" & lblPrd.Text & ")"
            End If
        End If
        SQLStr = SQLStr & " and C.Level='" & ddlLevel.SelectedValue & "'"
        SQLStr = SQLStr & " Order BY I.LastName,I.FirstName"

        Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
        ddlCoachName.DataSource = drCoaching
        ddlCoachName.DataBind()
        ddlCoachName.Visible = True
        If ddlCoachName.Items.Count > 0 Then
            ddlCoachName.Enabled = True
        Else
            ddlCoachName.Enabled = False
            lblErr.Text = "No Eligible Coaching session Available"
        End If
    End Sub


    Protected Sub dgselected_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgselected.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Status As String = CType(DataBinder.Eval(e.Item.DataItem, "Approved"), String)
                If Status.Trim = "Y" Then
                    CType(e.Item.FindControl("lbtnDelete"), LinkButton).Visible = False
                    ' CType(e.Item.FindControl("lblStatus1"), Label).Text = "Paid"
                Else

                    'Dim lbl As Label = CType(e.Item.FindControl("lblStatus1"), Label)
                    'Session("ContestsSelected") = Session("ContestsSelected") & CType(DataBinder.Eval(e.Item.DataItem, "ProductCode"), String) & "(" & CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), String) & ")(" & Session("CustIndID") & ")"
                End If
        End Select
    End Sub

    Protected Sub dgselected_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        dgselected.CurrentPageIndex = e.NewPageIndex
        dgselected.EditItemIndex = -1
        dgselected.DataSource = Session("drCoaching")
        dgselected.DataBind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cmdText As String = "select COUNT(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp C ON CR.ProductID  = C.ProductID and CR.EventYear = C.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo    "
        If (hdnRegType.Value = "Adult") Then
            cmdText = cmdText & " and CR.AdultId = " & lblChildNumber.Text & ""
        Else
            cmdText = cmdText & " and CR.ChildNumber = " & lblChildNumber.Text & ""
        End If
        cmdText = cmdText & " where CR.CoachRegID not in (" & lblCoachRegID.Text & ") and C.SignUpID =" & ddlCoachName.SelectedValue & " and CR.Approved='Y' and CR.Semester='" & hdnSemester.Value & "'"

        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0 Then
            lblErr.Text = "* You cannot select this coach, since you already have a coach for the same level you desire."
        ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN count(CR.CoachRegID) <(select MaxCapacity from CalSignUp where SignUpID=" & ddlCoachName.SelectedValue & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level and C.EventYear=CR.EventYear and C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  where CR.Approved='Y' and CR.Semester='" & hdnSemester.Value & "' AND C.SignUpID=" & ddlCoachName.SelectedValue & "") = "TRUE" Then '   ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)'''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT


            Dim joinUrl As String = "https://northsouth.zoom.us/j/" '" & joinUrl & "'+ C.MeetingKey

            Dim cmdUpdateText As String = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & ", CR.Approved='" & ddlApproved.SelectedValue & "',CR.Grade=Ch.Grade,CR.ProductID=C.ProductID , CR.ProductCode=C.ProductCode ,CR.ProductGroupID=C.ProductGroupID  , CR.ProductGroupCode=C.ProductGroupCode,CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL='" & joinUrl & "'+ C.MeetingKey from CoachReg CR, CalSignUp C,Child Ch where C.SignUpID =" & ddlCoachName.SelectedValue & " and CR.CoachRegID = " & lblCoachRegID.Text & " "
            'If (hdnRegType.Value = "Adult") Then
            '    cmdUpdateText = cmdUpdateText & " and Ch.AdultId=CR.ChildNumber"
            'Else
            '    cmdUpdateText = cmdUpdateText & " and Ch.ChildNumber=CR.ChildNumber"
            'End If

            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdUpdateText)



            'Dim strQuery As String = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level,CR.ModifyDate=Getdate(),CR.ModifiedBy=@LoginID, CR.Approved=@Approved,CR.Grade=Ch.Grade,CR.ProductID=C.ProductID , CR.ProductCode=C.ProductCode ,CR.ProductGroupID=C.ProductGroupID  , CR.ProductGroupCode=C.ProductGroupCode,CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL=@JoinURL from CoachReg CR, CalSignUp C,Child Ch where C.SignUpID =@SignupID and CR.CoachRegID = @CoachRegID and Ch.ChildNumber=CR.ChildNumber"

            'Dim cmd As New SqlCommand(strQuery)
            'cmd.Parameters.AddWithValue("@LoginID", Session("LoginID"))
            'cmd.Parameters.AddWithValue("@Approved", ddlApproved.SelectedValue)
            'cmd.Parameters.AddWithValue("@JoinURL", joinUrl + "C.MeetingKey")
            'cmd.Parameters.AddWithValue("@SignupID", ddlCoachName.SelectedValue)
            'cmd.Parameters.AddWithValue("@CoachRegID", lblCoachRegID.Text)
            'Dim objNSF As NSFDBHelper = New NSFDBHelper()
            ' objNSF.InsertUpdateData(cmd)

            ddlCoachName.Enabled = True

            ProcessCoachEmails()

            If (hdnmemberID.Value <> "") Then
                LoadCoaching(hdnmemberID.Value)

            Else
                LoadCoaching()
            End If
            lblErr.Text = "Updated Successfully"
        Else '************ Special Case When Capacity exceeds****************************'
            If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 96 Or Session("RoleID") = 89 Then
                ddlCoachName.Enabled = False
                trExcCapacity.Visible = True
            Else
                lblErr.Text = "There is no more room for this coach.  Please replace with another coach."
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub

    Private Sub clear()
        ddlProduct.DataSource = Nothing
        ddlProduct.DataBind()
        ddlCoachName.DataSource = Nothing
        ddlCoachName.DataBind()
        trEdit.Visible = False
        lblChildNumber.Text = ""
        lblCoachRegID.Text = ""
        lblSignUpID.Text = ""
    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = System.Configuration.ConfigurationManager.AppSettings("CoachingSupport").ToString()
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        Dim ok As Boolean = True
        Try

            client.Send(mail)

            Dim cmdtext As String = String.Empty
            cmdtext = "insert into SentEmailLog (Source, EventID, ChapterID, LoginEmail, FromEmail, EmailSubject,RoleID, MemberID,  StartTime, TotalEmails_Count, SentEmails_Count, Endtime ) values('CoachRegUpdate', 13, 112, '" & Session("LoginEmail").ToString() & "', '" & sFrom & "', 'Change Coach: Mail to " & sMailTo & "', " & Session("RoleID").ToString() & ", " & Session("LoginID").ToString() & ", getDate(), 1, 1, GetDate())"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdtext)

        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & ", CR.Approved='" & ddlApproved.SelectedValue & "',CR.Grade=Ch.Grade,CR.ProductID=C.ProductID , CR.ProductCode=C.ProductCode ,CR.ProductGroupID=C.ProductGroupID  , CR.ProductGroupCode=C.ProductGroupCode,CR.SessionNo=C.SessionNo from CoachReg CR, CalSignUp C,Child Ch where C.SignUpID =" & ddlCoachName.SelectedValue & " and CR.CoachRegID = " & lblCoachRegID.Text & " and Ch.ChildNumber=CR.ChildNumber")

        ProcessCoachEmails()
        If (hdnmemberID.Value <> "") Then
            LoadCoaching(hdnmemberID.Value)

        Else
            LoadCoaching()
        End If
        lblErr.Text = "Updated Successfully"
        trExcCapacity.Visible = False
    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        trExcCapacity.Visible = False
    End Sub
    Private Sub ProcessCoachEmails()

        Try
            Dim SignUpID1 As Integer, ProductID As Integer
            Dim FrmEMailid, ToEMailid, ParentMailId, FromStatus, ToStatus, MailBody, MailBody1, MailBody2, subj, ProductCode, CoachAdminEmail As String

            FrmEMailid = ""
            ToEMailid = ""
            ParentMailId = ""
            FromStatus = ""
            ToStatus = ""
            MailBody = ""
            MailBody1 = ""
            MailBody2 = ""
            subj = ""
            ProductCode = ""
            CoachAdminEmail = ""

            Dim whrCntn As String = Session("SignUpID")

            If Session("SignUpID") = ddlCoachName.SelectedValue Then ' Change only in Approved Status
                whrCntn = ddlCoachName.SelectedValue
                subj = "One more Change in (Approved flag)"
                MailBody1 = " Approved: " & ddlApproved.SelectedValue & " , Coach: "
                FromStatus = lblApproved.Text.Trim

                If Session("Approved").Trim = "Y" And ddlApproved.SelectedValue.Trim = "N" Then
                    subj = "One more (UnApproved flag)"
                ElseIf Session("Approved").Trim = "N" And ddlApproved.SelectedValue.Trim = "Y" Then
                    subj = "One more (Approved flag)"
                Else
                    lblErr.Text = "No Change done."
                    Exit Sub
                End If
            Else '*******Switching coach for Student*******************'
                subj = "A student is switching"
                whrCntn = whrCntn & "," & ddlCoachName.SelectedValue
            End If

            'Send emails to coaches & CoachAdmin
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select I.Email,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day ,CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.level,C.ProductID, CASE WHEN C.StartDate<GetDate() THEN 'Y' Else 'N' END as Status,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail FROM IndSpouse I INNER JOIN CalSignUp C ON I.AutoMemberID = C.MemberID  Inner JOIN Product P ON C.ProductID=P.ProductID INNER JOIN Child Ch ON Ch.ChildNumber=" & lblChildNumber.Text & " INNER JOIN IndSpouse I2 ON Ch.MEMBERID = I2.AutoMemberID  WHERE C.SignUpID IN(" & whrCntn & ")")
            While reader.Read()
                SignUpID1 = reader("SignUpID")
                ProductCode = reader("ProductCode")
                ProductID = reader("ProductID")
                ParentMailId = reader("ParentEMail")
                MailBody = "Dear Coach, <br><br>Note : Do not reply to the email above.  "
                MailBody = MailBody & "<br><br>Student: " & reader("ChildName") & ", Parent Name: " & reader("ParentName") & ", Email: " & reader("ParentEMail") & " , " & ProductCode & ", "
                '*** Get email of CoachAdmin***************************************************************************'
                CoachAdminEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 I.Email from IndSpouse I Inner JOIN Volunteer V ON V.MemberID=I.AutomemberID and V.RoleID=89 and v.ProductID=" & ProductID & " ORDER BY v.EventYear DESC ")
                ToStatus = ""
                FromStatus = ""
                If Session("SignUpID") <> ddlCoachName.SelectedValue Then 'SignUpID1 <> ddlCoachName.SelectedValue Then
                    'Old Coach
                    FromStatus = reader("Status")
                    FrmEMailid = reader("Email")
                    MailBody1 = " Switching from: " & FrmEMailid & ", Level : " & reader("level") & ", CoachDay:" & reader("Day") & ", Time : " & reader("Time") & "<br> To : "
                Else
                    'New Coach
                    ToStatus = reader("Status")
                    ToEMailid = reader("Email")
                    MailBody2 = ToEMailid & ", Level: " & reader("level") & ", CoachDay:" & reader("Day") & ", Time : " & reader("Time") & ", Approved Date: " & Now.ToString()
                End If
            End While
            MailBody = MailBody & MailBody1 & MailBody2

            If ToStatus.Trim() <> "" And ToStatus.Trim() = "Y" Then
                SendEmail(subj, MailBody, ToEMailid) '--Send  Email to Coach 
            End If
            If FromStatus.Trim() <> "" And FromStatus.Trim() = "Y" Then
                SendEmail(subj, MailBody, FrmEMailid) '--Send Email to Coach 
            End If
            SendEmail(subj, MailBody, CoachAdminEmail) '--Send Email to Coach Admin--

            If ParentMailId.ToString() <> "" Then
                MailBody = "Dear Parent, <br><br>Note : Do not reply to the email above.  "
                MailBody = MailBody & MailBody1 & MailBody2
                SendEmail(subj, MailBody, ParentMailId) '--Send Email to Parent--
            End If

            '*************************Load Grid after Update********************************************'
            If txtUserId.Text.Length > 3 Then
                getMemberid()
            Else
                LoadCoaching()
            End If
            clear()
            lblErr.Text = "Updated Successfully"

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub LoadLevel()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select distinct LevelCode, LevelID from ProdLevel where ProductID=" & ddlProduct.SelectedValue & " and  Eventyear=" & hdnEventyear.Value & "  order by LevelID "

        Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)

        ddlLevel.DataSource = drCoaching
        ddlLevel.DataBind()

        If ddlLevel.Items.Count > 0 Then
            ddlLevel.Enabled = True
            'ddlLevel.SelectedValue = hdnLevel.Value
        Else

        End If
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadCoach()
    End Sub
End Class

