<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/MasterPage.master" Inherits="VRegistration.SendLogonInfo" validateRequest="false" CodeFile="SendLogonInfo.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

    <div>
	<DIV>
	    <asp:button id="btnSend" runat="server" Text="SendEmailstoParents"></asp:button>
	</DIV>
			<DIV>&nbsp;</DIV>
			<P><br>
				<STRONG><U>Invalid Mail Address</U></STRONG></P>
			<P><br>
				&nbsp;</P>
			<div><asp:label id="lblInvalidMailAddress" runat="server"></asp:label></div>
			<DIV>&nbsp;</DIV>
			<DIV>&nbsp;</DIV>
			<DIV><STRONG><U>Duplicate Email Address</U></STRONG></DIV>
			<DIV>&nbsp;</DIV>
			<DIV>
				<div><h2>Select Email Address from Database
					</h2>
				</div>
				<DIV><asp:label id="lblDuplicate" runat="server"></asp:label></DIV>
				<DIV>&nbsp;</DIV>
				<DIV>&nbsp;</DIV>
				<div>
					<asp:Label Runat="server" id="Label1" CssClass="SmallFont"> <b>Please Type the SQL Command to Get the needed Emails Addresses</b> </asp:Label>
				</div>
				<DIV>&nbsp;</DIV>
				<div>
				</div>
				<div>
					<asp:TextBox id="txtSQLCommand" runat="server" MaxLength="4000" Width="800px" TextMode="MultiLine"
						Rows="10" BorderColor="DeepPink" BackColor="#FFE0C0">SELECT IND.LastName, IND.FirstName, LGM.user_email EMail from login_master LGM inner join indSpouse  IND on LGM.user_email = IND.Email where  LGM.user_email like '%teja%'</asp:TextBox></div>
				<DIV>&nbsp;</DIV>
			</DIV>
			<DIV>
				<asp:Button id="btnSubmit" runat="server" Text="Get Email List" CssClass="FormButton"></asp:Button></DIV>
			<DIV>&nbsp;</DIV>
			<DIV>
				<asp:DataGrid id="dgEmail" runat="server" CssClass="GridStyle" AutoGenerateColumns="False" AllowSorting="True"
					BorderWidth="0px" CellSpacing="1" CellPadding="3" Width="100%">
					<FooterStyle CssClass="GridFooter"></FooterStyle>
					<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
					<AlternatingItemStyle Wrap="False" CssClass="GridAltItem"></AlternatingItemStyle>
					<ItemStyle Wrap="False" CssClass="GridItem"></ItemStyle>
					<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="FirstName" HeaderText="First Name"></asp:BoundColumn>
						<asp:BoundColumn DataField="LastName" HeaderText="Last Name"></asp:BoundColumn>
						<asp:BoundColumn DataField="EMail" HeaderText="EMail Address"></asp:BoundColumn>
					</Columns>
				</asp:DataGrid>
			</DIV>
			<br>
			<div>
				<h2>
					Send Emails to Selected Addresses</h2>
			</div>
			<div>
				<br>
				<asp:label Runat="server" CssClass="SmallFont" id="Label2">
				Please Type/Paste the EMail Addresses one in each line of the following text 
				box</asp:label>
			</div>
			<div>
				<br>
				<asp:TextBox id="txtSendList" runat="server" Width="800px" TextMode="MultiLine" BorderColor="Black"
					Height="200px" BackColor="#FFE0C0"></asp:TextBox>
			</div>
			<DIV>&nbsp;</DIV>
			<DIV>Please Type the Email Subject below</DIV>
			<DIV>
				<asp:TextBox id="txtEmailSubject" runat="server" Width="800px" MaxLength="1000" BackColor="#FFE0C0"></asp:TextBox></DIV>
			<DIV>Please Type the Body of the Email Below</DIV>
			<DIV>
				<FTB:FreeTextBox id="txtEmailBodtText" runat="Server" />
			</DIV>
			<DIV>&nbsp;</DIV>
			<DIV>
				<asp:Button id="btnSendEmail" runat="server" Text="Send Emails" CssClass="FormButton"></asp:Button></DIV>
			<DIV>&nbsp;</DIV>
			<DIV>
				<DIV><STRONG><U>Email Problem</U></STRONG></DIV>
				<DIV>
					<asp:label id="lblEMailError" runat="server" Width="336px"></asp:label></DIV>
			</DIV>
			</div>
	</asp:Content>
 

 
 
 