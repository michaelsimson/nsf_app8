<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SVProductGroup.aspx.vb" Inherits="SVProductGroup" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:HyperLink ID="backHyperLink" runat="server" NavigateUrl="~/VolunteerFunctions.aspx"
        Width="93px">Back</asp:HyperLink><br />
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
    <asp:Label ID="Label1" runat="server" Font-Size="Larger" Text="Add /Update ProductGroup"></asp:Label><br />
    <br />
    <asp:DetailsView ID="ProdGroupDetailsView" runat="server" AutoGenerateRows="False"
        DataKeyNames="ProductGroupId" DataSourceID="ProdGroupObjectDataSource" DefaultMode="Insert"
        Height="50px" Width="485px">
        <Fields>
            <asp:TemplateField HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="ProductGroupSqlDataSource"
                        DataTextField="ProductGroupCode" DataValueField="ProductGroupCode" SelectedValue='<%# Bind("ProductGroupCode") %>'>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventCode" SortExpression="EventCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EventCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSource='<%# Eval("EventCode") %>'
                        DataSourceID="EventSqlDataSource" DataTextField="EventCode" DataValueField="EventCode"
                        SelectedValue='<%# Bind("EventCode") %>'>
                    </asp:DropDownList>&nbsp;
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("EventCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:CommandField InsertText="Add" ShowInsertButton="True" />
            <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" InsertVisible="False"
                ReadOnly="True" SortExpression="ProductGroupId" />
        </Fields>
    </asp:DetailsView>
    <br />
    <br />
    <asp:GridView ID="ProdGroupGridView" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataKeyNames="ProductGroupId" DataSourceID="ProdGroupObjectDataSource"
        EmptyDataText="There are no data records to display." Height="151px" PageSize="5"
        Width="482px">
        <Columns>
            <asp:CommandField EditText="Update" ShowEditButton="True" />
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />
            <asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <asp:SqlDataSource ID="ProdGrSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        DeleteCommand="DELETE FROM [ProductGroup] WHERE [ProductGroupId] = @ProductGroupId"
        InsertCommand="INSERT INTO [ProductGroup] ([ProductGroupCode], [EventId], [EventCode], [Name], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy]) VALUES (@ProductGroupCode, @EventId, @EventCode, @Name, @CreateDate, @CreatedBy, @ModifyDate, @ModifiedBy)"
        ProviderName="<%$ ConnectionStrings:NSFConnectionString.ProviderName %>" SelectCommand="SELECT [ProductGroupId], [ProductGroupCode], [EventId], [EventCode], [Name], [CreateDate], [CreatedBy], [ModifyDate], [ModifiedBy] FROM [ProductGroup]"
        UpdateCommand="UPDATE [ProductGroup] SET [ProductGroupCode] = @ProductGroupCode, [EventId] = @EventId, [EventCode] = @EventCode, [Name] = @Name, [CreateDate] = @CreateDate, [CreatedBy] = @CreatedBy, [ModifyDate] = @ModifyDate, [ModifiedBy] = @ModifiedBy WHERE [ProductGroupId] = @ProductGroupId">
        <DeleteParameters>
            <asp:Parameter Name="ProductGroupId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="ProductGroupId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:ObjectDataSource ID="ProdGroupObjectDataSource" runat="server" InsertMethod="InsertProdGr"
        SelectMethod="GetProdGr" TypeName="ProdGroupDataAccess" UpdateMethod="UpdateProdGr">
        <UpdateParameters>
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="original_ProductGroupId" Type="Int32" />
            <asp:Parameter Name="ProductGroupId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="ProductGroupSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="SELECT DISTINCT [ProductGroupCode] FROM [ProductGroup]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="EventSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="SELECT DISTINCT [EventCode] FROM [Event]"></asp:SqlDataSource>
    <br />
</asp:Content>



 
 
 