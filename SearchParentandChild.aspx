﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="SearchParentandChild.aspx.cs" Inherits="SearchParentandChild" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>

        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>
        <script type="text/javascript">



            $(function (e) {
                $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip tos
                    content: {

                        text: function (event, api) {
                            var type = $(this).attr("attr-Type");
                            var tblHtml = "";
                            return $.ajax({
                                url: 'SearchParentandChild.aspx/GetChildInfo', // URL to the local file
                                type: 'POST', // POST or GET
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({ AutoMemberId: $(this).attr("attr-memberid"), Type: type }), // Data to pass along with your request
                                success: function (data) {


                                    tblHtml += "<table style='border: 1px solid black; border-collapse: collapse; '>";
                                    tblHtml += "<tr class='Header'>";

                                    tblHtml += "<td class='Header'>Child Number</td>";
                                    tblHtml += "<td class='Header'>Child Name</td>";
                                    tblHtml += "<td class='Header'>Grade</td>";
                                    tblHtml += "<td class='Header'>Email</td>";
                                    if (data.d.length) {
                                        tblHtml += "</tr>";
                                        $.each(data.d, function (index, value) {

                                            tblHtml += "<tr>";

                                            tblHtml += "<td class='tblBody'>" + value.ChildNumber + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.ChildName + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.grade + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.Email + "</td>";

                                            tblHtml += "</tr>";
                                        });
                                    } else {
                                        tblHtml += "</tr><td class='tblBody' align='center' colspan='4'><span style='color:red;'>No record exists</span></td></tr>";
                                    }
                                    tblHtml += "</table>";


                                }
                            }).then(function (data) {
                                return tblHtml;
                            });
                        },


                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">Child Details</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })


                $(".lnkChildName").qtip({ // Grab some elements to apply the tooltip tos
                    content: {

                        text: function (event, api) {
                            var type = $(this).attr("attr-Type");
                            var tblHtml = "";
                            return $.ajax({
                                url: 'SearchParentandChild.aspx/GetparentsInfo', // URL to the local file
                                type: 'POST', // POST or GET
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({ ChildNumber: $(this).attr("attr-childno") }), // Data to pass along with your request
                                success: function (data) {


                                    tblHtml += "<table style='border: 1px solid black; border-collapse: collapse; '>";
                                    tblHtml += "<tr class='Header'>";

                                    tblHtml += "<td class='Header'>Parent Name</td>";
                                    tblHtml += "<td class='Header'>Email</td>";
                                    tblHtml += "<td class='Header'>HPhone</td>";
                                    tblHtml += "<td class='Header'>CPhone</td>";
                                    tblHtml += "<td class='Header'>Address</td>";
                                    tblHtml += "<td class='Header'>City</td>";
                                    tblHtml += "<td class='Header'>State</td>";
                                    tblHtml += "<td class='Header'>Chapter</td>";

                                    tblHtml += "</tr>";
                                    if (data.d.length) {
                                        $.each(data.d, function (index, value) {

                                            tblHtml += "<tr>";

                                            tblHtml += "<td class='tblBody'>" + value.ParentName + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.Email + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.CPhone + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.HPhone + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.Address + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.City + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.State + "</td>";
                                            tblHtml += "<td class='tblBody'>" + value.Chapter + "</td>";

                                            tblHtml += "</tr>";
                                        });
                                    } else {
                                        tblHtml += "</tr><td class='tblBody' align='center' colspan='4'><span style='color:red;'>No record exists</span></td></tr>"
                                    }
                                    tblHtml += "</table>";


                                }
                            }).then(function (data) {
                                return tblHtml;
                            });
                        },


                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">Parents Details</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })

            });



            function trimTextBox(sender) {

                // keyCode == 32 is space(' ').
                if ((sender.value.length == 0 || sender.selectionStart == 0) && (window.event.keyCode == 32)) {
                    return false;
                }
                else if (sender.value.charCodeAt(0) == 32) {
                    sender.value = sender.value.substring(1);
                }
                return true;
            }



        </script>

        <style type="text/css">
            .qTipWidth {
                max-width: 700px !important;
            }

            .Header {
                border: 1px solid black;
                border-collapse: collapse;
                vertical-align: middle;
                color: black;
                font-weight: bold;
                padding: 3px 3px;
            }

            .tblBody {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 3px 3px;
            }
        </style>
    </div>
    <div style="height: auto;">
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            Search Records of Parents and Children

      
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <table>
            <tr runat="server" visible="false">
                <td width="90px"></td>
                <td>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtChildName" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsearchChild" runat="server" Text="Search"
                    OnClick="btnsearchChild_Click" />
                </td>
                <td width="90px"></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Child Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="Search"
                    OnClick="Button1_Click" /></td>
            </tr>



            <tr>
                <td></td>
                <td style="height: 172px">
                    <div style="width: 200px; float: left; display: none;">
                        <asp:RadioButton ID="RbtnSearchParent" runat="server" GroupName="Search" Checked="true" OnCheckedChanged="RbtnSearchParent_CheckedChanged" AutoPostBack="true" Style="font-weight: bold;" Text="Search Parent Name" /><%--span style="position: relative; left: 50px;"><b>Search Parent Name</b></span>--%>
                    </div>
                    <div style="width: 200px; float: left; display: none;">
                        <asp:RadioButton ID="RbtnSearchChild" runat="server" GroupName="Search" OnCheckedChanged="RbtnSearchChild_CheckedChanged" AutoPostBack="true" Style="font-weight: bold;" Text="Search Child Name" />
                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div>
                        <asp:Panel ID="pIndSearch" Style="margin-left: auto; margin-right: auto; width: 900px;" runat="server" Visible="true">

                            <table border="1" runat="server" align="center" id="Table1" style="text-align: center" width="30%"
                                visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" onkeyup="return trimTextBox(this)" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" onkeyup="return trimTextBox(this)" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TextBox2" onkeyup="return trimTextBox(this)" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Phone Number:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPhoneNo" onkeyup="return trimTextBox(this)" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;MemberId:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtmemberId" onkeyup="return trimTextBox(this)" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="Button2" runat="server" Text="Find"
                                            CausesValidation="False" OnClick="Button2_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnIndClose" runat="server" Text="Close"
                            CausesValidation="False" OnClick="btnIndClose_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lbsearchresults" runat="server" ForeColor="Red"></asp:Label>
                        </asp:Panel>
                    </div>
                </td>
                <td style="height: 172px"></td>

                <td style="height: 172px;">
                    <asp:Panel ID="Pnlchild" runat="server" Visible="false" Style="margin-left: 100px;">
                        <table
                            border="1" runat="server" id="tblIndSearch" style="text-align: center"
                            width="30%" visible="true" bgcolor="silver">
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtChName" OnTextChanged="txtChName_TextChanged"
                                        runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Parent Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtParentName" runat="server"></asp:TextBox></td>
                            </tr>

                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Phone Number Contains:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSearch" runat="server" Text="Find" CausesValidation="False" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnclose" runat="server" Text="Close" OnClick="btnclose_Click" />
                                </td>
                                <%--  <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>--%>
                            </tr>

                        </table>
                        <asp:Label ID="lblchError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="1%">
                    <div>


                        <asp:Panel ID="Panel4" runat="server" HorizontalAlign="Center" Visible="false">
                            <b>Login(Table 1)</b>
                            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVLogin"
                                AutoGenerateColumns="false" runat="server"
                                RowStyle-CssClass="SmallFont">
                                <Columns>

                                    <asp:BoundField DataField="email" HeaderText="Email Address"></asp:BoundField>
                                    <asp:BoundField DataField="Exists" HeaderText="Exists"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </td>
                <td>
                    <asp:Panel ID="Panel3" runat="server" HorizontalAlign="Center" Visible="false">
                        <b>Parent Matches</b>
                        <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVprofile"
                            AutoGenerateColumns="false" runat="server"
                            RowStyle-CssClass="SmallFont" HeaderStyle-BackColor="#ffffcc">
                            <Columns>

                                <asp:BoundField DataField="automemberid" HeaderText="MemberID"></asp:BoundField>
                                <asp:BoundField DataField="firstname" Visible="false" HeaderText="Fist Name"></asp:BoundField>
                                <asp:BoundField DataField="lastname" Visible="false" HeaderText="Last Name"></asp:BoundField>

                                <asp:TemplateField HeaderText="Parent Name">

                                    <ItemTemplate>

                                        <asp:Label runat="server" CssClass="lnkParentName lnkParentId" Style="color: blue; cursor: pointer;" ID="lnkParentNamei" attr-MemberId='<%#DataBinder.Eval(Container.DataItem, "automemberid")%>' attr-Type='<%#DataBinder.Eval(Container.DataItem, "DonorType")%>' Text='<%# Bind("Name")%>' CommandName="SelectLink"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="email" HeaderText="Email"></asp:BoundField>
                                <asp:BoundField DataField="Exists" HeaderText="Login"></asp:BoundField>
                                <asp:BoundField DataField="DonorType" HeaderText="Donor Type"></asp:BoundField>
                                <asp:BoundField DataField="hphone" HeaderText="Hphone"></asp:BoundField>

                                <asp:BoundField DataField="Cphone" HeaderText="Cphone"></asp:BoundField>
                                <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                <asp:BoundField DataField="chapter" HeaderText="Chapter"></asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
            </tr>


            <tr>
                <td></td>
                <td>
                    <div>
                        <div style="clear: both; margin-bottom: 10px;"></div>

                        <asp:Panel ID="Panel5" runat="server" HorizontalAlign="Center" Visible="false">
                            <b>Child Matches</b>
                            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVchild"
                                AutoGenerateColumns="false" runat="server"
                                RowStyle-CssClass="SmallFont" HeaderStyle-BackColor="#1b95e0" HeaderStyle-ForeColor="White">
                                <Columns>

                                    <asp:BoundField DataField="ChildNumber" HeaderText="Child Number" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Child Name" HeaderStyle-ForeColor="White">

                                        <ItemTemplate>

                                            <asp:Label runat="server" CssClass="lnkChildName" Style="color: blue; cursor: pointer;" ID="lnkChildName" attr-ChildNo='<%#DataBinder.Eval(Container.DataItem, "ChildNumber")%>' Text='<%# Bind("ChildName")%>' CommandName="SelectLink"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="ChildName" Visible="false" HeaderText="Child Name" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="LAST_NAME" Visible="false" HeaderText="Last Name" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="DATE_OF_BIRTH" HeaderText="DOB" DataFormatString="{0:d}" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="GRADE" HeaderText="Grade" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="SchoolName" Visible="false" HeaderText="School" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="ParentName" HeaderText="Parent Name" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="ParentsEmail" HeaderText="Parent Email" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="ParentsPhone" HeaderText="Parent Phone" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="Parent2Name" HeaderText="Parent2 Name" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="Parents2Email" HeaderText="Parent2 Email" HeaderStyle-ForeColor="White"></asp:BoundField>
                                    <asp:BoundField DataField="Parents2Phone" HeaderText="Parent2 Phone" HeaderStyle-ForeColor="White"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </td>


            </tr>

        </table>
        <asp:HiddenField ID="hdnSeacrhTerm" runat="server" Value="parent" />
        <input type="hidden" id="hdnMemberId" value="0" />
    </div>
</asp:Content>

