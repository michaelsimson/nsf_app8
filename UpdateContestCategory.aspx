<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="UpdateContestCategory.aspx.vb" Inherits="UpdateContestCategory" title="Contest Category" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div style="text-align:left">
           <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
</div>
<table  cellpadding = "2" cellspacing = "0" border="0" width="700px">
<tr><td align="center" ></td></tr><tr><td align="center" >
<table  cellpadding = "5" cellspacing = "0" border="0">
<tr><td align="left"> Contest Year</td><td align="left" >
 <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" Width="75px" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server" Height="24px">
           </asp:DropDownList>
</td><td align="left" >
        Proposed Year</td><td align="left" >
        <asp:DropDownList ID="ddlPropYear" runat="server" Height="24px" Width="75px">
           </asp:DropDownList>
        </td></tr>
<tr><td align="center" colspan="4">
    <asp:Button ID="btnAddNew" runat="server" Text="Add New" /> &nbsp;
        <asp:Button ID="btnRelpicateAll" runat="server" Text="Replicate All" />  &nbsp;
        <asp:Button ID="btnRelpicateSel" runat="server" Text="Replicate Selected" /></td>
   </tr></table> </td></tr>
<tr><td align="center" >
<table id="traddUpdate" width="400px" runat="server" visible = "false" cellpadding = "2" cellspacing = "0" border="0">
 
<tr><td align="left"> Product Group</td><td align="left" ><asp:DropDownList ID="ddlProductGroup" 
                                    DataTextField="Name" DataValueField="ProductGroupCode" 
                                    OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" 
                                    AutoPostBack="true" Enabled="false"   runat="server" Height="24px" Width="150px"></asp:DropDownList></td></tr>
<tr><td align="left"> Product</td><td align="left" > <asp:DropDownList ID="ddlProduct"  DataTextField="Name" 
               DataValueField="ProductCode" Height="24px" Width="150px" Enabled="false" runat="server">
           </asp:DropDownList></td></tr>
<tr><td align="left"> Ph1Split</td><td align="left" >
 <asp:DropDownList ID="ddlPh1Split" Width="80px" runat="server" style="height: 22px">
   <asp:ListItem Value="N" Selected="True" >N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
 </asp:DropDownList> 
</td> </tr> 

<tr><td align="left"> Ph2Split</td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlPh2Split" runat="server">
        <asp:ListItem Value="N" Selected="True">N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Phase 2 - Reg</td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlPhase2_Reg" runat="server">
        <asp:ListItem Value="N" Selected="True">N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Phase 3 - Reg</td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlPhase3_Reg" runat="server">
        <asp:ListItem Value="N" Selected="True">N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Grade From</td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlGradeFrom" runat="server">
        <asp:ListItem>0</asp:ListItem>
        <asp:ListItem>1</asp:ListItem>
        <asp:ListItem>2</asp:ListItem>
        <asp:ListItem>3</asp:ListItem>
        <asp:ListItem>4</asp:ListItem>
        <asp:ListItem>5</asp:ListItem>
        <asp:ListItem>6</asp:ListItem>
        <asp:ListItem>7</asp:ListItem>
        <asp:ListItem>8</asp:ListItem>
         <asp:ListItem>9</asp:ListItem>
        <asp:ListItem>10</asp:ListItem>
        <asp:ListItem>11</asp:ListItem>
        <asp:ListItem>12</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Grade To</td><td align="left" >
 <asp:DropDownList Width="80px" ID="ddlGradeTo" runat="server">
   <asp:ListItem>0</asp:ListItem>
        <asp:ListItem>1</asp:ListItem>
        <asp:ListItem>2</asp:ListItem>
        <asp:ListItem>3</asp:ListItem>
        <asp:ListItem>4</asp:ListItem>
        <asp:ListItem>5</asp:ListItem>
        <asp:ListItem>6</asp:ListItem>
        <asp:ListItem>7</asp:ListItem>
        <asp:ListItem>8</asp:ListItem>
        <asp:ListItem>9</asp:ListItem>
        <asp:ListItem>10</asp:ListItem>
        <asp:ListItem>11</asp:ListItem>
        <asp:ListItem>12</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Regional Status</td><td align="left" >
<asp:DropDownList Width="80px" ID="ddlRegStatus" runat="server">
   <asp:ListItem>Active</asp:ListItem>
        <asp:ListItem>Inactive</asp:ListItem>
 </asp:DropDownList>
</td> </tr> 
<tr><td align="left"> Regional Fee</td><td align="left" >
    <asp:TextBox  Width="80px" ID="txtRegFee" runat="server" Text="0.00"></asp:TextBox></td></tr>
<tr><td align="left"> Regional DownloadLink</td><td align="left" >
<asp:TextBox ID="txtRegDownloadLink" runat="server" Width="175px" ></asp:TextBox></td> </tr> 

<tr><td align="left"> Regional TaxDed</td><td align="left" > <asp:TextBox ID="txtRegTax"  Width="80px" Text = "0.00" runat="server"></asp:TextBox></td></tr>
<tr><td align = "left" colspan = "2"><b>National Finals : -</b></td></tr>
<tr><td align="left"> Phase 2 - Finals </td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlPhase2_Fin" runat="server">
        <asp:ListItem Value="N" Selected="True">N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left"> Phase 3 - Finals</td><td align="left" >
    <asp:DropDownList Width="80px" ID="ddlPhase3_Fin" runat="server">
        <asp:ListItem Value="N" Selected="True">N</asp:ListItem>
        <asp:ListItem Value="Y">Y</asp:ListItem>
    </asp:DropDownList>
</td></tr>

<tr><td align="left"> National Selection Criteria</td><td align="left" >
<asp:DropDownList Width="120px" ID="ddlNatSelCriteria" runat="server">
 <asp:ListItem Value="I">Invitees Only</asp:ListItem>
 <asp:ListItem Value="O">Open</asp:ListItem>
 </asp:DropDownList>
</td> </tr> 
<tr><td align="left"> National Final Status</td><td align="left" >
<asp:DropDownList Width="80px" ID="ddlNationalStatus" runat="server">
   <asp:ListItem>Active</asp:ListItem>
        <asp:ListItem>Inactive</asp:ListItem>
 </asp:DropDownList>
</td> </tr>
<tr><td align="left"> National finals Fee</td><td align="left" >
    <asp:TextBox Width="80px" ID="txtNationalFinalsFee" runat="server" Text="0.00"></asp:TextBox></td></tr>
<tr><td align="left"> National DownloadLink</td><td align="left" >
<asp:TextBox ID="txtNationalDownloadLink" runat="server" Width="175px" ></asp:TextBox></td> </tr>  
<tr><td align="left"> National Tax Deduction</td><td align="left" >
    <asp:TextBox ID="txtNatTax" Width="80px" runat="server" Text="0.00"></asp:TextBox></td></tr>

<tr><td  align="center" colspan="2" >
    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add" /> &nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnCancel" runat="server" OnClick ="btnCancel_Click" Text="Clear" />&nbsp;&nbsp;&nbsp; 
        <asp:Button ID="btnClose" runat="server" OnClick ="btnClose_Click" Text="Close" />
    
    </td></tr>
<tr><td  align="center" colspan="2" >
    <asp:Label ID="lblErr" ForeColor="Red" runat="server"></asp:Label>&nbsp;<asp:Label Visible="false"  ID="lblEventFeesID" 
        runat="server" ></asp:Label></td></tr>
</table>
</td></tr><tr><td align="center" >
    <asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>
    <asp:Label ID="lblfileErr" ForeColor="Red"  runat="server"></asp:Label>
    <br />Note :  * You cannot update Event and Event year in any record.
</td></tr><tr><td align="center" >
    <asp:DataGrid ID="DGEventFees" runat="server" DataKeyField="ContestCategoryID" 
        AutoGenerateColumns="False"  OnItemCommand="DGEventFees_ItemCommand" CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
<asp:TemplateColumn HeaderText="Select"  HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
 <ItemTemplate>
<asp:CheckBox ID="chkSelect"  runat="server"  />        </ItemTemplate>	
<HeaderStyle ForeColor="White" Font-Bold="true" Wrap="False"></HeaderStyle>
<ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
</asp:TemplateColumn> 
           <ASP:TemplateColumn>
           <ItemTemplate>
		   <asp:LinkButton id="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
		   </ItemTemplate>
		   </ASP:TemplateColumn>
<ASP:TemplateColumn>
           <ItemTemplate>
		   <asp:LinkButton id="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
		   </ItemTemplate>
		   </ASP:TemplateColumn><asp:Boundcolumn DataField="ContestCategoryID"  HeaderText="ContestCategoryID" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestDesc"  HeaderText="ContestDesc" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Ph1Split" HeaderText="Ph1Split"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Ph2Split" HeaderText="Ph2Split"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase2_Reg" HeaderText="Phase2_Reg"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase3_Reg" HeaderText="Phase3_Reg"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="GradeFrom" HeaderText="GradeFrom"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="GradeTo" HeaderText="GradeTo"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="RegionalStatus" HeaderText="RegionalStatus"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="RegionalFee" HeaderText="RegionalFee" DataFormatString="{0:c}"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="DownloadLink" HeaderText="DownloadLink" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="TaxDedRegional" HeaderText="RegTxDedRate" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase2_Fin" HeaderText="Phase2_Fin"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase3_Fin" HeaderText="Phase3_Fin"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="NationalSelectionCriteria" HeaderText="NatSelCriteria"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="NationalFinalsStatus" HeaderText="FinalsStatus"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="NationalFinalsFee" HeaderText="FinalsFee" DataFormatString="{0:c}"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="DownloadLinkNational" HeaderText="NatDownloadLink" />
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="TaxDedNational" HeaderText="NatTxDedRate" />

 </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>
</td></tr></table> 
</asp:Content>


