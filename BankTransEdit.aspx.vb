﻿
Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class BankTransEdit
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim dblRegFee As Double
    Dim BusType As String
    Dim IRSCat As String
    Dim ChapterCode As String
    Dim Amount As String
    Dim DonationType As String
    Dim BankTransID As Integer
    Dim DonorType As String
    Dim purpose As String
    Dim EventName As String
    Dim checkNo As String
    Dim depositSlip As String
    Dim depDate As String
    Dim donDate As String
    Dim TransactionID As Integer
    Dim CheckNumber As String
    Dim BankID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                lblErr.Text = ""
                If Request.QueryString("DType").ToUpper() = "D" Then
                    GetDonationReceipt(Request.QueryString("DDate"), Request.QueryString("DSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
                    GetDonationsInfo(Request.QueryString("DDate"), Request.QueryString("DepositSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
                Else
                    GetExpTransData(Request.QueryString("CheckNumber"), Request.QueryString("DatePaid"), Request.QueryString("BankID"), Request.QueryString("Amount"))
                    GetExpJournalData(Request.QueryString("CheckNumber"), Request.QueryString("DatePaid"), Request.QueryString("BankID"), Request.QueryString("Amount"))
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub


    Public Sub GetDonationReceipt(ByVal Ddate As String, ByVal DSlip As String, ByVal BankID As String, ByVal Amount As Double)
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strsql As String = ""
        Try
            If (BankID = 1 Or BankID = 2 Or BankID = 3) Then
                strsql = "Select BankTransID as TransactionID,Amount,Transdate,Description, DepositNumber, DepSlipNumber," & Request.QueryString("DepositSlip") & " as DepositSlip,BankID from BankTrans where BankID = " & BankID & " and cast(TransDate as Date)>= cast('" & Ddate & "' as Date) and Amount =" & Amount
                If DSlip.ToString().Length > 0 Then
                    strsql = strsql & " AND DepSlipNumber=" & DSlip & ""
                End If

            ElseIf (BankID = 4 Or BankID = 5 Or BankID = 6 Or BankID = 7) Then
                strsql = "Select BrokTransID  as TransactionID,NetAmount as Amount,Transdate,Description,DepCheckNo as DepSlipNumber," & Request.QueryString("DepositSlip") & " as DepositSlip,BankID from BrokTrans where BankID = " & BankID & " and cast(TransDate as Date)>= cast('" & Ddate & "' as Date) and NetAmount =" & Amount
                If DSlip.ToString().Length > 0 Then
                    strsql = strsql & " and DepCheckNo=" & DSlip & ""
                End If
            End If
            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, strsql)

            If dsTransDonation.Tables(0).Rows.Count > 0 Then
                TrDetailView.Visible = True
                grdEditVoucher.DataSource = dsTransDonation.Tables(0)
                grdEditVoucher.DataBind()
                grdEditVoucher.Visible = True
            Else
                grdEditVoucher.DataSource = Nothing
                grdEditVoucher.DataBind()
                lblErr.Text = "Sorry No detailed view to show"
                'TrDetailView.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
            'Response.Write("select BankTransID,Amount,Transdate,Description, DepositNumber, DepSlipNumber    from BankTrans where BankID = 1 and TransType = 'Deposit' and TransDate = '" & Ddate & "'")
        End Try
    End Sub

    'Protected Sub grdEditVoucher_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdEditVoucher.CancelCommand
    '    grdEditVoucher.EditItemIndex = -1
    '    GetDonationReceipt(Request.QueryString("DDate"), Request.QueryString("DSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
    'End Sub

    'Protected Sub grdEditVoucher_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    Try

    '        grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)
    '        BankTransID = CInt(e.Item.Cells(1).Text)
    '        depositSlip = CType(e.Item.Cells(3).FindControl("lbldepositSlip"), Label).Text
    '        'depDate = CType(e.Item.Cells(4).FindControl("lblDepositdate"), Label).Text
    '        'donDate = CType(e.Item.Cells(13).FindControl("lblDonationdate"), Label).Text
    '        Session("BankTransID") = BankTransID
    '        'Session("DepositSlip") = depositSlip
    '        If Request.QueryString("DepositSlip") <> "" Then
    '            Session("DepositSlip") = Request.QueryString("DepositSlip")
    '        Else
    '            Session("DepositSlip") = depositSlip
    '        End If
    '        'Session("DonorType") = DonorType
    '        'Session("BusType") = BusType
    '        'Session("IRSCat") = IRSCat
    '        'Session("DonationType") = DonationType
    '        'Session("ChapterCode") = ChapterCode

    '        'Session("Purpose") = purpose
    '        'Session("EventName") = EventName
    '        'CType(e.Item.FindControl("txtDepositSlip"), TextBox).Text = Request.QueryString("DepositSlip")

    '        GetDonationReceipt(Request.QueryString("DDate"), Request.QueryString("DSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
    '    Catch ex As Exception
    '        'Response.Write(ex.ToString())
    '    End Try
    'End Sub
    Protected Sub grdEditVoucher_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        '**********Added on 05-06-2014 to directly match the deposit slip number to bankTrans table from DonationsInfo table************'
        If e.CommandName = "Select for Match" Then
            grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)
            BankTransID = CInt(e.Item.Cells(1).Text)

            depositSlip = CType(e.Item.Cells(3).FindControl("lbldepositSlip"), Label).Text
            BankID = CInt(e.Item.Cells(6).Text)

            Session("BankTransID") = BankTransID

            If Request.QueryString("DepositSlip") <> "" Then
                Session("DepositSlip") = Request.QueryString("DepositSlip")
            Else
                Session("DepositSlip") = depositSlip
            End If

            depositSlip = Request.QueryString("DepositSlip") 'CType(e.Item.FindControl("txtDepositSlip"), TextBox).Text

            If BankID = 1 Or BankID = 2 Or BankID = 3 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update BankTrans SET DepSlipNumber=" & depositSlip & " where BankTransID = " & Session("BankTransID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
            ElseIf BankID = 4 Or BankID = 5 Or BankID = 6 Or BankID = 6 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update BrokTrans SET DepCheckNo=" & depositSlip & " where BrokTransID = " & Session("BankTransID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
            End If

            '**If validation passess
            grdEditVoucher.EditItemIndex = -1
            GetDonationReceipt(Request.QueryString("DDate"), Request.QueryString("DSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
            'End If
            lblErr.Text = "Updated Successfully"
        End If
    End Sub

    'Protected Sub grdEditVoucher_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    Dim row As Integer = CInt(e.Item.ItemIndex)
    '    ''Dim ddlTemp As DropDownList

    '    ''Dim chapterId As Integer
    '    ''Dim EventId As Integer

    '    ''ddlTemp = e.Item.FindControl("ddlDonorType")
    '    ''DonorType = ddlTemp.SelectedItem.Text

    '    ''ddlTemp = e.Item.FindControl("ddlBusType")
    '    ''BusType = ddlTemp.SelectedItem.Text

    '    ''ddlTemp = e.Item.FindControl("ddlIRSCat")
    '    ''IRSCat = ddlTemp.SelectedItem.Text

    '    ''ddlTemp = e.Item.FindControl("ddlDonationType")
    '    ''DonationType = ddlTemp.SelectedItem.Text

    '    ''ddlTemp = e.Item.FindControl("ddlChapterCode")
    '    ''ChapterCode = ddlTemp.SelectedItem.Text
    '    ''chapterId = ddlTemp.SelectedValue

    '    ''ddlTemp = e.Item.FindControl("ddlEvent")
    '    ''EventName = ddlTemp.SelectedItem.Text
    '    ''EventId = ddlTemp.SelectedValue

    '    ''ddlTemp = e.Item.FindControl("ddlpurpose")
    '    ''purpose = ddlTemp.SelectedItem.Text


    '    ''Amount = CType(e.Item.FindControl("txtAmount"), TextBox).Text
    '    ' ''checkNo = CType(e.Item.FindControl("txtcheckNumber"), TextBox).Text
    '    'CType(e.Item.FindControl("txtDepositSlip"), TextBox).Text = Request.QueryString("DepositSlip")
    '    depositSlip = CType(e.Item.FindControl("txtDepositSlip"), TextBox).Text
    '    'depDate = CType(e.Item.FindControl("txtDepositdate"), TextBox).Text
    '    ''donDate = CType(e.Item.FindControl("txtDonationdate"), TextBox).Text
    '    'If DonorType.Trim = "OWN" Then
    '    '    'Change in DonationsInfo & Organization Table (BusType,IRSCat)
    '    '    If Session("BusType") <> BusType.Trim Or Session("IRSCat") <> IRSCat.Trim Then
    '    '        'Organization Table (BusType,IRSCat)
    '    '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update OrganizationInfo set BusType='" & BusType & "',IRSCat='" & IRSCat & "' where AutoMemberID= " & CType(e.Item.FindControl("lblMemberID"), Label).Text)
    '    '    End If
    '    'End If
    '    If depositSlip = "" Then
    '        lblErr.Text = "Please enter Deposit slip number"
    '        Exit Sub
    '    Else

    '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update BankTrans SET  DepSlipNumber=" & depositSlip & " where BankTransID = " & Session("BankTransID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
    '        lblErr.Text = "Updated Successfully"
    '        '**If validation passess
    '        grdEditVoucher.EditItemIndex = -1
    '        GetDonationReceipt(Request.QueryString("DDate"), Request.QueryString("DSlip"), Request.QueryString("BankID"), Request.QueryString("Amount"))
    '    End If
    'End Sub
    Public Sub GetDonationsInfo(ByVal Ddate As String, ByVal DSlip As String, ByVal BankID As String, ByVal Amount As Double)
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Try
            If Session("DCatID") = 1 Then
                strSql = " Select Distinct sum(Amount) as Amount, DepositSlip, DepositDate,'DGN'+RIGHT('00'+ CONVERT(VARCHAR,BankID),2)+Replace(CONVERT(VARCHAR(10), DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlip) as VoucherNo"
                strSql = strSql & ",BankID from DonationsInfo where METHOD IN ('Check','CASH') and BankID = " & BankID & " and cast(DepositDate as Date)= cast('" & Ddate & "' as Date) and DepositSlip = " & DSlip
                strSql = strSql & " Group By DepositSlip, DepositDate,BankID having Sum(Amount) = " & Amount '& " order by  DepositDate,DepositSlip "
            ElseIf Session("DCatID") = 2 Then
                strSql = "Select Distinct sum(Amount) as Amount, RTRIM(DepositSlipNo) as DepositSlip, DepositDate,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,BankID),2)+Replace(CONVERT(VARCHAR(10), DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlipNo) as VoucherNo ,"
                strSql = strSql & " BankID from OtherDeposits where BankID = " & BankID & " and cast(DepositDate as Date)= cast('" & Ddate & "' as Date) and DepositSlipNo=" & DSlip
                strSql = strSql & " Group By DepositSlipNo, DepositDate,BankID having Sum(Amount) = " & Amount '& " order by  DepositDate,DepositSlip "
            End If
            'Response.Write(strSql)
            Dim dsDonations As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            If dsDonations.Tables(0).Rows.Count > 0 Then
                TrDonationsView.Visible = True
                gvEditDonation.DataSource = dsDonations.Tables(0)
                gvEditDonation.DataBind()
                If Session("DCatID") = 1 Then
                    gvEditDonation.Caption = "Donations Info"
                ElseIf Session("DCatID") = 2 Then
                    gvEditDonation.Caption = "Other Deposits"
                End If
            Else
                gvEditDonation.DataSource = Nothing
                gvEditDonation.DataBind()
                lblErr.Text = "Sorry No Donations Info data to show"
                'TrDonationsView.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
            'Response.Write("<br />" & strSql)
        End Try
    End Sub

    Public Sub GetExpTransData(ByVal CheckNumber As String, ByVal DatePaid As String, ByVal BankID As Integer, ByVal Amount As Double)
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrSQL As String = ""
        Dim GrdCaption As String = ""
        Try
            If (BankID = 1 Or BankID = 2 Or BankID = 3) Then
                StrSQL = "Select BankTransID as TransactionID,Amount,Transdate,Description, CheckNumber," & Request.QueryString("CheckNumber") & " as CheckNo,BankID from BankTrans where BankID = " & BankID & " and TransType in('DEBIT','Check') and TransCat not in ('CreditCard', 'Fee')   and cast(TransDate as Date)>= cast('" & DatePaid & "' as Date) "
                'If CheckNumber.ToString().Length > 0 Then
                '    StrSQL = StrSQL & " AND CheckNumber=" & CheckNumber
                'End If
                StrSQL = StrSQL & " and Abs(Amount) =" & Amount
                GrdCaption = "Bank Transactions Data"
            ElseIf (BankID = 4 Or BankID = 5 Or BankID = 6 Or BankID = 7) Then
                StrSQL = "Select BrokTransID as TransactionID,NetAmount as Amount,Transdate,Description, ExpCheckNo as CheckNumber," & Request.QueryString("CheckNumber") & " as CheckNo,BankID  from BrokTrans where BankID = " & BankID & " and cast(TransDate as Date)>= cast('" & DatePaid & "' as Date)"
                'If CheckNumber.ToString().Length > 0 Then
                '    StrSQL = StrSQL & " AND ExpCheckNo=" & CheckNumber
                'End If
                StrSQL = StrSQL & " and Abs(NetAmount) =" & Amount ' and TransType='DEBIT' and TransCat not in ('CreditCard', 'Fee') 
                GrdCaption = "Brokerage Transactions Data"
            End If

            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)

            If dsTransDonation.Tables(0).Rows.Count > 0 Then
                TrBnkTrans.Visible = True
                grdBnkTransData.DataSource = dsTransDonation.Tables(0)
                grdBnkTransData.DataBind()
                grdBnkTransData.Visible = True
                grdBnkTransData.Caption = GrdCaption
            Else
                grdBnkTransData.DataSource = Nothing
                grdBnkTransData.DataBind()
                lblErr.Text = "Sorry No Bank Transactions Data to show"
                'TrBnkTrans.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdBnkTransData_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdBnkTransData.CancelCommand
        lblErr.Text = ""
        grdBnkTransData.EditItemIndex = -1
        GetExpTransData(Request.QueryString("CheckNumber"), Request.QueryString("DatePaid"), Request.QueryString("BankID"), Request.QueryString("Amount"))
    End Sub
    Protected Sub grdBnkTransData_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            lblErr.Text = ""
            grdBnkTransData.EditItemIndex = CInt(e.Item.ItemIndex)
            TransactionID = CInt(e.Item.Cells(1).Text)
            CheckNumber = CType(e.Item.Cells(2).FindControl("lblCheckNumber"), Label).Text
            Session("TransactionID") = TransactionID
            Session("BankID") = CType(e.Item.Cells(6).FindControl("lblBankID"), Label).Text
            'Session("DepositSlip") = depositSlip
            If Request.QueryString("CheckNumber") <> "" Then
                Session("CheckNumber") = Request.QueryString("CheckNumber")
            Else
                Session("CheckNumber") = CheckNumber
            End If
            GetExpTransData(Request.QueryString("CheckNumber"), Request.QueryString("DatePaid"), Request.QueryString("BankID"), Request.QueryString("Amount"))
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdBnkTransData_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            Dim row As Integer = CInt(e.Item.ItemIndex)
            CheckNumber = CType(e.Item.FindControl("txtCheckNumber"), TextBox).Text

            If CheckNumber = "" Then
                lblErr.Text = "Please enter Check Number"
                Exit Sub
            Else
                If Session("BankID") = "1" Or Session("BankID") = "2" Or Session("BankID") = "3" Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update BankTrans SET  CheckNumber=" & CheckNumber & " where BankTransID = " & Session("TransactionID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
                ElseIf Session("BankID") = "4" Or Session("BankID") = "5" Or Session("BankID") = "6" Or Session("BankID") = "7" Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update BrokTrans SET  ExpCheckNo=" & CheckNumber & " where BrokTransID = " & Session("TransactionID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
                End If

                lblErr.Text = "Updated Successfully"
                '**If validation passess
                grdBnkTransData.EditItemIndex = -1
                GetExpTransData(Request.QueryString("CheckNumber"), Request.QueryString("DatePaid"), Request.QueryString("BankID"), Request.QueryString("Amount"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub GetExpJournalData(ByVal CheckNumber As String, ByVal DatePaid As String, ByVal BankID As Integer, ByVal Amount As Double)
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Try
            Dim StrSQL As String = ""
            StrSQL = "Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber, E.DatePaid,'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,E.BankID "
            StrSQL = StrSQL & " From ExpJournal E  WHERE CheckNumber='" & CheckNumber & "' and BankID =" & BankID & " and DatePaid ='" & DatePaid & "' " 'and TransType='DEBIT' "
            StrSQL = StrSQL & " Group by E.CheckNumber, E.DatePaid,E.BankID"
            StrSQL = StrSQL & " having Sum(E.ExpenseAmount) = " & Amount
            'Response.Write(StrSQL)
            Dim dsDonations As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If dsDonations.Tables(0).Rows.Count > 0 Then
                TrExpJournal.Visible = True
                GVExpJournal.DataSource = dsDonations.Tables(0)
                GVExpJournal.DataBind()
            Else
                GVExpJournal.DataSource = Nothing
                GVExpJournal.DataBind()
                lblErr.Text = "Sorry No ExpJournal data to show"
                'TrExpJournal.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
            'Response.Write("<br />" & strSql)
        End Try

    End Sub


End Class
