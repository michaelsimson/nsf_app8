﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Alumni_Login.aspx.vb" Inherits="Alumni_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style = " width : 1000px; text-align :center ">
		<img src="Newsletter/NSFLogo.gif" /></div> 
		<div id="DivPers" runat = "server" style = " width : 1000px; text-align :center ">
    	
		 <table border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
            <td width="6px" height="6px" background="images/tl.gif"></td>
            <td  height="6px" background="images/t.gif"></td>
            <td width="11px" height="6px" background="images/tr.gif"></td>
            </tr>

            <tr>
            <td  background="images/l.gif"></td>
            <td align="center" style="font-family:Tahoma; font-size:12px; font-weight :bold ">
            
		<table   cellspacing="1" cellpadding="3" align="center">
							<tr bgcolor="#FFFFFF">
								<td align="center" colspan="2" >
								<b>	NSF scholar Alumni Login Form x</b></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="left">E-mail Address:</td>
								<td noWrap align="left"><asp:textbox id="txtPrimaryEmailInd" Width="150px" runat="server" CssClass="SmallFont"></asp:textbox><br /><asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator><br />
                                    <asp:requiredfieldvalidator id="rfvEmail" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="Email is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="left" style="height: 14px">Password :</td>
								<td noWrap align="left" style="height: 14px"><asp:textbox id="txtPwd" TextMode="Password" runat="server" CssClass="SmallFont"></asp:textbox>
							<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" ControlToValidate="txtPwd" Display="Dynamic" ErrorMessage="*"></asp:requiredfieldvalidator>
							</td>
							</tr>
							
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Submit" />
								&nbsp;&nbsp;&nbsp; 
                                    <asp:Button ID="btncancel" OnClick="btncancel_Click" CausesValidation="false" runat="server" Text="cancel" />
								</td></tr> 
								<tr bgcolor="#FFFFFF">
								<td   noWrap align="center" colspan="2">
                                    <asp:Label ID="lblErr" runat="server" ForeColor = "Red"></asp:Label>
								</td></tr> 								
								<tr bgcolor="#FFFFFF">
								<td   noWrap align="center" colspan="2">
                                    <asp:LinkButton ID="LinkButton1" PostBackUrl="Alumni_Pers_Form.aspx" runat="server">New User?</asp:LinkButton>
                                   </td></tr> 								
						</table>
			 <td background="images/r.gif"></td>
            </tr>

            <tr>
            <td width="6px" height="12px" background="images/bl.gif"></td>
            <td height="tpx" background="images/b.gif"></td>
            <td width="11px" height="12px" background="images/br.gif"></td>
            </tr>
            </table>	    
        </div> 

    </div>
    </form>
</body>
</html>
