﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;


public partial class StudentLoginList : System.Web.UI.Page
{
    string coach;

    string strstudent = "select distinct case when CR.AdultId is null then (Ch.FIRST_NAME + ' ' + Ch.LAST_NAME + '- Child') else  (IP.FIRSTNAME + ' ' + IP.LASTNAME + '- Adult') end AS MemberName, case when CR.AdultId is null then Ch.ChildNumber else CR.AdultId end as MemberID ,case when CR.AdultId is null then ch.FIRST_NAME else IP.FirstName end As First_Name,case when CR.AdultId is null then ch.LAST_NAME else IP.LastName end as Last_Name from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId)  INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID  Inner Join CalSignUp C ON CR.CMemberID = C.MemberID   AND C.EventYear = CR.EventYear  AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID";
    string strcoach = "select distinct (I.FirstName + ' ' + I.LastName) as MemberName,I.AutoMemberID as  MemberID,I.FirstName , I.LastName from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID   AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester   Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID ";
    string str1grid = "select ROW_NUMBER() Over (ORDER BY P.ProductGroupId,P.ProductId,C.Level,CR.SessionNo,Ch.LAST_NAME) as SNo,Ch.FIRST_NAME+' '+Ch.LAST_NAME as ChildName,I.FirstName + ' ' + I.LastName as CoachName,CR.Grade,CR.Approved,Ch.Email,Ch.Pwd,I1.FirstName +' '+ I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON   CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.Approved='Y' and C.Accepted='Y'";
    string strgridstud = " select ROW_NUMBER() Over (ORDER BY P.ProductGroupId,P.ProductId,C.Level,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo,Ch.FIRST_NAME+' '+Ch.LAST_NAME as ChildName,CR.Grade,CR.Approved,Ch.Email,Ch.Pwd,I1.FirstName +' '+ I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON   CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.Approved='Y' and C.Accepted='Y'";

    string strname = " select  ROW_NUMBER() Over (ORDER BY P.ProductCode,Ch.FIRST_NAME) as SNo,Ch.FIRST_NAME+' '+Ch.LAST_NAME as ChildName,CR.Grade,CR.Approved,Ch.Email,Ch.Pwd,I1.FirstName +' '+ I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time, 'Child' as Type from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON   CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.Approved='Y' and C.Accepted='Y'";

    string strnamecoach = "select  ROW_NUMBER() Over (ORDER BY P.ProductGroupId,P.ProductId,C.Level,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo,Ch.FIRST_NAME+' '+Ch.LAST_NAME as ChildName,I.FirstName + ' ' + I.LastName as CoachName,CR.Grade,CR.Approved,Ch.Email,Ch.Pwd,I1.FirstName +' '+ I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON   CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.Approved='Y' and C.Accepted='Y'";
    string wherecond = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblmain.Visible = false;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
        {
            Response.Redirect("~/login.aspx?entry=p");
        }
        if (!IsPostBack)
        {
            pnldisp.Visible = false;
            PnlCoachtitle.Visible = false;
            PnlStudenttitle.Visible = false;
            Lbldisp.Visible = false;
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "96")
                {
                    coach = "select distinct ProductGroupCode,ProductGroupId from Product where EventId =13 and Status='O'";

                }
                if (Session["RoleId"].ToString() == "89" || Session["RoleId"].ToString() == "88")
                {
                    if (Session["RoleId"].ToString() == "88")
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select TeamLead from volunteer where MemberID=" + Session["LoginID"].ToString() + "");
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string Team = dt.Rows[0]["TeamLead"].ToString();
                            if (Team != "Y")
                            {
                                coach = " select distinct productGroupCode,ProductGroupId from CalSignUp where Accepted='Y' and MemberID=" + Session["LoginID"].ToString() + " and ProductGroupCode is not null";
                            }
                            else
                            {
                                coach = " select distinct productGroupCode,ProductGroupId from  volunteer where TeamLead='y' and  MemberID=" + Session["LoginID"].ToString() + "  and ProductGroupCode is not null";
                            }

                        }

                    }
                    else
                    {
                        coach = " select distinct productGroupCode,ProductGroupId from  volunteer where   MemberID=" + Session["LoginID"].ToString() + "  and ProductGroupCode is not null";
                    }

                }
            }
        }
        if (!IsPostBack)
        {
            dd_productgroup(ddproductgroup);
            dd_product(ddproduct);
            dd_levelfilter(ddlevel);

            if (coachid.Checked == true)
            {
                dd_Names(strcoach, ddname);
            }
            else if (studentid.Checked == true)
            {
                //string Order = "order by ch.LAST_NAME,ch.FIRST_NAME ";
                dd_Names(strstudent, ddname);
            }
        }
        // Lblmain.Visible = false;
    }



    protected void dd_productgroup(DropDownList ddlPG)
    {

        try
        {

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, coach);
            ddlPG.DataSource = ds;
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count == 1)
            {
                ddlPG.DataTextField = "productgroupcode";
                ddlPG.DataValueField = "ProductGroupId";
                ddlPG.DataBind();
                dd_product(ddproduct);
                dd_levelfilter(ddlevel);
                ddlPG.Enabled = false;
            }
            else if (dt.Rows.Count > 1)
            {
                ddlPG.DataTextField = "productgroupcode";
                ddlPG.DataValueField = "ProductGroupId";
                ddlPG.DataBind();
                ddlPG.Items.Insert(0, "Select");
                dd_product(ddproduct);

            }


        }
        catch (Exception ex)
        {

        }


    }
    protected void dd_product(DropDownList ddlPR)
    {

        try
        {
            string CmdText = "select name,productid from Product where EventId='13' and status='O'";
            if (ddproductgroup.SelectedValue != "Select")
            {
                CmdText += "  and ProductGroupCode='" + ddproductgroup.SelectedItem.Text + "'";
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            //ToString(), CommandType.Text, "select name,productid from Product where  EventId='13' and status='O'");
            ddlPR.DataSource = ds;
            ddlPR.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlPR.DataValueField = "productid";
            ddlPR.DataBind();
            ddlPR.Items.Insert(0, "Select");
        }
        catch (Exception ex)
        {

        }

    }

    protected void dd_levelfilter(DropDownList ddlvl)
    {
        try
        {

            ddlvl.Items.Clear();
            ddlvl.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select distinct LevelCode from ProdLevel where   EventID=13";
            if (ddproductgroup.SelectedValue != "Select")
            {
                cmdText += " and ProductGroupID=" + ddproductgroup.SelectedValue + " ";
            }
            if (ddproduct.SelectedValue != "Select")
            {
                cmdText += " and ProductID=" + ddproduct.SelectedValue + "";
            }

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlvl.DataValueField = "LevelCode";
            ddlvl.DataTextField = "LevelCode";

            ddlvl.DataSource = ds;
            ddlvl.DataBind();
            ddlvl.Items.Insert(0, "Select");

        }
        catch
        {
        }
    }

    protected void dd_Names(string sqlString, DropDownList ddlobject)
    {

        try
        {
            ddlobject.Items.Clear();
            string conn = Application["Connectionstring"].ToString();
            if (studentid.Checked == true)
            {
                sqlString += genWhereConditons(true) + "order by LAST_NAME,FIRST_NAME";
            }
            else
            {
                sqlString += genWhereConditons(true) + "order by I.LastName,I.FirstName";
            }
            DataSet dsNames = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlString);
            ddlobject.DataSource = dsNames;
            ddlobject.DataTextField = "MemberName";
            ddlobject.DataValueField = "MemberID";
            ddlobject.DataBind();
            ddlobject.Items.Insert(0, "Select");

        }
        catch (Exception ex)
        {

        }

    }

    protected void ddproductgroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetResults();
    }

    protected void studentid_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            Pnldefault.Visible = false;
            PnlCoachtitle.Visible = false;
            PnlStudenttitle.Visible = true;
            lblcoach.Visible = false;
            lblstudent.Visible = true;
            lblmain.Visible = true;
            pnldisp.Visible = true;
            //pnlradio.Visible = false;
            pnlradio.Visible = true;

            ddname.Items.Insert(0, "Select");
            // dd_productgroup(ddproductgroup);

            if (coachid.Checked == true)
            {
                dd_Names(strcoach, ddname);
            }
            else if (studentid.Checked == true)
            {
                //string Order = "order by ch.LAST_NAME,ch.FIRST_NAME ";
                dd_Names(strstudent, ddname);
            }

        }
        catch (Exception ex)
        {

        }

    }

    protected void coachid_CheckedChanged1(object sender, EventArgs e)
    {
        try
        {
            PnlCoachtitle.Visible = true;
            PnlStudenttitle.Visible = false;
            Pnldefault.Visible = false;
            lblcoach.Visible = true;
            lblstudent.Visible = false;
            pnldisp.Visible = true;

            //  pnlradio.Visible = false;
            pnlradio.Visible = true;

            lblmain.Visible = true;
            //  ddlevel.Items.Insert(0, "Select");
            ddname.Items.Insert(0, "Select");

            if (coachid.Checked == true)
            {
                dd_Names(strcoach, ddname);
            }
            else if (studentid.Checked == true)
            {
                //string Order = "order by ch.LAST_NAME,ch.FIRST_NAME ";
                dd_Names(strstudent, ddname);
            }

        }
        catch (Exception ex)
        {

        }

    }
    protected void ddproductgroup_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            dd_product(ddproduct);
            dd_levelfilter(ddlevel);

            if (coachid.Checked == true)
            {
                dd_Names(strcoach, ddname);
            }
            else if (studentid.Checked == true)
            {
                //string Order = "order by ch.LAST_NAME,ch.FIRST_NAME ";
                dd_Names(strstudent, ddname);
            }
            GetResults();
            //Response.Write(strstudent + genWhereConditons());
        }
        catch (Exception ex)
        {

        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        GetResults();
    }
    public void GetResults()
    {
        try
        {
            string Querywhere1;

            if (studentid.Checked == true)
            {
                string Adult = ddname.SelectedItem.Text.Split('-')[1].Trim();
                if (Adult == "Adult")
                {
                    strname = " select ROW_NUMBER() Over(ORDER BY P.ProductCode, IP.FIRSTNAME) as SNo,IP.FIRSTNAME + ' ' + IP.LASTNAME as StudentName,CR.Grade,CR.Approved,IP.Email,LM.User_Pwd,I1.FirstName + ' ' + I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15), CAST(TIME AS TIME), 100) as Time, 'Adult' as Type from Coachreg CR Inner join Indspouse IP ON CR.AdultId = IP.AutoMemberId  INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID = C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester = CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID = I.AutoMemberID inner join Login_Master LM on(LM.USer_EMail = IP.Email) where CR.Approved = 'Y' and C.Accepted = 'Y'";
                }
                if (ddname.SelectedItem.Text != "Select")
                {
                    Querywhere1 = strname + genWhereConditons(false);

                }
                else
                {
                    Querywhere1 = strgridstud + genWhereConditons(false);
                }
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Querywhere1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridDisplay.DataSource = ds.Tables[0];
                    GridDisplay.DataBind();
                    Session["dsPaging"] = ds;
                    Lbldisp.Visible = false;
                    GridDisplay.Visible = true;
                }
                else
                {
                    Lbldisp.Visible = true;
                    GridDisplay.Visible = false;

                }


            }
            if (coachid.Checked == true)
            {
                strnamecoach = "   select  ROW_NUMBER() Over (ORDER BY P.ProductGroupId,P.ProductId,C.Level,CR.SessionNo, case when CR.AdultId is null then Ch.LAST_NAME else IP.FirstName end, case when CR.AdultId is null then Ch.FIRST_NAME else IP.LastName end) as SNo, case when CR.AdultId is null then Ch.FIRST_NAME+' '+Ch.LAST_NAME else IP.FirstName+ ' '+IP.LastName end as StudentName,I.FirstName + ' ' + I.LastName as CoachName,CR.Grade,CR.Approved,case when CR.AdultId is null then Ch.Email else IP.Email end as Email, case when CR.AdultId is null then Ch.Pwd else LM.User_Pwd end as Pwd,I1.FirstName +' '+ I1.LastName as FatherName,CR.ProductGroupCode,P.ProductCode,CR.SessionNo,C.Level,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time, case when CR.AdultId is null then 'Child' else 'Adult' end as Type from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId)  INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON   CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID  AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID left join Login_Master LM on (LM.user_email=IP.Email) where CR.Approved='Y' and C.Accepted='Y' ";
                if (ddname.SelectedItem.Text != "Select")
                {
                    Querywhere1 = strnamecoach + genWhereConditons(false);
                }
                else
                {
                    Querywhere1 = str1grid + genWhereConditons(false);
                }
                // string Querywhere = str1grid + genWhereConditons();
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Querywhere1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridDisplay.DataSource = ds.Tables[0];
                    GridDisplay.DataBind();
                    Session["dsPaging"] = ds;
                    GridDisplay.Visible = true;
                    Lbldisp.Visible = false;

                }
                else
                {
                    Lbldisp.Visible = true;
                    GridDisplay.Visible = false;

                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    protected string genWhereConditons(bool Ischange)
    {

        string iCondtions = string.Empty;
        if (ddproductgroup.SelectedItem.Text != "Select")
        {
            iCondtions += " and P.ProductGroupID=" + ddproductgroup.SelectedItem.Value.ToString();
        }
        if (ddproduct.SelectedItem.Text != "Select")
        {
            iCondtions += " and P.ProductID=" + ddproduct.SelectedItem.Value.ToString();
        }

        if (ddlevel.SelectedItem.Text != "Select")
        {
            iCondtions += " and CR.Level='" + ddlevel.SelectedItem.Text + "'";
        }

        if (ddname.Items.Count > 0)
        {
            if (ddname.SelectedItem.Text != "Select" && !Ischange)
            {
                if (studentid.Checked == true)
                {
                    string Adult = ddname.SelectedItem.Text.Split('-')[1].Trim();
                    if (Adult == "Adult")
                    {
                        iCondtions += " and CR.AdultId=" + ddname.SelectedItem.Value.ToString() + "";
                    }
                    else
                    {
                        iCondtions += " and Ch.ChildNumber=" + ddname.SelectedItem.Value.ToString() + "";
                    }
                   
                }
                else
                {
                    iCondtions += " and I.AutoMemberID=" + ddname.SelectedItem.Value.ToString() + "";
                }
            }
        }


        // if (iCondtions != string.Empty)
        // {
        var year = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        iCondtions = "  and CR.EventYear=" + year + iCondtions;

        //}
        return iCondtions;

    }
    protected void ddlevel_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (coachid.Checked == true)
        {

            dd_Names(strcoach, ddname);

        }
        else if (studentid.Checked == true)
        {
            dd_Names(strstudent, ddname);
        }


        GetResults();
    }
    protected void ddproduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        dd_levelfilter(ddlevel);
        if (coachid.Checked == true)
        {
            dd_Names(strcoach, ddname);
        }
        else if (studentid.Checked == true)
        {
            dd_Names(strstudent, ddname);
        }
        GetResults();

    }
    protected void dgvStudent_SelectedIndexChanged(object sender, EventArgs e)
    {



    }


    protected void GridDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        GridDisplay.PageIndex = e.NewPageIndex;
        GridDisplay.DataSource = (DataSet)Session["dsPaging"];
        GridDisplay.DataBind();

    }
    protected void ddname_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetResults();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        pnlradio.Visible = true;
        pnldisp.Visible = false;
        Pnldefault.Visible = true;
        PnlCoachtitle.Visible = false;
        PnlStudenttitle.Visible = false;
        GridDisplay.Visible = false;
        ddlevel.Items.Clear();
        ddname.Items.Clear();

    }
}