﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Web.SessionState
Imports nsf.Entities
Imports nsf.Data.SqlClient
Partial Class PatronRegistration
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim objChapters As New NorthSouth.BAL.Chapter
            
            Page.MaintainScrollPositionOnPostBack = True
            '*** Populate State DropDown
            Dim dsStates As DataSet
            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlStateInd.DataSource = dsStates.Tables(0)
                ddlStateInd.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlStateInd.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlStateInd.DataBind()
                ddlStateInd.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            ddlStateInd.SelectedIndex = 0
            
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect("reg_pay.aspx")
    End Sub


    Private Function loadStates(ByRef ddlControl As DropDownList, ByVal p_country As String) As Boolean
        Dim rtnValue As Boolean
        If p_country = "IN" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet

            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
            If dsIndiaStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsIndiaStates.Tables(0)
                ddlControl.DataTextField = dsIndiaStates.Tables(0).Columns("StateName").ToString
                ddlControl.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        ElseIf p_country = "US" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsStates As DataSet

            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsStates.Tables(0)
                ddlControl.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlControl.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        End If
        Return rtnValue
    End Function

    Sub btnChkSubmit_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Page.IsValid Then
            lblResult.Text = "You Got It!"
            lblResult.ForeColor = Drawing.Color.Green
        Else
            lblResult.Text = "Incorrect"
            lblResult.ForeColor = Drawing.Color.Red
        End If
    End Sub
End Class
