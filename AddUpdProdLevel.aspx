﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddUpdProdLevel.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="AddUpdProdLevel" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>

        <script type="text/javaScript">
            //window.onbeforeunload = function (e) {
            //    var message = "Are you sure ?";
            //    var firefox = /Firefox[\/\s](\d+)/.test(navigator.userAgent);
            //    if (firefox) {
            //        //Add custom dialog
            //        //Firefox does not accept window.showModalDialog(), window.alert(), window.confirm(), and window.prompt() furthermore
            //        var dialog = document.createElement("div");
            //        document.body.appendChild(dialog);
            //        dialog.id = "dialog";
            //        dialog.style.visibility = "hidden";
            //        dialog.innerHTML = message;
            //        var left = document.body.clientWidth / 2 - dialog.clientWidth / 2;
            //        dialog.style.left = left + "px";
            //        dialog.style.visibility = "visible";
            //        var shadow = document.createElement("div");
            //        document.body.appendChild(shadow);
            //        shadow.id = "shadow";
            //        //tip with setTimeout
            //        setTimeout(function () {
            //            document.body.removeChild(document.getElementById("dialog"));
            //            document.body.removeChild(document.getElementById("shadow"));
            //        }, 0);
            //    }

            //    return message;
            //};
            function DeleteConfirm() {


                if (confirm("Are you sure want to delete?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }
            }
        </script>
    </div>

    <asp:Button ID="btnDeleteMeeting" runat="server" Style="display: none;" OnClick="btnDeleteMeeting_Click" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Add/Update Product Level
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <table align="center" style="width: 500px;">
            <tr>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Event</td>
                <td align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>


                <td align="left" nowrap="nowrap" style="font-weight: bold;">Year</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="ddlYear" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>

                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Proposed Year</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="ddlProposedYr" runat="server" Width="100px">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>

                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Button ID="BtnAddNew" runat="server" Text="Add New" Visible="true" OnClick="BtnAddNew_Click" />
                    <asp:Button ID="BtnReplicateAll" runat="server" Text="Replicate All" OnClick="BtnReplicateAll_Click" />
                    <asp:Button ID="BtnReplicateSel" runat="server" Text="Replicate Selecetd" OnClick="BtnReplicateSel_Click" />
                </td>


            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 40px;"></div>
    <div align="center" runat="server" id="dvAddLevel" visible="false">
        <table align="center" style="width: 450px;">
            <tr>
                <td align="left" nowrap="nowrap"><b>Product Group</b></td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" Width="110px"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Grade From</td>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">
                    <asp:DropDownList ID="ddlGradefrom" runat="server">
                         <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>


            </tr>
            <tr>

                <td align="left" nowrap="nowrap"><b>Product</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddlProduct" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" runat="server" Width="110px" AutoPostBack="True">
                         <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>


                <td align="left" nowrap="nowrap" style="font-weight: bold;">Grade To</td>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">
                    <asp:DropDownList ID="ddlGradeTo" runat="server">
                         <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>
            <tr>
                <td align="left" nowrap="nowrap"><b>Level Code</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddlLevelCode" Width="110px" runat="server" OnSelectedIndexChanged="ddlLevelCode_SelectedIndexChanged" AutoPostBack="True">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Mandatory</td>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">
                    <asp:DropDownList ID="ddlMandatory" runat="server">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N">No</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>

            <tr runat="server" visible="false">
                <td align="left" nowrap="nowrap"><b>Level Code</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:TextBox ID="txtLevelCode" runat="server" Width="100px" Height="18px"></asp:TextBox>

                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>


            <%--  <tr>
                <td align="left" nowrap="nowrap"><b>Grade From</b> </td>

                <td align="left" nowrap="nowrap"></td>
            </tr>
            <tr>
                <td align="left" nowrap="nowrap"><b>Grade To</b> </td>

                <td align="left" nowrap="nowrap"></td>
            </tr>
            <tr>
                <td align="left" nowrap="nowrap"><b>Mandatory</b> </td>

                <td align="left" nowrap="nowrap"></td>
            </tr>--%>


            <tr>
                <td colspan="6" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" Visible="true" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Clear" OnClick="btnCancel_Click" />
                </td>


            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <div style="clear: both;"></div>
        <center>
            <asp:Label ID="Label1" runat="server" ForeColor="Green" Font-Bold="true">Table 1: Product Levels</asp:Label></center>
        <div style="clear: both;"></div>
        <div style="clear: both;"></div>
        <center>
            <asp:Label ID="lblLevelStatus" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label></center>
        <div style="clear: both;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdLevel" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 850px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdLevel_RowCommand" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdLevel_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:CheckBox ID="ChkLevel" runat="server" />
                        <div style="display: none;">
                            <asp:Label runat="server" ID="lblPrdLevelId" Text='<%#DataBinder.Eval(Container.DataItem,"ProdLevelID") %>'></asp:Label>
                        </div>

                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button ID="BtnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteLevel" />
                        <div style="display: none;">
                            <asp:Label runat="server" ID="lblProdLevelID" Text='<%#DataBinder.Eval(Container.DataItem,"ProdLevelID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lbEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label runat="server" ID="LblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                            <asp:Label runat="server" ID="LblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblLevel" Text='<%#DataBinder.Eval(Container.DataItem,"LevelCode") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblLevelId" Text='<%#DataBinder.Eval(Container.DataItem,"levelId") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblGradeFrom" Text='<%#DataBinder.Eval(Container.DataItem,"GradeFrom") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblGradeTo" Text='<%#DataBinder.Eval(Container.DataItem,"GradeTo") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblMandatory" Text='<%#DataBinder.Eval(Container.DataItem,"Mandatory") %>'></asp:Label>

                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Event Year"></asp:BoundField>
                <asp:BoundField DataField="EventID" HeaderText="Event ID"></asp:BoundField>
                <asp:BoundField DataField="EventName" HeaderText="Event Name"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupName" HeaderText="Product Group Name"></asp:BoundField>
                <asp:BoundField DataField="ProductName" HeaderText="Product Name"></asp:BoundField>
                <asp:BoundField DataField="LevelCode" HeaderText="Level Code"></asp:BoundField>
                <asp:BoundField DataField="GradeFrom" HeaderText="Grade From"></asp:BoundField>
                <asp:BoundField DataField="GradeTo" HeaderText="Grade To"></asp:BoundField>
                <asp:BoundField DataField="Mandatory" HeaderText="Mandatory"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <input type="hidden" id="hdnProdLevelID" value="0" runat="server" />
</asp:Content>
