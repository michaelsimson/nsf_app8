<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Sponsor_athon.aspx.vb" Inherits="VRegistration.Sponsor_athon" title="Sponsor_Athon Page"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
	         <tr>
	          	    <td class="ContentSubTitle" valign="top" nowrap align="center" colspan="2">
						<h1 align="left"><font face="Arial" color="#0000ff" size="3"><b>Sponsor Walk-a-thon/Marathon/Excel-a-thon</b></font>&nbsp;<br />
						</h1>
					</td>
			 </tr>
			 <tr>
			         <td style="width: 250px">
			            <asp:LinkButton ID="lnkback" runat="server" CausesValidation="false"></asp:LinkButton>
			         </td>
			 </tr>
	</table>
	<table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0">
			<tr>
					<td style="width: 196px" ><asp:label id="lblMessage" runat="server" align="right" CssClass="largewordingbold"></asp:label></td>
			</tr>
			<tr>
					<td class="largewordingbold" style="width: 196px" align="center">Donation Amount:</td>
					<td class="largewordingbold" style="height: 53px" align="left">
					    <asp:textbox id="txtDonation" runat="server"  Width="60px">250</asp:textbox>
                        <asp:HyperLink ID="HlnkTellme" NavigateUrl="reg_donate_tellMeMore.htm" Target="_blank" runat="server">Tell me more</asp:HyperLink>
					    <asp:requiredfieldvalidator id="rfvDonationAmount" runat="server" ErrorMessage="Value Required, please enter 0 or amount of your choice" ControlToValidate="txtDonation" Font-Size="Medium" Font-Bold="True"></asp:requiredfieldvalidator><br />
                        <asp:Label ID="lblInv" runat="server" Text="Make it to the amount you want to contribute."></asp:Label>
						<asp:RangeValidator id="rngDonationAmount" runat="server" CssClass="SmallFont" ErrorMessage="Donation Amount should be Greater than Zero" ControlToValidate="txtDonation" Type="Currency" MinimumValue="1" MaximumValue="1000000" Font-Size="Medium"></asp:RangeValidator><br />
					</td>
			 </tr>
	         <tr>
                    <td align="center" style="width: 196px; height: 48px;">Event:</td>
                    <td align="left" style="height: 48px">
                         <asp:DropDownList runat="server" ID="ddlEvent"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Font-Bold="True"
                               Font-Size="Medium" InitialValue ="-1" ControlToValidate="ddlEvent" ErrorMessage="select donation Event"
                               CssClass="mediumwording"></asp:RequiredFieldValidator>
                     </td>
              </tr>
                <%--<asp:panel id="Panel1" >--%>
              <tr id="TrPurpose" runat="server">
					<td align="center" >Purpose: </td> 
					<td align="left" style="height: 48px">
                          <asp:dropdownlist id="ddlDonationFor" runat="server" ></asp:dropdownlist>
                          <asp:label id="DonationForLabel" runat="server" CssClass="largewordingbold"></asp:label>
                          <asp:requiredfieldvalidator id="rfvDonationFor" runat="server" Font-Bold="True" Font-Size="Medium" ControlToValidate="ddlDonationFor" ErrorMessage="Select Donation Purpose" cssclass="mediumwording"></asp:requiredfieldvalidator>								</td>
			  </tr><%--</asp:panel>--%>
			  <tr>
                    <td align="center" style="width: 196px; height: 51px;">Anonymous:</td>
                    <td align="left" style="height: 51px">
                            <asp:DropDownList runat="server" ID="dllAnonymous">
                                <asp:ListItem>No</asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                            </asp:DropDownList>
                    </td>
              </tr>
              <tr>
                    <td align="center" style="width: 196px; height: 51px;">Matching Gift Flag:</td>
                    <td align="left" style="height: 51px">
                            <asp:DropDownList runat="server" ID="ddlmatching">
                                 <asp:ListItem>No</asp:ListItem>
                                 <asp:ListItem>Yes</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblmatching" runat="server" Text="Check yes, if matching gift form will be filed with your employer"></asp:Label>
                    </td>
              </tr>
              <tr>
                    <td style="width: 196px"> </td>
                    <td class="largewordingbold" align="left">
                             <asp:LinkButton ID="lbContinue" runat="server" Font-Size="Medium" Font-Bold="True">Continue</asp:LinkButton>&nbsp;&nbsp;
                    </td>
              </tr>
		</table>
</asp:Content>



 
 
 