<%@ Page Language="VB" MaintainScrollPositionOnPostback="true" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRaisingCal.aspx.vb" Inherits="FundRaisingCal"     %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
        <script type="text/javascript" language="javascript">
			window.history.forward(1);

			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}

		</script>
		<div align="left" >
<asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
	    <table width="100%">
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
					<td colspan="2" class="Heading" align="center" style="width: 991px" >
						<strong><asp:Label ID="lblHeading1" runat="server" >Calendar for Fundraising Dinner</asp:Label></strong><br />
						<strong><asp:Label ID="lblHeading2" runat="server" ></asp:Label></strong>
					</td>
				</tr>
				<tr id="trEvent" runat = "server" visible="false" >
				<td align="center" colspan="2"> 
                    <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlEventDates_SelectedIndexChanged" ID="ddlEventDates" DataTextField="EventDate" DataValueField="FundRCalID" runat="server">
                    </asp:DropDownList>
                </td> </tr> 
				<tr>
				<td align="center" colspan="2">
                    <asp:Panel Visible="false" ID="PnlMain" runat="server">
                    
				            <table cellpadding = "2" cellspacing = "0" border = "3">
				<tr><td align="left"> <strong> <asp:Label ID="lblYear" runat="server"  Text="Event Year"> </asp:Label></strong></td>
				<td align="left"><asp:DropDownList ID="drpYear" runat="server" AutoPostBack="true"></asp:DropDownList></td>
				<td align="left">Campaign Start Date </td>
				<td align="left"><asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox> <A href="javascript:PopupPicker('txtStartDate', 200, 200);"><IMG src="Images/Calendar.gif" border="0"> </A> 
                    </td>
				<td align="left"> Campaign End Date </td>
				<td align="left"><asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox> <A href="javascript:PopupPicker('txtEndDate', 200, 200);"><IMG src="Images/Calendar.gif" border="0"> </A>



				</td>
				</tr>
				<tr><td align="left">Chapter</td>
				<td align="left">
                    <asp:DropDownList ID="ddlChapter" runat="server">
                    </asp:DropDownList>
                </td> <td colspan="4"></td>
				</tr>
				<tr><td align="left">Event Date</td>
				<td align="left"> 
                    <asp:TextBox ID="txtEventDate" runat="server"></asp:TextBox><A href="javascript:PopupPicker('txtEventDate', 200, 200);"> <IMG src="Images/Calendar.gif" border="0"> </A>
</td>
                <td align="left">Start Time</td><td align="left"><asp:DropDownList ID="ddlStartTime" runat="server">
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							                        <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							                        <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							                        <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							                        <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							                        <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							                        <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							                        <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							                        <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							                        <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							                        <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							                        <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList></td>
                <td align="left">End Time </td><td align="left">
                
                <asp:DropDownList ID="ddlEndTime" runat="server">
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							                        <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							                        <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							                        <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							                        <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							                        <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							                        <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							                        <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							                        <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							                        <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							                        <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							                        <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>
                </td>
				</tr>
				<tr>
				<td align="left">Event Description</td>
				<td align="left" colspan="5">
                    <asp:TextBox ID="txtDescription" TextMode="MultiLine" Height="57px" Width ="500px" runat="server"></asp:TextBox>
				</td>
				</tr> 
				<tr>
				<td align="left">Venue</td>
				<td colspan="5" align="left"><asp:dropdownlist ID="ddlVenue" runat="server" Width="95%" CssClass="SmallFont"  DataTextField="VenueName" DataValueField="VenueID" >
        </asp:dropdownlist></td>
				</tr> 
				<tr>
				<td colspan="6" align="center">
                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
				</td>
				</tr> 
				</table>
				        <asp:Label ID="lblError" runat="server" CssClass="SmallFont" ForeColor="Red"></asp:Label>
                    </asp:Panel>
				</td>
				</tr>
				<tr><td align="center" >
                    <asp:Panel ID="pnlCopy" Visible="false"  runat="server">
                    <table cellpadding="3px">
                    <tr><td colspan="2"> Please select the event date to copy </td> </tr> 
                    <tr><td><asp:DropDownList  ID="ddlCopyEventDT" DataTextField="EventDate" DataValueField="FundRCalID" runat="server">
                    </asp:DropDownList></td><td>
                        <asp:Button ID="btnCopy" runat="server" Text="Copy" OnClick="btnCopy_Click" /></td></tr></table>
                    </asp:Panel>
                    <asp:Panel ID="pnlSelected" Visible="false"  runat="server">
                   <span class ="title04">Selected Sponsorship  Items</span><br />
    <asp:datagrid  id="DGSelected" runat="server" CssClass="GridStyle"  AllowSorting="True" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="3" OnItemCommand="DGSelected_ItemCommand"
              BackColor="White" BorderColor="#E7E7FF" DataKeyField="FundRFeesID" BorderStyle="None" GridLines="Horizontal" >
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle  BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
		<asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
		 <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ></asp:buttoncolumn>
        <asp:BoundColumn  DataField="FundRFeesID" Visible="False"  ></asp:BoundColumn>
        <asp:TemplateColumn HeaderText="ProductGroup" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductGroupID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupID") %>'>	</asp:Label> 												
        <asp:Label id="lblProductGroupCode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupCode") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Product" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label> 											
        <asp:Label id="lblProductCode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
      <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Amount_From" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
      <ItemTemplate >
        <asp:Label ID="lblAmtFrom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FeeFrom", "{0:c}")%>'></asp:Label>
         </ItemTemplate>
      <EditItemTemplate>
       <asp:TextBox Width="50px"  ID="txtAmtFrom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FeeFrom", "{0:F}")%>' CssClass="SmallFont" ></asp:TextBox> </EditItemTemplate>
    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
</asp:TemplateColumn>
 <asp:TemplateColumn  ItemStyle-HorizontalAlign="Right"  HeaderText="Amount_To" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
      <ItemTemplate >
        <asp:Label ID="lblAmtTo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FeeTo", "{0:c}")%>'></asp:Label>
         </ItemTemplate>
      <EditItemTemplate>
       <asp:TextBox Width="50px"  ID="txtAmtTo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FeeTo", "{0:F}")%>' CssClass="SmallFont" ></asp:TextBox> </EditItemTemplate>
    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
</asp:TemplateColumn>

<asp:TemplateColumn  HeaderText="Payment_Method">
      <ItemTemplate>
         <asp:Label ID="lblPaymentMode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.PaymentMode")%>'></asp:Label>
      </ItemTemplate>
 <EditItemTemplate>   
 <asp:DropDownList OnPreRender="SetDropDown_PaymntMthd"  ID="ddlPaymntMthd" runat="server">
             <asp:ListItem   Value="1"  Text="CreditCard only"></asp:ListItem>
              <asp:ListItem   Value="2"  Text="CreditCard or Check only"></asp:ListItem>
               <asp:ListItem   Value="3"  Text="CreditCard, Check or In Kind"></asp:ListItem>
            </asp:DropDownList>
</EditItemTemplate>
<HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
<ItemStyle Width="15%" />
</asp:TemplateColumn>

<asp:TemplateColumn  HeaderText="Exclusive">
      <ItemTemplate>
         <asp:Label ID="lblExclusive" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Exclusive")%>'></asp:Label>
      </ItemTemplate>
 <EditItemTemplate>   
 <asp:DropDownList OnPreRender="SetDropDown_Exclusive"  ID="ddlExclusive" runat="server">
           <asp:ListItem  Text="Yes"></asp:ListItem>
           <asp:ListItem Text="No"></asp:ListItem> 
</asp:DropDownList>
</EditItemTemplate>
<HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
<ItemStyle Width="15%" />
</asp:TemplateColumn>

<asp:TemplateColumn  HeaderText="By_Contest">
      <ItemTemplate>
         <asp:Label ID="lblByContest" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ByContest")%>'></asp:Label>
      </ItemTemplate>
 <EditItemTemplate>   
 <asp:DropDownList OnPreRender="SetDropDown_ByContest"  ID="ddlByContest" runat="server">
           <asp:ListItem  Text="Yes"></asp:ListItem>
           <asp:ListItem  Text="No"></asp:ListItem> 
</asp:DropDownList>
</EditItemTemplate>
<HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
<ItemStyle Width="15%" />
</asp:TemplateColumn>

<asp:TemplateColumn  HeaderText="Share">
      <ItemTemplate>
         <asp:Label ID="lblByShare" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Share")%>'></asp:Label>
      </ItemTemplate>
 <EditItemTemplate>   
 <asp:DropDownList OnPreRender="SetDropDown_Share"  ID="ddlShare" runat="server">
           <asp:ListItem  Text="Yes"></asp:ListItem>
           <asp:ListItem  Text="No"></asp:ListItem> 
</asp:DropDownList>
</EditItemTemplate>
<HeaderStyle Width="15%"  ForeColor="White" Font-Bold="True" />
<ItemStyle Width="15%" />
</asp:TemplateColumn>

<asp:TemplateColumn  ItemStyle-HorizontalAlign="Right"  HeaderText="Tax_Deduction" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
      <ItemTemplate>
        <asp:Label ID="lblTax" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TaxDeduction", "{0:F}")%>'></asp:Label>
         </ItemTemplate>
      <EditItemTemplate>
       <asp:TextBox Width="50px"  ID="txtTax" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TaxDeduction", "{0:F}")%>' CssClass="SmallFont" ></asp:TextBox> </EditItemTemplate>
    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
</asp:TemplateColumn>
</Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
                   <br /> 
				</asp:Panel>
				</td></tr>
				
				
				<tr><td align="center" >
                    <asp:Panel ID="pnlSelect" Visible="false"  runat="server">
                     <span class ="title04">To be Selected</span><br />
    <asp:datagrid  id="dgSelect" OnItemDataBound="dgSelect_ItemDataBound" runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" CellPadding="3" 
          DataKeyField="ProductID" BackColor="White" BorderColor="#E7E7FF" 
          BorderStyle="None" GridLines="Horizontal">
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
			<asp:TemplateColumn HeaderText="Select"  HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
            <ItemTemplate>
  <asp:CheckBox ID="chkSelect" OnCheckedChanged="chkSelect_CheckedChanged" runat="server"  AutoPostBack ="true"    />
            </ItemTemplate>	
                <HeaderStyle ForeColor="White" Font-Bold="true" Wrap="False"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
            </asp:TemplateColumn> 
             <asp:TemplateColumn HeaderText="ProductGroup" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductGroupID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupID") %>'>	</asp:Label> 												
        <asp:Label id="lblProductGroupCode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductGroupCode") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Product" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label> 											
        <asp:Label id="lblProductCode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="Amount_From" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
            <asp:TextBox Width="50px"  ID="txtAmtFrom" runat="server" CssClass="SmallFont" ></asp:TextBox>
<%--            <asp:RangeValidator ID="RangeValidator1"  runat="server" ControlToValidate="txtAmtFrom"  MaximumValue="100000" MinimumValue ="1" Type="Integer" ErrorMessage="*"></asp:RangeValidator>
--%>      </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Amount_To" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
            <asp:TextBox Width="50px"  ID="txtAmtTo" runat="server" CssClass="SmallFont" ></asp:TextBox>
<%--            <asp:RangeValidator ID="RangeValidator2"  runat="server" ControlToValidate="txtAmtTO"  MaximumValue="500000" MinimumValue ="1" Type="Integer" ErrorMessage="*"></asp:RangeValidator>
--%>      </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Payment_Method" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
            <asp:DropDownList ID="ddlPaymntMthd" runat="server">
             <asp:ListItem Value="1"  Text="CreditCard only"></asp:ListItem>
              <asp:ListItem Value="2"   Text="CreditCard or Check only"></asp:ListItem>
               <asp:ListItem Value="3"   Text="CreditCard, Check or In Kind"></asp:ListItem>
            </asp:DropDownList>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Exclusive" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
            <asp:DropDownList ID="ddlExclusive" runat="server">
             <asp:ListItem  Text="Yes"></asp:ListItem>
              <asp:ListItem Selected="True"   Text="No"></asp:ListItem>              
            </asp:DropDownList>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="By_Contest" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
            <asp:DropDownList ID="ddlByContest" runat="server">
             <asp:ListItem  Text="Yes"></asp:ListItem>
              <asp:ListItem Selected="True"   Text="No"></asp:ListItem>              
            </asp:DropDownList>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Share" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
            <asp:DropDownList ID="ddlShare" runat="server">
             <asp:ListItem  Text="Yes"></asp:ListItem>
              <asp:ListItem Selected="True"   Text="No"></asp:ListItem>              
            </asp:DropDownList>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Tax_Deduction" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
            <asp:TextBox Width="50px"  ID="txtTax" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "taxamt") %>'></asp:TextBox>
<%--            <asp:RangeValidator ID="RangeValidator3"  runat="server" ControlToValidate="txtTax"  MaximumValue="100" MinimumValue ="0" Type="Integer" ErrorMessage="*"></asp:RangeValidator>
--%>        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
                    <asp:Button ID="btnContinue" runat="server" Text="Submit" OnClick ="btnContinue_Click" />
                   <br /> <asp:Label ID="lblGridErr" runat="server"  ForeColor="Red" ></asp:Label>
				</asp:Panel>
				</td></tr>
  				</table>
 
    <asp:HiddenField ID="HdnFundRCalID" runat="server" />
    <asp:HiddenField ID="HdnFundRFeesID" runat="server" />
</asp:Content>

