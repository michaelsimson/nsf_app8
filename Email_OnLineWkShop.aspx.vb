Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
' Created by :Shalini

Partial Class Email_OnLineWkShop
    Inherits System.Web.UI.Page
    Dim strApprovedCondition As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("RoleID") = 1
        'Session("LoginId") = 4240

        'User code to initialize the page here
        'only Roleid = 1 or 2, Can access this page
        'Session("RoleID") = 1
        If Not Page.IsPostBack Then
            Session("EmailSource") = "Email_OnlineWS"
            Session("Productid") = ""
            Dim roleid As Integer = Session("RoleID")
            If (roleid = 1) Or (roleid = 2) Or (roleid = 96) Or (roleid = 97) Or (roleid = 5 And Session("SelChapterID") = 1) Then
                ' Load Year into ListBox
                Dim year As Integer = 0
                'year = Convert.ToInt32(DateTime.Now.Year)
                Dim first_year As Integer = Convert.ToInt32(DateTime.Now.Year) - 9

                year = Convert.ToInt32(DateTime.Now.Year) + 2
                Dim count As Integer = year - first_year

                For i As Integer = 0 To count - 1 'first_year To Convert.ToInt32(DateTime.Now.Year)
                    lstyear.Items.Insert(i, Convert.ToString(year - (i + 1)))
                    'year = year - 1
                Next
                lstyear.Items.Insert(0, "All")
                lstyear.Items(2).Selected = True
                Session("Year") = lstyear.Items(2).Text
                loadRegionalCategory()
                LoadProductGroup()   ' method to  Load Product group ListBox
                'LoadProductID()      ' method to  Load Product ID ListBox
                'LoadworkshopDates()
                'LoadRoleforassignedVol() ' method to  Load Assigned Volunteer ListBox
                'LoadRoleforUnassignedVol() ' method to  Load UnAssigned Volunteer ListBox
            Else
                lblerr.Visible = True
                tabletarget.Visible = False
            End If
        End If
        Session("emaillist") = ""
        ' Jan28

    End Sub


    Private Sub LoadProductGroup()
        Dim eventid As String = drpevent.SelectedValue
        Dim strSql As String

        strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, Registration_OnlineWkshop  r where P.ProductGroupCode = r.ProductGroupCode  and P.EventId =20 order by P.ProductGroupID"

        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()
        lstProductGroup.Items.Insert(0, "All")
        lstProductGroup.Items(0).Selected = True
        lstProductGroup_SelectedIndexChanged(lstProductGroup, New EventArgs)
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim eventid As String = drpevent.SelectedValue
        Dim productgroup As String = ""
        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            lstProductid.Enabled = False
            Dim sbvalues As New StringBuilder
            Dim strSql1 As String '= "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"

            strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, Registration_OnlineWkshop  r where P.ProductGroupCode = r.ProductGroupCode  and P.EventId =20 order by P.ProductGroupID"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If
        Dim strSql As String = "Select ProductID,ProductCode, Name from Product where EventID=20 and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductid.DataSource = drproductid
        lstProductid.DataBind()
        lstProductid.Items.Insert(0, "All")
        lstProductid.Items(0).Selected = True
        lstProductid_SelectedIndexChanged(lstProductid, New EventArgs)
    End Sub

    Private Sub LoadRoleforassignedVol()
        Dim strSql As String = "Select RoleID, Name from Role where Chapter = 'Y' order by RoleID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

    End Sub

    Private Sub LoadRoleforUnassignedVol()
        Dim strSql As String = "Select  VolunteerTaskID, TaskDescription from VolunteerTasks where LevelCode in (1,2,3,4) order by VolunteerTaskID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

    End Sub



    Protected Sub rbtall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtall.CheckedChanged
        'on selection change in All from Target
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        ddlWSDate.Enabled = False
        'ckboxExcludePaidReg.Enabled = False
        ddlnfYears.Enabled = True
    End Sub

    Protected Sub btnregisteredparetns_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnregisteredparetns.CheckedChanged
        'on selection change in registered parents  from Target
        ddlnfYears.Enabled = False
        drpregistrationtype.Enabled = True
        drpevent.Enabled = False
        lstProductGroup.Enabled = True
        lstProductid.Enabled = False
        lstyear.Enabled = True

    End Sub




    Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub loadRegionalCategory()
        drpregistrationtype.Items.Clear()
        drpregistrationtype.Items.Add(New ListItem("Paid Registrations", "1"))
        drpregistrationtype.Items.Add(New ListItem("Pending Registrations", "2"))
        drpregistrationtype.Items.Add(New ListItem("Both", "3"))
    End Sub



    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        Dim conn As New SqlConnection(Application("ConnectionString"))


        'Try

        '    Dim cmdText As String = " select ProductGroupId from Product where ProductId=" & lstProductGroup.SelectedValue & ""
        '    Dim Pgid As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
        '    lstProductGroup.SelectedValue = Pgid

        'Catch ex As Exception

        'End Try
        If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item in ProductId is ALL
            Dim sbvalues As New StringBuilder
            Dim strSql As String
            'strSql = "Select ProductID as ID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
            strSql = "Select ProductID as ID,ProductCode, Name from Product where EventID=20 and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If ds.Tables.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    'sbvalues.Append("'")
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    'sbvalues.Append("'")
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("Productid") = sbvalues.ToString
                LoadworkshopDates()
            End If

        Else
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            Dim dr As DataRow
            For i = 0 To lstProductid.Items.Count - 1
                If lstProductid.Items(i).Selected Then
                    dr = dt.NewRow()
                    dr("Values1") = lstProductid.Items(i).Value
                    dt.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dt)
            ds.AcceptChanges()

            If ds.Tables.Count > 0 Then
                Dim sbvalues As New StringBuilder
                Dim a As Integer
                For a = 0 To ds.Tables(0).Rows.Count - 1
                    'sbvalues.Append("'")
                    sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                    'sbvalues.Append("'")
                    If a < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Dim st As String = sbvalues.ToString
                Session("Productid") = sbvalues.ToString
            End If

        End If
        ' End If

        lstyear_SelectedIndexChanged(lstyear, New EventArgs)
    End Sub

    Protected Sub lstyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstyear.SelectedIndexChanged
        'on selection change in year from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstyear.Items.Count - 1
            If lstyear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstyear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Year") = sbvalues.ToString
        End If

        'Jan28

        If lstyear.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            ddlWSDate.Enabled = False
        Else
            ddlWSDate.Enabled = True
            LoadworkshopDates()
        End If
    End Sub
    Protected Sub LoadworkshopDates()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strWSDateQry As String
        'strWSDateQry = "select distinct Convert(nvarchar(10),EventDate, 101) as EventDate from Registration_OnlineWkshop where ProductGroupId In (" & Session("ProductGroupId") & ") and ProductId In (" & Session("Productid") & ") and EventYear In (" & Session("Year") & ")"
        strWSDateQry = "select distinct Convert(nvarchar(10),EventDate, 101) as EventDate from Registration_OnlineWkshop where ProductGroupId In (" & Session("ProductGroupId") & ") "
        If Session("ProductId") <> "" Then
            strWSDateQry = strWSDateQry & " and ProductId In (" & Session("Productid") & ") "
        End If
        strWSDateQry = strWSDateQry & " and EventYear In (" & Session("Year") & ") order by EventDate desc"
        Dim dsWSDate As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strWSDateQry)
        ddlWSDate.DataSource = dsWSDate
        ddlWSDate.DataBind()
        ddlWSDate.Items.Insert(0, "All")
    End Sub

    Protected Sub lstUnassignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Role from Unassigned Volunteer
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)


        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("UnAssignvolunteer") = sbvalues.ToString
        End If
    End Sub







    Protected Sub btnselectmail_Click(sender As Object, e As EventArgs) Handles btnselectmail.Click
        ' Continue to Email button
        ' will select emails depending on selections made
        ' tableemail.Visible = True
        Dim dsEmails As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        Dim eventid As String = drpevent.SelectedValue
        ' Dim chapterid As String = ""

        Session("mailEventID") = drpevent.SelectedItem.Value.ToString()

        Dim strPaidOption As String = ""



        If rbtall.Checked = True Then   ' If selected value is ALL from Target


            If ddlnfYears.SelectedItem.Value = "0" Then
                lblError.Text = "Please Select Number of Years."
                Exit Sub
            Else
                lblError.Text = ""
            End If
            Dim cYear As Integer = DateTime.Now.AddYears(Val(ddlnfYears.SelectedItem.Value)).Year

            If ckboxExcludePaidReg.Checked = True Then
                strApprovedCondition = "and (not(Approved='Y') or Approved is null) "
            Else
                strApprovedCondition = "and (Approved='Y')"
            End If
            Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null AND (year(CreateDate) >=" & cYear.ToString() & " or year(ModifyDate)>=" & cYear.ToString() & ") or exists(select * from Registration_OnlineWkshop N where AutoMemberID=N.MemberId and n.EventYear>=" & cYear.ToString() & " " & strApprovedCondition & " ) or exists(select * from Registration_OnlineWkshop N where Relationship=N.MemberId and N.EventYear>=" & cYear.ToString() & " " & strApprovedCondition & " ) AND (newsletter not in ('2','3') OR (Newsletter is null)) "

            'Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and ((year(CreateDate) >=" & cYear.ToString() & " or year(ModifyDate) >=" & cYear.ToString() & ")or exists(select * from nfg_Transactions N where AutoMemberID=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & ") or exists(select * from nfg_Transactions N where Relationship=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & "))  " & strPaidOption

            'Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and ((year(CreateDate) >=" & cYear.ToString() & " or year(ModifyDate) >=" & cYear.ToString() & "))  and automemberid in (select a.Memberid from Registration_OnlineWkshop a " & strApprovedCondition & ") group by Email"

            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, strAll)

        ElseIf btnregisteredparetns.Checked = True Then 'If selected value is Registered parents from Target


            strApprovedCondition = ""
            ' If Dropdown is selected
            If drpregistrationtype.SelectedItem.Value = 1 Then
                strApprovedCondition = " and (Approved='Y')"
                If ckboxExcludePaidReg.Checked = True Then
                    strApprovedCondition = " and not (Approved='Y')"
                End If
            ElseIf drpregistrationtype.SelectedItem.Value = 2 Then
                strApprovedCondition = " and Approved is null"
                If ckboxExcludePaidReg.Checked = True Then
                    strApprovedCondition = " and not (Approved is not null)"
                End If
            ElseIf drpregistrationtype.SelectedItem.Value = 3 Then

                If ckboxExcludePaidReg.Checked = True Then
                    strApprovedCondition = " and (Approved is null)"
                End If
            End If

            'If Checkbox is checked
            Dim strEventDatequery As String
            ' If ddl Then

            If ddlWSDate.Enabled = False Then
                strEventDatequery = ""
            Else
                strEventDatequery = "and a.EventDate='" & ddlWSDate.SelectedValue & "'"
                If ddlWSDate.SelectedIndex = 0 Then
                    strEventDatequery = ""
                End If

            End If


            If lstyear.Items(0).Selected = True Then  ' if selected item in Event Year is ALL
                Dim sbvalues As New StringBuilder
                Dim i As Integer
                For i = 1 To lstyear.Items.Count - 1
                    sbvalues.Append(lstyear.Items(i).Text + ",")
                Next
                sbvalues.Remove(sbvalues.Length - 1, 1)

                Session("Year") = sbvalues.ToString

            End If

            If eventid = 20 Then

                Dim StrSQL1 As String
                'StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.Memberid from Registration_OnlineWkshop a where a.EventYear in (" & Session("Year") & ") and  a.EventID in (20) and a.ProductId in (" & Session("Productid") & ") and a.ProductGroupId in (" & Session("ProductGroupId") & ")" & strEventDatequery & " " & strApprovedCondition & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and relationship in (select a.Memberid from Registration_OnlineWkshop a where a.EventYear in (" & Session("Year") & ") and  a.EventID in (20) and a.ProductId in (" & Session("Productid") & ") and a.ProductGroupId in (" & Session("ProductGroupId") & ")" & strEventDatequery & " " & strApprovedCondition & "))) group by Email"
                StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.Memberid from Registration_OnlineWkshop a where a.EventYear in (" & Session("Year") & ") and  a.EventID in (20) "
                If Session("Productid") <> "" Then
                    StrSQL1 = StrSQL1 & "  and a.ProductId in (" & Session("Productid") & ") "
                End If
                StrSQL1 = StrSQL1 & strEventDatequery & " " & strApprovedCondition & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and relationship in (select a.Memberid from Registration_OnlineWkshop a where a.EventYear in (" & Session("Year") & ") and  a.EventID in (20) "
                If Session("Productid") <> "" Then
                    StrSQL1 = StrSQL1 & "  and a.ProductId in (" & Session("Productid") & ") "
                End If
                StrSQL1 = StrSQL1 & " and a.ProductGroupId in (" & Session("ProductGroupId") & ")" & strEventDatequery & " " & strApprovedCondition & "))) group by Email"

                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)


            End If
        End If

        ' depending on selections  emails list will be selected
        Dim tblEmails() As String = {"EmailContacts"}
        Dim sbEmailList As New StringBuilder
        If dsEmails.Tables(0).Rows.Count > 0 Then
            Dim ctr As Integer
            For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                If ctr <= dsEmails.Tables(0).Rows.Count - 2 Then
                    sbEmailList.Append(",")
                End If
            Next
            Session("emaillist") = sbEmailList.ToString
        End If
        Session("sentemaillistenable") = "No"
        Response.Redirect("emaillist.aspx")


    End Sub



End Class
