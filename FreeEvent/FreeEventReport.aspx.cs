﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Data;
using System.Security.Cryptography;
using System.Drawing;
public partial class FreeEvent_FreeEventReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {
                hdnMemberID.Value = Session["LoginID"].ToString();
                hdnRoleID.Value = Session["RoleID"].ToString();
                hdnLoginMail.Value = Session["LoginEMail"].ToString();
            }
        }
    }

    public void ExportAttendeesToExcel()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select FR.FrMemberID,FR.Year ,FR.FreeEventID,FR.EventCode ,FR.FirstName ,FR.MiddleName,FR.LastName,FR.Email,FR.PartType ,FR.Attendees ,FR.Status, C.ChapterCode, FE.ChapterID FROM FreeEventReg FR inner join FreeEvent FE on  (FR.FreeEventID=FE.FreeEventID) inner join Chapter C on (C.ChapterID=FE.ChapterID) where FR.Year=" + hdnYear.Value + " and FR.FreeEventID=" + hdnFreeEventId.Value + " order by FR.FirstName, FR.LastName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Free Event Attendees";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Free Event Attendees Report";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Year";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Chapter";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "EventCode";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "First Name";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "Middle Name";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Last Name";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Email";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Part Type";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Attendees";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Status";
                    oSheet.Range["K3"].Font.Bold = true;


                    string EventName = string.Empty;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        //  surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["Year"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterCOde"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];
                        EventName = dr["EventCode"].ToString().Replace(" ", "");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["FirstName"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MiddleName"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["LastName"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["EMail"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["PartType"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["Attendees"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Status"];



                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = EventName + "_Attendees_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }
    public void ExportContestantsToExcel()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select F.ChildName,F.Year, F.SchoolName,P.ProductCode, FE.EventCode, C.ChapterCode, FR.Email, FR.FirstName, FR.LastName, FR.PArtType, F.ChildID, F.Grade  from FreeEventChild F inner join Product P on (F.SB=P.ProductCode or F.VB=P.ProductCode or F.MB=P.ProductCode or F.SC=P.ProductCode or F.GB=P.ProductCode or F.EW=P.ProductCode or F.PS=P.ProductCode or F.BB=P.ProductCode) inner join FreeEvent FE on (FE.FreeEventID=F.FreeEventID) inner join Chapter C on (C.ChapterID=FE.ChapterID) inner join FreeEventReg FR on (FR.FrMemberID=F.FrMemberID)  where P.EventID=2 and F.FreeEventID=" + hdnFreeEventId.Value + " and F.Year=" + hdnYear.Value + " order by F.ChildName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Free Event Contestants";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Free Event Contestants Report";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Year";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Chapter";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "EventCode";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "Child Name";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "School Name";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Grade";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Contest";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Part Type";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Name";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "EMail";
                    oSheet.Range["K3"].Font.Bold = true;




                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["Year"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterCOde"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["ChildName"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["SchoolName"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["Grade"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductCode"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["PartType"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["FirstName"] + " " + dr["LastName"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];



                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "FreeEventContestants_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }
    protected void btnExportToExcelAttendees_Click(object sender, EventArgs e)
    {
        ExportAttendeesToExcel();
    }
    protected void btnExportToExcelContestants_Click(object sender, EventArgs e)
    {
        ExportContestantsToExcel();
    }

    [WebMethod]
    public static int ExportSurveyResponse(int SurveyID)
    {

        int retVal = -1;

        try
        {


            retVal = 1;



        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return 1;

    }


    [WebMethod]
    public static int SendEmailsToPendingRegistarants(string FreeEventID, string Year, string EventName, string ChapterID, string LoginMail, string RoleID, string FrMemberID)
    {

        int retVal = -1;
        string tblHtml = "";
        int totailEmailsCount = 0;
        string subject = "";
        int EmailCount = 0;
        string fromEMail = "nsfcontests@gmail.com";
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select Email from FreeEventReg where FreeEventID=" + FreeEventID + " and Year=" + Year + " and Status='Pending'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string UserEmail = string.Empty;

            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tblHtml = "";
                        totailEmailsCount++;
                        UserEmail += dr["Email"].ToString() + ";";
                        tblHtml += "<table>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";
                        tblHtml += "<span>Dear Registrant,</span> </br>";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";

                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";

                        //Please request for a re-confirmation link again.

                        tblHtml += "<span>Thank you for registering for the Free Event ...." + EventName + ", but it is still pending as you did not confirm.  Without confirmation, you cannot attend and participate in the event.  Since you did not confirm on time, it has expired. Please go back to the Free Event Registration and select the Resend my confirmation link for a re-confirmation link again.</span> </br>";


                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";

                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";

                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<tr>";
                        tblHtml += "<td>";

                        tblHtml += "</td>";
                        tblHtml += "</tr>";

                        tblHtml += "</table>";
                        //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";

                        subject = "North South Foundation: Confirm your registration for a Free Event";

                        try
                        {
                            MailMessage mail = new MailMessage();
                            mail.From = new MailAddress(fromEMail);
                            mail.Body = tblHtml;
                            mail.Subject = subject;

                            //   mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

                            //mail.CC.Add(new MailAddress("michael.simson@capestart.com"));

                            String[] strEmailAddressList = null;
                            String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                            //Match EmailAddressMatch = default(Match);
                            //strEmailAddressList = UserEmail.Replace(',', ';').Split(';');
                            //foreach (object item in strEmailAddressList)
                            //{
                            //    EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);

                            //    if (EmailAddressMatch.Success)
                            //    {
                            //        EmailCount++;


                            //    }
                            //}

                            mail.Bcc.Add(new MailAddress(dr["Email"].ToString().Trim()));



                            SmtpClient client = new SmtpClient();
                            // client.Host = host;
                            mail.IsBodyHtml = true;
                            try
                            {
                                client.Send(mail);
                                retVal = 1;
                                EmailCount++;

                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        catch
                        {
                        }
                    }

                    string StartTime = Convert.ToDateTime(DateTime.Now).ToString();
                    string EndTime = Convert.ToDateTime(DateTime.Now).ToString();
                    cmdText = "insert into SentEmailLog (Source, EventID, ChapterID, LoginEMail, FromEMail, EmailSubject, RoleID, FrMemberID, StartTime, TotalEmails_Count, SentEmails_Count, EndTime) values('Free Event Report',2," + ChapterID + ", '" + LoginMail + "', '" + fromEMail + "', '" + subject + "', " + RoleID + ", " + FrMemberID + ", '" + StartTime + "', " + totailEmailsCount + ", " + EmailCount + ", '" + EndTime + "')";

                    cmd = new SqlCommand(cmdText, cn);

                    cmd.ExecuteNonQuery();

                }
            }
        }

        catch (Exception ex)
        {
        }
        return retVal;

    }

    [WebMethod]
    public static int DeleteRegistrants(string FrMemberId, string FreeEventId)
    {
        string AutomemberId = string.Empty;
        int retVal = -1;
        SqlCommand cmd;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = " delete from FreeEventReg where FrMemberID=" + FrMemberId + " and FreeEventId=" + FreeEventId + "; delete from FreeEventChild where FrMemberID=" + FrMemberId + " and FreeEventId=" + FreeEventId + "";
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;

        }
        catch
        {

        }
        return retVal;
    }
}