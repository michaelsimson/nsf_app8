﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeEventReg.aspx.cs" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="FreeEvent_FreeEventReg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />

    <link href="../css/Loader.css" rel="stylesheet" />
    <script src="../js/jquery.toast.js"></script>


    <style type="text/css">
        .bs-example {
            margin: 20px;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
        }

        .clear {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        var arrContest = [];
        var existChildId = [];
        var contestData;
        var isContestShow = 0;
        var isRegUserAdd = 0;


        $(function (e) {

            $('html, body').animate({
                scrollTop: $("#dvRegSection").offset().top
            }, 2000);

            //   $(document).scrol($('#dvRegSection'), 1000);

            $("#dvUpdateRegistration").hide();
            var year = $("#selyear").val();

            listFreeEvents(0, year);
            //  fillGrade("selGrade1");

            var id = getParameterByName("GUID");
            var memberId = getParameterByName("Memberid")


            if (id != "" && id != undefined) {

                $("#hdnisUpdate").val("1");
                dcryptURL(id);

            } else {
                // addChild(index++);
            }
            //  getAvailableSeats();

            if (memberId != "" && memberId != null) {
                loadProductGroup();
                var firstName = document.getElementById("<%=hdnFirstName.ClientID%>").value;
                var lastName = document.getElementById("<%=hdnLastName.ClientID%>").value;
                var email = document.getElementById("<%=hdnEmail.ClientID%>").value;

                $("#txtFirstName").val(firstName);
                $("#txtLastName").val(lastName);
                $("#txtEmailAddress").val(email == "0" ? "" : email);
                $("#dvNewRegistration").show();
                $("#dvRegHeader").show();
                $("#txtFirstName").attr("disabled", "disabled");
                $("#txtLastName").attr("disabled", "disabled");
                $("#txtEmailAddress").attr("disabled", "disabled");
                $('#selActionType').val(1);
                $('#txtParentName').val(firstName + " " + lastName);
                $('#txtParentEmail').val(email);


                $('#selActionType').attr("disabled", "disabled");
            }

        });

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function getDecryptedValue(URL, param) {
            var url = URL.slice(URL.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }


        function loadProductGroups(gradeFrom, gradeTo, childJSON) {

            showLoader();
            $("#dvContest").show()
            var eventId = 2;
            var year = $("#selyear").val();

            var freeEventID = $("#selEventCode").val();

            var jsonData = JSON.stringify({ Year: year, EventID: freeEventID, GradeFrom: gradeFrom, GradeTo: gradeTo });

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/ListContests",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;
                    contestDate = data;
                    if (JSON.stringify(data.d.length) > 0) {

                        var dHtml = "";
                        $.each(data.d, function (index, value) {

                            //dvHtml += "<div style='width:170px; float:left;'>";
                            //dvHtml += ' <div style="float: left; margin-left: 30px;">';
                            //dvHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductCode + ' id="chk' + value.ProductCode + '" />';
                            //dvHtml += ' </div>';
                            //dvHtml += '<div style="float: left; margin-left: 10px;">';
                            //dvHtml += ' <label for="InputName" class="control-label">' + value.Name + '</label>';
                            //dvHtml += ' </div>'
                            //dvHtml += ' </div>'

                            //if (i == 4) {
                            //    dvHtml += '<div style="clear:both;"></div>';
                            //}

                            if (getContestsCodesbasedOnGrade(value.ProductGroupID, gradeFrom) != "") {

                                i++;

                                dHtml += "<div style='margin-left:0px;'>";
                                dHtml += "<div style='width:235px; float:left;'>";
                                dHtml += ' <div style="float: left;">';


                                dHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductGroupID + ' id="chk' + value.ProductGroupID + '" />';
                                dHtml += ' </div>';
                                dHtml += '<div style="float: left; margin-left: 10px;">';
                                dHtml += ' <label for="InputName" class="control-label">' + getContestsbasedOnGrade(value.ProductGroupID, gradeFrom) + '</label>';
                                dHtml += ' </div>'
                                dHtml += ' </div>'
                                dHtml += ' </div>'



                            }

                        });

                        $("#dvContest").html(dHtml);
                        if (childJSON != "") {


                            $.each(childJSON, function (index1, value1) {
                                if (index1 == 0) {

                                    if (value1.SB != "" && value1.SB != null) {

                                        arrContest.push("8");
                                        $("#chk8").prop("checked", true);

                                    }

                                    if (value1.VB != "" && value1.VB != null) {
                                        arrContest.push("9");
                                        $("#chk9").prop("checked", true);
                                    }

                                    if (value1.MB != "" && value1.MB != null) {
                                        arrContest.push("10");
                                        $("#chk10").prop("checked", true);
                                    }

                                    if (value1.SC != "" && value1.SC != null) {
                                        arrContest.push("26");
                                        $("#chk26").prop("checked", true);
                                    }

                                    if (value1.GB != "" && value1.GB != null) {
                                        arrContest.push("11");
                                        $("#chk11").prop("checked", true);
                                    }

                                    if (value1.EW != "" && value1.EW != null) {
                                        arrContest.push("12");
                                        $("#chk12").prop("checked", true);
                                    }

                                    if (value1.PS != "" && value1.PS != null) {
                                        arrContest.push("13");
                                        $("#chk13").prop("checked", true);
                                    }

                                    if (value1.BB != "" && value1.BB != null) {
                                        arrContest.push("14");
                                        $("#chk14").prop("checked", true);
                                    }

                                }

                            });


                        }
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("change", "#selGrade1", function (e) {
            var grade = $(this).val();

            if (grade != "0") {
                var grades = "";

                var dHtml = "";

                var year = $("#selyear").val();

                var freeEventID = $("#selEventCode").val();

                if (freeEventID == "0") {
                    $(this).val("0");
                    statusMessage("Please select Event Code", "middle-center", "error");

                } else {
                    loadProductGroups(grade, grade, "");
                }

                var childName = $("#txtChildName").val();
                var schoolName = $("#txtSchoolName").val();

                if (schoolName != "" && childName != "" && $("#selEventCode").val() != "0") {
                    $("#iTopRow").removeClass("btnAddChildDisabled");
                    $("#iTopRow").removeClass("ancDelete");
                    $("#iTopRow").addClass("btnAddChild");
                    $("#iTopRow").css("cursor", "pointer");
                    $("#iTopRow").css("color", "");
                    $("#iTopRow").show();
                }

            }
        });

        $(document).on("keyup", "#txtParentName", function (e) {

            var adultName = $("#txtParentName").val();
            var adultLastName = $("#txtAttendeeLastName").val();
            var attendeeCount = $("#txtAttendeesCount").val();

            if (adultName != "" && adultLastName != "") {
                if (parseInt(attendeeCount) == 0) {
                    $("#txtAttendeesCount").val("1");
                }
                enableDelIcon();

            }
        });
        $(document).on("keyup", "#txtAttendeeLastName", function (e) {

            var adultName = $("#txtParentName").val();
            var adultLastName = $("#txtAttendeeLastName").val();
            var attendeeCount = $("#txtAttendeesCount").val();
            if (adultName != "" && adultLastName != "") {
                if (parseInt(attendeeCount) == 0) {
                    $("#txtAttendeesCount").val("1");
                }
                enableDelIcon();
            }
        });

        function enableDelIcon() {
            $("#iAdultRow").removeClass("btnAddAdultDisabled");
            $("#iAdultRow").removeClass("btnAddAdult");
            $("#iAdultRow").addClass("ancAdultDelete");
            $("#iAdultRow").css("cursor", "pointer");
            $('#iTopRow').removeClass("fa-user-plus");
            $('#iAdultRow').addClass("fa-user-times");
            $('#iAdultRow').attr("title", "Remove Attendee");
            $("#iAdultRow").css("color", "");
            $("#iAdultRow").show();
        }


        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }


        function validateEvent() {
            var retval = 1;
            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            var firstName = $("#txtFirstName").val();
            var middleName = $("#txtMiddleName").val();
            var lastName = $("#txtLastName").val();
            var partType = participantType;
            var emailAddress = $("#txtEmailAddress").val();
            var attendees = $("#txtAttendeesCount").val();
            //var fees = $("#txtFee").val();

            if (year == "0") {
                retval = "-1";
                statusMessage("Please select Year", "middle-center", "error");
            } else if (eventId == "0") {
                retval = "-1";
                statusMessage("Please select Event Code", "middle-center", "error");
            } else if (firstName == "") {
                retval = "-1";
                statusMessage("Please enter First Name", "middle-center", "error");



            } else if (lastName == "") {
                retval = "-1";
                statusMessage("Please enter Last Name", "middle-center", "error");



            } else if (emailAddress == "") {
                retval = "-1";
                statusMessage("Please enter Email address", "middle-center", "error");
            } else if (attendees == "0") {
                retval = "-1";
                statusMessage("Please enter Number of Attendees", "middle-center", "error");
            } else if (partType == "") {
                retval = "-1";
                statusMessage("Please select participant Type", "middle-center", "error");
            }

            //else if (fees == "") {
            //    retval = "-1";
            //    statusMessage("Fees should not be empty.", "middle-center", "error");
            //} 
            //else if (fees == "0") {
            //    retval = "-1";
            //    statusMessage("Fees should be greater than 0.", "middle-center", "error");
            //} 
            //else if (partType == "") {
            //    retval = "-1";
            //    statusMessage("Please select participant Type", "middle-center", "error");
            //}

            if (emailAddress != "") {

                if (!ValidateEmail(emailAddress)) {
                    retval = "-1";
                    statusMessage("Please enter valid Email Address", "middle-center", "error");
                }
            }

            return retval;
        }
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };

        function validateOnAddChild() {
            var retVal = 1;
            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            var isChildOptionUsed = -1;

            if ((schoolName != "" && schoolName != undefined) || (grade != "0" && grade != undefined) || (childName != "" && childName != undefined)) {
                isChildOptionUsed = 1;
            }
            if (isChildOptionUsed == 1) {

                if (childName == "") {
                    retVal = "-1";
                    statusMessage("Please enter Child Name", "middle-center", "error");
                    return retVal;
                } else if (grade == "0") {
                    retVal = "-1";
                    statusMessage("Please select Grade", "middle-center", "error");
                    return retVal;
                } else if (schoolName == "") {
                    retVal = "-1";
                    statusMessage("Please enter school Name", "middle-center", "error");
                    return retVal;
                } else if (arrContest.length <= 0 && isContestShow == 1) {
                    retVal = "-1";
                    statusMessage("Enter the information of a child only if participating in a contest.<br>If a child is not participating in a contest, remove his/her detail. Update only the number of attendees.", "middle-center", "error");
                    return retVal;
                }
            }

            if (arrChildId.length > 0) {
                for (var i = 0; i < arrChildId.length; i++) {

                    isChildOptionUsed = -1;
                    schoolName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    childName = $("#txtChildName" + arrChildId[i] + "").val();
                    grade = $("#selGrades" + arrChildId[i] + "").val();

                    var arrContestMult = [];
                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk8" + arrChildId[i] + "").val());
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk9" + arrChildId[i] + "").val());
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk10" + arrChildId[i] + "").val());
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk11" + arrChildId[i] + "").val());
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk12" + arrChildId[i] + "").val());
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk13" + arrChildId[i] + "").val());
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk14" + arrChildId[i] + "").val());
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk26" + arrChildId[i] + "").val());
                    }

                    if ((schoolName != "" && schoolName != undefined) || (grade != "0" && grade != undefined) || (childName != "" && childName != undefined)) {
                        isChildOptionUsed = 1;
                    }
                    if (isChildOptionUsed == 1) {

                        if (childName == "") {
                            retVal = "-1";
                            statusMessage("Please enter Child Name", "middle-center", "error");
                            return retVal;
                        } else if (grade == "0") {
                            retVal = "-1";
                            statusMessage("Please select Grade", "middle-center", "error");
                            return retVal;
                        } else if (schoolName == "") {
                            retVal = "-1";
                            statusMessage("Please enter school Name", "middle-center", "error");
                            return retVal;
                        } else if (arrContestMult.length <= 0 && isContestShow == 1) {
                            retVal = "-1";
                            statusMessage("Enter the information of a child only if participating in a contest.<br>If a child is not participating in a contest, remove his/her detail. Update only the number of attendees.", "middle-center", "error");
                            return retVal;
                        }
                    }
                }
            }
            return retVal;
        }


        function validateChild() {
            var retVal = "1";
            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            if (childName == "") {
                retVal = "-1";
                statusMessage("Please enter Child Name", "middle-center", "error");
            } else if (grade == "0") {
                retVal = "-1";
                statusMessage("Please select Grade", "middle-center", "error");
            } else if (schoolName == "") {
                retVal = "-1";
                statusMessage("Please enter school Name", "middle-center", "error");
            } else if (arrContest.length <= 0 && isContestShow == 1) {
                retVal = "-1";
                statusMessage("Select contest(s). No need to register a child, if the child is not going to participate in contests", "middle-center", "error");
            }

            if (arrChildId.length > 0) {
                for (var i = 0; i < arrChildId.length; i++) {

                    schoolName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    childName = $("#txtChildName" + arrChildId[i] + "").val();
                    grade = $("#selGrades" + arrChildId[i] + "").val();

                    var arrContestMult = [];

                    var sb;
                    var vb;
                    var mb;
                    var sc;
                    var gb;
                    var ew;
                    var ps;
                    var bb;

                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk8" + arrChildId[i] + "").val());
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk9" + arrChildId[i] + "").val());
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk10" + arrChildId[i] + "").val());
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk11" + arrChildId[i] + "").val());
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk12" + arrChildId[i] + "").val());
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk13" + arrChildId[i] + "").val());
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk14" + arrChildId[i] + "").val());
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk26" + arrChildId[i] + "").val());
                    }




                    if (childName == "") {
                        retVal = "-1";
                        statusMessage("Please enter Child Name", "middle-center", "error");
                        return retVal;
                    } else if (grade == "0") {
                        retVal = "-1";
                        statusMessage("Please select Grade", "middle-center", "error");
                        return retVal;
                    } else if (schoolName == "") {
                        retVal = "-1";
                        statusMessage("Please enter school Name", "middle-center", "error");
                        return retVal;
                    } else if (arrContestMult.length <= 0 && isContestShow == 1) {
                        retVal = "-1";
                        statusMessage("Select contest(s). No need to register a child, if the child is not going to participate in contests", "middle-center", "error");
                        return retVal;
                    }
                }
            }
            return retVal;
        }

        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        function statusMessage(message, position, type) {

            $.toast({

                text: '<b>' + message + '</b>',
                icon: type,
                position: 'mid-center',
                hideAfter: 5000,
                stack: 1

            })
        }

        $(document).on("click", "#btnSave", function (e) {
            var partType = $("#hdnPartType").val();
            if ($("#hdnisUpdate").val() == "0") {
                if (partType == "Adult") {

                    if (validateEvent() == "1" && validateAdult() == 1) {

                        var eventId = $('#selEventCode').val();
                        duplictateCheck(eventId);
                    }

                } else {
                    if (validateEvent() == "1" && validateOnAddChild() == 1) {

                        var eventId = $('#selEventCode').val();
                        duplictateCheck(eventId);
                    }
                }

            } else {

                if (partType == "Adult" || partType == "Parent") {
                    if (validateEvent() == "1" && validateAdult() == 1) {
                        $("#hdnIsChild").val("1");

                        validateAdultDuplicates(2);

                    }
                } else {
                    if (validateEvent() == "1" && validateOnAddChild() == 1) {
                        $("#hdnIsChild").val("1");

                        postNewRegistration(2);

                    }
                }
            }

            //duplictateCheck();

        });

        $(document).on("keyup", "#txtVenueAddr", function (e) {

            if ($(this).length == 128) {
                statusMessage("Venue Address could not be more than 128 character.", "middle-center", "error");
            }

        });



        function postNewRegistration(mode) {

            $("#hdnMode").val(mode);
            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            var firstName = $("#txtFirstName").val();
            var middleName = $("#txtMiddleName").val();
            var lastName = $("#txtLastName").val();
            var partType = participantType;
            var emailAddress = $("#txtEmailAddress").val();
            var attendees = $("#txtAttendeesCount").val();
            var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;
            var memberId = $("#hdnMemberID").val();
            var frMemberID = $("#hdnMemberID").val();

            var productGroupId = $("#selProductGroup").val();
            var productGroupCode = $("#selProductGroup option:selected").text();
            var productId = $("#selProduct").val();
            var productCode = $("#selProduct option:selected").text();

            if (getParameterByName("Memberid") != "") {

            }

            var parentName = $("#txtParentName").val() == undefined ? "" : $("#txtParentName").val() + ",";
            var parentEmail = $("#txtParentEmail").val() == undefined ? "" : $("#txtParentEmail").val() + ",";
            var parentLastName = $("#txtAttendeeLastName").val() == undefined ? "" : $("#txtAttendeeLastName").val() + ",";
            // $("#txtParentName").val() == "" ?


            for (var i = 0; i < arrAdult.length; i++) {
                parentName += $("#txtParentName" + arrAdult[i] + "").val() + ",";
                parentEmail += $("#txtParentEmail" + arrAdult[i] + "").val() + ",";
                parentLastName += $("#txtAttendeeLastName" + arrAdult[i] + "").val() + ",";
            }

            var status = $('#hdnRegStatus').val() == "" ? "Pending" : $('#hdnRegStatus').val();

            var jsonData = JSON.stringify({ FrEventReg: { EventCode: eventCode, FreeEventID: eventId, FirstName: firstName, Year: year, MiddleName: middleName, LastName: lastName, Email: emailAddress, PartType: partType, Attendees: attendees, Createdby: userId, Status: status, Mode: mode, MemberID: memberId, AttendeeName: parentName, AttendeeEmail: parentEmail, AttendeeLastName: parentLastName, AttendeeType: "Adult", FrMemberID: frMemberID } });



            var eventType = $("#hdnEventType").val();
            if (eventType == "Paid") {
                //var attendeeName = $("#txtParentName").val();
                //var attendeeEmail = $("#txtParentEmail").val();

                memberId = getParameterByName("Memberid");

                var existingAttendeeCount = $("#hdnExistingAttendeeCount").val();
                var currentAttendeeCount = $("#txtAttendeesCount").val();


                var fee = $("#txtFee").val();
                var existingChildIs = "";
                for (var i = 0; i < existChildId.length; i++) {
                    existingChildIs += existChildId[i] + ",";
                }
                jsonData = JSON.stringify({ FrEventReg: { EventCode: eventCode, FreeEventID: eventId, FirstName: firstName, Year: year, MiddleName: middleName, LastName: lastName, Email: emailAddress, PartType: partType, Attendees: attendees, Createdby: userId, Status: status, Mode: mode, MemberID: memberId, AttendeeType: "Adult", AttendeeName: parentName, AttendeeEmail: parentEmail, Fee: fee, EventType: eventType, productGroupID: productGroupId, productGroupCode: productGroupCode, productID: productId, productCode: productCode, ExistingAttendeeCount: existingAttendeeCount, CurrentAttendeeCount: currentAttendeeCount, ExistingChildIds: existingChildIs } });
            }
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/PostNewRegistartion",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (eventType == "Free" || eventType == "") {


                        if (JSON.parse(JSON.stringify(data.d)) != "-1") {
                            $("#hdnIsChild").val("0");
                            if ($("#hdnIsChild").val() == "1") {

                                validateChildDate(data.d, mode);

                                $("#dvNewRegistration").show();
                                $("#dvRegHeader").show();
                            } else {

                                if (mode == 1) {

                                    var index = JSON.stringify(data.d).indexOf("-");
                                    var retVal = JSON.stringify(data.d).split("-")[1];
                                    var frMemberid = JSON.stringify(data.d).substring(1, index);
                                    if (parseInt(retVal) == 1) {


                                        encryptURL(JSON.stringify(data.d), "Confirmed", "", frMemberid);
                                    } else {
                                        window.location.href = "FreeEventRegistrationThankU.aspx?Id=" + eventId + "&MId=" + frMemberid + "";
                                    }

                                } else {

                                    validateChildDate(data.d, mode);

                                    $("#dvNewRegistration").hide();
                                    $("#dvRegHeader").hide();
                                    $("#dvUpdateRegistration").hide();

                                }


                                hideLoader();
                            }


                        } else {

                            if (JSON.parse(JSON.stringify(data.d)) == "-1") {
                                statusMessage("Operation failed.", "mid-center", "error");
                            }

                            if (JSON.stringify(data.d) == "-4") {
                                statusMessage("Registration Date already has passed", "mid-center", "error");
                            }
                            hideLoader();
                        }
                    } else {
                        if (mode == "2") {
                            //var status = $("#hdnRegStatus").val();
                            //if (status == "Confirmed") {
                            //    statusMessage("Updated Successfully", "mid-center", "success");
                            //    hideLoader();
                            //} else {
                            window.location.href = "../FreeEventSelectionSum.aspx";
                            //}
                        } else {
                            window.location.href = "../FreeEventSelectionSum.aspx";
                        }
                    }
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }

            });
        }

        function validateChildDate(memberID, mode) {




            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();

            var success = "-1";
            var SB = null;
            var VB = null;
            var MB = null;
            var GB = null;
            var EW = null;
            var PS = null;
            var BB = null;
            var SC = null;

            //old code

            if ($.inArray("8", arrContest) >= 0) {

                SB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("8", grade) : null);
            }
            if ($.inArray("9", arrContest) >= 0) {

                VB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("9", grade) : null);
            }
            if ($.inArray("10", arrContest) >= 0) {
                MB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("10", grade) : null);
            }
            if ($.inArray("11", arrContest) >= 0) {
                GB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("11", grade) : null);
            }
            if ($.inArray("12", arrContest) >= 0) {
                EW = (isContestShow == 1 ? getContestsCodesbasedOnGrade("12", grade) : null);
            }
            if ($.inArray("13", arrContest) >= 0) {
                PS = (isContestShow == 1 ? getContestsCodesbasedOnGrade("13", grade) : null);
            }
            if ($.inArray("14", arrContest) >= 0) {
                BB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("14", grade) : null);
            }
            if ($.inArray("26", arrContest) >= 0) {
                SC = (isContestShow == 1 ? getContestsCodesbasedOnGrade("26", grade) : null);
            }
            var index = JSON.stringify(memberID).indexOf("-");
            var retVal = JSON.stringify(memberID).split("-")[1];
            var memberid = JSON.stringify(memberID).substring(1, index);

            var jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: childName, Grade: grade, SchoolName: schoolName, Year: year, MemberID: memberid, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

            showLoader();
            if (childName != "" && schoolName != "" && grade != "0") {

                $.ajax({
                    type: "POST",
                    url: "FreeEventReg.aspx/PostNewChild",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        $("#hdnFreeEventID").val("0")
                        var success = 0;
                        // if (JSON.stringify(data.d) > 0) {
                        hideLoader();
                        mode = 1;
                        FreeEventChildReg(childName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode);
                        // }
                    }
                });
            } else if (arrChildId.length > 0) {

                FreeEventChildReg(childName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode);
            }
            else {
                var index = JSON.stringify(memberID).indexOf("-");
                var retVal = JSON.stringify(memberID).split("-")[1];
                var memberid = JSON.stringify(memberID).substring(1, index);
                var freeEventID = $("#selEventName").val();


                retVal = JSON.stringify(memberID).split("-")[1].substring(0, 1);



                if (parseInt(retVal) == 5) {
                    window.location.href = "FreeEventRegistrationThankU.aspx?Id=" + freeEventID + "&MId=" + memberid + "";
                } else {
                    encryptURL(memberID, "Confirmed", "", 0);
                }


            }


            hideLoader();


        }

        function FreeEventChildReg(chilName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode) {

            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            //var tId = "";

            //var jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: chilName, Grade: grade, SchoolName: schoolName, Year: year, MemberID: memberID, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

            //showLoader();
            //$.ajax({
            //    type: "POST",
            //    url: "FreeEventReg.aspx/PostNewChild",
            //    data: jsonData,
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (data) {
            //        tId = JSON.stringify(data.d);
            //        $("#hdnFreeEventID").val("0")
            //        var success = 0;
            //        if (JSON.stringify(data.d) != "-1") {


            if (arrChildId.length > 0) {

                for (var i = 0; i < arrChildId.length; i++) {
                    if (i > 0) {
                        mode = 1;
                    }

                    var schlName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    var childName = $("#txtChildName" + arrChildId[i] + "").val();
                    var gradeInfo = $("#selGrades" + arrChildId[i] + "").val();



                    SB = null;
                    VB = null;
                    MB = null;
                    GB = null;
                    EW = null;
                    PS = null;
                    BB = null;
                    SC = null;

                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        SB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("8", gradeInfo) : null);
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        VB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("9", gradeInfo) : null);
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        MB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("10", gradeInfo) : null);
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        GB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("11", gradeInfo) : null);
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        EW = (isContestShow == 1 ? getContestsCodesbasedOnGrade("12", gradeInfo) : null);
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        PS = (isContestShow == 1 ? getContestsCodesbasedOnGrade("13", gradeInfo) : null);
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        BB = (isContestShow == 1 ? getContestsCodesbasedOnGrade("14", gradeInfo) : null);
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        SC = (isContestShow == 1 ? getContestsCodesbasedOnGrade("26", gradeInfo) : null);
                    }
                    var index = JSON.stringify(memberID).indexOf("-");
                    var retVal = JSON.stringify(memberID).split("-")[1];
                    var memberid = JSON.stringify(memberID).substring(1, index);
                    jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: childName, Grade: gradeInfo, SchoolName: schlName, Year: year, MemberID: memberid, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

                    if (schlName != "" && childName != "" && gradeInfo != "0") {
                        $.ajax({
                            type: "POST",
                            url: "FreeEventReg.aspx/PostNewChild",
                            data: jsonData,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                success = "1";
                                // tId = JSON.stringify(data.d);
                            }
                        });
                    }
                }

                // send email
                //reset();

                hideLoader();

            }
            if ($("#hdnMode").val() == "1") {
                var index = JSON.stringify(memberID).indexOf("-");
                var retVal = JSON.stringify(memberID).split("-")[1];
                var memberid = JSON.stringify(memberID).substring(1, index);
                var freeEventID = $("#selEventName").val();
                retVal = JSON.stringify(memberID).split("-")[1].substr(0, 1);

                if (parseInt(retVal) == 5) {
                    window.location.href = "FreeEventRegistrationThankU.aspx?Id=" + freeEventID + "&MId=" + memberid + "";
                } else {
                    encryptURL(memberID, "Confirmed", "", 0);
                }


                $("#dvNewRegistration").show();
                $("#dvRegHeader").show();
            } else if ($("#hdnMode").val() == "2") {

                statusMessage("Updated successfully", "middle-center", "success");

                $("#dvNewRegistration").hide();
                $("#dvRegHeader").hide();
                $("#dvUpdateRegistration").hide();
                reset();
            }


            //        } else {

            //            hideLoader();
            //        }

            //    },
            //    failure: function (response) {
            //        alert(response.d);
            //        hideLoader();
            //    }

            //});

        }


        function listFreeEvents(freeEventID, year) {

            $('#selEventCode').empty();
            $('#selEventName').empty();
            var sourcer = "Reg"
            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID, Source: sourcer });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    hideLoader();
                    var eventId = 0;
                    var count = 0;
                    var participantType = "";
                    // $("#selEventCode").append($("<option></option>").val
                    //(0).html("Select"));

                    //$("#selEventName").append($("<option></option>").val
                    //    (0).html("Select"));
                    $.each(data.d, function (index, value) {
                        $("#selEventCode").append($("<option></option>").val
                            (value.FreeEventID).html(value.EventCode));
                        count++;
                        $("#selEventName").append($("<option></option>").val
                            (value.FreeEventID).html(value.EventName));
                        eventId = value.FreeEventID;
                        ParticipantType = value.ParticipantType;
                        $("#hdnPartType").val(ParticipantType);
                        $("#hdnFreeEventID").val(eventId);
                        if (value.SB != "" || value.VB != "" || value.MB != "" || value.SC != "" || value.GB != "" || value.EW != "" || value.PS != "" || value.BB != "") {
                            isContestShow = 1;
                        }
                        fillAttendeeCount(value.EventType);
                    });
                    if (count > 1) {
                        $('#dvEventCode').show();
                    } else {
                        $('#dvEventCode').hide();
                        IsFreeEvent();

                    }
                    if (data.d.length == 1) {
                        $("#selEventCode").attr("disabled", "disabled");
                        $("#selEventName").attr("disabled", "disabled");
                        $("#selEventCode").val(eventId);
                        $("#selEventName").val(eventId);
                       <%-- $("#txtEmailAddress").val(document.getElementById("<%=hdnEmail.ClientID%>").value);--%>
                        //getUpdateDetails(eventId);
                        getEventFees();
                        getAvailableSeats();

                        if (isContestShow == 1) {
                            $(".spnChildTitle").text("Child Attending Contest(s)");
                            // $("#dvContestSesction").show();
                        } else {
                            // $("#dvContestSesction").hide();
                            $(".spnChildTitle").text("Child Attending");
                        }
                    } else if (data.d.length > 1) {
                        $("#selEventCode").removeAttr("disabled");
                        $("#selEventName").removeAttr("disabled");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }
        function duplictateCheck(eventId) {




            var email = $('#txtEmailAddress').val();
            var year = $('#selyear').val();

            if (eventId == 0) {
                email = $('#txtUpdateEmailAddress').val();
                year = $('#selUpdateYear').val();
            }

            var jsonData = JSON.stringify({ FrEventReg: { Email: email, FreeEventID: eventId, Year: year } });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/DuplicateExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {

                        $("#hdnEmailExists").val(JSON.stringify(data.d));

                        if (eventId > 0) {




                            statusMessage("The email address already exists. Please select the “Update my registration” option from the drop down menu.", "middle-center", "info");


                        } else {
                            if ($("#hdnEmailExists").val() != "0") {
                                if ($("#selActionType").val() == "2") {
                                    getmemberId(email);
                                } else if ($("#selActionType").val() == "3") {
                                    reSendEmailLink(email);
                                }
                            } else {

                                statusMessage("Email address does not match.", "middle-center", "error");
                                hideLoader();
                            }
                        }
                        hideLoader();
                    } else {
                        $("#hdnEmailExists").val(JSON.stringify(data.d))
                        if (eventId > 0) {
                            $("#hdnIsChild").val("1");
                            validateAdultDuplicates(1);

                            hideLoader();
                        } else {
                            statusMessage("Email address does not match.", "middle-center", "error");
                            hideLoader();
                        }
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }


        function getUpdateDetails(eventId) {




            var email = $('#txtEmailAddress').val();
            var year = $('#selyear').val();

            if (eventId == 0) {
                email = $('#txtUpdateEmailAddress').val();
                year = $('#selUpdateYear').val();
            }

            var jsonData = JSON.stringify({ FrEventReg: { Email: email, FreeEventID: eventId, Year: year } });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/DuplicateExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {

                        $("#hdnEmailExists").val(JSON.stringify(data.d));

                        if (eventId > 0) {


                            getmemberId(email);




                        } else {


                            hideLoader();

                        }
                        hideLoader();
                    } else {

                        hideLoader();

                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }



        function reset() {
            //$("#selEventCode").val("0");
            $("#txtFirstName").val("");
            $("#txtMiddleName").val("");
            $("#txtLastName").val("");
            $("#txtEmailAddress").val("");
            $("#txtAttendeesCount").val("");

            arrContest = [];
            arrChildId = [];
            $(".rbtnContests").removeAttr("checked");
            $("#txtChildName").val("");
            $("#selGrade1").val("0");
            $("#txtSchoolName").val("");

            $(".dvChildSection").hide();
            $("#dvContest").hide();
            $(".partType").prop("checked", false);

            $("#iTopRow").removeClass("btnAddChild");
            $("#iTopRow").addClass("btnAddChildDisabled");
            $("#iTopRow").css("cursor", "normal");
            $("#iTopRow").css("color", "Grey");

            if ($("#dvContestSesction").html() == "") {
                var dvHtml = "";

                dvHtml += '<div style="float: left; margin-left: 30px;">';
                dvHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; left:75px;" class="spnChildTitle">Child Attending Contest(s)</span></div> <div style= "clear: both; margin-bottom: 5px;" ></div >';
                dvHtml += '<div class="input-group" >';

                dvHtml += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';

                dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 386px;" name="InputName" id="txtChildName" placeholder="Child Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';
                dvHtml += '<div style="float: left; margin-left: 30px;  position:relative; top:25px;">';

                dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>';
                dvHtml += ' <select id="selGrade1" class="form-control" style="width:130px;">';
                dvHtml += ' <option value="0">Grade</option>';
                dvHtml += '<option value="1">1</option>';
                dvHtml += '<option value="2">2</option>';
                dvHtml += '<option value="3">3</option>';
                dvHtml += '<option value="4">4</option>';
                dvHtml += '<option value="5">5</option>';
                dvHtml += '<option value="6">6</option>';
                dvHtml += '<option value="7">7</option>';
                dvHtml += '<option value="8">8</option>';
                dvHtml += '<option value="9">9</option>';
                dvHtml += '<option value="10">10</option>';
                dvHtml += ' <option value="11">11</option>';
                dvHtml += '<option value="12">12</option>';
                dvHtml += '</select>';
                dvHtml += '</div>';
                dvHtml += '</div>';
                dvHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
                dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>';
                dvHtml += '<input type="text" autocomplete="off" class="form-control" name="InputName" style="width: 386px;" id="txtSchoolName" placeholder="School Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';

                dvHtml += '<div style="float: left; margin-left:3px;">';

                dvHtml += '<a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey;" id="iTopRow"></i></a>';
                dvHtml += '</div>';

                dvHtml += '<div class="clear" style="margin-bottom: 5px;"></div>';
                dvHtml += '<div>';

                dvHtml += '<div id="dvContest" style="position: relative; left: 32px; top:10px;">';
                dvHtml += '</div>';
                dvHtml += '</div>';
                $("#dvContestSesction").html(dvHtml)
            }
            $("#iTopRow").hide();
            //  listChapter();
        }



        $(document).on("click", "#btnReset", function (e) {

            reset();
            adultReset();
        });


        function fillGrade(id) {
            $('#id').empty();
            var i = 1;
            //         $("#" + id).append($("<option></option>").val
            //("0").html("Select"));
            for (i = 1; i < 13; i++) {
                $("#" + id).append($("<option></option>").val
                    (i).html(i));
            }
        }

        var childCount = 0;
        var index = 0;
        var arrChildId = [];

        function addChild(index) {

            //index++;
            $("#hdnMaxIndex").val(index);
            $("#hdnIndex").val(index);
            arrChildId.push(index.toString());

            $(".dvChildSection").show();
            var dHtml = "";
            // dHtml += $("#dvAttendees").html();
            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '<div id="dvNewChild' + index + '" class="dvChildSection">';

            dHtml += '<div  style="float: left; margin-left:30px; ">';
            dHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; left:75px;" class="spnChildTitle">Child Attending Contest(s)</span></div> <div style= "clear: both; margin-bottom: 5px;" ></div>';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i></span>';

            dHtml += '<input type="text" autocomplete="off" class="form-control childName" style="width:386px;" attr-index=' + index + ' name="InputName" id="txtChildName' + index + '" placeholder="Child Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';

            dHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px; ">';

            dHtml += '<div class="input-group">  ';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span>';
            dHtml += '<select id="selGrades' + index + '" attr-index=' + index + ' class="form-control selGrade" style="width:130px;">';
            dHtml += '<option value="0">Grade</option>';
            var j = 1;
            //         $("#" + id).append($("<option></option>").val
            //("0").html("Select"));
            for (j = 1; j < 13; j++) {
                dHtml += '<option value=' + j + '>' + j + '</option>';
            }

            dHtml += '</select>';

            dHtml += '</div>';
            dHtml += '</div>';
            dHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span >';
            dHtml += '<input type="text" autocomplete="off" class="form-control schoolName" style="width:386px;" attr-index=' + index + ' name="InputName" id="txtSchoolName' + index + '" placeholder="School Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';
            //dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="btnAddChild" attr-childId=' + index + ' title="Add a Child"><i class="fa fa-user-plus fa-2x"></i></a></div>';

            dHtml += '<div  style="float: left; margin-left: 3px; position:relative; top:30px;" id="dvAddDelete' + index + '"></div>';

            //   dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="ancDelete" attr-childId=' + index + ' title="Remove Attendee"><i class="fa fa-user-times fa-2x"></i></a></div>';

            dHtml += '<div style="clear: both; margin-bottom: 5px;"></div>';




            dHtml += "<div id='dvProductCodes" + index + "'>";
            dHtml += '</div>';
            dHtml += '</div>';


            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '</div>';



            var id = (index - 1);

            if ($("#dvNewChild" + id + "").html() != "" && $("#dvNewChild" + id + "").html() != undefined) {

                $("#dvNewChild" + id + "").before(dHtml);
            } else {
                $("#dvContestSesction").before(dHtml);
            }

            // dvChildHtml.push(dHtml);

            // fillGrade("selGrades" + childCount + "");



            childCount++;
        }

        function enableDisableAddDelIcon(cIndex, option) {
            var iconHtml = "";

            if (option == "Update") {
                iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Remove Attendee"><i class="fa fa-user-times fa-2x ancDelete "  attr-childId=' + cIndex + '></i></a>';
            } else {

                iconHtml += '<a style="cursor:pointer; display:none;" attr-childId=' + cIndex + ' title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChild "  attr-childId=' + cIndex + '></i></a>';

            }

            $("#dvAddDelete" + cIndex + "").html(iconHtml);
        }


        $(document).on("keydown", ".childName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#selGrades" + attrIndex + "").val() != "0" && $("#txtSchoolName" + attrIndex + "").val() != "") {
                    if ($("#dvAddDelete" + attrIndex + "").html() == "") {
                        enableDisableAddDelIcon(attrIndex, "Add");
                    }
                }
            }
        });


        $(document).on("keydown", ".schoolName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#selGrades" + attrIndex + "").val() != "0" && $("#txtChildName" + attrIndex + "").val() != "") {
                    if ($("#dvAddDelete" + attrIndex + "").html() == "") {
                        enableDisableAddDelIcon(attrIndex, "Add");
                        $("#iTopRow").show();
                    }
                }
            }
        });


        function getIndexNo(index) {
            return index;
        }


        $(document).on("change", ".selGrade", function (e) {



            if ($(this).val() != "0") {

                var cIndex = $(this).attr("attr-index");

                var grade = $(this).val();
                addContest(cIndex, grade, "");

                if ($("#txtChildName" + cIndex + "").val() != "0" && $("#txtSchoolName" + cIndex + "").val() != "" && grade != "0") {
                    if ($("#dvAddDelete" + cIndex + "").html() == "") {
                        enableDisableAddDelIcon(cIndex, "Add");
                    }
                }
            }
        });

        function addContest(cIndex, grade, childJSON) {



            $("#dvProductCodes" + cIndex + "").html("");


            var grades = "";

            var dHtml = "";

            var year = $("#selyear").val();

            var freeEventID = $("#selEventCode").val();

            if (freeEventID == "0") {
                $(this).val("0");
                statusMessage("Please select Event Code", "middle-center", "error");

            } else {


                var jsonData = JSON.stringify({ Year: year, EventID: freeEventID, GradeFrom: grade, GradeTo: grade });

                $.ajax({
                    type: "POST",
                    url: "FreeEventReg.aspx/ListContests",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var dvHtml = "";
                        var i = 0;

                        if (JSON.stringify(data.d.length) > 0) {
                            var dHtml = "";

                            $.each(data.d, function (index, value) {

                                if (getContestsCodesbasedOnGrade(value.ProductGroupID, grade) != "") {
                                    i++;


                                    dHtml += "<div>";
                                    dHtml += "<div style='width:235px; float:left;'>";
                                    dHtml += ' <div style="float: left; margin-left: 30px;">';
                                    dHtml += '<input type="checkbox" class="rbtnContests' + cIndex + '" value=' + value.ProductGroupID + ' id="chk' + value.ProductGroupID + '' + cIndex + '" />';
                                    dHtml += ' </div>';
                                    dHtml += '<div style="float: left; margin-left: 10px;">';
                                    dHtml += ' <label for="InputName" class="control-label">' + getContestsbasedOnGrade(value.ProductGroupID, grade) + '</label>';
                                    dHtml += ' </div>';
                                    dHtml += ' </div>';
                                    dHtml += ' </div>';

                                    //if (i == 4) {
                                    //    dHtml += '<div style="clear:both;"></div>';
                                    //}
                                }


                            });


                            $("#dvProductCodes" + cIndex + "").html(dHtml);

                            if (childJSON != "") {


                                $.each(childJSON, function (index1, value1) {
                                    if (index1 == cIndex) {

                                        if (value1.SB != "" && value1.SB != null) {

                                            $("#chk8" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.VB != "" && value1.VB != null) {
                                            $("#chk9" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.MB != "" && value1.MB != null) {
                                            $("#chk10" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.SC != "" && value1.SC != null) {
                                            $("#chk26" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.GB != "" && value1.GB != null) {
                                            $("#chk11" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.EW != "" && value1.EW != null) {
                                            $("#chk12" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.PS != "" && value1.PS != null) {
                                            $("#chk13" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.BB != "" && value1.BB != null) {
                                            $("#chk14" + cIndex + "").prop("checked", true);
                                        }
                                    }
                                });


                            }
                            hideLoader();
                        } else {

                            hideLoader();
                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                        hideLoader();
                    }
                });
            }
        }

        //$(document).on("click", "#btnAddChild", function (e) {

        //    addChild();

        //});



        $(document).on("click", ".ancAdultDelete", function (e) {


            var childId = $(this).attr("attr-childId");

            $("#dvNewAdult" + childId + "").empty();

            if ($(this).attr("id") == "iAdultRow") {
                // $("#dvAdultSection").empty();
                if (arrAdult.length == 0) {
                    $(this).hide();
                    $("#txtParentName").val("");
                    $("#txtAttendeeLastName").val("");
                    $("#txtParentEmail").val("");
                    isRegUserAdd = 1;
                } else {
                    $("#dvAdultSection").empty();
                }

            }
            var i = arrAdult.length == 0 ? 1 : arrAdult.length;
            $("#txtAttendeesCount").val((i));
            getEventFees();
            if ($(this).attr("id") != "iAdultRow") {

                arrAdult.splice($.inArray(childId, arrAdult), 1);
            }

            if (arrAdult.length == 0) {

                if ($("#dvAdultSection").html() == "") {

                    resetAttendee();
                    $("#dvAdultSection").show();
                }
            }

        });

        $(document).on("click", ".ancDelete", function (e) {


            var childId = $(this).attr("attr-childId");

            $("#dvNewChild" + childId + "").empty();

            if ($(this).attr("id") == "iTopRow") {
                $("#dvContestSesction").empty();
                arrContest = [];
                $("#txtSchoolName").val("");
                $("#txtChildName").val("");
                $("#selGrade1").val("0");
            }

            if ($(this).attr("id") != "iTopRow") {
                arrChildId.splice($.inArray(childId, arrChildId), 1);
            }

            // fillGrade();

        });


        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice($.inArray($(this).val(), arrContest), 1);
                    //arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        $(document).on("change", "#selActionType", function (e) {
            if ($(this).val() == "1") {
                var availableSeats = $("#hdnAvailableSeats").val();
                $("#dvNewRegistration").show();
                $("#dvRegHeader").show();
                $("#dvUpdateRegistration").hide();
                //if (parseInt(availableSeats) > 0) {
                //    $("#dvNewRegistration").show();
                //    $("#dvUpdateRegistration").hide();
                //    $("#txtFirstName").removeAttr("disabled");
                //    $("#txtLastName").removeAttr("disabled");
                //    $("#txtEmailAddress").removeAttr("disabled");
                //    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                //} else {

                //    statusMessage("Registration closed","mid-center", "error");
                //}
                $('html, body').animate({
                    scrollTop: $("#dvNewRegistration").offset().top
                }, 2000);

            } else if ($(this).val() == "2") {
                var year = $("#selUpdateYear").val();
                listFreeEvents(0, year);
                $("#dvNewRegistration").hide();
                $("#dvRegHeader").hide();
                $("#dvUpdateRegistration").show();
            } else if ($(this).val() == "3") {
                var year = $("#selUpdateYear").val();
                listFreeEvents(0, year);
                $("#dvNewRegistration").hide();
                $("#dvRegHeader").hide();
                $("#dvUpdateRegistration").show();
            } else if ($(this).val() == "4") {

                opennewtab("https://www.northsouth.org/public/FreeEvent/NSF-Parenting-the-i-generation-2018-v2.4.pdf");
            }

        });

        function opennewtab(url) {
            var win = window.open(url, '_blank');
        }

        var participantType = "";
        $(document).on("click", ".partType", function (e) {

            if ($(this).prop("checked") == true) {

                participantType = $(this).val();
            }



        });


        function getContestsbasedOnGrade(contestID, grade) {

            var contestCode = "";
            if (contestID == "8") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Spelling";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Senior Spelling";
                }
            } else if (contestID == "9") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Vocabulary";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Vocabulary";
                }

            } else if (contestID == "10") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 2) {
                    contestCode = "Math Level 1";
                } else if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "Math Level 2";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Math Level 3";
                }
            } else if (contestID == "11") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Geography";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Senior Geography";
                }
            } else if (contestID == "12") {
                if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "Junior Essay Writing";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Essay Writing";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "Senior Essay Writing";
                }

            } else if (contestID == "13") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Junior Public Speaking";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "Senior Public Speaking";
                }
            } else if (contestID == "14") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Brain Bee";
                } else if (parseInt(grade) > 7 && parseInt(grade) <= 11) {
                    contestCode = "Brain Bee";
                }
            } else if (contestID == "26") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Science";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 5) {
                    contestCode = "Intermediate Science";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Senior Science";
                }

            }

            return contestCode;
        }


        function getContestsCodesbasedOnGrade(contestID, grade) {

            var contestCode = "";
            if (contestID == "8") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JSB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "SSB";
                }
            } else if (contestID == "9") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JVB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "IVB";
                }

            } else if (contestID == "10") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 2) {
                    contestCode = "MB1";
                } else if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "MB2";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "MB3";
                }
            } else if (contestID == "11") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JGB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "SGB";
                }
            } else if (contestID == "12") {
                if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "EW1";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "EW2";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "EW3";
                }

            } else if (contestID == "13") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "PS1";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "PS3";
                }
            } else if (contestID == "14") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "IBB";
                } else if (parseInt(grade) > 7 && parseInt(grade) <= 11) {
                    contestCode = "BB";
                }
            } else if (contestID == "26") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JSC";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 5) {
                    contestCode = "ISC";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "SSC";
                }

            }

            return contestCode;
        }

        function encryptURL(UID, status, TimeID, frMemberId) {

            UID = JSON.parse(UID);

            var URl = "&UID=" + UID + "&Status=" + status + "";
            var jsonData = JSON.stringify({ clearText: URl });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Encrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var index = window.location.href.lastIndexOf('/');
                    var originalURL = window.location.href.substr(0, index);

                    originalURL = originalURL + "/" + "Confirmation.aspx";

                    var encryptedURl = originalURL + "?ID=" + JSON.stringify(data.d);
                    var email = $("#txtEmailAddress").val();

                    isEmailExists(encryptedURl, URl, 1, email, frMemberId);
                    // sendEmail(encryptedURl, URl, 1, email, frMemberId);

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }
        function isEmailExists(encryptedURl, URl, option, email, frMemberId) {


            var jsonData = JSON.stringify({ Email: email });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/IsEmailExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var retVal = JSON.stringify(data.d);
                    if (retVal == 0) {

                        sendEmail(encryptedURl, URl, 1, email, frMemberId);
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }
        function sendEmail(url, dUrl, mode, email, frMemberId) {

            // var email = $("#txtEmailAddress").val();
            var jsonData = JSON.stringify({ Email: email, URl: url, Mode: mode });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/SendEmailsToRegistarants",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (mode == 1) {
                        // statusMessage('Thank you for registering. An email is being sent to ' + $("#txtEmailAddress").val() + '. Please click the link to complete the registration.', "middle-center", "success");

                        //reset();
                        hideLoader();
                        var freeEventID = $("#selEventCode").val();
                        window.location.href = "FreeEventRegistrationThankU.aspx?Id=" + freeEventID + "&MId=" + frMemberId + "";

                        //Simson
                    } else if (mode == 2) {
                        statusMessage('An email is being sent to ' + $("#txtUpdateEmailAddress").val() + '. Please follow the instructions in the email to update your registration.', 'middle-center', 'success');

                        hideLoader();
                        $("#dvUpdateRegistration").hide();
                        $("#selActionType").val("0");
                    } else if (mode == 3) {

                        statusMessage('An email is being sent to ' + $("#txtUpdateEmailAddress").val() + '. Please click the link to complete the registration.', 'middle-center', 'success');


                        hideLoader();
                        $("#dvUpdateRegistration").hide();
                        $("#selActionType").val("0");

                    }
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function dcryptURL(id) {

            var URl = JSON.parse(id);

            var jsonData = JSON.stringify({ cipherText: URl });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Decrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var decryptedText = JSON.stringify(data.d);


                    var index = window.location.href.indexOf('?');

                    var url = window.location.href.substr(0, index);
                    url = url + "" + decryptedText;

                    var memberId = getDecryptedValue(url, "memberId");
                    var expired = getDecryptedValue(url, "expired");

                    expired = JSON.parse("\"" + expired);
                    var update = getDecryptedValue(url, "U");
                    var year = $("#selyear").val();
                    listFreeEvents(0, year);
                    $("#hdnMemberID").val(memberId);
                    if (memberId != "" && memberId != undefined && update != "" && update != undefined) {
                        getFreeEventregistrations(memberId, expired, 0);
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function getFreeEventregistrations(memberId, expired, isValidate) {

            $("#selActionType").val("2");
            $("#selActionType").attr("disabled", "disabled");
            $("#dvNewRegistration").show();
            $("#dvRegHeader").show();
            $("#dvUpdateRegistration").hide();

            var jsonData = JSON.stringify({ FrMemberID: memberId, Expired: expired, IsValidate: isValidate });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetFreeEventregistrations",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var i = 0;
                    var j = 0;
                    var k = 0;
                    if (JSON.stringify(data.d.length) > 0) {
                        var childJSON = "";
                        var partType = $("#hdnPartType").val();
                        $.each(data.d, function (index, value) {
                            childJSON = value.Child;

                            j++;
                            k = j;
                            $("#selyear").val(value.Year);
                            $("#selEventCode").val(value.FreeEventID);

                            $("#txtFirstName").val(value.FirstName);
                            $("#txtMiddleName").val(value.MiddleName);
                            $("#txtLastName").val(value.LastName);
                            $("#txtEmailAddress").val(value.Email);
                            $("#txtAttendeesCount").val(value.Attendees);
                            $("#hdnExistingAttendeeCount").val(value.Attendees);
                            $("#txtFee").val(value.Fee);

                            $("#hdnRegStatus").val(value.Status);
                            $("#rbtn" + value.PartType + "").prop("checked", true);

                            $("#txtFirstName").attr("disabled", "disabled");
                            $("#txtLastName").attr("disabled", "disabled");
                            $("#txtEmailAddress").attr("disabled", "disabled");
                            participantType = value.PartType;
                            $("#hdnPartType").val(value.PartType);
                            partType = value.ParticipantType;

                        });

                        if (childJSON != null) {
                            if (partType == "Child") {
                                var iIndex = 0;
                                $.each(childJSON, function (index1, value1) {
                                    iIndex++;
                                    if (i == 0) {
                                        $('#iTopRow').removeClass("fa-user-plus");
                                        $('#iTopRow').addClass("fa-user-times");
                                        $('#iTopRow').attr("title", "Remove Attendee");
                                        $('#iTopRow').addClass("ancDelete");
                                        $('#iTopRow').removeClass("btnAddChildDisabled ");
                                        $('#iTopRow').show();
                                        $('#iTopRow').css("color", "");

                                        $('#iTopRow').css("cursor", "pointer");
                                        $("#txtChildName").val(value1.ChildName);
                                        $("#selGrade1").val(value1.Grade);
                                        $("#txtSchoolName").val(value1.SchoolName);
                                        loadProductGroups(value1.Grade, value1.Grade, childJSON);



                                    } else {


                                        addChild(index1);
                                        enableDisableAddDelIcon(index1, "Update");

                                        $("#txtChildName" + index1 + "").val(value1.ChildName);
                                        $("#selGrades" + index1 + "").val(value1.Grade);
                                        $("#txtSchoolName" + index1 + "").val(value1.SchoolName);
                                        addContest(index1, value1.Grade, childJSON);


                                        if (value1.SB != "" && value1.SB != null) {

                                            $("#chk8" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.VB != "" && value1.VB != null) {
                                            $("#chk9" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.MB != "" && value1.MB != null) {
                                            $("#chk10" + index1 + "").prop("checked", true);
                                        }

                                        if (value1.SC != "" && value1.SC != null) {
                                            $("#chk26" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.GB != "" && value1.GB != null) {
                                            $("#chk11" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.EW != "" && value1.EW != null) {
                                            $("#chk12" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.PS != "" && value1.PS != null) {
                                            $("#chk13" + index1 + "").prop("checked", true);
                                        }
                                        if (value1.BB != "" && value1.BB != null) {
                                            $("#chk14" + index1 + "").prop("checked", true);
                                        }
                                    }
                                    i++;

                                });
                                iIndex = iIndex + 1;
                                addChild((iIndex - 1));
                            } else if (partType == "Parent" || partType == "Adult") {

                                var iIndex = 0;
                                //$("#txtAttendeesCount").attr("disabled", "disabled");
                                $.each(childJSON, function (index1, value1) {


                                    iIndex++;
                                    if (i == 0) {
                                        $("#dvAdultSection").show();

                                        if ($("#hdnRegStatus").val() == "Confirmed") {

                                            $("#txtParentName").val(value1.AttendeeFirstName);
                                            $("#txtAttendeeLastName").val(value1.AttendeeLastName);

                                            $("#txtParentEmail").val(value1.AttendeeEmail);
                                            //} else {
                                            $('#iAdultRow').removeClass("fa-user-plus");
                                            $('#iAdultRow').addClass("fa-user-times");
                                            $('#iAdultRow').addClass("ancAdultDelete");
                                            $('#iAdultRow').removeClass("btnAddAdultDisabled ");
                                            $('#iAdultRow').show();
                                            $('#iAdultRow').css("color", "");
                                            $('#iAdultRow').attr("title", "Remove an Adult");
                                            $('#iAdultRow').css("cursor", "pointer");
                                            // $("#txtParentName").val(value1.ChildName);

                                            //$("#txtParentEmail").val(value1.AttendeeEmail);
                                        }



                                    } else {

                                        //if (count > 2) {
                                        //    $(".dvAdultsSection").html("");
                                        //    arrAdult = [];
                                        //    for (var i = 0; i < count - 2; i++) {


                                        //        addAdult((parseInt(i) + 1));
                                        //    }
                                        //}
                                        existChildId.push(index1);
                                        addAdult(index1);

                                        enableDisableAdultAddDelIcon(index1, "Update");

                                        $("#txtParentName" + index1 + "").val(value1.AttendeeFirstName);
                                        $("#txtAttendeeLastName" + index1 + "").val(value1.AttendeeLastName);

                                        $("#txtParentEmail" + index1 + "").val(value1.AttendeeEmail);


                                    }
                                    i++;

                                });
                                iIndex = iIndex + 1;
                                //addAdult((iIndex - 1));
                                if ($("#hdnRegStatus").val() == "Pending") { //addAdult((iIndex - 1)); 
                                }


                            }
                        }

                    } else if (JSON.stringify(data.d.length) == -2) {
                        statusMessage("Confirmation link expired.", "middle-center", "error");
                    } else if (JSON.stringify(data.d.length) == -1) {
                        statusMessage("Operation failed.", "middle-center", "error");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", ".btnAddChild", function (e) {
            // if (validateAddChild() == 1) {

            $(this).removeClass("btnAddChild");

            $(this).removeClass("fa-user-plus");
            $(this).addClass("fa-user-times");

            $(this).addClass("ancDelete");

            $(this).attr("title", "Remove Attendee");
            var cIndex = $("#hdnMaxIndex").val();

            addChild((parseInt(cIndex) + 1));
            // }
        });
        $(document).on("click", ".btnAddAdult", function (e) {
            // if (validateAddChild() == 1) {

            $(this).removeClass("btnAddAdult");

            $(this).removeClass("fa-user-plus");
            $(this).addClass("fa-user-times");

            $(this).addClass("ancAdultDelete");

            $(this).attr("title", "Remove an Adult");
            var cIndex = $("#hdnAdultIndex").val();
            var selAttendee = parseInt($("#txtAttendeesCount").val());
            selAttendee = selAttendee + 1;
            $("#txtAttendeesCount").val(selAttendee);

            cIndex = arrAdult.length;
            addAdult((parseInt(cIndex) + 1));

            // }
        });

        function validateAddChild() {
            var retVal = 1;
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();
            var schollName = $("#txtSchoolName").val();

            var childName1 = $("#txtChildName" + index + "").val();
            var grade1 = $("#selGrade1" + index + "").val();
            var schollName1 = $("#txtSchoolName" + index + "").val();

            if (childName == "") {
                statusMessage("Please select Child Name", "middle-center", "error");
                retVal = -1;
            } else if (grade == "0") {
                statusMessage("Please select Grade", "middle-center", "error");
                retVal = -1;
            } else if (schollName == "") {
                statusMessage("Please School Name", "middle-center", "error");
                retVal = -1;
            }

            if (index > 0) {
                if (childName1 == "") {
                    statusMessage("Please select Child Name", "middle-center", "error");
                    retVal = -1;
                } else if (grade1 == "0") {
                    statusMessage("Please select Grade", "middle-center", "error");
                    retVal = -1;
                } else if (schollName1 == "") {
                    statusMessage("Please School Name", "middle-center", "error");
                    retVal = -1;
                }
            }

            return retVal;
        }

        function sendUpdateLink(memberId) {

            var email = $("#txtUpdateEmailAddress").val();

            var option = $("#selActionType").val();

            var url = "&memberId=" + memberId + "&U=1";
            if ($("#selActionType").val() == "2") {
                url = "&memberId=" + memberId + "&U=1";
            } else if ($("#selActionType").val() == "3") {
                url = "&UID=" + memberId + "&U=3&Status=Confirmed";
            }
            var jsonData = JSON.stringify({ clearText: url });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Encrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var index = window.location.href.lastIndexOf('/');
                    var resendURl = window.location.href.substr(0, index);
                    resendURl = resendURl + "/Confirmation.aspx";
                    var originalURL = window.location.href;

                    var encryptedURl = originalURL + "?GUID=" + JSON.stringify(data.d);
                    if (option == "2") {
                        encryptedURl = originalURL + "?GUID=" + JSON.stringify(data.d);
                        sendEmail(encryptedURl, encryptedURl, 2, email, 0);
                    } else if (option == "3") {
                        encryptedURl = resendURl + "?ID=" + JSON.stringify(data.d);
                        sendEmail(encryptedURl, encryptedURl, 3, email, 0);
                    }


                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", "#btnSubmit", function (e) {

            if (validateUpdateReg() == 1) {

                duplictateCheck(0);


            }
        });

        function validateUpdateReg() {
            var retVal = 1;
            var email = $("#txtUpdateEmailAddress").val();
            var year = $("#selUpdateYear").val();
            var eventrId = $("#selEventName").val();

            if (year == "") {
                retVal = -1;
                statusMessage("Please select Year", "middle-center", "error");
            } else if (eventrId == "0") {
                retVal = -1;
                statusMessage("Please select Event Name", "middle-center", "error");
            } else if (email == "") {
                retVal = -1;
                statusMessage("Please enter Email Address", "middle-center", "error");
            }
            if (email != "") {
                if (!ValidateEmail(email)) {
                    retVal = -1;
                    statusMessage("Please enter valid Email Address", "middle-center", "error");
                }
            }
            return retVal;
        }

        function getmemberId(email) {



            var jsonData = JSON.stringify({ Email: email });

            showLoader();
            var memberId = 0;
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetmemberID",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d.length) > 0) {

                        $.each(data.d, function (index, value) {

                            memberId = value.FrMemberID;


                        });

                        if ($("#selActionType").val() == "3") {
                            sendUpdateLink(memberId);
                        } else if ($("#selActionType").val() == "2") {
                            verifyStatus(memberId);

                        }
                        //  verifyStatus(memberId);
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function reSendEmailLink(email) {
            //getmemberId(email);
            validateStatus(email);
        }

        $(document).on("keydown", "#txtChildName", function (e) {
            var childName = $(this).val();
            var schoolName = $("#txtSchoolName").val();
            var grade = $("#selGrade1").val();

            if (schoolName != "" && childName != "" && grade != "0") {
                $("#iTopRow").removeClass("btnAddChildDisabled");
                $("#iTopRow").removeClass("ancDelete");
                $("#iTopRow").addClass("btnAddChild");
                $("#iTopRow").css("cursor", "pointer");
                $("#iTopRow").css("color", "");
                $("#iTopRow").show();
            }
        });

        $(document).on("keydown", "#txtSchoolName", function (e) {
            var childName = $("#txtChildName").val();

            var schoolName = $(this).val();
            var grade = $("#selGrade1").val();

            if (schoolName != "" && childName != "" && grade != "0") {
                $("#iTopRow").removeClass("btnAddChildDisabled");
                $("#iTopRow").removeClass("ancDelete");
                $("#iTopRow").addClass("btnAddChild");
                $("#iTopRow").css("cursor", "pointer");
                $("#iTopRow").css("color", "");
                $("#iTopRow").show();
            }
        });

        function validateStatus(email) {

            var jsonData = JSON.stringify({ Email: email, FreeEventID: $("#selEventName").val(), Year: $("#selUpdateYear").val() });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/ValidateStatus",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) == 0) {

                        getmemberId(email);
                        //  sendUpdateLink(memberId);

                    } else if (JSON.stringify(data.d) > 0) {
                        statusMessage('Your registration has already been confirmed', 'middle-center', 'info');

                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function verifyStatus(memberId) {

            var jsonData = JSON.stringify({ FrMemberID: memberId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/VerifyStatus",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) == 0) {
                        if (memberId > 0) {
                            getAutoMemberIDofNSF(memberId);
                            $("#hdnMemberID").val(memberId);
                            //sendUpdateLink(memberId);
                        }
                    } else if (JSON.stringify(data.d) > 0) {
                        //if ($("#hdnEventType").val() == "Paid") {
                        // getAutoMemberIDofNSF(memberId);
                        //} else {
                        statusMessage("Your registration is yet to be confirmed. Please try again after confirming your registration.", "middle-center", "info");
                        // }

                    }

                }
            });
        }

        function getAutoMemberIDofNSF(memberId) {
            var jsonData = JSON.stringify({ FrMemberID: memberId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAutoMemeberIdOfNSF",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var nsfmemberid = JSON.stringify(data.d);
                    if (parseInt(nsfmemberid) > 0) {
                        $("#hdnisUpdate").val("1");
                        getFreeEventregistrations(memberId, "", 0);

                    } else {
                        sendUpdateLink(memberId);
                    }

                }
            });

        }

        $(document).on("keyup", "#txtAttendeesCount", function (e) {

            var attendees = $(this).val();
            if (attendees.length > 0) {
                if (attendees != "") {

                    if (parseInt(attendees) <= 0 || parseInt(attendees) > 10) {
                        retval = "-1";
                        if (parseInt(attendees) <= 0) {
                            statusMessage("Please enter valid number of Attendees", "middle-center", "error");
                        } else {
                            statusMessage("Please enter the Number of Attendees(less than 10)", "middle-center", "error");
                        }
                        $("#txtAttendeesCount").val("");
                    }
                }
            }
        });

        function checkEmail(email) {


            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email.value)) {

                email.focus;
                return false;
            } else {
                return true;
            }
        }

        function getAvailableSeats() {
            var year = $("#selyear").val();
            var freeEventID = $("#selEventName").val();



            var jsonData = JSON.stringify({ Year: year, FreeEventId: freeEventID });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAvailableSeats",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var remainingSeats = JSON.stringify(data.d);
                    $("#hdnAvailableSeats").val(remainingSeats);
                    $("#spnAvailableSeats").text(remainingSeats);

                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }

        $(document).on("change", "#selEventCode", function (e) {
            IsFreeEvent();
        });
        function IsFreeEvent() {

            var freeEventID = $("#selEventCode").val();

            var jsonData = JSON.stringify({ FreeEventId: freeEventID });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/IsFreeEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var type = JSON.stringify(data.d);
                    type = JSON.parse(type);

                    var eventType = type.split(",")[1];
                    var partType = type.split(",")[0];

                    $("#hdnEventType").val(eventType);
                    $("#hdnPartType").val(partType);

                    if (eventType == "Paid") {
                        $("#dvFeesSection").show();
                    } else {
                        $("#dvFeesSection").hide();
                    }
                    if (partType == "Child") {
                        $("#dvAdultSection").hide();
                        $("#dvContestSesction").show();
                    } else if (partType == "Adult") {

                        $("#dvAdultSection").show();
                        $("#dvContestSesction").hide();
                    } else if (partType == "Both") {

                        $("#dvAdultSection").show();
                        $("#dvContestSesction").show();
                    } else {

                        $("#dvAdultSection").hide();
                        $("#dvContestSesction").show();
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }
        $(document).on("change", "#txtAttendeesCount", function (e) {
            var count = $(this).val();
            var partType = $("#hdnPartType").val();
            var attendeeCount = $("#txtAttendeesCount").val();
            if (parseInt(count) > 0) {
                var frstName = $('#txtFirstName').val();
                var lastName = $('#txtLastName').val();
                var email = $('#txtEmailAddress').val();
                if (frstName != "" && isRegUserAdd == 0) {
                    $("#txtParentName").val(frstName);
                }
                if (lastName != "" && isRegUserAdd == 0) {
                    $("#txtAttendeeLastName").val(lastName);
                }
                if (email != "" && isRegUserAdd == 0) {
                    $("#txtParentEmail").val(email);
                }
                if (frstName != "" && lastName != "") {
                    enableDelIcon();
                }
            }

            if (partType == "Adult" || partType == "Parent") {
                if (count == 1) {

                    $("#dvAdultSection").show();
                    getEventFees();
                }
                if (count > 1) {
                    if (arrAdult.length == 0) {
                        for (var i = 0; i < count - 1; i++) {
                            addAdult((parseInt(i) + 1));

                        }
                    } else {
                        for (var i = 0; i < count - 1; i++) {

                            if ($("#dvNewAdult" + (i + 1) + "").html() == undefined) {
                                var i = arrAdult.length;
                                $("#hdnAdultIndex").val(i);
                                $("#dvAdultSection").show();
                                addAdult((parseInt(i) + 1));

                                getEventFees();
                            }
                        }

                    }
                } else if (partType == "Child") {
                    $("#dvContestSesction").show();
                    getEventFees();
                } else if (partType == "Both") {
                    $("#dvChildSection").show();
                    $("#dvContestSesction").show();
                }
                if (count > 1) {
                    // $(".dvAdultsSection").html("");
                    // arrAdult = [];
                    // for (var i = 0; i < count - 2; i++) {

                    // }
                }


                if (count - 1 < arrAdult.length) {
                    var adultTot = [];
                    for (var j = 0; j < arrAdult.length; j++) {
                        adultTot.push(arrAdult[j]);
                    }

                    for (var i = adultTot.length; i > 0; i--) {
                        if (i != count - 1) {

                            arrAdult.splice($.inArray(i, arrAdult), 1);
                            $("#dvNewAdult" + i + "").html("");

                        }

                    }

                }
            }
        });

        function getEventFees() {

            var freeEventID = $("#selEventCode").val();

            var jsonData = JSON.stringify({ FreeEventId: freeEventID });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetEventFees",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var fees = JSON.stringify(data.d);
                    fees = JSON.parse(fees);
                    var adultfee = fees.split(",")[0];
                    var discount = fees.split(",")[1];
                    var count = $("#txtAttendeesCount").val();

                    if (count == "1") {
                        $("#txtFee").val(adultfee);

                    } else if (count == "0") {
                        $("#txtFee").val("");
                    } else {
                        adultfee = (adultfee * parseInt(count));
                        adultfee = parseInt(adultfee) - parseInt(discount);
                        $("#txtFee").val(adultfee);
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function fillAttendeeCount(eventType) {

            //if (eventType == "Paid") {
            //    $("#txtAttendeesCount").append($("<option></option>").val
            //        ("0").html("Select"));
            //    $("#txtAttendeesCount").append($("<option></option>").val
            //        ("1").html("1"));
            //    $("#txtAttendeesCount").append($("<option></option>").val
            //        ("2").html("2"));
            //} else {
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("0").html("Select"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("1").html("1"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("2").html("2"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("3").html("3"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("4").html("4"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("5").html("5"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("6").html("6"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("7").html("7"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("8").html("8"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("9").html("9"));
            //$("#txtAttendeesCount").append($("<option></option>").val
            //    ("10").html("10"));
            // }
        }
        function loadProductGroup() {

            showLoader();
            var eventId = 2;

            var jsonData = JSON.stringify({ ProductGroup: { EventID: 22 } });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListProductGroup",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;
                    var productgroupId = 0;
                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {
                            i++;
                            productgroupId = value.ProductGroupID;
                            $("#selProductGroup").append($("<option></option>").val
                                (value.ProductGroupID).html(value.ProductGroupName));
                        });
                        if (i > 0 && i == 1) {
                            $("#selProductGroup").val(productgroupId);
                            loadProducts();
                        }

                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }
        function loadProducts() {

            showLoader();
            var eventId = 2;
            var pgid = $("#selProductGroup").val();

            var jsonData = JSON.stringify({ Product: { EventID: 22, ProductGroupID: pgid } });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListProduct",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;
                    var productId = 0;

                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {
                            i++;
                            $("#selProduct").append($("<option></option>").val
                                (value.ProductID).html(value.ProductName));
                            productId = value.ProductID;
                        });
                        if (i > 0 && i == 1) {
                            $("#selProduct").val(productId);

                        }

                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("change", "#selProductGroup", function (e) {
            loadProducts();
        });

        var arrAdult = [];
        function addAdult(index) {

            //index++;
            // $("#hdnAdultIndex").val(index);

            arrAdult.push(index.toString());

            $(".dvAdultsSection").show();
            var dHtml = "";
            // dHtml += $("#dvAttendees").html();
            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '<div id="dvNewAdult' + index + '" class="dvAdultsSection">';

            dHtml += '<div  style="float: left; margin-left:15px; ">';
            dHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative;" class="spnAdultTitle"></span></div> <div style= "clear: both; margin-bottom: 5px;" ></div>';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i></span>';

            dHtml += '<input type="text" autocomplete="off" class="form-control adultName" style="width:220px;" attr-index=' + index + ' name="InputName" id="txtParentName' + index + '" placeholder="First Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';

            dHtml += '<div style="float: left; margin-left: 30px; position:relative;">';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span >';
            dHtml += '<input type="text" autocomplete="off" class="form-control adultLastName" style="width:220px;" attr-index=' + index + ' name="InputName" id="txtAttendeeLastName' + index + '" placeholder="Last Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';

            dHtml += '<div style="float: left; margin-left: 30px; position:relative; ">';

            dHtml += '<div class="input-group">  ';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span>';
            dHtml += '<input type="text" autocomplete="off" class="form-control adultEmail" style="width:300px;" attr-index=' + index + ' name="InputName" id="txtParentEmail' + index + '" placeholder="Email Address" required>';


            dHtml += '</div>';
            dHtml += '</div>';

            dHtml += '<div style="float: left; margin-left: 30px; position:relative; ">';

            dHtml += '<div class="input-group">  ';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span>';
            dHtml += ' <select id="selAttendeeType' + index + '" disabled="disabled" class="form-control" required><option value="0">Select</option><option value="Adult" selected>Adult</option> <option value="Child">Child</option></select>';


            dHtml += '</div>';
            dHtml += '</div>';

            //dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="btnAddChild" attr-childId=' + index + ' title="Add a Child"><i class="fa fa-user-plus fa-2x"></i></a></div>';

            dHtml += '<div  style="float: left; margin-left: 3px; position:relative; top:2px;" id="dvAddAdultDelete' + index + '"></div>';

            //   dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="ancDelete" attr-childId=' + index + ' title="Remove Attendee"><i class="fa fa-user-times fa-2x"></i></a></div>';

            dHtml += '<div style="clear: both; margin-bottom: 5px;"></div>';





            dHtml += '</div>';


            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '</div>';



            var id = (index - 1);

            if ($("#dvNewAdult" + id + "").html() != "" && $("#dvNewAdult" + id + "").html() != undefined) {

                $("#dvNewAdult" + id + "").after(dHtml);
            } else {
                $("#dvAdultSection").after(dHtml);
            }

            // dvChildHtml.push(dHtml);

            // fillGrade("selGrades" + childCount + "");

            enableDisableAdultAddDelIcon(index, "");

            childCount++;
        }

        function enableDisableAdultAddDelIcon(cIndex, option) {
            var iconHtml = "";

            if (option == "Update") {

                iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Remove an Adult"><i class="fa fa-user-times fa-2x ancAdultDelete"  attr-childId=' + cIndex + '></i></a>';

            } else {

                iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Remove an Adult"><i class="fa fa-user-times fa-2x ancAdultDelete"  attr-childId=' + cIndex + '></i></a>';

                //iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Add an Adult"><i class="fa fa-user-plus fa-2x btnAddAdult "  attr-childId=' + cIndex + '></i></a>';
            }

            $("#dvAddAdultDelete" + cIndex + "").html(iconHtml);
        }


        $(document).on("keydown", ".adultName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#txtParentName" + attrIndex + "").val() != "" && $("#txtAttendeeLastName" + attrIndex + "").val() != "") {
                    if ($("#dvAddAdultDelete" + attrIndex + "").html() == "") {
                        enableDisableAdultAddDelIcon(attrIndex, "Add");
                        $("#iAdultRow").show();
                    }
                }
            }
        });


        $(document).on("keydown", ".adultLastName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#txtAttendeeLastName" + attrIndex + "").val() != "" && $("#txtParentName" + attrIndex + "").val() != "") {
                    if ($("#dvAddAdultDelete" + attrIndex + "").html() == "") {
                        enableDisableAdultAddDelIcon(attrIndex, "Add");
                        $("#iAdultRow").show();
                    }
                }
            }
        });


        function adultReset() {
            $("#selEventCode").val("0");
            $("#txtFirstName").val("");
            $("#txtMiddleName").val("");
            $("#txtLastName").val("");
            $("#txtEmailAddress").val("");
            $("#txtAttendeesCount").val("");


            //arrAdult = [];
            //$(".rbtnContests").removeAttr("checked");
            $("#txtParentName").val("");
            $("#txtAttendeeLastName").val("");
            $("#txtParentEmail").val("");

            $(".dvAdultsSection").hide();



            //$("#iTopRow").removeClass("btnAddChild");
            //$("#iTopRow").addClass("btnAddChildDisabled");
            //$("#iTopRow").css("cursor", "normal");
            //$("#iTopRow").css("color", "Grey");

            //$('#iAdultRow').removeClass("fa-user-plus");
            //$('#iAdultRow').addClass("fa-user-times");
            //$('#iAdultRow').css("color", "");
            //$('#iAdultRow').attr("title", "Remove an Adult");
            //$('#iAdultRow').addClass("ancAdultDelete");
            //$('#iAdultRow').removeClass("btnAddAdultDisabled ");
            //$('#iAdultRow').show();
            $("#dvAdultSection").html();

            $('#iAdultRow').css("cursor", "pointer");

            if ($("#dvAdultSection").html() == "") {
                resetAttendee();
                //var dvHtml = "";

                //dvHtml += '<div style="float: left; margin-left: 30px;">';
                //dvHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; " class="spnAdultTitle">Adult Attending </span></div> <div style= "clear: both; margin-bottom: 5px;" ></div >';
                //dvHtml += '<div class="input-group" >';

                //dvHtml += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';

                //dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 300px;" name="InputName" id="txtParentName" placeholder="Adult First Name" required>';
                //dvHtml += '</div>';
                //dvHtml += '</div>';

                //dHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
                //dHtml += '<div class="input-group">';
                //dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span >';
                //dHtml += '<input type="text" autocomplete="off" class="form-control" style="width:300px;"  name="InputName" id="txtAttendeeLastName" placeholder="Adult Last Name" required>';
                //dHtml += '</div>';
                //dHtml += '</div>';


                //dvHtml += '<div style="float: left; margin-left: 30px;  position:relative; top:25px;">';

                //dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>';
                //dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 300px;" name="InputName" id="txtParentEmail" placeholder="Adult Email" required>';
                //dvHtml += '</div>';
                //dvHtml += '</div>';


                //dvHtml += '<div style="float: left; margin-left:3px;">';

                //dvHtml += '<a style="position: relative; top: 30px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddAdultDisabled" style="color: grey;" id="iAdultRow"></i></a>';
                //dvHtml += '</div>';

                //dvHtml += '<div class="clear" style="margin-bottom: 5px;"></div>';
                //dvHtml += '<div>';

                //dvHtml += '<div id="dvAdult" style="position: relative; left: 32px; top:10px;">';
                //dvHtml += '</div>';
                //dvHtml += '</div>';
                //$("#dvAdultSection").html(dvHtml)
            }
            $("#iAdultRow").hide();
            //  listChapter();
        }

        function resetAttendee() {
            //$("#dvAdultSection").html();

            $('#iAdultRow').css("cursor", "pointer");

            if ($("#dvAdultSection").html() == "") {

                var dvHtml = "";

                dvHtml += '<div style="float: left; margin-left: 15px;">';
                dvHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; " class="spnAdultTitle">Adult Attending </span></div> <div style= "clear: both; margin-bottom: 5px;" ></div >';
                dvHtml += '<div class="input-group" >';

                dvHtml += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';

                dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 220px;" name="InputName" id="txtParentName" placeholder="First Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';

                dvHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
                dvHtml += '<div class="input-group">';
                dvHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span >';
                dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width:220px;"  name="InputName" id="txtAttendeeLastName" placeholder="Last Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';


                dvHtml += '<div style="float: left; margin-left: 30px;  position:relative; top:25px;">';

                dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>';
                dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 300px;" name="InputName" id="txtParentEmail" placeholder="Email Address" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';

                dHtml += '<div style="float: left; margin-left: 30px; position:relative; ">';

                dHtml += '<div class="input-group">  ';
                dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span>';
                dHtml += ' <select id="selAttendeeType" disabled="disabled" class="form-control" required><option value="0">Select</option><option value="Adult" selected>Adult</option> <option value="Child">Child</option></select>';


                dHtml += '</div>';
                dHtml += '</div>';


                dvHtml += '<div style="float: left; margin-left:3px;">';

                dvHtml += '<a style="position: relative; top: 30px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddAdultDisabled" style="color: grey;" id="iAdultRow"></i></a>';
                dvHtml += '</div>';

                dvHtml += '<div class="clear" style="margin-bottom: 5px;"></div>';
                dvHtml += '<div>';

                dvHtml += '<div id="dvAdult" style="position: relative; left: 32px; top:10px;">';
                dvHtml += '</div>';
                dvHtml += '</div>';
                $("#dvAdultSection").html(dvHtml)
            }
            $("#iAdultRow").hide();
        }

        function validateAdult() {
            var retVal = "1";
            var parentName = $("#txtParentName").val();
            var parentLastName = $("#txtAttendeeLastName").val();

            if (parentName == "") {
                retVal = "-1";
                statusMessage("Please enter Attendee First Name and Last Name", "middle-center", "error");
            } else if (parentLastName == "") {
                retVal = "-1";
                statusMessage("Please enter Attendee First Name and Last Name", "middle-center", "error");
            }


            if (arrAdult.length > 0) {
                for (var i = 0; i < arrAdult.length; i++) {

                    parentName = $("#txtParentName" + arrAdult[i] + "").val();
                    parentLastName = $("#txtAttendeeLastName" + arrAdult[i] + "").val();

                    if (parentName == "") {
                        retVal = "-1";
                        statusMessage("Please enter Attendee First Name", "middle-center", "error");
                    } else if (parentLastName == "") {
                        retVal = "-1";
                        statusMessage("Please enter Attendee Last Name", "middle-center", "error");
                    }
                }
            }
            return retVal;
        }

        $(document).on("keyup", "#txtFirstName", function (e) {
            var firstName = $(this).val();
            var lastName = $("#txtAttendeeLastName").val();
            var email = $("#txtEmailAddress").val();
            var attendeeCount = $("#txtAttendeesCount").val();
            if (parseInt(attendeeCount) > 0 && isRegUserAdd == 0) {
                $("#txtParentName").val(firstName);
            }


            if (firstName != "" && lastName != "") {
                //$("#iAdultRow").removeClass("btnAddAdultDisabled");
                //$("#iAdultRow").removeClass("ancAdultDelete");
                //$("#iAdultRow").addClass("btnAddAdult");
                //$("#iAdultRow").css("cursor", "pointer");
                //$("#iAdultRow").css("color", "");
                //$("#iAdultRow").show();

                enableDelIcon();
            }
        });
        $(document).on("keyup", "#txtLastName", function (e) {
            var lastName = $(this).val();
            var firstName = $("#txtParentName").val();
            var attendeeCount = $("#txtAttendeesCount").val();
            if (parseInt(attendeeCount) > 0 && isRegUserAdd == 0) {
                $("#txtAttendeeLastName").val(lastName);
            }

            var email = $("#txtEmailAddress").val();
            if (firstName != "" && lastName != "") {
                //$("#iAdultRow").removeClass("btnAddAdultDisabled");
                //$("#iAdultRow").removeClass("ancAdultDelete");
                //$("#iAdultRow").addClass("btnAddAdult");
                //$("#iAdultRow").css("cursor", "pointer");
                //$("#iAdultRow").css("color", "");
                //$("#iAdultRow").show();
                enableDelIcon();
            }

        });

        $(document).on("keyup", "#txtEmailAddress", function (e) {
            var email = $(this).val();
            var lastName = $("#txtAttendeeLastName").val();
            var firstName = $("#txtParentName").val();
            var attendeeCount = $("#txtAttendeesCount").val();
            if (parseInt(attendeeCount) > 0 && isRegUserAdd == 0) {
                $("#txtParentEmail").val(email);
            }

            if (firstName != "" && lastName != "" && email != "") {

            }
            // $("#txtParentEmail").val(email);

        });

        function validateAdultDuplicates(mode) {

            var retVal = 1;
            var prevparentName = $("#txtParentName").val();
            var prevparentLastName = $("#txtAttendeeLastName").val();
            var parentName = "";
            var parentLastName = "";

            var duplicateName = "";
            var arrNames = [];
            if (prevparentName != undefined && prevparentLastName != undefined) {
                var name = prevparentName.trim() + " " + prevparentLastName.trim();
                arrNames.push(name);
            }

            if (arrAdult.length > 0) {
                for (var i = 0; i < arrAdult.length; i++) {

                    parentName = $("#txtParentName" + arrAdult[i] + "").val();
                    parentLastName = $("#txtAttendeeLastName" + arrAdult[i] + "").val();
                    var name = parentName.trim() + " " + parentLastName.trim();

                    for (var j = 0; j < arrNames.length; j++) {

                        if (name == arrNames[j]) {
                            retVal = -1;
                            duplicateName = name;
                        }
                    }
                    if (retVal == 1) {
                        arrNames.push(name);
                    }

                }
            }

            if (parseInt(retVal) > 0) {
                if (parseInt(mode) == 1) {

                    postNewRegistration(1);

                } else {
                    postNewRegistration(2);
                }
            } else {
                statusMessage(duplicateName + " appears twice. Pleas eliminate duplicates", "middle-center", "error");
            }

            //showLoader();

            //var existingAttendeeCount = $("#hdnExistingAttendeeCount").val();
            //var currentAttendeeCount = $("#txtAttendeesCount").val();


            //var fee = $("#txtFee").val();
            //var existingChildIs = "";
            //for (var i = 0; i < existChildId.length; i++) {
            //    existingChildIs += existChildId[i] + ",";
            //}
            //var jsonData = JSON.stringify({ FreeEventId: freeEventId, FrMemberid: frMemberId, FirstName: FirstName, LastName: LastName, ExistingChildId: existingChildIs });

            //$.ajax({
            //    type: "POST",
            //    url: "FreeEventReg.aspx/ValidateAdultDuplciate",
            //    data: jsonData,
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (data) {

            //        if (JSON.stringify(data.d) > 0) {
            //            statusMessage("Duplicate Attendee exists", "middle-center", "error");
            //            // 

            //            hideLoader();
            //        } else {

            //            postNewRegistration(2);
            //            hideLoader();
            //        }

            //    },
            //    failure: function (response) {
            //        alert(response.d);
            //        hideLoader();
            //    }
            //});
        }

        $(document).on("click", "#btnAddAttendee", function (e) {
            var firstName = $("#txtParentName").val();
            var lastName = $("#txtAttendeeLastName").val();
            var type = $("#selAttendeeType").val();

            if (arrAdult.length == 0) {
                if (firstName != "" && lastName != "" && type != "0") {
                    var index = arrAdult.length;
                    $("#txtAttendeesCount").val(parseInt(index) + 2);
                    addAdult(parseInt(index) + 1);

                } else {
                    validateAttendee(firstName, lastName, type);
                }
            } else if (arrAdult.length > 0) {
                var isAdd = 1;
                for (var i = 1; i <= arrAdult.length; i++) {
                    firstName = $("#txtParentName" + i + "").val();

                    lastName = $("#txtAttendeeLastName" + i + "").val();
                    type = $("#selAttendeeType" + i + "").val();
                    if (firstName == "" || lastName == "" || type == "0") {
                        isAdd = 0;
                    }
                }
                if (isAdd == 1) {
                    var index = arrAdult.length;
                    $("#txtAttendeesCount").val(parseInt(index) + 2);
                    addAdult(parseInt(index) + 1);

                } else {
                    validateAttendee(firstName, lastName, type);
                }
            }

        });

        function validateAttendee(firstName, lastName, type) {
            var retVal = 1;
            if (firstName == "") {
                statusMessage("Please enter FirstName", "mid-center", "error");
                retVal = -1;
            } else if (lastName == "") {
                statusMessage("Please enter LastName", "mid-center", "error");
                retVal = -1;
            } else if (type == "") {
                statusMessage("Please enter Type", "mid-center", "error");
                retVal = -1;
            }
            return retVal;
        }

        $(document).on("click", "#lblParent", function (e) {
            $("#rbtnParent").prop("checked", true);
            participantType = $("#rbtnParent").val();
        });
        $(document).on("click", "#lblTeacher", function (e) {
            $("#rbtnTeacher").prop("checked", true);
            participantType = $("#rbtnTeacher").val();
        });
        $(document).on("click", "#lblVolunteer", function (e) {
            $("#rbtnVolunteer").prop("checked", true);
            participantType = $("#rbtnVolunteer").val();
        });
        $(document).on("click", "#lblOther", function (e) {
            $("#rbtnOther").prop("checked", true);
            participantType = $("#rbtnOther").val();
        });

    </script>

    <div class="container" style="min-height: 450px; width: 1190px; background-color: #FFFFFF;">

        <%--        <div class="page-header">
            <center>

                <h3 style="color: #46A71C; font-weight: bold; font-family: 'Trebuchet MS'">Welcome to North South Foundation
                  <div class="clear" style="margin-bottom: 10px;"></div>
                    Register for a Free Event
                </h3>


            </center>
        </div>--%>

        <div style="float: left;">
            <div style="clear: both; margin-bottom: 20px;"></div>
            <span style="font-weight: bold; font-size: 18px;">To Register, scroll down to bottom</span>
        </div>

        <div class="page-header">
            <center>
                <div style="color: #9A0033; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                    Parenting 
                 
                </div>
                <div style="clear: both; margin-bottom: 25px;"></div>
                <div style="color: #9A0033; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS';">
                    I-Generation
        Seminar - 2018 
                 
                </div>
                <div style="clear: both; margin-bottom: 25px;"></div>
                <center>
                    <img src="../Images/NSF-Parenting-the-i-generation-2018.jpg" style="width: 100%;" />
                </center>

                <div style="clear: both; margin-bottom: 20px;"></div>
                <div style="color: #0070BF; padding-bottom: 20px; font-weight: bolder; font-size: 20px; font-family: 'Trebuchet MS'; display: none;">
                    The future is in your hands. Let’s build it together
                 
                </div>
            </center>

        </div>

        <%--    <div style="clear: both; margin-top: 10px; margin-bottom: 10px;">
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> This alert box could indicate a successful or positive action.
            </div>
        </div>--%>
        <div class="container">

            <div class="col-md-12" style="display: none;">
                <div class="col-md-3">
                    <img src="../Images/left.png" style="width: 100%" />
                </div>
                <div class="col-md-3 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">When</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>June 16th, Saturday</li>
                        <li>9:30 am — 4:30 pm</li>

                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">Where</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>NIU Naperville</li>
                        <li>Conference Center</li>
                        <li style="color: #C00000;">1120 E. Diehl Road, Naperville, IL 60563</li>

                    </ul>
                </div>
                <div class="col-md-3">
                    <img src="../Images/right.png" style="width: 100%" />
                </div>
                <%--<div class="col-md-4">
                    <img src="/Images/M.png" style="width:100%;" />
                </div>--%>
            </div>

            <div class="col-md-12" style="display: none;">
                <h3><span style="font-weight: bold; color: #C09100;">LEARN ABOUT </span>
                    <span style="font-weight: bold; color: black;">(GRADES 9 TO 12)</span></h3>
                <div class="col-md-8 ">
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: square; font-weight: bold;">
                        <li>How to educate your child to be a winner?</li>
                        <li>How to get admitted to Elite Schools (Ivy League)?</li>
                        <li>How to Excel at a University?</li>
                        <li>Scholarships and finance for higher 
            education</li>
                        <li>SAT and ACT preparation</li>
                        <li>Perfect college essay & career selection</li>
                        <%-- <li style="color: #07AF50;">Mock Bees in Math and Spelling (Grades 1 to 8)</li>--%>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div style="color: #3F3F3F; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                        STUDENTS IN
                 
                    </div>
                    <div style="clear: both; margin-bottom: 25px;"></div>
                    <div style="color: #3F3F3F; padding-bottom: 20px; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                        GRADES 9-12
                 
                    </div>
                </div>

            </div>
            <div class="col-md-12" style="display: none;">
                <div class="col-md-3">
                    <img src="../Images/M.png" style="width: 100%; position: relative; top: 10px;" />
                </div>
                <div class="col-md-3">
                    <img src="../Images/new_life.png" style="width: 100%; position: relative; top: 60px;" />
                </div>
                <div class="col-md-3">
                    <img src="../Images/win.png" style="width: 100%; position: relative; top: 50px;" />
                </div>

                <div class="col-md-3">
                    <img src="../Images/bee.png" style="width: 100%; position: relative; top: -15px;" />
                </div>
            </div>


            <%--   <div style="clear:both; border:1px solid #CECECE; height:1px;"></div>--%>
            <%--         <div class="col-md-12">
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold; color: #07AF50;">Available Seat(s)</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li><span id="spnAvailableSeats" style="color: #C00000; font-weight: bold;">450</span></li>

                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">When</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>Saturday, Jul 15th</li>
                        <li>9:30 am — 4:30 pm</li>

                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">Where</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>Yellow Box</li>
                        <li>Community Christian Church</li>
                        <li style="color: #C00000;">1635 Emerson Ln, Naperville, IL 60540 </li>

                    </ul>
                </div>
            </div>--%>


            <div style="clear: both; margin-bottom: 0px;"></div>
            <div class="container" id="dvRegSection">
                <center>
                    <select id="selActionType" style="width: 300px;" class="form-control">
                        <option value="0">Select</option>
                        <option value="1">New Registration</option>
                        <option value="2">Update my Registration</option>
                        <option value="3">Resend my confirmation link</option>
                        <option value="4">Event details</option>

                    </select>

                </center>
            </div>
            <div style="clear: both; margin-bottom: 20px;">
            </div>
            <div style="clear: both;">
                <div style="width: 235px; margin: auto;">
                    <div style="float: left;">
                        <span style="color: #C09100; font-weight: bold; font-size: 20px; color: #07AF50;">Available Seat(s):</span>
                    </div>
                    <div style="float: left;">
                        <h4><span id="spnAvailableSeats" style="color: #C00000; font-weight: bold; position: relative; left: 5px; top: -10px;">450</span></h4>
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 0px;"></div>
                <div style="width: 100%; height: 30px; border-bottom: 1px solid grey; display: none;" id="dvRegHeader">
                    <span id="spnRegTitle" style="font-size: 18px; font-weight: bold; position: relative; top: 5px; left: 10px;">Registering Person Details</span>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 5px;">
            </div>
            <div class="container" id="dvNewRegistration" style="display: none;">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-md-6" style="display: none;">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Year</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                <select id="selyear" class="form-control" required>
                                    <option value="0">Select</option>

                                    <option value="2017">2017</option>
                                    <option value="2018" selected="selected">2018</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <center>
                            <div class="form-group" style="position: relative; left: 270px;" id="dvEventCode">
                                <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Event Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-tasks"></i></span>
                                    <select id="selEventCode" class="form-control" required>
                                        <option value="0">Select</option>

                                    </select>

                                </div>
                            </div>
                        </center>
                    </div>
                </div>
                <%-- <hr />--%>

                <div class="col-md-12" style="margin-bottom: 10px; position: relative; left: -15px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; color: #58AD36; font-size: 15px; font-weight: bold;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtFirstName" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; left: 150px; color: #58AD36; font-size: 15px; font-weight: bold; visibility: hidden">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtMiddleName" placeholder="Middle Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtLastName" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 10px; position: relative; left: -15px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="email" class="form-control" id="txtEmailAddress" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Partcipant Type</label>

                            <ul style="margin-left: -40px; position: relative; top: 5px;">
                                <li style="display: inline-block; list-style: none;">

                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnParent" value="Parent" />
                                    <label id="lblParent" for="InputName" class="control-label" style="position: relative; top: -3px; cursor:pointer;">Parent</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnTeacher" value="Parent" />
                                    <label id="lblTeacher" for="InputName" class="control-label" style="position: relative; top: -3px; cursor:pointer;">Teacher</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnVolunteer" value="Parent" />
                                    <label id="lblVolunteer" for="InputName" class="control-label" style="position: relative; top: -3px; cursor:pointer;">Volunteer</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnOther" value="Parent" />
                                    <label id="lblOther" for="InputName" class="control-label" style="position: relative; top: -3px; cursor:pointer;">Other</label>
                                </li>
                            </ul>
                            <%--   <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="email" class="form-control" id="email">--%>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Number of Attendees</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select id="txtAttendeesCount" disabled class="form-control" required>
                                    <option value="0">Select</option>
                                    <option value="1" selected="true">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>

                                </select>

                            </div>
                        </div>
                    </div>
                </div>




                <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;" id="dvFeesSection">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Fee</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="text" class="form-control" disabled id="txtFee" placeholder="Fee">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="dvProductGroupCode">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Product Group</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <select id="selProductGroup" disabled="disabled" class="form-control" required>
                                    <option value="0">Select</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="dvProduct">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Product</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <select id="selProduct" disabled="disabled" class="form-control" required>
                                    <option value="0">Select</option>

                                </select>
                            </div>
                        </div>
                    </div>


                </div>
                <div style="clear: both; margin-bottom: 0px;"></div>

                <div style="width: 100%; height: 30px; border-bottom: 1px solid grey; position: relative; left: -15px;" id="dvAttendeeHeader">
                    <span id="spnAttendeeTitle" style="font-size: 18px; font-weight: bold; position: relative; top: 5px; left: 10px;">Attendee Details</span>
                    <div style="float: right; position: relative; right: 35px; top: -5px;">
                        <input type="button" id="btnAddAttendee" value="Add" class="btn btn-info" style="background-color: #0406B2; border: #0406B2;" name="Add" />
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 20px;"></div>

                <div style="clear: both; margin-bottom: 5px;"></div>
                <div style="background-color: #b3b3b3; width: 94.5%; position: relative; left: 15px; height: 25px;"><span style="font-weight: bold; font-size: 15px; background-color: #b3b3b3; position: relative; left: 90px; top: 2px;" class="spnAdultTitle">First Name</span> <span style="font-weight: bold; font-size: 15px; background-color: #b3b3b3; position: relative; left: 300px; top: 2px;" class="spnAdultLastName">Last Name</span> <span style="font-weight: bold; font-size: 15px; background-color: #b3b3b3; position: relative; left: 560px; top: 2px;" class="spnAdultEmail">Email</span> <span style="font-weight: bold; font-size: 15px; background-color: #b3b3b3; position: relative; left: 800px; top: 2px;" class="spnAdultType">Type</span></div>
                <div style="clear: both; margin-bottom: 5px;"></div>

                <div id="dvAdultSection">


                    <div style="float: left; margin-left: 15px;">

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" id="txtParentName" placeholder="First Name" style="width: 220px;">
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative;">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" id="txtAttendeeLastName" placeholder="Last Name" style="width: 220px;">
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative;">
                        <%--<div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative; left: 75px;">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>--%>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="email" class="form-control" id="txtParentEmail" placeholder=" Email Address" style="width: 300px;">
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative;">
                        <%--<div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative; left: 75px;">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>--%>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <select id="selAttendeeType" disabled="disabled" class="form-control" required>
                                <option value="0">Select</option>
                                <option value="Adult" selected="true">Adult</option>
                                <option value="Child">Child</option>
                            </select>
                        </div>
                    </div>

                    <div style="float: left; margin-left: 3px; position: relative; top: 0px;">
                        <a style="position: relative; top: 5px;" title="Add a Adult"><i class="fa fa-user-plus fa-2x btnAddAdultDisabled" style="color: grey; display: none;" id="iAdultRow"></i></a>
                    </div>

                    <div class="clear" style="margin-bottom: 5px;"></div>
                    <div>

                        <div id="dvAdult" style="position: relative; left: 32px;">
                        </div>
                    </div>
                </div>


                <div id="dvContestSesction" style="display: none;">

                    <div style="float: left; margin-left: 30px;">
                        <div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative;" class="spnChildTitle">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" autocomplete="off" class="form-control" style="width: 386px;" name="InputName" id="txtChildName" placeholder="Child Name" required>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative; top: 25px;">
                        <%--<div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative; left: 75px;">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>--%>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>

                            <select id="selGrade1" class="form-control" style="width: 130px;">
                                <option value="0">Grade</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative; top: 25px;">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>
                            <input type="text" class="form-control" name="InputName" autocomplete="off" style="width: 386px;" id="txtSchoolName" placeholder="School Name" required>
                        </div>
                    </div>

                    <div style="float: left; margin-left: 3px; position: relative; top: 25px;">
                        <a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey; display: none;" id="iTopRow"></i></a>
                    </div>

                    <div class="clear" style="margin-bottom: 5px;"></div>
                    <div>

                        <div id="dvContest" style="position: relative; left: 32px;">
                        </div>
                    </div>
                </div>

                <%--  <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtChildName" placeholder="Child Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; left: 100px; color: #58AD36; font-size: 15px; font-weight: bold;">Child Attending Contest(s)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select id="selGrade1" class="form-control">
                                    <option value="0">Grade</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>
                                <input type="email" class="form-control" id="txtSchoolName" placeholder="School Name">
                            </div>

                        </div>
                    </div>
                </div>--%>

                <div class="col-md-12 text-center" style="margin-top: 10px;">
                    <ul style="position: relative; left: -25px;">
                        <li style="display: inline-block; list-style: none;">
                            <input type="button" id="btnSave" value="Continue" class="btn btn-info" style="background-color: #0406B2; border: #0406B2;" name="Save" />
                        </li>
                        <li style="display: inline-block; list-style: none;">
                            <input type="button" id="btnReset" value="Reset" class="btn btn-info" style="background-color: #0406B2; border: #0406B2;" name="Save" />
                        </li>
                    </ul>
                </div>

                <div class="col-lg-5 col-md-push-1" style="display: none;">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <strong><span class="glyphicon glyphicon-ok"></span>Success! Message sent.</strong>
                        </div>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-remove"></span><strong>Error! Please check all page inputs.</strong>
                        </div>
                    </div>
                </div>

            </div>



            <div class="bs-example" id="dvUpdateRegistration" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">

                <div>
                    <div style="float: left; width: 35px;">
                        <label for="InputName" class="control-label">Year</label>
                    </div>
                    <div style="float: left;">
                        <select id="selUpdateYear" class="form-control" required>
                            <option value="0">Select</option>

                            <option value="2017">2017</option>
                            <option value="2018" selected="selected">2018</option>

                        </select>
                    </div>

                </div>

                <div>
                    <div style="float: left; width: 75px; margin-left: 20px;">
                        <label for="InputName" class="control-label">Event Name</label>
                    </div>
                    <div style="float: left;">
                        <select id="selEventName" class="form-control" required>
                            <option value="0">Select</option>

                        </select>
                    </div>

                </div>

                <div>
                    <div style="float: left; width: 95px; margin-left: 20px;">
                        <label for="InputName" class="control-label">Email Address</label>
                    </div>
                    <div style="float: left;">
                        <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtUpdateEmailAddress" style="width: 275px;" placeholder="Email Address" required>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <input type="button" id="btnSubmit" value="Submit" class="btn btn-info" name="Submit" />
                    </div>
                </div>

            </div>

            <div style="clear: both; margin-bottom: 20px;">
            </div>

            <%--           <div class="bs-example" id="dvNewRegistration" style="width: 80%; margin: auto; display: none;">



                <form role="form" class="form-horizontal">
                    <div>
                        <div class="well well-sm" style="float: right;"><strong><i style="color: red; font-size: 20px; position: relative; top: 6px;">* </i>mandatory fields</strong></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Year</label>
                            </div>
                            <div style="float: left;">
                                <select id="selyear" class="form-control" required>
                                    <option value="0">Select</option>

                                    <option value="2017" selected="selected">2017</option>
                                    <option value="2018">2018</option>

                                </select>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>

                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Event Name</label>
                            </div>
                            <div style="float: left;">
                                <select id="selEventCode" style="width: 371px;" class="form-control" required>
                                    <option value="0">Select</option>

                                </select>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>

                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Name</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtFirstName" placeholder="First Name" required style="width: 210px;">
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtMiddleName" placeholder="Middle Initial" required style="width: 106px;">
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtLastName" placeholder="Last Name" required style="width: 210px;">
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Email Address</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtEmailAddress" style="width: 375px;" placeholder="Email Address" required>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Partcipant Type</label>
                            </div>
                            <div style="float: left;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnParent" value="Parent" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Parent</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnTeacher" value="Teacher" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Teacher</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnVolunteer" value="Volunteer" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Volunteer</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnOther" value="Other" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Other</label>
                            </div>
                        </div>
                        <div style="float: left;">
                            <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Number of Attendees</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" style="width: 60px;" name="InputName" id="txtAttendeesCount" placeholder="" required>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>

                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div style="margin-left: 150px;">
                            <label for="InputName" class="control-label">Children attending contest(s):</label>
                        </div>
                        <div id="dvContestSesction">

                            <div style="float: left; margin-left: 150px;">
                                <input type="text" autocomplete="off" class="form-control" style="width: 250px;" name="InputName" id="txtChildName" placeholder="Child Name" required>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" class="form-control" name="InputName" autocomplete="off" style="width: 280px;" id="txtSchoolName" placeholder="School Name" required>
                            </div>

                            <div style="float: left; margin-left: 10px;">
                                <a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey; display: none;" id="iTopRow"></i></a>
                            </div>

                            <div class="clear" style="margin-bottom: 5px;"></div>
                            <div>

                                <div id="dvContest" style="position: relative; left: 120px;">
                                </div>
                            </div>
                        </div>

                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div id="dvAttendees"></div>


                            <div class="clear" style="margin-bottom: 15px;"></div>
                            <div>

                                <input type="button" id="btnSave" value="Submit" class="btn btn-info" name="Save" />
                                <input type="button" id="btnReset" value="Reset" class="btn btn-info" name="Reset" />
                            </div>
                        </div>
                </form>
               

            </div>--%>
            <div class="clear" style="margin-bottom: 10px;"></div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
                <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </div>
        </div>
        <div id="overlay"></div>
        <div id="ohsnap">
        </div>
        <input type="hidden" id="hdnFreeEventID" value="0" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
        <input type="hidden" value="0" id="hdnIndex" />
        <input type="hidden" value="0" id="hdnisUpdate" />
        <input type="hidden" value="0" id="hdnEmailExists" />
        <input type="hidden" value="0" id="hdnIsChild" />
        <input type="hidden" value="0" id="hdnMaxIndex" />
        <input type="hidden" value="0" id="hdnMode" />
        <input type="hidden" value="0" id="hdnMemberID" />
        <input type="hidden" value="0" id="hdnAvailableSeats" />
        <input type="hidden" value="0" id="hdnEventType" />
        <input type="hidden" value="0" id="hdnFirstName" runat="server" />
        <input type="hidden" value="0" id="hdnLastName" runat="server" />
        <input type="hidden" value="0" id="hdnEmail" runat="server" />
        <input type="hidden" value="0" id="hdnPartType" />
        <input type="hidden" value="0" id="hdnAdultIndex" />
        <input type="hidden" value="" id="hdnRegStatus" />
        <input type="hidden" value="0" id="hdnExistingAttendeeCount" />
</asp:Content>
