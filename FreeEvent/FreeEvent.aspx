﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeEvent.aspx.cs" Inherits="FreeEvent_FreeEvent" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>

    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


 
    <link href="../css/Loader.css" rel="stylesheet" />
    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <script src="../js/jquery.toast.js"></script>

    <%-- <script src="../Scripts/angular.min.js"></script>


    <script src="../Scripts/angular-route.min.js"></script>
    <script src="../Scripts/angular-sanitize.min.js"></script>
    <script src="../Scripts/FreeEvent/App.js"></script>
    
    <script src="../Scripts/FreeEvent/FreeEventCtrl.js"></script>
    <script src="../Scripts/FreeEvent/Common/CommonService.js"></script>--%>
    <style type="text/css">
        .bs-example {
            margin: 20px;
        }
        /* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
        .form-horizontal .control-label {
            padding-top: 7px;
        }

        .clear {
            clear: both;
        }

        .man {
            color: red;
            font-size: 14px;
        }
    </style>

    <script type="text/javascript">

        var arrContest = [];

        $(function (e) {



            $('#dateRangePicker').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });

            $('#dvEndDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });
            $('#dvRegStartDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });


            populateYear();

            listChapter();
            loadProductGroups();
            listFreeEvents(0);
        });

        function statusMessage(message, position, type) {

            $.toast({

                text: '<b>' + message + '</b>',
                icon: type,
                position: position,
                hideAfter: 5000,
                stack: 1

            })
        }

        function populateYear() {
            var d = new Date();
            var year = d.getFullYear();
            var prevyear = parseInt(year) - 1;
            var futureYear = parseInt(year) + 1;

            $("#selyear").append($("<option></option>").val
                (futureYear).html(futureYear));

            $("#selyear").append($("<option></option>").val
                (year).html(year));

            $("#selyear").append($("<option></option>").val
                (prevyear).html(prevyear));
            $("#selyear").val(year);

        }
        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }

        function loadProductGroups() {

            showLoader();
            var eventId = 2;

            var jsonData = JSON.stringify({ ProductGroup: { EventID: 2 } });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListProductGroup",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;

                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {
                            i++;
                            dvHtml += "<div style='width:170px; float:left;'>";
                            dvHtml += ' <div style="float: left; margin-left: 30px;">';
                            dvHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductGroupID + ' id="rbtn' + value.ProductGroupID + '" />';
                            dvHtml += ' </div>';
                            dvHtml += '<div style="float: left; margin-left: 10px;">';
                            dvHtml += ' <label for="InputName" class="control-label">' + value.ProductGroupName + '</label>';
                            dvHtml += ' </div>'
                            dvHtml += ' </div>'

                            if (i == 4) {
                                dvHtml += '<div style="clear:both;"></div>';
                            }

                        });

                        $("#dvProductGroup").html(dvHtml);
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function listChapter() {
            var chapterId = GetParameterValues("ChapterId");
            var jsonData = JSON.stringify({ Chapter: { ChapterID: chapterId } });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListChapter",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {


                            $("#selChapter").append($("<option></option>").val
                                (value.ChapterID).html(value.ChapterName));

                        });
                        var chapterID = GetParameterValues("ChapterId");
                        $("#selChapter").val(chapterID);
                        $("#selChapter").attr("disabled", "disabled");
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function validateEvent() {
            var retval = 1;
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var startDate = $("#txtStartDate").val();
            var startTime = $("#txtStartTime").val();
            var endTime = $("#txtEndTime").val();
            var venueName = $("#txtVenueName").val();
            var venueAddr = $("#txtVenueAddr").val();
            //var eventDesc = $("#txtEventDescription").val();
            var regCount = $("#txtRegCount").val();
            var regStartDate = $("#txtRegStartDate").val();
            var regStartTime = $("#selRegEndTime").val();
            var type = $("#selEventType").val();
            var adultFee = $("#txtAdultFee").val();
            var childFee = $("#txtChildFee").val();
            var partType = $("#selPartType").val();


            var lastDateToRegister = $("#txtEndDate").val();
            var regEndTime = $("#selEnDateEndTime").val();

            if (chapterId == "") {
                retval = "-1";
                statusMessage("Please select Chapter", "top-right", "error");
            } else if (eventCode == "") {
                retval = "-1";
                statusMessage("Please enter Event Code", "top-right", "error");
            } else if (eventName == "") {
                retval = "-1";
                statusMessage("Please enter Event Name", "top-right", "error");

                //} else if (eventDesc == "") {
                //    retval = "-1";
                //    statusMessage("Please enter Event Description");
            } else if (partType == "0") {
                retval = "-1";
                statusMessage("Please select Participant Type", "top-right", "error");
            } else if (type == "0") {
                retval = "-1";
                statusMessage("Please select Event Type", "top-right", "error");
            } else if (type == "Paid" && partType == "Adult" && adultFee == "") {
                retval = "-1";
                statusMessage("Please enter Adult Fee", "top-right", "error");
            } else if (type == "Paid" && partType == "Child" && childFee == "") {
                retval = "-1";
                statusMessage("Please enter Child Fee", "top-right", "error");
            } else if (type == "Paid" && partType == "Both" && (childFee == "" || adultFee == "")) {
                retval = "-1";
                statusMessage("Please enter Adult/Child Fee", "top-right", "error");
            } else if (type == "Paid" && partType == "Adult" && parseInt(adultFee) <= 0) {
                retval = "-1";
                statusMessage("Please enter the value for Adult Fee greater than 0", "top-right", "error");
            } else if (type == "Paid" && partType == "Child" && parseInt(childFee) <= 0) {
                retval = "-1";
                statusMessage("Please enter the value for Child Fee greater than 0", "top-right", "error");
            } else if (type == "Paid" && partType == "Both" && (parseInt(adultFee) <= 0 || parseInt(childFee) <= 0)) {
                retval = "-1";
                statusMessage("Please enter the value for Adult/Child Fee greater than 0", "top-right", "error");
            } else if (regCount == "") {
                retval = "-1";
                statusMessage("Please enter Maximum Registration Count", "top-right", "error");
            } else if (parseInt(regCount) <= 0) {
                retval = "-1";
                statusMessage("Maximum Registration Count should be greater than 0.", "top-right", "error");
            } else if (startDate == "") {
                retval = "-1";
                statusMessage("Please enter Event Date", "top-right", "error");
            } else if (startTime == "") {
                retval = "-1";
                statusMessage("Please enter Start Time", "top-right", "error");
            } else if (endTime == "") {
                retval = "-1";
                statusMessage("Please enter End Time", "top-right", "error");
            } else if (regStartDate == "") {
                retval = "-1";
                statusMessage("Please enter Registration Start Date", "top-right", "error");
            } else if (regStartTime == "") {
                retval = "-1";
                statusMessage("Please enter Registration Start Time", "top-right", "error");
            } else if (lastDateToRegister == "") {
                retval = "-1";
                statusMessage("Please enter Registration End Date", "top-right", "error");
            } else if (regEndTime == "0") {
                retval = "-1";
                statusMessage("Please enter Registration End Time", "top-right", "error");
            } else if (venueName == "") {
                retval = "-1";
                statusMessage("Please enter Venue Name", "top-right", "error");
            } else if (venueAddr == "") {
                retval = "-1";
                statusMessage("Please enter Venue Address", "top-right", "error");
            }

            if (venueAddr.length > 128) {
                retval = "-1";
                statusMessage("Venue Address could not be more than 128 character.", "top-right", "error");
            }

            if (startTime == endTime) {
                retval = "-1";
                statusMessage("Start Time and End Time could not be same.", "top-right", "error");
            }

            return retval;
        }

        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        //function statusMessage(message) {
        //    $().toastmessage('showToast', {
        //        text: message,
        //        sticky: true,
        //        position: 'top-right',
        //        type: 'error',
        //        close: function () { console.log("toast is closed ..."); }
        //    });
        //}

        $(document).on("click", "#btnSave", function (e) {



            if (validateEvent() == 1) {
                if ($("#hdnFreeEventID").val() == "0") {
                    duplictateCheck();
                } else {
                    var existingEventType = $("#hdnEventType").val();
                    var newEventType = $("#selEventType").val();
                    //if (existingEventType == newEventType) {
                    postNewEvent();
                    // } else {
                    // validatePaidToBoth($("#hdnFreeEventID").val());
                    // }
                }
            }

        });

        $(document).on("keyup", "#txtVenueAddr", function (e) {

            if ($(this).length == 128) {
                statusMessage("Venue Address could not be more than 128 character.", "top-right", "error");
            }

        });

        function postNewEvent() {

            var year = $("#selyear").val();
            var eventCode = $("#txtEventCode").val();
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var startDate = $("#txtStartDate").val();
            var startTime = $("#txtStartTime").val();
            var endTime = $("#txtEndTime").val();
            var venueName = $("#txtVenueName").val();
            var venueAddr = $("#txtVenueAddr").val();
            var eventDesc = $("#txtEventDescription").val();
            var eventEndDate = $("#txtEndDate").val();
            var eventDateEndTime = $("#selEnDateEndTime").val();
            var maxReg = $("#txtRegCount").val();
            var SB = null;
            var VB = null;
            var MB = null;
            var GB = null;
            var EW = null;
            var PS = null;
            var BB = null;
            var SC = null;
            var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;

            var regStartDate = $("#txtRegStartDate").val();
            var regStartTime = $("#selRegEndTime").val();
            var eventType = $("#selEventType").val();
            var adultFee = $("#txtAdultFee").val();
            var childFee = $("#txtChildFee").val();
            var partType = $("#selPartType").val();
            var discount = $("#txtDiscount").val();

            if (arrContest.indexOf("8") >= 0) {
                SB = "Y";
            }
            if (arrContest.indexOf("9") >= 0) {


                VB = "Y";
            }
            if (arrContest.indexOf("10") >= 0) {
                MB = "Y";
            }
            if (arrContest.indexOf("11") >= 0) {
                GB = "Y";
            }
            if (arrContest.indexOf("12") >= 0) {
                EW = "Y";
            }
            if (arrContest.indexOf("13") >= 0) {
                PS = "Y";
            }
            if (arrContest.indexOf("14") >= 0) {
                BB = "Y";
            }
            if (arrContest.indexOf("26") >= 0) {
                SC = "Y";
            }


            var jsonData = JSON.stringify({ FrEvent: { EventCode: eventCode, EventName: eventName, Chapter: chapterId, Year: year, EventDate: startDate, StartTime: startTime, EndTime: endTime, VenueName: venueName, VenueAddress: venueAddr, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, FreeEventID: $("#hdnFreeEventID").val(), CreatedBy: userId, EventDescription: eventDesc, lastDateToRegister: eventEndDate, RegEndTime: eventDateEndTime, MaxReg: maxReg, RegStartDate: regStartDate, RegStartTime: regStartTime, EventType: eventType, AdultFee: adultFee, ParticipantType: partType, ChildFee: childFee, Discount: discount } });

            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/PostNewEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var mode = parseInt($("#hdnFreeEventID").val());
                    var msg = "";
                    if (mode > 0) {
                        msg = "Changes to Event were saved";
                    } else {
                        msg = "Event Created";
                    }
                    $("#hdnFreeEventID").val("0")
                    var jsonObj = $.parseJSON(JSON.stringify(data.d));

                    var retVal = jsonObj[0].Retval;
                    var exception = jsonObj[0].Exception;

                    if (parseInt(retVal) > 0) {
                        statusMessage(msg, "top-center", "success");

                        arrContest = [];
                        listFreeEvents(0);
                        hideLoader();
                        reset();
                    } else {
                        if (parseInt(retVal) == -2) {
                            statusMessage("Start Time should be greater than current time", "top-right", "error");
                        } else if (parseInt(retVal) == -1) {
                            statusMessage("Operation failed. " + exception + ".", "top-right", "error");
                        } else if (parseInt(retVal) == -3) {
                            statusMessage("End Time should be greater than start time", "top-right", "error");
                        } else if (parseInt(retVal) == -4) {
                            statusMessage(" Registration End Date should be less than Event Start Date and greater than Current Date", "top-right", "error");
                        } else if (parseInt(retVal) == -5) {
                            statusMessage(" Registration End Date and Time should be greater than Registration Start Date and Time", "top-right", "error");
                        } else if (parseInt(retVal) == -6) {
                            statusMessage(" Registration Start Date should be less than Event Start Date.", "top-right", "error");
                        }
                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }

            });

        }


        function duplictateCheck() {
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var year = $("#selyear").val();
            var eventDate = $("#txtStartDate").val();
            var eventEndDate = $("#txtEndDate").val();
            var eventDateEndTime = $("#selEnDateEndTime").val();

            var jsonData = JSON.stringify({ FrEvent: { EventCode: eventCode, EventName: eventName, Chapter: chapterId, Year: year, EventDate: eventDate, lastDateToRegister: eventEndDate, RegEndTime: eventDateEndTime } });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/IsDuplicateExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {

                        statusMessage("Event already exists.");
                        hideLoader();
                    } else {
                        postNewEvent();

                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", ".active", function (e) {

            var freeEventid = $(this).attr("attr-FreeEventID");
            getEventName(freeEventid, "active");

        });
        $(document).on("click", ".inActive", function (e) {
            var freeEventid = $(this).attr("attr-FreeEventID");
            getEventName(freeEventid, "inactive");

        });

        function setEventActiveInActive(freeEventid, status) {

            var jsonData = JSON.stringify({ FrEvent: { Status: status, FreeEventID: freeEventid } });
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/SetActieInActiveFreeEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (JSON.stringify(data.d) == 1) {

                        statusMessage("Updated successfully.", "top-center", "success");


                        listFreeEvents(0);
                    } else {
                    }
                }
            });
        }



        function listFreeEvents(freeEventID) {
            var year = $("#selyear").val();
            var source = "E";
            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID, Source: source });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                    var roleId = document.getElementById("<%=hdnRoleID.ClientID%>").value;

                    if (freeEventID == 0) {
                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Ser#</th>";
                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Chapter</th>";
                        //tblHtml += "<th>Event Code</th>";
                        tblHtml += " <th>Event Name</th>";
                        tblHtml += " <th>Event Description</th>";
                        tblHtml += " <th>Event Type</th>";
                        tblHtml += " <th>Participant Type</th>";
                        tblHtml += " <th>Adult Fee($)</th>";
                        tblHtml += " <th>Child Fee($)</th>";
                        tblHtml += " <th>Discount($)</th>";
                        tblHtml += " <th>Max Reg Count</th>";
                        tblHtml += "<th>Event Date</th>";
                        tblHtml += "<th>Start Time</th>";
                        tblHtml += "<th>End Time</th>";
                        tblHtml += "<th>Reg Start Date</th>";
                        tblHtml += "<th>Reg Start Time</th>";
                        tblHtml += "<th>Reg End Date</th>";
                        tblHtml += "<th>Reg End Time</th>";
                        tblHtml += "<th style='width:150px;'>Status</th>";
                        tblHtml += "<th>Venue</th>";
                        tblHtml += "<th>Venue Address</th>";

                        tblHtml += "<th>SB</th>";
                        tblHtml += "<th>VB</th>";
                        tblHtml += "<th>MB</th>";
                        tblHtml += "<th>SC</th>";
                        tblHtml += "<th>GB</th>";
                        tblHtml += "<th>EW</th>";
                        tblHtml += "<th>PS</th>";
                        tblHtml += "<th>BB</th>";

                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        var i = 0;
                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {

                                i++;
                                tblHtml += "<tr>";

                                tblHtml += "<tr>";
                                tblHtml += "<td>" + (i) + "</td>";
                                var color = "blue";
                                var classStr = "modify";
                                var deleteStrt = "delete";
                                var cursor = "pointer";
                                var deleteColor = "red";
                                var activeStr = "active";
                                var activeTitle = "Active";
                                var activeColor = "blue";

                                if (value.Status == "" || value.Status == "Inactive") {
                                    activeStr = "active";
                                    activeTitle = "Active";

                                } else {
                                    activeStr = "inActive";
                                    activeTitle = "Inactive";
                                }

                                if (value.CreatedBy == userId || (roleId == 1 || roleId == 2 || roleId != 5)) {


                                } else {
                                    color = "Grey";
                                    classStr = "modifyDisabled";
                                    deleteStrt = "deleteDisabled";
                                    cursor = "normal";
                                    deleteColor = "Grey";
                                    activeStr = "activeDisabled";
                                    activeColor = "grey";
                                }
                                if (value.IsActive == "No") {
                                    activeTitle = "Inactive";
                                    activeColor = "grey";
                                    activeStr = "inActive";
                                }
                                var adultFee = (value.AdultFee == "0" ? "" : value.AdultFee);
                                var childFee = (value.ChildFee == "0" ? "" : value.ChildFee);
                                var discount = (value.Discount == "0" ? "" : value.Discount);

                                tblHtml += '<td ><div style="width:100px;"><div style="float:left;"><a attr-FreeEventID=' + value.FreeEventID + ' class=' + classStr + ' title="Modify" style="cursor:' + cursor + ';"><i style="color:' + color + ';" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div><div style="float:left; position:relative; left:10px;"><a style="cursor:' + cursor + ';" attr-FreeEventID=' + value.FreeEventID + ' class=' + deleteStrt + ' title="Delete"><i class="fa fa-trash-o fa-2x" style="color:' + deleteColor + ';" aria-hidden="true"></i></a></div> <div style="float:left; position:relative; left:22px;"><a style="cursor:pointer;" attr-FreeEventID=' + value.FreeEventID + ' class=' + activeStr + ' title=' + activeTitle + '><i style="color:' + activeColor + '"; class="fa fa-power-off fa-2x" aria-hidden="true"></i></a></div></div></td>';

                                tblHtml += "<td>" + value.ChapterName + "</td>";
                                //tblHtml += "<td>" + value.EventCode + "</td>";
                                tblHtml += "<td>" + value.EventName + "</td>";
                                $("#hdnFreeEventname").val(value.EventName);
                                tblHtml += "<td>" + value.EventDescription + "</td>";

                                tblHtml += "<td>" + (value.EventType) + "</td>";
                                tblHtml += "<td>" + (value.ParticipantType) + "</td>";
                                tblHtml += "<td>" + adultFee + "</td>";
                                tblHtml += "<td>" + childFee + "</td>";
                                tblHtml += "<td>" + discount + "</td>";
                                tblHtml += "<td>" + value.MaxReg + "</td>";
                                tblHtml += "<td>" + value.EventDate + "</td>";
                                tblHtml += "<td>" + value.StartTime + "</td>";
                                tblHtml += "<td>" + value.EndTime + "</td>";
                                tblHtml += "<td>" + value.RegStartDate + "</td>";
                                tblHtml += "<td>" + value.RegStartTime + "</td>";
                                tblHtml += "<td>" + value.lastDateToRegister + "</td>";
                                tblHtml += "<td>" + value.RegEndTime + "</td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.Status + "</span></td>";
                                tblHtml += "<td>" + value.VenueName + "</td>";
                                tblHtml += "<td>" + value.VenueAddress + "</td>";

                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.SB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.VB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.MB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.SC + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.GB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.EW + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.PS + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.BB + "</span></td>";


                                tblHtml += "</tr>";

                            });
                            hideLoader();

                        } else {

                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='19' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";
                            hideLoader();
                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);
                    } else {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selChapter").val(value.Chapter);
                                $("#selyear").val(value.Year);

                                $("#txtEventCode").val(value.EventCode);

                                $("#txtEventName").val(value.EventName);
                                $("#txtStartDate").val(value.EventDate);
                                $("#txtStartTime").val(value.StrStartTime);
                                $("#txtEndTime").val(value.StrEndTime);
                                $("#txtVenueName").val(value.VenueName);
                                $("#txtVenueAddr").val(value.VenueAddress);
                                $("#txtEventDescription").val(value.EventDescription);

                                $("#txtRegStartDate").val(value.RegStartDate)
                                $("#selRegEndTime").val(value.StrRegStartTime);

                                $("#txtEndDate").val(value.lastDateToRegister);
                                $("#selEnDateEndTime").val(value.StrRegEndTime);
                                $("#txtRegCount").val(value.MaxReg);
                                $("#selEventType").val(value.EventType == "" ? "0" : value.EventType);
                                $("#selPartType").val(value.ParticipantType == "" ? "0" : value.ParticipantType)
                                var eventType = $("#selEventType").val();
                                var partType = $("#selPartType").val();
                                $("#hdnEventType").val(eventType);

                                if (eventType == "Paid" && partType == "Adult") {
                                    $("#dvAdultFee").show();
                                    $("#dvChildFee").hide();
                                    $("#txtAdultFee").val(value.AdultFee);
                                    $("#txtDiscount").val(value.Discount == "0" ? "" : value.Discount);
                                    $("#dvDiscountFee").show();
                                } else if (eventType == "Paid" && partType == "Child") {
                                    $("#dvAdultFee").hide();
                                    $("#dvChildFee").show();
                                    $("#dvDiscountFee").show();
                                    $("#txtChildFee").val(value.ChildFee);
                                    $("#txtDiscount").val(value.Discount == "0" ? "" : value.Discount);

                                } else if (eventType == "Paid" && partType == "Both") {
                                    $("#dvAdultFee").show();
                                    $("#dvChildFee").show();
                                    $("#dvDiscountFee").show();
                                    $("#txtChildFee").val(value.ChildFee);
                                    $("#txtAdultFee").val(value.AdultFee);
                                    $("#txtDiscount").val(value.Discount == "0" ? "" : value.Discount);
                                } else {
                                    $("#dvAdultFee").hide();
                                    $("#dvChildFee").hide();
                                    $("#dvDiscountFee").hide();
                                }

                                if (value.SB == "Y") {

                                    $("#rbtn8").prop("checked", true);
                                    arrContest.push($("#rbtn8").val());
                                }
                                if (value.VB == "Y") {

                                    $("#rbtn9").prop("checked", true);
                                    arrContest.push($("#rbtn9").val());
                                }
                                if (value.MB == "Y") {

                                    $("#rbtn10").prop("checked", true);
                                    arrContest.push($("#rbtn10").val());
                                }
                                if (value.SC == "Y") {

                                    $("#rbtn26").prop("checked", true);
                                    arrContest.push($("#rbtn26").val());
                                }
                                if (value.GB == "Y") {

                                    $("#rbtn11").prop("checked", true);
                                    arrContest.push($("#rbtn11").val());
                                }
                                if (value.EW == "Y") {

                                    $("#rbtn12").prop("checked", true);
                                    arrContest.push($("#rbtn12").val());
                                }
                                if (value.PS == "Y") {

                                    $("#rbtn13").prop("checked", true);
                                    arrContest.push($("#rbtn13").val());
                                }
                                if (value.BB == "Y") {

                                    $("#rbtn14").prop("checked", true);
                                    arrContest.push($("#rbtn14").val());
                                }

                            });
                            hideLoader();
                        } else {
                            hideLoader();
                        }
                    }
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function reset() {
            $("#txtEventCode").val("");
            $("#txtEventCode").val("");
            $("#txtEventName").val("");
            $("#txtStartDate").val("");
            $("#txtStartTime").val("");
            $("#txtEndTime").val("");
            $("#txtVenueName").val("");
            $("#txtVenueAddr").val("");
            $("#txtEventDescription").val("");
            $("#txtEndDate").val("");
            $("#selEnDateEndTime").val("0");
            $("#txtRegCount").val("");
            $("#txtRegStartDate").val("")
            $("#selRegEndTime").val("0");
            $("#selEventType").val("0");
            $("#txtAdultFee").val("");
            arrContest = [];
            $(".rbtnContests").removeAttr("checked");

            $("#hdnFreeEventID").val("");
            //  listChapter();
        }

        $(document).on("click", "#btnReset", function (e) {

            reset();
        });

        $(document).on("click", ".modify", function (e) {
            var freeEventID = $(this).attr("attr-FreeEventID");
            $("#hdnFreeEventID").val(freeEventID);
            listFreeEvents(freeEventID);
        });

        $(document).on("click", ".delete", function (e) {
            var freeEventID = $(this).attr("attr-FreeEventID");
            $("#hdnFreeEventID").val(freeEventID);
            if (confirm("Are you sure want to delete?")) {
                validateRegistration(freeEventID);
            }
        });

        function deleteFreeEvent(freeEventID) {
            var jsonData = JSON.stringify({ FreeEventID: freeEventID });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/DeleteFreeEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {
                        statusMessage("Deleted successfully", "top-center", "success");

                        hideLoader();
                        listFreeEvents(0);

                    } else {
                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function validateTime(time) {

            var regexp = /([01][0-9]|[02][0-3]):[0-5][0-9]/;
            var correct = regexp.test(time);
            return correct;
        }

        function validateRegistration(freeEventID) {

            var jsonData = JSON.stringify({ FreeEventID: freeEventID });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ValidateRegistration",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) <= 0) {

                        deleteFreeEvent(freeEventID);


                    } else {
                        statusMessage("This event cannot be deleted since people have already registered.", "top-center", "error");

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }

        $(document).on("keypress", "#txtRegCount", function (e) {

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message

                return false;
            }
        });

        $(document).on("change", "#selEventType", function (e) {
            var type = $(this).val();
            var partType = $('#selPartType').val();
            showHideFee(type, partType);

        });
        $(document).on("change", "#selPartType", function (e) {
            var partType = $(this).val();
            var type = $('#selEventType').val();
            showHideFee(type, partType);

        });

        function showHideFee(type, partType) {
            if (type != "0" && partType != "0") {
                if (type == "Free") {
                    $("#dvAdultFee").hide();
                    $("#dvChildFee").hide();
                    $("#dvDiscountFee").hide();
                } else if (type == "Paid") {

                    if (partType == "Adult") {
                        $("#dvAdultFee").show();
                        $("#dvChildFee").hide();
                    } else if (partType == "Child") {
                        $("#dvAdultFee").hide();
                        $("#dvChildFee").show();
                    } else if (partType == "Both") {
                        $("#dvAdultFee").show();
                        $("#dvChildFee").show();
                    }

                    $("#dvDiscountFee").show();
                }
            }
        }

        function getEventName(freeEventId, active) {



            var jsonData = JSON.stringify({ FreeEventId: freeEventId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/GetEventName",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var eventName = JSON.stringify(data.d);
                    eventName = JSON.parse(eventName);

                    if (active == "active") {
                        if (confirm("Are you sure to activate the event " + eventName + "?")) {
                            setEventActiveInActive(freeEventId, "Active");
                        }
                    } else {
                        if (confirm("Are you sure to deactivate the event " + eventName + "?")) {
                            setEventActiveInActive(freeEventId, "Inactive");
                        }
                    }

                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function validatePaidToBoth(freeEventID) {

            var jsonData = JSON.stringify({ FreeEventID: freeEventID });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ValidateRegistration",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) <= 0) {

                        postNewEvent();


                    } else {
                        statusMessage("This event cannot be modifeied from Paid to Free or Free to Paid since people have already registered.", "top-center", "error");

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }

    </script>

    <div class="container">
        <div style="float: left;">

            <a href="../VolunteerFunctions.aspx">Back to Volunteer Functions</a>
        </div>
        <div class="page-header">
            <center>
                <h3 style="color: #00a0b0; font-weight: bold; font-family: 'Trebuchet MS'">Set Up a Free Event</h3>

            </center>
        </div>
        <div class="bs-example" style="width: 80%; margin: auto;">



            <form role="form" class="form-horizontal">
                <div>
                    <div class="well well-sm" style="float: right; color: red;"><strong>* fields are mandatory</strong></div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Chapter<sup class="man">*</sup></label>
                        <div class="col-md-8">

                            <select id="selChapter" class="form-control" required title="Select Chapter">
                            </select>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Year<sup class="man">*</sup></label>
                        <div class="col-md-8">

                            <select id="selyear" class="form-control" required title="Select Year">
                                <option value="0">Select</option>
                                <%--  <option value="2017" selected="selected">2017</option>
                                <option value="2016">2016</option>--%>
                            </select>
                        </div>

                    </div>


                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Code<sup class="man">*</sup></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="InputName" id="txtEventCode" placeholder="Enter Event Code" required title="Enter Event Code">
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Name<sup class="man">*</sup></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="InputName" id="txtEventName" placeholder="Enter Event Name" title="Enter Event Name" required style="width: 375px;">
                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Description</label>
                        <div class="col-md-8">
                            <textarea name="InputMessage" id="txtEventDescription" title="Event Description" class="form-control" rows="2" style="width: 375px;"></textarea>
                        </div>

                    </div>


                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Participant Type<sup class="man">*</sup></label>
                        <div class="col-md-8">

                            <select id="selPartType" class="form-control" required title="Select Event Type">
                                <option value="0">Select</option>
                                <option value="Adult">Adult</option>
                                <option value="Child">Child</option>
                                <option value="Both">Both</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Type<sup class="man">*</sup></label>
                        <div class="col-md-8">

                            <select id="selEventType" class="form-control" required title="Select Event Type">
                                <option value="0">Select</option>
                                <option value="Free">Free</option>
                                <option value="Paid">Paid</option>

                            </select>
                        </div>

                    </div>



                    <div class="form-group col-md-6 clear" id="dvAdultFee" style="display: none;">
                        <label for="InputName" class="control-label col-md-4" id="lblEventFee">Adult Fee<sup class="man">*</sup></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="InputName" id="txtAdultFee" placeholder="Enter Event Fee" title="Enter Event Fee" required>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear" id="dvChildFee" style="display: none;">
                        <label for="InputName" class="control-label col-md-4" id="lblChildEventFee">Child Fee<sup class="man">*</sup></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="InputName" id="txtChildFee" placeholder="Enter Event Fee" title="Enter Event Fee" required>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear" id="dvDiscountFee" style="display: none;">
                        <label for="InputName" class="control-label col-md-4" id="lblDiscount">Discount Fee</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="InputName" id="txtDiscount" placeholder="Enter Discount Fee" title="Enter Event Fee" required>
                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Max seats<sup class="man">*</sup> </label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" title="Enter Max Seats" name="InputName" id="txtRegCount" placeholder="Registration Count" required>
                        </div>

                    </div>


                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Date<sup class="man">*</sup></label>
                        <div class="col-md-7 input-group input-append date" style="padding-left: 15px;" id="dateRangePicker">
                            <input type="text" class="form-control" name="date" id="txtStartDate" placeholder="Enter Event Date" title="Enter Event Date">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">Start Time<sup class="man">*</sup></label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="txtStartTime" class="form-control" required title="Select Start Time">
                                <option value="0">Select</option>
                                <option value="06:00">6:00 AM</option>
                                <option value="06:30">6:30 AM</option>
                                <option value="07:00">7:00 AM</option>
                                <option value="07:30">7:30 AM</option>
                                <option value="08:00">8:00 AM</option>
                                <option value="08:30">8:30 AM</option>
                                <option value="09:00">9:00 AM</option>
                                <option value="09:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>

                            </select>


                        </div>

                    </div>



                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">End Time<sup class="man">*</sup></label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="txtEndTime" class="form-control" required title="Select End Time">
                                <option value="0">Select</option>
                                <option value="06:00">6:00 AM</option>
                                <option value="06:30">6:30 AM</option>
                                <option value="07:00">7:00 AM</option>
                                <option value="07:30">7:30 AM</option>
                                <option value="08:00">8:00 AM</option>
                                <option value="08:30">8:30 AM</option>
                                <option value="09:00">9:00 AM</option>
                                <option value="09:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>
                            </select>

                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Registration Start Date<sup class="man">*</sup></label>
                        <div class="col-md-7 input-group input-append date" style="padding-left: 15px;" id="dvRegStartDate">
                            <input type="text" class="form-control" name="date" id="txtRegStartDate" placeholder="Enter Registration Start Date" title="Enter Registration Start Date">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>

                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">Start Time<sup class="man">*</sup></label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="selRegEndTime" class="form-control" required title="Select Registration Start Time">
                                <option value="0">Select</option>
                                <option value="00:00">12:00 AM</option>
                                <option value="00:30">12:30 AM</option>
                                <option value="01:00">1:00 AM</option>
                                <option value="01:30">1:30 AM</option>
                                <option value="02:00">2:00 AM</option>
                                <option value="02:30">2:30 AM</option>
                                <option value="03:00">3:00 AM</option>
                                <option value="03:30">3:30 AM</option>
                                <option value="04:00">4:00 AM</option>
                                <option value="04:30">4:30 AM</option>
                                <option value="05:00">5:00 AM</option>
                                <option value="05:30">5:30 AM</option>
                                <option value="06:00">6:00 AM</option>
                                <option value="06:30">6:30 AM</option>
                                <option value="07:00">7:00 AM</option>
                                <option value="07:30">7:30 AM</option>
                                <option value="08:00">8:00 AM</option>
                                <option value="08:30">8:30 AM</option>
                                <option value="09:00">9:00 AM</option>
                                <option value="09:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>

                            </select>


                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Registration End Date<sup class="man">*</sup></label>
                        <div class="col-md-7 input-group input-append date" style="padding-left: 15px;" id="dvEndDate">
                            <input type="text" class="form-control" name="date" id="txtEndDate" placeholder="Enter End Date" title="Enter Registration End Date">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">End Time<sup class="man">*</sup></label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="selEnDateEndTime" class="form-control" required title="Select End Time">
                                <option value="0">Select</option>
                                <option value="06:00">6:00 AM</option>
                                <option value="06:30">6:30 AM</option>
                                <option value="07:00">7:00 AM</option>
                                <option value="07:30">7:30 AM</option>
                                <option value="08:00">8:00 AM</option>
                                <option value="08:30">8:30 AM</option>
                                <option value="09:00">9:00 AM</option>
                                <option value="09:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>

                            </select>


                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-xs-4">Venue name<sup class="man">*</sup></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="InputName" id="txtVenueName" placeholder="Enter Venue Name" title="Enter Venue Name" required style="width: 375px;">
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-xs-4">Venue Address<sup class="man">*</sup></label>
                        <div class="col-xs-8">
                            <textarea name="InputMessage" id="txtVenueAddr" class="form-control" rows="3" style="width: 375px;" title="Enter Venue Address" required></textarea>

                            <%-- <input type="text" class="form-control" maxlength="128" name="InputName" id="txtVenueAddr" placeholder="Enter Venue Address" required style="width: 375px;">--%>
                        </div>


                    </div>
                    <div class="form-group col-md-6 clear">
                        <div class="col-md-8">
                            <label for="InputName" class="control-label">Contests Offered</label>
                        </div>
                    </div>

                    <div id="dvProductGroup" class="clear">
                    </div>

                    <div class="clear" style="margin-bottom: 10px;"></div>
                    <div class="form-group col-md-6 clear">
                        <div class="col-md-8">
                            <input type="button" id="btnSave" value="Save" class="btn btn-info" name="submit" />
                            <input type="button" id="btnReset" value="Reset" class="btn btn-info" name="Reset" />
                            <%--   <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info">--%>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-lg-5 col-md-push-1" style="display: none;">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <strong><span class="glyphicon glyphicon-ok"></span>Success! Message sent.</strong>
                    </div>
                    <div class="alert alert-danger">
                        <span class="glyphicon glyphicon-remove"></span><strong>Error! Please check all page inputs.</strong>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear" style="margin-bottom: 10px;"></div>
        <center><b>Table 1: Free Event</b></center>
        <div style="width: 1150px; overflow-x: scroll;">
            <table id="table" style="width: 100%; overflow-x: scroll;">
                <%--  <thead>
                <tr>
                    <th>Action</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Class Stype</th>
                    <th>Week#</th>
                    <th>Status</th>
                    <th>Makeup</th>
                    <th>Substitute</th>
                    <th>Reason</th>
                    <th>HwRelDate</th>
                    <th>HWDueDate</th>
                    <th>SRelDate</th>
                    <th>ARelDate</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <button id="Button1" style="padding: 5px; font-size: 16px; cursor: pointer;">Modify</button></td>
                    <td>03/25/2017</td>
                    <td>6:00 PM</td>
                    <td>Regular</td>
                    <td>1</td>
                    <td>On</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>--%>
            </table>
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
            <div class="sk-fading-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
        <div id="overlay"></div>
        <input type="hidden" id="hdnFreeEventID" value="0" />
        <input type="hidden" id="hdnFreeEventname" value="0" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
        <input type="hidden" value="0" id="hdnEventType" runat="server" />
</asp:Content>
