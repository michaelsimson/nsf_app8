﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Data;

public partial class FreeEvent_FreeEvent : System.Web.UI.Page
{

    public class FreeEvent
    {

        public string FreeEventID { get; set; }
        public int Year { get; set; }
        public string Chapter { get; set; }
        public string ChapterName { get; set; }
        public string EventCode { get; set; }
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public string StartTime { get; set; }
        public string lastDateToRegister { get; set; }
        public string RegStartDate { get; set; }
        public string RegStartTime { get; set; }
        public string StrRegStartTime { get; set; }
        public string RegEndTime { get; set; }
        public string StrRegEndTime { get; set; }
        public string EndTime { get; set; }

        public string StrStartTime { get; set; }
        public string StrEndTime { get; set; }

        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public string SB { get; set; }
        public string VB { get; set; }
        public string MB { get; set; }
        public string SC { get; set; }
        public string GB { get; set; }
        public string EW { get; set; }
        public string PS { get; set; }
        public string BB { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string EventDescription { get; set; }
        public string Day { get; set; }
        public string StrDate { get; set; }
        public string Status { get; set; }
        public string MaxReg { get; set; }
        public string Exception { get; set; }
        public int Retval { get; set; }
        public string EventType { get; set; }
        public string AdultFee { get; set; }
        public string ChildFee { get; set; }
        public string Discount { get; set; }
        public string ParticipantType { get; set; }
        public string IsActive { get; set; }
    }

    public class Chapter
    {
        public int ChapterID { get; set; }
        public string ChapterName { get; set; }
        public string Exception { get; set; }
    }
    public class ProductGroup
    {
        public int ProductGroupID { get; set; }
        public string ProductGroupCode { get; set; }
        public string ProductGroupName { get; set; }
        public int EventID { get; set; }
        public string Exception { get; set; }
    }
    public class Product
    {
        public int ProductID { get; set; }
        public int ProductGroupID { get; set; }
        public string ProdcutCode { get; set; }
        public string ProductName { get; set; }
        public int EventID { get; set; }
        public string Exception { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {
                hdnLoginID.Value = Session["LoginID"].ToString();
                hdnRoleID.Value = Session["RoleID"].ToString();
            }
        }
    }

    [WebMethod]
    public static List<Chapter> ListChapter(Chapter Chapter)
    {

        int retVal = -1;
        List<Chapter> ObjListChapter = new List<Chapter>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select ChapterId, ChapterCode from Chapter ";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Chapter ObjChapt = new Chapter();

                        ObjChapt.ChapterID = Convert.ToInt32(dr["ChapterID"].ToString());
                        ObjChapt.ChapterName = dr["ChapterCode"].ToString();
                        ObjListChapter.Add(ObjChapt);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return ObjListChapter;

    }

    [WebMethod]
    public static List<ProductGroup> ListProductGroup(ProductGroup ProductGroup)
    {

        int retVal = -1;
        List<ProductGroup> ObjListPGroup = new List<ProductGroup>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select ProductGroupID,Name, case when Name='Spelling' then '1. Spelling' when Name='Vocabulary' then '2. Vocabulary' when Name='Math' then '3. Math' when Name='Science' then '4. Science' when Name='Geography' then '5. Geography' when Name='Essay Writing' then '6. Essay Writing' when Name='Public Speaking' then '7. Public Speaking' when Name='Brain Bee' then '8. Brain Bee' else Name end as PName from ProductGroup where EventID=" + ProductGroup.EventID + " order by PName";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroup objPGroup = new ProductGroup();
                        objPGroup.ProductGroupID = Convert.ToInt32(dr["ProductGroupId"].ToString());
                        objPGroup.ProductGroupName = dr["Name"].ToString();
                        ObjListPGroup.Add(objPGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return ObjListPGroup;

    }

    [WebMethod]
    public static List<Product> ListProduct(Product Product)
    {

        int retVal = -1;
        List<Product> ObjListProduct = new List<Product>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select productId, name from product where eventId=" + Product.EventID + " and ProductGroupId=" + Product.ProductGroupID + " order by Name";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Product objPGroup = new Product();
                        objPGroup.ProductID = Convert.ToInt32(dr["ProductID"].ToString());
                        objPGroup.ProductName = dr["Name"].ToString();
                        ObjListProduct.Add(objPGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return ObjListProduct;

    }

    [WebMethod]
    public static int IsDuplicateExists(FreeEvent FrEvent)
    {

        int retVal = -1;
        int Count = 0;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select count(*) as EventCount from FreeEvent where EventDate='" + FrEvent.EventDate + "' and  ChapterID=" + FrEvent.Chapter + " and year=" + FrEvent.Year + "";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Count = Convert.ToInt32(ds.Tables[0].Rows[0]["EventCount"].ToString());
                    retVal = Count;
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return retVal;
    }

    [WebMethod]
    public static List<FreeEvent> PostNewEvent(FreeEvent FrEvent)
    {

        int retVal = -1;
        List<FreeEvent> LstEvent = new List<FreeEvent>();
        FreeEvent ObjEvent = new FreeEvent();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            SqlCommand cmd;

            string EventDate = Convert.ToDateTime(FrEvent.EventDate).ToShortDateString();
            DateTime dtEventDate = Convert.ToDateTime(EventDate);
            string Today = Convert.ToDateTime(DateTime.Now).ToShortDateString();
            DateTime dtTodayDate = Convert.ToDateTime(Today);
            string valid = "true";
            string EventEndDate = Convert.ToDateTime(FrEvent.lastDateToRegister).ToShortDateString();
            DateTime dtEventEndDate = Convert.ToDateTime(EventEndDate);

            string RegStartDateTime = FrEvent.RegStartDate + " " + FrEvent.RegStartTime;
            DateTime DtRegStartDateTime = Convert.ToDateTime(RegStartDateTime);
            string RegEndDateTime = FrEvent.lastDateToRegister + " " + FrEvent.RegEndTime;
            DateTime DtRegEndDateTime = Convert.ToDateTime(RegEndDateTime);

            if (dtEventDate >= dtTodayDate)
            {
                string startTime = FrEvent.EventDate + " " + FrEvent.StartTime;
                DateTime dtStartTime = Convert.ToDateTime(startTime);
                string endTime = FrEvent.EventDate + " " + FrEvent.EndTime;
                DateTime dtEndTime = Convert.ToDateTime(endTime);

                string Current = Convert.ToDateTime(DateTime.Now).ToString();
                DateTime dtCurrent = Convert.ToDateTime(Current);

                if (dtStartTime > dtCurrent && dtEndTime > dtStartTime)
                {
                    valid = "true";
                }
                else
                {
                    if (dtStartTime <= dtCurrent)
                    {
                        ObjEvent.Retval = -2;
                        LstEvent.Add(ObjEvent);

                    }
                    if (dtEndTime < dtStartTime)
                    {
                        ObjEvent.Retval = -3;
                        LstEvent.Add(ObjEvent);
                    }
                    valid = "false";
                }
            }
            if (DtRegEndDateTime <= DtRegStartDateTime)
            {
                ObjEvent.Retval = -5;
                LstEvent.Add(ObjEvent);
                valid = "false";
            }
            if (DtRegStartDateTime > dtEventDate)
            {
                ObjEvent.Retval = -6;
                LstEvent.Add(ObjEvent);
                valid = "false";
            }

            //string valid = "true";
            //if (valid == "true")

            if (dtEventDate >= dtTodayDate && valid == "true")

            {
                //FrEvent.EventDate = "06/06/2018";
                //FrEvent.StartTime = "10:00:00";
                //FrEvent.EndTime = "10:00:00";
                //FrEvent.RegStartDate = "06/06/2018";
                //FrEvent.lastDateToRegister = "06/06/2018";

                if (Convert.ToInt32(FrEvent.FreeEventID) == 0)
                {

                    cmdText = "insert into FreeEvent ( Year,ChapterID,EventCode,EventName,EventDate,StartTime,EndTime,VenueName,VenueAddress,SB,VB,MB,SC, GB, EW, PS,BB, CreatedDate, CreatedBy, EventDescription, lastDateToRegister, RegEndTime,  MaxReg, RegStartDate, RegStartTime, EventType, AdultFee, ChildFee, Discount, ParticipantType) values(@Year,@Chapter,@EventCode,@EventName,@EventDate,@StartTime,@EndTime,@VenueName,@VenueAddress,@SB,@VB,@MB,@SC,@GB,@EW,@PS,@BB,@CreatedDate,@CreatedBy,@EventDescription, @lastDateToRegister, @RegEndTime,  @MaxReg, @RegStartDate, @RegStartTime, @EventType, @AdultFee,@ChildFee, @Discount, @ParticipantType )";



                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEvent.Year);
                    cmd.Parameters.AddWithValue("@Chapter", FrEvent.Chapter);
                    cmd.Parameters.AddWithValue("@EventCode", FrEvent.EventCode);
                    cmd.Parameters.AddWithValue("@EventName", FrEvent.EventName);
                    cmd.Parameters.AddWithValue("@EventDate", FrEvent.EventDate);
                    cmd.Parameters.AddWithValue("@StartTime", FrEvent.StartTime);
                    cmd.Parameters.AddWithValue("@EndTime", FrEvent.EndTime);
                    cmd.Parameters.AddWithValue("@VenueName", FrEvent.VenueName);
                    cmd.Parameters.AddWithValue("@VenueAddress", FrEvent.VenueAddress);
                    cmd.Parameters.AddWithValue("@SB", FrEvent.SB == null ? "" : FrEvent.SB);
                    cmd.Parameters.AddWithValue("@VB", FrEvent.VB == null ? "" : FrEvent.VB);
                    cmd.Parameters.AddWithValue("@MB", FrEvent.MB == null ? "" : FrEvent.MB);
                    cmd.Parameters.AddWithValue("@SC", FrEvent.SC == null ? "" : FrEvent.SC);
                    cmd.Parameters.AddWithValue("@GB", FrEvent.GB == null ? "" : FrEvent.GB);
                    cmd.Parameters.AddWithValue("@EW", FrEvent.EW == null ? "" : FrEvent.EW);
                    cmd.Parameters.AddWithValue("@PS", FrEvent.PS == null ? "" : FrEvent.PS);
                    cmd.Parameters.AddWithValue("@BB", FrEvent.BB == null ? "" : FrEvent.BB);

                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString());
                    cmd.Parameters.AddWithValue("@CreatedBy", FrEvent.CreatedBy);
                    cmd.Parameters.AddWithValue("@EventDescription", FrEvent.EventDescription);
                    cmd.Parameters.AddWithValue("@lastDateToRegister", FrEvent.lastDateToRegister);
                    cmd.Parameters.AddWithValue("@RegEndTime", FrEvent.RegEndTime);
                    cmd.Parameters.AddWithValue("@MaxReg", FrEvent.MaxReg);
                    cmd.Parameters.AddWithValue("@RegStartDate", FrEvent.RegStartDate);
                    cmd.Parameters.AddWithValue("@RegStartTime", FrEvent.RegStartTime);
                    cmd.Parameters.AddWithValue("@EventType", FrEvent.EventType);
                    cmd.Parameters.AddWithValue("@AdultFee", FrEvent.AdultFee);
                    cmd.Parameters.AddWithValue("@ChildFee", FrEvent.ChildFee);
                    cmd.Parameters.AddWithValue("@Discount", FrEvent.Discount);
                    cmd.Parameters.AddWithValue("@ParticipantType", FrEvent.ParticipantType);
                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        ObjEvent.Retval = 1;
                        LstEvent.Add(ObjEvent);
                    }

                }
                else
                {
                    string AdultFee = FrEvent.AdultFee;
                    string ChildFee = FrEvent.ChildFee;
                    string Discount = FrEvent.Discount;
                    if (FrEvent.EventType == "Paid")
                    {
                        if (FrEvent.ParticipantType == "Adult")
                        {
                            ChildFee = "";
                        }
                        if (FrEvent.ParticipantType == "Child")
                        {
                            AdultFee = "";
                        }
                    }
                    else
                    {
                        AdultFee = "";
                        ChildFee = "";
                        Discount = "";
                    }

                    cmdText = "Update FreeEvent set Year=@Year,ChapterID=@Chapter,EventCode=@EventCode,EventName=@EventName,EventDate=@EventDate,StartTime=@StartTime,EndTime=@EndTime,VenueName=@VenueName,VenueAddress=@VenueAddress,SB=@SB,VB=@VB,MB=@MB,SC=@SC, GB=@GB, EW=@EW, PS=@PS,BB=@BB, ModifiedDate=@CreatedDate, ModifiedBy=@CreatedBy, EventDescription= @EventDescription, lastDateToRegister=@lastDateToRegister, RegEndTime=@RegEndTime, MaxReg=@MaxReg, RegStartDate=@RegStartDate, RegStartTime=@RegStartTime, EventType=@EventType, AdultFee=@AdultFee, ChildFee=@ChildFee, Discount=@Discount, ParticipantType=@participantType where FreeEventID=@FreeEventID";
                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEvent.Year);
                    cmd.Parameters.AddWithValue("@Chapter", FrEvent.Chapter);
                    cmd.Parameters.AddWithValue("@EventCode", FrEvent.EventCode);
                    cmd.Parameters.AddWithValue("@EventName", FrEvent.EventName);
                    cmd.Parameters.AddWithValue("@EventDate", FrEvent.EventDate);
                    cmd.Parameters.AddWithValue("@StartTime", FrEvent.StartTime);
                    cmd.Parameters.AddWithValue("@EndTime", FrEvent.EndTime);
                    cmd.Parameters.AddWithValue("@VenueName", FrEvent.VenueName);
                    cmd.Parameters.AddWithValue("@VenueAddress", FrEvent.VenueAddress);
                    cmd.Parameters.AddWithValue("@SB", FrEvent.SB == null ? "" : FrEvent.SB);
                    cmd.Parameters.AddWithValue("@VB", FrEvent.VB == null ? "" : FrEvent.VB);
                    cmd.Parameters.AddWithValue("@MB", FrEvent.MB == null ? "" : FrEvent.MB);
                    cmd.Parameters.AddWithValue("@SC", FrEvent.SC == null ? "" : FrEvent.SC);
                    cmd.Parameters.AddWithValue("@GB", FrEvent.GB == null ? "" : FrEvent.GB);
                    cmd.Parameters.AddWithValue("@EW", FrEvent.EW == null ? "" : FrEvent.EW);
                    cmd.Parameters.AddWithValue("@PS", FrEvent.PS == null ? "" : FrEvent.PS);
                    cmd.Parameters.AddWithValue("@BB", FrEvent.BB == null ? "" : FrEvent.BB);

                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString());
                    cmd.Parameters.AddWithValue("@CreatedBy", FrEvent.CreatedBy);
                    cmd.Parameters.AddWithValue("@EventDescription", FrEvent.EventDescription);
                    cmd.Parameters.AddWithValue("@lastDateToRegister", FrEvent.lastDateToRegister);
                    cmd.Parameters.AddWithValue("@RegEndTime", FrEvent.RegEndTime);
                    cmd.Parameters.AddWithValue("@FreeEventID", FrEvent.FreeEventID);
                    cmd.Parameters.AddWithValue("@MaxReg", FrEvent.MaxReg);
                    cmd.Parameters.AddWithValue("@RegStartDate", FrEvent.RegStartDate);
                    cmd.Parameters.AddWithValue("@RegStartTime", FrEvent.RegStartTime);
                    cmd.Parameters.AddWithValue("@EventType", FrEvent.EventType);
                    cmd.Parameters.AddWithValue("@AdultFee", AdultFee);
                    cmd.Parameters.AddWithValue("@ChildFee", ChildFee);
                    cmd.Parameters.AddWithValue("@Discount", Discount);
                    cmd.Parameters.AddWithValue("@ParticipantType", FrEvent.ParticipantType);

                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        ObjEvent.Retval = 1;
                        LstEvent.Add(ObjEvent);
                    }
                }

            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
            ObjEvent.Retval = -1;
            ObjEvent.Exception = ex.Message;
            LstEvent.Add(ObjEvent);
        }
        return LstEvent;

    }


    [WebMethod]
    public static List<FreeEvent> ListFreeEvents(int Year, int FreeEventID, string Source)
    {

        int retVal = -1;
        List<FreeEvent> ObjListFreeEvent = new List<FreeEvent>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select FE.FreeEventID, FE.Year,FE.ChapterID,FE.EventCode,FE.EventName,FE.EventDate,FE.StartTime,FE.EndTime,FE.VenueName,FE.VenueAddress,FE.SB,FE.VB,FE.MB,FE.SC, FE.GB, FE.EW, FE.PS,FE.BB, C.Name, FE.EventDescription, FE.CreatedBy, FE.lastDateToRegister, FE.RegEndTime, FE.Status, FE.maxReg, FE.RegStartDate, FE.RegStartTime, FE.EventType, FE.AdultFee, FE.ChildFee, FE.ParticipantType, FE.Discount from FreeEvent FE inner join Chapter C on (FE.ChapterID=C.ChapterID) where FE.Year=" + Year + "";

            if (Source == "Reg")
            {
                cmdText += " and GetDAte()>=FE.RegStartDAte and FE.EventDate>=GetDAte()";
            }

            if (FreeEventID > 0)
            {
                cmdText += " and FE.FreeEventID=" + FreeEventID + "";
            }


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEvent objFreeEvent = new FreeEvent();
                        objFreeEvent.ChapterName = dr["name"].ToString();
                        objFreeEvent.Year = Convert.ToInt32(dr["year"].ToString());
                        objFreeEvent.EventDate = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("MM/dd/yyyy");
                        objFreeEvent.Day = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("dddd");
                        objFreeEvent.StrDate = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("MMMM dd, yyyy");

                        DateTime dtToday = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                        if (dtToday > Convert.ToDateTime(dr["EventDate"].ToString()))
                        {
                            objFreeEvent.IsActive = "No";
                        }
                        else
                        {
                            objFreeEvent.IsActive = "Yes";
                        }

                        objFreeEvent.EventName = dr["EventName"].ToString();
                        objFreeEvent.StartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("hh:mm tt");

                        objFreeEvent.StrStartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm");

                        objFreeEvent.EventCode = dr["EventCode"].ToString();
                        objFreeEvent.EndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("hh:mm tt");

                        objFreeEvent.StrEndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("HH:mm");

                        objFreeEvent.VenueName = dr["venuename"].ToString();
                        objFreeEvent.VenueAddress = dr["VenueAddress"].ToString();
                        objFreeEvent.SB = dr["SB"].ToString().Trim();
                        objFreeEvent.VB = dr["VB"].ToString().Trim();
                        objFreeEvent.MB = dr["MB"].ToString().Trim();
                        objFreeEvent.SC = dr["SC"].ToString().Trim();
                        objFreeEvent.GB = dr["GB"].ToString().Trim();
                        objFreeEvent.EW = dr["EW"].ToString().Trim();
                        objFreeEvent.PS = dr["PS"].ToString().Trim();
                        objFreeEvent.BB = dr["BB"].ToString().Trim();
                        objFreeEvent.Chapter = dr["ChapterID"].ToString();
                        objFreeEvent.FreeEventID = dr["FreeEventID"].ToString();
                        objFreeEvent.EventDescription = dr["EventDescription"].ToString();
                        objFreeEvent.CreatedBy = dr["CreatedBy"].ToString();
                        objFreeEvent.EventType = dr["EventType"].ToString();
                        objFreeEvent.ParticipantType = dr["ParticipantType"].ToString();
                        objFreeEvent.ChildFee = dr["ChildFee"].ToString();
                        objFreeEvent.Discount = dr["Discount"].ToString();
                        objFreeEvent.AdultFee = dr["AdultFee"].ToString();
                        if (dr["lastDateToRegister"].ToString() != "")
                        {
                            objFreeEvent.lastDateToRegister = Convert.ToDateTime(dr["lastDateToRegister"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["RegEndTime"].ToString() != "")
                        {
                            objFreeEvent.RegEndTime = Convert.ToDateTime(dr["RegEndTime"].ToString()).ToString("hh:mm tt").Trim();
                        }
                        if (dr["RegEndTime"].ToString() != "")
                        {
                            objFreeEvent.StrRegEndTime = Convert.ToDateTime(dr["RegEndTime"].ToString()).ToString("hh:mm").Trim();
                        }
                        objFreeEvent.Status = dr["Status"].ToString();
                        objFreeEvent.MaxReg = dr["MaxReg"].ToString();
                        if (dr["RegStartDate"].ToString() != "")
                        {
                            objFreeEvent.RegStartDate = Convert.ToDateTime(dr["RegStartDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["RegStartTime"].ToString() != "")
                        {
                            objFreeEvent.RegStartTime = Convert.ToDateTime(dr["RegStartTime"].ToString()).ToString("hh:mm tt");
                            objFreeEvent.StrRegStartTime = Convert.ToDateTime(dr["RegEndTime"].ToString()).ToString("hh:mm").Trim();
                        }
                        ObjListFreeEvent.Add(objFreeEvent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return ObjListFreeEvent;

    }


    [WebMethod]
    public static int DeleteFreeEvent(int FreeEventID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " Delete from FreeEvent where FreeEventID=" + FreeEventID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();

            retVal = 1;
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return retVal;

    }

    [WebMethod]
    public static int ValidateRegistration(int FreeEventID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " Select count(*) as CountSet from FreeEventReg where FreeEventId=" + FreeEventID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return retVal;

    }


    [WebMethod]
    public static int SetActieInActiveFreeEvent(FreeEvent FrEvent)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;




            cmdText = "update FreeEvent set [Status]=@Status where FreeEventid =@FreeEventid";

            SqlCommand cmd = new SqlCommand(cmdText);

            cmd.Parameters.AddWithValue("@Status", FrEvent.Status);
            cmd.Parameters.AddWithValue("@FreeEventid", FrEvent.FreeEventID);

            NSFDBHelper objNSF = new NSFDBHelper();
            if (objNSF.InsertUpdateData(cmd) == true)
            {
                retVal = 1;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
            retVal = -1;
        }
        return retVal;

    }
    [WebMethod]
    public static string GetEventName(string FreeEventId)
    {

        string EventName = "";

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " select EventName from FreeEvent where FreeEventId=" + FreeEventId + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EventName = ds.Tables[0].Rows[0]["EventName"].ToString();
                }
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Free Event Registration");
        }
        return EventName;

    }

}