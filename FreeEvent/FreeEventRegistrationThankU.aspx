﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="FreeEventRegistrationThankU.aspx.cs" Inherits="FreeEvent_FreeEventRegistrationThankU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />

    <link href="../css/Loader.css" rel="stylesheet" />
    <script src="../js/jquery.toast.js"></script>

    <script type="text/javascript">

        $(function (e) {
            var id = GetParameterValues("Id");
            var mId = GetParameterValues("MId");

            //  if (parseInt(mId) > 0) {


            // $("#spnReg1").hide();
            GetAttendeeDetails(mId);
            //} else {

            //}

            listFreeEvents(id, new Date().getFullYear());
        });
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }
        function listFreeEvents(freeEventID, year) {

            $('#selEventCode').empty();
            $('#selEventName').empty();

            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID, Source: "T" });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var eventId = 0;

                    $.each(data.d, function (index, value) {
                        $("#spnPageTitle").text(value.EventName);

                        $("#spnDay").text(value.Day + ", ");
                        $("#spnDate").text(value.StrDate);
                        $("#spnLocation").text(value.VenueAddress);
                        var startTime = value.StartTime.substr(0, 1);
                        var endTime = value.EndTime.substr(0, 1);
                        if (parseInt(startTime) == 0) {
                            startTime = value.StartTime.substr(1, value.StartTime.length);
                        }
                        if (parseInt(endTime) == 0) {
                            endTime = value.EndTime.substr(1, value.EndTime.length);
                        }
                        $("#spnStartTime").text(startTime);
                        $("#spnENdTime").text(endTime + " CDT");
                        $('#spnVenueName').text(value.VenueName);



                    });


                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }


        function GetAttendeeDetails(frMemberID) {
            var year = $("#selyear").val();
            var source = "E";
            var jsonData = JSON.stringify({ FrMemberID: frMemberID, Expired: "", IsValidate: "0" });
            //  showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetFreeEventregistrations",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    var tblHtml = "";
                    tblHtml += "<thead>";
                    tblHtml += " <tr>";

                    tblHtml += "<th>Ser#</th>";

                    //  tblHtml += " <th>Event Name</th>";
                    tblHtml += "<th>First Name</th>";
                    tblHtml += "<th>Last Name</th>";
                    tblHtml += "<th>Email</th>";



                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    var i = 0;
                    var status = "";
                    if (JSON.stringify(data.d.length) > 0) {

                        tblHtml += "<tbody>";
                        $.each(data.d, function (index, value) {

                            i++;
                            tblHtml += "<tr>";

                            tblHtml += "<tr>";
                            tblHtml += "<td>" + (i) + "</td>";


                            //  tblHtml += "<td>" + value.EventName + "</td>";
                            tblHtml += "<td>" + value.AttendeeName + "</td>";
                            tblHtml += "<td>" + value.AttendeeLastName + "</td>";
                            tblHtml += "<td>" + value.AttendeeEmail + "</td>";
                            status = value.Status;

                            tblHtml += "</tr>";

                        });
                    } else {

                        tblHtml += "<tr>";
                        tblHtml += "<td colspan='5' align='center'><span style='color:red; font-weight:bold;'>No record exists</span></td>";
                        tblHtml += "</tr>";
                    }
                    if (status == "Confirmed") {
                        //$("#spnReg").text("You have registered successfully!");
                        $("#spnConfirmed").show();
                        $("#spnReg").hide();
                    } else if (status == "Pending") {
                        //$("#spnReg").text("You have registered successfully!");
                        //  $("#spnReg").text(".");
                        $("#spnReg").show();
                        $("#spnConfirmed").hide();
                    }
                    //  hideLoader();
                    $("#table").html(tblHtml);

                }, failure: function (response) {
                    alert(response.d);
                    // hideLoader();
                }
            });


        }

    </script>

    <div class="container" style="min-height: 450px;">

        <div class="page-header" style="padding-bottom: 0px; margin-bottom: 4px;">
            <center>

                <h3 style="font-size: 20px; font-family: Calibri; font-weight: bold; color: green;">North South Foundation
                 
                    <%-- <div class="clear" style="margin-bottom: 10px;"></div>
                    <span style="">Naperville, IL</span>--%>
                    <%--<div class="clear" style="margin-bottom: 10px;"></div>
                    <span style="color: blue;">Prepare for College and Beyond!</span>--%>
                </h3>

            </center>
        </div>
        <div class="container">
            <center>

                <span style="font-weight: bold; font-family: Calibri; font-size: 18px;" id="spnPageTitle">McDonald’s Education Expo – 2018</span>
                <div class="clear" style="margin-bottom: 10px;"></div>
                <span style="font-weight: bold; font-family: Calibri; font-size: 18px;">Thank you!
                </span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-size: 21px; font-family: Calibri; font-weight: bold; color: blue;" id="spnReg">An email was sent to you. Please open and confirm your registration to join the event.
                    <br />
                    Otherwise, your seat(s) is not guaranteed.
                   
                </span>
                <span style="font-size: 22px; display: none; font-family: Calibri; font-weight: bold; color: blue;" id="spnConfirmed">You have registered successfully!
                   
                </span>
                <div class="clear" style="margin-bottom: 20px;"></div>
                <center><b style="font-size: 15px; font-family: Calibri;">Table: Attendee Details</b></center>
                <div style="width: 900px;">
                    <table id="table" style="width: 65%;">
                    </table>
                </div>
               <%-- <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: red; font-weight: bold; font-size: 18px; font-family: Calibri;" id="spnReg1">Please check your email to complete the registration.</span>--%>

                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: green; font-weight: bold; font-size: 18px; font-family: Calibri; text-decoration: underline;">Date and Time
                </span>

                <div class="clear" style="margin-bottom: 10px;"></div>
                <span style="font-size: 18px; font-family: Calibri;" id="spnDay">Saturday, 
                </span><span id="spnDate" style="font-family: Calibri; font-size: 18px;">July 15, 2017</span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-size: 18px; font-family: Calibri;" id="spnStartTime">10:00 AM 
                </span>
                <span style="font-size: 18px; font-family: Calibri;">to </span><span id="spnENdTime" style="font-size: 18px; font-family: Calibri;">4:00 PM CDT</span>
                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: green; text-decoration: underline; font-weight: bold; font-size: 18px; font-family: Calibri;">Location
                </span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-size: 18px; font-family: Calibri;" id="spnVenueName">Venue name
                </span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-size: 18px; font-family: Calibri;" id="spnLocation">Location
                </span>

                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="font-size: 18px; font-family: Calibri;">Please check your email for additional event details.

                </span>
                <div class="clear" style="margin-bottom: 10px;"></div>
                <a style="color: green; font-weight: bold; font-size: 18px; font-family: Calibri;" href="https://www.northsouth.org" target="_blank">www.northsouth.org
                </a>
            </center>
        </div>
    </div>
</asp:Content>
