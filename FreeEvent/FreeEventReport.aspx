﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeEventReport.aspx.cs" Inherits="FreeEvent_FreeEventReport" MasterPageFile="~/NSFInnerMasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../js/jquery.table2excel.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>
    <style type="text/css">
        .table {
            background: #f5f5f5;
            border-collapse: separate;
            box-shadow: inset 0 1px 0 #fff;
            font-size: 12px;
            line-height: 24px;
            margin: 10px auto;
            text-align: left;
            width: 900px;
            border-collapse: collapse;
        }

            .table th {
                background: url(http://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#4bc970, #fff);
                /*border-left: 1px solid #555;
        border-right: 1px solid #777;
        border-top: 1px solid #555;
        border-bottom: 1px solid #333;*/
                border: 1px solid #e8e8e8;
                box-shadow: inset 0 1px 0 #999;
                color: #000;
                font-weight: bold;
                font-size: 11px;
                padding: 3px 16px;
                border-collapse: collapse;
                position: relative;
                /*text-shadow: 0 1px 0 #000;*/
            }

                .table th:after {
                    background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
                    content: '';
                    display: block;
                    height: 25%;
                    left: 0;
                    margin: 1px 0 0 0;
                    position: absolute;
                    top: 25%;
                    width: 100%;
                }

                .table th:first-child {
                    /*border-left: 1px solid #777;*/
                    box-shadow: inset 1px 1px 0 #999;
                }

                .table th:last-child {
                    box-shadow: inset -1px 1px 0 #999;
                }

            .table td {
                border-right: 1px solid #fff;
                border-left: 1px solid #e8e8e8;
                border-top: 1px solid #fff;
                border-bottom: 1px solid #e8e8e8;
                padding: 3px 10px;
                position: relative;
                transition: all 300ms;
                border-collapse: collapse;
            }

                .table td:first-child {
                    box-shadow: inset 1px 0 0 #fff;
                }

                .table td:last-child {
                    border-right: 1px solid #e8e8e8;
                    box-shadow: inset -1px 0 0 #fff;
                }

            .table tr {
                background: url(http://jackrugile.com/images/misc/noise-diagonal.png);
                border-collapse: collapse;
            }

                .table tr:nth-child(odd) td {
                    background: #f1f1f1 url(http://jackrugile.com/images/misc/noise-diagonal.png);
                }

                .table tr:last-of-type td {
                    box-shadow: inset 0 -1px 0 #fff;
                }

                    .table tr:last-of-type td:first-child {
                        box-shadow: inset 1px -1px 0 #fff;
                    }

                    .table tr:last-of-type td:last-child {
                        box-shadow: inset -1px -1px 0 #fff;
                    }

            .table tbody:hover td {
                /*color: transparent;
        text-shadow: 0 0 3px #aaa;*/
            }

            .table tbody:hover tr:hover td {
                /*color: #444;
        text-shadow: 0 1px 0 #fff;*/
            }
    </style>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />

    <link href="../css/Loader.css" rel="stylesheet" />
    <script src="../js/jquery.toast.js"></script>

    <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <link href="../css/ezmodal.css" rel="stylesheet" />
    <script src="../js/ezmodal.js"></script>

    <script type="text/javascript">
        var chapterId = 0;
        $(function (e) {
            populateYear();
            var year = $("#selYear").val();
            listFreeEvents(0, year);
        });
        $(document).on("change", "#selYear", function (e) {
            var year = $("#selYear").val();
            listFreeEvents(0, year);
        });
        function populateYear() {
            var d = new Date();
            var year = d.getFullYear();
            var prevyear = parseInt(year) - 1;
            var futureYear = parseInt(year) + 1;

            $("#selYear").append($("<option></option>").val
                (futureYear).html(futureYear));

            $("#selYear").append($("<option></option>").val
                (year).html(year));

            $("#selYear").append($("<option></option>").val
                (prevyear).html(prevyear));
            $("#selYear").val(year);

        }

        $(document).on("click", ".delete", function (e) {

            var frMemberId = $(this).attr("attr-MemberID");
            var frFreeEventId = $(this).attr("attr-FreeEventID");
            if (confirm("Are you sure want to delete?")) {
                deleteRegistrants(frFreeEventId, frMemberId);
            }
        });
        function deleteRegistrants(freeEventId, frMemberId) {
            var jsonData = JSON.stringify({ FrMemberId: frMemberId, FreeEventId: freeEventId });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReport.aspx/DeleteRegistrants",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    hideLoader();
                    if (JSON.stringify(data.d) > 0) {
                        statusMessage("Deleted successfully!", "mid-center", "error");
                        listFreeEventReg();
                    }
                }

            });
        }

        function listFreeEvents(freeEventID, year) {


            $('#selEventName').empty();

            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID, Source: "R" });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    hideLoader();
                    var eventId = 0;


                    $("#selEventName").append($("<option></option>").val
                        (0).html("Select"));
                    $.each(data.d, function (index, value) {


                        $("#selEventName").append($("<option></option>").val
                            (value.FreeEventID).html(value.EventName));
                        eventId = value.FreeEventID;

                    });
                    if (data.d.length == 1) {

                        $("#selEventName").attr("disabled", "disabled");

                        $("#selEventName").val(eventId);
                    } else if (data.d.length > 1) {

                        $("#selEventName").removeAttr("disabled");
                    }
                    hideLoader();
                    //  getContestantsCount();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }



        function listFreeEventReg() {
            $("#dvFrEventAttendees").show();
            $("#dvFrEventContestants").hide();
            var year = $("#selYear").val();
            var freeEventId = $("#selEventName").val();
            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventId });

            showLoader();
            var regCount = 0;
            var pendingCount = 0;
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/ListFreeEventregistrations",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var totalCount = 0;
                    var confirmedCount = 0;

                    if (JSON.stringify(data.d.length) > 0) {

                        var tblHtml = "";
                        var i = 0;

                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Ser#</th>";
                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Year</th>";
                        tblHtml += "<th>Chapter</th>";
                        tblHtml += "<th>Event Code</th>";
                        tblHtml += " <th>First Name</th>";
                        tblHtml += " <th>Middle Name</th>";
                        tblHtml += "<th>Last Name</th>";
                        tblHtml += "<th>Email</th>";
                        tblHtml += "<th>Part Type</th>";
                        tblHtml += "<th>Attendees</th>";
                        tblHtml += "<th>Status</th>";


                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        tblHtml += "<tbody>";
                        $.each(data.d, function (index, value) {
                            totalCount++;
                            chapterId = value.ChapterID;
                            i++;
                            if (value.Status == "Pending") {
                                pendingCount++;
                            }
                            if (value.Status == "Confirmed") {
                                confirmedCount++;
                            }
                            tblHtml += " <tr>";

                            tblHtml += "<td>" + i + "</td>";

                            tblHtml += '<td><div style="float:left;"><a style="cursor:pointer;" attr-MemberID=' + value.FrMemberID + ' attr-FreeEventID=' + value.FreeEventID + ' attr-type=' + value.ParticipantType + '  title="View Attendees" class="view"><i class="fa fa-eye fa-2x" aria-hidden="true"></i></a> </div> <div style="float:left; position:relative; left:10px;"><a class="delete" style="cursor:pointer;" attr-MemberID=' + value.FrMemberID + ' attr-FreeEventID=' + value.FreeEventID + '><i class="fa fa-trash-o fa-2x"  aria-hidden="true"></i></a></div></td>';

                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.Year + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.ChapterCode + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.EventCode + "</span></td>";
                            tblHtml += " <td><span style='position:relative; left:3px;'>" + value.FirstName + "</span></td>";
                            tblHtml += " <td><span style='position:relative; left:6px;'>" + value.MiddleName + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.LastName + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.Email + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.PartType + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:10px;'>" + value.Attendees + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.Status + "</span></td>";


                            tblHtml += "</tr>";



                        });

                        $("#spnConfirmedCount").text(confirmedCount);
                        $("#spnPending").text(pendingCount);
                        $("#spnTotal").text(totalCount);

                        $("#btnExportAttendees").show();
                        if (pendingCount > 0) {
                            $("#btnSendEmailToPendingUser").show();
                        } else {
                            $("#btnSendEmailToPendingUser").hide();
                        }
                        getContestantsCount();
                        getAttendeesCount();
                        getAdultCount();
                    } else {
                        $("#btnExportAttendees").hide();
                        $("#btnSendEmailToPendingUser").hide();
                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='17' align='center'>No record exists";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        hideLoader();
                    }
                    tblHtml += "</tbody>";
                    $("#tblAttendees").html(tblHtml);
                    if (JSON.stringify(data.d.length > 0)) {
                        openPagination("tblAttendees");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", "#btnSubmit", function (e) {
            if (validateSubmit() == 1) {
                var option = $("#selReportType").val();
                if (option == "1") {
                    listFreeEventReg();
                } else if (option == "2") {
                    getContestantsReport();
                }
            }
        });

        $(document).on("click", ".view", function (e) {
            var freeEventID = $(this).attr("attr-FreeEventID");
            var memberID = $(this).attr("attr-MemberID");
            var type = $(this).attr("attr-type");
            if (type == "Adult") {
                getAdultBasedOnMemberId(memberID, freeEventID);

                $("#tblAdultInfo").show();
            } else if (type == "Child") {
                getChildBasedOnMemberId(memberID, freeEventID);
                $("#tblChildInfo").show();
            } else {
                getChildBasedOnMemberId(memberID, freeEventID);
                getAdultBasedOnMemberId(memberID, freeEventID);
                $("#tblChildInfo").show();
                $("#tblAdultInfo").show();
            }
            $(".btnPopUP").trigger("click");
        });

        function openPagination(id) {
            $("#" + id + "").dataTable({
                "bPaginate": true,
                "bInfo": true,
                "iDisplayLength": 10,

                "bFilter": false,
                "bLengthChange": false,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "bDestroy": true,
                //"bSortable": false,
                //"bSort": false,
                "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [1, 5] }],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //debugger;
                    var index = iDisplayIndexFull + 1;
                    $("td:first", nRow).html(index);
                    $("td:first", nRow).removeClass("sorting_1");
                    return nRow;

                }

            });
        }

        function openPaginationForChild(id) {
            $("#" + id + "").dataTable({
                "bPaginate": false,
                "bInfo": true,
                "iDisplayLength": 10,

                "bFilter": false,
                "bLengthChange": false,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "bDestroy": true,
                "bSortable": false,
                "bSort": false,
                "aoColumnDefs": [{ 'bSortable': false }],
                //"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //    //debugger;
                //    var index = iDisplayIndexFull + 1;
                //    $("td:first", nRow).html(index);
                //    $("td:first", nRow).removeClass("sorting_1");
                //    return nRow;

                //}

            });
        }

        function getChildBasedOnMemberId(memberId, freeEventId) {


            var jsonData = JSON.stringify({ FreeEventID: freeEventId, FrMemberID: memberId });
            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetChildrenBasedOnMemberID",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var tblHtml = "";
                    var i = 0;
                    tblHtml += "<thead>";
                    tblHtml += " <tr>";

                    tblHtml += "<th>Ser#</th>";
                    tblHtml += "<th>ChildName</th>";
                    tblHtml += "<th>Grade</th>";
                    tblHtml += "<th>School</th>";
                    tblHtml += "<th>Contest</th>";


                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    tblHtml += "<tbody>";
                    if (JSON.stringify(data.d.length) > 0) {

                        var contest = "";
                        $.each(data.d, function (index, value) {
                            contest = "";
                            i++;
                            tblHtml += " <tr>";

                            tblHtml += "<td>" + i + "</td>";



                            tblHtml += "<td>" + value.ChildName + "</td>";
                            tblHtml += "<td>" + value.Grade + "</td>";
                            tblHtml += "<td>" + value.SchoolName + "</td>";

                            if (value.VB != "") {
                                contest += value.VB + ", ";
                            }
                            if (value.SB != "") {
                                contest += value.SB + ", ";
                            }
                            if (value.MB != "") {
                                contest += value.MB + ", ";
                            }
                            if (value.GB != "") {
                                contest += value.GB + ", ";
                            }
                            if (value.SC != "") {
                                contest += value.SC + ", ";
                            }
                            if (value.EW != "") {
                                contest += value.EW + ", ";
                            }
                            if (value.PS != "") {
                                contest += value.PS + ", ";
                            }
                            if (value.BB != "") {
                                contest += value.BB + ", ";
                            }
                            contest = contest.substring(0, contest.length - 1);

                            tblHtml += "<td>" + contest.substring(0, contest.length - 1) + "</td>";

                            tblHtml += "</tr>";



                        });
                        hideLoader();
                        tblHtml += "</tbody>";
                        $("#tblChildInfo").html(tblHtml);


                        openPaginationForChild("tblChildInfo");

                    } else {

                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='5' align='center'>No record exists";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "</tbody>";
                        $("#tblChildInfo").html(tblHtml);

                        hideLoader();
                        //$("#tblChildInfo").dataTable({
                        //    "bDestroy":true
                        //});
                        openPaginationForChild("tblChildInfo");

                    }




                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function validateSubmit() {
            var retval = 1;
            var year = $("#selYear").val();
            var freeEventId = $("#selEventName").val();
            if (year == "0") {
                retval = -1;
                statusMessage("Please select year", "mid-center", "error");
            } else if (freeEventId == "0") {
                retval = -1;
                statusMessage("Please select Event Name", "mid-center", "error");
            }
            return retval;
        }


        function statusMessage(message, position, type) {

            $.toast({
                // heading: 'Can I add <em>icons</em>?',
                text: '<b>' + message + '</b>',
                icon: type,
                position: 'mid-center',
                hideAfter: false,
                stack: 1

            })
        }

        function getContestantsReport() {

            $("#dvFrEventAttendees").hide();
            $("#dvFrEventContestants").show();
            var year = $("#selYear").val();
            var freeEventId = $("#selEventName").val();
            var jsonData = JSON.stringify({ FreeEventID: freeEventId, Year: year });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetContestantsReport",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var tblHtml = "";
                    tblHtml += "<thead>";
                    tblHtml += " <tr>";

                    tblHtml += "<th>Ser#</th>";

                    tblHtml += "<th>Year</th>";
                    tblHtml += "<th>Chapter</th>";
                    tblHtml += "<th>Event Code</th>";
                    tblHtml += "<th>Child Name</th>";
                    tblHtml += "<th>School Name</th>";
                    tblHtml += "<th>Grade</th>";
                    tblHtml += "<th>Contest</th>";

                    tblHtml += "<th>Part Type</th>";
                    tblHtml += " <th>Name</th>";
                    tblHtml += "<th>Email</th>";

                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    tblHtml += "<tbody>";
                    if (JSON.stringify(data.d.length) > 0) {


                        var i = 0;

                        var name = "";
                        $.each(data.d, function (index, value) {
                            i++;
                            name = value.FirstName + " " + value.LastName;
                            tblHtml += " <tr>";

                            tblHtml += "<td>" + i + "</td>";


                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.Year + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3x;'>" + value.ChapterCode + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.EventCode + "</span></td>";
                            tblHtml += " <td><span style='position:relative; left:3px;'>" + value.ChildName + "</span></td>";
                            tblHtml += " <td><span style='position:relative; left:3px;'>" + value.SchoolName + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:10px;'>" + value.Grade + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.ProductCode + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.PartType + "</span></td>";
                            tblHtml += "<td><span style='position:relative; left:3px;'>" + name + "</span></td>";

                            tblHtml += "<td><span style='position:relative; left:3px;'>" + value.Email + "</span></td>";


                            tblHtml += "</tr>";



                        });

                        $("#btnExportContestants").show();
                        openPagination("tblContestants");
                    } else {
                        $("#btnExportContestants").hide();
                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='17' align='center'>No record exists";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        hideLoader();
                        openPagination("tblContestants");
                    }
                    tblHtml += "</tbody>";
                    $("#tblContestants").html(tblHtml);
                    // if (JSON.stringify(data.d.length > 0)) {

                    // }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function exportReportToExcel() {

            var jsonData = JSON.stringify({ SurveyID: 0 });
            $.ajax({
                type: "POST",
                url: "FreeEventReport.aspx/ExportSurveyResponse",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                async: true,
                cache: false,
                success: function (data) {

                    var retVal = data.d;
                    if (retVal == 1) {
                        var option = $("#selReportType").val();
                        document.getElementById('<%= hdnYear.ClientID%>').value = $("#selYear").val();
                        document.getElementById('<%= hdnFreeEventId.ClientID%>').value = $("#selEventName").val();
                        if (option == "1") {
                            document.getElementById('<%= btnExportToExcelAttendees.ClientID%>').click();
                        } else if (option == "2") {
                            document.getElementById('<%= btnExportToExcelContestants.ClientID%>').click();
                        }
                    }

                },

                failure: function (response) {
                    $("#fountainTextG").css("display", "none");
                    $("#overlay").css("display", "none");
                }
            });
        }

        $(document).on("click", "#btnExportAttendees", function (e) {
            e.preventDefault();
            exportReportToExcel();
        });
        $(document).on("click", "#btnExportContestants", function (e) {
            e.preventDefault();
            exportReportToExcel();
        });

        function senEmailToPendingReg() {
            var year = $("#selYear").val();
            var freeEventId = $("#selEventName").val();
            var eventName = $("#selEventName option:selected").text();
            var memberId = document.getElementById('<%= hdnMemberID.ClientID%>').value;
            var loginMail = document.getElementById('<%= hdnLoginMail.ClientID%>').value;
            var roleID = document.getElementById('<%= hdnRoleID.ClientID%>').value;
            var jsonData = JSON.stringify({ FreeEventID: freeEventId, Year: year, EventName: eventName, ChapterID: chapterId, LoginMail: loginMail, RoleID: roleID, FrMemberID: memberId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReport.aspx/SendEmailsToPendingRegistarants",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {

                    if (JSON.stringify(data.d) > 0) {
                        statusMessage("Email has been sent successfully", "mid-center", "success");
                    }
                    hideLoader();
                },
                failure: function (e) {
                    hideLoader();
                }
            });
        }

        $(document).on("click", "#btnSendEmailToPendingUser", function (e) {
            senEmailToPendingReg();
        });

        function getContestantsCount() {

            var year = $('#selYear').val();
            var freeEventid = $('#selEventName').val();
            var jsonData = JSON.stringify({ Year: year, FreeEventId: freeEventid });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetContestantsTotaCount",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    hideLoader();
                    var confirmed = 0;
                    var pending = 0;
                    $.each(data.d, function (index, value) {
                        $("#spnContestTitle").text(value.Title);
                        $("#spnTotalContestants").text(value.Confirmed);
                        $("#spnTotalPending").text(value.Pending);
                        $("#spnTotalCount").text(value.Total);

                    });


                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function getAttendeesCount() {

            var year = $('#selYear').val();
            var freeEventid = $('#selEventName').val();
            var jsonData = JSON.stringify({ Year: year, FreeEventId: freeEventid });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAttendeesCount",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    hideLoader();
                    var confirmed = 0;
                    var pending = 0;
                    $.each(data.d, function (index, value) {
                        $("#spnAttendeeConfirmed").text(value.Confirmed);
                        $("#spnAttendeePending").text(value.Pending);
                        $("#spnAttendeeTotal").text(value.Total);
                        $("#spnMaxReg").text(value.MaxReg);

                    });


                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function getAdultBasedOnMemberId(memberId, freeEventId) {


            var jsonData = JSON.stringify({ FreeEventID: freeEventId, FrMemberID: memberId });
            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAdultBasedOnMemberID",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var tblHtml = "";
                    var i = 0;
                    tblHtml += "<thead>";
                    tblHtml += " <tr>";

                    tblHtml += "<th>Ser#</th>";
                    tblHtml += "<th>Adult Name</th>";
                    tblHtml += "<th>Email</th>";

                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    tblHtml += "<tbody>";
                    if (JSON.stringify(data.d.length) > 0) {

                        var contest = "";
                        var name = "";
                        $.each(data.d, function (index, value) {
                            contest = "";
                            i++;
                            tblHtml += " <tr>";

                            tblHtml += "<td>" + i + "</td>";

                            name = value.AttendeeFirstName + " " + value.AttendeeLastName;

                            tblHtml += "<td>" + name + "</td>";
                            tblHtml += "<td>" + value.Email + "</td>";

                            tblHtml += "</tr>";



                        });
                        hideLoader();
                        tblHtml += "</tbody>";
                        $("#tblAdultInfo").html(tblHtml);
                        openPaginationForChild("tblAdultInfo");
                        $('#dvChildInfo').hide();
                        $('#dvAdultInfo').show();
                    } else {

                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='3' align='center'>No record exists";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        tblHtml += "</tbody>";
                        $("#tblAdultInfo").html(tblHtml);
                        hideLoader();
                        //$("#tblChildInfo").dataTable({
                        //    "bDestroy":true
                        //});
                        openPaginationForChild("tblAdultInfo");
                        $('#dvChildInfo').hide();
                        $('#dvAdultInfo').show();
                    }




                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function getAdultCount() {

            var year = $('#selYear').val();
            var freeEventid = $('#selEventName').val();
            var jsonData = JSON.stringify({ Year: year, FreeEventId: freeEventid });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAdultCount",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    hideLoader();
                    var confirmed = 0;
                    var pending = 0;
                    $.each(data.d, function (index, value) {
                       
                        $("#spnTotalAdult").text(value.Confirmed);
                        $("#spnAdultPending").text(value.Pending);
                        $("#spnAdultTotalCount").text(value.Total);

                    });


                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

    </script>

    <div class="container" style="min-height: 450px;">
        <asp:Button ID="btnExportToExcelAttendees" Style="display: none;" runat="server" OnClick="btnExportToExcelAttendees_Click" />

        <asp:Button ID="btnExportToExcelContestants" Style="display: none;" runat="server" OnClick="btnExportToExcelContestants_Click" />
        <div style="float: left;">

            <a href="../VolunteerFunctions.aspx">Back to Volunteer Functions</a>
        </div>
        <div class="page-header">
            <center>
                <h3 style="color: #00a0b0; font-weight: bold; font-family: 'Trebuchet MS'">Event Report</h3>

            </center>
        </div>

        <div class="bs-example" id="dvUpdateRegistration" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">

            <div>
                <div style="float: left; width: 35px;">
                    <label for="InputName" class="control-label">Year</label>
                </div>
                <div style="float: left;">
                    <select id="selYear" class="form-control" required>
                        <option value="0">Select</option>

                    </select>
                </div>

            </div>

            <div>
                <div style="float: left; width: 75px; margin-left: 20px;">
                    <label for="InputName" class="control-label">Event Name</label>
                </div>
                <div style="float: left;">
                    <select id="selEventName" class="form-control" required>
                        <option value="0">Select</option>

                    </select>
                </div>

            </div>


            <div>
                <div style="float: left; width: 75px; margin-left: 20px;">
                    <label for="InputName" class="control-label">Report Type</label>
                </div>
                <div style="float: left;">
                    <select id="selReportType" class="form-control" required>

                        <option value="1">Attendees Report</option>
                        <option value="2">Contestants Report</option>

                    </select>
                </div>

            </div>

            <div>

                <div style="float: left; margin-left: 10px;">
                    <input type="button" id="btnSubmit" value="Submit" class="btn btn-info" name="Submit" />
                </div>
            </div>

        </div>


        <div class="clear" style="margin-bottom: 20px;"></div>
        <div id="dvFrEventAttendees" style="display: none;">
            <center><b>Table 1: Free Event Attendees Report</b></center>

            <div style="float: right;">
                <div class="panel panel-default" style="margin-bottom: 0px; float: right;">

                    <div class="panel-body" style="padding: 10px;"><span style="font-weight: bold;"><span>Adult: </span><span>Confirmed - <span id="spnTotalAdult">0</span>, </span><span>Pending - <span id="spnAdultPending">0</span>,</span> <span>Total - <span id="spnAdultTotalCount">0</span></span> </span></div>


                </div>
            </div>

            <div style="float: right;">
                <div class="panel panel-default" style="margin-bottom: 0px; float: right;">

                    <div class="panel-body" style="padding: 10px;"><span style="font-weight: bold;"><span id="spnContestTitle">Contestant: </span><span>Confirmed - <span id="spnTotalContestants">0</span>, </span><span>Pending - <span id="spnTotalPending">0</span>,</span> <span>Total - <span id="spnTotalCount">0</span></span> </span></div>


                </div>
            </div>
            <div style="float: right; margin-right: 10px;">
                <div class="panel panel-default" style="margin-bottom: 0px;">
                    <div class="panel-body" style="padding: 10px;"><span style="font-weight: bold;">Families: <span>Confirmed - <span id="spnConfirmedCount">0</span>, </span><span>Pending - <span id="spnPending">0</span>,</span> <span>Total - <span id="spnTotal">0</span></span></span></div>
                </div>


            </div>

            <div style="float: right; margin-right: 10px;">
                <div class="panel panel-default" style="margin-bottom: 0px;">
                    <div class="panel-body" style="padding: 10px;"><span style="font-weight: bold;">Attendees: <span>Confirmed - <span id="spnAttendeeConfirmed">0</span>, </span><span>Pending - <span id="spnAttendeePending">0</span>,</span> <span>Total - <span id="spnAttendeeTotal">0</span></span></span></div>
                </div>


            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">
                <input type="button" id="btnExportAttendees" value="Export To Excel" class="btn btn-info" name="Export To Excel" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <input type="button" id="btnSendEmailToPendingUser" value="Send Email To Pending Registrant(s)" class="btn btn-info" name="Send Email" />
            </div>
            <div style="float: left; position:relative; left:10px;">
                <div class="panel panel-default" style="margin-bottom: 0px;">
                    <div class="panel-body" style="padding: 6px;"><span style="font-weight: bold;">Max Reg:  <span id="spnMaxReg">0</span></div>
                </div>


            </div>
            <div style="margin-bottom: 5px; clear: both;"></div>
            <div style="width: 1150px;">
                <table id="tblAttendees" class="table" style="width: 100%;">
                </table>
            </div>
        </div>
        <div style="margin-bottom: 20px; clear: both;"></div>
        <div id="dvFrEventContestants" style="display: none;">
            <center><b>Table 1: Free Event Contestant Report</b></center>
            <div>
                <input type="button" id="btnExportContestants" value="Export To Excel" class="btn btn-info" name="Export To Excel" />
            </div>
            <div class="clear" style="margin-bottom: 5px;"></div>
            <div style="width: 1150px;">
                <table id="tblContestants" class="table" style="width: 100%;">
                </table>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
    <div id="overlay"></div>

    <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>
    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Attendee Information</span>
            </div>

            <div class="ezmodal-content">


                <div class="dvgridContent">
                    <div id="dvChildInfo">
                        <table align="center" class="table" id="tblChildInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px; display: none;">
                        </table>
                    </div>
                    <div id="dvAdultInfo">
                        <table align="center" class="table" id="tblAdultInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px; display: none;">
                        </table>
                    </div>


                </div>
            </div>



            <div class="ezmodal-footer">

                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

    <input type="hidden" runat="server" id="hdnYear" value="2017" />
    <input type="hidden" runat="server" id="hdnFreeEventId" value="1" />

    <input type="hidden" runat="server" id="hdnLoginMail" value="" />
    <input type="hidden" runat="server" id="hdnMemberID" value="1" />
    <input type="hidden" runat="server" id="hdnRoleID" value="5" />

</asp:Content>
