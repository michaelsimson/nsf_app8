﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="FreeEventRegistrationThankU.aspx.cs" Inherits="FreeEvent_FreeEventRegistrationThankU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>

    <script type="text/javascript">

        $(function (e) {
            var id = GetParameterValues("Id");

            listFreeEvents(id, new Date().getFullYear());
        });
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }
        function listFreeEvents(freeEventID, year) {

            $('#selEventCode').empty();
            $('#selEventName').empty();

            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var eventId = 0;

                    $.each(data.d, function (index, value) {
                        $("#spnPageTitle").text(value.EventName);

                        $("#spnDay").text(value.Day + ", ");
                        $("#spnDate").text(value.StrDate);
                        $("#spnStartTime").text(value.StartTime);
                        $("#spnENdTime").text(value.EndTime + " CDT");
                        $("#spnLocation").text(value.VenueAddress);


                    });


                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }
    </script>

    <div class="container" style="min-height: 450px;">

        <div class="page-header">
            <center>

                <h3 style="font-size: 40px; font-family: Calibri;">North South Foundation
                  <div class="clear" style="margin-bottom: 10px;"></div>
                    <span style="" id="spnPageTitle">McDonald’s Education Expo – 2017</span>
                    <div class="clear" style="margin-bottom: 10px;"></div>
                    <span style="">Naperville, IL</span>
                    <div class="clear" style="margin-bottom: 10px;"></div>
                    <span style="color: blue;">Prepare for College and Beyond!</span>
                </h3>

            </center>
        </div>
        <div class="container">
            <center>
                <span style="color: green; font-weight: bold;">Thank you!
                </span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="color: green; font-weight: bold;">We have received your registration and look forward to seeing you.
                   
                </span>
                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: blue; font-weight: bold;">Please check your email to complete the registration.</span>

                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: green; font-weight: bold;">Date and Time
                </span>

                <div class="clear" style="margin-bottom: 10px;"></div>
                <span style="font-weight: bold;" id="spnDay">Saturday, 
                </span><span id="spnDate" style="font-weight: bold;">July 15, 2017</span>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-weight: bold;" id="spnStartTime">10:00 AM 
                </span>
                <span style="font-weight: bold;">to </span><span id="spnENdTime" style="font-weight: bold;">4:00 PM CDT</span>
                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="color: green; font-weight: bold;">Location
                </span>

                <div class="clear" style="margin-bottom: 5px;"></div>
                <span style="font-weight: bold;" id="spnLocation">Location
                </span>

                <div class="clear" style="margin-bottom: 20px;"></div>
                <span style="font-weight: bold;">Please check your email for additional event details.

                </span>
                <div class="clear" style="margin-bottom: 10px;"></div>
                <a style="color: green; font-weight: bold;" href="https://www.northsouth.org" target="_blank">www.northsouth.org
                </a>
            </center>
        </div>
    </div>
</asp:Content>
