﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Data;

public partial class FreeEvent_FreeEvent : System.Web.UI.Page
{

    public class FreeEvent
    {

        public string FreeEventID { get; set; }
        public int Year { get; set; }
        public string Chapter { get; set; }
        public string ChapterName { get; set; }
        public string EventCode { get; set; }
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public string StartTime { get; set; }
        public string lastDateToRegister { get; set; }
        public string RegEndTime { get; set; }
        public string StrRegEndTime { get; set; }
        public string EndTime { get; set; }

        public string StrStartTime { get; set; }
        public string StrEndTime { get; set; }

        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public string SB { get; set; }
        public string VB { get; set; }
        public string MB { get; set; }
        public string SC { get; set; }
        public string GB { get; set; }
        public string EW { get; set; }
        public string PS { get; set; }
        public string BB { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string EventDescription { get; set; }
        public string Day { get; set; }
        public string StrDate { get; set; }
        public string Status { get; set; }
    }

    public class Chapter
    {
        public int ChapterID { get; set; }
        public string ChapterName { get; set; }
    }
    public class ProductGroup
    {
        public int ProductGroupID { get; set; }
        public string ProductGroupCode { get; set; }
        public string ProductGroupName { get; set; }
        public int EventID { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {
                hdnLoginID.Value = Session["LoginID"].ToString();
                hdnRoleID.Value = Session["RoleID"].ToString();
            }
        }
    }

    [WebMethod]
    public static List<Chapter> ListChapter(Chapter Chapter)
    {

        int retVal = -1;
        List<Chapter> ObjListChapter = new List<Chapter>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select ChapterId, ChapterCode from Chapter ";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Chapter ObjChapt = new Chapter();
                        ObjChapt.ChapterID = Convert.ToInt32(dr["ChapterID"].ToString());
                        ObjChapt.ChapterName = dr["ChapterCode"].ToString();
                        ObjListChapter.Add(ObjChapt);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListChapter;

    }

    [WebMethod]
    public static List<ProductGroup> ListProductGroup(ProductGroup ProductGroup)
    {

        int retVal = -1;
        List<ProductGroup> ObjListPGroup = new List<ProductGroup>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select ProductGroupID,Name, case when Name='Spelling' then '1. Spelling' when Name='Vocabulary' then '2. Vocabulary' when Name='Math' then '3. Math' when Name='Science' then '4. Science' when Name='Geography' then '5. Geography' when Name='Essay Writing' then '6. Essay Writing' when Name='Public Speaking' then '7. Public Speaking' when Name='Brain Bee' then '8. Brain Bee'  end as PName from ProductGroup where EventID=" + ProductGroup.EventID + " order by PName";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroup objPGroup = new ProductGroup();
                        objPGroup.ProductGroupID = Convert.ToInt32(dr["ProductGroupId"].ToString());
                        objPGroup.ProductGroupName = dr["Name"].ToString();
                        ObjListPGroup.Add(objPGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListPGroup;

    }

    [WebMethod]
    public static int IsDuplicateExists(FreeEvent FrEvent)
    {

        int retVal = -1;
        int Count = 0;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Select count(*) as EventCount from FreeEvent where EventDate='" + FrEvent.EventDate + "' and  ChapterID=" + FrEvent.Chapter + " and year=" + FrEvent.Year + "";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Count = Convert.ToInt32(ds.Tables[0].Rows[0]["EventCount"].ToString());
                    retVal = Count;
                }
            }


        }
        catch (Exception ex)
        {
        }
        return retVal;
    }

    [WebMethod]
    public static int PostNewEvent(FreeEvent FrEvent)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            SqlCommand cmd;

            string EventDate = Convert.ToDateTime(FrEvent.EventDate).ToShortDateString();
            DateTime dtEventDate = Convert.ToDateTime(EventDate);
            string Today = Convert.ToDateTime(DateTime.Now).ToShortDateString();
            DateTime dtTodayDate = Convert.ToDateTime(Today);
            string valid = "true";
            string EventEndDate = Convert.ToDateTime(FrEvent.lastDateToRegister).ToShortDateString();
            DateTime dtEventEndDate = Convert.ToDateTime(EventEndDate);

            if (dtEventDate >= dtTodayDate)
            {
                string startTime = FrEvent.EventDate + " " + FrEvent.StartTime;
                DateTime dtStartTime = Convert.ToDateTime(startTime);
                string endTime = FrEvent.EventDate + " " + FrEvent.EndTime;
                DateTime dtEndTime = Convert.ToDateTime(endTime);

                string Current = Convert.ToDateTime(DateTime.Now).ToString();
                DateTime dtCurrent = Convert.ToDateTime(Current);

                if (dtStartTime > dtCurrent && dtEndTime > dtStartTime)
                {
                    valid = "true";
                }
                else
                {
                    if (dtStartTime <= dtCurrent)
                    {
                        retVal = -2;

                    }
                    if (dtEndTime < dtStartTime)
                    {
                        retVal = -3;
                    }
                    valid = "false";
                }
            }
            if (dtEventEndDate < dtEventDate && dtEventEndDate > dtTodayDate)
            {
                valid = "true";
            }
            else
            {
                valid = "false";
                retVal = -4;
            }




            if (dtEventDate >= dtTodayDate && valid == "true")
            {

                if (Convert.ToInt32(FrEvent.FreeEventID) == 0)
                {
                    //cmdText = "insert into FreeEvent ( Year,ChapterID,EventCode,EventName,EventDate,StartTime,EndTime,VenueName,VenueAddress,SB,VB,MB,SC, GB, EW, PS,BB, CreatedDate, CreatedBy, EventDescription) values(" + FrEvent.Year + ", " + FrEvent.Chapter + ", '" + FrEvent.EventCode + "', '" + FrEvent.EventName + "', '" + FrEvent.EventDate + "', '" + FrEvent.StartTime + "', '" + FrEvent.EndTime + "', '" + FrEvent.VenueName + "', '" + FrEvent.VenueAddress + "', '" + FrEvent.SB + "', '" + FrEvent.VB + "', '" + FrEvent.MB + "', '" + FrEvent.SC + "', '" + FrEvent.GB + "', '" + FrEvent.EW + "', '" + FrEvent.PS + "', '" + FrEvent.BB + "', '" + DateTime.Now.ToShortDateString() + "', '" + FrEvent.CreatedBy + "', '" + FrEvent.EventDescription + "')";


                    cmdText = "insert into FreeEvent ( Year,ChapterID,EventCode,EventName,EventDate,StartTime,EndTime,VenueName,VenueAddress,SB,VB,MB,SC, GB, EW, PS,BB, CreatedDate, CreatedBy, EventDescription, lastDateToRegister, RegEndTime, Status) values(@Year,@Chapter,@EventCode,@EventName,@EventDate,@StartTime,@EndTime,@VenueName,@VenueAddress,@SB,@VB,@MB,@SC,@GB,@EW,@PS,@BB,@CreatedDate,@CreatedBy,@EventDescription, @lastDateToRegister, @RegEndTime, @Status)";

                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEvent.Year);
                    cmd.Parameters.AddWithValue("@Chapter", FrEvent.Chapter);
                    cmd.Parameters.AddWithValue("@EventCode", FrEvent.EventCode);
                    cmd.Parameters.AddWithValue("@EventName", FrEvent.EventName);
                    cmd.Parameters.AddWithValue("@EventDate", FrEvent.EventDate);
                    cmd.Parameters.AddWithValue("@StartTime", FrEvent.StartTime);
                    cmd.Parameters.AddWithValue("@EndTime", FrEvent.EndTime);
                    cmd.Parameters.AddWithValue("@VenueName", FrEvent.VenueName);
                    cmd.Parameters.AddWithValue("@VenueAddress", FrEvent.VenueAddress);
                    cmd.Parameters.AddWithValue("@SB", FrEvent.SB == null ? "" : FrEvent.SB);
                    cmd.Parameters.AddWithValue("@VB", FrEvent.VB == null ? "" : FrEvent.VB);
                    cmd.Parameters.AddWithValue("@MB", FrEvent.MB == null ? "" : FrEvent.MB);
                    cmd.Parameters.AddWithValue("@SC", FrEvent.SC == null ? "" : FrEvent.SC);
                    cmd.Parameters.AddWithValue("@GB", FrEvent.GB == null ? "" : FrEvent.GB);
                    cmd.Parameters.AddWithValue("@EW", FrEvent.EW == null ? "" : FrEvent.EW);
                    cmd.Parameters.AddWithValue("@PS", FrEvent.PS == null ? "" : FrEvent.PS);
                    cmd.Parameters.AddWithValue("@BB", FrEvent.BB == null ? "" : FrEvent.BB);

                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString());
                    cmd.Parameters.AddWithValue("@CreatedBy", FrEvent.CreatedBy);
                    cmd.Parameters.AddWithValue("@EventDescription", FrEvent.EventDescription);
                    cmd.Parameters.AddWithValue("@lastDateToRegister", FrEvent.lastDateToRegister);
                    cmd.Parameters.AddWithValue("@RegEndTime", FrEvent.RegEndTime);
                    cmd.Parameters.AddWithValue("@Status", FrEvent.Status);
                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        retVal = 1;
                    }


                }
                else
                {
                    // cmdText = "Update FreeEvent set Year=" + FrEvent.Year + ",ChapterID=" + FrEvent.Chapter + ",EventCode='" + FrEvent.EventCode + "',EventName='" + FrEvent.EventName + "',EventDate='" + FrEvent.EventDate + "',StartTime='" + FrEvent.StartTime + "',EndTime='" + FrEvent.EndTime + "',VenueName='" + FrEvent.VenueName + "',VenueAddress='" + FrEvent.VenueAddress + "',SB='" + FrEvent.SB + "',VB='" + FrEvent.VB + "',MB='" + FrEvent.MB + "',SC='" + FrEvent.SC + "', GB='" + FrEvent.GB + "', EW='" + FrEvent.EW + "', PS='" + FrEvent.PS + "',BB='" + FrEvent.BB + "', ModifiedBy=" + FrEvent.CreatedBy + ", ModifiedDate='" + DateTime.Now.ToShortDateString() + "',EventDescription='" + FrEvent.EventDescription + "'  where FreeEventID=" + FrEvent.FreeEventID + "";


                    cmdText = "Update FreeEvent set Year=@Year,ChapterID=@Chapter,EventCode=@EventCode,EventName=@EventName,EventDate=@EventDate,StartTime=@StartTime,EndTime=@EndTime,VenueName=@VenueName,VenueAddress=@VenueAddress,SB=@SB,VB=@VB,MB=@MB,SC=@SC, GB=@GB, EW=@EW, PS=@PS,BB=@BB, ModifiedDate=@CreatedDate, ModifiedBy=@CreatedBy, EventDescription= @EventDescription, lastDateToRegister=@lastDateToRegister, RegEndTime=@RegEndTime where FreeEventID=@FreeEventID";
                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEvent.Year);
                    cmd.Parameters.AddWithValue("@Chapter", FrEvent.Chapter);
                    cmd.Parameters.AddWithValue("@EventCode", FrEvent.EventCode);
                    cmd.Parameters.AddWithValue("@EventName", FrEvent.EventName);
                    cmd.Parameters.AddWithValue("@EventDate", FrEvent.EventDate);
                    cmd.Parameters.AddWithValue("@StartTime", FrEvent.StartTime);
                    cmd.Parameters.AddWithValue("@EndTime", FrEvent.EndTime);
                    cmd.Parameters.AddWithValue("@VenueName", FrEvent.VenueName);
                    cmd.Parameters.AddWithValue("@VenueAddress", FrEvent.VenueAddress);
                    cmd.Parameters.AddWithValue("@SB", FrEvent.SB == null ? "" : FrEvent.SB);
                    cmd.Parameters.AddWithValue("@VB", FrEvent.VB == null ? "" : FrEvent.VB);
                    cmd.Parameters.AddWithValue("@MB", FrEvent.MB == null ? "" : FrEvent.MB);
                    cmd.Parameters.AddWithValue("@SC", FrEvent.SC == null ? "" : FrEvent.SC);
                    cmd.Parameters.AddWithValue("@GB", FrEvent.GB == null ? "" : FrEvent.GB);
                    cmd.Parameters.AddWithValue("@EW", FrEvent.EW == null ? "" : FrEvent.EW);
                    cmd.Parameters.AddWithValue("@PS", FrEvent.PS == null ? "" : FrEvent.PS);
                    cmd.Parameters.AddWithValue("@BB", FrEvent.BB == null ? "" : FrEvent.BB);

                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString());
                    cmd.Parameters.AddWithValue("@CreatedBy", FrEvent.CreatedBy);
                    cmd.Parameters.AddWithValue("@EventDescription", FrEvent.EventDescription);
                    cmd.Parameters.AddWithValue("@lastDateToRegister", FrEvent.lastDateToRegister);
                    cmd.Parameters.AddWithValue("@RegEndTime", FrEvent.RegEndTime);
                    cmd.Parameters.AddWithValue("@FreeEventID", FrEvent.FreeEventID);


                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        retVal = 1;
                    }
                }


                //SqlCommand cmd = new SqlCommand(cmdText, cn);
                // cmd = new SqlCommand(cmdText, cn);
                // cmd.ExecuteNonQuery();

                //  retVal = 1;
            }
            else
            {
                // retVal = -2;
            }
        }
        catch (Exception ex)
        {

        }
        return retVal;

    }


    [WebMethod]
    public static List<FreeEvent> ListFreeEvents(int Year, int FreeEventID)
    {

        int retVal = -1;
        List<FreeEvent> ObjListFreeEvent = new List<FreeEvent>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select FE.FreeEventID, FE.Year,FE.ChapterID,FE.EventCode,FE.EventName,FE.EventDate,FE.StartTime,FE.EndTime,FE.VenueName,FE.VenueAddress,FE.SB,FE.VB,FE.MB,FE.SC, FE.GB, FE.EW, FE.PS,FE.BB, C.Name, FE.EventDescription, FE.CreatedBy, FE.lastDateToRegister, FE.RegEndTime, FE.Status from FreeEvent FE inner join Chapter C on (FE.ChapterID=C.ChapterID) where FE.Year=" + Year + "";

            if (FreeEventID > 0)
            {
                cmdText += " and FE.FreeEventID=" + FreeEventID + "";
            }


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEvent objFreeEvent = new FreeEvent();
                        objFreeEvent.ChapterName = dr["name"].ToString();
                        objFreeEvent.Year = Convert.ToInt32(dr["year"].ToString());
                        objFreeEvent.EventDate = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("MM/dd/yyyy");
                        objFreeEvent.Day = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("dddd");
                        objFreeEvent.StrDate = Convert.ToDateTime(dr["EventDate"].ToString()).ToString("MMMM dd, yyyy");

                        objFreeEvent.EventName = dr["EventName"].ToString();
                        objFreeEvent.StartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("hh:mm tt");

                        objFreeEvent.StrStartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm");

                        objFreeEvent.EventCode = dr["EventCode"].ToString();
                        objFreeEvent.EndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("hh:mm tt");

                        objFreeEvent.StrEndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("HH:mm");

                        objFreeEvent.VenueName = dr["venuename"].ToString();
                        objFreeEvent.VenueAddress = dr["VenueAddress"].ToString();
                        objFreeEvent.SB = dr["SB"].ToString().Trim();
                        objFreeEvent.VB = dr["VB"].ToString().Trim();
                        objFreeEvent.MB = dr["MB"].ToString().Trim();
                        objFreeEvent.SC = dr["SC"].ToString().Trim();
                        objFreeEvent.GB = dr["GB"].ToString().Trim();
                        objFreeEvent.EW = dr["EW"].ToString().Trim();
                        objFreeEvent.PS = dr["PS"].ToString().Trim();
                        objFreeEvent.BB = dr["BB"].ToString().Trim();
                        objFreeEvent.Chapter = dr["ChapterID"].ToString();
                        objFreeEvent.FreeEventID = dr["FreeEventID"].ToString();
                        objFreeEvent.EventDescription = dr["EventDescription"].ToString();
                        objFreeEvent.CreatedBy = dr["CreatedBy"].ToString();
                        if (dr["lastDateToRegister"].ToString() != "")
                        {
                            objFreeEvent.lastDateToRegister = Convert.ToDateTime(dr["lastDateToRegister"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["RegEndTime"].ToString() != "")
                        {
                            objFreeEvent.RegEndTime = Convert.ToDateTime(dr["RegEndTime"].ToString()).ToString("hh:mm tt").Trim();
                        }
                        if (dr["RegEndTime"].ToString() != "")
                        {
                            objFreeEvent.StrRegEndTime = Convert.ToDateTime(dr["RegEndTime"].ToString()).ToString("hh:mm").Trim();
                        }
                        objFreeEvent.Status = dr["Status"].ToString();


                        ObjListFreeEvent.Add(objFreeEvent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListFreeEvent;

    }


    [WebMethod]
    public static int DeleteFreeEvent(int FreeEventID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " Delete from FreeEvent where FreeEventID=" + FreeEventID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();

            retVal = 1;
        }
        catch (Exception ex)
        {

        }
        return retVal;

    }

    [WebMethod]
    public static int ValidateRegistration(int FreeEventID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " Select count(*) as CountSet from FreeEventReg where FreeEventId=" + FreeEventID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
        }
        catch (Exception ex)
        {

        }
        return retVal;

    }


    [WebMethod]
    public static int SetActieInActiveFreeEvent(FreeEvent FrEvent)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;




            cmdText = "update FreeEvent set [Status]=@Status where FreeEventid =@FreeEventid";

            SqlCommand cmd = new SqlCommand(cmdText);

            cmd.Parameters.AddWithValue("@Status", FrEvent.Status);
            cmd.Parameters.AddWithValue("@FreeEventid", FrEvent.FreeEventID);

            NSFDBHelper objNSF = new NSFDBHelper();
            if (objNSF.InsertUpdateData(cmd) == true)
            {
                retVal = 1;
            }


        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return retVal;

    }


}