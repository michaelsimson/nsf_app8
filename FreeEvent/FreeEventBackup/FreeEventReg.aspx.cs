﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Data;
using System.Security.Cryptography;

public partial class FreeEvent_FreeEventReg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    public class FreeEventReg
    {
        public int MemberID { get; set; }
        public int Year { get; set; }
        public int FreeEventID { get; set; }
        public string ChapterCode { get; set; }
        public string ChapterID { get; set; }
        public string EventCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PartType { get; set; }
        public string Attendees { get; set; }
        public string CreatedDate { get; set; }
        public string Createdby { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Status { get; set; }
        public List<FreeEventChild> Child { get; set; }
        public int Mode { get; set; }
        public string EventName { get; set; }
        public int RetVal { get; set; }
    }
    public class FreeEventChild
    {
        public int ChildID { get; set; }
        public int Year { get; set; }
        public int MemberID { get; set; }
        public int FreeEventID { get; set; }
        public string EventCode { get; set; }
        public string ChildName { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string SB { get; set; }
        public string VB { get; set; }
        public string MB { get; set; }
        public string SC { get; set; }
        public string GB { get; set; }
        public string EW { get; set; }
        public string PS { get; set; }
        public string BB { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDtae { get; set; }
        public int Mode { get; set; }
        public string ProductCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ChapterCode { get; set; }
        public string PartType { get; set; }
    }

    public class Product
    {
        public string ProductCode { get; set; }
        public string Name { get; set; }
    }

    public class Productgroup
    {
        public string ProductGroupID { get; set; }
        public string Name { get; set; }
    }

    [WebMethod]
    public static int PostNewRegistartion(FreeEventReg FrEventReg)
    {

        int retVal = -1;
        SqlCommand cmd;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            string DateValidatetext = "select lastDateToRegister, RegEndTime from FreeEvent where Year=" + FrEventReg.Year + " and FreeEventId=" + FrEventReg.FreeEventID + "";

            SqlCommand cmdDate = new SqlCommand(DateValidatetext, cn);
            SqlDataAdapter daDate = new SqlDataAdapter(cmdDate);
            DataSet dsDate = new DataSet();
            daDate.Fill(dsDate);
            string lastDateToRegister = string.Empty;
            string RegEndTime = string.Empty;
            string lastDateToRegisterAndTime = string.Empty;
            if (null != dsDate)
            {
                if (dsDate.Tables[0].Rows.Count > 0)
                {
                    if (dsDate.Tables[0].Rows[0]["lastDateToRegister"].ToString() != "")
                    {
                        lastDateToRegister = Convert.ToDateTime(dsDate.Tables[0].Rows[0]["lastDateToRegister"]).ToString("MM/dd/yyyy");
                        RegEndTime = dsDate.Tables[0].Rows[0]["RegEndTime"].ToString();
                    }
                }
            }
            DateTime dtEventDate = DateTime.Now;
            string Today = Convert.ToDateTime(DateTime.Now).ToString("MM/dd/yyyy HH:mm:ss");
            DateTime dtTodayDate = Convert.ToDateTime(Today);

            if (lastDateToRegister != "")
            {

                lastDateToRegisterAndTime = lastDateToRegister + " " + RegEndTime;

                dtEventDate = Convert.ToDateTime(lastDateToRegisterAndTime);
                Today = Convert.ToDateTime(DateTime.Now).ToString("MM/dd/yyyy HH:mm:ss");
                dtTodayDate = Convert.ToDateTime(Today);
            }

            if (lastDateToRegister != "" && dtEventDate >= dtTodayDate)
            {

                if (FrEventReg.Mode == 1)
                {
                    // cmdText = " insert into FreeEventReg(Year ,FreeEventID,EventCode,FirstName ,MiddleName ,LastName,Email ,PartType,Attendees ,CreatedDate ,Createdby, Status) values(" + FrEventReg.Year + ", " + FrEventReg.FreeEventID + ", '" + FrEventReg.EventCode + "','" + FrEventReg.FirstName + "', '" + FrEventReg.MiddleName + "', '" + FrEventReg.LastName + "', '" + FrEventReg.Email + "', '" + FrEventReg.PartType + "', " + FrEventReg.Attendees + ", '" + DateTime.Now.ToString() + "', '" + FrEventReg.Createdby + "', '" + FrEventReg.Status + "')";

                    cmdText = "insert into FreeEventReg ( Year ,FreeEventID,EventCode,FirstName ,MiddleName ,LastName,Email ,PartType,Attendees ,CreatedDate ,Createdby, Status) values(@Year,@FreeEventID,@EventCode,@FirstName,@MiddleName,@LastName,@Email,@PartType,@Attendees,@CreatedDate,@Createdby,@Status)";

                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEventReg.Year);
                    cmd.Parameters.AddWithValue("@FreeEventID", FrEventReg.FreeEventID);
                    cmd.Parameters.AddWithValue("@EventCode", FrEventReg.EventCode);
                    cmd.Parameters.AddWithValue("@FirstName", FrEventReg.FirstName);
                    cmd.Parameters.AddWithValue("@MiddleName", FrEventReg.MiddleName);
                    cmd.Parameters.AddWithValue("@LastName", FrEventReg.LastName);
                    cmd.Parameters.AddWithValue("@Email", FrEventReg.Email);
                    cmd.Parameters.AddWithValue("@PartType", FrEventReg.PartType);
                    cmd.Parameters.AddWithValue("@Attendees", FrEventReg.Attendees);

                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToString());
                    cmd.Parameters.AddWithValue("@CreatedBy", FrEventReg.Createdby);
                    cmd.Parameters.AddWithValue("@Status", FrEventReg.Status);
                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        retVal = 1;
                    }

                }
                else if (FrEventReg.Mode == 2)
                {
                    //  cmdText = " Update FreeEventReg set Year =" + FrEventReg.Year + ",FreeEventID=" + FrEventReg.FreeEventID + ",EventCode='" + FrEventReg.EventCode + "',FirstName='" + FrEventReg.FirstName + "' ,MiddleName='" + FrEventReg.MiddleName + "' ,LastName='" + FrEventReg.LastName + "',Email='" + FrEventReg.Email + "' ,PartType='" + FrEventReg.PartType + "',Attendees= " + FrEventReg.Attendees + " ,ModifiedDate ='" + DateTime.Now.ToString() + "', ModifiedBy=" + FrEventReg.MemberID + " where Email='" + FrEventReg.Email + "' and FreeEventID=" + FrEventReg.FreeEventID + "";

                    cmdText = "Update FreeEventReg set Year=@Year ,FreeEventID=@FreeEventID,EventCode=@EventCode,FirstName=@FirstName ,MiddleName= @MiddleName,LastName=@LastName,Email=@Email ,PartType=@PartType,Attendees=@Attendees ,ModifiedDate=@ModifiedDate ,ModifiedBy=@ModifiedBy where Email=@Email and FreeEventID=@FreeEventID";

                    cmd = new SqlCommand(cmdText);
                    cmd.Parameters.AddWithValue("@Year", FrEventReg.Year);
                    cmd.Parameters.AddWithValue("@FreeEventID", FrEventReg.FreeEventID);
                    cmd.Parameters.AddWithValue("@EventCode", FrEventReg.EventCode);
                    cmd.Parameters.AddWithValue("@FirstName", FrEventReg.FirstName);
                    cmd.Parameters.AddWithValue("@MiddleName", FrEventReg.MiddleName);
                    cmd.Parameters.AddWithValue("@LastName", FrEventReg.LastName);
                    cmd.Parameters.AddWithValue("@Email", FrEventReg.Email);
                    cmd.Parameters.AddWithValue("@PartType", FrEventReg.PartType);
                    cmd.Parameters.AddWithValue("@Attendees", FrEventReg.Attendees);

                    cmd.Parameters.AddWithValue("@ModifiedDate", DateTime.Now.ToString());
                    cmd.Parameters.AddWithValue("@ModifiedBy", FrEventReg.MemberID);
                    //cmd.Parameters.AddWithValue("@Email", FrEventReg.Email);
                    //cmd.Parameters.AddWithValue("@FreeEventID", FrEventReg.FreeEventID);

                    NSFDBHelper objNSF = new NSFDBHelper();
                    if (objNSF.InsertUpdateData(cmd) == true)
                    {
                        retVal = 1;
                    }
                }
            }
            else
            {
                retVal = -4;
            }

            //SqlCommand cmd = new SqlCommand(cmdText, cn);
            //cmd = new SqlCommand(cmdText, cn);
            //cmd.ExecuteNonQuery();

            if (retVal == 1)
            {

                cmdText = "delete from FreeEventChild where memberID=" + FrEventReg.MemberID + "";
                cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();

                if (FrEventReg.Mode == 1)
                {
                    cmdText = "SELECT max(MemberId) as MemberID from FreeEventreg";
                }
                else
                {
                    cmdText = "SELECT MemberId as MemberID from FreeEventreg  where Email='" + FrEventReg.Email + "' and FreeEventID=" + FrEventReg.FreeEventID + "";
                }


                cmd = new SqlCommand(cmdText, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (null != ds)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["MemberID"].ToString());
                    }
                }


                if (FrEventReg.Mode == 2)
                {
                    retVal = FrEventReg.MemberID;

                }

            }
        }
        catch (Exception ex)
        {

        }
        return retVal;

    }

    [WebMethod]
    public static int DuplicateExists(FreeEventReg FrEventReg)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " select count(*) as CountSet from FreeEventReg where Email='" + FrEventReg.Email + "' and Year=" + FrEventReg.Year + "";

            if (FrEventReg.FreeEventID > 0)
            {
                cmdText += " and FreeEventID=" + FrEventReg.FreeEventID + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }



        }
        catch (Exception ex)
        {

        }
        return retVal;

    }

    [WebMethod]
    public static int PostNewChild(FreeEventChild FrEventChild)
    {

        int retVal = -1;
        string strCurrentDate = "";
        SqlCommand cmd = new SqlCommand();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            if (FrEventChild.Mode == 1)
            {
                // cmdText = " Insert into FreeEventChild(Year,MemberID ,FreeEventID ,EventCode ,ChildName, SchoolName ,Grade ,SB ,VB ,MB ,SC,GB ,EW,PS ,BB,CreatedDate,CreatedBy) values(" + FrEventChild.Year + ", " + FrEventChild.MemberID + ", " + FrEventChild.FreeEventID + ", '" + FrEventChild.EventCode + "', '" + FrEventChild.ChildName + "', '" + FrEventChild.SchoolName + "', " + FrEventChild.Grade + ", '" + FrEventChild.SB + "', '" + FrEventChild.VB + "', '" + FrEventChild.MB + "','" + FrEventChild.SC + "', '" + FrEventChild.GB + "', '" + FrEventChild.EW + "', '" + FrEventChild.PS + "', '" + FrEventChild.BB + "', '" + DateTime.Now.ToString() + "', " + FrEventChild.MemberID + ")";

                cmdText = "insert into FreeEventChild ( Year,MemberID ,FreeEventID ,EventCode ,ChildName, SchoolName ,Grade ,SB ,VB ,MB ,SC,GB ,EW,PS ,BB,CreatedDate,CreatedBy) values(@Year,@MemberID ,@FreeEventID ,@EventCode ,@ChildName, @SchoolName ,@Grade ,@SB ,@VB ,@MB ,@SC,@GB ,@EW,@PS ,@BB,@CreatedDate,@CreatedBy)";

                cmd = new SqlCommand(cmdText);
                cmd.Parameters.AddWithValue("@Year", FrEventChild.Year);
                cmd.Parameters.AddWithValue("@MemberID", FrEventChild.MemberID);
                cmd.Parameters.AddWithValue("@FreeEventID", FrEventChild.FreeEventID);
                cmd.Parameters.AddWithValue("@EventCode", FrEventChild.EventCode);
                cmd.Parameters.AddWithValue("@ChildName", FrEventChild.ChildName);
                cmd.Parameters.AddWithValue("@SchoolName", FrEventChild.SchoolName);
                cmd.Parameters.AddWithValue("@Grade", FrEventChild.Grade);
                cmd.Parameters.AddWithValue("@SB", FrEventChild.SB == null ? "" : FrEventChild.SB);
                cmd.Parameters.AddWithValue("@VB", FrEventChild.VB == null ? "" : FrEventChild.VB);
                cmd.Parameters.AddWithValue("@MB", FrEventChild.MB == null ? "" : FrEventChild.MB);
                cmd.Parameters.AddWithValue("@SC", FrEventChild.SC == null ? "" : FrEventChild.SC);
                cmd.Parameters.AddWithValue("@GB", FrEventChild.GB == null ? "" : FrEventChild.GB);
                cmd.Parameters.AddWithValue("@EW", FrEventChild.EW == null ? "" : FrEventChild.EW);
                cmd.Parameters.AddWithValue("@PS", FrEventChild.PS == null ? "" : FrEventChild.PS);
                cmd.Parameters.AddWithValue("@BB", FrEventChild.BB == null ? "" : FrEventChild.BB);

                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToString());
                cmd.Parameters.AddWithValue("@CreatedBy", FrEventChild.MemberID);

                NSFDBHelper objNSF = new NSFDBHelper();
                if (objNSF.InsertUpdateData(cmd) == true)
                {
                    retVal = 1;
                }
            }
            else if (FrEventChild.Mode == 2)
            {
                try
                {
                    cmdText = "delete from FreeEventChild where memberID=" + FrEventChild.MemberID + "";


                    cmd = new SqlCommand(cmdText, cn);
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                }
                //    cmdText = " Insert into FreeEventChild(Year,MemberID ,FreeEventID ,EventCode ,ChildName, SchoolName ,Grade ,SB ,VB ,MB ,SC,GB ,EW,PS ,BB,CreatedDate,CreatedBy) values(" + FrEventChild.Year + ", " + FrEventChild.MemberID + ", " + FrEventChild.FreeEventID + ", '" + FrEventChild.EventCode + "', '" + FrEventChild.ChildName + "', '" + FrEventChild.SchoolName + "', " + FrEventChild.Grade + ", '" + FrEventChild.SB + "', '" + FrEventChild.VB + "', '" + FrEventChild.MB + "','" + FrEventChild.SC + "', '" + FrEventChild.GB + "', '" + FrEventChild.EW + "', '" + FrEventChild.PS + "', '" + FrEventChild.BB + "', '" + DateTime.Now.ToString() + "', " + FrEventChild.MemberID + ")";

                cmdText = "insert into FreeEventChild ( Year,MemberID ,FreeEventID ,EventCode ,ChildName, SchoolName ,Grade ,SB ,VB ,MB ,SC,GB ,EW,PS ,BB,CreatedDate,CreatedBy) values(@Year,@MemberID ,@FreeEventID ,@EventCode ,@ChildName, @SchoolName ,@Grade ,@SB ,@VB ,@MB ,@SC,@GB ,@EW,@PS ,@BB,@CreatedDate,@CreatedBy)";

                cmd = new SqlCommand(cmdText);
                cmd.Parameters.AddWithValue("@Year", FrEventChild.Year);
                cmd.Parameters.AddWithValue("@MemberID", FrEventChild.MemberID);
                cmd.Parameters.AddWithValue("@FreeEventID", FrEventChild.FreeEventID);
                cmd.Parameters.AddWithValue("@EventCode", FrEventChild.EventCode);
                cmd.Parameters.AddWithValue("@ChildName", FrEventChild.ChildName);
                cmd.Parameters.AddWithValue("@SchoolName", FrEventChild.SchoolName);
                cmd.Parameters.AddWithValue("@Grade", FrEventChild.Grade);
                cmd.Parameters.AddWithValue("@SB", FrEventChild.SB == null ? "" : FrEventChild.SB);
                cmd.Parameters.AddWithValue("@VB", FrEventChild.VB == null ? "" : FrEventChild.VB);
                cmd.Parameters.AddWithValue("@MB", FrEventChild.MB == null ? "" : FrEventChild.MB);
                cmd.Parameters.AddWithValue("@SC", FrEventChild.SC == null ? "" : FrEventChild.SC);
                cmd.Parameters.AddWithValue("@GB", FrEventChild.GB == null ? "" : FrEventChild.GB);
                cmd.Parameters.AddWithValue("@EW", FrEventChild.EW == null ? "" : FrEventChild.EW);
                cmd.Parameters.AddWithValue("@PS", FrEventChild.PS == null ? "" : FrEventChild.PS);
                cmd.Parameters.AddWithValue("@BB", FrEventChild.BB == null ? "" : FrEventChild.BB);

                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToString());
                cmd.Parameters.AddWithValue("@CreatedBy", FrEventChild.MemberID);

                NSFDBHelper objNSF = new NSFDBHelper();
                if (objNSF.InsertUpdateData(cmd) == true)
                {
                    retVal = 1;
                }

            }


            // cmd = new SqlCommand(cmdText, cn);
            //  cmd.ExecuteNonQuery();

            retVal = 1;

            DateTime dtTodayDate = Convert.ToDateTime(DateTime.Now);

            strCurrentDate = Convert.ToDateTime(dtTodayDate.AddDays(1)).ToString();


        }
        catch (Exception ex)
        {

        }
        return retVal;

    }


    [WebMethod]
    public static int SendEmailsToRegistarants(string Email, string URl, string Mode)
    {

        int retVal = -1;
        string tblHtml = "";
        try
        {
            tblHtml += "<table>";
            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<span>Hi, </span> </br>";
            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";

            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";

            if (Mode == "1")
            {
                tblHtml += "<span>Thank you for registering. Please confirm that you received this email by clicking on the link below. Unless you confirm, you cannot attend the event.</span> </br>";
            }
            else if (Mode == "2")
            {
                tblHtml += "<span>We received a request to update your registration information for North South Foundation's Free Event. Please click the link in this email or copy and paste the entire line to your browser.</span> </br>";
            }
            else
            {
                tblHtml += "<span>Thank you for registering. Please confirm that you received this email by clicking on the link below.</span> </br>";
            }
            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";

            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";

            tblHtml += "<a href=" + URl.Replace("\"", "%22") + ">" + URl.Replace("\"", "%22") + "<a/>";
            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";

            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<span>If clicking on the link does not work, please copy the link and paste into a browser.</span>";
            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "</table>";
            //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";
            string fromEMail = "nsfevents@northsouth.org";
            string subject = "North South Foundation: Confirm your registration for a Free Event";
            if (Mode == "1")
            {
                subject = "North South Foundation: Confirm your registration for a Free Event";
            }
            else if (Mode == "2")
            {
                subject = "Link to update your registration for NSF Free Event";
            }
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromEMail);
                mail.Body = tblHtml;
                mail.Subject = subject;

                // mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

                String[] strEmailAddressList = null;
                String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                Match EmailAddressMatch = default(Match);

                mail.Bcc.Add(new MailAddress(Email.ToString().Trim()));

                //  mail.Bcc.Add(new MailAddress("michael.simson@capestart.com"));

                SmtpClient client = new SmtpClient();
                // client.Host = host;
                mail.IsBodyHtml = true;
                try
                {
                    client.Send(mail);
                    retVal = 1;
                    //lblError.Text = ex.ToString();
                }
                catch (Exception ex)
                {
                }
            }
            catch
            {
            }
        }

        catch (Exception ex)
        {
        }
        return retVal;

    }

    [WebMethod]
    public static string Encrypt(string clearText)
    {
        string date = DateTime.Now.ToShortDateString();
        string time = DateTime.Now.ToShortTimeString();
        string dateTime = date + "-" + time;

        clearText += "&expired=" + dateTime + "";

        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    [WebMethod]
    public static string Decrypt(string cipherText)
    {

        string EncryptionKey = "MAKV2SPBNI99212";
        cipherText = cipherText.Replace(" ", "+");
        cipherText = cipherText.Replace("%22", "");

        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }


    [WebMethod]
    public static List<FreeEventReg> UpdateStatus(string Status, string MemberID)
    {

        int retVal = -1;
        List<FreeEventReg> ObjList = new List<FreeEventReg>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            FreeEventReg ObjReg = new FreeEventReg();

            string cmdText = string.Empty;

            string Date = "";
            string StatusReg = "";

            cmdText = "select FR.CreatedDate, FR.Status, FE.EventName from FreeEventReg FR inner join FreeEvent FE on (FR.FreeEventID=FE.FreeEventID) where MemberID=" + MemberID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Date = Convert.ToDateTime(ds.Tables[0].Rows[0]["CreatedDate"].ToString()).ToString("MM/dd/yyyy HH:mm");
                    StatusReg = ds.Tables[0].Rows[0]["Status"].ToString();
                    ObjReg.EventName = ds.Tables[0].Rows[0]["EventName"].ToString();
                }
            }

            if (StatusReg != "" && StatusReg.Trim() == "Pending")
            {


                DateTime dtValid = Convert.ToDateTime(Date).AddDays(1);
                DateTime dtTodayDate = Convert.ToDateTime(DateTime.Now);

                if (dtValid > dtTodayDate)
                {
                    cmdText = " Update FreeEventReg set Status='" + Status.TrimEnd('"') + "', ModifiedDate='" + DateTime.Now.ToString() + "', ModifiedBy=" + MemberID + " where MemberID=" + MemberID + "";

                    cmd = new SqlCommand(cmdText, cn);
                    cmd = new SqlCommand(cmdText, cn);
                    cmd.ExecuteNonQuery();

                    retVal = 1;
                    ObjReg.RetVal = 1;
                }
                else
                {
                    retVal = -2;
                    ObjReg.RetVal = -1;
                }
            }
            else
            {
                retVal = -3;
                ObjReg.RetVal = -3;
            }



            ObjList.Add(ObjReg);
        }
        catch (Exception ex)
        {

        }
        return ObjList;

    }

    [WebMethod]
    public static List<Productgroup> ListContests(string Year, string EventID, string GradeFrom, string GradeTo)
    {

        int retVal = -1;
        List<Product> ObjPrdList = new List<Product>();

        List<Productgroup> ObjPGList = new List<Productgroup>();


        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            string ProductGroupId = "";

            cmdText = " select case when  replace(SB, ' ', '')='Y' then '8' when VB='Y' then '9' when MB='Y' then '10' when SC='Y' then '26' when GB='Y' then '11' when EW='Y' then '12' when PS='Y' then '13' when BB='Y' then '14'  end as Contest from FreeEvent where Year=" + Year + " and FreeEventID=" + EventID + "";

            cmdText = " select SB, VB, MB, SC, GB, EW, PS, BB from FreeEvent where Year=" + Year + " and FreeEventID=" + EventID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        if (dr["SB"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "8";

                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["VB"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "9";

                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["MB"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "10";
                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["SC"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "26";
                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["GB"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "11";
                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["EW"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "12";
                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["PS"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "13";
                            ObjPGList.Add(ObjPG);
                        }
                        if (dr["BB"].ToString().Trim() == "Y")
                        {
                            Productgroup ObjPG = new Productgroup();
                            ObjPG.ProductGroupID = "14";
                            ObjPGList.Add(ObjPG);
                        }

                        // ProductGroupId += dr["Contest"].ToString() + ",";
                    }
                }
            }

            //ProductGroupId = ProductGroupId.TrimEnd(',');

            //cmdText = "select distinct P.Name, case when P.ProductGroupCode='SB' then '1. SB' when P.ProductGroupCode='VB' then '2. VB' when P.ProductGroupCode='MB' then '3. MB' when P.ProductGroupCode='SC' then '4. SC' when P.ProductGroupCode='GB' then '5. GB' when P.ProductGroupCode='EW' then '6. EW' when P.ProductGroupCode='PS' then '7. PS' when P.ProductGroupCode='BB' then '8. BB' end as Contest from Product P inner join ContestCategory C on (C.ContestCode=P.ProductCode) where P.ProductGroupId  in (" + ProductGroupId + ") and( (C.GradeFrom between " + GradeFrom + " and " + GradeTo + ") or (C.GradeTo between " + GradeFrom + " and " + GradeTo + "))";

            //cmd = new SqlCommand(cmdText, cn);
            //da = new SqlDataAdapter(cmd);
            //DataSet ds1 = new DataSet();
            //da.Fill(ds1);
            //if (null != ds1)
            //{
            //    if (ds1.Tables[0].Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in ds1.Tables[0].Rows)
            //        {
            //            Product prd = new Product();

            //            prd.ProductCode = dr["ProductCode"].ToString();
            //            prd.Name = dr["ProductCode"].ToString();
            //            ObjPrdList.Add(prd);
            //        }
            //    }
            //}


        }
        catch (Exception ex)
        {

        }
        return ObjPGList;

    }

    [WebMethod]
    public static List<FreeEventReg> GetFreeEventregistrations(string MemberID, string Expired)
    {

        int retVal = -1;
        List<FreeEventReg> ObjFrReg = new List<FreeEventReg>();

        List<FreeEventChild> ObjFrChild = new List<FreeEventChild>();

        string DateTimeExpired = Expired.Replace('-', ' ');

        DateTime dtDateTime = Convert.ToDateTime(DateTimeExpired).AddDays(1);
        DateTime dtToday = Convert.ToDateTime(DateTime.Now.ToString());

        try
        {
            if (dtDateTime > dtToday)
            {

                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
                cn.Open();

                string cmdText = string.Empty;

                cmdText = "  select FR.MemberID,FR.Year ,FR.FreeEventID,FR.EventCode ,FR.FirstName ,FR.MiddleName,FR.LastName,FR.Email,FR.PartType ,FR.Attendees ,FR.Status,FC.ChildId, FC.ChildName,FC.Grade, FC.SchoolName,  FC.SB,FC.VB,FC.MB,FC.SC,FC.GB,FC.EW,FC.PS,FC.BB FROM FreeEventReg FR left join FreeEventChild FC on  (FC.MemberID=FR.MemberID) where FR.MemberId='" + MemberID + "'";

                SqlCommand cmd = new SqlCommand(cmdText, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (null != ds)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            FreeEventReg ObjReg = new FreeEventReg();
                            ObjReg.Year = Convert.ToInt32(dr["Year"].ToString());
                            ObjReg.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                            ObjReg.FreeEventID = Convert.ToInt32(dr["FreeEventID"].ToString());
                            ObjReg.EventCode = dr["EventCode"].ToString();
                            ObjReg.FirstName = dr["FirstName"].ToString();
                            ObjReg.MiddleName = dr["MiddleName"].ToString();
                            ObjReg.LastName = dr["LastName"].ToString();
                            ObjReg.Email = dr["Email"].ToString();
                            ObjReg.PartType = dr["PartType"].ToString().Trim();
                            ObjReg.Attendees = dr["Attendees"].ToString();

                            FreeEventChild ObjChild = new FreeEventChild();

                            if (dr["ChildId"].ToString() != "")
                            {
                                ObjChild.ChildID = Convert.ToInt32(dr["ChildId"].ToString());
                            }
                            if (dr["ChildName"].ToString() != "")
                            {
                                ObjChild.ChildName = dr["ChildName"].ToString();
                            }
                            if (dr["Grade"].ToString() != "")
                            {
                                ObjChild.Grade = dr["Grade"].ToString();
                            }
                            if (dr["SchoolName"].ToString() != "")
                            {
                                ObjChild.SchoolName = dr["SchoolName"].ToString();
                            }
                            if (dr["SB"].ToString().Trim() != "")
                            {
                                ObjChild.SB = dr["SB"].ToString().Trim();
                            }
                            if (dr["VB"].ToString().Trim() != "")
                            {
                                ObjChild.VB = dr["VB"].ToString().Trim();
                            }
                            if (dr["MB"].ToString().Trim() != "")
                            {
                                ObjChild.MB = dr["MB"].ToString().Trim();
                            }
                            if (dr["SC"].ToString().Trim() != "")
                            {
                                ObjChild.SC = dr["SC"].ToString().Trim();
                            }
                            if (dr["GB"].ToString().Trim() != "")
                            {
                                ObjChild.GB = dr["GB"].ToString().Trim();
                            }
                            if (dr["EW"].ToString().Trim() != "")
                            {
                                ObjChild.EW = dr["EW"].ToString().Trim();
                            }
                            if (dr["PS"].ToString().Trim() != "")
                            {
                                ObjChild.PS = dr["PS"].ToString().Trim();
                            }
                            if (dr["BB"].ToString().Trim() != "")
                            {
                                ObjChild.BB = dr["BB"].ToString().Trim();
                            }

                            if (dr["ChildId"].ToString() != "")
                            {
                                ObjFrChild.Add(ObjChild);

                                ObjReg.Child = ObjFrChild;
                            }

                            ObjFrReg.Add(ObjReg);
                        }
                    }
                }


            }
            else
            {
                retVal = -2;
            }
        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return ObjFrReg;

    }



    [WebMethod]
    public static List<FreeEventReg> GetmemberID(string Email)
    {

        int retVal = -1;
        List<FreeEventReg> ObjFrReg = new List<FreeEventReg>();

        List<FreeEventChild> ObjFrChild = new List<FreeEventChild>();

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select FR.MemberID, FR.Year, FR.Email, FR.FreeEventID from FreeEventReg FR where FR.Email='" + Email + "'";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEventReg ObjReg = new FreeEventReg();
                        ObjReg.Year = Convert.ToInt32(dr["Year"].ToString());
                        ObjReg.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                        ObjReg.FreeEventID = Convert.ToInt32(dr["FreeEventID"].ToString());
                        ObjReg.Email = dr["Email"].ToString();


                        ObjFrReg.Add(ObjReg);
                    }
                }
            }



        }
        catch (Exception ex)
        {

        }
        return ObjFrReg;

    }

    [WebMethod]
    public static int ValidateStatus(string Email, int FreeEventID, string Year)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " select count(*) as CountSet from FreeEventReg where Email='" + Email + "' and Year=" + Year + " and Status='Confirmed'";

            if (FreeEventID > 0)
            {
                cmdText += " and FreeEventID=" + FreeEventID + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }



        }
        catch (Exception ex)
        {

        }
        return retVal;

    }


    [WebMethod]
    public static List<FreeEventReg> RequestUpdate(string Status, string MemberID, string Expired)
    {

        int retVal = -1;
        List<FreeEventReg> ObjList = new List<FreeEventReg>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            FreeEventReg ObjReg = new FreeEventReg();

            string cmdText = string.Empty;

            string Date = "";

            cmdText = "select FR.CreatedDate, FR.Status, FE.EventName from FreeEventReg FR inner join FreeEvent FE on (FR.FreeEventID=FE.FreeEventID) where MemberID=" + MemberID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    ObjReg.EventName = ds.Tables[0].Rows[0]["EventName"].ToString();
                }
            }


            string DateTimeExpired = Expired.Replace('-', ' ');

            DateTime dtDateTime = Convert.ToDateTime(DateTimeExpired).AddDays(1);
            DateTime dtToday = Convert.ToDateTime(DateTime.Now.ToString());



            if (dtDateTime > dtToday)
            {
                cmdText = " Update FreeEventReg set Status='" + Status.TrimEnd('"') + "', ModifiedDate='" + DateTime.Now.ToString() + "', ModifiedBy=" + MemberID + " where MemberID=" + MemberID + "";


                cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();

                retVal = 1;
                ObjReg.RetVal = 1;
            }
            else
            {
                retVal = -2;
                ObjReg.RetVal = -2;
            }




            ObjList.Add(ObjReg);
        }
        catch (Exception ex)
        {

        }
        return ObjList;

    }

    [WebMethod]
    public static int VerifyStatus(string MemberId)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " select count(*) as CountSet from FreeEventReg where MemberID=" + MemberId + " and Status='Pending'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }



        }
        catch (Exception ex)
        {

        }
        return retVal;

    }


    [WebMethod]
    public static List<FreeEventReg> ListFreeEventregistrations(string Year, string FreeEventID)
    {

        int retVal = -1;
        List<FreeEventReg> ObjFrReg = new List<FreeEventReg>();

        List<FreeEventChild> ObjFrChild = new List<FreeEventChild>();

        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select FR.MemberID,FR.Year ,FR.FreeEventID,FR.EventCode ,FR.FirstName ,FR.MiddleName,FR.LastName,FR.Email,FR.PartType ,FR.Attendees ,FR.Status, C.ChapterCode, FE.ChapterID FROM FreeEventReg FR inner join FreeEvent FE on  (FR.FreeEventID=FE.FreeEventID) inner join Chapter C on (C.ChapterID=FE.ChapterID) where FR.Year=" + Year + " and FR.FreeEventID=" + FreeEventID + " order by Fr.FirstName, FR.LastName ASC";

            // cmdText = "  select FR.MemberID,FR.Year ,FR.FreeEventID,FR.EventCode ,FR.FirstName ,FR.MiddleName,FR.LastName,FR.Email,FR.PartType ,FR.Attendees ,FR.Status,FC.ChildId, FC.ChildName,FC.Grade, FC.SchoolName,  FC.SB,FC.VB,FC.MB,FC.SC,FC.GB,FC.EW,FC.PS,FC.BB FROM FreeEventReg FR left join FreeEventChild FC on  (FC.MemberID=FR.MemberID) where FR.Year=" + Year + " and FR.FreeEventID=" + FreeEventID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEventReg ObjReg = new FreeEventReg();
                        ObjReg.Year = Convert.ToInt32(dr["Year"].ToString());
                        ObjReg.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                        ObjReg.FreeEventID = Convert.ToInt32(dr["FreeEventID"].ToString());
                        ObjReg.EventCode = dr["EventCode"].ToString();
                        ObjReg.FirstName = dr["FirstName"].ToString();
                        ObjReg.MiddleName = dr["MiddleName"].ToString();
                        ObjReg.LastName = dr["LastName"].ToString();
                        ObjReg.Email = dr["Email"].ToString();
                        ObjReg.PartType = dr["PartType"].ToString().Trim();
                        ObjReg.Attendees = dr["Attendees"].ToString();
                        ObjReg.Status = dr["Status"].ToString();
                        ObjReg.ChapterCode = dr["ChapterCode"].ToString();
                        ObjReg.ChapterID = dr["ChapterID"].ToString();


                        ObjFrReg.Add(ObjReg);
                    }
                }
            }



        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return ObjFrReg;

    }

    [WebMethod]
    public static List<FreeEventChild> GetChildrenBasedOnMemberID(string FreeEventID, string MemberID)
    {
        int retVal = -1;

        List<FreeEventChild> ObjFrChild = new List<FreeEventChild>();

        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select FC.ChildId, FC.ChildName,FC.Grade, FC.SchoolName,  FC.SB,FC.VB,FC.MB,FC.SC,FC.GB,FC.EW,FC.PS,FC.BB FROM FreeEventChild FC inner join FreeEventReg FR on  (FC.MemberID=FR.memberId) where FC.MemberID=" + MemberID + " and FR.FreeEventID=" + FreeEventID + " order by FC.ChildName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEventChild ObjChild = new FreeEventChild();
                        ObjChild.ChildID = Convert.ToInt32(dr["ChildID"].ToString());
                        ObjChild.ChildName = dr["ChildName"].ToString();
                        ObjChild.Grade = dr["Grade"].ToString();
                        ObjChild.SchoolName = dr["SchoolName"].ToString();
                        ObjChild.SB = dr["SB"].ToString().Trim();
                        ObjChild.VB = dr["VB"].ToString().Trim();
                        ObjChild.MB = dr["MB"].ToString().Trim();
                        ObjChild.SC = dr["SC"].ToString().Trim();
                        ObjChild.GB = dr["GB"].ToString().Trim();
                        ObjChild.EW = dr["EW"].ToString().Trim();
                        ObjChild.PS = dr["PS"].ToString().Trim();
                        ObjChild.BB = dr["BB"].ToString().Trim();



                        ObjFrChild.Add(ObjChild);
                    }
                }
            }



        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return ObjFrChild;
    }

    [WebMethod]
    public static List<FreeEventChild> GetContestantsReport(string FreeEventID, string Year)
    {
        int retVal = -1;

        List<FreeEventChild> ObjFrChild = new List<FreeEventChild>();

        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "select F.ChildName,F.Year, F.SchoolName,P.ProductCode, FE.EventCode, C.ChapterCode, FR.Email, FR.FirstName, FR.LastName, FR.PArtType, F.ChildID, F.Grade  from FreeEventChild F inner join Product P on (F.SB=P.ProductCode or F.VB=P.ProductCode or F.MB=P.ProductCode or F.SC=P.ProductCode or F.GB=P.ProductCode or F.EW=P.ProductCode or F.PS=P.ProductCode or F.BB=P.ProductCode) inner join FreeEvent FE on (FE.FreeEventID=F.FreeEventID) inner join Chapter C on (C.ChapterID=FE.ChapterID) inner join FreeEventReg FR on (FR.MemberID=F.MemberID)  where P.EventID=2 and F.FreeEventID=" + FreeEventID + " and F.Year=" + Year + " order by ChildName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        FreeEventChild ObjChild = new FreeEventChild();
                        ObjChild.ChildID = Convert.ToInt32(dr["ChildID"].ToString());
                        ObjChild.ChildName = dr["ChildName"].ToString();
                        ObjChild.Grade = dr["Grade"].ToString();
                        ObjChild.SchoolName = dr["SchoolName"].ToString();
                        ObjChild.ProductCode = dr["ProductCode"].ToString();
                        ObjChild.EventCode = dr["EventCode"].ToString();
                        ObjChild.FirstName = dr["FirstName"].ToString();
                        ObjChild.Year = Convert.ToInt32(dr["Year"].ToString());

                        ObjChild.LastName = dr["LastName"].ToString();
                        ObjChild.Email = dr["Email"].ToString();
                        ObjChild.PartType = dr["PartType"].ToString().Trim();
                        ObjChild.ChapterCode = dr["ChapterCode"].ToString();

                        ObjFrChild.Add(ObjChild);
                    }
                }
            }



        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return ObjFrChild;
    }

    [WebMethod]
    public static int GetContestantsCount(string Year)
    {

        int retVal = 0;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = " select count(*) as ContestantCount from freeEventChild where Year=" + Year + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ContestantCount"].ToString());
                }
            }



        }
        catch (Exception ex)
        {

        }
        return retVal;

    }




}