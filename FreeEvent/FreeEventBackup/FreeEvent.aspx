﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeEvent.aspx.cs" Inherits="FreeEvent_FreeEvent" MasterPageFile="~/NSFInnerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>

    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />

    <link href="../css/Loader.css" rel="stylesheet" />


    <%-- <script src="../Scripts/angular.min.js"></script>


    <script src="../Scripts/angular-route.min.js"></script>
    <script src="../Scripts/angular-sanitize.min.js"></script>
    <script src="../Scripts/FreeEvent/App.js"></script>
    
    <script src="../Scripts/FreeEvent/FreeEventCtrl.js"></script>
    <script src="../Scripts/FreeEvent/Common/CommonService.js"></script>--%>
    <style type="text/css">
        .bs-example {
            margin: 20px;
        }
        /* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
        .form-horizontal .control-label {
            padding-top: 7px;
        }

        .clear {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        var arrContest = [];

        $(function (e) {



            $('#dateRangePicker').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });

            $('#dvEndDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });
            alert("");
            populateYear();

            listChapter();
            loadProductGroups();
            listFreeEvents(0);
        });

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }

        function loadProductGroups() {

            showLoader();
            var eventId = 2;

            var jsonData = JSON.stringify({ ProductGroup: { EventID: 2 } });

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListProductGroup",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;

                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {
                            i++;
                            dvHtml += "<div style='width:170px; float:left;'>";
                            dvHtml += ' <div style="float: left; margin-left: 30px;">';
                            dvHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductGroupID + ' id="rbtn' + value.ProductGroupID + '" />';
                            dvHtml += ' </div>';
                            dvHtml += '<div style="float: left; margin-left: 10px;">';
                            dvHtml += ' <label for="InputName" class="control-label">' + value.ProductGroupName + '</label>';
                            dvHtml += ' </div>'
                            dvHtml += ' </div>'

                            if (i == 4) {
                                dvHtml += '<div style="clear:both;"></div>';
                            }

                        });

                        $("#dvProductGroup").html(dvHtml);
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function listChapter() {
            var chapterId = GetParameterValues("ChapterId");
            var jsonData = JSON.stringify({ Chapter: { ChapterID: chapterId } });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListChapter",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d.length) > 0) {


                        $.each(data.d, function (index, value) {


                            $("#selChapter").append($("<option></option>").val
          (value.ChapterID).html(value.ChapterName));

                        });
                        var chapterID = GetParameterValues("ChapterId");
                        $("#selChapter").val(chapterID);
                        $("#selChapter").attr("disabled", "disabled");
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function validateEvent() {
            var retval = 1;
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var startDate = $("#txtStartDate").val();
            var startTime = $("#txtStartTime").val();
            var endTime = $("#txtEndTime").val();
            var venueName = $("#txtVenueName").val();
            var venueAddr = $("#txtVenueAddr").val();
            var eventDesc = $("#txtEventDescription").val();

            var lastDateToRegister = $("#txtEndDate").val();
            var regEndTime = $("#selEnDateEndTime").val();

            if (chapterId == "") {
                retval = "-1";
                statusMessage("Please select Chapter");
            } else if (eventCode == "") {
                retval = "-1";
                statusMessage("Please enter Event Code");
            } else if (eventName == "") {
                retval = "-1";
                statusMessage("Please enter Event Name");
            } else if (eventDesc == "") {
                retval = "-1";
                statusMessage("Please enter Event Description");
            } else if (startDate == "") {
                retval = "-1";
                statusMessage("Please enter Start Date");
            } else if (startTime == "") {
                retval = "-1";
                statusMessage("Please enter Start Time");
            } else if (endTime == "") {
                retval = "-1";
                statusMessage("Please enter End Time");
            } else if (lastDateToRegister == "") {
                retval = "-1";
                statusMessage("Please enter Registration End Date");
            } else if (regEndTime == "0") {
                retval = "-1";
                statusMessage("Please enter Registration End Time");
            } else if (venueName == "") {
                retval = "-1";
                statusMessage("Please enter Venue Name");
            } else if (venueAddr == "") {
                retval = "-1";
                statusMessage("Please enter Venue Address");
            } else if (arrContest.length <= 0) {
                retval = "-1";
                statusMessage("Please select Contests");
            }
            //if (startTime != "") {
            //    if (validateTime($("#txtStartTime").val()) == false) {
            //        retval = "-1";
            //        statusMessage("Please enter valid Start Time.");
            //    }
            //}
            //if (endTime != "") {
            //    if (validateTime(endTime) == false) {
            //        retval = "-1";
            //        statusMessage("Please enter valid End Time.");
            //    }
            //}
            if (venueAddr.length > 128) {
                retval = "-1";
                statusMessage("Venue Address could not be more than 128 character.");
            }

            if (startTime == endTime) {
                retval = "-1";
                statusMessage("Start Time and End Time could not be same.");
            }

            return retval;
        }

        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        function statusMessage(message) {
            $().toastmessage('showToast', {
                text: message,
                sticky: true,
                position: 'top-right',
                type: 'error',
                close: function () { console.log("toast is closed ..."); }
            });
        }

        $(document).on("click", "#btnSave", function (e) {



            if (validateEvent() == 1) {
                if ($("#hdnFreeEventID").val() == "0") {
                    duplictateCheck();
                } else {
                    postNewEvent();
                }
            }

        });

        $(document).on("keyup", "#txtVenueAddr", function (e) {

            if ($(this).length == 128) {
                statusMessage("Venue Address could not be more than 128 character.");
            }

        });

        function postNewEvent() {

            var year = $("#selyear").val();
            var eventCode = $("#txtEventCode").val();
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var startDate = $("#txtStartDate").val();
            var startTime = $("#txtStartTime").val();
            var endTime = $("#txtEndTime").val();
            var venueName = $("#txtVenueName").val();
            var venueAddr = $("#txtVenueAddr").val();
            var eventDesc = $("#txtEventDescription").val();
            var eventEndDate = $("#txtEndDate").val();
            var eventDateEndTime = $("#selEnDateEndTime").val();
            var SB = null;
            var VB = null;
            var MB = null;
            var GB = null;
            var EW = null;
            var PS = null;
            var BB = null;
            var SC = null;
            var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;



            if (arrContest.indexOf("8") >= 0) {
                SB = "Y";
            }
            if (arrContest.indexOf("9") >= 0) {


                VB = "Y";
            }
            if (arrContest.indexOf("10") >= 0) {
                MB = "Y";
            }
            if (arrContest.indexOf("11") >= 0) {
                GB = "Y";
            }
            if (arrContest.indexOf("12") >= 0) {
                EW = "Y";
            }
            if (arrContest.indexOf("13") >= 0) {
                PS = "Y";
            }
            if (arrContest.indexOf("14") >= 0) {
                BB = "Y";
            }
            if (arrContest.indexOf("26") >= 0) {
                SC = "Y";
            }


            var jsonData = JSON.stringify({ FrEvent: { EventCode: eventCode, EventName: eventName, Chapter: chapterId, Year: year, EventDate: startDate, StartTime: startTime, EndTime: endTime, VenueName: venueName, VenueAddress: venueAddr, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, FreeEventID: $("#hdnFreeEventID").val(), CreatedBy: userId, EventDescription: eventDesc, lastDateToRegister: eventEndDate, RegEndTime: eventDateEndTime } });

            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/PostNewEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $("#hdnFreeEventID").val("0")

                    if (JSON.stringify(data.d) > 0) {

                        $().toastmessage('showToast', {
                            text: 'Saved successfully',
                            sticky: true,
                            position: 'top-center',
                            type: 'success',
                            close: function () { console.log("toast is closed ..."); }
                        });
                        arrContest = [];
                        listFreeEvents(0);
                        hideLoader();
                        reset();
                    } else {
                        if (JSON.stringify(data.d) == -2) {
                            statusMessage("Start Time should be greater than current time");
                        } else if (JSON.stringify(data.d) == -1) {
                            statusMessage("Operation failed. Please try again.");
                        } else if (JSON.stringify(data.d) == -3) {
                            statusMessage("End Time should be greater than start time");
                        } else if (JSON.stringify(data.d) == -4) {
                            statusMessage(" Registration End Date should be less than Event Start Date and greater than Current Date");
                        }
                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }

            });

        }


        function duplictateCheck() {
            var chapterId = $("#selChapter").val();
            var eventCode = $("#txtEventCode").val();
            var eventName = $("#txtEventName").val();
            var year = $("#selyear").val();
            var eventDate = $("#txtStartDate").val();
            var eventEndDate = $("#txtEndDate").val();
            var eventDateEndTime = $("#selEnDateEndTime").val();

            var jsonData = JSON.stringify({ FrEvent: { EventCode: eventCode, EventName: eventName, Chapter: chapterId, Year: year, EventDate: eventDate, lastDateToRegister: eventEndDate, RegEndTime: eventDateEndTime } });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/IsDuplicateExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {

                        statusMessage("Event already exists.");
                        hideLoader();
                    } else {
                        postNewEvent();

                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", ".active", function (e) {

            var freeEventid = $(this).attr("attr-FreeEventID");
            listFreeEvents(freeEventid);
            if (confirm("Are you sure to activate the event " + $("#hdnFreeEventname").val() + "?")) {
                setEventActiveInActive(freeEventid, "Active");
            }
        });
        $(document).on("click", ".inActive", function (e) {
            var freeEventid = $(this).attr("attr-FreeEventID");
            listFreeEvents(freeEventid);
            if (confirm("Are you sure to deactivate the event " + $("#hdnFreeEventname").val() + "?")) {
                setEventActiveInActive(freeEventid, "Inactive");
            }
        });

        function setEventActiveInActive(freeEventid, status) {

            var jsonData = JSON.stringify({ FrEvent: { Status: status, FreeEventID: freeEventid } });
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/SetActieInActiveFreeEvent",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (JSON.stringify(data.d) == 1) {
                        $().toastmessage('showToast', {
                            text: 'Updated successfully.',
                            sticky: true,
                            position: 'top-center',
                            type: 'success',
                            close: function () { console.log("toast is closed ..."); }
                        });
                        listFreeEvents(0);
                    } else {
                    }
                }
            });
        }



        function listFreeEvents(freeEventID) {
            var year = $("#selyear").val();
            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;

                    if (freeEventID == 0) {
                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Ser#</th>";
                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Chapter</th>";
                        tblHtml += "<th>Event Code</th>";
                        tblHtml += " <th>Event Name</th>";
                        tblHtml += " <th>Event Description</th>";
                        tblHtml += "<th>Event Date</th>";
                        tblHtml += "<th>Start Time</th>";
                        tblHtml += "<th>End Time</th>";
                        tblHtml += "<th>Registration End Date</th>";
                        tblHtml += "<th>Registration End Time</th>";
                        tblHtml += "<th style='width:150px;'>Status</th>";
                        tblHtml += "<th>Venue</th>";
                        tblHtml += "<th>Venue Address</th>";

                        tblHtml += "<th>SB</th>";
                        tblHtml += "<th>VB</th>";
                        tblHtml += "<th>MB</th>";
                        tblHtml += "<th>SC</th>";
                        tblHtml += "<th>GB</th>";
                        tblHtml += "<th>EW</th>";
                        tblHtml += "<th>PS</th>";
                        tblHtml += "<th>BB</th>";

                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        var i = 0;
                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {

                                i++;
                                tblHtml += "<tr>";

                                tblHtml += "<tr>";
                                tblHtml += "<td>" + (i) + "</td>";
                                var color = "blue";
                                var classStr = "modify";
                                var deleteStrt = "delete";
                                var cursor = "pointer";
                                var deleteColor = "red";
                                var activeStr = "active";
                                var activeTitle = "Active";
                                var activeColor = "blue";

                                if (value.Status == "" || value.Status == "Inactive") {
                                    activeStr = "active";
                                    activeTitle = "Active";

                                } else {
                                    activeStr = "inActive";
                                    activeTitle = "Inactive";
                                }

                                if (value.CreatedBy != userId) {
                                    color = "Grey";
                                    classStr = "modifyDisabled";
                                    deleteStrt = "deleteDisabled";
                                    cursor = "normal";
                                    deleteColor = "Grey";
                                    activeStr = "activeDisabled";
                                    activeColor = "grey";

                                }



                                tblHtml += '<td ><div style="width:100px;"><div style="float:left;"><a attr-FreeEventID=' + value.FreeEventID + ' class=' + classStr + ' title="Modify" style="cursor:' + cursor + ';"><i style="color:' + color + ';" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div><div style="float:left; position:relative; left:10px;"><a style="cursor:' + cursor + ';" attr-FreeEventID=' + value.FreeEventID + ' class=' + deleteStrt + ' title="Delete"><i class="fa fa-trash-o fa-2x" style="color:' + deleteColor + ';" aria-hidden="true"></i></a></div> <div style="float:left; position:relative; left:22px;"><a style="cursor:pointer;" attr-FreeEventID=' + value.FreeEventID + ' class=' + activeStr + ' title=' + activeTitle + '><i style="color:' + activeColor + '"; class="fa fa-power-off fa-2x" aria-hidden="true"></i></a></div></div></td>';

                                tblHtml += "<td>" + value.ChapterName + "</td>";
                                tblHtml += "<td>" + value.EventCode + "</td>";
                                tblHtml += "<td>" + value.EventName + "</td>";
                                $("#hdnFreeEventname").val(value.EventName);
                                tblHtml += "<td>" + value.EventDescription + "</td>";
                                tblHtml += "<td>" + value.EventDate + "</td>";
                                tblHtml += "<td>" + value.StartTime + "</td>";
                                tblHtml += "<td>" + value.EndTime + "</td>";
                                tblHtml += "<td>" + value.lastDateToRegister + "</td>";
                                tblHtml += "<td>" + value.RegEndTime + "</td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.Status + "</span></td>";
                                tblHtml += "<td>" + value.VenueName + "</td>";
                                tblHtml += "<td>" + value.VenueAddress + "</td>";

                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.SB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.VB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.MB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.SC + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.GB + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.EW + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.PS + "</span></td>";
                                tblHtml += "<td><span style='position:relative; left:10px;'>" + value.BB + "</span></td>";


                                tblHtml += "</tr>";

                            });
                            hideLoader();

                        } else {

                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='17' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";
                            hideLoader();
                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);
                    } else {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selChapter").val(value.Chapter);
                                $("#selyear").val(value.Year);

                                $("#txtEventCode").val(value.EventCode);

                                $("#txtEventName").val(value.EventName);
                                $("#txtStartDate").val(value.EventDate);
                                $("#txtStartTime").val(value.StrStartTime);
                                $("#txtEndTime").val(value.StrEndTime);
                                $("#txtVenueName").val(value.VenueName);
                                $("#txtVenueAddr").val(value.VenueAddress);
                                $("#txtEventDescription").val(value.EventDescription);

                                $("#txtEndDate").val(value.lastDateToRegister);
                                $("#selEnDateEndTime").val(value.StrRegEndTime);

                                if (value.SB == "Y") {

                                    $("#rbtn8").prop("checked", true);
                                    arrContest.push($("#rbtn8").val());
                                }
                                if (value.VB == "Y") {

                                    $("#rbtn9").prop("checked", true);
                                    arrContest.push($("#rbtn9").val());
                                }
                                if (value.MB == "Y") {

                                    $("#rbtn10").prop("checked", true);
                                    arrContest.push($("#rbtn10").val());
                                }
                                if (value.SC == "Y") {

                                    $("#rbtn26").prop("checked", true);
                                    arrContest.push($("#rbtn26").val());
                                }
                                if (value.GB == "Y") {

                                    $("#rbtn11").prop("checked", true);
                                    arrContest.push($("#rbtn11").val());
                                }
                                if (value.EW == "Y") {

                                    $("#rbtn12").prop("checked", true);
                                    arrContest.push($("#rbtn12").val());
                                }
                                if (value.PS == "Y") {

                                    $("#rbtn13").prop("checked", true);
                                    arrContest.push($("#rbtn13").val());
                                }
                                if (value.BB == "Y") {

                                    $("#rbtn14").prop("checked", true);
                                    arrContest.push($("#rbtn14").val());
                                }

                            });
                            hideLoader();
                        } else {
                            hideLoader();
                        }
                    }
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
            }

            function reset() {
                $("#txtEventCode").val("");
                $("#txtEventCode").val("");
                $("#txtEventName").val("");
                $("#txtStartDate").val("");
                $("#txtStartTime").val("");
                $("#txtEndTime").val("");
                $("#txtVenueName").val("");
                $("#txtVenueAddr").val("");
                $("#txtEventDescription").val("");
                $("#txtEndDate").val("");
                $("#selEnDateEndTime").val("0");
                arrContest = [];
                $(".rbtnContests").removeAttr("checked");

                $("#hdnFreeEventID").val("");
                //  listChapter();
            }

            $(document).on("click", "#btnReset", function (e) {

                reset();
            });

            $(document).on("click", ".modify", function (e) {
                var freeEventID = $(this).attr("attr-FreeEventID");
                $("#hdnFreeEventID").val(freeEventID);
                listFreeEvents(freeEventID);
            });

            $(document).on("click", ".delete", function (e) {
                var freeEventID = $(this).attr("attr-FreeEventID");
                $("#hdnFreeEventID").val(freeEventID);
                if (confirm("Are you sure want to delete?")) {
                    validateRegistration(freeEventID);
                }
            });

            function deleteFreeEvent(freeEventID) {
                var jsonData = JSON.stringify({ FreeEventID: freeEventID });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "FreeEvent.aspx/DeleteFreeEvent",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {


                        if (JSON.stringify(data.d) > 0) {

                            $().toastmessage('showToast', {
                                text: 'Deleted successfully',
                                sticky: true,
                                position: 'top-center',
                                type: 'success',
                                close: function () { console.log("toast is closed ..."); }
                            });
                            hideLoader();
                            listFreeEvents(0);

                        } else {
                            hideLoader();
                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                        hideLoader();
                    }
                });
            }

            function validateTime(time) {

                var regexp = /([01][0-9]|[02][0-3]):[0-5][0-9]/;
                var correct = regexp.test(time);
                return correct;
            }

            function validateRegistration(freeEventID) {

                var jsonData = JSON.stringify({ FreeEventID: freeEventID });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "FreeEvent.aspx/ValidateRegistration",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {


                        if (JSON.stringify(data.d) <= 0) {

                            deleteFreeEvent(freeEventID);


                        } else {
                            $().toastmessage('showToast', {
                                text: 'This event cannot be deleted since people have already registered.',
                                sticky: true,
                                position: 'top-center',
                                type: 'error',
                                close: function () { console.log("toast is closed ..."); }
                            });
                            hideLoader();
                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                        hideLoader();
                    }
                });

            }
            function populateYear() {
                var d = new Date();
                var year = d.getFullYear();
                var prevyear = parseInt(year) - 1;
                var futureYear = parseInt(year) + 1;
                alert(prevyear);
                alert(year);
                alert(futureYear);

                $("#selyear").append($("<option></option>").val
                    (prevyear).html(prevyear));
                $("#selyear").append($("<option></option>").val
                    (year).html(year));
                $("#selyear").append($("<option></option>").val
                    (futureYear).html(futureYear));
            }
    </script>

    <div class="container">
        <div style="float: left;">

            <a href="../VolunteerFunctions.aspx">Back to Volunteer Functions</a>
        </div>
        <div class="page-header">
            <center>
                <h3 style="color: #00a0b0; font-weight: bold; font-family: 'Trebuchet MS'">Set Up a Free Event</h3>

            </center>
        </div>
        <div class="bs-example" style="width: 80%; margin: auto;">



            <form role="form" class="form-horizontal">
                <div>
                    <div class="well well-sm" style="float: right;"><strong>All fields are mandatory</strong></div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Chapter</label>
                        <div class="col-md-8">

                            <select id="selChapter" class="form-control" required>
                            </select>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Year</label>
                        <div class="col-md-8">

                            <select id="selyear" class="form-control" required>
                                <option value="0">Select</option>
                              <%--  <option value="2017" selected="selected">2017</option>
                                <option value="2016">2016</option>--%>
                            </select>
                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Code</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="InputName" id="txtEventCode" placeholder="Enter Event Code" required>
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="InputName" id="txtEventName" placeholder="Enter Event Name" required style="width: 375px;">
                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Event Description</label>
                        <div class="col-md-8">
                            <textarea name="InputMessage" id="txtEventDescription" class="form-control" rows="2" style="width: 375px;"></textarea>
                        </div>

                    </div>

                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Start Date</label>
                        <div class="col-md-7 input-group input-append date" style="padding-left: 15px;" id="dateRangePicker">
                            <input type="text" class="form-control" name="date" id="txtStartDate" placeholder="Enter Start Date">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">Start Time</label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="txtStartTime" class="form-control" required>
                                <option value="0">Select</option>
                                <option value="6:00">6:00 AM</option>
                                <option value="6:30">6:30 AM</option>
                                <option value="7:00">7:00 AM</option>
                                <option value="7:30">7:30 AM</option>
                                <option value="8:00">8:00 AM</option>
                                <option value="8:30">8:30 AM</option>
                                <option value="9:00">9:00 AM</option>
                                <option value="9:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>

                            </select>


                        </div>

                    </div>



                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">End Time</label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="txtEndTime" class="form-control" required>
                                <option value="0">Select</option>
                                <option value="6:00">6:00 AM</option>
                                <option value="6:30">6:30 AM</option>
                                <option value="7:00">7:00 AM</option>
                                <option value="7:30">7:30 AM</option>
                                <option value="8:00">8:00 AM</option>
                                <option value="8:30">8:30 AM</option>
                                <option value="9:00">9:00 AM</option>
                                <option value="9:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>
                            </select>

                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-md-4">Registration End Date</label>
                        <div class="col-md-7 input-group input-append date" style="padding-left: 15px;" id="dvEndDate">
                            <input type="text" class="form-control" name="date" id="txtEndDate" placeholder="Enter End Date">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <div class="form-group col-md-3">
                        <label for="InputName" class="control-label col-md-4" style="padding: 0px;">End Time</label>
                        <div class="col-md-8" style="padding: 0 0 0 2px;">
                            <select id="selEnDateEndTime" class="form-control" required>
                                <option value="0">Select</option>
                                <option value="6:00">6:00 AM</option>
                                <option value="6:30">6:30 AM</option>
                                <option value="7:00">7:00 AM</option>
                                <option value="7:30">7:30 AM</option>
                                <option value="8:00">8:00 AM</option>
                                <option value="8:30">8:30 AM</option>
                                <option value="9:00">9:00 AM</option>
                                <option value="9:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                                <option value="22:30">10:30 PM</option>
                                <option value="23:00">11:00 PM</option>
                                <option value="23:30">11:30 PM</option>

                            </select>


                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-xs-4">Venue name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="InputName" id="txtVenueName" placeholder="Enter Venue Name" required style="width: 375px;">
                        </div>

                    </div>
                    <div class="form-group col-md-6 clear">
                        <label for="InputName" class="control-label col-xs-4">Venue Address</label>
                        <div class="col-xs-8">
                            <textarea name="InputMessage" id="txtVenueAddr" class="form-control" rows="3" style="width: 375px;" required></textarea>

                            <%-- <input type="text" class="form-control" maxlength="128" name="InputName" id="txtVenueAddr" placeholder="Enter Venue Address" required style="width: 375px;">--%>
                        </div>


                    </div>
                    <div class="form-group col-md-6 clear">
                        <div class="col-md-8">
                            <label for="InputName" class="control-label">Contests Offered</label>
                        </div>
                    </div>

                    <div id="dvProductGroup" class="clear">
                    </div>

                    <div class="clear" style="margin-bottom: 10px;"></div>
                    <div class="form-group col-md-6 clear">
                        <div class="col-md-8">
                            <input type="button" id="btnSave" value="Save" class="btn btn-info" name="submit" />
                            <input type="button" id="btnReset" value="Reset" class="btn btn-info" name="Reset" />
                            <%--   <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info">--%>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-lg-5 col-md-push-1" style="display: none;">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <strong><span class="glyphicon glyphicon-ok"></span>Success! Message sent.</strong>
                    </div>
                    <div class="alert alert-danger">
                        <span class="glyphicon glyphicon-remove"></span><strong>Error! Please check all page inputs.</strong>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear" style="margin-bottom: 10px;"></div>
        <center><b>Table 1: Free Event</b></center>
        <div style="width: 1150px; overflow-x: scroll;">
            <table id="table" style="width: 100%; overflow-x: scroll;">
                <%--  <thead>
                <tr>
                    <th>Action</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Class Stype</th>
                    <th>Week#</th>
                    <th>Status</th>
                    <th>Makeup</th>
                    <th>Substitute</th>
                    <th>Reason</th>
                    <th>HwRelDate</th>
                    <th>HWDueDate</th>
                    <th>SRelDate</th>
                    <th>ARelDate</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <button id="Button1" style="padding: 5px; font-size: 16px; cursor: pointer;">Modify</button></td>
                    <td>03/25/2017</td>
                    <td>6:00 PM</td>
                    <td>Regular</td>
                    <td>1</td>
                    <td>On</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>--%>
            </table>
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
            <div class="sk-fading-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
        <div id="overlay"></div>
        <input type="hidden" id="hdnFreeEventID" value="0" />
        <input type="hidden" id="hdnFreeEventname" value="0" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
</asp:Content>
