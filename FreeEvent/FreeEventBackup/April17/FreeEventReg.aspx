﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeEventReg.aspx.cs" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="FreeEvent_FreeEventReg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%--<link href="../css/wickedpicker.css" rel="stylesheet" />
    <script src="../Scripts/FreeEvent/wickedpicker.js"></script>--%>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <link href="../Bootsstrap/Css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/HtmlGridTable.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="../Bootsstrap/Js/bootstrap.min.js"></script>


    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />

    <link href="../css/Loader.css" rel="stylesheet" />
    <script src="../js/jquery.toast.js"></script>


    <style type="text/css">
        .bs-example {
            margin: 20px;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
        }

        .clear {
            clear: both;
        }
    </style>

    <script type="text/javascript">

        var arrContest = [];
        var contestData;



        $(function (e) {


            $("#dvUpdateRegistration").hide();
            var year = $("#selyear").val();

            listFreeEvents(0, year);
            //  fillGrade("selGrade1");

            var id = getParameterByName("GUID");

            if (id != "" && id != undefined) {

                $("#hdnisUpdate").val("1");
                dcryptURL(id);

            } else {
                // addChild(index++);
            }
            getAvailableSeats();



        });

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function getDecryptedValue(URL, param) {
            var url = URL.slice(URL.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }


        function loadProductGroups(gradeFrom, gradeTo, childJSON) {

            showLoader();
            $("#dvContest").show()
            var eventId = 2;
            var year = $("#selyear").val();

            var freeEventID = $("#selEventCode").val();

            var jsonData = JSON.stringify({ Year: year, EventID: freeEventID, GradeFrom: gradeFrom, GradeTo: gradeTo });

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/ListContests",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var dvHtml = "";
                    var i = 0;
                    contestDate = data;
                    if (JSON.stringify(data.d.length) > 0) {

                        var dHtml = "";
                        $.each(data.d, function (index, value) {

                            //dvHtml += "<div style='width:170px; float:left;'>";
                            //dvHtml += ' <div style="float: left; margin-left: 30px;">';
                            //dvHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductCode + ' id="chk' + value.ProductCode + '" />';
                            //dvHtml += ' </div>';
                            //dvHtml += '<div style="float: left; margin-left: 10px;">';
                            //dvHtml += ' <label for="InputName" class="control-label">' + value.Name + '</label>';
                            //dvHtml += ' </div>'
                            //dvHtml += ' </div>'

                            //if (i == 4) {
                            //    dvHtml += '<div style="clear:both;"></div>';
                            //}

                            if (getContestsCodesbasedOnGrade(value.ProductGroupID, gradeFrom) != "") {

                                i++;

                                dHtml += "<div style='margin-left:0px;'>";
                                dHtml += "<div style='width:235px; float:left;'>";
                                dHtml += ' <div style="float: left;">';


                                dHtml += '<input type="checkbox" class="rbtnContests" value=' + value.ProductGroupID + ' id="chk' + value.ProductGroupID + '" />';
                                dHtml += ' </div>';
                                dHtml += '<div style="float: left; margin-left: 10px;">';
                                dHtml += ' <label for="InputName" class="control-label">' + getContestsbasedOnGrade(value.ProductGroupID, gradeFrom) + '</label>';
                                dHtml += ' </div>'
                                dHtml += ' </div>'
                                dHtml += ' </div>'



                            }

                        });

                        $("#dvContest").html(dHtml);
                        if (childJSON != "") {


                            $.each(childJSON, function (index1, value1) {
                                if (index1 == 0) {

                                    if (value1.SB != "" && value1.SB != null) {

                                        arrContest.push("8");
                                        $("#chk8").prop("checked", true);

                                    }

                                    if (value1.VB != "" && value1.VB != null) {
                                        arrContest.push("9");
                                        $("#chk9").prop("checked", true);
                                    }

                                    if (value1.MB != "" && value1.MB != null) {
                                        arrContest.push("10");
                                        $("#chk10").prop("checked", true);
                                    }

                                    if (value1.SC != "" && value1.SC != null) {
                                        arrContest.push("26");
                                        $("#chk26").prop("checked", true);
                                    }

                                    if (value1.GB != "" && value1.GB != null) {
                                        arrContest.push("11");
                                        $("#chk11").prop("checked", true);
                                    }

                                    if (value1.EW != "" && value1.EW != null) {
                                        arrContest.push("12");
                                        $("#chk12").prop("checked", true);
                                    }

                                    if (value1.PS != "" && value1.PS != null) {
                                        arrContest.push("13");
                                        $("#chk13").prop("checked", true);
                                    }

                                    if (value1.BB != "" && value1.BB != null) {
                                        arrContest.push("14");
                                        $("#chk14").prop("checked", true);
                                    }

                                }

                            });


                        }
                        hideLoader();
                    } else {

                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("change", "#selGrade1", function (e) {
            var grade = $(this).val();

            if (grade != "0") {
                var grades = "";

                var dHtml = "";

                var year = $("#selyear").val();

                var freeEventID = $("#selEventCode").val();

                if (freeEventID == "0") {
                    $(this).val("0");
                    statusMessage("Please select Event Code", "middle-center", "error");

                } else {
                    loadProductGroups(grade, grade, "");
                }

                var childName = $("#txtChildName").val();
                var schoolName = $("#txtSchoolName").val();

                if (schoolName != "" && childName != "" && $("#selEventCode").val() != "0") {
                    $("#iTopRow").removeClass("btnAddChildDisabled");
                    $("#iTopRow").removeClass("ancDelete");
                    $("#iTopRow").addClass("btnAddChild");
                    $("#iTopRow").css("cursor", "pointer");
                    $("#iTopRow").css("color", "");
                    $("#iTopRow").show();
                }

            }
        });

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }


        function validateEvent() {
            var retval = 1;
            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            var firstName = $("#txtFirstName").val();
            var middleName = $("#txtMiddleName").val();
            var lastName = $("#txtLastName").val();
            var partType = participantType;
            var emailAddress = $("#txtEmailAddress").val();
            var attendees = $("#txtAttendeesCount").val();

            if (year == "0") {
                retval = "-1";
                statusMessage("Please select Year", "middle-center", "error");
            } else if (eventId == "0") {
                retval = "-1";
                statusMessage("Please select Event Code", "middle-center", "error");
            } else if (firstName == "") {
                retval = "-1";
                statusMessage("Please enter First Name", "middle-center", "error");



            } else if (lastName == "") {
                retval = "-1";
                statusMessage("Please enter Last Name", "middle-center", "error");



            } else if (emailAddress == "") {
                retval = "-1";
                statusMessage("Please enter Email address", "middle-center", "error");
            } else if (partType == "") {
                retval = "-1";
                statusMessage("Please select participant Type", "middle-center", "error");
            } else if (attendees == "") {
                retval = "-1";
                statusMessage("Please enter Number of Attendees", "middle-center", "error");
            }

            if (emailAddress != "") {

                if (!ValidateEmail(emailAddress)) {
                    retval = "-1";
                    statusMessage("Please enter valid Email Address", "middle-center", "error");
                }
            }

            return retval;
        }
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };

        function validateOnAddChild() {
            var retVal = 1;
            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            var isChildOptionUsed = -1;

            if ((schoolName != "" && schoolName != undefined) || (grade != "0" && grade != undefined) || (childName != "" && childName != undefined)) {
                isChildOptionUsed = 1;
            }
            if (isChildOptionUsed == 1) {

                if (childName == "") {
                    retVal = "-1";
                    statusMessage("Please enter Child Name", "middle-center", "error");
                    return retVal;
                } else if (grade == "0") {
                    retVal = "-1";
                    statusMessage("Please select Grade", "middle-center", "error");
                    return retVal;
                } else if (schoolName == "") {
                    retVal = "-1";
                    statusMessage("Please enter school Name", "middle-center", "error");
                    return retVal;
                } else if (arrContest.length <= 0) {
                    retVal = "-1";
                    statusMessage("Enter the information of a child only if participating in a contest.<br>If a child is not participating in a contest, remove his/her detail. Update only the number of attendees.", "middle-center", "error");
                    return retVal;
                }
            }

            if (arrChildId.length > 0) {
                for (var i = 0; i < arrChildId.length; i++) {

                    isChildOptionUsed = -1;
                    schoolName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    childName = $("#txtChildName" + arrChildId[i] + "").val();
                    grade = $("#selGrades" + arrChildId[i] + "").val();

                    var arrContestMult = [];
                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk8" + arrChildId[i] + "").val());
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk9" + arrChildId[i] + "").val());
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk10" + arrChildId[i] + "").val());
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk11" + arrChildId[i] + "").val());
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk12" + arrChildId[i] + "").val());
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk13" + arrChildId[i] + "").val());
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk14" + arrChildId[i] + "").val());
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk26" + arrChildId[i] + "").val());
                    }

                    if ((schoolName != "" && schoolName != undefined) || (grade != "0" && grade != undefined) || (childName != "" && childName != undefined)) {
                        isChildOptionUsed = 1;
                    }
                    if (isChildOptionUsed == 1) {

                        if (childName == "") {
                            retVal = "-1";
                            statusMessage("Please enter Child Name", "middle-center", "error");
                            return retVal;
                        } else if (grade == "0") {
                            retVal = "-1";
                            statusMessage("Please select Grade", "middle-center", "error");
                            return retVal;
                        } else if (schoolName == "") {
                            retVal = "-1";
                            statusMessage("Please enter school Name", "middle-center", "error");
                            return retVal;
                        } else if (arrContestMult.length <= 0) {
                            retVal = "-1";
                            statusMessage("Enter the information of a child only if participating in a contest.<br>If a child is not participating in a contest, remove his/her detail. Update only the number of attendees.", "middle-center", "error");
                            return retVal;
                        }
                    }
                }
            }
            return retVal;
        }


        function validateChild() {
            var retVal = "1";
            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            if (childName == "") {
                retVal = "-1";
                statusMessage("Please enter Child Name", "middle-center", "error");
            } else if (grade == "0") {
                retVal = "-1";
                statusMessage("Please select Grade", "middle-center", "error");
            } else if (schoolName == "") {
                retVal = "-1";
                statusMessage("Please enter school Name", "middle-center", "error");
            } else if (arrContest.length <= 0) {
                retVal = "-1";
                statusMessage("Select contest(s). No need to register a child, if the child is not going to participate in contests", "middle-center", "error");
            }

            if (arrChildId.length > 0) {
                for (var i = 0; i < arrChildId.length; i++) {

                    schoolName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    childName = $("#txtChildName" + arrChildId[i] + "").val();
                    grade = $("#selGrades" + arrChildId[i] + "").val();

                    var arrContestMult = [];

                    var sb;
                    var vb;
                    var mb;
                    var sc;
                    var gb;
                    var ew;
                    var ps;
                    var bb;

                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk8" + arrChildId[i] + "").val());
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk9" + arrChildId[i] + "").val());
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk10" + arrChildId[i] + "").val());
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk11" + arrChildId[i] + "").val());
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk12" + arrChildId[i] + "").val());
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk13" + arrChildId[i] + "").val());
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk14" + arrChildId[i] + "").val());
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        arrContestMult.push($("#chk26" + arrChildId[i] + "").val());
                    }




                    if (childName == "") {
                        retVal = "-1";
                        statusMessage("Please enter Child Name", "middle-center", "error");
                        return retVal;
                    } else if (grade == "0") {
                        retVal = "-1";
                        statusMessage("Please select Grade", "middle-center", "error");
                        return retVal;
                    } else if (schoolName == "") {
                        retVal = "-1";
                        statusMessage("Please enter school Name", "middle-center", "error");
                        return retVal;
                    } else if (arrContestMult.length <= 0) {
                        retVal = "-1";
                        statusMessage("Select contest(s). No need to register a child, if the child is not going to participate in contests", "middle-center", "error");
                        return retVal;
                    }
                }
            }
            return retVal;
        }

        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        function statusMessage(message, position, type) {

            $.toast({

                text: '<b>' + message + '</b>',
                icon: type,
                position: 'mid-center',
                hideAfter: false,
                stack: 1

            })
        }

        $(document).on("click", "#btnSave", function (e) {

            if ($("#hdnisUpdate").val() == "0") {
                if (validateEvent() == "1" && validateOnAddChild() == 1) {

                    var eventId = $('#selEventCode').val();
                    duplictateCheck(eventId);
                }
            } else {
                if (validateEvent() == "1" && validateOnAddChild() == 1) {
                    $("#hdnIsChild").val("1");
                    postNewRegistration(2);
                }
            }

            //duplictateCheck();

        });

        $(document).on("keyup", "#txtVenueAddr", function (e) {

            if ($(this).length == 128) {
                statusMessage("Venue Address could not be more than 128 character.", "middle-center", "error");
            }

        });



        function postNewRegistration(mode) {

            $("#hdnMode").val(mode);
            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            var firstName = $("#txtFirstName").val();
            var middleName = $("#txtMiddleName").val();
            var lastName = $("#txtLastName").val();
            var partType = participantType;
            var emailAddress = $("#txtEmailAddress").val();
            var attendees = $("#txtAttendeesCount").val();
            var userId = document.getElementById("<%=hdnLoginID.ClientID%>").value;
            var memberId = $("#hdnMemberID").val();

            var jsonData = JSON.stringify({ FrEventReg: { EventCode: eventCode, FreeEventID: eventId, FirstName: firstName, Year: year, MiddleName: middleName, LastName: lastName, Email: emailAddress, PartType: partType, Attendees: attendees, Createdby: userId, Status: "Pending", Mode: mode, MemberID: memberId } });

            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/PostNewRegistartion",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {



                    if (JSON.stringify(data.d) > 0) {

                        if ($("#hdnIsChild").val() == "1") {


                            validateChildDate(JSON.stringify(data.d), mode);

                            $("#dvNewRegistration").show();
                        } else {

                            if (mode == 1) {


                                encryptURL(JSON.stringify(data.d), "Confirmed", "");
                            } else {
                                validateChildDate(JSON.stringify(data.d), mode);
                                //$().toastmessage('showToast', {
                                //    text: 'Updated successfully',
                                //    sticky: true,
                                //    position: 'middle-center',
                                //    type: 'success',
                                //    close: function () { console.log("toast is closed ..."); }
                                //});
                                //arrContest = [];
                                //arrChildId = [];
                                $("#dvNewRegistration").hide();
                                $("#dvUpdateRegistration").hide();

                            }


                            hideLoader();
                        }


                    } else {

                        if (JSON.stringify(data.d) == -4) {
                            statusMessage("Registration Date already has passed", "mid-center", "error");
                        }
                        hideLoader();
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }

            });
        }

        function validateChildDate(memberID, mode) {




            var schoolName = $("#txtSchoolName").val();
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();

            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();

            var success = "-1";
            var SB = null;
            var VB = null;
            var MB = null;
            var GB = null;
            var EW = null;
            var PS = null;
            var BB = null;
            var SC = null;

            //old code

            if ($.inArray("8", arrContest) >= 0) {

                SB = getContestsCodesbasedOnGrade("8", grade);
            }
            if ($.inArray("9", arrContest) >= 0) {

                VB = getContestsCodesbasedOnGrade("9", grade);
            }
            if ($.inArray("10", arrContest) >= 0) {
                MB = getContestsCodesbasedOnGrade("10", grade);
            }
            if ($.inArray("11", arrContest) >= 0) {
                GB = getContestsCodesbasedOnGrade("11", grade);
            }
            if ($.inArray("12", arrContest) >= 0) {
                EW = getContestsCodesbasedOnGrade("12", grade);
            }
            if ($.inArray("13", arrContest) >= 0) {
                PS = getContestsCodesbasedOnGrade("13", grade);
            }
            if ($.inArray("14", arrContest) >= 0) {
                BB = getContestsCodesbasedOnGrade("14", grade);
            }
            if ($.inArray("26", arrContest) >= 0) {
                SC = getContestsCodesbasedOnGrade("26", grade);
            }
            var jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: childName, Grade: grade, SchoolName: schoolName, Year: year, MemberID: memberID, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

            showLoader();
            if (childName != "" && schoolName != "" && grade != "0" && arrContest.length > 0) {

                $.ajax({
                    type: "POST",
                    url: "FreeEventReg.aspx/PostNewChild",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        $("#hdnFreeEventID").val("0")
                        var success = 0;
                        // if (JSON.stringify(data.d) > 0) {
                        hideLoader();
                        mode = 1;
                        FreeEventChildReg(childName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode);
                        // }
                    }
                });
            } else if (arrChildId.length > 0) {

                FreeEventChildReg(childName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode);
            }
            else {
                encryptURL(memberID, "Confirmed", "");
            }


            hideLoader();


        }

        function FreeEventChildReg(chilName, grade, schoolName, SB, VB, MB, SC, GB, EW, PS, BB, memberID, mode) {

            var year = $("#selyear").val();
            var eventCode = $("#selEventCode option:selected").text();
            var eventId = $("#selEventCode").val();
            //var tId = "";

            //var jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: chilName, Grade: grade, SchoolName: schoolName, Year: year, MemberID: memberID, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

            //showLoader();
            //$.ajax({
            //    type: "POST",
            //    url: "FreeEventReg.aspx/PostNewChild",
            //    data: jsonData,
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (data) {
            //        tId = JSON.stringify(data.d);
            //        $("#hdnFreeEventID").val("0")
            //        var success = 0;
            //        if (JSON.stringify(data.d) != "-1") {


            if (arrChildId.length > 0) {

                for (var i = 0; i < arrChildId.length; i++) {
                    if (i > 0) {
                        mode = 1;
                    }

                    var schlName = $("#txtSchoolName" + arrChildId[i] + "").val();
                    var childName = $("#txtChildName" + arrChildId[i] + "").val();
                    var gradeInfo = $("#selGrades" + arrChildId[i] + "").val();



                    SB = null;
                    VB = null;
                    MB = null;
                    GB = null;
                    EW = null;
                    PS = null;
                    BB = null;
                    SC = null;

                    if ($("#chk8" + arrChildId[i] + "").prop("checked") == true) {
                        SB = getContestsCodesbasedOnGrade("8", gradeInfo);
                    }
                    if ($("#chk9" + arrChildId[i] + "").prop("checked") == true) {
                        VB = getContestsCodesbasedOnGrade("9", gradeInfo);
                    }
                    if ($("#chk10" + arrChildId[i] + "").prop("checked") == true) {
                        MB = getContestsCodesbasedOnGrade("10", gradeInfo);
                    }
                    if ($("#chk11" + arrChildId[i] + "").prop("checked") == true) {
                        GB = getContestsCodesbasedOnGrade("11", gradeInfo);
                    }
                    if ($("#chk12" + arrChildId[i] + "").prop("checked") == true) {
                        EW = getContestsCodesbasedOnGrade("12", gradeInfo);
                    }
                    if ($("#chk13" + arrChildId[i] + "").prop("checked") == true) {
                        PS = getContestsCodesbasedOnGrade("13", gradeInfo);
                    }
                    if ($("#chk14" + arrChildId[i] + "").prop("checked") == true) {
                        BB = getContestsCodesbasedOnGrade("14", gradeInfo);
                    }
                    if ($("#chk26" + arrChildId[i] + "").prop("checked") == true) {
                        SC = getContestsCodesbasedOnGrade("26", gradeInfo);
                    }

                    jsonData = JSON.stringify({ FrEventChild: { EventCode: eventCode, FreeEventID: eventId, ChildName: childName, Grade: gradeInfo, SchoolName: schlName, Year: year, MemberID: memberID, SB: SB, VB: VB, MB: MB, GB: GB, SC: SC, EW: EW, PS: PS, BB: BB, Mode: mode } });

                    if (schlName != "" && childName != "" && gradeInfo != "0") {
                        $.ajax({
                            type: "POST",
                            url: "FreeEventReg.aspx/PostNewChild",
                            data: jsonData,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                success = "1";
                                // tId = JSON.stringify(data.d);
                            }
                        });
                    }
                }

                // send email
                //reset();

                hideLoader();

            }
            if ($("#hdnMode").val() == "1") {
                encryptURL(memberID, "Confirmed", "");

                $("#dvNewRegistration").show();
            } else if ($("#hdnMode").val() == "2") {

                statusMessage("Updated successfully", "middle-center", "success");

                $("#dvNewRegistration").hide();
                $("#dvUpdateRegistration").hide();
                reset();
            }


            //        } else {

            //            hideLoader();
            //        }

            //    },
            //    failure: function (response) {
            //        alert(response.d);
            //        hideLoader();
            //    }

            //});

        }


        function listFreeEvents(freeEventID, year) {

            $('#selEventCode').empty();
            $('#selEventName').empty();

            var jsonData = JSON.stringify({ Year: year, FreeEventID: freeEventID });
            showLoader();
            $.ajax({
                type: "POST",
                url: "FreeEvent.aspx/ListFreeEvents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    hideLoader();
                    var eventId = 0;
                    var isContestShow = 0;
                    $("#selEventCode").append($("<option></option>").val
                        (0).html("Select"));

                    $("#selEventName").append($("<option></option>").val
                        (0).html("Select"));
                    $.each(data.d, function (index, value) {
                        $("#selEventCode").append($("<option></option>").val
                            (value.FreeEventID).html(value.EventName));

                        $("#selEventName").append($("<option></option>").val
                            (value.FreeEventID).html(value.EventName));
                        eventId = value.FreeEventID;
                        if (value.SB != "" && value.VB != "" && value.MB != "" && value.SC != "" && value.GB != "" && value.EW != "" && value.PS != "" && value.BB != "") {
                            isContestShow = 1;
                        }
                    });
                    if (data.d.length == 1) {
                        $("#selEventCode").attr("disabled", "disabled");
                        $("#selEventName").attr("disabled", "disabled");
                        $("#selEventCode").val(eventId);
                        $("#selEventName").val(eventId);
                       
                        if (isContestShow == 1) {
                            $("#dvContestSesction").show();
                        } else {
                            $("#dvContestSesction").hide();
                        }
                    } else if (data.d.length > 1) {
                        $("#selEventCode").removeAttr("disabled");
                        $("#selEventName").removeAttr("disabled");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }
        function duplictateCheck(eventId) {




            var email = $('#txtEmailAddress').val();
            var year = $('#selyear').val();

            if (eventId == 0) {
                email = $('#txtUpdateEmailAddress').val();
                year = $('#selUpdateYear').val();
            }

            var jsonData = JSON.stringify({ FrEventReg: { Email: email, FreeEventID: eventId, Year: year } });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/DuplicateExists",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    if (JSON.stringify(data.d) > 0) {

                        $("#hdnEmailExists").val(JSON.stringify(data.d));

                        if (eventId > 0) {

                            statusMessage("The email address already exists. Please select the “Update my registration” option from the drop down menu.", "middle-center", "info");


                        } else {
                            if ($("#hdnEmailExists").val() != "0") {
                                if ($("#selActionType").val() == "2") {
                                    getmemberId(email);
                                } else if ($("#selActionType").val() == "3") {
                                    reSendEmailLink(email);
                                }
                            } else {

                                statusMessage("Email address does not match.", "middle-center", "error");
                                hideLoader();
                            }
                        }
                        hideLoader();
                    } else {
                        $("#hdnEmailExists").val(JSON.stringify(data.d))
                        if (eventId > 0) {
                            $("#hdnIsChild").val("1");
                            postNewRegistration(1);
                            hideLoader();
                        } else {
                            statusMessage("Email address does not match.", "middle-center", "error");
                            hideLoader();
                        }
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }



        function reset() {
            //$("#selEventCode").val("0");
            $("#txtFirstName").val("");
            $("#txtMiddleName").val("");
            $("#txtLastName").val("");
            $("#txtEmailAddress").val("");
            $("#txtAttendeesCount").val("");

            arrContest = [];
            arrChildId = [];
            $(".rbtnContests").removeAttr("checked");
            $("#txtChildName").val("");
            $("#selGrade1").val("0");
            $("#txtSchoolName").val("");

            $(".dvChildSection").hide();
            $("#dvContest").hide();
            $(".partType").prop("checked", false);

            $("#iTopRow").removeClass("btnAddChild");
            $("#iTopRow").addClass("btnAddChildDisabled");
            $("#iTopRow").css("cursor", "normal");
            $("#iTopRow").css("color", "Grey");

            if ($("#dvContestSesction").html() == "") {
                var dvHtml = "";

                dvHtml += '<div style="float: left; margin-left: 30px;">';
                dvHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; left:75px;">Child Attending Contest(s)</span></div> <div style= "clear: both; margin-bottom: 5px;" ></div >';
                dvHtml += '<div class="input-group" >';

                dvHtml += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';

                dvHtml += '<input type="text" autocomplete="off" class="form-control" style="width: 351px;" name="InputName" id="txtChildName" placeholder="Child Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';
                dvHtml += '<div style="float: left; margin-left: 30px;  position:relative; top:25px;">';

                dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>';
                dvHtml += ' <select id="selGrade1" class="form-control" style="width:200px;">';
                dvHtml += ' <option value="0">Grade</option>';
                dvHtml += '<option value="1">1</option>';
                dvHtml += '<option value="2">2</option>';
                dvHtml += '<option value="3">3</option>';
                dvHtml += '<option value="4">4</option>';
                dvHtml += '<option value="5">5</option>';
                dvHtml += '<option value="6">6</option>';
                dvHtml += '<option value="7">7</option>';
                dvHtml += '<option value="8">8</option>';
                dvHtml += '<option value="9">9</option>';
                dvHtml += '<option value="10">10</option>';
                dvHtml += ' <option value="11">11</option>';
                dvHtml += '<option value="12">12</option>';
                dvHtml += '</select>';
                dvHtml += '</div>';
                dvHtml += '</div>';
                dvHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
                dvHtml += '<div class="input-group" ><span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>';
                dvHtml += '<input type="text" autocomplete="off" class="form-control" name="InputName" style="width: 351px;" id="txtSchoolName" placeholder="School Name" required>';
                dvHtml += '</div>';
                dvHtml += '</div>';

                dvHtml += '<div style="float: left; margin-left:3px;">';

                dvHtml += '<a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey;" id="iTopRow"></i></a>';
                dvHtml += '</div>';

                dvHtml += '<div class="clear" style="margin-bottom: 5px;"></div>';
                dvHtml += '<div>';

                dvHtml += '<div id="dvContest" style="position: relative; left: 32px; top:10px;">';
                dvHtml += '</div>';
                dvHtml += '</div>';
                $("#dvContestSesction").html(dvHtml)
            }
            $("#iTopRow").hide();
            //  listChapter();
        }

        $(document).on("click", "#btnReset", function (e) {

            reset();
        });


        function fillGrade(id) {
            $('#id').empty();
            var i = 1;
            //         $("#" + id).append($("<option></option>").val
            //("0").html("Select"));
            for (i = 1; i < 13; i++) {
                $("#" + id).append($("<option></option>").val
                    (i).html(i));
            }
        }

        var childCount = 0;
        var index = 0;
        var arrChildId = [];

        function addChild(index) {

            //index++;
            $("#hdnMaxIndex").val(index);
            $("#hdnIndex").val(index);
            arrChildId.push(index.toString());

            $(".dvChildSection").show();
            var dHtml = "";
            // dHtml += $("#dvAttendees").html();
            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '<div id="dvNewChild' + index + '" class="dvChildSection">';

            dHtml += '<div  style="float: left; margin-left:30px; ">';
            dHtml += '<div><span style="color:#58AD36; font-weight:bold; font-size:15px; position:relative; left:75px;">Child Attending Contest(s)</span></div> <div style= "clear: both; margin-bottom: 5px;" ></div>';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i></span>';

            dHtml += '<input type="text" autocomplete="off" class="form-control childName" style="width:351px;" attr-index=' + index + ' name="InputName" id="txtChildName' + index + '" placeholder="Child Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';

            dHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px; ">';

            dHtml += '<div class="input-group">  ';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span>';
            dHtml += '<select id="selGrades' + index + '" attr-index=' + index + ' class="form-control selGrade" style="width:200px;">';
            dHtml += '<option value="0">Grade</option>';
            var j = 1;
            //         $("#" + id).append($("<option></option>").val
            //("0").html("Select"));
            for (j = 1; j < 13; j++) {
                dHtml += '<option value=' + j + '>' + j + '</option>';
            }

            dHtml += '</select>';

            dHtml += '</div>';
            dHtml += '</div>';
            dHtml += '<div style="float: left; margin-left: 30px; position:relative; top:25px;">';
            dHtml += '<div class="input-group">';
            dHtml += '<span class="input-group-addon" > <i class="glyphicon glyphicon-user"></i></span >';
            dHtml += '<input type="text" autocomplete="off" class="form-control schoolName" style="width:351px;" attr-index=' + index + ' name="InputName" id="txtSchoolName' + index + '" placeholder="School Name" required>';
            dHtml += '</div>';
            dHtml += '</div>';
            //dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="btnAddChild" attr-childId=' + index + ' title="Add a Child"><i class="fa fa-user-plus fa-2x"></i></a></div>';

            dHtml += '<div  style="float: left; margin-left: 3px; position:relative; top:30px;" id="dvAddDelete' + index + '"></div>';

            //   dHtml += '<div style="float: left; margin-left: 10px; position:relative; top:5px;"><a style="cursor:pointer;" class ="ancDelete" attr-childId=' + index + ' title="Remove a Child"><i class="fa fa-user-times fa-2x"></i></a></div>';

            dHtml += '<div style="clear: both; margin-bottom: 5px;"></div>';




            dHtml += "<div id='dvProductCodes" + index + "'>";
            dHtml += '</div>';
            dHtml += '</div>';


            dHtml += '<div style="clear: both; margin-bottom: 10px;"></div>';
            dHtml += '</div>';



            var id = (index - 1);

            if ($("#dvNewChild" + id + "").html() != "" && $("#dvNewChild" + id + "").html() != undefined) {

                $("#dvNewChild" + id + "").before(dHtml);
            } else {
                $("#dvContestSesction").before(dHtml);
            }

            // dvChildHtml.push(dHtml);

            // fillGrade("selGrades" + childCount + "");



            childCount++;
        }

        function enableDisableAddDelIcon(cIndex, option) {
            var iconHtml = "";

            if (option == "Update") {
                iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Remove a Child"><i class="fa fa-user-times fa-2x ancDelete "  attr-childId=' + cIndex + '></i></a>';
            } else {
                iconHtml += '<a style="cursor:pointer;" attr-childId=' + cIndex + ' title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChild "  attr-childId=' + cIndex + '></i></a>';
            }

            $("#dvAddDelete" + cIndex + "").html(iconHtml);
        }

        $(document).on("keydown", ".childName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#selGrades" + attrIndex + "").val() != "0" && $("#txtSchoolName" + attrIndex + "").val() != "") {
                    if ($("#dvAddDelete" + attrIndex + "").html() == "") {
                        enableDisableAddDelIcon(attrIndex, "Add");
                    }
                }
            }
        });





        $(document).on("keydown", ".schoolName", function (e) {
            var attrIndex = $(this).attr("attr-index");
            if ($(this).val().length > 0) {

                if ($("#selGrades" + attrIndex + "").val() != "0" && $("#txtChildName" + attrIndex + "").val() != "") {
                    if ($("#dvAddDelete" + attrIndex + "").html() == "") {
                        enableDisableAddDelIcon(attrIndex, "Add");
                        $("#iTopRow").show();
                    }
                }
            }
        });


        function getIndexNo(index) {
            return index;
        }


        $(document).on("change", ".selGrade", function (e) {



            if ($(this).val() != "0") {

                var cIndex = $(this).attr("attr-index");

                var grade = $(this).val();
                addContest(cIndex, grade, "");

                if ($("#txtChildName" + cIndex + "").val() != "0" && $("#txtSchoolName" + cIndex + "").val() != "" && grade != "0") {
                    if ($("#dvAddDelete" + cIndex + "").html() == "") {
                        enableDisableAddDelIcon(cIndex, "Add");
                    }
                }
            }
        });

        function addContest(cIndex, grade, childJSON) {



            $("#dvProductCodes" + cIndex + "").html("");


            var grades = "";

            var dHtml = "";

            var year = $("#selyear").val();

            var freeEventID = $("#selEventCode").val();

            if (freeEventID == "0") {
                $(this).val("0");
                statusMessage("Please select Event Code", "middle-center", "error");

            } else {


                var jsonData = JSON.stringify({ Year: year, EventID: freeEventID, GradeFrom: grade, GradeTo: grade });

                $.ajax({
                    type: "POST",
                    url: "FreeEventReg.aspx/ListContests",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var dvHtml = "";
                        var i = 0;

                        if (JSON.stringify(data.d.length) > 0) {
                            var dHtml = "";

                            $.each(data.d, function (index, value) {

                                if (getContestsCodesbasedOnGrade(value.ProductGroupID, grade) != "") {
                                    i++;


                                    dHtml += "<div>";
                                    dHtml += "<div style='width:235px; float:left;'>";
                                    dHtml += ' <div style="float: left; margin-left: 30px;">';
                                    dHtml += '<input type="checkbox" class="rbtnContests' + cIndex + '" value=' + value.ProductGroupID + ' id="chk' + value.ProductGroupID + '' + cIndex + '" />';
                                    dHtml += ' </div>';
                                    dHtml += '<div style="float: left; margin-left: 10px;">';
                                    dHtml += ' <label for="InputName" class="control-label">' + getContestsbasedOnGrade(value.ProductGroupID, grade) + '</label>';
                                    dHtml += ' </div>';
                                    dHtml += ' </div>';
                                    dHtml += ' </div>';

                                    //if (i == 4) {
                                    //    dHtml += '<div style="clear:both;"></div>';
                                    //}
                                }


                            });


                            $("#dvProductCodes" + cIndex + "").html(dHtml);

                            if (childJSON != "") {


                                $.each(childJSON, function (index1, value1) {
                                    if (index1 == cIndex) {

                                        if (value1.SB != "" && value1.SB != null) {

                                            $("#chk8" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.VB != "" && value1.VB != null) {
                                            $("#chk9" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.MB != "" && value1.MB != null) {
                                            $("#chk10" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.SC != "" && value1.SC != null) {
                                            $("#chk26" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.GB != "" && value1.GB != null) {
                                            $("#chk11" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.EW != "" && value1.EW != null) {
                                            $("#chk12" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.PS != "" && value1.PS != null) {
                                            $("#chk13" + cIndex + "").prop("checked", true);
                                        }
                                        if (value1.BB != "" && value1.BB != null) {
                                            $("#chk14" + cIndex + "").prop("checked", true);
                                        }
                                    }
                                });


                            }
                            hideLoader();
                        } else {

                            hideLoader();
                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                        hideLoader();
                    }
                });
            }
        }

        //$(document).on("click", "#btnAddChild", function (e) {

        //    addChild();

        //});

        $(document).on("click", ".ancDelete", function (e) {


            var childId = $(this).attr("attr-childId");

            $("#dvNewChild" + childId + "").empty();

            if ($(this).attr("id") == "iTopRow") {
                $("#dvContestSesction").empty();
                arrContest = [];
                $("#txtSchoolName").val("");
                $("#txtChildName").val("");
                $("#selGrade1").val("0");
            }

            if ($(this).attr("id") != "iTopRow") {
                arrChildId.splice($.inArray(childId, arrChildId), 1);
            }

            // fillGrade();

        });


        $(document).on("click", ".rbtnContests", function (e) {


            var contestID = $(this).val();

            if ($(this).prop("checked") == true) {

                arrContest.push(contestID);
            }

            if ($(this).prop("checked") == false) {

                if (arrContest.indexOf($(this).val()) > -1) {

                    arrContest.splice($.inArray($(this).val(), arrContest), 1);
                    //arrContest.splice(arrContest.indexOf($(this).val()), 1)
                }
            }

        });

        $(document).on("change", "#selActionType", function (e) {
            if ($(this).val() == "1") {
                $("#dvNewRegistration").show();
                $("#dvUpdateRegistration").hide();
                $("#txtFirstName").removeAttr("disabled");
                $("#txtLastName").removeAttr("disabled");
                $("#txtEmailAddress").removeAttr("disabled");

            } else if ($(this).val() == "2") {
                var year = $("#selUpdateYear").val();
                listFreeEvents(0, year);
                $("#dvNewRegistration").hide();
                $("#dvUpdateRegistration").show();
            } else if ($(this).val() == "3") {
                var year = $("#selUpdateYear").val();
                listFreeEvents(0, year);
                $("#dvNewRegistration").hide();
                $("#dvUpdateRegistration").show();
            } else if ($(this).val() == "4") {

                opennewtab("https://www.northsouth.org/public/FreeEvent/NSF-MCD-EducationExpo-2018.pdf");
            }

        });

        function opennewtab(url) {
            var win = window.open(url, '_blank');
        }

        var participantType = "";
        $(document).on("click", ".partType", function (e) {



            if ($(this).prop("checked") == true) {

                participantType = $(this).val();
            }



        });


        function getContestsbasedOnGrade(contestID, grade) {

            var contestCode = "";
            if (contestID == "8") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Spelling";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Senior Spelling";
                }
            } else if (contestID == "9") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Vocabulary";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Vocabulary";
                }

            } else if (contestID == "10") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 2) {
                    contestCode = "Math Level 1";
                } else if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "Math Level 2";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Math Level 3";
                }
            } else if (contestID == "11") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Geography";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "Senior Geography";
                }
            } else if (contestID == "12") {
                if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "Junior Essay Writing";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Essay Writing";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "Senior Essay Writing";
                }

            } else if (contestID == "13") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Junior Public Speaking";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "Senior Public Speaking";
                }
            } else if (contestID == "14") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Intermediate Brain Bee";
                } else if (parseInt(grade) > 7 && parseInt(grade) <= 11) {
                    contestCode = "Brain Bee";
                }
            } else if (contestID == "26") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "Junior Science";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 5) {
                    contestCode = "Intermediate Science";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "Senior Science";
                }

            }

            return contestCode;
        }


        function getContestsCodesbasedOnGrade(contestID, grade) {

            var contestCode = "";
            if (contestID == "8") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JSB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "SSB";
                }
            } else if (contestID == "9") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JVB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "IVB";
                }

            } else if (contestID == "10") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 2) {
                    contestCode = "MB1";
                } else if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "MB2";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "MB3";
                }
            } else if (contestID == "11") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JGB";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 8) {
                    contestCode = "SGB";
                }
            } else if (contestID == "12") {
                if (parseInt(grade) > 2 && parseInt(grade) <= 5) {
                    contestCode = "EW1";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "EW2";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "EW3";
                }

            } else if (contestID == "13") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "PS1";
                } else if (parseInt(grade) > 8 && parseInt(grade) <= 12) {
                    contestCode = "PS3";
                }
            } else if (contestID == "14") {
                if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "IBB";
                } else if (parseInt(grade) > 7 && parseInt(grade) <= 11) {
                    contestCode = "BB";
                }
            } else if (contestID == "26") {
                if (parseInt(grade) > 0 && parseInt(grade) <= 3) {
                    contestCode = "JSC";
                } else if (parseInt(grade) > 3 && parseInt(grade) <= 5) {
                    contestCode = "ISC";
                } else if (parseInt(grade) > 5 && parseInt(grade) <= 8) {
                    contestCode = "SSC";
                }

            }

            return contestCode;
        }

        function encryptURL(UID, status, TimeID) {

            var URl = "&UID=" + UID + "&Status=" + status + "";
            var jsonData = JSON.stringify({ clearText: URl });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Encrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var index = window.location.href.lastIndexOf('/');
                    var originalURL = window.location.href.substr(0, index);

                    originalURL = originalURL + "/" + "Confirmation.aspx";

                    var encryptedURl = originalURL + "?ID=" + JSON.stringify(data.d);
                    var email = $("#txtEmailAddress").val();
                    sendEmail(encryptedURl, URl, 1, email);

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }
        function sendEmail(url, dUrl, mode, email) {

            // var email = $("#txtEmailAddress").val();
            var jsonData = JSON.stringify({ Email: email, URl: url, Mode: mode });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/SendEmailsToRegistarants",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (mode == 1) {
                        // statusMessage('Thank you for registering. An email is being sent to ' + $("#txtEmailAddress").val() + '. Please click the link to complete the registration.', "middle-center", "success");

                        //reset();
                        hideLoader();
                        var freeEventID = $("#selEventCode").val();
                        window.location.href = "FreeEventRegistrationThankU.aspx?Id=" + freeEventID + "";

                        //Simson
                    } else if (mode == 2) {
                        statusMessage('An email is being sent to ' + $("#txtUpdateEmailAddress").val() + '. Please follow the instructions in the email to update your registration.', 'middle-center', 'success');

                        hideLoader();
                        $("#dvUpdateRegistration").hide();
                        $("#selActionType").val("0");
                    } else if (mode == 3) {

                        statusMessage('An email is being sent to ' + $("#txtUpdateEmailAddress").val() + '. Please click the link to complete the registration.', 'middle-center', 'success');


                        hideLoader();
                        $("#dvUpdateRegistration").hide();
                        $("#selActionType").val("0");

                    }
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function dcryptURL(id) {

            var URl = JSON.parse(id);

            var jsonData = JSON.stringify({ cipherText: URl });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Decrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var decryptedText = JSON.stringify(data.d);


                    var index = window.location.href.indexOf('?');

                    var url = window.location.href.substr(0, index);
                    url = url + "" + decryptedText;

                    var memberId = getDecryptedValue(url, "memberId");
                    var expired = getDecryptedValue(url, "expired");

                    expired = JSON.parse("\"" + expired);
                    var update = getDecryptedValue(url, "U");
                    var year = $("#selyear").val();
                    listFreeEvents(0, year);
                    $("#hdnMemberID").val(memberId);
                    if (memberId != "" && memberId != undefined && update != "" && update != undefined) {
                        getFreeEventregistrations(memberId, expired);
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function getFreeEventregistrations(memberId, expired) {

            $("#selActionType").val("2");
            $("#selActionType").attr("disabled", "disabled");
            $("#dvNewRegistration").show();
            $("#dvUpdateRegistration").hide();

            var jsonData = JSON.stringify({ MemberID: memberId, Expired: expired });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetFreeEventregistrations",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var i = 0;
                    var j = 0;
                    var k = 0;
                    if (JSON.stringify(data.d.length) > 0) {
                        var childJSON = "";
                        $.each(data.d, function (index, value) {
                            childJSON = value.Child;
                            j++;
                            k = j;
                            $("#selyear").val(value.Year);
                            $("#selEventCode").val(value.FreeEventID);

                            $("#txtFirstName").val(value.FirstName);
                            $("#txtMiddleName").val(value.MiddleName);
                            $("#txtLastName").val(value.LastName);
                            $("#txtEmailAddress").val(value.Email);
                            $("#txtAttendeesCount").val(value.Attendees);

                            $("#rbtn" + value.PartType + "").prop("checked", true);

                            $("#txtFirstName").attr("disabled", "disabled");
                            $("#txtLastName").attr("disabled", "disabled");
                            $("#txtEmailAddress").attr("disabled", "disabled");
                            participantType = value.PartType;

                        });
                        var iIndex = 0;
                        $.each(childJSON, function (index1, value1) {
                            iIndex++;
                            if (i == 0) {
                                $('#iTopRow').removeClass("fa-user-plus");
                                $('#iTopRow').addClass("fa-user-times");
                                $('#iTopRow').addClass("ancDelete");
                                $('#iTopRow').removeClass("btnAddChildDisabled ");
                                $('#iTopRow').show();
                                $('#iTopRow').css("color", "");
                                $('#iTopRow').attr("title", "Remove a Child");
                                $('#iTopRow').css("cursor", "pointer");
                                $("#txtChildName").val(value1.ChildName);
                                $("#selGrade1").val(value1.Grade);
                                $("#txtSchoolName").val(value1.SchoolName);
                                loadProductGroups(value1.Grade, value1.Grade, childJSON);



                            } else {


                                addChild(index1);
                                enableDisableAddDelIcon(index1, "Update");

                                $("#txtChildName" + index1 + "").val(value1.ChildName);
                                $("#selGrades" + index1 + "").val(value1.Grade);
                                $("#txtSchoolName" + index1 + "").val(value1.SchoolName);
                                addContest(index1, value1.Grade, childJSON);


                                if (value1.SB != "" && value1.SB != null) {

                                    $("#chk8" + index1 + "").prop("checked", true);
                                }
                                if (value1.VB != "" && value1.VB != null) {
                                    $("#chk9" + index1 + "").prop("checked", true);
                                }
                                if (value1.MB != "" && value1.MB != null) {
                                    $("#chk10" + index1 + "").prop("checked", true);
                                }

                                if (value1.SC != "" && value1.SC != null) {
                                    $("#chk26" + index1 + "").prop("checked", true);
                                }
                                if (value1.GB != "" && value1.GB != null) {
                                    $("#chk11" + index1 + "").prop("checked", true);
                                }
                                if (value1.EW != "" && value1.EW != null) {
                                    $("#chk12" + index1 + "").prop("checked", true);
                                }
                                if (value1.PS != "" && value1.PS != null) {
                                    $("#chk13" + index1 + "").prop("checked", true);
                                }
                                if (value1.BB != "" && value1.BB != null) {
                                    $("#chk14" + index1 + "").prop("checked", true);
                                }
                            }
                            i++;

                        });
                        iIndex = iIndex + 1;
                        addChild((iIndex - 1));
                    } else if (JSON.stringify(data.d.length) == -2) {
                        statusMessage("Confirmation link expired.", "middle-center", "error");
                    } else if (JSON.stringify(data.d.length) == -1) {
                        statusMessage("Operation failed.", "middle-center", "error");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", ".btnAddChild", function (e) {
            // if (validateAddChild() == 1) {

            $(this).removeClass("btnAddChild");

            $(this).removeClass("fa-user-plus");
            $(this).addClass("fa-user-times");

            $(this).addClass("ancDelete");

            $(this).attr("title", "Remove a child");
            var cIndex = $("#hdnMaxIndex").val();

            addChild((parseInt(cIndex) + 1));
            // }
        });

        function validateAddChild() {
            var retVal = 1;
            var childName = $("#txtChildName").val();
            var grade = $("#selGrade1").val();
            var schollName = $("#txtSchoolName").val();

            var childName1 = $("#txtChildName" + index + "").val();
            var grade1 = $("#selGrade1" + index + "").val();
            var schollName1 = $("#txtSchoolName" + index + "").val();

            if (childName == "") {
                statusMessage("Please select Child Name", "middle-center", "error");
                retVal = -1;
            } else if (grade == "0") {
                statusMessage("Please select Grade", "middle-center", "error");
                retVal = -1;
            } else if (schollName == "") {
                statusMessage("Please School Name", "middle-center", "error");
                retVal = -1;
            }

            if (index > 0) {
                if (childName1 == "") {
                    statusMessage("Please select Child Name", "middle-center", "error");
                    retVal = -1;
                } else if (grade1 == "0") {
                    statusMessage("Please select Grade", "middle-center", "error");
                    retVal = -1;
                } else if (schollName1 == "") {
                    statusMessage("Please School Name", "middle-center", "error");
                    retVal = -1;
                }
            }

            return retVal;
        }

        function sendUpdateLink(memberId) {

            var email = $("#txtUpdateEmailAddress").val();

            var option = $("#selActionType").val();

            var url = "&memberId=" + memberId + "&U=1";
            if ($("#selActionType").val() == "2") {
                url = "&memberId=" + memberId + "&U=1";
            } else if ($("#selActionType").val() == "3") {
                url = "&UID=" + memberId + "&U=3&Status=Confirmed";
            }
            var jsonData = JSON.stringify({ clearText: url });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Encrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var index = window.location.href.lastIndexOf('/');
                    var resendURl = window.location.href.substr(0, index);
                    resendURl = resendURl + "/Confirmation.aspx";
                    var originalURL = window.location.href;

                    var encryptedURl = originalURL + "?GUID=" + JSON.stringify(data.d);
                    if (option == "2") {
                        encryptedURl = originalURL + "?GUID=" + JSON.stringify(data.d);
                        sendEmail(encryptedURl, encryptedURl, 2, email);
                    } else if (option == "3") {
                        encryptedURl = resendURl + "?ID=" + JSON.stringify(data.d);
                        sendEmail(encryptedURl, encryptedURl, 3, email);
                    }


                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        $(document).on("click", "#btnSubmit", function (e) {

            if (validateUpdateReg() == 1) {

                duplictateCheck(0);


            }
        });

        function validateUpdateReg() {
            var retVal = 1;
            var email = $("#txtUpdateEmailAddress").val();
            var year = $("#selUpdateYear").val();
            var eventrId = $("#selEventName").val();

            if (year == "") {
                retVal = -1;
                statusMessage("Please select Year", "middle-center", "error");
            } else if (eventrId == "0") {
                retVal = -1;
                statusMessage("Please select Event Name", "middle-center", "error");
            } else if (email == "") {
                retVal = -1;
                statusMessage("Please enter Email Address", "middle-center", "error");
            }
            if (email != "") {
                if (!ValidateEmail(email)) {
                    retVal = -1;
                    statusMessage("Please enter valid Email Address", "middle-center", "error");
                }
            }
            return retVal;
        }

        function getmemberId(email) {



            var jsonData = JSON.stringify({ Email: email });

            showLoader();
            var memberId = 0;
            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetmemberID",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d.length) > 0) {

                        $.each(data.d, function (index, value) {

                            memberId = value.MemberID;


                        });

                        if ($("#selActionType").val() == "3") {
                            sendUpdateLink(memberId);
                        } else if ($("#selActionType").val() == "2") {
                            verifyStatus(memberId);

                        }
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function reSendEmailLink(email) {
            //getmemberId(email);
            validateStatus(email);
        }

        $(document).on("keydown", "#txtChildName", function (e) {
            var childName = $(this).val();
            var schoolName = $("#txtSchoolName").val();
            var grade = $("#selGrade1").val();

            if (schoolName != "" && childName != "" && grade != "0") {
                $("#iTopRow").removeClass("btnAddChildDisabled");
                $("#iTopRow").removeClass("ancDelete");
                $("#iTopRow").addClass("btnAddChild");
                $("#iTopRow").css("cursor", "pointer");
                $("#iTopRow").css("color", "");
                $("#iTopRow").show();
            }
        });

        $(document).on("keydown", "#txtSchoolName", function (e) {
            var childName = $("#txtChildName").val();

            var schoolName = $(this).val();
            var grade = $("#selGrade1").val();

            if (schoolName != "" && childName != "" && grade != "0") {
                $("#iTopRow").removeClass("btnAddChildDisabled");
                $("#iTopRow").removeClass("ancDelete");
                $("#iTopRow").addClass("btnAddChild");
                $("#iTopRow").css("cursor", "pointer");
                $("#iTopRow").css("color", "");
                $("#iTopRow").show();
            }
        });

        function validateStatus(email) {

            var jsonData = JSON.stringify({ Email: email, FreeEventID: $("#selEventName").val(), Year: $("#selUpdateYear").val() });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/ValidateStatus",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) == 0) {

                        getmemberId(email);
                        //  sendUpdateLink(memberId);

                    } else if (JSON.stringify(data.d) > 0) {
                        statusMessage('Your registration has already been confirmed', 'middle-center', 'info');

                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function verifyStatus(memberId) {

            var jsonData = JSON.stringify({ MemberId: memberId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/VerifyStatus",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) == 0) {
                        if (memberId > 0) {
                            sendUpdateLink(memberId);
                        }
                    } else if (JSON.stringify(data.d) > 0) {

                        statusMessage("Your registration is yet to be confirmed. Please try again after confirming your registration.", "middle-center", "info");

                    }

                }
            });
        }

        $(document).on("keyup", "#txtAttendeesCount", function (e) {

            var attendees = $(this).val();
            if (attendees.length > 0) {
                if (attendees != "") {

                    if (parseInt(attendees) <= 0 || parseInt(attendees) > 10) {
                        retval = "-1";
                        if (parseInt(attendees) <= 0) {
                            statusMessage("Please enter valid number of Attendees", "middle-center", "error");
                        } else {
                            statusMessage("Please enter the Number of Attendees(less than 10)", "middle-center", "error");
                        }
                        $("#txtAttendeesCount").val("");
                    }
                }
            }
        });

        function checkEmail(email) {


            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email.value)) {

                email.focus;
                return false;
            } else {
                return true;
            }
        }

        function getAvailableSeats() {
            var year = $("#selyear").val();
            var jsonData = JSON.stringify({ Year: year });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/GetAvailableSeats",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var remainingSeats = JSON.stringify(data.d);
                    $("#spnAvailableSeats").text(remainingSeats);
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });

        }

    </script>

    <div class="container" style="min-height: 450px; width: 1190px; background-color: #FFFFFF;">

        <%--        <div class="page-header">
            <center>

                <h3 style="color: #46A71C; font-weight: bold; font-family: 'Trebuchet MS'">Welcome to North South Foundation
                  <div class="clear" style="margin-bottom: 10px;"></div>
                    Register for a Free Event
                </h3>


            </center>
        </div>--%>

        <div class="page-header">
            <center>
                <div style="color: #9A0033; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                    McDONALD’S  CHICAGO 
                 
                </div>
                <div style="clear: both; margin-bottom: 25px;"></div>
                <div style="color: #9A0033; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                    EDUCATION EXPO 2018
                 
                </div>
                <div style="clear: both; margin-bottom: 20px;"></div>
                <div style="color: #0070BF; padding-bottom: 20px; font-weight: bolder; font-size: 20px; font-family: 'Trebuchet MS'">
                    The future is in your hands. Let’s build it together
                 
                </div>
            </center>

        </div>

        <%--    <div style="clear: both; margin-top: 10px; margin-bottom: 10px;">
            <div class="alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> This alert box could indicate a successful or positive action.
            </div>
        </div>--%>
        <div class="container">

            <div class="col-md-12">
                <div class="col-md-3">
                    <img src="../Images/left.png" style="width: 100%" />
                </div>
                <div class="col-md-3 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">When</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>June 16th, Saturday</li>
                        <li>9:30 am — 4:30 pm</li>

                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">Where</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>NIU Naperville</li>
                        <li>Conference Center</li>
                        <li style="color: #C00000;">1120 E. Diehl Road, Naperville, IL 60563</li>

                    </ul>
                </div>
                <div class="col-md-3">
                    <img src="../Images/right.png" style="width: 100%" />
                </div>
                <%--<div class="col-md-4">
                    <img src="/Images/M.png" style="width:100%;" />
                </div>--%>
            </div>

            <div class="col-md-12">
                <h3><span style="font-weight: bold; color: #C09100;">LEARN ABOUT </span>
                    <span style="font-weight: bold; color: black;">(GRADES 9 TO 12)</span></h3>
                <div class="col-md-8 ">
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: square; font-weight: bold;">
                        <li>How to educate your child to be a winner?</li>
                        <li>How to get admitted to Elite Schools (Ivy League)?</li>
                        <li>How to Excel at a University?</li>
                        <li>Scholarships and finance for higher 
            education</li>
                        <li>SAT and ACT preparation</li>
                        <li>Perfect college essay & career selection</li>
                        <%-- <li style="color: #07AF50;">Mock Bees in Math and Spelling (Grades 1 to 8)</li>--%>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div style="color: #3F3F3F; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                        STUDENTS IN
                 
                    </div>
                    <div style="clear: both; margin-bottom: 25px;"></div>
                    <div style="color: #3F3F3F; padding-bottom: 20px; font-weight: bolder; font-size: 50px; font-family: 'Trebuchet MS'">
                        GRADES 9-12
                 
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <div class="col-md-3">
                    <img src="../Images/M.png" style="width: 100%; position: relative; top: 10px;" />
                </div>
                <div class="col-md-3">
                    <img src="../Images/new_life.png" style="width: 100%; position: relative; top: 60px;" />
                </div>
                <div class="col-md-3">
                    <img src="../Images/win.png" style="width: 100%; position: relative; top: 50px;" />
                </div>

                <div class="col-md-3">
                    <img src="../Images/bee.png" style="width: 100%; position: relative; top: -15px;" />
                </div>
            </div>

            <div style="clear: both;">
                <div style="width: 235px; margin: auto;">
                    <div style="float: left;">
                        <h3 style="color: #C09100; font-weight: bold; color: #07AF50;">Available Seat(s):</h3>
                    </div>
                    <div style="float: left;">
                        <h4><span id="spnAvailableSeats" style="color: #C00000; font-weight: bold; position: relative; left: 5px; top: 14px;">450</span></h4>
                    </div>
                </div>
            </div>
            <%--   <div style="clear:both; border:1px solid #CECECE; height:1px;"></div>--%>
            <%--         <div class="col-md-12">
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold; color: #07AF50;">Available Seat(s)</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li><span id="spnAvailableSeats" style="color: #C00000; font-weight: bold;">450</span></li>

                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">When</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>Saturday, Jul 15th</li>
                        <li>9:30 am — 4:30 pm</li>

                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <h3 style="color: #C09100; font-weight: bold;">Where</h3>
                    <style>
                        .list-styl li {
                            font-size: 22px;
                            line-height: 30px;
                        }
                    </style>
                    <ul class="list-styl" style="list-style-type: none; font-weight: bold;">
                        <li>Yellow Box</li>
                        <li>Community Christian Church</li>
                        <li style="color: #C00000;">1635 Emerson Ln, Naperville, IL 60540 </li>

                    </ul>
                </div>
            </div>--%>


            <div style="clear: both; margin-bottom: 20px;"></div>
            <div class="container">
                <center>
                    <select id="selActionType" style="width: 300px;" class="form-control">
                        <option value="0">Select</option>
                        <option value="1">New Registration</option>
                        <option value="2">Update my Registration</option>
                        <option value="3">Resend my confirmation link</option>
                        <option value="4">Event details</option>

                    </select>

                </center>
            </div>
            <div style="clear: both; margin-bottom: 30px;">
            </div>
            <div class="container" id="dvNewRegistration" style="display: none;">
                <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;">
                    <div class="col-md-6" style="display: none;">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Year</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                <select id="selyear" class="form-control" required>
                                    <option value="0">Select</option>

                                    <option value="2017">2017</option>
                                    <option value="2018" selected="selected">2018</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <center>
                            <div class="form-group" style="position: relative; left: 270px;">
                                <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Event Name</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-tasks"></i></span>
                                    <select id="selEventCode" class="form-control" required>
                                        <option value="0">Select</option>

                                    </select>

                                </div>
                            </div>
                        </center>
                    </div>
                </div>
                <hr />
                <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; color: #58AD36; font-size: 15px; font-weight: bold;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtFirstName" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; left: 150px; color: #58AD36; font-size: 15px; font-weight: bold; visibility: hidden">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtMiddleName" placeholder="Middle Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtLastName" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="email" class="form-control" id="txtEmailAddress" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Partcipant Type</label>

                            <ul style="margin-left: -40px; position: relative; top: 5px;">
                                <li style="display: inline-block; list-style: none;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnParent" value="Parent" />
                                    <label for="InputName" class="control-label" style="position: relative; top: -3px;">Parent</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnTeacher" value="Parent" />
                                    <label for="InputName" class="control-label" style="position: relative; top: -3px;">Teacher</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnVolunteer" value="Parent" />
                                    <label for="InputName" class="control-label" style="position: relative; top: -3px;">Volunteer</label>
                                </li>
                                <li style="display: inline-block; list-style: none; margin-left: 15px;">
                                    <input type="radio" class="partType" name="ParticipantType" id="rbtnOther" value="Parent" />
                                    <label for="InputName" class="control-label" style="position: relative; top: -3px;">Other</label>
                                </li>
                            </ul>
                            <%--   <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="email" class="form-control" id="email">--%>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="email" style="color: #58AD36; font-size: 15px; font-weight: bold;">Number of Attendees</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select id="txtAttendeesCount" class="form-control" required>
                                    <option value="0">Select</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="dvContestSesction">

                    <div style="float: left; margin-left: 30px;">
                        <div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative;">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" autocomplete="off" class="form-control" style="width: 386px;" name="InputName" id="txtChildName" placeholder="Child Name" required>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative; top: 25px;">
                        <%--<div><span style="color: #58AD36; font-weight: bold; font-size: 15px; position: relative; left: 75px;">Child Attending Contest(s)</span></div>
                        <div style="clear: both; margin-bottom: 5px;"></div>--%>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>

                            <select id="selGrade1" class="form-control" style="width: 130px;">
                                <option value="0">Grade</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px; position: relative; top: 25px;">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>
                            <input type="text" class="form-control" name="InputName" autocomplete="off" style="width: 386px;" id="txtSchoolName" placeholder="School Name" required>
                        </div>
                    </div>

                    <div style="float: left; margin-left: 3px; position: relative; top: 25px;">
                        <a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey; display: none;" id="iTopRow"></i></a>
                    </div>

                    <div class="clear" style="margin-bottom: 5px;"></div>
                    <div>

                        <div id="dvContest" style="position: relative; left: 32px;">
                        </div>
                    </div>
                </div>

                <%--  <div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="email" class="form-control" id="txtChildName" placeholder="Child Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="position: relative; left: 100px; color: #58AD36; font-size: 15px; font-weight: bold;">Child Attending Contest(s)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select id="selGrade1" class="form-control">
                                    <option value="0">Grade</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" style="visibility: hidden;">Name</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-object-align-bottom"></i></span>
                                <input type="email" class="form-control" id="txtSchoolName" placeholder="School Name">
                            </div>

                        </div>
                    </div>
                </div>--%>

                <div class="col-md-12 text-center" style="margin-top: 10px;">
                    <ul style="position: relative; left: -25px;">
                        <li style="display: inline-block; list-style: none;">
                            <input type="button" id="btnSave" value="Submit" class="btn btn-info" style="background-color: #46A71C; border: #46A71C;" name="Save" />
                        </li>
                        <li style="display: inline-block; list-style: none;">
                            <input type="button" id="btnReset" value="Reset" class="btn btn-info" style="background-color: #46A71C; border: #46A71C;" name="Save" />
                        </li>
                    </ul>
                </div>

                <div class="col-lg-5 col-md-push-1" style="display: none;">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <strong><span class="glyphicon glyphicon-ok"></span>Success! Message sent.</strong>
                        </div>
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-remove"></span><strong>Error! Please check all page inputs.</strong>
                        </div>
                    </div>
                </div>

            </div>



            <div class="bs-example" id="dvUpdateRegistration" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">

                <div>
                    <div style="float: left; width: 35px;">
                        <label for="InputName" class="control-label">Year</label>
                    </div>
                    <div style="float: left;">
                        <select id="selUpdateYear" class="form-control" required>
                            <option value="0">Select</option>

                            <option value="2017" selected="selected">2017</option>
                            <option value="2018">2018</option>

                        </select>
                    </div>

                </div>

                <div>
                    <div style="float: left; width: 75px; margin-left: 20px;">
                        <label for="InputName" class="control-label">Event Name</label>
                    </div>
                    <div style="float: left;">
                        <select id="selEventName" class="form-control" required>
                            <option value="0">Select</option>

                        </select>
                    </div>

                </div>

                <div>
                    <div style="float: left; width: 95px; margin-left: 20px;">
                        <label for="InputName" class="control-label">Email Address</label>
                    </div>
                    <div style="float: left;">
                        <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtUpdateEmailAddress" style="width: 275px;" placeholder="Email Address" required>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <input type="button" id="btnSubmit" value="Submit" class="btn btn-info" name="Submit" />
                    </div>
                </div>

            </div>

            <div style="clear: both; margin-bottom: 20px;">
            </div>

            <%--           <div class="bs-example" id="dvNewRegistration" style="width: 80%; margin: auto; display: none;">



                <form role="form" class="form-horizontal">
                    <div>
                        <div class="well well-sm" style="float: right;"><strong><i style="color: red; font-size: 20px; position: relative; top: 6px;">* </i>mandatory fields</strong></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Year</label>
                            </div>
                            <div style="float: left;">
                                <select id="selyear" class="form-control" required>
                                    <option value="0">Select</option>

                                    <option value="2017" selected="selected">2017</option>
                                    <option value="2018">2018</option>

                                </select>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>

                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Event Name</label>
                            </div>
                            <div style="float: left;">
                                <select id="selEventCode" style="width: 371px;" class="form-control" required>
                                    <option value="0">Select</option>

                                </select>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>

                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Name</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtFirstName" placeholder="First Name" required style="width: 210px;">
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtMiddleName" placeholder="Middle Initial" required style="width: 106px;">
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtLastName" placeholder="Last Name" required style="width: 210px;">
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Email Address</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" name="InputName" id="txtEmailAddress" style="width: 375px;" placeholder="Email Address" required>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Partcipant Type</label>
                            </div>
                            <div style="float: left;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnParent" value="Parent" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Parent</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnTeacher" value="Teacher" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Teacher</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnVolunteer" value="Volunteer" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Volunteer</label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="radio" class="partType" name="ParticipantType" id="rbtnOther" value="Other" />
                                <label for="InputName" class="control-label" style="position: relative; top: -3px;">Other</label>
                            </div>
                        </div>
                        <div style="float: left;">
                            <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div style="float: left; width: 150px;">
                                <label for="InputName" class="control-label">Number of Attendees</label>
                            </div>
                            <div style="float: left;">
                                <input type="text" autocomplete="off" class="form-control" style="width: 60px;" name="InputName" id="txtAttendeesCount" placeholder="" required>
                            </div>
                            <div style="float: left;">
                                <i style="color: red; font-size: 20px; position: relative; top: 5px;">*</i>
                            </div>

                        </div>
                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div style="margin-left: 150px;">
                            <label for="InputName" class="control-label">Children attending contest(s):</label>
                        </div>
                        <div id="dvContestSesction">

                            <div style="float: left; margin-left: 150px;">
                                <input type="text" autocomplete="off" class="form-control" style="width: 250px;" name="InputName" id="txtChildName" placeholder="Child Name" required>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <input type="text" class="form-control" name="InputName" autocomplete="off" style="width: 280px;" id="txtSchoolName" placeholder="School Name" required>
                            </div>

                            <div style="float: left; margin-left: 10px;">
                                <a style="position: relative; top: 5px;" title="Add a Child"><i class="fa fa-user-plus fa-2x btnAddChildDisabled" style="color: grey; display: none;" id="iTopRow"></i></a>
                            </div>

                            <div class="clear" style="margin-bottom: 5px;"></div>
                            <div>

                                <div id="dvContest" style="position: relative; left: 120px;">
                                </div>
                            </div>
                        </div>

                        <div class="clear" style="margin-bottom: 15px;"></div>
                        <div>
                            <div id="dvAttendees"></div>


                            <div class="clear" style="margin-bottom: 15px;"></div>
                            <div>

                                <input type="button" id="btnSave" value="Submit" class="btn btn-info" name="Save" />
                                <input type="button" id="btnReset" value="Reset" class="btn btn-info" name="Reset" />
                            </div>
                        </div>
                </form>
               

            </div>--%>
            <div class="clear" style="margin-bottom: 10px;"></div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
                <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </div>
        </div>
        <div id="overlay"></div>
        <div id="ohsnap">
        </div>
        <input type="hidden" id="hdnFreeEventID" value="0" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
        <input type="hidden" value="0" id="hdnIndex" />
        <input type="hidden" value="0" id="hdnisUpdate" />
        <input type="hidden" value="0" id="hdnEmailExists" />
        <input type="hidden" value="0" id="hdnIsChild" />
        <input type="hidden" value="0" id="hdnMaxIndex" />
        <input type="hidden" value="0" id="hdnMode" />
        <input type="hidden" value="0" id="hdnMemberID" />
</asp:Content>
