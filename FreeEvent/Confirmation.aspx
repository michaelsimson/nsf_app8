﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Confirmation.aspx.cs" MasterPageFile="~/NSFInnerMasterPage.master" Inherits="FreeEvent_Confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../Scripts/jquery.toastmessage.js"></script>
    <link href="../css/jquery.toastmessage.css" rel="stylesheet" />
    <link href="../css/Loader.css" rel="stylesheet" />

    <script type="text/javascript">

        $(function (e) {


            var id = getParameterByName("ID");

            if (id != "" && id != undefined) {

                dcryptURL(id);

            }


        });

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function getDecryptedValue(URL, param) {
            var url = URL.slice(URL.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        function dcryptURL(id) {

            var URl = JSON.parse(id);

            var jsonData = JSON.stringify({ cipherText: "" + URl + "" });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/Decrypt",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var decryptedText = JSON.stringify(data.d);

                   

                    var index = window.location.href.indexOf('?');

                    var url = window.location.href.substr(0, index);
                    url = url + "" + decryptedText;

                    var status = getDecryptedValue(url, "Status");

                    var memberID = getDecryptedValue(url, "UID");

                    //var index = JSON.stringify(memberID).indexOf("-");
                    //var retVal = JSON.stringify(memberID).split("-")[1];
                    //memberID = JSON.stringify(memberID).substring(1, index);
                    var u = getDecryptedValue(url, "U");

                    var expired = getDecryptedValue(url, "expired");

                    expired = JSON.parse("\"" + expired);

                    if (u == "" || u == undefined) {
                        u = 1;
                    } else {
                        u = 3;
                    }

                    if (status != "" && status != undefined && memberID != "" && memberID != undefined && u == 1) {

                        updateStatus(status, memberID);
                    } else if (status != "" && status != undefined && memberID != "" && memberID != undefined && u == 3) {

                        requestUpdate(status, memberID, expired);
                    }

                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function updateStatus(status, uId) {

            var jsonData = JSON.stringify({ Status: status, FrMemberID: uId });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/UpdateStatus",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var retVal;
                    var eventName;
                    $.each(data.d, function (index, value) {
                        retVal = value.RetVal;
                        eventName = value.EventName;
                    });

                    if (retVal == -2) {
                        $("#dvConfirmation").show();
                        $("#spnConfirmation").text("Your activation link has been expired.");
                        $("#spnConfirmation").css("color", "red");

                    } else if (retVal == 1) {

                        $("#dvConfirmation").show();
                        $("#dvStatus").html("<h3><span id='spnConfirmation' style='color:green;'>Your registration for <span style='font-weight:bold; color:blue;'>" + eventName + " - A NSF Event</span> was successful.</span></h3>.");
                        $("#spnConfirmation").css("color", "green");
                    } else if (retVal == -3) {

                        $("#dvConfirmation").show();
                        $("#spnConfirmation").text("Your registration has already been confirmed.");
                        $("#spnConfirmation").css("color", "Blue");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }

        function requestUpdate(status, uId, expired) {

            var jsonData = JSON.stringify({ Status: status, FrMemberID: uId, Expired: expired });

            showLoader();

            $.ajax({
                type: "POST",
                url: "FreeEventReg.aspx/RequestUpdate",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var retVal;
                    var eventName;
                    $.each(data.d, function (index, value) {
                        retVal = value.RetVal;
                        eventName = value.EventName;
                    });

                    if (retVal == -2) {
                        $("#dvConfirmation").show();
                        $("#spnConfirmation").text("Your confirmation link has expired.  You can go back and request for another confirmation link.");
                        $("#spnConfirmation").css("color", "red");

                    } else if (retVal == 1) {

                        $("#dvConfirmation").show();
                        $("#dvStatus").html("<h3><span id='spnConfirmation' style='color:green;'>Your registration for <span style='font-weight:bold; color:blue;'>" + eventName + " - A NSF Event</span> was successful.</span></h3>.");
                        $("#spnConfirmation").css("color", "green");
                    } else if (retVal == -3) {

                        $("#dvConfirmation").show();
                        $("#spnConfirmation").text("Your registration has already been confirmed.");
                        $("#spnConfirmation").css("color", "Blue");
                    }
                    hideLoader();
                },
                failure: function (response) {
                    alert(response.d);
                    hideLoader();
                }
            });
        }



        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }
    </script>

    <div class="container" style="min-height: 450px;">

        <div class="page-header">
            <center>
                <h1 style="color: #00a0b0; font-weight: bold; font-family: 'Trebuchet MS'">Registration Confirmation
               
                </h1>
            </center>
        </div>
        <div style="clear: both; margin-bottom: 20px;">
        </div>

        <div id="dvConfirmation" style="display: none;">
            <div class="container">
                <center>
                    <div style="height: 50px; width: 800px; border: 2px solid #00a0b0;" id="dvStatus">
                        <h3><span id="spnConfirmation" style="color: green;">You are now registered successfully for “McDonald’s Education Expo 2017 - NSF Free Event”.</span></h3>
                    </div>
                </center>
            </div>

        </div>
    </div>
</asp:Content>
