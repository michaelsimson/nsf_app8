﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="FreeEvent_test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <link href="../css/jquery.toast.css" rel="stylesheet" />
    <style type="text/css">
        * {
            font-family: 'Roboto Slab', serif;
            margin: 0;
            padding: 0;
        }

        hr {
            border: 1px solid #ccc;
            margin-bottom: 20px;
        }

        h1, h2 {
            margin: 20px 0px 10px;
        }

        a {
            text-decoration: none;
        }

        .container {
            display: block;
            width: 1200px;
            margin: auto;
        }

        strong {
            padding-bottom: 1px;
            border-bottom: 1px dotted;
        }

        p {
            margin: 10px 0px;
        }

        code {
            font-family: monospace;
            color: #2d2d2d;
            background: whitesmoke;
            display: block;
            padding: 10px;
            border: 1px solid #ccc;
            overflow: scroll;
        }

        a.eval-js {
            color: whitesmoke;
            padding: 5px 10px;
            display: inline-block;
            margin-bottom: 10px;
            background: #2d2d2d;
            border-radius: 2px;
        }

        span.muted {
            font-size: 17px;
            color: #8B7373;
        }

        span.code {
            font-family: monospace;
            color: #2d2d2d;
            background: whitesmoke;
            padding: 5px;
            border: 1px solid #ccc;
        }

        div.code-runner {
            margin: 20px 20px;
        }

        ul, ol {
            margin: 10px 50px;
        }

            ul li {
                margin-bottom: 10px;
            }

        .hidden {
            display: none;
        }

        span.k {
            display: inline-block;
            width: 175px;
        }

        span.c {
            color: #F7BCBC;
            padding-left: 30px;
        }

        .plugin-options label {
            display: inline-block;
            width: 265px;
        }

        span.toast-position span.k {
            display: inline;
        }

        .toast-index .muted {
            display: block;
            color: #AD9D9D;
        }

            .toast-index .muted:hover {
                color: #333;
            }

        .latest-update {
            padding: 0px 30px;
            border: 1px dashed;
            margin: 30px 0;
            box-sizing: border-box;
        }
    </style>
    <script src="../js/jquery.toast.js"></script>
    <script type="text/javascript">
        $(function (e) {
            $.toast({
               // heading: 'Can I add <em>icons</em>?',
                text: '<b>Yes! check this</b> <a href="https://github.com/kamranahmedse/jquery-toast-plugin/commits/master">update</a>.',
                hideAfter: false,
                icon: 'info',
                position:'mid-center'
            })
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    </form>
</body>
</html>
