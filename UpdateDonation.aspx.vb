Imports System.Data.SqlClient
Imports NorthSouth.DAL
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Partial Class UpdateDonation
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim MemberID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not IsPostBack Then


            MemberID = Request.QueryString("MemberID")
            If Not Request.QueryString("MemberID") Is Nothing Then
                lnkMenu.NavigateUrl = "ViewDonations.aspx?Id=" & Request.QueryString("MemberID") & "&type=" & Request.QueryString("type")
                MemberID = Request.QueryString("MemberID")
                If Request.QueryString("type").ToUpper = "OWN" Then
                    'TrEduGame.Visible = False
                    dllEGames.Visible = False
                    lblDdlGame.Visible = False
                    RequiredfieldvalidatorGame.EnableClientScript = False
                    TrEndowment.Visible = True
                    TrDesign.Visible = False
                    Trmatch.Visible = False
                End If
            End If
            LoadYear()
            LoadOrganizations()
            LoadDonationPurpose()
            LoadEvents()
            LoadEducationGame()
            LoadChapter()
            loadbank()
            'strSql = "SELECT * FROM DONATIONSINFO WHERE MemberID=106"
            strSql = "SELECT * FROM DONATIONSINFO WHERE MemberID=" & MemberID
            strSql = strSql & " AND DonationID=" & Request.QueryString("DonationID")

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim drDonations As SqlDataReader
            drDonations = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            If drDonations.Read Then
                Dim Amount As Object = drDonations("Amount")
                Dim DonationDate As Object = drDonations("DonationDate")
                Dim MatchingEmployer As Object = drDonations("MatchingEmployer")
                Dim MemoryOf As Object = drDonations("InMemoryOf")
                Dim Remarks As Object = drDonations("Remarks")
                Dim Designation As Object = drDonations("Designation")
                Dim Purpose As Object = drDonations("Purpose")
                Dim TrnsNumber As Object = drDonations("Transaction_Number")
                Dim EventName As Object = drDonations("EventID")
                Dim MatchingFlag As Object = drDonations("MatchingFlag")
                Dim Method As Object = drDonations("Method")
                Dim Endowment As Object = drDonations("Endowment")
                Dim Anonymous As Object = drDonations("Anonymous")
                Dim Access As Object = drDonations("Access")
                Dim DepositDate As Object = drDonations("DepositDate")
                Dim DepositSlip As Object = drDonations("DepositSlip")
                Dim Status As Object = drDonations("Status")
                Dim donationtype As Object = drDonations("DonationType")
                Dim chapterid As Object = drDonations("ChapterID")
                Dim BankID As Object = drDonations("BankID")
                Dim Project As Object = drDonations("Project")
                Dim EventYear As Object = drDonations("EventYear")
                Dim SuppCode As Object = drDonations("suppcode")

                If Not SuppCode Is DBNull.Value And SuppCode.ToString().Trim <> String.Empty Then



                    DDSuppCode.SelectedItem.Text = drDonations("suppcode")
                    ' DDSuppCode.Items.Remove(drDonations("suppcode"))
                Else
                    ' DDSuppCode.SelectedItem.Text = "Select SuppCode"
                    DDSuppCode.SelectedIndex = 0
                End If

                If Not Amount Is System.DBNull.Value Then
                    txtDonation.Text = String.Format("{0:N}", drDonations("Amount"))
                Else
                    txtDonation.Text = ""
                End If
                If Not DonationDate Is System.DBNull.Value Then
                    txtDonationDt.Text = Convert.ToDateTime(drDonations("DonationDate")).ToString("MM/dd/yyyy")
                Else
                    txtDonationDt.Text = ""
                End If
                If Not MatchingEmployer Is DBNull.Value Then
                    ddlEmployer.SelectedValue = drDonations("MatchEmpID").ToString()
                Else
                    ddlEmployer.SelectedValue = 0
                End If
                If Not MemoryOf Is DBNull.Value Then
                    txtInNameOf.Text = drDonations("InMemoryOf")
                Else
                    txtInNameOf.Text = ""
                End If
                If Not Remarks Is DBNull.Value Then
                    txtRemarks.Text = drDonations("Remarks")
                Else
                    txtRemarks.Text = ""
                End If
                If Not Designation Is DBNull.Value Then
                    ddlDesignation.SelectedValue = drDonations("Designation")
                Else
                    ddlDesignation.SelectedIndex = 0
                End If
                If Not Purpose Is DBNull.Value Then
                    ddlDonationFor.SelectedValue = drDonations("Purpose")
                Else
                    ddlDonationFor.SelectedIndex = 5
                End If
                If Not Anonymous Is DBNull.Value Then
                    ddlAnonymous.SelectedValue = drDonations("Anonymous")
                Else
                    ddlAnonymous.SelectedValue = "No"
                End If
                If Not EventName Is DBNull.Value Then
                    ddlDonationEvent.SelectedValue = drDonations("EventID")
                Else
                    ddlDonationEvent.SelectedIndex = 0
                End If
                If Not MatchingFlag Is DBNull.Value Then
                    ddlMachingFlag.SelectedValue = drDonations("MatchingFlag")
                Else
                    ddlMachingFlag.SelectedIndex = 1
                End If
                If Not Method Is DBNull.Value Then
                    ddlMethod.SelectedValue = drDonations("method")
                Else
                    ddlMethod.SelectedIndex = 0
                End If
                If Not Endowment Is DBNull.Value Then
                    ddlEndowment.SelectedValue = drDonations("Endowment")
                Else
                    ddlEndowment.SelectedIndex = 0
                End If
                If Not Access Is DBNull.Value Then
                    dllEGames.SelectedValue = drDonations("Access")
                Else
                    dllEGames.SelectedIndex = 0
                End If
                If Not DepositDate Is System.DBNull.Value Then
                    txtddate.Text = Convert.ToDateTime(drDonations("DepositDate")).ToString("MM/dd/yyyy")
                Else
                    txtddate.Text = ""
                End If
                If Not DepositSlip Is System.DBNull.Value Then
                    txtdsn.Text = drDonations("DepositSlip")
                Else
                    txtdsn.Text = ""
                End If
                If Not Status Is System.DBNull.Value Then
                    ddlEvent.SelectedValue = drDonations("Status")
                Else
                    ddlEvent.SelectedValue = 0
                End If

                If Not TrnsNumber Is DBNull.Value Then
                    txtTrnsNumber.Text = TrnsNumber.ToString()
                Else
                    txtTrnsNumber.Text = ""
                End If
                If Not BankID Is System.DBNull.Value Then
                    ddlBankId.SelectedIndex = ddlBankId.Items.IndexOf(ddlBankId.Items.FindByValue(BankID.ToString))
                End If
                If Not Project Is DBNull.Value Then
                    txtProject.Text = Project.ToString()
                Else
                    txtProject.Text = ""
                End If
                If Not donationtype Is System.DBNull.Value Then
                    If donationtype = "Unrestricted" Then
                        drpdonationtype.SelectedValue = 1
                    Else
                        drpdonationtype.SelectedValue = 2
                    End If
                Else
                    drpdonationtype.SelectedValue = 1
                End If

                If Not chapterid Is System.DBNull.Value Then
                    drpchapterid.SelectedValue = chapterid
                Else
                    drpchapterid.SelectedValue = 1
                End If
                If Not EventYear Is DBNull.Value Then
                    ddlEventYear.SelectedValue = drDonations("EventYear")
                Else
                    If Now.Month < 5 Then
                        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year - 1))
                    Else
                        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year))
                    End If
                End If
            End If
            txtDonation.Attributes.Add("onkeypress", "return numeric();")
        End If
    End Sub

    Private Sub loadbank()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank")
        ddlBankId.DataSource = ds
        ddlBankId.DataBind()
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        lblerr.Text = ""
        lblerr1.Text = ""
        lbl3.Text = ""
        lblError.Text = ""
        lblErrDate.Text = ""
        If Not IsDate(txtDonationDt.Text) Then
            lblErrDate.Text = "Please enter Donation date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtddate.Text) Then
            lblerr.Text = "Please enter Deposit date in mm/dd/yyyy format"
            Exit Sub
        End If
        If ddlEvent.SelectedItem.Text = "None" Then
            lblError.Text = "Please select valid status"
            Exit Sub
        ElseIf ddlDonationEvent.SelectedItem.Text = "Select Event" Then
            lblError.Text = "Please select valid Donation Event"
            Exit Sub
        End If
        If Request.QueryString("Type").ToString.ToUpper.Trim() = "OWN" Then
            If DDSuppCode.SelectedItem.Text = "Select SuppCode" Then
                lblError.Text = "Please select valid SuppCode"
                Exit Sub
            End If
        End If
        If ddlMethod.SelectedItem.Text = "Check" Or (ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Or (ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
            If (txtddate.Text = Nothing Or txtdsn.Text = Nothing) Or (ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Or (ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
                If ddlMethod.SelectedItem.Text = "Check" Then
                    If txtddate.Text = "" Then
                        lblerr.Text = "Enter Deposit Date"
                    End If
                    If txtdsn.Text = "" Then
                        lblerr1.Text = "Enter Deposit Slip Number"
                    End If
                End If
                If ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "No" And Not (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "") Then
                    lbl3.Text = "When the matching flag is No, then Matching employer should not be selected."
                End If
                If (ddlMachingFlag.Visible = True And ddlMachingFlag.SelectedItem.Text = "Yes" And (ddlEmployer.SelectedItem.Text = "Select Employer Name" Or ddlEmployer.SelectedItem.Text = "")) Then
                    lbl3.Text = "When the matching flag is Yes, then Matching employer should be selected."
                End If

            Else
                ' added By Ferdine to Avoid Duplicate on Oct 01, 2010
                Dim memberid As Integer
                If Request.QueryString("MemberID") Is Nothing Then
                    memberid = Session("LoginID")
                Else
                    memberid = Request.QueryString("MemberID")
                End If
                Try
                    If ddlMethod.SelectedItem.Text = "Check" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from DonationsInfo where DonationDate ='" & txtDonationDt.Text & "' and TRANSACTION_NUMBER='" & txtTrnsNumber.Text & "' and MEMBERID =" & memberid & " and AMOUNT=" & txtDonation.Text.Trim.Replace(",", "") & " and  DepositSlip=" & txtdsn.Text.Trim & " AND DonationID not in (" & Request.QueryString("DonationID") & ")") > 0 Then
                        lblError.Text = "Duplicate entries for the same check."
                        Exit Sub
                    End If
                Catch ex As Exception
                    'Response.Write("select COUNT(*) from DonationsInfo where DonationDate ='" & txtDonationDt.Text & "' and TRANSACTION_NUMBER='" & txtTrnsNumber.Text & "' and MEMBERID =" & memberid & " and AMOUNT=" & txtDonation.Text.Trim & " and  DepositSlip=" & txtdsn.Text.Trim & " AND DonationID not in (" & Request.QueryString("DonationID") & ")")
                    'Response.Write(ex.ToString())
                    'Exit Sub
                End Try
                Update()
            End If
        Else
            Update()
        End If
    End Sub

    Protected Sub Update()
        Dim donation As New Donation()
        donation.MemberID = Convert.ToInt32(Request.QueryString("MemberID"))
        'donation.TransactionNumber = Convert.ToString(Request.QueryString("TransNumber"))
        donation.TransactionNumber = txtTrnsNumber.Text
        donation.Amount = txtDonation.Text
        donation.DonationDate = txtDonationDt.Text
        ' donation.MatchingEmployer = ddlEmployer.SelectedItem.ToString
        donation.InMemoryOf = txtInNameOf.Text
        donation.Remarks = txtRemarks.Text
        donation.Designation = ddlDesignation.SelectedItem.Value
        donation.Purpose = ddlDonationFor.SelectedItem.Value
        donation.Event = ddlDonationEvent.SelectedItem.ToString
        donation.Endowment = ddlEndowment.SelectedItem.Value
        donation.EventID = ddlDonationEvent.SelectedItem.Value
        donation.Status = ddlEvent.SelectedItem.Text
        donation.DonationID = Convert.ToInt32(Request.QueryString("DonationID"))
        donation.MatchingFlag = ddlMachingFlag.SelectedItem.Value
        donation.BankID = ddlBankId.SelectedValue
        If donation.MatchingFlag = "No" Then
            donation.MatchingEmployer = DBNull.Value.ToString
        Else
            donation.MatchingEmployer = ddlEmployer.SelectedItem.ToString
        End If
        donation.Method = ddlMethod.SelectedItem.Value
        donation.Anonymous = ddlAnonymous.SelectedValue
        Dim access As String = dllEGames.SelectedItem.Value
        If access = "None" Then
            access = ""
        Else
            access = access.ToString
        End If
        donation.Access = access
        donation.DepositDate = txtddate.Text
        donation.DepositSlip = txtdsn.Text
        If (donation.MatchingEmployer = "Select Employer Name" Or donation.MatchingEmployer = "") Then
            donation.MatchEmpID = 0

        Else
            donation.MatchEmpID = ddlEmployer.SelectedItem.Value
        End If
        If Request.QueryString("MemberId") Is Nothing Then
            donation.MemberID = Session("LoginID")
        Else
            donation.MemberID = Request.QueryString("MemberId")
        End If

        donation.ModifiedBy = Session("LoginID")
        donation.ModifyDate = Convert.ToDateTime(Now).ToShortDateString
        donation.DonationType = drpdonationtype.SelectedItem.Text.ToString
        donation.ChapterID = drpchapterid.SelectedValue
        donation.EventYear = ddlEventYear.Selectedvalue ' Year(CDate(txtddate.Text))
        donation.Project = txtProject.Text
        If DDSuppCode.SelectedItem.Text = "Select SuppCode" Or DDSuppCode.SelectedItem.Text = "None" Then
            donation.SuppCode = DBNull.Value.ToString

        Else
            donation.SuppCode = DDSuppCode.SelectedItem.Text
        End If
        donation.UpdateDonation(Application("ConnectionString"))
        'Update Access value with NULL for user memeberid
        'Disabled by Ferdine on Nov 02
        ''Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
        ''Dim strSql1 As String
        ''Dim strSql2 As String
        ''Dim strSql3 As String
        ''Dim strSql4 As String
        ''strSql = "Update DonationsInfo Set Access = NULL Where DonationID = " & Request.QueryString("DonationID") & "and MEMBERID = " & Request.QueryString("MemberId") & "and Access = 0"
        ''strSql1 = "Update DonationsInfo Set MatchEmpID = NULL, MATCHINGEMPLOYER = NULL Where DonationID = " & Request.QueryString("DonationID") & "and MEMBERID = " & Request.QueryString("MemberId") & "and MatchEmpID = 0"
        ''strSql2 = "Update DonationsInfo Set DepositDate = NULL where DonationID = " & Request.QueryString("DonationID") & "and MEMBERID = " & Request.QueryString("MemberId") & "and DepositDate = '1/1/1900'"
        ''strSql3 = "Update DonationsInfo Set DepositSlip = NULL where DonationID = " & Request.QueryString("DonationID") & "and MEMBERID = " & Request.QueryString("MemberId") & "and DepositSlip = '0'"
        ''strSql4 = "Update DonationsInfo Set MATCHINGEMPLOYER = NULL where DonationID = " & Request.QueryString("DonationID") & "and MEMBERID = " & Request.QueryString("MemberId") & "and MATCHINGFLAG = 'No'"
        ''Dim ds1 As DataSet
        ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
        ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql2)
        ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql3)
        ''ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)


        If Not Request.QueryString("MemberId") Is Nothing Then
            Response.Redirect("ViewDonations.aspx?Id=" & Request.QueryString("MemberId") & "&Type=" & Request.QueryString("type"))
        Else
            Response.Redirect("ViewDonations.aspx")
        End If
    End Sub

    Private Sub LoadEvents()
        'strSql = "SELECT EventCode,Name FROM Event "
        'Dim drEvents As SqlDataReader
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        'drEvents = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        'While drEvents.Read()
        '    ddlDonationEvent.Items.Add(New ListItem(drEvents(1).ToString(), drEvents(0).ToString()))
        'End While
        'ddlDonationEvent.Items.Insert(0, New ListItem("Select Event", ""))
        'ddlDonationEvent.SelectedIndex = 0
        Dim drEvents1 As DataSet
        'Dim drEvents As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))

        strSql = "Select EventID,EventCode, Name from event order by Name asc"
        drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        Dim Count1 As Integer = 0
        If (drEvents1.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                ddlDonationEvent.DataTextField = "Name"
                ddlDonationEvent.DataValueField = "EventID"
                Dim li As ListItem = New ListItem(Name)
                li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                ddlDonationEvent.Items.Add(li)
            Next i
            ddlDonationEvent.Items.Insert(0, New ListItem("Select Event", ""))
            'ddlDonationEvent.Items.Insert(1, "None")
            ddlDonationEvent.SelectedIndex = 0

        End If

    End Sub

    Private Sub LoadOrganizations()

        strSql = "SELECT AutoMemberID,Organization_Name FROM OrganizationInfo order BY Organization_Name ASC"
        Dim drEvents As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drEvents = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        While drEvents.Read()
            ddlEmployer.Items.Add(New ListItem(drEvents(1).ToString(), drEvents(0).ToString()))
        End While
        ddlEmployer.Items.Insert(0, New ListItem("Select Employer Name", ""))
        ddlEmployer.SelectedIndex = 0

        'strSql = "SELECT AutoMemberID,Organization_Name FROM OrganizationInfo order BY Organization_Name ASC "
        'Dim drEvents As SqlDataReader
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        'drEvents = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        'While drEvents.Read()
        '    ddlEmployer.Items.Add(New ListItem(drEvents(1).ToString(), drEvents(1).ToString()))
        'End While
        'ddlEmployer.Items.Insert(0, New ListItem("Select Employer Name", ""))
        'ddlEmployer.SelectedIndex = 0
    End Sub

    Private Sub LoadDonationPurpose()
        strSql = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose   "
        Dim drPurpose As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drPurpose = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        While drPurpose.Read()
            ddlDonationFor.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
            ddlEndowment.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
        End While
        ddlDonationFor.Items.Insert(0, New ListItem("Select Purpose", ""))
        ddlDonationFor.SelectedIndex = 0
        ddlEndowment.Items.Insert(0, New ListItem("Select Endowment", ""))
        ddlEndowment.SelectedIndex = 0
    End Sub

    Private Sub LoadEducationGame()
        strSql = "select ProductID, Name from Product where EventID =4 and Status = 'O'"
        Dim drGames As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim Count1 As Integer = 0


        drGames = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        If (drGames.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(drGames.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim Name As String = drGames.Tables(0).Rows(i)(1).ToString()
                Dim Product As String = drGames.Tables(0).Rows(i)(0).ToString()
                dllEGames.DataValueField = "Product"
                dllEGames.DataTextField = "Name"
                Dim li As ListItem = New ListItem(Name)
                li.Text = drGames.Tables(0).Rows(i)(1).ToString()
                li.Value = drGames.Tables(0).Rows(i)(0).ToString()

                dllEGames.Items.Add(li)
            Next i

            'dllEGames.Items.Insert(0, New ListItem("Select Game", ""))
            dllEGames.Items.Insert(0, "None")
            dllEGames.SelectedIndex = 0

        End If

    End Sub

    Private Sub LoadChapter()
        Dim dschapters As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "Select chapterID, ChapterCode from Chapter order by state,chaptercode"
        dschapters = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        Dim Count1 As Integer = 0
        If (dschapters.Tables(0).Rows.Count > 0) Then
            Count1 = Convert.ToInt32(dschapters.Tables(0).Rows.Count.ToString())

            Dim i As Integer
            For i = 0 To Count1 - 1
                Dim chapterID As String = dschapters.Tables(0).Rows(i)(0).ToString()
                Dim Name As String = dschapters.Tables(0).Rows(i)(1).ToString()
                drpchapterid.DataTextField = "Name"
                drpchapterid.DataValueField = "EventID"
                Dim li As ListItem = New ListItem(Name)
                li.Text = dschapters.Tables(0).Rows(i)(1).ToString()
                li.Value = dschapters.Tables(0).Rows(i)(0).ToString()
                drpchapterid.Items.Add(li)
            Next i
            ' drpchapterid.Items.Insert(0, New ListItem("Select Event", ""))
            drpchapterid.SelectedValue = 1

        End If
    End Sub
    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)
        'ddlEventYear.Items.Insert(0, Convert.ToString(year))
        For i As Integer = 0 To 11
            ddlEventYear.Items.Insert(i, year - (i))
        Next
        If Now.Month < 5 Then
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year - 1))
        Else
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(Now.Year))
        End If

    End Sub
End Class
