﻿using System;
using System.Net;
using System.Text;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Drawing;
/*

References

http://www.socketlabs.com/api-reference/notification-api/
 * http://www.socketlabs.com/api-reference/reporting-api/messagesfblreported/
 * http://www.socketlabs.com/api-reference/reporting-api/messagesfailed/
 * https://github.com/socketlabs/email-on-demand-examples/blob/master/DotNet/Reporting%20API/csharp.api/Program.cs
 * 

*/

public partial class SuppressionList : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["LoginID"] = 4240;
        Session["RoleID"] = 1;
        //Response.Write(Session["selChapterID"]);
        if (!IsPostBack)
        {
            
                if (Session["LoginID"] == null)
                {
                    Response.Redirect("~/Maintest.aspx");
                }
                else
                {
                   
                }

              
           
        }
       // loaddata(false);
        
    }


    
   
    protected void fillsupremovallist()
    {
        string strqry = "select F.FailCompID, I.Email, I.FirstName, I.LastName, I.HPhone, I.CPhone, I.State,I.City,I.Chapter ,F.ComplaintType,I.NewsLetter , I.AutoMemberID,I.ReqRem,convert(varchar(20),I.ReqRemDate,101) as ReqRemDate,convert(varchar(20),F.Date,101) as from Date FailComplaint F";
        string strChapter = "";
        if (Session["selChapterID"] != null)
        {
            strChapter = " and Chapterid=" + Session["selChapterID"] + " ";
        }
        strqry = strqry + " Inner Join  IndSpouse I on I.Email=F.MailTo where ComplaintType='Abuse' " + strChapter + " ";
        strqry = strqry + "and ((exists(select * from Contestant C where C.ContestYear in (year(getDate())-1,year(getDate())) and C.ParentID=I.AutoMemberID " + strChapter + " )) ";
        strqry = strqry + "or (exists (select * from CoachReg CR where CR.EventYear in (year(getDate())-1,year(getDate())) and     (CR.CMemberID=I.AutoMemberID or CR.PMemberID=I.AutoMemberID) " + strChapter + " ))";
        strqry = strqry + "or (exists (select * from Registration_OnlineWkshop OW where OW.EventYear in (year(getDate())-1,year(getDate())) and OW.MemberID=I.AutoMemberID " + strChapter + " )) or (exists (select * from Registration Reg where Reg.EventYear in (year(getDate())-1,year(getDate())) and Reg.MemberID=I.AutoMemberID " + strChapter + " ))";
        strqry = strqry + "or (exists (select * from Registration_PrepClub Prep where Prep.EventYear in (year(getDate())-1,year(getDate())) and Prep.MemberID=I.AutoMemberID " + strChapter + " ))) Order by State,Chapter,LastName,FirstName";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            Session["SuppressionRemovalList"] = ds.Tables[0];
            gvSupRemovalList.DataSource = ds;
            gvSupRemovalList.DataBind();
            gvSupRemovalList.Columns[0].Visible = false;
            divGridView.Visible = true;
        }
        else {
            divGridView.Visible = false;
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Red;
            lblAlert.Text = "No Record Exists!";
        }
    }
    protected void fillRemovalrequestList() {
        string strremovallistqry = "select F.FailCompID, I.Email, I.FirstName, I.LastName, I.HPhone, I.CPhone, I.State,I.City,I.Chapter ,";
        strremovallistqry = strremovallistqry + " F.ComplaintType,I.NewsLetter , I.AutoMemberID,I.ReqRem,convert(varchar(20),I.ReqRemDate,101) as ReqRemDate,convert(varchar(20),F.Date,101) as Date ";
        strremovallistqry=strremovallistqry+"from IndSpouse I left Join FailComplaint F on f.mailto = i.email and f.complainttype='Abuse' where i.ReqRem='Y'";
        if (Session["selChapterID"] != null)
        {
            strremovallistqry = strremovallistqry + " and I.Chapterid=" + Session["selChapterID"] + " ";
        }
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strremovallistqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            Session["RemovalRequestList"] = ds.Tables[0];
            gvSupRemovalList.DataSource = ds;
            gvSupRemovalList.DataBind();
            gvSupRemovalList.Columns[0].Visible = false;
            divGridView.Visible = true;
        }
        else {
            divGridView.Visible = false;
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Red;
            lblAlert.Text = "No Record Exists!";
        }
    }
    protected void fillUpdateRemovalFlagList() {

        string strupdateremflagqry = "select F.FailCompID, I.Email, I.FirstName, I.LastName, I.HPhone, I.CPhone, I.State,I.City,I.Chapter ,";
        strupdateremflagqry = strupdateremflagqry + " F.ComplaintType,I.NewsLetter , I.AutoMemberID,I.ReqRem,convert(varchar(20),I.ReqRemDate,101) as ReqRemDate,convert(varchar(20),F.Date,101) as Date ";
        strupdateremflagqry = strupdateremflagqry + "from IndSpouse I left Join FailComplaint F on f.mailto = i.email and f.complainttype='Abuse' where i.ReqRem='Y'";
        if (Session["selChapterID"] != null)
        {
            strupdateremflagqry = strupdateremflagqry + " and I.Chapterid=" + Session["selChapterID"] + " ";
        }
        DataSet dsUpdate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strupdateremflagqry);
        if (dsUpdate.Tables[0].Rows.Count > 0)
        {
            gvSupRemovalList.DataSource = dsUpdate;
            gvSupRemovalList.DataBind();
            gvSupRemovalList.Columns[0].Visible = true;
            Session["UpdateRemovalFlagList"] = dsUpdate.Tables[0];
            divGridView.Visible = true;
        }
        else
        {
            divGridView.Visible = false;
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Red;
            lblAlert.Text = "No Record Exists!";
        }
    }
    protected void fillUpdatesupFlag()
    {
        string strqry2 = "select F.FailCompID, I.Email, I.FirstName, I.LastName, I.HPhone, I.CPhone, I.State,I.City,I.Chapter ,F.ComplaintType,I.NewsLetter , I.AutoMemberID,I.ReqRem,convert(varchar(20),I.ReqRemDate,101) as ReqRemDate,convert(varchar(20),F.Date,101) as Date from FailComplaint F";
        string strChapter = "";
        if (Session["selChapterID"] != null)
        {
            strChapter = " and Chapterid=" + Session["selChapterID"] + " ";
        }
        strqry2 = strqry2 + " Inner Join  IndSpouse I on I.Email=F.MailTo where ComplaintType='Abuse' "+strChapter+" ";

        strqry2 = strqry2 + " and ((not exists(select * from Contestant C where C.ContestYear in (year(getDate())-1,year(getDate())) and C.ParentID=I.AutoMemberID " + strChapter + ")) ";
        strqry2 = strqry2 + "and (not exists (select * from CoachReg CR where CR.EventYear in (year(getDate())-1,year(getDate())) and (CR.CMemberID=I.AutoMemberID or CR.PMemberID=I.AutoMemberID) " + strChapter + "))";
        strqry2 = strqry2 + "and (not exists (select * from Registration_OnlineWkshop OW where OW.EventYear in (year(getDate())-1,year(getDate())) and OW.MemberID=I.AutoMemberID " + strChapter + "))";
        strqry2 = strqry2 + "and (not exists (select * from Registration Reg where Reg.EventYear in (year(getDate())-1,year(getDate())) and Reg.MemberID=I.AutoMemberID " + strChapter + "))";
        strqry2 = strqry2 + "and (not exists (select * from Registration_PrepClub Prep where Prep.EventYear in (year(getDate())-1,year(getDate())) and Prep.MemberID=I.AutoMemberID " + strChapter + "))) Order by State,Chapter,LastName,FirstName";
        DataSet dsUpdate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry2);
        if (dsUpdate.Tables[0].Rows.Count > 0)
        {
            gvSupRemovalList.DataSource = dsUpdate;
            gvSupRemovalList.DataBind();
            gvSupRemovalList.Columns[0].Visible = true;
            Session["UpdateSuppressionFlag"] = dsUpdate.Tables[0];
            divGridView.Visible = true;
        }
        else
        {
            divGridView.Visible = false;
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Red;
            lblAlert.Text = "No Record Exists!";
        }
    }
    protected void gvSupRemovalList_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (ddlSuppressionOptions.SelectedIndex == 4)
        {
            Label failEmailID = (Label)(gvSupRemovalList.Rows[e.RowIndex].Cells[2].FindControl("Email") as Label);
            string id = failEmailID.Text;

            string strupdateqry = "update IndSpouse set newsletter=3 where email='" + id + "'";
            DataSet dsupdate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strupdateqry);
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Blue;
            lblAlert.Text = "Updated Successfully";
            fillUpdatesupFlag();
        }
        else if (ddlSuppressionOptions.SelectedIndex == 3)
        {
            Label failEmailID = (Label)(gvSupRemovalList.Rows[e.RowIndex].Cells[2].FindControl("Email") as Label);
            string id = failEmailID.Text;

            string strupdateqry = "update IndSpouse set newsletter=null,ReqRem=null,ReqRemDate=null where email='" + id + "'";
            DataSet dsupdate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strupdateqry);
            lblAlert.Visible = true;
            lblAlert.ForeColor = Color.Blue;
            lblAlert.Text = "Updated Successfully";
            //fillUpdateRemovalFlagList();
        }
    }



    protected void bSubmit_Click(object sender, EventArgs e)
    {
       
            Session["SuppressionRemovalList"] = string.Empty;
            Session["UpdateSuppressionFlag"] = string.Empty;
            Session["RemovalRequestList"] = string.Empty;
            Session["UpdateRemovalFlagList"] = string.Empty;
            lblAlert.Text = "";
            if (ddlSuppressionOptions.SelectedIndex == 1)
            {
                fillsupremovallist();

            }
            else if (ddlSuppressionOptions.SelectedIndex == 2)
            {
                fillRemovalrequestList();
            }
            else if (ddlSuppressionOptions.SelectedIndex == 3)
            {
                fillUpdateRemovalFlagList();
                hfSelection.Value = "Show";
            }
            else if (ddlSuppressionOptions.SelectedIndex == 4)
            {
                fillUpdatesupFlag();
                hfSelection.Value = "Hide";

            }
            else
            {
                lblAlert.Visible = true;
                lblAlert.ForeColor = Color.Red;
                lblAlert.Text = "Choose Any one Options";
                divGridView.Visible = false;
            }
       
    }

    protected void gvSupRemovalList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        lblAlert.Text = "";
        gvSupRemovalList.PageIndex = e.NewPageIndex;
        if (ddlSuppressionOptions.SelectedIndex == 1)
        {
            fillsupremovallist();
        }
        else if (ddlSuppressionOptions.SelectedIndex == 2)
        {
            fillRemovalrequestList();
        }
        else if (ddlSuppressionOptions.SelectedIndex == 3)
        {
            fillUpdateRemovalFlagList();
        }
        else if (ddlSuppressionOptions.SelectedIndex == 4)
        {
            fillUpdatesupFlag();
        }
    }
    protected void excel_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["SuppressionRemovalList"]!=string.Empty)
            {
                DataTable dtsuppressionlist = (DataTable)Session["SuppressionRemovalList"];
                
                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment;filename=Suppression_RemovalList.xls");

                Response.Charset = "";

                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtsuppressionlist.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Suppression Removal List </td></tr>");


                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtsuppressionlist.Columns)
                {

                    Response.Write("<th>" + dc.ColumnName + "</th>");

                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtsuppressionlist.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtsuppressionlist.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else if (Session["UpdateSuppressionFlag"] != string.Empty)
            {
                DataTable dtsuppressionlist = (DataTable)Session["UpdateSuppressionFlag"];

                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment;filename=Update_SuppressionList.xls");

                Response.Charset = "";

                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtsuppressionlist.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Update Suppression List </td></tr>");


                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtsuppressionlist.Columns)
                {

                    Response.Write("<th>" + dc.ColumnName + "</th>");

                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtsuppressionlist.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtsuppressionlist.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else if (Session["RemovalRequestList"] != string.Empty)
            {
                DataTable dtremovalreqList = (DataTable)Session["RemovalRequestList"];

                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment;filename=Removal_RequestList.xls");

                Response.Charset = "";

                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtremovalreqList.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Removal Request List</td></tr>");


                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtremovalreqList.Columns)
                {

                    Response.Write("<th>" + dc.ColumnName + "</th>");

                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtremovalreqList.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtremovalreqList.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else if (Session["UpdateRemovalFlagList"] != string.Empty)
            {
                DataTable dtremovalflagList = (DataTable)Session["UpdateRemovalFlagList"];

                Response.Clear();

                Response.AppendHeader("content-disposition", "attachment;filename=Update_RemovalFlag_List.xls");

                Response.Charset = "";

                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtremovalflagList.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Update Removal Flag List</td></tr>");


                Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
                foreach (DataColumn dc in dtremovalflagList.Columns)
                {

                    Response.Write("<th>" + dc.ColumnName + "</th>");

                }
                Response.Write("</tr>");

                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtremovalflagList.Rows)
                {
                    Response.Write("<tr>");
                    for (int i = 0; i < dtremovalflagList.Columns.Count; i++)
                    {
                        Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                    }
                    Response.Write("</tr>");
                }

                Response.Write("</table></center>");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }
        catch (Exception ex)
        { 
        
        }
    }
    protected void gvSupRemovalList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if(e.Row.RowType=
        GridViewRow gvr = e.Row;
        

            string strquery = "select [national] from volunteer where memberID=" + Session["LoginID"] + " and roleID = 38";
            string natvalue = "";
            string suplist = "";
            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, strquery);
            if (ds.Tables[0].Rows.Count > 0)
            {
                natvalue = ds.Tables[0].Rows[0]["national"].ToString();
            }
            if (Session["supListFromTreasury"] != null)
            {
                suplist = Session["supListFromTreasury"].ToString();
            }
            if ((suplist.Equals("Y")) || (Convert.ToInt32(Session["RoleID"]) == 1) || (Convert.ToInt32(Session["RoleID"]) == 2) || (Convert.ToInt32(Session["RoleID"]) == 38 && natvalue.Equals("Y")))
            {
                Button upButton1 = (Button)(gvr.FindControl("upButton") as Button);
                if (upButton1 != null)
                    upButton1.Enabled = true;
            }
       if (ddlSuppressionOptions.SelectedIndex == 4)
       {
            Label lbNewsletter = (Label)(gvr.FindControl("NewsLetter") as Label);
            if (lbNewsletter != null)
            {

                if ((lbNewsletter.Text).Equals("3"))
                {

                    Button upButton = (Button)(gvr.FindControl("upButton") as Button);
                    upButton.Enabled = false;
                }
            }
        }
       

      
    
    }
}




