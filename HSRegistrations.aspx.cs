﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ApplicationBlocks.Data;
using NativeExcel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HSRegistrations : System.Web.UI.Page
{
    string conn = "";
    DataSet dsStud;
    DataSet dsDuplicateStud;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["SchoolId"] == null)
            Response.Redirect("Maintest.aspx");
        conn = Application["ConnectionString"].ToString();
        hdnLoginId.Value = Convert.ToString(Session["SchoolId"]);
        hdnSchoolId.Value = Convert.ToString(Session["SchoolId"]);

        hdnSystemId.Value = Convert.ToString(Session["SystemId"]);
        hdnStateId.Value = Convert.ToString(Session["StateId"]);
        hdnRegionId.Value = Convert.ToString(Session["RegionId"]);
    }




    protected void UploadExcel()
    {
        lblErr.Text = "";
        string ServerPath = Server.MapPath("~/UploadFiles");
        string FileName = bulkFUpload.FileName;
        string fileExt = System.IO.Path.GetExtension(bulkFUpload.FileName);
        if (!(fileExt.Equals(".xlsx") || (fileExt.Equals(".xls"))))
        {
            lblErr.Text = "The chosen file is not an excel file type. <br>Please verify and upload the correct excel file type";
            return;
        }
        bulkFUpload.PostedFile.SaveAs(ServerPath + "/" + hdnSchoolId.Value + "" + fileExt);
        IWorkbook oWorkbooks = NativeExcel.Factory.OpenWorkbook(ServerPath + "/" + hdnSchoolId.Value + "" + fileExt);
        if (oWorkbooks != null)
        {
            IWorksheet oSheet = oWorkbooks.Worksheets[1];
            NativeExcel.IRange cells = oSheet.UsedRange;
            System.Data.DataTable dt = dsStud.Tables[0].Clone();
            dt.Columns.Add("ProductCodes");
            int i;
            string studNames = "";
            int iStudentId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select isnull(max(StudentId),'" + hdnSchoolId.Value + "' +'0001') from Student where SchoolId=" + hdnSchoolId.Value + " and SystemId=" + hdnSystemId.Value));
            //  int isno = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select max(sno) from Student"));

            string SystemId = string.Empty;
            int RegionID;
            int StateID;
            string SchoolId = string.Empty;
            string StudentNo = string.Empty;
            int StudentId;
            string StudentName = string.Empty;
            string Grade = string.Empty;
            string DOB = string.Empty;
            string Gender = string.Empty;
            string Language = string.Empty;
            string EMail = string.Empty;
            string Phone = string.Empty;
            string DOJ = string.Empty;
            string Pwd = string.Empty;
            string Image = string.Empty;
            string Photo = string.Empty;
            string PhotoContentType = string.Empty;
            string Status = string.Empty;
            string ParentName = string.Empty;
            string SB = string.Empty;
            string VB = string.Empty;
            string MB = string.Empty;
            string SC = string.Empty;
            string CreatedBy = string.Empty;
            string CreatedDate = string.Empty;
            string Eventyear = DateTime.Now.Year.ToString();
            string ProductCodes = string.Empty;
            string[] ids = { };

            if (cells.Rows.Count > 2)
            {

                if ((cells[2, 5].Value == null) || (!cells[2, 5].Value.ToString().Trim().Equals("Please double check")))
                {
                    lblErr.Text = "The chosen excel sheet is not a valid format. <br>Please verify and upload the correct excel file format";
                    return;
                }
                else if ((cells[2, 6].Value == null) || (!cells[2, 6].Value.ToString().Trim().Equals("Please double check")))
                {
                    lblErr.Text = "The chosen excel sheet is not a valid format. <br>Please verify and upload the correct excel file format";
                    return;
                }
                for (i = 3; i <= cells.Rows.Count; i++)
                {
                    StudentName = cells[i, 5] != null && cells[i, 5].Value != null ? cells[i, 5].Value.ToString() : DBNull.Value.ToString();
                    studNames += "'" + StudentName.ToString().Replace("'", "''") + "'" + ",";
                }

                if (studNames.Length > 0)
                {

                    studNames = studNames.Remove(studNames.LastIndexOf(","));

                    try
                    {
                        string SQL = "SELECT isnull(Stuff((SELECT N', ' + studentname FROM Student where StudentName in (" + studNames + ") and StudentName<>'' and StudentName is not null and systemID=" + hdnSystemId.Value + " and SchoolId=" + hdnSchoolId.Value + " FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''),'')";
                        string strDupStudName = SqlHelper.ExecuteScalar(conn, CommandType.Text, SQL).ToString();



                        for (i = 3; i <= cells.Rows.Count; i++)
                        {
                            ProductCodes = "";
                            if (cells[i, 5].Value == null)
                            {
                                break;
                            }
                            dt.Rows.Add();


                            dt.Rows[i - 3]["StudentId"] = iStudentId;
                            dt.Rows[i - 3]["RegionID"] = Convert.ToInt32(Session["RegionId"]);
                            dt.Rows[i - 3]["SystemId"] = hdnSystemId.Value;
                            dt.Rows[i - 3]["StateID"] = Convert.ToInt32(Session["StateId"]);
                            dt.Rows[i - 3]["SchoolId"] = hdnSchoolId.Value;
                            dt.Rows[i - 3]["StudentNo"] = iStudentId.ToString().Substring(5, 4);
                            dt.Rows[i - 3]["StudentName"] = cells[i, 5] != null && cells[i, 5].Value != null ? cells[i, 5].Value.ToString().Replace("'", "''") : DBNull.Value.ToString();
                            dt.Rows[i - 3]["Grade"] = cells[i, 7] != null && cells[i, 7].Value != null ? cells[i, 7].Value.ToString() : null;
                            dt.Rows[i - 3]["DOB"] = DBNull.Value;
                            dt.Rows[i - 3]["Gender"] = cells[i, 8] != null && cells[i, 8].Value != null ? cells[i, 8].Value.ToString() : "";
                            dt.Rows[i - 3]["Pwd"] = iStudentId.ToString();

                            SystemId = hdnSystemId.Value;

                            RegionID = Convert.ToInt32(Session["RegionId"]);

                            StateID = Convert.ToInt32(Session["StateId"]);

                            SchoolId = hdnSchoolId.Value;
                            iStudentId = iStudentId + 1;

                            StudentId = iStudentId;

                            StudentNo = iStudentId.ToString().Substring(5, 4);
                            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                            StudentName = cells[i, 5] != null && cells[i, 5].Value != null ? cells[i, 5].Value.ToString().Replace("'", "''") : DBNull.Value.ToString();
                            StudentName = textInfo.ToTitleCase(StudentName.ToLower());
                            dt.Rows[i - 3]["StudentName"] = StudentName;
                            studNames = studNames + "'" + dt.Rows[i - 3]["StudentName"].ToString() + "',";

                            DOB = "";
                            if (cells[i, 6] != null && cells[i, 6].Value != null)
                            {
                                if (IsDate(cells[i, 6].Value.ToString()))
                                {
                                    //dt.Rows[i - 3]["DOB"] = Convert.ToDateTime(cells[i, 6].Value, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat); //Convert.ToDateTime();
                                    dt.Rows[i - 3]["DOB"] = Convert.ToDateTime(cells[i, 6].Value);
                                    DOB = dt.Rows[i - 3]["DOB"].ToString();
                                    //DOB = Convert.ToDateTime(cells[i, 6].Value).ToShortDateString();
                                }
                                else if (IsDate_IN(cells[i, 6].Value.ToString()))
                                {
                                    dt.Rows[i - 3]["DOB"] = Convert.ToDateTime(cells[i, 6].Value, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                    DOB = dt.Rows[i - 3]["DOB"].ToString();
                                }
                                else
                                {
                                    lblErr.Text = "In Line No:" + i + ",  DOB is not a Valid format.";
                                    return;
                                }
                            }
                            else
                            {
                                lblErr.Text = "In Line No:" + i + ",  DOB should not be an Empty Value.";
                                return;
                            }

                            Grade = cells[i, 7] != null && cells[i, 7].Value != null ? cells[i, 7].Value.ToString() : null;
                            if (Grade == null)
                            {
                                lblErr.Text = "In Line No:" + i + ",  Class Number should not be an Empty Value.";
                                return;
                            }
                            else
                            {
                                if (!IsGrade(Grade))
                                {
                                    lblErr.Text = "In Line No:" + i + ",  Class is not in Valid Numeric Format.";
                                    return;
                                }
                            }


                            Gender = cells[i, 8] != null && cells[i, 8].Value != null ? cells[i, 8].Value.ToString() : "";

                            Phone = "null";
                            PhotoContentType = "null";
                            Photo = "null";

                            Pwd = iStudentId.ToString();
                            if (cells[i, 9].Value != null && (cells[i, 9].Value.ToString().Trim().Equals("Y") || cells[i, 9].Value.ToString().Trim().Equals("y")))
                            {
                                dt.Rows[i - 3]["VB"] = cells[i, 9] != null && cells[i, 9].Value != null ? cells[i, 9].Value.ToString() : "";
                                VB = cells[i, 9] != null && cells[i, 9].Value != null ? cells[i, 9].Value.ToString() : "";
                                ProductCodes += "JVB,";
                            }
                            if (cells[i, 10].Value != null && (cells[i, 10].Value.ToString().Trim().Equals("Y") || cells[i, 10].Value.ToString().Trim().Equals("y")))
                            {
                                dt.Rows[i - 3]["VB"] = cells[i, 10] != null && cells[i, 10].Value != null ? cells[i, 10].Value.ToString() : "";
                                VB = cells[i, 10] != null && cells[i, 10].Value != null ? cells[i, 10].Value.ToString() : "";
                                if (ProductCodes.Contains("JVB"))
                                {
                                    lblErr.Text = "In Line No:" + i + ",  Contest details should not be duplicated for the same product group.<br>Please update the proper & unique contest details for the student and re-upload.";
                                    return;
                                }
                                else
                                {
                                    ProductCodes += "SVB,";
                                }
                            }
                            if (cells[i, 11].Value != null && (cells[i, 11].Value.ToString().Trim().Equals("Y") || cells[i, 11].Value.ToString().Trim().Equals("y")))
                            {
                                dt.Rows[i - 3]["MB"] = cells[i, 11] != null && cells[i, 11].Value != null ? cells[i, 11].Value.ToString() : "";
                                MB = cells[i, 11] != null && cells[i, 11].Value != null ? cells[i, 11].Value.ToString() : "";
                                ProductCodes += "MB2,";
                            }
                            if (cells[i, 12].Value != null && (cells[i, 12].Value.ToString().Trim().Equals("Y") || cells[i, 12].Value.ToString().Trim().Equals("y")))
                            {
                                dt.Rows[i - 3]["MB"] = cells[i, 12] != null && cells[i, 12].Value != null ? cells[i, 12].Value.ToString() : "";
                                MB = cells[i, 12] != null && cells[i, 12].Value != null ? cells[i, 12].Value.ToString() : "";
                                if (ProductCodes.Contains("MB2"))
                                {
                                    lblErr.Text = "In Line No:" + i + ",  Contest details should not be duplicated for the same product group.<br>Please update the proper & unique contest details for the student and re-upload.";
                                    return;
                                }
                                else
                                {
                                    ProductCodes += "MB3,";
                                }
                            }
                            if (ProductCodes.Length == 0)
                            {
                                lblErr.Text = "There is no (Y) in Line No:" + i + ",  Please update Contest details (Y) under the corresponding column for the Student and re-upload.";
                                return;
                            }
                            else
                            {
                                dt.Rows[i - 3]["ProductCodes"] = ProductCodes.Substring(0, ProductCodes.Length - 1);
                            }
                            dt.Rows[i - 3]["CreatedBy"] = Session["SchoolId"];
                            CreatedBy = Session["SchoolId"].ToString();
                            dt.Rows[i - 3]["CreateDate"] = DateTime.Now;
                            CreatedDate = DateTime.Now.ToString();


                        }

                        DataSet DSWithDupAndOriginal = checkduplicate(dt);
                        //int iDelete = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "delete from DuplicateStudent where SchoolId=" + Session["Schoolid"] + ""));
                        if (DSWithDupAndOriginal.Tables[3].Rows.Count > 0)
                        {
                            insertDuplicates(DSWithDupAndOriginal.Tables[3]);
                        }
                        DataSet dsDuplicateExists = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from DuplicateStudent where Schoolid=" + Session["Schoolid"] + " and Status='Pending' and IsActive is null and Year=" + DateTime.Now.Year + "");
                        if (dsDuplicateExists.Tables[0].Rows.Count > 0)
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:$('#ashowpopup').trigger('click');", true);


                            //ClientScript.RegisterStartupScript(this.GetType(), "jquery", "<script type=\"text/JavaScript\" language=\"javascript\">showPopup();</script>");
                            //this.RegisterJS("showPopup();");
                            lblSuccess.ForeColor = Color.Blue;
                            // Session["DuplicateList"] = dsDuplicateExists.Tables[0];
                            lblSuccess.Text = "" + DSWithDupAndOriginal.Tables[1].Rows.Count + " Students Registered Successfully.";
                            lblDuplicateError.ForeColor = Color.Red;
                            lblDuplicateError.Text = lblErr.Text + "<br>The following records are duplicates. Please Edit/Update the student details and click on submit button.";
                            //gvRejectedDuplicates.DataSource = dsDuplicateExists.Tables[0];
                            //gvRejectedDuplicates.DataBind();
                        }
                        else if (DSWithDupAndOriginal.Tables[1].Rows.Count > 0)
                        {
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "" + DSWithDupAndOriginal.Tables[1].Rows.Count + " Students Registered Successfully.";

                        }
                         
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblErr.Text = "No record to upload";
                    return;
                }
            }
            else
            {
                lblErr.Text = "No record to upload";
                return;
            }
        }
        else
        {
            lblErr.Text = "The chosen excel sheet is not a valid format. <br>Please verify and upload the correct excel file format";
            return;
        }
    }

    protected DataSet checkduplicate(System.Data.DataTable dt)
    {
        Boolean IsActive = true;
        DataSet returnDS = new DataSet();
        string strisMasterExist_WithDOB = "";
        string strisMasterExist_WithOutDOB = "";
        System.Data.DataTable dtCopy = dt.Copy();
        dtCopy.TableName = "Original";
        dtCopy.Columns.Add("IsDuplicate");
        dtCopy.Columns.Add("IsNewinDuptbl");
        // dtCopy.Columns.Add("Remarks");
        DataSet dsDuplicateLists;
        if (IsActive == true)
        {
            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update DuplicateStudent set IsActive='N' where Schoolid=" + Session["Schoolid"] + "");
            IsActive = false;
        }
        System.Data.DataTable dtMasterDuplicateList = dsDuplicateStud.Tables[0].Clone();
        dtMasterDuplicateList.TableName = "Duplicate";
        int iindex = 0;
        string strCatStudentid = "";
        foreach (DataRow dr in dt.Rows)
        {
            try
            {
                DataRow drCopy = dtCopy.Rows[iindex];
                strisMasterExist_WithDOB = "select * from Student where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and  (DOB='" + dr["DOB"] + "' or DOB is null)";
                strisMasterExist_WithOutDOB = "select * from Student where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and (Grade+1)='" + dr["Grade"] + "'";
                DataSet dsMaster_WithDOB = SqlHelper.ExecuteDataset(conn, CommandType.Text, strisMasterExist_WithDOB);
                DataSet dsMaster_WithOutDOB = SqlHelper.ExecuteDataset(conn, CommandType.Text, strisMasterExist_WithOutDOB);
                if (dsMaster_WithDOB.Tables[0].Rows.Count == 1)
                {
                    drCopy["Studentid"] = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select StudentID from Student where SchoolId=" + dr["Schoolid"] + " and SystemId=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and (DOB='" + dr["DOB"] + "' or DOB is null)").Tables[0].Rows[0][0].ToString();
                    drCopy["StudentNo"] = drCopy["Studentid"].ToString().Substring(5, 4);

                }
                else if (dsMaster_WithDOB.Tables[0].Rows.Count > 1)
                {
                    dsDuplicateLists = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select Schoolid,Studentid,StudentName,Gender,Grade,CONVERT(datetime, DOB, 103) as DOB from Student where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "'");

                    foreach (DataRow drDuplicate in dsDuplicateLists.Tables[0].Rows)
                    {
                        dtMasterDuplicateList.Rows.Add(drDuplicate.ItemArray);
                    }
                    drCopy["IsDuplicate"] = "Y";
                    int i = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from DuplicateStudent where Year=" + DateTime.Now.Year + " and SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "' and status='Pending'"));
                    if (i == 0)
                    {
                        drCopy["IsNewinDuptbl"] = 'Y';
                    }
                    else
                    {
                        //Update ProductCodes is record already exists
                        string updateQry = "";
                        updateQry = "update DuplicateStudent set ProductCodes=ProductCodes+'," + dr["ProductCodes"] + "' , ModifiedBy=" + Session["Schoolid"] + " , ModifiedDate='" + DateTime.Now + "' where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "' and status='Pending' and ProductCodes not like '%" + dr["ProductCodes"] + "%'";
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, updateQry);
                        //update DuplicateStudent IsActive null for existing records in the excel file
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update DuplicateStudent set IsActive=null, ModifiedBy=" + Session["Schoolid"] + " , ModifiedDate='" + DateTime.Now + "' where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "' and status='Pending'");
                    }
                    drCopy["Status"] = "Pending";
                    iindex++;
                    continue;
                }
                else
                {
                    if (dsMaster_WithOutDOB.Tables[0].Rows.Count == 1)
                    {
                        drCopy["Studentid"] = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select StudentID from Student where SchoolId=" + dr["Schoolid"] + " and SystemId=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and (Grade+1)='" + dr["Grade"] + "'").Tables[0].Rows[0][0].ToString();
                        drCopy["StudentNo"] = drCopy["Studentid"].ToString().Substring(5, 4);

                        //SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update Student set DOB=" + dr["DOB"] + " where Studentid=" + dr["Studentid"] + "");
                    }
                    else if (dsMaster_WithOutDOB.Tables[0].Rows.Count > 1)
                    {
                        dsDuplicateLists = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select Schoolid,Studentid,StudentName,Gender,Grade,CONVERT(datetime, DOB, 103) as DOB from Student where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Gender='" + dr["Gender"] + "' and (Grade+1)='" + dr["Grade"] + "'");

                        foreach (DataRow drDuplicate in dsDuplicateLists.Tables[0].Rows)
                        {
                            dtMasterDuplicateList.Rows.Add(drDuplicate.ItemArray);
                        }
                        drCopy["IsDuplicate"] = "Y";
                        int i = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from DuplicateStudent where Year=" + DateTime.Now.Year + " and SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and status='Pending'"));
                        if (i == 0)
                        {
                            drCopy["IsNewinDuptbl"] = 'Y';
                        }
                        else
                        {
                            //Update ProductCodes is record already exists
                            string updateQry = "";
                            updateQry = "update DuplicateStudent set ProductCodes=ProductCodes+'," + dr["ProductCodes"] + "' , ModifiedBy=" + Session["Schoolid"] + " , ModifiedDate='" + DateTime.Now + "' where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "' and status='Pending' and ProductCodes not like '%" + dr["ProductCodes"] + "%'";
                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, updateQry);
                            //update DuplicateStudent IsActive null for existing records in the excel file
                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update DuplicateStudent set IsActive=null, ModifiedBy=" + Session["Schoolid"] + " , ModifiedDate='" + DateTime.Now + "' where SchoolId=" + dr["Schoolid"] + " and systemID=" + dr["Systemid"] + " and StudentName='" + dr["StudentName"] + "' and Grade='" + dr["Grade"] + "' and Gender='" + dr["Gender"] + "' and  DOB='" + dr["DOB"] + "' and status='Pending'");
                        }
                        drCopy["Status"] = "Pending";
                        iindex++;
                        continue;
                        //drCopy.Delete();
                    }
                    else
                    {
                        drCopy["Studentid"] = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select isnull(max(StudentId),(CONVERT(varchar(10),'" + dr["Schoolid"] + "')+'0000'))+1 from Student where SchoolId=" + dr["Schoolid"] + " and SystemId=" + dr["Systemid"] + "").Tables[0].Rows[0][0].ToString();
                        drCopy["StudentNo"] = drCopy["Studentid"].ToString().Substring(5, 4);
                    }
                }

               // string cmdtext = "usp_BulkStudentNewRegInsertion 	" + drCopy["Systemid"] + "," + drCopy["Regionid"] + "," + drCopy["Stateid"] + ",	" + drCopy["Schoolid"] + ",'" + drCopy["StudentNo"] + "', " + drCopy["Studentid"] + ",'" + drCopy["StudentName"] + "'," + drCopy["Grade"] + ",'" + drCopy["DOB"] + "','" + drCopy["Gender"] + "','" + drCopy["Language"] + "','" + drCopy["EMail"] + "','" + drCopy["Phone"] + "','" + drCopy["DOJ"] + "','" + drCopy["Pwd"] + "','" + drCopy["Image"] + "','" + drCopy["PhotoContentType"] + "','" + drCopy["Status"] + "','" + drCopy["ParentName"] + "','" + drCopy["SB"] + "','" + drCopy["VB"] + "','" + drCopy["MB"] + "','" + drCopy["SC"] + "'," + drCopy["CreatedBy"] + ",'" + drCopy["CreateDate"] + "','" + drCopy["ProductCodes"] + "', " + DateTime.Now.Year + "";
                string cmdtext = " usp_HSStudentInsertion " + drCopy["Systemid"].ToString() + "," + drCopy["Regionid"].ToString() + ", " + drCopy["Stateid"].ToString() + ", " + drCopy["Schoolid"].ToString() + ", 0,0,'" + drCopy["StudentName"].ToString() + "', '" + drCopy["Grade"].ToString() + "', '" + drCopy["DOB"].ToString() + "', '" + drCopy["Gender"].ToString() + "', '" + drCopy["Language"].ToString() + "', '" + drCopy["EMail"].ToString() + "', '" + drCopy["Phone"].ToString() + "', '" + DateTime.Now.ToShortDateString() + "', '" + drCopy["Pwd"].ToString() + "','','','" + drCopy["Status"].ToString() + "','" + drCopy["ParentName"].ToString() + "', " + drCopy["CreatedBy"].ToString() + ", '" + DateTime.Now.ToShortDateString() + "'," + DateTime.Now.Year + ", 'A' ";

                DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, cmdtext);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        strCatStudentid = strCatStudentid + "" + ds.Tables[0].Rows[0][0].ToString() + ",";
                    }
            }
            catch (Exception ex)
            {
                // lblErr.Text = "In Line No:" + iindex + ", " + ex.Message + " <br>Please do the correction and re-upload";
            }
            iindex++;
        }
        strCatStudentid = strCatStudentid.Substring(0, strCatStudentid.Length - 1);
        string removeOldStudentlist = "delete from HSContestant where Schoolid=" + hdnSchoolId.Value + " and EventYear=" + DateTime.Now.Year + " and Studentid not in (" + strCatStudentid + ")";
        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, removeOldStudentlist);

        dtCopy.AcceptChanges();
        dtMasterDuplicateList.AcceptChanges();
        returnDS.Tables.Add(dtMasterDuplicateList);
        System.Data.DataTable DataTableOriginal = GetFilteredTable(dtCopy, " IsDuplicate is null");
        returnDS.Tables.Add(DataTableOriginal);
        System.Data.DataTable DataTableDuplicate = GetFilteredTable(dtCopy, " IsDuplicate ='Y'");
        DataTableDuplicate.TableName = "FileDuplicates";
        returnDS.Tables.Add(DataTableDuplicate);
        System.Data.DataTable DataTableDuplicateInsert = GetFilteredTable(dtCopy, " IsNewinDuptbl ='Y'");
        DataTableDuplicateInsert.TableName = "InsertDuplicates";
        returnDS.Tables.Add(DataTableDuplicateInsert);
        returnDS.AcceptChanges(); 
        return returnDS; 
    }
    public static DataTable GetFilteredTable(DataTable sourceTable, string selectFilter)
    {
        DataTable filteredTable = sourceTable.Clone();
        DataRow[] rows = sourceTable.Select(selectFilter);
        foreach (DataRow row in rows)
        {
            filteredTable.ImportRow(row);
        }
        return filteredTable;
    }

    protected bool IsDate(String date)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(date);
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected bool IsDate_IN(String date)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(date, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected bool IsGrade(String Grade)
    {
        try
        {
            int grade = Convert.ToInt32(Grade);
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected void btnBulkUpload_Click(object sender, System.EventArgs e)
    {
        UploadExcel();
    }

    private void SaveFile(string sVirtualPath, FileUpload objFileUpload, string fileName)
    {

        string folderPath = Server.MapPath("~/Profile/Student/");

        //Check whether Directory (Folder) exists.
        if (!Directory.Exists(folderPath))
        {
            //If Directory (Folder) does not exists. Create it.
            Directory.CreateDirectory(folderPath);
        }

        //Save the File to the Directory (Folder).
        string NewFileName = hdnStudentId.Value;
        objFileUpload.SaveAs(folderPath + Path.GetFileName(fileName));

        //if (objFileUpload.HasFile)
        //{
        //    objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), fileName));
        //}
        //else
        //{

        //}
    }

    protected void BtnFileUpload_Click(object sender, EventArgs e)
    {
        if (FStudentPhoto.HasFile)
        {
            Byte[] bytes = new Byte[] { };
            if (FStudentPhoto.HasFile)
            {
                Stream fs = FStudentPhoto.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                bytes = br.ReadBytes((Int32)fs.Length);
            }

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            string FileName = "";
            string Extension = System.IO.Path.GetExtension(this.FStudentPhoto.PostedFile.FileName);
            FileName = hdnStudentId.Value + "" + Extension;
            string filePath = "Profile/Student/" + FileName + "";

            SqlCommand cmd = new SqlCommand();
            cmdText = " Update Student set Photo=@photo, PhotoContentType=@PhotoContentType, PhotoPath=@PhotoPath where StudentId=@StudentId";
            cmd = new SqlCommand(cmdText);
            cmd.Parameters.Add("@Photo", SqlDbType.Binary).Value = bytes;
            cmd.Parameters.Add("@PhotoContentType", SqlDbType.VarChar).Value = FStudentPhoto.PostedFile.ContentType.ToString();
            cmd.Parameters.Add("@PhotoPath", SqlDbType.VarChar).Value = filePath;
            cmd.Parameters.Add("@StudentId", SqlDbType.Int).Value = hdnStudentId.Value;
            NSFDBHelper objNSF = new NSFDBHelper();

            if (objNSF.InsertUpdateData(cmd) == true)
            {
                hdnIsSuccess.Value = "1";
                SaveFile(Server.MapPath("~/Profile/Student/"), FStudentPhoto, FileName);
            }
        }
        else
        {
            hdnIsSuccess.Value = "1";
        }
    }
    protected void insertDuplicates(System.Data.DataTable dtDup)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //Set the database table name
                    sqlBulkCopy.DestinationTableName = "DuplicateStudent";

                    //[OPTIONAL]: Map the DataTable columns with that of the database table
                    //sqlBulkCopy.ColumnMappings.Add("sno", "sno");
                    sqlBulkCopy.ColumnMappings.Add("SystemId", "SystemId");
                    sqlBulkCopy.ColumnMappings.Add("RegionID", "RegionID");
                    sqlBulkCopy.ColumnMappings.Add("StateID", "StateID");

                    sqlBulkCopy.ColumnMappings.Add("SchoolId", "SchoolId");

                    sqlBulkCopy.ColumnMappings.Add("StudentName", "StudentName");
                    sqlBulkCopy.ColumnMappings.Add("Grade", "Grade");
                    sqlBulkCopy.ColumnMappings.Add("DOB", "DOB");
                    sqlBulkCopy.ColumnMappings.Add("Gender", "Gender");
                    sqlBulkCopy.ColumnMappings.Add("Language", "Language");
                    sqlBulkCopy.ColumnMappings.Add("EMail", "EMail");
                    sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                    sqlBulkCopy.ColumnMappings.Add("ParentName", "ParentName");
                    sqlBulkCopy.ColumnMappings.Add("ProductCodes", "ProductCodes");
                    //sqlBulkCopy.ColumnMappings.Add("Remarks", "Remarks");
                    sqlBulkCopy.ColumnMappings.Add("Status", "Status");
                    sqlBulkCopy.ColumnMappings.Add("CreatedBy", "CreatedBy");
                    sqlBulkCopy.ColumnMappings.Add("CreateDate", "CreateDate");
                    con.Open();
                    sqlBulkCopy.WriteToServer(dtDup);
                    con.Close();
                    //lblErr.ForeColor = Color.Blue;
                    //lblErr.Text = "Inserted Successfully.";
                    //   btnSubmit_Click(btnSubmit,new EventArgs());
                }
            }
        }
        catch
        {
        }
    }
}