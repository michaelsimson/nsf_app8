<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.UpdatePhotoStatus" CodeFile="UpdatePhotoStatus.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UpdatePhotoStatus</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table id="tblMain" width="60%">
				<tr>
					<td class="Heading" colSpan="3">Update National Finalist Photo Info</td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" width="25%">First Name:</td>
					<td vAlign="top" noWrap align="left" width="25%"><asp:textbox id="txtFirstName" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstName" runat="server" ErrorMessage="Should be entered" Display="Dynamic"
							ControlToValidate="txtFirstName"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" width="25%">Last name:</td>
					<td vAlign="top" noWrap align="left" width="25%"><asp:textbox id="txtLastName" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvLastName" runat="server" ErrorMessage="Should be entered" Display="Dynamic"
							ControlToValidate="txtLastName"></asp:requiredfieldvalidator></td>
				</tr>
				<tr id="trContestant" runat="server">
					<td class="ItemLabel" vAlign="top" noWrap align="right" width="25%">Contest Name 
						Matching:</td>
					<td><asp:radiobuttonlist id="rblContestants" runat="server" CssClass="SmallFont"></asp:radiobuttonlist></td>
				</tr>
				<tr id="trPhoto" runat="server">
					<td class="ItemLabel" noWrap align="right">Photo File Name:&nbsp;</td>
					<td noWrap align="left"><asp:textbox id="txtPhotoFile" runat="server" CssClass="SmallFont" Width="216px"></asp:textbox></td>
					<td><asp:button id="btnUpdatePhoto" runat="server" CssClass="FormButton" Text="Update Photo Info"
							CausesValidation="False"></asp:button></td>
				</tr>
				<tr>
					<td class="ItemCenter" colSpan="2">
						<asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Find Contestant"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button id="btnReset" runat="server" CssClass="FormButton" Text="Reset"></asp:button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

 

 
 
 