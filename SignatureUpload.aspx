﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SignatureUpload.aspx.vb" Inherits="SignatureUpload"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left" >
<asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
<div align="center" style ="width:100%">
<table cellpadding ="3" cellspacing = "0" border = "0" align="center">
<tr><td align="center" class="title02">Upload Signatures for <asp:Label ID="lblChapter" CssClass="title02" runat="server" /> </td></tr>
<tr><td align="center" ><br /><asp:DropDownList Width="200px" ID="ddlSign" OnSelectedIndexChanged="ddlSign_SelectedIndexChanged" AutoPostBack="true" DataTextField="VName" DataValueField="MemberID" runat="server">
    </asp:DropDownList></td></tr>
<tr><td align="center" >
    <asp:Label ID="lblErr" runat="server" ForeColor = "Red"></asp:Label>
</td> </tr> 
<tr runat="server" id="trupload" visible = "false" ><td align="center" >
<br />
<table cellpadding ="3" cellspacing = "0" border = "0" align="center">
<tr><td align="center" >
    <asp:FileUpload ID="FileUpload1" runat="server" /></td> </tr> <tr><td align="center" >
    <asp:Label ID="lblFilname" runat="server"></asp:Label>
    </td> </tr> <tr><td align="center" >
    <asp:Button ID="btnUpload" OnClick="btnUpload_Click" runat="server" Text="Upload" /></td> </tr></table> 
</td></tr>
<tr><td align="center" ><br />
 <asp:GridView  HorizontalAlign="Center"   ID="GridMemberSign" DataKeyNames="MemberID" AutoGenerateColumns = "false" runat="server"  RowStyle-CssClass="SmallFont">
     <Columns>
        <asp:BoundField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"  DataField="VName" headerText="Name" ></asp:BoundField>
        <asp:ImageField HeaderText="Signature" DataImageUrlField="SignFileName" ControlStyle-Height="60px" ControlStyle-Width="150px" ></asp:ImageField>
       
     </Columns> 
 </asp:GridView> 
    </td></tr>
<tr><td align="center" >
    * only .jpg file is accepted.</td></tr>



</table> 
</div> 
</asp:Content>

