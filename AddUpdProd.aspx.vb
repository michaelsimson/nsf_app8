Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddUpdProd
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            GetRecords()
            GetEvent()
        End If
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Product")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("EventID") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from Product where ProductId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtProdCode.Text = dsRecords.Tables(0).Rows(0)("ProductCode").ToString()
        txtProdName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByText(dsRecords.Tables(0).Rows(0)("EventCode").ToString()))
        txtProdCode.Enabled = False
        GetProdGrp(ddlEvent.SelectedValue)
        ddlProd.SelectedIndex = ddlProd.Items.IndexOf(ddlProd.Items.FindByText(dsRecords.Tables(0).Rows(0)("ProductGroupCode").ToString()))
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Status").ToString()))
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        If BtnAdd.Text = "Add" Then
            If txtProdCode.Text.Length < 1 Then
                Pnl3Msg.Text = "Please Enter Product Code"
            ElseIf txtProdName.Text.Length < 1 Then
                Pnl3Msg.Text = "Please Enter Product Name"
            ElseIf ddlProd.SelectedItem.Text = "Select Product Group" Then
                Pnl3Msg.Text = "Please Select Product Group Code"
            ElseIf ddlStatus.SelectedItem.Text = "Select Status" Then
                Pnl3Msg.Text = "Please Select Status"
            Else
                Dim cnt As Integer = 0
                Strsql = "Select count(ProductCode) from Product where ProductCode='" & txtProdCode.Text & "' AND EventId=" & ddlEvent.SelectedItem.Value & " And ProductGroupId=" & ddlProd.SelectedItem.Value
                cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                If cnt = 0 Then
                    Strsql = "Insert Into Product(ProductCode,Name, ProductGroupId, ProductGroupCode, EventId, EventCode, Status, CreateDate, CreatedBy) Values ('"
                    Strsql = Strsql & txtProdCode.Text & "','" & txtProdName.Text & "'," & ddlProd.SelectedItem.Value & ",'" & ddlProd.SelectedItem.Text & "'," & ddlEvent.SelectedItem.Value & ",'"
                    Strsql = Strsql & ddlEvent.SelectedItem.Text & "','" & ddlStatus.SelectedItem.Value & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "','" & Session("LoginID") & "')"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                    GetRecords()
                    clear()
                    Pnl3Msg.Text = "Record Added Successfully"
                Else
                    Pnl3Msg.Text = "The Code with same Event and Product Group already Exist"
                End If
            End If
        ElseIf BtnAdd.Text = "Update" Then
            If txtProdCode.Text.Length < 1 Then
                Pnl3Msg.Text = "Please Enter Product Code"
            ElseIf txtProdName.Text.Length < 1 Then
                Pnl3Msg.Text = "Please Enter Product Name"
            ElseIf ddlEvent.Text = "Select Product Group" Then
                Pnl3Msg.Text = "Please Select Product Group Code"
            ElseIf ddlStatus.Text = "Select Status" Then
                Pnl3Msg.Text = "Please Select Status"
            Else
                Strsql = "Update Product Set EventId=" & ddlEvent.SelectedItem.Value & ", EventCode='" & ddlEvent.SelectedItem.Text & "', ModifyDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy='" & Session("LoginID") & "', ProductCode='" & txtProdCode.Text & "',Name = '" & txtProdName.Text & "',ProductGroupCode = '" & ddlProd.SelectedItem.Text & "',ProductGroupId = " & ddlProd.SelectedItem.Value & ",Status = '" & ddlStatus.SelectedItem.Value & "'  Where ProductID=" & Session("EventID")
                'MsgBox(Strsql)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                GetRecords()
                clear()
                Pnl3Msg.Text = "Record Updated Successfully"
                BtnAdd.Text = "Add"
            End If
        End If
    End Sub
    Private Sub clear()
        txtProdCode.Text = ""
        txtProdName.Text = ""
        BtnAdd.Text = "Add"
        ddlEvent.ClearSelection()
        ddlEvent.Items.FindByText("Select Event").Selected = True
        ddlStatus.ClearSelection()
        ddlStatus.Items.FindByText("Select Status").Selected = True
        ddlProd.ClearSelection()
        ddlProd.Items.FindByText("Select Product Group").Selected = True
        ddlProd.Enabled = False
        txtProdCode.Enabled = True

    End Sub
    Private Sub GetEvent()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Event order by EventId Desc")
        While dsRecords.Read
            ddlEvent.Items.Insert(++i, New ListItem(dsRecords("EventCode"), dsRecords("EventId")))
        End While
    End Sub
    Private Sub GetProdGrp(ByVal EveID As Integer)
        ddlProd.Items.Clear()
        ddlProd.Items.Insert(0, New ListItem("Select Product Group"))
        ddlProd.Items(0).Selected = True
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        Dim strsql As String = "SELECT * FROM ProductGroup where EventId=" & EveID & " order by ProductGroupId Desc"
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        While dsRecords.Read
            ddlProd.Items.Insert(++i, New ListItem(dsRecords("ProductGroupCode"), dsRecords("ProductGroupId")))
        End While
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        If ddlEvent.SelectedItem.Text = "Select Event" Then
            ddlProd.Enabled = False
        Else
            ddlProd.Enabled = True
            GetProdGrp(ddlEvent.SelectedItem.Value)
        End If
    End Sub
End Class
