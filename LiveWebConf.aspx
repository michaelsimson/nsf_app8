﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiveWebConf.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="LiveWebConf" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script language="javascript" type="text/javascript">
            function ConfirmmeetingCancel() {

                var sessionKey = document.getElementById("<%=hdnSessionKey.ClientID%>").value;
                var WebExID = document.getElementById("<%=hdnWebExID.ClientID%>").value;
                var WebExPwd = document.getElementById("<%=hdnPwd.ClientID%>").value;
                document.getElementById("<%=btnTerminateMeeting.ClientID%>").click();


            }
            function confirmKillMeeting() {
                if (confirm("Are you sure you want to terminate?")) {
                    ConfirmmeetingCancel();
                }
            }

        </script>
    </div>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <asp:Button ID="btnTerminateMeeting" runat="server" Text="Terminate Meeting" Style="display: none;" OnClick="btnTerminateMeeting_Click" />
    <table id="tblLiveConfSessions" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Live Web Conference Sessions
                    </h2>
                </center>
            </td>

        </tr>
        <tr>
            <td align="center">

                <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
                <div style="clear: both; margin-top: 10px;"></div>
                <div align="center" style="font-weight: bold; color: #64A81C;">
                    Table 1 : Current Live Sessions
                <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
                    <Columns>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                            <ItemTemplate>
                                <div style="display: none;">
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                    <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                                    <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                    <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                    <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                                    <asp:Label ID="lblHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'>'></asp:Label>
                                </div>
                                <asp:Button ID="btnModifyMeeting" runat="server" Text="Terminate" CommandName="Terminate" />

                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                        <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                        <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                        <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                        <asp:TemplateField HeaderText="Coach">

                            <ItemTemplate>

                                <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                        <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                        <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>
                        <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:TemplateField HeaderText="Begin Time">

                            <ItemTemplate>
                                <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Time">

                            <ItemTemplate>
                                <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>

                        <asp:BoundField DataField="UserID" HeaderText="User ID" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="PWD" HeaderText="Pwd" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                        <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>

                    </Columns>

                    <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                    <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
                </asp:GridView>
                </div>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center" style="font-weight: bold; color: #64A81C;">
                    Table 2 : Follow up Sessions
                <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdFollowUpSessions" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" EnableModelValidation="True" PageSize="20" AllowPaging="true">
                    <Columns>

                        <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                        <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                        <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                        <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                        <asp:TemplateField HeaderText="Coach">

                            <ItemTemplate>

                                <asp:LinkButton runat="server" ID="lnkCoach1" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                        <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>
                        <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:TemplateField HeaderText="Begin Time">

                            <ItemTemplate>
                                <asp:Label ID="hlTime1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Time">

                            <ItemTemplate>
                                <asp:Label ID="hlEndTime1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>

                        <asp:BoundField DataField="UserID" HeaderText="User ID"></asp:BoundField>
                        <asp:BoundField DataField="PWD" HeaderText="Pwd"></asp:BoundField>
                        <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                        <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>

                    </Columns>

                    <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                    <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
                </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    <a id="ancKillMeeting" style="display: none" target="_blank"></a>
    <input type="hidden" id="hdnSessionKeys" runat="server" value="" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdnPwd" value="0" runat="server" />

    <input type="hidden" id="hdnStartDate" value="0" runat="server" />
    <input type="hidden" id="hdnHostID" value="0" runat="server" />
</asp:Content>
