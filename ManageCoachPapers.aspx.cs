using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Collections.Generic;
using VRegistration;
using System.Drawing;


#region " Class ManageTestPapers "

public partial class ManageTestPapers : System.Web.UI.Page
{
    public int PreviousOptIndex = 0;
    public int CurrentOptIndex = 0;
    #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion

    #region " Event Handlers "
    string upFile;
    int MemberID = -1;
    int RoleCode = -1;
    int AccLevel = -1;
    bool SearchFlag = false;
    int SectionFlag = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = "31453";
        //Session["LoggedIn"] = true;
        //Session["RoleId"] = 96;
        lblValText.Text = "";
        lblError.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
            Response.Redirect("login.aspx?entry=v");

        if (!IsPostBack)
        {
            Tab1.CssClass = "Clicked";
            ddlDocType.Items.Insert(0, new ListItem("[Select Doc. Type]", "-1"));
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "96" || Session["RoleId"].ToString() == "30" || Session["RoleId"].ToString() == "89")
                {
                    hdnpaperType.Value = "UploadPapers";
                    hdntlead.Value = "Y";
                    GetDropDownChoice(dllfileChoice, true);
                    dllfileChoice.Visible = true;
                    lblNoPermission.Visible = false;
                    Label8.Text = "Upload Papers";

                    LoadUploadPanel();
                    hdnPGId.Value = ddlProductGroup.SelectedValue;
                    hdnDocType.Value = ddlDocType.SelectedValue;
                    hdnSectionR.Value = ddlSections.SelectedValue;
                    string sqltext = "    select distinct CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] AS[IDandCode], p.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlContestYear.SelectedValue + " and cs.Accepted = 'Y' and cs.Semester='" + ddlSemester.SelectedValue + "'";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqltext);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        ddlProduct.DataTextField = "Name";
                        ddlProduct.DataValueField = "IDandCode";
                        ddlProduct.DataSource = ds;
                        ddlProduct.DataBind();
                        ddlProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                        ddlProduct.SelectedIndex = 0;
                    }
                }
                else if (Session["RoleId"].ToString() == "88")
                {
                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                    string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead='Y' and MemberID=" + Session["LoginID"].ToString();
                    int mcount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql));
                    if (mcount > 0)
                    {
                        GetDropDownChoice(dllfileChoice, true);
                        dllfileChoice.Visible = true;
                        lblNoPermission.Visible = false;
                        hdntlead.Value = "Y";
                    }
                    else
                    {
                        GetDropDownChoice(dllfileChoice, true);
                        dllfileChoice.Visible = true;
                        dllfileChoice.Items.RemoveAt(1);
                        dllfileChoice.Items.RemoveAt(2);
                        dllfileChoice.Items.RemoveAt(2);
                        lblNoPermission.Visible = false;
                        hdntlead.Value = "N";
                        dllfileChoice.SelectedValue = "DownloadPapers";
                        dllfileChoice.Visible = false;
                        LoadSearchAndDownloadPanels(true);
                        Tab1.Visible = false;
                        Tab3.Visible = false;
                        Tab4.Visible = false;
                        Tab5.Visible = false;
                        Tab6.Visible = true;
                        Tab2.CssClass = "Clicked";
                        //Simson
                    }
                }
                else
                {
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
                }
            }
        }
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPGId.Value = ddlProductGroup.SelectedValue;
        GetProductCodes(PGId(ddlProductGroup), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup), ddlContestYear);
        }
        else
        {
            PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
        }
        /****Commented on Oct 10 2014 as Level is included for Product UV *******************/
        if (PGCode(ddlProductGroup) == "UV")
        {
            ddlDocType.SelectedIndex = 1;
            ddlSections.SelectedItem.Text = "0";
            ddlSections.Enabled = false;
        }
        else
        {
            ddlDocType.SelectedIndex = 0;
            PopulateSection(ddlSections, true);
            ddlSections.Enabled = true;
        }

        if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                GetCoachPapers("Modify");
            }
        }
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblstatus.Text = "";
        pnlReplicate.Visible = false;
        //  CurrentOptIndex = Convert.ToInt32(dllfileChoice.SelectedValue);
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            LoadSearchAndDownloadPanels(true);
        }
        else if (hdnpaperType.Value == ScreenChoice.UploadPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                Label8.Text = "Upload Papers";

                LoadUploadPanel();


                //GetContextYear(ddlContestYear, true); commented and moved above on Oct 28 2014.

                //ddlContestYear.SelectedIndex = 1;

                if (ddlFlrYear.Items.Count > 0 && ddlFlrYear.SelectedValue != "0")
                {
                    ddlContestYear.SelectedValue = ddlFlrYear.SelectedValue;
                }
                if (ddlFlrSemester.Items.Count > 0 && ddlFlrSemester.SelectedValue != "0")
                {
                    ddlSemester.SelectedValue = ddlFlrSemester.SelectedValue;
                    GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedValue, ddlProduct);
                }
                if (ddlFlrProductGroup.Items.Count > 0 && ddlFlrProductGroup.SelectedValue != "-1")
                {
                    ddlProductGroup.SelectedValue = ddlFlrProductGroup.SelectedValue;
                    GetProductCodes(PGId(ddlProductGroup), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
                }
                if (ddlFlrProduct.Items.Count > 0 && ddlFlrProduct.SelectedValue != "-1")
                {
                    ddlProduct.SelectedValue = ddlFlrProduct.SelectedValue;
                    PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
                }
                if (ddlFlrLevel.Items.Count > 0 && ddlFlrLevel.SelectedValue != "-1")
                {
                    ddlLevel.SelectedValue = ddlFlrLevel.SelectedValue;
                    PopulateContestants(ddlNoOfContestants, true);
                }
                if (ddlFlrNoOfContestants.Items.Count > 0 && ddlFlrNoOfContestants.SelectedValue != "-1")
                {
                    ddlNoOfContestants.SelectedValue = ddlFlrNoOfContestants.SelectedValue;
                }
                if (DDlDDocType.Items.Count > 0 && DDlDDocType.SelectedValue != "0")
                {
                    ddlDocType.SelectedValue = DDlDDocType.SelectedValue;
                    GetWeek(ddlWeek, true);
                }
                if (ddlFlrWeek.Items.Count > 0 && ddlFlrWeek.SelectedValue != "-1")
                {
                    ddlWeek.SelectedValue = ddlFlrWeek.SelectedValue;
                    PopulateSets(ddlSet, true);
                }
                if (ddlFlrSet.Items.Count > 0 && ddlFlrSet.SelectedValue != "-1")
                {
                    ddlSet.SelectedValue = ddlFlrSet.SelectedValue;
                }
            }
        }
        else if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                Label8.Text = "Modify Papers";
                LoadModifyPanel();
            }

        }
        else if (hdnpaperType.Value == ScreenChoice.ReplicatePapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "96"))
                {
                    ddlRepToWeekNo.Enabled = true;
                    ddlRepToSetNo.Enabled = true;
                }
                else
                {
                    ddlRepToWeekNo.Enabled = false;
                    ddlRepToSetNo.Enabled = false;
                }
                Label8.Text = "Replicate Papers";
                pnlReplicate.Visible = true;
                LoadReplicatePanel();
                ResetReplicatePapers(false);
            }

        }
    }

    protected void UpdateData()
    {
        int value = 0;
        if (ddlLevel.Enabled != true)
        {
            // RequiredFieldValidator11.Enabled = false;
        }
        else
        {
            //   RequiredFieldValidator11.Enabled = true;
            if (ddlLevel.SelectedIndex == 0 && ddlLevel.SelectedValue == "-1")
            {
                //  RequiredFieldValidator11.IsValid = false;
            }
        }

        if (!ddlSections.Enabled)
        {
            // RequiredFieldValidator10.Enabled = false;
        }
        else
        {
            if (ddlSections.SelectedIndex == 0 & PGCode(ddlProductGroup) != "UV")
            {
                //RequiredFieldValidator10.Enabled = true;
                //RequiredFieldValidator10.Visible = true;
                //RequiredFieldValidator10.IsValid = false;
            }

        }
        if (this.IsValid)
        {

            if (FileUpLoad1.HasFile | ULFile.Value != "")
            {

                EntityTestPaper objETP = new EntityTestPaper();
                objETP.ProductId = PGId(ddlProduct);
                objETP.ProductCode = PGCode(ddlProduct);
                objETP.ProductGroupCode = PGCode(ddlProductGroup);
                objETP.ProductGroupId = PGId(ddlProductGroup);
                objETP.EventCode = ddlEvent.SelectedItem.Text;
                objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
                objETP.SetNum = int.Parse(ddlWeek.SelectedItem.Value);
                objETP.Sections = int.Parse(ddlSections.SelectedItem.Text);
                objETP.PaperType = ddlNoOfContestants.SelectedItem.Value.ToString();
                objETP.ContestYear = ddlContestYear.SelectedItem.Value;
                objETP.DocType = ddlDocType.SelectedItem.Value.ToString();
                objETP.Description = txtDescription.Text;
                objETP.Password = TxtPassword.Text;
                objETP.CreateDate = System.DateTime.Now;
                objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
                objETP.Event = ddlEvent.SelectedItem.Text;
                objETP.Semester = ddlSemester.SelectedValue;

                if (ddlLevel.SelectedItem.Value.ToString() != "-1")
                {
                    objETP.Level = ddlLevel.SelectedItem.Text;
                }
                else
                {
                    objETP.Level = "NA";
                }

                if (ckFname.Checked == true)
                {
                    objETP.TestFileName = ModifiedFileName(objETP, false);
                }
                else
                {
                    if (SectionFlag == 1 | SectionFlag == 2)
                    {
                        objETP.TestFileName = ULFile.Value;
                    }
                    else
                    {
                        objETP.TestFileName = FileUpLoad1.FileName;
                    }
                }

                if (ValidateFileName(objETP))
                {
                    try
                    {
                        value = TestPapers_Insert(objETP);
                        if (value != 0)
                        {
                            // Simson Add release Dates

                            try
                            {
                                SetReleaseDates();
                            }
                            catch
                            {

                            }

                            if (ckFname.Checked == true)
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                    File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value));
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                }
                                else
                                {
                                    SaveFileNewName(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], upFile);
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                }
                            }
                            else
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                    File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded: " + ULFile.Value.Replace("cpy_", "").ToString();

                                }
                                else
                                {
                                    SaveFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], FileUpLoad1);
                                    lblMessage.ForeColor = System.Drawing.Color.Blue;
                                    lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                                }
                            }
                        }
                        else
                        {

                            string cpID = Convert.ToString(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT CoachPaperId From CoachPapers where ProductGroupId=" + objETP.ProductGroupId + " and ProductId=" + objETP.ProductId + " and EventId=13 and EventYear=" + ddlContestYear.SelectedItem.Value + "and EventCode='Coaching' and PaperType='" + objETP.PaperType + "' and DocType='" + objETP.DocType + "' and WeekId =" + objETP.WeekId + " and SetNum=" + objETP.SetNum + " and Semester='" + objETP.Semester + "' and [Sections]=" + objETP.Sections + " and (Level='" + objETP.Level + "' OR Level is null)"));
                            hdnCPID.Value = cpID;
                            Session["CoachPaperID"] = cpID;
                            DataSet dsDuplicate = (SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT * From CoachPapers where ProductGroupId=" + objETP.ProductGroupId + " and ProductId=" + objETP.ProductId + " and EventId=13 and EventYear=" + ddlContestYear.SelectedItem.Value + "and EventCode='Coaching' and PaperType='" + objETP.PaperType + "' and DocType='" + objETP.DocType + "' and WeekId =" + objETP.WeekId + " and SetNum=" + objETP.SetNum + " and Semester='" + objETP.Semester + "' and [Sections]=" + objETP.Sections + " and Level='" + objETP.Level + "'"));
                            lbluiyear.Text = ddlContestYear.Text;
                            lblPtype.Text = objETP.PaperType;
                            lbldoctype.Text = objETP.DocType;
                            lblpgroup.Text = objETP.ProductGroupCode;
                            lblProduct.Text = objETP.ProductCode;
                            lbllevel.Text = objETP.Level;
                            lblsec.Text = objETP.Sections.ToString();
                            lblweek.Text = objETP.WeekId.ToString();
                            lbluiset.Text = objETP.SetNum.ToString();
                            lbluifname.Text = objETP.TestFileName;
                            if (hdnuploaded.Value == "Y") { gvModify.Visible = false; }
                            string Uinput = ddlContestYear.Text + "&nbsp;&nbsp;" + objETP.PaperType + "&nbsp;&nbsp;" + objETP.DocType + "&nbsp;&nbsp;" + objETP.ProductGroupCode + "&nbsp;&nbsp;" + objETP.ProductCode + "&nbsp;&nbsp;" + objETP.Level + "&nbsp;&nbsp;" + objETP.Sections + "&nbsp;&nbsp;" + objETP.SetNum + "&nbsp;&nbsp;" + objETP.WeekId + "&nbsp;&nbsp;" + objETP.TestFileName;
                            if (dsDuplicate.Tables[0].Rows.Count > 0)
                            {
                                gvDuplicate.DataSource = dsDuplicate;
                                gvDuplicate.DataBind();
                            }
                            if (ckFname.Checked == true)
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + upFile) | cpID != string.Empty)
                                    {
                                        //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                        Panel1.Visible = false;
                                        PanelAdd.Visible = false;
                                        Panel5.Visible = true;
                                        Panel6.Visible = true;
                                        dllfileChoice.Enabled = false;
                                    }
                                    else
                                    {
                                        File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), upFile), true);
                                        File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                                        lblMessage.ForeColor = System.Drawing.Color.Blue;
                                        lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                    }
                                }
                                else
                                {
                                    if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + upFile) | cpID != string.Empty)
                                    {
                                        FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "Temp_" + upFile));
                                        hdnTempFileName.Value = "Temp_" + upFile;
                                        Panel1.Visible = false;
                                        PanelAdd.Visible = false;
                                        Panel5.Visible = true;
                                        Panel6.Visible = true;
                                        dllfileChoice.Enabled = false;
                                    }
                                    else
                                    {

                                        SaveFileNewName(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], upFile);
                                        lblMessage.ForeColor = System.Drawing.Color.Blue;
                                        lblMessage.Text = "File Uploaded. " + "<br>" + "Modified File Name: " + upFile;
                                    }
                                }
                            }
                            else
                            {
                                if (SectionFlag == 1 | SectionFlag == 2)
                                {
                                    Panel1.Visible = false;
                                    PanelAdd.Visible = false;
                                    Panel5.Visible = true;
                                    Panel6.Visible = true;
                                    dllfileChoice.Enabled = false;
                                }
                                else
                                {
                                    FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "Temp_" + FileUpLoad1.FileName.ToString()));
                                    hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                                    Panel1.Visible = false;
                                    PanelAdd.Visible = false;
                                    Panel5.Visible = true;
                                    Panel6.Visible = true;
                                    dllfileChoice.Enabled = false;
                                }
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        lblMessage.Text = err.Message;
                    }
                }
            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "No File Uploaded.";
            }
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "Validation Error.";
        }
    }
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        if (ValidateUploadCoachPapers() == 1)
        {
            lblstatus.Text = "";
            if (ddlLevel.Enabled != true)
            {
                // RequiredFieldValidator11.Enabled = false;

            }
            else
            {
                //  RequiredFieldValidator11.Enabled = true;
                if (ddlLevel.SelectedIndex == 0 && ddlLevel.SelectedValue == "-1")
                {
                    // RequiredFieldValidator11.IsValid = false;
                }

            }
            if (!ddlSections.Enabled)
            {
                // RequiredFieldValidator10.Enabled = false;
            }
            else
            {
                if (ddlSections.SelectedItem.Text == "0" & PGCode(ddlProductGroup) != "UV")
                {
                    //RequiredFieldValidator10.Enabled = true;
                    //RequiredFieldValidator10.Visible = true;
                    //RequiredFieldValidator10.IsValid = false;
                }
            }
            if (this.IsValid)
            {
                lblMessage.Text = "";
                SectionFlag = 0;
                hdnsection.Value = "0";
                Boolean Q_Flag = true;
                //Commented on Nov03_2015 by Bindhu - to allow all uploads
                if (ddlDocType.SelectedValue != "Q")
                {
                    string pcode = PGCode(ddlProductGroup);
                    if (pcode != "UV" & pcode != "SC" & pcode != "GB")
                    {
                        string strCmd = " Select Count(*) from CoachPapers where EventYear = " + ddlContestYear.SelectedItem.Value + " and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductID =" + PGId(ddlProduct) + " and Level='" + ddlLevel.SelectedItem.Text + "' and WeekID= " + ddlWeek.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' and SetNum =" + ddlSet.SelectedValue + " and PaperType ='" + ddlNoOfContestants.SelectedValue + "' and DocType ='Q'";
                        if (ddlLevel.SelectedItem.Text == "-1")
                        {
                            strCmd = " select Count(*) from CoachPapers where EventYear = " + ddlContestYear.SelectedItem.Value + " and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductID =" + PGId(ddlProduct) + " and Level is null and WeekID= " + ddlWeek.SelectedValue + " and SetNum =" + ddlSet.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' and PaperType ='" + ddlNoOfContestants.SelectedValue + "' and DocType ='Q'";
                        }
                        if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strCmd)) > 0)
                            Q_Flag = true;
                        else
                            Q_Flag = false;
                    }
                }

                if (Q_Flag == true)
                {
                    lblMessage.Text = "";
                    if (ddlSections.SelectedItem.Text == "0" & PGCode(ddlProductGroup) != "UV")
                    {
                        FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + FileUpLoad1.FileName));
                        ULFile.Value = FileUpLoad1.PostedFile.FileName;
                        PanelSections.Visible = true;
                        Panel1.Visible = false;
                        PanelAdd.Visible = false;
                        dllfileChoice.Enabled = false;
                    }
                    else
                    {
                        UpdateData();
                    }
                }
                else
                {
                    lblMessage.Text = "Q file needs to the uploaded first. If there is no home work, then use a blank page containing a message, 'There is no home work this week'.";
                    return;
                }
            }
        }
    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        //if (hdnTempFileName.Value.Length > 0)
        //{
        if (hdnsection.Value == "1")
        {
            if (ckFname.Checked == true)
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnupfile.Value.ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + hdnupfile.Value.ToString();
            }
            else
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), ULFile.Value.Replace("cpy_", "").ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + ULFile.Value.Replace("cpy_", "").ToString();
            }
        }
        else if (hdnTempFileName.Value.Length > 0)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update coachpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate(),Sections=" + ddlSections.SelectedItem.Value.ToString() + ",Description='" + txtDescription.Text + "' ,Password='" + TxtPassword.Text + "',testFileName ='" + hdnupfile.Value.ToString() + "' where  CoachPaperId=" + Session["CoachPaperID"].ToString());
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
                //File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
                //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), true);
                File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()), true);
                File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.ToString()));
                Panel6.Visible = false;
                Panel5.Visible = false;
                Panel1.Visible = true;
                PanelAdd.Visible = true;
                if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
                UploadedData();
                lblMessage.ForeColor = System.Drawing.Color.Blue;
                lblMessage.Text = "File Replaced : " + "<br>" + hdnTempFileName.Value.Replace("Temp_", "").ToString();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString()); 
            }

        }

        else
        {
            Panel5.Visible = false;
            PanelAdd.Visible = false;
            Panel1.Visible = true;
        }

    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + "cpy_" + ULFile.Value.ToString()))
        {
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));

        }
        //if (hdnTempFileName.Value.Length > 0)
        //   // File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value));
        if (hdnTempFileName.Value.Length > 0)
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), hdnTempFileName.Value.ToString()));
        Panel6.Visible = false;
        Panel5.Visible = false;
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        if (hdnuploaded.Value == "Y") { gvModify.Visible = true; }
    }

    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                int coachPaperId = (int)this.gvTestPapers.DataKeys[index]["CoachPaperId"];
                objETP.TestPaperId = coachPaperId;
                objETP.TestFileName = gvTestPapers.Rows[index].Cells[15].Text;

                if (e.CommandName == "Download")
                {
                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], objETP);
                }
            }
        }
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {

    }

    private int PGId(DropDownList ddlObject)
    {
        int index;
        int PGI = -1;
        index = ddlObject.SelectedValue.ToString().IndexOf("-");
        if (index > 0)
        {
            PGI = Int32.Parse(ddlObject.SelectedValue.ToString().Trim().Substring(0, index));
        }
        return PGI;
    }

    private string PGCode(DropDownList ddlObject)
    {
        int index = -1;
        string PGC = string.Empty;
        index = ddlObject.SelectedValue.ToString().Trim().LastIndexOf("-");
        if (index > 0)
        {
            index++;
            PGC = ddlObject.SelectedValue.ToString().Substring(index);
        }
        return PGC;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string RoleID = Session["RoleId"].ToString();
        if (RoleID != "88")
        {
            LoadDownloadCoachpapers();
        }
        else
        {
            string CmdValidation = "select distinct EndDate from CoachingDateCal where EventYear=" + ddlFlrYear.SelectedValue + " and Semester='" + ddlFlrSemester.SelectedValue + "' and ProductId=" + ddlFlrProduct.SelectedValue.ToString().Split('-')[0] + " and ScheduleType='Term'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdValidation);
            string EndDate = string.Empty;
            string TodayDate = Convert.ToDateTime(DateTime.Now.ToString()).ToShortDateString();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["EndDate"] != null)
                {
                    EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToShortDateString();
                }
            }
            if (EndDate != "")
            {
                DateTime dtTodayDate = Convert.ToDateTime(TodayDate);
                DateTime dtEndDate = Convert.ToDateTime(EndDate);
                if (dtEndDate >= dtTodayDate)
                {
                    LoadDownloadCoachpapers();
                }
            }
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            LblexamRecErr.Text = "";
            ddlFlrProduct.SelectedIndex = 0;
            ddlFlrProductGroup.SelectedIndex = 0;
            ddlFlrWeek.SelectedIndex = 0;
            ddlFlrSet.SelectedIndex = 0;
            ddlFlrNoOfContestants.SelectedIndex = 0;
            ddlFlrEvent.SelectedIndex = 0;
            ddlFlrLevel.SelectedIndex = 0;
            m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
            GetCoachPapers("Search");

        }
        catch (Exception ex)
        {
        }
    }

    protected void GetCoachPapers(string input)
    {
        string strOrderBy = " EventYear,WeekId,SetNum,DocType, ProductCode,Level,Sections ";
        DataSet ds;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || (Session["RoleId"].ToString() == "96") || Session["RoleId"].ToString() == "30")
        {
            string whrCondition = "";
            if (ddlContestYear.SelectedValue != "-1")
            {
                whrCondition = " Where EventYear=" + ddlContestYear.SelectedValue;
            }

            //if (ddlProductGroup.SelectedValue != "-1")
            //{
            //    whrCondition = whrCondition + " and ProductGroupId=" + PGId(ddlProductGroup).ToString();
            //}
            //if (ddlProduct.SelectedValue != "-1")
            //{
            //    whrCondition = whrCondition + " and ProductId =" + PGId(ddlProduct).ToString();
            //}
            //if (ddlLevel.SelectedValue != "-1" && ddlLevel.Items.Count > 0)
            //{
            //    whrCondition = whrCondition + " and Level ='" + ddlLevel.SelectedItem.Value + "'";
            //}
            //whrCondition = whrCondition + " and Semester ='" + ddlSemester.SelectedItem.Value + "'";




            if (ddlProductGroupFilter.SelectedValue != "-1" && ddlProductGroupFilter.SelectedValue != "Select")
            {
                whrCondition += "  and ProductGroupId=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if (ddlProductFilter.SelectedValue != "-1" && ddlProductFilter.SelectedValue != "Select")
            {
                whrCondition += "  and Productid=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlLevelFilter.SelectedValue != "-1" && ddlLevelFilter.SelectedValue != "Select")
            {
                whrCondition += "  and Level='" + ddlLevelFilter.SelectedValue + "'";
            }
            if (ddlDocTypeFilter.SelectedValue != "-1" && ddlDocTypeFilter.SelectedValue != "Select")
            {
                whrCondition += "  and DocType='" + ddlDocTypeFilter.SelectedValue + "'";
            }
            if (ddlPaperTypeFilter.SelectedValue != "-1")
            {
                whrCondition += "  and paperType='" + ddlPaperTypeFilter.SelectedValue + "'";
            }
            if (ddlWeekFilter.SelectedValue != "-1" && ddlWeekFilter.SelectedValue != "")
            {
                whrCondition += "  and WeekId=" + ddlWeekFilter.SelectedValue + "";
            }

            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from CoachPapers " + whrCondition + " ORDER BY " + strOrderBy); // CoachPaperId");// where ProductGroupId=" + m_objETP.ProductGroupId + " and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + m_objETP.ProductGroupId + " and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ")");
        }
        else if (Session["RoleId"].ToString() == "89")
        {
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from CoachPapers where ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and Semester='" + ddlSemester.SelectedValue + "' and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + PGId(ddlProductGroup).ToString() + "  and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY " + strOrderBy); //CoachPaperId"); //ddlFlrProductGroup
        }
        else
        {
            string sqlstring1 = string.Empty;
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value == "Y")
            {
                sqlstring1 = "select * from CoachPapers where ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and Semester='" + ddlSemester.SelectedValue + "' and ProductCode in (select distinct V.ProductCode from Volunteer V where V.ProductGroupId=" + PGId(ddlProductGroup).ToString() + " and V.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY " + strOrderBy; //CoachPaperId";//ddlFlrProductGroup
            }
            else
            {
                sqlstring1 = "select * from CoachPapers where ProductGroupId=" + PGId(ddlFlrProductGroup).ToString() + " and Semester='" + ddlSemester.SelectedValue + "' and ProductCode in (select distinct C.ProductCode from CalSignUp C where C.ProductGroupId=" + PGId(ddlFlrProductGroup).ToString() + " and C.MemberId = " + Int32.Parse(Session["LoginID"].ToString()) + ") and [Level]  in (select distinct dbo.[CalSignUp].[Level] from dbo.[CalSignUp] where dbo.[CalSignUp].[Level] = dbo.[CalSignUp].[Level] and dbo.[CalSignUp].MemberID =" + Int32.Parse(Session["LoginID"].ToString()) + ") ORDER BY " + strOrderBy; // CoachPaperId";
            }
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstring1);
        }
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
        {
            lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
            lblSearchErr.Visible = true;
        }
        else
        {
            lblSearchErr.Text = string.Empty;
        }
        DataView dv = new DataView(dt);
        if (input == "Search")
        {
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
        }
        else
        {
            gvModify.DataSource = dv;
            gvModify.DataBind();
            // lblSearchErr.Text = "";
        }
    }
    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear.SelectedValue, ddlFlrSemester.SelectedValue);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
        }
        else
        {
            PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true, ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, ddlFlrProductGroup.SelectedValue, ddlFlrProduct.SelectedValue);
        }
        ddlFlrProduct_SelectedIndexChanged(ddlProduct, e);
    }

    #endregion

    #region " Private Methods - Data Access Layer "
    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_Insert";
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@EventCode", objETP.EventCode);
        param[5] = new SqlParameter("@WeekId", objETP.WeekId);
        param[6] = new SqlParameter("@SetNum", objETP.WeekId);
        param[7] = new SqlParameter("@PaperType", objETP.PaperType); //Modified 22/07/2013
        param[8] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[9] = new SqlParameter("@Description", objETP.Description);
        param[10] = new SqlParameter("@Password", objETP.Password);
        param[11] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[12] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[13] = new SqlParameter("@ModifyDate", objETP.ModifyDate);
        param[14] = new SqlParameter("@ModifiedBy", objETP.ModifiedBy);
        param[15] = new SqlParameter("@DocType", objETP.DocType);
        param[16] = new SqlParameter("@EventYear", objETP.ContestYear);
        param[17] = new SqlParameter("@Sections", objETP.Sections);
        if (objETP.Level == "NA") { param[18] = new SqlParameter("@Level", DBNull.Value); } else { param[18] = new SqlParameter("@Level", objETP.Level); }
        param[19] = new SqlParameter("@Semester", objETP.Semester);
        value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
        hdnupfile.Value = objETP.TestFileName;
        if (value == null)
        {
            return -1;
            // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }

    private void GetProductGroupCodeByRoleId(DropDownList ddlObject, bool blnCreateEmptyItem, int EventYear, string Semester, DropDownList ddlProdObject)
    {
        try
        {
            string conn = Application["ConnectionString"].ToString();
            DataSet dsproductgroup;
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleId"].ToString()));
            param[1] = new SqlParameter("@MemberID", Int32.Parse(Session["LoginId"].ToString()));
            param[2] = new SqlParameter("@TLead", hdntlead.Value.ToString());
            param[3] = new SqlParameter("@EventYear", EventYear);
            param[4] = new SqlParameter("@Semester", Semester);

            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductGroupByRoleID", param);
            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "IDandCode";
            ddlObject.DataBind();
            if (dsproductgroup.Tables[0].Rows.Count == 1)
            {
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
                ddlObject.SelectedIndex = 0;
                if (PGCode(ddlProductGroup) == "UV")
                {
                    ddlDocType.SelectedIndex = 1;
                }
                else
                {
                    ddlDocType.SelectedIndex = 0;
                }

                GetProductCodes(PGId(ddlObject), ddlProdObject, true, EventYear.ToString(), Semester);

            }
            else if (dsproductgroup.Tables[0].Rows.Count < 1)
            {
                ddlObject.SelectedIndex = -1;
                GetProductCodes(PGId(ddlObject), ddlProdObject, true, EventYear.ToString(), Semester);
            }
            else
            {

                ddlObject.SelectedIndex = 0;
                ddlObject.Enabled = true;
                try
                {
                    if (hdnPGId.Value.IndexOf('-') > 0)
                    {
                        ddlObject.SelectedValue = hdnPGId.Value;
                    }
                    else
                    {
                        try
                        {

                            string PgId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupId from ProductGroup where ProductGroupId=" + hdnPGId.Value + "").ToString();
                            string pgCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupcode from ProductGroup where ProductGroupId=" + hdnPGId.Value + "").ToString();


                            string selValue = PgId + "-" + pgCode;
                            ddlObject.SelectedValue = selValue;
                        }
                        catch
                        {
                        }
                    }
                    GetProductCodes(PGId(ddlObject), ddlProdObject, true, EventYear.ToString(), Semester);
                }
                catch
                {
                }
            }


            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select]", "-1"));
            }

        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }

    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsproductgroup;
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88"))
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.[ProductGroupId], V.[ProductGroupCode] AS EventCodeAndProductGroupCode FROM dbo.[Volunteer] V where V.[RoleId]=" + Int32.Parse(Session["RoleId"].ToString()) + " and V.[MemberId]=" + Int32.Parse(Session["LoginID"].ToString()));
                if (dsproductgroup.Tables[0].Rows.Count < 2)
                {
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;
                }
            }
            else
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(13) Order by A.EventCode");

            }

            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void FillReplicateDetails()
    {
        // Fill Replicate Year for From and To
        ddlRepFrmYear.Items.Clear();
        ddlRepToYear.Items.Clear();
        int Minyear, Maxyear;
        try
        {
            Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(Eventyear) from CoachPapers"));
            Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(Eventyear) from CoachPapers"));

            int year = DateTime.Now.Year;
            if (Maxyear > year)
                year = Maxyear;
            int j = 0;
            for (int i = year; i >= Minyear; i--)
            {
                // ddlRepFrmYear.Items.Insert(j, new ListItem(i.ToString()));
                ddlRepFrmYear.Items.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString().Substring(2, 2), i.ToString()));
                j = j + 1;
            }
            ddlRepFrmYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            loadPhase(ddlRepFrmSemester, ddlRepFrmYear.SelectedValue);

            GetProductGroupCodeByRoleId(ddlRepFrmPrdGroup, true, int.Parse(ddlRepFrmYear.SelectedValue), ddlRepFrmSemester.SelectedValue, ddlRepFrmProduct);
            GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepFrmProduct, true, ddlRepFrmYear.SelectedValue, ddlRepFrmSemester.SelectedValue);
            GetWeek(ddlRepFrmWeekNo, true);
            PopulateContestants(ddlRepFrmPaperType, true);
            PopulateEvents(ddlRepFrmEvent, false);
            PopulateSets(ddlRepFrmSetNo, true);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlRepFrmLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
            }
            else
            {
                //PopulateLevel(ddlRepFrmLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlRepFrmLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
            }


            j = 0;
            for (int i = year + 1; i >= Minyear + 1; i--)
            {
                //ddlRepToYear.Items.Insert(j, new ListItem(i.ToString()));
                ddlRepToYear.Items.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString().Substring(2, 2), i.ToString()));
                j = j + 1;
            }
            ddlRepToYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            loadPhase(ddlRepToSemester, ddlRepToYear.SelectedValue);

            GetProductGroupCodeByRoleId(ddlRepToPrdGroup, true, int.Parse(ddlRepToYear.SelectedValue), ddlRepToSemester.SelectedValue, ddlRepToProduct);
            GetProductCodes(PGId(ddlRepToPrdGroup), ddlRepToProduct, true, ddlRepToYear.SelectedValue, ddlRepToSemester.SelectedValue);
            GetWeek(ddlRepToWeekNo, true);
            PopulateContestants(ddlRepToPaperType, true);
            PopulateEvents(ddlRepToEvent, false);
            PopulateSets(ddlRepToSetNo, true);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlRepToLevel, true, PGId(ddlRepToPrdGroup), ddlRepFrmYear);
            }
            else
            {
                //PopulateLevel(ddlRepToLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlRepToLevel, ddlRepToPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepToEvent.SelectedValue, ddlRepToPrdGroup.SelectedValue, ddlRepToProduct.SelectedValue);
            }
            ddlRepToLevel.Enabled = false;

        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }


    }


    private void GetFlrContextYear()
    {
        ddlFlrYear.Items.Clear();
        int Minyear, Maxyear;
        try
        {
            Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from testpapers"));
            Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from testpapers"));

            int year = DateTime.Now.Year;
            int j = 0;
            for (int i = Minyear; i <= year; i++)
            {
                ddlFlrYear.Items.Add(new ListItem((i.ToString() + "-" + (i + 1).ToString().Substring(2, 2)), i.ToString()));
                // ddlFlrYear.Items.Insert(j, new ListItem(i.ToString()));
                j = j + 1;
            }
            //ddlFlrYear.SelectedIndex = ddlFlrYear.Items.IndexOf(ddlFlrYear.Items.FindByText(Maxyear.ToString()));
            ddlFlrYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(EventYear) from EventFees where Eventid=13").ToString();
        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }


    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {
        ddlContestYear.Items.Clear();
        try
        {

            ddlContestYear.Items.Add(new ListItem((DateTime.Now.AddYears(-1).Year.ToString() + "-" + DateTime.Now.Year.ToString().Substring(2, 2)), DateTime.Now.AddYears(-1).Year.ToString()));

            ddlContestYear.Items.Add(new ListItem((DateTime.Now.Year.ToString() + "-" + DateTime.Now.AddYears(1).Year.ToString().Substring(2, 2)), DateTime.Now.Year.ToString()));

            ddlContestYear.Items.Add(new ListItem((DateTime.Now.AddYears(1).Year.ToString() + "-" + DateTime.Now.AddYears(2).Year.ToString().Substring(2, 2)), DateTime.Now.AddYears(1).Year.ToString()));

            //ddlContestYear.Items.Insert(0, new ListItem(DateTime.Now.AddYears(-1).Year.ToString()));
            //ddlContestYear.Items.Insert(1, new ListItem(DateTime.Now.Year.ToString()));
            //ddlContestYear.Items.Insert(2, new ListItem(DateTime.Now.AddYears(1).Year.ToString()));

            //if (DateTime.Now.Month <= 3)
            //    ddlContestYear.SelectedIndex = 0;
            //else
            //    ddlContestYear.SelectedIndex = 1;

            ddlContestYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(EventYear) from EventFees where Eventid=13").ToString();
            hdnYear.Value = ddlContestYear.SelectedValue;
        }

        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;

        }
        //ddlContestYear.Items.Clear();
        //int Minyear,Maxyear;
        //try
        //{
        //    Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from testpapers"));
        //    Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from testpapers"));

        //    int year = DateTime.Now.Year;
        //    int j = 0;
        //    for (int i = Minyear; i <= year; i++)
        //    {
        //        ddlContestYear.Items.Insert(j, new ListItem(i.ToString()));
        //        j = j + 1;
        //    }
        //    ddlContestYear.SelectedIndex = ddlContestYear.Items.IndexOf(ddlContestYear.Items.FindByText(Maxyear.ToString()));
        //}
        //catch (SqlException se)
        //{
        //    lblMessage.ForeColor = System.Drawing.Color.Red;
        //    lblMessage.Text = se.Message.ToString();
        //    return;
        //}

    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem, string Year, string Semester)
    {
        DataSet dsproduct;
        try
        {
            ddlObject.Items.Clear();

            if (ProductGroupId != -1)
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleID"].ToString()));
                param[1] = new SqlParameter("@MemberId", Int32.Parse(Session["LoginID"].ToString()));
                param[2] = new SqlParameter("@ProductGroupId", ProductGroupId);
                param[3] = new SqlParameter("@EventYear", Int32.Parse(Year));//DateTime.Now.Year.ToString()));
                param[4] = new SqlParameter("@TLead", hdntlead.Value.ToString());
                param[5] = new SqlParameter("@Semester", Semester);

                //Response.Write(Session["RoleID"].ToString() + "--" + Session["LoginID"].ToString() + "Product group" + ProductGroupId + "TeamLead" + hdntlead.Value.ToString());
                dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductCodeByRoleID", param);
                ddlObject.SelectedIndex = -1;
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "IDandCode";
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select]", "-1"));
                if (dsproduct.Tables[0].Rows.Count < 2)
                {
                    try
                    {
                        ddlObject.SelectedIndex = 1;
                    }
                    catch
                    {
                    }
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;

                    //Sims
                    DropDownList ddlObjLevel = ddlLevel;
                    DropDownList ddlObjyear = ddlContestYear;
                    DropDownList ddlObjProdGroup = ddlProductGroup;
                    if (ddlObject == ddlProduct)
                    {
                        ddlObjLevel = ddlLevel;
                        ddlObjyear = ddlContestYear;
                        ddlObjProdGroup = ddlProductGroup;
                    }
                    else if (ddlObject == ddlFlrProduct)
                    {
                        ddlObjLevel = ddlFlrLevel;
                        ddlObjyear = ddlFlrYear;
                        ddlObjProdGroup = ddlFlrProductGroup;
                    }
                    else if (ddlObject == ddlRepFrmProduct)
                    {
                        ddlObjLevel = ddlRepFrmLevel;
                        ddlObjyear = ddlRepFrmYear;
                        ddlObjProdGroup = ddlRepFrmPrdGroup;
                    }
                    else if (ddlObject == ddlRepToProduct)
                    {
                        ddlObjLevel = ddlRepToLevel;
                        ddlObjyear = ddlRepToYear;
                        ddlObjProdGroup = ddlRepToPrdGroup;
                    }
                    if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
                    {
                        PopulateLevelNew(ddlObjLevel, true, ProductGroupId, ddlObjyear);
                    }
                    else
                    {
                        //PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
                        PopulateLevel(ddlObjLevel, ddlObjProdGroup.SelectedItem.Text, true, ddlObjyear.SelectedValue, "13", ddlObjProdGroup.SelectedValue, ddlObject.SelectedValue);
                    }
                }
                else
                {
                    ddlObject.SelectedIndex = 0;
                    ddlObject.Enabled = true;
                    try
                    {
                        if (hdnPGId.Value.IndexOf('-') > 0)
                        {
                            ddlObject.SelectedValue = hdnPId.Value;
                        }
                        else
                        {
                            string PId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductId from Product where ProductId=" + hdnPId.Value + "").ToString();
                            string PCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductId from Product where ProductId=" + hdnPId.Value + "").ToString();


                            string selValue = PId + "-" + PCode;
                            ddlObject.SelectedValue = selValue;
                        }

                        DropDownList ddlObjLevel = ddlLevel;
                        DropDownList ddlObjyear = ddlContestYear;
                        DropDownList ddlObjProdGroup = ddlProductGroup;
                        if (ddlObject == ddlProduct)
                        {
                            ddlObjLevel = ddlLevel;
                            ddlObjyear = ddlContestYear;
                            ddlObjProdGroup = ddlProductGroup;
                        }
                        else if (ddlObject == ddlFlrProduct)
                        {
                            ddlObjLevel = ddlFlrLevel;
                            ddlObjyear = ddlFlrYear;
                            ddlObjProdGroup = ddlFlrProductGroup;
                        }
                        else if (ddlObject == ddlRepFrmProduct)
                        {
                            ddlObjLevel = ddlRepFrmLevel;
                            ddlObjyear = ddlRepFrmYear;
                            ddlObjProdGroup = ddlRepFrmPrdGroup;
                        }
                        else if (ddlObject == ddlRepToProduct)
                        {
                            ddlObjLevel = ddlRepToLevel;
                            ddlObjyear = ddlRepToYear;
                            ddlObjProdGroup = ddlRepToPrdGroup;
                        }
                        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
                        {
                            PopulateLevelNew(ddlObjLevel, true, ProductGroupId, ddlObjyear);
                        }
                        else
                        {
                            //PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
                            PopulateLevel(ddlObjLevel, ddlObjProdGroup.SelectedItem.Text, true, ddlObjyear.SelectedValue, "13", ddlObjProdGroup.SelectedValue, ddlObject.SelectedValue);
                        }

                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                // ddlObject.Items.Insert(0, new ListItem("[Select]", "-1"));
                //ddlObject.SelectedIndex = 0;
            }
        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }
    }

    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }
    }

    private void GetWeek(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Week#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
        try
        {
            ddlObject.SelectedIndex = Convert.ToInt32(hdnWeekNo.Value);
        }
        catch
        {
        }
    }

    private void PopulateEvents(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Coaching", "13"));

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Event]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Homework", "HW"));
        ddlObject.Items.Add(new ListItem("PreTest", "PT"));
        ddlObject.Items.Add(new ListItem("RegTest", "RT"));
        ddlObject.Items.Add(new ListItem("FinalTest", "FT"));
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Paper Type]", "-1"));
        }
        ddlObject.SelectedIndex = 1;
    }
    private void PopulatePapertype(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Homework", "HW"));
        ddlObject.Items.Add(new ListItem("PreTest", "PT"));
        ddlObject.Items.Add(new ListItem("RegTest", "RT"));
        ddlObject.Items.Add(new ListItem("FinalTest", "FT"));
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("Select", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("Select", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void PopulateSection(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        List<int> list = new List<int>();


        int[] Nos = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        int producgrouptId = PGId(ddlProductGroup);
        if (producgrouptId == 42)
        {
            Nos = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        }
        else
        {
            Nos = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


        }
        //Nos = list.ToArray();
        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Section#]", "-1"));
        }
        if (PGCode(ddlProductGroup) == "UV")
        {
            ddlObject.SelectedItem.Text = "0";
            ddlObject.Enabled = false;
        }
        else
        {
            ddlObject.Enabled = true;
            ddlObject.SelectedIndex = 0;
        }

        ddlObject.SelectedValue = "1";
    }

    private void PopulateLevelNew(DropDownList ddlObject, bool blnCreateEmptyItem, int ProductGroupID, DropDownList ddlYear)
    {
        try
        {
            ddlObject.Enabled = true;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT DISTINCT Level FROM CalSignUp WHERE Accepted='Y' and  MemberID =" + Session["LoginId"].ToString() + " AND EventYear =" + Int32.Parse(ddlYear.SelectedValue) + " and ProductGroupId=" + ProductGroupID); //DateTime.Now.Year.ToString()
                                                                                                                                                                                                                                                                                                                                //"SELECT DISTINCT Level FROM CalSignUp WHERE Level is not null and MemberID =" + Session["LoginId"].ToString() + "and Accepted='Y'"); //" and ProductGroupID in (" + ProductGroupID + ") 
                                                                                                                                                                                                                                                                                                                                // + " AND EventYear =" + Int32.Parse(DateTime.Now.Year.ToString()));
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "Level";
            ddlObject.DataValueField = "Level";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
            if (ds.Tables[0].Rows.Count < 2)
            {
                try
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        ddlObject.SelectedValue = ds.Tables[0].Rows[0]["Level"].ToString();
                    }
                    //
                }
                catch
                {
                }

                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;

            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                blnCreateEmptyItem = true;
                ddlObject.Enabled = false;
            }

            if (blnCreateEmptyItem)
            {
                //    ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));

            }

            if (ds.Tables[0].Rows.Count > 1)
            {
                try
                {
                    ddlObject.SelectedValue = hdnLevel.Value;
                }
                catch
                {
                }
            }
        }
        catch
        {
        }
    }

    private void PopulateLevel(DropDownList ddlObject, string iCondition, bool blnCreateEmptyItem, string eventYear, string eventID, string ProductGroup, string ProductID)
    {


        try
        {
            int pgID = 0;
            int pId = 0;
            int index;
            int PGI = -1;
            index = ProductGroup.ToString().IndexOf("-");
            if (index > 0)
            {
                pgID = Int32.Parse(ProductGroup.Trim().Substring(0, index));
            }

            index = ProductID.ToString().IndexOf("-");
            if (index > 0)
            {
                pId = Int32.Parse(ProductID.Trim().Substring(0, index));
            }

            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select distinct ProdLevelID,LevelCode from ProdLevel P inner join CalSignup C on (C.Level=P.LevelCode) where P.EventYear=" + eventYear + " and P.ProductGroupID=" + pgID + " and P.ProductID=" + pId + " and P.EventID=" + eventID + "";
            if (Session["RoleID"].ToString() == "88")
            {
                cmdText += " and  C.MemberID =" + Session["LoginId"].ToString() + "";
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlObject.DataValueField = "LevelCode";
            ddlObject.DataTextField = "LevelCode";

            ddlObject.DataSource = ds;
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        ddlObject.SelectedValue = ds.Tables[0].Rows[0]["LevelCode"].ToString();
                    }

                }
                catch
                {
                }

                try
                {
                    ddlObject.SelectedValue = hdnLevel.Value;
                }
                catch
                {
                }
            }
        }
        catch
        {
        }
    }
    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "UploadPapers", "DownloadPapers", "ModifyPapers", "ReplicatePapers" };
        // string[] Choice = { "UploadPapers", "DownloadPapers" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void GetTestPapers(EntityTestPaper objETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        if (ddlFlrYear.SelectedValue == DateTime.Now.Year.ToString())
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        else
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@PaperType", objETP.PaperType);// Modified 22/07/2013
                                                                    //param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@EventYear", objETP.ContestYear);
        param[12] = new SqlParameter("@DocType", objETP.DocType);
        param[13] = new SqlParameter("@Sections", objETP.Sections);
        param[14] = new SqlParameter("EventId", objETP.EventId);
        param[15] = new SqlParameter("@Level", objETP.Level);
        param[16] = new SqlParameter("@RoleId", objETP.RoleId);
        param[17] = new SqlParameter("@MemberId", objETP.MemberId);
        param[18] = new SqlParameter("@Tlead", hdntlead.Value);
        param[19] = new SqlParameter("@Semester", objETP.Semester);
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
            {
                lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
                lblSearchErr.Visible = true;
            }
            else
            {
                lblSearchErr.Text = string.Empty;
            }
            DataView dv = new DataView(dt);
            //, EventID, ProductGroupID, ProductID, LevelID, PaperType, SecNum, WeekID,   QPaperID, CoachPaperID
            dv.Sort = "EventYear, EventId, ProductGroupID, ProductID, Level, PaperType,WeekId, Sections,  QPaperId, CoachPaperId";
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    private int GetTestPapers(int memberid, int roleid, string contestYear, int weekID)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetTestPapersforDownload";
        string SQLContest = "";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@memberid", memberid);
        param[1] = new SqlParameter("@contestyear", contestYear);
        param[2] = new SqlParameter("@WeekId", weekID);
        param[3] = new SqlParameter("@NoOfDaysBeforeContestDate", 10);
        param[4] = new SqlParameter("@Chapter", hdnChapterID.Value);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            DataTable dtNew = dt.Clone();
            String CurrTestFileName;
            String CurrTestFilePrefix;
            String PrevTestFilePrefix = "";
            try
            {
                //Following loop is to filter out the Test Paprers, which belong to the same product code and same set but with 
                //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
                //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                    CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                    if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                    {
                        PrevTestFilePrefix = CurrTestFilePrefix;
                        DataRow dr = dt.Rows[i];
                        dtNew.ImportRow(dr);
                    }
                }
                SQLContest = " Select COUNT(*) from CoachPapers where ProductCode in (Select Distinct c.ProductCode From Contestant c Inner Join Contest cn on cn.Contest_year=c.ContestYear and Cn.ContestId=c.Contestcode ";
                SQLContest = SQLContest + " where c.parentid=" + memberid + " and c.ContestYear= " + contestYear + " and cn.contestdate - 10 <= Convert(datetime, Convert(int, GetDate())) and ExamRecID=" + memberid + ") and ContestYear=" + contestYear;
                if (weekID > 0)
                {
                    SQLContest = SQLContest + " and WeekId=" + weekID;
                }
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, SQLContest)) > 0)
                {
                    lblPrdError.Text = "Your children are in the same contest(s).  So you cannot receive the other test papers.";
                }
            }

            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            DataView dv = new DataView(dtNew);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();

            return dtNew.Rows.Count;
        }
        else
        {
            return 0;
        }
    }

    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CoachPaperId", objDelETP.TestPaperId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }

    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion

    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "No uploaded file";
        }
    }


    private void SaveFileNewName(string sVirtualPath, string uploadFileName)
    {
        if (FileUpLoad1.HasFile)
        {
            FileUpLoad1.SaveAs(string.Concat(Server.MapPath(sVirtualPath), uploadFileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "No uploaded file";
        }
    }
    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {

            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }
    }
    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }
    }

    private string ModifyFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode = "";
        if (objETP.ProductGroupCode != string.Empty)
        {
            ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        }
        string extension;
        string l_value = string.Empty;
        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                if (ProductGroupCode == "UV")
                    l_value = "JUV";
                else
                    l_value = "JR";
                break;
            case "Senior":
                if (ProductGroupCode == "UV")
                    l_value = "SUV";
                else
                    l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                if (ProductGroupCode == "UV")
                    l_value = "IUV";
                else
                    l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }

        TFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
        // System.DateTime.Now.Year
        extension = Path.GetExtension(txtDescriptionM.Text);
        upFile = string.Concat(TFileName.ToString(), extension);
        return upFile;
    }

    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string ModifiedFileName(EntityTestPaper objETP, bool option)
    {
        StringBuilder CFileName = new StringBuilder();
        string exten;
        string l_value = string.Empty;

        string ProductGroupCode = "";
        if (objETP.ProductGroupCode != string.Empty)
        {
            ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        }

        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                if (ProductGroupCode == "UV")
                    l_value = "JUV";
                else
                    l_value = "JR";
                break;
            case "Senior":
                if (ProductGroupCode == "UV")
                    l_value = "SUV";
                else
                    l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                if (ProductGroupCode == "UV")
                    l_value = "IUV";
                else
                    l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }

        CFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);


        if (option == false)
        {
            if (SectionFlag == 1 | SectionFlag == 2)
            {
                exten = Path.GetExtension(ULFile.Value);
                upFile = string.Concat(CFileName.ToString(), exten);
            }
            else
            {
                exten = Path.GetExtension(FileUpLoad1.PostedFile.FileName);
                upFile = string.Concat(CFileName.ToString(), exten);
            }
        }
        else
        {
            upFile = CFileName.ToString();
        }

        return upFile;
    }

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {

        string ProductGroupCode = EventCodeAndProductGroupCode.Replace("Coaching - ", "");
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = EventCodeAndProductGroupCode.Substring(0, EventCodeAndProductGroupCode.Length - 5);
        return EventCode;
    }

    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {
        try
        {
            lblSearchErr.Text = "";
            Panel1.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            gvModify.Visible = false;
            Panel2.Visible = searchPanel;
            Panel3.Visible = searchPanel;
            pnlReplicate.Visible = false;
            if (searchPanel == false)
            {
                Panel4.Visible = true;
                GetWeek(ddlFlrWeekForExamReceiver, true);
            }
            GetFlrContextYear();
            loadPhase(ddlFlrSemester, ddlFlrYear.SelectedValue);
            GetProductGroupCodeByRoleId(ddlFlrProductGroup, true, int.Parse(ddlFlrYear.SelectedValue), ddlFlrSemester.SelectedValue, ddlFlrProduct);

            GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear.SelectedValue, ddlFlrSemester.SelectedValue);
            GetWeek(ddlFlrWeek, true);
            // GetFlrContextYear();
            PopulateContestants(ddlFlrNoOfContestants, true);
            PopulateEvents(ddlFlrEvent, false);
            PopulateSets(ddlFlrSet, true);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
            }
            else
            {
                //PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true, ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, ddlFlrProductGroup.SelectedValue, ddlFlrProduct.SelectedValue);
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    private void LoadReplicatePanel()
    {
        try
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
            Panel3.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            pnlReplicate.Visible = true;
            FillReplicateDetails();
        }
        catch (Exception ex)
        {
        }
    }
    private void LoadModifyPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = true;
            ModifyGrid.Visible = true;
            if (gvModify.Columns[0].Visible == false)
                gvModify.Columns[0].Visible = true;
            gvModify.Visible = true;
            GetContextYear(ddlContestYear, true);
            loadPhase(ddlSemester, ddlContestYear.SelectedValue);
            GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedItem.Value, ddlProduct);
            //GetContextYear(ddlContestYear, true);

            //ddlContestYear.SelectedIndex = 1;
            GetWeek(ddlWeek, true);
            try
            {
                // GetProductCodes(PGId(ddlProductGroup), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
            }
            catch
            {
            }
            PopulateContestants(ddlNoOfContestants, true);
            PopulateEvents(ddlEvent, false);
            PopulateSets(ddlSet, true);
            PopulateSection(ddlSections, true);
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup), ddlContestYear);
            }
            else
            {
                //PopulateLevel(ddlLevel, "NA", true); // Hard Coded - Not retrieving from database 
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
            }
            ddlDocType.SelectedIndex = 0;
            m_objETP.ProductGroupId = PGId(ddlProductGroup);
            GetCoachPapers("Modify");
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }

    }
    private void LoadUploadPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
            PanelAdd.Visible = true;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            gvModify.Visible = false;
            ddlDocType.Enabled = true;
            GetContextYear(ddlContestYear, true);
            loadPhase(ddlSemester, ddlContestYear.SelectedValue);
            GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedValue, ddlProduct);
            //GetContextYear(ddlContestYear, true); commented and moved above on Oct 28 2014.

            //ddlContestYear.SelectedIndex = 1;
            GetWeek(ddlWeek, true);
            GetProductCodes(PGId(ddlProductGroup), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
            PopulateContestants(ddlNoOfContestants, true);
            PopulateEvents(ddlEvent, false);
            PopulateSets(ddlSet, true);
            PopulateSection(ddlSections, true);
            LinkButton1.Text = "Show Uploaded files";
            string rid = Session["RoleId"].ToString();
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup), ddlContestYear);
                ddlDocType.SelectedIndex = 0;
            }
            else
            {
                //PopulateLevel(ddlLevel, "NA", true); // Hard Coded - Not retrieving from database 
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
                //  ddlDocType.SelectedIndex = 0;
            }

        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region " Private Methods - Validations "

    private bool ValidateFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode = "";
        if (objETP.ProductGroupCode != string.Empty)
        {
            ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        }
        bool IsValidFileName = false;
        // Get the initial part of the file name
        string extension;
        string l_value = string.Empty;
        switch (ddlLevel.SelectedItem.Text)
        {
            case "Junior":
                if (ProductGroupCode == "UV")
                    l_value = "JUV";
                else
                    l_value = "JR";
                break;
            case "Senior":
                if (ProductGroupCode == "UV")
                    l_value = "SUV";
                else
                    l_value = "SR";
                break;
            case "Beginner":
                l_value = "BEG";
                break;
            case "Intermediate":
                if (ProductGroupCode == "UV")
                    l_value = "IUV";
                else
                    l_value = "INT";
                break;
            case "Advanced":
                l_value = "ADV";
                break;
            default:
                l_value = "NA";
                break;
        }
        TFileName.AppendFormat("{0}_{1}_{2}_{3}_{4}_{5}_Set{6}_Wk{7}_Sec{8}_{9}", objETP.ContestYear, objETP.Event, objETP.PaperType, objETP.ProductGroupCode, objETP.ProductCode, l_value.ToString(), objETP.SetNum, objETP.WeekId, objETP.Sections, objETP.DocType);
        //System.DateTime.Now.Year
        if (SectionFlag == 1 | SectionFlag == 2)
        {
            extension = Path.GetExtension(ULFile.Value);
            upFile = string.Concat(TFileName.ToString(), extension);
        }
        else
        {
            extension = Path.GetExtension(FileUpLoad1.PostedFile.FileName);
            upFile = string.Concat(TFileName.ToString(), extension);
        }
        if (SectionFlag == 1 | SectionFlag == 2)
        {
            if (ULFile.Value.ToLower() == string.Concat(TFileName.ToString().ToLower(), extension))
            {
                IsValidFileName = true;
            }
        }
        else
        {
            if (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), extension))
            {
                IsValidFileName = true;
            }
        }
        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            if (ckFname.Checked == true)
            {
                m_objETP.TestFileName = upFile;
                return true;
            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString() + extension);
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion

    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnWeekNo.Value = ddlWeek.SelectedIndex.ToString();
        if (ddlWeek.SelectedValue != "-1")
        {
            ddlSet.SelectedIndex = ddlSet.Items.IndexOf(ddlSet.Items.FindByValue(ddlWeek.SelectedValue));
            ddlSet.Enabled = false;

            if (hdnpaperType.Value == "UploadPapers")

                if (ddlDocType.SelectedValue == "Q")
                    ddlSections.Enabled = true;
                else
                {
                    String StrSQL = "";
                    StrSQL = "Select Distinct Sections from CoachPapers where [ProductId]=" + PGId(ddlProduct) + " and [ProductCode]='" + PGCode(ddlProduct) + "' and [ProductGroupId]=" + PGId(ddlProductGroup) + " and [ProductGroupCode]='" + PGCode(ddlProductGroup) + "' and [Level]='" + ddlLevel.SelectedItem.Text + "' and [EventYear]=" + ddlContestYear.SelectedItem.Value;
                    StrSQL = StrSQL + " and [EventId]= " + ddlEvent.SelectedValue + " and [EventCode]='" + ddlEvent.SelectedItem.Text + "' and [WeekId]=" + int.Parse(ddlWeek.SelectedItem.Value) + " and [SetNum]=" + int.Parse(ddlSet.SelectedValue);
                    StrSQL = StrSQL + " and [PaperType]='" + ddlNoOfContestants.SelectedItem.Value.ToString() + "' and DocType ='Q' and Sections>0";//" + ddlDocType.SelectedItem.Value.ToString()+ "
                    int sections = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL));
                    if (sections >= 0)
                    {
                        try
                        {
                            ddlSections.SelectedValue = sections.ToString();
                        }
                        catch
                        {
                        }
                    }
                    // ddlSections.SelectedIndex = ddlSections.Items.IndexOf(ddlSections.Items.FindByValue(sections.ToString()));
                    ddlSections.Enabled = false;
                }



        }
    }
    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnDocType.Value = ddlDocType.SelectedValue;
        if (hdnpaperType.Value == "UploadPapers")

            if (ddlDocType.SelectedValue == "Q")
                ddlSections.Enabled = true;
            else
            {
                String StrSQL = "";
                StrSQL = "Select Distinct Sections from CoachPapers where [ProductId]=" + PGId(ddlProduct) + " and [ProductCode]='" + PGCode(ddlProduct) + "' and [ProductGroupId]=" + PGId(ddlProductGroup) + " and [ProductGroupCode]='" + PGCode(ddlProductGroup) + "' and [Level]='" + ddlLevel.SelectedItem.Text + "' and [EventYear]=" + ddlContestYear.SelectedItem.Value;
                StrSQL = StrSQL + " and [EventId]= " + ddlEvent.SelectedValue + " and [EventCode]='" + ddlEvent.SelectedItem.Text + "' and [WeekId]=" + int.Parse(ddlWeek.SelectedItem.Value) + " and [SetNum]=" + int.Parse(ddlSet.SelectedValue);
                StrSQL = StrSQL + " and [PaperType]='" + ddlNoOfContestants.SelectedItem.Value.ToString() + "' and DocType ='Q'";//" + ddlDocType.SelectedItem.Value.ToString()+ "
                int sections = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL));
                if (sections >= 0)
                {
                    try
                    {
                        ddlSections.SelectedValue = sections.ToString();
                    }
                    catch
                    {
                    }
                }
                //  ddlSections.SelectedIndex = ddlSections.Items.IndexOf(ddlSections.Items.FindByValue(sections.ToString()));
                ddlSections.Enabled = false;
            }
    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }

    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
        int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), ddlFlrYear.SelectedValue, selectedWeekId); //DateTime.Now.Year.ToString()
        if (Count == 0)
        {
            lblNoPermission.Visible = true;
            lblNoPermission.Text = "There are no records";
        }
    }

    protected void ddlFlrProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true, ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, ddlFlrProductGroup.SelectedValue, ddlFlrProduct.SelectedValue);

        ddlFlrLevel_SelectedIndexChanged(ddlFlrLevel, e);
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPId.Value = ddlProduct.SelectedValue;
        if (ddlProductGroup.Items.Count > 0)
        {

            string PgId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + PGId(ddlProduct) + "").ToString();
            string pgCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupcode from Product where ProductId=" + PGId(ddlProduct) + "").ToString();


            string selValue = PgId + "-" + pgCode;
            ddlProductGroup.SelectedValue = selValue;
            hdnPGId.Value = selValue;
        }

        PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
        if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                GetCoachPapers("Modify");
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {

    }
    protected void gvModify_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMerror.Text = "";
        try
        {
            GridViewRow row = gvModify.SelectedRow;
            ddlNoOfContestants.SelectedValue = row.Cells[6].Text;
            ddlDocType.SelectedValue = row.Cells[7].Text;
            ddlWeek.SelectedValue = row.Cells[12].Text;
            ddlSet.SelectedValue = row.Cells[13].Text;
            ddlSections.SelectedValue = row.Cells[14].Text;


            lblhiddenCpId.Text = gvModify.DataKeys[row.RowIndex].Values["CoachPaperId"].ToString();

            ddlProductGroup.SelectedValue = gvModify.DataKeys[row.RowIndex].Values["ProductGroupId"].ToString() + "-" + row.Cells[8].Text;
            GetProductCodes(PGId(ddlProductGroup), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);

            ddlProduct.SelectedValue = gvModify.DataKeys[row.RowIndex].Values["ProductId"].ToString() + "-" + row.Cells[9].Text;
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlLevel, true, PGId(ddlProductGroup), ddlContestYear);
            }
            else
            {
                PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
            }
            if (ddlLevel.Enabled)
            {
                ddlLevel.SelectedValue = ddlLevel.Items.FindByText(row.Cells[10].Text).Value;
            }
            //ddlDocType.Enabled = false;
            lblhiddenFname.Text = row.Cells[15].Text.Trim();
            txtDescriptionM.Text = row.Cells[16].Text.Replace("&nbsp;", "");
            TxtPasswordM.Text = row.Cells[17].Text.Replace("&nbsp;", "");
            btnUpdate.Enabled = true;
            btncancel.Enabled = true;
        }
        catch (Exception ex)
        {
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        // RequiredFieldValidator11.Enabled = false;
        lblMerror.Text = "";
        SearchFlag = true;
        gvModify_SelectedIndexChanged(null, null);
        btnUpdate.Enabled = false;
        btncancel.Enabled = false;

    }
    protected void ddlProductGroup_TextChanged(object sender, EventArgs e)
    {
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblMerror.Text = "";
        //if (ddlLevel.Enabled != true)
        //{
        //    // RequiredFieldValidator11.Enabled = false;
        //}
        //else
        //{
        //    //  RequiredFieldValidator11.Enabled = true;
        //    if (ddlLevel.SelectedValue == "-1")
        //    {
        //        //   RequiredFieldValidator11.IsValid = false;
        //    }
        //}
        //if (!ddlSections.Enabled)
        //{
        //    //  RequiredFieldValidator10.Enabled = false;
        //}
        //else
        //{
        //    if (ddlSections.SelectedIndex == 0)
        //    {
        //        //RequiredFieldValidator10.Enabled = true;
        //        //RequiredFieldValidator10.Visible = true;
        //        //RequiredFieldValidator10.IsValid = false;
        //    }
        //}

        if (ValidateUploadCoachPapers() == 1)
        {

            if (Page.IsValid)
            {
                EntityTestPaper objETP = new EntityTestPaper();
                lblMerror.Text = "";
                //  RequiredFieldValidator11.Enabled = false;
                //ddlLevel.Enabled = true;
                objETP.ProductId = PGId(ddlProduct);
                objETP.ProductCode = PGCode(ddlProduct);
                objETP.ProductGroupCode = PGCode(ddlProductGroup);
                objETP.ProductGroupId = PGId(ddlProductGroup);
                objETP.EventCode = ddlEvent.SelectedItem.Text;
                objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
                objETP.SetNum = int.Parse(ddlWeek.SelectedValue);
                objETP.Sections = int.Parse(ddlSections.SelectedItem.Text);
                objETP.PaperType = ddlNoOfContestants.SelectedItem.Value.ToString(); // Modified 22/07/2013
                //objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
                objETP.ContestYear = ddlContestYear.SelectedItem.Value;
                objETP.DocType = ddlDocType.SelectedItem.Value.ToString();
                objETP.Description = txtDescriptionM.Text;
                objETP.Password = TxtPasswordM.Text;
                objETP.CreateDate = System.DateTime.Now;
                objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
                objETP.Event = ddlEvent.SelectedItem.Text;
                objETP.Semester = ddlSemester.SelectedValue;
                if (ddlLevel.Enabled == true)
                {
                    objETP.Level = ddlLevel.SelectedItem.Text;
                }
                else
                {
                    objETP.Level = "NA";
                }
                string mFile = ModifiedFileName(objETP, true);
                string extension;
                extension = Path.GetExtension(lblhiddenFname.Text);
                mFile += extension;
                objETP.TestFileName = mFile;
                string strsql = "UPDATE Coachpapers Set [ProductId]=" + objETP.ProductId + ",[ProductCode]='" + objETP.ProductCode;
                strsql += "',[ProductGroupId]=" + objETP.ProductGroupId + ",[ProductGroupCode]='" + objETP.ProductGroupCode;
                if (objETP.Level == "NA")
                    strsql += "',[Level]=" + DBNull.Value;
                else
                    strsql += "',[Level]='" + objETP.Level + "'";

                strsql += ",[WeekId]=" + objETP.WeekId + ",[SetNum]=" + objETP.SetNum;
                strsql += ",[PaperType]='" + objETP.PaperType + "',[Sections]=" + objETP.Sections + ",[TestFileName]='" + objETP.TestFileName + "',[Description]='" + objETP.Description;
                strsql += "',[Password]='" + objETP.Password + "',[ModifyDate]='" + objETP.CreateDate + "',[ModifiedBy]=" + objETP.CreatedBy + " WHERE [CoachPaperId]=" + lblhiddenCpId.Text;
                try
                {
                    string oldFileName = lblhiddenFname.Text;
                    string newFileName = mFile;
                    File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), oldFileName), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), newFileName));
                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsql);
                    GetCoachPapers("Modify");
                    btnUpdate.Enabled = false;
                    btncancel.Enabled = false;
                    lblMerror.Text = "Updated successfully.";
                    txtDescriptionM.Text = "";
                }
                catch (Exception ex)
                {
                    lblMerror.Text = ex.Message.ToString();
                }
            }
        }
    }

    protected void bttnYes_Click(object sender, EventArgs e)
    {
        SectionFlag = 1;
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        PanelSections.Visible = false;
        dllfileChoice.Enabled = true;
        hdnsection.Value = "1";
        UpdateData();
    }
    protected void bttnNo_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()) + "cpy_" + ULFile.Value.ToString()))
        {
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), "cpy_" + ULFile.Value.ToString()));
        }
        Panel1.Visible = true;
        PanelAdd.Visible = true;
        PanelSections.Visible = false;
        hdnsection.Value = "0";
        SectionFlag = 0;
        //RequiredFieldValidator5.Enabled = true;
        //RequiredFieldValidator5.Visible = true;
        //RequiredFieldValidator5.IsValid = false;
        lblMessage.Text = "Please select a section value other than 0.";
    }

    protected void UploadedData()
    {
        try
        {
            lblstatus.Text = "";
            lblstatus.Visible = false;
            string StrSql = string.Empty;
            Boolean VolFlag = false;
            String WhereCndn = "";
            if (Session["RoleId"].ToString() == "1" | Session["RoleId"].ToString() == "2" | Session["RoleId"].ToString() == "96" | Session["RoleId"].ToString() == "30")
            {
                StrSql = "SELECT * FROM CoachPapers ";
                VolFlag = true;
            }
            else
            {
                StrSql = "select * from CoachPapers C where C.ProductID in(select ProductID  from volunteer where MemberID =" + Session["LoginID"].ToString() + ")";
                VolFlag = false;
                //Response.Write(StrSql);
            }
            if (VolFlag == true)
            {
                if (ddlProductGroup.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " Where ProductGroupId=" + PGId(ddlProductGroup);

                if (ddlProduct.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and ProductId=" + PGId(ddlProduct);
                    else
                        WhereCndn = WhereCndn + " Where ProductId=" + PGId(ddlProduct);

                if (ddlLevel.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and Level='" + ddlLevel.SelectedItem.Text + "'";
                    else
                        WhereCndn = WhereCndn + " Where Level='" + ddlLevel.SelectedItem.Text + "'";

                if (ddlNoOfContestants.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and PaperType='" + ddlNoOfContestants.SelectedValue + "'";
                    else
                        WhereCndn = WhereCndn + " Where PaperType='" + ddlNoOfContestants.SelectedValue + "'";

                if (ddlDocType.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and DocType='" + ddlDocType.SelectedValue + "'";
                    else
                        WhereCndn = WhereCndn + " Where DocType='" + ddlDocType.SelectedValue + "'";

                if (ddlWeek.SelectedValue != "-1")
                    if (WhereCndn != "")
                        WhereCndn = WhereCndn + " and WeekID=" + ddlWeek.SelectedValue;
                    else
                        WhereCndn = WhereCndn + " Where WeekID=" + ddlWeek.SelectedValue;

                WhereCndn = WhereCndn + " and Semester='" + ddlSemester.SelectedValue + "'";
            }
            else
            {
                if (ddlProductGroup.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and ProductGroupId=" + PGId(ddlProductGroup);
                if (ddlProduct.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and ProductId=" + PGId(ddlProduct);
                if (ddlLevel.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and Level='" + ddlLevel.SelectedItem.Text + "'";
                if (ddlNoOfContestants.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and PaperType='" + ddlNoOfContestants.SelectedValue + "'";
                if (ddlDocType.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and DocType='" + ddlDocType.SelectedValue + "'";
                if (ddlWeek.SelectedValue != "-1")
                    WhereCndn = WhereCndn + " and WeekID=" + ddlWeek.SelectedValue;

                WhereCndn = WhereCndn + " and Semester='" + ddlSemester.SelectedValue + "'";
            }

            if (ddlContestYear.SelectedValue != "-1")
                if (WhereCndn != "")
                    WhereCndn = WhereCndn + " and EventYear=" + ddlContestYear.SelectedItem.Value;
                else
                    WhereCndn = WhereCndn + " Where EventYear=" + ddlContestYear.SelectedItem.Value;


            StrSql = StrSql + WhereCndn + " ORDER BY CoachPaperID";
            if (LinkButton1.Text == "Show Uploaded files")
            {

                DataSet dsu = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSql);
                if (dsu.Tables[0].Rows.Count > 0)
                {
                    gvModify.DataSource = dsu;
                    gvModify.DataBind();
                    gvModify.SelectedIndex = -1;
                    gvModify.Visible = true;
                    gvModify.Columns[0].Visible = false;
                    LinkButton1.Text = "Hide Uploaded files";
                    hdnuploaded.Value = "Y";
                }
                else
                {
                    gvModify.DataSource = null;
                    gvModify.DataBind();
                    lblstatus.Text = "No files uploaded.";
                    lblstatus.Visible = true;
                    hdnuploaded.Value = "N";
                }

            }
            else if (LinkButton1.Text == "Hide Uploaded files")
            {
                gvModify.Visible = false;
                LinkButton1.Text = "Show Uploaded files";
                hdnuploaded.Value = "N";
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        UploadedData();
    }


    protected void ddlFlrWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFlrWeek.SelectedValue != "-1")
        {
            ddlFlrSet.SelectedIndex = ddlFlrSet.Items.IndexOf(ddlFlrSet.Items.FindByValue(ddlFlrWeek.SelectedValue));
            ddlFlrSet.Enabled = false;
        }
        else
        {
            ddlFlrSet.SelectedIndex = -1;
        }
        ddlFlrSet_SelectedIndexChanged(ddlFlrSet, e);
    }

    public void SetReleaseDates()
    {
        try
        {
            string cmdtext = string.Empty;
            string coachClassDate = string.Empty;
            cmdtext = "select * from coachclasscal where Eventyear=" + ddlContestYear.SelectedValue + " and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductID=" + PGId(ddlProduct) + " and Level='" + ddlLevel.SelectedValue + "' and Semester='" + ddlSemester.SelectedValue + "' and memberID not in (select memberID from coachreldates where  Eventyear=" + ddlContestYear.SelectedValue + " and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductID=" + PGId(ddlProduct) + " and Level='" + ddlLevel.SelectedValue + "'  and CoachPaperID=2851) and WeekNo=" + ddlWeek.SelectedValue + " and ClassType in ('Regular', 'Makeup', 'Substitute','Holiday') and Status='On'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string cmdText = "select CoachPaperId from CoachPapers where EventYear=" + ddlContestYear.SelectedValue + " and EventId=13 and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductId= " + PGId(ddlProduct) + " and Level='" + ddlLevel.SelectedValue + "' and WeekId=" + ddlWeek.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' and DocType='Q'";
                int iCoachPaperId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    coachClassDate = dr["Date"].ToString();

                    cmdText = "select coachRelID from coachreldates where EventYear=" + ddlContestYear.SelectedValue + "  and ProductGroupId=" + PGId(ddlProductGroup) + " and ProductId= " + PGId(ddlProduct) + " and Level='" + ddlLevel.SelectedValue + "' and Semester='" + ddlSemester.SelectedValue + "' and Session=" + dr["SessionNo"].ToString() + " and MemberID=" + dr["MemberID"].ToString() + " and CoachPaperID=" + iCoachPaperId + "";
                    int coachRelID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                    if (coachRelID <= 0)
                    {
                        DateTime QRDate = Convert.ToDateTime(coachClassDate);
                        DateTime QDDate = Convert.ToDateTime(coachClassDate).AddDays(5);

                        DateTime ARDate = Convert.ToDateTime(coachClassDate).AddDays(7);
                        DateTime SRDate = Convert.ToDateTime(coachClassDate).AddDays(-1);
                        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

                        string sqlCommand = "usp_Insert_CoachRelDate";
                        SqlParameter[] param = new SqlParameter[10];
                        param[0] = new SqlParameter("@CoachPaperId", iCoachPaperId);
                        param[1] = new SqlParameter("@Semester", ddlSemester.SelectedValue);
                        param[2] = new SqlParameter("@SessionNo", dr["SessionNo"].ToString());
                        param[3] = new SqlParameter("@QReleaseDate", QRDate);
                        param[4] = new SqlParameter("@QDeadlineDate", QDDate);
                        param[5] = new SqlParameter("@AReleaseDate", ARDate);
                        param[6] = new SqlParameter("@SReleaseDate", SRDate);
                        param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
                        param[8] = new SqlParameter("@CreatedBy", Int32.Parse(Session["LoginID"].ToString()));
                        param[9] = new SqlParameter("@MemberId", Int32.Parse(dr["MemberID"].ToString()));

                        SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                    }
                }
            }
        }
        catch
        {
        }
    }



    #endregion

    #region "Enums"
    public enum ScreenChoice
    {
        DownloadPapers,
        UploadPapers,
        ModifyPapers,
        ReplicatePapers,
        TestSections,
        Answerkey

    }

    #endregion

    #region " Class EntityTestPaper (ETP) "
    public class EntityTestPaper
    {
        public Int32 TestPaperId = -1;
        public Int32 ProductId = -1;
        public string ProductCode = DBNull.Value.ToString();
        public string Event = DBNull.Value.ToString();
        public Int32 EventId = -1;
        public Int32 ProductGroupId = -1;
        public string ProductGroupCode = DBNull.Value.ToString();
        public string EventCode = DBNull.Value.ToString();
        public Int32 WeekId = -1;
        public Int32 SetNum = -1;
        public string PaperType = DBNull.Value.ToString(); //Modified on 22/07/2013
        public Int32 Sections = -1;
        public string TestFileName = DBNull.Value.ToString();
        public string Description = DBNull.Value.ToString();
        public string Password = DBNull.Value.ToString();
        public DateTime CreateDate = new System.DateTime(1900, 1, 1);
        public Int32 CreatedBy = -1;
        public DateTime? ModifyDate = null;
        public Int32? ModifiedBy = null;
        public string ContestYear = DBNull.Value.ToString();
        public string DocType = DBNull.Value.ToString();
        public string WeekOf = DBNull.Value.ToString();
        public string ReceivedBy = DBNull.Value.ToString();
        public string Level = DBNull.Value.ToString();
        public Int32 RoleId = -1;
        public Int32 MemberId = -1;
        public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
        public string Semester = DBNull.Value.ToString();


        public string getCompleteFTPFilePath(String TestFileName)
        {
            return String.Format("{0}/{1}",
                      System.Configuration.ConfigurationManager.AppSettings["FTPCoachPapersPath"], TestFileName);
        }
        public EntityTestPaper()
        {
        }

    }
    protected void ddlContestYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnYear.Value = ddlContestYear.SelectedValue;
        if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                GetCoachPapers("Modify");
            }
        }
    }
    protected void ddlFlrYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnYear.Value = ddlFlrYear.SelectedValue;
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            GetProductGroupCodeByRoleId(ddlFlrProductGroup, true, int.Parse(ddlFlrYear.SelectedValue), ddlFlrSemester.SelectedValue, ddlFlrProduct);

            GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear.SelectedValue, ddlFlrSemester.SelectedValue);
            GetWeek(ddlFlrWeek, true);
            PopulateContestants(ddlFlrNoOfContestants, true);
            PopulateEvents(ddlFlrEvent, false);
            PopulateSets(ddlFlrSet, true);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
            }
            else
            {
                // PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true, ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, ddlFlrProductGroup.SelectedValue, ddlFlrProduct.SelectedValue);
            }
        }

        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
        {
            PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
        }
        else
        {
            //PopulateLevel(ddlFlrLevel, "NA", true);//Hard Coded - Not retrieving from database
            PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true, ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, ddlFlrProductGroup.SelectedValue, ddlFlrProduct.SelectedValue);
        }
    }




    private void GetRepTestPapers(EntityTestPaper objETP, GridView gv)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "CoachPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@PaperType", objETP.PaperType);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@EventYear", objETP.ContestYear);
        param[12] = new SqlParameter("@DocType", objETP.DocType);
        param[13] = new SqlParameter("@Sections", objETP.Sections);
        param[14] = new SqlParameter("EventId", objETP.EventId);
        param[15] = new SqlParameter("@Level", objETP.Level);
        param[16] = new SqlParameter("@RoleId", objETP.RoleId);
        param[17] = new SqlParameter("@MemberId", objETP.MemberId);
        param[18] = new SqlParameter("@Tlead", hdntlead.Value);
        param[19] = new SqlParameter("@Semester", objETP.Semester);
        DataView dv;
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
                lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
            else
                lblSearchErr.Text = string.Empty;

            dv = new DataView(dt);
            dv.Sort = "CoachPaperID";
            if (hideColumns == true)
            {
                gv.DataSource = dv;
                gv.DataBind();
            }
            gvTPDestination.DataSource = dv;
            gvTPDestination.DataBind();

            if (gvTPDestination.Rows.Count > 0)
            {
                lblRep_NoRecord.Text = "";
                lblToTSNoRec.Text = "";
            }
            else
            {
                lblRep_NoRecord.Text = "No record exists";

                lblToTSNoRec.Text = "No record exists";
            }
            if (hideColumns == true)
            {
                lblRep_NoRecord.Text = "No record exists";

                lblToTSNoRec.Text = "No record exists";
                gvTPDestination.Visible = false;
            }
            else
            {
                lblRep_NoRecord.Text = "";

                lblToTSNoRec.Text = "";
                gvTPDestination.Visible = true;
            }

            if (hideColumns == false)
            {
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CoachPapers_TestAnwerKey_GetByCriteria]", param);
                dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    dv = new DataView(dt);
                    dv.Sort = "AnswerKeyRecID";
                    gvToAK.DataSource = dv;
                    gvToAK.DataBind();
                    gvToAK.Visible = true;
                    lblToAKNoRec.Text = "";
                }
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CoachPapers_TestSetUp_GetByCriteria]", param);
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dv = new DataView(dt);
                    dv.Sort = "TestSetupSectionsID";
                    gvToTS.DataSource = dv;
                    gvToTS.DataBind();
                    gvToTS.Visible = true;
                }


            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }




    }
    bool hideColumns = true;
    protected void btnRepConfirmToSubmit_Click(object sender, EventArgs e)
    {
        hideColumns = true;
        CoachPapersIDs = "";

        m_objETP = new EntityTestPaper();
        m_objETP.ContestYear = ddlRepFrmYear.SelectedValue;
        m_objETP.ProductId = PGId(ddlRepFrmProduct);
        m_objETP.ProductGroupId = PGId(ddlRepFrmPrdGroup);
        m_objETP.WeekId = int.Parse(ddlRepFrmWeekNo.SelectedValue);
        m_objETP.SetNum = int.Parse(ddlRepFrmSetNo.SelectedValue);
        m_objETP.RoleId = Int32.Parse(Session["RoleId"].ToString());
        m_objETP.MemberId = Int32.Parse(Session["LoginId"].ToString());
        m_objETP.Semester = ddlRepFrmSemester.SelectedValue;

        if (DDLDocTypeRepFrm.SelectedValue != "0")
        {
            m_objETP.DocType = DDLDocTypeRepFrm.SelectedValue; //Modified 27/07/2013
        }

        if (ddlRepFrmPaperType.SelectedValue != "-1")
        {
            m_objETP.PaperType = ddlRepFrmPaperType.SelectedValue; //Modified 27/07/2013
        }
        if (ddlRepFrmLevel.Items.Count > 0 && ddlRepFrmLevel.SelectedValue != "-1")
        {
            m_objETP.Level = ddlRepFrmLevel.SelectedItem.Text;
        }
        else
        {
            m_objETP.Level = string.Empty;// = ddlFlrLevel.SelectedItem.Text;
        }
        if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y" & ddlFlrLevel.SelectedValue == "-1")
        {
            PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
        }
        m_objETP.Description = string.Empty;
        m_objETP.TestFileName = string.Empty;
        GetRepTestPapers(m_objETP, gvTPSource);
        if (gvTPSource.Rows.Count > 0)
        {
            lblRep_NoRec.Text = "";
        }
        else
        {
            lblRep_NoRec.Text = "No record exists";
        }
        // Assign value to Destination dropdown      
        ddlRepToEvent.SelectedValue = ddlRepFrmEvent.SelectedValue;
        ddlRepToProduct.SelectedValue = ddlRepFrmProduct.SelectedValue;
        ddlRepToLevel.SelectedValue = ddlRepFrmLevel.SelectedValue;
        ddlRepToPaperType.SelectedValue = ddlRepFrmPaperType.SelectedValue;
        DDlRepToDocType.SelectedValue = DDLDocTypeRepFrm.SelectedValue;
        ResetReplicatePapers(true);
        btnRepSave.Enabled = true;
        GetRepTestSections();

        GetRepTestAnswerKey();

    }

    private void GetRepTestSections()
    {
        try
        {
            string s = "select * from TestSetUpSections where coachpaperid in (" + CoachPapersIDs + ")";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, s);
            gvFromTS.DataSource = ds.Tables[0];
            gvFromTS.DataBind();

            gvToTS.DataSource = ds.Tables[0];
            gvToTS.DataBind();
            if (gvFromTS.Rows.Count > 0)
            {
                lblFromTSNoRec.Text = "";
            }
            else
            {
                lblFromTSNoRec.Text = "No record exists";
            }

            if (gvToTS.Rows.Count > 0)
            {

                lblToAKNoRec.Text = "";
            }
            else
            {
                lblToAKNoRec.Text = "No record exists";
            }

            if (hideColumns == true)
            {
                lblToAKNoRec.Text = "No record exists";


                gvToTS.Visible = false;
            }
            else
            {
                lblToAKNoRec.Text = "";
                gvToTS.Visible = true;
            }
        }
        catch (Exception e)
        {
            lblFromTSNoRec.Text = "No record exists";
            lblToAKNoRec.Text = "No record exists";
        }
    }

    private void GetRepTestAnswerKey()
    {
        try
        {
            string s = "select * from TestAnswerKey where coachpaperid in (" + CoachPapersIDs + ")";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, s);
            gvFromAK.DataSource = ds.Tables[0];
            gvFromAK.DataBind();
            gvToAK.DataSource = ds.Tables[0];
            gvToAK.DataBind();
            if (gvFromAK.Rows.Count > 0)
            {
                lblFromAKNoRec.Text = "";
            }
            else
            {
                lblFromAKNoRec.Text = "No record exists";
            }

            if (gvToAK.Rows.Count > 0)
            {
                lblToAKNoRec.Text = "";
            }
            else
            {
                lblToAKNoRec.Text = "No record exists";
            }

            if (hideColumns == true)
            {
                lblToAKNoRec.Text = "No record exists";


                gvToAK.Visible = false;
            }
            else
            {
                lblToAKNoRec.Text = "";
                gvToAK.Visible = true;
            }
        }
        catch (Exception e)
        {
            lblFromAKNoRec.Text = "No record exists";
            lblToAKNoRec.Text = "No record exists";
        }
    }


    protected void btnRepSubmit_Click(object sender, EventArgs e)
    {
        lblRepMsg.Text = "";
        if (ddlRepFrmYear.SelectedItem.Value == ddlRepToYear.SelectedItem.Value)
        {
            lblRepMsg.ForeColor = System.Drawing.Color.Red;
            lblRepMsg.Text = "Source and Destination year must be different.";
            return;
        }

        if (ddlRepFrmWeekNo.SelectedValue != ddlRepToWeekNo.SelectedValue || ddlRepFrmSetNo.SelectedValue != ddlRepToSetNo.SelectedValue)
        {
            //Show warning to  replace;
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Replicate", "ConfirmWithWeekSetNo();", true);
        }
        else
        {
            btnRepConfirmToSubmit_Click(sender, e);
        }

    }

    protected void ddlRepFrmPrdGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPgIdRP.Value = ddlRepFrmPrdGroup.SelectedValue;
        GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepFrmProduct, true, ddlRepFrmYear.SelectedValue, ddlRepFrmSemester.SelectedValue);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlRepFrmLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
        }
        else
        {
            PopulateLevel(ddlRepFrmLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
        }
        GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepToProduct, true, ddlRepToYear.SelectedValue, ddlRepToSemester.SelectedValue);
        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlRepToLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
        }
        else
        {
            PopulateLevel(ddlRepToLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
        }
        ddlRepToPrdGroup.SelectedValue = ddlRepFrmPrdGroup.SelectedValue;
        ddlRepToProduct.Enabled = false; ddlRepToLevel.Enabled = false;
    }
    protected void ddlRepFrmProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPIdRP.Value = ddlRepFrmProduct.SelectedValue;
        if (ddlRepFrmPrdGroup.Items.Count > 0)
        {

            string PgId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + PGId(ddlRepFrmProduct) + "").ToString();
            string pgCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupcode from Product where ProductId=" + PGId(ddlRepFrmProduct) + "").ToString();


            string selValue = PgId + "-" + pgCode;
            ddlRepFrmPrdGroup.SelectedValue = selValue;
            hdnPgIdRP.Value = selValue;
        }

        ddlRepToProduct.SelectedValue = ddlRepFrmProduct.SelectedValue;
        PopulateLevel(ddlRepFrmLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
        hdnLevelRP.Value = ddlRepFrmLevel.SelectedValue;

        if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
        {
            PopulateLevelNew(ddlRepToLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
            //  hdnLevelRP.Value = ddlRepToLevel.SelectedValue;
        }
        else
        {
            PopulateLevel(ddlRepToLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);

        }
        ddlRepToPrdGroup.SelectedValue = ddlRepFrmPrdGroup.SelectedValue;
        ddlRepToProduct.Enabled = false; ddlRepToLevel.Enabled = false;

    }
    protected void ddlRepFrmLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnLevelRP.Value = ddlRepFrmLevel.SelectedValue;
        lblRepMsg.Text = "";
        ddlRepToLevel.SelectedValue = ddlRepFrmLevel.SelectedValue;
        ddlRepToLevel.Enabled = false;
    }
    protected void ddlRepFrmPaperType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRepToPaperType.SelectedValue = ddlRepFrmPaperType.SelectedValue;
        hdnPaperTypeRP.Value = ddlRepFrmPaperType.SelectedValue;
    }
    protected void ddlRepFrmWeekNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRepToWeekNo.SelectedValue = ddlRepFrmWeekNo.SelectedValue;
        ddlRepFrmSetNo.SelectedValue = ddlRepFrmWeekNo.SelectedValue;
        ddlRepToSetNo.SelectedValue = ddlRepFrmWeekNo.SelectedValue;
        hdnWeekRP.Value = ddlRepFrmWeekNo.SelectedValue;
    }
    protected void ddlRepToWeekNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRepToSetNo.SelectedValue = ddlRepToWeekNo.SelectedValue;
    }
    protected void ddlRepFrmSetNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRepToSetNo.SelectedValue = ddlRepFrmSetNo.SelectedValue;
    }
    protected void gvTPSource_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                objETP.TestPaperId = int.Parse(gvTPSource.Rows[index].Cells[1].Text);
                objETP.TestFileName = gvTPSource.Rows[index].Cells[15].Text;

                if (e.CommandName == "Download")
                {
                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], objETP);
                }
            }
        }
    }
    protected void gvTPDestination_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                //  objETP.TestPaperId = int.Parse(gvTPDestination.Rows[index].Cells[1].Text);
                objETP.TestFileName = gvTPDestination.Rows[index].Cells[15].Text;

                if (e.CommandName == "Download")
                {
                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], objETP);
                }
            }
        }
    }
    protected void gvTPDestination_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ddlRepToWeekNo.SelectedItem.Value != "-1")
            {
                e.Row.Cells[13].Text = ddlRepToWeekNo.SelectedItem.Text;
            }
            if (ddlRepToSetNo.SelectedValue != "-1")
            {
                e.Row.Cells[12].Text = ddlRepToSetNo.SelectedItem.Text;
            }
            e.Row.Cells[3].Text = ddlRepToYear.SelectedItem.Value;
            e.Row.Cells[15].Text = e.Row.Cells[15].Text.Replace(ddlRepFrmYear.SelectedValue, ddlRepToYear.SelectedValue);
            int stIndex = e.Row.Cells[15].Text.ToLower().IndexOf("_set");
            int enIndex = e.Row.Cells[15].Text.IndexOf(".");
            string startString = e.Row.Cells[15].Text.Substring(0, stIndex);
            string CoachFileName = startString + "_Set" + e.Row.Cells[12].Text + "_Wk" + e.Row.Cells[13].Text;
            CoachFileName = CoachFileName + "_Sec" + e.Row.Cells[14].Text + "_" + e.Row.Cells[7].Text + e.Row.Cells[15].Text.Substring(enIndex);
            e.Row.Cells[15].Text = CoachFileName;
            if (hideColumns == true)
            {
                e.Row.Cells[1].Text = "";
                e.Row.Cells[2].Text = "";
            }
        }
    }

    protected void gvToTS_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (hideColumns == true)
            {
                e.Row.Cells[0].Text = "";
                e.Row.Cells[1].Text = "";

            }
            e.Row.Cells[5].Text = ddlRepToYear.SelectedValue;
        }
    }
    protected void gvToAK_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (hideColumns == true)
            {
                e.Row.Cells[0].Text = "";
                e.Row.Cells[1].Text = "";

            }
            e.Row.Cells[4].Text = ddlRepToYear.SelectedValue;
        }
    }

    protected void btnRepSave_Click(object sender, EventArgs e)
    {
        //Validate Duplicate
        int rCnt = 0;
        string strCmd = "select count(*) from CoachPapers where EventYear=" + ddlRepToYear.SelectedItem.Value;// +" and ProductGroupId=" + PGId(ddlRepToPrdGroup) + " and ProductId=" + PGId(ddlRepToProduct);
        //Show warning to  replace;
        int wkNo = 0;
        if (ddlRepFrmProduct.Items.Count > 0 && ddlRepFrmProduct.SelectedItem.Text != "[Select]")
        {
            strCmd = strCmd + " and ProductId=" + PGId(ddlRepFrmProduct);
        }
        if (ddlRepFrmLevel.Items.Count > 0 && ddlRepFrmLevel.SelectedItem.Text != "[Select Level]")
        {
            strCmd = strCmd + " and Level='" + ddlRepFrmLevel.SelectedItem.Text + "'";
        }
        if (ddlRepToWeekNo.Items.Count > 0 && ddlRepToWeekNo.SelectedItem.Text != "[Week#]")
        {
            wkNo = Convert.ToInt16(ddlRepToWeekNo.SelectedItem.Text);
            strCmd = strCmd + " and WeekId=" + wkNo;
        }
        if (ddlRepToSetNo.Items.Count > 0 && ddlRepToSetNo.SelectedItem.Text != "[Select Set#]")
        {
            strCmd = strCmd + " and SetNum=" + Convert.ToInt16(ddlRepToSetNo.SelectedItem.Text);
        }

        strCmd = strCmd + " and Semester='" + ddlRepToSemester.SelectedValue + "'";

        rCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strCmd));
        if (rCnt > 0)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Replicate", "ConfirmRepOverWrite(" + ddlRepToYear.SelectedItem.Value + "," + wkNo + ");", true);
        }
        else
        {
            btnRepConfirmToSave_Click(sender, e);
        }
    }


    protected void btnRepConfirmToSave_Click(object sender, EventArgs e)
    {
        lblRepMsg.ForeColor = System.Drawing.Color.Red;
        try
        {
            // validate duplicate
            string sqlCommand = "CoachPapers_Replicate_Insert";
            SqlParameter[] param = new SqlParameter[17];
            param[0] = new SqlParameter("@MemberId", int.Parse(Session["LoginID"].ToString()));
            param[1] = new SqlParameter("@RoleId", int.Parse(Session["RoleID"].ToString()));
            param[2] = new SqlParameter("@ProductId", PGId(ddlRepToProduct));
            param[3] = new SqlParameter("@EventId", int.Parse(ddlRepToEvent.SelectedValue));
            param[4] = new SqlParameter("@ProductCode", PGCode(ddlRepToProduct));
            param[5] = new SqlParameter("@ProductGroupId", PGId(ddlRepToPrdGroup));
            param[6] = new SqlParameter("@EventYear", ddlRepFrmYear.SelectedItem.Value);
            param[7] = new SqlParameter("@ProductGroupCode", PGCode(ddlRepToPrdGroup));
            if (ddlRepToLevel.SelectedValue != "-1")
            {
                param[8] = new SqlParameter("@Level", ddlRepToLevel.SelectedItem.Text);
            }
            else
            {
                param[8] = new SqlParameter("@Level", string.Empty);
            }
            if (ddlRepFrmWeekNo.SelectedIndex == 0)
            {
                param[9] = new SqlParameter("@WeekId", -1);
            }
            else
            {
                param[9] = new SqlParameter("@WeekId", int.Parse(ddlRepFrmWeekNo.SelectedItem.Value));
            }
            if (ddlRepFrmSetNo.SelectedIndex == 0)
            {
                param[10] = new SqlParameter("@SetNum", -1);
            }
            else
            {
                param[10] = new SqlParameter("@SetNum", int.Parse(ddlRepFrmSetNo.SelectedValue));
            }
            param[11] = new SqlParameter("@TLead", hdntlead.Value);
            param[12] = new SqlParameter("@RepToYear", int.Parse(ddlRepToYear.SelectedItem.Value));
            if (ddlRepToSetNo.SelectedIndex == 0)
            {
                param[13] = new SqlParameter("@RepToSetNum", -1);
            }
            else
            {
                param[13] = new SqlParameter("@RepToSetNum", int.Parse(ddlRepToSetNo.SelectedItem.Text));
            }
            if (ddlRepToWeekNo.SelectedIndex == 0)
            {
                param[14] = new SqlParameter("@RepToWeekId", -1);
            }
            else
            {
                param[14] = new SqlParameter("@RepToWeekId", int.Parse(ddlRepToWeekNo.SelectedItem.Text));
            }
            param[15] = new SqlParameter("@RepToSemester", ddlRepToSemester.SelectedValue);
            param[16] = new SqlParameter("@Semester", ddlRepFrmSemester.SelectedValue);
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, sqlCommand, param);

            sqlCommand = "[TestAnswerKey_Replicate_Insert]";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, sqlCommand, param);

            sqlCommand = "[TestSection_Replicate_Insert]";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, sqlCommand, param);

            CopyDestinationFile();
            lblRepMsg.ForeColor = System.Drawing.Color.Green;
            lblRepMsg.Text = "Replicated Successfully";
            // FillReplicateDetails();
            // ResetReplicatePapers(false);
            btnRepSave.Enabled = false;

            //Re Fill To Coach Papers
            m_objETP = new EntityTestPaper();
            m_objETP.ContestYear = ddlRepToYear.SelectedValue;
            //m_objETP.ProductId = PGId(ddlRepFrmProduct);
            //m_objETP.ProductGroupId = PGId(ddlRepFrmPrdGroup);

            m_objETP.ProductId = PGId(ddlRepToProduct);
            m_objETP.ProductGroupId = PGId(ddlRepToPrdGroup);

            m_objETP.WeekId = int.Parse(ddlRepToWeekNo.SelectedValue);
            m_objETP.SetNum = int.Parse(ddlRepToSetNo.SelectedValue);
            m_objETP.RoleId = Int32.Parse(Session["RoleId"].ToString());
            m_objETP.MemberId = Int32.Parse(Session["LoginId"].ToString());
            if (DDlRepToDocType.SelectedValue != "0")
            {
                m_objETP.DocType = DDlRepToDocType.SelectedValue;
            }
            if (ddlRepToPaperType.SelectedValue != "-1")
            {
                m_objETP.PaperType = ddlRepToPaperType.SelectedValue; //Modified 27/07/2013
            }
            if (ddlRepToLevel.SelectedValue != "-1")
            {
                m_objETP.Level = ddlRepToLevel.SelectedItem.Text;
            }
            else
            {
                m_objETP.Level = string.Empty;// = ddlFlrLevel.SelectedItem.Text;
            }
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y" & ddlFlrLevel.SelectedValue == "-1")
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
            }
            m_objETP.Description = string.Empty;
            m_objETP.TestFileName = string.Empty;
            m_objETP.Semester = ddlRepToSemester.SelectedValue;

            hideColumns = false;
            GetRepTestPapers(m_objETP, gvTPDestination);
            //  GetRepTestPapers(m_objETP, gvTPSource);

            if (gvTPSource.Rows.Count > 0)
            {
                lblRep_NoRec.Text = "";
            }
            else
            {
                lblRep_NoRec.Text = "No record exists";
            }

        }
        catch (Exception ex)
        {

            lblRepMsg.Text = "Error raised while replicating";//  lblRepMsg.Text ="Err:" + ex.ToString();
        }

    }

    private void CopyDestinationFile()
    {
        foreach (GridViewRow gvr in gvTPSource.Rows)
        {
            string fNameSource = string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), gvr.Cells[15].Text);
            string fNameDestination = string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"].ToString()), gvTPDestination.Rows[gvr.RowIndex].Cells[15].Text);

            // Validate is exists
            if (File.Exists(fNameDestination) == true)
            { // delete existing file
                File.Delete(fNameDestination);
            }

            if (File.Exists(fNameSource) == true)
            {
                // create new file
                File.Copy(fNameSource, fNameDestination);
            }
        }
    }

    private void ResetReplicatePapers(bool b)
    {
        lblRepMsg.ForeColor = System.Drawing.Color.Red;
        lblRep_Source.Visible = b;
        lblRep_Destination.Visible = b;
        lblFromTS.Visible = b;
        lblToTS.Visible = b;
        lblFromAK.Visible = b;
        lblToAK.Visible = b;

        gvTPSource.Visible = b;
        //  gvTPDestination.Visible = b;

        gvFromTS.Visible = b;
        gvFromAK.Visible = b;

        //  gvToTS.Visible = b;
        // gvToAK.Visible = b;
    }
    protected void ddlFlrLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnLevel.Value = ddlFlrLevel.SelectedValue;
        ddlFlrNoOfContestants_SelectedIndexChanged(ddlFlrNoOfContestants, e);
    }
    protected void ddlFlrNoOfContestants_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlFlrWeek_SelectedIndexChanged(ddlFlrWeek, e);
    }
    protected void ddlFlrSet_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnSearch_Click(btnSearch, e);
    }

    protected void ddlRepFrmYear_SelectedIndexChanged(object sender, EventArgs e)
    {

        try
        {
            try
            {
                string sqltext = "    select distinct CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] AS[IDandCode], p.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlRepFrmYear.SelectedValue + " and cs.Accepted = 'Y'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqltext);
                if (null != ds && ds.Tables.Count > 0)
                {
                    ddlRepFrmProduct.DataTextField = "Name";
                    ddlRepFrmProduct.DataValueField = "IDandCode";
                    ddlRepFrmProduct.DataSource = ds;
                    ddlRepFrmProduct.DataBind();
                    ddlRepFrmProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                    ddlRepFrmProduct.SelectedIndex = 0;

                    sqltext = "    select distinct CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] AS[IDandCode], p.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlRepToYear.SelectedValue + " and cs.Accepted = 'Y'";

                    DataSet dsTo = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqltext);

                    ddlRepToProduct.DataTextField = "Name";
                    ddlRepToProduct.DataValueField = "IDandCode";
                    ddlRepToProduct.DataSource = ds;
                    ddlRepToProduct.DataBind();
                    ddlRepToProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                    ddlRepToProduct.SelectedIndex = 0;


                }
            }
            catch
            {
            }
            GetProductGroupCodeByRoleId(ddlRepFrmPrdGroup, true, int.Parse(ddlRepFrmYear.SelectedValue), ddlRepFrmSemester.SelectedValue, ddlRepFrmProduct);
            //  GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepFrmProduct, true, ddlRepFrmYear.SelectedValue, ddlRepFrmSemester.SelectedValue);
            GetWeek(ddlRepFrmWeekNo, true);
            PopulateContestants(ddlRepFrmPaperType, true);
            PopulateEvents(ddlRepFrmEvent, false);
            PopulateSets(ddlRepFrmSetNo, true);
            try
            {
                ddlRepFrmProduct.SelectedValue = hdnPIdRP.Value;
                ddlRepToProduct.SelectedValue = hdnPIdRP.Value;
                ddlRepFrmPrdGroup.SelectedValue = hdnPgIdRP.Value;
            }
            catch
            {
            }
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlRepFrmLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);

            }
            else
            {
                //PopulateLevel(ddlRepFrmLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlRepFrmLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);

            }
            try
            {

                ddlRepFrmPaperType.SelectedValue = hdnPaperTypeRP.Value;
                DDLDocTypeRepFrm.SelectedValue = hdnDocTypeRP.Value;
                ddlRepFrmLevel.SelectedValue = hdnLevelRP.Value;
                ddlRepFrmWeekNo.SelectedValue = hdnWeekRP.Value;

            }
            catch
            {
            }


            GetProductGroupCodeByRoleId(ddlRepToPrdGroup, true, int.Parse(ddlRepToYear.SelectedValue), ddlRepToSemester.SelectedValue, ddlRepToProduct);

            // GetProductCodes(PGId(ddlRepToPrdGroup), ddlRepToProduct, true, ddlRepToYear.SelectedValue, ddlRepToSemester.SelectedValue);
            GetWeek(ddlRepToWeekNo, true);
            PopulateContestants(ddlRepToPaperType, true);
            PopulateEvents(ddlRepToEvent, false);
            PopulateSets(ddlRepToSetNo, true);
            try
            {
                ddlRepToPrdGroup.SelectedValue = hdnPgIdRP.Value;
            }
            catch
            {
            }
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y") //
            {
                PopulateLevelNew(ddlRepToLevel, true, PGId(ddlRepToPrdGroup), ddlRepFrmYear);

            }
            else
            {
                //PopulateLevel(ddlRepToLevel, "NA", true);//Hard Coded - Not retrieving from database
                PopulateLevel(ddlRepToLevel, ddlRepToPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepToEvent.SelectedValue, ddlRepToPrdGroup.SelectedValue, ddlRepToProduct.SelectedValue);

            }
            ddlRepToLevel.Enabled = false;

            try
            {

                ddlRepToLevel.SelectedValue = hdnLevelRP.Value;
                ddlRepToPaperType.SelectedValue = hdnPaperTypeRP.Value;
                DDlRepToDocType.SelectedValue = hdnDocTypeRP.Value;
                ddlRepToWeekNo.SelectedValue = hdnWeekRP.Value;
            }
            catch
            {
            }

        }
        catch (SqlException se)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = se.Message.ToString();
            return;
        }

    }
    string CoachPapersIDs = "";
    protected void gvTPSource_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string s = e.Row.Cells[1].Text;
                if (CoachPapersIDs.Length > 0)
                {
                    CoachPapersIDs = CoachPapersIDs + "," + s;
                }
                else
                    CoachPapersIDs = s;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnLevel.Value = ddlLevel.SelectedValue;
        if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
        {
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                GetCoachPapers("Modify");
            }
        }
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnSemester.Value = ddlSemester.SelectedValue;
        GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedValue, ddlProduct);

        if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
        {
            GetCoachPapers("Modify");
        }
    }
    protected void ddlFlrSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnSemester.Value = ddlFlrSemester.SelectedValue;
        GetProductGroupCodeByRoleId(ddlFlrProductGroup, true, int.Parse(ddlFlrYear.SelectedValue), ddlFlrSemester.SelectedValue, ddlFlrProduct);
    }
    protected void ddlRepFrmSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRepToSemester.SelectedValue = ddlRepFrmSemester.SelectedValue;

        GetProductGroupCodeByRoleId(ddlRepFrmPrdGroup, true, int.Parse(ddlRepFrmYear.SelectedValue), ddlRepFrmSemester.SelectedValue, ddlRepFrmProduct);


        GetProductGroupCodeByRoleId(ddlRepToPrdGroup, true, int.Parse(ddlRepToYear.SelectedValue), ddlRepFrmSemester.SelectedValue, ddlRepToProduct);

        if (ddlRepFrmProduct.SelectedValue != "-1")
        {
            GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepFrmProduct, true, ddlRepFrmYear.SelectedValue, ddlRepFrmSemester.SelectedValue);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlRepFrmLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
            }
            else
            {
                PopulateLevel(ddlRepFrmLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
            }
            GetProductCodes(PGId(ddlRepFrmPrdGroup), ddlRepToProduct, true, ddlRepToYear.SelectedValue, ddlRepToSemester.SelectedValue);
            if (Session["RoleId"].ToString() == "88" && hdntlead.Value != "Y")
            {
                PopulateLevelNew(ddlRepToLevel, true, PGId(ddlRepFrmPrdGroup), ddlRepFrmYear);
            }
            else
            {
                PopulateLevel(ddlRepToLevel, ddlRepFrmPrdGroup.SelectedItem.Text, true, ddlRepFrmYear.SelectedValue, ddlRepFrmEvent.SelectedValue, ddlRepFrmPrdGroup.SelectedValue, ddlRepFrmProduct.SelectedValue);
            }
            ddlRepToPrdGroup.SelectedValue = ddlRepFrmPrdGroup.SelectedValue;
            ddlRepToProduct.Enabled = false; ddlRepToLevel.Enabled = false;
        }

    }
    protected void ddlRepToSemester_SelectedIndexChanged(object sender, EventArgs e)
    {


        GetProductGroupCodeByRoleId(ddlRepToPrdGroup, true, int.Parse(ddlRepToYear.SelectedValue), ddlRepToSemester.SelectedValue, ddlRepToProduct);


    }
    private void loadPhase(DropDownList ddlObject, string year)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(year);
        if (Session["RoleId"].ToString() == "88")
        {
            string Cmdtext = "Select max(Semester) from CalSignup where MemberId=" + Session["LoginId"].ToString() + " and EventYear='" + ddlFlrYear.SelectedValue + "'";
            try
            {
                string Semester = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext).ToString();
                ddlObject.SelectedValue = Semester;
            }
            catch
            {
            }
        }
    }
    protected void DDlDDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnSearch_Click(btnSearch, e);
    }

    protected void ddlPaperTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }
    }

    protected void ddlDocTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnDocType.Value = ddlDocTypeFilter.SelectedValue;
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }
    }

    protected void ddlWeekFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        // ddlWeekFilter.SelectedIndex = Convert.ToInt32(ddlWeekFilter.SelectedValue);
        hdnWeekNo.Value = ddlWeekFilter.SelectedIndex.ToString();
        hdnWeekNoDw.Value = ddlWeekFilter.SelectedItem.Value;
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }
    }


    public void fillYearFiletr()
    {
        //int Year = Convert.ToInt32(DateTime.Now.Year);

        //ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)));
        //ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)));
        //ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)));

        //ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)));
        //ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)));



        int MinYear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select min(eventyear) from CoachPapers where EventId=13").ToString());
        MinYear = MinYear - 1;
        int CurYear = DateTime.Now.Year;
        int Target = CurYear + 1;

        for (int i = MinYear; i < Target; i++)
        {
            ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(i + 1) + "-" + Convert.ToString(i + 2).Substring(2, 2)), Convert.ToString(i + 1)));

        }
        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(eventyear) from EventFees where EventId=13").ToString();
        hdnEventYearDw.Value = ddlEventyearFilter.SelectedValue;
        hdnYear.Value = ddlEventyearFilter.SelectedValue;
    }

    public void loadProductGroupFilter()
    {
        try
        {
            string strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN CalSignup EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND  P.EventId=13  ";

            if ((ddlPhaseFilter.SelectedValue != "-1"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + " order by P.ProductGroupID";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductGroupFilter.DataValueField = "ProductGroupID";
            ddlProductGroupFilter.DataTextField = "Name";

            ddlProductGroupFilter.DataSource = drproductgroup;
            ddlProductGroupFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {
                ddlProductGroupFilter.Items.Insert(0, new ListItem("Select", "-1"));

                ddlProductGroupFilter.Enabled = true;
                try
                {
                    if (hdnPGId.Value.IndexOf('-') > 0)
                    {
                        ddlProductGroupFilter.SelectedValue = hdnPGId.Value.Substring(0, hdnPGId.Value.IndexOf('-'));
                    }
                    else
                    {
                        ddlProductGroupFilter.SelectedValue = hdnPGId.Value;
                    }
                    if (ddlEventyearFilter.SelectedValue == hdnEventYearDw.Value)
                    {
                    }
                    else
                    {
                        ddlProductGroupFilter.SelectedValue = hdnPGIdDW.Value;
                    }
                }
                catch
                {
                }
            }
            else
            {
                ddlProductGroupFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadProductFilter()
    {
        try
        {
            string strSql = " Select distinct P.ProductID, P.Name from Product P INNER JOIN CalSignup EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND P.EventID=13";

            if (ddlProductGroupFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and P.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if ((ddlPhaseFilter.SelectedValue != "-1"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + "order by P.ProductID";


            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());


            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductFilter.DataValueField = "ProductID";
            ddlProductFilter.DataTextField = "Name";

            ddlProductFilter.DataSource = drproductgroup;
            ddlProductFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlProductFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlProductFilter.Enabled = true;
                try
                {

                    if (hdnPId.Value.IndexOf('-') > 0)
                    {
                        ddlProductFilter.SelectedValue = hdnPId.Value.Substring(0, hdnPId.Value.IndexOf('-'));
                    }
                    else
                    {
                        ddlProductFilter.SelectedValue = hdnPId.Value;
                    }

                    if (ddlEventyearFilter.SelectedValue == hdnEventYearDw.Value)
                    {
                    }
                    else
                    {
                        ddlProductFilter.SelectedValue = hdnPIdDw.Value;

                    }
                }
                catch
                {
                }
            }
            else
            {
                ddlProductFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadLevelFilter()
    {
        try
        {
            string strSql = " select distinct LevelCode from ProdLevel where EventYear=" + ddlEventyearFilter.SelectedValue + "  and EventID=13";
            if (ddlProductGroupFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductID=" + ddlProductFilter.SelectedValue + "";
            }

            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
            ddlLevelFilter.DataValueField = "LevelCode";
            ddlLevelFilter.DataTextField = "LevelCode";
            ddlLevelFilter.DataSource = drproductgroup;
            ddlLevelFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlLevelFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlLevelFilter.Enabled = true;
                try
                {
                    ddlLevelFilter.SelectedValue = hdnLevel.Value;
                    loadWeekFilter();
                    if (ddlEventyearFilter.SelectedValue == hdnEventYearDw.Value)
                    {
                    }
                    else
                    {

                        ddlLevelFilter.SelectedValue = hdnLevelDw.Value;
                        loadWeekFilter();
                    }
                }
                catch
                {
                }
            }
            else
            {
                ddlLevelFilter.Enabled = false;
                loadWeekFilter();
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }

    }

    public void loadWeekFilter()
    {
        try
        {
            string strSql = " select distinct WeekId from Coachpapers where EventYear=" + ddlEventyearFilter.SelectedValue + "  and EventID=13";
            if (ddlProductGroupFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductID=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlLevelFilter.SelectedItem.Value != "-1")
            {
                strSql = strSql + " and Level='" + ddlLevelFilter.SelectedValue + "'";
            }
            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);


            ddlWeekFilter.DataValueField = "WeekId";
            ddlWeekFilter.DataTextField = "WeekId";
            ddlWeekFilter.DataSource = drproductgroup;
            ddlWeekFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlWeekFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlWeekFilter.Enabled = true;

                try
                {
                    ddlWeekFilter.SelectedIndex = Convert.ToInt32(hdnWeekNo.Value);
                    // LoadSectionNumber();
                    if (ddlEventyearFilter.SelectedValue == hdnEventYearDw.Value)
                    {
                    }
                    else
                    {

                        ddlWeekFilter.SelectedValue = hdnWeekNoDw.Value;

                    }
                }
                catch
                {
                }
            }
            else
            {
                ddlWeekFilter.Enabled = false;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    protected void ddlProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPGId.Value = ddlProductGroupFilter.SelectedValue;
        hdnPGIdDW.Value = ddlProductGroupFilter.SelectedValue;
        loadProductFilter();
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }



    }

    protected void ddlProductFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPId.Value = ddlProductFilter.SelectedValue;
        hdnPIdDw.Value = ddlProductFilter.SelectedValue;
        loadLevelFilter();

        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }

    }

    protected void btnClearFilter_Click(object sender, EventArgs e)
    {

        //ddlProductGroupFilter.SelectedValue = "-1";
        //ddlProductFilter.SelectedValue = "-1";
        //ddlLevelFilter.SelectedValue = "-1";

        //ddlWeekFilter.SelectedValue = "-1";
        ddlDocTypeFilter.SelectedValue = "-1";
        ddlPaperTypeFilter.SelectedValue = "-1";
        // ddlPhaseFilter.SelectedValue = "-1";
        loadPhase(ddlPhaseFilter, ddlEventyearFilter.SelectedValue);

        fillYearFiletr();
        loadProductGroupFilter();
        loadProductFilter();

        loadLevelFilter();
        loadWeekFilter();

        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }


    }

    protected void ddlLevelFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnLevel.Value = ddlLevelFilter.SelectedValue;
        hdnLevelDw.Value = ddlLevelFilter.SelectedValue;
        loadWeekFilter();
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {
            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }

    }

    protected void ddlPhaseFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnSemesterDw.Value = ddlPhaseFilter.SelectedValue;
        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadWeekFilter();
        if (hdnpaperType.Value == ScreenChoice.DownloadPapers.ToString())
        {

            FilterDownloadGrid();
        }
        else
        {
            GetCoachPapers("Modify");
        }


    }

    public void FilterDownloadGrid()
    {
        try
        {

            m_objETP.ProductId = Convert.ToInt32(ddlProductFilter.SelectedValue);
            m_objETP.ProductGroupId = Convert.ToInt32(ddlProductGroupFilter.SelectedValue);
            try
            {
                m_objETP.WeekId = int.Parse(ddlWeekFilter.SelectedValue);
            }
            catch
            {
                m_objETP.WeekId = 0;
            }
            m_objETP.Sections = int.Parse(DDLSetNoFilter.SelectedValue);
            m_objETP.RoleId = Int32.Parse(Session["RoleId"].ToString());
            m_objETP.MemberId = Int32.Parse(Session["LoginId"].ToString());
            m_objETP.Semester = ddlPhaseFilter.SelectedValue;
            m_objETP.ContestYear = ddlEventyearFilter.SelectedValue;

            if (ddlDocTypeFilter.SelectedValue != "-1")
            {

                m_objETP.DocType = ddlDocTypeFilter.SelectedValue;
            }

            //  m_objETP.Level = ddlFlrLevel.SelectedItem.Text;

            if (ddlPaperTypeFilter.SelectedValue != "-1")
            {
                m_objETP.PaperType = ddlPaperTypeFilter.SelectedValue; //Modified 27/07/2013
            }

            if (ddlLevelFilter.SelectedValue != "-1" && ddlLevelFilter.SelectedValue != "")
            {
                //if (m_objETP.ProductGroupId == 42)
                //{
                //    m_objETP.Level = "NA";
                //}
                //else
                //{
                m_objETP.Level = ddlLevelFilter.SelectedItem.Text;
                //}
            }
            else
            {
                //if (m_objETP.ProductGroupId == 42)
                //{
                //    m_objETP.Level = "NA";
                //}
                //else
                //{
                m_objETP.Level = string.Empty;// = ddlFlrLevel.SelectedItem.Text;
                //}
            }

            m_objETP.Description = string.Empty;// tbxFlrDescription.Text;
            m_objETP.TestFileName = string.Empty;// tbxFlrTestFileName.Text;

            GetTestPapers(m_objETP);

        }
        catch (Exception ex)
        {

        }
    }
    public void LoadDownloadCoachpapers()
    {
        try
        {
            m_objETP.ProductId = PGId(ddlFlrProduct);
            m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
            m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedValue);
            m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
            m_objETP.RoleId = Int32.Parse(Session["RoleId"].ToString());
            m_objETP.MemberId = Int32.Parse(Session["LoginId"].ToString());
            m_objETP.Semester = ddlFlrSemester.SelectedValue;
            m_objETP.ContestYear = ddlFlrYear.SelectedValue;

            if (DDlDDocType.SelectedValue != "0")
            {
                m_objETP.DocType = DDlDDocType.SelectedValue;
            }

            //  m_objETP.Level = ddlFlrLevel.SelectedItem.Text;

            if (ddlFlrNoOfContestants.SelectedValue != "-1")
            {
                m_objETP.PaperType = ddlFlrNoOfContestants.SelectedValue; //Modified 27/07/2013
            }
            if (ddlFlrLevel.SelectedValue != "-1" && ddlFlrLevel.SelectedValue != "")
            {
                //if (m_objETP.ProductGroupId == 42)
                //{
                //    m_objETP.Level = "NA";
                //}
                //else
                //{
                m_objETP.Level = ddlFlrLevel.SelectedItem.Text;
                //}
            }
            else
            {
                //if (m_objETP.ProductGroupId == 42)
                //{
                //    m_objETP.Level = "NA";
                //}
                //else
                //{
                m_objETP.Level = string.Empty;// = ddlFlrLevel.SelectedItem.Text;
                //}
            }
            if (Session["RoleId"].ToString() == "88" & hdntlead.Value != "Y" & ddlFlrLevel.SelectedValue == "-1")
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
            }
            m_objETP.Description = string.Empty;// tbxFlrDescription.Text;
            m_objETP.TestFileName = string.Empty;// tbxFlrTestFileName.Text;

            GetTestPapers(m_objETP);

        }
        catch (Exception ex)
        {

        }
    }

    protected void DDLSetNoFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        FilterDownloadGrid();
    }

    protected void ddlEventyearFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnYear.Value = ddlEventyearFilter.SelectedValue;
        // hdnEventYearDw.Value = ddlEventyearFilter.SelectedValue;
        loadPhase(ddlPhaseFilter, ddlEventyearFilter.SelectedValue);
        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadWeekFilter();
        FilterDownloadGrid();
    }

    public int ValidateUploadCoachPapers()
    {
        int Retval = 1;
        var eventYear = ddlContestYear.SelectedValue;
        var Semester = ddlSemester.SelectedValue;
        var ProductId = ddlProduct.SelectedValue;
        var ProductGrpId = ddlProductGroup.SelectedValue;
        var Level = ddlLevel.SelectedValue;
        var PaperType = ddlNoOfContestants.SelectedValue;
        var DocType = ddlDocType.SelectedValue;
        var WeekId = ddlWeek.SelectedValue;
        var Section = ddlSections.SelectedValue;
        var fileName = FileUpLoad1.FileName;
        var msg = "";

        if (eventYear == "-1" || eventYear == "")
        {
            msg = "Please select Event Year";
            Retval = -1;

        }
        else if (Semester == "-1" || Semester == "")
        {
            msg = "Please select Semester";

        }
        else if (ProductGrpId == "-1" || ProductGrpId == "")
        {
            msg = "Please select Product Group";
            Retval = -1;
        }
        else if (ProductId == "-1" || ProductId == "")
        {
            msg = "Please select Product";
            Retval = -1;
        }
        else if (Level == "-1" || Level == "")
        {
            msg = "Please select Level";
            Retval = -1;
        }
        else if (PaperType == "-1" || PaperType == "")
        {
            msg = "Please select Paper Type";
            Retval = -1;
        }
        else if (DocType == "-1" || DocType == "")
        {
            msg = "Please select Doc Type";
            Retval = -1;
        }
        else if (WeekId == "-1" || WeekId == "")
        {
            msg = "Please select Week#";
            Retval = -1;

        }
        else if (Section == "-1" || Section == "")
        {
            msg = "Please select Section";

            Retval = -1;
        }
        else if (fileName == "" && hdnpaperType.Value != ScreenChoice.ModifyPapers.ToString())
        {
            msg = "Please choose File";
            Retval = -1;
        }
        if (msg != "")
        {
            if (hdnpaperType.Value == ScreenChoice.ModifyPapers.ToString())
            {
                lblMerror.Text = msg;
            }
            else
            {
                lblMessage.Text = msg;
            }
        }
        return Retval;
    }

    protected void Tab1_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        Tab4.CssClass = "Initial";
        Tab5.CssClass = "Initial";
        Tab6.CssClass = "Initial";
        dvTestSectionPanel.Visible = false;
        dvSearchDownloadPapers.Visible = false;
        pnlReplicate.Visible = false;

        gvModify.Visible = false;
        hdnpaperType.Value = "UploadPapers";


        // MainView.ActiveViewIndex = 0;
        if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
        {
            Label8.Text = "Upload Papers";

            LoadUploadPanel();

            try
            {
                //GetContextYear(ddlContestYear, true); commented and moved above on Oct 28 2014.

                //ddlContestYear.SelectedIndex = 1;

                //if (ddlEventyearFilter.Items.Count > 0 && ddlEventyearFilter.SelectedValue != "0")
                //{
                //    ddlContestYear.SelectedValue = ddlEventyearFilter.SelectedValue;
                //}

                //if (ddlPhaseFilter.Items.Count > 0 && ddlPhaseFilter.SelectedValue != "0")
                //{
                //    ddlSemester.SelectedValue = ddlPhaseFilter.SelectedValue;
                //    GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedValue, ddlProduct);
                //}
                if (ddlProductGroupFilter.Items.Count > 0 && ddlProductGroupFilter.SelectedValue != "-1")
                {
                    GetProductGroupCodeByRoleId(ddlProductGroup, true, int.Parse(ddlContestYear.SelectedValue), ddlSemester.SelectedValue, ddlProduct);

                    string PgId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupId from Productgroup where ProductgroupId=" + ddlProductGroupFilter.SelectedValue + "").ToString();
                    string pgCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupcode from Productgroup where ProductgroupId=" + ddlProductGroupFilter.SelectedValue + "").ToString();
                    string selValue = PgId + "-" + pgCode;
                    ddlProductGroup.SelectedValue = selValue;
                    GetProductCodes(Convert.ToInt32(ddlProductGroupFilter.SelectedValue), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
                }
                if (ddlProductFilter.Items.Count > 0 && ddlProductFilter.SelectedValue != "-1")
                {
                    string PgId = "-1";
                    string pgCode = "-1";
                    string pCode = "-1";
                    if (ddlProductGroupFilter.SelectedValue == "-1")
                    {
                        PgId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductFilter.SelectedValue + "").ToString();
                        pgCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupcode from Product where ProductId=" + ddlProductFilter.SelectedValue + "").ToString();

                        GetProductCodes(Convert.ToInt32(PgId), ddlProduct, true, ddlContestYear.SelectedValue, ddlSemester.SelectedValue);
                        string selValue = PgId + "-" + pgCode;
                        ddlProductGroup.SelectedValue = selValue;
                    }
                    pCode = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select Productcode from Product where ProductId=" + ddlProductFilter.SelectedValue + "").ToString();

                    string selPrdVal = ddlProductFilter.SelectedValue + "-" + pCode;
                    ddlProduct.SelectedValue = selPrdVal;
                    PopulateLevel(ddlLevel, ddlProductGroup.SelectedItem.Text, true, ddlContestYear.SelectedValue, ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue);
                }
                if (ddlLevelFilter.Items.Count > 0 && ddlLevelFilter.SelectedValue != "-1")
                {
                    try
                    {
                        ddlLevel.SelectedValue = ddlLevelFilter.SelectedValue;
                        PopulateContestants(ddlNoOfContestants, true);
                    }
                    catch
                    {

                    }
                }
                if (ddlPaperTypeFilter.Items.Count > 0 && ddlPaperTypeFilter.SelectedValue != "-1")
                {
                    ddlNoOfContestants.SelectedValue = ddlPaperTypeFilter.SelectedValue;
                }
                if (ddlDocTypeFilter.Items.Count > 0 && ddlDocTypeFilter.SelectedValue != "0")
                {
                    ddlDocType.SelectedValue = ddlDocTypeFilter.SelectedValue;
                    GetWeek(ddlWeek, true);
                }
                if (ddlWeekFilter.Items.Count > 0 && ddlWeekFilter.SelectedValue != "-1")
                {
                    ddlWeek.SelectedValue = ddlWeekFilter.SelectedValue;
                    PopulateSets(ddlSet, true);
                    ddlSet.SelectedValue = ddlWeek.SelectedValue;
                }
                if (DDLSetNoFilter.Items.Count > 0 && DDLSetNoFilter.SelectedValue != "-1")
                {
                    ddlSet.SelectedValue = DDLSetNoFilter.SelectedValue;
                }

                if (ddlProductGroup.SelectedValue == "-1" && (ddlProduct.SelectedValue == "-1" || ddlProduct.Items.Count == 0))
                {
                    string sqltext = "    select distinct CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] AS[IDandCode], p.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlContestYear.SelectedValue + " and cs.Accepted = 'Y'";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqltext);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        ddlProduct.DataSource = ds;
                        ddlProduct.DataBind();
                        ddlProduct.DataTextField = "Name";
                        ddlProduct.DataValueField = "IDandCode";
                        ddlProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                        ddlProduct.SelectedIndex = 0;
                    }
                }
            }
            catch
            {
            }
        }
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";

        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
        Tab4.CssClass = "Initial";
        Tab5.CssClass = "Initial";
        Tab6.CssClass = "Initial";
        dvTestSectionPanel.Visible = false;
        hdnpaperType.Value = "DownloadPapers";
        pnlReplicate.Visible = false;
        gvModify.Visible = false;
        spnDonloadPaper.InnerText = "Download Papers";
        // LoadSearchAndDownloadPanels(true);

        if (Session["RoleId"].ToString() != "88")
        {
            LoadDownloadCoachpapers();
            Panel1.Visible = false;
            PanelAdd.Visible = false;
            PanelModify.Visible = false;
            ModifyGrid.Visible = false;
            gvModify.Visible = false;
            Panel2.Visible = false;
            Panel3.Visible = false;
            pnlReplicate.Visible = false;
            Panel2.Visible = false;

            dvSearchDownloadPapers.Visible = true;
            fillYearFiletr();
            loadProductGroupFilter();
            loadProductFilter();
            loadLevelFilter();
            loadWeekFilter();
            DDLSetNoFilter.Items.Clear();
            PopulateSets(DDLSetNoFilter, true);
            ddlPaperTypeFilter.SelectedValue = "-1";
            ddlDocTypeFilter.SelectedValue = hdnDocType.Value;
            FilterDownloadGrid();
            Panel3.Visible = true;
            gvTestPapers.Visible = true;
            //  ddlDocTypeFilter.SelectedValue = "Q";

        }
        else
        {
            LoadSearchAndDownloadPanels(true);
        }

        // MainView.ActiveViewIndex = 1;
    }

    protected void Tab3_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        Tab4.CssClass = "Initial";
        Tab5.CssClass = "Initial";
        Tab6.CssClass = "Initial";
        dvTestSectionPanel.Visible = false;
        dvSearchDownloadPapers.Visible = true;
        hdnpaperType.Value = "ModifyPapers";
        pnlReplicate.Visible = false;
        gvModify.Visible = true;
        spnDonloadPaper.InnerText = "Search Coach Papers";
        if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
        {
            // ddlPaperTypeFilter.SelectedValue = "HW";
            ddlDocTypeFilter.SelectedValue = "-1";
            Label8.Text = "Modify Papers";
            fillYearFiletr();
            loadProductGroupFilter();
            loadProductFilter();
            loadLevelFilter();
            loadWeekFilter();
            DDLSetNoFilter.Items.Clear();

            PopulateSets(DDLSetNoFilter, true);
            ddlSections.SelectedValue = hdnSectionR.Value;
            ddlDocType.SelectedValue = hdnDocType.Value;
            LoadModifyPanel();



        }
        // MainView.ActiveViewIndex = 2;

    }
    protected void Tab4_Click(object sender, EventArgs e)
    {
        try
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Clicked";
            Tab5.CssClass = "Initial";
            Tab6.CssClass = "Initial";
            dvTestSectionPanel.Visible = false;
            dvSearchDownloadPapers.Visible = false;
            hdnpaperType.Value = "ReplicatePapers";
            pnlReplicate.Visible = false;
            gvModify.Visible = false;

            // MainView.ActiveViewIndex = 2;
            if ((Session["RoleId"].ToString() == "89") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "96") || (Session["RoleId"].ToString() == "30"))
            {
                if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "96"))
                {
                    ddlRepToWeekNo.Enabled = true;
                    ddlRepToSetNo.Enabled = true;
                }
                else
                {
                    ddlRepToWeekNo.Enabled = false;
                    ddlRepToSetNo.Enabled = false;
                }
                Label8.Text = "Replicate Papers";
                pnlReplicate.Visible = true;
                LoadReplicatePanel();
                ResetReplicatePapers(false);
                try
                {
                    hdnPaperTypeRP.Value = ddlRepFrmPaperType.SelectedValue;
                    hdnDocTypeRP.Value = DDLDocTypeRepFrm.SelectedValue;
                    string sqltext = "    select distinct CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] AS[IDandCode], p.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlContestYear.SelectedValue + " and cs.Accepted = 'Y'";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqltext);
                    if (null != ds && ds.Tables.Count > 0)
                    {
                        ddlRepFrmProduct.DataTextField = "Name";
                        ddlRepFrmProduct.DataValueField = "IDandCode";
                        ddlRepFrmProduct.DataSource = ds;
                        ddlRepFrmProduct.DataBind();
                        ddlRepFrmProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                        ddlRepFrmProduct.SelectedIndex = 0;

                        ddlRepToProduct.DataTextField = "Name";
                        ddlRepToProduct.DataValueField = "IDandCode";
                        ddlRepToProduct.DataSource = ds;
                        ddlRepToProduct.DataBind();
                        ddlRepToProduct.Items.Insert(0, new ListItem("[Select]", "-1"));
                        ddlRepToProduct.SelectedIndex = 0;


                    }
                }
                catch
                {
                }
            }
        }
        catch
        {
        }
    }


    protected void Tab5_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        Tab4.CssClass = "Initial";
        Tab5.CssClass = "Clicked";
        Tab6.CssClass = "Initial";

        dvSearchDownloadPapers.Visible = false;
        hdnpaperType.Value = "TestSections";
        pnlReplicate.Visible = false;

        spnTestTitle.InnerText = "Test Sections Setup";

        gvModify.Visible = false;
        Panel1.Visible = false;
        PanelAdd.Visible = false;
        PanelModify.Visible = false;
        ModifyGrid.Visible = false;
        gvModify.Visible = false;
        Panel2.Visible = false;
        Panel3.Visible = false;
        pnlReplicate.Visible = false;
        Panel2.Visible = false;
        dvTestSectionPanel.Visible = true;
        dvTestAnswerKeySubmit.Visible = false;
        dvTestAnswerKey.Visible = false;
        dvTestSectionSubmit.Visible = true;
        dvTestSectionSetup.Visible = true;

        hdnPGIDTM.Value = ddlProductGroupTM.SelectedValue;
        hdnPIdTM.Value = ddlProductTM.SelectedValue;
        hdnLevelTM.Value = ddlLevelTM.SelectedValue;
        hdnWeekNoTM.Value = ddlWeekIDTM.SelectedItem.Value;
        hdnSectionTM.Value = ddlSectionsTM.SelectedValue;

        LoadTestSectionsTab();
        //try
        //{
        //    if (hdnPGIDTM.Value != "")
        //    {
        //        ddlProductGroupTM.SelectedValue = hdnPGIDTM.Value;
        //    }
        //    if (hdnPIdTM.Value != "")
        //    {
        //        ddlProductTM.SelectedValue = hdnPIdTM.Value;
        //        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        //    }
        //    if (hdnLevelTM.Value != "")
        //    {
        //        ddlLevelTM.SelectedValue = hdnLevelTM.Value;
        //        PopulateWeekNo();
        //    }
        //    if (hdnWeekNoTM.Value != "")
        //    {
        //        ddlWeekIDTM.SelectedValue = hdnWeekNoTM.Value;
        //        LoadSectionsTM();
        //    }
        //    if (hdnSectionTM.Value != "")
        //    {
        //        ddlSectionsTM.SelectedValue = hdnSectionTM.Value;
        //    }
        //}
        //catch
        //{
        //}


    }
    protected void Tab6_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        Tab4.CssClass = "Initial";
        Tab5.CssClass = "Initial";
        Tab6.CssClass = "Clicked";
        dvSearchDownloadPapers.Visible = false;
        hdnpaperType.Value = "TestAnswerKey";
        pnlReplicate.Visible = false;

        spnTestTitle.InnerText = "Answer Key";

        gvModify.Visible = false;
        Panel1.Visible = false;
        PanelAdd.Visible = false;
        PanelModify.Visible = false;
        ModifyGrid.Visible = false;
        gvModify.Visible = false;
        Panel2.Visible = false;
        Panel3.Visible = false;
        pnlReplicate.Visible = false;
        Panel2.Visible = false;
        dvTestSectionPanel.Visible = true;
        dvTestAnswerKeySubmit.Visible = true;
        dvTestAnswerKey.Visible = true;
        dvTestSectionSubmit.Visible = false;
        dvTestSectionSetup.Visible = false;

        hdnPGIDTM.Value = ddlProductGroupTM.SelectedValue;
        hdnPIdTM.Value = ddlProductTM.SelectedValue;
        hdnLevelTM.Value = ddlLevelTM.SelectedValue;
        hdnWeekNoTM.Value = ddlWeekIDTM.SelectedItem.Value;
        hdnSectionTM.Value = ddlSectionsTM.SelectedValue;

        LoadTestSectionsTab();
        //try
        //{
        //    if (hdnPGIDTM.Value != "")
        //    {
        //        ddlProductGroupTM.SelectedValue = hdnPGIDTM.Value;
        //    }
        //    if (hdnPIdTM.Value != "")
        //    {
        //        ddlProductTM.SelectedValue = hdnPIdTM.Value;
        //        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        //    }
        //    if (hdnLevelTM.Value != "")
        //    {
        //        ddlLevelTM.SelectedValue = hdnLevelTM.Value;
        //        PopulateWeekNo();
        //    }
        //    if (hdnWeekNoTM.Value != "")
        //    {
        //        ddlWeekIDTM.SelectedValue = hdnWeekNoTM.Value;
        //        LoadSectionsTM();
        //    }
        //    if (hdnSectionTM.Value != "")
        //    {
        //        ddlSectionsTM.SelectedValue = hdnSectionTM.Value;
        //    }
        //}
        //catch
        //{
        //}

    }

    protected void LoadTestSectionsTab()
    {
        LoadYear();
        loadPhase();
        LoadProductGroup();
        LoadDropdown(ddlSetNoTM, 40);
        LoadProductStuffs();
        //  LoadDropdown(ddlSectionsTM, 10);
        //ddlSectionsTM.SelectedValue = "1";


    }
    private void LoadYear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlEventYearTM.Items.Add(new ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)));
        for (int i = 0; i <= 8; i++)
        {
            ddlEventYearTM.Items.Add(new ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))));
        }

        ddlEventYearTM.SelectedValue = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    private void LoadSectionNumber()
    {
        ddlSectionNo.Items.Clear();
        for (int i = 5; i >= 0; i--)
        {
            ddlSectionNo.Items.Insert(0, new ListItem((i + 1).ToString()));
            //ddlSectionNo.Items.Insert(0, i + 1);
        }
    }

    private void LoadProductGroup()
    {
        ddlProductTM.Items.Clear();
        string strSql = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y')  WHERE PG.EventId=13  ";

        if (Session["RoleId"].ToString() == "88")
        {
            strSql += " and cs.MemberId=" + Session["LoginId"].ToString() + "";
        }
        strSql += " order by PG.ProductGroupID";
        SqlDataReader drproductgroup;
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
        ddlProductGroupTM.DataSource = drproductgroup;
        ddlProductGroupTM.DataBind();

        if (ddlProductGroupTM.Items.Count < 1)
        {
            lblErr.Text = "No Product is opened.";
        }
        else if (ddlProductGroupTM.Items.Count > 1)
        {
            ddlProductGroupTM.Items.Insert(0, new ListItem("Select", "0"));
            ddlProductGroupTM.Items[0].Selected = true;
            ddlProductGroupTM.Enabled = true;
            try
            {
                if (hdnPGId.Value.IndexOf('-') > 0)
                {
                    ddlProductGroupTM.SelectedValue = hdnPGId.Value.Substring(0, hdnPGId.Value.IndexOf('-'));
                    LoadProductID();
                }
                else
                {
                    ddlProductGroupTM.SelectedValue = hdnPGId.Value;
                    LoadProductID();
                }
            }
            catch
            {
            }
        }
        else
        {
            ddlProductGroupTM.Enabled = false;
            LoadProductID();

            LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        }
    }

    private void LoadProductID()
    {
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        if (ddlProductGroupTM.Items[0].Selected == true & ddlProductGroupTM.SelectedItem.Text == "Select Product Group")
        {
            ddlProductTM.Enabled = false;
        }
        else
        {
            string strSql;
            try
            {
                strSql = "Select distinct P.ProductID, P.Name from Product  P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y') where P.EventID=13 and P.ProductGroupID =" + ddlProductGroupTM.SelectedValue + " ";
                if (Session["RoleId"].ToString() == "88")
                {
                    strSql += " and cs.MemberId=" + Session["LoginId"].ToString() + "";
                }
                strSql += " order by P.ProductID";
                SqlDataReader drproductid;
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
                ddlProductTM.DataSource = drproductid;
                ddlProductTM.DataBind();
                if (ddlProductTM.Items.Count > 1)
                {
                    ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                    ddlProductTM.Items[0].Selected = true;
                    ddlProductTM.Enabled = true;
                    try
                    {

                        if (hdnPId.Value.IndexOf('-') > 0)
                        {
                            ddlProductTM.SelectedValue = hdnPId.Value.Substring(0, hdnPId.Value.IndexOf('-'));
                            LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedValue);
                        }
                        else
                        {
                            ddlProductTM.SelectedValue = hdnPId.Value;
                            LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedValue);
                        }
                    }
                    catch
                    {
                    }
                }
                else if (ddlProductTM.Items.Count < 1)
                {
                    ddlProductTM.Enabled = false;
                }
                else
                {
                    ddlProductTM.Enabled = false;
                    LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
                }
            }
            catch (Exception ex)
            {
                //lblErr.Text = ex.ToString;
            }
        }
    }


    protected void ddlProductGroupTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        hdnPGId.Value = ddlProductGroupTM.SelectedValue;
        ddlProductTM.Items.Clear();
        LoadProductID();
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
        dvTestAnswerKey.Visible = false;
    }

    protected void LoadDropdown(DropDownList ddlObject, int MaxLimit)
    {
        ddlObject.Items.Clear();
        for (int i = 0; i <= MaxLimit - 1; i++)
        {
            ddlObject.Items.Insert(i, Convert.ToString(i + 1));
        }

        if (ddlObject.Items.Count > 1)
        {
            ddlObject.Items.Insert(0, "Select");
        }
    }

    protected void btnAdd_Click(object sender, System.EventArgs e)
    {
        try
        {
            lblError.Text = "";
            lblErr.Text = "";
            if (ddlSectionNo.Items.Count > 1 & ddlSectionNo.SelectedValue == "0")
            {
                lblErr.Text = "Please Select SectionNo";
            }
            else if (ddlSubject.Enabled == true & ddlSubject.Items.Count > 1 & ddlSubject.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Subject";
            }
            else if (txtTimeLimit.Text == "")
            {
                lblErr.Text = "Please Select Section Time Limit";
            }
            else if (ddlQuestionType.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Question Type";
            }
            else if (ddlQuestionType.SelectedValue == "RadioButton" & ddlNoOfChoices.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Number Of Choices";
            }
            else if (txtNoOfQues.Text == "")
            {
                lblErr.Text = "Please Select # of Questions";


            }
            else if (txtQuesNoFrom.Text == "")
            {
                lblErr.Text = "Please Select Question Number From";
            }
            else if (txtQuesNoTo.Text == "")
            {
                lblErr.Text = "Please Select Question Number To";
            }
            else if (Convert.ToInt32(txtQuesNoTo.Text) < Convert.ToInt32(txtQuesNoFrom.Text))
            {
                lblErr.Text = " Question Number To must be greater than QuestionNumber From";
            }
            else if (Convert.ToInt32(txtQuesNoTo.Text) > Convert.ToInt32(txtNoOfQues.Text) | Convert.ToInt32(txtQuesNoFrom.Text) > Convert.ToInt32(txtNoOfQues.Text))
            {
                lblErr.Text = " Question Number From /Question Number To cannot be greater than the Number of Questions";
            }
            else
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select NumberOfQuestions,QuestionNumberFrom,QuestionNumberto,QuestionType From TestSetUpSections Where CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " order by NumberOfQuestions desc");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (btnAdd.Text == "Add")
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["NumberOfQuestions"]) > 0 & Convert.ToInt32(txtNoOfQues.Text) != Convert.ToInt32(ds.Tables[0].Rows[0]["NumberOfQuestions"]))
                        {
                            lblErr.Text = " Number of Questions must be the same in all rows of the same section/paper";
                            return;
                        }

                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) <= Convert.ToInt32(txtQuesNoFrom.Text) && Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) >= Convert.ToInt32(txtQuesNoTo.Text))
                            {
                                lblErr.Text = "QuestionNumbers already exists for the same section/paper";
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) == Convert.ToInt32(txtQuesNoFrom.Text) || Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) == Convert.ToInt32(txtQuesNoTo.Text))
                            {
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo already exists for the same section/paper";
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) == Convert.ToInt32(txtQuesNoTo.Text) || Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) == Convert.ToInt32(txtQuesNoFrom.Text))
                            {
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo mismatch exists for the same section/paper";
                            }
                        }
                    }
                }
            }

            if (lblErr.Text != "")
            {
                return;
            }

            string StrInsert = "";
            string StrUpdate = "";
            bool AddFlag = false;
            StrInsert = " Insert into TestSetUpSections (CoachPaperID,SectionNumber,Level,Subject,EventYear,SectionTimeLimit,NumberOfQuestions,QuestionType,NumberOfChoices,Penalty,QuestionNumberFrom,QuestionNumberTo,CreatedBy,CreateDate) Values ";
            StrInsert = StrInsert + "(" + txtTestNumber.Text + "," + ddlSectionNo.SelectedValue + "," + "'" + ddlLevel1.SelectedItem.Text + "'" + "," + (ddlSubject.Items.Count <= 0 ? "NULL" : "'" + ddlSubject.SelectedValue + "'") + "," + ddlEventYearTM.SelectedValue + "," + txtTimeLimit.Text + "," + txtNoOfQues.Text + ",'" + ddlQuestionType.SelectedValue + "'," + (ddlNoOfChoices.Enabled == true ? ddlNoOfChoices.SelectedItem.Value : "Null") + ",'" + ddlPenalty.SelectedValue + "'," + txtQuesNoFrom.Text + "," + txtQuesNoTo.Text + "," + Session["LoginID"] + ",GETDATE()" + ")";
            StrUpdate = " Update TestSetUpSections set CoachPaperID=" + txtTestNumber.Text + ",SectionNumber=" + ddlSectionNo.SelectedValue + ",Subject=" + (ddlSubject.Items.Count <= 0 ? "Null" : "'" + ddlSubject.SelectedValue + "'") + ",EventYear=" + ddlEventYearTM.SelectedValue + ",SectionTimeLimit=" + txtTimeLimit.Text + ",NumberOfQuestions=" + txtNoOfQues.Text + ",QuestionType='" + ddlQuestionType.SelectedValue + "',Penalty='" + ddlPenalty.SelectedValue + "',NumberOfChoices=" + (ddlNoOfChoices.Enabled == true ? ddlNoOfChoices.SelectedItem.Value : "Null") + " ,QuestionNumberFrom=" + txtQuesNoFrom.Text + ",QuestionNumberTo=" + txtQuesNoTo.Text + ",ModifiedBy=" + Session["LoginID"] + ",ModifyDate=GETDATE() ";
            StrUpdate = StrUpdate + " Where EventYear=" + ddlEventYearTM.SelectedValue + " and TestSetUpSectionsID=" + hdnTestSetUpSectionsId.Value + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " and Level='" + ddlLevel1.SelectedItem.Text + "'";
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " and Level='" + ddlLevel1.SelectedItem.Text + "'" + " and QuestionNumberFrom =" + txtQuesNoFrom.Text + " and QuestionNumberTo=" + txtQuesNoTo.Text)) == 0 & btnAdd.Text == "Add")
            {
                if (ddlSectionNo.Items.Count > 1 & Convert.ToInt32(ddlSectionNo.SelectedValue) > 1)
                {
                    for (int i = 1; i <= Convert.ToInt32(ddlSectionNo.SelectedValue) - 1; i++)
                    {
                        if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + i + " and Level='" + ddlLevel1.SelectedItem.Text + "'")) == 0)
                        {
                            AddFlag = true;
                        }
                    }

                    if (AddFlag == true)
                    {
                        lblErr.Text = "Please add the Sections in Sequential Order.";
                        return;
                    }
                }

                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, StrInsert) > 0)
                {
                    lblErr.Text = "Inserted Successfully";
                    LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
                }
            }
            else if (btnAdd.Text == "Update")
            {
                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, StrUpdate) > 0)
                {
                    lblErr.Text = "Updated Successfully";
                    LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
                    btnAdd.Text = "Add";
                }
            }
            else
            {
                lblErr.Text = "Data already exists.Please test the Test Sections data.";
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlEventYearTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
    }

    private void LoadGrid_TestSections(int CoachPaperId)
    {
        lblError.Text = "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" + CoachPaperId + " and EventYear=" + ddlEventYearTM.SelectedValue + "  order by SectionNumber,QuestionNumberFrom");
        if (ds.Tables[0].Rows.Count > 0)
        {
            tblDGTestSection.Visible = true;
            DGTestSection.Visible = true;
            lblTestSection.Text = "Test SetUp Sections";
            DGTestSection.DataSource = ds;
            DGTestSection.DataBind();
        }
        else
        {
            lblError.Text = "No records exists in the Test Setup Sections table.";
            tblDGTestSection.Visible = false;
            DGTestSection.Visible = false;
        }
    }



    private void FillCopyCoachPapers()
    {
        string StrWhereCndn = "";
        StrWhereCndn = StrWhereCndn + "EventYear=" + ddlEventYearTM.SelectedValue;
        if (ddlPaperTypeTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and PaperType= '" + ddlPaperTypeTM.SelectedValue + "'";
        }

        if (ddlProductGroupTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and ProductGroupID= " + ddlProductGroupTM.SelectedValue + "";
        }

        if (ddlProductTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and ProductID=" + ddlProductTM.SelectedValue + "";
        }

        if (ddlLevelTM.Enabled == true & ddlLevelTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and Level='" + ddlLevelTM.SelectedItem.Text + "'";
        }

        if (ddlSetNoTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and SetNum=" + ddlSetNoTM.SelectedValue + "";
        }

        if (ddlWeekIDTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " And WeekId = " + ddlWeekIDTM.SelectedItem.Text + "";
        }

        if (ddlSectionsTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + "  And Sections = " + ddlSectionsTM.SelectedValue + "";
        }

        if (lblPrdGrp.Text != "")
        {
            StrWhereCndn = StrWhereCndn + " and ProductGroupID in (" + lblPrdGrp.Text + ")";
        }

        if (lblPrd.Text != "")
        {
            StrWhereCndn = StrWhereCndn + " and ProductID in (" + lblPrd.Text + ")";
        }

        StrWhereCndn = StrWhereCndn + " and DocType in ('Q')";
        ddlCopyFrom.Items.Clear();
        ddlCopyFrom.Items.Insert(0, new ListItem("Select PaperID"));
        ddlCopyFrom.Enabled = false;
        ddlCopyTo.Items.Clear();
        ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
        ddlCopyTo.Enabled = false;
        DataSet dsCopyfrom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct C.CoachPaperID From CoachPapers C Where C.CoachPaperID in (Select Top 50 CoachPaperID from CoachPapers Where " + StrWhereCndn + " order by CreateDate desc)  and C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ") order by C.CoachPaperID asc ");
        if (dsCopyfrom.Tables[0].Rows.Count > 0)
        {
            ddlCopyFrom.Enabled = true;
            ddlCopyFrom.DataSource = dsCopyfrom;
            ddlCopyFrom.DataBind();
            if (ddlCopyFrom.Items.Count > 1)
            {
            }
            else if (ddlCopyFrom.Items.Count == 1)
            {
                LoadCopyToPapers();
            }
        }
    }



    private void clear()
    {
        lblErr.Text = "";
        lblError.Text = "";
        if (ddlSectionNo.Items.Count > 0)
        {
            ddlSectionNo.Enabled = true;
            ddlSectionNo.SelectedIndex = 0;
        }

        ddlQuestionType.SelectedIndex = 0;
        ddlNoOfChoices.Enabled = false;
        txtTimeLimit.Text = "";
        txtNoOfQues.Text = "";
        txtQuesNoFrom.Text = "";
        txtQuesNoTo.Text = "";
        btnAdd.Text = "Add";
        for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
        {
            DGTestSection.Items[i].BackColor = Color.White;
        }
    }

    protected void DGTestSection_Itemcommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        lblErr.Text = "";
        int TestSetUpSectionsId = Convert.ToInt32(e.Item.Cells[2].Text);
        int CoachPaperID = Convert.ToInt32(e.Item.Cells[4].Text);
        string Level = (string)e.Item.Cells[5].Text;
        int SectionNumber = Convert.ToInt32(e.Item.Cells[6].Text);
        int QuestionNumberFrom = Convert.ToInt32(e.Item.Cells[13].Text);
        int QuestionNumberTo = Convert.ToInt32(e.Item.Cells[14].Text);
        bool Flag_DelAnsKey = false;
        for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
        {
            DGTestSection.Items[i].BackColor = Color.White;
        }
        ddlSectionNo.SelectedValue = Convert.ToString(SectionNumber);
        if (e.CommandName == "Delete")
        {
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestAnswerKey Where CoachPaperID = " + CoachPaperID + " and  Level='" + Level + "' and SectionNumber =" + SectionNumber + " and QuestionNumber  between " + QuestionNumberFrom + " and " + QuestionNumberTo)) > 0)
            {
                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, "Delete From TestAnswerKey Where CoachPaperID = " + CoachPaperID + " and  Level='" + Level + "' and SectionNumber =" + SectionNumber + " and QuestionNumber  between " + QuestionNumberFrom + " and " + QuestionNumberTo) > 0)
                {
                    Flag_DelAnsKey = true;
                }
                else
                {
                    Flag_DelAnsKey = false;
                }
            }
            else
            {
                Flag_DelAnsKey = true;
            }

            if (Flag_DelAnsKey == true & SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, "Delete From TestSetUpSections Where TestSetUpSectionsId=" + TestSetUpSectionsId) > 0)
            {
                lblErr.Text = "Deleted Successfully";
                LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
            }
        }
        else if (e.CommandName == "Edit")
        {
            btnAdd.Text = "Update";
            e.Item.BackColor = Color.Gainsboro;
            hdnTestSetUpSectionsId.Value = Convert.ToString(TestSetUpSectionsId);
            LoadforUpdate(TestSetUpSectionsId);
        }
        else
        {
            e.Item.BackColor = Color.White;
        }
    }

    private void LoadforUpdate(int TestSetUpSectionsId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ("Select * from TestSetUpSections Where TestSetUpSectionsID =" + TestSetUpSectionsId));
        if ((ds.Tables[0].Rows.Count > 0))
        {
            txtTestNumber.Text = ds.Tables[0].Rows[0]["CoachPaperID"].ToString();
            ddlSectionNo.SelectedIndex = ddlSectionNo.Items.IndexOf(ddlSectionNo.Items.FindByValue(ds.Tables[0].Rows[0]["SectionNumber"].ToString()));
            ddlSectionNo.Enabled = false;
            if (!(ds.Tables[0].Rows[0]["Level"] == DBNull.Value))
            {
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(ds.Tables[0].Rows[0]["Level"].ToString()));
                ddlLevelTM.Enabled = false;
            }

            if ((ddlSubject.Enabled == true))
            {
                ddlSubject.SelectedIndex = ddlSubject.Items.IndexOf(ddlSubject.Items.FindByValue(ds.Tables[0].Rows[0]["Subject"].ToString()));
            }

            txtTimeLimit.Text = ds.Tables[0].Rows[0]["SectionTimeLimit"].ToString();
            txtNoOfQues.Text = ds.Tables[0].Rows[0]["NumberOfQuestions"].ToString();
            ddlQuestionType.SelectedIndex = ddlQuestionType.Items.IndexOf(ddlQuestionType.Items.FindByValue(ds.Tables[0].Rows[0]["Questiontype"].ToString()));
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByValue(((ds.Tables[0].Rows[0]["Penalty"] == DBNull.Value) ? "N" : ds.Tables[0].Rows[0]["Penalty"].ToString())));
            if ((ds.Tables[0].Rows[0]["QuestionType"] == "RadioButton"))
            {
                ddlPenalty.Enabled = true;
            }
            else
            {
                ddlPenalty.Enabled = false;
            }

            if ((ds.Tables[0].Rows[0]["NumberOfChoices"] == DBNull.Value))
            {
                ddlNoOfChoices.SelectedIndex = -1;
                ddlNoOfChoices.Enabled = false;
            }
            else
            {
                this.LoadDropdown(ddlNoOfChoices, 10);
                ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.IndexOf(ddlNoOfChoices.Items.FindByValue(ds.Tables[0].Rows[0]["NumberOfChoices"].ToString()));
                ddlNoOfChoices.Enabled = true;
            }

            txtQuesNoFrom.Text = ds.Tables[0].Rows[0]["QuestionNumberFrom"].ToString();
            txtQuesNoTo.Text = ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString();
        }

    }

    protected void DGCoachPapers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        int CoachPaperID = Convert.ToInt32(e.Item.Cells[3].Text);
        int sections = Convert.ToInt32(e.Item.Cells[10].Text);
        string ProductCode = (string)e.Item.Cells[5].Text;
        string ProductGroupCode = (string)e.Item.Cells[4].Text;
        if (e.CommandName == "Select")
        {
            DGTestSection.Visible = true;
            clear();
            LoadTestSectionSetUp(CoachPaperID, sections, ProductCode);
            LoadLevel(ddlLevel1, ProductGroupCode);
            if (ddlLevel1.Items.Count > 0)
            {
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText((string)e.Item.Cells[6].Text));
                ddlLevel1.Enabled = false;
            }

            LoadGrid_TestSections(CoachPaperID);
            LoadProductGroup();
            //string PGId = (Label)e.Item.FindControl("lblProductGroupID").Text;
            //ddlProductGroupTM.SelectedValue = PGId;
            //LoadProductID();
            //string PId = (Label)e.Item.FindControl("lblProductID").Text;
            //ddlProductTM.SelectedValue = PId;
            //LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
            //string Level = (string)e.Item.Cells[6].Text;
            //ddlLevelTM.SelectedValue = Level;
            //string PaperType = (string)e.Item.Cells[11].Text;
            //ddlPaperTypeTM.SelectedValue = PaperType;
            //string WeekNo = (string)e.Item.Cells[8].Text;
            //ddlWeekIDTM.SelectedValue = WeekNo;
            //string Setnumber = (string)e.Item.Cells[9].Text;
            //ddlSetNoTM.SelectedValue = Setnumber;
            //LoadLevel(ddlLevel1, ProductGroupCode);
            //ddlLevel1.SelectedValue = Level;
        }
    }

    private void LoadTestSectionSetUp(int CoachPaperID, int sections, string ProductCode)
    {
        tblTestSection.Visible = true;
        txtTestNumber.Text = Convert.ToString(CoachPaperID);
        // LoadDropdown(ddlSectionNo, sections);
        LoadSectionNumber();
        LoadSubject(ProductCode);

    }

    private void LoadSubject(string ProductCode)
    {
        ddlSubject.Enabled = true;
        ddlSubject.Items.Clear();
        if (ProductCode.Contains("SATM") | ProductCode.Contains("MB2") | ProductCode.Contains("MB3"))
        {
            ddlSubject.Items.Insert(0, new ListItem("Math", "Math"));
            ddlSubject.Enabled = false;
        }
        else if (ProductCode.Contains("SATE"))
        {
            ddlSubject.Items.Insert(0, new ListItem("Select", ""));
            ddlSubject.Items.Insert(1, new ListItem("Critical Reading", "Critical Reading"));
            ddlSubject.Items.Insert(2, new ListItem("Writing", "Writing"));
        }
        else
        {
            ddlSubject.Enabled = false;
        }
    }

    private void LoadLevel(DropDownList ddlObject, string ProductGroup)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        try
        {
            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select distinct cs.Level from ProdLevel P inner join CalSignup cs on cs.Level=p.LevelCode where cs.EventYear=" + ddlEventYearTM.SelectedValue + " and cs.ProductGroupID=" + ddlProductGroupTM.SelectedValue + " and cs.ProductID=" + ddlProductTM.SelectedValue + " and cs.EventID=13";
            if (Session["RoleId"].ToString() == "88")
            {
                cmdText += " and cs.MemberId=" + Session["LoginId"].ToString() + "";
            }

            SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
            SqlDataReader level;
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText);
            ddlObject.DataSource = level;
            ddlObject.DataValueField = "Level";
            ddlObject.DataTextField = "Level";
            ddlObject.DataBind();
            DataTable dt = new DataTable();
            dt.Load(level);
            if ((ddlObject.Items.Count > 1))
            {
                ddlObject.Items.Insert(0, "Select Level");
                try
                {
                    ddlObject.SelectedValue = hdnLevel.Value;
                    PopulateWeekNo();
                }
                catch
                {
                }
            }
            else
            {


                PopulateWeekNo();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlQuestionType_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlQuestionType.SelectedValue == "RadioButton")
        {
            ddlNoOfChoices.Enabled = true;
            LoadDropdown(ddlNoOfChoices, 10);
            ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.Count - 1;
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("Yes"));
            ddlPenalty.Enabled = true;
        }
        else
        {
            ddlNoOfChoices.Enabled = false;
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("No"));
            ddlPenalty.Enabled = false;
        }
    }

    protected void ddlCopyFrom_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            ddlCopyTo.Items.Clear();
            ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
            ddlCopyTo.Enabled = false;
            if (Convert.ToInt32(ddlCopyFrom.SelectedValue) > 0)
            {
                LoadCopyToPapers();
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadCopyToPapers()
    {
        try
        {
            if (Convert.ToInt32(ddlCopyFrom.SelectedValue) > -1)
            {
                string StrCopyTo = "";
                lblCopyErr.Text = "";
                StrCopyTo = StrCopyTo + " Select Distinct C1.CoachPaperID From CoachPapers C Inner Join CoachPapers C1 on C.EventYear =C1.EventYear and C.ProductGroupId =C1.ProductGroupId and C.ProductId=C1.ProductId and C.WeekId =C1.WeekId and C.SetNum =C1.SetNum and C.Sections =C1.Sections and C.PaperType =C1.PaperType and C.DocType =C1.DocType ";
                StrCopyTo = StrCopyTo + " and C.CoachPaperID <> C1.CoachPaperID and C.Level <> C1.Level Where C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ") and C1.CoachPaperId not in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ")";
                StrCopyTo = StrCopyTo + " and C.CoachPaperId = " + ddlCopyFrom.SelectedValue;
                DataSet dsCopyTo = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrCopyTo);
                if (dsCopyTo.Tables[0].Rows.Count > 0)
                {
                    ddlCopyTo.DataSource = dsCopyTo;
                    ddlCopyTo.DataBind();
                    ddlCopyTo.Enabled = true;
                    if (ddlCopyTo.Items.Count > 1)
                    {
                        ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
                    }
                }
                else
                {
                    ddlCopyTo.Enabled = false;
                    lblCopyErr.Text = "No eligible coach paper available to copy into.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnContinue_Click(object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('TestSections_AnsKeyCopy.aspx?ID=AnsKey&From=" + ddlCopyFrom.SelectedValue + "&To=" + ddlCopyTo.SelectedValue + "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ");
    }

    protected void ddlProductTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPId.Value = ddlProductTM.SelectedValue;
        string PgId = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductTM.SelectedValue + "").ToString();
        ddlProductGroupTM.SelectedValue = PgId;
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
        dvTestAnswerKey.Visible = false;

        hdnPGId.Value = ddlProductGroupTM.SelectedValue;
    }

    protected void btnRepTM_Click(object sender, EventArgs e)
    {
        if ((ValdiateTestSections() == "1"))
        {
            tblCopy.Visible = true;
            btnRepTM.Visible = false;
            FillCopyCoachPapers();
        }
    }

    protected void btnCancelCopy_Click(object sender, EventArgs e)
    {
        btnRepTM.Visible = true;
        tblCopy.Visible = false;
    }

    protected void ddlWeekIDTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnWeekNo.Value = ddlWeekIDTM.SelectedIndex.ToString();
        ddlSetNoTM.SelectedIndex = ddlWeekIDTM.SelectedIndex;
        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
        dvTestAnswerKey.Visible = false;
        LoadSectionsTM();
    }

    protected void ddlLevelTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnLevel.Value = ddlLevelTM.SelectedValue;
        PopulateWeekNo();

        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
        dvTestAnswerKey.Visible = false;
    }

    protected void ddlSemesterTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadProductGroup();
        tblTestSection.Visible = false;
        tblDGTestSection.Visible = false;
        dvTestAnswerKey.Visible = false;
        LoadProductStuffs();
    }

    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {
            ddlSemesterTM.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlSemesterTM.SelectedValue = objCommon.GetDefaultSemester(ddlEventYearTM.SelectedValue);
    }

    protected void PopulateWeekNo()
    {
        string CmdText = "";
        CmdText = " select distinct CP.WeekId, cp.Coachpaperid from  CoachPapers CP where CP.eventyear=" + ddlEventYearTM.SelectedValue + " and CP.ProductGroupId=" + ddlProductGroupTM.SelectedValue + " and CP.ProductId=" + ddlProductTM.SelectedValue + " and CP.level='" + ddlLevelTM.SelectedValue + "' and CP.Semester='" + ddlSemesterTM.SelectedValue + "' and DOCType='Q' order by WeekId ASC";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlWeekIDTM.DataValueField = "CoachPaperId";
            ddlWeekIDTM.DataTextField = "WeekId";
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
            ddlWeekIDTM.Items.Insert(0, new ListItem("Select", "0"));
            try
            {
                //ddlWeekIDTM.SelectedIndex = Convert.ToInt32(hdnWeekNo.Value);
                try
                {
                    ddlWeekIDTM.SelectedIndex = Convert.ToInt32(hdnWeekNo.Value);
                    LoadSectionsTM();

                }
                catch
                {

                }
            }
            catch
            {
            }
        }
        else
        {
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
        }
    }

    protected void btnClearFilterTM_Click(object sender, EventArgs e)
    {
        if (ValdiateTestSections() == "1")
        {
            LoadtestSetup();
        }
    }

    public void LoadtestSetup()
    {
        tblTestSection.Visible = true;
        tblDGTestSection.Visible = true;
        LoadTestSections();

        if (ddlProductGroupTM.SelectedValue == "41")
        {
            ddlPenalty.SelectedValue = "N";
        }
    }

    protected void LoadTestSections()
    {
        if ((ValdiateTestSections() == "1"))
        {
            DGTestSection.Visible = true;
            LoadTestSectionSetUp(Convert.ToInt32(ddlWeekIDTM.SelectedValue), Convert.ToInt32(ddlSectionsTM.SelectedValue), ddlProductTM.SelectedValue);
            LoadLevel(ddlLevel1, ddlProductGroupTM.SelectedValue);
            if (ddlLevel1.Items.Count > 0)
            {
                ddlLevel1.Enabled = false;
            }

            LoadGrid_TestSections(Convert.ToInt32(ddlWeekIDTM.SelectedValue));
            LoadLevel(ddlLevel1, ddlProductGroupTM.SelectedValue);
            ddlLevel1.SelectedValue = ddlLevelTM.SelectedValue;
        }
    }

    public string ValdiateTestSections()
    {
        string retVal = "1";
        if (ddlEventYearTM.SelectedValue == "-1")
        {
            lblValText.Text = "Please select Event Year";
            retVal = "-1";
        }
        else if (ddlSemesterTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlProductGroupTM.SelectedValue == "0" || ddlProductGroupTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProductTM.SelectedValue == "Select Product" || ddlProductTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product";
            retVal = "-1";
        }
        else if (ddlLevelTM.SelectedValue == "Select Level" || ddlLevelTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Level";
            retVal = "-1";
        }
        else if (ddlPaperTypeTM.SelectedValue == "" || ddlPaperTypeTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Papertype";
            retVal = "-1";
        }
        else if (ddlWeekIDTM.SelectedValue == "-1" || ddlWeekIDTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select WeekId";
            retVal = "-1";
        }
        else if (ddlSectionsTM.SelectedValue == "Select" || ddlSectionsTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Sections";
            retVal = "-1";
        }

        return retVal;
    }

    public string ValidateAnswerKey()
    {
        string retVal = "1";
        if (ddlEventYearTM.SelectedValue == "-1")
        {
            lblValText.Text = "Please select Event Year";
            retVal = "-1";
        }
        else if (ddlSemesterTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlProductGroupTM.SelectedValue == "0" || ddlProductGroupTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProductTM.SelectedValue == "Select Product" || ddlProductTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product";
            retVal = "-1";
        }
        else if (ddlLevelTM.SelectedValue == "Select Level" || ddlLevelTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Level";
            retVal = "-1";
        }
        else if (ddlPaperTypeTM.SelectedValue == "" || ddlPaperTypeTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Papertype";
            retVal = "-1";
        }
        else if (ddlWeekIDTM.SelectedValue == "-1" || ddlWeekIDTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select WeekId";
            retVal = "-1";
        }
        else if (ddlSectionsTM.SelectedValue == "Select" || ddlSectionsTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Sections";
            retVal = "-1";
        }

        return retVal;
    }

    private void LoadGrid_TestSections_AK(int TestSetUpSectionsId)
    {
        lblError.Text = "";
        TblTestSection_AK.Visible = true;
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" + TestSetUpSectionsId + "   order by SectionNumber");
        if (ds.Tables[0].Rows.Count > 0)
        {
            DGTestSection1.DataSource = ds;
            DGTestSection1.DataBind();
            lblAnswerKey_AK.Text = "Test SetUp Sections";
            if ((ds.Tables[0].Rows.Count == 1))
            {
                try
                {
                    Session["CoachPaperID"] = ds.Tables[0].Rows[0]["CoachPaperID"].ToString();
                    Session["SectionNumber"] = ds.Tables[0].Rows[0]["SectionNumber"].ToString();
                    Session["QuestionType"] = ds.Tables[0].Rows[0]["QuestionType"].ToString();
                    Session["Level"] = ds.Tables[0].Rows[0]["Level"].ToString();
                    if (ds.Tables[0].Rows[0]["QuestionType"].ToString() == "RadioButton")
                    {
                        Session["NoOfChoices"] = (ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString() != "" ? ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString() : "0");
                    }

                    Session["QuestionNumberFrom"] = ds.Tables[0].Rows[0]["QuestionNumberFrom"].ToString();
                    Session["QuestionNumberTo"] = ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString();
                    for (int i = 0; i <= DGTestSection1.Items.Count - 1; i++)
                    {
                        DGTestSection1.Items[i].BackColor = Color.White;
                    }

                    tblDGAnswerKey.Visible = false;
                    LoadAnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
                }
                catch (Exception ex)
                {
                }
            }
        }
        else
        {
            lblAnswerKey_AK.Text = "No records exists in the Test Setup Sections table.";
            DGTestSection1.DataSource = null;
            DGTestSection1.DataBind();
        }
    }

    protected void LoadTestSections_AK()
    {
        if ((ValidateAnswerKey() == "1"))
        {
            string CoachPaperID = ddlWeekIDTM.SelectedValue;
            LoadGrid_TestSections_AK(Convert.ToInt32(CoachPaperID));
        }
    }

    protected void DGTestSection1_Itemcommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        try
        {
            lblError.Text = "";
            Session["CoachPaperID"] = e.Item.Cells[3].Text;
            Session["SectionNumber"] = e.Item.Cells[4].Text;
            Session["Level"] = e.Item.Cells[5].Text;
            Session["QuestionType"] = e.Item.Cells[9].Text;
            if ((string)e.Item.Cells[9].Text == "RadioButton")
            {
                Session["NoOfChoices"] = (e.Item.Cells[10].Text != "" ? e.Item.Cells[10].Text : "0");
            }

            Session["QuestionNumberFrom"] = e.Item.Cells[11].Text;
            Session["QuestionNumberTo"] = e.Item.Cells[12].Text;
            for (int i = 0; i <= DGTestSection1.Items.Count - 1; i++)
            {
                DGTestSection1.Items[i].BackColor = Color.White;
            }

            if (e.CommandName == "Select")
            {
                tblDGAnswerKey.Visible = false;
                e.Item.BackColor = Color.Gainsboro;
                LoadAnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadAnswerKey(int CoachPaperID, int SectionNumber, string Level)
    {
        try
        {
            string StrSQLInsert = "";
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select Count(*) from TestAnswerKey where EventYear=" + ddlEventYearTM.SelectedValue + "and  CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"])) > 0)
            {
                LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level);
            }
            else
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumberFrom=" + Session["QuestionNumberFrom"] + " and QuestionNumberTo=" + Session["QuestionNumberTo"]);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    StrSQLInsert = StrSQLInsert + "Insert into TestAnswerKey (EventYear,CoachPaperID,SectionNumber,Level,QuestionNumber,CreatedBy,CreateDate) Values";
                    int Count = Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberTo"]) - Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberFrom"]);
                    for (int i = 0; i <= Count; i++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberFrom"]) + i <= Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberTo"]))
                        {
                            StrSQLInsert = StrSQLInsert + "(" + ddlEventYearTM.SelectedValue + "," + CoachPaperID + "," + SectionNumber + ",'" + Level + "', " + (Convert.ToInt32(Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberFrom"]) + i)) + "," + Session["LoginID"] + ",GETDATE()),";
                        }
                    }

                    StrSQLInsert = StrSQLInsert.TrimEnd(',');
                    if (Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, StrSQLInsert)) > 0)
                    {
                        LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadGrid_AnswerKey(int CoachPaperID, int SectionNumber, string Level)
    {
        try
        {
            tblDGAnswerKey.Visible = true;
            string CmdText = string.Empty;
            CmdText = "Select AnswerKeyRecID,CoachPaperID,SectionNumber,Level,QuestionNumber,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then CorrectAnswer End as CorrectAnswer_DD,Case When Correctanswer not in ('A','B','C','D','E','F','G','H','I','J') then IsNull(Correctanswer,'') end as CorrectAnswer,IsNull(DifficultyLevel,'') as DifficultyLevel,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then 'RadioButton' Else 'TextArea'end  as QuestionType,IsNull(Manual,'') as Manual from TestAnswerKey where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"] + " order by QuestionNumber";
            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0].Rows.Count > 0)
            {
                tblDGAnswerKey.Visible = true;
                dgAnswerKey.DataSource = ds;
                dgAnswerKey.DataBind();
                tblDGAnswerKey.Visible = true;
                lblAnswerKeyTitle.Text = ds.Tables[0].Rows[0]["CoachPaperID"].ToString() + " " + ddlProductTM.SelectedItem.Text + " - " + ddlLevelTM.SelectedValue + " - WeekID: " + ddlWeekIDTM.SelectedItem.Text + " Sec#: " + ds.Tables[0].Rows[0]["SectionNumber"].ToString() + "";
                foreach (DataGridItem row in dgAnswerKey.Items)
                {
                    try
                    {
                        if ((Session["RoleID"] == null))
                        {
                        }
                        else
                        {
                            if ((Session["RoleID"].ToString() == "88"))
                            {
                                DropDownList ddlCorrectAnswer;
                                ddlCorrectAnswer = (DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer");
                                ddlCorrectAnswer.Enabled = false;
                                DropDownList ddlDiffLevel;
                                ddlDiffLevel = (DropDownList)row.Cells[6].FindControl("ddlDiffLevel");
                                ddlDiffLevel.Enabled = false;
                                DropDownList ddlManual;
                                ddlManual = (DropDownList)row.Cells[6].FindControl("ddlManual");
                                ddlManual.Enabled = false;
                                btnSaveAnswers.Enabled = false;

                                btnCancelAnswerKey.Enabled = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }

                lblAnswerKey.Text = "Test Answer Key";
                if (Session["QuestionType"] == "RadioButton")
                {
                    foreach (DataGridItem row in dgAnswerKey.Items)
                    {
                        row.Cells[5].FindControl("ddlCorrectAnswer").Visible = true;
                        row.Cells[5].FindControl("txtCorrectAnswer").Visible = false;
                        DropDownList ddlChoice;
                        ddlChoice = (DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer");
                        if (Convert.ToInt32(Session["NoOfChoices"]) + 1 < ddlChoice.Items.Count)
                        {
                            for (int i = Convert.ToInt32(Session["NoOfChoices"]) + 1; i <= ddlChoice.Items.Count - 1; i++)
                            {
                                ddlChoice.Items[i].Enabled = false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridItem row in dgAnswerKey.Items)
                    {
                        row.Cells[5].FindControl("ddlCorrectAnswer").Visible = false;
                        row.Cells[5].FindControl("txtCorrectAnswer").Visible = true;
                    }
                }
            }
            else
            {
                tblDGAnswerKey.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void dgAnswerKey_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        TextBox txtTemp = (TextBox)e.Item.FindControl("ProjectDD");


    }
    private void LoadChoices(DropDownList ddlObject, int Choices)
    {
        Choices = Choices + 65;
        for (int i = 65; i <= Choices - 1; i++)
        {
            ddlObject.Items.Add(Convert.ToChar(i).ToString());
        }

        ddlObject.Items.Insert(0, "Select");
    }
    protected void dgAnswerKey_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
    {
        lblError.Text = "";
        dgAnswerKey.Visible = true;
        dgAnswerKey.CurrentPageIndex = e.NewPageIndex;
        LoadGrid_AnswerKey(Convert.ToInt32(Session["CoachPaperID"]), Convert.ToInt32(Session["SectionNumber"]), Session["Level"].ToString());
    }
    public void txtCorrectAnswer_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool txtflag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp;
                    txtTemp = (TextBox)sender;
                    txtTemp.Text = CorrectAnswer;
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlCorrectAnswer_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool ddlFlag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp = (TextBox)sender;

                    txtTemp.Text = CorrectAnswer;
                }
                else
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    System.Web.UI.WebControls.DropDownList ddlTemp;
                    ddlTemp = (DropDownList)sender;
                    LoadChoices(ddlTemp, Convert.ToInt32(Session["NoOfChoices"]));
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((CorrectAnswer == "" ? "Select" : CorrectAnswer)));
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlDiffLevel_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string DifficultyLevel = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                DifficultyLevel = dr["DifficultyLevel"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((DifficultyLevel == "" ? "Select" : dr["DifficultyLevel"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlManual_Prerender(object sender, System.EventArgs e)
    {
        try
        {
            string Manual = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                Manual = dr["Manual"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((Manual == "" ? "" : dr["Manual"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }


    protected void btnSaveAnswers_Click(object sender, System.EventArgs e)
    {
        try
        {
            string StrUpdate = "";
            string CorrectAnswer = "";
            string DiffLevel = "";
            string Manual = "";
            lblError.Text = "";
            foreach (DataGridItem row in dgAnswerKey.Items)
            {
                string s = ((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).SelectedValue;
                CorrectAnswer = (((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).Visible == true ? ((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).SelectedValue : ((TextBox)row.Cells[5].FindControl("txtCorrectAnswer") as TextBox).Text);

                DiffLevel = ((DropDownList)row.Cells[6].FindControl("ddlDiffLevel") as DropDownList).SelectedValue.Trim();
                Manual = ((DropDownList)row.Cells[6].FindControl("ddlManual") as DropDownList).SelectedValue.Trim();

                StrUpdate = StrUpdate + " Update TestanswerKey Set CorrectAnswer=" + (CorrectAnswer == "" ? "NULL" : "'" + CorrectAnswer + "'") + ",DifficultyLevel=" + (DiffLevel == "" ? "NULL" : "'" + DiffLevel + "'") + ",Manual = " + (Manual == "" ? "NULL" : "'" + Manual + "'") + ",ModifyDate=GETDATE(),ModifiedBy=" + Session["LoginID"] + " Where AnswerKeyRecID=" + row.Cells[0].Text + " and EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + row.Cells[1].Text + " and SectionNumber=" + row.Cells[2].Text + " and Level='" + row.Cells[3].Text + "' and QuestionNumber=" + row.Cells[4].Text + "";
            }
            if ((SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, StrUpdate) > 0))
            {
                lblError.Text = " Answer Keys Updated Successfully.";
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        if (ValidateAnswerKey() == "1")
        {
            LoadtestAnswerKeyManage();
        }
    }
    public void LoadtestAnswerKeyManage()
    {
        Clear();
        tblDGAnswerKey.Visible = false;
        tblDGCoachPaper.Visible = false;
        Session["CoachPaperID"] = ddlWeekIDTM.SelectedValue;
        dvTestAnswerKey.Visible = true;
        LoadTestSections_AK();
    }
    protected void btnCancelPage_Click(object sender, System.EventArgs e)
    {
        lblError.Text = "";
        tblDGAnswerKey.Visible = false;
        tblDGTestSection.Visible = false;
        tblDGCoachPaper.Visible = false;
        TblTestSection_AK.Visible = false;
    }



    private void Clear()
    {
        lblAnswerKey_AK.Text = "";
        DGTestSection1.SelectedIndex = -1;
        DGCoachPapers.SelectedIndex = -1;
        lblError.Text = "";
    }

    protected void btnManualAll_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["CoachPaperID"] != null)
            {
                SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Update TestAnswerKey SET Manual='Y' where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + Session["CoachPaperID"] + " and SectionNumber=" + Session["SectionNumber"] + "and Level='" + Session["Level"] + "'and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"]);
                lblError.Text = " Updated Successfully. Now a coach can provide score for their students";
                LoadGrid_AnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Getting problem while updating manual for all AnswerKeys";
        }
    }

    protected void ddlCorrectAnswer_PreRender1(object sender, EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool ddlFlag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp = (TextBox)sender;

                    txtTemp.Text = CorrectAnswer;
                }
                else
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    System.Web.UI.WebControls.DropDownList ddlTemp;
                    ddlTemp = (DropDownList)sender;
                    LoadChoices(ddlTemp, Convert.ToInt32(Session["NoOfChoices"]));
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((CorrectAnswer == "" ? "Select" : CorrectAnswer)));
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlDiffLevel_PreRender1(object sender, EventArgs e)
    {
        try
        {
            string DifficultyLevel = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                DifficultyLevel = dr["DifficultyLevel"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((DifficultyLevel == "" ? "Select" : dr["DifficultyLevel"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlManual_PreRender(object sender, EventArgs e)
    {
        try
        {
            string Manual = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                Manual = dr["Manual"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((Manual == "" ? "" : dr["Manual"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }


    protected void btnCancelAnswerKey_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        DGTestSection1.SelectedIndex = -1;
        tblDGAnswerKey.Visible = false;
    }
    protected void BtnCancelTestSection_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void LoadSectionsTM()
    {
        string CmdText = string.Empty;
        try
        {
            CmdText = " select distinct Sections from CoachPapers where EventYear=" + ddlEventYearTM.SelectedValue + " and WeekId =" + ddlWeekIDTM.SelectedItem.Text + " and ProductGroupId=" + ddlProductGroupTM.SelectedValue + " and ProductId=" + ddlProductTM.SelectedValue + " and Level='" + ddlLevelTM.SelectedValue + "'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlSectionsTM.DataValueField = "Sections";
                ddlSectionsTM.DataTextField = "Sections";
                ddlSectionsTM.DataSource = ds;
                ddlSectionsTM.DataBind();
                try
                {
                    ddlSectionsTM.SelectedValue = hdnSectionR.Value;
                    if (hdnpaperType.Value == "TestSections")
                    {
                        if (ValdiateTestSections() == "1")
                        {
                            //   LoadtestSetup();
                        }
                    }
                    else
                    {
                        if (ValidateAnswerKey() == "1")
                        {
                            LoadtestAnswerKeyManage();
                        }
                    }
                }
                catch
                {

                }

            }
        }
        catch
        {
        }
    }
    protected void DDLDocTypeRepFrm_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDlRepToDocType.SelectedValue = DDLDocTypeRepFrm.SelectedValue;
        hdnDocType.Value = DDLDocTypeRepFrm.SelectedValue;
    }
    protected void DDlRepToDocType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    public void LoadProductStuffs()
    {
        if ((Session["RoleId"].ToString() == "1") | (Session["RoleId"].ToString() == "2") | (Session["RoleId"].ToString() == "96") | (Session["RoleId"].ToString() == "30"))
        {
            // LoadProductGroup();
            string sqltext = "    select distinct P.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlEventYearTM.SelectedValue + " and cs.Accepted = 'Y' and cs.Semester='" + ddlSemesterTM.SelectedValue + "'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString().ToString(), CommandType.Text, sqltext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlProductTM.DataSource = ds;
                    ddlProductTM.DataBind();
                    if (ddlProductTM.Items.Count > 1)
                    {
                        ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                        ddlProductTM.Items[0].Selected = true;
                        ddlProductTM.Enabled = true;

                        try
                        {

                            if (hdnPId.Value.IndexOf('-') > 0)
                            {
                                ddlProductTM.SelectedValue = hdnPId.Value.Substring(0, hdnPId.Value.IndexOf('-'));
                                if (ddlProductGroupTM.SelectedValue == "0")
                                {
                                    string PgId = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductTM.SelectedValue + "").ToString();
                                    ddlProductGroupTM.SelectedValue = PgId;
                                    LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
                                }
                                else
                                {
                                    LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
                                }
                            }
                            else
                            {
                                ddlProductTM.SelectedValue = hdnPId.Value;

                                if (ddlProductGroupTM.SelectedValue == "0")
                                {
                                    string PgId = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductTM.SelectedValue + "").ToString();
                                    ddlProductGroupTM.SelectedValue = PgId;
                                    LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
                                }
                                else
                                {
                                    LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
                                }

                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }
        else if (Session["RoleId"].ToString() == "88" | Session["RoleId"].ToString() == "89")
        {
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select count (*) from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + "  and ProductId is not Null")) > 1)
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                int i;
                string prd = string.Empty;
                string Prdgrp = string.Empty;
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (prd.Length == 0)
                    {
                        prd = ds.Tables[0].Rows[i][1].ToString();
                    }
                    else
                    {
                        prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();
                    }

                    if (Prdgrp.Length == 0)
                    {
                        Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                    }
                    else
                    {
                        Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();
                    }
                }

                lblPrd.Text = prd;
                lblPrdGrp.Text = Prdgrp;
                LoadProductGroup();
            }
            else
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                string prd = string.Empty;
                string Prdgrp = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    prd = ds.Tables[0].Rows[0][1].ToString();
                    Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                    lblPrd.Text = prd;
                    lblPrdGrp.Text = Prdgrp;
                }

                LoadProductGroup();
            }
        }
    }
}


#endregion

