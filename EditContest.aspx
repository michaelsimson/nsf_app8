<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.EditContest" CodeFile="EditContest.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EditContest</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
		<script language="javascript">
			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('DatePicker.aspx?Ctl=' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}
		</script>
	</HEAD>
	<body>
		<form id="frmContest" method="post" runat="server">
			<table class="" width="100%" align="center">
				<tr>
					<td nowrap class="Heading" align="center" colspan="2">Update Contest
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">
						Contest&nbsp;Name:
					</td>
					<td noWrap align="left"><asp:textbox id="txtDescription" runat="server" Enabled="False" CssClass="SmallFont" Width="300pt"></asp:textbox></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Contest&nbsp;Year:
					</td>
					<td noWrap align="left"><asp:textbox id="txtContestYear" runat="server" Enabled="False" CssClass="SmallFont"></asp:textbox></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Contest&nbsp;Date:
					</td>
					<td noWrap align="left"><asp:textbox id="txtContestDate" runat="server" CssClass="SmallFont"></asp:textbox><A href="javascript:PopupPicker('txtContestDate', 200, 200);"><IMG src="images/calendar.gif" border="0">
						</A>
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Contest&nbsp;Type:
					</td>
					<td noWrap align="left"><asp:radiobuttonlist id="rblContestType" runat="server" Enabled="False" CssClass="SmallFont" RepeatDirection="Horizontal">
							<asp:ListItem Value="1">National</asp:ListItem>
							<asp:ListItem Value="2">Chapter</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">NSF Chapter:
					</td>
					<td noWrap align="left">&nbsp;
						<asp:label id="lblChapter" runat="server" CssClass="SmallFont"></asp:label></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Start Time:
					</td>
					<td><asp:dropdownlist id="ddlStartTime" runat="server" CssClass="SmallFont">
							<asp:ListItem Value="">Select Start Time</asp:ListItem>
							<asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
							<asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
							<asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							<asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							<asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							<asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							<asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							<asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							<asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							<asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							<asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							<asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							<asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							<asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							<asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							<asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							<asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							<asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							<asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							<asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							<asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							<asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							<asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							<asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							<asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							<asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							<asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							<asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							<asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							<asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							<asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							<asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							<asp:ListItem></asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">End Time:
					</td>
					<td><asp:dropdownlist id="ddlEndTime" runat="server" CssClass="SmallFont">
							<asp:ListItem Value="">Select End Time</asp:ListItem>
							<asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
							<asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
							<asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							<asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							<asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							<asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							<asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							<asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							<asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							<asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							<asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							<asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							<asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
							<asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							<asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							<asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							<asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							<asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							<asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							<asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							<asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							<asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							<asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							<asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							<asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							<asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							<asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							<asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							<asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							<asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							<asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							<asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							<asp:ListItem></asp:ListItem>
						</asp:dropdownlist></td>
					</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Building:
					</td>
					<td><asp:textbox id="txtBuilding" runat="server" CssClass="SmallFont"></asp:textbox></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Room:
					</td>
					<td><asp:textbox id="txtRoom" runat="server" CssClass="SmallFont"></asp:textbox></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Registration Deadline Date:
					</td>
					<td noWrap align="left"><asp:textbox id="txtRegistrationDeadlineDate" runat="server" CssClass="SmallFont"></asp:textbox><A href="javascript:PopupPicker('txtRegistrationDeadlineDate', 200, 200);"><IMG src="images/calendar.gif" border="0">
						</A>
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">CheckIn Time:
					</td>
					<td><asp:dropdownlist id="ddlCheckInTime" runat="server" CssClass="SmallFont">
							<asp:ListItem Value="">Select CheckIn Time</asp:ListItem>
							<asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
							<asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
							<asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
							<asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
							<asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
							<asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
							<asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
							<asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
							<asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
							<asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
							<asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
							<asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
							<asp:ListItem Value="1:00">1:00 PM</asp:ListItem>
							<asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
							<asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
							<asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							<asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							<asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							<asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							<asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							<asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							<asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							<asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							<asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							<asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							<asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							<asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							<asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							<asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							<asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							<asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							<asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							<asp:ListItem></asp:ListItem>
						</asp:dropdownlist></td>
					</tr>
				<tr>
					<td noWrap align="center" colSpan="2"><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Update Contest"></asp:button></td>
				</tr>
				<tr>
					<td noWrap align="center" colSpan="2"><asp:label id="lblError" runat="server" CssClass="SmallFont" Visible="False" ForeColor="Red"></asp:label></td>
				</tr>
			</table>
			<div>
				<asp:HyperLink id="hlinkChapterFunctions" runat="server" NavigateUrl="ChapterMain.aspx">Back to Chapter Functions</asp:HyperLink>
			</div>
		</form>
	</body>
</HTML>

 

 
 
 