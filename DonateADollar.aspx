﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="DonateADollar.aspx.cs" Inherits="DonateADollar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     
    <div align="center">
        <table id="tblDonateADollor" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;">
            <tr>
                <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                    <center>
                        <h2>Welcome To North South Foundation</h2>
                    </center>
                </td>
            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="left">
        <div>
            <span class="txt02" style="font-weight: bold; font-size: 14px;">Dear NSF Patron</span>
        </div>
        <div style="clear: both;"></div>
        <div style="margin-left: 10px;">
            <p>
                <font class="txt02">The Nort South Foundation (NSF) is celebrating its 25th Anniversary in Naperville, IL (the southwest suburbs of Chicago )
                on October 10, 2015. During the past 25 years, NSF has provided more than 13,000 scholorships in India for children who excelled academycally, but could not afford to enter college, In the US, since 1993, more than 120,000 children have had the chance to hone their academic skills by participating in our educational contests.
                </font>
            </p>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div>
        <table id="tblErrorLogin" runat="server" visible="false">
            <tr>
                <td class="ErrorFont">
                    <p id="se">
                        Login Attempt failed. Invalid Email and/or password. Please try again.
                    </p>
                </td>
            </tr>
        </table>
        <div style="clear: both;"></div>
        <table id="tblLogin" runat="server" style="width: 90%;margin-left:100px;" border="0">
            <tr>
                <td class="title04" valign="middle" align="left">Sign On
                </td>
            </tr>

            <tr>
                <td valign="top" nowrap align="right" >
                    <asp:Label ID="lbllogin" CssClass="txt01_strong" runat="server">E-Mail</asp:Label>
                </td>
                <td >
                    <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" Width="300" CssClass="text1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="Enter Login Id." ControlToValidate="txtUserId"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
                            ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId"
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                <td class="txt01">Click here if you
                <asp:HyperLink ID="Hyperlink1" runat="server" CssClass="btn_02" NavigateUrl="Forgot.aspx"> forgot your password</asp:HyperLink>
                    <div style="clear: both;">
                    </div>
                    Click here if you
                <asp:HyperLink ID="Hyperlink3" runat="server" CssClass="btn_02" NavigateUrl="~/Forgot_loginid.aspx"> forgot login ID</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td valign="top" class="txt01_strong" nowrap align="right">Password&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" Width="300" CssClass="text1"
                        TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            runat="server" ErrorMessage="Enter Password." ControlToValidate="txtPassword"></asp:RequiredFieldValidator></td>
                <td class="txt01" style="width: 689px">Click here if you
                <asp:HyperLink ID="Hyperlink2" runat="server" CssClass="btn_02" NavigateUrl="createuser.aspx"> new to NSF</asp:HyperLink>
                </td>
            </tr>

        </table>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div style="margin-left: 100px; font-family: Tahoma; font-weight: bold;">
        <span>Please type your greetings for the occasion to be shared with the audience</span>
    </div>
    <div style="clear: both;">
    </div>
    <div style="margin-left: 100px;">
        <asp:TextBox ID="txtGreetings" runat="server" TextMode="MultiLine" Width="600px" Height="75px"></asp:TextBox>
    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div align="center">
        <asp:Label runat="server" ID="lblValidateMsg" Style="color: red;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>

    <div style="width: 600px; height: 100px; margin-left: 100px; font-family: Tahoma; border: 1px solid black; border-radius: 10px;">
        <div style="margin-top: 10px; font-weight: bold; margin-left: 10px;">
            <span>Please donate $1 to support the event. To donate, please select the amount below:</span>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="margin-left: 10px;">
            <asp:RadioButton ID="RbtnNoDonate" GroupName="col" runat="server" Text="No donation right now" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 10px;">
            <span style="font-weight:bold;">Amount (100% tax deductable)</span>
        </div>
        <div style="float: left;">
            <asp:RadioButton ID="RbtnOneDollor" GroupName="col" runat="server" Text="$ 1" OnCheckedChanged="RbtnOneDollor_CheckedChanged" />
            <asp:RadioButton ID="RbtnTwoDollor" GroupName="col" runat="server" Text="$ 2" OnCheckedChanged="RbtnTwoDollor_CheckedChanged" />
            <asp:RadioButton ID="RbtnFiveDollor" GroupName="col" runat="server" Text="$ 5" OnCheckedChanged="RbtnFiveDollor_CheckedChanged" />
            <asp:RadioButton ID="RbtnTenDollor" GroupName="col" runat="server" Text="$ 10" OnCheckedChanged="RbtnTenDollor_CheckedChanged" />
            <asp:RadioButton ID="RbtnTwentyFive" GroupName="col" runat="server" Text="$ 25" OnCheckedChanged="RbtnTwentyFive_CheckedChanged" />
            <asp:RadioButton ID="RbtnOther" GroupName="col" runat="server" Text="Other" OnCheckedChanged="RbtnOther_CheckedChanged" />
            <asp:TextBox ID="TxtOther" runat="server" Width="50px"></asp:TextBox>
        </div>
           
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
  <div align="center"><center>
       <div class="g-recaptcha" id="rcaptcha" data-sitekey="6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"></div>
  <noscript>
  <div style="width: 302px;">
    <div style="width: 302px; height: 422px; position: relative;">
      <div style="width: 302px; height: 422px; position: absolute;">
        <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"
                frameborder="0" scrolling="no"
                style="width: 302px; height:422px; border-style: none;">
        </iframe>
      </div> 
</div>
      <div style="width: 300px; height: 60px; border-style: none;
                  bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                  background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
        <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                  class="g-recaptcha-response"
                  style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                         margin: 10px 25px; padding: 0px; resize: none;" rows="5" cols="210" >
        </textarea>
      </div>
    
  </div>
</noscript> <span id="spCaptcha" style="margin-left:100px;color:red" runat="server" visible="false" >You can't leave Captcha Code empty</span>
                      </center></div>
    <div style="clear: both; margin-bottom: 10px;"></div>
 
    <div align="center">
        <asp:Button ID="btnSaveProceed" Text="Save & Proceed" runat="server" OnClick="btnSaveProceed_Click" />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblSuccess" runat="server" Style="color: blue;"></asp:Label>
    </div>
      </span>
</asp:Content>
