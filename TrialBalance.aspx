﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="TrialBalance.aspx.vb" Inherits="TrialBalance"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

 <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="left">
            <asp:LinkButton ID="hlnkMainPage" OnClick="hlnkMainPage_Click"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:LinkButton>
      </td>
     </tr>
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="Heading">Trial Balance</td>
     </tr>
     
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="txt01"><asp:Label ID="LblMessage" runat="server" ></asp:Label><br />
        </td>
     </tr>
      <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="txt01">
       
           <table cellspacing="0" cellpadding="2" width="40%"  align="center" border="0" 
                style="height: 132px" >
               <tr><td align="left">Category  </td><td align="left" colspan="2">: 
                   <asp:DropDownList ID="ddlCategory" runat="server">
                       <asp:ListItem Value="0">Select Category </asp:ListItem>
                       <asp:ListItem Value="1">Income Roster</asp:ListItem>
                       <asp:ListItem Value="2">Expense Roster</asp:ListItem>
                       <asp:ListItem Value="3">Balance Sheet Roster</asp:ListItem>
                       <asp:ListItem Value="4">Functional Expenses (FE)</asp:ListItem>
                       <asp:ListItem Value="5">Income Statement (IS)</asp:ListItem>
                     
                       <%--<asp:ListItem Value="1">Functional Expenses (FE)</asp:ListItem>
                       <asp:ListItem <Value="2">Income Statement (IS)</asp:ListItem>--%>
                       
                       <asp:ListItem Value="6">Balance Sheet (BS) </asp:ListItem>
                       <asp:ListItem Value="7">Cash Flows (CF) </asp:ListItem>
                       <asp:ListItem Value="8" Enabled="false" >Notes</asp:ListItem>
                   </asp:DropDownList>
               </td> </tr> 
           <tr><td align="left">From </td><td align="left" width="60px"> : 
                   <asp:DropDownList ID="DdlFromMM" runat="server">
                   <asp:ListItem Value="1">Jan</asp:ListItem>
                   <asp:ListItem Value="2">Feb</asp:ListItem>
                   <asp:ListItem Value="3">Mar</asp:ListItem>
                   <asp:ListItem Value="4">Apr</asp:ListItem>
                   <asp:ListItem Value="5">May</asp:ListItem>
                   <asp:ListItem Value="6">Jun</asp:ListItem>
                   <asp:ListItem Value="7">Jul</asp:ListItem>
                   <asp:ListItem Value="8">Aug</asp:ListItem>
                   <asp:ListItem Value="9">Sep</asp:ListItem>
                   <asp:ListItem Value="10">Oct</asp:ListItem>
                   <asp:ListItem Value="11">Nov</asp:ListItem>
                   <asp:ListItem Value="12">Dec</asp:ListItem>
               </asp:DropDownList>
             </td> <td  align="left">
                   <asp:DropDownList ID="ddlYearFrom" Width="75px" runat="server"></asp:DropDownList>
               </td>
           </tr>
            <tr><td align="left">To </td><td align="left"> : 
              <asp:DropDownList ID="DdlToMM" runat="server">
                   <asp:ListItem Value="1">Jan</asp:ListItem>
                   <asp:ListItem Value="2">Feb</asp:ListItem>
                   <asp:ListItem Value="3">Mar</asp:ListItem>
                   <asp:ListItem Value="4">Apr</asp:ListItem>
                   <asp:ListItem Value="5">May</asp:ListItem>
                   <asp:ListItem Value="6">Jun</asp:ListItem>
                   <asp:ListItem Value="7">Jul</asp:ListItem>
                   <asp:ListItem Value="8">Aug</asp:ListItem>
                   <asp:ListItem Value="9">Sep</asp:ListItem>
                   <asp:ListItem Value="10">Oct</asp:ListItem>
                   <asp:ListItem Value="11">Nov</asp:ListItem>
                   <asp:ListItem Value="12">Dec</asp:ListItem>
               </asp:DropDownList>
               </td> <td align="left" >
                <asp:DropDownList ID="ddlYearTo" Width="75px" runat="server"></asp:DropDownList>
               </td>
           </tr>
             <tr><td align="center" colspan="3">
                 <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" runat="server" Text="Submit" />
                 &nbsp;&nbsp;&nbsp;
              <asp:Button ID="btnExport" OnClick="btnExport_Click"    runat="server" Text="Export to Excel" />
               
             </td> 
           </tr>
             <tr><td align="center" colspan="3">
                 <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
             
             </td> 
           </tr>
           </table> 
        
        </td>
     </tr>
     <tr runat="server" id="TrDetailView"  bgcolor="#FFFFFF" >
    <td  align="center"> 
    <asp:DataGrid Visible="false" ShowFooter="false" runat="server" ID="dgFE"  
							            AutoGenerateColumns="False" Width="600px" BorderWidth="1px" CellPadding="4"  AllowSorting="True">					            							            
							         
							             <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>                                        
                                        <asp:TemplateColumn Visible="false" ItemStyle-Width="80px"  HeaderText="Account"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblAccount" Text='<%# DataBinder.Eval(Container, "DataItem.AccNo") %>'></asp:Label>
                                                </ItemTemplate>
                                        </asp:TemplateColumn>                                       
                                            <asp:TemplateColumn ItemStyle-Width="150px"  ItemStyle-HorizontalAlign="Left"  HeaderText="Functional Expenses"  HeaderStyle-HorizontalAlign="Center"   HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Wrap="false" HeaderText="Program Services" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:Right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblPservices" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PSAmount"),0) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"   HeaderText="General & Administrative" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right" >
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.GAAmount"),0) %>'></asp:Label>
                                                     </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>                                           
                                           
                                        </Columns>
                                    </asp:DataGrid>
     <asp:DataGrid Visible="false" ShowFooter="false" runat="server" ID="dgIS"  
							            AutoGenerateColumns="False" Width="600px" BorderWidth="1px" CellPadding="4"  AllowSorting="True">					            							            
							         
							             <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>                                        
                                        <asp:TemplateColumn Visible="false" ItemStyle-Width="80px"  HeaderText="Account"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblAccount" Text='<%# DataBinder.Eval(Container, "DataItem.AccNo") %>'></asp:Label>
                                                </ItemTemplate>
                                        </asp:TemplateColumn>                                       
                                            <asp:TemplateColumn ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Left"   HeaderText=""  HeaderStyle-HorizontalAlign="Center"   HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Wrap="false" HeaderText="Unrestricted" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:Right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblPservices" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.URAmount"),0) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"   HeaderText="Temporarily Restricted" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right" >
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblTRAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.TRAmount"),0) %>'></asp:Label>
                                                     </div> 
                                                </ItemTemplate>                                               
                                            </asp:TemplateColumn>                                           
                                            <asp:TemplateColumn ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"   HeaderText="Permanently Restricted" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right" >
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblPRAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PRAmount"),0) %>'></asp:Label>
                                                     </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn> 
                                           
                                        </Columns>
                                    </asp:DataGrid>
    </td> </tr> </table> 
</asp:Content>

