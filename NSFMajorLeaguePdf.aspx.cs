﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
 
public partial class NSFMajorLeaguePdf: System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}

        if (!IsPostBack)
        {
           
            load();
        }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        load();

    }
    public void load()
    {
       // string YearRange = FromYear.SelectedValue + "-" + ToYear.SelectedValue;
        string ProductGroupQry = "select distinct contest from MLWinners order by contest desc";
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ProductGroupQry);
        if (dsProductGroup.Tables[0].Rows.Count > 0)
        {
            DataLists.DataSource = dsProductGroup.Tables[0];
            DataLists.DataBind();
        }
    }

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label lblyear = (Label)e.Item.FindControl("lblyear");
        Label lblContest = (Label)e.Item.FindControl("lblContest");
        Repeater rp = (Repeater)e.Item.FindControl("rp_Postings");
        string DDListItem = "select ML.contestyear, ML.contest,ML.place,C.FIRST_NAME,C.LAST_NAME from MLWinners ML left join Child c on C.ChildNumber=Ml.ChildNumber where ContestYear=" + lblyear.Text + " and Contest='" + lblContest.Text + "' order by ML.place asc";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDListItem);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            rp.DataSource = dsstate.Tables[0];
            rp.DataBind();
        }

    }
 
    protected void btnsearchChild0_Click(object sender, EventArgs e)
    {
        try
        {
            load();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=MajorLeagueWinners.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Div1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void DataLists_ItemDataBound(object sender, DataListItemEventArgs e)
    {
       
        HiddenField hdnYear = (HiddenField)e.Item.FindControl("hdn");
        DataList DataList1 = (DataList)e.Item.FindControl("DataList1");
        string DDLstate = "select distinct contestyear,contest from MLWinners where contest='" + hdnYear.Value + "' order by contestyear desc";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            DataList1.DataSource = dsstate.Tables[0];
            DataList1.DataBind();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        load();
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        load();
    }
}