<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ChkPaymentNotes.aspx.vb" Inherits="ChkPaymentNotes"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

 <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="left">
            <asp:LinkButton ID="hlnkMainPage" OnClick="hlnkMainPage_Click"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:LinkButton>
      </td>
     </tr>
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="Heading">Payment Notes</td>
     </tr>
     
     <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="txt01"><asp:Label ID="LblMessage" runat="server" ></asp:Label><br />
        </td>
     </tr>
      <tr bgcolor="#FFFFFF" >
        <td colspan="2" align="center" class="txt01">
       
           <table cellspacing="0" cellpadding="2" width="40%"  align="center" border="0" >
               <tr><td align="left">Category  </td><td align="left" colspan="2">: 
                   <asp:DropDownList ID="ddlCategory" runat="server">
                       <asp:ListItem Value="PaymentNotes">Payment Notes</asp:ListItem>
                       <asp:ListItem Selected="True" Value="CalcPaymentsNotes">CalcPayment Notes</asp:ListItem> 
                   </asp:DropDownList>
               </td> </tr> 
               <tr><td align="left">Event  </td><td align="left" colspan="2">: 
                    <asp:ListBox ID="lstEvent" runat="server" Width ="130px" Rows="5" SelectionMode="Multiple"></asp:ListBox>  
               </td> </tr> 
           <tr><td align="left">From </td><td align="left"> : 
               <asp:TextBox ID="TxtFrom" runat="server"></asp:TextBox></td> <td>
                   <asp:RequiredFieldValidator ControlToValidate="TxtFrom" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
               </td>
           </tr>
            <tr><td align="left">To </td><td align="left"> : 
               <asp:TextBox ID="txtTo" runat="server"></asp:TextBox></td> <td>
               <asp:RequiredFieldValidator ControlToValidate="txtTo" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
               </td>
           </tr>
             <tr><td align="center" colspan="2">
                 <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" runat="server" Text="Submit" />
                 &nbsp;&nbsp;&nbsp;
              <asp:Button ID="btnExport" OnClick="btnExport_Click"  Enabled="false"    runat="server" Text="Export to Excel" />
               
             </td> 
           </tr>
             <tr><td align="center" colspan="3">
                 <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
             
             </td> 
           </tr>
           </table> 
        
        </td>
     </tr>
     <tr runat="server" id="TrDetailView"  bgcolor="#FFFFFF" >
    <td  align="center"> 
    <asp:GridView ID="GVPaymentNotes" AutoGenerateColumns="false" runat="server">
        <Columns>  
            <asp:BoundField DataField ="FName" HeaderText="FName" />
            <asp:BoundField DataField ="LName" HeaderText="LName" />
            <asp:BoundField DataField ="CDate" HeaderText="CDate" DataFormatString="{0:d}"   />
            <asp:BoundField DataField ="CAmount" HeaderText="CAmount" DataFormatString="{0:c}"  />
            <asp:BoundField DataField ="DType" HeaderText="DType" />
            <asp:BoundField DataField ="PDate" HeaderText="PDate" DataFormatString="{0:d}" />
            <asp:BoundField DataField ="unique_id" HeaderText="unique_id" />
            <asp:BoundField DataField ="event_for" HeaderText="event_for" />
            <asp:BoundField DataField ="asp_session_id" HeaderText="asp_session_id" />
            <asp:BoundField DataField ="MealsAmount" HeaderText="MealsAmount" DataFormatString="{0:c}"  />
            <asp:BoundField DataField ="EventId" HeaderText="EventId" />
            <asp:BoundField DataField ="MemberId" HeaderText="MemberId" />
            <asp:BoundField DataField ="ChapterId" HeaderText="ChapterId" />
            <asp:BoundField DataField ="Fee" HeaderText="Fee"  DataFormatString="{0:c}" />
            <asp:BoundField DataField ="TotalPayment" HeaderText="TotalPayment" DataFormatString="{0:c}" />
            <asp:BoundField DataField ="EventYear" HeaderText="EventYear" />
            <asp:BoundField DataField ="PaymentNotes" HeaderText="PaymentNotes" />
            <asp:BoundField DataField ="CalcpaymentsNotes" HeaderText="CalcpaymentsNotes" />
            <asp:BoundField DataField ="Matched" HeaderText="Matched" />                 
        </Columns> 
        </asp:GridView>
    </td>
    </tr> </table> 
    </asp:Content>

