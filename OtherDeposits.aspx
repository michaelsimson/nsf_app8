﻿vol<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="OtherDeposits.aspx.cs" Inherits="OtherDeposits"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left">
  &nbsp;&nbsp;<asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/volunteerfunctions.aspx" >Back to Volunteer Functions</asp:hyperlink>&nbsp;&nbsp;&nbsp;
</div>
    <div align="center">
<table width="75%" runat="server" id="tblRevenue">
           <tr>
                   <td colspan="4" align="center" style="background-color:#88c800" class="title03">Other Deposits</td>
                </tr>
                <tr>
                    <td align="left" >
                        &nbsp;</td>
                    <td align="left" >
                    <asp:Label  CssClass="SmallFont" ID="ErrorRev" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
             <tr>
                    <td align="left">
                   
                         <asp:Label  CssClass="SmallFont" ID="Label19" runat="server" Text="Label">Bank</asp:Label>
                    </td>
                    <td  align="left">                          
                        <asp:DropDownList ID="ddlBank" width="160px"   runat="server" Height="20px" >
                        </asp:DropDownList>&nbsp;            
                    </td>
                    <td align="left">
                   
                         <asp:Label  CssClass="SmallFont" ID="Label2" runat="server" Text="Label">Year</asp:Label>
                    </td>
                    <td  align="left">                          
                        <asp:DropDownList ID="DdlYear" width="160px"   runat="server" Height="20px" >
                        </asp:DropDownList>&nbsp;            
                    </td>
                </tr> 
                   
             
                <tr>
                    <td align="left" >
                        <asp:Label ID="Label4" runat="server" CssClass="SmallFont" 
                            Text="Event"></asp:Label>
                    </td>
                    <td align="left" >
                        <asp:DropDownList ID="ddlEvent" runat="server"  Height="20px" Width="160px"></asp:DropDownList>
                    </td>
                   
                </tr>
                   
             
    <tr>
        <td align="left">
            <asp:Label  CssClass="SmallFont" ID="Label6" runat="server" Text="Chapter"></asp:Label>
       </td>
        <td align="left">
            <asp:DropDownList ID="DdlChapter" Width="160px" runat="server" Height="20px">
            </asp:DropDownList>&nbsp;

        </td>
         
    </tr>
      <tr>
                <td align="left">
                    <asp:Label  CssClass="SmallFont" ID="Label5" runat="server" Text="Deposit Date"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDepositDate" Width="160px" runat="server"></asp:TextBox>
                    </td>
                     <td align="left">
                    <asp:Label  CssClass="SmallFont" ID="Label3" runat="server" Text="Deposit Slip#"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDepositSlipNo" Width="160px" runat="server"></asp:TextBox>
                    </td>
    </tr>
    
    <tr>
        <td align="left">       
            <asp:Label  CssClass="SmallFont" ID="Label8" runat="server" Text="Amount"></asp:Label>
        </td>
        <td align="left">        
            <asp:TextBox ID="txtAmount" Width="160px" runat="server"></asp:TextBox>
           </td>
    </tr>    
   
  
    <tr>
        <td align="left">
            <asp:Label  CssClass="SmallFont" ID="Label10" runat="server" Text="Revenue Type" ></asp:Label></td>
        <td align="left">
        <asp:DropDownList ID="DdlRevType" runat="server" Height="20px" Width="160px" 
                onselectedindexchanged="DdlRevType_SelectedIndexChanged" AutoPostBack="true" >
                <asp:ListItem Selected="True" Value = "Fees">Fees</asp:ListItem>
                 <asp:ListItem  Value = "Sponsorship">Sponsorship</asp:ListItem>
                  <asp:ListItem  Value = "Sales">Sales</asp:ListItem>
                   <asp:ListItem  Value = "Small Donations">Small Donations</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label  CssClass="SmallFont" ID="Label1" runat="server" Text="SponsorShip Type" ></asp:Label>
        
        </td>
        <td align="left">
            <asp:DropDownList ID="ddlSponsorshipType" Enabled=false runat="server" Height="20px" Width="160px" >
            </asp:DropDownList>
          
        </td>
    </tr>
    <tr>
      <td align="left">
           
       
            <asp:Label  CssClass="SmallFont" ID="Label11" runat="server" Text="Sales Type"></asp:Label>
       </td>
        <td align="left">
            <asp:DropDownList ID="ddlSalesType" runat="server" Height="20px" Width="160px" Enabled="False">
            </asp:DropDownList>
             
        </td>
    </tr>
   <tr>
      <td align="left">
           
       
            <asp:Label  CssClass="SmallFont" ID="Label7" runat="server" Text="Money Received From"></asp:Label>
       </td>
        <td align="left" colspan="2"> <asp:TextBox ID="txtName" runat="server" Enabled="False"></asp:TextBox> &nbsp;&nbsp; 
                        <asp:Button ID="BtnincueredExpFind" Enabled="false"  runat="server" CausesValidation="False" 
                            OnClick="Find_Click" Text="Search" />
             
        </td>
    </tr>
   
    <tr>
      <td align="center" colspan="4"> 
          <asp:Button ID="Save" runat="server" Text="Submit" onclick="Save_Click" /> &nbsp; &nbsp; 
          <asp:Button ID="BtnClear" runat="server" Text="Clear" 
              onclick="BtnClear_Click" />
          <asp:Label ID="lblErr" ForeColor = "red" runat="server" ></asp:Label></td>
    </tr>
  
   
  
   
    <tr>
      <td align="center" colspan="4"> 
          <asp:HiddenField ID="HdnMemberID" runat="server" />
          <asp:HiddenField ID="HdnDonorType" runat="server" />
            <asp:HiddenField ID="HdnOtherDepositID" runat="server" />-
        </td>
    </tr>
  
   
  
   
    </table>
    <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF member</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
         <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Donor Type:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server" 
                    onselectedindexchanged="ddlDonorType_SelectedIndexChanged">
                    <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                    <asp:ListItem Value="Organization">Organization</asp:ListItem>
                </asp:DropDownList></td>
    	</tr>
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Organization Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>
    	
    	<tr>
    	    <td align="right">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    </td>
    	    <td  align="left">					
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>

<br />

<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns>        
         
      </asp:GridView>    
 </asp:Panel>
 </div>
	</asp:Panel>
	<asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GVOtherDeposits" OnRowCommand = "GVOtherDeposits_RowCommand" DataKeyNames="OtherDepositID" AutoGenerateColumns = "false" runat="server" RowStyle-CssClass="SmallFont">
           <Columns>
            <asp:ButtonField Text="Edit" headerText="Modify" ></asp:ButtonField>
           <asp:BoundField DataField="OtherDepositID" headerText="ID" ></asp:BoundField>
            <asp:BoundField DataField="BankCode" headerText="BankCode" ></asp:BoundField>
             <asp:BoundField DataField="EventName" headerText="Event" ></asp:BoundField>
              <asp:BoundField DataField="ChapterCode" headerText="ChapterCode" ></asp:BoundField>
               <asp:BoundField DataField="EventYear" headerText="EventYear" ></asp:BoundField>
                <asp:BoundField DataField="DepositSlipNo" headerText="DepositSlip#" ></asp:BoundField>
               <asp:BoundField DataField="DepositDate" headerText="DepositDate"  DataFormatString="{0:d}"></asp:BoundField>
               <asp:BoundField DataField="Amount" headerText="Amount" DataFormatString="{0:c}" ></asp:BoundField>
               <asp:BoundField DataField="RevenueType" headerText="RevenueType" ></asp:BoundField>
             <asp:BoundField DataField="SponsorDesc" headerText="SponsorDesc" ></asp:BoundField> 
             <asp:BoundField DataField="SalesCatDesc" headerText="SalesCatDesc" ></asp:BoundField> 
             <asp:BoundField DataField="Name" headerText="Name" ></asp:BoundField>
              <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
               <asp:BoundField DataField="CreatedBy" headerText="CreatedBy" ></asp:BoundField>
                <asp:BoundField DataField="CreatedDate" headerText="CreatedDate" DataFormatString="{0:d}" ></asp:BoundField>
           </Columns>        
         
      </asp:GridView>
</div>
</asp:Content>

