Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data

Partial Class Emaillist_NC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtSendList.Text = Session("emaillist")
        'txtSendList.Text = txtSendList.Text
        txtfrom.Text = Convert.ToString(Session("LoginEmail"))
        ' txtfrom.Text = "chitturi9@gmail.com"
        lblerr.Text = ""
    End Sub

    Protected Sub btnsendemail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsendemail.Click
        'will send email to selected emails
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim sbEmailSuccess As New StringBuilder
        Dim sbEmailFailure As New StringBuilder
        Dim mm As MailMessage

        If txtfrom.Text = "" Then
            lblerr.Text = "From email should not be empty"
        Else
            If txtSendList.Text = "" Then
                lblerr.Text = "Send email list is empty"
            Else
                If txtEmailSubject.Text = "" Then
                    lblerr.Text = "Subject line should not be blank"
                Else
                    If txtEmailBody.Text = "" Then
                        lblerr.Text = "Message Text box should not be blank"
                    Else

                        strEmailAddressList = Split(txtSendList.Text, ",")

                        Dim count As Integer = 0
                        Dim sentemailscount = 0
                        Try
                            For Each item In strEmailAddressList
                                ' If (count = 2000) Then
                                If (count = 5) Then
                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert(' " & sentemailscount & "message sent ');"), True)
                                    System.Threading.Thread.Sleep(5000)
                                    count = 0
                                End If
                                If EmailAddressCheck(item.ToString) Then
                                    mm = GetMailMessage(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), item.ToString, txtfrom.Text)
                                    If SendEmail(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), item.ToString, txtfrom.Text, mm) = True Then
                                        'If SendEmail(txtEmailSubject.Text, txtEmailBody.Text, item.ToString, txtfrom.Text) = True Then
                                        sbEmailSuccess.Append("Email Send To:" & item.ToString & "<BR>")
                                        'count += 1
                                        'sentemailscount += 1
                                    Else
                                        sbEmailFailure.Append("Email Failed To:" & item.ToString & "<BR>")
                                        count += 1
                                        sentemailscount += 1
                                    End If
                                End If
                            Next
                        Catch
                            Response.Write("Error Occured: <BR>")
                        End Try
                        lblEMailError.Text = sbEmailFailure.ToString
                        If lblEMailError.Text = "" Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert('All Emails Sent');"), True)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal smailfrom As String, ByVal email As MailMessage) As Boolean


        'Build Email Message
        'Dim email As New MailMessage
        email.From = New MailAddress(smailfrom)
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server


        Dim client As New SmtpClient()
        Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        client.Host = host

        Dim ok As Boolean = True
        Try
            client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            ' client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
            '    sendemails.IsBodyHtml = True
            '    smtp.Credentials = SmtpUser
            client.Timeout = 20000
            client.Send(email)
        Catch e As Exception
            lblerr.Text = e.Message.ToString
            ok = False
            'Return False
        End Try
        Return ok
    End Function

    Private Function GetMailMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sMailFrom As String) As MailMessage
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim mm As MailMessage

        '(1) Create the MailMessage instance
        Try
            mm = New MailMessage(sMailFrom, sMailTo)
        Catch
            Response.Write("Email Address Having Problem is " & sMailTo & " From Address:" & sMailFrom)
        End Try

        '(2) Assign the MailMessage's properties
        mm.Subject = sSubject
        mm.Body = sBody
        mm.IsBodyHtml = True

        strEmailAddressList = Split(sMailTo, ",")
        For Each item In strEmailAddressList
            'check if email address is valid
            If EmailAddressCheck(item.ToString) Then
                mm.Bcc.Add(item.ToString)
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            End If
        Next
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then
            ' Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            Dim FileName As String = System.IO.Path.GetFileName(AttachmentFile.PostedFile.FileName)
            mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, FileName))

        End If
        Return mm

    End Function

    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        'Validate email address
        Try
            Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
            If emailAddressMatch.Success Then
                EmailAddressCheck = True
            Else
                EmailAddressCheck = False
            End If
        Catch ex As Exception
            EmailAddressCheck = False
        End Try
    End Function

End Class
