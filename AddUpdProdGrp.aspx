<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdProdGrp.aspx.vb" Inherits="AddUpdProdGrp" title="Add/Update Product Group" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<asp:hyperlink id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink><br />
    
  <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004px"><tr><td align = "center"> 
     <center>
    <table cellpadding = "4" cellspacing = "0"  border="0" width="400px">
        <tr>
            <td align="center" colspan="3">
                <h2>
                ADD/UPDATE PRODUCT GROUP</h2>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
        </tr>
    <tr><td align = "left">Event Code</td><td></td><td align="left"><asp:DropDownList ID="ddlEvent" runat="server" Width="155px" Height="23px">
        <asp:ListItem Selected="True">Select Event</asp:ListItem>
    </asp:DropDownList></td>
    </tr>    
        <tr>
            <td align="left">
                Group Code</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtGrpCode" runat="server" Width="150px" Height="18px"></asp:TextBox>
                 <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Red" Text=""></asp:Label>
                </td>
        </tr>
        <tr>
            <td align="left">
                Group&nbsp;
            Name</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtGrpName" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="panel3"   Visible="False">

    <center> <asp:Label ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>    </center>
     <br />
     <br />

    <asp:GridView ID="GridView1" runat="server" DataKeyNames="ProductGroupID" AutoGenerateColumns="False"  OnRowCommand="GridView1_RowCommand" >
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="ProductGroupID"  HeaderText="ProductGroupId" />
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"/>
            <asp:BoundField DataField="Name" HeaderText="Product Group Name"  ItemStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="EventId" HeaderText="EventId" />
            <asp:BoundField DataField="EventCode" HeaderText="EventCode" />
            <asp:BoundField DataField="CreateDate" HeaderText="Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
            <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By"/>           
        </Columns>
    
    </asp:GridView>


</asp:Panel>
    </center>
    </td></tr></table>
</asp:Content>
