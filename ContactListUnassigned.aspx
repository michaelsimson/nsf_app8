<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContactListUnassigned.aspx.vb" Inherits="ContactListUnassigned" title="Volunteers Unassigned Contact List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<asp:SqlDataSource ID="ChapInZonesDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                            SelectCommand="usp_GetChapterWithinZone" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="ChaWithinClustersDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                            SelectCommand="usp_GetChapterWithinCluster" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="ChapersDSet" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                            SelectCommand="usp_GetChapterAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
	<table border=0  width=100%>
   			<tr><td>
						<b><asp:Label  ForeColor=green ID="lblLoginName" runat=server></asp:Label></b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<b><asp:Label forecolor=green ID="lblLoginRole" runat=server></asp:Label></b>												
						
					</td>					
				</tr>	
				<tr><td><asp:Label ID="lblSessionTimeout" Visible=false ForeColor=red runat=server></asp:Label></td></tr>
				<tr><td>			
				<table  border=1 runat="server" id="tblAssignRoles" bgcolor=silver width =30%>				
				<TBODY>					
				<tr><td  colspan="2" bgcolor=white nowrap >
						<b>1. Select Category and Task Description to get Contact List (Volunteers Unassigned)</b>						
					</td>
				</tr>	
						
				<tr>				
						<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px; height: 25px;">
                            &nbsp; Category:
						</td>
						<td style="width: 500px; height: 25px;">
						<asp:dropdownlist id="ddlCategory" tabIndex="7" autopostback=true OnSelectedIndexChanged="ddlCategory_selectedIndexChanged"  runat="server" CssClass="SmallFont" Width="160px">
						<asp:ListItem Text="Select a Category" Value="0" Selected=True></asp:ListItem>
						<asp:ListItem Text="Finals" Value="1"></asp:ListItem>
						<asp:ListItem Text="National" Value="2"></asp:ListItem>
						<asp:ListItem Text="Chapter" Value="3"></asp:ListItem>
						<asp:ListItem Text="India" Value="4"></asp:ListItem>
						</asp:dropdownlist>							
						</td>
				</tr>
				
			
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">Task Description:
						</td>
						<td style="width: 347px">
						   <asp:ListBox id="ddlTaskDesc" SelectionMode="Multiple" tabIndex="7"   DataTextField="TaskDescription" DataValueField="TaskDescription" runat="server" CssClass="SmallFont" Width="250px">
						   </asp:ListBox><br />(Hold Ctrl key to select muplitple roles)
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">Zone:
						</td>
						<td style="width: 347px">
						    <asp:SqlDataSource ID="ZoneDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                            SelectCommand="usp_Getzones" SelectCommandType="StoredProcedure">
                        </asp:SqlDataSource>
						 <asp:DropDownList ID="DdlZonalCoordinator" runat="server" Width="160px" AppendDataBoundItems="True" OnSelectedIndexChanged="DdlZonalCoordinator_SelectedIndexChanged" AutoPostBack="True">
                        <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                    </asp:DropDownList>
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">Cluster:
						</td>
						<td style="width: 347px">
						    <asp:DropDownList ID="DdlCluster" runat="server"  Width="162px" OnSelectedIndexChanged="Cluster_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True">
                             <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                        </asp:DropDownList>
                         <asp:SqlDataSource ID="ClusterDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                            SelectCommand="usp_Getclusters" SelectCommandType="StoredProcedure">
                        </asp:SqlDataSource>
                         
						</td>
				</tr>
				<tr><td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">Chapter:
						</td>
						<td style="width: 347px">
						   <asp:DropDownList ID="ddlChapter" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem>Select Chapter</asp:ListItem>
                                    </asp:DropDownList><asp:ObjectDataSource ID="ChapterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                                    </asp:ObjectDataSource>
						</td>
				</tr>
				<tr>
				<td style="height: 4px; width: 103px;">
				
				</td><td style="width: 347px; height: 4px;">
                    <asp:Button ID="Find" runat="server" Text="Find" /></td>				
				</tr>										
			</table>			
			</td>
			</tr>
			<tr><td>				
				<table id="TblSearchString" visible=false runat="server" width=100%>
				<tr>
					<td >
						<asp:label id="txtSearchCriteria"  runat="server" Visible="False" ForeColor="blue">
						</asp:label>							
					</td>
				</tr>				
			</table>			
			<table id="tblMessage"  visible=false runat="server" width=65%>
				<tr>
					<td>					  	
						<asp:Label id="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>						
						<asp:Label id="lblMsg" runat="server" Visible="False" ForeColor="blue"></asp:Label>						<br />
						<p></p>
					</td>
				</tr>					       		
			</table>	
			</td></tr>		
			<tr><td>
			
			<table id="tblVolRoleResults" visible=false runat="server" width="100%">
				<tr>
					<td align="left" >
					<strong><asp:Label ID="lblGrdVolResultsHdg" Visible=true runat=server></asp:Label><br />
					<asp:Label id="lblUpdateError" runat="server" Visible="False" ForeColor="Red"></asp:Label>	
					</strong>						
					</td>
				</tr>
				<tr><td align=right>
				<asp:button id="btnExport1" runat="server" Text="Export to Excel"></asp:button>
				
				<asp:button id="btnEmailExport" runat="server" Text="Export Emails"></asp:button>
				</td></tr>
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" style="height: 103px" >
				   <asp:Panel runat=server>
	               <asp:DataGrid ID="grdVolResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" 
					Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" AllowPaging=true PageSize=10  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
					<ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>  
					<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor="Gainsboro" ></FooterStyle>
					<%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                    <Columns>
                        <asp:BoundColumn DataField="memberID" HeaderText="Member Id" readonly=true  Visible="true" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="name"  headerText="Member Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="Email" headerText="Email" ReadOnly=true Visible="true" ></asp:BoundColumn>
                        <asp:BoundColumn  ItemStyle-Wrap=false DataField="HPhone"  headerText="Home Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-Wrap=false DataField="CPhone" headerText="Cell Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-Wrap=false DataField="WPhone" headerText="Work Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>                    
                 </Columns>
  				</asp:DataGrid>&nbsp;
  				</asp:Panel>
  				</td></tr>
  				</table> 
  				</td></tr>
			</table>    
</asp:Content>






 

 
 
 