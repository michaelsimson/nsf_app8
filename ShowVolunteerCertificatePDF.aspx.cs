using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;



public partial class ShowVolunteerCertificatePDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["LoggedIn"] != "true")
        //{
        //    Server.Transfer("login.aspx?entry=" + Session["entryToken"]);
        //}
        LoadCertificates();

    }
    private void LoadCertificates()
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition",
         "attachment;filename=" + Session["Reg"].ToString() + ".pdf");
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        rptCertificate.DataBind();
        rptCertificate.RenderControl(hw);
        //DataList7.DataBind();
        //DataList7.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4_LANDSCAPE, 20f, 20f, 20f, 5f);
        //PdfWriter.GetInstance(pdfDoc, new FileStream("E:\\App5\\Doc\\" + Session["newid"].ToString() + ".pdf", FileMode.Create));
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
}



  



