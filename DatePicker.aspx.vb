'**********************************************
'* WebLogger v.1.0
'* Copyright 2003 by Marco Bellinaso
'* Web: http://www.vb2themax.com
'* E-mail: mbellinaso@vb2themax.com
'**********************************************

Namespace VRegistration

Partial Class DatePicker
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ID = "DatePick"

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
        Public Sub Page_Load()
            Response.Write("ID" & Request.QueryString("ID"))
        End Sub
#End Region
        Private Sub Picker_DayRender(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Picker.DayRender
            ' create a hyperlink control, and set its text to the cell's current text
            Dim hl As New HyperLink()
            hl.Text = CType(e.Cell.Controls(0), LiteralControl).Text
            ' set the hyperlink's colors according to whether this month is or is not in the current month
            If e.Day.IsOtherMonth Then
                hl.ForeColor = Color.Black 'Picker.OtherMonthDayStyle.ForeColor
            Else
                hl.ForeColor = Picker.DayStyle.ForeColor
            End If
            If e.Day.IsToday Then hl.ForeColor = Picker.TodayDayStyle.ForeColor
            'hl.NavigateUrl = "SecondPage.aspx?Date=" & e.Day.Date.ToShortDateString()
            ' set the navigation url (the href attribute) to the javascript procedure
            ' defined wihin the client-side <script> block, and pass in input
            ' the clicked date in short format

            hl.NavigateUrl = "javascript:SetDate('" & e.Day.Date.ToShortDateString() & "');"
            ' remove the cell's current child controls, and add the new hyperlink
            e.Cell.Controls.Clear()
            e.Cell.Controls.Add(hl)

        End Sub
End Class

End Namespace

