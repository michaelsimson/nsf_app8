using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_MedalCountWeekly : System.Web.UI.Page
{
    DataSet dsMedals = new DataSet();
    DataTable dt = new DataTable();
    DataRow dr;
    int[] category = new int[33];
    int tCount = 0;
    //int chapID;
    int prevChapterID;
    string Year = "";

    ArrayList al = new ArrayList();
		
    protected void Page_Load(object sender, EventArgs e)
    {
        // Put user code to initialize the page here
        //if(Session["Admin"].ToString() != "Yes")
        //	Response.Redirect("Default.aspx");
        //chapID = Convert.ToInt32(Request.QueryString["Chap"]);
        //chapID = 15;
        Year = System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        lblChapter.Text = ddlWeek.SelectedItem.ToString();
        GetCounts();
    }
    private void GetCounts()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        DateTime startDate = DateTime.Parse("3/1/" + Year);

        while (startDate.DayOfWeek.ToString() != "Saturday")
            startDate = startDate.AddDays(1);
        startDate = startDate.AddDays(7);

        // create and open the connection object
        SqlConnection connection =
            new SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        DateTime where1, where2;
        switch (ddlWeek.SelectedValue)
        {
            case "1":
                where1 = startDate.AddDays(7);
                where2 = startDate.AddDays(8);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "2":
                where1 = startDate.AddDays(14);
                where2 = startDate.AddDays(15);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "3":
                where1 = startDate.AddDays(21);
                where2 = startDate.AddDays(22);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "4":
                where1 = startDate.AddDays(28);
                where2 = startDate.AddDays(29);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "5":
                where1 = startDate.AddDays(35);
                where2 = startDate.AddDays(36);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "6":
                where1 = startDate.AddDays(42);
                where2 = startDate.AddDays(43);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "7":
                where1 = startDate.AddDays(49);
                where2 = startDate.AddDays(50);
                lblDate.Text = where1.ToShortDateString();
                break;
            case "8":
                where1 = startDate.AddDays(56);
                where2 = startDate.AddDays(57);
                lblDate.Text = where1.ToShortDateString();
                break;
            default:
                where1 = startDate.AddDays(63);
                where2 = startDate.AddDays(64);
                lblDate.Text = where1.ToShortDateString();
                break;
        }
        // get records from the products table
        string commandString = "Select Contest.ContestID, Contest.ContestCategoryID, Contest.ContestDate, Contest.NSFChapterID,Contestant.ChildNumber FROM Contest " +
            " INNER JOIN Contestant ON Contest.ContestID = Contestant.ContestCode WHERE Contest_Year='" + Year + "' AND Contestant.PaymentReference IS NOT NULL " +
            " AND ContestDate >= '" + Convert.ToDateTime(where1) + "' AND ContestDate <= '" + Convert.ToDateTime(where2) + "' ORDER BY Contest.NSFChapterID,Contest.ContestID ";
        SqlCommand myCommand = new SqlCommand(commandString, connection);


        SqlDataAdapter daMedals = new SqlDataAdapter(commandString, connection);

        tCount = daMedals.Fill(dsMedals);
        dt.Columns.Add("Contest", typeof(string));
        dt.Columns.Add("PartCert", typeof(string));
        dt.Columns.Add("ExcelCert", typeof(string));
        dt.Columns.Add("Gold", typeof(string));
        dt.Columns.Add("Silver", typeof(string));
        dt.Columns.Add("Bronze", typeof(string));
        dt.Columns.Add("PartMed", typeof(string));
        dt.Columns.Add("VolCert", typeof(string));
        if (dsMedals.Tables[0].Rows.Count > 0)
        {
            prevChapterID = Convert.ToInt32(dsMedals.Tables[0].Rows[0].ItemArray[3]);
            dr = dt.NewRow();
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Contest"] = "Chapter: " + GetChapterName(prevChapterID);
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dt.Rows.Add(dr);
            for (int j = 0; j < dsMedals.Tables[0].Rows.Count; )
            {
                if (prevChapterID != Convert.ToInt32(dsMedals.Tables[0].Rows[j].ItemArray[3]))
                    CtrlBrk(j);
                else j = GetContestCount(j);
            }
            //	dsMedals.Tables[0].Columns.Add(dc);
            //DataGrid1.DataSource = dsMedals.Tables[0];
            //DataGrid1.DataBind();
            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();

        }
        else
        {
            Response.Write("No data exists");
            DataGrid1.Visible = false;
            btnSave.Visible = false;
        }
    }
    void CtrlBrk(int j)
    {
        prevChapterID = Convert.ToInt32(dsMedals.Tables[0].Rows[j].ItemArray[3]);
        dr = dt.NewRow();
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Chapter: " + GetChapterName(prevChapterID);
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dt.Rows.Add(dr);
        dr = dt.NewRow();
        dr["Contest"] = "Contest";
        dr["PartCert"] = "PartCert";
        dr["ExcelCert"] = "ExcelCert";
        dr["Gold"] = "Gold";
        dr["Silver"] = "Silver";
        dr["Bronze"] = "Bronze";
        dr["PartMed"] = "PartMed";
        dr["VolCert"] = "VolCert";
        dt.Rows.Add(dr);
        al.RemoveRange(0, al.Count);

    }
    public int GetContestCount(int j)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        //connection.Open();

        // get records from the products table
        //string commandString = "Select ContestCode, ChildNumber from Contestant where ChapterID=" + ChapterID;

        //SqlDataAdapter daCounts = new SqlDataAdapter(commandString,connection);
        //DataSet dsCounts = new DataSet();
        //int retValue = daCounts.Fill(dsCounts);


        int p = j;
        string[] catName = new string[33];
        int[] catCount = new int[33];
        int cCount = 0;
        int goldCount = 0, silverCount = 0, bronzeCount = 0, excelCount = 0;
        int partCount = 0;
        bool found;
        //
        for (; j < dsMedals.Tables[0].Rows.Count; j++)
        {
            if (prevChapterID != Convert.ToInt32(dsMedals.Tables[0].Rows[j].ItemArray[3]))
                break;
            found = false;
            for (int i = 0; i < cCount; i++)
            {
                if (catName[i] == dsMedals.Tables[0].Rows[j].ItemArray[0].ToString())
                {
                    catCount[i]++;
                    found = true;
                    //lblError.Text = dr["Contest"].ToString();// dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                }
            }
            if (found == false)
            {
                catName[cCount] = dsMedals.Tables[0].Rows[j].ItemArray[0].ToString();
                catCount[cCount] = 1;
                cCount++;
            }
            partCount++;
        }

        for (; p < j - 1; p++)
        {
            if (al.Contains(dsMedals.Tables[0].Rows[p].ItemArray[4].ToString()) == false)
                al.Add(dsMedals.Tables[0].Rows[p].ItemArray[4].ToString());
        }
        for (int i = 0; i < cCount; i++)
        {
            dr = dt.NewRow();
            dr["Contest"] = GetLabels(Convert.ToInt32(catName[i]), "ContestCategoryID");
            dr["PartCert"] = catCount[i];
            if (catCount[i] >= 10)
            {
                dr["Gold"] = 1;
                dr["Bronze"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 3;
                goldCount++;
                silverCount++;
                bronzeCount++;
                excelCount += 3;
            }
            else if (catCount[i] >= 8)
            {
                dr["Gold"] = 1;
                dr["Silver"] = 1;
                dr["ExcelCert"] = 2;
                goldCount++;
                silverCount++;
                excelCount += 2;
            }
            else if (catCount[i] >= 5)
            {
                dr["Gold"] = 1;
                dr["ExcelCert"] = 1;
                goldCount++;
                excelCount += 1;
            }

            dt.Rows.Add(dr);
        }
        int increment = 1;
        if (partCount > 50)
            increment = 2;
        else if (partCount > 100)
            increment = 3;
        else if (partCount > 150)
            increment = 4;
        int distRankMedals = (int)(Math.Round(.67 * (goldCount + .5), 0) + Math.Round(.67 * (silverCount + .5), 0) +
            Math.Round(.67 * (bronzeCount + .5), 0));
        dr = dt.NewRow();
        dr["PartCert"] = partCount;
        dr["ExcelCert"] = excelCount + increment;
        dr["Gold"] = goldCount;
        dr["Silver"] = silverCount;
        dr["Bronze"] = bronzeCount;
        //distinct contestants - distinct rank holders
        dr["PartMed"] = al.Count - distRankMedals + increment;
        if (partCount < 51)
            dr["VolCert"] = 8;
        else if (partCount < 100)
            dr["VolCert"] = 12;
        else if (partCount < 150)
            dr["VolCert"] = 15;
        else if (partCount < 200)
            dr["VolCert"] = 20;
        else dr["VolCert"] = 25;
        dt.Rows.Add(dr);
        return j;
        //DataGrid1.DataSource = dt;
        //DataGrid1.DataBind();
        //lblError.Text = dr["Contest"].ToString();
        //lblError.Text = dt.Rows.Count.ToString();

        //	return al.Count;
        //return retValue;

    }
    public string GetLabels(int idNumber, string colName)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();


        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString1 = "Select ContestCategoryID from Contest where ContestID = " + idNumber;


        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString1;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        reader.Read();
        int retVal;
        retVal = reader.GetInt32(0);
        connection.Close();
        string commandString2 = "Select ContestDesc from ContestCategory where " + colName + " = " + retVal;
        System.Data.SqlClient.SqlCommand command2 =
            new System.Data.SqlClient.SqlCommand();
        command2.CommandText = commandString2;
        command2.Connection = connection;
        connection.Open();
        System.Data.SqlClient.SqlDataReader reader2 = command2.ExecuteReader();
        string retValue;
        reader2.Read();
        retValue = reader2.GetString(0);
        // close connection return value
        connection.Close();
        return retValue;
    }
    //Get chapter name instead of ID
    public string GetChapterName(int idNumber)
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        string retValue = "";

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select City, State from Chapter where ChapterID = " + idNumber;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        if (reader.Read() != false)
            retValue = reader.GetString(0) + ", " + reader.GetString(1);
        // close connection, return values
        connection.Close();
        return retValue;

    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=NSF-"+
            Year.Substring(2,2) +"-Reg-" + ddlWeek.SelectedItem.ToString() + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataGrid1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();
    }
}
