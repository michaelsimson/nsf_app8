﻿Imports System
Imports System.Web
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization

Partial Class fsmailtest
    Inherits System.Web.UI.Page

    Protected Sub btnCheckEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckEmail.Click
        If txttestEmail.Text.Length > 3 And txtFrom.Text.Length > 3 And txtContent.Text.Length > 10 And txtCheck.Text = "258448" Then
            Dim re As StreamReader
            Dim emailBody As String = ""
            Dim subMail As String = IIf(txtSub.Text.Length > 3, txtSub.Text, "Test mail")
            emailBody = txtContent.Text
            SendEmail(subMail, emailBody.ToString, txttestEmail.Text)
        Else
            lblBounced.Text = "Check Entries"
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
        'Build Email Message

        Dim email As New MailMessage
        Dim ok As Boolean = True
        Try
            email.From = New MailAddress(txtFrom.Text)
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            client.Host = "207.150.192.108"
            client.Credentials = New NetworkCredential("fsilva@redegginfoexpert.com", "258448")
            client.Send(email)
        Catch e As Exception
            ' lblBounced.Text = lblBounced.Text & e.Message.ToString & " : "
            ok = False
        End Try
        lblBounced.Text = "Mail Send"
        Return ok
    End Function
   
End Class


