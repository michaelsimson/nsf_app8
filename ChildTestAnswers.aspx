<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" ValidateRequest="false" EnableViewStateMac="false"
    CodeFile="ChildTestAnswers.aspx.cs" Inherits="ChildTestAnswers" %>


<%--<asp:Content ID="Content2" ContentPlaceHolderID="Content_Head" runat="server">
 </asp:Content>--%>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="server">
    <script src="Scripts/jquery-1.9.1.js"></script>


    <script type="text/javascript">
        //window.onload = function () {
        //    warningMessage();
        //};
        $(document).on("click", ".level1", function (e) {
            var val = document.getElementById("<%=hdnIsAnswer.ClientID%>").value;
           
            if (val == "Yes") {
                if (confirm("Your answer is not yet updated in the system. Are you sure to leave this page?")) {
                    
                } else {
                    return false;
                }
            }
           
        });
        function warningMessage() {

            var msg = "Your answer is not yet updated in the system. Are you sure to leave this page?"

            if (confirm(msg)) {
                window.location.href = "UserFunctions.aspx";
            }
        }
        $(function (e) {

            $(".ancHelp").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {

                        var dvHtml = "";
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">1. Make selections on the top row and press <span style="font-weight:bold; color:blue;">Submit.</span></span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">2. Press on <span style="font-weight:bold; color:blue;">Start Exam</span> button.</span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">3. Press on appropriate <span style="font-weight:bold; color:blue;">Section</span> (among highlighted)</span></div> ';

                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">4. Press <span style="font-weight:bold; color:blue;">Save Answers</span> button frequently to avoid losing data.</span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">5. Once you are done with all <span style="font-weight:bold; color:blue;">Sections</span> and <span style="font-weight:bold; color:blue;">Saved</span>, press <span style="font-weight:bold; color:blue;">Submit for Grading.</span><span> Once submitted, you cannot change your answers.</span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">6. <span style="font-weight:bold; color:blue;">Regrade</span> is only to correct grading errors, <span style="font-weight:bold; color:blue;">not to change answers.</span></span></div> ';
                        return dvHtml;

                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Instructions to follow</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                }

            })
        });
    </script>

    <style type="text/css">
        .qTipWidth {
            max-width: 450px !important;
        }

        .style8 {
            width: 99px;
        }

        .style10 {
            width: 147px;
        }

        .style12 {
            width: 84px;
            text-align: left;
        }

        .style16 {
            width: 126px;
        }

        .style17 {
            width: 88px;
        }

        .style20 {
            width: 113px;
            text-align: left;
        }

        .style47 {
            width: 100%;
        }

        .style53 {
            width: 179px;
        }

        .style60 {
            width: 68px;
        }

        .style67 {
            width: 15px;
            font-weight: bold;
        }

        .style70 {
            width: 286px;
        }

        .style87 {
        }

        .style90 {
            width: 184px;
        }

        .style95 {
            width: 93px;
        }

        .style97 {
            width: 117px;
        }

        .style98 {
            width: 37px;
        }

        .style99 {
            width: 120px;
            text-align: left;
        }

        .style101 {
            width: 116px;
        }

        .style102 {
            width: 10px;
        }

        .style118 {
            width: 286px;
            text-align: left;
        }

        .style120 {
            width: 15px;
        }

        .style121 {
            width: 63px;
        }

        .style129 {
            width: 133px;
        }

        .style1 {
            width: 100%;
            height: 92px;
        }

        .style158 {
            width: 95px;
        }

        .style160 {
            width: 271px;
            text-align: left;
        }

        .style162 {
            width: 121px;
        }

        .style163 {
            width: 4px;
        }

        .style165 {
        }

        .style167 {
            text-align: left;
        }

        .style176 {
            width: 199px;
            text-align: left;
        }

        .style177 {
            width: 101px;
            text-align: left;
        }

        .style178 {
            width: 95px;
            text-align: left;
        }

        .style185 {
            width: 152px;
        }

        .style186 {
            width: 101px;
        }

        .style187 {
            width: 181px;
            text-align: left;
        }

        .style191 {
            width: 158px;
        }

        .style195 {
            font-weight: bold;
            width: 158px;
        }

        .style197 {
            width: 54px;
            font-weight: bold;
        }

        .style198 {
            width: 54px;
        }

        .style199 {
            text-align: left;
            width: 182px;
        }

        .style200 {
            width: 120px;
        }

        .style201 {
            width: 179px;
            height: 29px;
        }

        .style202 {
            width: 68px;
            height: 29px;
        }

        .style203 {
            width: 15px;
            height: 29px;
        }

        .style204 {
            width: 10px;
            height: 29px;
        }

        .style205 {
            width: 117px;
            height: 29px;
        }

        .style206 {
            height: 29px;
        }

        .style207 {
            width: 286px;
            height: 29px;
        }
    </style>



    <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>

    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr id="trVol" runat="server" visible="false">
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlnkMainMenu" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <tr id="trParent" runat="server" visible="false">
            <td align="left">
                <asp:LinkButton CssClass="btn_02" runat="server" ID="lnkParent" OnClick="lnkParent_Click" Text="Back to Parent Functions"></asp:LinkButton>
                <asp:HyperLink CssClass="btn_02" ID="hlinkParent" runat="server" NavigateUrl="UserFunctions.aspx" Visible="false">Back to Parent Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <tr id="trStudent" runat="server" visible="false">
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkStudent" runat="server" NavigateUrl="StudentFunctions.aspx">Back to Student Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <!-- COUNTDOWN TIMER -->
        <!-- This goes in the HEAD of the html file -->
        <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
        <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>

        <script type="text/javascript">
</script>

        <%-- <asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="_ctl0_Content_main" runat="server">
        --%><tr>
            <td valign="top">
                <table class="admin-tan-border" cellpadding="5px" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="16" align="center">
                            <div style="float: inherit; color: Blue">
                                <h3>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Enter Your Answers"></asp:Literal></h3>
                            </div>
                            <div style="float: right;">
                                <table>
                                    <tr>
                                        <td width="100%" align="center">
                                            <span id="theTime" style="font-size: xx-large" runat="server"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="center">
                            <div style="float: right">
                            </div>
                            <table runat="server" width="100%" style="color: Gray;">
                                <tr align="center">
                                    <td colspan="2" align="left" style="font-size: large; color: #6B33EE"><a class="ancHelp" style="cursor: pointer; color: blue;">Help?</a>

                                    </td>
                                </tr>

                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" class="style47">
                                <tr>
                                    <%--   <td class="style53">&nbsp;</td>
                                    <td class="style60">&nbsp;</td>
                                    <td class="style67">&nbsp;</td>--%>
                                    <td style="display: none;" class="" bgcolor="Honeydew">Event</td>
                                    <td bgcolor="Honeydew" style="font-weight: bold;" class="style102">EventYear</td>
                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">Semester</td>
                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">Student Name</td>
                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">PaperType</td>

                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">Product Group</td>
                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">Product</td>
                                    <td bgcolor="Honeydew" style="font-weight: bold;" class="">Level</td>

                                    <td class="" style="font-weight: bold;" bgcolor="Honeydew">Week#</td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td class="" style="display: none;">
                                        <asp:DropDownList ID="ddlEvent" runat="server">
                                            <asp:ListItem Value="13">Coaching</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="">
                                        <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                    </td>
                                    <td class="">
                                        <asp:DropDownList ID="ddlSemester" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" runat="server"></asp:DropDownList>

                                    </td>
                                    <td class="" id="td_ChildPar" runat="server">
                                        <asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="ChildName" DataValueField="ChildNumber" runat="server"></asp:DropDownList>
                                        <asp:HiddenField ID="hdnChGrade" runat="server" Visible="false" />
                                    </td>
                                    <td class="" id="td_ddlChildPar" runat="server">
                                        <asp:DropDownList ID="ddlPaperType" runat="server">
                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                            <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                                            <asp:ListItem Value="PT">Pretest</asp:ListItem>
                                            <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                                            <asp:ListItem Value="FT">Final Test</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="">
                                        <asp:DropDownList ID="ddlProductGroup"
                                            DataTextField="Name" DataValueField="ProductGroupID"
                                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="">
                                        <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" Enabled="false" runat="server"
                                            OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlLevel" DataTextField="Level" DataValueField="Level" Enabled="false" runat="server" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                    <td class="">
                                        <asp:DropDownList ID="ddlWeekNo" DataTextField="WeekId" OnSelectedIndexChanged="ddlWeekNo_SelectedIndexChanged" DataValueField="Coachpaperid" runat="server"></asp:DropDownList>
                                    </td>
                                    <td class="">
                                        <asp:Button ID="Btn_Submit" runat="server" Text="Submit" OnClick="Btn_Submit_Click" /></td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:Button ID="BtnGradeCard" runat="server" Visible="false" Text="Grade Card"
                                OnClick="BtnGradeCard_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                            <asp:HiddenField ID="hdnCoachPaperID" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                                            BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                                            OnItemCommand="DGCoachPapers_ItemCommand" OnItemDataBound="DGCoachPapers_ItemDataBound" Visible="false">
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                                            <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                                            <ItemStyle BackColor="White" />
                                            <Columns>

                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnReGrade" runat="server" CommandName="Regrade" Text="Regrade" Enabled="false"></asp:Button>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventCode" HeaderText="EventCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroupCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="ProductCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>
                                                <%-- <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QReleaseDate" HeaderText="QReleaseDate" Visible="false"></asp:BoundColumn>
                                                --%>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QDeadlineDate" HeaderText="QDeadlineDate" Visible="false"></asp:BoundColumn>

                                            </Columns>

                                        </asp:DataGrid>
                                        <asp:HiddenField ID="hdnSections" runat="server" />
                                    </td>
                                </tr>


                                <%--<tr><td><b>Test Number</b></td><td><asp:DropDownList ID="ddlTestNumber" runat="server" DataTextField="CoachPaperID" DataValueField="CoachPaperID"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlTestNumber_SelectedIndexChanged"
                                    Width="80px" Enabled="false">
                                    <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                              
                            </td> </tr>--%>
                                <%--<tr>
                            <td id="tdChild" runat="server" Visible="false"><b>ChildName</b> 
                                <asp:DropDownList ID="ddlmemberID" runat="server" DataTextField="ChildName" DataValueField="ChildNumber"
                                    AppendDataBoundItems="true"  AutoPostBack="true" 
                                    onselectedindexchanged="ddlmemberID_SelectedIndexChanged">
                                    <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="StudentIDSource" runat="server"></asp:SqlDataSource>
                            </td></tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" visible="false" runat="server">
                            <h3 style="color: Blue; font-size: large; font-family: Sans-Serif">Section Timer:<span id="sectiontimer"></span></h3>
                        </td>
                    </tr>
                    <tr visible="false" runat="server">
                        <td>
                            <asp:Label ID="lblNote" runat="server" Text="Note**: Please select TestNumber and ChildName to Start Test" ForeColor="Green" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td runat="server" align="center">
                            <asp:Panel ID="pnlsubmit" runat="server" Visible="false" BorderStyle="Inset" Width="45%" BackColor="White">
                                <table class="announcement_text" border="0" width="100%">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblTotalCount" runat="server" ForeColor="Red" Font-Size="Large"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="background-color: Silver; width: 100%">
                                            <h2 style="padding: 0px; margin-top: 0px;">
                                                <asp:Label ID="Label1" runat="server" Text="Once you click below Submit button, answers cannot be modified.  Are you sure you want to submit?"></asp:Label></h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <%-- <ContentTemplate>   
                                <asp:UpdatePanel runat="server" id="UpdatePanel1" > 
                                    <ContentTemplate> --%>
                                            <asp:Button ID="btnConfirmSubmit" runat="server" CausesValidation="False"
                                                CssClass="standard-text" OnClick="btnConfirmSubmit_Click" Text="Submit" />
                                            <%--OnClientClick="$('#MainContent_AllSections :input').removeAttr('disabled');return true;" --%>
                                            <%-- </ContentTemplate>
                               </asp:UpdatePanel> 
                           </ContentTemplate>--%>
                                            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False"
                                                CssClass="standard-text" Text="Cancel" OnClick="BtnCancel_Click" />
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblSuccess" CssClass="announcement_text" runat="server" Text="" ForeColor="Red" Font-Size="Medium" Font-Bold="false"></asp:Label></td>
                    </tr>

                    <tr id="TrTimers" visible="false" runat="server">
                        <td>
                            <table>
                                <tr>
                                    <td><%--Section1:--%>

                                        <asp:Label ID="Timer1" Visible="false" runat="server"></asp:Label>
                                        <asp:Button ID="btnstart" Text="Start Exam" runat="server" Enabled="false" OnClick="btnstart_Click" />&nbsp;
                      
                                    </td>
                                    <td><%--Section2:--%>
                                        <asp:Button ID="Btn_Save" runat="server" Text="Save Answers" Enabled="false" OnClick="Btn_Save_Click" />&nbsp;
                                        <asp:Label ID="Timer2" Visible="false" runat="Server"></asp:Label>
                                    </td>
                                    <td><%--Section3:--%>
                                        <asp:Button ID="btnSubmit" Text="Submit for Grading" runat="server" Enabled="false" OnClick="btnSubmit_Click1" CausesValidation="False" />
                                        <asp:Label ID="Timer3" Visible="false" runat="Server"></asp:Label>
                                    </td>
                                    <td><%--Section4:--%>
                                        <asp:Button ID="BtnRegrading" runat="server" Text="Regrade" Enabled="false" OnClick="BtnRegrading_Click" Visible="false"></asp:Button>
                                        <asp:Label ID="Timer4" Visible="false" runat="Server"></asp:Label>
                                    </td>
                                    <td><%--Section5:--%>
                                        <asp:HyperLink CssClass="btn_02" ID="Hyperlink36" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/GradingToolInstruction.docx">Guide to Entering Math Answers
                                        </asp:HyperLink>
                                        <asp:Label ID="Timer5" Visible="false" runat="Server"></asp:Label>
                                    </td>


                                </tr>
                            </table>
                        </td>


                    </tr>
                    <%-- <tr>
                        <td align="center">
                            <table runat="server" width="600px">
                                <tr>
                                    <td colspan="7" width="600px" align="right"></td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <asp:Label ID="lblWrngmsg" runat="server" Text="To enter answers, select Section.  Press Save Answers button frequently to avoid losing data." ForeColor="Red" Visible="false"></asp:Label></td>
                    </tr>
                    <tr id="trBtnSection" runat="server">
                        <td>
                            <asp:Button ID="btnsection1" Visible="false" runat="server" Text="Section1" Enabled="false" class="buttonClass" OnClick="btnsection1_click" />
                            <asp:Button ID="btnsection2" Visible="false" runat="server" Text="Section2" Enabled="false" class="buttonClass" OnClick="btnsection2_click" />
                            <asp:Button ID="btnsection3" Visible="false" runat="server" Text="Section3" Enabled="false" class="buttonClass" OnClick="btnsection3_click" />
                            <asp:Button ID="btnsection4" Visible="false" runat="server" Text="Section4" Enabled="false" class="buttonClass" OnClick="btnsection4_click" />
                            <asp:Button ID="btnsection5" Visible="false" runat="server" Text="Section5" Enabled="false" class="buttonClass" OnClick="btnsection5_click" />
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:Panel ID="AllSections" runat="server">
                                <table>
                                    <tr id="trSectionGridviews" runat="server">
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl1">
                                                <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSectionNumber,TestSetUpSectionsID,QuestionType"
                                                    OnRowDataBound="GridView1_RowDataBound"
                                                    OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber" />
                                                        <asp:TemplateField HeaderText="Section 1">

                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" RepeatDirection="Horizontal" runat="server">
                                                                    <%----%>
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />

                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel ID="pnl2" runat="server">
                                                <asp:GridView ID="GridView2" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView2_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 2">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl3">
                                                <asp:GridView ID="GridView3" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView3_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 3">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl4">
                                                <asp:GridView ID="GridView4" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView4_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="true" />
                                                        <asp:TemplateField HeaderText="Section 4">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel ID="pnl5" runat="server">
                                                <asp:GridView ID="GridView5" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView5_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 5">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnIsAnswer" runat="server" Value="0" />
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Button runat="server" ID="hiddenTargetControlForSubmitExam" Style="display: none" />
    <%--  <asp:Panel ID="pnlsubmit" runat="server">
        <table class="announcement_text">
            <tr><td><h2 style="padding: 0px; margin-top: 0px">
                        <asp:Label ID="Label1" runat="server" Text="Confirm Submit(DoubleClickOnSubmitButton)"></asp:Label></h2>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td align="center">
                <ContentTemplate>   
                    <asp:UpdatePanel runat="server" id="UpdatePanel1" > 
                        <ContentTemplate> 
                            <asp:Button ID="Button5" runat="server" CausesValidation="False"  
                               CssClass="announcement_text" OnClick="btnSubmit_Click" Text="Submit" /><%--OnClientClick="$('#MainContent_AllSections :input').removeAttr('disabled');return true;" - -%>
                         </ContentTemplate>
                   </asp:UpdatePanel> 
               </ContentTemplate>
               <asp:Button ID="Button6" runat="server" CausesValidation="False" CssClass="standard-text" Text="Cancel" />
              </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:modalpopupextender ID="pnlsubmitE" runat="server" 
        TargetControlID="btnSubmit" CancelControlID="Button6"  
        PopupControlID="pnlsubmit" BackgroundCssClass="modalBackground">
    </asp:modalpopupextender>--%>


    <script type="text/javascript">
        document.getElementById("_ctl0_Content_main_pnl1").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl2").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl3").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl4").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl5").style.display = "Block";

    </script>
</asp:Content>
