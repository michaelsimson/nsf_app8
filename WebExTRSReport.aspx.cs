﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;

public partial class WebExTRSReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblErrMsg.Text = "";
            if (!IsPostBack)
            {

                fillCoach();

                // TestCreateTrainingSession();
            }
        }
    }

    public void fillCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Accepted='Y' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDlCoach.DataSource = ds;
                DDlCoach.DataTextField = "Name";
                DDlCoach.DataValueField = "AutoMemberID";
                DDlCoach.DataBind();
                DDlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //DDlCoach.Items.Insert(1, new ListItem("All", "All"));
                }
                else if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlCoach.SelectedValue = Session["LoginId"].ToString();
                }

            }
        }
        catch
        {
        }

    }

    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where  EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + DDlCoach.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and Accepted='Y')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where  ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                DDlProduct.Enabled = true;
                DDlProduct.DataSource = ds;
                DDlProduct.DataTextField = "ProductCode";
                DDlProduct.DataValueField = "ProductID";
                DDlProduct.DataBind();
                // DDlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = false;



                }
                else
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = true;
                }
                LoadSessionNo();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void DDlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }

    public void ListWebExSessionHistoryReport(string Name, string Pwd)
    {
        hdnUserID.Value = Name;
        hdnPwd.Value = Pwd;
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + Name + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.history.LsttrainingsessionHistory\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        //strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        //strXML += "<attendeeName>" + Name + "</attendeeName>";
        //strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
        //strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
        //strXML += "<RegID>" + RegsiterID + "</RegID>";

        if (DDlCoach.SelectedValue != "0")
        {
            strXML += "<confName>" + DDlCoach.SelectedItem.Text + " - " + DDlProduct.SelectedItem.Text + "</confName>";
        }


        strXML += "<startTimeScope>";
        strXML += "<sessionStartTimeStart>" + TxtStartDate.Text + " 12:00:00";
        strXML += "</sessionStartTimeStart>";
        strXML += "<sessionStartTimeEnd>" + TxtEndDate.Text + " 12:00:00</sessionStartTimeEnd>";
        strXML += "</startTimeScope>";
        strXML += "<endTimeScope>";
        strXML += "<sessionEndTimeStart>" + TxtStartDate.Text + " 12:00:00";
        strXML += "</sessionEndTimeStart>";
        strXML += "<sessionEndTimeEnd>" + TxtEndDate.Text + " 12:00:00</sessionEndTimeEnd>";
        strXML += "</endTimeScope>";
        strXML += "<listControl>";
        strXML += "<serv:startFrom>1</serv:startFrom>";
        strXML += "<serv:maximumNum>200</serv:maximumNum>";
        strXML += "<serv:listMethod>AND</serv:listMethod>";
        strXML += "</listControl>";
        strXML += "<order>";

        strXML += "<orderBy>STARTTIME</orderBy>";
        strXML += "<orderAD>DESC</orderAD>";
        strXML += "</order>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        ProcessTrainingStatusTestResponse(xmlReply);
        //string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }

    private string ProcessTrainingStatusTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");
            manager.AddNamespace("history", "http://www.webex.com/schemas/2002/06/service/history");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            string ConfName = string.Empty;
            string confName = string.Empty;
            string Date = string.Empty;
            string stTime = string.Empty;
            string MeetingType = string.Empty;
            string Duration = string.Empty;
            string Invited = string.Empty;
            string Registered = string.Empty;
            string Attended = string.Empty;
            string SessionNo = string.Empty;
            if (status == "SUCCESS")
            {

                string strHtml = string.Empty;

                confName = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory", manager).InnerText;
                confName = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory/history:sessionKey", manager).InnerXml;


                strHtml += "<div align='center' style='font-weight:bold;'>Table 1: WebEx Usage Report</div>";
                strHtml += "<div style='clear:both;'></div>";
                strHtml += "<div align='left' style='font-weight:bold;'><input type='button' id='btnGenerateReportUsage' value='Export To Excel' /></div>";
                strHtml += "<div style='clear:both;'></div>";
                strHtml += "<table style='width:100%; border:1px solid Black; border-collapse:collapse;'>";
                strHtml += "<tr style='background-color:#FFFFCC; font-weight:bold; font-family:Calibri;'>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Topic";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Metting Number";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Metting Type";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Date";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Start Time";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Duration";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Invited";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Registered";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Attended";
                strHtml += "</td>";
                strHtml += "</tr>";


                try
                {
                    for (int i = 1; i < 100; i++)
                    {
                        confName = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:confName", manager).InnerXml;
                        MeetingType = "TRS";
                        Date = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:sessionStartTime", manager).InnerXml;
                        stTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:sessionStartTime", manager).InnerXml;
                        Duration = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:duration", manager).InnerXml;
                        Registered = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:totalRegistered", manager).InnerXml;
                        Invited = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:totalInvited", manager).InnerXml;
                        Attended = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:totalAttendee", manager).InnerXml;
                        SessionNo = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingSessionHistory[" + i + "]/history:sessionKey", manager).InnerXml;
                        Invited = (Invited == "-1" ? "0" : Invited);
                        Registered = (Registered == "-1" ? "0" : Registered);
                        Attended = (Attended == "-1" ? "0" : Attended);
                        string bgcolor = "";
                        if (i % 2 == 0)
                        {
                            bgcolor = "#E8F2FC";
                        }
                        else
                        {
                            bgcolor = "#ffffff";
                        }
                        strHtml += "<tr style='Background-color:" + bgcolor + "'>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'><a id='ancConfName' attr-Date=" + Date + " attr-topic=" + confName + " style='cursor:pointer; color:blue; text-decoration:underline;'>" + confName + "</a>";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + SessionNo + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + MeetingType + "";
                        strHtml += "</td>";

                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Date.Substring(0, 10) + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + stTime.Substring(10, 9) + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Duration + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Invited + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Registered + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Attended + "";
                        strHtml += "</td>";
                        strHtml += "</tr>";
                    }
                }
                catch
                {
                }
                strHtml += "</table>";

                LtrUsageReport.Text = strHtml;
                if (hdnIsExcel.Value == "1")
                {
                    ExportToExcel(strHtml);
                }
            }
            else
            {
                LtrUsageReport.Text = "<span style='color:red;'>No record found</span>";
            }

        }
        catch (Exception e)
        {
            // sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }

    public void GenerateWebExReport()
    {
        dvAttendeeReport.Visible = false;
        string WebExID = string.Empty;
        string Pwd = string.Empty;
        string cmdText = string.Empty;
        cmdText = "select UserID, Pwd from CalSignup where MemberID=" + DDlCoach.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and ProductId=" + DDlProduct.SelectedValue + " and userID is not null and SessionNo=" + DDlSessionNo.SelectedValue + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                WebExID = ds.Tables[0].Rows[0]["UserID"].ToString();
                Pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
            }
        }


        ListWebExSessionHistoryReport(WebExID, Pwd);


    }


    protected void BtnGenerateReport_Click(object sender, EventArgs e)
    {
        if (validateReport() == "1")
        {
            GenerateWebExReport();
        }
    }

    public void UsageReportHtml()
    {

    }


    public string validateReport()
    {
        string Retval = "1";
        if (DDlCoach.SelectedValue == "0")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please select Coach";
        }
        else if (DDlProduct.SelectedValue == "0")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please select Product";
        }
        else if (TxtStartDate.Text == "")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please fill From Date";
        }
        else if (TxtEndDate.Text == "")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please fill To Date";
        }
        return Retval;
    }



    public void ListWebExSessionAttendeeHistoryReport(string Name, string Pwd)
    {
        try
        {
            string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


            WebRequest request = WebRequest.Create(strXMLServer);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Create POST data and convert it to a byte array.
            string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


            strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
            strXML += "<header>\r\n";
            strXML += "<securityContext>\r\n";
            strXML += "<webExID>" + Name + "</webExID>\r\n";
            strXML += "<password>" + Pwd + "</password>\r\n";

            //strXML += "<siteID>690319</siteID>";
            //strXML += "<partnerID>g0webx!</partnerID>\r\n";
            strXML += "<siteName>northsouth</siteName>\r\n";
            strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
            strXML += "</securityContext>\r\n";
            strXML += "</header>\r\n";
            strXML += "<body>\r\n";
            strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.history.LsttrainingattendeeHistory\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

            //strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
            //strXML += "<attendeeName>" + Name + "</attendeeName>";
            //strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
            //strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
            //strXML += "<RegID>" + RegsiterID + "</RegID>";

            if (DDlCoach.SelectedValue != "0")
            {
                strXML += "<confName>" + DDlCoach.SelectedItem.Text + " - " + DDlProduct.SelectedItem.Text + "</confName>";
            }


            DateTime dtG2 = Convert.ToDateTime(hdnDate.Value);
            dtG2 = dtG2.AddDays(1);
            string strDateG2 = dtG2.Month + "/" + dtG2.Day + "/" + dtG2.Year;
            DateTime dt3 = Convert.ToDateTime(strDateG2);
            strDateG2 = Convert.ToDateTime(strDateG2).ToString("MM/dd/yyyy");
            //string strDateG2 = dtG2.Day + "/" + dtG2.Month + "/" + dtG2.Year;

            DateTime dtG4 = Convert.ToDateTime(hdnDate.Value);
            dtG4 = dtG4.AddDays(-1);
            string strDateG3 = dtG4.Month + "/" + dtG4.Day + "/" + dtG4.Year;
            DateTime dt5 = Convert.ToDateTime(strDateG3);
            strDateG3 = Convert.ToDateTime(strDateG3).ToString("MM/dd/yyyy");

            strXML += "<startTimeScope>";
            strXML += "<sessionStartTimeStart>" + strDateG3 + " 12:00:00";
            strXML += "</sessionStartTimeStart>";
            strXML += "<sessionStartTimeEnd>" + strDateG2 + " 12:00:00</sessionStartTimeEnd>";
            strXML += "</startTimeScope>";
            strXML += "<endTimeScope>";
            strXML += "<sessionEndTimeStart>" + strDateG3 + " 12:00:00";
            strXML += "</sessionEndTimeStart>";
            strXML += "<sessionEndTimeEnd>" + strDateG2 + " 12:00:00</sessionEndTimeEnd>";
            strXML += "</endTimeScope>";
            strXML += "<listControl>";
            strXML += "<serv:startFrom>1</serv:startFrom>";
            strXML += "<serv:maximumNum>200</serv:maximumNum>";
            strXML += "<serv:listMethod>AND</serv:listMethod>";
            strXML += "</listControl>";
            strXML += "<order>";

            strXML += "<orderBy>NAME</orderBy>";
            strXML += "<orderAD>DESC</orderAD>";
            strXML += "</order>";


            strXML += "</bodyContent>\r\n";
            strXML += "</body>\r\n";
            strXML += "</serv:message>\r\n";
            byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();

            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            XmlDocument xmlReply = null;
            if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
            {

                xmlReply = new XmlDocument();
                xmlReply.Load(dataStream);

            }
            ProcessTrainingAttendeeResponse(xmlReply);
            //string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
            //string result = this.ProcessMeetingResponse(xmlReply);
            //if (!string.IsNullOrEmpty(result))
            //{
            //lblMsg2.Text += result;
            //}
        }
        catch (Exception ex)
        {

        }
    }

    private string ProcessTrainingAttendeeResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");
            manager.AddNamespace("history", "http://www.webex.com/schemas/2002/06/service/history");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            string ConfName = string.Empty;
            string confName = string.Empty;
            string Date = string.Empty;
            string stTime = string.Empty;
            string stEndTime = string.Empty;
            string MeetingType = string.Empty;
            string Duration = string.Empty;
            string Invited = string.Empty;
            string Registered = string.Empty;
            string Attended = string.Empty;
            string SessionNo = string.Empty;

            string AttendeeName = string.Empty;
            string AttendeeEmail = string.Empty;
            string ipAddress = string.Empty;
            string clientAgent = string.Empty;
            string AttendeeID = string.Empty;
            if (status == "SUCCESS")
            {

                string strHtml = string.Empty;




                strHtml += "<div align='center' style='font-weight:bold;'>Table 2: WebEx Meeting Attendee Report</div>";
                strHtml += "<div style='clear:both;'></div>";
                strHtml += "<div align='left' style='font-weight:bold;'><input type='button' id='btnGenerateReportAttendee' value='Export To Excel' /></div>";
                strHtml += "<div style='clear:both;'></div>";
                strHtml += "<table style='width:100%; border:1px solid Black; border-collapse:collapse;'>";
                strHtml += "<tr style='background-color:#FFFFCC; font-weight:bold; font-family:Calibri;'>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Topic";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Metting Number";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Attendee Name";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Attendee Email";
                strHtml += "</td>";
                //strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>AttendeeID";
               // strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Date";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Start Time";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>End Time";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Duration";
                strHtml += "</td>";

                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Ip Adress";
                strHtml += "</td>";
                strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Client Agent";
                strHtml += "</td>";
                strHtml += "</tr>";


                try
                {
                    for (int i = 1; i < 100; i++)
                    {
                        confName = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:confName", manager).InnerXml;
                        MeetingType = "TRS";
                        Date = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:startTime", manager).InnerXml;
                        stTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:startTime", manager).InnerXml;
                        Duration = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:duration", manager).InnerXml;
                        // Registered = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:totalRegistered", manager).InnerXml;
                        //Invited = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:totalInvited", manager).InnerXml;
                        //Attended = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:totalAttendee", manager).InnerXml;
                        SessionNo = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:sessionKey", manager).InnerXml;
                        AttendeeName = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:attendeeName", manager).InnerXml;
                        AttendeeEmail = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:attendeeEmail", manager).InnerXml;
                        ipAddress = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:ipAddress", manager).InnerXml;
                        AttendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:confID", manager).InnerXml;
                        stEndTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:endTime", manager).InnerXml;
                        clientAgent = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/history:trainingAttendeeHistory[" + i + "]/history:clientAgent", manager).InnerXml;
                        //Invited = (Invited == "-1" ? "0" : Invited);
                        // Registered = (Registered == "-1" ? "0" : Registered);
                        //Attended = (Attended == "-1" ? "0" : Attended);
                        string bgcolor = "";
                        if (i % 2 == 0)
                        {
                            bgcolor = "#E8F2FC";
                        }
                        else
                        {
                            bgcolor = "#ffffff";
                        }
                        strHtml += "<tr style='Background-color:" + bgcolor + "'>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + confName + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + SessionNo + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + AttendeeName + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + AttendeeEmail + "";
                        strHtml += "</td>";
                        //strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + AttendeeID + "";
                        //strHtml += "</td>";

                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Date.Substring(0, 10) + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + stTime.Substring(10, 9) + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + stEndTime.Substring(10, 9) + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Duration + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + ipAddress + "";
                        strHtml += "</td>";
                        strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + clientAgent + "";
                        strHtml += "</td>";
                        //strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Attended + "";
                        //strHtml += "</td>";
                        strHtml += "</tr>";
                        BtnCloseTable2.Visible = true;
                    }
                }
                catch
                {
                }
                strHtml += "</table>";

                LtrAttendeeReport.Text = strHtml;
                if (hdnIsExcel.Value == "1")
                {
                    ExportToExcelAttendee(strHtml);
                }
            }
            else
            {
                LtrAttendeeReport.Text = "<span style='color:red;'>No record found</span>";
            }

        }
        catch (Exception e)
        {
            // sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        dvAttendeeReport.Visible = true;
        ListWebExSessionAttendeeHistoryReport(hdnUserID.Value, hdnPwd.Value);
    }
    protected void BtnExportExcelUsage_onClick(object sender, EventArgs e)
    {
        hdnIsExcel.Value = "1";
        GenerateWebExReport();
    }
    protected void BtnExportAttendee_onClick(object sender, EventArgs e)
    {
        hdnIsExcel.Value = "1";
        ListWebExSessionAttendeeHistoryReport(hdnUserID.Value, hdnPwd.Value);
    }

    public void ExportToExcel(string strHtml)
    {
        hdnIsExcel.Value = "0";
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;


        string filename = "WebExUsageReport_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.Write(strHtml.ToString());

        Response.End();

    }

    public void ExportToExcelAttendee(string strHtml)
    {
        hdnIsExcel.Value = "0";
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;


        string filename = "WebExAttendeeReport_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.Write(strHtml.ToString());

        Response.End();

    }
    protected void BtnCloseTable2_Click(object sender, EventArgs e)
    {
        dvAttendeeReport.Visible = false;
        BtnCloseTable2.Visible = false;
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + DDlCoach.SelectedValue + "' and V.ProductID=" + DDlProduct.SelectedValue + "";


        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.ProductID=" + DDlProduct.SelectedValue + "";

        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {


            DDlSessionNo.DataTextField = "SessionNo";
            DDlSessionNo.DataValueField = "SessionNo";
            DDlSessionNo.DataSource = ds;
            DDlSessionNo.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                DDlSessionNo.Enabled = true;
            }
            else
            {
                DDlSessionNo.Enabled = false;
            }
        }

    }
    protected void DDlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }
}