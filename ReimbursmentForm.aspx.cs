using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using NativeExcel;

public partial class ExpenseJournal_ReimbursmentForm : System.Web.UI.Page
{
    private bool locatedAnywhere = false;
    Expensejournal journal = new Expensejournal();
    Revenuejournal revenue = new Revenuejournal();

    #region " Event Handlers "
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
            HyperLink2.Visible = true;

        if (!IsPostBack)
        {
            lblAddress.Text = String.Empty;
            btnApproveAll.Attributes.Add("onclick", "return confirm('Are you sure you want to approve all transactions at once?');");
            // BtnPaid.Attributes.Add("onclick", "showhide('div11');");
            // onclick="showhide('div11');"
             DataSet dsBanks = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT BankID as Value, BankCode as ReimbursedTo from Bank");
            ddlBankId.DataSource = dsBanks.Tables[0];
            ddlBankId.DataTextField = "ReimbursedTo";
            ddlBankId.DataValueField = "Value";
            ddlBankId.DataBind();
            ddlBankId.SelectedIndex = ddlBankId.Items.IndexOf(ddlBankId.Items.FindByValue("2"));
            lblReportError.Visible = false;
            ViewState["MemberType"] = null;
            ViewState["DonorType"] = null;
            ViewState["IncurMemberID"] = -1;
            this.National = "N";
            LocatedAnywhere = false;
            ddlChapters.Enabled = false;
            this.TransactionID = -1;
            this.IsChapterID = false;
            Session["ReportDate"] = "";
            //GetUserChoice(ddlChapters);
            GetTreatment(ddlTreatement);
            //  GetChapter(ddlChapters, int.Parse(Session["RoleId"].ToString()));
            //*** Notes  ***
            //Roles 37, 38 with Chapter flag:  they can only have access to their own chapter.  Only allow Chaper Expenses and Revenue
            //Roles 37, 38 with Finals flag:  they can only have access to chapter = 1.  Only allow Chapgter Expenses and Revenue
            //Roles 37, 38 with National flag:  they have access to all

            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "37") || (Session["RoleId"].ToString() == "5") || (Session["RoleId"].ToString() == "38")))
            {
                //Displays the respective chapter.
                GetChapter(ddlChapters, int.Parse(Session["LoginID"].ToString()));
                GetChapter(ddlToChapter, int.Parse(Session["LoginID"].ToString()));
                GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),false);
                GetAcctgTransType();
                if (Session["RoleId"].ToString() == "5")
                {
                    btnClearDate.Visible = false;
                }
                
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select Count(*) From Volunteer Where [National]='Y' and MemberID=" + Session["LoginID"] + "")) > 0)
                {
                    btnApproveAll.Visible = true;
                    BtnPaid.Visible = true;
                    btnClearDate.Visible = true;
                }
                else 
                {
                    btnApproveAll.Visible = false;
                    BtnPaid.Visible = false;
                    btnClearDate.Visible = false;

                }
            }
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84")))
            {
                //Dsiaplys all the chapters so the user can make a choice.
                GetChapters(ddlChapters);
                GetChapters(ddlToChapter);
                ddlChapters.Enabled = true;
                //DataEntry & Approval            

            }
            //GetEvents(ddlEvent);
            //GetEvents(ddlRevEvent);
            GetStates();
            GetEventYear(ddlYear);
            //** Used for Reports only
            //GetEventYear1(LstYear);
            GetExpenseCategory(ddlExCategory);
            //GetChapters(ddlChapter);
            //Get Values for Revenue DropDown
            GetRevCat(DdlRevSprType);
            GetSponser(DdlRevSprFor);
            GetDonPur(DdlDonationPurpose);
            GetRevCatFee(DdlRevFeetype);
            GetSalesType(ddlRevSalesType);
            GetAppovalFlag(DdlRevNationalApproval);
            GetAppovalFlag(DdlRevChapterApproval);
            GetAppovalFlag(ddlChapterApprovalFlag);
            GetAppovalFlag(ddlNationalApproverFlag);

           // Panel1.Visible = true;
            PnlRev .Visible = false;
            //RbtnExpense.Checked = true;

            
        }
    }

    protected void btnIndClose_onclick(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;
        if((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
        {
        panel3.Visible = true;
        }
    }

    //Fires when Find button of search panel is clicked
    protected void Find_Click(object sender, EventArgs e)
    {

       // lblSearchFNameOrg.Text = "Organization Name";
        Button btn = (Button)sender;
        if (btn.ID == "ReimburExpFind")
        {
            ViewState["MemberType"] = "ReimburExp";
            ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByValue("INDSPOUSE"));
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
            //if (ddlChapters.SelectedValue == "1") 
               ddlDonorType.Enabled = true;
            //else
            //    ddlDonorType.Enabled = false;
        }
        else if (btn.ID == "incueredExpFind")
        {
            ViewState["MemberType"] = "IncurrExp";
            ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByValue("INDSPOUSE"));
            ddlDonorType.Enabled = false;
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
        }
        else
        {
            ViewState["MemberType"] = "VendorName";
            ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByValue("Organization"));
            ddlDonorType.Enabled = false;
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtOrgName.Text = String.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true;
        }
        
        pIndSearch.Visible = true;
        //panel3.Visible = false;
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmail.Text = "";
        ddlState.SelectedIndex = 0;  
       
    }

    protected void ddlDonorType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblExpError.Text = "";
        if (ddlDonorType.SelectedValue == "Organization")
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtOrgName.Text = String.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true; 
        }
        else
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        ErrorRev.Text = "";
        if (btnAdd.Text == "Add")
        {
            if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7"))
            {
                if (ValidExp())
                {
                    try
                    {
                        journal.expenseCategoryID = int.Parse(GetCode(ddlExCategory.SelectedItem.Value.ToString())[0]);
                   
                    journal.eventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                    //  journal.Report = DateTime.Parse(txtReport.Text);
                    if (reportnew.Visible == true)
                        journal.Report = DateTime.Parse(txtReport.Text);
                    else
                        journal.Report = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString());
                    journal.expenseCatCode = GetCode(ddlExCategory.SelectedItem.Value.ToString())[1];

                    if (txtExAmount.Text == "")
                    {
                        journal.expenseAmount = 0 ;
                    }
                    else 
                    {
                        journal.expenseAmount = decimal.Round(decimal.Parse(txtExAmount.Text), 2);
                    }

                    if (txtSpAmount.Text == "")
                    {
                        journal.spAmount = 0;// decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                    }
                    else
                    {
                        journal.spAmount = decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                    }



                    journal.comments = txtExDes.Text;
                    journal.dateIncurred = DateTime.Parse(txtDate.Text);
                    journal.treatmentCode = GetCode(ddlTreatement.SelectedItem.Value.ToString())[1]; ;
                    journal.treatementID = int.Parse(GetCode(ddlTreatement.SelectedItem.Value.ToString())[0]);
                    journal.eventID = int.Parse(ddlEvent.SelectedItem.Value.ToString());
                    journal.chapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());
                    journal.ReimMemberId = ReimMemberID;
                    journal.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                    //journal.IncMemberId = IncurMemberID;
                    if (txtIncurrEx.Text.Length == 0)
                        journal.IncMemberId = -1;
                    else
                        journal.IncMemberId = IncurMemberID;
                    journal.VendorName = txtVendorName.Text;
                    journal.DonorType = ViewState["DonorType"].ToString();
                    journal.tochapterID = int.Parse(ddlToChapter.SelectedValue.ToString());
                    int i = AddExpensejournal(journal);
                    if (i > 0)
                    {
                        lblMessage.Text = "Inserted Successfully";
                        DisplayRecords();
                    }
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.ToString());
                    }
                    //disableExpControls();
                }
                else
                {
                    lblMessage.Text = "";
                    ErrorMsg.Visible = true;
                }
            }
            else
            {
                if (ValidRev())
                {

                    revenue.RevSource = DdlRevSource.SelectedItem.Text;
                    revenue.RevCatID = int.Parse(DdlRevSprType.SelectedItem.Value);
                   // revenue.RevReport = DateTime.Parse(txtReport.Text);
                    if (reportnew.Visible == true)
                        revenue.RevReport = DateTime.Parse(txtReport.Text);
                    else
                        revenue.RevReport = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString()); 
                    revenue.RevCatCode = DdlRevSprType.SelectedItem.Text;
                    revenue.DateReceived = DateTime.Parse(txtRevDate.Text);
                    revenue.RevComments = txtRevComments.Text;
                    revenue.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                    revenue.RevAmount = decimal.Round(decimal.Parse(TxtRevAmt.Text), 2);
                    revenue.RevEventID = int.Parse(ddlRevEvent.SelectedItem.Value.ToString());
                    revenue.RevEventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                    revenue.RevChapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());
                    revenue.Givername = txtGiverName.Text;
                    // Change According to the RevSource Selected 
                    if (DdlRevSource.SelectedItem.Text == "Sponsorship")
                    {
                        revenue.SponsorID = int.Parse(DdlRevSprFor.SelectedItem.Value);
                        revenue.SponsorCode = GetSponCode(revenue.SponsorID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Donation")
                    {
                        revenue.DonPurposeID = int.Parse(DdlDonationPurpose.SelectedItem.Value);
                        revenue.DonPurposeCode = GetDonPurCode(revenue.DonPurposeID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Fees")
                    {
                        revenue.RevCatID = int.Parse(DdlRevFeetype.SelectedItem.Value);
                        revenue.RevCatCode = DdlRevFeetype.SelectedItem.Text;
                        revenue.FeesType = DdlRevFeetype.SelectedItem.Text;
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Sales")
                    {
                        revenue.SalesCatId = int.Parse(ddlRevSalesType.SelectedItem.Value);
                        revenue.SalesCatCode = ddlRevSalesType.SelectedItem.Text;
                    }
                    revenue.PaymentMethod = DdlRevType.SelectedItem.Text;

                    int i = AddRevenuejournal(revenue);
                    if (i > 0)
                    {
                        DisplayRecords();
                        disableRevControls();
                        ErrorRev.Text = ""; 
                        lblMessage.Text="Inserted Successfully.";
                    }
                }
            }
        }

        if (btnAdd.Text == "Save")
        {
            if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7"))
            {
                if (ValidExp())
                {
                journal.expenseCategoryID = int.Parse(GetCode(ddlExCategory.SelectedItem.Value.ToString())[0]);
                journal.expenseCatCode = GetCode(ddlExCategory.SelectedItem.Value.ToString())[1];
              //  journal.Report = DateTime.Parse(txtReport.Text);
                if (reportnew.Visible == true)
                    journal.Report = DateTime.Parse(txtReport.Text);
                else
                    journal.Report = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString()); 
                journal.treatementID = int.Parse(GetCode(ddlTreatement.SelectedItem.Value.ToString())[0]);
                journal.eventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                //journal.expenseAmount = decimal.Round(decimal.Parse(txtExAmount.Text), 2);
                //journal.spAmount = decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                if (txtExAmount.Text == "")
                {
                    journal.expenseAmount = 0;
                }
                else
                {
                    journal.expenseAmount = decimal.Round(decimal.Parse(txtExAmount.Text), 2);
                }

                if (txtSpAmount.Text == "")
                {
                    journal.spAmount = 0;// decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                }
                else
                {
                    journal.spAmount = decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                }

                journal.comments = txtExDes.Text;
                journal.dateIncurred = DateTime.Parse(txtDate.Text);
                journal.treatmentCode = GetCode(ddlTreatement.SelectedItem.Value.ToString())[1];
                // journal.treatementID = int.Parse(ddlTreatement.SelectedItem.Value.ToString());
                journal.eventID = int.Parse(ddlEvent.SelectedItem.Value.ToString());
                journal.ReimMemberId = ReimMemberID;
                journal.chapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());

                if (txtIncurrEx.Text.Length == 0)
                    journal.IncMemberId = -1;
                else
                    journal.IncMemberId = IncurMemberID;
                journal.VendorName = txtVendorName.Text;
                journal.DonorType = ViewState["DonorType"].ToString();
                journal.tochapterID = int.Parse(ddlToChapter.SelectedValue.ToString());
                journal.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                UpdateExpensejournal(journal);
                lblMessage.Text = "Record Updated";
                DisplayRecords();
                disableExpControls();
                }
            }
            else
            {
                if (ValidRev())
                {
                    revenue.RevSource = DdlRevSource.SelectedItem.Text;
                    revenue.RevCatID = int.Parse(DdlRevSprType.SelectedItem.Value);
                    revenue.RevCatCode = DdlRevSprType.SelectedItem.Text;
                    //revenue .RevReport = DateTime.Parse(txtReport.Text);
                    if (reportnew.Visible == true)
                        revenue.RevReport = DateTime.Parse(txtReport.Text);
                    else
                        revenue.RevReport = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString()); 
                    revenue.DateReceived = DateTime.Parse(txtRevDate.Text);
                    revenue.RevComments = txtRevComments.Text;
                    revenue.RevAmount = decimal.Round(decimal.Parse(TxtRevAmt.Text), 2);
                    revenue.RevEventID = int.Parse(ddlRevEvent.SelectedItem.Value.ToString());
                    revenue.RevEventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                    revenue.RevChapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());
                    revenue.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                    revenue.Givername = txtGiverName.Text;
                    // Change According to the RevSource Selected 
                    if (DdlRevSource.SelectedItem.Text == "Sponsorship")
                    {
                        revenue.SponsorID = int.Parse(DdlRevSprFor.SelectedItem.Value);
                        revenue.SponsorCode = GetSponCode(revenue.SponsorID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Donation")
                    {
                        revenue.DonPurposeID = int.Parse(DdlDonationPurpose.SelectedItem.Value);
                        revenue.DonPurposeCode = GetDonPurCode(revenue.DonPurposeID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Fees")
                    {
                        revenue.RevCatID = int.Parse(DdlRevFeetype.SelectedItem.Value);
                        revenue.RevCatCode = DdlRevFeetype.SelectedItem.Text;
                        revenue.FeesType = DdlRevFeetype.SelectedItem.Text;
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Sales")
                    {
                        revenue.SalesCatId = int.Parse(ddlRevSalesType.SelectedItem.Value);
                        revenue.SalesCatCode = ddlRevSalesType.SelectedItem.Text;
                    }
                    revenue.PaymentMethod = DdlRevType.SelectedItem.Text;

                    int i = UpdateRevenuejournal(revenue);
                    DisplayRecords();
                    disableRevControls();
                    Pnl6Msg.Text = "Record Updated";

                }
            }
        }
        if (btnAdd.Text.Equals("Update"))
        {
            if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7"))
            {
                journal.expenseCategoryID = int.Parse(GetCode(ddlExCategory.SelectedItem.Value.ToString())[0]);
                journal.eventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                if (reportnew.Visible == true)
                    journal.Report = DateTime.Parse(txtReport.Text);
                else
                    journal.Report = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString()); 
                journal.expenseCatCode = GetCode(ddlExCategory.SelectedItem.Value.ToString())[1];

                //journal.expenseAmount = decimal.Round(decimal.Parse(txtExAmount.Text), 2);
                //journal.spAmount = decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                if (txtExAmount.Text == "")
                {
                    journal.expenseAmount = 0;
                }
                else
                {
                    journal.expenseAmount = decimal.Round(decimal.Parse(txtExAmount.Text), 2);
                }

                if (txtSpAmount.Text == "")
                {
                    journal.spAmount = 0;// decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                }
                else
                {
                    journal.spAmount = decimal.Round(decimal.Parse(txtSpAmount.Text), 2);
                }

                journal.treatmentCode = GetCode(ddlTreatement.SelectedItem.Value.ToString())[1];
                journal.comments = txtExDes.Text;
                journal.dateIncurred = DateTime.Parse(txtDate.Text);
                journal.treatementID = int.Parse(GetCode(ddlTreatement.SelectedItem.Value.ToString())[0]);
                journal.eventID = int.Parse(ddlEvent.SelectedItem.Value.ToString());
                journal.chapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());
                journal.ReimMemberId = ReimMemberID;
                if (ddlChapters.SelectedValue == "1" && txtIncurrEx.Text.Length < 1)
                    journal.IncMemberId = -1;
                else
                    journal.IncMemberId = IncurMemberID;
                journal.chapterApprover = int.Parse(Session["LoginID"].ToString());
                journal.chapterApprovalFlag = ddlChapterApprovalFlag.SelectedItem.Text;
                journal.nationalApprover = int.Parse(Session["LoginID"].ToString());
                journal.nationalAprovalFlag = ddlNationalApproverFlag.SelectedItem.Text;
                journal.VendorName = txtVendorName.Text;
                journal.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                // journal.checkNumber = int.Parse(txtCheckNumber.Text);
                // journal.checkDate = DateTime.Parse(txtCheckDate.Text);
                // journal.checkAmount = decimal.Parse(txtCheckAmount.Text);
                journal.ChapterApprovedDate = DateTime.Now;
                journal.NationalApprovalDate = DateTime.Now;

                //Chapter Approval Update...
                if (Panel2.Visible)
                {
                    updateChapterApproval(journal);
                }
                // National Approval Update...
                else if (Panel5.Visible)
                {
                    updateNationalApproval(journal);
                }
                lblMessage.Text = "Record Approved";
                DisplayRecords();
            }
            else
            {
                // only approve
                if (ValidRev())
                {
                    revenue.RevSource = DdlRevSource.SelectedItem.Text;
                    revenue.RevCatID = int.Parse(DdlRevSprType.SelectedItem.Value);
                    if (reportnew.Visible == true)
                       revenue.RevReport = DateTime.Parse(txtReport.Text);
                    else
                       revenue.RevReport = DateTime.Parse(ddlReportDate.SelectedItem.Text.ToString()); 
                    revenue.RevCatCode = DdlRevSprType.SelectedItem.Text;
                    revenue.DateReceived = DateTime.Parse(txtRevDate.Text);
                    revenue.RevComments = txtRevComments.Text;
                    revenue.RevAmount = decimal.Round(decimal.Parse(TxtRevAmt.Text), 2);
                    revenue.RevEventID = int.Parse(ddlRevEvent.SelectedItem.Value.ToString());
                    revenue.RevEventYear = int.Parse(ddlYear.SelectedItem.Text.ToString());
                    revenue.RevChapterID = int.Parse(ddlChapters.SelectedItem.Value.ToString());
                    revenue.Givername = txtGiverName.Text;
                    revenue.TransType = GetCode(ddlTransactionsType.SelectedItem.Value.ToString())[1]; ;
                    // Change According to the RevSource Selected 
                    if (DdlRevSource.SelectedItem.Text == "Sponsorship")
                    {
                        revenue.SponsorID = int.Parse(DdlRevSprFor.SelectedItem.Value);
                        revenue.SponsorCode = GetSponCode(revenue.SponsorID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Donation")
                    {
                        revenue.DonPurposeID = int.Parse(DdlDonationPurpose.SelectedItem.Value);
                        revenue.DonPurposeCode = GetDonPurCode(revenue.DonPurposeID);
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Fees")
                    {
                        revenue.FeesType = DdlRevFeetype.SelectedItem.Text;
                    }
                    else if (DdlRevSource.SelectedItem.Text == "Sales")
                    {
                        revenue.SalesCatId = int.Parse(ddlRevSalesType.SelectedItem.Value);
                        revenue.SalesCatCode = ddlRevSalesType.SelectedItem.Text;
                    }
                    revenue.PaymentMethod = DdlRevType.SelectedItem.Text;

                    revenue.chapterApprover = int.Parse(Session["LoginID"].ToString());
                    revenue.chapterApprovalFlag = DdlRevChapterApproval.SelectedItem.Text;
                    revenue.nationalApprover = int.Parse(Session["LoginID"].ToString());
                    revenue.nationalAprovalFlag = DdlRevNationalApproval.SelectedItem.Text;
                    revenue.ChapterApprovedDate = DateTime.Now;
                    revenue.NationalApprovalDate = DateTime.Now;
                    //Revenue Chapter Approval Update...
                    if (Panel7.Visible)
                    {
                        updateRevChapterApproval(revenue);
                    }
                    //Revenue National Approval Update...
                    else if (Panel8.Visible)
                    {
                        updateRevNationalApproval(revenue);
                       
                    }
                    //lblMessage.Text = "Record Approved";
                    
                    DisplayRecords();
                    disableRevControls();
                }
            }
            }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {

        txtExAmount.Text = string.Empty;
        txtSpAmount.Text = string.Empty;
        txtExDes.Text = string.Empty;
        txtIncurrEx.Text = string.Empty;
        txtReimburse.Text = string.Empty;
        txtDate.Text = string.Empty;
        txtReport.Text = string.Empty;
        txtGiverName.Text = string.Empty;
        txtVendorName.Text = string.Empty;
        ErrorMsg.Text = string.Empty;
        if (ddlEvent.Items.Count > 0) 
            ddlEvent.SelectedIndex = 0;
        if (ddlRevEvent.Items.Count > 0)  
            ddlRevEvent.SelectedIndex = 0;
        lblMessage.Text = string.Empty;

       // ddlYear.SelectedIndex = 0;
        if (ddlExCategory.Items.Count > 0) 
            ddlExCategory.SelectedIndex = 0;
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "4" && GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "7")
        {
            if (ddlTreatement.Items.Count > 0)
                ddlTreatement.SelectedIndex = 0;
            ddlTreatement.Enabled = true;
        }

        txtExAmount.Enabled = true;
        txtSpAmount.Enabled = true;
        txtExDes.Enabled = true;
        txtIncurrEx.Enabled = false;
        txtReimburse.Enabled = false;
        txtDate.Enabled = true;

        ddlEvent.Enabled = true;
        ddlRevEvent.Enabled = true;
        ddlYear.Enabled = true;
        ddlExCategory.Enabled = true;
      
        Pnl3Msg.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
        //panel6.Visible = false;
        disableRevControls();
        GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),false);
        btnAdd.Text = "Add";

    }

    protected void GridMemberDt_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
          int index = int.Parse(e.CommandArgument.ToString());
          GridViewRow row = GridMemberDt.Rows[index];
        // IncurredExpence name Search
          if (ViewState["MemberType"].ToString() == "IncurrExp")
          {
              txtIncurrEx.Text = row.Cells[1].Text + " " + row.Cells[2].Text;
              IncurMemberID = (int)GridMemberDt.DataKeys[index].Value;
              ViewState["IncDonorType"] = row.Cells[4].Text;
          }
          // ReimburseExpence name Search
          else if (ViewState["MemberType"].ToString() == "VendorName")
          {
              txtVendorName.Text = row.Cells[1].Text;
              //VendorID = (int)GridMemberDt.DataKeys[index].Value;

          }
          else if (ViewState["MemberType"].ToString() == "ReimburExp")
          {
              txtReimburse.Text = row.Cells[4].Text.Trim() == "OWN"?row.Cells[1].Text: row.Cells[1].Text.Trim () + " " + row.Cells[2].Text.Trim() ;
              ReimMemberID = (int)GridMemberDt.DataKeys[index].Value;
              ViewState["DonorType"] = row.Cells[4].Text;
              // Aug 13 2011 request to remove Payvendor for IND/Spouse and Romove to be Reimb for OWN
              string strsql = row.Cells[4].Text.Trim() == "OWN" ? "4" : "3";

              if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "4" && GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "7")
              {
                  DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Convert(varchar,Treatid) + '-' + TreatCode as code,Case when TreatID  in (1,2,6) then TreatDesc + ' � 100%' Else TreatDesc End as TreatDesc,Case when TreatID = 3 Then 1 Else Case when TreatID = 4 Then 2 Else Case when TreatID = 5 Then 3 Else Case when TreatID = 1 Then 4 Else Case when TreatID = 6 Then 5 Else Case when TreatID = 2 Then 6 End End End End End  End as RowNo from Treatment where TreatID in (1,2," + strsql + ",5) Order by RowNo");
                  ddlTreatement.DataSource = dsChoice;
                  ddlTreatement.DataTextField = "TreatDesc";
                  ddlTreatement.DataValueField = "code";
                  ddlTreatement.DataBind();
                  ddlTreatement.Items.Insert(0, new ListItem("Select One", "-1"));
                  ddlTreatement.SelectedIndex = 0;
              }

          }
           pIndSearch.Visible = false;
         if((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
          {
          panel3.Visible = true;
          }
          Panel4.Visible = false;
    }

    //Build Query
    protected void btnSearch_onClick(object sender, EventArgs e)
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        state = ddlState.SelectedItem.Value.ToString();
        email = txtEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = "";
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname  + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");           
        }

        if(lastName.Length > 0)
        {
           int length =  strSql.ToString().Length;
           if (length > 0)
           {
               strSql.Append(" and I.lastName like '%" + lastName + "%'");
               //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
           }
           else
           {
               strSql.Append("  I.lastName like '%" + lastName + "%'");
              // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
           }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");               
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");                
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");                
            else
                strSql.Append("  I.Email like '%" + email + "%'"); 
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
               strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");
           
        }

        //Ferdine silvaa 27/05/2010

        ////If ChapterID == null  and if NAtional Flag == y then LocatedAnywhere = true.
        //if ( LocatedAnywhere != null && !LocatedAnywhere)
        //{
        //    if (ddlChapters.SelectedIndex != 0)
        //    {
        //        strSql.Append(" and  ChapterID = '" + Convert.ToInt32(ddlChapters.SelectedItem.Value) + "'");
        //    }
        //    else
        //    {
        //        lblIndSearch.Visible = true;
        //        return;
        //    }
        //}

        if (firstName.Length > 0|| orgname.Length >0  || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
                SearchMembers(strSql.ToString());
            else
                SearchOrganization(strSqlOrg.ToString());
        }
        else
        {
            lblIndSearch.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";
            lblIndSearch.Visible = true;
            return;
        }            
    }

    private void SearchOrganization(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }

    private void SearchMembers(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }
   protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
   {
       lblExpError.Text = "";
       lblMessage.Text = "";
       DisplayRecords();
   }

    protected void ddlChapters_SelectedIndexChanged(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        PnlRev.Visible = false;
        lblAddress.Text = "";
        lblExpError.Text = "";
       
        SqlDataReader readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "Select I.FirstName,I.LastName, I.Address1,I.City,I.State,I.Zip,I.HPhone,I.CPhone  From IndSpouse I Where I.AutoMemberID=(Select TOP 1 V.MemberId from Volunteer V Where V.ChapterId=" + ddlChapters.SelectedValue + " And V.RoleId= 5)" ) ;// Chapte Co-Ordinator -RoleID-5  
        String CPhone;
        while (readr.Read())
        {
            if (readr["CPhone"].ToString() == "") // String.Empty
            {
                CPhone = String.Empty;
            }
            else 
            {
                CPhone = " CPhone : " + readr["CPhone"].ToString();
            }

            lblAddress.Text = "Chapter Coordinator :<br>" + readr["FirstName"].ToString() + " " + readr["LastName"].ToString() + "<br>" + readr["Address1"].ToString() + "<br> " + readr["City"].ToString() + ", " + readr["State"].ToString() + " " + readr["Zip"].ToString() + "<br>" + "HPhone : " + readr["HPhone"].ToString() + "<br>" + CPhone + "<br>";
            
        }
        readr.Close();

        if (ddlChapters.SelectedValue == "1" || ddlChapters.SelectedValue == "108" || ddlChapters.SelectedValue == "109")
        {
            ddlToChapter.Enabled = true;
           // ddlToChapter.SelectedIndex = 0;
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue(ddlChapters.SelectedValue));  
            FindVendorName.Visible = true;
            txtVendorName.Enabled = false;
            //lblReimburse.Text = "Pay To";
            trvendorname.Visible = false; 
        }
        else
        {
            //lblReimburse.Text = "Person to be Reimbursed";
            FindVendorName.Visible = false;
            txtVendorName.Enabled = true;
            ddlToChapter.Enabled = false;
            trvendorname.Visible = true;
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue(ddlChapters.SelectedValue));            
        }
        GetAcctgTransType();
        DisplayRecords();

        if (ddlChapters.SelectedIndex > 0)
        {
            //** ferdine 27/05/2010 if (RwViewReport.Visible == false)
            GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),false);
            //** ferdine 27/05/2010 GetViewReport(LstReport, int.Parse(ddlChapters.SelectedItem.Value));
            lblIndSearch.Visible = false;
            btnClearDate.Visible = true;

        }
        else
        {
            reportnew.Visible = false;
            reportold.Visible = false;
            btnClearDate.Visible = false;
        }

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString().Contains("Delete"))
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            String SqlStr = "Delete from ExpJournal where   TransactionID=" + e.CommandArgument.ToString() + "  and ChapterApprovalFlag='Pending' and NationalApprovalFlag='Pending' ";
            if (SqlHelper.ExecuteNonQuery(conn, CommandType.Text, SqlStr) == 1)
            {
                lblMessage.Text = "Transaction ID : " + e.CommandArgument.ToString() + " -  Record Deleted";
                DisplayRecords();
            }
            else
                lblMessage.Text = "Record Cannot be Deleted";
        }
        else if ((e.CommandName.ToString() == "Modify") || (e.CommandName.ToString() == "Approve"))
        {
            
            int index = int.Parse(e.CommandArgument.ToString());
            int TransID = (int)GridView1.DataKeys[index].Value;

            if (TransID != null && TransID > 0)
            {
                GetSelectedRecord(TransID, e.CommandName.ToString());
            }
        }
        else if (e.CommandName.ToString() == "Paid")
        {
            BtnUpdatePaidAll.Text = "Update";
            int index = int.Parse(e.CommandArgument.ToString());
            hdnTransID.Value = GridView1.DataKeys[index].Value.ToString();
            GridViewRow row = GridView1.Rows[index];
            trans1.Visible = false;
            trans2.Visible = false;
            ddlRestTypeFrom.SelectedIndex = ddlRestTypeFrom.Items.IndexOf(ddlRestTypeFrom.Items.FindByValue("Unrestricted"));
            if (row.Cells[6].Text.Trim() == "Grantee" || row.Cells[6].Text.Trim() == "Return")
            {
                //Unrestricted                
                //Response.Write(row.Cells[6].Text.Trim());
                trans1.Visible = true;
            }
            div11.Visible = true;
            GetPayto();
        }
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
       
    //    if (e.Row.RowState != DataControlRowState.Edit) // check for RowState
    //    {
    //      if (e.Row.RowType == DataControlRowType.DataRow) //check for RowType
    //        {

    //            LinkButton lb = (LinkButton)e.Row.Cells[3].Controls[2]; //cast the ShowDeleteButton link to linkbutton                if (lb != null)
    //            {
    //                lb.Attributes.Add("onclick", "return ConfirmOnDelete();"); //attach the JavaScript function with the ID as the paramter
    //            }
    //        }
    //    }
    //}
  

    #endregion


    #region "Helper Functions"

    private string[] GetCode(string value)
    {

        string[] code = value.Split('-');

        return code;
    }
    private void DisplayRecords()
    {
        hdnTransID.Value = string.Empty;
        hdnTransferTransID.Value = string.Empty;
        lblerrpaidAll.Text = "";
        if (ddlTransactionsType.SelectedItem.Text != "Transfers")
        {
            div11.Visible = false;
            BtnUpdatePaidAll.Text = "Update All";
            txtCheckAllPaid.Text = "";
            txtCheckNumber.Text = "";
            txtTotalAmount.Text = "";
        }
        if (ddlChapters.SelectedIndex != 0 && (ddlReportDate.Items.Count > -1 || txtReport.Text !=""))
        {
            String ReportDate;
            try
            {
                if (((reportold.Visible == true && ddlReportDate.SelectedItem.Text != "Select Date" && ddlReportDate.SelectedItem.Text != "New Filing") || (reportnew.Visible == true && txtReport.Text != "")) && (Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "37") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "38") || (Session["RoleId"].ToString() == "5")))
               //  if (((ddlReportDate.SelectedItem.Text.Trim() != "Select Date" || txtReport.Text != "") && (ddlReportDate.SelectedItem.Text.Trim() != "New Filing"|| txtReport.Text != "")) && (Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "37") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "38") || (Session["RoleId"].ToString() == "5")))
                {
                    if (reportnew.Visible == true && txtReport.Text != "")
                    {
                        ReportDate = txtReport.Text.ToString();
                        Session["ReportDate"] = ReportDate;
                        reportold.Visible = true;
                        GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value), true);
                        reportnew.Visible = false;
                    }
                    else if (reportold.Visible == true && ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing") //ddlReportDate.SelectedIndex > -1 &&
                    {
                        ReportDate = ddlReportDate.SelectedItem.Text.ToString();
                    }
                    else
                    {
                        ReportDate="01/01/1900";
                    }
                    GetRecords(int.Parse(ddlChapters.SelectedItem.Value.ToString()),ReportDate );
                 
                    if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "84") || (Session["RoleId"].ToString() == "38"))
                    {
                        GridView1.Columns[1].Visible = true;
                        GridView1.Columns[2].Visible = true;
                        GridView2.Columns[1].Visible = true;
                        GridView2.Columns[2].Visible = true;
                    }
                    else if (Session["RoleId"].ToString() == "5") 
                    {
                        GridView2.Columns[1].Visible = false;
                        GridView2.Columns[2].Visible = false;
                        GridView1.Columns[1].Visible = false;
                        GridView1.Columns[2].Visible = false;
                        GridView1.Columns[3].Visible = false;
                        GridView1.Columns[5].Visible = false;
                        for (int i = 12;i<23;i++)
                            GridView1.Columns[i].Visible = false;
                        GridView2.Columns[15].Visible = false;
                        GridView2.Columns[16].Visible = false;
                        for (int i = 18; i < 21; i++)
                            GridView2.Columns[i].Visible = false;
                    }
                    else
                    {
                        GridView1.Columns[1].Visible = false;
                        GridView1.Columns[2].Visible = false;
                        GridView2.Columns[1].Visible = false;
                        GridView2.Columns[2].Visible = false;
                    }
                }               
           
    }
            catch(Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            }
        
        
    }

    private void GetUserChoice(DropDownList ddlObject)
    {
        string[] Choice = { "ReimbursementForm", "Approve Reimburse","Modify Record", "Search/Update Expenses" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select the Screen", "-1"));
        ddlObject.SelectedIndex = 1; 
    }

    private void GetAcctgTransType()
    {
        String Str;
        if (ddlChapters.SelectedValue =="1")
              Str = "select Convert(varchar,AcctgTransTypeID) + '-' + Code as code, Name  from AcctgTransType where AcctgTransTypeID in (1,6) ";// (1,2,3,4,5,6)";
        else if(ddlChapters.SelectedValue =="108")
              Str = "select Convert(varchar,AcctgTransTypeID) + '-' + Code as code, Name  from AcctgTransType where AcctgTransTypeID in (3) ";// (1,2,3,4,5,6)";
        else if (ddlChapters.SelectedValue =="109")
            Str = "select Convert(varchar,AcctgTransTypeID) + '-' + Code as code, Name  from AcctgTransType where AcctgTransTypeID in (2,4,5,7) ";// (1,2,3,4,5,6)"; '7 -Return ' Added on 09-04-2014 for Returning Donation to the Donor
        else
              Str = "select Convert(varchar,AcctgTransTypeID) + '-' + Code as code, Name  from AcctgTransType where AcctgTransTypeID in (2,6)";
        DataSet dsAcctgTransType = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Str);
        ddlTransactionsType.DataSource = dsAcctgTransType;
        ddlTransactionsType.DataTextField = "Name";
        ddlTransactionsType.DataValueField = "code";
        ddlTransactionsType.DataBind();
        ddlTransactionsType.Enabled = true;
        ddlTransactionsType.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlTransactionsType.SelectedIndex = 0;
    }

    //AcctgTransType

    private void GetAppovalFlag(DropDownList ddlObject)
    {
        string[] flag =  { "Pending", "Approved" ,"Rejected"};
        ddlObject.DataSource = flag;
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;

    }

    private void GetEventYear(DropDownList ddlObject)
    {
        int[] year = new int[5];

        year[0] = DateTime.Now.Year;
        year[1] = DateTime.Now.AddYears(-1).Year;
        year[2] = DateTime.Now.AddYears(-2).Year;
        year[3] = DateTime.Now.AddYears(-3).Year;
        year[4] = DateTime.Now.AddYears(-4).Year;
        ddlObject.DataSource = year;
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;

    }
    private void GetEventYear1(ListBox ddlObject)
    {
        int[] year = new int[5];

        year[0] = DateTime.Now.Year;
        year[1] = DateTime.Now.AddYears(-1).Year;
        year[2] = DateTime.Now.AddYears(-2).Year;
        year[3] = DateTime.Now.AddYears(-3).Year;
        year[4] = DateTime.Now.AddYears(-4).Year;
        ddlObject.DataSource = year;
        ddlObject.DataBind();      

    }

    #endregion

    #region " Private Methods - Data Access Layer "

    private void GetTreatment(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Convert(varchar,Treatid) + '-' + TreatCode as code,Case when TreatID  in (1,2,6)then TreatDesc + ' � 100%' Else TreatDesc End as TreatDesc,Case when TreatID = 3 Then 1 Else Case when TreatID = 4 Then 2 Else Case when TreatID = 5 Then 3 Else Case when TreatID = 1 Then 4 Else Case when TreatID = 6 Then 5 Else Case when TreatID = 2 Then 6 End End End End End  End as RowNo from Treatment Order by RowNo");

        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "TreatDesc";
        ddlObject.DataValueField = "code";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }


    private void GetRevCat(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT   RevCatDesc, RevCatID FROM  RevCat where RevCatID in (1,2,3)");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "RevCatDesc";
        ddlObject.DataValueField = "RevCatID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetRevCat(DropDownList ddlObject,String i)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT   RevCatDesc, RevCatID FROM  RevCat where RevCatID in (" + i + ")");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "RevCatDesc";
        ddlObject.DataValueField = "RevCatID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetRevCatFee(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT   RevCatDesc, RevCatID FROM  RevCat where RevCatID in (4,5)");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "RevCatDesc";
        ddlObject.DataValueField = "RevCatID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetReportDate(DropDownList ddlObject,int ChaptID,Boolean RepDate_Session)
    {
        ////string StrSql = "SELECT MAX(CONVERT(VARCHAR(10), ReportDate, 101)) FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag = 'Pending') AND (ReportDate IS NOT NULL)";
       string StrSql = "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10)";
       int count1= int.Parse (SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSql).ToString());
       if (count1 > 0)
       {
           reportnew.Visible = false;
           reportold.Visible = true;
           //StrSql = "SELECT DISTINCT CONVERT(VARCHAR(10), ReportDate, 101) AS ReportDate  FROM ExpJournal  WHERE      (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag like 'Pending')";
           StrSql = "SELECT DISTINCT CAST(Reportdate AS date) ,CONVERT(VARCHAR(10), CAST(Reportdate AS date), 101) as ReportDate  FROM ExpJournal  WHERE      (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) GROUP By CAST(Reportdate AS date) Order by CAST(Reportdate AS date) DESC";
           DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSql);
           ddlObject.DataSource = dsChoice;
           ddlObject.DataTextField = "ReportDate";
           ddlObject.DataValueField = "ReportDate";
           ddlObject.DataBind();
           ddlObject.Enabled = true;
           ddlObject.Items.Insert(0, new ListItem("Select Date", "-1"));
           ddlObject.Items.Insert(1, new ListItem("New Filing", "-2"));

           /*********** Added on 06-06-2014 for Issue with ReportDate drop down moving to default value ***************/
           if (RepDate_Session == false)
               ddlObject.SelectedIndex = 0;
           else
               ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText(Convert.ToDateTime(Session["ReportDate"]).ToString("MM/dd/yyyy")));
       }
       else
       {
           reportnew.Visible = true;
           reportold.Visible = false;
           txtReport.Text = DateTime.Now.ToShortDateString();
       }
    
    }
    //** ferdine 27/05/2010 
    //private void GetViewReport(ListBox ddlObject, int ChaptID)
    //{
    //    LstReport.Items.Clear();
    //    string StrSql ="SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101)) FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag LIKE 'Approved') AND (CONVERT(VARCHAR(10),ReportDate, 101) NOT IN(SELECT DISTINCT CONVERT(VARCHAR(10), ReportDate, 101) FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag LIKE 'Pending')))";
    //    int count1 = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, StrSql).ToString());
    //    if (count1 > 0)
    //    {
    //        StrSql = "SELECT DISTINCT CONVERT(VARCHAR(10), ReportDate, 101) AS ReportDate FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag LIKE 'Approved') AND (CONVERT(VARCHAR(10),ReportDate, 101) NOT IN(SELECT DISTINCT CONVERT(VARCHAR(10), ReportDate, 101) FROM ExpJournal WHERE (ChapterID = " + ChaptID + ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) AND (ChapterApprovalFlag LIKE 'Pending')))";
    //        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSql);
    //        ddlObject.DataSource = dsChoice;
    //        ddlObject.DataTextField = "ReportDate";
    //        ddlObject.DataValueField = "ReportDate";
    //        ddlObject.DataBind();
    //        lblReport1.Text = "";
    //     }
    //    else 
    //      lblReport1.Text = "No Closed Report Date";

    //}
    private void GetSponser(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT SponsorDesc, SponsorId FROM SponsorCat");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "SponsorDesc";
        ddlObject.DataValueField = "SponsorId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetDonPur(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT PurposeDesc, PurposeID FROM DonationPurpose");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "PurposeDesc";
        ddlObject.DataValueField = "PurposeID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetSalesType(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT SalesCatDesc, SalesCatID FROM SalesCat");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "SalesCatDesc";
        ddlObject.DataValueField = "SalesCatID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }

    private void GetEvents(DropDownList ddlObject,String EvntIDs)
    {
        DataSet dsEvent = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Name,EventId  from Event where EventID in (" + EvntIDs + ") Group by Name,EventId");
        ddlObject.DataSource = dsEvent;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "EventId";
        ddlObject.DataBind();
        if (dsEvent.Tables[0].Rows.Count > 1)
        {
            ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
        }
        else
            ddlObject.Enabled = false;
    }

    private void GetChapter(DropDownList ddlObject, int LoginID)
    {
        string ChapterID = string.Empty;
        string strQuery = string.Empty;
        if (Session["RoleId"] != null)
        {
            strQuery = "select ChapterId,ChapterCode,[National],Finals from Volunteer where memberid = " + LoginID + " And RoleId = " + Session["RoleId"].ToString();
            if (Session["SelChapterID"] != null)//.ToString()!="")
            {
                strQuery = strQuery + " and chapterID in (" + Session["SelChapterID"] + ")";
            }
        }
        else
        {
            strQuery = "select ChapterId,ChapterCode,[National],Finals from Volunteer where memberid = " + LoginID;
        }
        DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strQuery);
        DataTable Dt = new DataTable();
        Dt = dsChapter.Tables[0];

            ChapterID = Dt.Rows[0]["ChapterID"].ToString();
            this.National = Dt.Rows[0][2].ToString().ToLower();
       
        if ((Dt.Rows[0]["ChapterID"] != null && Dt.Rows[0]["ChapterID"].ToString() != ""))
        {
            ddlObject.DataSource = dsChapter;
            ddlObject.DataTextField = "ChapterCode";
            ddlObject.DataValueField = "ChapterID";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
            ddlObject.SelectedIndex = 1;
            ddlObject.Enabled = false;
            this.IsChapterID = true;
        }
        else if (ChapterID == "" && Dt.Rows[0]["National"].ToString().ToLower() == "y")
        {
            GetChapters(ddlObject);
            ddlObject.Enabled = true;

        }

        else if (Dt.Rows[0]["Finals"].ToString().ToLower() == "y")
        {
            GetChapters(ddlObject);

            for (int i = 0; i <= ddlChapters.Items.Count - 1; i++)
            {
                if (ddlChapters.Items[i].Text == "Home")
                {
                    ddlChapters.SelectedIndex = i;
                    break;
                }

            }

            LocatedAnywhere = true;
            ddlObject.Enabled = false;
        }
        else
        {
            lblReportError.Visible = true;
            lblReportError.Text = "There is a problem.  Please report to your team lead";
        }
    }

    private void GetExpenseCategory(DropDownList ddlObject)
    {
       String strSQL;
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4") //Grant
            strSQL = " select ExpCatDesc, Convert(varchar,[ExpCatID]) + '-' + [ExpCatCode] as Code  from ExpenseCategory where ExpGroup='Grants' order by ExpCatDesc";
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3")//General and Administrative
            strSQL = " select ExpCatDesc, Convert(varchar,[ExpCatID]) + '-' + [ExpCatCode] as Code  from ExpenseCategory where ExpCatID in (1,14,43,24) order by ExpCatDesc";
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7")//Return of Money - 10-04-2014
            strSQL = " select ExpCatDesc, Convert(varchar,[ExpCatID]) + '-' + [ExpCatCode] as Code  from ExpenseCategory where ExpCatID in (44) order by ExpCatDesc"; //44-Return
        else
            strSQL = " select ExpCatDesc, Convert(varchar,[ExpCatID]) + '-' + [ExpCatCode] as Code  from ExpenseCategory where ExpGroup<>'Grants' order by ExpCatDesc";
        DataSet dsCategory = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strSQL);
        ddlObject.DataSource = dsCategory;
        ddlObject.DataTextField = "ExpCatDesc";
        ddlObject.DataValueField = "Code"; 
        ddlObject.DataBind();
        if (ddlObject.Items.Count > 1)
        {
            ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
        }
        else if (ddlObject.Items.Count == 1)
        { 
            ddlObject.Enabled = false;
        }
    }

    private void GetChapters(DropDownList ddlObject)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetChapterAll");
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0; 

    }

      
    private void GetRecords(int ChapterID,String ReportDate)
    {
        try
        {
        btnApproveAll.Enabled = false;
        BtnPaid.Enabled = false;
                          //"SELECT ExpJournal.*,ToCh.ChapterCode as ToChapterCode,CASE WHEN ExpJournal.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as RName,I.FirstName + ' ' + I.Lastname as IncName, T.TreatDesc,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  Left JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID Left Join OrganizationInfo O ON ExpJournal.DonorType = 'OWN' and O.AutoMemberid =ExpJournal.ReimbMemberid Left Join IndSpouse II ON ExpJournal.DonorType <> 'OWN' AND ExpJournal.ReimbMemberid = II.AutoMemberID Left Join Chapter ToCh On ExpJournal.ToChapterID=ToCh.ChapterId INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants') and ExpJournal.ChapterID =" + ddlChapters.SelectedValue.ToString() + "and ExpJournal.ReportDate = '" + ddlReportDate.SelectedItem.Text.ToString() + "' ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.ExpCatCode");
        hdnSQlQuery.Value = "SELECT ExpJournal.*,ToCh.ChapterCode as ToChapterCode,CASE WHEN ExpJournal.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as RName,I.FirstName + ' ' + I.Lastname as IncName, T.TreatDesc,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  Left JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID Left Join OrganizationInfo O ON ExpJournal.DonorType = 'OWN' and O.AutoMemberid =ExpJournal.ReimbMemberid Left Join IndSpouse II ON ExpJournal.DonorType <> 'OWN' AND ExpJournal.ReimbMemberid = II.AutoMemberID Left Join Chapter ToCh On ExpJournal.ToChapterID=ToCh.ChapterId INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID left JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID Inner Join AcctgTransType ATT ON ATT.Code = ExpJournal.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 7) where  ExpJournal.ChapterID =" + ChapterID + " and CAST(ExpJournal.ReportDate as Date)=  cast('" + ReportDate + "' as Date) ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.ExpCatCode";
            DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, hdnSQlQuery.Value);

          //Response.Write(hdnSQlQuery.Value);
        //  = "SELECT ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName, T.TreatDesc,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='E' and ExpJournal.ChapterID =" + ChapterID + "and ExpJournal.EventYear = " + year + " ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.ExpCatCode";
            if (Session["RoleId"].ToString() == "5")
                Session["StrSQLExp"] = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc as Treat_Code,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as ReimbursedTo,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,E.DonorType,ToCh.ChapterCode as ToChapter,Ev.Name As Event FROM ExpJournal E Left JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse II ON E.DonorType <> 'OWN' AND E.ReimbMemberid = II.AutoMemberID Left Join Chapter ToCh On E.ToChapterID=ToCh.ChapterId INNER JOIN Treatment T ON T.TreatID=E.TreatID left JOIN EVENT Ev ON Ev.EventID=E.EventID  Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 7) where E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "' as Date) order by E.reportdate";
             else
                Session["StrSQLExp"] = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc as Treat_Code,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as ReimbursedTo,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,E.DonorType,ToCh.ChapterCode as ToChapter,E.ChapterApprover as ChApprover,E.NationalApprover as NatApprover,E.NationalApprovalFlag As NatApproval,E.CheckNumber,E.DatePaid,E.ExpCatID,E.Account,E.ReimbMemberID,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E Left JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID Left Join Chapter ToCh On E.ToChapterID=ToCh.ChapterId Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse II ON E.DonorType <> 'OWN' AND E.ReimbMemberid = II.AutoMemberID INNER JOIN Treatment T ON T.TreatID=E.TreatID left JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID  Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 7) where  E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "' as Date) order by E.reportdate";
            DataTable dt = dsRecords.Tables[0];

            DataView dv = new DataView(dt);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.Columns[1].Visible = true;
            GridView1.Columns[2].Visible = true;
           // panel6.Visible = false;
            panel3.Visible = true;
          
            if (dt.Rows.Count == 0)
            {
                Pnl3Msg.Text = "No Expense Record to Display";
                lblExpenseTot.Text = "";
                lblTobePaid.Text = "";
                BtnExpExport.Enabled = false;
            }
            else
            {
                Pnl3Msg.Text = "";
                if (Session["RoleId"].ToString() == "5" || Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "38")
                            btnApproveAll.Enabled = true;
                if (Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1" || (Session["RoleId"].ToString() == "38" && this.National == "y"))
                            BtnPaid.Enabled = true;
                lblExpenseTot.Text = String.Empty;
                lblTobePaid.Text= String.Empty;
                SqlDataReader Readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select SUM(E.ExpenseAmount) As TotAmount,T.TreatDesc,E.TreatCode ,E.ChapterID  from ExpJournal E Inner Join Treatment T ON E.TreatCode = T.TreatCode WHERE E.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') and E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "' as Date)  Group By E.TreatCode,T.TreatDesc ,E.ChapterID");
                while (Readr.Read())
                {
                    lblExpenseTot.Text = lblExpenseTot.Text + " " + Readr["TreatDesc"].ToString() + ": $ " + Math.Round(Decimal.Parse(Readr["TotAmount"].ToString()), 2);
                }
                Readr.Close();
                decimal tot = 0;
                decimal tobepaid =0 ,totalExpAmt =0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tot = tot + Convert.ToDecimal(dt.Rows[i]["ExpenseAmount"].ToString().Trim());
                }
                lblExpenseTot.Text = lblExpenseTot.Text + " Total Expense : $" + Math.Round(Decimal.Parse(tot.ToString()), 2);
                BtnExpExport.Enabled = true;
                SqlDataReader Readr1 = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select SUM(E.ExpenseAmount) As Paid,E.ChapterID  from ExpJournal E Inner Join Treatment T ON E.TreatCode = T.TreatCode WHERE E.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') and E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "'  as Date) and E.CheckNumber > 0 Group By E.ChapterID");
                while (Readr1.Read())
                {
                    lblTobePaid.Text = lblTobePaid.Text + " Paid : $ " + Math.Round(Decimal.Parse(Readr1["Paid"].ToString()), 2);
                    totalExpAmt = totalExpAmt + Math.Round(Decimal.Parse(Readr1["Paid"].ToString()), 2);
                }
                Readr1.Close();
                SqlDataReader Readr2 = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select SUM(E.ExpenseAmount) As Owed,E.ChapterID  from ExpJournal E Inner Join Treatment T ON E.TreatCode = T.TreatCode WHERE E.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') and E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "' as Date) and T.TreatDesc in ('Pay Vendor','To be Reimbursed','Grantee') Group By E.ChapterID");
                while (Readr2.Read())
                {
                    lblTobePaid.Text = lblTobePaid.Text + " Owed : $ " + Math.Round(Decimal.Parse(Readr2["Owed"].ToString()), 2);
                    tobepaid = tobepaid + Math.Round(Decimal.Parse(Readr2["Owed"].ToString()), 2);
                }
                Readr2.Close();
                if (lblTobePaid.Text != "")
                {
                    lblTobePaid.Text = lblTobePaid.Text + " To be Paid : $ " + (totalExpAmt - tobepaid);
                }

                SqlDataReader Readr3 = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select SUM(E.SpAmount) As SpAmount,E.ChapterID  from ExpJournal E Inner Join Treatment T ON E.TreatCode = T.TreatCode WHERE E.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') and E.ChapterID =" + ChapterID + " AND CAST(E.ReportDate as Date)=  cast('" + ReportDate + "' as Date) and T.TreatDesc in ('Pay Vendor','To be Reimbursed','Grantee','Donation','Sponsor', 'Took from Cash') Group By E.ChapterID");
                while (Readr3.Read())
                {
                    lblTobePaid.Text = lblTobePaid.Text + " Total SpAmount : $ " + Math.Round(Decimal.Parse(Readr3["SpAmount"].ToString()), 2);
                }
                Readr3.Close();
              
            }
        

        //transfers
            if (ddlChapters.SelectedValue == "109") //"1" Modified on Sep 06 2013,as TransType'Transfers' is moved under Chapter- US_HomeOffice,109
            {
                DataSet dsTransfer = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select EJ.TransactionID,EJ.TransType,EJ.BanKID, EJ.TobankID, EJ.CheckNumber, EJ.ExpenseAmount, EJ.DatePaid,EJ.ReportDate,EJ.EventYear,B1.BankCode as BankFrom ,B2.BankCode as BankTo,Ch.ChapterCode,EJ.RestTypeFrom, EJ.RestTypeTo from ExpJournal EJ Inner Join Bank B1 On B1.BankID = EJ.BanKID Inner Join Bank B2 On B2.BankID = EJ.TobankID Inner Join Chapter Ch On Ch.ChapterID=EJ.ChapterID Where CAST(EJ.ReportDate as Date)=  cast('" + ReportDate + "' as Date) and EJ.TransType = 'Transfers'");
                DataTable dtTrans = dsTransfer.Tables[0];
                DataView dvTrans = new DataView(dtTrans);
                gvTransfer.DataSource = dtTrans;
                gvTransfer.DataBind();
                gvTransfer.Visible = true;
                if (dtTrans.Rows .Count > 0)
                    BtnExpExport.Enabled = true;
            }
            else
                gvTransfer.Visible = false; 

       
             dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" + ChapterID + "and ExpJournal.ReportDate = '" + ReportDate + "' ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.RevCatCode");
           // Session["StrSQL"] = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='R' and ExpJournal.ChapterID =" + ChapterID + "and ExpJournal.EventYear = " + year + " ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.RevCatCode";
            if (Session["RoleId"].ToString() == "5")
                Session["StrSQLRev"] = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" + ChapterID + " AND E.ReportDate='" + ReportDate + "' order by E.reportdate";
            //StrSQL = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType='" & type & "' and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & "and ReportDate in (" & reportdates() & ") order by reportdate"
            else
                Session["StrSQLRev"] = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.PaymentMethod,E.CreatedBy,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.ChapterApprover as ChApprover,E.NationalApprovalFlag as NatApproval,E.NationalApprover as NationalApprover,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E  INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" + ChapterID + " AND E.ReportDate='" + ReportDate + "' order by E.reportdate";
               
             dt = dsRecords.Tables[0];
             dv = new DataView(dt);
            GridView2.DataSource = dt;
            GridView2.DataBind();
            panel6.Visible = true;
          //  panel3.Visible = false;
            if (dt.Rows.Count == 0)
            {
                Pnl6Msg.Text = "No Revenue Record to Display";
                lblRevenueTot.Text = "";
                BtnRevExport.Enabled = false; 
            }
            else
            {
                Pnl6Msg.Text = "";
                if (Session["RoleId"].ToString() == "5" || Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "38")
                     btnApproveAll.Enabled = true;
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tot = tot + Convert.ToDecimal(dt.Rows[i]["ExpenseAmount"].ToString().Trim());
                }
                lblRevenueTot.Text = "Total Revenue : $" + Math.Round(Decimal.Parse(tot.ToString()), 2);
                BtnRevExport.Enabled = true;
            }
        
        }
    catch(Exception ex)
    {
           //Response.Write(ex.ToString());
    }

    }
    private void GetStates()
    {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("Select State", String.Empty));                   
        ddlState.SelectedIndex = 0;
    }

   
    private string GetDonPurCode(int DonPurId)
    {
        string strsql = "SELECT PurposeCode FROM DonationPurpose WHERE PurposeID =" + DonPurId;
        SqlDataReader Readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, strsql);
        while (Readr.Read())
        {
            return Readr[0].ToString ();
        }
        return null;
    }
    private string GetSponCode(int SponId)
    {
        string strsql = "SELECT SponsorCode FROM SponsorCat WHERE SponsorId =" + SponId;
        SqlDataReader Readr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, strsql);
        while (Readr.Read())
        {
            return Readr[0].ToString();
        }
        return null;
    }
            
    private int AddExpensejournal(Expensejournal journal)
    { 
        object value;

        int Value_1;
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string sqlCommand = "Sp_ExpJournal";
            String SqlStr = "Select Account from ExpenseCategory  where  ExpCatID=" + journal.expenseCategoryID;
            SqlParameter[] param = new SqlParameter[23];
            //param[0] = new SqlParameter("@IncMemberID", journal.IncMemberId);
            if (journal.IncMemberId != -1)
                param[0] = new SqlParameter("@IncMemberId ", journal.IncMemberId);
            else
                param[0] = new SqlParameter("@IncMemberId ", DBNull.Value);
            param[1] = new SqlParameter("@ReimbMemberID", journal.ReimMemberId);
            param[2] = new SqlParameter("@ExpCatID", journal.expenseCategoryID);
            param[3] = new SqlParameter("@expenseAmount", journal.expenseAmount);
            if (journal.eventID == -1)
                param[4] = new SqlParameter("@eventID", null);
            else
                param[4] = new SqlParameter("@eventID", journal.eventID);

            param[5] = new SqlParameter("@ExpCatCode", journal.expenseCatCode);
            param[6] = new SqlParameter("@dateIncurred", journal.dateIncurred);
            param[7] = new SqlParameter("@treatID", journal.treatementID);
            param[8] = new SqlParameter("@chapterID", journal.chapterID);
            param[9] = new SqlParameter("@treatCode", journal.treatmentCode);
            param[10] = new SqlParameter("@nationalApprovalFlag", journal.nationalAprovalFlag);
            param[11] = new SqlParameter("@chapterApprovalFlag", journal.chapterApprovalFlag);
            param[12] = new SqlParameter("@EventYear", journal.eventYear);
            param[13] = new SqlParameter("@Comment", journal.comments);
            param[14] = new SqlParameter("@CreatedDate", DateTime.Now);
            param[15] = new SqlParameter("@CreatedBy", int.Parse(Session["LoginID"].ToString()));
            param[16] = new SqlParameter("@Account", SqlHelper.ExecuteScalar(conn, CommandType.Text, SqlStr));
            param[17] = new SqlParameter("@Report", journal.Report);
            param[18] = new SqlParameter("@VendorName", journal.VendorName);
            param[19] = new SqlParameter("@DonorType", journal.DonorType);
            if (journal.tochapterID == -1)
                param[20] = new SqlParameter("@ToChapterID", DBNull.Value);
            else
                param[20] = new SqlParameter("@ToChapterID", journal.tochapterID);
            param[21] = new SqlParameter("@TransType", journal.TransType);
            param[22] = new SqlParameter("@SpAmount", journal.spAmount);
         
            
            //value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
            Value_1 = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

            //if (value == null)
            //{
            //    return -1;
            //    lblMessage.Text = "You are inserting duplicate record";
            //}
   if (Value_1 >1)
            {
                DisplayRecords();
            }

            return (int)Value_1;
         
            //GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value)); 
    
         
    }

    private int AddRevenuejournal(Revenuejournal revenue)
    {
        try
        {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        String SqlStr = "Select Account from RevCat  where  RevCatID=" + revenue.RevCatID;
        int account = int.Parse(SqlHelper.ExecuteScalar(conn, CommandType.Text, SqlStr).ToString()) ;
        String AddSql = "INSERT INTO ExpJournal(ChapterID, EventYear, EventID,ExpenseAmount, DateIncurred, Comments,CreatedDate, CreatedBy, DatePaid,TransType,ChapterApprovalFlag,NationalApprovalFlag,ReportDate,";
        AddSql = AddSql + "RevSource, RevCatID, RevCatCode, Account, PaymentMethod, SponsorID,SponsorCode, DonPurposeID, DonPurposeCode, FeesType, SalesCatID,SalesCatCode,ProviderName) VALUES("; 
        AddSql = AddSql + revenue.RevChapterID  + "," + revenue.RevEventYear + "," + revenue.RevEventID + "," + revenue.RevAmount + ",'" + revenue.DateReceived + "','" + revenue .RevComments + "','" + DateTime.Now + "'," + int.Parse(Session["LoginID"].ToString()) + ",'" + revenue .DateReceived + "','" + revenue.TransType   + "','" + revenue.chapterApprovalFlag + "','" + revenue.nationalAprovalFlag + "','" + revenue .RevReport + "','";
        AddSql = AddSql + revenue.RevSource + "'," + revenue.RevCatID + ",'" + revenue.RevCatCode + "'," + account + ",'" + revenue.PaymentMethod + "',";
        if (revenue.RevSource == "Sponsorship")
        {
            AddSql = AddSql + revenue.SponsorID +",'" + revenue.SponsorCode + "',NULL,NULL,NULL,NULL,NULL"; 
        }
        else if (revenue.RevSource == "Donation")
        {
            AddSql = AddSql + "NULL,NULL," + revenue.DonPurposeID  + ",'" + revenue.DonPurposeCode + "',NULL,NULL,NULL";
        }
        else if (revenue.RevSource == "Fees")
        {
            AddSql = AddSql + "NULL,NULL,NULL,NULL,'" + revenue.FeesType + "',NULL,NULL"; 
        }
        else if (revenue.RevSource == "Sales")
        {
            AddSql = AddSql + "NULL,NULL,NULL,NULL,NULL," + revenue .SalesCatId + ",'" + revenue .SalesCatCode +"'";  
        }
        AddSql = AddSql + ",'" + revenue.Givername  +"')";
        //Response.Write(AddSql);
        value = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, AddSql);
        if (value == null)
        {
            //lblMessage.Text = lblMessage.Text + "<br> You are inserting duplicate record";
            return -1;            
        }        
        return (int)value;
    }
    catch (Exception ex)
    {
        lblMessage.Text = ex.ToString();
        lblMessage.Text = lblMessage.Text + " : : " +DdlRevSprType.SelectedItem.Value;
        return -1;
    }
    }
    //Update Revenue Record

    private int UpdateRevenuejournal(Revenuejournal revenue)
    {
        if (Session["transID"] != null)
        {
            object value;
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            String SqlStr = "Select Account from RevCat  where  RevCatID=" + revenue.RevCatID;
            int account = int.Parse(SqlHelper.ExecuteScalar(conn, CommandType.Text, SqlStr).ToString());
            String AddSql = "UPDATE ExpJournal set ChapterID=" + revenue.RevChapterID + ",EventYear=" + revenue.RevEventYear + ",EventID=" + revenue.RevEventID + ",ExpenseAmount=" + revenue.RevAmount + ",DateIncurred='" + revenue.DateReceived + "',Comments='" + revenue.RevComments + "',ModifiedDate='" + DateTime.Now + "',ModifiedBy=" + int.Parse(Session["LoginID"].ToString()) + ",DatePaid='" + revenue.DateReceived + "',";
            AddSql = AddSql + "RevSource='" + revenue.RevSource + "', RevCatID=" + revenue.RevCatID + ",RevCatCode='" + revenue.RevCatCode + "',Account=" + account + ",PaymentMethod='" + revenue.PaymentMethod + "',ReportDate='" + revenue .RevReport + "',ProviderName='" + revenue .Givername  + "',";
            if (revenue.RevSource == "Sponsorship")
            {
                AddSql = AddSql + "SponsorID=" + revenue.SponsorID + ",SponsorCode='" + revenue.SponsorCode + "'";
            }
            else if (revenue.RevSource == "Donation")
            {
                AddSql = AddSql + "DonPurposeID=" + revenue.DonPurposeID + ",DonPurposeCode='" + revenue.DonPurposeCode + "'";
            }
            else if (revenue.RevSource == "Fees")
            {
                AddSql = AddSql + "FeesType='" + revenue.FeesType + "'";
            }
            else if (revenue.RevSource == "Sales")
            {
                AddSql = AddSql + "SalesCatID=" + revenue.SalesCatId + ",SalesCatCode='" + revenue.SalesCatCode + "'";
            }
            AddSql = AddSql + "WHERE  TransactionID=" + int.Parse(Session["transID"].ToString ());
            value = SqlHelper.ExecuteScalar(conn, CommandType.Text, AddSql);
            if (value == null)
            {
                return -1;
                // lblMessage.Text = "You are inserting duplicate record";
            }
            return (int)value;
        }
        else
            return -1;
    }

    private void UpdatePaid()
    {
        object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set Paid ='Y',RestTypeFrom = 'Unrestricted', DatePaid = '" + DateTime.Now + "' where TransactionID = " + this.TransactionID);
    }
    private void UpdateExpensejournal(Expensejournal journal)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "Sp_UpdateExpJournal";
        String SqlStr = "Select Account from ExpenseCategory  where  ExpCatID=" + journal.expenseCategoryID;
        SqlParameter[] param = new SqlParameter[22];
        param[0] = new SqlParameter("@ExpCatID ", journal.expenseCategoryID);
        param[1] = new SqlParameter("@Comments ", journal.comments);
        param[2] = new SqlParameter("@ExpCatCode ", journal.expenseCatCode);
        param[3] = new SqlParameter("@ExpAmount ", journal.expenseAmount);
        param[4] = new SqlParameter("@DateIncurred ", journal.dateIncurred);
        param[5] = new SqlParameter("@TreatId ", journal.treatementID);
        param[6] = new SqlParameter("@EventYear ", journal.eventYear);
        //param[7] = new SqlParameter("@EventId ", journal.eventID);

        if (journal.eventID == -1)
            param[7] = new SqlParameter("@EventId ", null);
        else
            param[7] = new SqlParameter("@EventId ", journal.eventID);

        if (journal.IncMemberId != -1)
            param[8] = new SqlParameter("@IncMemberId ", journal.IncMemberId);
        else
            param[8] = new SqlParameter("@IncMemberId ", DBNull.Value );
        param[9] = new SqlParameter("@ReimbMemberId ", journal.ReimMemberId);
        param[10] = new SqlParameter("@ChapterId ", journal.chapterID);
        param[11] = new SqlParameter("@Modifiedby ", int.Parse(Session["LoginID"].ToString()));
        param[12] = new SqlParameter("@ModifiedDate ", DateTime.Now);
        param[13] = new SqlParameter("@TransID ", this.TransactionID);
        param[14] = new SqlParameter("@treatCode", journal.treatmentCode);
        param[15] = new SqlParameter("@Report", journal.Report);
        param[16] = new SqlParameter("@Vendorname", journal.VendorName);
        param[17] = new SqlParameter("@DonorType", journal.DonorType);
        if (journal.tochapterID == -1)
            param[18] = new SqlParameter("@ToChapterID", DBNull.Value);
        else
            param[18] = new SqlParameter("@ToChapterID", journal.tochapterID);
        param[19] = new SqlParameter("@TransType", journal.TransType);
        param[20] = new SqlParameter("@Account", SqlHelper.ExecuteScalar(conn, CommandType.Text, SqlStr));
        param[21] = new SqlParameter("@SpAmount", journal.spAmount);
       
        value = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

        //Response.Write(SqlStr);
        //object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set ExpCatID = " + journal.expenseCategoryID + ", Comments =  '" + journal.comments + "', ExpCatCode = '" + journal.expenseCatCode + "', ExpenseAmount =" + journal.expenseAmount + ", DateIncurred = '" + journal.dateIncurred + "', TreatID = " + journal.treatementID +  ", EventYear = " + journal.eventYear +", EventId =" + journal.eventID + ", IncMemberID =" + IncurMemberID + ", ReimbMemberID =" + ReimMemberID + ", ChapterID =" + journal.chapterID + ", ModifiedBy = " + int.Parse(Session["LoginID"].ToString()) + ", ModifiedDate ='" + DateTime.Now + "' where TransactionID = " + this.TransactionID);


        //	string query = 	"update ExpenseJournal Set ExpenseCategory = @expenseCategory,ExpenseDescription = @expenseDescription,ExpenseAmount = @expenseAmount,DateIncurred = @dateIncurred,Treatement = @treatement,Event = @events,IndividualIncurringExpense = @individualIncurringExpenses,PersonReimbursed = @personReimbursing,Chapter = @chapter where TransactionID = @ID";

    }
    private void updateChapterApproval(Expensejournal journal)
    {
        object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set ChapterApprover ='" + journal.chapterApprover + "' , ChapterApprovalDate = '" + DateTime.Now + "' ,ChapterApprovalFlag = '" + journal.chapterApprovalFlag + "' where TransactionID = " + this.TransactionID);
       // return (int) value;
    }

    private void updateNationalApproval(Expensejournal journal)
    {
        object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set NationalApprover ='" + journal.nationalApprover + "', NationalApprovalDate = '" + DateTime.Now  + "', NationalApprovalFlag = '" + journal.nationalAprovalFlag + "' where TransactionID = " + this.TransactionID);
        // return (int) value;
    }
    private void updateRevChapterApproval(Revenuejournal revenue)
    {
        object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set ChapterApprover ='" + revenue.chapterApprover + "' , ChapterApprovalDate = '" + DateTime.Now + "' ,ChapterApprovalFlag = '" + revenue.chapterApprovalFlag + "' where TransactionID = " + this.TransactionID);
        // return (int) value;
    }

    private void updateRevNationalApproval(Revenuejournal revenue)
    {
        //Pnl6Msg.Text = "update ExpJournal Set NationalApprover ='" + revenue.nationalApprover + "', NationalApprovalDate = '" + DateTime.Now + "', NationalApprovalFlag = '" + revenue.nationalAprovalFlag + "' where TransactionID = " + this.TransactionID;
        object value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set NationalApprover ='" + revenue.nationalApprover + "', NationalApprovalDate = '" + DateTime.Now + "', NationalApprovalFlag = '" + revenue.nationalAprovalFlag + "' where TransactionID = " + this.TransactionID);
        // return (int) value;
      
    }

    private void GetSelectedRecord(int transID, string status)
    {

        Panel1.Visible = true;
        PnlRev.Visible = false;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TransID", transID);
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "Sp_GetSelectedRecordToModify", param);
        //DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select * from ExpJournal where TransactionID =" + transID);
        string ExpCatCode = string.Empty;

        this.TransactionID = Int32.Parse(dsRecords.Tables[0].Rows[0]["TransactionID"].ToString());
        if (dsRecords.Tables[0].Rows[0]["IncMemberID"] != DBNull.Value)
                IncurMemberID  = int.Parse(dsRecords.Tables[0].Rows[0]["IncMemberID"].ToString());
        
        ReimMemberID = int.Parse(dsRecords.Tables[0].Rows[0]["ReimbMemberID"].ToString());
        journal.expenseCategoryID = int.Parse(dsRecords.Tables[0].Rows[0]["ExpCatID"].ToString());
        if (dsRecords.Tables[0].Rows[0]["ExpenseAmount"].ToString().Equals(""))
        {
            journal.expenseAmount = 0;
        }
        else
        {
            journal.expenseAmount = decimal.Round(decimal.Parse(dsRecords.Tables[0].Rows[0]["ExpenseAmount"].ToString()), 2);
        }
        
        journal.spAmount = decimal.Round(decimal.Parse(dsRecords.Tables[0].Rows[0]["SpAmount"].ToString()), 2);
        journal.expenseCatCode = dsRecords.Tables[0].Rows[0]["ExpCatCode"].ToString();
        journal.comments = dsRecords.Tables[0].Rows[0]["Comments"].ToString();
        journal.eventYear = int.Parse(dsRecords.Tables[0].Rows[0]["EventYear"].ToString());
        journal.Report = DateTime.Parse(dsRecords.Tables[0].Rows[0]["ReportDate"].ToString());
        journal.dateIncurred = DateTime.Parse(dsRecords.Tables[0].Rows[0]["DateIncurred"].ToString());
        journal.treatementID = int.Parse(dsRecords.Tables[0].Rows[0]["TreatID"].ToString());
        journal.chapterID =int.Parse(dsRecords.Tables[0].Rows[0]["ChapterID"].ToString());
        journal.nationalAprovalFlag = dsRecords.Tables[0].Rows[0]["NationalApprovalFlag"].ToString();
        journal.chapterApprovalFlag = dsRecords.Tables[0].Rows[0]["ChapterApprovalFlag"].ToString().ToLower();
        journal.TransType = dsRecords.Tables[0].Rows[0]["TransTypeName"].ToString();
        ddlTransactionsType.SelectedIndex = ddlTransactionsType.Items.IndexOf(ddlTransactionsType.Items.FindByText(dsRecords.Tables[0].Rows[0]["TransTypeName"].ToString()));
        SetForTransType();
        if (dsRecords.Tables[0].Rows[0]["EventID"] == DBNull.Value) 
            journal.eventID = -1;
        else
            journal.eventID = int.Parse(dsRecords.Tables[0].Rows[0]["EventID"].ToString());        
        journal.DonorType = dsRecords.Tables[0].Rows[0]["DonorType"].ToString();
        journal.VendorName = dsRecords.Tables[0].Rows[0]["ProviderName"].ToString();

        if (dsRecords.Tables[0].Rows[0]["IncMemberID"] != DBNull.Value)
            txtIncurrEx.Text = dsRecords.Tables[0].Rows[0]["Incrmember"].ToString();

        txtReimburse.Text = dsRecords.Tables[0].Rows[0]["RembMember"].ToString();
        ViewState["DonorType"] = dsRecords.Tables[0].Rows[0]["DonorType"].ToString();
        // Aug 13 2011 request to remove Payvendor for IND/Spouse and Romove to be Reimb for OWN
        string strsql = ViewState["DonorType"].ToString () == "OWN" ? "4" : "3";
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "7")
        {
            DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Convert(varchar,Treatid) + '-' + TreatCode as code,Case when TreatID in (1,2,6) then TreatDesc + ' � 100%' Else TreatDesc End as TreatDesc,Case when TreatID = 3 Then 1 Else Case when TreatID = 4 Then 2 Else Case when TreatID = 5 Then 3 Else Case when TreatID = 1 Then 4 Else Case when TreatID = 6 Then 5 Else Case when TreatID = 2 Then 6 End End End End End  End as RowNo from Treatment where TreatID in (1,2," + strsql + ",5,6)  order by RowNo ");
            ddlTreatement.DataSource = dsChoice;
            ddlTreatement.DataTextField = "TreatDesc";
            ddlTreatement.DataValueField = "code";
            ddlTreatement.SelectedIndex = 0;
            ddlTreatement.DataBind();
        }


        txtExAmount.Text = journal.expenseAmount.ToString();
        txtSpAmount.Text = journal.spAmount.ToString();
        txtExDes.Text = journal.comments.ToString();
        txtDate.Text =  journal.dateIncurred.ToString("d");
        txtVendorName.Text = journal.VendorName.ToString();
        ddlReportDate.Items.Clear();
        ddlReportDate.Items.Insert(0, new ListItem(journal.Report.ToString("d"), journal.Report.ToString("d")));
        ddlReportDate.SelectedIndex = 0;
        for (int i = 0; i <= ddlEvent.Items.Count - 1; i++)
        {
            if (ddlEvent.Items[i].Value == journal.eventID.ToString())
            {
                ddlEvent.SelectedIndex = i;
                break;
            }
        }
        GetExpenseCategory(ddlExCategory);
        for (int i = 0; i <= ddlExCategory.Items.Count - 1; i++)
        {
            ExpCatCode = GetCode(ddlExCategory.Items[i].Value.ToString())[1];
            if (ExpCatCode.Trim() == journal.expenseCatCode.ToString())
            {
                ddlExCategory.SelectedIndex = i;
                break;
            }
        }  

        for (int i = 0; i <= ddlChapters.Items.Count - 1; i++)
        {
            if (ddlChapters.Items[i].Value == journal.chapterID.ToString())
            {
                ddlChapters.SelectedIndex = i;
                break;
            }
        }
     
        if (dsRecords.Tables[0].Rows[0]["ToChapterID"] != DBNull.Value)
        {
            journal.tochapterID = int.Parse(dsRecords.Tables[0].Rows[0]["ToChapterID"].ToString());
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue(journal.tochapterID.ToString()));
        }
         else
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue(ddlChapters.SelectedValue));


        if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3" || ddlEvent.SelectedValue == "1") && ddlChapters.SelectedValue == "1")
        {
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue("1"));
            ddlToChapter.Enabled = false;
        }
        else if (ddlChapters.SelectedValue == "1")
        {
            ddlToChapter.Enabled = true;
            //ddlToChapter.SelectedIndex = 0;
        }

        for (int i = 0; i <= ddlYear.Items.Count - 1; i++)
        {
            if (ddlYear.Items[i].Text == journal.eventYear.ToString())
            {
                ddlYear.SelectedIndex = i;
                break;
            }
        }
        
        for (int i = 0; i <=ddlTreatement.Items.Count - 1; i++)
        {
            if (GetCode(ddlTreatement.Items[i].Value)[0] == journal.treatementID.ToString())
            {
                ddlTreatement.SelectedIndex = i;
                break;
            }
        }

        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4")
        {
            ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("Grantee"));
            ddlTreatement.Enabled = false;
        }
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7")
        {
            ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("To be Reimbursed"));
            ddlTreatement.Enabled = false;
        }
        else
        {
            ddlTreatement.Enabled = true;
        }
        
        for (int i = 0; i <= ddlNationalApproverFlag.Items.Count - 1; i++)
        {
            if (ddlNationalApproverFlag.Items[i].Text.ToLower() == journal.nationalAprovalFlag.ToLower())
            {
                ddlNationalApproverFlag.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i <= ddlChapterApprovalFlag.Items.Count - 1; i++)
        {
            if (ddlChapterApprovalFlag.Items[i].Text.ToLower() == journal.chapterApprovalFlag.ToLower())
            {
                ddlChapterApprovalFlag.SelectedIndex = i;
                break;
            }
        }
        if(status == "Approve")
        {
           // Panel2.Visible = true;
            lblMessage.Text = "";
            txtIncurrEx.Enabled = false;
            txtReimburse.Enabled = false;
            ddlExCategory.Enabled = false;
            txtExAmount.Enabled = false;
            txtSpAmount.Enabled = false;
            ddlEvent.Enabled = false;
            txtExDes.Enabled = false;
            txtDate.Enabled = false;
            ddlTreatement.Enabled = false;
            ddlChapters.Enabled = false;
            ddlYear.Enabled = false;
            ddlReportDate.Enabled = false;
            if (Session["RoleId"].ToString() == "5")
            {
                Panel2.Visible = true;
            }
            if (Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1")
            {
                Panel5.Visible = true;
            }
            if (Session["RoleId"].ToString() == "38" && this.National == "y")
            {
                Panel5.Visible = true;
            }
            
            else if(Session["RoleId"].ToString() == "38"  &&  this.IsChapterID == true)
            {
                Panel2.Visible =  true;
            }
            if (btnAdd.Text == "Add" || btnAdd.Text== "Save")
            {
                btnAdd.Text = "Update";
            }
        }
        else if (status == "Modify")
        {
            lblMessage.Text = "";
            txtIncurrEx.Enabled = false;
            txtReimburse.Enabled = false;

            txtExAmount.Enabled = true;
            txtSpAmount.Enabled = true;
            //txtReport.Enabled = true;
            ddlReportDate.Enabled = true;
            if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7")
            {
                ddlEvent.Enabled = false;
                ddlTreatement.Enabled = false;
                ddlExCategory.Enabled = false;
            }
            else
            {
                ddlEvent.Enabled = true;
                ddlExCategory.Enabled = true;
                ddlTreatement.Enabled = true;
            }
            txtExDes.Enabled = true;
            txtDate.Enabled = true;
           
            ddlChapters.Enabled = true;
            ddlYear.Enabled = true;
            Panel2.Visible = false;
            Panel5.Visible = false;
            if (btnAdd.Text == "Add" || btnAdd.Text == "Update")
            {
                btnAdd.Text = "Save";
            }
        }

        else if (status == "Paid")
        {
            UpdatePaid();
        }               
    }
    private void GetTransSelectedRecord(int transID, string status)
    {
        lblMessage.Text = "";
        disableRevControls();
        hdnTransferTransID.Value = transID.ToString();
        Session["transID"] = transID;
        String str = "Select * from  ExpJournal where  TransactionID=" + transID;
       //Response.Write(str);
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, str);
        //"Insert Into ExpJournal(ChapterID,TransType,BanKID, TobankID, CheckNumber, ExpenseAmount, DatePaid,ReportDate,EventYear,CreatedBy,CreatedDate,RestTypeFrom,RestTypeTo) values (" + ddlChapters.SelectedValue + ",'" + GetCode(ddlTransactionsType.SelectedValue.ToString())[1] + "'," + ddlBankId.SelectedValue + "," + ddlPayTo.SelectedValue + ",'" + txtCheckNumber.Text + "'," + txtTotalAmount.Text + ",'" + txtCheckAllPaid.Text + "','" + Reportdte + "'," + ddlYear.SelectedValue + "," + Session["LoginID"] + ",GetDate(),'"+ ddlRestTypeFrom.SelectedValue  +"','" + ddlRestTypeTo.SelectedValue +  "') "
        ddlTransactionsType.SelectedIndex = ddlTransactionsType.Items.IndexOf(ddlTransactionsType.Items.FindByText("Transfers"));
        SetForTransType();
        ddlBankId.SelectedIndex = ddlBankId.Items.IndexOf(ddlBankId.Items.FindByValue(dsRecords.Tables[0].Rows[0]["BanKID"].ToString()));
        ddlPayTo.SelectedIndex = ddlPayTo.Items.IndexOf(ddlPayTo.Items.FindByValue(dsRecords.Tables[0].Rows[0]["TobankID"].ToString()));
        txtCheckNumber.Text = dsRecords.Tables[0].Rows[0]["CheckNumber"].ToString();
        txtTotalAmount.Text = Math.Round(Convert.ToDecimal(dsRecords.Tables[0].Rows[0]["ExpenseAmount"].ToString()), 2).ToString();//Math.Round(Convert.ToDecimal(dsRecords.Tables[0].Rows[0]["ExpenseAmount"].ToString(),2).ToString ;
        txtCheckAllPaid.Text = DateTime.Parse(dsRecords.Tables[0].Rows[0]["DatePaid"].ToString()).ToString("d");
        ddlRestTypeFrom.SelectedIndex = ddlRestTypeFrom.Items.IndexOf(ddlRestTypeFrom.Items.FindByValue(dsRecords.Tables[0].Rows[0]["RestTypeFrom"].ToString()));
        ddlRestTypeTo.SelectedIndex = ddlRestTypeTo.Items.IndexOf(ddlRestTypeTo.Items.FindByValue(dsRecords.Tables[0].Rows[0]["RestTypeTo"].ToString()));
        ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue(dsRecords.Tables[0].Rows[0]["EventYear"].ToString()));
    }

    private void GetRevSelectedRecord(int transID, string status)
    {
        disableRevControls();
        Panel1.Visible = false;
        PnlRev.Visible = true;
        Session["transID"] = transID;
        DdlRevSource.Enabled = false;
        String str = "Select * from  ExpJournal where  TransactionID=" + transID;
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, str);
        this.TransactionID = Int32.Parse(dsRecords.Tables[0].Rows[0]["TransactionID"].ToString()); revenue.RevAmount = decimal.Round(decimal.Parse(dsRecords.Tables[0].Rows[0]["ExpenseAmount"].ToString()), 2);
        revenue.RevComments = dsRecords.Tables[0].Rows[0]["Comments"].ToString();
        revenue.RevEventYear = int.Parse(dsRecords.Tables[0].Rows[0]["EventYear"].ToString());
        revenue.TransType = dsRecords.Tables[0].Rows[0]["TransType"].ToString();
        revenue.RevReport = DateTime.Parse(dsRecords.Tables[0].Rows[0]["ReportDate"].ToString());
        revenue.DateReceived = DateTime.Parse(dsRecords.Tables[0].Rows[0]["DateIncurred"].ToString());
        revenue.RevChapterID = int.Parse(dsRecords.Tables[0].Rows[0]["ChapterID"].ToString());
        revenue.RevEventID = int.Parse(dsRecords.Tables[0].Rows[0]["EventID"].ToString());
        revenue.RevSource = dsRecords.Tables[0].Rows[0]["RevSource"].ToString();
        revenue.RevCatID = int.Parse(dsRecords.Tables[0].Rows[0]["RevCatID"].ToString());
        revenue.RevCatCode = dsRecords.Tables[0].Rows[0]["RevCatCode"].ToString();
        revenue.PaymentMethod = dsRecords.Tables[0].Rows[0]["PaymentMethod"].ToString();
        revenue.nationalAprovalFlag = dsRecords.Tables[0].Rows[0]["NationalApprovalFlag"].ToString();
        revenue.chapterApprovalFlag = dsRecords.Tables[0].Rows[0]["ChapterApprovalFlag"].ToString().ToLower();
        revenue.Givername = dsRecords.Tables[0].Rows[0]["ProviderName"].ToString();
        ddlTransactionsType.SelectedIndex = ddlTransactionsType.Items.IndexOf(ddlTransactionsType.Items.FindByText("Revenue"));
        SetForTransType();
        // Change According to the RevSource Selected 
        if (revenue.RevSource == "Sponsorship")
        {
            DdlRevSprType.Enabled = true;
            Rtr3.Visible = true;
            DdlRevSprFor.Enabled = true;
            Label5.Text = "Type of Sponsor";
            Rtr2.Visible = true;
            revenue.SponsorID = int.Parse(dsRecords.Tables[0].Rows[0]["SponsorID"].ToString());
            revenue.SponsorCode = dsRecords.Tables[0].Rows[0]["SponsorCode"].ToString();
            for (int i = 0; i <= DdlRevSprFor.Items.Count - 1; i++)
            {
                if (DdlRevSprFor.Items[i].Value == revenue.SponsorID.ToString())
                {
                    DdlRevSprFor.SelectedIndex = i;
                    break;
                }
            }
        }
        else if (revenue.RevSource == "Donation")
        {
            DdlRevSprType.Enabled = true;
            Label5.Text = "Type of Donor";
            Rtr3.Visible = true;
            DdlDonationPurpose.Enabled = true;
            Rtr5.Visible = true;
            //revenue.DonPurposeID = int.Parse(DdlDonationPurpose.SelectedItem.Value);
            //revenue.DonPurposeCode = DdlDonationPurpose.SelectedItem.Text;
            revenue.DonPurposeID = int.Parse(dsRecords.Tables[0].Rows[0]["DonPurposeID"].ToString());
            revenue.DonPurposeCode = dsRecords.Tables[0].Rows[0]["DonPurposeCode"].ToString();
            for (int i = 0; i <= DdlDonationPurpose.Items.Count - 1; i++)
            {
                if (DdlDonationPurpose.Items[i].Value == revenue.DonPurposeID.ToString())
                {
                    DdlDonationPurpose.SelectedIndex = i;
                    break;
                }
            }
        }
        else if (revenue.RevSource == "Fees")
        {
            //revenue.FeesType = DdlRevFeetype.SelectedItem.Text;
            DdlRevFeetype.Enabled = true;
            Rtr6.Visible = true;
            revenue.FeesType = dsRecords.Tables[0].Rows[0]["FeesType"].ToString();
            for (int i = 0; i <= DdlRevFeetype.Items.Count - 1; i++)
            {
                if (DdlRevFeetype.Items[i].Text == revenue.FeesType.ToString())
                {
                    DdlRevFeetype.SelectedIndex = i;
                    break;
                }
            }
        }
        else if (revenue.RevSource == "Sales")
        {
            //revenue.SalesCatCode = ddlRevSalesType.SelectedItem.Text;
            ddlRevSalesType.Enabled = true;
            Rtr7.Visible = true;
            revenue.SalesCatId = int.Parse(dsRecords.Tables[0].Rows[0]["SalesCatID"].ToString());
            revenue.SalesCatCode = dsRecords.Tables[0].Rows[0]["SalesCatCode"].ToString();
            for (int i = 0; i <= ddlRevSalesType.Items.Count - 1; i++)
            {
                if (ddlRevSalesType.Items[i].Text == revenue.SalesCatCode.ToString())
                {
                    ddlRevSalesType.SelectedIndex = i;
                    break;
                }
            }
        }

        txtGiverName.Text = revenue.Givername.ToString();
        TxtRevAmt.Text = revenue.RevAmount.ToString();
        txtRevComments.Text = revenue.RevComments.ToString();
        txtRevDate.Text = revenue.DateReceived.ToString("d");
        //txtReport.Text = revenue.RevReport.ToString("d");

        for (int i = 0; i <= DdlRevSprType.Items.Count - 1; i++)
        {
            if (DdlRevSprType.Items[i].Text == revenue.RevCatCode.ToString())
            {
                DdlRevSprType.SelectedIndex = i;
                break;
            }
        }
       
        ddlReportDate.Items.Clear();
        ddlReportDate.Items.Insert(0, new ListItem(revenue.RevReport.ToString("d"), revenue.RevReport.ToString("d")));
        ddlReportDate.SelectedIndex = 0; 

        for (int i = 0; i <= ddlRevEvent.Items.Count - 1; i++)
        {
            if (ddlRevEvent.Items[i].Value == revenue.RevEventID.ToString())
            {
                ddlRevEvent.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i <= ddlYear.Items.Count - 1; i++)
        {
            if (ddlYear.Items[i].Text == revenue.RevEventYear.ToString())
            {
                ddlYear.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i <= ddlChapters.Items.Count - 1; i++)
        {
            if (ddlChapters.Items[i].Value == revenue.RevChapterID.ToString())
            {
                ddlChapters.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i <= DdlRevSource.Items.Count - 1; i++)
        {
            if (DdlRevSource.Items[i].Text == revenue.RevSource.ToString())
            {
                DdlRevSource.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i <= DdlRevType.Items.Count - 1; i++)
        {
            if (DdlRevType.Items[i].Text == revenue.PaymentMethod.ToString())
            {
                DdlRevType.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i <= DdlRevNationalApproval.Items.Count - 1; i++)
        {
            if (DdlRevNationalApproval.Items[i].Text.ToLower() == revenue.nationalAprovalFlag.ToLower())
            {
                DdlRevNationalApproval.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i <= DdlRevChapterApproval.Items.Count - 1; i++)
        {
            if (DdlRevChapterApproval.Items[i].Text.ToLower() == revenue.chapterApprovalFlag.ToLower())
            {
                DdlRevChapterApproval.SelectedIndex = i;
                break;
            }
        }
        if (status == "Approve")
        {
            lblMessage.Text = "";
            TxtRevAmt.Enabled = false;
            txtRevComments.Enabled = false;
            txtRevDate.Enabled = false;
            DdlRevType.Enabled = false;
            ddlRevSalesType.Enabled = false;
            DdlRevFeetype.Enabled = false;
            DdlDonationPurpose.Enabled = false;
            DdlRevSprType.Enabled = false;
            DdlRevSprFor.Enabled = false;
            DdlRevType.Enabled = false;
            //txtReport.Enabled = false;
            ddlReportDate.Enabled = true;

            if (Session["RoleId"].ToString() == "5")
            {
                Panel7.Visible = true;

            }

            if (Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1")
            {
                Panel8.Visible = true;
            }


            if (Session["RoleId"].ToString() == "38" && this.National == "y")
            {

                Panel8.Visible = true;
            }


            if (Session["RoleId"].ToString() == "38" && this.IsChapterID == true)
            {

                Panel7.Visible = true;
            }

            if (btnAdd.Text == "Add" || btnAdd.Text == "Save")
            {
                btnAdd.Text = "Update";
            }

        }
        else if (status == "Modify")
        {
            lblMessage.Text = "";
            Panel7.Visible = false;
            Panel8.Visible = false;
            txtReport.Enabled = true;
            if (btnAdd.Text == "Add" || btnAdd.Text == "Update")
            {
                btnAdd.Text = "Save";
            }
        }
      

    }
    #endregion

    #region ---- private property

    private string National
    {

        set
        {
            ViewState["National"] = value;
        }

        get
        {
            return (string) ViewState["National"];

        }



    }

    private int TransactionID
    {
        set
        {
            ViewState["TransactionID"] = value;
        }

        get
        {
            return (int) ViewState["TransactionID"];

        }


    }

    private int IncurMemberID
    {
        get
        {
            return (int) ViewState["IncurMemberID"];
        }

        set
        {
            ViewState["IncurMemberID"] = value;
        }

    }

    //private int VendorID
    //{
    //    get
    //    {
    //        return (int)ViewState["VendorID"];
    //    }

    //    set
    //    {
    //        ViewState["VendorID"] = value;
    //    }

    //}

    private int ReimMemberID
    {
        get
        {
            return (int)ViewState["ReimMemberID"];
        }

        set
        {
            ViewState["ReimMemberID"] = value;
        }

    }
    private bool LocatedAnywhere
    {

        get
        {
            return (bool)ViewState["LocatedAnywhere"];
        }

        set
        {
            ViewState["LocatedAnywhere"] = value;
        }
    }

    private bool IsChapterID
    {

        get
        {
            return (bool)ViewState["IsChapterID"];
        }

        set
        {
            ViewState["IsChapterID"] = value;
        }


    }
    #endregion


    public class Expensejournal
    {
        //public string personReimbursing = string.Empty;
        public int expenseCategoryID = -1;
        public  decimal expenseAmount =  -1;
        public decimal spAmount = -1;
        public int eventID = -1;
        public int eventYear = -1;
        public string comments = string.Empty;
        public string expenseCatCode = string.Empty;
        public DateTime dateIncurred = new System.DateTime(1900, 1, 1);
        public DateTime Report = new System.DateTime(1900, 1, 1);
        public string treatmentCode = string.Empty;
        public int treatementID = -1;
        public int account = -1;
        public int chapterID = -1;
        public int tochapterID = -1;
        public string DonorType = string.Empty;
        public int IncMemberId = -1;
        public int ReimMemberId = -1;
        public string VendorName = string.Empty;
        public string TransType = string.Empty;
        public string chapterApprovalFlag = "Pending";
        public string nationalAprovalFlag = "Pending";
        public int chapterApprover = -1;
        public int nationalApprover = -1;
      //  public int checkNumber = -1;
       // public DateTime checkDate = new System.DateTime(1900, 1, 1);
       // public decimal checkAmount = -1;
        public DateTime ChapterApprovedDate = new System.DateTime(1900, 1, 1);
        public DateTime NationalApprovalDate = new System.DateTime(1900, 1, 1);
    }

    //Declare Object items for Revenue 

    public class Revenuejournal
    {
        public int RevEventID = -1;
        public int RevEventYear = -1;
        public int RevChapterID = -1;
        public string RevSource = string.Empty;
        public string RevComments = string.Empty;
        public string RevCatCode = string.Empty;
        public int RevCatID = -1;
        public DateTime DateReceived = new System.DateTime(1900, 1, 1);
        public DateTime RevReport = new System.DateTime(1900, 1, 1);
        public Decimal RevAmount = -1;
        public string TransType = string.Empty;
        public int RevAccount = -1;
        public int SponsorID = -1;
        public string Givername = string.Empty; 
        public string SponsorCode = string.Empty ;
        public int DonPurposeID = -1;
        public string DonPurposeCode = string.Empty;
        public string FeesType = string.Empty;
        public int SalesCatId = -1;
        public string SalesCatCode = string.Empty;
        public string PaymentMethod = string.Empty;
        public string chapterApprovalFlag = "Pending";
        public string nationalAprovalFlag = "Pending";
        public int chapterApprover = -1;
        public int nationalApprover = -1;
        public DateTime ChapterApprovedDate = new System.DateTime(1900, 1, 1);
        public DateTime NationalApprovalDate = new System.DateTime(1900, 1, 1);

    }


    //protected void RbtnRev_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (RbtnRev.Checked == true)
    //    {
    //        PnlRev.Visible = true;
    //        disableRevControls();
    //        Panel1.Visible = false;
    //        DisplayRecords();
    //    }      

    //}    
    //protected void RbtnExpense_CheckedChanged(object sender, EventArgs e)
    //{
        
    //   if (RbtnExpense.Checked == true)
    //    {
    //        PnlRev.Visible = false;
    //        Panel1.Visible = true;
    //        //** ferdine 27/05/2010 if (RwViewReport.Visible == false)
    //        DisplayRecords();
           
    //    }
    //}
    protected void DdlRevSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        lblExpError.Text = "";
        if (DdlRevSource.Text == "Sponsorship")
        {
            GetRevCat(DdlRevSprType);
            DdlRevSprType.Enabled = true;
            Rtr3.Visible = true;
            Label5.Text = "Type of Sponsor";
            DdlRevSprFor.Enabled = true;
            Rtr2.Visible = true;
            DdlDonationPurpose.Enabled = false;
            Rtr5.Visible = false;
            DdlRevFeetype.Enabled = false;
            Rtr6.Visible = false;
            ddlRevSalesType.Enabled = false;
            Rtr7.Visible = false;
           
        }
        else if (DdlRevSource.Text == "Donation")
        {
            Label5.Text = "Type of Donor";
            DdlDonationPurpose.Enabled = true;
            Rtr5.Visible = true;
            GetRevCat(DdlRevSprType);
            DdlRevSprType.Enabled = true;
            Rtr3.Visible = true;
            DdlRevSprFor.Enabled = false;
            Rtr2.Visible = false;
            DdlRevFeetype.Enabled = false;
            Rtr6.Visible = false;
            ddlRevSalesType.Enabled = false;
            Rtr7.Visible = false;
        }
        else if (DdlRevSource.Text == "Fees")
        {
            DdlRevFeetype.Enabled = true;
            Rtr6.Visible = true;
            String str = "4,5";
            GetRevCat(DdlRevSprType, str);
            for (int i = 0; i <= DdlRevSprType.Items.Count - 1; i++)
            {
                if (DdlRevSprType.Items[i].Value == "5")
                {
                    DdlRevSprType.SelectedIndex = i;
                    break;
                }

            }             
            DdlDonationPurpose.Enabled = false;
            Rtr5.Visible = false;
            DdlRevSprType.Enabled = false;
            Rtr3.Visible = false;
            DdlRevSprFor.Enabled = false;
            Rtr2.Visible = false;
            ddlRevSalesType.Enabled = false;
            Rtr7.Visible = false;
        }
        else if (DdlRevSource.Text == "Sales")
        {
            ddlRevSalesType.Enabled = true;
            Rtr7.Visible = true; 
            DdlRevFeetype.Enabled = false;
            Rtr6.Visible = false;
            String str = "6";
            GetRevCat(DdlRevSprType, str);
            DdlRevSprType.SelectedIndex = 1;
            Rtr3.Visible = true;
            DdlDonationPurpose.Enabled = false;
            Rtr5.Visible = false;
            DdlRevSprType.Enabled = false;
            DdlRevSprFor.Enabled = false;
            Rtr2.Visible = false;
            
        }
        else 
        {
            disableRevControls();  

        }
    }
    private void disableExpControls()
    {
        txtExAmount.Text = string.Empty;
        txtSpAmount.Text = string.Empty;
        txtExDes.Text = string.Empty;
        trans1.Visible = false;
        trans2.Visible = false; 
        //** doneon request BY Prasanna and Dr.Chitturi on on July 3rd, 2010
        //txtIncurrEx.Text = string.Empty;
        //txtReimburse.Text = string.Empty;
        //** silva needs
       /************* Was commented on 29-08-2013 to avoid date getting cleared for every Event *********/
        //if (reportnew.Visible == true && txtReport.Text != string.Empty)
        //{
        //   // txtReport.Text = string.Empty;
        //    GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value));
        //}
        /***************************************************************************************/
        txtDate.Text = string.Empty;
        //GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value)); 
           txtGiverName.Text = string.Empty;
        //txtVendorName.Text = string.Empty;
        ddlExCategory.SelectedIndex = 0;
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7")
            ddlExCategory.Enabled = false;
        else
            ddlExCategory.Enabled = true;
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "4" && GetCode(ddlTransactionsType.SelectedValue.ToString())[0] != "7" )
                 ddlTreatement.SelectedIndex = 0;  
     
        //ddlEvent.SelectedIndex = 0;
        ErrorMsg.Text = string.Empty;
   }
    private void disableRevControls()
    {
        ddlRevSalesType.Enabled = false;
        DdlRevFeetype.Enabled = false;         
        DdlDonationPurpose.Enabled = false;
        DdlRevSprType.Enabled = false;
        DdlRevSprFor.Enabled = false;
        ddlRevSalesType.SelectedIndex = 0;
        DdlRevFeetype.SelectedIndex = 0;
        DdlDonationPurpose.SelectedIndex = 0;
        DdlRevSprType.SelectedIndex = 0;
        DdlRevSprFor.SelectedIndex = 0;
        //ddlRevEvent.SelectedIndex = 0;
        TxtRevAmt.Text = "";
        txtRevComments.Text = "";
        txtRevDate.Text = "";
        txtGiverName.Text = "";
        Session["transID"] = null;
        DdlRevSource.Enabled = true;
        TxtRevAmt.Enabled = true;
        txtRevComments.Enabled = true;
        txtRevDate.Enabled = true;
        DdlRevType.Enabled = true;
        Panel7.Visible = false;
        Panel8.Visible = false;
        Rtr2.Visible = false;
        Rtr3.Visible = false;
        Rtr5.Visible = false;
        Rtr6.Visible = false;
        Rtr7.Visible = false;
        btnAdd .Text = "Add";
        //** silva needs
        /************* Was commented on 29-08-2013 to avoid date getting cleared for every Event *********/
        //if (reportnew.Visible ==true && txtReport.Text != "")
        //{
        //    txtReport.Text = string.Empty;
        //    GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value));
        //}
        /*************************************************************************************************/    
        for (int i = 0; i <= DdlRevSource.Items.Count - 1; i++)
        {
            if (DdlRevSource.Items[i].Text == "Select")
            {
                DdlRevSource.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i <= DdlRevType.Items.Count - 1; i++)
        {
            if (DdlRevType.Items[i].Text == "Select One")
            {
                DdlRevType.SelectedIndex = i;
                break;
            }
        }
    }
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString().Contains("Delete"))
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            String SqlStr = "Delete from ExpJournal where   TransactionID=" + e.CommandArgument.ToString() + "  and ChapterApprovalFlag='Pending' and NationalApprovalFlag='Pending' ";
            if (SqlHelper.ExecuteNonQuery(conn, CommandType.Text, SqlStr) == 1)
            {
                lblMessage.Text = "Transaction ID : " + e.CommandArgument.ToString() + " -  Record Deleted";
                DisplayRecords();
            }
            else
                lblMessage.Text = "Record Cannot be Deleted";

        }
        else if ((e.CommandName.ToString() == "Modify") || (e.CommandName.ToString() == "Approve"))
        {
            //For Modify the revenue
            int index = int.Parse(e.CommandArgument.ToString());
            int TransID = (int)GridView2.DataKeys[index].Value;

            if (TransID != null && TransID > 0)
            {
                GetRevSelectedRecord(TransID, e.CommandName.ToString());

            }
        }
    }
    protected void gvTransfer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if ((e.CommandName.ToString() == "Modify") || (e.CommandName.ToString() == "Approve"))
        {
            //For Modify the Transfer
            hdnTransferTransID.Value = string.Empty;
            disableExpControls();
            Panel5.Visible = false;
           
            int index = int.Parse(e.CommandArgument.ToString());
            int TransID = (int)gvTransfer.DataKeys[index].Value;
             if (TransID != null && TransID > 0)
             GetTransSelectedRecord(TransID, e.CommandName.ToString());             
        }
    }
    public Boolean ValidExp()
    {
        try
        {
            ErrorMsg.Text = "";
            if (ddlChapters.SelectedItem.Value.ToString() == "-1")
            {
                ErrorMsg.Text = "Please Select Chapter";
                return false;
            }
            else if (ddlYear.SelectedItem.Value.ToString() == "-1")
            {
                ErrorMsg.Text = "Please Select Event Year";
                return false;
            }
            else if ((ddlEvent.SelectedItem.Value.ToString() == "-1") && (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2"))
            {
                ErrorMsg.Text = "Please Select Event";
                return false;
            }
            //else if ((txtReport.Text).Length < 8)
            //{
            //    ErrorMsg.Text = "Please Enter Report Date in MM/DD/YYYY Format";
            //    return false;
            //}
            else if ((reportnew.Visible == true) && (txtReport.Text.Length < 8))
            {
                ErrorMsg.Text = "Please Enter Report Date in MM/DD/YYYY Format";
                return false;
            }
            //else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0))
            //{
            //    //validation
            //    ErrorMsg.Text = "This Report date already Exist 3";
            //    reportold.Visible = true;
            //    Session["Report Date"] = txtReport.Text;
            //    GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),True);
            //    reportnew.Visible = false;
                  
            //    return false;
            //}
            else if(reportnew.Visible == true && Convert.ToDateTime(txtReport.Text)> DateTime.Today)
            {
                ErrorMsg.Text = "This Report cannot be future date ";
                return false;
            }
            else if ((reportold.Visible == true) && (IsDate(ddlReportDate.SelectedValue) == false))
            {
                ErrorMsg.Text = "Please Select Report Filling Date";
                return false;
            }

            //else if ((txtIncurrEx.Text.Length < 1) && (ddlChapters.SelectedValue != "1") && (ViewState["DonorType"].ToString().Trim() != "OWN"))
            else if ((txtIncurrEx.Text.Length < 1) && (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3")&& (ViewState["DonorType"].ToString().Trim() != "OWN"))
            {
                ErrorMsg.Text = "Please Enter Individual Incurring Expenses";
                //Response.Write(ViewState["DonorType"].ToString ());
                return false;
            }
            //else if ((txtIncurrEx.Text.Length < 1) && (ddlTreatement.SelectedItem.Text.Trim() != "Pay Vendor") && (ddlTreatement.SelectedItem.Text.Trim() != "Grantee") && (ddlChapters.SelectedValue == "1"))
            //{
            //    ErrorMsg.Text = "Please Enter Individual Incurring Expenses";
            //    return false;
            //}
            else if (txtReimburse.Text.Length < 1)
            {
                ErrorMsg.Text = ddlChapters.SelectedValue == "1" ? "Please Enter Pay To" : "Please Enter Person to be Reimbursed";
                return false;
            }
            //else if ((txtVendorName.Text.Length < 1) && (ddlChapters.SelectedValue != "1"))
            else if ((txtVendorName.Visible == true && txtVendorName.Text.Length < 1) && (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3") && (ViewState["DonorType"].ToString().Trim() != "OWN"))
            {
                ErrorMsg.Text = "Please Enter Vendor Name";
                return false;
            }
            else if (ddlExCategory.SelectedItem.Value.ToString() == "-1")
            {
                ErrorMsg.Text = "Please Select Expense Category";
                return false;
            }
            else if ((ddlExCategory.SelectedItem.Text.ToString() == "Other Business Expenses") && (txtExDes.Text.Length < 1))
            {
                ErrorMsg.Text = "Please Enter Comments about Other Business Expenses";
                return false;
            }
            else if (Convert.ToInt32(ddlToChapter.SelectedItem.Value) <= 0 && (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3"))
            {
                ErrorMsg.Text = "Please Select To Chapter";
                return false;
            }
            else if ((GetCode(ddlTreatement.SelectedValue.ToString())[0] == "3" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "4" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "5") && (txtExAmount.Text.Length <= 1 || Decimal.Parse(txtExAmount.Text) == 0 ||  Double.Parse(txtExAmount.Text) == 0.00)) //
            {
                ErrorMsg.Text = "Please Enter Amount";
                return false;
            }
            else if ((GetCode(ddlTreatement.SelectedValue.ToString())[0] == "1" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "2" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "6") && (txtExAmount.Text.Length > 1 || txtSpAmount.Text.Length <= 1)) //))
            {
                if (txtSpAmount.Text.Length <= 1 || Decimal.Parse(txtSpAmount.Text) == 0 || Double.Parse(txtSpAmount.Text) == 0.00) 
                {
                    ErrorMsg.Text = "Please Enter Sponsor Amount";
                    return false;
                }
                else if (txtExAmount.Text.Length > 1 && Decimal.Parse(txtExAmount.Text) > 0)
                {
                    ErrorMsg.Text = "Expense Amount must be zero.";
                    return false;
                }
                else 
                { 
                    ErrorMsg.Text = "";
                    return true;
                }

            }
            //else if ((GetCode(ddlTreatement.SelectedValue.ToString())[0] == "1" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "2" || GetCode(ddlTreatement.SelectedValue.ToString())[0] == "6") && (txtSpAmount.Text.Length < 1 || Convert.ToInt32(txtSpAmount.Text) == 0.00))
            //{
            //    ErrorMsg.Text = "Please Enter Sponsor Amount";
            //    return false;
            //}
            else if ((txtDate.Text).Length < 8)
            {
                ErrorMsg.Text = "Please Enter Date in MM/DD/YYYY Format";
                return false;
            }
            else if (Convert.ToDateTime(txtDate.Text) > DateTime.Now)
            {
                ErrorMsg.Text = "Please don't enter future date in Date Incurred";
                return false;
            }
            else if ((ddlTreatement.SelectedItem.Value.ToString() == "-1") && (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2" || GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3"))
            {
                ErrorMsg.Text = "Please Select Treatment Category";
                return false;
            }
            else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0))
            {
                //validation
                //ErrorMsg.Text = "This Report date already Exist 3";
                reportold.Visible = true;
                Session["ReportDate"] = txtReport.Text;
                GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value), true);
                reportnew.Visible = false;

                return true;
            }


            else
                ErrorMsg.Text = "";
           
        }
        catch (Exception ex)
        {
            ErrorMsg.Text = "Please verify all the data entered.";
            //Response.Write(ex.ToString());
        }
 return true;
    }

    public Boolean ValidRev()
    {
        if (ddlChapters.SelectedItem.Value.ToString() == "-1")
        {
            ErrorRev.Text = "Please Select Chapter";
            return false;
        }
        else if (ddlYear.SelectedItem.Value.ToString() == "-1")
        {
            ErrorRev.Text = "Please Select Event Year";
            return false;
        }
        else if (ddlRevEvent.SelectedItem.Value.ToString() == "-1")
        {
            ErrorRev.Text = "Please Select Event";
            return false;
        }
        //else if ((txtReport.Text).Length < 8)
        //{
        //    ErrorRev.Text = "Please Enter Report Date in MM/DD/YYYY Format";
        //    return false;
        //}
        else if ((reportnew.Visible == true) && (txtReport.Text.Length < 8))
            {
                ErrorRev.Text = "Please Enter Report Date in MM/DD/YYYY Format";
                return false;
            }

        //else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE  (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString())>0))
        //    {
        //        //validation
        //        ErrorRev.Text = "This Report date already Exist 1 ";
        //        reportold.Visible = true;
        //        reportnew.Visible = false;
        //        return false;  
              
        //    }

        else if ((reportnew.Visible == false) && (IsDate(ddlReportDate.SelectedValue) == false))
        {
             ErrorRev.Text = "Select Report date from Dropdown";
             return false;           

        }
        else if ((txtGiverName.Text).Length < 1)
        {
            ErrorRev.Text = "Please Enter Giver Name";
            return false;
        }

        else if ((TxtRevAmt.Text).Length < 1)
        {
            ErrorRev.Text = "Please Enter Amount";
            return false;
        }
        else if (DdlRevType.SelectedItem.Text == "Select One")
        {
            ErrorRev.Text = "Please Select Revenue Type";
            return false;
        }
        else if ((txtRevDate.Text).Length < 8)
        {
            ErrorRev.Text = "Please Enter Date in MM/DD/YYYY Format";
            return false;
        }

        else if (DdlRevSource.SelectedItem.Value.ToString() == "Select")
        {
            ErrorRev.Text = "Please Select Revenue Source";
            return false;
            
        }
        else if (DdlRevSource.SelectedItem.Text == "Sponsorship")
        {
            if (DdlRevSprType.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Type of Sponsor";
                return false;
            }
            else if (DdlRevSprFor.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Sponsor For";
                return false;
            }
            else
                return true;

        }
        else if (DdlRevSource.SelectedItem.Text == "Donation")
        {
            if (DdlRevSprType.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Donor Type";
                return false;
            }
            else if (DdlDonationPurpose.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Donation Purpose";
                return false;
            }
            else
                return true;
        }
        else if (DdlRevSource.SelectedItem.Text == "Fees")
        {
            if (DdlRevFeetype.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Fee Type";
                return false;
            }
            else
                return true;
        }
        else if (DdlRevSource.SelectedItem.Text == "Sales")
        {
            if (ddlRevSalesType.SelectedItem.Value.ToString() == "-1")
            {
                ErrorRev.Text = "Please Select Sales Type";
                return false;
            }
            else
                return true;
        }
        else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE  (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0))
        {
            //validation
            /* Modified on 06-06-2014 for Issues with report date getting reloaded*/
            //ErrorRev.Text = "This Report date already Exist";
            reportold.Visible = true;
            Session["ReportDate"] = txtReport.Text;
            GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),true);
            reportnew.Visible = false;
            return true;

        }
        else
        {
            ErrorRev.Text = "";
            return true;
        }
        
    }
    protected void grdView_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, hdnSQlQuery.Value);
        DataTable dt = dsRecords.Tables[0];
        DataView dv = new DataView(dt);
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    protected void gvTransfer_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        String ReportDate;
        if (ddlReportDate.SelectedIndex > 1)// (ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing")
        {
            ReportDate = ddlReportDate.SelectedItem.Text.ToString();
        }
        else
        {
            ReportDate = txtReport.Text.ToString();
        }
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select EJ.BanKID, EJ.TobankID, EJ.CheckNumber, EJ.ExpenseAmount, EJ.DatePaid,EJ.ReportDate,EJ.EventYear,B1.BankCode as BankFrom ,B2.BankCode as BankTo,Ch.ChapterCode from ExpJournal EJ Inner Join Bank B1 On B1.BankID = EJ.BanKID Inner Join Bank B2 On B2.BankID = EJ.TobankID Inner Join Chapter Ch On Ch.ChapterID=EJ.ChapterID Where EJ.ReportDate = '" + ReportDate + "' and EJ.TransType = 'Transfers'");//ddlReportDate.SelectedValue
        DataTable dt = dsRecords.Tables[0];
        DataView dv = new DataView(dt);
        gvTransfer.PageIndex = e.NewPageIndex;
        gvTransfer.DataSource = dt;
        gvTransfer.DataBind();
    }

    protected void grdView1_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        String ReportDate;
        
        if (reportnew.Visible == true && txtReport.Text != "")
        {
            ReportDate = txtReport.Text.ToString();
        }
        else if (reportold.Visible == true && ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing") //ddlReportDate.SelectedIndex > -1 &&
        {
            ReportDate = ddlReportDate.SelectedItem.Text.ToString();
        }
        else
        {
            ReportDate = "01/01/1900";
        }
        
        //"SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" + ChapterID + "and ExpJournal.ReportDate = '" + ReportDate + "' ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.RevCatCode");
                                                                                                                    //"SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" + ddlChapters.SelectedItem.Value + "and ExpJournal.EventYear = " + ddlYear.SelectedItem.Value + " ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.RevCatCode "
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" + ddlChapters.SelectedItem.Value + "and ExpJournal.ReportDate = '" + ReportDate +"' ORDER BY ExpJournal.ReportDate DESC, ExpJournal.CreatedDate, ExpJournal.RevCatCode");//ddlReportDate.SelectedValue 
        DataTable dt = dsRecords.Tables[0];
        DataView dv = new DataView(dt);
        GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataSource = dt;
        GridView2.DataBind();
    }

  
    public bool IsDate(string sdate)
    {
        DateTime dt;
        bool isDate = true;
        try
        {
            dt = DateTime.Parse(sdate);
        }
        catch
        {
            isDate = false;
        }
        return isDate;
    }
    protected void ddlReportDate_SelectedIndexChanged(object sender, EventArgs e)
    {
       //Response.Write(ddlReportDate.SelectedItem.Value);
        lblExpError.Text = "";
        if (ddlReportDate.SelectedItem.Value == "-2")
        {
            reportold.Visible = false ;
            reportnew.Visible = true;
            txtReport.Text = DateTime.Now.ToShortDateString();
        }
       else
            DisplayRecords();
    }

    protected void ddlExCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblExpError.Text = "";
        lblMessage.Text = "";
        if (ddlExCategory.SelectedItem.Text.ToString() == "Other Business Expenses" )
            ErrorMsg.Text = "Please Enter Comments about Other Business Expenses";

    }
    
    protected void BtnRevExport_Click(object sender, EventArgs e)
    {
        lblExpError.Text = "";
        IWorkbook book = NativeExcel.Factory.CreateWorkbook();
      if (ddlReportDate.SelectedIndex == 0)
      {
          lblExpError.Text = "Please Select Report Filing Date to export";
          return;
      }
      DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Session["StrSQLExp"].ToString());
      
      if (ds.Tables[0].Rows.Count > 0)
      {
          IWorksheet sheet = book.Worksheets.Add();
          for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
              sheet.Cells[1, i + 1].Value = ds.Tables[0].Columns[i].ColumnName.ToString();

          for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
          {
              for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
              {
                  sheet.Cells[i + 2, j + 1].Value = ds.Tables[0].Rows[i][j];
              }
          }
          IRange Range2 = sheet.Range["A" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2) + ":O" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2).ToString() + ""];
          Range2.Merge();
          Range2.Value = lblExpenseTot.Text.ToString();
          Range2.Font.Bold = true;
          Range2.HorizontalAlignment = XlHAlign.xlHAlignCenter; 
      }
       
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Session["StrSQLRev"].ToString());
        if (ds.Tables[0].Rows.Count > 0)
        {
            IWorksheet sheet2 = book.Worksheets.Add();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                sheet2.Cells[1, i + 1].Value = ds.Tables[0].Columns[i].ColumnName.ToString();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                {
                    try
                    {
                        sheet2.Cells[i + 2, j + 1].Value = ds.Tables[0].Rows[i][j];
                    }
                    catch (Exception ex)
                    {
                       //Response.Write(ex.ToString());
                    }
                }
            }
            sheet2.Name = "Revenue";
            IRange Range1 = sheet2.Range["A" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2) + ":O" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2).ToString() + ""];
            Range1.Merge();
            Range1.Value = lblRevenueTot.Text;
            Range1.Font.Bold = true;
            Range1.HorizontalAlignment = XlHAlign.xlHAlignCenter; 
        }
        //transfer
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select EJ.BanKID, EJ.TobankID, EJ.CheckNumber, EJ.ExpenseAmount, EJ.DatePaid,EJ.ReportDate,EJ.EventYear,B1.BankCode as BankFrom ,B2.BankCode as BankTo,Ch.ChapterCode from ExpJournal EJ Inner Join Bank B1 On B1.BankID = EJ.BanKID Inner Join Bank B2 On B2.BankID = EJ.TobankID Inner Join Chapter Ch On Ch.ChapterID=EJ.ChapterID Where EJ.ReportDate = '" + ddlReportDate.SelectedItem.Text + "' and EJ.TransType = 'Transfers'");
        if (ds.Tables[0].Rows.Count > 0 && ddlChapters.SelectedValue == "1") //ddlChapters.SelectedValue == "1"
        {
            IWorksheet sheet2 = book.Worksheets.Add();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                sheet2.Cells[1, i + 1].Value = ds.Tables[0].Columns[i].ColumnName.ToString();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                {
                    try
                    {
                        sheet2.Cells[i + 2, j + 1].Value = ds.Tables[0].Rows[i][j];
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.ToString());
                    }
                }
            }
            sheet2.Name = "Transfers";
            IRange Range1 = sheet2.Range["A" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2) + ":G" + (Convert.ToInt32(ds.Tables[0].Rows.Count) + 2).ToString() + ""];
            Range1.Merge();
            Range1.Value = lblRevenueTot.Text;
            Range1.Font.Bold = true;
            Range1.HorizontalAlignment = XlHAlign.xlHAlignCenter;
        }
                        
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=ReimbursementReport_" + ddlChapters.SelectedItem.Text.Replace(",", "_").Replace(" ", "") + "_" + ddlReportDate.SelectedItem.Text + ".xls");
        book.SaveAs(Response.OutputStream);
        Response.End();
    }
    protected void btnApproveAll_Click(object sender, EventArgs e)
    {
        object value;
        String ReportDate;
        
        if (reportnew.Visible == true && txtReport.Text != "")
        {
            ReportDate = txtReport.Text.ToString();
        }
        else if (reportold.Visible == true && ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing") //ddlReportDate.SelectedIndex > -1 &&
        {
            ReportDate = ddlReportDate.SelectedItem.Text.ToString();
        }
        else
        {
            ReportDate = "01/01/1900";
        }

        if (Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1" || (Session["RoleId"].ToString() == "38" && this.National == "y"))
        {
            value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set NationalApprover ='" + Session["LoginID"].ToString() + "', NationalApprovalDate = '" + DateTime.Now + "', NationalApprovalFlag = 'Approved' where  ChapterID=" + ddlChapters.SelectedValue + " AND  NationalApprovalFlag ='pending' AND ReportDate = '" + ReportDate + "'"); // ddlReportDate.SelectedItem.Text
            DisplayRecords();
            lblMessage.Text = "Record(s) Approved";

        }
         else if ((Session["RoleId"].ToString() == "38" && this.IsChapterID == true) || (Session["RoleId"].ToString() == "5"))
        {
            value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set ChapterApprover ='" + Session["LoginID"].ToString() + "' , ChapterApprovalDate = '" + DateTime.Now + "' ,ChapterApprovalFlag = 'Approved' where  ChapterID=" + ddlChapters.SelectedValue + " AND  ChapterApprovalFlag ='pending' AND ReportDate = '" + ReportDate + "'"); //ddlReportDate.SelectedItem.Text 
           DisplayRecords();
           lblMessage.Text = "Record(s) Approved";

        }

    }
   
    protected void BtnUpdatePaidAll_Click(object sender, EventArgs e)
    {

        if (txtCheckNumber.Text.Length < 1)
        {
            lblerrpaidAll.Text = "Please Enter Check Number";
            return;
        }
        else if(Convert.ToInt32(txtCheckNumber.Text) == 0)
        {
            lblerrpaidAll.Text = "Check Number must be greater than zero";
            return;
        }
        else if (!IsDate(txtCheckAllPaid.Text))
        {
            lblerrpaidAll.Text = "Please Enter Date Paid in MM/DD/YYYY format";
            return;
        }
        else if (txtTotalAmount.Text.Length < 1)
        {
            lblerrpaidAll.Text = "Please Enter Total Amount";
            return;
        }
        else if (ddlPayTo.Items.Count == 0)
        {
            lblerrpaidAll.Text = "No Person/Organization for Pay to";
            return;
        }

        else if (BtnUpdatePaidAll.Text == "Transfer")
        {
           //For Transfer
            if (ddlChapters.SelectedItem.Value.ToString() == "-1")
            {
                lblerrpaidAll.Text = "Please Select Chapter";
                return;
            }
            else if (ddlYear.SelectedItem.Value.ToString() == "-1")
            {
                lblerrpaidAll.Text = "Please Select Event Year";
                return;
            }
           else if ((reportnew.Visible == true) && (txtReport.Text.Length < 8))
            {
                lblerrpaidAll.Text = "Please Enter Report Date in MM/DD/YYYY Format";
                return;
            }
            /*Commented on 06-06-2014 for Issues with report date getting reloaded*/
  //        else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE      (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0))
  //        {
  //              //validation
  //              lblerrpaidAll.Text = "This Report date already Exist 2 ";
  //              reportold.Visible = true;
  //              reportnew.Visible = false;
  //              return;
  //        }
         
            else if ((reportold.Visible == true) && (IsDate(ddlReportDate.SelectedValue) == false))
            {
                lblerrpaidAll.Text = "Please Select Report Filling Date ";
                return;
            }
                /*Commented on 06-06-2014 for Issues with report date getting reloaded*/
            //else if ((reportnew.Visible == true) && (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE      (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0))
            //{
            //    //validation
            //    lblerrpaidAll.Text = "This Report date already Exist 2 ";
            //    reportold.Visible = true;
            //    Session["Report Date"] = txtReport.Text;
            //    GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),true);
            //    reportnew.Visible = false;
            //    return;
            //}
        
            else
            {
                try
                {
                    String Reportdte;
                    if (reportnew.Visible == true)
                    {
                        Reportdte = txtReport.Text;
                        if (int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT COUNT(DISTINCT CONVERT(VARCHAR(10), ReportDate, 101))  FROM ExpJournal  WHERE      (ChapterID = " + ddlChapters.SelectedItem.Value + ") AND ReportDate='" + DateTime.Parse(txtReport.Text) + "'").ToString()) > 0)
                        {
                            reportold.Visible = true;
                            Session["ReportDate"] = txtReport.Text;
                            GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value), true);
                            reportnew.Visible = false;
                        }
                    }
                    else
                    {
                        Reportdte = ddlReportDate.SelectedItem.Text.ToString();
                    }
                    //Response.Write("Update ExpJournal set BankID=" + ddlBankId.SelectedValue + ", TobankID=" + ddlPayTo.SelectedValue + ", CheckNumber='" + txtCheckNumber.Text + "', ExpenseAmount=" + txtTotalAmount.Text + ", DatePaid='" + txtCheckAllPaid.Text + "', ModifiedBy=" + Session["LoginID"] + ", ModifiedDate=GetDate(),RestTypeFrom='" + ddlRestTypeFrom.SelectedValue + "',RestTypeTo='" + ddlRestTypeTo.SelectedValue + "' WHERE TransactionID=" + hdnTransferTransID.Value + ""); 
                    //Response.Write(hdnTransferTransID.Value);

            
                    if (hdnTransferTransID.Value.Length > 0)
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update ExpJournal set BankID=" + ddlBankId.SelectedValue + ", TobankID=" + ddlPayTo.SelectedValue + ", CheckNumber='" + txtCheckNumber.Text + "', ExpenseAmount=" + txtTotalAmount.Text + ", DatePaid='" + txtCheckAllPaid.Text + "', ModifiedBy=" + Session["LoginID"] + ", ModifiedDate=GetDate(),RestTypeFrom='" + ddlRestTypeFrom.SelectedValue + "',RestTypeTo='" + ddlRestTypeTo.SelectedValue + "' WHERE TransactionID=" + hdnTransferTransID.Value + "");
                    else
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Insert Into ExpJournal(ChapterID,TransType,BanKID, TobankID, CheckNumber, ExpenseAmount, DatePaid,ReportDate,EventYear,CreatedBy,CreatedDate,RestTypeFrom,RestTypeTo,Paid) values (" + ddlChapters.SelectedValue + ",'" + GetCode(ddlTransactionsType.SelectedValue.ToString())[1] + "'," + ddlBankId.SelectedValue + "," + ddlPayTo.SelectedValue + ",'" + txtCheckNumber.Text + "'," + txtTotalAmount.Text + ",'" + txtCheckAllPaid.Text + "','" + Reportdte + "'," + ddlYear.SelectedValue + "," + Session["LoginID"] + ",GetDate(),'" + ddlRestTypeFrom.SelectedValue + "','" + ddlRestTypeTo.SelectedValue + "','Y') ");
                    txtCheckAllPaid.Text = "";
                    txtCheckNumber.Text = "";
                    txtTotalAmount.Text = "";
                    DisplayRecords();
                }
                catch(Exception ex)
                {
                    //Response.Write(ex.ToString());
                }
            }
        }
        else
        {
            object value;
            String[] str;
            String whrcntn;
            Boolean flag = false;
            String ReportDate;
            if (ddlChapters.SelectedValue == "1")
            {
                str = ddlPayTo.SelectedValue.ToString().Split('_');
                whrcntn = "AND DonorType = '" + str[0].ToString() + "' AND ReimbMemberid=" + str[1].ToString();
            }
            else
            {
                String memberIDs = "0";
                for (int i = 0; i < lstChPayTo.Items.Count; i++)
                {
                    if (lstChPayTo.Items[i].Selected == true)
                    {
                        str = lstChPayTo.Items[i].Value.ToString().Split('_');
                        memberIDs = memberIDs + "," + str[1].ToString();
                        flag = true;
                    }
                }
                whrcntn = "AND DonorType IN ('IND','SPOUSE','OWN') AND ReimbMemberid in (" + memberIDs.ToString() + ") ";
            }
            if ((flag == false) && (ddlChapters.SelectedValue != "1"))
            {
                lblerrpaidAll.Text = "'Pay To' is not selected";
                return;
            }
            // Search for Unique Check Number and BankID
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count (TransactionID) from ExpJournal WHERE DATEDIFF (M,ReportDate,GETDATE()) < 25 AND BankID=" + ddlBankId.SelectedValue + " AND CheckNumber = '" + txtCheckNumber.Text + "'")) > 0)
            {
                DataSet Ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 CONVERT(nvarchar(30), DatePaid, 107) as DatePaid , CONVERT(nvarchar(30), ReportDate, 107) as ReportDate ,DATEDIFF (M,ReportDate,GETDATE()),CheckNumber FROM ExpJournal where DATEDIFF (M,ReportDate,GETDATE()) < 25 AND BankID=" + ddlBankId.SelectedValue + " AND CheckNumber = '" + txtCheckNumber.Text + "' Order By ExpenseAmount Desc");
                lblerrpaidAll.Text = "* Same Check number already Exist. Date Paid : " + Ds.Tables[0].Rows[0][0].ToString () + " Report Date :" +  Ds.Tables[0].Rows[0][1].ToString () + " CheckNumber : " +  Ds.Tables[0].Rows[0][3].ToString () ;
                return;
            }
            if (Session["RoleId"].ToString() == "84" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "1" || (Session["RoleId"].ToString() == "38" && this.National == "y"))
            {
                if (reportnew.Visible == true && txtReport.Text != "")
                {
                    ReportDate = txtReport.Text.ToString();
                }
                else if (reportold.Visible == true && ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing") //ddlReportDate.SelectedIndex > -1 &&
                {
                    ReportDate = ddlReportDate.SelectedItem.Text.ToString();
                }
                else
                {
                    ReportDate = "01/01/1900";
                }

                if (hdnTransID.Value.Length > 0)
                {
                    Decimal totalamt;
                    try
                    {
                        totalamt = Math.Round(Convert.ToDecimal(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ExpenseAmount FROM  ExpJournal WHERE   TreatCode in ('TBR','PayVendor','Grantee') AND  ChapterID=" + ddlChapters.SelectedValue + " AND TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') AND NationalApprovalFlag='Approved' AND ReportDate = '" + ReportDate + "'  AND TransactionID = " + hdnTransID.Value + whrcntn + "")), 2);
                    }
                    catch (Exception ex)
                    {
                        lblerrpaidAll.Text = "No record available to update.";
                        return;
                    }
                    if (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 2)) //|| (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 0) + 1) || (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 0) - 1)
                    {
                        value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Update ExpJournal Set BanKID =" + ddlBankId.SelectedValue + ", DatePaid = '" + txtCheckAllPaid.Text + "', CheckNumber = '" + txtCheckNumber.Text + "',Paid='Y',RestTypeFrom='" + ddlRestTypeFrom.SelectedValue + "' where TreatCode in ('TBR','PayVendor','Grantee') AND ChapterID=" + ddlChapters.SelectedValue + " AND  TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') AND NationalApprovalFlag='Approved' AND ReportDate = '" + ddlReportDate.SelectedItem.Text + "' AND TransactionID = " + hdnTransID.Value + whrcntn);
                        hdnTransID.Value = string.Empty;
                        BtnUpdatePaidAll.Text = "Update All";
                    }
                    else
                    {
                        lblerrpaidAll.Text = "Amount must be " + totalamt.ToString();
                        return;
                    }
                }
                else
                {
                    Decimal totalamt;
                    try
                    {
                        //Response.Write("SELECT SUM(ExpenseAmount) FROM  ExpJournal WHERE TreatCode in ('TBR','PayVendor','Grantee') AND  DatePaid is Null and  ChapterID=" + ddlChapters.SelectedValue + " AND TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants') AND NationalApprovalFlag='Approved' AND ReportDate = '" + ddlReportDate.SelectedItem.Text + "'" + whrcntn);
                        totalamt = Math.Round(Convert.ToDecimal(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT SUM(ExpenseAmount) FROM  ExpJournal WHERE TreatCode in ('TBR','PayVendor','Grantee') AND  DatePaid is Null and  ChapterID=" + ddlChapters.SelectedValue + " AND TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') AND NationalApprovalFlag='Approved' AND ReportDate = '" + ReportDate + "'" + whrcntn)), 2);
                    }
                    catch (Exception ex)
                    {
                        lblerrpaidAll.Text = "No record available to update.";
                        return;
                    }
                    //Response.Write("SELECT SUM(ExpenseAmount) FROM  ExpJournal WHERE TransType='E' AND NationalApprovalFlag='Approved' AND ReportDate = '" + ddlReportDate.SelectedItem.Text + "'");
                    if (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 2)) //|| (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 0) + 1) || (totalamt == Math.Round(Convert.ToDecimal(txtTotalAmount.Text), 0) - 1)
                    {
                        value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "update ExpJournal Set BanKID =" + ddlBankId.SelectedValue + ", DatePaid = '" + txtCheckAllPaid.Text + "', CheckNumber = '" + txtCheckNumber.Text + "',Paid='Y',RestTypeFrom='" + ddlRestTypeFrom.SelectedValue + "' where TreatCode in ('TBR','PayVendor','Grantee') AND  DatePaid is Null and   ChapterID=" + ddlChapters.SelectedValue + " AND  TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') AND NationalApprovalFlag='Approved' AND ReportDate = '" + ReportDate + "'" + whrcntn);
                    }
                    else
                    {
                        lblerrpaidAll.Text = "Total Amount must be " + totalamt.ToString();
                        return;
                    }
                }
                txtCheckAllPaid.Text = "";
                txtCheckNumber.Text = "";
                txtTotalAmount.Text = "";
                lblerrpaidAll.Text = "";
                div11.Visible = false;
                DisplayRecords();
            }
        }
    }
    protected void BtnPaid_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        hdnTransID.Value = string.Empty; 
        
        Panel5.Visible = false;
        lblPayTo.Text = "Pay To";
        BtnUpdatePaidAll.Text = "Update All";
        DataSet dsBanks = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT BankID as Value, BankCode as ReimbursedTo from Bank");
        ddlBankId.DataSource = dsBanks.Tables[0];
               ddlBankId.DataTextField = "ReimbursedTo";
        ddlBankId.DataValueField = "Value";
        ddlBankId.DataBind();
        ddlBankId.SelectedIndex = ddlBankId.Items.IndexOf(ddlBankId.Items.FindByValue("2"));
        //Response.Write(ddlBankId.SelectedValue);
        div11.Visible = true;
        GetPayto();
        disableExpControls();
    }

    protected void GetPayto()
    {
        try
        {
            DataSet dsPayTo;
            String Str = (hdnTransID.Value.Length > 0) ? " AND E.TransactionID = " + hdnTransID.Value : "" + " order by 1"; ;
            String ReportDate;
           
            if (reportnew.Visible == true && txtReport.Text != "")
            {
                ReportDate = txtReport.Text.ToString();
            }
            else if (reportold.Visible == true && ddlReportDate.SelectedItem.Text.Trim() != "Select Date" && ddlReportDate.SelectedItem.Text.Trim() != "New Filing") //ddlReportDate.SelectedIndex > -1 &&
            {
                ReportDate = ddlReportDate.SelectedItem.Text.ToString();
            }
            else
            {
                ReportDate = "01/01/1900";
            }
            dsPayTo = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Distinct CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as ReimbursedTo,E.DonorType+'_'+ Convert(Varchar,E.ReimbMemberid) as  Value FROM ExpJournal E Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse II ON E.DonorType <> 'OWN' AND E.ReimbMemberid = II.AutoMemberID where E.TransType in ('FinalsExp','ChapterExp','GenAdmin','Grants','Return') AND  E.DatePaid is Null AND NationalApprovalFlag='Approved' AND E.ReportDate='" + ReportDate + "' and E.TreatCode in ('TBR','PayVendor','Grantee')  and E.ChapterID =" + ddlChapters.SelectedValue.ToString() + Str);
            ddlPayTo.DataSource = dsPayTo.Tables[0];
            ddlPayTo.DataBind();
            lstChPayTo.DataSource = dsPayTo.Tables[0];
            lstChPayTo.DataBind();
            if (dsPayTo.Tables[0].Rows.Count == 1)
            {
                ddlPayTo.SelectedIndex = 0;
                lstChPayTo.SelectedIndex = 0;
            }
            if (ddlChapters.SelectedValue == "1")
            {
                ddlPayTo.Visible = true;
                lstChPayTo.Visible = false;
            }
            else
            {
                lstChPayTo.Visible = true;
                ddlPayTo.Visible = false;
            }

            if (ddlPayTo.Items.Count > 0)
                BtnUpdatePaidAll.Enabled = true;
            else
            {
                BtnUpdatePaidAll.Enabled = false;
                lblerrpaidAll.Text = "No data to Update.<br />(Please Verify if the record is Approved or already Paid.)";
            }
        }

        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }


    ////protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////   //** event selection changed
    ////    if ((ddlEvent.SelectedValue == "1") && ddlChapters.SelectedValue == "1")
    ////    {
    ////        ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue("1"));
    ////        ddlToChapter.Enabled = false;
    ////    } 
    ////}

    protected void BtnUpdatePaidClose_Click(object sender, EventArgs e)
    {
        txtCheckAllPaid.Text = "";
        hdnTransferTransID.Value = string.Empty;
        txtCheckNumber.Text = "";
        txtTotalAmount.Text = "";
        lblerrpaidAll.Text = "";
        div11.Visible = false;
    }
    protected void ddlTransactionsType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblExpError.Text = "";
        hdnTransID.Value = string.Empty;
        disableExpControls();
        Panel5.Visible = false;
        SetForTransType();
        GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value), false);
    }
    protected void SetForTransType()
    {
        //GetCode(ddlExCategory.Items[i].Value.ToString())[1]
        lblPayTo.Text = "Pay To";
        BtnUpdatePaidAll.Text = "Update All";
        ddlTreatement.Enabled = true;
        div11.Visible = false;
        btnAdd.Visible = true;
        btnCancel.Visible = true;
        tblShowall.Visible = true;
        if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "6")// Revenue
        {
            PnlRev.Visible = true;
            disableRevControls();
            Panel1.Visible = false;
            GetEvents(ddlRevEvent, "1,2,3,5,9,14");
            lblRevText.Text = ddlTransactionsType.SelectedItem.Text;
        }
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "5")//Transfer
        {
            PnlRev.Visible = false;
            Panel1.Visible = false;
            trans1.Visible = true;
            trans2.Visible = true;
            btnAdd.Visible = false;
            btnCancel.Visible = false;
            DataSet dsBanks;
            dsBanks = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT BankID as Value, BankCode as ReimbursedTo from Bank");
            ddlBankId.DataSource = dsBanks.Tables[0];
            ddlBankId.DataTextField = "ReimbursedTo";
            ddlBankId.DataValueField = "Value";
            ddlBankId.SelectedIndex = ddlBankId.Items.IndexOf(ddlBankId.Items.FindByValue("2"));
            ddlBankId.DataBind();
            ddlPayTo.DataSource = dsBanks.Tables[0];
            ddlPayTo.DataBind();
            lblPayTo.Text = "To Bank";
            BtnUpdatePaidAll.Text = "Transfer";
            lblerrpaidAll.Text = ""; 
            BtnUpdatePaidAll.Enabled = true;
            ddlPayTo.Visible = true;
            lstChPayTo.Visible = false;
            div11.Visible = true;
        }
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "2")//ChapterExp
        {
            PnlRev.Visible = false;
            Panel1.Visible = true;
            lblExpText.Text = ddlTransactionsType.SelectedItem.Text;
            GetEvents(ddlEvent, "2,3,13,19"); //2,3,5,9,14
        }
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "1")//FinalsExp - 1
        {
            PnlRev.Visible = false;
            Panel1.Visible = true;
            lblExpText.Text = ddlTransactionsType.SelectedItem.Text;
            GetEvents(ddlEvent, "1");
        }
        else if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3") || (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4")) // ,GenAdmin - 3 ,Grants - 4
        {
            PnlRev.Visible = false;
            Panel1.Visible = true;
            lblExpText.Text = ddlTransactionsType.SelectedItem.Text;
            ddlEvent.Items.Clear();
            ddlEvent.Items.Insert(0, new ListItem(" ", "-1"));
            ddlEvent.SelectedIndex = 0;
            ddlEvent.Enabled = false;
            if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4")
            {
                ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("Grantee"));
                ddlTreatement.Enabled = false;                
            }
        }
        else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7") //Added on 09-04-2014 for Returning Donations
        {
            PnlRev.Visible = false;
            Panel1.Visible = true;
            incueredExpFind.Enabled = false;
            BtnIncClear.Enabled = false;
            lblExpText.Text = ddlTransactionsType.SelectedItem.Text;
            GetEvents(ddlEvent, "11");
            ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("To be Reimbursed"));
            ddlTreatement.Enabled = false; 
        }
        else
        {
            tblShowall.Visible = false;
            return;
        }

        if ((GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "3" || ddlEvent.SelectedValue == "1") && ddlChapters.SelectedValue == "1")
        {
            ddlToChapter.SelectedIndex = ddlToChapter.Items.IndexOf(ddlToChapter.Items.FindByValue("1"));
            ddlToChapter.Enabled = false;
        }
        GetExpenseCategory(ddlExCategory);
    }
    protected void BtnIncClear_Click(object sender, EventArgs e)
    {
        txtIncurrEx.Text = string.Empty;
        IncurMemberID = -1;
        txtReimburse.Text = string.Empty;
        ReimMemberID = -1;
        ViewState["DonorType"] = string.Empty;
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {
        if (txtIncurrEx.Text.Length > 1)
        {
            txtReimburse.Text = txtIncurrEx.Text;
            ReimMemberID = IncurMemberID;
            ViewState["DonorType"] = ViewState["IncDonorType"];
            DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Convert(varchar,Treatid) + '-' + TreatCode as code,Case when TreatID in (1,2,6) then TreatDesc + ' � 100%' Else TreatDesc End as TreatDesc,Case when TreatID = 3 Then 1 Else Case when TreatID = 4 Then 2 Else Case when TreatID = 5 Then 3 Else Case when TreatID = 1 Then 4 Else Case when TreatID = 6 Then 5 Else Case when TreatID = 2 Then 6 End End End End End  End as RowNo from Treatment where TreatID in (1,2,3,5,6) order by RowNo");
            ddlTreatement.DataSource = dsChoice;
            ddlTreatement.DataTextField = "TreatDesc";
            ddlTreatement.DataValueField = "code";
            ddlTreatement.DataBind();
            ddlTreatement.Items.Insert(0, new ListItem("Select One", "-1"));
            if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "4")
            {
                ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("Grantee"));
            }
            else if (GetCode(ddlTransactionsType.SelectedValue.ToString())[0] == "7")
            {
                ddlTreatement.SelectedIndex = ddlTreatement.Items.IndexOf(ddlTreatement.Items.FindByText("To be Reimbursed"));
            }
            else { ddlTreatement.SelectedIndex = 0; }


        }
    }
    protected void txtReport_TextChanged(object sender, EventArgs e)
    {
        if (ddlReportDate.SelectedItem.Value == "-2" && txtReport.Text == "")
        {
            reportold.Visible = false;
            reportnew.Visible = true;
            txtReport.Text = DateTime.Now.ToShortDateString();
        }
        else
            DisplayRecords();
    }
    protected void btnClearDate_Click(object sender, EventArgs e)
    {
        GetReportDate(ddlReportDate, int.Parse(ddlChapters.SelectedItem.Value),false);
    }
}