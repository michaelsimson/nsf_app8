using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.UI.WebControls.WebParts;


public partial class ScholarshipReport : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection("data source=sql.northsouth.org;user id=northsouthdev;password=everykosamu;Initial catalog=NSFIndia_Dev");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"]!= null)
        {
            tblyear.Visible = true;
        }
      
    }
    protected void btn_Click(object sender, EventArgs e)
    {
        gvcount.DataSource = null;
        gvcount.DataBind();
        gvchapter.DataSource = null;
        gvchapter.DataBind();
        gvcourse.DataSource = null;
        gvcourse.DataBind();

        if (Request.QueryString["id"].ToString() == "1")
        {
            String strsql = "Select b.chapter,b.ID,COUNT(e.Gender)AS Male,COUNT(d.Gender)AS Female,SUM(amount)AS Total,Avg(amount)As Average from  applicant c Left JOIN applicant d ON  d.id=c.id and d.Gender ='F' Left JOIN applicant e ON  e.id=c.id and e.Gender ='M' INNER JOIN fin_aid a ON a.appli_id = c.id INNER JOIN rd_chapter b ON b.id = c.chapter_id And a.accyear=" + ddlyear.SelectedValue + " Group BY b.chapter,b.id";
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter(strsql, conn);
            DataSet ds = new DataSet();
            adap.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvchapter.DataSource = ds;
                gvchapter.DataBind();
            }
            else
            {
                if (ds.Tables[0].Rows.Count == 0)
                {
                    gvchapter.DataSource = null;
                    gvchapter.DataBind();
               }
            }
        }
        else
        {
            String strsql = "Select f.crs as Course,COUNT(e.Gender)AS Male,COUNT(d.Gender)AS Female,Avg(amount) As Average,SUM(amount)AS Total from  applicant c Left JOIN applicant d ON  d.id=c.id and d.Gender ='F' Left JOIN applicant e ON  e.id=c.id and e.Gender ='M' INNER JOIN curr_edu g ON g.id=c.curr_edu_id INNER JOIN  rd_course f ON f.id=g.crs_id INNER JOIN fin_aid a ON a.appli_id=c.id  Where  a.accyear=" + ddlyear.SelectedValue + " Group BY f.crs";
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter(strsql, conn);
            DataSet ds = new DataSet();
            adap.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvcourse.DataSource = ds;
                gvcourse.DataBind();
            }
            else
            {
                if (ds.Tables[0].Rows.Count == 0)
                {
                    gvcourse.DataSource = null;
                    gvcourse.DataBind();
                }
            }
        }
    }
   
    protected void gvchapter_Rowcommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        try
        {
            String chapterid = gvchapter.DataKeys[index].Value.ToString();
            hdchapid.Value = chapterid;
            String strsql = "Select f.crs as Course,COUNT(e.Gender)AS Male,COUNT(d.Gender)AS Female,Avg(amount) As Average,SUM(amount)AS Total from  applicant c Left JOIN applicant d ON  d.id=c.id and d.Gender ='F' Left JOIN applicant e ON  e.id=c.id and e.Gender ='M' INNER JOIN curr_edu g ON g.id=c.curr_edu_id INNER JOIN  rd_course f ON f.id=g.crs_id INNER JOIN fin_aid a ON a.appli_id=c.id  Where c.chapter_id=" + chapterid + " And a.accyear=" + ddlyear.SelectedValue + " Group BY f.crs";
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter(strsql, conn);
            DataSet ds = new DataSet();
            adap.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvcourse.DataSource = ds;
                gvcourse.DataBind();
            }

        }
        catch( Exception ex)
        {
            Response.Write(index);
            Response.Write(ex.ToString());

        }
       
    }
     protected void gvcourse_Rowcommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        try
        {
            String course= gvcourse.DataKeys[index].Value.ToString();
            String chapterwrcntn = "";
            if (hdchapid.Value.Length > 0)
                chapterwrcntn = " app.chapter_id= " + hdchapid.Value + " And";
            String strsql = "Select app.id,chap.chapter,app.firstname,app.lastname,app.Gender,app.dob,app.pob,app.email,ad.line1,ad.line2,ad.vtc,ad.district,st.state,ad.country,ad.pin from applicant app Left JOIN address ad ON  app.p_address_id=ad.id Left JOIN rd_state st ON  st.id=ad.state_id Left JOIN fin_aid fin ON fin.appli_id = app.id INNER JOIN curr_edu cur ON cur.id=app.curr_edu_id Left JOIN  rd_course cour ON cour.id=cur.crs_id Left JOIN rd_chapter chap ON chap.id = app.chapter_id where " + chapterwrcntn + " fin.accyear=" + ddlyear.SelectedValue + " And cour.crs='" + course + "'Group BY chap.chapter,app.id,app.firstname,app.lastname,app.Gender,app.dob,app.pob,app.email,ad.line1,ad.line2,ad.vtc,ad.district,st.state,ad.country,ad.pin";
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter(strsql, conn);
            DataSet ds = new DataSet();
            adap.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvcount.DataSource = ds;
                gvcount.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(index);
            Response.Write(ex.ToString());

        }

    }
    
}
