

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ScholarshipReport.aspx.cs" Inherits="ScholarshipReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Scholarship Details</title>
</head>
<body>
    <form id="form1" runat="server">
   <table border="1" cellpadding="4" cellspacing="0" width="900px" align="center">
    <tr><td colspan="2" align="center" style="font-family:Arial; font-size:16px; font-weight:bold; color : Green "> 
        India Scholarship Report</td></tr>
    <tr>
    <td align="center" width="50%"><a href="ScholarshipReport.aspx?id=1">Scholarships by Chapters</a> </td>
    <td align="center" width="50%"><a href="ScholarshipReport.aspx?id=2">Scholarships by Course of Study </a> </td>
    </tr>
    <tr>
    <td runat="server" id="tblyear" visible="false"  align="center" colspan ="2">
    
    <asp:dropdownlist ID="ddlyear" runat="server" Width="120px">
        <asp:ListItem>2007</asp:ListItem>
        <asp:ListItem>2008</asp:ListItem>
        <asp:ListItem>2009</asp:ListItem>
        <asp:ListItem>2010</asp:ListItem>
        <asp:ListItem>2011</asp:ListItem>
        </asp:dropdownlist>
        <br /><br />
          <asp:Button ID="btn" runat="server" Text="Submit" onclick="btn_Click" 
              Width="80px"/>
          
    </td>
    </tr>
    </table>
    <br />
            
    <br />
    <table  cellpadding="4" cellspacing="0">
    <tr>
    <td align="left">
    <asp:GridView ID="gvchapter" OnRowCommand="gvchapter_Rowcommand" DataKeyNames="ID" 
            AutoGenerateColumns ="False"  runat="server" CellPadding="4" 
            ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#E3EAEB" />
    <Columns>
    <asp:BoundField DataField ="ID" HeaderText="ID" />
    <asp:ButtonField CommandName="Select" DataTextField="Chapter"   headerText="Chapter" ></asp:ButtonField>
    <asp:BoundField DataField ="Male" HeaderText="Male" />
    <asp:BoundField DataField ="Female" HeaderText="Female" />
    <asp:BoundField DataField ="Average" HeaderText="Average" DataFormatString="{0:F}"/>
    <asp:BoundField DataField ="Total" HeaderText="Total" DataFormatString="{0:F}"/>
</Columns>
        <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#7C6F57" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    </td>
     </tr>
   <tr>
    <td  align="left">
        <asp:GridView ID="gvcourse"  OnRowCommand="gvcourse_Rowcommand" 
            DataKeyNames="Course" BorderWidth="2px" AutoGenerateColumns ="False"  runat="server" 
             CellPadding="4" GridLines="None" ForeColor="#333333" >
            <RowStyle BackColor="#E3EAEB" />
            <Columns >
     <asp:ButtonField CommandName="Select" DataTextField="Course"   headerText="Course" ></asp:ButtonField>
    <asp:BoundField DataField ="Male" HeaderText="Male" />
    <asp:BoundField DataField ="Female" HeaderText="Female" />
    <asp:BoundField DataField ="Average" HeaderText="Average" DataFormatString="{0:F}"/>
    <asp:BoundField DataField ="Total" HeaderText="Total" DataFormatString="{0:F}"/>
    </Columns>
            <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="#F7F7F7" />
       </asp:GridView>
       </td>
 
    </tr>
   
    <tr>
    <td  align="left">
    <asp:GridView ID="gvcount" DataKeyNames="id" AutoGenerateColumns ="False"  
            runat="server" BackColor="White" BorderColor="#3366CC" BorderStyle="None" 
            BorderWidth="1px" CellPadding="4">
            <RowStyle BackColor="White" ForeColor="#003399" />
            <Columns>
            <asp:BoundField DataField ="id" HeaderText="ID" />
            <asp:BoundField DataField ="chapter" HeaderText="Chapter" />
            <asp:BoundField DataField ="firstname" HeaderText="First Name" />
            <asp:BoundField DataField ="lastname" HeaderText="Last Name" />
            <asp:BoundField DataField ="Gender" HeaderText="Gender" />
            <asp:BoundField DataField ="dob" HeaderText="Date of Birth" DataFormatString="{0:d}"/>
            <asp:BoundField DataField ="pob" HeaderText="Place of Birth" />
            <asp:BoundField DataField ="email" HeaderText="Email" />
            <asp:BoundField DataField ="line1" HeaderText="Address Line1" />
            <asp:BoundField DataField ="line2" HeaderText="Address Line2" />
            <asp:BoundField DataField ="vtc" HeaderText="Village/Town/City" />
            <asp:BoundField DataField ="district" HeaderText="District" />
            <asp:BoundField DataField ="state" HeaderText="State" />
            <asp:BoundField DataField ="country" HeaderText="Country" />
            <asp:BoundField DataField ="pin" HeaderText="Pincode" />
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
       </asp:GridView>
       </td>
     </tr>
    </table>
    <asp:HiddenField ID="hdchapid" runat="server" />
    </form>
</body>
</html>
