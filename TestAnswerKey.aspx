<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="TestAnswerKey.aspx.vb" Inherits="TestAnswerKey" Title="Test Answer Key" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    </div>
    <table cellpadding="2" cellspacing="0" border="0" width="1250px" style="display: none;">
        <tr>
            <td class="btn_02" align="center">
                <asp:Label ID="lblmsg" runat="server" ForeColor="Green" Text="Add/Update TestAnswerKey for HomeWork/Tests"></asp:Label></td>
        </tr>
    </table>
    <table border="1" width="100%" style="color: Gray; display: none;">
        <tr>
            <td width="110px"><b>EventYear:</b>
            </td>
            <td width="110px"><b>Event:</b>
                <asp:DropDownList ID="ddlEvent" runat="server">
                    <asp:ListItem Value="13">Coaching</asp:ListItem>
                </asp:DropDownList></td>

            <td width="110px"><b>Semester:</b>
            </td>

            <td width="160px"><b>ProductGroup:</b></td>
            <td width="180px"><b>Product:</b>
            </td>
            <td width="180px"><b>Level:</b>
            </td>
            <td width="140px"><b>PaperType:</b></td>
            <td width="100px"><b>WeekID:</b>
            </td>
            <td width="100px"><b>SetNum:</b>
            </td>
            <td width="100px"><b>Sections:</b>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center">
                <%-- <asp:Button ID="" runat="server" Text="Submit" />
                <asp:Button ID="" runat="server" Text="Cancel" />--%>

            </td>
        </tr>
    </table>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvSeacrhCoachpapers" runat="server" style="margin: auto; width: 1100px;" visible="true">
        <div style="margin-top: 10px; margin-left: 20px;">
            <center>
                <h2><span style="color: green;">Answer keys</span></h2>
            </center>

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroup"
                            DataTextField="Name" DataValueField="ProductGroupID"
                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                            AutoPostBack="true" Enabled="false" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProduct" DataTextField="Name" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" DataValueField="ProductID" runat="server" AutoPostBack="true" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlLevel" DataTextField="Name" DataValueField="Level" Enabled="false" runat="server" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="True" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">

                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label19" runat="server" Font-Bold="true" Text="Paper Type"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 3px;">
                        <asp:DropDownList ID="ddlPaperType" runat="server" Width="110">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                            <asp:ListItem Value="PT">Pretest</asp:ListItem>
                            <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                            <asp:ListItem Value="FT">Final Test</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 32px;">
                    <div style="float: left;">
                        <asp:Label ID="Label21" runat="server" Font-Bold="true">Week# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 23px;">
                        <asp:DropDownList ID="ddlWeekID" runat="server" OnSelectedIndexChanged="ddlWeekID_SelectedIndexChanged" AutoPostBack="True" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 32px; display: none;">
                    <div style="float: left;">
                        <asp:Label ID="Label2" runat="server" Font-Bold="true">Set# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 65px;">
                        <asp:DropDownList ID="ddlSetNo" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div id="Div1" style="float: left; margin-left: 30px;" runat="server">
                    <div style="float: left;">
                        <asp:Label ID="Label20" runat="server" Font-Bold="true" Text="Sections"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 43px;">
                        <asp:DropDownList ID="ddlSections" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


                <div style="float: left; margin-left: 20px;">

                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnCancelPage" runat="server" Text="Cancel" OnClick="btnCancelPage_Click" />
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <table align="center">
        <tr>
            <td align="center">
                <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrd" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblPrdGrp" runat="server" Text="" Visible="false"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAnsKeyError" runat="server" Text="" ForeColor="Red"></asp:Label></td>
        </tr>

    </table>

    <table align="center" id="tblDGAnswerKey" runat="server" width="600px" visible="false" border="1">

        <tr bgcolor="#FFFFFF">
            <td colspan="8" align="center">
                <asp:Label ID="lblAnswerKey" runat="server" Text="" ForeColor="#AE5C00" Font-Bold="true"></asp:Label>
                <span style="float: right">
                    <asp:Button ID="btnManualAll" runat="server" Text="Set All Manual" /></span>
                <asp:DataGrid ID="dgAnswerKey" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" OnItemDataBound="dgAnswerKey_ItemDataBound"
                    PagerStyle-Mode="numericpages" DataKeyField="AnswerKeyRecID" AllowPaging="True" AllowSorting="True" PageSize="30">
                    <FooterStyle CssClass="GridFooter"></FooterStyle>
                    <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                    <AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                    <HeaderStyle BackColor="#C49C00"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="AnswerKeyRecID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>
                        <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="CoachPaperID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="TestNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="SectionNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="SectionNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="Level" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="Level" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="QuestionNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="QuestionNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="CorrectAnswer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="105px" HeaderStyle-Width="105px" ItemStyle-BackColor="Gainsboro">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCorrectAnswer" runat="server" TextMode="MultiLine" Rows="3" Columns="20" OnPreRender="ddlCorrectAnswer_PreRender" Text="<%# Bind('CorrectAnswer') %>"></asp:TextBox>
                                <asp:DropDownList ID="ddlCorrectAnswer" runat="server" Width="95px" OnPreRender="ddlCorrectAnswer_Prerender" SelectedValue="<%# Bind('CorrectAnswer_DD') %>">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="A">A</asp:ListItem>
                                    <asp:ListItem Value="B">B</asp:ListItem>
                                    <asp:ListItem Value="C">C</asp:ListItem>
                                    <asp:ListItem Value="D">D</asp:ListItem>
                                    <asp:ListItem Value="E">E</asp:ListItem>
                                    <asp:ListItem Value="F">F</asp:ListItem>
                                    <asp:ListItem Value="G">G</asp:ListItem>
                                    <asp:ListItem Value="H">H</asp:ListItem>
                                    <asp:ListItem Value="I">I</asp:ListItem>
                                    <asp:ListItem Value="J">J</asp:ListItem>
                                    <%----%>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="DifficultyLevel" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlDiffLevel" runat="server" OnPreRender="ddlDiffLevel_Prerender" SelectedValue="<%# Bind('DifficultyLevel') %>">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="Manual" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlManual" runat="server" OnPreRender="ddlManual_Prerender" SelectedValue="<%# Bind('Manual') %>">
                                    <asp:ListItem Value="">No</asp:ListItem>
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="QuestionType" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="QuestionType" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="CorrectAnswer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="CorrAnswer" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>

                    </Columns>
                    <PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr id="TrSaveAnsKey" runat="server">
            <td align="center">
                <asp:Button ID="btnSaveAnswers" runat="server" Text="Save Answers" /></td>
            <td align="center">
                <asp:Button ID="btnCancel" runat="server" Text="Clear" /></td>


        </tr>
    </table>

    <table id="tblDGTestSection" runat="server" cellpadding="2" cellspacing="0" align="center" border="0">
        <tr>
            <td align="center">
                <asp:Label ID="lblTestSection" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label>

                <asp:DataGrid ID="DGTestSection" runat="server" DataKeyField="TestSetUpSectionsID" AutoGenerateColumns="False"
                    CellPadding="4" BorderStyle="None" CellSpacing="2" ForeColor="Black"
                    OnItemCommand="DGTestSection_Itemcommand">
                    <FooterStyle BackColor="#CCCCCC" />
                    <%--<SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />--%>
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <HeaderStyle BackColor="#A69B00" ForeColor="Blue" />
                    <ItemStyle BackColor="White" />
                    <EditItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />


                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnTSSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TestSetUpSectionsID" HeaderText="ID" Visible="false" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionNumber" HeaderText="SectionNumber" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Subject" HeaderText="Subject" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionTimelimit" HeaderText="Timelimit" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfQuestions" HeaderText="#OfQuestions" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionType" HeaderText="QuestionType" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfChoices" HeaderText="NumberOfChoices" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberFrom" HeaderText="QuestionNoFrom" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberTo" HeaderText="QuestionNumberTo" />

                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <table id="tblDGCoachPaper" runat="server" align="center" cellpadding="2" cellspacing="0" border="0">
        <tr>
            <td colspan="8" align="center">
                <asp:Label CssClass="btn02" ID="lblCoachPaper" runat="server" Text="" ForeColor="Brown" Font-Bold="true"></asp:Label>

                <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                    BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                    OnItemCommand="DGCoachPapers_ItemCommand">
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                    <ItemStyle BackColor="White" />
                    <Columns>

                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                <div style="display: none;">
                                    <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestSetup" HeaderText="TestSetup"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="PaperID"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>

                    </Columns>

                </asp:DataGrid></td>
        </tr>
    </table>

</asp:Content>

