﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShoppingSummary.aspx.vb" Inherits="ShoppingSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <LINK href="Styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
    <title>Shopping Summary</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <table border="0" cellpadding ="3" cellspacing = "0" width ="1000">
    <tr><td align="center">
   <img src="images/trilogo.gif" width="980px" alt ="" />
	</td></tr>
	<tr>
  <td align="left"> &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
  &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="Hyperlink3" runat="server" NavigateUrl="ShoppingCatalog.aspx?id=1">Back to Previous Page</asp:hyperlink>
  </td> </tr>
  <tr>
  <td align="center">
   <table border="0" cellpadding ="3" cellspacing = "0" >
    <tr><td align="center">
    <h4>Selected Items for Purchase</h4>
    </td> </tr> 
     <tr><td align="Right">Chapter: 
         <asp:DropDownList ID="ddlChapter" Enabled="false"  runat="server">
         </asp:DropDownList>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Event    
            <asp:DropDownList Width="175px" Enabled="false"  ID="ddlEvent" runat="server">
            <asp:ListItem Value="1">National Finals</asp:ListItem>
            <asp:ListItem Value="2">Chapter Contests</asp:ListItem>
            <asp:ListItem Value="3">Workshop</asp:ListItem> 
            <asp:ListItem Value="5">Walk-a-thon</asp:ListItem> 
            <asp:ListItem Value="12">Marathon</asp:ListItem> 
            <asp:ListItem Value="9">Entertainment Program</asp:ListItem> 
            <asp:ListItem Value="10">None (Shop)</asp:ListItem>             
            </asp:DropDownList>
    </td> </tr> 
    
    <tr><td align="center" >
    <asp:datagrid id="dgCatalog"  runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="2px" CellPadding="3" 
          DataKeyField="SaleTranID" BackColor="White" BorderColor="White" 
          BorderStyle="Ridge" GridLines="None" CellSpacing="1">
			<FooterStyle CssClass="GridFooter" BackColor="#C6C3C6" ForeColor="Black"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#9471DE" Font-Bold="True" 
                ForeColor="White"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#DEDFDE" 
                ForeColor="Black"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" 
                ForeColor="#E7E7FF" ></HeaderStyle>
			<Columns>
		<asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
			<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
            <asp:Label ID="lblQuantity" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:label>
       </ItemTemplate>	
      <EditItemTemplate>
      <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>' Width="50px" id="txtQuantity"></asp:TextBox>
      </EditItemTemplate>       											
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="CatID" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
           <asp:Label ID="lblCatID" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "CatID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Category" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblCategory" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Category") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="ShortName" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label ID="lblShortName" Runat=server CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ShortName")  %>'>
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Description" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblDescription" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Description") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblUnitPrice" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"UnitPrice","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Amount" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblAmount" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="SaleTranID" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblSaleTranID" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"SaleTranID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#C6C3C6"></PagerStyle>
		</asp:datagrid>
		</td></tr>
		<tr><td align = "right" >
		  <asp:Label ID="lblTotal" runat="server"></asp:Label>	&nbsp;&nbsp;&nbsp;	
		</td> </tr>
		<tr><td align = "right" >
		         <asp:Button ID="btnPay" runat="server" Width="100px" OnClick="btnPay_Click" Text="Pay" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td> </tr> 
		 </table> 
     </td> </tr>
    
     <tr>
  <td align="center">
      <asp:Label ID="lblerr" ForeColor = "Red" runat="server"></asp:Label>
      <asp:Label ID="lblCustIndID" ForeColor="White" runat="server"></asp:Label>
   </td> </tr>
    
     <tr>
  <td align="left">
   <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>       
  </td></tr>  </table>       
    </div>
    </form>
</body>
</html>
