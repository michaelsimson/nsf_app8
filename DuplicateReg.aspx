<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false"  Inherits="VRegistration.DuplicateReg" CodeFile="DuplicateReg.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main"  Runat="Server">
        <div>
            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" NavigateUrl="~/volunteerfunctions.aspx" Text="Back to Main Menu" ></asp:HyperLink>
            &nbsp;&nbsp;&nbsp;			
        </div>
        <div>
        <asp:Panel runat="server" ID="pnlDuplicate">
			<P><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">
                Your record &nbsp;is already in our database.&nbsp; You can follow one of the steps below 
                to proceed further:</SPAN></FONT></P>
			<P><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">1. 
                If you forgot your password,</SPAN></FONT>
                <asp:LinkButton ID="hlnkForgotPwd" runat="server"  CssClass="btn_02" NavigateUrl="~/Forgot.aspx"> 
                Click here</asp:LinkButton></P>
			<P><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">2. 
                If you forgot your login ID (Primary Email Address), &nbsp;<asp:LinkButton runat="server" ID="hlnkLoginID"  CssClass="btn_02"  Text="Click here"></asp:LinkButton></SPAN></FONT></P>
            <p>
                <font face="Arial" size="2"><span style="font-size: 11pt; font-family: Arial"></span>
                </font><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">
                    3. If your primary email address is no longer valid, but you know your password, &nbsp;<asp:LinkButton runat="server" ID="hlnkEmailAddress"  CssClass="btn_02"  Text="Click here" ></asp:LinkButton> </SPAN></FONT></p>			
			<p>
                <font face="Arial" size="2"><span style="font-size: 11pt; font-family: Arial"></span>
                </font><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">
                    4. If your primary email address is no longer valid plus you also forgot your password, &nbsp;<asp:LinkButton runat="server" ID="hlnkSendEmailAddress"  CssClass="btn_02"  Text="Click here" ></asp:LinkButton> </SPAN></FONT></p>			
			
			<p>
                <font face="Arial" size="2"><span style="font-size: 11pt; font-family: Arial"></span>
                </font><FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">
                    5. Otherwise, &nbsp;<asp:LinkButton runat="server"  CssClass="btn_02"   ID="hlnkSendEmail" PostBackUrl="#" Text="Click here" ></asp:LinkButton> 
                    &nbsp; for customer service to contact you.</SPAN></FONT></p>
			<P>&nbsp;
	        </asp:Panel>
	    <asp:Panel runat="server" ID="pnlMessage" Visible="false" >
	        <div>
	            <p>
	                <FONT face="Arial" size="2"><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">
	                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
	                </SPAN> 
	                </FONT> 
	            </p>
	        </div>
	    </asp:Panel>
			</div>
			
			</asp:Content>

 

 
 
 