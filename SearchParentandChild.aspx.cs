﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Data.SqlClient;

public partial class SearchParentandChild : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    string strSql;
    bool Ischild = false;
    string StrQrySearch;
    string WhCont = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"] = "4240";

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }


        if (!IsPostBack)
        {
            States();
        }

    }
    protected void States()
    {
        try
        {
            string DDLstate = "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
            ddlState.DataSource = dsstate;
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "StateCode";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "Select State");
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        FindChild();
    }
    //public void SearchChild()
    //{
    //    lblchError.Text = "";
    //    bool executeQuery = false;

    //    txtEmail.Text = txtEmail.Text.Trim();

    //    string sqlString = "SELECT distinct  CH.ChildNumber,CH.FIRST_NAME ,CH.LAST_NAME ,CH.GRADE,CH.SchoolName,CH.DATE_OF_BIRTH, ";
    //    sqlString += " I.FirstName +' ' + I.LastName as ParentName,CH.MEMBERID,I.Chapter as Center,I.City,I.State,";
    //    sqlString += " I.Email as ParentsEmail,I.HPhone as ParentsPhone ";
    //    sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID  ";
    //    sqlString += "  left JOIN IndSpouse I1 ON CH.Memberid=I1.Relationship  ";
    //    // sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON (CH.Memberid=I.AutoMemberID or CH.Memberid=I.relationship)  ";
    //    sqlString += "  left join StateMaster St on st.StateCode=I.State ";
    //    string chName, pName, pEmail;

    //    if (txtChName.Text != string.Empty)
    //    {
    //        chName = txtChName.Text.Replace(" ", "");
    //        if (chName.Contains("_"))
    //        {
    //            chName = "'%" + chName.Replace("_", "/_") + "%' ESCAPE '/' ";
    //        }
    //        else
    //            chName = "'%" + chName + "%'";

    //        if (WhCont == string.Empty)
    //        {
    //            WhCont = WhCont + " Where ltrim(rtrim(CH.first_name)) + ltrim(rtrim(CH.last_name)) like " + chName + "  ";
    //            executeQuery = true;
    //        }
    //        else
    //        {
    //            WhCont = WhCont + " and ltrim(rtrim(CH.first_name)) + ltrim(rtrim(CH.last_name)) like " + chName + " ";
    //        }
    //    }
    //    if (txtParentName.Text != string.Empty)
    //    {
    //        pName = txtParentName.Text.Replace(" ", "");
    //        if (pName.Contains("_"))
    //        {
    //            pName = "'%" + pName.Replace("_", "/_") + "%' ESCAPE '/' ";
    //        }
    //        else
    //            pName = "'%" + pName + "%'";

    //        if (WhCont == string.Empty)
    //        {
    //            WhCont = WhCont + " Where (ltrim(rtrim(I.firstname)) + ltrim(rtrim(I.lastname)) like " + pName + " OR ltrim(rtrim(I1.firstname)) + ltrim(rtrim(I1.lastname)) like " + pName + ")";
    //            executeQuery = true;
    //        }
    //        else
    //        {
    //            WhCont = WhCont + " and ( ltrim(rtrim(I.firstname)) + ltrim(rtrim(I.lastname)) like " + pName + " OR ltrim(rtrim(I1.firstname)) + ltrim(rtrim(I1.lastname)) like " + pName + ")";
    //            executeQuery = true;
    //        }
    //    }

    //    if (txtEmail.Text != string.Empty)
    //    {
    //        pEmail = txtEmail.Text;
    //        if (pEmail.Contains("_"))
    //        {
    //            pEmail = "'%" + pEmail.Replace("_", "/_") + "%' ESCAPE '/' ";
    //        }
    //        else
    //            pEmail = "'%" + pEmail + "%'";
    //        if (WhCont == string.Empty)
    //        {
    //            WhCont = WhCont + " Where (I.Email like " + pEmail + " OR I1.Email like " + pEmail + ")";
    //            executeQuery = true;
    //        }
    //        else
    //        {
    //            WhCont = WhCont + " and (I.Email like " + pEmail + " OR I1.Email like " + pEmail + ")";
    //            executeQuery = true;
    //        }
    //    }
    //    if (txtPhone.Text != string.Empty)
    //    {
    //        if (WhCont == string.Empty)
    //        {
    //            WhCont = WhCont + " Where ( I.Hphone like '%" + txtPhone.Text + "%' OR I1.Hphone like '%" + txtPhone.Text + "%')";
    //            executeQuery = true;
    //        }
    //        else
    //        {
    //            WhCont = WhCont + " and ( I.Hphone like '%" + txtPhone.Text + "%' OR I1.Hphone like '%" + txtPhone.Text + "%')";
    //            executeQuery = true;
    //        }
    //    }
    //    try
    //    {
    //        DataSet ds = null;
    //        Ischild = true;

    //        // SearchMembers(WhCont);
    //        if (executeQuery == true)
    //        {
    //            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlString + WhCont);
    //        }
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            Panel5.Visible = true;
    //            GVchild.Visible = true;
    //            GVchild.DataSource = ds.Tables[0];
    //            GVchild.DataBind();
    //            lblchError.Text = "";
    //        }
    //        else
    //        {
    //            Panel5.Visible = false;
    //            lblchError.Text = "No Record found";
    //        }
    //    }
    //    catch (Exception ex) { }
    //}
    protected void Button2_Click(object sender, EventArgs e)
    {
        //  if (hdnSeacrhTerm.Value.ToLower() == "parent")
        //  {
        FindParent();
        FindChild();
        //  }
        // else if (hdnSeacrhTerm.Value.ToLower() == "child")
        // {
        //  FindChild();
        // }

    }
    private void SearchMembers(string StrSearchcond)
    {
        try
        {
            //if (Ischild == true)
            //{
            StrQrySearch = "select distinct automemberid,Employer,firstname,lastname, firstName+' '+lastname as name,donortype,email,case when Email<>'' then 'Yes' else 'NO' end as [Exists],hphone,chapter,Cphone,"
    + " address1,city,state,zip,chapterCode ,referredby,liasonperson from ( select distinct I.automemberid,I.Employer,I.firstname,I.lastname,I1.firstname+' '+I1.lastName as name,I.donortype,I.email,case when I.Email<>'' then 'Yes' else 'NO' end as [Exists],I.hphone,I.chapter,I.Cphone,"
    + " I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID left JOIN IndSpouse I1 ON I.automemberid=I1.Relationship and I.AutomemberID is not null  " + StrSearchcond + " "
    + " UNION ALL select distinct I1.automemberid,I1.Employer,I1.firstname,I1.lastname, I1.firstname+' '+I1.lastName as name,I1.donortype,I1.email,case when I1.Email<>'' then 'Yes' else 'NO' end as [Exists],I1.hphone,I1.chapter,I1.Cphone,"
    + " I1.address1,I1.city,I1.state,I1.zip,C.chapterCode ,I1.referredby,I1.liasonperson from indspouse I1 LEFT JOIN Chapter C ON C.ChapterID = I1.ChapterID left JOIN IndSpouse I ON I.automemberid=I1.Relationship and I1.AutomemberID is not null " + StrSearchcond + ""
    + " ) as tb order by name";

            //    }
            //    else
            //    {
            //        StrQrySearch = "select distinct I.automemberid,I.Employer,I.firstname,I.lastname,I.donortype,I.email,case when I.Email<>'' then 'Yes' else 'NO' end as [Exists],I.hphone,I.chapter,I.Cphone,"
            //+ " I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID where  " + StrSearchcond + ""; 

            //    }

            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
            {
                lbsearchresults.Visible = true;
                lbsearchresults.Text = "No Search found ";
            }
            else
            {
                lbsearchresults.Visible = false;
            }

            Panel4.Visible = false;
            Panel3.Visible = true;
            GVLogin.Visible = true;
            GVprofile.Visible = true;
            GVLogin.DataSource = ds;

            GVLogin.DataBind();
            GVprofile.DataSource = ds;
            GVprofile.DataBind();
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        pIndSearch.Visible = true;
        Pnlchild.Visible = false;
        visiblePanel();
        txtLastName.Text = string.Empty;
        txtFirstName.Text = string.Empty;
        TextBox2.Text = string.Empty;
        ddlState.SelectedIndex = 0;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Pnlchild.Visible = true;
        Ischild = true;
        pIndSearch.Visible = false;
        visiblePanel();
        txtChName.Text = string.Empty;
        txtParentName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhone.Text = string.Empty;

    }
    protected void btnIndClose_Click(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;
        Panel4.Visible = false;
        Panel3.Visible = false;
        visiblePanel();

    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        Pnlchild.Visible = false;
        GVchild.Visible = false;
        Panel5.Visible = false;
        visiblePanel();
    }

    protected void visiblePanel()
    {
        GVchild.Visible = false;
        GVLogin.Visible = false;
        GVprofile.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }

    protected void RbtnSearchParent_CheckedChanged(object sender, EventArgs e)
    {
        hdnSeacrhTerm.Value = "Parent";
        Clear();
    }


    protected void RbtnSearchChild_CheckedChanged(object sender, EventArgs e)
    {
        hdnSeacrhTerm.Value = "Child";
        Clear();
    }

    public void FindParent()
    {
        try
        {
            Panel5.Visible = false;

            string firstName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            string state = string.Empty;
            string PhoneNo = string.Empty;
            string MemberId = string.Empty;
            firstName = txtFirstName.Text;
            lastName = txtLastName.Text;
            email = TextBox2.Text;
            PhoneNo = TxtPhoneNo.Text;
            MemberId = txtmemberId.Text;

            int length = 0;
            if (firstName.Length > 0)
            {
                firstName = firstName.Replace(" ", "");
                if (firstName.Contains("_"))
                {
                    firstName = "'%" + firstName.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                    firstName = "'%" + firstName + "%'";

                strSql += (" (ltrim(rtrim(I.firstName)) like " + firstName + " or  ltrim(rtrim(I1.firstName)) like " + firstName + " )");
            }
            if (lastName.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                lastName = lastName.Replace(" ", "");
                if (lastName.Contains("_"))
                {
                    lastName = "'%" + lastName.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                {
                    lastName = "'%" + lastName + "%'";
                    //if (firstName != "")
                    //{
                    //    lastName = "'" + lastName + "'";
                    //}
                    //else
                    //{
                    //    lastName = "'%" + lastName + "'";
                    //}
                }

                if (length > 0)
                {
                    strSql += (" and (ltrim(rtrim(I.lastName)) like " + lastName + " or ltrim(rtrim(I1.lastName)) like " + lastName + " )");
                }
                else
                {
                    strSql += ("  (ltrim(rtrim(I.lastName)) like " + lastName + " or ltrim(rtrim(I1.lastName)) like " + lastName + " )");
                }
            }

            if (email.Length > 0)
            {
                if (email.Contains("_"))
                {
                    email = "'%" + email.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                    email = "'%" + email + "%'";

                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and (I.Email like " + email + " or I1.Email like " + email + " ) ");
                }
                else
                {
                    strSql += (" (I.Email like " + email + " or I1.Email like " + email + ")");
                }
            }
            if (PhoneNo.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and (I.Hphone like '%" + TxtPhoneNo.Text + "%' OR I1.Hphone like '%" + TxtPhoneNo.Text + "%') ");
                }
                else
                {
                    strSql += "  (I.Hphone like '%" + TxtPhoneNo.Text + "%' OR I1.Hphone like '%" + TxtPhoneNo.Text + "%')";
                }
            }
            if (ddlState.SelectedItem.Text != "Select State")
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                state = ddlState.SelectedValue;
                if (length > 0)
                {
                    strSql += (" and ( I.state like '%" + state + "%' or I1.state like '%" + state + "%')");
                }
                else
                {
                    strSql += (" (I.state like '%" + state + "%' or I1.state like '%" + state + "%')");
                }
            }

            if (MemberId.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and (I.MemberId like '%" + txtmemberId.Text + "%' OR I1.MemberId like '%" + txtmemberId.Text + "%') ");
                }
                else
                {
                    strSql += "  (I.MemberId like '%" + txtmemberId.Text + "%' OR I1.MemberId like '%" + txtmemberId.Text + "%')";
                }
            }

            if (strSql.Length > 0)
            {
                strSql = " Where " + strSql;
            }

            if ((firstName.Length > 0) || (lastName.Length > 0) || (email.Length > 0) || (state.Length > 0) || (MemberId.Length > 0))
            {
                // strSql += ("  order by I.lastname,I.firstname");
                SearchMembers(strSql);
                //  SearchChild();
                // FindChild();
            }
            else
            {
                lbsearchresults.Text = "Please Enter Email (or) First Name (or) Last Name (or) state";
                lbsearchresults.Visible = true;
            }
        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }

    public void FindChild()
    {
        lblchError.Text = "";
        bool executeQuery = false;

        txtEmail.Text = txtEmail.Text.Trim();

        string sqlString = "SELECT distinct  CH.ChildNumber,CH.FIRST_NAME ,CH.LAST_NAME, CH.FIRST_NAME+' '+CH.LAST_NAME as ChildName ,CH.GRADE,CH.SchoolName,CH.DATE_OF_BIRTH, ";
        sqlString += " I.FirstName +' ' + I.LastName as ParentName,CH.MEMBERID,I.Chapter as Center,I.City,I.State, I1.FirstName +' ' + I1.LastName as Parent2Name, I1.HPhone as Parents2Phone, I1.Email as Parents2Email,";
        sqlString += " I.Email as ParentsEmail,I.HPhone as ParentsPhone ";
        sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID  ";
        sqlString += "  left JOIN IndSpouse I1 ON CH.Memberid=I1.Relationship  ";
        // sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON (CH.Memberid=I.AutoMemberID or CH.Memberid=I.relationship)  ";
        sqlString += "  left join StateMaster St on st.StateCode=I.State ";
        string chName, pName, pEmail;

        string ChildFirstName = string.Empty;
        string ChildLastName = string.Empty;

        ChildFirstName = txtFirstName.Text.Trim();
        ChildLastName = txtLastName.Text.Trim();
        int length = 0;
        if (ChildFirstName.Length > 0)
        {
            ChildFirstName = ChildFirstName.Replace(" ", "");
            if (ChildFirstName.Contains("_"))
            {
                ChildFirstName = "'%" + ChildFirstName.Replace("_", "/_") + "%' ESCAPE '/' ";
            }
            else
                ChildFirstName = "'%" + ChildFirstName + "%'";

            if (WhCont.Length > 0)
            {
                WhCont += (" and (ltrim(rtrim(CH.first_name)) like " + ChildFirstName + " or  ltrim(rtrim(ch.first_name)) like " + ChildFirstName + " )");
                executeQuery = true;
            }
            else
            {
                WhCont += (" where (ltrim(rtrim(CH.first_name)) like " + ChildFirstName + " or  ltrim(rtrim(ch.first_name)) like " + ChildFirstName + " )");
                executeQuery = true;
            }

        }
        if (ChildLastName.Length > 0)
        {

            ChildLastName = ChildLastName.Replace(" ", "");
            if (ChildLastName.Contains("_"))
            {
                ChildLastName = "'%" + ChildLastName.Replace("_", "/_") + "%' ESCAPE '/' ";
            }
            else
            {
                ChildLastName = "'%" + ChildLastName + "%'";
                //if (ChildFirstName != "")
                //{
                //    ChildLastName = "'" + ChildLastName + "'";
                //}
                //else
                //{
                //    ChildLastName = "'%" + ChildLastName + "'";
                //}
            }

            if (WhCont.Length > 0)
            {
                WhCont += (" and (ltrim(rtrim(ch.last_name)) like " + ChildLastName + " or ltrim(rtrim(ch.last_name)) like " + ChildLastName + " )");
                executeQuery = true;
            }
            else
            {
                WhCont += (" where  (ltrim(rtrim(ch.last_name)) like " + ChildLastName + " or ltrim(rtrim(ch.last_name)) like " + ChildLastName + " )");
                executeQuery = true;
            }
        }

        //if (txtChName.Text != string.Empty)
        //{
        //    chName = txtChName.Text.Replace(" ", "");
        //    if (chName.Contains("_"))
        //    {
        //        chName = "'%" + chName.Replace("_", "/_") + "%' ESCAPE '/' ";
        //    }
        //    else
        //        chName = "'%" + chName + "%'";

        //    if (WhCont == string.Empty)
        //    {
        //        WhCont = WhCont + " Where ltrim(rtrim(CH.first_name)) + ltrim(rtrim(CH.last_name)) like " + chName + "  ";
        //        executeQuery = true;
        //    }
        //    else
        //    {
        //        WhCont = WhCont + " and ltrim(rtrim(CH.first_name)) + ltrim(rtrim(CH.last_name)) like " + chName + " ";
        //    }
        //}


        //if (txtParentName.Text != string.Empty)
        //{
        //    pName = txtParentName.Text.Replace(" ", "");
        //    if (pName.Contains("_"))
        //    {
        //        pName = "'%" + pName.Replace("_", "/_") + "%' ESCAPE '/' ";
        //    }
        //    else
        //        pName = "'%" + pName + "%'";

        //    if (WhCont == string.Empty)
        //    {
        //        WhCont = WhCont + " Where (ltrim(rtrim(I.firstname)) + ltrim(rtrim(I.lastname)) like " + pName + " OR ltrim(rtrim(I1.firstname)) + ltrim(rtrim(I1.lastname)) like " + pName + ")";
        //        executeQuery = true;
        //    }
        //    else
        //    {
        //        WhCont = WhCont + " and ( ltrim(rtrim(I.firstname)) + ltrim(rtrim(I.lastname)) like " + pName + " OR ltrim(rtrim(I1.firstname)) + ltrim(rtrim(I1.lastname)) like " + pName + ")";
        //        executeQuery = true;
        //    }
        //}

        if (TextBox2.Text != string.Empty)
        {
            pEmail = TextBox2.Text;
            if (pEmail.Contains("_"))
            {
                pEmail = "'%" + pEmail.Replace("_", "/_") + "%' ESCAPE '/' ";
            }
            else
                pEmail = "'%" + pEmail + "%'";
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where (ch.Email like " + pEmail + " OR ch.Email like " + pEmail + ")";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and (ch.Email like " + pEmail + " OR ch1.Email like " + pEmail + ")";
                executeQuery = true;
            }
        }
        if (txtPhone.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {

                WhCont = WhCont + " Where ( I.Hphone like '%" + TxtPhoneNo.Text + "%' OR I1.Hphone like '%" + TxtPhoneNo.Text + "%')";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and ( I.Hphone like '%" + TxtPhoneNo.Text + "%' OR I1.Hphone like '%" + TxtPhoneNo.Text + "%')";
                executeQuery = true;
            }
        }
        if (ddlState.SelectedItem.Text != "Select State")
        {
            if (!string.IsNullOrEmpty(WhCont))
            {
                length = WhCont.ToString().Length;
            }
            string state = ddlState.SelectedValue;
            if (length > 0)
            {
                WhCont += (" and ( I.state like '%" + state + "%' or I1.state like '%" + state + "%')");
                executeQuery = true;
            }
            else
            {
                WhCont += (" where (I.state like '%" + state + "%' or I1.state like '%" + state + "%')");
                executeQuery = true;
            }
        }
        try
        {
            DataSet ds = null;
            Ischild = true;

            // SearchMembers(WhCont);
            if (executeQuery == true)
            {
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlString + WhCont);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                Panel5.Visible = true;
                GVchild.Visible = true;
                GVchild.DataSource = ds.Tables[0];
                GVchild.DataBind();
                lblchError.Text = "";
            }
            else
            {
                Panel5.Visible = false;
                lblchError.Text = "No Record found";
            }
        }
        catch (Exception ex) { }
    }

    public void Clear()
    {
        txtFirstName.Text = "";
        txtLastName.Text = "";
        TextBox2.Text = "";
        TxtPhoneNo.Text = "";
        ddlState.SelectedValue = "Select State";
        if (hdnSeacrhTerm.Value.ToLower() == "parent")
        {
            Panel5.Visible = false;
        }
        else
        {
            Panel3.Visible = false;
        }
    }

    [WebMethod]
    public static List<ChildInfo> GetChildInfo(string AutoMemberId, string Type)
    {

        int retVal = -1;
        List<ChildInfo> ObjListChild = new List<ChildInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            if (Type.ToLower() == "spouse")
            {
                AutoMemberId = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select Relationship from Indspouse where AutoMemberId=" + AutoMemberId + "").ToString();
            }
            string cmdText = string.Empty;
            cmdText = "select Ch.First_Name, ch.Last_Name, ch.childnumber, CH.GRADE,CH.SchoolName,CH.DATE_OF_BIRTH,ch.email  FROM   CHILD CH Inner JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID    left JOIN IndSpouse I1 ON CH.Memberid=I1.Relationship where ch.MemberId=" + AutoMemberId + "";


            cmdText += " order by ch.First_Name, ch.Last_Name ASC";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ChildInfo ObjChild = new ChildInfo();

                        ObjChild.FirstName = dr["First_Name"].ToString();
                        ObjChild.LastName = dr["Last_Name"].ToString();
                        ObjChild.ChildName = dr["First_Name"].ToString() + " " + dr["Last_Name"].ToString();
                        ObjChild.ChildNumber = dr["ChildNumber"].ToString();

                        ObjChild.School = dr["SchoolName"].ToString();
                        ObjChild.grade = dr["GRADE"].ToString();
                        ObjChild.Email = dr["Email"].ToString();
                        if (dr["DATE_OF_BIRTH"].ToString() != "")
                        {
                            ObjChild.DOB = Convert.ToDateTime(dr["DATE_OF_BIRTH"].ToString()).ToString("MM/dd/yyyy");
                        }
                        ObjListChild.Add(ObjChild);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListChild;

    }

    [WebMethod]
    public static List<ParentInfo> GetparentsInfo(string ChildNumber)
    {

        int retVal = -1;
        List<ParentInfo> ObjListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string AutoMemberId = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select MemberId from Child where ChildNumber=" + ChildNumber + "").ToString();


            string cmdText = string.Empty;
            cmdText = " select IP.FirstName + ' ' + IP.LastName as Name, IP.email,IP.HPhone, IP.CPhone,IP.address1, IP.City, IP.State, IP.Chapter from Indspouse IP inner join Child C on(IP.AutoMemberId = C.MemberId) where C.ChildNumber = " + ChildNumber + " Union All select IP.FirstName + ' ' + IP.LastName as Name, IP.email,IP.HPhone, IP.CPhone,IP.address1, IP.City, IP.State, IP.Chapter from Indspouse IP where IP.relationship = " + AutoMemberId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo ObjParent = new ParentInfo();

                        ObjParent.ParentName = dr["Name"].ToString();
                        ObjParent.Email = dr["Email"].ToString();
                        ObjParent.CPhone = dr["CPhone"].ToString();
                        ObjParent.HPhone = dr["HPhone"].ToString();
                        ObjParent.Address = dr["Address1"].ToString();
                        ObjParent.City = dr["City"].ToString();
                        ObjParent.State = dr["State"].ToString();
                        ObjParent.Chapter = dr["Chapter"].ToString();
                        ObjListParent.Add(ObjParent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListParent;

    }

    public class ChildInfo
    {
        public string ChildNumber { get; set; }
        public string ChildName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string grade { get; set; }
        public string School { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        public string ParentEmail { get; set; }
        public string Email { get; set; }
    }
    public class ParentInfo
    {
        public string ParentName { get; set; }
        public string Email { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Chapter { get; set; }

    }

    protected void txtChName_TextChanged(object sender, EventArgs e)
    {
        string text = txtChName.Text;
    }

    protected void txtFirstName_TextChanged(object sender, EventArgs e)
    {
        string text = txtChName.Text;

    }
}