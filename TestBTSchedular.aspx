﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="TestBTSchedular.aspx.vb" Inherits="TestBTSchedular" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <br />
    <asp:Button ID="Button1" runat="server" Text="Disbursement Details" />
    <asp:GridView ID="gvBTDetails" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
        <Columns>
                <asp:BoundField  DataField="MS_TransDate" HeaderText="Trans Date"/>
                <asp:BoundField DataField="DisbDate" HeaderText="Disburse On"/>
                <asp:BoundField DataField="Id"  HeaderText="ID"/>
                <asp:BoundField DataField="CustomerId" HeaderText="CustomerId"/>
                <asp:BoundField DataField="Email" HeaderText="Email"/>
                <asp:BoundField DataField="CHName" HeaderText="Card Holder Name"/>
                <asp:BoundField DataField="PaymentReference" HeaderText="PaymentRef"/>
                <asp:BoundField DataField="RefundReference" HeaderText="RefundRef"/>
                <asp:BoundField DataField="Token" HeaderText="Token"/>
                <asp:BoundField DataField="Brand" HeaderText="Brand"/>
                <asp:BoundField DataField="MS_Amount" HeaderText="Amount"/>
        </Columns>
    </asp:GridView>
</asp:Content>

