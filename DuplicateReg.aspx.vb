Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail

Namespace VRegistration

    Partial Class DuplicateReg
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'lblWhos.Text = Request("dup")
        End Sub

        

        Protected Sub hlnkSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkSendEmail.Click
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@gmail.com"
            sMailTo = "nsfcontests@gmail.com"
            sSubject = "User wants to contact you"
            Dim mm As New MailMessage(SMailFrom, sMailTo)
            mm.Subject = sSubject
            Dim strSql As String
            strSql = "UPDATE indDuplicate SET Status='Pending' WHERE IndDupID=" & Request.QueryString("MemberID")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            strSql = "SELECT * FROM indDuplicate WHERE IndDupID=" & Request.QueryString("MemberID")
            Dim drIndDup As SqlDataReader
            drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If (drIndDup.Read()) Then
                sBody = "First Name: " & drIndDup("FirstName") & vbCrLf
                sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
                sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
                sBody = sBody & "Address2: " & drIndDup("Address2") & vbCrLf
                sBody = sBody & "City: " & drIndDup("City") & vbCrLf
                sBody = sBody & "State: " & drIndDup("State") & vbCrLf
                sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
                sBody = sBody & "Country: " & drIndDup("Country") & vbCrLf
                sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
                sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
                sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
                sBody = sBody & "Fax: " & drIndDup("Fax") & vbCrLf
                sBody = sBody & "Work Phone: " & drIndDup("WPhone") & vbCrLf
                sBody = sBody & "Work Fax: " & drIndDup("WFax") & vbCrLf
                sBody = sBody & "Primary Email: " & drIndDup("Email") & vbCrLf
                sBody = sBody & "Secondary Email: " & drIndDup("SecondaryEmail") & vbCrLf
                sBody = sBody & "Education: " & drIndDup("Education") & vbCrLf
                sBody = sBody & "Career: " & drIndDup("Career") & vbCrLf
                sBody = sBody & "Country Of Origin: " & drIndDup("CountryOfOrigin") & vbCrLf
                sBody = sBody & "State Of Origin: " & drIndDup("StateOfOrigin") & vbCrLf
                sBody = sBody & "Marital Status: " & drIndDup("MaritalStatus") & vbCrLf
                sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
                sBody = sBody & "Volunteer Flag: " & drIndDup("VolunteerFlag") & vbCrLf
                sBody = sBody & "Referred By: " & drIndDup("ReferredBy") & vbCrLf
            End If
            If Request.QueryString("MemberSID") > 0 Then
                Dim strSql1 As String
                strSql1 = "UPDATE indDuplicate SET Status='Pending' WHERE IndDupID=" & Request.QueryString("MemberSID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql1)
                strSql1 = "SELECT * FROM indDuplicate WHERE IndDupID=" & Request.QueryString("MemberSID")
                Dim drIndDup1 As SqlDataReader
                drIndDup1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql1)
                If (drIndDup.Read()) Then
                    sBody = sBody & "Spoue First Name: " & drIndDup1("FirstName") & vbCrLf
                    sBody = sBody & "Spoue Last Name: " & drIndDup1("LastName") & vbCrLf
                    sBody = sBody & "Spoue Address1: " & drIndDup1("Address1") & vbCrLf
                    sBody = sBody & "Spoue Address2: " & drIndDup1("Address2") & vbCrLf
                    sBody = sBody & "Spoue City: " & drIndDup1("City") & vbCrLf
                    sBody = sBody & "Spoue State: " & drIndDup1("State") & vbCrLf
                    sBody = sBody & "Spoue Zip: " & drIndDup1("Zip") & vbCrLf
                    sBody = sBody & "Spoue Country: " & drIndDup1("Country") & vbCrLf
                    sBody = sBody & "Spoue Gender: " & drIndDup1("Gender") & vbCrLf
                    sBody = sBody & "Spoue Home Phone: " & drIndDup1("HPhone") & vbCrLf
                    sBody = sBody & "Spoue Cell Phone: " & drIndDup1("CPhone") & vbCrLf
                    sBody = sBody & "Spoue Fax: " & drIndDup1("Fax") & vbCrLf
                    sBody = sBody & "Spoue Work Phone: " & drIndDup1("WPhone") & vbCrLf
                    sBody = sBody & "Spoue Work Fax: " & drIndDup1("WFax") & vbCrLf
                    sBody = sBody & "Spoue Primary Email: " & drIndDup1("Email") & vbCrLf
                    sBody = sBody & "Spoue Secondary Email: " & drIndDup1("SecondaryEmail") & vbCrLf
                    sBody = sBody & "Spoue Education: " & drIndDup1("Education") & vbCrLf
                    sBody = sBody & "Spoue Career: " & drIndDup1("Career") & vbCrLf
                    sBody = sBody & "Spoue Country Of Origin: " & drIndDup1("CountryOfOrigin") & vbCrLf
                    sBody = sBody & "Spoue State Of Origin: " & drIndDup1("StateOfOrigin") & vbCrLf
                    sBody = sBody & "Spoue Marital Status: " & drIndDup1("MaritalStatus") & vbCrLf
                    sBody = sBody & "Spoue Chapter: " & drIndDup1("Chapter") & vbCrLf
                    sBody = sBody & "Spoue Volunteer Flag: " & drIndDup1("VolunteerFlag") & vbCrLf
                    sBody = sBody & "Spoue Referred By: " & drIndDup1("ReferredBy") & vbCrLf
                End If
            End If


            mm.Body = sBody
            
            '(3) Create the SmtpClient object
            Dim client As New SmtpClient()

            '(4) Send the MailMessage (will use the Web.config settings)
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            'client.Timeout = 20000
            pnlMessage.Visible = True
            pnlDuplicate.Visible = False
            Try
                client.Send(mm)
            Catch ex As Exception
                ' MsgBox(ex.ToString)
                lblMessage.Text = ex.Message
            End Try
            lblMessage.Text = "Mail Sent to customer care successfully."
        End Sub
        Protected Sub hlnkForgotPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkForgotPwd.Click
            Dim strSql As String
            strSql = "UPDATE indDuplicate SET Status='FORGOT PASSWORD' WHERE IndDupID=" & Request.QueryString("MemberID")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            If Request.QueryString("MemberSID") > 0 Then
                strSql = "UPDATE indDuplicate SET Status='FORGOT PASSWORD' WHERE IndDupID=" & Request.QueryString("MemberSID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            End If
            Response.Redirect("Forgot.aspx")
        End Sub

        Protected Sub hlnkLoginID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkLoginID.Click
            Dim strSql As String
            strSql = "UPDATE indDuplicate SET Status='FORGOT LOGIN ID' WHERE IndDupID=" & Request.QueryString("MemberID")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            If Request.QueryString("MemberSID") > 0 Then
                strSql = "UPDATE indDuplicate SET Status='FORGOT LOGIN ID' WHERE IndDupID=" & Request.QueryString("MemberSID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            End If
            Response.Redirect("Forgot_loginid.aspx")
        End Sub

        Protected Sub hlnkEmailAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkEmailAddress.Click
            Dim strSql As String
            strSql = "UPDATE indDuplicate SET Status='Invalid Email - Know Pwd' WHERE IndDupID=" & Request.QueryString("MemberID")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            If Request.QueryString("MemberSID") > 0 Then
                strSql = "UPDATE indDuplicate SET Status='Invalid Email - Know Pwd' WHERE IndDupID=" & Request.QueryString("MemberSID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            End If
            Response.Redirect("ChangeEmail.aspx")
        End Sub

        Protected Sub hlnkSendEmailAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkSendEmailAddress.Click
            Dim strSql As String
            strSql = "UPDATE indDuplicate SET Status='Invalid Email � Forgot Pwd' WHERE IndDupID=" & Request.QueryString("MemberID")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            If Request.QueryString("MemberSID") > 0 Then
                strSql = "UPDATE indDuplicate SET Status='Invalid Email � Forgot Pwd' WHERE IndDupID=" & Request.QueryString("MemberSID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            End If
            Dim AutoMemberID As Integer = Request.QueryString("AMemID")
            Dim iDuplicateID As Integer = Request.QueryString("MemberID")
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@gmail.com"
            sMailTo = "nsfcontests@gmail.com"
            sSubject = "Invalid Email and Forgot Password"
            Dim mm As New MailMessage(SMailFrom, sMailTo)
            mm.Subject = sSubject
            strSql = "SELECT * FROM IndSpouse WHERE AutoMemberID=" & AutoMemberID
            Dim drIndDup As SqlDataReader
            drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If (drIndDup.Read()) Then
                sBody = "Please contact the following parent for duplicate registration with another email. Kindly do a follow up thourgh phone and correct his/her Record to avoid confusion. " & vbCrLf
                sBody = sBody & " Original Record Details in IndSpouse table " & vbCrLf
                sBody = sBody & "First Name: " & drIndDup("FirstName") & vbCrLf
                sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
                sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
                sBody = sBody & "City: " & drIndDup("City") & vbCrLf
                sBody = sBody & "State: " & drIndDup("State") & vbCrLf
                sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
                sBody = sBody & "Email: " & drIndDup("Email") & vbCrLf
                sBody = sBody & "Email Valid Flag : " & drIndDup("ValidEmailFlag") & vbCrLf
                sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
                sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
                sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
                sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
            Else
                Exit Sub
            End If

            strSql = "SELECT * FROM IndDuplicate WHERE IndDupID=" & iDuplicateID
            drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If (drIndDup.Read()) Then
                sBody = sBody & " Duplicate, New Record Details in IndDuplicate table " & vbCrLf
                sBody = sBody & "First Name: " & drIndDup("FirstName") & vbCrLf
                sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
                sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
                sBody = sBody & "City: " & drIndDup("City") & vbCrLf
                sBody = sBody & "State: " & drIndDup("State") & vbCrLf
                sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
                sBody = sBody & "Email: " & drIndDup("Email") & vbCrLf
                sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
                sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
                sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
                sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
            Else
                Exit Sub
            End If

            mm.Body = sBody
            '(3) Create the SmtpClient object
            Dim client As New SmtpClient()
            pnlMessage.Visible = True
            pnlDuplicate.Visible = False
            Try
                client.Send(mm)
            Catch ex As Exception
                ' MsgBox(ex.ToString)
            End Try
            lblMessage.Text = "Mail Sent to customer care successfully.<br>If you do not hear a response in the next two days,  please send an email to nsfcontests@gmail.com."
        End Sub
    End Class
End Namespace

