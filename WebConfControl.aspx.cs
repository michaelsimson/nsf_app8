﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Drawing;

public partial class WebConfControl : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        lblerr.Text = "";
        if (!IsPostBack)
        {
            loadyear();

            fillProductGroup();
            if (ddlProductGroup.SelectedValue == "0")
            {
                fillProduct();
            }

            LoadSessionCreation();
        }
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddlYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            cmdText = "select ProductGroupId,Name from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + year + ")";

            //cmdText = "select ProductGroupID,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "Name";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            ddlProduct.Items.Clear();
            if (ddlProductGroup.SelectedValue == "0")
            {
                ddlProduct.Items.Insert(0, new ListItem("All", "0"));
                ddlProduct.Enabled = false;
            }
            else
            {
                int year = DateTime.Now.Year;
                //year = 2014;
                cmdText = "select ProductId,Name from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + year + ")";

                //cmdText = "select ProductID,ProductCode from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "Name";
                ddlProduct.DataValueField = "ProductId";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlProduct.Enabled = true;
                }
                else
                {
                    ddlProduct.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProductGroup.SelectedValue == "0")
        {
            ddlProduct.Items.Clear();
        }
        else
        {
            fillProduct();
        }
    }

    public void LoadSessionCreation()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();

        CmdText = "select WC.EventYear,WC.WebConfControlID, WC.EventID,E.Name as EventName,WC.ProductGroupID,WC.ProductID,PG.Name as ProductGroupName,P.Name as ProductName,WC.SessionCreationDate,WC.CoachChange from WebConfControl WC inner join Event E on (WC.EventID=E.EventiD) left join ProductGroup PG on(WC.ProductGroupID=PG.ProductGroupID) left join Product P on (WC.ProductID=P.ProductId) order by WC.EventYear ASC";

        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdSessionCreation.DataSource = ds;
                GrdSessionCreation.DataBind();
            }
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        InsertUpdateSession();
    }

    public void InsertUpdateSession()
    {
        if (ValidateSession() == "1")
        {

            string CmdText = string.Empty;
            string productGroup = string.Empty;
            string ProductGroupID = string.Empty;
            string Product = string.Empty;
            string ProductID = string.Empty;
            string Msg = string.Empty;

            if (ddlProductGroup.SelectedValue == "0")
            {
                ProductGroupID = "null";
                productGroup = "null";
            }
            else
            {
                ProductGroupID = "'" + ddlProductGroup.SelectedValue + "'";
                productGroup = "'" + ddlProductGroup.SelectedItem.Text + "'";
            }
            if (ddlProduct.SelectedValue == "0")
            {
                Product = "null";
                ProductID = "null";
            }
            else
            {
                Product = "'" + ddlProduct.SelectedValue + "'";
                ProductID = "'" + ddlProduct.SelectedItem.Text + "'";
            }
            if (btnSubmit.Text == "Save")
            {




                // CmdText = "Insert into WebConfControl (EventYear, EventID, ProductGroupID, ProductGroupCode, ProductID,ProductCode,SessionCreationDate,CoachChange,CreatedDate,CreatedBy) values(" + ddlYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ProductGroupID + "," + productGroup + "," + Product + "," + ProductID + ",'" + txtSessionCreationDate.Text + "','" + DdlCoachChange.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + ")";
                Msg = "Inserted Successfully";

            }
            else
            {
                CmdText = "Update WebConfControl set Eventyear=" + ddlYear.SelectedValue + ", ProductGroupID=" + ProductGroupID + ", ProductGroupCode=" + productGroup + ", ProductID=" + Product + ", ProductCode=" + ProductID + ", SessionCreationDate='" + txtSessionCreationDate.Text + "', CoachChange='" + DdlCoachChange.SelectedValue + "', ModifyDate=GetDate(), ModifiedBy=" + Session["LoginID"].ToString() + " where WebConfControlID=" + hdnWebConfControlID.Value + "";
                Msg = "Updated Successfully";
            }
            if (btnSubmit.Text == "Save")
            {
                int count = 0;
                string cmdCount = "select count(*) as CountSet from WebConfControl where EventYear=" + ddlYear.SelectedValue + " and EventID=" + ddEvent.SelectedValue + "";
                if (ProductGroupID != "null")
                {
                    cmdCount += " and ProductGroupID=" + ProductGroupID + "";

                }
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdCount);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());

                    }

                }
                if (count > 0)
                {
                    lblerr.Text = "Duplicate exists..!";
                }
                else
                {
                    string strQuery;


                    SqlCommand cmd;
                    strQuery = "Insert into WebConfControl (EventYear, EventID, ProductGroupID, ProductGroupCode, ProductID,ProductCode,SessionCreationDate,CoachChange,CreatedDate,CreatedBy) values(@EventYear,@EventID,@ProductGroupID,@ProductGroupCode,@ProductID,@ProductCode,@SessionCreationDate,@CoachChange,@CreatedDate,@CreatedBy)";

                    cmd = new SqlCommand(strQuery);
                    cmd.Parameters.AddWithValue("@EventYear", ddlYear.SelectedValue);
                    cmd.Parameters.AddWithValue("@EventID", ddEvent.SelectedValue);

                    cmd.Parameters.AddWithValue("@ProductGroupID", ddlProductGroup.SelectedValue);
                    cmd.Parameters.AddWithValue("@ProductGroupCode", ddlProductGroup.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@ProductID", ddlProduct.SelectedValue);
                    cmd.Parameters.AddWithValue("@ProductCode", ddlProduct.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@SessionCreationDate", txtSessionCreationDate.Text);
                    cmd.Parameters.AddWithValue("@CoachChange", DdlCoachChange.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString());
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["LoginID"].ToString());


                    NSFDBHelper objNSF = new NSFDBHelper();
                    objNSF.InsertUpdateData(cmd);
                    // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = Msg;
                    btnSubmit.Text = "Save";
                    txtSessionCreationDate.Text = "";
                }
            }
            else
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                lblerr.Text = Msg;
                btnSubmit.Text = "Save";
                txtSessionCreationDate.Text = "";
            }

            LoadSessionCreation();
        }
        else
        {
            ValidateSession();
        }
    }

    public string ValidateSession()
    {
        string Retval = "1";
        if (ddlYear.SelectedValue == "0")
        {
            lblerr.Text = "Please select Event Year";
            Retval = "-1";
        }
        else if (txtSessionCreationDate.Text == "")
        {
            lblerr.Text = "Please fill Session Creation Date";
            Retval = "-1";
        }
        return Retval;
    }

    protected void GrdSessionCreation_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                btnSubmit.Text = "Update";
                GridViewRow row = null;
                GrdSessionCreation.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdSessionCreation.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string WebConfControl = string.Empty;
                WebConfControl = ((Label)GrdSessionCreation.Rows[selIndex].FindControl("LblWebConfControlID") as Label).Text;
                hdnWebConfControlID.Value = WebConfControl;

                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                ProductGroupID = ((Label)GrdSessionCreation.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdSessionCreation.Rows[selIndex].FindControl("LblProductID") as Label).Text;

                txtSessionCreationDate.Text = GrdSessionCreation.Rows[selIndex].Cells[7].Text;
                DdlCoachChange.SelectedValue = GrdSessionCreation.Rows[selIndex].Cells[8].Text;
                ddEvent.SelectedValue = ((Label)GrdSessionCreation.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ddlYear.SelectedValue = ((Label)GrdSessionCreation.Rows[selIndex].FindControl("lbEventYear") as Label).Text;

                if (ProductGroupID != "")
                {
                    ddlProductGroup.SelectedValue = ProductGroupID;
                }
                if (ProductID != "")
                {
                    ddlProduct.SelectedValue = ProductID;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnSubmit.Text = "Save";
        txtSessionCreationDate.Text = "";

    }
}