USE [northsouth_prd]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetProductCodeByRoleID]    Script Date: 09/26/2013 15:38:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetProductCodeByRoleID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetProductCodeByRoleID]
GO

USE [northsouth_prd]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetProductCodeByRoleID]    Script Date: 09/26/2013 15:38:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---Created By Joseph Remilton---

CREATE PROCEDURE [dbo].[usp_GetProductCodeByRoleID]
(
@RoleId int,
@MemberId int,
@ProductGroupId int,  
@EventYear int,
@TLead varchar(10)
)
AS

IF(@RoleId =1 or @RoleId =2)
BEGIN
select distinct  CAST(P.[ProductId] as varchar(15))+'-'+ P.[ProductCode] 
AS [IDandCode], P.[Name] 
FROM dbo.[Product] P
WHERE P.[ProductGroupId] = @ProductGroupId AND  P.[Status] ='O' 
END

IF(@RoleId =89)
BEGIN
SELECT DISTINCT  CAST(V.[ProductId] as varchar(15))+'-'+ V.[ProductCode] AS [IDandCode], P.[Name]
from dbo.[Volunteer] V, dbo.[Product] P where RoleId= @RoleId  and MemberID = @MemberId  and P.[ProductId]=V.[ProductId] and P.[Status] ='O' and V.EventYear =@EventYear 
END

IF(@RoleId =88 and @TLead ='Y')
BEGIN
SELECT DISTINCT  CAST(V.[ProductId] as varchar(15))+'-'+ V.[ProductCode] AS [IDandCode], P.[Name]
from dbo.[Volunteer] V, dbo.[Product] P where RoleId= @RoleId  and MemberID = @MemberId  and P.[ProductId]=V.[ProductId] and P.[Status] ='O' and V.EventYear =@EventYear 
END

IF (@RoleId =88 and @TLead ='N')
BEGIN
SELECT DISTINCT  CAST(CS.[ProductId] as varchar(15))+'-'+ CS.[ProductCode] AS [IDandCode], P.[Name]
from dbo.[CalSignup] CS, dbo.[Product] P where MemberID = @MemberId   and P.[ProductId]=CS.[ProductId] and P.[Status] ='O' and CS.EventYear =@EventYear 
END
GO

