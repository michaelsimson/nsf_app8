USE [NSFNew]
GO
/****** Object:  StoredProcedure [dbo].[usp_Conditional_CoachPapers_GetByCriteria]    Script Date: 08/29/2013 14:58:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_Conditional_CoachPapers_GetByCriteria]
 (    
 @ProductId int, 
 @EventId int,
 @ProductCode varchar(50), 
 @ProductGroupId int, 
 @EventYear int,  
 @ProductGroupCode varchar(50),  
 @Level varchar(25),   
 @WeekId int,     
 @SetNum int,     
 @PaperType varchar(15), 
 @DocType varchar(30), 
 @Sections int,    
 @TestFileName varchar(50),     
 @Description varchar(500),     
 @CreateDate smalldatetime,     
 @CreatedBy int,
 @MemberId int,
 @RoleId int
 
  )
  
   AS BEGIN  
   If (@ProductId = -1)   
   Begin       
   Set @ProductId = NULL    
   End    
   If (@EventId = -1)   
   Begin       
   Set @EventId = NULL    
   End    
   If (@ProductCode = '')    
   Begin      
   Set @ProductCode = NULL 
   End    
   If (@ProductGroupId = -1) 
   Begin 
   Set @ProductGroupId = NULL  
   End    
   If (@ProductGroupCode = '') 
   Begin 
   Set @ProductGroupCode = NULL 
   End  
   if(@Level ='')
   Begin
   set @Level=NULL
   end 
   If (@WeekId = -1)    
   Begin       
   Set @WeekId = NULL    
   End    
   If (@SetNum = -1)    
   Begin       
   Set @SetNum = NULL    
   End  
   If (@PaperType = '')    
   Begin       
   Set @PaperType = NULL    
   End   
   If (@DocType = '')    
   Begin       
   Set @DocType = NULL    
   End   
   If (@Sections = -1)   
   Begin       
   Set @Sections = NULL    
   End    
   If (@TestFileName = '')    
   Begin       
   Set @TestFileName = NULL    
   End    
   If (@Description = '')    
   Begin       
   Set @Description = NULL    
   End    
   If (@CreatedBy = -1)    
   Begin       
   Set @CreatedBy = NULL    
   End    
   If (@CreateDate = '1/1/1900')    
   Begin       
   Set @CreateDate = NULL    
   End    
   IF (@RoleId =89)
   BEGIN   
   SELECT CP.[CoachPaperId]
		  ,CP.[EventYear]
          ,CP.[ProductId] 
          ,CP.[EventId]  
          ,CP.[ProductCode]       
          ,CP.[ProductGroupId]       
          ,CP.[ProductGroupCode]   
          ,CP.[Level]     
          ,CP.[WeekId]       
          ,CP.[SetNum]       
          ,CP.[PaperType]  
          ,CP.[DocType] 
          ,CP.[Sections]       
          ,CP.[TestFileName]       
          ,CP.[Description]      
          ,CP.[Password]             
          ,CP.[CreateDate]       
          ,CP.[CreatedBy]       
          ,CP.[ModifyDate]       
          ,CP.[ModifiedBy]     
           ,ISP.FirstName +  '  '+ ISP.[LastName] AS [NameOfCoach]
          FROM [CoachPapers] CP
          --WHERE [CoachPaperId] NOT IN (select CoachPaperId from dbo.[CoachRelDates] where dbo.[CoachRelDates].[MemberID] = @MemberID )
          INNER JOIN IndSpouse ISP ON CP.CreatedBy=ISP.AutoMemberID   
          WHERE EventYear = @EventYear 
          and CP.ProductId = ISNULL(@ProductId, CP.ProductId) 
          and CP.EventId=ISNULL(@EventId,CP.EventId) 
          and CP.ProductCode  in (select distinct dbo.[Volunteer].ProductCode from dbo.[Volunteer]  where dbo.[Volunteer].ProductGroupId=@ProductGroupId  and dbo.[Volunteer].[MemberId]=@MemberId )
          and CP.ProductCode = ISNULL(@ProductCode, CP.ProductCode) 
          and CP.ProductGroupId = ISNULL(@ProductGroupId, CP.ProductGroupId) 
          and CP.[Level] =ISNULL(@Level ,[Level])
          and CP.ProductGroupCode = ISNULL(@ProductGroupCode, CP.ProductGroupCode) 
          and CP.WeekId = ISNULL(@WeekId, CP.WeekId) 
          and CP.SetNum = ISNULL(@SetNum, CP.SetNum) 
          and CP.PaperType = ISNULL(@PaperType, CP.PaperType)
          and CP.DocType =@DocType 
          and CP.Sections = ISNULL (@Sections, CP.Sections)  
          and CP.TestFileName like ISNULL('%' + @TestFileName + '%', CP.TestFileName) 
          and CP.[Description] like ISNULL('%' + @Description + '%', CP.[Description]) 
          and CP.CreateDate = ISNULL(@CreateDate, CP.CreateDate) 
          and CP.CreatedBy = ISNULL(@CreatedBy, CP.CreatedBy)  
          order by
          CoachPaperId, 
          WeekId, 
          ProductId, 
          DocType, 
          PaperType 
   END
   ELSE
   BEGIN
   SELECT CP.[CoachPaperId]
		  ,CP.[EventYear]
          ,CP.[ProductId] 
          ,CP.[EventId]  
          ,CP.[ProductCode]       
          ,CP.[ProductGroupId]       
          ,CP.[ProductGroupCode]  
          ,CP.[Level]      
          ,CP.[WeekId]       
          ,CP.[SetNum]       
          ,CP.[PaperType]  
          ,CP.[DocType] 
          ,CP.[Sections]       
          ,CP.[TestFileName]       
          ,CP.[Description]      
          ,CP.[Password]             
          ,CP.[CreateDate]       
          ,CP.[CreatedBy]       
          ,CP.[ModifyDate]                 
          ,CP.[ModifiedBy]     
          ,ISP.FirstName +  '  '+ ISP.[LastName] AS [NameOfCoach]
          FROM [CoachPapers] CP
          INNER JOIN IndSpouse ISP ON CP.CreatedBy=ISP.AutoMemberID 
          --WHERE [CoachPaperId] NOT IN (select CoachPaperId from dbo.[CoachRelDates] where dbo.[CoachRelDates].[MemberID] = @MemberID )
          WHERE CP. EventYear = @EventYear 
          and CP.ProductId = ISNULL(@ProductId, CP.ProductId) 
          and CP.EventId=ISNULL(@EventId,CP.EventId) 
          and CP.ProductCode = ISNULL(@ProductCode, CP.ProductCode) 
          and CP.ProductGroupId = ISNULL(@ProductGroupId, CP.ProductGroupId) 
          and CP.ProductGroupCode = ISNULL(@ProductGroupCode, CP.ProductGroupCode) 
          and CP.[Level] =ISNULL (@Level ,CP.[Level])
          and CP.WeekId = ISNULL(@WeekId, CP.WeekId) 
          and CP.SetNum = ISNULL(@SetNum, CP.SetNum) 
          and CP.PaperType = ISNULL(@PaperType, CP.PaperType)
          and CP.DocType =@DocType 
          and CP.Sections = ISNULL (@Sections, CP.Sections)  
          and CP.TestFileName like ISNULL('%' + @TestFileName + '%', CP.TestFileName) 
          and CP.[Description] like ISNULL('%' + @Description + '%', CP.[Description]) 
          and CP.CreateDate = ISNULL(@CreateDate, CP.CreateDate) 
          and CP.CreatedBy = ISNULL(@CreatedBy, CP.CreatedBy)  
          order by 
          CP.CoachPaperId,
          CP.WeekId, 
          CP.ProductId, 
          CP.DocType, 
          CP.PaperType 
   END
          END
