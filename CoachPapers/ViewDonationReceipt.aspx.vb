﻿Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ViewDonationReceipt
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim dblRegFee As Double
    Dim dblDonationAmt As Double
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '*************************************************************************************
        '*************************************************************************************
        '*  Emails with Donation Receipt attachement were send to Parent, Donor and Volunteer*
        '*  (Personal Functions in Volunteer Functions Page)                                 *
        '*  Volunteer have option to send mail to other by searching it....                  *
        '*************************************************************************************
        '*************************************************************************************

        Try
            If Not IsPostBack Then


                If LCase(Session("LoggedIn")) <> "true" Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                If Len(Trim("" & Session("LoginID"))) = 0 Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                If Len(Trim("" & Session("entryToken"))) = 0 Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                If Session("entryToken") = "Parent" Then
                    txtToken.Value = "Parent"
                    hlinkParentRegistration.NavigateUrl = "UserFunctions.aspx"
                ElseIf Session("entryToken") = "Donor" Then
                    txtToken.Value = "Donor"
                    hlinkParentRegistration.NavigateUrl = "DonorFunctions.aspx"
                ElseIf Session("entryToken") = "Volunteer" Then
                    hlinkParentRegistration.NavigateUrl = "VolunteerFunctions.aspx"
                    txtToken.Value = "Volunteer"
                    If Not Request.QueryString("MemberID") Is Nothing Then
                        hlnkSearch.Visible = True
                        'hlnkSearch.NavigateUrl = "SearchDonationReceipt.aspx?id=5"
                    Else
                        hlnkSearch.Visible = False
                    End If
                End If
              
                If Request.QueryString("Type") = "Multiple" Then
                    hlnkHome.Visible = False
                    hlnkSearch.Visible = False
                    hlinkParentRegistration.Visible = False
                    btnPrint.Visible = False
                    txtHidden.Visible = False
                    hlnkDownload.Visible = False
                End If
                If Request.QueryString("Type") = "OWN" Then
                    lblFromDate.Text = Request.QueryString("DonationDate")
                    GetCorporationsDonationReceipt()
                    divCorp.Visible = True
                    divall.Visible = False
                Else
                    divCorp.Visible = False
                    divall.Visible = True


                    Dim dtCurrDate1 As String
                    dtCurrDate1 = Request.QueryString("DonationDate")
                    ' lblFromDate.Text = Session("Donationdate")
                    'Dim dtCurrDate1 As String
                    'dtCurrDate1 = Session("Donationdate")
                    Dim sessionDona As String
                    sessionDona = Request.QueryString("Donationdate")
                    If sessionDona = "" Then
                        sessionDona = Session("Donationdate")
                    End If
                    lblFromDate.Text = sessionDona
                    GetDonationReceipt()
                    'GetCorporationsDonationReceipt()

                End If

                Dim strSql As String
                Dim m_OutputFile As String
                Dim strFileName As String
                Dim bIsEmailExist As Boolean
                bIsEmailExist = False
                'Response.Write(Request.QueryString("Type"))
                If Request.QueryString("Type") <> "Multiple" Then
                    'lbemail.Text = Request.QueryString("Type")

                    If Not Request.QueryString("MemberID") Is Nothing Then
                        If Request.QueryString("Type") = "OWN" Then
                            strSql = "SELECT email FROM OrganizationInfo WHERE AUTOMEMBERID=" & Request.QueryString("MemberID")
                        Else
                            strSql = "  Select Case when I.MaritalStatus = 'Deceased'  Then Case when I2.Email is not null then I2.Email Else I.Email  End Else I.Email end  As Email from IndSpouse I LEFT JOIN IndSpouse I2 ON ((I.DonorType='IND' AND I.AutoMemberID =   I2.Relationship) OR (I.DonorType='SPOUSE' AND  I.Relationship = I2.AutoMemberID)) WHERE I.AUTOMEMBERID=" & Request.QueryString("MemberID")
                        End If

                        Dim drIndSpouse As SqlDataReader
                        drIndSpouse = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                        'Dim str As String

                        'str = Convert.ToString(drIndSpouse("Email").ToString())


                        ' If drIndSpouse("Email") <> System.DBNull.Value Then
                        If drIndSpouse.Read() Then

                            If Len(Trim(drIndSpouse("Email"))) > 0 Then
                                lbtnSend.Visible = True
                                Btnemail.Visible = True
                                btnPrint.Visible = False

                                m_OutputFile = Server.MapPath("DonorReceipts/Email_DonationReceipt_" & Request.QueryString("MemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm")
                                strFileName = "DonorReceipts/Email_DonationReceipt_" & Request.QueryString("MemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm"

                                bIsEmailExist = True
                            Else
                                lbtnSend.Visible = False

                                m_OutputFile = Server.MapPath("DonorReceipts/NoEmail_DonationReceipt_" & Request.QueryString("MemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm")
                                strFileName = "DonorReceipts/NoEmail_DonationReceipt_" & Request.QueryString("MemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm"
                                bIsEmailExist = False
                            End If
                            'End If
                        End If
                    Else
                        m_OutputFile = Server.MapPath("DonorReceipts/Email_DonationReceipt_" & Session("LoginID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm")
                        strFileName = "DonorReceipts/Email_DonationReceipt_" & Session("LoginID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm"
                        bIsEmailExist = True
                    End If

                    Try
                        Dim sw As StreamWriter
                        sw = New StreamWriter(m_OutputFile, False)
                        If Request.QueryString("Type") = "OWN" And Not Request.QueryString("MemberID") Is Nothing Then
                            Session("Donationdate") = Request.QueryString("DonationDate")
                            Server.Execute("ViewDonationReceipt.aspx?MemberID=" & Request.QueryString("MemberID") & "&[DonationDate]) = '" & Request.QueryString("DonationDate") & "'" & "&Type=OWN", sw)
                        ElseIf Not Request.QueryString("MemberID") Is Nothing Then
                            Session("Donationdate") = Request.QueryString("DonationDate")
                            Server.Execute("ViewDonationReceipt.aspx?MemberID=" & Request.QueryString("MemberID") & "&[DonationDate]) = '" & Request.QueryString("DonationDate") & "'" & "&Type=Multiple", sw)
                        Else
                            Server.Execute("ViewDonationReceipt.aspx?Year=" & Request.QueryString("Year") & "&Type=Multiple", sw)
                        End If
                        sw.Flush()
                        sw.Close()
                    Catch ex As Exception
                        'lbemail.Text = ex.Message

                    End Try

                    hlnkDownload.Enabled = True
                    hlnkDownload.NavigateUrl = strFileName
                    Dim strBody As String
                    'strBody = "Your NSF Donation Receipt."
                    strBody = "Dear donor," & vbCrLf
                    strBody = strBody & vbCrLf
                    strBody = strBody & "            Attached you will find a receipt for your donation(s).  "
                    strBody = strBody & " Please keep this for your tax records.  "
                    strBody = strBody & " If you have a Login ID and password, "
                    strBody = strBody & " you can also get a receipt by going to the NSF website.  "
                    strBody = strBody & " We are grateful for your contribution." & vbCrLf
                    strBody = strBody & vbCrLf
                    strBody = strBody & "Thank you. " & vbCrLf
                    strBody = strBody & vbCrLf
                    strBody = strBody & " With regards," & vbCrLf
                    strBody = strBody & vbCrLf
                    strBody = strBody & " NSF Fundraising Team" & vbCrLf
                    Dim sAttachment As String
                    sAttachment = m_OutputFile

                    'If Not Request.QueryString("Mail") Is Nothing Then
                    If Request.QueryString("Type") = "OWN" Then
                        strSql = "SELECT email FROM OrganizationInfo WHERE AUTOMEMBERID=" & Request.QueryString("MemberID")
                    Else
                        strSql = "SELECT email FROM INDSPOUSE WHERE AUTOMEMBERID=" & Request.QueryString("MemberID")
                    End If

                    Dim drIndSpouse1 As SqlDataReader
                    drIndSpouse1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                    If drIndSpouse1.Read() Then
                        Dim sessionDona1 As String

                        sessionDona1 = Session("LoginEmail")
                        Session("Strbodyval") = strBody
                        Session("Attachmentfile") = sAttachment
                        'lbemail.Text = sAttachment

                        'SendEmail("NSF Donation Receipt", strBody, Session("LoginEmail"), sAttachment)

                    ElseIf Request.QueryString("Mail").Trim = "N" Then
                        ' lbtnSend.Visible = True
                        hdnAttach.Value = sAttachment
                        hdnMailBody.Value = strBody
                    End If

                End If
            End If
        Catch ex As Exception
            ' lbtnSend.Visible = False

            'Response.Write(ex.ToString())
        End Try
        'lbtnSend.Visible = False
    End Sub
   
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sAttachment As String)
        'Response.Write(sBody.ToString())
        'Response.Write(sSubject.ToString())
        'Response.Write(sMailTo.ToString())


        'Response.Write(sAttachment.ToString())
        Dim email As New MailMessage
        email.From = New MailAddress("nsffundraising@gmail.com")
        'sMailTo = "ahila.cs@capestart.com"
        email.To.Add(sMailTo)
        email.Subject = sSubject
        Dim attach As New Attachment(sAttachment)
        email.Attachments.Add(attach)
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        email.Bcc.Add("chitturi9@gmail.com")

        'email.Bcc.Add("ahila.cs@capestart.com")

        lbemail.Visible = True


        Dim client As New SmtpClient()
        Dim ok As Boolean = True
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

        'client.Host = host
        'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")

        ' client.Send(email)

        Try
            lbemail.Visible = True
            client.Send(email)
            lbemail.Visible = True

        Catch e As Exception
            ok = False
            lbemail.Visible = True
            lbemail.Text = e.Message

        End Try
    End Sub
    Private Sub GetCorporationsDonationReceipt()
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        lblCurrDate.Text = DateTime.Now.ToString("MMMM dd, yyyy")
        txtHidden.Text = lblCurrDate.Text
        'Chapter CoOrdinator
        Dim strChapterID As String
        strChapterID = ""
        Dim ReqMemberIDs As String = String.Empty
        strSql = " SELECT AutoMemberID, 'OWN' as DONORTYPE, ORGANIZATION_NAME as DName, MIDDLE_INITIAL as MiddleInitial, FIRST_NAME+' '+LAST_NAME as LastName, ADDRESS1, ADDRESS2, STATE, ZIP,CITY,Email, PHONE as HPhone, LIAISONPERSON as LiasonPerson, '' as ReferredBy, ChapterID FROM  OrganizationInfo "
        strSql = strSql & " WHERE AutoMemberID = " & Request.QueryString("MemberID")
        Dim drIndividual As SqlDataReader
        drIndividual = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        If drIndividual.Read() Then
            lblName.Text = Trim(drIndividual("DName"))
            lblAddress1.Text = Trim(drIndividual("Address1"))
            Dim addr2 As String = ""

            If Not drIndividual("Address2") Is DBNull.Value Then
                addr2 = Trim(drIndividual("Address2"))
            End If
            ReqMemberIDs = drIndividual("AutoMemberID")
            hdnEmail.Value = ReqMemberIDs
            If Len(addr2) > 0 Then
                lblAddress1.Text = lblAddress1.Text & ","
                lblAddress2.Text = "#" & addr2
            Else
                lblAddress2.Text = addr2
            End If
            lblCity.Text = Trim(drIndividual("City"))
            lblState.Text = Trim(drIndividual("State"))
            lblZip.Text = Trim(drIndividual("Zip"))
        End If
        drIndividual.Close()
        Dim dtCurrDate As DateTime
        dtCurrDate = DateTime.Now
        'Donations
        strSql = "SELECT  "
        strSql = strSql & " amount, transaction_number,DonationID,ChapterID,DonorType, Convert(Varchar,donationdate,101) donationdate, method, Event,DonationNumber "
        strSql = strSql & "  FROM DonationsInfo  "

        If ReqMemberIDs.Length > 0 Then
            strSql = strSql & " where MemberID in (" & ReqMemberIDs & ")"
        ElseIf Not Request.QueryString("MemberID") Is Nothing Then
            strSql = strSql & " where MemberID=" & Request.QueryString("MemberID")
        Else
            strSql = strSql & " where MemberID=" & Session("LoginID")
        End If
        strSql = strSql & "  AND DonorType IN ('OWN')"
        Dim sessionDona As String
        sessionDona = Request.QueryString("Donationdate")
        If sessionDona = "" Then
            sessionDona = Session("Donationdate")
        End If
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then

            strSql = strSql & "  and Convert(nvarchar,[DonationDate],101) = '" & sessionDona & "'"
        Else
            strSql = strSql & " and Convert(nvarchar,[DonationDate],101) = '" & sessionDona & "'"
        End If
        strSql = strSql
        Dim tableName As String() = New String(0) {}
        tableName(0) = "Donation"
        SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsDonation, tableName)
        dgFess.Visible = False
        lblTaxDeductibleAmt.Visible = False
        lblTaxDonation.Text = "Tax-deductible Amount from Donation(s)"
        If dsDonation.Tables(0).Rows.Count > 0 Then
            dgDonation.DataSource = dsDonation
            dgDonation.DataBind()
            pnlData.Visible = True
            pnlMessage.Visible = False
        Else
            pnlMessage.Visible = True
            pnlData.Visible = False
            lblTaxDonation.Visible = False
        End If

        'lblFromDate.Text = Request.QueryString("DonationDate")
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            'lblToDate.Text = dtCurrDate.ToString("MM") & "/" & dtCurrDate.Day & "/" & dtCurrDate.Year
        Else
            'lblToDate.Text = "12/31/" & Request.QueryString("Year")
        End If
    End Sub
    Private Sub GetDonationReceipt()

        'Session("Donationdate1") = Request.QueryString("DonationDate")
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        ' Session("Donationdate") = Request.QueryString("DonationDate")
        lblCurrDate.Text = DateTime.Now.ToString("MMMM dd, yyyy")
        txtHidden.Text = lblCurrDate.Text
        'Chapter CoOrdinator
        Dim strChapterID As String
        strChapterID = ""
        Dim ReqMemberIDs As String = String.Empty
        If Session("RoleID") = "5" Then
            strSql = "Select chapterid, chaptercode, state from chapter where "
            strSql = strSql & " clusterid in (Select clusterid from chapter where "
            strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") order by state, chaptercode"
            Dim con As New SqlConnection(Application("ConnectionString"))

            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
            drNSFChapters.Close()
        End If




        strSql = " Select  CASE WHEN I2.AutoMemberID IS Not NULL THEN CONVERT(VARCHAR,I.AutoMemberID) +','+ CONVERT(VARCHAR,I2.AutoMemberID) ELSE CONVERT(VARCHAR,I.AutoMemberID) END as ReqMemberIDs,Case when I.MaritalStatus = 'married' AND I2.MaritalStatus='Married' Then Case when I2.FirstName is not null then I.FirstName + ' and '+I2.FirstName + ' ' + I.LastName   Else I.FirstName + ' ' + I.LastName End when I.MaritalStatus = 'married' AND I2.MaritalStatus='Deceased' Then "
        strSql = strSql & "I.FirstName + ' ' + I.LastName when I.MaritalStatus = 'Deceased' AND I2.MaritalStatus='Married' Then  I2.FirstName + ' ' + I2.LastName ELSE I.FirstName + ' ' + I.LastName End AS DName, I.donortype, I.email, I.firstname, I.lastname, I.address1,I.address2, I.city, I.state, I.zip from IndSpouse I LEFT JOIN IndSpouse I2 ON ((I.DonorType='IND' AND I.AutoMemberID =   I2.Relationship) OR (I.DonorType='SPOUSE' AND  I.Relationship = I2.AutoMemberID)) WHERE"


        If Not Request.QueryString("MemberID") Is Nothing Then
            strSql = strSql & " I.AutoMemberID = " & Request.QueryString("MemberID")
        Else
            strSql = strSql & " I.AutoMemberID = " & Session("LoginID")
        End If

        Dim drIndividual As SqlDataReader
        drIndividual = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        If drIndividual.Read() Then
           
            

            lblName.Text = Trim(drIndividual("DName"))


            lblAddress1.Text = Trim(drIndividual("Address1"))
            Dim addr2 As String = ""

            If Not drIndividual("Address2") Is DBNull.Value Then
                addr2 = Trim(drIndividual("Address2"))
            End If
            ReqMemberIDs = drIndividual("ReqMemberIDs")
            hdnEmail.Value = ReqMemberIDs
            If Len(addr2) > 0 Then
                lblAddress1.Text = lblAddress1.Text & ","
                lblAddress2.Text = "#" & addr2
            Else
                lblAddress2.Text = addr2
            End If
            lblCity.Text = Trim(drIndividual("City"))
            lblState.Text = Trim(drIndividual("State"))
            lblZip.Text = Trim(drIndividual("Zip"))
        End If

        ' End If
        drIndividual.Close()
        Dim dtCurrDate As DateTime
        dtCurrDate = DateTime.Now
        'Registration Fee
        strSql = " Select chapterid,"
        strSql = strSql & " memberid, eventid,""Fee"" as RegFee, Convert(Varchar,""Payment Date"",101) as DonationDate"
        strSql = strSql & "  from nfg_transactions "
        strSql = strSql & "  where ID Is null And fee > 0"
        If ReqMemberIDs.Length > 0 Then
            strSql = strSql & " and MemberID in (" & ReqMemberIDs & ")"
        ElseIf Not Request.QueryString("MemberID") Is Nothing Then
            strSql = strSql & " and MemberID = " & Request.QueryString("MemberID")
        Else
            strSql = strSql & "  and memberid=" & Session("LoginID")
        End If

        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            strSql = strSql & " and [Payment Date] = '" & Request.QueryString("DonationDate") & "'"
            'strSql = strSql & " AND ""Payment Date"" BETWEEN '01-JAN-" & DateTime.Now.Year & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
        Else
            strSql = strSql & " and [Payment Date] = '" & Request.QueryString("DonationDate") & "'"
            'strSql = strSql & "  and ""Payment Date"" BETWEEN '01-JAN-" & DateTime.Now.Year & " 00:00:00' AND '31-DEC-" & DateTime.Now.Year & " "
        End If
        strSql = strSql & "  ORDER BY [Payment Date]"
        Dim dsRegistrationFee As New DataSet
        Dim tblTrans As String() = New String(0) {}
        tblTrans(0) = "NFGTRANSACTION"
        SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsRegistrationFee, tblTrans)
        Dim bIsFeeExits As Boolean
        bIsFeeExits = False
        If dsRegistrationFee.Tables(0).Rows.Count > 0 Then
            dgFess.Visible = True
            dgFess.DataSource = dsRegistrationFee
            dgFess.DataBind()
            lblTaxDonation.Visible = True
            bIsFeeExits = True
        Else
            dgFess.Visible = False
            bIsFeeExits = False
            lblTaxDeductibleAmt.Visible = False
            lblTaxDonation.Text = "Tax-deductible Amount from Donation(s)"
        End If
        'Donations
        strSql = "SELECT MemberID, "
        strSql = strSql & " amount, transaction_number,DonationID,ChapterID,DonorType, Convert(Varchar,donationdate,101) donationdate, method, Event,DonationNumber "
        strSql = strSql & "  FROM DonationsInfo  "
        'strSql = strSql & "  TRANSACTION_NUMBER in (SELECT TRANSACTION_NUMBER From DonationsInfo A"
        If ReqMemberIDs.Length > 0 Then
            strSql = strSql & " where MemberID=" & Request.QueryString("MemberID")
        ElseIf Not Request.QueryString("MemberID") Is Nothing Then
            strSql = strSql & " where MemberID=" & Request.QueryString("MemberID")
        Else
            strSql = strSql & " where MemberID=" & Session("LoginID")
        End If
        strSql = strSql & "  AND DonorType IN ('IND','SPOUSE','OWN')"
        'If Session("RoleID") <> "5" Then
        '    If Len(Session("LoginChapterID")) > 0 Then
        '        strSql = strSql & " AND a.ChapterID = " & Session("LoginChapterID")
        '        strSql = strSql & " AND a.Anonymous <> 'Yes'"
        '    End If
        'Else
        '    strSql = strSql & " AND a.ChapterID in ( " & strChapterID & ")"
        '    strSql = strSql & " AND a.Anonymous <> 'Yes'"
        'End If
        Dim sessionDona As String
        sessionDona = Request.QueryString("Donationdate")
        If sessionDona = "" Then
            sessionDona = Session("Donationdate")
        End If
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then


            strSql = strSql & " and Convert(nvarchar,[DonationDate],101) = '" & sessionDona & "'"
            ' strSql = strSql & " AND DonationDate BETWEEN '01-JAN-DateTime.No" & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
        Else
            strSql = strSql & " and  Convert(nvarchar,[DonationDate],101) = '" & sessionDona & "'"
            'strSql = strSql & " AND DonationDate BETWEEN '01-JAN-DateTime.Now.Year" & " 00:00:00' AND '31-DEC-DateTime.Now.Year" & " 23:59:59'"
        End If
        strSql = strSql & "  ORDER BY DonationDate"
        Dim tableName As String() = New String(0) {}
        tableName(0) = "Donation"
        SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsDonation, tableName)
        If dsDonation.Tables(0).Rows.Count > 0 Then
            dgDonation.DataSource = dsDonation
            dgDonation.DataBind()
            pnlData.Visible = True
            pnlMessage.Visible = False
        Else
            If bIsFeeExits = False Then
                pnlMessage.Visible = True
                pnlData.Visible = False
                lblAlert.Text = "No donations Found."
            Else
                lblTaxDonation.Visible = False
            End If
        End If
        'End If
        'lblFromDate.Text = Request.QueryString("DonationDate")
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            'lblToDate.Text = dtCurrDate.ToString("MM") & "/" & dtCurrDate.Day & "/" & dtCurrDate.Year
        Else
            'lblToDate.Text = "12/31/" & Request.QueryString("Year")
        End If
    End Sub
    Protected Sub dgFess_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFess.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim donationAmount As Double = GetDonationAmount(DataBinder.Eval(e.Item.DataItem, "EventID"), DataBinder.Eval(e.Item.DataItem, "RegFee"))
            dblRegFee += donationAmount
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.Cells(3).Font.Bold = True
            e.Item.Cells(3).Font.Size = 12
            'e.Item.Cells(3).Text = "Sub Total: " & FormatNumber(dblRegFee, 2)
            txtRegFee.Value = FormatNumber(dblRegFee, 2)
        End If
        lblGrandTotal.Text = FormatNumber(CDbl("0" & txtRegFee.Value) + CDbl("0" & txtDonation.Value), 2)
    End Sub
    Protected Sub dgDonation_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDonation.ItemDataBound
        e.Item.Cells(1).Attributes.Add("align", "right")
        e.Item.Cells(2).Attributes.Add("align", "right")
        e.Item.Cells(3).Attributes.Add("align", "right")
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim donationAmount As Double = DataBinder.Eval(e.Item.DataItem, "Amount")
            dblDonationAmt += donationAmount
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.Cells(3).Font.Bold = True
            e.Item.Cells(3).Font.Size = 12
            ' dblDonationAmt += 100
            'e.Item.Cells(3).Text = "Sub Total: " & FormatNumber(dblDonationAmt, 2)
            e.Item.Cells(2).Visible = False
            e.Item.Cells(1).Visible = False
            e.Item.Cells(0).Visible = False
            e.Item.Cells(3).Visible = False



            txtDonation.Value = FormatNumber(dblDonationAmt, 2)
        End If
        lblGrandTotal.Text = FormatNumber(CDbl("0" & txtRegFee.Value) + CDbl("0" & txtDonation.Value), 2)
    End Sub
    Protected Function GetTaxablePercentage(ByVal EventID As Integer) As Decimal
        Dim iTaxablePercentage As Decimal
        If EventID = 1 Or EventID = 2 Then
            strSql = " SELECT taxdedregional, "
            strSql = strSql & " taxdednational from contestcategory "
            strSql = strSql & " where contestyear=" & DateTime.Now.Year & " and contestcode = 'JSB'"
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iTaxablePercentage = FormatNumber(CDbl("0" & drContestCategory("taxdednational")), 2)
            End If
            drContestCategory.Close()
        ElseIf EventID > 2 Then
            strSql = " select top 1 TaxDedRegional  from eventFees where EventYear =" & Request.QueryString("Year") & " and eventID=" & EventID & " and (ProductGroupCode='SB' OR ProductGroupCode='MB') "
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iTaxablePercentage = FormatNumber(CDbl("0" & drContestCategory("TaxDedRegional")), 2)
            End If
            drContestCategory.Close()
        End If
        Return iTaxablePercentage
    End Function
    Protected Function GetDonationAmount(ByVal EventID As Integer, ByVal RegFee As Double) As Decimal
        Dim iDonationAmount As Decimal
        If EventID = 1 Or EventID = 2 Then
            strSql = " SELECT taxdedregional, "
            strSql = strSql & " taxdednational from contestcategory "
            strSql = strSql & " where contestyear=" & DateTime.Now.Year & " and contestcode = 'JSB'"
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iDonationAmount = FormatNumber((drContestCategory("taxdednational") * RegFee) / 100, 2)
            End If
            drContestCategory.Close()
        ElseIf EventID > 2 Then
            strSql = " select top 1 TaxDedRegional  from eventFees where EventYear =" & Request.QueryString("Year") & " and eventID=" & EventID & " and (ProductGroupCode='SB' OR ProductGroupCode='MB') "
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iDonationAmount = FormatNumber((drContestCategory("TaxDedRegional") * RegFee) / 100, 2)
            End If
            drContestCategory.Close()
        End If
        Return iDonationAmount
    End Function

    Protected Sub txtHidden_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblCurrDate.Text = Date.Parse(txtHidden.Text).ToString("MMMM dd, yyyy")
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lbtnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim SendEmailAdd As String
            Dim Sendattach As String
            Dim sendbody As String

            If Request.QueryString("Type") = "OWN" Then
                SendEmailAdd = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select ISNull(Email,'') from OrganizationInfo where Automemberid in (" & Request.QueryString("MemberID") & ")")
            Else
                SendEmailAdd = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, " Select Case when I.MaritalStatus = 'Deceased'  Then Case when I2.Email is not null then I2.Email Else I.Email  End Else I.Email end  As Email from IndSpouse I LEFT JOIN IndSpouse I2 ON ((I.DonorType='IND' AND I.AutoMemberID =   I2.Relationship) OR (I.DonorType='SPOUSE' AND  I.Relationship = I2.AutoMemberID)) WHERE I.AUTOMEMBERID in (" & Request.QueryString("MemberID") & ")")
            End If
            Try
                'lbemail.Visible = True

                'lbemail.Text = "1" & Session("Attachmentfile").ToString()
                'lbemail.Text = "2" & Session("Strbodyval").ToString()
                'lbemail.Text = "3" & SendEmailAdd.ToString()
            Catch ex As Exception
                lbemail.Text = ex.Message
            End Try

            If SendEmailAdd.Trim.Length > 3 Then




                Sendattach = Session("Attachmentfile")
                sendbody = Session("Strbodyval")

                SendEmail("NSF Donation Receipt", sendbody, SendEmailAdd, Sendattach)


                'SendEmail("NSF Donation Receipt", strBody, Session("LoginEmail"), sAttachment)
                'SendEmail("NSF Donation Receipt", hdnMailBody.Value, SendEmailAdd, Sendattach)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub hlnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkSearch.Click
        'Response.Redirect("ViewDonations.aspx?ID=&" & Request.QueryString("MemberID") & "&Type=" & Request.QueryString("Type"))
        Response.Redirect("ViewDonations.aspx" & "?id=" & Request.QueryString("MemberID") & "&type=" & Request.QueryString("Type"))
    End Sub
End Class
