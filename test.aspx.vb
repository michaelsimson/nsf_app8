﻿Imports System.IO
Imports System.Text.RegularExpressions
Partial Class test
    Inherits System.Web.UI.Page
    Public i As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsPostBack = False Then
        '    Dim ant As Integer = 901 / 200
        '    Response.Write(ant & "<BR>")
        '    lblerr.Text = Now().ToString()
        '    i = 58448
        '    If Not Date.Parse("3/13/2011") < Now.Date Then
        '        lblerr.Text = "Okay : " & "3/13/2011 ; " & Now.Date.ToString()
        '    Else
        '        lblerr.Text = "False : " & Now.Date.ToString()
        '    End If
        'End If
        'Response.Write(Request.ApplicationPath)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strFileSize As String = ""
            'Dim di As New IO.DirectoryInfo(Server.MapPath(TextBox1.Text))
            Dim di As New IO.DirectoryInfo(TextBox1.Text)
            Dim aryFi As IO.FileInfo() = di.GetFiles()
            Dim fi As IO.FileInfo
            For Each fi In aryFi
                Response.Write("<br>" & fi.Name)
            Next
            lblerr.Text = ""
        Catch ex As Exception
            lblerr.Text = Server.MapPath(Server.MapPath(TextBox1.Text.Replace("/", "\")))
            lblerr.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strFileSize As String = ""
            'Dim di As New IO.DirectoryInfo(Server.MapPath(TextBox1.Text))
            Dim di As New IO.DirectoryInfo(TextBox1.Text)
            Dim aryFi As IO.DirectoryInfo() = di.GetDirectories()
            Dim fi As IO.DirectoryInfo
            For Each fi In aryFi
                Response.Write("<br>" & fi.Name)
            Next
            lblerr.Text = ""
        Catch ex As Exception
            'lblerr.Text = Server.MapPath(Server.MapPath(TextBox1.Text.Replace("/", "\")))
            lblerr.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        lblerr.Text = String.Empty
        '"^[a-zA-Z0-9][\w\.-\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        '"[A-Z0-9._%+-]+(@)[A-Z0-9-.]+[A-Za-z]{2,4}"

        '"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$" ' validatr the IP Address 
        'Dim pattern As String = "[(][-]*[0-9]+[.][0-9]+[)]"
        'JVB(27878)(33)(35.00)MB2(27878)(18)(-35.00) RegFee:70 Mealsamount:0 LateFee:0 Donation:0 Total:-35
        Dim pattern As String = "[T][o][t][a][l][:][-]*[0-9]+"
        Dim Matches As MatchCollection = Regex.Matches(txtIP.Text, pattern)
        Dim match As Match
        Dim capture As Capture
        For Each match In Matches
            For Each capture In match.Captures
                lblerr.Text = lblerr.Text & "" & Convert.ToDecimal(capture.Value.Replace("Total:", ""))
            Next
        Next
        'If Not IPAddressMatch.Success Then
        '    lblerr.Text = "Not a Valid Email" '"IP Address must be in nnn.nnn.nnn.nnn format"
        '    Exit Sub
        'Else
        '    lblerr.Text = "Valid Email"
        'End If
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
     
        Try
            Dim decodedBytes As Byte() = Encoding.UTF8.GetBytes(TextBox2.Text)
            Dim decodedText As String = Convert.ToBase64String(decodedBytes)
            lblerr.Text = decodedText
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub
End Class
