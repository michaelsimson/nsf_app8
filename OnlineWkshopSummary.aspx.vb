Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Partial Class OnlineWkshopSummary
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Now.Month <= 5 Then
                Session("Year") = Now.Year - 1
            Else
                Session("Year") = Now.Year
            End If
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                Try
                    lblAddress2.Text = drIndSpouse.Item("Address2")
                Catch ex As Exception

                End Try
                ' lblCity.Text = drIndSpouse.Item("City")
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If

        Dim objChild As New Child
        Dim dsChild As New DataSet

        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")

        If dsChild.Tables.Count > 0 Then
            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Rows.Count
            ViewState("ChildInfo") = dsChild
        End If


        Dim cmdText As String = " select distinct PD.ProductCode, PD.Name, EF.GradeFrom, EF.GradeTo From   dbo.OnlineWSCal  OWS Inner Join dbo.EventFees  EF ON  EF.ProductId = OWS.ProductId  and EF.EventYear = OWS.EventYear and EF.EventID = OWS.EventID    "
        cmdText = cmdText & "   Inner Join dbo.Product PD on PD.ProductID=OWS.ProductID and PD.EventID =  OWS.EVentID  where ows.eventyear = " & Session("Year") & " And ows.eventid = 20  and GetDate() <=  dbo.ufn_GetDeadLine(OWS.LateFeeDeadLine)  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
        If ds.Tables(0).Rows.Count > 0 Then
            divPrdList.Visible = True
            spEligTitle.Visible = True
            dgPrdDetails.DataSource = ds.Tables(0)
            dgPrdDetails.DataBind()
        End If

    End Sub

    Protected Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                'Dim dsContest As DataSet

                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cmd As New SqlCommand
                Dim dsInvitees As New DataSet
                Dim da As New SqlDataAdapter
                '  MsgBox("@EventID" & Session("EventID") & "@MemberID" & Session("CustIndID") & "@ChapterID" & Session("CustIndChapterID"))
                cmd.Connection = conn
                conn.Open()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "usp_GetEligibleEvents_OnlineWSCalendar"
                cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
                cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
                cmd.Parameters.Add(New SqlParameter("@EventYear", Session("Year")))

                'MsgBox(Session("CustIndChapterID"))
                da.SelectCommand = cmd
                da.Fill(dsInvitees)

                If dsInvitees.Tables(0).Rows.Count <= 0 Then
                    Dim cmdText As String = "select  case when (max(date)<=getdate() and min(date)<=getdate() ) or max(date) is null then 1 else 0 end from onlinewscal where eventyear=" & Session("Year") & " and eventid=20"

                    Dim i As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                    lblContestInfo.Text = "Child is not eligible for any online workshop based on grade."
                    If i = 1 Then
                        lblContestInfo.Text = "There are no online workshops scheduled at this time."
                    End If
                    ' lblContestInfo.Text = "There are no Online Workshops to register at this time. "
                    lblContestInfo.Visible = True
                Else
                    lblContestInfo.Visible = False
                End If

                Dim sbContest, sbDate, sbTime, sbRegDeadline, sbLateRegDeadline As New StringBuilder, strContestDesc, StrDate, strTime, strRegDeadline, strLateRegDeadline As String
                strContestDesc = ""
                sbContest.Append("<ul>")

                StrDate = ""
                sbDate.Append("<ul>")

                strTime = ""
                sbTime.Append("<ul>")

                strRegDeadline = ""
                sbRegDeadline.Append("<ul>")


                strLateRegDeadline = ""
                sbLateRegDeadline.Append("<ul>")

                For Each dr As DataRow In dsInvitees.Tables(0).Rows
                    If dr.Item("ChildNumber") = e.Item.DataItem("ChildNumber") Then
                        ' If strContestDesc <> dr.Item("ProductName") Then
                        sbContest.Append("<li>" & dr.Item("ProductName") & "</li>")
                        strContestDesc = dr.Item("ProductName")

                        sbDate.Append("<li>" & dr.Item("EventDate") & "</li>")
                        StrDate = dr.Item("EventDate")

                        sbTime.Append("<li>" & dr.Item("Time").ToString() & "</li>")
                        strTime = dr.Item("Time").ToString()

                        sbRegDeadline.Append("<li>" & dr.Item("RegistrationDeadline").ToString() & "</li>")
                        strRegDeadline = dr.Item("RegistrationDeadline").ToString()

                        sbLateRegDeadline.Append("<li>" & dr.Item("LateFeeDeadLine").ToString() & "</li>")
                        strLateRegDeadline = dr.Item("LateFeeDeadLine").ToString()

                        'End If
                        If InStr(dr.Item("EventDate"), "Announced") > 0 Then
                            lblContestDateInfo.Text = "Date(s)/Venue is not yet available for your Online Workshops."
                            lblContestDateInfo.Visible = True
                        Else
                            lblContestDateInfo.Visible = False
                        End If
                    End If
                Next
                sbContest.Append("</ul>")
                sbDate.Append("</ul>")
                sbTime.Append("</ul>")
                sbRegDeadline.Append("</ul>")
                sbLateRegDeadline.Append("</ul>")

                CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString
                CType(e.Item.FindControl("lblDate"), Label).Text = sbDate.ToString
                CType(e.Item.FindControl("lblTime"), Label).Text = sbTime.ToString
                CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text = sbRegDeadline.ToString
                CType(e.Item.FindControl("lblLateRegistrationDeadline"), Label).Text = sbLateRegDeadline.ToString


        End Select
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Response.Redirect("~/OnlineWkshopRegistration.aspx")
    End Sub
End Class
