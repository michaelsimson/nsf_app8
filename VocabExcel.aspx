﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VocabExcel.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="VocabExcel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            Vocab Excel
                     <br />
            <br />
        </div>
        <center>
            Import Excel File:  
        <asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
            <br />
            <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" />
            <asp:Button ID="BtnExport" runat="server" OnClick="BtnExport_Click" Text="Export"  />
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" ForeColor="red"></asp:Label>
            <br />
            <asp:GridView ID="gvExcelFile" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </center>
    </div>
</asp:Content>
