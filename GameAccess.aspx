<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="GameAccess.aspx.cs" Inherits="GameAccess" Title="Game Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:HyperLink ID="backToUserFunctions"  CssClass="btn_02"  NavigateUrl="UserFunctions.aspx" runat="server" >Back to Parent Functions Page</asp:HyperLink>
        <center>
        <asp:Label ID="lblTitle" runat="server" Text="Create Game Login" 
                Font-Bold="True" Font-Size="Large" ForeColor="#009900"></asp:Label>
            <br />
            <br />
        </center>
    <div>

        <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>

    </div>
        <div   align = "center">
            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" 
                BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                CellPadding="4" Height="63px" 
                onselectedindexchanged="gvChild_SelectedIndexChanged" Width="303px" EnableModelValidation="True" OnRowDataBound="gvChild_RowDataBound">
                <RowStyle BackColor="White" ForeColor="#330099" />
                <Columns>
                    <asp:BoundField DataField="ChildNumber" HeaderText="Child Number" 
                        SortExpression="ChildNumber">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False" 
                            BorderColor="Black" />
                        <ItemStyle Wrap="False" BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ChildName" HeaderText="Name Of Child" 
                        SortExpression="ChildName">
                        <ControlStyle Width="100px" BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" HorizontalAlign="Left" 
                            VerticalAlign="Middle" Wrap="False" BorderColor="Black" />
                        <ItemStyle Wrap="False" BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product Code" SortExpression="ProductCode" Visible="False">
                    <HeaderStyle BackColor="#009900" BorderColor="Black" ForeColor="White" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductName" HeaderText="Product Name" SortExpression="ProductName" Visible="False">
                    <HeaderStyle BackColor="#009900" BorderColor="Black" ForeColor="White" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="GRADE" HeaderText="Grade" SortExpression="Grade">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" 
                        SortExpression="EndDate" Visible="False">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False" 
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LoginID" HeaderText="LoginID" 
                        SortExpression="LoginID" HtmlEncode="False" HtmlEncodeFormatString="False">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False" 
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False" 
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            </asp:GridView>
             </div>
          
            <asp:HiddenField ID="hdn_userID" runat="server" />
        <asp:HiddenField ID="hdn_pcode" runat="server" />
        <asp:HiddenField ID="hdn_childID" runat="server" />
            <br />
       
       
     <div>
            <asp:Panel ID="Panel2" runat="server" Visible="False">
             <div   align = "center">
                    <table style="width: 52%; "left" ; height: 57px;">
                        <tr>
                            <td style="width: 83px"align="left" >
                                Login ID</td>
                            <td style="width: 299px"align="left">
                                <asp:TextBox ID="txtEmailID" runat="server" Enabled="False" 
                                    style="text-align: left" Width="292px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtEmailID" Display="Dynamic" ErrorMessage="Enter Login ID."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 83px"align="left">
                                Password</td>
                            <td style="width: 299px" align="left">
                                <asp:TextBox ID="txtPassword" runat="server" Enabled="False" Width="210px" 
                                    style="text-align: left"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txtPassword" Display="Dynamic" 
                                    ErrorMessage="Enter Password."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 83px; height: 31px;" align="left" >
                            </td>
                            <td style="width: 299px; height: 31px" align="left" ;>
                                <asp:Button ID="btnAdd" runat="server" Enabled="False" onclick="btnAdd_Click" 
                                    Text="Add" />
                                <asp:Button ID="btnUpdate" runat="server" Enabled="False" 
                                    onclick="btnUpdate_Click" Text="Update" />
                                <asp:Label ID="lblResult" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblNote" runat="server" Font-Bold="False"  ForeColor="Green"
                                    Text="Note: If LoginID and PWD are blank or if you want to change them, press 'Select'.  Without these, your child will not be able to access the game."></asp:Label>
                            </td>
                        </tr>
                    </table>
               </div>
            </asp:Panel>      
        </div>
</asp:Content>

