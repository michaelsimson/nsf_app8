Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ChkPaymentNotes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If IsPostBack = False Then
                loadEvent()
            End If
        Catch ex As Exception
            Server.Transfer("Maintest.aspx")
        End Try
    End Sub

    Private Sub loadEvent()
        Dim i As Integer
        lstEvent.Items.Clear()
        Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from Event where EventID in (1,2,3,5,11,13,18)")
        i = 1
        While read1.Read()
            Dim litem As New ListItem
            litem.Text = read1("Name")
            litem.Value = read1("EventID")
            lstEvent.Items.Add(litem)
            i = i + 1
        End While
        lstEvent.Items.Insert(0, "All Events")
        lstEvent.SelectedIndex = 0
        read1.Close()
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub

    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim i As Integer
            Dim Eventstr As String = ""
            If Not lstEvent.Items(0).Selected = True Then
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If
            Dim StrSQL As String
            StrSQL = " Select [Last Name] as LName, [First Name] as FName,[Contribution Date] as CDate,[Contribution Amount] as CAmount, [Donation Type] as DType, [Payment Date] as PDate,unique_id, event_for,asp_session_id, MealsAmount,LateFee, EventId, MemberId, ChapterId, Fee,"
            StrSQL = StrSQL & " TotalPayment, EventYear, PaymentNotes,CalcpaymentsNotes," & ddlCategory.SelectedValue & " as ToCheckNotes, '' as Matched from  NFG_Transactions"
            StrSQL = StrSQL & "  WHERE PaymentNotes is not null " & Eventstr & " AND "
            StrSQL = StrSQL & " [Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'"
            'Response.Write(StrSQL)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                GVPaymentNotes.DataSource = ds
                GVPaymentNotes.DataBind()
                btnExport.Enabled = True
            Else
                btnExport.Enabled = False
                GVPaymentNotes.DataSource = Nothing
                GVPaymentNotes.DataBind()
                lblErr.Text = "No records with Payment Notes record to show"
                Exit Sub
            End If
        End If

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & ddlCategory.SelectedItem.Text.Replace(" ", "") & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GVPaymentNotes.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub GVPaymentNotes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVPaymentNotes.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If Not DataBinder.Eval(e.Row.DataItem, "ToCheckNotes") Is DBNull.Value Then
                Dim chkpaymentnotes As String = DataBinder.Eval(e.Row.DataItem, "ToCheckNotes")
                e.Row.Cells(18).Text = IIf(CheckPaymentNotes(chkpaymentnotes), "True", "False")
            Else
                e.Row.Cells(18).Text = ""
            End If
        End If

    End Sub

    Function CheckPaymentNotes(ByVal chkpaymentnotes As String) As Boolean
        'Get the amount in it and Check
        Dim match As Match
        Dim capture As Capture
        Dim Amt As Decimal = 0
        Dim Totamt As Decimal = 0
        Dim Mealamt As Decimal = 0
        Dim Donamt As Decimal = 0
        'individual Registrations
        Dim pattern As String = "[(][-]*[0-9]+[.][0-9]+[)]"
        ' LateFee:0 Donation:0 
        Dim Matches As MatchCollection = Regex.Matches(chkpaymentnotes, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Amt = Amt + Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
            Next
        Next

        ''Mealsamount:0
        pattern = "[M][e][a][l][s][a][m][o][u][n][t][:][-]*[0-9]+"
        Matches = Regex.Matches(chkpaymentnotes, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Mealamt = Convert.ToDecimal(capture.Value.Replace("Mealsamount:", ""))
            Next
        Next

        'Donation:0 
        pattern = "[D][o][n][a][t][i][o][n][:][-]*[0-9]+"
        Matches = Regex.Matches(chkpaymentnotes, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Donamt = Convert.ToDecimal(capture.Value.Replace("Donation:", ""))
            Next
        Next

        'Total:
        pattern = "[T][o][t][a][l][:][-]*[0-9]+"
        Matches = Regex.Matches(chkpaymentnotes, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Totamt = Convert.ToDecimal(capture.Value.Replace("Total:", ""))
            Next
        Next

        If (Amt + Mealamt + Donamt) <> Totamt Then
            Return False
        End If
        Return True
    End Function
End Class
