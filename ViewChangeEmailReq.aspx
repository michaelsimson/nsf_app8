﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewChangeEmailReq.aspx.vb" Inherits="ViewChangeEmailReq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
    
				<tr bgcolor="#FFFFFF">	
				    <td colspan="4">				
					    <asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/VolunteerFunctions.aspx" >Back to Volunteer Functions</asp:hyperlink>&nbsp;&nbsp;&nbsp;					    
					</td>
				</tr>	
				<tr bgcolor="#FFFFFF">
					<td  colspan="2" align="left">
					
					<table cellpadding = "3" cellspacing ="0" border = "0" style ="width:900px">
					<tr bgcolor="#FFFFFF">
					<td  class="Heading" colspan="4" align="center">View Change Email Requests</td>
				</tr>
				<tr bgcolor="#FFFFFF" >
				    <td class ="SmallFont"  colspan="2" align="right" style="width:50%" >
				        Status : 				        <asp:DropDownList runat="server" ID="ddlStatus" Width="200px">
				            <asp:ListItem Text="AutoApproval" Value="AutoApproval"></asp:ListItem>
				            <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
				            <asp:ListItem Text="Approved " Value="Approved "></asp:ListItem>
				          </asp:DropDownList>&nbsp;&nbsp;
				    </td>
				     <td class ="SmallFont"  colspan="2" align="left" >&nbsp;&nbsp;
				        #of Records : 
				        <asp:DropDownList runat="server" ID="DdlRecords" Width="100px">
				            <asp:ListItem Text="100" Value="100" Selected="True"></asp:ListItem>
				            <asp:ListItem Text="500" Value="500"></asp:ListItem>
				            <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
				            <asp:ListItem Text="All" Value="All"></asp:ListItem>
				        </asp:DropDownList>
				    </td>
				</tr>			
				<tr>
				    <td class="ItemCenter"  colspan="4" align="center" >
				        <asp:Button runat="server" ID="btnRunReport" OnClick="btnRunReport_Click"  Text="Run Report"/>
				    </td>
				</tr>
					</table>
				    </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td colspan="2">
					    <asp:datagrid id="dgDuplicates" runat="server" Width="100%"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="ChangeEmailID" AllowPaging="True" AllowSorting="True" PageSize="30">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
							<ItemStyle Wrap="False" ></ItemStyle>
							<HeaderStyle  Wrap="False"></HeaderStyle>
							<Columns>
							    
                                   <asp:TemplateColumn  ItemStyle-Wrap="false" HeaderStyle-Wrap="false"  HeaderText="Change Email">
							        <ItemTemplate>
							             <asp:LinkButton id="lbtnChangeEmail" runat="server" CommandName="ChangeEmail" Text="Change Email"></asp:LinkButton>
							        </ItemTemplate>							        
							    </asp:TemplateColumn>
							  	<asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="OldEmail"  HeaderText="OldEmail" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="NewEmail"  HeaderText="NewEmail" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="OldE_IndCount"  HeaderText="OldE_IndCount" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="NewE_IndCount"  HeaderText="NewE_IndCount" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="OldMemberID"  HeaderText="OldMemberID" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="NewMemberID"  HeaderText="NewMemberID" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Status"  HeaderText="Status" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Remarks"  HeaderText="Remarks" />
								 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="CreatedDate"  HeaderText="CreatedDate" DataFormatString="{0:d}" />
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center" colspan="2"><asp:label id="lblAlert" runat="server"  CssClass="ErrorFont" ></asp:label></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  class="Heading" colspan="2" height="15"></td>
				</tr>
 				<tr bgcolor="#FFFFFF">
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label></td>
				</tr> 
	            </table>
</asp:Content>