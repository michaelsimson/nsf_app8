
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/other.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation</title>

<!--#include file="incfiles.aspx"-->

</head>
<body>
<!--#include file="/public/main/header.aspx"-->

                <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center"></td>
                          </tr>
                          <tr>
                            <td align="center">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			    <tr>
			    <td align="left">
			       
			        
			    </td>
			    </tr>
			    <!--
			    <tr>
			    <td align="left">
			        <table border="0" align="left" cellpadding="0" cellspacing="0">
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td height="16" align="center" valign="middle" class="btn_05">&nbsp;&nbsp;Follow Us: </td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://twitter.com/" target="_blank"><img src="/public/images/icon_twitter_15x15.gif" alt="Twitter" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.facebook.com/" target="_blank"><img src="../images/icon_facebook_15x15.gif" alt="Facebook" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.youtube.com/" target="_blank"><img src="../images/icon_youtube_35x15.gif" alt="Youtube" width="35" height="15" border="0" /></a>&nbsp;</td>
			        </tr>
			        
			        <tr>
			            <td width="64%"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                <td width="32%" align="center"><a href="http://twitter.com/" target="_blank"><img src="../images/twitter.png" alt="Twitter" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.facebook.com/" target="_blank"><img src="../images/facebook.png" alt="Facebook" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.youtube.com/" target="_blank"><img src="../images/youtube.png" alt="Youtube" width="32" height="32" border="0" /></a></td>
			                </tr>
			            </table></td>
			        </tr>
			        
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        </table>
			    </td>
			    </tr>
			    -->
			    <tr>
			    <td><!--#include file="excelathon_links.aspx"--></td>
			    </tr>
			    <%--<tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="faq_excelathon.aspx" class="btn_02" >Excelathon FAQ</a></td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=cha" class="btn_02" >Chapterwise Report</a> (Top 100)</td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=chl" class="btn_02" >Studentswise Report</a> (Top 100)</td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=cha" class="btn_02" >National Report</a> (Top 100)</td>
			    </tr>--%>
			    </table>

                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td>

<div class="title02" align="center">Excel-a-thon<br />
Encourage Education - The Gift of a Lifetime</div>

<div class="txt01">
Did you know that <B>just $250 pays one year's tuition for a college student in India?</B><br><br>
North South Foundation (NSF) was founded to promote and foster academic excellence.  Through contests and programs we have helped fund more than 5,000 scholarships in India.  These scholarships are for academically gifted students in India who cannot afford to pay tuition. Now in its 23rd year, the NSF community has set a goal to increase the number of scholarships tenfold over the next five years. 
<br><br>
<b>How can you help?</b> As a contestant, we challenge you to raise funds by asking your network to sponsor your cause to spread academic excellence. Excel-a-thon is a new program which allows others to sponsor NSF contestants who want to raise funds for scholarships in India. We hope that you will reach out to your family, relatives, neighbors and friends, and ask them to sponsor your cause. <b>If you can get only ten donors to contribute $25 each, you will help one student in India go to college.</b> You can be proud of your achievement and help someone in need excel academically - a Noble Cause Through a Brilliant Mind!
<br><br>
This program is voluntary but our hope is that we will have 100% participation.  NSF is entirely volunteer driven with no overhead of administrative costs. 100% of donations go directly to the cause.  Please register now to make a difference.  <insert link> Once you register you will be able to set up your own personalized page on the NSF website. We have also included instructions and frequently asked questions to provide you with more information on the program. There will be special recognition and prizes for contestants who raise the most.  
<br><br>
<b>You are doing more than participating in a contest; you are bringing education to those in need through Excel-a-thon! </b> Together we can make a huge impact to Indian society and the lives of academically gifted students who just do not have the money to pay for a college education.  We hope that you will reach out to your family, relatives, friends and neighbors, and ask them to support our critical mission. 
<br><br><div class="title02">
Please <a href="../../App9/DThonRegis.aspx?Ev=18" >  click here </a> to finish the registration process.
 </div>
Sincerely, <br>
Dr. Ratnam Chitturi and the NSF 2015 Fundraising Committee<br><br>

<!-- Histats.com START (hidden counter)--> <script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script> <a href="http://www.histats.com" target="_blank" title="stats counter" ><script type="text/javascript" > try {Histats.start(1,1896389,4,0,0,0,""); Histats.track_hits();} catch(err){}; </script></a> <noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?1896389&101" alt="stats counter" border="0"></a></noscript> <!-- Histats.com END --> 

<hr />
<div align="center">
Thank you for your generous support in education!<br />
If you have problems in making payment contact <a class="btn_02" href="mailto:admin@northsouth.org">admin@northsouth.org</a> for assistance.
</div>
</div>

<!--#include file="simple_footer.aspx"-->

