<%@ Page language="C#" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<!--#include file="scholarships_home_header1.aspx"-->
<%--    <asp:Content ID="Content2" ContentPlaceHolderID="Cheader" runat="server">
        <title>North South Foundation</title>
    <meta name="keywords" content="North South" />
    <meta name="description" content="North South Foundation,Encouraging Excellence in Education" />
    <link href="main.css" rel="Stylesheet" type="text/css" />
    
</asp:Content>

<asp:Content ID="content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
    
   
    </script>--%>
    <script runat="server">
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String Str = Request.QueryString["SubID"];
                if (Str != "" & Str != "##ID##")
                {
                    BtnSubmit.Enabled = true;
                    txtEmail.Text = Request.QueryString["SubID"].ToString();
                }
                else
                {
                    lblhelp.Text = "Unsubscribe cannot be done from this page";
                    BtnSubmit.Enabled = false;
                }
            }
            catch 
            {
                lblhelp.Text = "Unsubscribe cannot be done from this page";
                BtnSubmit.Enabled = false;
            }
        }
        
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            
            string email = txtEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                String strquery;
                int numberOfRecords = 0;
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                conn.Open();
                strquery = "Update IndSpouse Set Newsletter = '1'  where Email = '" + txtEmail.Text + "'";
                SqlCommand cmd = new SqlCommand(strquery, conn);
                numberOfRecords = numberOfRecords + cmd.ExecuteNonQuery();

                strquery = "delete from newsletter_subscribers where email_address = '" + txtEmail.Text + "'";
                cmd = new SqlCommand(strquery, conn);
                numberOfRecords = numberOfRecords + cmd.ExecuteNonQuery();
                conn.Close();
                if (numberOfRecords > 0)
                {
                    lblfinal.Visible = true;
                    lblhelp.Visible = false;

                }
                else
                {
                    lblfinal.Visible = false;
                    lblhelp.Text = "Email ID not matching with the existing records";
                }
            }

            else

            {
                lblhelp.Text = "Email ID not Valid";
            }
        }
    </script>
    


<html>
<head id="Head1" runat="server">
    <title>Unsubscribe Details</title>
</head>
<body>
 <form id="form1" runat="server">
  <div align="center" >
 <table width="700px" runat="server">
 <tr>
 <td align="center">
<h1> Newsletter Unsubscribe </h1>
<br />
 </td>
 </tr>
 </table>
 <table width="450px" runat="server">
 <tr>
 <td align="right">
                               
                            </td>
                            <td align="center">
                             <asp:Label ID="lblFirstName" runat="server" Text="Email"></asp:Label>&nbsp;&nbsp;
                                <asp:TextBox ID="txtEmail" ReadOnly="true" Width="235px" runat="server"></asp:TextBox>

                            </td>
                            <td align="center">
                                <asp:RequiredFieldValidator ID="rfvTxtEmail" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Please enter FirstName" ValidationGroup="vg1">*</asp:RequiredFieldValidator>
                                    
                            </td>
 </tr>

 <tr>
 <td colspan="3" >
 <br />
 </td>
 </tr>
 <tr >
 <td colspan="3" align="center">
  <asp:Button ID="BtnSubmit" runat="server" Text="Unsubscribe" ValidationGroup="vg1"  
         Width="130px" onclick="BtnSubmit_Click"  />
 </td>
 </tr>
 <tr>
 <td colspan="3" align="center">
    <asp:Label ID="lblfinal" runat="server" Text="" Visible="false">  You have been removed from mailing list. <br /> <a href="home.aspx"> Click here to go back to Homepage </a></asp:Label>
    <br />
     <asp:Label ID="lblhelp" runat="server" Text="" Visible="True">Click: Unsubscribe Button To Complete</asp:Label>
 </td>
 
 </tr>
 </table>
    </div>
    <br />
     <br />
      <br />
     <br />
     <br />
     <br />
     <br />
</form>

   </body>
   <!--#include file="simple_footer.aspx"-->
</html>