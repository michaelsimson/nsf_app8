
<!--#include file="simple_header.aspx"-->

<div class="title02">Contact Us</div>

<div class="txt01">

<p><span class="title04">Founder</span></p>

<p><span class="txt01_strong">Dr. Ratnam Chitturi</span><br/>
North South Foundation<br/>
2 Marissa Ct<br/>
Burr Ridge, IL 60527 (USA)<br/>
Phone: 630-323-1966<br/>
Email: chitturi9@gmail.com
</p>

<hr/>
<p>
Please go to the <a class="btn_02" href="/public/USContests/Regionals/calendar.aspx">Regional Contests Calendar</a>
for more details on the contests at your chapter and the contact info about your chapter coordinator.
</p>
<hr/>

<p><span class="title04">Zonal Coordinators</span></p>

<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td width="40%" height="24" class="btn_06">&nbsp;&nbsp; Zone</td>
        <td width="30%" height="24" class="btn_06">&nbsp;&nbsp; Coordinator</td>
        <td width="30%" height="24" class="btn_06">&nbsp;&nbsp; Email</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">North-East (NH, MA)</td>
        <td width="30%" bgcolor="#FFFFFF">Savitha Rajiv</td>
        <td width="30%" bgcolor="#FFFFFF">savitha_usa@yahoo.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">Atlantic (CT, NY, MD, VA)</td>
        <td width="30%" bgcolor="#FFFFFF">Venkat Gade</td>
        <td width="30%" bgcolor="#FFFFFF">venkatgade1@yahoo.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">Mid-Atlantic (NJ, PA)</td>
        <td width="30%" bgcolor="#FFFFFF">Jagadeesh Gullapalli</td>
        <td width="30%" bgcolor="#FFFFFF">jag.gullapalli@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">South-Central (NC, SC)</td>
        <td width="30%" bgcolor="#FFFFFF">Prasad Lanka</td>
        <td width="30%" bgcolor="#FFFFFF">lspras@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">South (TN, LA)</td>
        <td width="30%" bgcolor="#FFFFFF">Palani Ponnapakkam</td>
        <td width="30%" bgcolor="#FFFFFF">pponnapa@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">South-East (GA, FL, AL)</td>
        <td width="30%" bgcolor="#FFFFFF">Janevi Ramaji </td>
        <td width="30%" bgcolor="#FFFFFF">janevi@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">Texas (TX, OK)</td>
        <td width="30%" bgcolor="#FFFFFF">Pavankumar Petluru </td>
        <td width="30%" bgcolor="#FFFFFF">pavanpetluru@yahoo.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">MW-East (OH, MI, IN)</td>
        <td width="30%" bgcolor="#FFFFFF">Srinivas Gudeti </td>
        <td width="30%" bgcolor="#FFFFFF">sgudeti@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">MW-Central (MN, WI, IA, IL)</td>
        <td width="30%" bgcolor="#FFFFFF">MS Rao</td>
        <td width="30%" bgcolor="#FFFFFF">msrao9999@yahoo.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">MW-West (KY, MO, KS, NE)</td>
        <td width="30%" bgcolor="#FFFFFF">Praveen Katta</td>
        <td width="30%" bgcolor="#FFFFFF">kattap@gmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">North-West (WA, OR, Sacramento)</td>
        <td width="30%" bgcolor="#FFFFFF">Prasanna Paralkar </td>
        <td width="30%" bgcolor="#FFFFFF">paralkar@comcast.net</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">Northern-CA (Palo Alto, Milpitas, San Ramon)</td>
        <td width="30%" bgcolor="#FFFFFF">Sumana Sur</td>
        <td width="30%" bgcolor="#FFFFFF">sumana_sur@yahoo.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">Southern-CA (LA, Irvine, Bakersfield, San Diego)</td>
        <td width="30%" bgcolor="#FFFFFF">Surendra Dara </td>
        <td width="30%" bgcolor="#FFFFFF">surendra_d@hotmail.com</td>
    </tr>
    <tr>
        <td width="40%" bgcolor="#FFFFFF" class="txt01_strong">West (AZ, CO, UT)</td>
        <td width="30%" bgcolor="#FFFFFF">Jyotsna Gunturu </td>
        <td width="30%" bgcolor="#FFFFFF">jgunturu@gmail.com</td>
    </tr>
</table>

<p><span class="title04">India Scholarships</span></p>
<p>
<span class="txt01_strong">Madavi Nathan Oliver</span><br/>
Phone: 978-486-0686
</p>

<p><span class="title04">Web Administrator</span></p>
<p>
nsfcontests@gmail.com
</p>

</div>

<!--#include file="simple_footer.aspx"-->

