
<!--#include file="simple_header.aspx"-->

<div class="title02">Frequently Asked Questions (FAQ)</div>

<div class="txt01">

<span class="btn_02" align="justify">
<ul>
<a name="top"></a>
<li><a href="#Q1">When was the North South Foundation incorporated?</a></li>
<li><a href="#Q2">When did the scholarship program start?</a></li>
<li><a href="#Q3">Who administers the program in India?</a></li>
<li><a href="#Q4">Where do you find the students for awarding scholarships?</a></li>
<li><a href="#Q5">What is the selection criteria for giving scholarships?</a></li>
<li><a href="#Q6">What are the guidelines in screening the applications?</a></li>
<li><a href="#Q7">Who selects the candidates for awarding scholarships?</a></li>
<li><a href="#Q8">How do you know that a student is really in need of financial assistance?</a></li>
<li><a href="#Q9">Are the renewals automatic?</a></li>
<li><a href="#Q10">How much scholarship money do the students receive?</a></li>
<li><a href="#Q11">What has been the track record so far?</a></li>
<li><a href="#Q12">What happens to the money I donate to the Foundation?</a></li>
<li><a href="#Q13">Does NSF-India raise contributions domestically in India?</a></li>
<li><a href="#Q14">What is the overhead and who covers it?</a></li>
<li><a href="#Q15">Do the Board members of the Foundation make contributions each year?</a></li>
<li><a href="#Q16">Can one designate a contribution to a specific school?</a></li>
<li><a href="#Q17">Does the Foundation produce periodic financial statements?</a></li>
<li><a href="#Q18">What centers are in operation for awarding scholarships?</a></li>
<li><a href="#Q19">What is the criteria to open a new center?</a></li>
</ul>
</span>

<hr />

<p><a name="Q1"></a><span class="btn_02">
When was the North South Foundation incorporated?
</span></p>
<p>
North South Foundation (NSF) was incorporated in 1989 as a not-for-profit entity in Illinois. It was granted tax-exempt status by the IRS under the 501(c)(3) program. Its federal tax ID is 36-3659998.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q2"></a><span class="btn_02">
When did the scholarship program start?
</span></p>
<p>
The program started in 1989 with one scholarship.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q3"></a><span class="btn_02">
Who administers the program in India?
</span></p>
<p>
North South Foundation has chapters in several states. Each chapter is run by a group of dedicated and committed volunteers who manage the process beginning with publicity to screening and awarding scholarships. Each team takes total responsibility.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q4"></a><span class="btn_02">
Where do you find the students for awarding scholarships?
</span></p>
<p>
The availability of scholarships is announced during June and July in all major news media. Interested students respond by requesting applications. Along with the filled out application, students are asked to submit academic scores, evidence of need for financial assistance from relevant revenue officers, and character references.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q5"></a><span class="btn_02">
What is the selection criteria for giving scholarships?
</span></p>
<p>
Merit and need are the only criteria for eligibility. The scholarships are awarded to students regardless of their religion, gender, caste, or geographic origin.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q6"></a><span class="btn_02">
What are the guidelines in screening the applications?
</span></p>
<p>
There are two basic guidelines: 1) The student should be among the top 5% (relaxed to 10% for rural and girl students) in the Common Entrance Test (CET),or achieve 85 percent or above marks in Intermediate or Higher Secondary School Exam, in absence of CET or its equivalent. 2) Annual family income must be less than Rs 38,000 in urban areas and Rs. 26,000 in rural areas.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q7"></a><span class="btn_02">
Who selects the candidates for awarding scholarships?
</span></p>
<p>
At each chapter, a selection committee is formed to assure that the most deserving students are awarded scholarships. The selection committee mostly consists of educators with excellent track records and integrity.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q8"></a><span class="btn_02">
How do you know that a student is really in need of financial assistance?
</span></p>
<p>
First, students coming through the public school stream are preferred, as they tend come from poor families. Second, the student is asked to submit an income certificate from a revenue officer or the equivalent. While the certificate is a starting point, NSF doesn't solely depend on it. Further, each qualified and short-listed student is called for a personal interview along with a parents. For the out of towners, NSF pays one way fare to attned the interview. The interview is conducted one-on-one along with the parent. Final selection is made based on the facts presented and the deliberations at the interview. In some cases the determination of the need is relatively straightforward (ex: coolie,tailor, rikshawala, blacksmith, etc.) In other cases, the determination of the need may not be as easy. For example, if the father is a farmer, it can sometimes present a challenge in verifying the number of acres and the associated crop potential. When found necessary, additional investigation is made from the area where the student comes from to ascertain the real financial need of the student.

When a student is selected for a scholarship, the student is asked to receive the award in person at an awards ceremony, in front of a full audience. NSF makes it a point to announce that all these awards are being given to poor families.

In summary, North South Foundation is sensitive to this issue and is trying its best to ensure that the most deserving students receive the scholarships.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q9"></a><span class="btn_02">
Are the renewals automatic?
</span></p>
<p>
No. Recipients need to demonstrate continued academic excellence to receive support each year. In other words, it is simply not enough to pass, but they should maintain the high standards they had before they received their scholarships.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q10"></a><span class="btn_02">
How much scholarship money do the students receive?
</span></p>
<p>
Awards cover the tuition fee. The student is expected to secure funding for other expenses from other sources. Scholarship awards are generally in the range of $200 - $250 per year per student.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q11"></a><span class="btn_02">
What has been the track record so far?
</span></p>
<p>
The Foundation started with one scholarship in 1989 and steadily increased over time. During 2004, it awarded 300 scholarships. They were divided among the polytechnic, engineering, medicine, and science majors. The target for 2005 is 500 scholarships.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q12"></a><span class="btn_02">
What happens to the money I donate to the Foundation?
</span></p>
<p>
Your money gets deposited into the North South Foundation account. In turn, NSF sends forward your money to NSF-India Chapters for disbursements. Chapter directly disburses the funds to the students via bank demand deposits.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q13"></a><span class="btn_02">
Does NSF-India raise contributions domestically in India?
</span></p>
<p>
To date NSF-India has not solicited any outside funding.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q14"></a><span class="btn_02">
What is the overhead and who covers it?
</span></p>
<p>
In most of the US charities, the overhead typically runs 30 percent or higher. In contrast, the Foundation's overhead amounted to less than 5 percent over the years. The Foundation is very sensitive to the overhead expense, as it is the money that could otherwise go to the deserving students. No wages are paid to the officers, directors or volunteers. Most of the expense is towards printing and mailing. Part of this expense is met from other activities like selling T-shirts or specially designated as such by particular donors.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q15"></a><span class="btn_02">
Do the Board members of the Foundation make contributions each year?
</span></p>
<p>
Yes. Their contributions are significant over the years.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q16"></a><span class="btn_02">
Can one designate a contribution to a specific school?
</span></p>
<p>
Yes. You can designate to a college or area of your choice. One donor, for example, has been contributing money for 15 scholarships to Telangana, a backward area in Andhra Pradesh. Similarly there are designated scholarships in Bangalore and Kolkata.

In designation, one should keep in mind that a scholarship should be given for not just one year but through graduation. Thus it may cost $800 to graduate a student through college. Also it is not a good idea to just sponsor one student and then wait for the student to graduate before giving to another student. Ideally one new student should be selected each year. Thus in a four-year college, there will be four students receiving scholarships at the end of the fourth year. Thus a contributor should ideally commit $800 every year to be able to sponsor a new student each year. Obviously all of this discussion applies only to designated scholarships. Without designation, you can make any amount of contribution you like.

In order to avail this opportunity, you simply write a letter to the Foundation along with your contribution. The scholarships can also be given in the name of a parent, spouse, or child.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q17"></a><span class="btn_02">
Does the Foundation produce periodic financial statements?
</span></p>
<p>
Yes. The Foundation files annual financial statements with the IRS. They are available for public inspection at the home office. In addition, the financial highlights are included in the North South Review, published annually by the Foundation. You can call or write to the Foundation to receive the latest copy of the North South Review. Similarly NSF-India Chapters files are financial statements with the government of India.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q18"></a><span class="btn_02">
What centers are in operation for awarding scholarships?
</span></p>
<p>
Currently there are seventeen centers in operation: Bangalore, Bhavnagar, Bhubaneswar, Chennai, Northeast States, Hyderabad, Jodhpur, Kolkata, Pune, Ahmedabad, Jamshedpur, Kanpur, Katihar, Kochi, Madurai, Mau and Trivandrum. Hyderabad was opened in 1989, Kolkatta in 1993, Bangalore in 1994, Bhavnagar in 1999, Bhubaneswar in 2000, Chennai in 2000, Northeast States (Guwahati) in 2001, and Jodhpur, Lucknow & Pune in 2002 and the rest in 2004-5. There is a plan to add more centers as suitable coordinators are found in other parts of the country.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q19"></a><span class="btn_02">
What is the criteria to open a new center?
</span></p>
<p>
Dedicated volunteers and reliable funding are the two major criteria for opening new centers. Once a center opens, the Foundation wants to make sure that the funds are available to operate it successfully on a sustained basis. For example, if a center is added to provide 10 new scholarships each year, the number of scholarships will grow to 40 after four years including renewals. At $200 for each scholarship, the total outlay will come to $8,000 per year. Thus there has to be a reasonable assurance that such level of funding would be forthcoming before embarking on a new center.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

</div>

<!--#include file="simple_footer.aspx"-->

