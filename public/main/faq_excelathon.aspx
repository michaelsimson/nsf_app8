﻿


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/other.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation</title>

<!--#include file="incfiles.aspx"-->

</head>
<body>
<!--#include file="/public/main/header.aspx"-->

                <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center"></td>
                          </tr>
                          <tr>
                            <td align="center">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			    <tr>
			    <td align="left">
			       
			        
			    </td>
			    </tr>
			    <!--
			    <tr>
			    <td align="left">
			        <table border="0" align="left" cellpadding="0" cellspacing="0">
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td height="16" align="center" valign="middle" class="btn_05">&nbsp;&nbsp;Follow Us: </td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://twitter.com/" target="_blank"><img src="/public/images/icon_twitter_15x15.gif" alt="Twitter" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.facebook.com/" target="_blank"><img src="../images/icon_facebook_15x15.gif" alt="Facebook" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.youtube.com/" target="_blank"><img src="../images/icon_youtube_35x15.gif" alt="Youtube" width="35" height="15" border="0" /></a>&nbsp;</td>
			        </tr>
			        
			        <tr>
			            <td width="64%"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                <td width="32%" align="center"><a href="http://twitter.com/" target="_blank"><img src="../images/twitter.png" alt="Twitter" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.facebook.com/" target="_blank"><img src="../images/facebook.png" alt="Facebook" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.youtube.com/" target="_blank"><img src="../images/youtube.png" alt="Youtube" width="32" height="32" border="0" /></a></td>
			                </tr>
			            </table></td>
			        </tr>
			        
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        </table>
			    </td>
			    </tr>
			    -->
			    <tr>
			    <td><!--#include file="excelathon_links.aspx"--></td>
			    </tr>
			    <%--<tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="faq_excelathon.aspx" class="btn_02" >Excelathon FAQ</a></td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=cha" class="btn_02" >Chapterwise Report</a> (Top 100)</td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=chl" class="btn_02" >Studentswise Report</a> (Top 100)</td>
			    </tr>
			    <tr>
			    <td align="left" style="padding-left:20px" class="txt01"><a href="excelathon_report.aspx?rpt=cha" class="btn_02" >National Report</a> (Top 100)</td>
			    </tr>--%>
			    </table>

                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td>

<div class="title02">Excelathon - Frequently Asked Questions (FAQ)</div>
<div class="txt01">

<span class="btn_02" align="justify">
<ul>
<a name="top"></a>
<li><a href="#Q1">Why is NSF starting this program?</a></li>
<li><a href="#Q2">How will this money be used?</a></li>
<li><a href="#Q3">Why is NSF asking us to participate in this program?</a></li>
<li><a href="#Q4">Do I have to participate in this program?</a></li>
<li><a href="#Q5">How can we succeed in this program?</a></li>
<li><a href="#Q6">How much money should I raise?</a></li>
<li><a href="#Q7">Is there a minimum donation amount expected from the donors?</a></li>
<li><a href="#Q8">Is the donation tax deductible to the donor?</a></li>
<li><a href="#Q9">How does one make a donation?</a></li>
<li><a href="#Q10">Can the donations be made anonymously?</a></li>
<li><a href="#Q11">Is there any acknowledgement or recognition for the fund-raisers?</a></li>
</ul>
</span>

<hr />

<p><a name="Q1"></a><span class="btn_02">
Why is NSF starting this program?
</span></p>
<p>
With its 23 year history, NSF is looking to further increase its ability to serve more deserving students by providing scholarships. In order to do so, we need assistance from students, parents and everyone who can help grow this mission.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q2"></a><span class="btn_02">
How will this money be used?
</span></p>
<p>
100% of the money goes towards providing educational scholarships to deserving students in India. Please check the NSF website to learn more about our process of screening candidates and disbursing scholarships: <br>
<a class="btn_02" target="_blank" href="http://www.northsouth.org/public/india/schguidelines.aspx">www.northsouth.org/public/india/schguidelines.aspx</a><br>
<a  class="btn_02" target="_blank" href="http://www.northsouth.org/public/india/generalinfo.aspx">www.northsouth.org/public/india/generalinfo.aspx</a>

</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q3"></a><span class="btn_02">
Why is NSF asking us to participate in this program?
</span></p>
<p>
As contestants like you are more familiar with NSF and its mission, we are counting on you to spread the word around to your friends and family members to increase NSF’s profile and encourage participation in NSF's programs.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q4"></a><span class="btn_02">
Do I have to participate in this program?
</span></p>
<p>
No, participation in this program is voluntary. NSF encourages everyone to participate in this program to help its cause and further its mission.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q5"></a><span class="btn_02">
How can we succeed in this program?
</span></p>
<p>
NSF provides plenty of resources to make you successful and the NSF website at <a class="btn_02" target="_blank" href="www.northsouth.org">www.northsouth.org</a> has plenty of information that will help you explain this program to potential donors. 
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q6"></a><span class="btn_02">
How much money should I raise?
</span></p>
<p>
NSF suggests a minimum target of $250, which funds a scholarship for a student for a period of one year.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q7"></a><span class="btn_02">
Is there a minimum donation amount expected from the donors?
</span></p>
<p>
No, there is no minimum donation amount. Any amount will help.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q8"></a><span class="btn_02">
Is the donation tax deductible to the donor?
</span></p>
<p>
Yes, NSF is a 501(c)(3) charitable organization, which means that donations for this program are 100 percent tax deductible to the donor. To obtain a tax receipt for donations exceeding $250, the donor can get a receipt online or contact NSF organizers. The NSF tax identification number is 36-3659998.
When a student is selected for a scholarship, the student is asked to receive the award in person at an awards ceremony, in front of a full audience. NSF makes it a point to announce that all these awards are being given to poor families.
In summary, North South Foundation is sensitive to this issue and is trying its best to ensure that the most deserving students receive the scholarships.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q9"></a><span class="btn_02">
How does one make a donation?
</span></p>
<p>
Donations can be made in several forms. Donors can pay by cash or check directly to you. Alternatively, donations can be mailed to NSF headquarters at 2 Marissa Ct, Burr Ridge, IL 60527.On the check, donors can indicate that they are sponsoring you by writing your name in the memo section of the check.
Credit card payment can be made directly on the NSF website at<br>
<a class="btn_02" target="_blank" href="http://www.northsouth.org/public/main/donate.aspx">www.northsouth.org/public/main/donate.aspx</a>.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q10"></a><span class="btn_02">
Can the donations be made anonymously?
</span></p>
<p>
Yes, we respect your and any donor’s privacy. We encourage you to provide this data so we can recognize significant contributions.  Also we need this information in case of an IRS audit.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>

<p><a name="Q11"></a><span class="btn_02">
Is there any acknowledgement or recognition for the fund-raisers?
</span></p>
<p>
Yes, NSF will acknowledge and recognize the fundraisers, donors, and chapters.
</p>
<p><span class="btn_02"><a href="#top">Top</a></span></p>
</div>

<!--#include file="simple_footer.aspx"-->



