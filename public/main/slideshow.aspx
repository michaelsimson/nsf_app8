<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );

	effect: 'fade',
    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 2000, function() {
            $active.removeClass('active last-active');
        });
}

$(function() {
    setInterval( "slideSwitch()", 3000 );
});

</script>
<style type="text/css">
/*** set the width and height to match your images **/
#slideshow {
    position:relative;
    height:141px;
}

#slideshow IMG {
    position:absolute;
    top:0;
    left:0;
    z-index:8;
    opacity:0.0;
}

#slideshow IMG.active {
    z-index:10;
    opacity:1.0;
}

#slideshow IMG.last-active {
    z-index:9;
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to North South Foundation</title>
</head>
<body>
<table width="912" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="slideshow">
    <img src="/public/images/header_04.jpg" alt="Slideshow Image 1" class="active" />
        <img src="/public/images/header_05.jpg" alt="Slideshow Image 2" />
    <img src="/public/images/img_11.jpg" alt="Slideshow Image 3" />
    <img src="/public/images/header.jpg" alt="Slideshow Image 4" />
    <img src="/public/images/header_01.jpg" alt="Slideshow Image 5" />
    <img src="/public/images/header_02.jpg" alt="Slideshow Image 6" />
    <img src="/public/images/header_03.jpg" alt="Slideshow Image 7" />
</div></td>
  </tr>
</table>
</body>
</html>
