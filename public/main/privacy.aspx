﻿
<!--#include file="simple_header.aspx"-->

<div class="title02">North South Foundation : Privacy Statement</div>

<div class="txt01">
 This privacy statement discloses the privacy practices of the North South Foundation
 having website at the address: <a class="btn_02" href="http://www.northsouth.org">www.northsouth.org</a>

<p><span class="txt01_strong">Information Collection and Use</span><br/>
 North South Foundation is the sole owner of the information collected on this site.
 We will not sell, share, or rent this information to others in ways different from what is disclosed in this statement.
 We collect information from our users at several different points on our website.
</p>

<p><span class="txt01_strong">Registration</span><br/>
 You are not required to complete any registration forms to use our website.
 However, in order to participate in the activities of the Foundation, you need to register.
 You must also register with us to be able to use any restricted access facilities such as our databases,
 reports, or online games.  During registration we collect contact information (such as name and e-mail address.)
 This information is used to contact the user regarding the activities of the Foundation.
 Users might be contacted through phone, e-mail or postal mail as per need. We collect date of birth
 to ensure registrations are valid for participating in its activities. We collect employer information
 for promoting matching gift programs. The information we collect is solely used in support of the activities
 of the Foundation by authorized persons. We do not collect any credit card information at the moment.
</p>

<p><span class="txt01_strong">Cookies</span><br/>
 A cookie is a piece of data stored on the user’s hard drive containing information about the user.
 Usage of a cookie is in no way linked to any personally identifiable information while on our site.
 Once the user closes their browser, the cookie simply terminates.  For general use of our website and
 registration, we do not use any cookies at this point of time.
</p>

<p><span class="txt01_strong">Log Files</span><br/>
 We use IP addresses to analyze trends, administer the site, track user’s movement,
 and gather broad demographic information for aggregate use.  IP addresses are not linked
 to personally identifiable information.
</p>

<p><span class="txt01_strong">Sharing</span><br/>
 We do not share information collected on our website with any third parties.</p>


<p><span class="txt01_strong">Links</span><br/>
 This web site contains links to other sites. Please be aware that we are not responsible for the privacy
 practices of such other sites.  We encourage our users, when they leave our site, to read the privacy
 statements of each and every web site so that they fully understand the individual practices.
 This privacy statement applies solely to information collected by this Web site.
</p>

<p><span class="txt01_strong">Newsletter</span><br/>
 If a user wishes to subscribe to our newsletter, we ask for contact information such as name and e-mail address.</p>

<p><span class="txt01_strong">Security</span><br/>
 We use secure protocol when credit card information is necessary to enter. Credit card information
 is required for making donations or registration payments through our website.
</p>

<p><span class="txt01_strong">Notification of Changes</span><br/>
 We will use information in accordance with the privacy policy stated here. If we decide to change our
 privacy policy, we will post those changes on this page so that our users are always aware of what
 information we collect, how we use it, and under what circumstances.  If at any point we decide to use
 personally identifiable information in a manner different from that stated here, we will notify users
 by way of an e-mail.  Users will have a choice as to whether or not we can use their information in this
 different manner.
</p>

<p>If you have any questions related to the above statement contact web administrator at
<a class="btn_02" href="mailto:admin@northsouth.org">admin@northsouth.org</a></p>
</div>

<!--#include file="simple_footer.aspx"-->

