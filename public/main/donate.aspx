
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/other.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation</title>

<!--#include file="incfiles.aspx"-->

</head>
<body>
<!--#include file="/public/main/header.aspx"-->

                <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			    <tr>
			    <td align="left">
			       
			        
			    </td>
			    </tr>
			    <!--
			    <tr>
			    <td align="left">
			        <table border="0" align="left" cellpadding="0" cellspacing="0">
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td height="16" align="center" valign="middle" class="btn_05">&nbsp;&nbsp;Follow Us: </td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://twitter.com/" target="_blank"><img src="/public/images/icon_twitter_15x15.gif" alt="Twitter" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.facebook.com/" target="_blank"><img src="../images/icon_facebook_15x15.gif" alt="Facebook" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.youtube.com/" target="_blank"><img src="../images/icon_youtube_35x15.gif" alt="Youtube" width="35" height="15" border="0" /></a>&nbsp;</td>
			        </tr>
			        
			        <tr>
			            <td width="64%"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                <td width="32%" align="center"><a href="http://twitter.com/" target="_blank"><img src="../images/twitter.png" alt="Twitter" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.facebook.com/" target="_blank"><img src="../images/facebook.png" alt="Facebook" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.youtube.com/" target="_blank"><img src="../images/youtube.png" alt="Youtube" width="32" height="32" border="0" /></a></td>
			                </tr>
			            </table></td>
			        </tr>
			        
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        </table>
			    </td>
			    </tr>
			    -->
			    <tr>
			    <td>&nbsp;</td>
			    </tr>
			    </table>

                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td>

<div class="title02" align="center">Donate to North South Foundation<br />
Encourage Education - The Gift of a Lifetime</div>

<div class="txt01">
North South Foundation is a non-profit organization focusing on excellence in education.
All contributions made to NSF are tax-deductible. Tax ID: 36-3659998.
While making a donation, you can designate your contribution to the program of your choice.
<hr />
<div class="title04" align="center">Donate by Credit Card</div>
<ul>
<li>Accepted Credit Cards: Mastercard,Visa, American Express, Discover</li>
<li><a class="btn_02" href="https://www.northsouth.org/app9/login.aspx?entry=d">Donate Now</a> to North South Foundation using credit card</li>
</ul>

<hr />
<div class="title04" align="center">Donate by Check</div>
<ul>
<li>Please write on the check, the cause for which the donation is intended (NSF Scholarships in India / NSF Contests in India)</li>
<li>Checks can be mailed to the following address :<br /><br />
    <span class="txt01_strong">North South Foundation</span><br />
    2 Marissa Ct<br />
    Burr Ridge, IL 60527-6864
</li>
</ul>

<hr />
<div class="title04" align="center">Directing Your United Way Contributions</div>
Many employers in the US help raise donations for United Way, which in turn supports hundreds of charities across the country.
Some of the employers contribute funds, matching the employee donations to United Way.
So you may be able to double your contributions to NSF through United Way.
<p>
By directing your United Way contributions to NSF, you can help the financially poor, but meritorious, children in India.
Please provide the following contact name, address, phone number, and Tax ID to your employer for directing your
contributions to NSF through United Way.
</p>
<p>
<span class="txt01_strong">Contact Name:</span> Dr. Ratnam Chitturi
</p>
<p>
<span class="txt01_strong">Address:</span><br />
North South Foundation<br />
2 Marissa Ct<br />
Burr Ridge, IL 60527-6864
</p>
<p><span class="txt01_strong">Phone Number:</span> 630-323-1966</p>

<p><span class="txt01_strong">Tax ID:</span> 36-3659998</p>

<hr />
<div align="center">
Thank you for your generous support in education!<br />
If you have problems in making payment contact <a class="btn_02" href="mailto:admin@northsouth.org">admin@northsouth.org</a> for assistance.
</div>
</div>

<!--#include file="simple_footer.aspx"-->

