 <%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)
        Dim SQL As String
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter
        Dim ds As DataSet = New DataSet
        If Request.QueryString("rpt") Is Nothing Then
            loadChapterwise()
        ElseIf Request.QueryString("rpt") = "cha" Then
            loadChapterwise()
        ElseIf Request.QueryString("rpt") = "chl" Then
            SQL = "select Top 100 ISNULL(Ch.First_Name + ' ' +  Ch.Last_Name,'') as ChName, ISNULL(W.Email,'') as ChEmail,W.WalkMarID,SUM(WM1.Pledged) as Pledged,SUM(WM.Raised) as Raised,W.TargetAmount  from   WalkMarathon W INNER JOIN Chapter C  On W.ChapterID = C.ChapterID Inner JOIN Child Ch On Ch.ChildNumber=W.WMemberID  LEFT JOIN (Select WalkMarID,SUM(ISNULL(PaidAmount,0)) as Raised from WMSponsor WHERE  (PaymentMode ='Credit Card' and PaymentReference is not null) OR NationalApprovalFlag='Y' Group By WalkMarID) WM ON WM.WalkMarID = W.WalkMarID AND W.EventID = 18"
            SQL = SQL & " LEFT JOIN (Select WalkMarID,SUM(ISNULL(PledgeAmount,0)) as Pledged from WMSponsor WHERE  (PaymentMode ='Credit Card' and PaymentReference is not null) OR (PaymentMode <>'Credit Card') Group By WalkMarID) WM1 ON WM1.WalkMarID = W.WalkMarID "
            SQL = SQL & " WHERE W.EventYear = YEAR(GETDATE()) AND C.status='A' "
            'select COUNT(EventId) from Event WHERE Status ='O' AND EventId=1 and EventYear =
            'cn.Open()
            'cmd = New SqlCommand("select COUNT(EventId) from Contestant WHERE  EventId=1 and ContestYear = YEAR(GETDATE())", cn)
            'If cmd.ExecuteScalar() > 0 Then
                SQL = SQL & " AND W.DonateAThonCalID is null "
            'Else
            If Not Request.QueryString("ch") Is Nothing Then
                SQL = SQL & " AND C.ChapterID=" & Request.QueryString("ch")
            End If
            'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
            'cn.Close()
            
            SQL = SQL & " Group by Ch.First_Name + ' ' +  Ch.Last_Name,W.Email ,W.WalkMarID,W.TargetAmount"
            SQL = SQL & " Order By 5 Desc,6 Desc  "
            'Response.Write(SQL)
            
            cn.Open()
            cmd = New SqlCommand(SQL, cn)
            da.SelectCommand = cmd
            'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
            da.Fill(ds)
            cn.Close()
            If ds.Tables(0).Rows.Count > 0 Then
                RptChapterOne.DataSource = ds
                RptChapterOne.DataBind()
                lblErr.Text = ""
            Else
                lblErr.Text = "No Record to Show"
            End If
            
        ElseIf Request.QueryString("rpt") = "chld" Then
            SQL = "select Top 100 CASE WHEN Anonymous='Y' then 'Anonymous' else I.FirstName+' '+I.LastName END as Name,I.Email,CASE WHEN DonAnonymous='Y' then Null else WM.PledgeAmount End as Pledged,WM.Comment,W.TargetAmount from WalkMarathon W LEFT Join WMSponsor WM On W.WalkMarID=WM.WalkMarID Inner Join IndSpouse I ON I.AutoMemberID = WM.MemberID  where ((WM.PaymentMode ='Credit Card' and WM.PaymentReference is not null) OR (WM.PaymentMode <>'Credit Card')) AND W.EventID=18 AND W.EventYear=YEAR(GETDATE()) "
            If Not Request.QueryString("ch") Is Nothing Then
                SQL = SQL & " AND WM.WalkMarID =  " & Request.QueryString("ch")
            End If
            'cn.Open()
            'cmd = New SqlCommand("select COUNT(EventId) from Contestant WHERE  EventId=1 and ContestYear = YEAR(GETDATE())", cn)
            'If cmd.ExecuteScalar() > 0 Then
                SQL = SQL & " AND WM.DonateAThonCalID is null "
            'End If
            'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
            'cn.Close()
            SQL = SQL & " Order by 3 Desc "
            cn.Open()
            cmd = New SqlCommand(SQL, cn)
            da.SelectCommand = cmd
            'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
            da.Fill(ds)
            cn.Close()
            If ds.Tables(0).Rows.Count > 0 Then
                RptChildOne.DataSource = ds
                RptChildOne.DataBind()
                lblErr.Text = ""
            Else
                lblErr.Text = "No Record to Show"
            End If
            
        End If
    End Sub
        
    
    Function encode(ByVal inputText As String) As String
        Dim bytesToEncode As Byte()
        bytesToEncode = Encoding.UTF8.GetBytes(inputText)
        Dim encodedText As String
        encodedText = Convert.ToBase64String(bytesToEncode)
        Return encodedText
    End Function
    
    Private Sub loadChapterwise()
        Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)
        Dim SQL As String
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter
        Dim ds As DataSet = New DataSet
        SQL = "select ISNULL(I.FirstName + ' ' +  I.LastName,'') as CCName, ISNULL(I.HPhone,'') as CCPhone, ISNULL(I.CPhone,'') as CCCPhone, ISNULL(I.Email,'') as CCEmail,C.ChapterCode,C.ChapterID,SUM(WM1.Pledged) as Pledged,SUM(WM.Raised) as Raised,SUM(W.TargetAmount) AS TargetAmount  from  Chapter C LEFT JOIN volunteer V  ON C.ChapterID=V.ChapterID AND V.RoleID=5 LEFT JOIN IndSpouse I On V.MemberID=I.AutoMemberID Inner Join WalkMarathon W On W.ChapterID = C.ChapterID LEFT JOIN (Select WalkMarID,SUM(ISNULL(PaidAmount,0)) as Raised from WMSponsor WHERE  PaymentMode ='Credit Card' and PaymentReference is not null Group By WalkMarID) WM ON WM.WalkMarID = W.WalkMarID AND W.EventID = 18  "
        SQL = SQL & " LEFT JOIN (Select WalkMarID,SUM(ISNULL(PledgeAmount,0)) as Pledged from WMSponsor WHERE  (PaymentMode ='Credit Card' and PaymentReference is not null) OR (PaymentMode <>'Credit Card') Group By WalkMarID) WM1 ON WM1.WalkMarID = W.WalkMarID "
        SQL = SQL & " WHERE W.EventYear = YEAR(GETDATE()) AND C.status='A' AND W.Relationship = 'Child' "
        'cn.Open()
        'cmd = New SqlCommand("select COUNT(EventId) from Contestant WHERE  EventId=1 and ContestYear = YEAR(GETDATE())", cn)
        'If cmd.ExecuteScalar() > 0 Then
            SQL = SQL & " AND W.DonateAThonCalID is null "
        'End If
        'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
        'cn.Close()
        SQL = SQL & " Group by I.FirstName + ' ' +  I.LastName , I.HPhone , I.CPhone , I.Email ,C.ChapterCode,C.ChapterID"
        SQL = SQL & " Order by 8 Desc"
        cn.Open()
        cmd = New SqlCommand(SQL, cn)
        da.SelectCommand = cmd
        'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
        da.Fill(ds)
        cn.Close()
        If ds.Tables(0).Rows.Count > 0 Then
            RptChapter.DataSource = ds
            RptChapter.DataBind()
            lblErr.Text = ""
        Else
            lblErr.Text = "No Record to Show"
        End If
    End Sub

    Protected Sub RptChapterOne_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.FindControl("LinkBtn"), LinkButton)
            '"window.open('http://thomsonreuters.com/products_services/financial/financial_products/corporate_services/','_blank');"
            btn.OnClientClick = "window.open('http://www.northsouth.org/App9/Don_athon_custom.aspx?NSFDONATE=" + encode(e.Item.DataItem("WalkMarID")) + "','_blank');"
            'btn.Attributes.Add("onclick", "return confirm('Coaching End Date has already passed. Do you still want to change?');")
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/other.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation</title>

<!--#include file="incfiles.aspx"-->

</head>
<body>
<!--#include file="/public/main/header.aspx"-->

                <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center"></td>
                          </tr>
                          <tr>
                            <td align="center">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			    <tr>
			    <td align="left">
			       
			        
			    </td>
			    </tr>
			    <!--
			    <tr>
			    <td align="left">
			        <table border="0" align="left" cellpadding="0" cellspacing="0">
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td height="16" align="center" valign="middle" class="btn_05">&nbsp;&nbsp;Follow Us: </td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://twitter.com/" target="_blank"><img src="/public/images/icon_twitter_15x15.gif" alt="Twitter" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.facebook.com/" target="_blank"><img src="../images/icon_facebook_15x15.gif" alt="Facebook" width="15" height="15" border="0" /></a>&nbsp;</td>
			            <td height="16" align="center" valign="middle" class="btn_05"><a href="http://www.youtube.com/" target="_blank"><img src="../images/icon_youtube_35x15.gif" alt="Youtube" width="35" height="15" border="0" /></a>&nbsp;</td>
			        </tr>
			        
			        <tr>
			            <td width="64%"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                <td width="32%" align="center"><a href="http://twitter.com/" target="_blank"><img src="../images/twitter.png" alt="Twitter" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.facebook.com/" target="_blank"><img src="../images/facebook.png" alt="Facebook" width="32" height="32" border="0" /></a></td>
			                <td width="32%" align="center"><a href="http://www.youtube.com/" target="_blank"><img src="../images/youtube.png" alt="Youtube" width="32" height="32" border="0" /></a></td>
			                </tr>
			            </table></td>
			        </tr>
			        
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        </table>
			    </td>
			    </tr>
			    -->
			    <tr>
			    <td>
			    <!--#include file="excelathon_links.aspx"-->
			    
			    </td>
			    </tr>
			    
			    </table>

                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" style="height :400px" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td valign="top">

<div class="title02" align="center">Excel-a-thon<br />
    Encourage Education - The Gift of a Lifetime</div>

<div align="center" class="txt01">
    <asp:Repeater ID="RptChapter" runat="server">
   
    <HeaderTemplate>
  <table class="text1"  cellspacing="0" cellpadding="2" bordercolor="#337816" border="1">
   <tr>
     <td class="title"> Chapter </td>
      <td  class="title"> Chapter Info </td>
       <td  class="title"> Target </td>
        <td  class="title"> Raised </td></tr>
     
  </HeaderTemplate>
          <ItemTemplate>
     <tr>   
     <td> <a class="btn_02" href='excelathon_report.aspx?rpt=chl&ch=<%#DataBinder.Eval(Container.DataItem, "ChapterID")%>'> <%#DataBinder.Eval(Container.DataItem, "ChapterCode")%> </a> </td>
     <td align="left" ><%#DataBinder.Eval(Container.DataItem, "CCName")%><br />
      <%#DataBinder.Eval(Container.DataItem, "CCPhone")%> &nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "CCCPhone")%><br />
      <%#DataBinder.Eval(Container.DataItem, "CCEmail")%> </td>    
      <td align="Right"><%#DataBinder.Eval(Container.DataItem, "TargetAmount", "{0:c}")%> </td>
       <td><%#DataBinder.Eval(Container.DataItem, "Raised", "{0:c}")%> </td>         
      </tr>      
  </ItemTemplate>
  <FooterTemplate>
 </table>
 </FooterTemplate>
    </asp:Repeater>
        <form id="form1" runat="server">
     <asp:Repeater ID="RptChapterOne" runat="server" OnItemDataBound="RptChapterOne_ItemDataBound">
   
    <HeaderTemplate>
  <table class="text1"  cellspacing="0" cellpadding="2" bordercolor="#337816" border="1">
   <tr>
     <td  class="title"> Child Name </td>
     
     <%-- <td style="background-color:#CCCC99"> Contact Info </td>--%>
       <td  class="title"> Target </td>
        <td  class="title"> Raised </td></tr>
     
  </HeaderTemplate>
          <ItemTemplate>
     <tr>   
     <td> 
     
<%--     <a class="btn_02" href='excelathon_report.aspx?rpt=chld&ch=<%#DataBinder.Eval(Container.DataItem, "WalkMarID")%>'> <%#DataBinder.Eval(Container.DataItem, "ChName")%> </a> --%>
    <asp:LinkButton id="LinkBtn" CssClass="btn_02" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChName")%>'  ></asp:LinkButton></td>
     <%--<td align="left" ><%#DataBinder.Eval(Container.DataItem, "ChEmail")%> </td>   --%> 
      <td><%#DataBinder.Eval(Container.DataItem, "TargetAmount", "{0:c}")%> </td>
       <td><%#DataBinder.Eval(Container.DataItem, "Raised", "{0:c}")%> </td>         
      </tr>      
  </ItemTemplate>
  <FooterTemplate>
 </table>
 </FooterTemplate>
    </asp:Repeater>
    </form> 
    <asp:Repeater ID="RptChildOne" runat="server">
   
    <HeaderTemplate>
  <table class="text1" cellspacing="0" cellpadding="2" bordercolor="#337816" border="1">
   <tr>
     <td  class="title"> Patron Name </td>
      <td  class="title"> Email </td>
       <td  class="title"> Pledged </td>
        <td  class="title"> Comments </td></tr>
     
  </HeaderTemplate>
          <ItemTemplate>
     <tr>   
     <td> <%#DataBinder.Eval(Container.DataItem, "Name")%> </td>
     <td align="left" ><%#DataBinder.Eval(Container.DataItem, "Email")%> </td>    
      <td><%#DataBinder.Eval(Container.DataItem, "Pledged", "{0:c}")%> </td>
       <td><%#DataBinder.Eval(Container.DataItem, "Comment")%> </td>         
      </tr>      
  </ItemTemplate>
  <FooterTemplate>
 </table>
 </FooterTemplate>
    </asp:Repeater>
    <asp:Label ID="lblErr" runat="server" ForeColor = "Red" ></asp:Label>
</div>


<!--#include file="simple_footer.aspx"-->

