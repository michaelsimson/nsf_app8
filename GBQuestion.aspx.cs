﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
public partial class GBQuestion : System.Web.UI.Page
{
    DataSet ds;
    string DRORAVl;
    string DRORAV2;
    string ConnectionString = "ConnectionString";
    bool Choices = false;
    DateTime dateTime;
    bool DateChoice = true;
    bool boolSearch = false;
    string Sortingval;
    string SearchWhere;
           
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            {
                Response.Redirect("~/login.aspx?entry=p");
            }
           
            if (!IsPostBack)
            {
                Txtpreparer.Text = "";
                string nameQry = "select Firstname +' '+lastname from indspouse where automemberid="+Session["LoginID"]+"";
                object Name = SqlHelper.ExecuteScalar(Application[ConnectionString].ToString(), CommandType.Text, nameQry);
                if (Name!=null)
                {
                    Txtpreparer.Text = Name.ToString();
                }
                Yearscount();
                Category();
                QuestionGrid();
                DDAnswer();
            }
        }
        catch(Exception ex)
        {
            SessionExp();
        }
        
    }
    private void SessionExp()
    {
        if (Session["LoggedIn"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
    }

    protected void Yearscount()
    {
        try
        {
            DDListYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();
           
                for (int i = 2000; i <= MaxYear+5; i++)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
                DDListYear.DataSource = list;
                DDListYear.DataTextField = "Text";
                DDListYear.DataValueField = "Value";
                DDListYear.DataBind();
                DDListYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            SessionExp();
        }
       
    }
    protected void DDAnswer()
    {
        ArrayList list = new ArrayList();
        try
        {
            for (int i = 65; i < 65 + Convert.ToInt32(DDOddChoice.SelectedItem.Value); i++)
            {
                list.Add(new ListItem(((char)i).ToString(), i.ToString()));
            }
        }
        catch (Exception ex)
        {

        }
        DDLetterAnswer.DataSource = list;
        DDLetterAnswer.DataTextField = "Text";
        DDLetterAnswer.DataValueField = "Value";
        DDLetterAnswer.DataBind();
        DDLetterAnswer.Items.Insert(0, new ListItem("Select LetterAnswer", "0"));
    }
    protected void Category()
    {
        try
        {
        string DDLCategory = "select distinct category from Rules";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLCategory);
        DDListcategory.DataSource = ds;
        DDListcategory.DataTextField = "category";
        DDListcategory.DataBind();
        DDListcategory.Items.Insert(0, "Select Category");
         }
        catch
        {
            SessionExp();
        }
    }
    protected void FalseEnable()
    {
        TxtchoiceA.Enabled = false;
        TxtchoiceB.Enabled = false;
        TxtchoiceC.Enabled = false;
        TxtchoiceD.Enabled = false;
        TxtchoiceE.Enabled = false;
    }
    protected void EnableFunction()
    {
        try
        {
            FalseEnable();
            if (DDMultichoice.SelectedItem.Text == "Y")
            {
                for (int i = 65; i < 65 + Convert.ToInt32(DDOddChoice.SelectedItem.Value); i++)
                {
                    string str = "Txtchoice" + ((char)i).ToString();
                    EnableTextbox(str);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void EnableTextbox(string TEXTEnable)
    {
        TextBox text = (TextBox)table1.FindControl(TEXTEnable);
        text.Enabled = true;
    }
    protected void QuestionGrid()
    {
        try
        {
     
        if ((boolSearch == true) && (DDSearch.SelectedValue != "0"))
        {
            if ((DDSearch.SelectedValue == "5") || (DDSearch.SelectedValue == "9"))
            {
                SearchWhere = " WHERE   convert(date," + DDSearch.SelectedItem.Text + "    )between '" + TxtFrom.Text + "'  and  '" + TxtTo.Text + "' ";
            }
            else if (DDSearch.SelectedValue=="1")
            {
                SearchWhere = " WHERE ID like '%" + TxtSearch.Text + "%' or YEAR like '%" + TxtSearch.Text + "%' or Source like  '%" + TxtSearch.Text + "%' or " +
                             "SourceDate like  '%" + TxtSearch.Text + "%' or PersonEntering like'%" + TxtSearch.Text + "%' or Question like'%" + TxtSearch.Text
                             + "%' or Answer like  '%" + TxtSearch.Text + "%' " +
                             "or CreateDate like '%" + TxtSearch.Text + "%' or createdby like'%" + TxtSearch.Text + "%' ";
            }
            else if (DDSearch.SelectedValue == "10")
            {
                string Str = TxtSearch.Text;
                Int32 aVal;
                if (Int32.TryParse(Str, out aVal))
                {
                    SearchWhere = " WHERE " + DDSearch.SelectedItem.Text + " like '%" + TxtSearch.Text + "%'";
                }
                else
                {
                    SearchWhere = "G left join indspouse I   on I.automemberid=G.createdby WHERE I.Firstname +' '+I.lastname like  '%" + TxtSearch.Text + "%'";
                }
            }
            else
            {
                SearchWhere = " WHERE " + DDSearch.SelectedItem.Text + " like '%" + TxtSearch.Text + "%'";
            }
        }
        else
        {
            SearchWhere = "";
        }
        if(DDSort.SelectedValue=="0")
        {
            if ((DDSearch.SelectedValue == "5") || (DDSearch.SelectedValue == "9"))
            {
                Sortingval = "  order by " + DDSearch.SelectedItem.Text + " desc";
            }
            else
            {
                Sortingval = "  order by ID desc";
            }
        }
        else if ((DDSort.SelectedValue == "3") && (DDSearch.SelectedValue == "5"))
        {
            Sortingval=" order by "  + DDSort.SelectedItem.Text +"," +DDSearch.SelectedItem.Text+  "  desc";
        }

       
        else
        {
            Sortingval=" order by "  + DDSort.SelectedItem.Text +"  asc";
        }

        string QuestionQry = "Select Id,Year,convert(date, SourceDate) as SourceDate,Source,SeqNo,Category,levelNo,SublevelNo," +
              "  Question,MultipleChoice,choices,[A],[B],[C],[D],[E],Answer,LetterAnswer," +
              "Duplicate,Audio,Video,Link,assign,page,PersonEntering from GBQ   " + SearchWhere + "  " + Sortingval + "";
        //Response.Write(QuestionQry);
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, QuestionQry);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            lbsearchresults.Visible = true;
            lbsearchresults.Text = "No Search found ";
        }
        else
        {
            lbsearchresults.Visible = false;
        }
        Session["GridQuestion"]= ds;
        GridQuesDisplay.DataSource = ds;
        GridQuesDisplay.DataBind();
        }
        catch
        {
            SessionExp();
        }
    }
    protected void DDSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
        QuestionGrid();
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridQuesDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
        GridQuesDisplay.PageIndex = e.NewPageIndex;
        GridQuesDisplay.DataBind();
        }
        catch
        {
            SessionExp();
        }
    }
    
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            TextboxFunction();
            string StrDropDown;
            LByearDateValid.Text = "";
            if (DDLetterAnswer.SelectedItem.Text == "Select LetterAnswer")
            {
                if (DDMultichoice.SelectedItem.Text == "Y")
                {
                    LbAsnw.Visible = true;
                }
            }
            if (DDListcategory.SelectedItem.Text == "Select Category")
            {
                Lbcatval.Visible = true;
            }
            if (DDListYear.SelectedItem.Text == "Select Year")
            {
                Lbyearval.Visible = true;
            }
            dateandYearVAl();
            Choicesvalidation();
            ChoicesFunction();
          
            if ((!string.IsNullOrEmpty(TextSouce.Text)) && (Choices != true) && ((DDListcategory.SelectedItem.Text != "Select Category")
                && (!string.IsNullOrEmpty(TxtSeq.Text)) && (!string.IsNullOrEmpty(TxtAnswer.Text)) && (DDListYear.SelectedItem.Text != "Select Year") && (DDListYear.SelectedItem.Text == dateTime.Year.ToString())
                 && (DDListLevel.SelectedItem.Text != "Select Level") && (DateChoice!=false)
                && (DDLIstSublevel.SelectedItem.Text != "Select Sub-Level") && (!string.IsNullOrEmpty(TxtQuestion.Text))
               ))
            {
                AddFunction();
                QuestionGrid();
                LByearDateValid.Text = "";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void ChoicesFunction()
    {
        EnableFunction();
        switch (DDOddChoice.SelectedItem.Text)
        {
            case "1":
                if (string.IsNullOrEmpty(TxtchoiceA.Text))
                {
                    LabelQuest.Visible = true;
                    Choices = true;
                }
                TxtchoiceA.Enabled = true;
                break;
            case "2":
                if (string.IsNullOrEmpty(TxtchoiceA.Text) || string.IsNullOrEmpty(TxtchoiceB.Text))
                {
                    LabelQuest.Visible = true;
                    Choices = true;
                }
                
                break;
            case "3":
                if (string.IsNullOrEmpty(TxtchoiceA.Text) || string.IsNullOrEmpty(TxtchoiceB.Text) || string.IsNullOrEmpty(TxtchoiceC.Text))
                {
                    LabelQuest.Visible = true;
                    Choices = true;
                }
                break;
            case "4":
                if (string.IsNullOrEmpty(TxtchoiceA.Text) || string.IsNullOrEmpty(TxtchoiceB.Text) || string.IsNullOrEmpty(TxtchoiceC.Text) || string.IsNullOrEmpty(TxtchoiceD.Text))
                {
                    LabelQuest.Visible = true;
                    Choices = true;
                }
                 TxtchoiceD.Enabled = true;
                break;
            case "5":
                if (string.IsNullOrEmpty(TxtchoiceA.Text) || string.IsNullOrEmpty(TxtchoiceB.Text) || string.IsNullOrEmpty(TxtchoiceC.Text) || string.IsNullOrEmpty(TxtchoiceD.Text) || string.IsNullOrEmpty(TxtchoiceE.Text))
                {
                    LabelQuest.Visible = true;
                    Choices = true;
                }
                break;
            default:
               
                break;
        }
    }
    protected void AddFunction()
    {
        try
        {
            ValidForAddandUpdate();
            DDropChoices();
            if (DDMultichoice.SelectedValue == "1")
            {
                if ((DDOddChoice.SelectedValue != "0") && (DDLetterAnswer.SelectedItem.Text != "Select LetterAnswer"))
                {
                    Query();
                    FalseEnable();
                }
            }
            else
            {
                Query();
                DDOddChoice.SelectedItem.Text = "Select Choice";
            }
           
        }
        catch (Exception ex)
        {
            Response.Write("Admin Error");
        }
   }
    protected void dateandYearVAl()
    {
        if((!string.IsNullOrEmpty(TxtSource.Text)) && (DDListYear.SelectedItem.Text != "Select Year"))
        {
        dateTime = DateTime.Parse(TxtSource.Text);
          DateTime  date1 = Convert.ToDateTime(TxtSource.Text);
          DateTime date2 = DateTime.Today;
          if (date1> date2)
        {
            LByearDateValid.Visible = true;
            LByearDateValid.Text = "Future dates are not allowed";
            DateChoice = false;
        }
        else if (DDListYear.SelectedItem.Text != dateTime.Year.ToString())
        {
            LByearDateValid.Visible = true;
            LByearDateValid.Text = "Year and Source date should have the same “Year” value";
            DateChoice = false;
        }
          
        }
    }
    protected void Query()
    {
        string SqlAddQry = "Insert into GBQ (Year,SourceDate,Source,SeqNo,page,Category,levelNo,SublevelNo,MultipleChoice,CreatedBy,CreateDate," +
        "Question,[A],[B],Answer,LetterAnswer,[C],[D],[E],Assign,Review1,Review2,Duplicate,Audio,Video,Link,choices,PersonEntering) values(" + DDListYear.SelectedItem.Text + ",'"
            +TxtSource.Text + "','" + TextSouce.Text +
         "'," + TxtSeq.Text + ",'" + Txtpage.Text + "','" + DDListcategory.SelectedItem.Text + "'," + DDListLevel.SelectedItem.Text +
         "," + DDLIstSublevel.SelectedItem.Text + ",'" + DDMultichoice.SelectedItem.Text +
         "'," + Session["loginID"] + ",Getdate(),'" + TxtQuestion.Text + "','" + TxtchoiceA.Text + "','" + TxtchoiceB.Text +
         "','" + TxtAnswer.Text + "','" + DDLetterAnswer.SelectedItem.Text + "','" + TxtchoiceC.Text + "','" + TxtchoiceD.Text + "','" + TxtchoiceE.Text +
         "','" + TxtAssigned.Text + "','" + TxtRev1.Text + "','" + TxtReview2.Text +
         "','" + DDListDuplicate.SelectedItem.Text + "','" + TxtAudio.Text + "','" + TxtVideo.Text + "','" + TxtLink.Text + "'," + DDOddChoice.SelectedItem.Text + ",'" + Txtpreparer.Text + "')";
        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, SqlAddQry);
        Response.Write("<script>alert('Added successfully')</script>");
        LabelQuest.Visible = false;
        LabelQuest.Visible = false;
       
        cancel();
    }
    protected void ValidForAddandUpdate()
    {
        if (string.IsNullOrEmpty(TxtSource.Text))
        {
            TxtSource.Text = null;
        }
        if (string.IsNullOrEmpty(Txtpage.Text))
        {
            Txtpage.Text = null;
        }
        if (string.IsNullOrEmpty(Txtpage.Text))
        {
            Txtpage.Text = null;
        }
        if ((TxtchoiceA.Enabled == false) || (string.IsNullOrEmpty(TxtchoiceA.Text)))
        {
            TxtchoiceA.Text = null;
        }

        if ((TxtchoiceB.Enabled == false) || (string.IsNullOrEmpty(TxtchoiceB.Text)))
        {
            TxtchoiceB.Text = null;
        }
        if ((TxtchoiceC.Enabled == false) || (string.IsNullOrEmpty(TxtchoiceC.Text)))
        {
            TxtchoiceC.Text = null;
        }
        if ((TxtchoiceD.Enabled == false) || (string.IsNullOrEmpty(TxtchoiceD.Text)))
        {
            TxtchoiceD.Text = null;
        }
        if ((TxtchoiceE.Enabled == false) || (string.IsNullOrEmpty(TxtchoiceE.Text)))
        {
            TxtchoiceE.Text = null;
        }
        if (string.IsNullOrEmpty(TxtAssigned.Text))
        {
            TxtAssigned.Text = null;
        }
        if (string.IsNullOrEmpty(TxtRev1.Text))
        {
            TxtRev1.Text = null;
        }
        if (string.IsNullOrEmpty(TxtReview2.Text))
        {
            TxtReview2.Text = null;
        }
        if (string.IsNullOrEmpty(TxtAudio.Text))
        {
            TxtAudio.Text = null;
        }
        if (string.IsNullOrEmpty(TxtVideo.Text))
        {
            TxtVideo.Text = null;
        }
        if (string.IsNullOrEmpty(TxtLink.Text))
        {
            TxtLink.Text = null;
        }
        if (string.IsNullOrEmpty(Txtpreparer.Text))
        {
            Txtpreparer.Text = null;
        }
        if (DDMultichoice.SelectedItem.Text == "N")
        {
            DDOddChoice.SelectedItem.Text = "NULL";
            DDLetterAnswer.SelectedItem.Text = "";
        }
    }
    protected void GridQuesDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            cancel();
            Btnupdate.Enabled = true;
            BtnAdd.Enabled = false;
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GridQuesDisplay.Rows[index];

            GridQuesDisplay.Rows[index].BackColor = System.Drawing.Color.SkyBlue;

            for (int i = 0; i <= GridQuesDisplay.Rows.Count - 1; i++)
            {
                if (i != index)
                {
                    GridQuesDisplay.Rows[i].BackColor = System.Drawing.Color.White;
                }
            }
            TxtID.Text = row.Cells[1].Text.Replace("&nbsp;", "");
            DDListYear.SelectedItem.Text = row.Cells[2].Text.Replace("&nbsp;", "Select Year");
            TxtSource.Text = row.Cells[3].Text.Replace("&nbsp;", "");
            TextSouce.Text = row.Cells[4].Text.Replace("&nbsp;", "");
            TxtSeq.Text = row.Cells[5].Text.Replace("&nbsp;", "");
            DDListcategory.SelectedItem.Text = row.Cells[6].Text.Replace("&nbsp;", "Select Category");
            if ((row.Cells[7].Text != "&nbsp;") && (row.Cells[7].Text.Length > 0))
            {
                DDListLevel.SelectedIndex = Convert.ToInt16(row.Cells[7].Text);
            }
            if (row.Cells[8].Text != "&nbsp;")
            {
                DDLIstSublevel.SelectedIndex = Convert.ToInt16(row.Cells[8].Text);
            }
            TxtQuestion.Text = row.Cells[9].Text.Replace("&nbsp;", "");
            if ((row.Cells[10].Text == "Y"))
            {
                DDMultichoice.SelectedIndex = 1;
            }
            else
            {
                DDMultichoice.SelectedIndex = 2;
            }

            if ((row.Cells[11].Text != "&nbsp;") && (row.Cells[11].Text != "0") && (row.Cells[11].Text.Length > 0))
            {
                DDOddChoice.SelectedIndex = Convert.ToInt16(row.Cells[11].Text);
            }
            TxtchoiceA.Text = row.Cells[12].Text.Replace("&nbsp;", "");
            TxtchoiceB.Text = row.Cells[13].Text.Replace("&nbsp;", "");
            TxtchoiceC.Text = row.Cells[14].Text.Replace("&nbsp;", "");
            TxtchoiceD.Text = row.Cells[15].Text.Replace("&nbsp;", "");
            TxtchoiceE.Text = row.Cells[16].Text.Replace("&nbsp;", "");
            DDAnswer();
            if (row.Cells[18].Text.Trim().Length > 0)
            {
                char charct = Convert.ToChar(row.Cells[18].Text);
                int i = (int)charct;
                if (DDLetterAnswer.Items.Count>1)
                {
                    DDLetterAnswer.SelectedValue = i.ToString();
                }
            }
            TxtAnswer.Text = row.Cells[17].Text.Replace("&nbsp;", "");
            if ((row.Cells[19].Text == "Y") && (row.Cells[19].Text != "&nbsp;") && (row.Cells[19].Text.Length > 1))
            {
                DDListDuplicate.SelectedIndex = 1;
            }
            else
            {
                DDListDuplicate.SelectedIndex = 2;
            }
            TxtAudio.Text = row.Cells[20].Text.Replace("&nbsp;", "");
            TxtVideo.Text = row.Cells[21].Text.Replace("&nbsp;", "");
            TxtLink.Text = row.Cells[22].Text.Replace("&nbsp;", "");
           // Txtpreparer.Text = "";
            Txtpreparer.Enabled = false;
            //Txtpreparer.Text = row.Cells[23].Text.Replace("&nbsp;", "");
            TxtAssigned.Text = row.Cells[24].Text.Replace("&nbsp;", "");
            Txtpage.Text = row.Cells[25].Text.Replace("&nbsp;", "");
            MultichoiceCondition();
           
        }
        catch (Exception ex)
        {

        }
    }
    protected void DDMultichoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultichoiceCondition();
    }
    protected void MultichoiceCondition()
    {
        if (DDMultichoice.SelectedItem.Text == "Y")
        {
            DDOddChoice.Enabled = true;
            DDLetterAnswer.Enabled = true;
            Choicesvalidation();
            TxtAnswer.Enabled = false;
        }
        else
        {
            textboxClear();
            DDOddChoice.SelectedIndex = 0;
            DDOddChoice.Enabled = false;
            Lblvalid.Visible = false;
            DDLetterAnswer.Enabled = false;
            Choicesvalidation();
            EnableFunction();
            DDAnswer();
            TxtAnswer.Enabled = true;
        }
    }
    protected void Choicesvalidation()
    {
        if (DDMultichoice.SelectedItem.Text == "Y")
        {
            DDOddChoice.Enabled = true;
            if (DDOddChoice.SelectedItem.Value == "0")
            {
                Lblvalid.Visible = true;
            }
            else
            {
                ChoicesFunction();
            }
        }
    }
    protected void DDOddChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (DDOddChoice.SelectedItem.Value != "0")
            {
                Lblvalid.Visible = false;
                LbAsnw.Visible = true;
            }
            else
            {
                FalseEnable();
            }
            Choicesvalidation();
            DDAnswer();
            if (DDMultichoice.SelectedItem.Text == "Y")
            {
                DDLetterAnswer.Enabled = true;
            }
            else
            {
                textboxClear();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void DDLetterAnswer_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextboxFunction();
    }
    protected void TextboxFunction()
    {
        if (DDLetterAnswer.SelectedItem.Text != "Select LetterAnswer")
        {
            LbAsnw.Visible = false;
            string Textboxname = "Txtchoice" + DDLetterAnswer.SelectedItem.Text;
            TextBox textval = (TextBox)table1.FindControl(Textboxname);
            TxtAnswer.Text = textval.Text;

        }
    }
    protected void clear()
    {
        TxtSource.Text = "";
        TextSouce.Text = "";
        textboxClear();
        TxtSeq.Text = "";
        Txtpage.Text = "";
        TxtAssigned.Text = "";
        TxtRev1.Text = "";
        TxtReview2.Text = "";
        TxtAudio.Text = "";
        TxtVideo.Text = "";
        TxtLink.Text = "";
        TxtAnswer.Text = "";
    }
    protected void textboxClear()
    {
        TxtchoiceA.Text = "";
        TxtchoiceB.Text = "";
        TxtchoiceC.Text = "";
        TxtchoiceD.Text = "";
        TxtchoiceE.Text = "";
       
    }
    protected void DDListcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDListcategory.SelectedItem.Text != "Select Category")
        {
            Lbcatval.Visible = false;
        }
    }
    protected void DDListYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDListYear.SelectedItem.Text != "Select Year")
        {
            Lbyearval.Visible = false;
        }

    }
    protected void DDropChoices()
    {
        if (DDMultichoice.SelectedItem.Text == "N")
        {
            DDOddChoice.SelectedItem.Text = "0";
            DDLetterAnswer.SelectedItem.Text ="";
        }
    }
    protected void Btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            TextboxFunction();
            ValidForAddandUpdate();
            Choicesvalidation();
            ChoicesFunction();
            dateandYearVAl();
            if (DDMultichoice.SelectedItem.Text == "N")
            {
                DDOddChoice.SelectedItem.Text = "Select Choice";
            }
            if ((!string.IsNullOrEmpty(TextSouce.Text)) && (Choices != true) && ((DDListcategory.SelectedItem.Text != "Select Category")
                   && (!string.IsNullOrEmpty(TxtSeq.Text)) && (!string.IsNullOrEmpty(TxtAnswer.Text)) && (DDListYear.SelectedItem.Text != "Select Year")
                    && (DDListLevel.SelectedItem.Text != "Select Level") && (DateChoice != false)
                   && (DDLIstSublevel.SelectedItem.Text != "Select Sub-Level") && (!string.IsNullOrEmpty(TxtQuestion.Text))
                  ))
            {
                DDropChoices();
                string SqlAddQry = "update  GBQ set Year=" + DDListYear.SelectedItem.Text + ",SourceDate='" + TxtSource.Text + "',Source='" + TextSouce.Text +
               "',SeqNo=" + TxtSeq.Text + ",page='" + Txtpage.Text + "',Category='" + DDListcategory.SelectedItem.Text + "',levelNo=" + DDListLevel.SelectedItem.Text +
               ",SublevelNo=" + DDLIstSublevel.SelectedItem.Text + ",MultipleChoice='" + DDMultichoice.SelectedItem.Text +
               "',Question='" + TxtQuestion.Text + "',ModifiedBy=" + Session["loginID"] + ",ModifiedDate=Getdate(),[A]='" + TxtchoiceA.Text + "',[B]='" + TxtchoiceB.Text +
               "',Answer='" + TxtAnswer.Text + "',LetterAnswer='" + DDLetterAnswer.SelectedItem.Text + "',[C]='" + TxtchoiceC.Text + "',[D]='" + TxtchoiceD.Text + "',[E]='" + TxtchoiceE.Text +
               "',Assign='" + TxtAssigned.Text + "',Review1='" + TxtRev1.Text + "',Review2='" + TxtReview2.Text +
               "',Duplicate='" + DDListDuplicate.SelectedItem.Text + "',Audio='" + TxtAudio.Text + "',Video='" + TxtVideo.Text + "',Link='" + TxtLink.Text + "',choices=" + DDOddChoice.SelectedItem.Text + "  where ID=" + TxtID.Text + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, SqlAddQry);
                Response.Write("<script>alert('Updated successfully')</script>");
                LabelQuest.Visible = false;
                LabelQuest.Visible = false;
                cancel();
                QuestionGrid();
                DDOddChoice.SelectedItem.Text = "Select Choice";
                LByearDateValid.Text = "";
            }
            
         }
        catch (Exception ex)
        {

            Response.Write("Admin Error");
        }

        }

    protected void GridQuesDisplay_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridQuesDisplay.PageIndex = e.NewPageIndex;
        GridQuesDisplay.DataSource =Session["GridQuestion"];
        GridQuesDisplay.DataBind();
        cancel();
        }
        catch
        {
            SessionExp();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cancel();
    }
    protected void cancel()
    {
        clear();
        DDLIstSublevel.SelectedIndex = 0;
        DDListLevel.SelectedIndex = 0;
        DDMultichoice.SelectedIndex = 0;
        DDOddChoice.SelectedIndex = 0;
       DDAnswer();
        TxtAnswer.Text = "";
        TxtID.Text = "";
      
        Yearscount();
        Category();
        TxtQuestion.Text = "";
        TxtchoiceA.Text = "";
        TxtchoiceB.Text = "";
        TxtchoiceC.Text = "";
        TxtchoiceD.Text = "";
        TxtchoiceE.Text = "";
        BtnAdd.Enabled = true;
        Btnupdate.Enabled = false;
        LByearDateValid.Text = "";
        LabelQuest.Text = "";
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        boolSearch = true;
        QuestionGrid();
        

    }
    protected void DDSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        TxtSearch.Text = "";
        TxtFrom.Text = "";
        TxtTo.Text = "";
        if ((DDSearch.SelectedValue == "5") || (DDSearch.SelectedValue == "9"))
        {
            PnlDate.Visible = true;
            Divdatesearch.Visible = true;
            TxtSearch.Visible = false;
        }
        else
        {
            Divdatesearch.Visible = false;
            TxtSearch.Visible = true;
        }
    }
    protected void BtnsearchClear_Click(object sender, EventArgs e)
    {
        DDSort.SelectedIndex = 0;
        DDSearch.SelectedIndex = 0;
        TxtSearch.Text = "";
        PnlDate.Visible = false;
        QuestionGrid();

    }
}

