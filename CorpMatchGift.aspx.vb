﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.UI


Partial Class CorpMatchGift
    Inherits System.Web.UI.Page
    Dim PaymentID As String
    Dim Connectionstring As String = "Connectionstring"
    Dim boolNo As Boolean = True
    Dim ACHbool As Boolean = False
    'Modifyied by Ahila
    'Date 18/10/2014

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("LoginID") = 4240
        Session("LoggedIn") = "true"
        Session("EntryToken") = "VOLUNTEER"
        Session("RoleId") = 1





        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Server.Transfer("login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Or (Session("RoleId").ToString() = "92") Then

                If SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select count(VolunteerId) from Volunteer where roleid =" & Session("RoleId") & " and memberid=" & Session("LoginID") & " and [National]='Y' ") < 1 Then
                    Server.Transfer("VolunteerFunctions.aspx")
                End If
            Else
                Server.Transfer("maintest.aspx")
            End If
            Session("PaymentID") = ""
            lblerr.Text = ""

            lblFlag.Text = "False"
            LoadmatchingURL()
            LoadPayYear()
        End If

    End Sub
    Private Sub ExportToExcel(ByVal dtset As GridView, ByVal Strname As String)
        Dim TempGrid As New GridView
        ' Dim Filename As String = GetData(TempGrid)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Strname & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        TempGrid.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    'Protected Sub btnOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOrg.Click
    '    cleargrids()
    '    lblerr.Text = ""
    '    pORGSearch.Visible = True
    '    txtOrgName.Text = ""
    '    txtCity.Text = ""
    '    LoadStates(ddlOrgState)
    '    LoadChapter(ddlNSFChapter)
    'End Sub
    Private Sub LoadmatchingURL()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, "select AutoMemberID,ORGANIZATION_NAME from organizationinfo where   ORGANIZATION_NAME is not null and  Matching_Gift='Yes' order by ORGANIZATION_NAME asc")
        DDMatchURL.DataSource = ds
        If ds.Tables(0).Rows.Count > 0 Then
            DDMatchURL.DataTextField = "ORGANIZATION_NAME"
            DDMatchURL.DataValueField = "AutoMemberID"
            DDMatchURL.DataBind()
            DDMatchURL.Items.Insert(0, New ListItem("Organization Name", 0))
        Else
            DDMatchURL.Items.Insert(0, New ListItem("Organization Name", 0))
        End If
    End Sub
    Protected Sub SearchMemberID(ByVal sender As Object, ByVal e As System.EventArgs)

        lblerr.Text = ""

        PDonations.Visible = False
        Panel1.Visible = False

        ' FillDonation(DDMatchURL.SelectedValue)
        pIndSearch.Visible = True
        lblIndSearch.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtEmail.Text = ""
        LoadStates(ddlState)

    End Sub
    Private Sub LoadPayYear()
        Dim Year As Integer = Now.Year
        For i As Integer = 0 To 9
            ddlPayYear.Items.Insert(i, New ListItem(Year - i, Year - i))

        Next
        ddlPayYear.Items.Insert(0, New ListItem("Year", 0))
    End Sub

    Private Sub LoadStates(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application(Connectionstring))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlst.Items.Insert(0, New ListItem("Select State", "0"))
        ddlst.SelectedIndex = 0
    End Sub
    Private Sub LoadChapter(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application(Connectionstring))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "select chapterID,chaptercode from chapter where Status='A' order by State,Chaptercode")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlst.Items.Insert(0, New ListItem("Select NSF Chapter", "0"))
        ddlst.SelectedIndex = 0
    End Sub

    Protected Sub btnORGSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        Dim OrgName As String = String.Empty
        Dim City As String = String.Empty
        Dim strSql As New StringBuilder
        OrgName = txtOrgName.Text
        City = txtCity.Text
        Dim length As Integer = 0
        If OrgName.Length > 0 Then
            strSql.Append("  O.Organization_name like '%" + OrgName + "%'")
        End If
        If (City.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.CITY like '%" + City + "%'")
            Else
                strSql.Append("  O.City like '%" + City + "%'")
            End If
        End If
        If (ddlNSFChapter.SelectedValue > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.ChapterID = " + ddlNSFChapter.SelectedValue + "")
            Else
                strSql.Append(" and O.ChapterID = " + ddlNSFChapter.SelectedValue + "")
            End If
        End If

        If Not ddlOrgState.SelectedItem.Text = "Select State" Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.State like '%" + ddlOrgState.SelectedValue + "%'")
            Else
                strSql.Append("  O.State like '%" + ddlOrgState.SelectedValue + "%'")
            End If
        End If

        If OrgName.Length > 0 Or ddlNSFChapter.SelectedValue > 0 Or City.Length > 0 Or Not ddlOrgState.SelectedItem.Text = "Select State" Then
            SearchORG(strSql.ToString())
        Else
            lblORGSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblORGSearch.Visible = True
        End If
    End Sub

    Private Sub SearchORG(ByVal sqlSt As String)
        Dim strSQL As String = "select O.AutomemberId,O.Organization_name,O.address1,O.city,O.state,O.zip,Ch.ChapterCode from  OrganizationInfo O Left Join Chapter Ch ON O.ChapterID=Ch.ChapterID where " & sqlSt
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridORG.DataSource = dt
        GridORG.DataBind()
        If (Count > 0) Then
            Panel5.Visible = True
            lblORGSearch.Text = ""
            lblORGSearch.Visible = False
        Else
            lblORGSearch.Text = "No Volunteer match found"
            lblORGSearch.Visible = True
            Panel5.Visible = False
        End If
    End Sub

    Protected Sub GridORG_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridORG.Rows(index)
        txtOrganization.Text = row.Cells(1).Text
        lblOrgID.Text = GridORG.DataKeys(index).Value
        pORGSearch.Visible = False
        If txtOrganization.Text.ToLower.Contains("united way") = True Then
            tremployer.Visible = True
            FillDonation(lblOrgID.Text)
            lblFlag.Text = "True"

            ddlPrgmType.Items.FindByText("Volunteering").Enabled = False
        Else
            tremployer.Visible = False
            FillDonation(lblOrgID.Text)
            'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text)
            'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text & " AND CE.EmployerMembID = " & lblOrgID.Text)
        End If
    End Sub

    Protected Sub btnORGClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pORGSearch.Visible = False
    End Sub

    Protected Sub btnEmployer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmployer.Click
        cleargrids()
        pEmployerSearch.Visible = True
        LoadStates(ddlEmployerState)
        LoadChapter(ddlEmployerNSFChapter)
    End Sub

    Protected Sub btnEmployerSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        Dim EmployerName As String = String.Empty
        Dim City As String = String.Empty
        Dim strSql As New StringBuilder
        EmployerName = txtEmployerName.Text
        City = txtEmployerCity.Text
        Dim length As Integer = 0
        If EmployerName.Length > 0 Then
            strSql.Append("  O.Organization_name like '%" + EmployerName + "%'")
        End If
        If (City.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.CITY like '%" + City + "%'")
            Else
                strSql.Append("  O.City like '%" + City + "%'")
            End If
        End If
        If (ddlEmployerNSFChapter.SelectedValue > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.ChapterID = " + ddlEmployerNSFChapter.SelectedValue + "")
            Else
                strSql.Append(" and O.ChapterID = " + ddlEmployerNSFChapter.SelectedValue + "")
            End If
        End If

        If Not ddlEmployerState.SelectedItem.Text = "Select State" Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and O.State like '%" + ddlEmployerState.SelectedValue + "%'")
            Else
                strSql.Append("  O.State like '%" + ddlEmployerState.SelectedValue + "%'")
            End If
        End If

        If EmployerName.Length > 0 Or ddlNSFChapter.SelectedValue > 0 Or City.Length > 0 Or Not ddlOrgState.SelectedItem.Text = "Select State" Then
            'Response.Write(strSql.ToString() & " AND O.Organization_name NOT like '%united way%'  AND O.Automemberid <> " & lblOrgID.Text)
            SearchEmployer(strSql.ToString() & " AND O.Organization_name NOT like '%united way%'  AND O.Automemberid <> " & lblOrgID.Text)
        Else
            lblEmployerSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblEmployerSearch.Visible = True
        End If
    End Sub

    Private Sub SearchEmployer(ByVal sqlSt As String)
        Dim strSQL As String = "Select O.AutomemberId,O.Organization_name,O.address1,O.city,O.state,O.zip,Ch.ChapterCode from  OrganizationInfo O Left Join Chapter Ch ON O.ChapterID=Ch.ChapterID where " & sqlSt
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridEmployer.DataSource = dt
        GridEmployer.DataBind()
        If (Count > 0) Then
            PanelEmployer.Visible = True
            lblEmployerSearch.Text = ""
            lblEmployerSearch.Visible = False
        Else
            lblEmployerSearch.Text = "No match found"
            lblEmployerSearch.Visible = True
            PanelEmployer.Visible = False
        End If
    End Sub

    Protected Sub btnEmployerClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pEmployerSearch.Visible = False
    End Sub

    Protected Sub GridEmployer_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridEmployer.Rows(index)
        txtEmployer.Text = row.Cells(1).Text
        lblEmployerID.Text = GridEmployer.DataKeys(index).Value
        FillDonation(lblEmployerID.Text)
        'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text & " AND CE.EmployerMembID = " & lblEmployerID.Text)
        pEmployerSearch.Visible = False
    End Sub

    Protected Sub btnEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmployee.Click
        lblerr.Text = ""
        cleargrids()
        ' FillDonation(DDMatchURL.SelectedValue)
        pIndSearch.Visible = True
        lblIndSearch.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtEmail.Text = ""
        LoadStates(ddlState)
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim state As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  I.lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If Not ddlState.SelectedItem.Text = "Select State" Then
            length = strSql.ToString().Length
            state = ddlState.SelectedValue
            If (length > 0) Then
                strSql.Append(" and I.state like '%" + state + "%'")
            Else
                strSql.Append("  I.state like '%" + state + "%'")
            End If
        End If

        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Or state.Length > 0 Then
            strSql.Append("  order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If
    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        'Dim prmArray(1) As SqlParameter
        'prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, "usp_SelectWhereIndspouse2", prmArray)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, "select I.automemberid,I.Employer,I.firstname,I.lastname,I.email,I.hphone,I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID where  " & sqlSt & "")
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        txtEmployee.Text = row.Cells(1).Text + " " + row.Cells(2).Text
        lblEmployeeID.Text = GridMemberDt.DataKeys(index).Value
        pIndSearch.Visible = False
    End Sub
    'Modifyied by Ahila
    'Date 18/10/2014
    Private Sub FillDonation(ByVal sqlSt As String)
        Try

            lblCorp.Text = ""
            lblHead.Text = ""
            Dim strSQL As String = "select distinct D.DonationID,O.ORGANIZATION_NAME,D.MemberID as OrgMemberID,"
            strSQL += " D.DepositSlip,case  When exists(select top 1 OrgCheckNo from corpemp ce where CE.OrgMemberID=D.Memberid "
            strSQL += " and convert(date, CE.CheckDate)=Convert(Date,D.DonationDate) "
            strSQL += "and ((D.Transaction_number=CE.OrgCheckNo) or ('00'+CE.OrgCheckNo=D.Transaction_number))  "
            strSQL += " ) Then  'Matched'else 'Not Matched'End   as Reconcile,  "
            strSQL += " D.DonorType, D.AMOUNT ,D.DonationDate, D.Transaction_number as Transaction_number, O.CITY ,O.STATE  from  "
            strSQL += " DonationsInfo D left Join OrganizationInfo O ON O.AutoMemberID = d.MEMBERID "
            strSQL += " where    DonorType ='OWN' AND D.MEMBERID =" & sqlSt & " ORDER BY D.DonationDate Desc"
            ' Dim strSQL As String = "select distinct D.DonationID,O.ORGANIZATION_NAME,D.MemberID as OrgMemberID, case When CE.OrgCheckNo=D.Transaction_number then 'Matched' else 'Not Matched' end as Reconcile, D.DonorType, D.AMOUNT ,D.DonationDate, D.TRANSACTION_NUMBER, O.CITY ,O.STATE  from DonationsInfo D Inner Join OrganizationInfo O ON O.AutoMemberID = d.MEMBERID Left join CorpEmp CE  on CE.OrgMemberID=D.Memberid and DATEADD(dd, 0, DATEDIFF(dd, 0, CE.CheckDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, D.DonationDate ))   where  DonorType ='OWN' AND D.MEMBERID =" + DDMatchURL.SelectedValue + " ORDER BY D.DonationDate Desc"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim dv As DataView = New DataView(dt)
            Session("FillDonFirst") = dt
            GridDonations.DataSource = dt
            GridDonations.DataBind()
            LabLCorp.Text = ""
            FillCorpEmpTbl()
            ACHEFT()
            LabelACH.Visible = True

            GridDonations.Visible = True
            If (Count > 0) Then
                PDonations.Visible = True
                ExpExcel1.Visible = True
                ' btnSearchDon.Visible = True
                lblerr.Text = ""
                GridDonations.Columns(0).Visible = True
                If Count = 1 Then
                    LoadGrid("WHERE  CE.OrgCheckNo = '" & ds.Tables(0).Rows(0)("TRANSACTION_NUMBER") & "'")
                End If
                lblCorp.Visible = True
                lblCorp.Text = "Table 3: Employee List"
            Else
                lblerr.Text = ""
                lblerr.Text = "No Donation found"
                lblCorp.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Modifyied by Ahila
    'Date 18/10/2014
    Private Sub ACHEFT()
        Try
            Dim strSQL As String

            strSQL = "SELECT  B.BankID,B.BankTransID, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,B.checknumber,"
            strSQL += " B.Reason, case  When exists(select top 1 checkdate from corpemp ce where B.TransDate=CE.checkdate and OrgMemberID=" + DDMatchURL.SelectedValue + " ) Then  'Matched'else 'Not Matched'End   as Reconcile,"
            strSQL += "'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
            strSQL += "  B.VendCust, N.Description ,N.DonorType,B.MemberID FROM BankTrans B "
            strSQL += " LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat = C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID=1"
            strSQL += "LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat = H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID=2"
            strSQL += "  Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
            strSQL += "  Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation'  and N.donorType='OWN'  and b.MemberID=" + DDMatchURL.SelectedValue + ""
            strSQL += "Group By B.BankID,N.Description, B.Reason ,N.DonorType,B.TransCat,B.TransDate,B.Vendcust,B.checknumber,B.BankTransID,B.MemberID Order by B.TransDate desc"
            'Response.Write(strSQL)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            GridVACHEFT.Visible = True
            Panel1.Visible = True
            Session("ACHeFTtable2") = dt
            GridVACHEFT.DataSource = dt
            GridVACHEFT.DataBind()
            GridVACHEFT.Visible = True

            Dim Count As Integer = dt.Rows.Count
            If (Count > 0) Then
                BtncloseAcH.Visible = True
                ExpExcel2.Visible = True
                LabeAchnorecord.Text = ""
            Else
                LabeAchnorecord.Visible = True
                BtncloseAcH.Visible = False
                LabeAchnorecord.Text = "No Records found"
                ' LabeAchnorecord.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub FillCorpEmpTbl()
        Try
            Dim strSQL As String
            strSQL = "select distinct CE.CorpEmpID,CE.OrgMemberID,I.FirstName+' '+I.LastName as Name,CE.EmployerMembID ,"
            strSQL += "CE.MemberID,CE.ProgramType, convert(date, CE.CheckDate) as CheckDate,CE.OrgCheckNo,"
            strSQL += "CONVERT(DECIMAL(10,2),CE.EmpFunds) as EmpFunds,convert(varchar,CE.CompFunds) as CompFunds,"
            strSQL += "convert(varchar,CE.TotAmount) as TotAmount,CE.PaymentID,CE.GiftDate,CE.PayYear,CE.Hours,CE.Email,"
            strSQL += "case When (CE.OrgCheckNo=D.Transaction_number) or ('00'+CE.OrgCheckNo=D.Transaction_number)"
            strSQL += "  and convert(date, CE.CheckDate)=Convert(Date,D.DonationDate)"
            strSQL += "then 'Matched' else 'Not Matched' end as Reconcile from CorpEmp CE "
            strSQL += "left join donationsinfo D  on CE.OrgMemberID=D.Memberid and   ((D.Transaction_number=CE.OrgCheckNo) or ('00'+CE.OrgCheckNo=D.Transaction_number)) and"
            strSQL += "  convert(date, CE.CheckDate)=Convert(Date,D.DonationDate)  left join "
            strSQL += "Indspouse I on I.AutoMemberID=CE.MemberID where OrgMemberID=" + DDMatchURL.SelectedValue + " order by  convert(date, CE.CheckDate) desc"
            'Response.Write(strSQL)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            GridCorpEmp.Visible = True
            GridCorpEmp.DataSource = dt
            Session("CorpEmpTable3") = dt
            GridCorpEmp.DataBind()
            GridCorp.Visible = False

            Dim Count As Integer = dt.Rows.Count
            If (Count > 0) Then
                BtnClose.Visible = True
                ExpExcel3.Visible = True
            Else
                LabLCorp.Text = "No Records found"
                lblCorp.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridDonations_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridDonations.RowDataBound
        '    ferdine
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    'Dim btn As Button = Nothing
        '    'btn = e.Row.Cells(0).Controls(0)
        '    Dim DAmt As Decimal, TAmt As Decimal
        '    DAmt = CDec(e.Row.Cells(4).Text.ToString())
        '    TAmt = SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select CASE WHEn COUNT(EmpFunds) = 0 THEN 0.0 Else SUM(EmpFunds) END from CorpEmp where OrgCheckNo='" & e.Row.Cells(6).Text & "'")
        '    'MsgBox(DAmt.ToString() + " " + TAmt.ToString())
        '    If DAmt <= TAmt Then
        '        'btn.Enabled = False
        '        e.Row.Cells(0).Enabled = False
        '    End If
        'End If
    End Sub

    Protected Sub GridDonations_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridDonations.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridDonations.Rows(index)
        lblGrid.Text = "False"
        txtChkDate.Text = row.Cells(7).Text

        txtChckNo.Text = row.Cells(8).Text
        If txtChckNo.Text = "&nbsp;" Then
            txtChckNo.Text = 0
        End If
        If txtChkDate.Text = "&nbsp;" Then
            txtChkDate.Text = 0
        End If

        lblDonationID.Text = row.Cells(1).Text
        txtEmprMemID.Enabled = True

        txtEmprMemID.Text = ""
        'PDonations.Visible = False
        ' btnSearchDon.Visible = True
        GridDonations.Columns(0).Visible = False
        GridDonations.Rows(index).BackColor = Color.SkyBlue
        For i As Integer = 0 To GridDonations.Rows.Count - 1
            If i <> index Then
                GridDonations.Rows(i).BackColor = Color.White
            End If
        Next
        LoadGrid("WHERE  CE.OrgCheckNo = '" & row.Cells(6).Text & "'")
    End Sub

    Function CheckDonation(ByVal DonID As Integer) As Boolean
        Try
            Dim result As String = SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select CASE WHEN (CASE WHEN COUNT(CE.CorpEmpID) > 0 then SUM(CE.EmpFunds) ELSE 0.00 END) <(select Amount from DonationsInfo where DonationID=" & DonID & ") THEN 'TRUE' ELSE 'FALSE' END from CorpEmp CE Inner Join DonationsInfo D ON D.TRANSACTION_NUMBER  = CE.OrgCheckNo and D.DonationDate=CE.CheckDate where D.DonationID = " & DonID & "")
            If result.ToLower = "true" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            'Response.Write("<br />" & ex.ToString())
        End Try
    End Function



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            lblerr.Text = ""
            ' Session("PaymentID") = ""
            Dim TxtEmp As String = ""
            Dim Txtmatch As String = ""
            Dim DtAmuontval As String = ""
            Dim wherecntn As String = ""
            Dim txtEmpAmtval As String = txtEmpAmt.Text.Trim()
            Dim txtmatchAmtval As String = txtMatchAmt.Text.Trim()
            If txtEmpAmt.Text = "" Then
                txtEmpAmtval = 0
            End If
            If txtMatchAmt.Text = "" Then
                txtmatchAmtval = 0
            End If


            DtAmuontval = Convert.ToDouble(txtEmpAmtval) + Convert.ToDouble(txtmatchAmtval)


            If btnSubmit.Text = "Modify" And txtChckNo.Text.Length > 0 And txtChkDate.Text.Length > 0 Then
                wherecntn = " and CE.CorpEmpID NOT IN (" & lblCorpEmpID.Text & ")"
                lblDonationID.Text = SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select DonationID from DonationsInfo where  TRANSACTION_NUMBER  = '" & txtChckNo.Text & "' and cast(DonationDate as Date)=cast('" & txtChkDate.Text & "' as Date)")
            End If

            If DDMatchURL.SelectedValue = 0 Then
                lblerr.Text += "<li>Organization</li>"

            End If

            If ddlPayYear.SelectedValue = 0 Then
                lblerr.Text += "<li>Year</li>"

            End If
            Dim s1t As String = Session("PaymentID")
            If txtPaymentID.Text = "0" Or txtPaymentID.Text.Length < 1 And Session("PaymentID") <> "N" Then
                Session("PaymentID") = ""


            End If
            If txtEmployee.Text = "" And ddlEmployee.Items.Count <= 0 Then
                lblerr.Text += "<li>Select Employee</li>"
                ' lblerr.Text = "Please select Employee"
            End If
            If lblFlag.Text = "True" And txtEmployer.Text.Length < 4 Then
                lblerr.Text += "<li> Search and select for Employer</li>"
                ' lblerr.Text += "Please Search and select for Employer"

            End If
            If lblFlag.Text = "false" And lblEmployeeID.Text.Length < 1 Then
                lblerr.Text += "<li> Search and select for Employer</li>"
                ' lblerr.Text += "Please Search and select for Employer"

            End If

            If txtChckNo.Text.Length < 1 And lblFlag.Text = "False" Then
                ' FillDonation(lblOrgID.Text)

                If lblGrid.Text = "True" Then
                    lblerr.Text += "<li> Check Number is Populated with BankTransID</li>"
                Else
                    lblerr.Text += "<li> Check number</li>"
                End If


            End If

            If txtEmpAmt.Text.Length < 1 And txtMatchAmt.Text.Length < 1 Then
                lblerr.Text += "<li>Employee amount or Matching Amount</li>"
                '  lblerr.Text += "Please fill Emp.amount or Matching Amount"

            End If

            If txtAmount.Text.Length < 1 Then
                lblerr.Text += "<li> Total amount</li>"

            End If
            If txtAmount.Text.Length > 0 Then


                If txtEmpAmt.Text.Length > 0 Or txtMatchAmt.Text.Length > 0 Then

                    If txtAmount.Text <> Convert.ToDouble(DtAmuontval) Then
                        lblerr.Text += "<li>Total Amount should be sum of Employee amount and Matching amount</li>"
                        'lblerr.Text = "Total Amount is wrong Please fill Total Amount of Employee Amount and  Matching Amount"
                        'ElseIf btnSubmit.Text = "Modify" And SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select CASE WHEN (CASE WHEN COUNT(CE.CorpEmpID) > 0 then SUM(CE.EmpFunds) ELSE 0.00 END)+" & txtEmpAmt.Text & " <=(select Amount from DonationsInfo where DonationID=" & lblDonationID.Text & ") THEN 'TRUE' ELSE 'FALSE' END from CorpEmp CE Inner Join DonationsInfo D ON D.TRANSACTION_NUMBER  = CE.OrgCheckNo and D.DonationDate=CE.CheckDate where D.DonationID = " & lblDonationID.Text & wherecntn & "") = False Then ' txtAmount.Text 
                        '    lblerr.Text = "Sum of donations exceeds remittance selected."
                        '    Response.Write("select CASE WHEN (CASE WHEN COUNT(CE.CorpEmpID) > 0 then SUM(CE.EmpFunds) ELSE 0.00 END)+" & txtEmpAmt.Text & " <=(select Amount from DonationsInfo where DonationID=" & lblDonationID.Text & ") THEN 'TRUE' ELSE 'FALSE' END from CorpEmp CE Inner Join DonationsInfo D ON D.TRANSACTION_NUMBER  = CE.OrgCheckNo and D.DonationDate=CE.CheckDate where D.DonationID = " & lblDonationID.Text & wherecntn & "")
                    End If
                End If
            End If
            If txtChkDate.Text.Length < 3 And lblGrid.Text = "False" Then
                lblerr.Text += "<li> Check Date</li>"
            ElseIf txtChkDate.Text.Length < 3 And lblGrid.Text = "True" Then
                lblerr.Text += "<li> Check date is populated with TransDate</li>"
            ElseIf txtChkDate.Text.Length < 3 Then
                lblerr.Text += "<li> Check Date</li>"
            ElseIf IsDate(txtChkDate.Text) = False Then
                lblerr.Text += "<li>Checkdate should be  in mm/dd/YYYY format</li>"

            End If
            If ddlPrgmType.SelectedValue = "0" Then
                lblerr.Text += "<li>Program Type</li>"
            End If

            If txtGiftDate.Text.Length < 3 Then
                If txtEmpAmt.Text.Length > 0 And txtEmpAmt.Text <> "0" Then
                    lblerr.Text += "<li>Gift Date</li>"
                End If
            ElseIf IsDate(txtGiftDate.Text) = False Then
                lblerr.Text += "<li>Giftdate should be in mm/dd/YYYY format</li>"

            End If
            If ddlPayMethod.SelectedIndex = 0 Then
                lblerr.Text += "<li>Pay Method</li>"

            End If

            If txtPaymentID.Text.Length > 0 Then
                TrPaymentIDErr.Visible = False


            End If
            If lblerr.Text <> "" Then
                lblerr.Visible = True
                lblerr.Text = "Required fileds: <ul>" + lblerr.Text + "</ul>"
                Session("PaymentID") = ""


                Exit Sub
            Else
                Dim st As String = Session("PaymentID")
                If (txtPaymentID.Text = "" Or txtPaymentID.Text = "0") And (Session("PaymentID") = "Y" Or Session("PaymentID") = "") And (Session("PaymentID") <> "N") Then
                    TrPaymentIDErr.Visible = True
                    ' lblerr.Text += "<li> PaymentID</li>"
                    ' lblerr.Text = "Please Enter PaymentID"
                    Exit Sub
                End If

                If btnSubmit.Text = "Modify" Then
                    If SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, "select CASE WHEN (CASE WHEN COUNT(CE.CorpEmpID) > 0 then SUM(CE.EmpFunds) ELSE 0.00 END)+" & txtEmpAmt.Text & " <=(select Amount from DonationsInfo where DonationID=" & lblDonationID.Text & ") THEN 'TRUE' ELSE 'FALSE' END from CorpEmp CE Inner Join DonationsInfo D ON D.TRANSACTION_NUMBER = CE.OrgCheckNo and D.DonationDate=CE.CheckDate where D.DonationID = " & lblDonationID.Text & wherecntn & "") = "FALSE" Then ' txtAmount.Text 
                        lblerr.Text = "Sum of donations exceeds remittance selected."
                        Exit Sub
                    End If
                End If

                lblerr.Text = ""



                Dim Sql As String
                Dim employerMembID As Integer
                Dim EmployeeAmount As String = txtEmpAmt.Text
                Dim MatchAmount As String = txtMatchAmt.Text

                If lblFlag.Text = "True" Then
                    employerMembID = Integer.Parse(lblEmployerID.Text)
                Else
                    employerMembID = Integer.Parse(DDMatchURL.SelectedValue) 'lblOrgID.Text)
                End If
                If btnSubmit.Text = "Add" Then
                    If txtEmpAmt.Text = "0" Or String.IsNullOrEmpty(txtEmpAmt.Text) Then
                        EmployeeAmount = "NULL"
                    End If
                    If txtMatchAmt.Text = "0" Or String.IsNullOrEmpty(txtMatchAmt.Text) Then
                        MatchAmount = "NULL"
                    End If
                    If (String.IsNullOrEmpty(txtHours.Text)) Then
                        txtHours.Text = "NULL"
                    End If
                    If (String.IsNullOrEmpty(txtMemEmail.Text)) Then
                        txtMemEmail.Text = ""
                    End If
                    If (String.IsNullOrEmpty(txtEmprMemID.Text)) Then
                        txtEmprMemID.Text = "NULL"
                    End If
                    If (String.IsNullOrEmpty(txtPaymentID.Text)) Then
                        txtPaymentID.Text = "0"
                    End If
                    If txtEmployee.Text = "" And ddlEmployee.Items.Count <= 0 Then
                        txtEmployee.Text = "NULL"
                    End If
                    Sql = "SELECT count (CorpEmpID) FROM CorpEmp where OrgMemberID=" & DDMatchURL.SelectedValue & " AND EmployerMembID=" & txtEmprMemID.Text & " AND MemberID=" & IIf(txtEmployee.Text = "", ddlEmployee.SelectedValue, lblEmployeeID.Text) & " AND OrgCheckNo = '" & txtChckNo.Text & "'" ' lblOrgID.Text
                    ' Response.Write(Sql)
                    'Session("loginID") = "33"
                    If SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, Sql) < 1 Then
                        Sql = "INSERT INTO CorpEmp(OrgMemberID, EmployerMembID, MemberID, ProgramType, OrgCheckNo, CheckDate, EmpFunds, CompFunds, TotAmount, PayYear, GiftDate, PaymentID, PayMethod, Hours, Email ,CreateDate, CreatedBy) Values ("
                        Sql = Sql & DDMatchURL.SelectedValue & "," & txtEmprMemID.Text & "," & IIf(txtEmployee.Text = "", ddlEmployee.SelectedValue, lblEmployeeID.Text) & ",'" & ddlPrgmType.SelectedItem.Text & "','" & txtChckNo.Text & "','" & txtChkDate.Text & "'," & _
                           EmployeeAmount & "," ' lblOrgID.Text
                        Sql = Sql & MatchAmount & "," & txtAmount.Text & "," & ddlPayYear.SelectedValue & ",'" & txtGiftDate.Text & "'," & IIf((txtPaymentID.Text.Length = 0 Or txtPaymentID.Text = "0"), "NULL", txtPaymentID.Text) & ",'" & ddlPayMethod.SelectedValue & "'," & IIf(txtHours.Text = "", "NULL", txtHours.Text) & "," & IIf(txtMemEmail.Text = "", "NULL", "'" & txtMemEmail.Text & "'")
                        Sql = Sql & " ,Getdate()," & Session("loginID") & ")"
                        'Response.Write(Sql)


                        SqlHelper.ExecuteNonQuery(Application(Connectionstring), CommandType.Text, Sql)
                        'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text)
                        'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text & " AND CE.EmployerMembID = " & employerMembID)
                        LoadGrid(" WHERE CE.OrgCheckNo = '" & txtChckNo.Text & "'")
                        clear()
                        ' lblerr.Visible = False
                        ddlPayYear.SelectedIndex = 0
                        Response.Write("<script>alert('Added successfully')</script>")

                        Session("PaymentID") = ""
                        'DDMatchURL.SelectedValue = "0"
                        lblerr.Text = ""
                        lblerr.Text = ""
                        lblerr.Visible = False
                        ' DDMatchURL.SelectedIndex = 0
                        '  lblerr.Text = "Added Successfully"

                        TrPaymentIDErr.Visible = False
                    Else
                        lblerr.Text = "Data Already Found"
                    End If
                Else
                    btnSubmit.Text = "Add"
                    Sql = "SELECT count (CorpEmpID) FROM CorpEmp where OrgMemberID=" & DDMatchURL.SelectedValue & " AND EmployerMembID=" & employerMembID & " AND MemberID=" & IIf(txtEmployee.Text = "", ddlEmployee.SelectedValue, lblEmployeeID.Text) & " AND OrgCheckNo = '" & txtChckNo.Text & "' and CorpEmpID NOT IN (" & lblCorpEmpID.Text & ")" ' lblOrgID.Text 
                    If SqlHelper.ExecuteScalar(Application(Connectionstring), CommandType.Text, Sql) < 1 Then
                        Sql = "UPDATE CorpEmp SET OrgMemberID=" & DDMatchURL.SelectedValue & ", EmployerMembID=" & employerMembID & ", MemberID=" & IIf(txtEmployee.Text = "", ddlEmployee.SelectedValue, lblEmployeeID.Text) & ", ProgramType='" & ddlPrgmType.SelectedItem.Text & "', OrgCheckNo='" & txtChckNo.Text & "', CheckDate='" & txtChkDate.Text & "', EmpFunds=" & txtEmpAmt.Text & ",CompFunds=" & IIf(txtMatchAmt.Text = "", "NULL", txtMatchAmt.Text) & ",TotAmount=" & txtAmount.Text & ", PayYear=" & ddlPayYear.SelectedValue & ", GiftDate='" & txtGiftDate.Text & "', PaymentID=" & IIf((txtPaymentID.Text.Length = 0 Or txtPaymentID.Text = "0"), "NULL", txtPaymentID.Text) & ", PayMethod='" & ddlPayMethod.Text & "', Hours=" & IIf(txtHours.Text = "", "NULL", txtHours.Text) & ", Email=" & IIf(txtMemEmail.Text = "", "NULL", "'" & txtMemEmail.Text & "'") & " ,ModifiedDate=GetDate(), ModifiedBy=" & Session("loginID") & " WHERE CorpEmpID =" & lblCorpEmpID.Text
                        If Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application(Connectionstring), CommandType.Text, Sql)) > 0 Then
                            lblerr.Text = "Updated Successfully"
                            LoadGrid("WHERE  CE.OrgCheckNo = '" & txtChckNo.Text & "'")
                            clearall()
                        Else
                            lblerr.Text = ""
                            lblerr.Text = "Error in Updating records."
                        End If
                        'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text & " AND CE.EmployerMembID = " & employerMembID)
                        'LoadGrid()
                    Else
                        lblerr.Text = ""
                        lblerr.Text = "Data Already Found"
                    End If
                End If

            End If
            GridCorp.Visible = False
            FillDonation(DDMatchURL.SelectedValue)
            TrPaymentIDErr.Visible = False
            lblerr.Text = ""
            lblerr.Visible = False
            ' clearall()
            ' clearcorp()
        Catch ex As Exception
            lblerr.Text = ""
            'Response.Write("<br />" & ex.ToString())
        End Try
    End Sub
    Protected Sub ClearDrop()
        If txtEmpAmt.Text = 0 Or (String.IsNullOrEmpty(txtEmpAmt.Text)) Then
            txtEmpAmt.Text = ""


        End If
        If txtMatchAmt.Text = 0 Or (String.IsNullOrEmpty(txtMatchAmt.Text)) Then
            txtMatchAmt.Text = ""

        End If
    End Sub

    Private Sub clear()
        If lblDonationID.Text <> "" Then
            If CheckDonation(lblDonationID.Text) Then
                txtAmount.Text = String.Empty
                txtEmployee.Text = String.Empty
                lblEmployeeID.Text = String.Empty
                ddlPrgmType.Items.FindByText("Volunteering").Enabled = True
                ' btnSearchDon.Visible = False
                lblCorpEmpID.Text = String.Empty
                ddlPrgmType.SelectedIndex = ddlPrgmType.Items.IndexOf(ddlPrgmType.Items.FindByValue("0"))
                txtEmail.Text = String.Empty
                txtEmprMemID.Text = String.Empty
                txtHours.Text = String.Empty
                'txtChckNo.Text = String.Empty
                'txtChkDate.Text = String.Empty
                'txtOrganization.Text = String.Empty
                'txtEmployer.Text = String.Empty
                'lblEmployerID.Text = String.Empty
                'lblOrgID.Text = String.Empty
                'lblFlag.Text = "False"
                'tremployer.Visible = False
            Else
                clearall()
            End If
        Else
            clearall()
        End If
    End Sub

    Private Sub clearall()
        txtAmount.Text = String.Empty
        txtChckNo.Text = String.Empty
        txtChkDate.Text = String.Empty
        txtOrganization.Text = String.Empty
        txtEmployer.Text = String.Empty
        txtEmployee.Text = String.Empty
        lblEmployeeID.Text = String.Empty
        lblEmployerID.Text = String.Empty
        lblOrgID.Text = String.Empty
        lblDonationID.Text = String.Empty
        lblerr.Text = ""
        ddlPrgmType.Items.FindByText("Volunteering").Enabled = True
        lblFlag.Text = "False"
        ddlPrgmType.SelectedIndex = ddlPrgmType.Items.IndexOf(ddlPrgmType.Items.FindByValue("0"))
        'btnSearchDon.Visible = False
        tremployer.Visible = False
        lblerr.Text = String.Empty
        lblCorpEmpID.Text = String.Empty
        cleargrids()
        ' GridDonations.DataSource = Nothing
        ' GridDonations.DataBind()
        PDonations.Visible = False

        txtEmpAmt.Text = ""
        txtMatchAmt.Text = ""
        txtPaymentID.Text = ""
        txtGiftDate.Text = ""
        txtHours.Text = ""
        txtMemEmail.Text = ""
        txtEmprMemID.Text = ""
        ddlPayMethod.SelectedIndex = ddlPayMethod.Items.IndexOf(ddlPayMethod.Items.FindByValue("0"))

    End Sub

    Private Sub cleargrids()
        GridEmployer.DataSource = Nothing
        GridEmployer.DataBind()
        pEmployerSearch.Visible = False
        GridMemberDt.DataSource = Nothing
        GridMemberDt.DataBind()
        pIndSearch.Visible = False
        GridORG.DataSource = Nothing
        GridORG.DataBind()
        pORGSearch.Visible = False
    End Sub

    Protected Sub btnSearchDon_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If lblFlag.Text = "True" And lblEmployerID.Text.Length > 0 Then
        '    FillDonation(lblEmployerID.Text)
        '    GridDonations.Columns(0).Visible = True
        'ElseIf lblFlag.Text = "False" And lblOrgID.Text.Length > 0 Then
        '    FillDonation(lblOrgID.Text)
        '    GridDonations.Columns(0).Visible = True
        'End If
        If lblOrgID.Text.Length > 0 Then
            FillDonation(lblOrgID.Text)
            GridDonations.Columns(0).Visible = True
            'clearcorp()
        End If

    End Sub

    Private Sub LoadGrid(ByVal SQL As String)
        Try

            Dim strSQL As String = "SELECT CE.CorpEmpID, CE.OrgMemberID,OI1.ORGANIZATION_NAME as ORGName , CE.EmployerMembID, OI2.ORGANIZATION_NAME as EmpName, "
            strSQL = strSQL & " CE.MemberID, I.FirstName + ' ' + I.LastName as EmployeeName, CE.ProgramType, CE.OrgCheckNo, cast(IsNull(CE.CheckDate,'') as DATE) as CheckDate, CE.EmpFunds, IsNull(CE.CompFunds,0) as CompFunds,  IsNull(CE.TotAmount,0) as TotAmount,  IsNull(CE.PayYear,2014) as PayYear,  cast(IsNull(CE.GiftDate,'') as DATE) as GiftDate,  IsNull(CE.PaymentID,'') as PaymentID,  IsNull(CE.PayMethod,'') as PayMethod,  IsNull(CE.Hours,0) as Hours,  IsNull(CE.Email,'') as Email "
            strSQL = strSQL & " FROM CorpEmp CE Inner Join OrganizationInfo OI1 ON OI1.AutoMemberID = CE.OrgMemberID "
            strSQL = strSQL & " Inner Join OrganizationInfo OI2 ON OI2.AutoMemberID = CE.EmployerMembID"
            strSQL = strSQL & " Inner Join IndSpouse I ON I.AutoMemberID = CE.MemberID " & SQL

            'Response.Write(strSQL)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim i As Integer
            Dim total As Integer = 0
            For i = 0 To dt.Rows.Count - 1
                If Not dt.Rows(i).Item("EmpFunds") Is DBNull.Value Then

                    total = total + dt.Rows(i).Item("EmpFunds")
                End If
            Next
            GridCorp.DataSource = dt
            GridCorp.DataBind()
            PnlCorpEmp.Visible = True
            If (Count < 1) Then
                lblCorp.Text = "Table 3: Employee List"
                lblHead.Visible = False
                ltltotal.Text = ""
            Else
                lblHead.Visible = True
                lblCorp.Text = ""
                ltltotal.Text = "Total so far : $" & total
                ltltotal.Visible = False
            End If
            GridDonations.Columns(0).Visible = True
            GridCorp.DataBind()
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub GridCorp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridCorp.Rows(index)
        ' IncurredExpence name Search
        Dim CorpID As String = row.Cells(2).Text
        Session("PaymentID") = ""
        If e.CommandName() = "Modify" Then
            lblerr.Text = ""
            clearall()
            btnSubmit.Text = "Modify"
            txtOrganization.Text = row.Cells(4).Text
            txtEmployer.Text = row.Cells(6).Text
            getValues(CorpID)
            If Not lblOrgID.Text = lblEmployerID.Text Then
                lblFlag.Text = "True"
                tremployer.Visible = True
            End If
            FillDonation(lblOrgID.Text)

            txtEmployee.Text = row.Cells(8).Text
            ddlPrgmType.SelectedIndex = ddlPrgmType.Items.IndexOf(ddlPrgmType.Items.FindByText(row.Cells(9).Text))
            txtChckNo.Text = row.Cells(10).Text
            txtChkDate.Text = row.Cells(11).Text

            'txtAmount.Text = row.Cells(12).Text.Replace(",", "").Replace("$", "")
            txtEmpAmt.Text = Convert.ToDouble(row.Cells(12).Text.Replace(",", "").Replace("$", "")) '
            txtMatchAmt.Text = Convert.ToDouble(row.Cells(13).Text.Replace(",", "").Replace("$", "")) ' Same as Corp Amount 'row.Cells(13).Text '.Replace(",", "").Replace("$", "")
            'txtAmount.Text = row.Cells(14).Text  '.Replace(",", "").Replace("$", "")
            txtAmount.Text = Convert.ToDouble(txtEmpAmt.Text) + Convert.ToDouble(txtMatchAmt.Text)

            ddlPayYear.SelectedIndex = ddlPayYear.Items.IndexOf(ddlPayYear.Items.FindByText(row.Cells(15).Text))
            txtGiftDate.Text = row.Cells(16).Text
            txtPaymentID.Text = row.Cells(17).Text
            ddlPayMethod.SelectedIndex = ddlPayMethod.Items.IndexOf(ddlPayMethod.Items.FindByText(row.Cells(18).Text))
            txtHours.Text = row.Cells(19).Text
            txtEmail.Text = row.Cells(20).Text

            txtEmprMemID.Text = row.Cells(5).Text

            'btnSearchDon.Visible = True
        ElseIf e.CommandName() = "Delete1" Then
            SqlHelper.ExecuteNonQuery(Application(Connectionstring), CommandType.Text, "Delete from CorpEmp where CorpEmpID=" & CorpID & "")
            'LoadGrid("WHERE CE.OrgMemberID = " & lblOrgID.Text)
            LoadGrid("WHERE  CE.OrgCheckNo = '" & row.Cells(10).Text & "'")
        End If
    End Sub

    Private Sub getValues(ByVal CorpID As String)
        Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application(Connectionstring), CommandType.Text, "SELECT OrgMemberID, EmployerMembID,MemberID FROM CorpEmp where CorpEmpID=" & CorpID & "")
        While Readr.Read()
            lblOrgID.Text = Readr("OrgMemberID")
            lblEmployerID.Text = Readr("EmployerMembID")
            lblEmployeeID.Text = Readr("MemberID")
            lblCorpEmpID.Text = CorpID.ToUpper()
        End While
        Readr.Close()
    End Sub

    Protected Sub GridCorp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridCorp.RowDataBound
        '    ferdine
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim btn As LinkButton = Nothing
        '    btn = CType(e.Row.Cells(1).Controls(0), LinkButton)
        '    btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        'End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        clearall()
        clearcorp()
        FillDonation(DDMatchURL.SelectedValue)
        GridDonations.Visible = True

    End Sub
    Private Sub clearcorp()
        GridCorp.DataSource = Nothing
        GridCorp.DataBind()
        ltltotal.Text = ""
        lblCorp.Text = ""
        lblHead.Visible = False
    End Sub

    Protected Sub DDMatchURL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDMatchURL.SelectedIndexChanged
        Try
            GidDisplayFunction()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GidDisplayFunction()
        clearall()
        lblerr.Text = ""
        lblGrid.Text = ""
        lblHead.Visible = False
        Dim i As String
        'i = DDMatchURL.SelectedValue
        'FillDonation(176)
        If DDMatchURL.SelectedValue = 0 Then
            lblerr.Text = ""
            BtnClose.Visible = False
            lblCorp.Text = ""
            LabeAchnorecord.Visible = False
            LabelACH.Visible = False
            GridCorpEmp.Visible = False
            lblerr.Text = "<li>Required filed: Organization name</li>"

        Else


            FillDonation(DDMatchURL.SelectedValue)
            ddlEmployee.Items.Clear()
            '/*****31-03-2014*****Load the drop down with Employee names from the existing records of CorpEmp table ***********************/
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, "Select Distinct I.FirstName + ''+ I.LastName as Name,I.AutoMemberID from CorpEmp C Inner Join IndSpouse I on I.AutoMemberID =C.MemberID Where C.OrgMemberID = " & DDMatchURL.SelectedValue)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlEmployee.DataSource = ds
                ddlEmployee.DataTextField = "Name"
                ddlEmployee.DataValueField = "AutoMemberID"
                ddlEmployee.DataBind()
            Else
                ddlEmployee.DataSource = Nothing
                ddlEmployee.DataBind()
            End If

            '/******01-04-2014*********Load the Grid from CorpEmp table for the Organization selected ***************************************/
            LoadGrid("WHERE  CE.OrgMemberID =  " & DDMatchURL.SelectedValue)

            ' txtEmprMemID.Text = DDMatchURL.SelectedValue
            '/******01-04-2014********Show the Matching Gift [login] Details from OrganizationInfo table for the Organization selected*************/
            Dim dsMatch As DataSet = SqlHelper.ExecuteDataset(Application(Connectionstring), CommandType.Text, "Select Matching_Gift_URL,	Matching_Gift_Account_Number,Matching_Gift_User_Id,Matching_Gift_Password from OrganizationInfo Where AutoMemberID=" & DDMatchURL.SelectedValue)
            If dsMatch.Tables(0).Rows.Count > 0 Then
                TrMatGiftDet.Visible = True
                lblMatGiftURL.Text = dsMatch.Tables(0).Rows(0)("Matching_Gift_URL")
                'lblMatGiftURL.Columns = Len(lblMatGiftURL.Text) + 2
                lblMatGiftAccNo.Text = dsMatch.Tables(0).Rows(0)("Matching_Gift_Account_Number")
                lblMatGiftUserId.Text = dsMatch.Tables(0).Rows(0)("Matching_Gift_User_Id")
                lblMatGiftPwd.Text = dsMatch.Tables(0).Rows(0)("Matching_Gift_Password")
            End If
            lblCorp.Text = ""
            lblCorp.Text = "Table 3: Employee List"
            txtEmprMemID.Enabled = True
            GridCorp.Visible = False

        End If
    End Sub

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        Session("PaymentID") = "Y"
        TrPaymentIDErr.Visible = False
        lblerr.Text = "Please Enter PaymentID"

    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        lblerr.Text = ""
        Session("PaymentID") = "N"
        boolNo = False
        txtPaymentID.Text = ""
        TrPaymentIDErr.Visible = False
    End Sub

    Protected Sub btnCloseOrgRem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOrgRem.Click
        PDonations.Visible = False
    End Sub

    Protected Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        lblCorp.Text = ""
        GridCorpEmp.Visible = False
        BtnClose.Visible = False
    End Sub

    Protected Sub GridCorpEmp_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GridCorpEmp.RowEditing
        lblEmployeeID.Text = String.Empty
        GridCorpEmp.EditIndex = e.NewEditIndex
        FillCorpEmpTbl()

    End Sub

    Protected Sub GridCorpEmp_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GridCorpEmp.RowCancelingEdit
        GridCorpEmp.EditIndex = -1
        FillCorpEmpTbl()
    End Sub
    '********It is actually updating function updating Event fire having some problem .So this Event having updating function***
    Protected Sub GridCorpEmp_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GridCorpEmp.RowDeleting
        Try

            Dim row = GridCorpEmp.Rows(e.RowIndex)
            Dim TxtEpmfunds As String = CType(row.FindControl("TxtEpmfunds"), TextBox).Text
            Dim TxtCompfunds As String = CType(row.FindControl("TxtCompfunds"), TextBox).Text
            Dim TxtTotAmt As String = CType(row.FindControl("TxtTotAmt"), TextBox).Text
            Dim TxtAmt As String = CType(row.FindControl("TxtTotAmt"), TextBox).Text
            Dim CorpEmpID As String = CType(row.FindControl("lbEmpID"), Label).Text
            Dim TxtOrgCheckNumber As String = CType(row.FindControl("TxtCheckNumber"), TextBox).Text
            Dim TxtOrgCheckdate As String = CType(row.FindControl("TxtCheckdate"), TextBox).Text
            Dim programType As String = CType(row.FindControl("TxtProgramtype"), TextBox).Text
            Dim MemberID As String = CType(row.FindControl("lbMember"), Label).Text

            If (String.IsNullOrEmpty(TxtEpmfunds)) Then
                TxtEpmfunds = "NULL"
            End If
            If (String.IsNullOrEmpty(TxtCompfunds)) Then
                TxtCompfunds = "NULL"
            End If

            If (String.IsNullOrEmpty(TxtTotAmt)) Then
                TxtTotAmt = "NULL"
            End If
            If (String.IsNullOrEmpty(TxtAmt)) Then
                TxtAmt = "NULL"
            End If
            If (String.IsNullOrEmpty(TxtOrgCheckNumber)) Then
                TxtOrgCheckNumber = "NULL"
            End If

            If (String.IsNullOrEmpty(TxtOrgCheckdate)) Then
                TxtOrgCheckdate = "NULL"
            End If
            If (String.IsNullOrEmpty(programType)) Then
                programType = "null"
            End If
            If (String.IsNullOrEmpty(lblEmployeeID.Text)) Then
                lblEmployeeID.Text = MemberID

            End If
            Dim stringQuery As String
            stringQuery = "update CorpEmp set MemberID=" + lblEmployeeID.Text + ", EmpFunds=" + TxtEpmfunds + ",CompFunds=" + TxtCompfunds + ","
            stringQuery += "OrgCheckNo=" + TxtOrgCheckNumber + ",ProgramType='" + programType + "',"
            stringQuery += "CheckDate='" + TxtOrgCheckdate + "',TotAmount=" + TxtTotAmt + " where CorpEmpID=" + CorpEmpID + ""
            'Response.Write(stringQuery)
            SqlHelper.ExecuteNonQuery(Application(Connectionstring).ToString(), CommandType.Text, stringQuery)
            FillCorpEmpTbl()
            Response.Write("<script>alert('Updated successfully')</script>")
            GidDisplayFunction()
            GridCorpEmp.EditIndex = -1
            FillCorpEmpTbl()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridDonations_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridDonations.PageIndexChanging
        GridDonations.PageIndex = e.NewPageIndex
        FillDonation(DDMatchURL.SelectedValue)
    End Sub
    Protected Sub GridCorpEmp_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridCorpEmp.PageIndexChanging
        GridCorpEmp.PageIndex = e.NewPageIndex
        FillCorpEmpTbl()
    End Sub

    Protected Sub GridCorpEmp_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridCorpEmp.RowCommand
        '' GridCorpEmp.Rows(GridCorpEmp.SelectedIndex).BackColor = Color.Blue
        'If (e.CommandName = "Edit") Then
        '    Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        '    Dim row As GridViewRow = GridCorpEmp.Rows(index)
        '    GridCorpEmp.Rows(index).BackColor = Color.SkyBlue
        '    For i As Integer = 0 To GridCorpEmp.Rows.Count - 1
        '        If i <> index Then
        '            GridCorpEmp.Rows(i).BackColor = Color.White
        '        End If
        '    Next
        'End If
    End Sub


    Protected Sub BtncloseAcH_Click(sender As Object, e As EventArgs) Handles BtncloseAcH.Click
        GridVACHEFT.Visible = False
        LabelACH.Visible = False
    End Sub

    Protected Sub GridVACHEFT_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridVACHEFT.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridVACHEFT.Rows(index)
        lblGrid.Text = "True"

        txtChkDate.Text = row.Cells(2).Text

        txtChckNo.Text = row.Cells(8).Text
        If txtChckNo.Text = "&nbsp;" Then
            txtChckNo.Text = 0
        End If
        If txtChkDate.Text = "&nbsp;" Then
            txtChkDate.Text = 0
        End If

        lblDonationID.Text = row.Cells(1).Text
        txtEmprMemID.Enabled = True

        txtEmprMemID.Text = ""
        'PDonations.Visible = False
        ' btnSearchDon.Visible = True
        GridVACHEFT.Columns(0).Visible = False
        GridVACHEFT.Rows(index).BackColor = Color.SkyBlue
        For i As Integer = 0 To GridVACHEFT.Rows.Count - 1
            If i <> index Then
                GridVACHEFT.Rows(i).BackColor = Color.White
            End If
        Next
        LoadGrid("WHERE  CE.OrgCheckNo = '" & row.Cells(6).Text & "'")
    End Sub

    Protected Sub GridVACHEFT_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridVACHEFT.PageIndexChanging
        GridVACHEFT.PageIndex = e.NewPageIndex
        ACHEFT()
    End Sub

    Protected Sub txtChckNo_TextChanged(sender As Object, e As EventArgs) Handles txtChckNo.TextChanged
        txtChkDate.Focus()

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvCreditCardchapter As Control)
    End Sub

    Protected Sub ExpExcel1_Click(sender As Object, e As EventArgs) Handles ExpExcel1.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Organization Remittances.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridDonations.RenderControl(htmlWrite)

        'GridDonations.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>North South Foundation<br />Organization Remittances</font></td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())
        Response.Write("</td></tr>") '<tr><td align='right' colspan='4'>Total : " & lblTotal.Text & "</td></tr>")
        Response.Write("</table>")
        Response.End()



        ' ExportToExcel(GridDonations, "Organization Remittances")
    End Sub





    Protected Sub ExpExcel2_Click(sender As Object, e As EventArgs) Handles ExpExcel2.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=ACH/EFT.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridVACHEFT.RenderControl(htmlWrite)
        'GridDonations.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>North South Foundation<br />ACH/EFT</font></td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())
        Response.Write("</td></tr>") '<tr><td align='right' colspan='4'>Total : " & lblTotal.Text & "</td></tr>")
        Response.Write("</table>")
        Response.End()

    End Sub

    Protected Sub ExpExcel3_Click(sender As Object, e As EventArgs) Handles ExpExcel3.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Employee List.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridCorpEmp.RenderControl(htmlWrite)
        'GridDonations.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>North South Foundation<br />Employee List</font></td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())
        Response.Write("</td></tr>") '<tr><td align='right' colspan='4'>Total : " & lblTotal.Text & "</td></tr>")
        Response.Write("</table>")
        Response.End()

    End Sub
End Class
