<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="ChangeEmail2.aspx.cs" Inherits="ChangeEmail2" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div>
    <asp:GridView ID="GridView1" runat="server"  AutoGenerateColumns="False"
         OnRowEditing="GridView1_RowEditing" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField= "NewEmail" HeaderText="New Email" />
            <asp:BoundField DataField="User_Email" HeaderText="Current Email" />
            <asp:BoundField DataField="DonorType" HeaderText="Type" />
            <asp:BoundField  DataField="Gender" HeaderText="Gender" />
            <asp:BoundField DataField="IsVolunteer" HeaderText="IsVolunteer" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
            <asp:BoundField DataField="HPhone" HeaderText="H Phone" />
           
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="State" HeaderText="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" />
            <asp:TemplateField>
            <HeaderTemplate>
            Change
            </HeaderTemplate>
           
            <ItemTemplate>
                        <asp:LinkButton id="lnkmemberID" CommandName="select" Text='<%#Eval("automemberid") %>'  runat="server" />
            </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="ChangeEmailDs" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="&#13;&#10; SELECT login_master.NewEmail ,&#13;&#10;                login_master.user_email,&#13;&#10;                IndSpouse .AutoMemberID, &#13;&#10;                IndSpouse.ChapterID ,&#13;&#10;                IndSpouse.DonorType,&#13;&#10;                IndSpouse.Gender ,&#13;&#10;                IndSpouse.LastName,&#13;&#10;                IndSpouse .FirstName,&#13;&#10;                IndSpouse.HPhone ,&#13;&#10;                IndSpouse.State,&#13;&#10;                IndSpouse.Zip ,&#13;&#10;                IndSpouse.City ,&#13;&#10;                login_master. RequestFlag,&#13;&#10;               dbo.IsVolunteer( autoMemberID)as IsVolunteer&#13;&#10;       FROM IndSpouse &#13;&#10;       INNER JOIN login_master&#13;&#10;       ON IndSpouse.Email = login_master.user_email&#13;&#10;       WHERE (login_master.RequestFlag = 'Yes')&#13;&#10;&#13;&#10;&#13;&#10;" OnSelecting="ChangeEmailDs_Selecting">
    </asp:SqlDataSource>
    &nbsp;&nbsp;&nbsp;&nbsp;<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    </div>
        <div><asp:HyperLink runat="server" NavigateUrl="~/VolunteerFunctions.aspx" ID="hlinkChapterFunctions">Go back to Menu</asp:HyperLink></div>

</asp:Content>


 

 
 
 