Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL


Namespace VRegistration


Partial Class UpdateChild
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents revDOB As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents cuvDOB As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objChild As Child
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        If Page.IsPostBack = False Then
                Dim str As Integer = Request("ChildID")
                Dim intParentID As Integer
                If Not (Session("CustIndID") Is Nothing) Then
                    intParentID = Session("CustIndID")
                Else
                    intParentID = Session("CustSpouseID")
                End If
                If Not Request.QueryString("Id") Is Nothing Then
                    hlinkParentRegistration.NavigateUrl = "MainChild.aspx?ID=" & Request.QueryString("Id")
                    intParentID = Request.QueryString("Id")
                End If
            Session("CID") = str
            If Not Request("ChildID") Is Nothing Then
                    objChild = New Child
                    objChild.GetChildByID(Application("ConnectionString"), str, intParentID)
                With objChild
                    txtFirstName.Text = StrConv(.FIRST_NAME, vbProperCase)
                    txtLastName.Text = StrConv(.LAST_NAME, vbProperCase)
                    txtMiddleName.Text = StrConv(.MIDDLE_INITIAL, vbProperCase)
                    If Not IsDBNull(.GENDER) Then
                        Try
                            ddlGender.SelectedValue = .GENDER
                        Catch

                        End Try
                    End If
                    If IsDate(.DATE_OF_BIRTH) Then txtDOB.Text = Convert.ToDateTime(.DATE_OF_BIRTH).ToShortDateString
                    'To Check whether the child is registered for any contest for the year already
                    'If yes disable the child grade dropdown

                        'Start Grade locking code
                        '*******************************************
                        '**** Code Added By FERDINE Jan 04 2010 ****
                        '*******************************************
                        If (Not (Session("RoleId") = Nothing)) Then
                            If (((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5"))) Then
                            Else
                                Dim conn As New SqlConnection(Application("ConnectionString"))
                                Dim EvntYear As Integer = 0
                                Dim CurrentYear As Integer = Now.Year()
                                EvntYear = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT ContestYear FROM Contestant where contestant_id = (select MAX(contestant_id) FROM  Contestant where ChildNumber=" & Session("CID") & ")")
                                If EvntYear < CInt(CurrentYear) Then
                                Else
                                    Dim chkDate As String = "08/15/" & EvntYear.ToString()
                                    If EvntYear = CInt(CurrentYear) And (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then
                                    Else
                                        ddlGrade.Enabled = False
                                    End If
                                End If
                            End If
                        Else
                            Dim conn As New SqlConnection(Application("ConnectionString"))
                            Dim EvntYear As Integer = 0
                            Dim CurrentYear As Integer = Now.Year()
                            EvntYear = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT ContestYear FROM Contestant where contestant_id = (select MAX(contestant_id) FROM  Contestant where ChildNumber=" & Session("CID") & ")")
                            If EvntYear < CInt(CurrentYear) Then
                            Else
                                Dim chkDate As String = "08/15/" & EvntYear.ToString()
                                If EvntYear = CInt(CurrentYear) And (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then
                                    '** corrected By ferdine on 29/06/2010
                                    'This is the coding to make the Grade -1 ferdine silvaa
                                    Dim lngHowLong As Long
                                    If IsDate(.ModifyDate) Then
                                        lngHowLong = DateDiff("m", .ModifyDate, Now)
                                    End If
                                    If Application("ContestType") = "1" Then
                                        If Not IsDBNull(.GRADE) Then
                                            'ddlGrade.SelectedValue = .GRADE
                                            ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByValue(.GRADE))
                                        Else
                                            ddlGrade.SelectedValue = "-1"
                                        End If
                                    Else
                                        If lngHowLong < 3 Then
                                            If Not IsDBNull(.GRADE) Then
                                                ' ddlGrade.SelectedValue = .GRADE
                                                ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByValue(.GRADE))
                                            Else
                                                ddlGrade.SelectedValue = "-1"
                                            End If
                                        Else
                                            ddlGrade.SelectedValue = "-1"
                                        End If
                                    End If
                                    '**end of June 29th
                                Else
                                    'ddlGrade.SelectedValue = .GRADE
                                    ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByValue(.GRADE))
                                    ddlGrade.Enabled = False
                                End If
                            End If
                        End If
                        'End Grade Locking code
                        txtSchoolName.Text = .SchoolName
                        If .CountryOfBirth.ToString <> "" Then
                            Try
                                ddlCountryofBirth.SelectedValue = .CountryOfBirth.ToString
                            Catch

                            End Try
                        End If
                        txtAchievements.Text = .Acheivements
                        txtHobbies.Text = .Hobbies
                    End With
            Else
                Response.Redirect("MainChild.aspx")
                End If
            End If
        End Sub
    Private Sub UpdateChildInfo()

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            'sandhya - to take care of session time out issues
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                Exit Sub
            End If

            objChild = New Child
            objChild.GetChildByID(Application("ConnectionString"), Session("CID"), Session("CustIndID"))
            Dim iMemberID As Double
            If Request.QueryString("Id") Is Nothing Then
                iMemberID = Session("CustIndID")
            Else
                iMemberID = Request.QueryString("Id")
            End If

            If txtDOB.Text = "" Then
                rfvBirthDate.IsValid = False
                rfvBirthDate.ErrorMessage = "Dat of Birth should not be Blank"
                Exit Sub
            End If

            If Not IsDate(txtDOB.Text) Then
                CustDOB.IsValid = False
                CustDOB.ErrorMessage = "Date of Birth Should be Valid Date in MM/DD/YYYY form"
                Exit Sub
            End If

            If ddlGrade.SelectedValue = -1 And ddlGrade.Enabled = True Then
                lblerr.Text = "Grade should be selected"
                Exit Sub
            End If

            If ddlGrade.SelectedValue = -1 Then
                'rfvGrade.IsValid = False
                'rfvGrade.ErrorMessage = "Please select appropriate Grade."
                'Exit Sub
            End If

        If Convert.ToDateTime(Server.HtmlEncode(txtDOB.Text)) > DateAdd(DateInterval.Year, -3, Now.Date()) Then
            CustDOB.IsValid = False
            CustDOB.ErrorMessage = "Child Should be atleast 3 Years old"
            Exit Sub
            End If
            Dim bIsValidate As Boolean
            bIsValidate = True
            Dim iChildAge As Integer
            iChildAge = DateDiff(DateInterval.Year, CDate(txtDOB.Text), CDate(Date.Now))
            If ((iChildAge < Convert.ToInt32(ddlGrade.SelectedValue) + 4) Or (iChildAge > Convert.ToInt32(ddlGrade.SelectedValue) + 6)) Then
                pnlMessage.Visible = True
                bIsValidate = False
                If rblOptions.SelectedValue = "Correct" Then
                    bIsValidate = True
                Else
                    bIsValidate = False
                End If
            End If
            Dim bIsDuplicate As Boolean
            Dim strSql As String
            strSql = "SELECT * FROM CHILD WHERE memberid = " & iMemberID & " AND UPPER(FIRST_NAME) = '" & txtFirstName.Text.ToUpper() & "' AND ChildNumber <>" & Session("CID")
            Dim drChildInfo As SqlDataReader
            drChildInfo = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drChildInfo.Read() Then
                lblDuplicateMessage.Visible = True
                lblDuplicateMessage.Text = "Name already Exists."
                bIsDuplicate = True
            End If
            If bIsDuplicate = False Then
                If bIsValidate = True Then
                    With objChild
                        .MEMBERID = CType(iMemberID, Integer)
                        .SPOUSEID = CType(Session("CustSpouseID"), Integer)
                        .FIRST_NAME = StrConv(Server.HtmlEncode(txtFirstName.Text), vbProperCase)
                        .LAST_NAME = StrConv(Server.HtmlEncode(txtLastName.Text), vbProperCase)
                        .MIDDLE_INITIAL = StrConv(Server.HtmlEncode(txtMiddleName.Text), vbProperCase)
                        .GENDER = ddlGender.SelectedValue
                        .DATE_OF_BIRTH = Convert.ToDateTime(Server.HtmlEncode(txtDOB.Text))
                        .GRADE = ddlGrade.SelectedValue
                        .SchoolName = txtSchoolName.Text
                        .CountryOfBirth = ddlCountryofBirth.SelectedValue
                        .Acheivements = txtAchievements.Text
                        .Hobbies = txtHobbies.Text
                        .ModifyDate = Now
                        If .CreateDate.ToString = "" Then
                            .CreateDate = .CreateDate
                        Else
                            .CreateDate = Now
                        End If
                        .UpdateChild(Application("ConnectionString"))
                    End With
                    If Not Request.QueryString("Id") Is Nothing Then
                        Response.Redirect("MainChild.aspx?ID=" & Request.QueryString("Id"))
                    Else
                        Response.Redirect("MainChild.aspx")
                    End If
                End If
            End If
        End Sub
    End Class
End Namespace

