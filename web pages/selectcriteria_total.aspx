<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!--#include file="finals_header.aspx"-->

<div class="title02">Finals Selection Criteria for Total List</div>

<div class="txt01" align="left">
<p>
For the benefit of parents and contestants alike, here�s a clarification on the National Finalists selection process: 
</p>
<span class="title04">Principles:</span>
<ul>
<li>Our selection process should be fair and non-discriminatory.</li>
<li>Higher grade children should not be penalized by favoring lower grade kids.</li>
<li>In general the lower grade children have lesser chance of competing at the high end of the syllabus for a given contest, therefore lower grade children should have reasonable representation so that they are not discouraged.</li>
<li>Chapters should have a reasonable representation.</li>
</ul>

<span class="title04">Guidelines:</span>
<ul>
<li>Percentage of invitees should be highest at the highest grade. For lower grades, if the representation (percentage) is much lower, the cut-off score will be lowered so that representation becomes reasonable.</li>
<li>
In case of lowered cut-offs for lower grades, rank 1 (defined only where #of contestants in that contest at that center >5) should be invited if the rank1 score meets a minimum threshold. The minimum threshold should be lower than the uniform cut-off which is the score for the highest grade.
</li>
<li>For newer chapters and host centers, rank 1 (defined only where #of contestants in that contest at that center >5) should be invited if the rank1 score meets a new chapter minimum threshold, which should be lower than the uniform cut-off or the lowest cut-off in the sliding scale (whichever is the minimum).</li>
</ul>

<span class="title04">Results:</span>
<ul>
<li>
Since ranks are not comparable across chapters, we use scores for determining the invitees as shown in the table below.
</li>
<li>
However, where necessary we have lowered the cutoffs to invite contestants from lower grades as well.  While we would like to encourage younger contestants to participate, we will not be giving grade-based trophies for each contest. At the Finals, we will be giving out �top 10� rank trophies for contests in spelling, vocabulary and math. In the case of Geography, only top 5 ranks will be recognized with trophies.  In the case of Essay, Public Speaking and Brain Bee, only top 3 ranks will be recognized with trophies.  Monetary awards apply only to the top 3 ranks in all contests except in Brain Bee.  Rank certificates will be distributed along with trophies.
</li>
<li>For Senior Essay writing (EW3) and Senior Public Speaking (PS3), all participants will be invited to Finals � in addition, others who fall in the grade range (Grade 9-12) can also apply. However, registration is limited to a total of 30 contestants in each contest.
</li>
<li>
As in previous years, we will have Brain Bee (Grade 8-12) again this year with a limit of 30 contestants.
</li>
</ul>

<span class="title04">National Finals Counts and Cutoffs for Total List:</span>
<%
	Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
	Dim queryYear As String
    
    cn.Open()

    'queryYear = "2011"
   
    Dim cmdyear As SqlCommand = New SqlCommand("Select top 1 ContestYear From InviteeCount Where List='Total' order by ContestYear desc", cn)
    queryYear = Convert.ToInt32(cmdyear.ExecuteScalar())
%>
 <span class="title04"><% =queryYear %></span>

<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td height="24" class="btn_06">Contest</td>
		<td class="btn_06">Grade</td>
        <td class="btn_06">Min Score</td>
		<td class="btn_06">Count</td>
		<td class="btn_06">Cumulative</td>
		<td class="btn_06">Num of Contestants</td>
		<td class="btn_06">% Invited</td>
		<td class="btn_06">Incr. Rank 1</td>
		<td class="btn_06">Incr. New Chapter</td>
		<td class="btn_06">Cum with Rank 1/New Chap</td>
		<td class="btn_06">Avg Score</td>
    </tr>
<%
    strquery = "select * from InviteeCount where ContestYear = " & queryYear & " and List='Total'"
	
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
	While rs.read()
%>
	<tr>
		<td bgcolor="#FFFFFF"><%=rs(1)%></td>
		<td bgcolor="#FFFFFF"><%=rs(2)%></td>
		<td bgcolor="#FFFFFF"><%=rs(3)%></td>
		<td bgcolor="#FFFFFF"><%=rs(4)%></td>
		<td bgcolor="#FFFFFF"><%=rs(5)%></td>
		<td bgcolor="#FFFFFF"><%=rs(6)%></td>
		<td bgcolor="#FFFFFF"><%=rs(7)%></td>
		<td bgcolor="#FFFFFF"><%=rs(8)%></td>
		<td bgcolor="#FFFFFF"><%=rs(9)%></td>
		<td bgcolor="#FFFFFF"><%=rs(10)%></td>
		<td bgcolor="#FFFFFF"><%=rs(11)%></td>
	</tr>
<%
	End While
	cn.Close()
%>

</table>

<ul>
<li><span class="txt01_strong">Min Score:</span> Children are invited to the Finals if they meet the Minimum Score (min score)</li>
<li><span class="txt01_strong">Count:</span> Number of Children invited</li>
<li><span class="txt01_strong">Cumulative:</span> Number of Children invited at or below that Grade</li>
<li><span class="txt01_strong">Num of Contestants:</span> Number of Children who Participated in the regional contests
across the country</li>
<li><span class="txt01_strong">% Invited:</span> (Count/Num of Contestants)*100</li>
<li><span class="txt01_strong">Incr. Rank 1:</span> Rank 1 holders who meet the lowest Min Score</li>
<li><span class="txt01_strong">Cum with Rank1/New Chap:</span> Total Invitees</li>
<li><span class="txt01_strong">Avg Score:</span> Average score of the children for a given contest participated in the regionals  across the country</li>
</ul>

</div>

<!--#include file="finals_footer.aspx"-->