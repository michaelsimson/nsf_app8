<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!--#include file="finals_header.aspx"-->

<div class="title02">Finals Results - 
<%
    Dim Year As integer= Now.Year()
    If Now.Month >= 1 And Now.Month <= 7 Then
        Year = Now.Year() - 1
    End If
 %>
 <%=Year%>

</div>

<div class="btn_01" align="left">
<!-- Spelling Bee -->
	<span class="title04">Spelling: </span>
	<a href="#JSB"> Junior </a>
	| <a href="#SSB"> Senior </a>
	
	<!-- Vocabulary Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04"> Vocabulary: </span>
	<a href="#JVB"> Junior </a>
	| <a href="#IVB"> Intermediate </a>

	<!-- Geography Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04"> Geography: </span>
	<a href="#JGB"> Junior</a>
	| <a href="#SGB"> Senior </a>
	
	<br />
	<!-- Math Bee -->
	<span class="title04">Math: </span>
	<a href="#MB1"> Level 1</a>
	| <a href="#MB2"> Level 2</a>
	| <a href="#MB3"> Level 3</a>
	
	<!-- Science Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04">Science: </span>
	<a href="#JSC"> Junior </a>
	| <a href="#ISC"> Intermediate </a>
	| <a href="#SSC"> Senior </a>
	
	<br />
	<span class="title04">Essay Writing: </span>
	<a href="#EW3"> Senior</a>
	
	&nbsp;&nbsp;&nbsp;<span class="title04">Public Speaking: </span>
	<a href="#EW3"> Senior</a>
	
	&nbsp;&nbsp;&nbsp;<span class="title04">Brain Bee: </span>
	<a href="#BB"> Senior</a>
</div>

<div class="txt01" align="justify" style="font-size:medium">

<table class="title03" style="color:GrayText;font-size:medium;font-bold:false" ><tr><td></td></tr><tr><td>
Please click <a href="DisplayPercentilesNationals.aspx"><u><b>here</b></u></a>
to view the overall percentile values for each contest.
</td></tr></table> 
<table>

<%
	Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
	Dim productName As String
	Dim productNew As String
    
    cn.Open()

	productName = ""
    'previously used YEAR(GETDATE()) in the query, but from Jan 1, it will stop showing previous finals results
	'so hardcoded and has to be changed whenever we have the new finals results.
    strquery = "select a.rank, a.productcode,P.Name as ProductName, " & _
               "c.First_Name, c.Last_Name, a.Grade, " & _
                  "b.city, b.state, Ch.ChapterCode " & _
         "from contestant a Inner Join indspouse b on b.automemberid = a.parentid " & _
         "Inner Join child c on c.childnumber=a.childnumber " & _
         "Inner JOIN Product P ON P.Productcode = A.ProductCode and P.EventID=A.EventID Left JOIN  contestant ad ON ad.ProductCode = a.ProductCode AND ad.EventId = 2 AND ad.ContestYear = a.ContestYear  AND a.ChildNumber = ad.ChildNumber " & _
         "INNER JOIN Chapter Ch ON Ch.ChapterId=CASE WHEN ad.ChapterID IS NULL THEN b.ChapterID  ELSE ad.ChapterID END " & _
         "where a.contestyear = " & Year & " and a.chapterid=1 " & _
         "and a.PaymentReference <> '' and ((a.ProductCode not in ('EW3', 'PS3','BB') AND  a.rank between 1 and 10) OR  (a.ProductCode in ('EW3', 'PS3','BB') AND  a.rank between 1 and 3)) " & _
         "order by a.productid, a.rank"
	
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
	While rs.read()
		productNew = rs(2)
		
		if not productName = productNew then
		productName = productNew
%>
	</table>
	<table class="title02"><tr><td><%=rs(2)%> </td></tr></table>
	<%-- <a name="<%=rs(1)%>" style="font-size:large"></a>--%>
	<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td width="10%" height="24" class="btn_06">&nbsp;&nbsp;&nbsp;Rank</td>
		<td width="40%" class="btn_06">&nbsp;&nbsp;&nbsp;Contestant Name</td>
		<td width="10%" class="btn_06">&nbsp;&nbsp;&nbsp;Grade</td>
		<td width="20%" class="btn_06">&nbsp;&nbsp;&nbsp;City, State</td>
		<td width="20%" class="btn_06">&nbsp;&nbsp;&nbsp;NSF Chapter</td>
    </tr>
<%
	End If
%>
	<tr>
		<td bgcolor="#FFFFFF"><%=rs(0)%></td>
		<td bgcolor="#FFFFFF"><%=rs(3)%>,&nbsp;<%=rs(4)%></td>
		<td bgcolor="#FFFFFF"><%=rs(5)%></td>
		<td bgcolor="#FFFFFF"><%=rs(6)%>,&nbsp;<%=rs(7)%></td>
		<td bgcolor="#FFFFFF"><%=rs(8)%></td>
	</tr>
<%
	End While
	cn.Close()
%>

</table>
</div>

<!--#include file="finals_footer.aspx"-->