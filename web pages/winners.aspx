<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!--#include file="regionals_header.aspx"-->

<div class="title02">Regional Placements - 
<%
    Dim Year As integer= Now.Year()
    If Now.Month = 1 Or Now.Month = 2 Then
        Year = Now.Year() - 1
    End If
 %>
 <%=Year%>
</div>

<div class="btn_01" align="left">

	<%
	Dim pageurl As String
	pageurl = "winners.aspx"
	%>
	
	<!-- Spelling Bee -->
	<% if request.querystring("contest") = "Junior Spelling" then%>
	Junior Spelling
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Spelling")%>> Junior Spelling </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Spelling" then%>
	| Senior Spelling
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Spelling")%>> Senior Spelling </a>
	<% end if %>
	
	<!-- Vocabulary Bee -->
	<% if request.querystring("contest") = "Junior Vocabulary" then%>
	| Junior Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Vocabulary")%>> Junior Vocabulary </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Vocabulary" then%>
	| Intermediate Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Vocabulary")%>> Intermediate Vocabulary </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Vocabulary" then%>
	| Senior Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Vocabulary")%>> Senior Vocabulary </a>
	<% end if %>
	
	<br />
	<!-- Math Bee -->
	<% if request.querystring("contest") = "Math Level 1" then%>
	Math Level 1
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 1")%>> Math Level 1</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Math Level 2" then%>
	| Math Level 2
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 2")%>> Math Level 2</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Math Level 3" then%>
	| Math Level 3
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 3")%>> Math Level 3</a>
	<% end if %>
	
	<!-- Geography Bee -->
	<% if request.querystring("contest") = "Junior Geography" then%>
	| Junior Geography
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Geography")%>> Junior Geography</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Geography" then%>
	| Senior Geography
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Geography")%>> Senior Geography</a>
	<% end if %>
	
	<br />
	<!-- Public Speaking -->
	<% if request.querystring("contest") = "Junior Public Speaking" then%>
	Junior Public Speaking
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Public Speaking")%>> Junior Public Speaking </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Public Speaking" then%>
	| Senior Public Speaking
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Public Speaking")%>> Senior Public Speaking </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Junior Science" then%>
	| Junior Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Science")%>> Junior Science </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Science" then%>
	| Intermediate Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Science")%>> Intermediate Science </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Science" then%>
	| Senior Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Science")%>> Senior Science </a>
	<% end if %>
	
	<br />
	<!-- Essay Writing -->
	<% if request.querystring("contest") = "Junior Essay Writing" then%>
	Junior Essay Writing
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Essay Writing")%>> Junior Essay Writing </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Essay Writing" then%>
	| Intermediate Essay Writing
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Essay Writing")%>> Intermediate Essay Writing </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Essay Writing" then%>
	| Senior Essay Writing
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Essay Writing")%>> Senior Essay Writing </a>
	<% end if %>
</div>
<div>
<table class="title03" style="color:GrayText;font-size:medium;font-bold:false" ><tr><td></td></tr><tr><td>
Please click <a href="DisplayPercentiles.aspx"><u><b>here</b></u></a> to view the overall percentile values for each contest. 
</td></tr></table></div> 
<div class="txt01_strong" align="justify">
Note: The placements are subject to further verification. The following are just placements in each contest/center and do not correspond to the announced ranks at the given center. Ranks were given out at different centers, depending on the number of contestants present. This list also does not correspond to the national finalists. These will be announced separately based on national cutoff scores.
</div>
<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td width="25%" height="24" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Center</td>
		<td width="35%" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Participant Name</td>
        <td width="20%" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Contest</td>
        <td width="10%" class="btn_06">&nbsp;&nbsp;Place&nbsp;&nbsp;</td>
		<td width="10%" class="btn_06">&nbsp;&nbsp;Grade&nbsp;&nbsp;</td>
    </tr>
	
<%
	Dim queryContest As String
	Dim IsAnyError As Boolean
	
	IsAnyError = True
	queryContest = ""
	
	If Request.QueryString.Count = 0 Then
		queryContest = ""
		IsAnyError = False

	ElseIf Request.QueryString.Count = 1 Then
    
		'accept only one querystring value
		'check if contest value included - only one contest value
		If Request.QueryString("contest").Length > 0 Then
			'contest value can not be empty
			If Request.querystring("contest") <> "" then
				'read contest value
				queryContest = Request.querystring("contest")
				'check if state name is in our States list
				Select Case queryContest
				Case "Junior Spelling Bee", "Senior Spelling Bee", "Junior Vocabulary Bee", "Intermediate Vocabulary Bee", "Senior Vocabulary Bee", "Junior Spelling", "Senior Spelling", "Junior Vocabulary", "Intermediate Vocabulary", "Senior Vocabulary"
					IsAnyError = False
				Case "Math Level 1", "Math Level 2", "Math Level 3", "Math Level 4", "Junior Public Speaking", "Senior Public Speaking"
					IsAnyError = False
				Case "Junior Geography Bee", "Senior Geography Bee", "Junior Essay Writing", "Intermediate Essay Writing", "Senior Essay Writing", "Junior Geography", "Senior Geography"
					IsAnyError = False
				Case "Junior Science", "Intermediate Science", "Senior Science"
					IsAnyError = False
				Case else
					IsAnyError = True
			   End Select
			End If
		End if
	End If

	If IsAnyError = True Then
		response.redirect("http://www.northsouth.org")
	End If

	Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
    
    cn.Open()
    Try
        If queryContest = "" Then
            strquery = "select register_for,[Participant Name],grade,center,rank from  v_Registration_details_New where rank in (1,2,3) and year = " & Year & " order by center,register_for,rank"
        Else
            strquery = "select register_for,[Participant Name],grade,center,rank from  v_Registration_details_New where rank in (1,2,3) and year = " & Year & " and register_for='" & queryContest & "' order by center,rank"
        End If
    Catch ex As Exception
        Response.Write("Contest details not yet updated")
    End Try
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
    While rs.Read()
%>
	<tr>
		<td bgcolor="#FFFFFF"><%=rs(3)%></td>
		<td bgcolor="#FFFFFF"><%=rs(1)%></td>
		<td bgcolor="#FFFFFF"><%=rs(0)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs(4)%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs(2)%></td>
	</tr>
<%
	End While
	cn.Close()
%>
</table>
	

<!--#include file="regionals_footer.aspx"-->