<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!--#include file="finals_header.aspx"-->

<div class="title02">Priority List of Finals Invitees 
<%
    Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)

    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
    Dim queryYear As String
     
    cn.Open()

    Dim cmdyear As SqlCommand = New SqlCommand("Select top 1 ContestYear From Contestant Where PriorityInvite =1 order by ContestYear desc", cn)
    queryYear = Convert.ToInt32(cmdyear.ExecuteScalar())
    
%>
<%=queryYear%>
</div>
<div class="txt01" align="justify">

<div class="btn_01" align="left">

<%
Dim lContestName(99),lContestId As String

lContestName(19) = "Junior Spelling"
lContestName(20) = "Senior Spelling"
lContestName(21) = "Junior Vocabulary"
lContestName(22) = "Intermediate Vocabulary"
lContestName(23) = "Senior Vocabulary"
lContestName(24) = "Math Level 1"
lContestName(25) = "Math Level 2"
lContestName(26) = "Math Level 3"
lContestName(27) = "Math Level 4"
lContestName(28) = "Junior Geography"
lContestName(29) = "Senior Geography"
lContestName(30) = "Senior Essay Writing"
lContestName(31) = "Senior Public Speaking"
lContestName(52) = "Junior Science"
lContestName(53) = "Intermediate Science"
lContestName(54) = "Senior Science"

lContestId = request.querystring("contest")
%>

	<%
	Dim pageurl As String
	pageurl = "finalists.aspx"
	%>
	
	<!-- Spelling Bee -->
	<span class="title04">Spelling: </span>
	<% if request.querystring("contest") = "19" then%>
	Junior
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("19")%>> Junior </a>
	<% end if %>
	
	<% if request.querystring("contest") = "20" then%>
	| Senior
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("20")%>> Senior </a>
	<% end if %>
	
	<!-- Vocabulary Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04"> Vocabulary: </span>
	<% if request.querystring("contest") = "21" then%>
	Junior
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("21")%>> Junior </a>
	<% end if %>
	
	<% if request.querystring("contest") = "22" then%>
	| Intermediate
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("22")%>> Intermediate </a>
	<% end if %>
	
	<% if request.querystring("contest") = "23" then%>
	| Senior
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("23")%>> Senior </a>
	<% end if %>
	
	<!-- Geography Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04"> Geography: </span>
	<% if request.querystring("contest") = "28" then%>
	Junior
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("28")%>> Junior</a>
	<% end if %>
	
	<% if request.querystring("contest") = "29" then%>
	| Senior
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("29")%>> Senior </a>
	<% end if %>
	
	<br>
	<!-- Math Bee -->
	<span class="title04">Math: </span>
	<% if request.querystring("contest") = "24" then%>
	Level 1
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("24")%>> Level 1</a>
	<% end if %>
	
	<% if request.querystring("contest") = "25" then%>
	| Level 2
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("25")%>> Level 2</a>
	<% end if %>
	
	<% if request.querystring("contest") = "26" then%>
	| Level 3
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("26")%>> Level 3</a>
	<% end if %>
	
	<!-- Science Bee -->
	&nbsp;&nbsp;&nbsp;<span class="title04">Science: </span>
	<% if request.querystring("contest") = "52" then%>
	Junior
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("52")%>> Junior </a>
	<% end if %>
	
	<% if request.querystring("contest") = "53" then%>
	| Intermediate
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("53")%>> Intermediate </a>
	<% end if %>
	
	<% if request.querystring("contest") = "54" then%>
	| Senior
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("54")%>> Senior </a>
	<% end if %>
</div>
<p>
Please click <a class="btn_02" href="selectcriteria.aspx">here</a> to see the selection criteria for the finals.
</p>
<%
	Dim queryContest As String
	Dim IsAnyError As Boolean
	Dim ContestValue As String
	
	IsAnyError = True
	queryContest = ""
	
	If Request.QueryString.Count = 0 Then
		queryContest = ""
		IsAnyError = False

	ElseIf Request.QueryString.Count = 1 Then
    
		'accept only one querystring value
		'check if contest value included - only one contest value
		If Request.QueryString("contest").Length > 0 Then
			'contest value can not be empty
			If Request.querystring("contest") <> "" then
				'read contest value
	            ContestValue = request.querystring("contest")

	            'expect 2 characters only
	            If Len(ContestValue) = 2 Then
	                'contest value must be integer
	                If IsNumeric(ContestValue) Then
	                    'convert ContestValue to integer
	                    ContestValue = CInt(ContestValue)
	                    'value must be between 19 and 54
	                    if ContestValue >= 19 and ContestValue <=54 Then
	                        'queryContest = " and CTST.ContestCategoryId = " & ContestValue
	                        queryContest = " and CTST.ProductId = " & ContestValue
	                        IsAnyError = False
	                    End if
	                End If
	           End If
			End If
		End if
	End If

	If IsAnyError = True Then
		response.redirect("http://www.northsouth.org")
	End If

%>
<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td width="30%" height="24" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Center</td>
		<td width="45%" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Participant Name</td>
        <td width="25%" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Contest</td>
    </tr>
    <%
        strquery = "select CHLD.ChildNumber, CHLD.First_Name, CHLD.Last_Name" & _
            ", CHLD.Grade , CHPT.ChapterID, CHPT.ChapterCode, CTST.ContestYear" & _
                  ", CTST.ProductCode, CTGY.ContestDesc, CTST.Productid" & _
                  " From   dbo.Contestant CTST" & _
                  " Inner Join dbo.Child CHLD ON CTST.ChildNumber = CHLD.ChildNumber" & _
                  " INNER JOIN dbo.ContestCategory CTGY ON " & _
                  " CTGY.Contestcategoryid = CTST.ContestCategoryID " & _
                  " INNER JOIN dbo.Chapter   CHPT ON CHPT.ChapterID = CTST.ChapterID" & _
                  " where CTST.ContestYear = " & queryYear & _
                  " And CTST.PriorityInvite = 1 " & queryContest & _
                  " order by ctst.contestcategoryid, chpt.chaptercode, chld.first_name" 'and CTST.nationalinvitee = 1
	
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
	While rs.read()
%>
	<tr>
		<td bgcolor="#FFFFFF"><%=rs(5)%></td>
		<td bgcolor="#FFFFFF"><%=rs(1)%>&nbsp;<%=rs(2)%></td>
		<td bgcolor="#FFFFFF"><%=rs(8)%></td>
	</tr>
<%
	End While
	cn.Close()
%>

</table>
</div>

<!--#include file="finals_footer.aspx"-->