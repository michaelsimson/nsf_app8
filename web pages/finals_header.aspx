<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/other.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation : Finals</title>
<link href="/public/css/style.css" rel="stylesheet" type="text/css" />
<link href="/public/css/menu_style.css" rel="stylesheet" type="text/css" />
<link href="/public/css/glossymenu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/public/js/layer.js"></script>
<script type="text/javascript" src="/public/js/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/ddaccordion.js"></script>
<script type="text/javascript" src="/public/js/nsfddaccordion.js"></script>
<script type="text/javascript" src="/public/js/imageswap.js"></script>
</head>
<body>
<!--#include file="/public/main/header.aspx"-->

                <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td valign="top"><!-- InstanceBeginEditable name="EditRegion2" -->
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                
                                <tr>
                                  <td width="4%">&nbsp;</td>
                                  <td width="96%"><table width="88%" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td>
                                          <div class="glossymenu">
                                            <a class="menuheader" href="/public/USContests/Finals/nationalfinals.aspx">National Finals</a>
                                            						<a class="menuitem" href="/public/USContests/Finals/parentcommunications.aspx">Parents Communications</a> 
											<a class="menuitem" href="/public/USContests/Finals/Results.aspx">Results</a> 
											<a class="menuitem" href="/public/USContests/Finals/wordlist.aspx">Word Lists</a> 
											<a class="menuitem submenuheader" href="#" >Finalists</a>
                                            <div class="submenu">
                                                <ul>
                                                    <li><a href="/public/USContests/Finals/finalists.aspx">Priority List</a></li>
                                                    <li><a href="/public/USContests/Finals/finalists_total.aspx">Total List</a></li>
                                                </ul>
                                            </div>
											<a class="menuitem" href="/public/USContests/Finals/schedule.aspx">Program Schedule</a> 
                                            <a class="menuitem" target="_blank" href="https://www.northsouth.org/app9/login.aspx?entry=p">Register Online</a>
											<a class="menuitem" href="/public/USContests/Finals/general_rules.aspx">General Rules</a> 
                                            <a class="menuitem submenuheader" href="#" >Contests</a>
                                            <div class="submenu">
                                                <ul>
                                                    <li><a href="/public/USContests/Finals/spelling/spelling.aspx">Spelling</a></li>
                                                    <li><a href="/public/USContests/Finals/vocabulary/vocabulary.aspx">Vocabulary</a></li>
                                                    <li><a href="/public/USContests/Finals/math/math.aspx">Math</a></li>
                                                    <li><a href="/public/USContests/Finals/science/science.aspx">Science</a></li>
                                                    <li><a href="/public/USContests/Finals/geography/geography.aspx">Geography </a></li>
                                                    <li><a href="/public/USContests/Finals/essaywriting/essaywriting.aspx">Essay Writing</a></li>
                                                    <li><a href="/public/USContests/Finals/publicspeaking/publicspeaking.aspx">Public Speaking</a></li>
													<li><a href="/public/USContests/Finals/brainbee/brainbee.aspx">Brain Bee</a></li>
                                                </ul>
                                            </div>
											<a class="menuitem" href="/public/USContests/Finals/venue.aspx">Venue / Directions / Parking</a>
											<a class="menuitem" href="/public/USContests/Finals/airlines.aspx">Airlines / Car / Hotel</a>
											<a class="menuitem" href="/public/USContests/Finals/explore.aspx">Explore Ann Arbor/ Metro Detroit</a>
											<a class="menuitem" href="/public/USContests/Finals/beearticles.aspx">Bee Book Articles</a>
											<a class="menuitem" href="/public/USContests/Finals/beephoto.aspx">Bee Book Photo Instructions</a>
											<a class="menuitem" href="/public/USContests/Finals/finalsteam.aspx">Finals Team</a> 
											<a class="menuitem" href="/public/USContests/Finals/winners.aspx">Past Winners</a> 
											<!-- <a class="menuitem" href="/public/USContests/Finals/sponsors.aspx">Sponsors</a> -->
										  </div>
                                          </td>
                                        </tr>
                                      </table></td>
                                    </tr>
                                    
                                  </table></td>
                                </tr>
                            </table>
                            <!-- InstanceEndEditable --></td>
                          </tr>
                          <tr>
                            <td align="center">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center">
                            <!--#include file="/public/main/qlinkinc.aspx"-->
                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td>