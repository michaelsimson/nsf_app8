 <%@ Page Language="C#" runat="server"%>
 <%@ Import Namespace="System.Data"%>
 <%@ Import Namespace="System.Data.SqlClient"%>
 
<!--#include file="regionals_header.aspx"-->

<script runat="server" >

    int Year;//= DateTime.Now.Year;//Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString());
    String ProductCode;
    string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString);
                connection.Open();
                    SqlCommand cmdyear = new SqlCommand("Select distinct top 1 Contestyear from PercentilesByGrade WHERE EventID=2 order by ContestYear desc", connection);
                    Year = Convert.ToInt32(cmdyear.ExecuteScalar());
                    lblYear.Text = Year.ToString();
                connection.Close();
            
                if (Request.QueryString["Contest"] == null)
                {
                    ProductCode = "JSB";
                }
                else if (Request.QueryString["Contest"] != null)
                {
                    ProductCode = Request.QueryString["Contest"];
                }
                ReadAllJSBScores();
            }
        }
        void ReadAllJSBScores()
        {
              // connect to the database
                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString);
                connection.Open();
 
                DataSet dsPercentiles = new DataSet();
                DataSet dsProductName = new DataSet();
                DataSet dsYear = new DataSet();
                try
                {
                    string commandString = "Select Distinct TotalScore,TotalPercentile as " + ProductCode + "_Percentile,Total_Count as " + ProductCode + "_Count,Total_Cume_Count as " + ProductCode + "_Cume_Count From PercentilesByGrade Where ContestYear=(Select distinct top 1 Contestyear from PercentilesByGrade WHERE EventID=2 order by ContestYear desc)" + //+ Year + //  Year + 
                      " AND ProductCode = '" + ProductCode + "' and ContestPeriod in (Select distinct top 1 ContestPeriod from PercentilesByGrade WHERE ProductCode='" + ProductCode + "' and EventID=2 order by ContestPeriod desc) And EventId=2 ORDER BY TotalScore";
                   
                    SqlDataAdapter daPercentiles = new SqlDataAdapter(commandString, connectionString);//Application["ConnectionString"].ToString());
                    dsPercentiles = new DataSet();
                    daPercentiles.Fill(dsPercentiles);

                    SqlCommand cmdProduct = new SqlCommand("Select Distinct Name from Product where ProductCode='" + ProductCode + "'", connection);
                    String ProductName = cmdProduct.ExecuteScalar().ToString();

                    if (ProductName != "")
                    {
                        //DGShowPercentiles.Caption = ProductName + " as of " + Year;
                        lblProductCode.Text = ProductName;
                        DGShowPercentiles.Font.Bold = true;
                    }

                    if (dsPercentiles.Tables[0].Rows.Count > 0)
                    {
                        DGShowPercentiles.Style.Add("Width", "100%");
                        DGShowPercentiles.Style.Add("Height", "100%");
                        DGShowPercentiles.Visible = true;
                        DGShowPercentiles.DataSource = dsPercentiles;
                        DGShowPercentiles.DataBind();
                    }
                    else
                    {
                        DGShowPercentiles.Visible = true;
                        DataTable dt = new DataTable();
                        dt.Columns.Add("No data to display");
                        DGShowPercentiles.Style.Add("Width", "50%px");
                        DGShowPercentiles.Style.Add("Height", "10%");
                        DGShowPercentiles.DataSource = dt;
                        DGShowPercentiles.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text =ex.ToString()+  "No Percentile Calculations done";
                }
                connection.Close();
        }

</script>


 <div class="title02">Percentile Values for Regional Contests <asp:Label ID="lblYear" runat="server"></asp:Label></div> 
    <table align="center" class="btn_01"><tr><td colspan="3">
           
                              <a href="DisplayPercentiles.aspx?Contest=JSB"> Junior Spelling  </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=SSB"> Senior Spelling </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=JVB"> Junior Vocabulary </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=IVB"> Intermediate Vocabulary </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=MB1"> Math Level 1</a>|
	                          <a href="DisplayPercentiles.aspx?Contest=MB2"> Math Level 2</a>|
	                          <a href="DisplayPercentiles.aspx?Contest=MB3"> Math Level 3</a>|
	                          <a href="DisplayPercentiles.aspx?Contest=JGB"> Junior Geography</a>|
	                          <a href="DisplayPercentiles.aspx?Contest=SGB"> Senior Geography</a>|
	                          <a href="DisplayPercentiles.aspx?Contest=JSC"> Junior Science </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=ISC"> Intermediate Science </a>|
	                          <a href="DisplayPercentiles.aspx?Contest=SSC"> Senior Science </a>

                    </td></tr> 
       <tr><td align="center"><asp:Label ID="lblProductCode" runat="server"  Font-Bold="False" Font-Size="Large" ForeColor="Green"></asp:Label></td></tr> 
 
        <tr>
             <td>  <asp:datagrid id="DGShowPercentiles"  runat="server" Visible="true" AutoGenerateColumns="true" 
		                    Width="945px" Height="304px" BorderColor="#E7E7FF" BorderStyle="Inset" BorderWidth="1px" BackColor="White"
		                    CellPadding="3" GridLines="Both" class="btn_01" Font-Bold="false">
		                    <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
		                    <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
		                    <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
		                    <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
		                    <HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#A9A6A6"></HeaderStyle>
		                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		                    <Columns>
		                    </Columns> 
		               </asp:datagrid> 
		    </td>
        </tr>
        <tr><td><asp:Label ID="lblError" runat="server" Text=""></asp:Label></td></tr>
   </table>   
         
<!--#include file="regionals_footer.aspx"-->

 
 
 