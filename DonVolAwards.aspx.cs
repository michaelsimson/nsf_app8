﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;

public partial class DonVolAwards : System.Web.UI.Page
{
    //CapeStrat//
    string ConnectionString = "ConnectionString";
    string strSql;
    string RecType;
    string AwardType;
    string sDatakey;
    string Upchapter;
    bool duplicateYear = false;
    bool DuplicateAwrard = false;
    bool ValidAwardType = false;
    string Anona;
    string SpouseUp;
    string space = "space";
    protected void GidRecType(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(TxtRecType1.Text))
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ddlTemp.Items.Insert(0, new ListItem("Donor", "1"));
            ddlTemp.Items.Insert(0, new ListItem("Volunteer", "2"));
            ddlTemp.Items.Insert(0, new ListItem("Select RecType", "0"));
        }
    }
    protected void AnonandSpueEdit(object sender, System.EventArgs e)
    {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ddlTemp.Items.Insert(0, new ListItem("Yes", "2"));
            ddlTemp.Items.Insert(0, new ListItem("No", "1"));
            ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
     
    }
   
    protected void DDChapterEdit(object sender, System.EventArgs e)
    {

        try
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            string DDLstate = "select state,ChapterID,chaptercode from chapter order by State,ChapterCode ";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
            ddlTemp.DataSource = dsstate;
            ddlTemp.DataTextField = "chaptercode";
            ddlTemp.DataValueField = "ChapterID";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "Select Chapter");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
        {
            Response.Redirect("~/login.aspx?entry=p");
        }
        
            if (!IsPostBack)
            {
                Yearscount();
                GridData();
            }
    }
    protected void GridData()
    {
        AwardsDetails("Donor", GridDonor);
        AwardsDetails("Volunteer", GridVolunter);
    }
    protected void Yearscount()
    {
        try
        {
            DDYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = 2012; i <= DateTime.Now.Year; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DDYear.DataSource = list;
            DDYear.DataTextField = "Text";
            DDYear.DataValueField = "Value";
            DDYear.DataBind();
            DDYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            SessionExp();
        }

    }
    protected void DropTypeSelect(DropDownList DDLTypeSelect,int VlaueField)
    {
        try
        {
            ArrayList list = new ArrayList();
            if (VlaueField == 1)
            {
                list.Add(new ListItem("Honorable Donor Award", "1"));
                list.Add(new ListItem("Distinguished Donor Award", "2"));
                list.Add(new ListItem("Lifetime Donor Award", "3"));
            }
            else
            {
                list.Add(new ListItem("Distinguished Service Award", "1"));
                list.Add(new ListItem("Lifetime Achievement Award", "2"));
                list.Add(new ListItem("Founder’s Award", "3"));
            }
            DDLTypeSelect.Items.Clear();
            DDLTypeSelect.DataSource = list;
            DDLTypeSelect.DataTextField = "Text";
            DDLTypeSelect.DataValueField = "Value";
            DDLTypeSelect.DataBind();
            DDLTypeSelect.Items.Insert(0, new ListItem("Select Choice", "-1"));
            DDLTypeSelect.Visible = true;
            LbType.Visible = true;
        }
        catch
        {
            SessionExp();
        }
    }
    private void SessionExp()
    {
        if (Session["LoggedIn"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ChoiceVal = Convert.ToInt16(ddlChoice.SelectedValue);
        DropTypeSelect(DDTypeselection, ChoiceVal);
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        try
        {
            States();
            clearFunction();
            txtName.Text = string.Empty;
            TextMemberIDVal.Text = string.Empty;
            pIndSearch.Visible = true;
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void States()
    {
        try
        {
            string DDLstate = "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
            ddlState.DataSource = dsstate;
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "StateCode";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "Select State");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void ddlChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ChoiceVal =Convert.ToInt16(ddlChoice.SelectedValue);
        DropTypeSelect(DDTypeselection,ChoiceVal);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Lbsearchresult.Text = "";
            string firstName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            string state = string.Empty;
            firstName = txtFirstName.Text;
            lastName = txtLastName.Text;
            email = txtEmail.Text;
            int length = 0;
            if (firstName.Length > 0)
            {
                strSql += ("I.firstName like '%" + firstName + "%'");
            }
            if (lastName.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += (" and I.lastName like '%" + lastName + "%'");
                }
                else
                {
                    strSql += ("  I.lastName like '%" + lastName + "%'");
                }
            }

            if (email.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and I.Email like '%" + email + "%'");
                }
                else
                {
                    strSql += ("  I.Email like '%" + email + "%'");
                }
            }
            if (ddlState.SelectedItem.Text != "Select State")
            {
                length = strSql.ToString().Length;
                state = ddlState.SelectedValue;
                if (length > 0)
                {
                    strSql += ("  and I.state like '%" + state + "%'");
                }
                else
                {
                    strSql += ("   I.state like '%" + state + "%'");
                }
            }
            if ((firstName.Length > 0) || (lastName.Length > 0) || (email.Length > 0) || (state.Length > 0))
            {
                strSql += ("  order by I.lastname,I.firstname");
                SearchMembers(strSql);
            }
            else
            {
                lbsearchresults.Text = "Please Enter Email / First Name / Last Name / state";
                lbsearchresults.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void SearchMembers(string StrSearchcond)
    {
        try
        {
            string    StrQrySearch = "select I.automemberid,I.Employer,I.firstname,I.lastname,I.donortype,I.email,I.hphone,I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID where  " + StrSearchcond + "";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
            {
                lbsearchresults.Visible = true;
                lbsearchresults.Text = "No Search found ";
            }
            else
            {
                lbsearchresults.Visible = false;
            }
          
            Panel4.Visible = true;
            GridSearchSoln.Visible = true;
            GridSearchSoln.DataSource = ds;
            Session["GridSearchDetails"] = ds;
            GridSearchSoln.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void AwardsDetails(string RecTypeVal,GridView GvRAwardDisp)
    {
        try
        {
            string StrQrySearchDonor = "select I.Firstname,I.lastname ,I.city,I.state,D.DonVolAwardsID,D.[YEAR],D.chapter,D.MemberID,Anon,Spouse,D.DonorType,D.RecType," +
                                  "D.AwardsType from donvolawards D left join indspouse I on I.automemberid=D.memberid where D.RecType='" + RecTypeVal 
                                  + "' order by D.year desc,D.AwardsType,I.LastName,I.FirstName";
           
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchDonor);
            if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
            {
                lbsearchresults.Visible = true;
            }
            else
            {
                lbsearchresults.Visible = false;
            }

            GvRAwardDisp.Visible = true;
            GvRAwardDisp.DataSource = ds;
            DataTable DtnAward = ds.Tables[0];
            Session["AwardDetails"] = DtnAward;
            GvRAwardDisp.DataBind();
            
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
      protected void GridSearchSoln_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GridSearchSoln.Rows[index];
            GridSearchSoln.Rows[index].BackColor = System.Drawing.Color.SkyBlue;

            for (int i = 0; i <= GridSearchSoln.Rows.Count - 1; i++)
            {
                if (i != index)
                {
                    GridSearchSoln.Rows[i].BackColor = System.Drawing.Color.White;
                }
            }
            string StrFirstName = string.Empty;
            string StrLastName = string.Empty;
            StrFirstName = row.Cells[1].Text.Replace(space, string.Empty);
            StrLastName = row.Cells[2].Text.Replace(space, string.Empty);
            txtName.Text = StrFirstName + ' ' + StrLastName;
            ViewState["MemberID"] = GridSearchSoln.DataKeys[index].Value;
            ViewState["DonorType"] = row.Cells[3].Text.Replace(space, string.Empty);
            ViewState["ChapterId"] = row.Cells[11].Text.Replace(space, string.Empty);
            ViewState["FirstName"] = row.Cells[1].Text.Replace(space, string.Empty);
            ViewState["LastName"] = row.Cells[2].Text.Replace(space, string.Empty);
            ViewState["City"] = row.Cells[8].Text.Replace(space, string.Empty);
            ViewState["State"] = row.Cells[9].Text.Replace(space, string.Empty);
            if (!string.IsNullOrEmpty(TextMemberIDVal.Text))
            {
                TextMemberIDVal.Text = Convert.ToString(GridSearchSoln.DataKeys[index].Value);
            }
            txtName.Enabled = false;
            pIndSearch.Visible = false;
            Panel4.Visible = false;
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                AddRecord();
            }
            else
            {
                Lbsearchresult.Visible = true;
                Lbsearchresult.Text = "Please click Search button and find your name";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void DuplicateAwardDetailsYear(int MemberID,int Year)
    {
        try
        {
            string QryDuplicateAward = "select  donvolawardsid, AwardsType from  donvolawards where memberid=" + MemberID + " and  year=" + Year + "";
            DataSet dsDuplicte = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, QryDuplicateAward);
            if (null != dsDuplicte && dsDuplicte.Tables.Count > 0 && dsDuplicte.Tables[0].Rows.Count > 0)
            {
                 duplicateYear = true;
                LbDuplicateresults.Visible = true;
                LbDuplicateresults.Text = "A person cannot receive more than one award in a year.";
            }
            else
            {
                
                duplicateYear = false;
                LifetimeDuplicateAwardDetails(Convert.ToInt32(ViewState["MemberID"]), DDTypeselection.SelectedItem.Text);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void LifetimeDuplicateAwardDetails(int MemberID, string AwardType)
    {
        try
        {
            string QryDuplicateAward = "select  donvolawardsid, AwardsType from  donvolawards where memberid=" + MemberID + " and  AwardsType='" + AwardType + "'";
            DataSet dsDuplicte = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, QryDuplicateAward);
            if (null != dsDuplicte && dsDuplicte.Tables.Count > 0 && dsDuplicte.Tables[0].Rows.Count > 0)
            {
                DuplicateAwrard = true;
                LbDuplicateresults.Visible = true;
                LbDuplicateresults.Text = "A person cannot receive the same award more than once during lifetime.";
            }
            else
            {
                DuplicateAwrard = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void AddRecord()
    {
        try
        {
            string RecType = string.Empty;
            string Anon;
            string spouse;
            if (ddlChoice.SelectedValue == "1")
            {
                RecType = "Donor";
            }
            else
            {
                RecType = "Volunteer";

            }
            if (DDAnonymous.SelectedValue == "2")
            {
                Anon= "'Y'";
            }
            else
            {
                Anon = "NULL";
            }
            if (DDSpouse.SelectedValue == "2")
            {
                spouse = "'Y'";
            }
            else
            {
                spouse = "NULL";
            }
            if ((!string.IsNullOrEmpty(txtName.Text)) && (DDYear.SelectedItem.Text != "Select Year")
                    && (ddlChoice.SelectedItem.Text != "Select Awards") && (DDTypeselection.SelectedItem.Text != "Select Choice"))
            {
                Lbsearchresult.Visible = false;
                DuplicateAwardDetailsYear(Convert.ToInt32(ViewState["MemberID"]), Convert.ToInt32(DDYear.SelectedValue));
               
                if ((duplicateYear != true) && (DuplicateAwrard!=true))
                {
                    string SqlAddQry = "Insert into donvolawards (Year,Chapter,MemberID,DonorType,RecType,AwardsType,Anon,Spouse,CreateDate,CreatedBy) values(" + DDYear.SelectedValue + ",'"
                   + ViewState["ChapterId"] + "','" + ViewState["MemberID"] +
                "','" + ViewState["DonorType"] + "','" + RecType.ToString() + "','" + DDTypeselection.SelectedItem.Text
                + "',"+Anon+","+spouse+",Getdate()," + Session["loginID"] + ")";

                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, SqlAddQry);
                Response.Write("<script>alert('Added successfully')</script>");
                txtName.Text = string.Empty;
                GridData();
                DDYear.SelectedIndex = 0;
                ddlChoice.SelectedIndex = 0;
                DDTypeselection.SelectedIndex = 0;
                DDAnonymous.SelectedValue ="1";
                DDSpouse.SelectedValue = "1";
                txtName.Text = "";
                LbDuplicateresults.Text = "";
                 }
            }
            else
            {
                Lbsearchresult.Visible = true;
                Lbsearchresult.Text = "Please Select All values";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void BtnExport_Click(object sender, EventArgs e)
    {
        AwardsDetails("Donor", GridDonor);
        GeneralExport((DataTable)Session["AwardDetails"], "DonorAwardDetails.xls");
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        try
        {
            string attach = string.Empty;
            attach = "attachment;filename=" + fname;
            Response.ClearContent();
            Response.AddHeader("content-disposition", attach);
            Response.ContentType = "application/vnd.xls";
            if (dtdata != null)
            {
                foreach (DataColumn dc in dtdata.Columns)
                {
                    Response.Write(dc.ColumnName + "\t");
                }
                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtdata.Rows)
                {
                    for (int i = 0; i < dtdata.Columns.Count; i++)
                    {
                        Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                    }
                    Response.Write("\n");
                }
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
    protected void AnonSpouseEditOption(DropDownList DDAnon)
    {
        if (DDAnon.SelectedValue == "2")
        {
            Anona = "'Y'";
        }
        else
        {
            Anona = "NULL";
        }
    }
   
    protected void GridDonor_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            sDatakey = GridDonor.DataKeys[e.RowIndex].Value.ToString();
            Label MemberID = (Label)GridDonor.Rows[e.RowIndex].FindControl("lbMember");
            ViewState["GrdMemberId"] = MemberID.Text;
            Label Year = (Label)GridDonor.Rows[e.RowIndex].FindControl("lblYear");
            TextBox RecTypeText = (TextBox)GridDonor.Rows[e.RowIndex].FindControl("TxtRecType");
            TextBox AwardTypeText = (TextBox)GridDonor.Rows[e.RowIndex].FindControl("TxtRecAwardType");
            DropDownList ddDonaorRecType = (DropDownList)GridDonor.Rows[e.RowIndex].FindControl("DDNewRecType");
            DropDownList ddDonaorRecAwardType = (DropDownList)GridDonor.Rows[e.RowIndex].FindControl("DDNewRecAwardType");
              DropDownList ddAnonSelect = (DropDownList)GridDonor.Rows[e.RowIndex].FindControl("DDAnon");
            TextBox TxtRecAnon = (TextBox)GridDonor.Rows[e.RowIndex].FindControl("TxtRecAnon");
            TextBox TxtRecSpouse = (TextBox)GridDonor.Rows[e.RowIndex].FindControl("TxtRecSpouse");
            DropDownList ddSpouseSelect = (DropDownList)GridDonor.Rows[e.RowIndex].FindControl("DDSpouse");
            DropDownList DDChapterUp = (DropDownList)GridDonor.Rows[e.RowIndex].FindControl("DDChapterUp");
            TextBox TxtChapter = (TextBox)GridDonor.Rows[e.RowIndex].FindControl("TxtChapter");

            EditanonandSpouse(ddAnonSelect, ddSpouseSelect, TxtRecAnon, TxtRecSpouse);

            if (DDChapterUp.SelectedItem.Text != "Select Chapter")
            {
                Upchapter = DDChapterUp.SelectedItem.Text;
            }
            else
            {
                Upchapter = TxtChapter.Text;
            }
            if (ddDonaorRecType.SelectedItem.Text != "Select RecType")
            {
                RecType = ddDonaorRecType.SelectedItem.Text;
            }
            else
            {
                RecType = RecTypeText.Text;
            }
            if (ddDonaorRecAwardType.SelectedItem.Text != "Select Choice")
            {
                AwardType = ddDonaorRecAwardType.SelectedItem.Text;
            }
            else
            {
                if (ddDonaorRecType.SelectedItem.Text != "Select RecType")
                {
                    ValidAwardType = true;
                    Response.Write("<script>alert('Please Select AwardType')</script>");
                }
                else
                {
                    AwardType = AwardTypeText.Text;
                }
            }
            LifetimeDuplicateAwardDetails(Convert.ToInt32(ViewState["GrdMemberId"]), ddDonaorRecAwardType.SelectedItem.Text);
            if ((ValidAwardType != true)&&(duplicateYear != true) && (DuplicateAwrard!=true))
            {
                updatefunction();
                GridDonor.EditIndex = -1;
                GridData();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void EditanonandSpouse(DropDownList DDAnon,DropDownList DDSpouse,TextBox TxtAnon,TextBox TxtSpouse)
    {
           
              if (DDAnon.SelectedItem.Text != "Select")
            {
                AnonSpouseEditOption(DDAnon);
            }
            else
            {
                if (string.IsNullOrEmpty(TxtAnon.Text))
                {
                    Anona = "NULL";
                }
                else
                {
                    Anona = "'"+TxtAnon.Text+"'";
                }
            }

            if (DDSpouse.SelectedItem.Text != "Select")
            {
                SpouseEditOption(DDSpouse);
            }
            else
            {
                if (string.IsNullOrEmpty(TxtSpouse.Text))
                {
                    SpouseUp = "NULL";
                }
                else
                {
                    SpouseUp ="'"+TxtSpouse.Text+"'";
                }
            }
    }
    protected void updatefunction()
    {
        string UpdatememberId;
        if (!string.IsNullOrEmpty(TextMemberIDVal.Text))
        {
            UpdatememberId = " Memberid=" + ViewState["MemberID"] + ",donortype='" + ViewState["DonorType"]
                + "',chapter='" + ViewState["ChapterId"] + "',";
        }
        else
        {
            UpdatememberId = "";
        }
      
        string qryUpdate = "update donvolawards set " + UpdatememberId + " RecType='" + RecType + "' ,Anon=" + Anona + ",Spouse=" + SpouseUp + ",AwardsType='"
                        + AwardType + "',Chapter='" + Upchapter + "',modifydate=Getdate(),modifiedby=" + Session["loginID"] 
                        + " where DonVolAwardsID=" + sDatakey + "";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
        Response.Write("<script>alert('Updated successfully')</script>");
        txtName.Text = string.Empty;
        GridData();
        TxtRecType1.Text = string.Empty;
        LbDuplicateresults.Text = string.Empty;
    }
    protected void SpouseEditOption(DropDownList DDSpouse)
    {
        if (DDSpouse.SelectedValue == "2")
        {
            SpouseUp = "'Y'";
        }
        else
        {
            SpouseUp = "NULL";
        }
    }
    protected void GridDonor_RowEditing(object sender, GridViewEditEventArgs e)
    {
        TextMemberIDVal.Text = string.Empty;

       // AnonandSpouse=
        GridDonor.EditIndex = e.NewEditIndex;
        GridData();
    }
    protected void GridDonor_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        TxtRecType1.Text = "";
        GridDonor.EditIndex = -1;
        GridData();
     
    }
    protected void ddProductGroup(object sender, System.EventArgs e)
    {
        try
        {
            DropDownList ddlTemp = null;
            if (!string.IsNullOrEmpty(TxtRecType1.Text))
            {
                int i = Convert.ToInt16(TxtRecType1.Text);
                ddlTemp = (DropDownList)sender;
                DropTypeSelect(ddlTemp, i);
            }
            else
            {
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Insert(0, new ListItem("Select Choice", "-1"));
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    
    protected void DDNewRecType_SelectedIndexChanged(object sender, EventArgs e)
    {
        TxtRecType1.Text = "";
        this.TxtRecType1.Text = ((DropDownList)sender).SelectedValue;
    }
    protected void GridVolunter_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            TextMemberIDVal.Text = string.Empty;
            GridVolunter.EditIndex = e.NewEditIndex;
            GridData();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void GridVolunter_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            TxtRecType1.Text = string.Empty;
            GridVolunter.EditIndex = -1;
            GridData();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void GridVolunter_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            sDatakey = GridVolunter.DataKeys[e.RowIndex].Value.ToString();
            Label MemberID = (Label)GridVolunter.Rows[e.RowIndex].FindControl("lblMemberID");
            TextBox RecTypeText = (TextBox)GridVolunter.Rows[e.RowIndex].FindControl("TxtRecType");
            TextBox AwardTypeText = (TextBox)GridVolunter.Rows[e.RowIndex].FindControl("TxtRecAwardType");
            DropDownList ddDonaorRecType = (DropDownList)GridVolunter.Rows[e.RowIndex].FindControl("DDNewRecType");
            DropDownList ddDonaorRecAwardType = (DropDownList)GridVolunter.Rows[e.RowIndex].FindControl("DDNewRecAwardType");

            DropDownList ddAnonSelect = (DropDownList)GridVolunter.Rows[e.RowIndex].FindControl("DDAnon");
            TextBox TxtRecAnon = (TextBox)GridVolunter.Rows[e.RowIndex].FindControl("TxtRecAnon");
            TextBox TxtRecSpouse = (TextBox)GridVolunter.Rows[e.RowIndex].FindControl("TxtRecSpouse");
            DropDownList ddSpouseSelect = (DropDownList)GridVolunter.Rows[e.RowIndex].FindControl("DDSpouse");
            EditanonandSpouse(ddAnonSelect, ddSpouseSelect, TxtRecAnon, TxtRecSpouse);


            DropDownList DDChapterUp = (DropDownList)GridVolunter.Rows[e.RowIndex].FindControl("DDChapterUp");
            TextBox TxtChapter = (TextBox)GridVolunter.Rows[e.RowIndex].FindControl("TxtChapter");

            if (DDChapterUp.SelectedItem.Text != "Select Chapter")
            {
                Upchapter = DDChapterUp.SelectedItem.Text;
            }
            else
            {
                Upchapter = TxtChapter.Text;
            }
            if (ddDonaorRecType.SelectedItem.Text != "Select RecType")
            {
                RecType = ddDonaorRecType.SelectedItem.Text;
            }
            else
            {
                RecType = RecTypeText.Text;
            }
            if (ddDonaorRecAwardType.SelectedItem.Text != "Select Choice")
            {
                AwardType = ddDonaorRecAwardType.SelectedItem.Text;
            }
            else
            {
                if (ddDonaorRecType.SelectedItem.Text != "Select RecType")
                {
                    ValidAwardType = true;
                    Response.Write("<script>alert('Please Select AwardType')</script>");
                }
                else
                {
                    AwardType = AwardTypeText.Text;
                }
            }
             LifetimeDuplicateAwardDetails(Convert.ToInt32(ViewState["GrdMemberId"]), ddDonaorRecAwardType.SelectedItem.Text);
             if ((ValidAwardType != true) && (duplicateYear != true) && (DuplicateAwrard != true))
             {
                 updatefunction();
                 GridVolunter.EditIndex = -1;
                 GridData();
             }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            AwardsDetails("Volunteer", GridVolunter);
            GeneralExport((DataTable)Session["AwardDetails"], "VolunteerAwardDetails.xls");
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void btnIndClose_Click(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;
        clearFunction();
    }
    private void clearFunction()
    {
        txtLastName.Text = string.Empty;
        txtFirstName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        ddlState.SelectedIndex = 0;
    }
    protected void SearchMemberID(object sender, EventArgs e)
    {
        States();
        clearFunction();
        pIndSearch.Visible = true;
        TextMemberIDVal.Text = "ValidMem";
    }
  
    protected void GridSearchSoln_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridSearchSoln.PageIndex = e.NewPageIndex;
        GridSearchSoln.DataSource = Session["GridSearchDetails"];
        GridSearchSoln.DataBind();
        GridSearchSoln.Visible = true;
        GridSearchSoln.Visible = true;
        Panel4.Visible = true;
    }
}