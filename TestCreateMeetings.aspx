﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestCreateMeetings.aspx.cs" Inherits="TestCreateMeetings" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to cancel this meeting?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }

            function JoinMeeting() {

                var url = document.getElementById("<%=hdnWebExMeetURL.ClientID%>").value;

                window.open(url, '_blank');

            }
            function StartMeeting() {

                JoinMeeting();

            }

            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();
                //var win = window.open(url, '_blank');

                //if (win) {
                //    //Browser has allowed it to be opened
                //    win.focus();
                //} else {
                //    //Broswer has blocked it
                //    alert('Please allow popups for this site');
                //}

            }
            function joinChildMeeting() {
                startChildMeeting();
            }

            function showAlert(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }



        </script>
    </div>
    <a id="ancClick" target="_blank" href=""></a>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <table id="tblCreateMeetings" border="0" cellpadding="0" cellspacing="0" runat="server" align="center" style="margin-left: auto; width: 100%;" class="tableclass">
        <tr>
            <td colspan="2">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>
        </tr>
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>WebEx Sessions</h2>
                </center>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td align="center">

                <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">
                    <tr class="ContentSubTitle" align="center">
                        <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

                        <td align="left" nowrap="nowrap" style="width: 41px">
                            <asp:DropDownList ID="ddYear" runat="server" Width="75px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="2016">2016</asp:ListItem>
                                <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                            </asp:DropDownList>

                        </td>
                        <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Event </td>
                        <td style="width: 439px" align="left">
                            <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                                <%--<asp:ListItem Value="13">Coaching</asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 100px" align="left" nowrap="nowrap" id="Td1" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chapter </td>
                        <td style="width: 100px" runat="server" align="left" id="Td2">
                            <asp:DropDownList ID="ddchapter" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td id="tdCoachTitle" runat="server" style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach
                        </td>
                        <td id="tdCoach" runat="server" style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddlCoach" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select</asp:ListItem>

                            </asp:DropDownList>

                        </td>
                        <td style="text-align: right; font-weight: bold;">Product Group</td>
                        <td style="text-align: left;">
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right; font-weight: bold;">Product</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>




                    </tr>
                    <tr>
                        <td align="center" colspan="12">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />
                            <asp:Button ID="BtnCreateSession" runat="server" Text="Create Meeting" OnClick="btnCreateMeeting_Click" />
                            <asp:Button ID="btnCreateAllSessions" runat="server" Text="Create All Sessions" Visible="false" Style="height: 26px" OnClick="btnCreateAllSessions_Click" />
                        </td>
                    </tr>



                </table>
                <br />
                <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>

            </td>

        </tr>

        <tr>
            <td>

                <table id="tblAddNewMeeting" runat="server" visible="false" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">

                    <tr class="ContentSubTitle">


                        <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phase
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddlPhase" Enabled="false" runat="server" Width="75px" AutoPostBack="True">

                                <asp:ListItem Value="1">One</asp:ListItem>
                                <asp:ListItem Value="2">Two</asp:ListItem>
                                <asp:ListItem Value="3">Three</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddlLevel" Enabled="false" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Session#
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddlSession" Enabled="false" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlSession_SelectedIndexChanged">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td align="left" nowrap="nowrap" id="Td5" runat="server" visible="false">Meeting Password </td>
                        <td style="width: 100px" runat="server" align="left" id="Td6" visible="false">
                            <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server" Width="100" Wrap="False"></asp:TextBox>
                            <br />
                            <br />
                        </td>
                    </tr>


                    <tr class="ContentSubTitle">



                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start Date</td>

                        <td align="left" nowrap="nowrap" style="width: 41px">
                            <asp:TextBox ID="txtDate" runat="server" Style="width: 95px;" Enabled="false"></asp:TextBox>
                            <br />
                            MM/DD/YYYY</td>

                        <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Time </td>
                        <td style="width: 439px" align="left">
                            <asp:TextBox ID="txtOriginalTime" runat="server" Style="width: 95px;"></asp:TextBox>
                            <br />
                            HH : MM : SS
                        </td>
                        <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Begin Time </td>
                        <td style="width: 439px" align="left">
                            <asp:TextBox ID="txtTime" runat="server" Style="width: 95px;"></asp:TextBox>
                            <br />
                            HH : MM : SS
                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duration (Mins)</td>
                        <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                            <asp:TextBox ID="txtDuration" runat="server" Width="93px"></asp:TextBox>
                        </td>
                        <td align="left" nowrap="nowrap" id="Td3" runat="server" visible="false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Zone </td>
                        <td style="width: 100px" runat="server" align="left" id="Td4" visible="false">
                            <asp:DropDownList ID="ddlTimeZone" runat="server" Width="110px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="11" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                                <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                                <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                                <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>

                    <tr class="ContentSubTitle">
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End Date</td>

                        <td align="left" nowrap="nowrap" style="width: 41px">
                            <asp:TextBox ID="txtEndDate" Enabled="false" runat="server" Style="width: 95px;"></asp:TextBox>
                            <br />
                            MM/DD/YYYY</td>

                        <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End Time </td>
                        <td style="width: 439px" align="left">
                            <asp:TextBox ID="txtEndTime" runat="server" Style="width: 95px;"></asp:TextBox>
                            <br />
                            HH : MM : SS
                        </td>
                        <td align="left" nowrap="nowrap" id="Td7" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Day </td>
                        <td style="width: 100px" runat="server" align="left" id="Td8">
                            <asp:DropDownList ID="ddlDay" runat="server" Width="110px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="MONDAY">Monday</asp:ListItem>
                                <asp:ListItem Value="TUESDAY">Tuesday</asp:ListItem>
                                <asp:ListItem Value="WEDNESDAY">Wednesday</asp:ListItem>
                                <asp:ListItem Value="THURSDAY">Thursday</asp:ListItem>
                                <asp:ListItem Value="FRIDAY">Friday</asp:ListItem>
                                <asp:ListItem Value="SATURDAY">Saturday</asp:ListItem>
                                <asp:ListItem Value="SUNDAY">Sunday</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td align="left" nowrap="nowrap" id="TdUserIDTitle" runat="server" visible="false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WebEx UserID</td>
                        <td align="left" nowrap="nowrap" id="TdUserID" runat="server" visible="false">
                            <asp:TextBox ID="txtUserID" runat="server" Style="width: 95px;"></asp:TextBox>
                        </td>
                        <td align="left" nowrap="nowrap" id="TdPWDTitle" runat="server" visible="false">PWD</td>
                        <td align="left" nowrap="nowrap" id="TdPWD" runat="server" visible="false">
                            <asp:TextBox ID="txtPWD" TextMode="Password" runat="server" Style="width: 95px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">



                            <table align="center">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Meeting" OnClick="btnCreateMeeting_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" OnClick="btnCancelMeeting_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="tblAddNewMeetingInsert" runat="server" visible="false" style="margin-left: auto; margin-right: auto; width: 25%; background-color: #ffffcc;">
                    <tr class="ContentSubTitle">



                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MeetingPassword</td>
                        <td style="width: 100px" runat="server" align="left" id="Td9">
                            <asp:TextBox ID="TxtMeetPwd" TextMode="Password" runat="server" Width="100" Wrap="False"></asp:TextBox>

                        </td>
                    </tr>
                    <tr class="ContentSubTitle">



                        <td align="left" nowrap="nowrap" id="Td10" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Zone </td>
                        <td style="width: 100px" runat="server" align="left" id="Td11">
                            <asp:DropDownList ID="DDLTimeZoneList" runat="server" Width="110px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="11" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                                <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                                <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                                <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">



                            <table align="center">
                                <tr>

                                    <td>
                                        <asp:Button ID="BtnCancelSession" runat="server" Text="Cancel" OnClick="btnCancelMeeting_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td align="center">

                <asp:Label ID="lblSuccess" runat="server" Style="color: blue;"></asp:Label>
                <asp:Label ID="lblMsg3" runat="server" Style="color: green;"></asp:Label>
            </td>

        </tr>

        <tr id="trChildEntry" runat="server" visible="false">
            <td align="center">
                <table style="width: 200px; background-color: #ffffcc;">
                    <tr>
                        <td style="width: 125px;" align="center">

                            <span style="font-weight: bold;">Child</span>
                            <asp:DropDownList ID="ddlChildrenList" runat="server" Width="75px" Style="margin-left: 20px;" AutoPostBack="True" OnSelectedIndexChanged="ddlChildrenList_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select</asp:ListItem>

                            </asp:DropDownList>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td>
                <div align="center" style="font-weight: bold; color: #64A81C;">
                    <span id="spnTable1Title" runat="server" visible="false">Table 1 : Coach Meeting List</span>


                </div>
                <div style="clear: both;"></div>
                <asp:Button ID="btnAddNewMeeting" runat="server" Visible="false" Text="New Meeting" OnClick="btnAddNewMeeting_Click" Style="float: right;" />
                <div style="clear: both;"></div>
                <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" OnRowCancelingEdit="GrdMeeting_RowCancelingEdit" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                            <ItemTemplate>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                            <ItemTemplate>
                                <div style="display: none;">
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                    <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                    <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                                    <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                    <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                    <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                                    <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                                    <asp:Label ID="lblOrgTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'>'></asp:Label>
                                </div>
                                <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                                <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="Cancel" />
                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                        <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                        <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                        <asp:BoundField DataField="SessionNo" HeaderText="Session"></asp:BoundField>
                        <asp:TemplateField HeaderText="Coach">

                            <ItemTemplate>

                                <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                        <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                        <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                        <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:TemplateField HeaderText="Begin Time">

                            <ItemTemplate>
                                <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Time">

                            <ItemTemplate>
                                <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                        <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                        <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                        <asp:TemplateField HeaderText="meeting URL">

                            <ItemTemplate>

                                <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                                <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UserID" HeaderText="User ID"></asp:BoundField>
                        <asp:BoundField DataField="Pwd" HeaderText="Pwd"></asp:BoundField>
                    </Columns>

                    <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                    <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
                </asp:GridView>
            </td>
        </tr>

        <tr id="trChildList" runat="server" visible="false">
            <td align="center">
                <div style="font-weight: bold; color: #64A81C;">Table 2 : Child List</div>
                <div style="clear: both;">
                </div>
                <div style="width: 1000px; display: none;">
                    <asp:Button ID="btnSwitchStudent" runat="server" OnClick="btnSwitchStudent_Click" Visible="false" Text="Switch Child" Style="float: right;" />
                </div>
                <div style="clear: both;"></div>
                <asp:Label ID="lblChildMsg" runat="server" Style="color: red;"></asp:Label>
                <div style="clear: both;"></div>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1000px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdStudentsList_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                            <ItemTemplate>
                                <div style="display: none;">
                                    <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                    <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                    <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                    <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeJoinURL") %>'>'></asp:Label>
                                    <asp:Label ID="lblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>
                                    <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                                </div>

                                <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                                <asp:Button ID="BtnReRegister" runat="server" Text="Update Registration" Visible="false" CommandName="ReRegister" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>
                        <asp:BoundField DataField="ParentName" HeaderText="Parent"></asp:BoundField>
                        <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                        <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                        <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                        <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                        <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                        <asp:BoundField DataField="AttendeeID" Visible="false" HeaderText="Attendee ID"></asp:BoundField>
                        <asp:BoundField DataField="RegisteredID" Visible="false" HeaderText="Registered ID"></asp:BoundField>
                        <asp:TemplateField HeaderText="meeting URL">

                            <ItemTemplate>

                                <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text="Join" CommandName="SelectMeetingURL"></asp:LinkButton>

                                <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
                <div style="clear: both;"></div>
                <asp:Button ID="btnClosetable2" Style="margin-bottom: 10px;" runat="server" Text="Close Table 2" OnClick="btnClosetable2_Click" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnChildID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
    <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />

    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />

    <input type="hidden" id="HdnLevel" value="0" runat="server" />
    <input type="hidden" id="hdnCoachname" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
    <input type="hidden" id="hdnSessionNo" value="1" runat="server" />

</asp:Content>
