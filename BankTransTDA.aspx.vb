
Imports System
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Specialized
Imports System.Data.OleDb
'Asset Type	AssetClass is coding Line# 427
'We didnt have brokTransLog table for this.

Namespace VRegistration
    Partial Class BankTransTDA
        Inherits System.Web.UI.Page
        Dim StrTransCat As String
        Dim StrMedium As String
        Dim StrRestType As String
        Dim StrToRestType As String
        Dim StrDonation As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If
            'Put user code to initialize the page here
            Session("ErrString") = ""
            If Page.IsPostBack = False Then
                ''If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                ''    If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                ''        'for Volunteer more than roleid 1,84,85   
               
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank where BankId>3")
                ddlBankUpload.DataSource = ds
                ddlBankUpload.DataBind()
                ddlBankUpload.Items.Add(New ListItem("Select Account", "0"))
                ddlBankUpload.SelectedIndex = ddlBankUpload.Items.IndexOf(ddlBankUpload.Items.FindByValue("0"))
                ddlVerifyTrans.DataSource = ds
                ddlVerifyTrans.DataBind()
                ddlVerifyTrans.Items.Add(New ListItem("Select Account", "0"))
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue("0"))
                ddlEditUpdate.DataSource = ds
                ddlEditUpdate.DataBind()
                ddlEditUpdate.Items.Add(New ListItem("Select Account", "0"))
                ddlEditUpdate.SelectedIndex = ddlEditUpdate.Items.IndexOf(ddlEditUpdate.Items.FindByValue("0"))
                ddlPostDB.DataSource = ds
                ddlPostDB.DataBind()
                ddlPostDB.Items.Add(New ListItem("Select Account", "0"))
                ddlPostDB.SelectedIndex = ddlPostDB.Items.IndexOf(ddlPostDB.Items.FindByValue("0"))
                ddlEditUpdateDB.DataSource = ds
                ddlEditUpdateDB.DataBind()
                ddlEditUpdateDB.Items.Add(New ListItem("Select Account", "0"))
                ddlEditUpdateDB.SelectedIndex = ddlEditUpdateDB.Items.IndexOf(ddlEditUpdateDB.Items.FindByValue("0"))
                ' Else
                Response.Write("<script language='javascript'>window.open('BankTransTDAStatus.aspx','_blank','left=150,top=0,width=620,height=550,toolbar=0,location=0,scrollbars=1');</script> ")
                ''    Else
                ''        Response.Redirect("VolunteerFunctions.aspx")
                ''    End If
                ''Else
                ''    Response.Redirect("Login.aspx")
                ''End If
            End If
        End Sub

        Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Try

                Label1.Text = ""
                closepanel()
                If RbtnUploadFile.Checked = True Then
                    'Changed 10/21/2011
                    uploadfile()
                ElseIf RbtnVerifyTrans.Checked = True Then
                    'Changed 10/22/2011
                    verifyTrans()
                ElseIf RbtnEditUpdate.Checked = True Then
                    If ddlEditUpdateColumn.SelectedItem.Text <> "Search Column" Then
                        Dim wherecntn As String '
                        If ddlEditUpdateSel.SelectedItem.Text = "Null" Then
                            wherecntn = " is Null "
                        ElseIf ddlEditUpdateSel.SelectedItem.Text = "Not Null" Then
                            wherecntn = " is Not Null "
                        ElseIf ddlEditUpdateSel.SelectedItem.Text = "Yes" Then
                            wherecntn = " = 'Y' " '
                        ElseIf ddlEditUpdateSel.SelectedItem.Text = "None of Above" Then
                            wherecntn = " Not in ('BUY', 'DIV', 'SELL', 'TRN','VTR', 'TRD', 'Split') AND " & ddlEditUpdateColumn.SelectedItem.Text & " is Not Null " 'None of Above
                        Else
                            wherecntn = " = '" & ddlEditUpdateSel.SelectedItem.Text & "' "
                        End If
                        Session("StrSql") = "Select * from BrokTemp where (" & ddlEditUpdateColumn.SelectedItem.Text & " " & wherecntn & ") order by date Desc"
                        loadGrid(True)
                    Else
                        Label1.Text = "Plese Select a Column to search Edit/Update"
                    End If
                ElseIf RbtnPostDB.Checked = True Then
                    postdata()
                ElseIf RbtnEditUpdateDB.Checked = True Then
                    Select Case ddlEditUpdateDB.SelectedValue
                        Case 4, 5, 6, 7
                            If ddlEditUpdateColumnDB.SelectedItem.Text <> "Search Column" Then
                                Dim wherecntn As String '
                                If ddlEditUpdateSelDB.SelectedItem.Text = "Null" Then
                                    wherecntn = " is Null "
                                ElseIf ddlEditUpdateSelDB.SelectedItem.Text = "Not Null" Then
                                    wherecntn = " is Not Null "
                                ElseIf ddlEditUpdateSelDB.SelectedItem.Text = "Yes" Then
                                    wherecntn = " = 'Y' " '
                                ElseIf ddlEditUpdateSelDB.SelectedItem.Text = "None of Above" Then
                                    wherecntn = " Not in ('BUY', 'DIV', 'SELL', 'TRN', 'VTR', 'TRD', 'Split') AND " & ddlEditUpdateColumnDB.SelectedItem.Text & " is Not Null " 'None of Above
                                Else
                                    wherecntn = " = '" & ddlEditUpdateSelDB.SelectedItem.Text & "' "
                                End If
                                Session("StrSql") = "Select TOP 2000 * from BrokTrans where " & ddlEditUpdateColumnDB.SelectedItem.Text & " " & wherecntn & " and BankId = " & ddlEditUpdateDB.SelectedValue & " order by Transdate Desc"
                                LoadBankTrans(True)
                            Else
                                Label1.Text = "Plese Select a Column to search Edit/Update"
                            End If
                        Case Else
                            Label1.Text = "Please Select Transaction Account"
                    End Select
                Else
                    Label1.Text = "Please Choose the option"
                End If
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub uploadfile()
            'Moving data from CSV file to Temp Tables
            If FileUpLoad1.HasFile And ddlBankUpload.SelectedValue > 0 Then
                Dim fileExt, filename As String
                Dim filecount As Integer = 0
                fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName)
                If (fileExt.ToLower = ".csv") Then
                    Try
                        Dim strfilename, FileType, FileNameSpec As String
                        FileNameSpec = ddlBankUpload.SelectedItem.Text.Trim & "_Trans"
                        filename = Left(FileUpLoad1.FileName.Trim, FileNameSpec.Length)
                        FileType = ddlBankUpload.SelectedItem.Text
                        If filename.ToUpper.Trim = FileNameSpec.ToUpper.Trim Then
                            'strfilename = FileType & "file" & Now.Date.ToString("ddMMyyyy")
                            strfilename = FileUpLoad1.FileName
                            FileUpLoad1.PostedFile.SaveAs(Server.MapPath("UploadFiles/" & strfilename))
                            Label1.Text = "Received " & strfilename & " Content Type " & FileUpLoad1.PostedFile.ContentType & " Length " & FileUpLoad1.PostedFile.ContentLength
                            Dim flag As Integer = 0
                            Dim dataCount As Integer = 0
                            dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from BrokTemp")
                            If dataCount = 0 Then
                                ImportExcel_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                            Else
                                Label1.Text = "Sorry! Data exists in Temp file for Bank Account - " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select BankCode from Bank where ActNumber IN (select top 1 AcctNumber  from  BrokTemp)") & ". It needs to be posted first"
                                Exit Sub
                            End If
                            ''Select Case ddlBankUpload.SelectedValue
                            ''    Case 1
                            ''        dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChaseTemp")
                            ''        If dataCount = 0 Then
                            ''            ImportChaseCSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                            ''        Else
                            ''            Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                            ''        End If
                            ''    Case 2
                            ''        dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT1Temp")
                            ''        If dataCount = 0 Then
                            ''            ImportHBT1CSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                            ''        Else
                            ''            Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                            ''        End If
                            ''    Case 3
                            ''        dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT2Temp")
                            ''        If dataCount = 0 Then
                            ''            ImportHBT2CSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                            ''        Else
                            ''            Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                            ''        End If
                            ''End Select
                            ddlBankUpload.SelectedIndex = ddlBankUpload.Items.IndexOf(ddlBankUpload.Items.FindByValue(0))
                        Else
                            Label1.Text = "File type selected Mismatch with Filename."
                        End If
                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                    End Try
                Else
                    Label1.Text = "Not a Csv file"
                End If

            Else
                Label1.Text = "No uploaded file / File Type Not Selected"
            End If
        End Sub

        Private Sub ImportExcel_Database(ByVal TransFileWithPath As String, ByVal FileName As String)
            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            Dim SqlInsert As String
            Try
                'open imported file as dataset
                Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
                Dim dsFile As DataSet = OpenFileConnection(TransFile, 1)
                ' Validate If the Column "NetAmt, Price, Quantity contains ,$,(,). If so, show error message
                'BEGIN
                Dim strCondition As String = "Convert([Net Amt], 'System.String') like  '%,%' or Convert([Net Amt], 'System.String') like  '%(%' or Convert([Net Amt], 'System.String') like  '%)%' or Convert([Net Amt], 'System.String') like  '%(%' or Convert([Net Amt], 'System.String') like  '%$%' "
                strCondition = strCondition & " OR  Convert([Price], 'System.String') like  '%,%' or  Convert([Price], 'System.String') like  '%(%' or  Convert([Price], 'System.String') like  '%)%' or  Convert([Price], 'System.String') like  '%(%' or  Convert([Price], 'System.String') like  '%$%' "
                strCondition = strCondition & " OR  Convert([Quantity], 'System.String') like  '%,%' or Convert([Quantity], 'System.String') like  '%(%' or Convert([Quantity], 'System.String') like  '%)%' or Convert([Quantity], 'System.String') like  '%(%' or Convert([Quantity], 'System.String') like  '%$%' "

                Dim drTest() As DataRow = dsFile.Tables(0).Select(strCondition)
                If drTest.Length > 0 Then
                    Label1.ForeColor = Drawing.Color.Red
                    Label1.Text = " Invalid data. Comma, $ sign and parentheses are not allowed."
                    Exit Sub
                End If
                'END
                Dim RowsImported As Integer = 0
                Dim AcctNumber As String
                Dim AcctDesc As String
                Dim TDate As String
                Dim Description As String
                Dim Symbol As String
                Dim Type As String
                Dim Quantity As String
                Dim Price As String
                Dim NetAmount As String
                Dim Comm As String
                Dim TranID As String
                Dim AssetType As String
                Dim cmd As SqlCommand
                Dim count As Integer = 0

                AcctNumber = dsFile.Tables(0).Rows(1)(2).ToString()
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(BankID) from Bank where ActNumber =" & AcctNumber & " and BankID=" & ddlBankUpload.SelectedValue & "") = 0 Then
                    Label1.ForeColor = Drawing.Color.Red
                    Label1.Text = " Account number mismatch with Bank Selected "
                    Exit Sub
                End If
                For Each TranRow As DataRow In dsFile.Tables(0).Rows
                    If TranRow(0).ToString.Trim <> "" Then

                        AcctNumber = TranRow(2).ToString ' TranRow(0).ToString
                        AcctDesc = TranRow(3).ToString 'TranRow(1).ToString
                        TDate = TranRow(0).ToString ' TranRow(2).ToString
                        Description = TranRow(4).ToString.Replace("'", "") 'TranRow(3).ToString.Replace("'", "")
                        Symbol = IIf(TranRow(5).ToString().Trim.Length > 0, "'" & TranRow(5).ToString() & "'", "Null") 'IIf(TranRow(4).ToString().Trim.Length > 0, "'" & TranRow(4).ToString() & "'", "Null")
                        Type = TranRow(6).ToString ' TranRow(5).ToString

                        Quantity = TranRow(7).ToString ' TranRow(6).ToString
                        If Quantity = "" Then
                            Quantity = "Null"
                        End If
                        Price = TranRow(8).ToString ' TranRow(7).ToString
                        NetAmount = TranRow(9).ToString 'TranRow(8).ToString
                        Comm = TranRow(10).ToString 'TranRow(9).ToString
                        TranID = "Null" 'TranRow(14).ToString
                        AssetType = "Null" ''''IIf(TranRow(19).ToString().Trim.Length > 0, "'" & TranRow(19).ToString() & "'", "Null") 'TranRow(19).ToString

                        'create insert statement
                        SqlInsert = "INSERT INTO BrokTemp(AcctNumber, AcctDesc, Date, Description, Symbol, Type, Quantity, Price, NetAmount, Comm, TranID, AssetType, CreatedDate, CreatedBy,Brok_BankID) VALUES("
                        SqlInsert = SqlInsert & AcctNumber & ",'" & AcctDesc & "','" & TDate & "','" & Description & "'," & Symbol & ",'" & Type & "'," & Quantity & ",'" & Price & "','" & NetAmount & "','" & Comm & "'," & TranID & "," & AssetType & ",GetDate()," & Session("LoginID") & "," & ddlBankUpload.SelectedValue & ")"
                        'Label1.Text = Label1.Text & "<br>" & SqlInsert

                        'Response.Write(SqlInsert)

                        'create SQL command 
                        cmd = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                        count = count + 1
                    End If
                Next
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & FileName & "','" & ddlBankUpload.SelectedItem.Text & "',GetDate()," & Session("LoginID") & ")")
                ''SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "delete from Chasetemp Where datediff(d,(Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 1 order by Transdate desc),Tdate)<=0")
                ' ''display number of rows imported
                ' ''label1.Text = label1.Text & "<br> Rows Imported Successfully: " & RowsImported
                ''Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ChaseTemp")
                ''SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BegRecdate, EndRecDate, TempRecRej, Postedby,CreatedDate) select '" & FileName & "'," & RowsImported & ", convert(varchar(10),min(tdate),101),convert(varchar(10),max(tdate),101)," & (RowsImported - count) & "," & Session("LoginID") & ",Getdate() from chaseTemp")
                Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & count
            Catch ex As Exception
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate TABLE  BrokTemp")
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                'Response.Write(SqlInsert & ex.ToString())
            End Try
        End Sub


        Private Function OpenFileConnection(ByVal TransFileTable As FileInfo, ByVal type As Integer) As DataSet
            Dim dsFile As New DataSet
            ' MsgBox(ddlBankUpload.SelectedIndex)
            Try
                Dim ConnCSV As String
                'If type = 1 Then
                ConnCSV = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=Yes;FMT=Delimited';Data Source=" & TransFileTable.DirectoryName.ToString()
                'ConnCSV = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='Excel 8.0;HDR=Yes';Data Source=" & TransFileTable.DirectoryName.ToString()
                'End If
                Dim sqlSelect As String
                Dim objOleDBConn As OleDbConnection
                Dim objOleDBDa As OleDbDataAdapter
                objOleDBConn = New OleDbConnection(ConnCSV)
                objOleDBConn.Open()
                'sqlSelect = "select [Store ID],[Order #],[Date],[User ID],Type,[PayerAuth],[Invoice #],[PO #],[Trans ID],[Card/Route Number],[Exp Date],Approval,Amount from [" + TransFileTable.Name.ToString() + "]"
                sqlSelect = "select * from [" + TransFileTable.Name.ToString() + "]"
                objOleDBDa = New OleDbDataAdapter(sqlSelect, objOleDBConn)
                objOleDBDa.Fill(dsFile)
                objOleDBConn.Close()
            Catch ex As Exception
                Label1.Text = Label1.Text & "<br> ERROR in OpenFileConnection: " & ex.Message.ToString()
            End Try
            Return dsFile
        End Function
        Sub verifyTrans()
            Try
                Dim count As Integer
                Dim tempcount As Integer = 0
                Dim CTransLastDate, CTempFirstDate As Date
                Select Case ddlVerifyTrans.SelectedValue
                    Case 4, 5, 6, 7
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from Broktemp")
                        If tempcount > 0 Then
                            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from BrokTrans where BankID = " & ddlVerifyTrans.SelectedValue)
                            CTempFirstDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 CONVERT(VARCHAR(8), Date, 1)  as CTempFirstDate from Broktemp order by Date ")
                            If count > 0 Then
                                CTransLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 TransDate as CTransLastDate from BrokTrans where BankID = " & ddlVerifyTrans.SelectedValue & " order by Transdate desc")
                                If CTempFirstDate <= CTransLastDate Then
                                    'Response.Write(CTempFirstDate.ToString() & " Temp1st : transLast" & CTransLastDate.ToString())
                                    'We should not post transactions for dates
                                    ''TDS step 1
                                    'Delete from BrokTemp  WHERE Date <= '06/30/2011'
                                    Session("ErrString") = "No. of rows deleted due to overlap of dates : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from BrokTemp  WHERE Date <='" & CTransLastDate & "'") & "<BR>"
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from BrokTemp  WHERE Date <='" & CTransLastDate & "'")
                                    Dim strCmd As String = "select count(*) from BrokTemp where [NetAmount] like  '%,%' or [NetAmount] like  '%(%' or [NetAmount] like  '%)%' or [NetAmount] like  '%(%' or [NetAmount] like  '%$%'  OR [Price] like  '%,%' or [Price] like  '%(%' or [Price] like  '%)%' or [Price] like  '%(%' or [Price] like  '%$%'  OR [Quantity] like  '%,%' or [Quantity] like  '%(%' or [Quantity] like  '%)%' or [Quantity] like  '%(%' or [Quantity] like  '%$%' "
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd) > 0 Then
                                        Label1.ForeColor = Drawing.Color.Red
                                        Label1.Text = " Invalid data. Comma, $ sign and parentheses are not allowed."
                                        Exit Sub
                                    End If
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.ModifiedDate=GetDate(), Bk.ModifiedBy=" & Session("LoginID") & ", Bk.Brok_BankID=B.BankID, Bk.Brok_BankCode=LTRIM(RTRIM(B.BankCode)),Bk.Brok_Ticker=Case WHEN Bk.Symbol Is Null Then 'Cash' Else  Bk.Symbol END,Bk.Brok_TransType = Bk.Type ,Bk.Brok_TransCat = Case WHEN Bk.Type in ('Div','Buy','Sell','Split')  THEN Bk.Type Else Null END from BrokTemp Bk Inner Join Bank B On Bk.AcctNumber = B.ActNumber") '' August 13, 2012 Removed , Bk.Brok_AssetClass = CASE WHEN Bk.AssetType is NULL THEN 'CASH' ELSE CASE WHEN Bk.AssetType='Mutual Fund' THEN 'MutFund' ELSE CASE WHEN Bk.AssetType='Money Market' THEN 'MMK' ELSE Bk.AssetType END END END
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.Brok_AssetClass = T.AssetClass  from BrokTemp Bk Inner Join ticker T On T.Ticker = Bk.Brok_Ticker")
                                    VerifyDesc()
                                ElseIf DateDiff(DateInterval.Day, CTransLastDate, CTempFirstDate) > 1 Then
                                    Session("CTransLastDate") = CTransLastDate
                                    TrMsg.Visible = True
                                    Literal1.Text = "The Last date in BrokTrans table is " & CTransLastDate & ".  The First date in the Temp table is " & CTempFirstDate & ". So there is a gap in dates.<br> Was a CSV file missing or being skipped?"
                                    'Literal1.Text = "Was a CSV file missing or being skipped?"
                                Else
                                    Dim strCmd As String = "select count(*) from BrokTemp where [NetAmount] like  '%,%' or [NetAmount] like  '%(%' or [NetAmount] like  '%)%' or [NetAmount] like  '%(%' or [NetAmount] like  '%$%'  OR [Price] like  '%,%' or [Price] like  '%(%' or [Price] like  '%)%' or [Price] like  '%(%' or [Price] like  '%$%'  OR [Quantity] like  '%,%' or [Quantity] like  '%(%' or [Quantity] like  '%)%' or [Quantity] like  '%(%' or [Quantity] like  '%$%' "
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd) > 0 Then
                                        Label1.ForeColor = Drawing.Color.Red
                                        Label1.Text = " Invalid data. Comma, $ sign and parentheses are not allowed."
                                        Exit Sub
                                    End If
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.ModifiedDate=GetDate(), Bk.ModifiedBy=" & Session("LoginID") & ", Bk.Brok_BankID=B.BankID, Bk.Brok_BankCode=LTRIM(RTRIM(B.BankCode)),Bk.Brok_Ticker=Case WHEN Bk.Symbol Is Null Then 'Cash' Else  Bk.Symbol END,Bk.Brok_TransType = Bk.Type ,Bk.Brok_TransCat = Case WHEN Bk.Type in ('Div','Buy','Sell','Split')  THEN Bk.Type Else Null END from BrokTemp Bk Inner Join Bank B On Bk.AcctNumber = B.ActNumber") '' August 13, 2012 Removed , Bk.Brok_AssetClass = CASE WHEN Bk.AssetType is NULL THEN 'CASH' ELSE CASE WHEN Bk.AssetType='Mutual Fund' THEN 'MutFund' ELSE CASE WHEN Bk.AssetType='Money Market' THEN 'MMK' ELSE Bk.AssetType END END END
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.Brok_AssetClass =  T.AssetClass  from BrokTemp Bk Inner Join ticker T On T.Ticker = Bk.Brok_Ticker")
                                    Session("CTransLastDate") = CTransLastDate
                                    VerifyDesc()
                                End If
                            Else
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.ModifiedDate=GetDate(), Bk.ModifiedBy=" & Session("LoginID") & ", Bk.Brok_BankID=B.BankID, Bk.Brok_BankCode=LTRIM(RTRIM(B.BankCode)),Bk.Brok_Ticker=Case WHEN Bk.Symbol Is Null Then 'Cash' Else  Bk.Symbol END,Bk.Brok_TransType = Bk.Type ,Bk.Brok_TransCat = Case WHEN Bk.Type in ('Div','Buy','Sell','Split')  THEN Bk.Type Else Null END from BrokTemp Bk Inner Join Bank B On Bk.AcctNumber = B.ActNumber") '', Bk.Brok_AssetClass = CASE WHEN Bk.AssetType is NULL THEN 'CASH' ELSE CASE WHEN Bk.AssetType='Mutual Fund' THEN 'MutFund' ELSE CASE WHEN Bk.AssetType='Money Market' THEN 'MMK' ELSE Bk.AssetType END END END
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.Brok_AssetClass =  T.AssetClass  from BrokTemp Bk Inner Join ticker T On T.Ticker = Bk.Brok_Ticker")
                                VerifyDesc()
                            End If
                        Else
                            Label1.Text = "No record found in BrokTemp"
                        End If
                    Case Else
                        Label1.ForeColor = Drawing.Color.Red
                        Label1.Text = "Please select an item from DropDown"
                End Select
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub VerifyDesc()
            Try
                Dim unParsedTranscnt As Integer = 0
                If RbtnVerifyTrans.Checked = True Then
                    Select Case ddlVerifyTrans.SelectedValue
                        Case 4, 5, 6, 7
                            ''    unParsedTranscnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where (Parse_TransType is null OR Parse_TransType<>'Y')")
                            ''    If unParsedTranscnt > 0 Then
                            ''        Session("StrSql") = "Select * from ChaseTemp where (Parse_TransType is null OR Parse_TransType<>'Y') order by Tdate Desc"
                            ''        loadGrid(True)
                            ''    Else
                            ''        'Everything Parsed Successfully Move for Next Parsing
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.ModifiedDate=GetDate(),Bk.Parse_Desc='Y', Bk.ModifiedBy=" & Session("LoginID") & ", Bk.Brok_ExpCheckNo = CASE WHEN b.BrokDescID=2 THEN RTRIM(LTRIM(substring(bk.Description,CharIndex('Check #:',bk.Description)+Len('Check #:'),Len(bk.Description)))) ELSE CASE WHEN b.BrokDescID=10 THEN RTRIM(LTRIM(substring(bk.Description,CharIndex('CHECK NUMBER',bk.Description)+Len('CHECK NUMBER'),Len(bk.Description)))) ELSE NULL END END ,Bk.Brok_DepCheckNo=CASE WHEN b.BrokDescID=1 and CharIndex('RDC ',bk.Description) > 0 THEN RTRIM(LTRIM(substring(bk.Description,CharIndex('RDC ',bk.Description)+Len('RDC '),(CharIndex('FOR',bk.Description)-(CharIndex('RDC ',bk.Description)+Len('RDC ')))))) ELSE NULL END,Bk.Brok_TransCat=CASE WHEN b.BrokDescID IN(11,12,13) THEN CASE WHEN Bk.Type ='Div' THEN b.Brok_TransCat ELSE Null END ELSE b.Brok_TransCat END,Bk.Brok_Medium=b.Brok_Medium FROM BrokTemp Bk INNER JOIN BrokDesc b ON ((CHARINDEX(b.Token, Bk.Description) >0 AND B.BrokDescID not in (4,5)) OR (CHARINDEX(b.Token, Bk.Description) >0 AND B.BrokDescID = 4 AND  Bk.Quantity >=0) OR (CHARINDEX(b.Token, Bk.Description) >0 AND B.BrokDescID = 5 AND  Bk.Quantity <0))")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.Brok_TransCat = CASE WHEN Ex.TransType = 'Grants' THEN Ex.TransType ELSE CASE WHEN Ex.TransType = 'Transfers' and Bk.NetAmount <0 THEN 'TransferOut' ELSE 'TransferIn' END  END from  BrokTemp Bk Inner Join ExpJournal Ex ON (Ex.CheckNumber = Bk.Brok_ExpCheckNo OR Ex.CheckNumber = Bk.Brok_DepCheckNo)  WHERE DateDiff(m,Ex.DatePaid ,Bk.Date) < 25 and Ex.TransType in ('Grants','Transfers') and Ex.TransType in ('Grants','Transfers') and Ex.CheckNumber is Not Null and (Bk.Brok_DepCheckNo is not null or Bk.Brok_ExpCheckNo is not null)")
                            ''        Dim unParsedDesccnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where (Parse_Desc is null OR Parse_Desc<>'Y')")
                            ''        If unParsedDesccnt > 0 Then
                            Session("StrSql") = "Select * from BrokTemp where Brok_AssetClass is  Null OR  Brok_TransType is null OR Brok_Ticker is null order by [Date] Desc"
                            loadGrid(True)
                            ''        Else
                            ''            Label1.Text = "All Data Were Parsed Successfully"
                            ''        End If
                            ''    End If                            
                    End Select
                End If
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Public Sub loadGrid(ByVal BlnReload As Boolean)
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            lblChaseTemp.Text = ""
            If (BlnReload = True) Then
                Session("ChaseDataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("ChaseDataSet") = ds
                    ChaseResults.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("ChaseDataSet") Is Nothing) Then
                    ds = CType(Session("ChaseDataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                PnlChaseTemp.Visible = False
                Label1.Text = "There is No Record to display in Grid"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                PnlChaseTemp.Visible = False
                Label1.Text = Session("ErrString") & "There is No Record to display in Grid"
            Else
                If Session("ErrString").ToString().Length > 0 Then
                    Label1.Text = Session("ErrString")
                End If
                lblChaseTemp.Text = "**Note: Records from BrokTemp Table. Brok_AssetClass/Brok_TransType/Brok_Ticker Field must not be null"
                Dim i As Integer = ds.Tables(0).Rows.Count
                ChaseResults.DataSource = ds.Tables(0)
                ChaseResults.DataBind()
                'GridView1.DataSource = ds.Tables(0)
                'GridView1.DataBind()
                PnlChaseTemp.Visible = True
            End If
        End Sub

        Protected Sub ChaseResults_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.UpdateCommand
            Dim Sql As String
            Try
                ' TransType,Description, Parse_TransType, Parse_Desc, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber,Bk_VendCust, Bk_Reason
                Dim BrokTempID As Integer = CInt(e.Item.Cells(2).Text)
                Dim AcctNumber As String = CType(e.Item.FindControl("TxtAcctNumber"), TextBox).Text
                Dim AcctDesc As String = CType(e.Item.FindControl("txtAcctDesc"), TextBox).Text
                Dim Description As String = CType(e.Item.FindControl("txtDescription"), TextBox).Text
                Dim Symbol As String = CType(e.Item.FindControl("txtSymbol"), TextBox).Text
                Dim Type As String = CType(e.Item.FindControl("txtType"), TextBox).Text
                Dim Quantity As String = CType(e.Item.FindControl("txtQuantity"), TextBox).Text
                Dim Price As String = CType(e.Item.FindControl("txtPrice"), TextBox).Text
                Dim Comm As String = CType(e.Item.FindControl("txtComm"), TextBox).Text
                Dim AssetType As String = CType(e.Item.FindControl("txtAssetType"), TextBox).Text
                Dim Parse_Desc As String = CType(e.Item.FindControl("txtParse_Desc"), TextBox).Text
                ' Dim Brok_BankCode As String = CType(e.Item.FindControl("txtBrok_BankCode"), TextBox).Text
                Dim Brok_Ticker As String = CType(e.Item.FindControl("txtBrok_Ticker"), TextBox).Text
                Dim Brok_TransType As String = CType(e.Item.FindControl("txtBrok_TransType"), TextBox).Text
                Dim Brok_AssetClass As String = CType(e.Item.FindControl("txtBrok_AssetClass"), TextBox).Text
                Dim Brok_ExpCheckNo As String = CType(e.Item.FindControl("txtBrok_ExpCheckNo"), TextBox).Text
                Dim Brok_DepCheckNo As String = CType(e.Item.FindControl("txtBrok_DepCheckNo"), TextBox).Text
                Dim Brok_TransCat As String = CType(e.Item.FindControl("txtBrok_TransCat"), TextBox).Text
                Dim Brok_Medium As String = CType(e.Item.FindControl("txtBrok_Medium"), TextBox).Text


                Sql = "update BrokTemp set"
                'TransType,Description, Parse_TransType, Parse_Desc, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber,Bk_VendCust, Bk_Reason
                If AcctNumber = "" Then
                    Sql = Sql & " AcctNumber=Null,"
                Else
                    Sql = Sql & " AcctNumber='" & AcctNumber & "',"
                End If
                If Description = "" Then
                    Sql = Sql & " AcctDesc=Null,"
                Else
                    Sql = Sql & " AcctDesc='" & AcctDesc & "',"
                End If
                If Description = "" Then
                    Sql = Sql & " Description=Null,"
                Else
                    Sql = Sql & " Description='" & Description & "',"
                End If
                If Symbol = "" Then
                    Sql = Sql & " Symbol=Null,"
                Else
                    Sql = Sql & " Symbol='" & Symbol & "',"
                End If
                If Type = "" Then
                    Sql = Sql & " Type=Null,"
                Else
                    Sql = Sql & " Type='" & Type & "',"
                End If

                If Quantity = "" Then
                    Sql = Sql & " Quantity=Null,"
                Else
                    Sql = Sql & " Quantity=" & Quantity & ","
                End If

                If Price = "" Then
                    Sql = Sql & " Price=Null,"
                Else
                    Sql = Sql & " Price=" & Price & ","
                End If

                If Comm = "" Then
                    Sql = Sql & " Comm=Null,"
                Else
                    Sql = Sql & " Comm=" & Comm & ","
                End If
                If AssetType = "" Then
                    Sql = Sql & " AssetType=Null,"
                Else
                    Sql = Sql & " AssetType='" & AssetType & "',"
                End If

                If Parse_Desc = "" Then
                    Sql = Sql & " Parse_Desc=Null,"
                Else
                    Sql = Sql & " Parse_Desc='" & Parse_Desc & "',"
                End If

                If Brok_Ticker = "" Then
                    Sql = Sql & " Brok_Ticker=Null,"
                Else
                    Sql = Sql & " Brok_Ticker='" & Brok_Ticker & "',"
                End If
                If Brok_TransType = "" Then
                    Sql = Sql & " Brok_TransType=Null,"
                Else
                    Sql = Sql & " Brok_TransType='" & Brok_TransType & "',"
                End If

                If Brok_AssetClass = "" Then
                    Sql = Sql & " Brok_AssetClass=Null,"
                Else
                    Sql = Sql & " Brok_AssetClass='" & Brok_AssetClass & "',"
                End If

                If Brok_ExpCheckNo = "" Then
                    Sql = Sql & " Brok_ExpCheckNo=Null,"
                Else
                    Sql = Sql & " Brok_ExpCheckNo='" & Brok_ExpCheckNo & "',"
                End If
                If Brok_DepCheckNo = "" Then
                    Sql = Sql & " Brok_DepCheckNo=Null,"
                Else
                    Sql = Sql & " Brok_DepCheckNo='" & Brok_DepCheckNo & "',"
                End If

                If Brok_TransCat = "" Then
                    Sql = Sql & " Brok_TransCat=Null,"
                Else
                    Sql = Sql & " Brok_TransCat='" & Brok_TransCat & "',"
                End If

                If Brok_Medium = "" Then
                    Sql = Sql & " Brok_Medium=Null"
                Else
                    Sql = Sql & " Brok_Medium='" & Brok_Medium & "'"
                End If
                Sql = Sql & " where BrokTempID = " & BrokTempID
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
                ChaseResults.EditItemIndex = -1
                loadGrid(True)
                Label1.Text = "updated Successfully"
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString() ' & Sql
            End Try
        End Sub

        Protected Sub ChaseResults_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.CancelCommand
            Dim ds As DataSet = CType(Session("ChaseDataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim currentRowIndex As Integer
                currentRowIndex = e.Item.ItemIndex
                'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                ChaseResults.EditItemIndex = -1
                loadGrid(False)
            Else
                Return
            End If
        End Sub

        Protected Sub ChaseResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles ChaseResults.PageIndexChanged
            ChaseResults.CurrentPageIndex = e.NewPageIndex
            ChaseResults.EditItemIndex = -1
            loadGrid(False)
        End Sub

        Protected Sub ChaseResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim ChaseTempId As Integer

                ChaseTempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM BrokTemp WHERE BrokTempID =" + CStr(ChaseTempId))
                Catch se As SqlException
                    Return
                End Try
                loadGrid(True)
                ChaseResults.EditItemIndex = -1
            End If
        End Sub

        Protected Sub ChaseResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.EditCommand
            ChaseResults.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("ChaseDataSet"), DataSet)
            Dim page As Integer = ChaseResults.CurrentPageIndex
            Dim pageSize As Integer = ChaseResults.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            Cache("editRow") = dr
            loadGrid(False)
        End Sub

        Public Sub LoadBankTrans(ByVal BlnReload As Boolean)
            'Response.Write(Session("StrSql"))
            'Label1.Text = Session("StrSql")
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (BlnReload = True) Then
                Session("BankTransDataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("BankTransDataSet") = ds
                    BankTransResults.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("BankTransDataSet") Is Nothing) Then
                    ds = CType(Session("BankTransDataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                pnlBankTrans.Visible = False
                Label1.Text = "There is No Record to display"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                pnlBankTrans.Visible = False
                Label1.Text = "There is No Record to display"
            Else
                Dim i As Integer = ds.Tables(0).Rows.Count
                BankTransResults.DataSource = ds.Tables(0)
                BankTransResults.DataBind()
                pnlBankTrans.Visible = True
            End If
        End Sub

        Protected Sub BankTransResults_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.UpdateCommand
            'TransType,TransCat,CheckNumber,DepositNumber,VendCust,Reason,DepSlipNumber,GLAccount
           
            Dim BankTransID As Integer = CInt(e.Item.Cells(2).Text)
            Dim TransType As String = CType(e.Item.FindControl("txtTransType"), TextBox).Text
            'Dim TransCat As String = CType(e.Item.FindControl("txtTransCat"), TextBox).Text
            Dim TransCat As String = CType(e.Item.FindControl("ddlTransCat"), DropDownList).SelectedItem.Value
            Dim Medium As String = CType(e.Item.FindControl("ddlMedium"), DropDownList).SelectedItem.Value 'CType(e.Item.FindControl("txtMedium"), TextBox).Text
            Dim AssetClass As String = CType(e.Item.FindControl("txtAssetClass"), TextBox).Text
            Dim Ticker As String = CType(e.Item.FindControl("txtTicker"), TextBox).Text
            'If e.Item.Cells(5).Text = "TransferOut" Then
            Dim Quantity As String = CType(e.Item.FindControl("txtQuantity"), TextBox).Text
            Dim Price As String = CType(e.Item.FindControl("txtPrice"), TextBox).Text
            Dim NetAmount As String = CType(e.Item.FindControl("txtAmount"), TextBox).Text
            Dim TranID As String = CType(e.Item.FindControl("txtTranID"), TextBox).Text
            Dim ExpCheckNo As String = CType(e.Item.FindControl("txtExpCheckNo"), TextBox).Text
            Dim DepCheckNo As String = CType(e.Item.FindControl("txtDepCheckNo"), TextBox).Text
            Dim RestType As String = CType(e.Item.FindControl("ddlRestType"), DropDownList).SelectedItem.Value
            Dim ToRestType As String = CType(e.Item.FindControl("ddlToRestType"), DropDownList).SelectedItem.Value
            Dim Donation As String = CType(e.Item.FindControl("ddlDonation"), DropDownList).SelectedItem.Value

            'TransType,TransCat,Medium,AssetClass,Ticker,TranID,ExpCheckNo,DepCheckNo
            Dim Sql As String
            Sql = "update BrokTrans set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ","
            If TransType = "" Then
                Sql = Sql & " TransType=Null,"
            Else
                Sql = Sql & " TransType='" & TransType & "',"
            End If

            If TransCat = "" Then
                Sql = Sql & " TransCat=Null,"
            Else
                Sql = Sql & " TransCat='" & TransCat & "',"
            End If

            If Medium = "" Then
                Sql = Sql & " Medium=Null,"
            Else
                Sql = Sql & " Medium='" & Medium & "',"
            End If

            If AssetClass = "" Then
                Sql = Sql & " AssetClass=Null,"
            Else
                Sql = Sql & " AssetClass='" & AssetClass & "',"
            End If
            If Ticker = "" Then
                Sql = Sql & " Ticker=Null,"
            Else
                Sql = Sql & " Ticker='" & Ticker & "',"
            End If

            If Quantity = "" Then
                Sql = Sql & " Quantity=0.0,"
            Else
                Sql = Sql & " Quantity=" & Quantity & ","
            End If
            If Price = "" Then
                Sql = Sql & " Price=0.0,"
            Else
                Sql = Sql & " Price='" & Price & "',"
            End If
            If NetAmount = "" Then
                Sql = Sql & " NetAmount=0.00,"
            Else
                Sql = Sql & " NetAmount='" & NetAmount & "',"
            End If

            If TranID = "" Then
                Sql = Sql & " TranID=Null,"
            Else
                Sql = Sql & " TranID=" & TranID & ","
            End If

            If ExpCheckNo = "" Then
                Sql = Sql & " ExpCheckNo=Null,"
            Else
                Sql = Sql & " ExpCheckNo='" & ExpCheckNo & "',"
            End If
            If DepCheckNo = "" Then
                Sql = Sql & " DepCheckNo=Null,"
            Else
                Sql = Sql & " DepCheckNo='" & DepCheckNo & "',"
            End If
            If RestType = "" Then
                Sql = Sql & " RestType=Null, "
            Else
                Sql = Sql & " RestType='" & RestType & "', "
            End If
            If ToRestType = "" Then
                Sql = Sql & " ToRestType=Null, "
            Else
                Sql = Sql & " ToRestType='" & ToRestType & "', "
            End If
            If Donation = "" Or Donation = "N" Then
                Sql = Sql & " Donation=Null "
            Else
                Sql = Sql & " Donation='" & Donation & "' "
            End If
            Sql = Sql & " where BrokTransId = " & BankTransID
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
            Catch ex As Exception
                Label1.Text = ex.ToString()
            End Try
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set Parse_Desc='" & Parse_Desc & "',Parse_AddInfo='" & Parse_AddInfo & "',Bk_TransType='" & Bk_TransType & "',Bk_TransCat='" & Bk_TransCat & "',Bk_CheckNumber=" & Bk_CheckNumber & ",Bk_DepositNumber=" & Bk_DepositNumber & ",Bk_VendCust='" & Bk_VendCust & "',Bk_Reason='" & Bk_Reason & "' where HBT2tempId=" & HBT2tempId & "")
            BankTransResults.EditItemIndex = -1
            LoadBankTrans(True)
        End Sub

        Protected Sub BankTransResults_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.CancelCommand
            Dim ds As DataSet = CType(Session("BankTransDataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim page As Integer = BankTransResults.CurrentPageIndex
                Dim pageSize As Integer = BankTransResults.PageSize
                Dim currentRowIndex As Integer
                If (page = 0) Then
                    currentRowIndex = e.Item.ItemIndex
                Else
                    currentRowIndex = (page * pageSize) + e.Item.ItemIndex
                End If

                'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                BankTransResults.EditItemIndex = -1
                LoadBankTrans(False)
            Else
                Return
            End If
        End Sub

        Protected Sub BankTransResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim HBT2TempId As Integer

                HBT2TempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM HBT2TEMP WHERE HBT2TEMPID =" + CStr(HBT2TempId))
                Catch se As SqlException
                    Return
                End Try
                LoadBankTrans(True)
                BankTransResults.EditItemIndex = -1
            End If
        End Sub

        Protected Sub BankTransResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles BankTransResults.PageIndexChanged
            BankTransResults.CurrentPageIndex = e.NewPageIndex
            BankTransResults.EditItemIndex = -1
            LoadBankTrans(False)
        End Sub

        Protected Sub BankTransResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.EditCommand
            BankTransResults.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("BankTransDataSet"), DataSet)
            Dim page As Integer = BankTransResults.CurrentPageIndex
            Dim pageSize As Integer = BankTransResults.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            Cache("editRow") = dr
            StrTransCat = CType(e.Item.Cells(6).FindControl("lblTransCat"), Label).Text 'DropDownList).SelectedItem.Value
            StrMedium = CType(e.Item.Cells(7).FindControl("lblMedium"), Label).Text
            StrRestType = CType(e.Item.Cells(18).FindControl("lblRestType"), Label).Text
            StrToRestType = CType(e.Item.Cells(19).FindControl("lblToRestType"), Label).Text
            StrDonation = CType(e.Item.Cells(20).FindControl("lblDonation"), Label).Text

            'DropDownList ddl1 = (DropDownList)BankTransResults.Rows[0].Cells[1].FindControl("DropDownList1")

            LoadBankTrans(False)

        End Sub

        Sub postdata()
            Try
                Dim PTransTypeCnt, PDescCnt, PAddinfoCnt, count, tempcount As Integer
                count = 0
                Dim filename As String
                'Moving data from temp table to Original Table
                Select Case ddlPostDB.SelectedValue
                    Case 4, 5, 6, 7
                        ' Coding for ChaseTemmp
                        'PTransTypeCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where Parse_TransType is null or Parse_TransType <>'Y'")
                        'PDescCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where Parse_Desc is null or Parse_Desc <>'Y'")
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from Broktemp")
                        If tempcount > 0 Then
                            '    If PTransTypeCnt > 0 Then
                            '        Label1.Text = "One or more transactions were not parsed (TransType)"
                            '    ElseIf PDescCnt > 0 Then
                            '        Label1.Text = "One or more transactions were not parsed (Description)"
                            '    Else
                            '        count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans")
                            Dim i As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BrokTransID) from BrokTrans")
                            ExecuteQuery("Insert Into BrokTrans(BankID, BankCode, TransDate, TransType, TransCat, Medium, AssetClass, Ticker, Quantity, Price, NetAmount, Comm, TranID, ExpCheckNo,DepCheckNo, Description,CreatedDate, CreatedBy) select  Brok_BankID, LTRIM(RTRIM(Brok_BankCode)),Date, Brok_TransType, Brok_TransCat, Brok_Medium, Brok_AssetClass, Brok_Ticker, Quantity,  Price, NetAmount, Comm, TranID, Brok_ExpCheckNo, Brok_DepCheckNo,Description,GETDATE()," & Session("loginID") & " from BrokTemp Where Brok_AssetClass is Not Null and  Brok_TransType is not null and Brok_Ticker is not null ")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from BrokTemp Where  Brok_AssetClass is Not Null and  Brok_TransType is not null and Brok_Ticker is not null")
                            '    filename = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 filename from fileupload where filetype like '" & &  "' order by fileID desc")
                            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BeginRecID, BegRecdate, EndRecID, EndRecDate, TempRecRej, Postedby,CreatedDate) Select '" & filename & "'," & tempcount & "," & count + 1 & ", MIN(TransDate),MAX(BankTransID),MAX(TransDate),0," & Session("LoginID") & ",GetDate() from BankTrans where BANKID = 1 and banktransID>=" & count + 1 & "")
                            Label1.Text = "Records Inserted Successfully : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BrokTransID) from BrokTrans") - i
                            'End If
                        Else
                            Label1.Text = "No Record found to post."
                        End If
                    Case Else
                        Label1.Text = "Please select The type to be posted"
                End Select
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                If RbtnVerifyTrans.Checked = True Then
                    Select Case ddlVerifyTrans.SelectedValue
                        Case 4, 5, 6, 7
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.ModifiedDate=GetDate(), Bk.ModifiedBy=" & Session("LoginID") & ", Bk.Brok_BankID=B.BankID, Bk.Brok_BankCode=LTRIM(RTRIM(B.BankCode)),Bk.Brok_Ticker=Case WHEN Bk.Symbol Is Null Then 'Cash' Else  Bk.Symbol END,Bk.Brok_TransType = Bk.Type ,Bk.Brok_TransCat = Case WHEN Bk.Type in ('Div','Buy','Sell','Split')  THEN Bk.Type Else Null END from BrokTemp Bk Inner Join Bank B On Bk.AcctNumber = B.ActNumber")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Bk set Bk.Brok_AssetClass =  T.AssetClass  from BrokTemp Bk Inner Join ticker T On T.Ticker = Bk.Brok_Ticker")

                            VerifyDesc()
                    End Select
                End If
                TrMsg.Visible = False
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub closepanel()
            PnlChaseTemp.Visible = False
            'add/Modify Panel
            pnlChaseDesc.Visible = False
            pnlTicker.Visible = False
            'pnlBankTrans.Visible = False
            If RbtnEditUpdate.Checked <> True Then
                ddlEditUpdate.SelectedIndex = ddlEditUpdate.Items.IndexOf(ddlEditUpdate.Items.FindByValue(0))
                'ddlEditUpdateColumn.Enabled = False
                'ddlEditUpdateSel.Enabled = False
            End If
            If RbtnEditUpdateDB.Checked <> True Then
                ddlEditUpdateDB.SelectedIndex = ddlEditUpdateDB.Items.IndexOf(ddlEditUpdateDB.Items.FindByValue(0))
                ddlEditUpdateColumnDB.SelectedIndex = ddlEditUpdateColumnDB.Items.IndexOf(ddlEditUpdateColumnDB.Items.FindByValue(0))
                'ddlEditUpdateColumnDB.Enabled = False
            End If
            If RbtnVerifyTrans.Checked <> True Then
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
            End If
            If RbtnUploadFile.Checked <> True Then
                ddlBankUpload.SelectedIndex = ddlBankUpload.Items.IndexOf(ddlBankUpload.Items.FindByValue(0))
            End If
            If RbtnPostDB.Checked <> True Then
                ddlPostDB.SelectedIndex = ddlPostDB.Items.IndexOf(ddlPostDB.Items.FindByValue(0))
            End If

        End Sub
        Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                TrMsg.Visible = False
                Select Case ddlVerifyTrans.SelectedValue
                    Case 4, 5, 6, 7
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table BrokTemp")

                End Select
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
                Label1.Text = "Upload the missing csv file first"
                'ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        'Protected Sub ddlEditUpdate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '    closepanel()
        '    If ddlEditUpdate.SelectedValue = 4 Or ddlEditUpdate.SelectedValue = 5 Or ddlEditUpdate.SelectedValue = 6 Or ddlEditUpdate.SelectedValue = 7 Then
        '        ddlEditUpdateColumn.Items.Clear()
        '        ddlEditUpdateColumn.Items.Add("Search Column")
        '        ddlEditUpdateColumn.Items.Add("Parse_Desc")
        '        ddlEditUpdateColumn.Items.Add("Posted")
        '        ddlEditUpdateColumn.Items.Add("Brok_BankID")
        '        ddlEditUpdateColumn.Items.Add("Brok_BankCode")
        '        ddlEditUpdateColumn.Items.Add("Brok_Ticker")
        '        ddlEditUpdateColumn.Items.Add("Brok_TransType")
        '        ddlEditUpdateColumn.Items.Add("Brok_AssetClass")
        '        ddlEditUpdateColumn.Items.Add("Brok_TransCat")
        '        ddlEditUpdateColumn.Enabled = True
        '    Else
        '        ddlEditUpdateColumn.Enabled = False
        '        ddlEditUpdateSel.Enabled = False
        '        Label1.Text = "Please Select valid Account"
        '    End If
        'End Sub

        Private Sub GetChaseDesc()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM BrokDesc")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridChaseDesc.DataSource = dt
            GridChaseDesc.DataBind()
            pnlChaseDesc.Visible = True
        End Sub
        Protected Sub GridChaseDesc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridChaseDesc.RowCommand
            BtnAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridChaseDesc.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM BrokDesc where BrokDescId=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtToken.Text = dsRecords.Tables(0).Rows(0)("Token").ToString()
            txtBk_TransCat.Text = dsRecords.Tables(0).Rows(0)("Brok_TransCat").ToString()
            txtBk_VendorCust.Text = dsRecords.Tables(0).Rows(0)("Brok_Medium").ToString()
            txtBk_Reason.Text = dsRecords.Tables(0).Rows(0)("Notes").ToString()
        End Sub
        Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
            Try
                Dim Strsql As String
                If BtnAdd.Text = "Add" Then
                    If txtToken.Text.Length > 2 And (txtBk_TransCat.Text.Length > 2 Or txtBk_VendorCust.Text.Length > 2) Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from BrokDesc where Token  Like '" & txtToken.Text & "%'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Dim token, Bk_TransCat, Bk_VendorCust, Bk_Reason As String
                            If txtToken.Text.Length > 1 Then
                                token = "'" & txtToken.Text & "'"
                            Else
                                token = "null"
                            End If
                            If txtBk_TransCat.Text.Length > 1 Then
                                Bk_TransCat = "'" & txtBk_TransCat.Text & "'"
                            Else
                                Bk_TransCat = "Null"
                            End If
                            If txtBk_VendorCust.Text.Length > 1 Then
                                Bk_VendorCust = "'" & txtBk_VendorCust.Text & "'"
                            Else
                                Bk_VendorCust = "Null"
                            End If
                            If txtBk_Reason.Text.Length > 1 Then
                                Bk_Reason = "'" & txtBk_Reason.Text & "'"
                            Else
                                Bk_Reason = "Null"
                            End If
                            Strsql = "Insert Into BrokDesc(Token, Brok_TransCat, Brok_Medium, Notes) Values ("
                            Strsql = Strsql & token & "," & Bk_TransCat & "," & Bk_VendorCust & "," & Bk_Reason & ")"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetChaseDesc()
                            lblChaseDesc.Text = "Record Added Successfully"
                            clear()
                        Else
                            lblChaseDesc.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblChaseDesc.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnAdd.Text = "Update" Then
                    If txtToken.Text.Length > 2 And (txtBk_TransCat.Text.Length > 2 Or txtBk_VendorCust.Text.Length > 2) Then
                        Dim Bk_TransCat, Bk_VendorCust, Bk_Reason As String

                        If txtBk_TransCat.Text.Length > 1 Then
                            Bk_TransCat = "'" & txtBk_TransCat.Text & "'"
                        Else
                            Bk_TransCat = "Null"
                        End If
                        If txtBk_VendorCust.Text.Length > 1 Then
                            Bk_VendorCust = "'" & txtBk_VendorCust.Text & "'"
                        Else
                            Bk_VendorCust = "Null"
                        End If
                        If txtBk_Reason.Text.Length > 1 Then
                            Bk_Reason = "'" & txtBk_Reason.Text & "'"
                        Else
                            Bk_Reason = "Null"
                        End If
                        Strsql = "Update BrokDesc Set Token='" & txtToken.Text & "',Brok_TransCat=" & Bk_TransCat & ",Brok_Medium=" & Bk_VendorCust & ",Notes=" & Bk_Reason & " where BrokDescID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetChaseDesc()
                        lblChaseDesc.Text = "Record Updated Successfully"
                        clear()
                        BtnAdd.Text = "Add"
                    Else
                        lblChaseDesc.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                lblChaseDesc.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
            clear()
        End Sub
        Sub clear()
            txtToken.Text = ""
            txtBk_TransCat.Text = ""
            txtBk_VendorCust.Text = ""
            txtBk_Reason.Text = ""
            Session("ID") = Nothing
            BtnAdd.Text = "Add"
        End Sub

        Protected Sub btnChase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChase.Click
            GetChaseDesc()
            pnlTicker.Visible = False
        End Sub

        Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            clear()
            pnlChaseDesc.Visible = False
        End Sub

        Private Sub GetTicker()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM TICKER")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridTicker.DataSource = dt
            GridTicker.DataBind()
            pnlTicker.Visible = True
        End Sub
        Protected Sub GridTicker_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridTicker.RowCommand
            BtnTicAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridTicker.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetTransSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetTransSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM Ticker where Id=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
            txtTicker.Text = dsRecords.Tables(0).Rows(0)("Ticker").ToString()
            txtClass.Text = dsRecords.Tables(0).Rows(0)("Class").ToString()
            txtYear.Text = dsRecords.Tables(0).Rows(0)("Year").ToString()
            txtAssetClass.Text = dsRecords.Tables(0).Rows(0)("AssetClass").ToString()
        End Sub
        Protected Sub BtnTicAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTicAdd.Click
            Try
                Dim Strsql As String
                Dim Name, Ticker, Classs, Year, AssetClass As String
                If BtnTicAdd.Text = "Add" Then
                    If txtTicker.Text.Length > 2 And txtAssetClass.Text.Length > 2 Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from Ticker where Ticker = '" & txtTicker.Text & "'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            If txtName.Text.Length > 1 Then
                                Name = "'" & txtName.Text & "'"
                            Else
                                Name = "null"
                            End If

                            If txtTicker.Text.Length > 1 Then
                                Ticker = "'" & txtTicker.Text & "'"
                            Else
                                Ticker = "null"
                            End If

                            If txtClass.Text.Length > 1 Then
                                Classs = "'" & txtClass.Text & "'"
                            Else
                                Classs = "null"
                            End If

                            If txtYear.Text.Length > 1 Then
                                Year = txtYear.Text
                            Else
                                Year = "null"
                            End If

                            If txtAssetClass.Text.Length > 1 Then
                                AssetClass = "'" & txtAssetClass.Text & "'"
                            Else
                                AssetClass = "Null"
                            End If
                            Strsql = "Insert Into Ticker( Name, Ticker, Class, Year, AssetClass) Values ("
                            Strsql = Strsql & Name & "," & Ticker & "," & Classs & "," & Year & "," & AssetClass & ")"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetTicker()
                            lblTicker.Text = "Record Added Successfully"
                            Transclear()
                        Else
                            lblTicker.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblTicker.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnTicAdd.Text = "Update" Then
                    If txtTicker.Text.Length > 2 And txtAssetClass.Text.Length > 2 Then

                        If txtName.Text.Length > 1 Then
                            Name = "'" & txtName.Text & "'"
                        Else
                            Name = "null"
                        End If

                        If txtTicker.Text.Length > 1 Then
                            Ticker = "'" & txtTicker.Text & "'"
                        Else
                            Ticker = "null"
                        End If

                        If txtClass.Text.Length > 1 Then
                            Classs = "'" & txtClass.Text & "'"
                        Else
                            Classs = "null"
                        End If

                        If txtYear.Text.Length > 1 Then
                            Year = txtYear.Text
                        Else
                            Year = "null"
                        End If

                        If txtAssetClass.Text.Length > 1 Then
                            AssetClass = "'" & txtAssetClass.Text & "'"
                        Else
                            AssetClass = "Null"
                        End If

                        Strsql = "Update Ticker Set Name=" & Name & ", Ticker=" & Ticker & ", Class=" & Classs & ", Year=" & Year & ", AssetClass=" & AssetClass & " where ID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetTicker()
                        lblTicker.Text = "Record Updated Successfully"
                        Transclear()
                        BtnTicAdd.Text = "Add"
                    Else
                        lblTicker.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                lblTicker.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnTicCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTicCancel.Click
            Transclear()
        End Sub
        Sub Transclear()
            txtName.Text = ""
            txtClass.Text = ""
            txtAssetClass.Text = ""
            txtTicker.Text = ""
            txtYear.Text = ""
            BtnTicAdd.Text = "Add"
        End Sub

        'Protected Sub btnChaseTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChaseTrans.Click
        '    GetChaseTransType()
        '    pnlChaseDesc.Visible = False
        'End Sub

        Protected Sub BtnTicClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTicClose.Click
            Transclear()
            pnlTicker.Visible = False
        End Sub
        Sub ExecuteQuery(ByVal StrSQL As String)
            Dim conn As New SqlConnection
            conn.ConnectionString = Application("ConnectionString")
            Dim cmd As SqlCommand = New SqlCommand(StrSQL, conn)
            conn.Open()
            cmd.CommandTimeout = 600
            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            conn.Close()
        End Sub

        Protected Sub ddlEditUpdateDB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            closepanel()
            ddlEditUpdateColumnDB.Enabled = True
        End Sub
        Public Sub ChaseBk_TransType_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transType from ChaseTransType Where Bk_transType is NOt NULL order by Bk_transType Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transType"), dsRecords("Bk_transType")))
                End While
            End If
        End Sub
        Public Sub ChaseBk_TransType_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransType As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransType") Is DBNull.Value) Then
                    Bk_TransType = dr.Item("Bk_TransType")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransType))
            Else
                Return
            End If
        End Sub
        Public Sub ChaseBk_TransCat_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransCat As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransCat") Is DBNull.Value) Then
                    Bk_TransCat = dr.Item("Bk_TransCat")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransCat))
            Else
                Return
            End If
        End Sub
        Public Sub ChaseBk_TransCat_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Brok_transCat from BrokeDesc Where Brok_transCat is Not NULL order by Brok_transCat Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transCat"), dsRecords("Bk_transCat")))
                End While
            End If
        End Sub

        Protected Sub lbtnVolunteerFunctions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Session.Remove("StrSQL")
            Session.Remove("HBT1DataSet")
            Session.Remove("HBT2DataSet")
            Session.Remove("ChaseDataSet")
            Session.Remove("CTransLastDate")
            Session.Remove("ID")
            Cache.Remove("editRow")
            Response.Redirect("~/VolunteerFunctions.aspx")
        End Sub

        Protected Sub ddlEditUpdateColumn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            ' ddlEditUpdateSel
            '9 - Brok_TransType :  Null,  BUY, DIV, SELL, TRN,VTR, TRD, Split, None of Above
            '10 - Brok_AssetClass: Null, Cash, MMK, MutFund, Stock
            '11 - Brok_TransCat: Null, BUY, DIV, Grants, SELL, Split,Reinvest, TransferIn, TransferOut
            If ddlEditUpdateColumn.SelectedValue = "0" Then
                ddlEditUpdateSel.Enabled = False
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Items.Add("Null")
            ElseIf ddlEditUpdateColumn.SelectedValue = "1" Then
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Enabled = True
                ddlEditUpdateSel.Items.Add("Null")
                ddlEditUpdateSel.Items.Add("Yes")
            ElseIf ddlEditUpdateColumn.SelectedValue = "9" Then
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Enabled = True
                ddlEditUpdateSel.Items.Add("Null")
                ddlEditUpdateSel.Items.Add("BUY")
                ddlEditUpdateSel.Items.Add("DIV")
                ddlEditUpdateSel.Items.Add("SELL")
                ddlEditUpdateSel.Items.Add("TRN")
                ddlEditUpdateSel.Items.Add("VTR")
                ddlEditUpdateSel.Items.Add("TRD")
                ddlEditUpdateSel.Items.Add("Split")
                ddlEditUpdateSel.Items.Add("None of Above")
            ElseIf ddlEditUpdateColumn.SelectedValue = "10" Then
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Enabled = True
                ddlEditUpdateSel.Items.Add("Null")
                ddlEditUpdateSel.Items.Add("Cash")
                ddlEditUpdateSel.Items.Add("MMK")
                ddlEditUpdateSel.Items.Add("MutFund")
                ddlEditUpdateSel.Items.Add("Stock")
            ElseIf ddlEditUpdateColumn.SelectedValue = "11" Then
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Enabled = True
                ddlEditUpdateSel.Items.Add("Null")
                ddlEditUpdateSel.Items.Add("BUY")
                ddlEditUpdateSel.Items.Add("DIV")
                ddlEditUpdateSel.Items.Add("Grants")
                ddlEditUpdateSel.Items.Add("SELL")
                ddlEditUpdateSel.Items.Add("Split")
                ddlEditUpdateSel.Items.Add("Reinvest")
                ddlEditUpdateSel.Items.Add("TransferIn")
                ddlEditUpdateSel.Items.Add("TransferOut")
            Else
                ddlEditUpdateSel.Items.Clear()
                ddlEditUpdateSel.Enabled = True
                ddlEditUpdateSel.Items.Add("Null")
                ddlEditUpdateSel.Items.Add("Not Null")
            End If
            '        ddlEditUpdateSel.Items.Clear()
            '        ddlEditUpdateSel.Items.Add("Search Column")
            '        ddlEditUpdateSel.Items.Add("Parse_Desc")
            '        ddlEditUpdateSel.Items.Add("Posted")
            '        ddlEditUpdateSel.Items.Add("Brok_BankID")
            '        ddlEditUpdateSel.Items.Add("Brok_BankCode")
            '        ddlEditUpdateSel.Items.Add("Brok_Ticker")
            '        ddlEditUpdateSel.Items.Add("Brok_TransType")
            '        ddlEditUpdateSel.Items.Add("Brok_AssetClass")

        End Sub

        Protected Sub ddlEditUpdateColumnDB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            '1.Ticker:     Null, Not Null
            '2.TransType :  Null,  BUY, DIV, SELL, TRN,VTR, TRD, Split, None of Above
            '3.AssetClass: Null, Cash, MMK, MutFund, Stock
            '4.TransCat: Null,  BUY, DIV, Donation, Expense, Grants, Reinvest, Reverse Split,  SELL, Split,Reinvest, Transfer In, Transfer Out
            '5.ExpCheckNo: Null, Not Null
            '6.DepCheckNo: Null, Not Null
            '7.Medium:     Null, Not Null
            'ddlEditUpdateSelDB
            If ddlEditUpdateColumnDB.SelectedValue = "0" Then
                ddlEditUpdateSelDB.Enabled = False
                ddlEditUpdateSelDB.Items.Clear()
                ddlEditUpdateSelDB.Items.Add("Null")
            ElseIf ddlEditUpdateColumnDB.SelectedValue = "2" Then
                ddlEditUpdateSelDB.Items.Clear()
                ddlEditUpdateSelDB.Enabled = True
                ddlEditUpdateSelDB.Items.Add("Null")
                ddlEditUpdateSelDB.Items.Add("BUY")
                ddlEditUpdateSelDB.Items.Add("DIV")
                ddlEditUpdateSelDB.Items.Add("SELL")
                ddlEditUpdateSelDB.Items.Add("TRN")
                ddlEditUpdateSelDB.Items.Add("VTR")
                ddlEditUpdateSelDB.Items.Add("TRD")
                ddlEditUpdateSelDB.Items.Add("Split")
                ddlEditUpdateSelDB.Items.Add("None of Above")
            ElseIf ddlEditUpdateColumnDB.SelectedValue = "3" Then
                ddlEditUpdateSelDB.Items.Clear()
                ddlEditUpdateSelDB.Enabled = True
                ddlEditUpdateSelDB.Items.Add("Null")
                ddlEditUpdateSelDB.Items.Add("Cash")
                ddlEditUpdateSelDB.Items.Add("MMK")
                ddlEditUpdateSelDB.Items.Add("MutFund")
                ddlEditUpdateSelDB.Items.Add("Stock")
            ElseIf ddlEditUpdateColumnDB.SelectedValue = "4" Then
                ddlEditUpdateSelDB.Items.Clear()
                ddlEditUpdateSelDB.Enabled = True
                ddlEditUpdateSelDB.Items.Add("Null")
                ddlEditUpdateSelDB.Items.Add("BUY")
                ddlEditUpdateSelDB.Items.Add("DIV")
                ddlEditUpdateSelDB.Items.Add("Grants")
                ddlEditUpdateSelDB.Items.Add("SELL")
                ddlEditUpdateSelDB.Items.Add("Split")
                ddlEditUpdateSelDB.Items.Add("Reinvest")
                ddlEditUpdateSelDB.Items.Add("STCGDiv")
                ddlEditUpdateSelDB.Items.Add("OrdDiv")
                ddlEditUpdateSelDB.Items.Add("LTCGDiv")
                ddlEditUpdateSelDB.Items.Add("TransferIn")
                ddlEditUpdateSelDB.Items.Add("TransferOut")
            Else
                ddlEditUpdateSelDB.Items.Clear()
                ddlEditUpdateSelDB.Enabled = True
                ddlEditUpdateSelDB.Items.Add("Null")
                ddlEditUpdateSelDB.Items.Add("Not Null")
            End If
        End Sub

        Protected Sub btnTicker_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTicker.Click
            GetTicker()
            pnlChaseDesc.Visible = False
        End Sub

        Protected Sub BankTransResults_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BankTransResults.SelectedIndexChanged

        End Sub
        Public Sub SetTransCat_Dropdown(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(StrTransCat))
            Catch ex As Exception
            End Try
        End Sub
        Public Sub SetMedium_Dropdown(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(StrMedium))
            Catch ex As Exception
            End Try
        End Sub
        Public Sub SetRestType_Dropdown(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(StrRestType))
            Catch ex As Exception
            End Try
        End Sub

        Public Sub SetToRestType_Dropdown(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(StrToRestType))
            Catch ex As Exception
            End Try
        End Sub
        Public Sub SetToRestType_Donation(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(StrDonation))
            Catch ex As Exception
            End Try
        End Sub
    End Class
End Namespace