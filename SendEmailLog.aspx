﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="SendEmailLog.aspx.cs" Inherits="SendEmailLog" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

    <script type="text/javascript">

        $(function () {
            var pickerOpts = {
                dateFormat: $.datepicker.regional['fr']
            };
            $("#<%=txtFrom.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtTo.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });

        });

    </script>

    <style type="text/css">
        .ui-datepicker-trigger {
            padding-left: 5px;
            position: relative;
            top: 3px;
        }

        .ui-datepicker {
            font-size: 8pt !important;
        }

        .style8 {
            width: 99px;
        }

        .style10 {
            width: 147px;
        }

        .style12 {
            width: 84px;
            text-align: left;
        }

        .style16 {
            width: 126px;
        }

        .style17 {
            width: 88px;
        }

        .style20 {
            width: 113px;
            text-align: left;
        }

        .style47 {
            width: 100%;
        }

        .style53 {
            width: 179px;
        }

        .style60 {
            width: 68px;
        }

        .style67 {
            width: 15px;
            font-weight: bold;
        }

        .style70 {
            width: 286px;
        }

        .style87 {
        }

        .style90 {
            width: 184px;
        }

        .style95 {
            width: 93px;
        }

        .style97 {
            width: 117px;
        }

        .style98 {
            width: 37px;
        }

        .style99 {
            width: 120px;
            text-align: left;
        }

        .style101 {
            width: 116px;
        }

        .style102 {
            width: 10px;
        }

        .style118 {
            width: 286px;
            text-align: left;
        }

        .style120 {
            width: 15px;
        }

        .style121 {
            width: 63px;
        }

        .style129 {
            width: 133px;
        }

        .style1 {
            width: 100%;
            height: 92px;
        }

        .style158 {
            width: 95px;
        }

        .style160 {
            width: 271px;
            text-align: left;
        }

        .style162 {
            width: 121px;
        }

        .style163 {
            width: 4px;
        }

        .style165 {
        }

        .style167 {
            text-align: left;
        }

        .style176 {
            width: 199px;
            text-align: left;
        }

        .style177 {
            width: 101px;
            text-align: left;
        }

        .style178 {
            width: 95px;
            text-align: left;
        }

        .style185 {
            width: 152px;
        }

        .style186 {
            width: 101px;
        }

        .style187 {
            width: 181px;
            text-align: left;
        }

        .style191 {
            width: 158px;
        }

        .style195 {
            font-weight: bold;
            width: 158px;
        }

        .style197 {
            width: 54px;
            font-weight: bold;
        }

        .style198 {
            width: 54px;
        }

        .style199 {
            text-align: left;
            width: 182px;
        }

        .style200 {
            width: 120px;
        }

        .style201 {
            width: 179px;
            height: 29px;
        }

        .style202 {
            width: 68px;
            height: 29px;
        }

        .style203 {
            width: 15px;
            height: 29px;
        }

        .style204 {
            width: 10px;
            height: 29px;
        }

        .style205 {
            width: 117px;
            height: 29px;
        }

        .style206 {
            height: 29px;
        }

        .style207 {
            width: 286px;
            height: 29px;
        }
    </style>


    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="VolunteerFunctions.aspx" runat="server">  Back to Volunteer Functions</asp:HyperLink>

    </div>
    <br />
    <asp:Label ID="lblError" runat="server" Text="" Visible="false"></asp:Label>
    <br />
    <asp:Panel runat="server" ID="Pnldefault">
        <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);">
            Send Email Log
        
            <br />

            <br />
            <table style="width: 50%">
                <tr>
                    <td align="left" nowrap="nowrap" style="width: 61px">Source</td>
                    <td align="left" nowrap="nowrap" style="width: 133px">
                        <asp:DropDownList ID="ddlSource" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddlSource_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="0">[Select Source]</asp:ListItem>
                            <asp:ListItem Value="1">All</asp:ListItem>
                            <asp:ListItem Value="2">EmailCoach</asp:ListItem>
                            <asp:ListItem Value="3">Email_CC</asp:ListItem>
                            <asp:ListItem Value="4">Email_NC</asp:ListItem>
                            <asp:ListItem Value="5">Email_OnlineWS</asp:ListItem>

                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap" style="width: 44px">Zone</td>
                    <td align="left" nowrap="nowrap" style="width: 133px">
                        <asp:DropDownList ID="ddlZone" runat="server" Style="margin-left: 0px" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">Cluster</td>
                    <td align="left" nowrap="nowrap" style="width: 134px">
                        <asp:DropDownList ID="ddlCluster" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddlCluster_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">Chapter</td>
                    <td align="left" nowrap="nowrap">
                        <asp:DropDownList ID="ddlChapter" runat="server" Width="125px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">From Email</td>
                    <td align="left" nowrap="nowrap">
                        <asp:DropDownList ID="ddlFromEmail" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">Login Email</td>
                    <td align="left" nowrap="nowrap">
                        <asp:DropDownList ID="ddlLoginEmail" runat="server" Width="105px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">From</td>
                    <td align="left" nowrap="nowrap">
                        <asp:TextBox ID="txtFrom" runat="server" Width="70px"></asp:TextBox>
                    </td>
                    <td align="left" nowrap="nowrap">To</td>
                    <td align="left" nowrap="nowrap">
                        <asp:TextBox ID="txtTo" runat="server" Width="70px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" nowrap="nowrap" style="width: 61px">&nbsp;</td>
                    <td align="left" nowrap="nowrap" colspan="7">
                        <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" Text="Submit" Width="63px" />
                        &nbsp;
                        <asp:Button ID="btn_export" runat="server" Enabled="False" OnClick="btn_export_Click" Text="Export To Excel" Width="116px" />
                    </td>
                </tr>
            </table>

            <br />
            <asp:Label ID="lblnorecord" runat="server" Text="No records exist" ForeColor="Red" Visible="false" Style="font-size: x-large"></asp:Label>


        </div>
    </asp:Panel>
    <asp:GridView ID="GridEmail" runat="server" AllowPaging="True" OnPageIndexChanging="GridEmail_PageIndexChanging" PageSize="50">
    </asp:GridView>

</asp:Content>

