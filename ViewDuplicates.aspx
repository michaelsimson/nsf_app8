<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewDuplicates.aspx.vb" Inherits="ViewDuplicates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
    
				<tr bgcolor="#FFFFFF">	
				    <td colspan="4">				
					    <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx" >Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;					    
					</td>
				</tr>	
				<tr bgcolor="#FFFFFF">
					<td  colspan="2" align="left">
					
					<table cellpadding = "3" cellspacing ="0" border = "0" style ="width:900px">
					<tr bgcolor="#FFFFFF">
					<td  class="Heading" colspan="4">Duplicate Records</td>
				</tr>
				<tr bgcolor="#FFFFFF" >
				    <td class ="SmallFont"  colspan="2" align="right" style="width:50%" >
				        Select Status : 
				        <asp:DropDownList runat="server" ID="ddlStatus" Width="200px">
				            <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
				            <asp:ListItem Text="Open - Non Completed Cases" Value="Open"></asp:ListItem>
				            <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
				            <asp:ListItem Text="CUSTOMER CARE" Value="CUSTOMER CARE"></asp:ListItem>
				            <asp:ListItem Text="FORGOT PASSWORD" Value="FORGOT PASSWORD"></asp:ListItem>
				            <asp:ListItem Text="FORGOT LOGIN ID" Value="FORGOT LOGIN ID"></asp:ListItem>
				            <asp:ListItem Text="Invalid Email - Know Pwd" Value="Invalid Email - Know Pwd"></asp:ListItem>
				             <asp:ListItem Text="Invalid Email � Forgot Pwd" Value="Invalid Email � Forgot Pwd"></asp:ListItem>
				        </asp:DropDownList>&nbsp;&nbsp;
				    </td>
				     <td class ="SmallFont"  colspan="2" align="left" >&nbsp;&nbsp;
				        Select #of Records : 
				        <asp:DropDownList runat="server" ID="DdlRecords" Width="100px">
				            <asp:ListItem Text="100" Value="100" Selected="True"></asp:ListItem>
				            <asp:ListItem Text="500" Value="500"></asp:ListItem>
				            <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
				            <asp:ListItem Text="All" Value="All"></asp:ListItem>
				        </asp:DropDownList>
				    </td>
				</tr>			
				<tr>
				    <td class="ItemCenter"  colspan="4" align="center" >
				        <asp:Button runat="server" ID="btnRunReport"  Text="Run Report"/>
				    </td>
				</tr>
					</table>
				    </td>
				</tr>
				<asp:Panel runat="server" ID="pnlReport">
				<tr bgcolor="#FFFFFF">
					<td colspan="2">
					    <asp:datagrid id="dgDuplicates" runat="server" Width="100%"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="IndDupID" AllowPaging="True" AllowSorting="True" PageSize="30">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
							<ItemStyle Wrap="False" ></ItemStyle>
							<HeaderStyle  Wrap="False"></HeaderStyle>
							<Columns>
							    
                                <asp:EditCommandColumn EditText="Edit" UpdateText="Update /" CancelText="Cancel"></asp:EditCommandColumn>
							    <asp:TemplateColumn  ItemStyle-Wrap="false" HeaderStyle-Wrap="false"  HeaderText="Select(Existing)">
							        <ItemTemplate >
							            <asp:HyperLink  Text="View IndSpouse" runat="server" ID="hlnkView" NavigateUrl="javascript:var w=window.open('ShowIndRecord.aspx','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"  ></asp:HyperLink>
							        </ItemTemplate>							        
							    </asp:TemplateColumn>
							      <asp:TemplateColumn  ItemStyle-Wrap="false" HeaderStyle-Wrap="false"  HeaderText="Create(New)">
							        <ItemTemplate >
							            <asp:HyperLink  Text="Create IndSpouse" runat="server" ID="hlnkCreate" NavigateUrl="javascript:var w=window.open('CreateIndSpouse.aspx','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"  ></asp:HyperLink>							            
							        </ItemTemplate>							        
							    </asp:TemplateColumn>
							     <asp:TemplateColumn  ItemStyle-Wrap="false" HeaderStyle-Wrap="false"  HeaderText="Change Email(Existing)">
							        <ItemTemplate >
							            <asp:HyperLink  Text="Change Email" runat="server" ID="hlnkEmailChange" NavigateUrl="javascript:var w=window.open('VolChangeEmail.aspx','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no;');"  ></asp:HyperLink>							            
							        </ItemTemplate>							        
							    </asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Ind Duplicate ID" >
									<ItemTemplate>
										<asp:Label id="lblDupID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IndDupID") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Entry Token" >
									<ItemTemplate>
										<asp:Label id="lblEntryToken" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EntryToken") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Status" >
									<ItemTemplate>
										<asp:Label id="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>	
									<EditItemTemplate>
									    <asp:DropDownList runat="server" ID="ddlStatus">
									        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
									        <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
									    </asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="AutomemberID" >
									<ItemTemplate>
										<asp:Label id="lblAutomemberID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AutoMemberID") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Donor Type" >
									<ItemTemplate>
										<asp:Label id="lblDonorType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Relationship" >
									<ItemTemplate>
										<asp:Label id="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Relationship") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>								
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="First Name" >
									<ItemTemplate>
										<asp:Label id="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Last Name" >
									<ItemTemplate>
										<asp:Label id="lblLastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Address1" >
									<ItemTemplate>
										<asp:Label id="lblAddress1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address1") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Address2" Visible="false" >
									<ItemTemplate>
										<asp:Label id="lblAddress2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address2") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="City" >
									<ItemTemplate>
										<asp:Label id="lblCity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="State" >
									<ItemTemplate>
										<asp:Label id="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Zip" >
									<ItemTemplate>
										<asp:Label id="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Home Phone" >
									<ItemTemplate>
										<asp:Label id="lblHomePhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HPhone") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Email" >
									<ItemTemplate>
										<asp:Label id="lblEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="CreateDate">
									<ItemTemplate>
										<asp:Label id="lblCreateDate" runat="server" Text='<%#   DateTime.Parse(DataBinder.Eval(Container.DataItem,"CreateDate")).ToShortDateString()%>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false"  HeaderText="Remarks" >
									<ItemTemplate>
										<asp:Label id="lblRemarks" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Remarks") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
									<EditItemTemplate>
									    <asp:TextBox TextMode="MultiLine" runat="server" ID="txtRemarks" Rows="4" Columns="10"></asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				</asp:Panel>
				<tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center" colspan="2"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red" Font-Size="Medium"></asp:label></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  class="Heading" colspan="2" height="15"></td>
				</tr>
 				<tr bgcolor="#FFFFFF">
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label></td>
				</tr> 
	            </table>
</asp:Content>



 
 
 