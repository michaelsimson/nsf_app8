﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ContestTimings : System.Web.UI.Page
{
    string LoginIDColumnName = "LoginID";
    string LoginChapterID = "LoginChapterID";
    string ConnectionString = "ConnectionString";
    string retFlag = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //  Session["EntryToken"]= "VOLUNTEER";
        //Session["RoleId"] = 1; //89;
        //Session["LoginID"] = 4240 ;//22214       ;
        //Session["LoggedIn"] = "true";
        //Session[LoginChapterID] = 1;  
        if (Session[LoginIDColumnName] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                Events();

                ChapterDrop(ddchapter);
                //BindBeeBookSchedule();
                if (Session[LoginChapterID].ToString() != string.Empty)
                {
                    ddchapter.SelectedValue = Session[LoginChapterID].ToString();
                    if (Session[LoginChapterID].ToString() == "1")
                    {
                        ddEvent.SelectedValue = "1";
                    }
                    else
                    {
                        ddEvent.SelectedValue = "2";
                    }
                }
                Yearscount();
                string ddChapterstr = "select isnull(max(ltrim(rtrim(Contest_Year))),Year(GetDate())) from Contest where EventID=" + ddEvent.SelectedValue + " and NSFChapterID='" + ddchapter.SelectedValue + "'";

                try
                {
                    DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
                    if (dschapter.Tables[0].Rows.Count > 0)
                    {
                        ddYear.SelectedValue = dschapter.Tables[0].Rows[0][0].ToString().Trim();
                    }
                }
                catch (Exception ex) { }
                // ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                loadContest();


                loadContestTimingDay2();
                loadContestTimingDay3();
            }
        }

    }


    protected void Events()
    {
        try
        {
            ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
            ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
            ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
            ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
            ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
            ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
            ddEvent.Items.Insert(0, new ListItem("Finals", "1"));

            ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year + 1;
            ArrayList list = new ArrayList();
            // list.Add(new ListItem("2014", "2014"));

            for (int i = MaxYear; i >= MaxYear - 4; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();

            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {
        string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        try
        {
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {

                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }



    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddchapter.SelectedValue != "0")
        {
            if (ddchapter.SelectedValue == "1")
            {
                ddEvent.SelectedValue = "1";
            }
            else
            {
                ddEvent.SelectedValue = "2";
            }
        }
    }
    public void loadContest()
    {
        string eventId = ddEvent.SelectedValue;
        string year = ddYear.SelectedValue;
        string chapterID = ddchapter.SelectedValue;
        string contestDate = string.Empty;

        string cmdtext = string.Empty;
        DataSet objDs = new DataSet();
        cmdtext = "select distinct ContestDate from Contest where EventID='" + eventId + "' and NSFChapterID='" + chapterID + "' and Contest_Year='" + year + "'";
        objDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
        if (objDs.Tables[0] != null)
        {
            if (objDs.Tables[0].Rows.Count > 0)
            {
                if (objDs.Tables[0].Rows[0]["ContestDate"] != null && objDs.Tables[0].Rows[0]["ContestDate"].ToString() != string.Empty)
                {
                    contestDate = Convert.ToDateTime(objDs.Tables[0].Rows[0]["ContestDate"].ToString()).ToString("yyyy-MM-dd");
                }
            }
        }

        DataSet ds = new DataSet();
        try
        {
            if (contestDate != string.Empty)
            {
                //cmdtext = string.Format("select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as ProductName,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ContestID,C.ProductGroupId,C.ProductId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where {0}=" + eventId + " and {1}=" + chapterID + " and {2}='" + year + "' and {3}='" + minDate + "' order by ProductId ASC", "C.EventId", "C.NSFChapterID", "C.Contest_Year", "C.ContestDate");
                cmdtext = "select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as ProductName,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ContestID,C.ProductGroupId,C.ProductId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where C.EventId=" + eventId + " and C.NSFChapterID=" + chapterID + " and C.Contest_Year='" + year + "' and C.ContestDate='" + contestDate + "' order by ProductId ASC";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        trContestDay1.Visible = true;
                        string date = Convert.ToDateTime(objDs.Tables[0].Rows[0]["ContestDate"].ToString()).ToString("MMMM dd,yyyy");
                        lblContestDay1.Text = "Day 1 :" + date + string.Empty;

                        grdContestTiming.DataSource = ds.Tables[0];
                        grdContestTiming.DataBind();
                        int iYear = Convert.ToInt32(ddYear.SelectedValue);
                        int sYear = DateTime.Now.Year;
                        if (iYear < sYear)
                        {
                            grdContestTiming.HeaderRow.Cells[9].Visible = false;
                            for (int i = 0; i < grdContestTiming.Rows.Count; i++)
                            {
                                grdContestTiming.Rows[i].Cells[9].Visible = false;
                                if (((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase1") as DropDownList) != null)
                                {
                                    ((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase1") as DropDownList).Visible = false;
                                }
                                if (((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase2") as DropDownList) != null)
                                {
                                    ((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase2") as DropDownList).Visible = false;
                                }
                                if (((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase3") as DropDownList) != null)
                                {
                                    ((DropDownList)grdContestTiming.Rows[i].FindControl("ddlPhase3") as DropDownList).Visible = false;
                                }
                            }
                        }
                        lblErrMsg.Text = string.Empty;

                    }
                    else
                    {
                        grdContestTiming.DataSource = null;
                        grdContestTiming.DataBind();

                        lblErrMsg.Text = "No record found";
                        lblErrMsg.ForeColor = Color.Red;
                    }
                }


            }

            else
            {
                grdContestTiming.DataSource = null;
                grdContestTiming.DataBind();

                lblErrMsg.Text = "No record found";
                lblErrMsg.ForeColor = Color.Red;

                trContestDay1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public void loadContestTimingDay2()
    {
        string eventId = ddEvent.SelectedValue;
        string year = ddYear.SelectedValue;
        string chapterID = ddchapter.SelectedValue;
        string contestDate = string.Empty;
        string cmdtext = string.Empty;
        DataSet objDs = new DataSet();
        cmdtext = "select  distinct ContestDate  from Contest where EventID='" + eventId + "' and NSFChapterID='" + chapterID + "' and Contest_Year='" + year + "'";

        objDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
        DataSet ds = new DataSet();

        if (objDs.Tables[0] != null)
        {
            if (objDs.Tables[0].Rows.Count > 1)
            {
                if (objDs.Tables[0].Rows[1]["ContestDate"] != null && objDs.Tables[0].Rows[1]["ContestDate"].ToString() != string.Empty)
                {
                    contestDate = Convert.ToDateTime(objDs.Tables[0].Rows[1]["ContestDate"].ToString()).ToString("MMMM dd,yyyy");
                    try
                    {
                        if (contestDate != string.Empty)
                        {
                            cmdtext = "select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as ProductName,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ContestID,C.ProductGroupId,C.ProductId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where C.EventId=" + eventId + " and C.NSFChapterID=" + chapterID + " and C.Contest_Year='" + year + "' and C.ContestDate='" + contestDate + "' order by ProductId ASC";
                            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
                            if (ds.Tables[0] != null)
                            {
                                if (ds.Tables[0].Rows.Count > 1)
                                {
                                    trContestDay2.Visible = true;
                                    // string date = Convert.ToDateTime(objDs.Tables[0].Rows[0]["MaxDate"].ToString()).ToString("MMMM dd,yyyy");
                                    lblContestDay2.Text = "Day 2 :" + contestDate + string.Empty;

                                    grdContestTimingsDay2.DataSource = ds.Tables[0];
                                    grdContestTimingsDay2.DataBind();

                                    int iYear = Convert.ToInt32(ddYear.SelectedValue);
                                    int sYear = DateTime.Now.Year;
                                    if (iYear < sYear)
                                    {
                                        grdContestTimingsDay2.HeaderRow.Cells[9].Visible = false;
                                        for (int i = 0; i < grdContestTimingsDay2.Rows.Count; i++)
                                        {
                                            grdContestTimingsDay2.Rows[i].Cells[9].Visible = false;
                                            if (((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase1Day2") as DropDownList) != null)
                                            {
                                                ((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase1Day2") as DropDownList).Visible = false;
                                            }
                                            if (((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase2Day2") as DropDownList) != null)
                                            {
                                                ((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
                                            }
                                            if (((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase3Day3") as DropDownList) != null)
                                            {
                                                ((DropDownList)grdContestTimingsDay2.Rows[i].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                                            }
                                        }
                                    }
                                    lblErrMsg.Text = string.Empty;

                                }
                                else
                                {
                                    grdContestTimingsDay2.DataSource = null;
                                    grdContestTimingsDay2.DataBind();

                                    lblErrMsg.Text = "No record found";
                                    lblErrMsg.ForeColor = Color.Red;
                                }
                            }
                        }
                        else
                        {
                            grdContestTimingsDay2.DataSource = null;
                            grdContestTimingsDay2.DataBind();

                            trContestDay2.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }
    }

    public void loadContestTimingDay3()
    {
        try
        {


            string eventId = ddEvent.SelectedValue;
            string year = ddYear.SelectedValue;
            string chapterID = ddchapter.SelectedValue;
            string contestDate = string.Empty;
            string cmdtext = string.Empty;
            DataSet objDs = new DataSet();
            cmdtext = "select distinct ContestDate  from Contest where EventID='" + eventId + "' and NSFChapterID='" + chapterID + "' and Contest_Year='" + year + "'";

            objDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            DataSet ds = new DataSet();

            if (objDs.Tables[0] != null)
            {
                if (objDs.Tables[0].Rows.Count > 1)
                {
                    if (objDs.Tables[0].Rows[2]["ContestDate"] != null && objDs.Tables[0].Rows[2]["ContestDate"].ToString() != string.Empty)
                    {
                        contestDate = Convert.ToDateTime(objDs.Tables[0].Rows[2]["ContestDate"].ToString()).ToString("MMMM dd,yyyy");
                        try
                        {
                            if (contestDate != string.Empty)
                            {
                                cmdtext = "select distinct C.ContestDate,(PG.Name+'-'+C.ProductGroupCode) as ProductGroupName,(P.Name+'-'+C.ProductCode) as ProductName,cc.GradeFrom,CC.GradeTo,C.Ph1Start,C.Ph2Start,C.Ph3Start,C.Ph1End,C.Ph2End,C.Ph3End,C.ContestID,C.ProductGroupId,C.ProductId from Contest C inner join ContestCategory CC on(C.ContestCategoryID=CC.ContestCategoryID) inner join ProductGroup PG on(PG.ProductGroupId=C.ProductGroupId) inner join Product P on(P.ProductId=C.ProductId) where C.EventId=" + eventId + " and C.NSFChapterID=" + chapterID + " and C.Contest_Year='" + year + "' and C.ContestDate='" + contestDate + "' order by ProductId ASC";
                                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
                                if (ds.Tables[0] != null)
                                {
                                    if (ds.Tables[0].Rows.Count > 1)
                                    {
                                        trContestDay3.Visible = true;
                                        lblContestDay3.Text = "Day 3 :" + contestDate + string.Empty;

                                        grdContestTimingsDay3.DataSource = ds.Tables[0];
                                        grdContestTimingsDay3.DataBind();

                                        int iYear = Convert.ToInt32(ddYear.SelectedValue);
                                        int sYear = DateTime.Now.Year;
                                        if (iYear < sYear)
                                        {
                                            grdContestTimingsDay3.HeaderRow.Cells[9].Visible = false;
                                            for (int i = 0; i < grdContestTimingsDay3.Rows.Count; i++)
                                            {
                                                grdContestTimingsDay3.Rows[i].Cells[9].Visible = false;
                                                if (((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase1Day2") as DropDownList) != null)
                                                {
                                                    ((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase1Day2") as DropDownList).Visible = false;
                                                }
                                                if (((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase2Day2") as DropDownList) != null)
                                                {
                                                    ((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
                                                }
                                                if (((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase3Day3") as DropDownList) != null)
                                                {
                                                    ((DropDownList)grdContestTimingsDay3.Rows[i].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                                                }
                                            }
                                        }
                                        lblErrMsg.Text = string.Empty;

                                    }
                                    else
                                    {
                                        grdContestTimingsDay3.DataSource = null;
                                        grdContestTimingsDay3.DataBind();

                                        lblErrMsg.Text = "No record found";
                                        lblErrMsg.ForeColor = Color.Red;
                                    }
                                }
                            }
                            else
                            {
                                grdContestTimingsDay3.DataSource = null;
                                grdContestTimingsDay3.DataBind();

                                trContestDay3.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
            }
        }
        catch
        {
        }
    }

    protected void grdContestTiming_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdContestTiming.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        string Ph1Start = "0";
        string Ph2Start = "0";
        string Ph3Start = "0";
        Ph1Start = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblPhase1") as Label).Text;
        Ph2Start = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblPhase2") as Label).Text;
        Ph3Start = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblPhase3") as Label).Text;

        Ph1Start = (Ph1Start == string.Empty ? "-1" : Ph1Start);
        Ph2Start = (Ph2Start == string.Empty ? "-1" : Ph2Start);
        Ph3Start = (Ph3Start == string.Empty ? "-1" : Ph3Start);

        loadContest();
        if (retFlag == "Edit")
        {

            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).SelectedValue = Ph1Start;
            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).SelectedValue = Ph2Start;
            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).SelectedValue = Ph3Start;

            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnEdit") as Button).Visible = false;

            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnCancel") as Button).Visible = true;
            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnUpdate") as Button).Visible = true;
            string productGroupId = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblProductGroupId") as Label).Text;
            string productId = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblProductId") as Label).Text;
            string cmdText = string.Empty;

            cmdText = "select Phase from ContestSettings where ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and ContestYear='" + ddYear.SelectedValue + "' Order by Phase ASC";

            DataSet ds = new DataSet();
            string phase = string.Empty;
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string ph = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (ph != dr["Phase"].ToString())
                        {
                            phase += dr["Phase"].ToString();
                        }
                        ph = dr["Phase"].ToString();
                    }
                }
                if (phase == "1")
                {
                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = false;
                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;
                }
                else if (phase == "12")
                {

                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;
                }
                else if (phase == "123")
                {
                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = true;
                }
                else
                {
                    if (ddEvent.SelectedValue == "1")
                    {
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = true;
                    }
                    else
                    {
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;
                    }
                }
            }
        }
        else if (retFlag == "Cancel")
        {
            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnEdit") as Button).Visible = true;

            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnCancel") as Button).Visible = false;
            ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnUpdate") as Button).Visible = false;
            grdContestTiming.EditIndex = -1;
            loadContest();
        }
        else if (retFlag == "Update")
        {
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = false;
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = false;
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;


        }
    }
    protected void grdContestTiming_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            retFlag = "Edit";
        }
        else if (e.CommandName == "Update")
        {
            retFlag = "Update";
        }
        else
        {
            retFlag = "Cancel";
        }
    }
    protected void grdContestTiming_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdContestTiming.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        grdContestTiming.EditIndex = -1;
        loadContest();
        ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnUpdate") as Button).Visible = false;
        ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnCancel") as Button).Visible = false;
        ((Button)grdContestTiming.Rows[rowIndex].FindControl("btnEdit") as Button).Visible = true;

    }
    protected void grdContestTiming_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int rowIndex = e.RowIndex;
        string phase1Start = string.Empty;
        string phase2Start = string.Empty;
        string phase3Start = string.Empty;

        string ph1 = string.Empty;
        string ph2 = string.Empty;
        string ph3 = string.Empty;
        string productGroupId = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblProductGroupId") as Label).Text;
        string productId = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblProductId") as Label).Text;
        if (((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible == true)
        {
            phase1Start = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).SelectedItem.Text;
            ph1 = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).SelectedValue;
        }
        else
        {
            phase1Start = string.Empty;
            ph1 = "0";
        }
        if (((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible == true)
        {
            phase2Start = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).SelectedItem.Text;
            ph2 = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).SelectedValue;
        }
        else
        {
            phase2Start = string.Empty;
            ph2 = "0";
        }
        if (((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible == true)
        {
            phase3Start = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).SelectedItem.Text;
            ph3 = ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).SelectedValue;
        }
        else
        {
            phase3Start = string.Empty;
            ph3 = "0";
        }
        string contestId = ((Label)grdContestTiming.Rows[rowIndex].FindControl("lblContestID") as Label).Text;
        string cmdtext = string.Empty;
        int phase1Duration = 0;
        int phase2Duration = 0;
        int phase3Duration = 0;

        if (phase1Start != string.Empty && phase1Start != "Select" && phase2Start != string.Empty && phase2Start != "Select" && phase3Start != string.Empty && phase3Start != "Select")
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            if (phase3Start == "Select")
            {
                phase3Start = string.Empty;
            }
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "", "EventID", "ContestYear", "ProductGroupID", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;
            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "3")
                                {
                                    phase3Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph3Start='" + phase3Start + "',Ph1End=" + ResultPhase1End + ",ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;

        }
        else if (phase1Start != string.Empty && phase2Start != string.Empty)
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            string cmdDurText = string.Empty;
            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "", "EventID", "ContestYear", "ProductGroupID", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }

                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");

            if (phase2Start == "")
            {
                ResultPhase2End = "null";
            }

            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph1End=" + ResultPhase1End + ",ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;

        }
        else if (phase1Start != string.Empty && phase2Start == string.Empty && phase3Start == string.Empty)
        {
            string cmdDurText = string.Empty;

            //  cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "", "EventID", "ContestYear", "ProductGroupID", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }

                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph1End=" + ResultPhase1End + ",ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;
        }

        if (validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, string.Empty) == "1")
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            loadContest();

            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = false;
            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = false;
            ((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;
            grdContestTiming.EditIndex = -1;
            grdContestTimingsDay2.EditIndex = -1;
            loadContest();
            loadContestTimingDay2();
            loadContestTimingDay3();
            lblErrMsg.Text = "Contest timing updated successfully";
            lblErrMsg.ForeColor = Color.Blue;
        }
        else
        {
            validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, string.Empty);
        }
        //if (phase1Start != string.Empty && phase2Start != string.Empty && phase3Start != string.Empty)
        //{
        //ph1 = ph1.Replace(":", string.Empty);
        //ph2 = ph2.Replace(":", string.Empty);
        //ph3 = ph3.Replace(":", string.Empty);
        //int ph1Result = Convert.ToInt32(ph1);
        //int ph2Result = Convert.ToInt32(ph2);
        //int ph3Result = Convert.ToInt32(ph3);
        //if ((ph2Result > ph1Result) && ph3Result > ph2Result)
        //{
        //SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
        //loadContest();

        //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = false;
        //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = false;
        //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;
        //grdContestTiming.EditIndex = -1;
        //loadContest();
        //lblErrMsg.Text = "Contest timing updated successfully";
        //lblErrMsg.ForeColor = Color.Blue;
        //}
        //else
        //{
        //    if (ph2Result <= ph1Result)
        //    {
        //        lblErrMsg.Text = "Phase 2 Start time should be greater than Phase 1 Start time.";
        //        lblErrMsg.ForeColor = Color.Red;
        //    }
        //    else if (ph3Result <= ph2Result)
        //    {
        //        lblErrMsg.Text = "Phase 3 Start time should be greater than Phase 2 Start time.";
        //        lblErrMsg.ForeColor = Color.Red;
        //    }
        //}
        //}
        //else
        //{
        //    if (phase1Start == "Select")
        //    {
        //        lblErrMsg.Text = "Please fill Phase 1 Start time";
        //        lblErrMsg.ForeColor = Color.Red;
        //    }
        //    else if (phase2Start == "Select")
        //    {
        //        lblErrMsg.Text = "Please fill Phase 2 Start time";
        //        lblErrMsg.ForeColor = Color.Red;
        //    }
        //    else if (phase3Start == "Select")
        //    {
        //        lblErrMsg.Text = "Please fill Phase 3 Start time";
        //        lblErrMsg.ForeColor = Color.Red;
        //    }
        //}
    }
    public void reloadContests()
    {
        loadContest();
    }
    public string validateSearch()
    {
        string retVal = "1";
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
        if (ddYear.SelectedValue == "0")
        {
            //lblErrMsg.Text = "Please select year";
            //lblErrMsg.ForeColor = Color.Red;
            setMessage("Year");
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "0")
        {
            //lblErrMsg.Text = "Please select event";
            //lblErrMsg.ForeColor = Color.Red;
            setMessage("Event");
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "0")
        {
            //lblErrMsg.Text = "Please select chapter";
            //lblErrMsg.ForeColor = Color.Red;
            setMessage("Chapter");
            retVal = "-1";
        }

        return retVal;

    }

    public void setMessage(string message)
    {
        lblErrMsg.Text = "Please select " + message + string.Empty;
        lblErrMsg.ForeColor = Color.Red;
    }

    protected void grdContestTimingsDay2_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdContestTimingsDay2.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        string Ph1Start = "0";
        string Ph2Start = "0";
        string Ph3Start = "0";
        Ph1Start = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblPhase1Day2") as Label).Text;
        Ph2Start = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblPhase2Day2") as Label).Text;
        Ph3Start = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblPhase3Day3") as Label).Text;

        Ph1Start = (Ph1Start == string.Empty ? "-1" : Ph1Start);
        Ph2Start = (Ph2Start == string.Empty ? "-1" : Ph2Start);
        Ph3Start = (Ph3Start == string.Empty ? "-1" : Ph3Start);


        loadContestTimingDay2();
        if (retFlag == "Edit")
        {
            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedValue = Ph1Start;
            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedValue = Ph2Start;
            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedValue = Ph3Start;

            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = false;

            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = true;
            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = true;

            string productGroupId = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblProductGroupIdDay2") as Label).Text;
            string productId = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblProductIdDay2") as Label).Text;
            string cmdText = string.Empty;

            //cmdText = string.Format("select Phase from ContestSettings where {0}=" + productGroupId + " and {1}=" + productId + " and {2}='" + ddYear.SelectedValue + "' Order by Phase ASC", "ProductGroupID", "ProductID", "ContestYear");
            cmdText = "select Phase from ContestSettings where ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and ContestYear='" + ddYear.SelectedValue + "' Order by Phase ASC";

            DataSet ds = new DataSet();
            string phase = string.Empty;
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string ph = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (ph != dr["Phase"].ToString())
                        {
                            phase += dr["Phase"].ToString();
                        }
                        ph = dr["Phase"].ToString();
                    }
                }
                if (phase == "1")
                {
                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                }
                else if (phase == "12")
                {

                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                }
                else if (phase == "123")
                {
                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = true;
                }
                else
                {
                    if (ddEvent.SelectedValue == "1")
                    {
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = true;
                    }
                    else
                    {
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                    }
                }
            }
        }
        else if (retFlag == "Cancel")
        {
            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = true;

            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = false;
            ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = false;
            grdContestTimingsDay2.EditIndex = -1;
            loadContestTimingDay2();
        }
        else if (retFlag == "Update")
        {
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase1") as DropDownList).Visible = false;
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase2") as DropDownList).Visible = false;
            //((DropDownList)grdContestTiming.Rows[rowIndex].FindControl("ddlPhase3") as DropDownList).Visible = false;


        }
    }
    protected void grdContestTimingsDay2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            retFlag = "Edit";
        }
        else if (e.CommandName == "Update")
        {
            retFlag = "Update";
        }
        else
        {
            retFlag = "Cancel";
        }
    }
    protected void grdContestTimingsDay2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdContestTimingsDay2.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        grdContestTimingsDay2.EditIndex = -1;
        loadContestTimingDay2();

        ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = false;
        ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = false;
        ((Button)grdContestTimingsDay2.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = true;

    }
    protected void grdContestTimingsDay2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int rowIndex = e.RowIndex;
        string phase1Start = string.Empty;
        string phase2Start = string.Empty;
        string phase3Start = string.Empty;

        string ph1 = string.Empty;
        string ph2 = string.Empty;
        string ph3 = string.Empty;
        string productGroupId = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblProductGroupIdDay2") as Label).Text;
        string productId = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblProductIdDay2") as Label).Text;
        string ProductGroup = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblProductGroup2") as Label).Text;

        if (((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible == true)
        {
            phase1Start = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedItem.Text;
            ph1 = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedValue;
        }
        else
        {
            phase1Start = string.Empty;
            ph1 = "0";
        }
        if (((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible == true)
        {
            phase2Start = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedItem.Text;
            ph2 = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedValue;
        }
        else
        {
            phase2Start = string.Empty;
            ph2 = "0";
        }
        if (((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible == true)
        {
            phase3Start = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedItem.Text;
            ph3 = ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedValue;
        }
        else
        {
            phase3Start = string.Empty;
            ph3 = "0";
        }
        string contestId = ((Label)grdContestTimingsDay2.Rows[rowIndex].FindControl("lblContestID2") as Label).Text;
        string cmdtext = string.Empty;
        int phase1Duration = 0;
        int phase2Duration = 0;
        int phase3Duration = 0;

        if (phase1Start != string.Empty && phase2Start != string.Empty && phase3Start != string.Empty)
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            if (phase3Start == "Select")
            {
                phase3Start = string.Empty;
            }
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "", "EventID", "ContestYear", "ProductGroupID", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;
            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "3")
                                {
                                    phase3Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = string.Format("Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph3Start='" + phase3Start + "',Ph1End=" + ResultPhase1End + ",ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId + "", "ContestID");

        }
        else if (phase1Start != string.Empty && phase2Start != string.Empty)
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" +productGroupId + " and {3}=" + productId +"" , "EventID", "ContestYear", "ProductGroupID", "", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }

                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph1End=" + ResultPhase1End + ",Ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;


        }
        else if (phase1Start != string.Empty && phase2Start == string.Empty && phase3Start == string.Empty)
        {
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "EventID", "ContestYear", "ProductGroupID", "", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }

                            }
                        }
                    }
                }
            }

            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");



            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph1End=" + ResultPhase1End + ",Ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;

        }

        if (validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, ProductGroup) == "1")
        {

            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            loadContest();

            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = false;
            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
            ((DropDownList)grdContestTimingsDay2.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
            grdContestTimingsDay2.EditIndex = -1;
            grdContestTiming.EditIndex = -1;
            loadContest();
            loadContestTimingDay2();
            loadContestTimingDay3();
            lblErrMsg.Text = "Contest timing updated successfully";
            lblErrMsg.ForeColor = Color.Blue;
        }
        else
        {
            validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, ProductGroup);
        }
    }

    #region " Day 3"
    protected void grdContestTimingsDay3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            retFlag = "Edit";
        }
        else if (e.CommandName == "Update")
        {
            retFlag = "Update";
        }
        else
        {
            retFlag = "Cancel";
        }
    }
    protected void grdContestTimingsDay3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdContestTimingsDay3.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        grdContestTimingsDay3.EditIndex = -1;
        loadContestTimingDay3();

        ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = false;
        ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = false;
        ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = true;

    }
    protected void grdContestTimingsDay3_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdContestTimingsDay3.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        string Ph1Start = "0";
        string Ph2Start = "0";
        string Ph3Start = "0";
        Ph1Start = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblPhase1Day2") as Label).Text;
        Ph2Start = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblPhase2Day2") as Label).Text;
        Ph3Start = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblPhase3Day3") as Label).Text;

        Ph1Start = (Ph1Start == string.Empty ? "-1" : Ph1Start);
        Ph2Start = (Ph2Start == string.Empty ? "-1" : Ph2Start);
        Ph3Start = (Ph3Start == string.Empty ? "-1" : Ph3Start);


        loadContestTimingDay3();
        if (retFlag == "Edit")
        {
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedValue = Ph1Start;
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedValue = Ph2Start;
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedValue = Ph3Start;

            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = false;

            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = true;
            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = true;

            string productGroupId = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblProductGroupIdDay2") as Label).Text;
            string productId = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblProductIdDay2") as Label).Text;
            string cmdText = string.Empty;

            //cmdText = string.Format("select Phase from ContestSettings where {0}=" + productGroupId + " and {1}=" + productId + " and {2}='" + ddYear.SelectedValue + "' Order by Phase ASC", "ProductGroupID", "ProductID", "ContestYear");
            cmdText = "select Phase from ContestSettings where ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and ContestYear='" + ddYear.SelectedValue + "' Order by Phase ASC";

            DataSet ds = new DataSet();
            string phase = string.Empty;
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string ph = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (ph != dr["Phase"].ToString())
                        {
                            phase += dr["Phase"].ToString();
                        }
                        ph = dr["Phase"].ToString();
                    }
                }
                if (phase == "1")
                {
                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                }
                else if (phase == "12")
                {

                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                }
                else if (phase == "123")
                {
                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                    ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = true;
                }
                else
                {
                    if (ddEvent.SelectedValue == "1")
                    {
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = true;
                    }
                    else
                    {
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = true;
                        ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
                    }
                }
            }
        }
        else if (retFlag == "Cancel")
        {
            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnEditDay2") as Button).Visible = true;

            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnCancelDay2") as Button).Visible = false;
            ((Button)grdContestTimingsDay3.Rows[rowIndex].FindControl("btnUpdateDay2") as Button).Visible = false;
            grdContestTimingsDay3.EditIndex = -1;
            loadContestTimingDay3();
        }
        else if (retFlag == "Update")
        {

        }
    }
    protected void grdContestTimingsDay3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int rowIndex = e.RowIndex;
        string phase1Start = string.Empty;
        string phase2Start = string.Empty;
        string phase3Start = string.Empty;

        string ph1 = string.Empty;
        string ph2 = string.Empty;
        string ph3 = string.Empty;
        string productGroupId = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblProductGroupIdDay2") as Label).Text;
        string productId = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblProductIdDay2") as Label).Text;
        string ProductGroup = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblProductGroup2") as Label).Text;

        if (((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible == true)
        {
            phase1Start = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedItem.Text;
            ph1 = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).SelectedValue;
        }
        else
        {
            phase1Start = string.Empty;
            ph1 = "0";
        }
        if (((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible == true)
        {
            phase2Start = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedItem.Text;
            ph2 = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).SelectedValue;
        }
        else
        {
            phase2Start = string.Empty;
            ph2 = "0";
        }
        if (((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible == true)
        {
            phase3Start = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedItem.Text;
            ph3 = ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).SelectedValue;
        }
        else
        {
            phase3Start = string.Empty;
            ph3 = "0";
        }
        string contestId = ((Label)grdContestTimingsDay3.Rows[rowIndex].FindControl("lblContestID2") as Label).Text;
        string cmdtext = string.Empty;
        int phase1Duration = 0;
        int phase2Duration = 0;
        int phase3Duration = 0;

        if (phase1Start != string.Empty && phase2Start != string.Empty && phase3Start != string.Empty)
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            if (phase3Start == "Select")
            {
                phase3Start = string.Empty;
            }
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "", "EventID", "ContestYear", "ProductGroupID", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;
            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "3")
                                {
                                    phase3Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = string.Format("Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph3Start='" + phase3Start + "',Ph1End=" + ResultPhase1End + ",ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId + "", "ContestID");

        }
        else if (phase1Start != string.Empty && phase2Start != string.Empty)
        {
            if (phase2Start == "Select")
            {
                phase2Start = string.Empty;
            }
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" +productGroupId + " and {3}=" + productId +"" , "EventID", "ContestYear", "ProductGroupID", "", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                                else if (dr["Phase"].ToString() == "2")
                                {
                                    phase2Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }

                            }
                        }
                    }
                }
            }
            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");


            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph2Start='" + phase2Start + "',Ph1End=" + ResultPhase1End + ",Ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;

        }
        else if (phase1Start != string.Empty && phase2Start == string.Empty && phase3Start == string.Empty)
        {
            string cmdDurText = string.Empty;

            //cmdDurText = string.Format("select Phase,Duration from ContestSettings where {0}=" + ddEvent.SelectedValue + " and {1}='" + ddYear.SelectedValue + "' and {2}=" + productGroupId + " and {3}=" + productId + "EventID", "ContestYear", "ProductGroupID", "", "ProductID");
            cmdDurText = "select Phase,Duration from ContestSettings where EventID=" + ddEvent.SelectedValue + " and ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId;

            DataSet dsDur = new DataSet();
            dsDur = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDurText);
            if (dsDur.Tables[0] != null)
            {
                if (dsDur.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsDur.Tables[0].Rows)
                    {
                        if (dr["Phase"] != null && dr["Phase"].ToString() != string.Empty)
                        {
                            if (dr["Duration"] != null && dr["Duration"].ToString() != string.Empty)
                            {
                                if (dr["Phase"].ToString() == "1")
                                {
                                    phase1Duration = Convert.ToInt32(dr["Duration"].ToString());
                                }
                            }
                        }
                    }
                }
            }

            TimeSpan ph1End = GetTimeFromString1(phase1Start, phase1Duration);
            TimeSpan ph2End = GetTimeFromString1(phase2Start, phase2Duration);
            TimeSpan ph3End = GetTimeFromString1(phase3Start, phase3Duration);

            string phase1End = ph1End.ToString();
            string phase2End = ph2End.ToString();
            string phase3End = ph3End.ToString();

            string ResultPhase1End = phase1End.Remove(phase1End.Length - 3);
            string ResultPhase2End = phase2End.Remove(phase2End.Length - 3);
            string ResultPhase3End = phase3End.Remove(phase3End.Length - 3);

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            ResultPhase2End = (phase2Duration > 0 ? "'" + ResultPhase2End + "'" : "null");
            ResultPhase3End = (phase3Duration > 0 ? "'" + ResultPhase3End + "'" : "null");

            ResultPhase1End = (phase1Duration > 0 ? "'" + ResultPhase1End + "'" : "null");
            cmdtext = "Update Contest set Ph1Start='" + phase1Start + "',Ph1End=" + ResultPhase1End + ",Ph2End=" + ResultPhase2End + ",Ph3End=" + ResultPhase3End + " where ContestID=" + contestId;
        }

        if (validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, ProductGroup) == "1")
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            loadContest();
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase1Day2") as DropDownList).Visible = false;
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase2Day2") as DropDownList).Visible = false;
            ((DropDownList)grdContestTimingsDay3.Rows[rowIndex].FindControl("ddlPhase3Day3") as DropDownList).Visible = false;
            grdContestTimingsDay3.EditIndex = -1;
            grdContestTimingsDay2.EditIndex = -1;
            grdContestTiming.EditIndex = -1;
            loadContest();
            loadContestTimingDay2();
            loadContestTimingDay3();
            lblErrMsg.Text = "Contest timing updated successfully";
            lblErrMsg.ForeColor = Color.Blue;
        }
        else
        {
            validateTimings(phase1Start, phase2Start, phase3Start, productGroupId, productId, ProductGroup);
        }
    }
    #endregion

    public string validateTimings(string phase1, string phase2, string phase3, string productGroupId, string productId, string ProductGroup)
    {
        string retVal = "1";

        string phase1Duration = string.Empty;
        string phase2Duration = string.Empty;
        DataSet ds = new DataSet();
        string cmdText = string.Empty;
        if (phase1 != string.Empty && phase2 == string.Empty && phase3 == string.Empty)
        {
            if (phase1 == "Select")
            {
                retVal = "-1";
                validationMessage("Please fill Phase 1 Start time");
                lblErrMsg.ForeColor = Color.Red;
            }
        }
        else if (phase1 != string.Empty && phase2 != string.Empty && phase3 == string.Empty)
        {
            if (phase1 == "Select")
            {
                retVal = "-1";
                validationMessage("Please fill Phase 1 Start time");

            }
            else
            {
                //cmdText = string.Format("select Duration from ContestSettings where {0}='" + ddYear.SelectedValue + "' and {1}=" + productGroupId + " and {2}=" + productId + " and {3}=1","ContestYear", "ProductGroupID", "ProductID", "Phase");
                cmdText = "select Duration from ContestSettings where ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and Phase=1";

                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["Duration"] != null && ds.Tables[0].Rows[0]["Duration"].ToString() != string.Empty)
                        {
                            phase1Duration = ds.Tables[0].Rows[0]["Duration"].ToString();
                        }
                        else
                        {
                            phase1Duration = "0";
                        }
                    }
                    else
                    {
                        phase1Duration = "0";
                    }
                }
                string phase1Time = phase1.Replace(":", string.Empty);
                int ph1Time = Convert.ToInt32(phase1Time);
                int durationTime = Convert.ToInt32(phase1Duration);
                if (durationTime == 60)
                {
                    durationTime = 100;
                }
                string phase2Time = string.Empty;
                if (phase2 != "Select")
                {
                    phase2Time = phase2.Replace(":", string.Empty);
                }
                else
                {
                    phase2Time = "0";
                }

                int ph2Time = Convert.ToInt32(phase2Time);
                int resultTime = ph1Time + durationTime;
                if (phase2 != "Select")
                {
                    if (ProductGroup == "Math-MB" || ProductGroup == "Science-SC")
                    {
                        if (ph2Time < ph1Time)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time");

                        }
                    }
                    else
                    {
                        if (ph2Time <= resultTime)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time + Duration of Phase 1");

                        }
                    }
                }
            }
        }
        else if (phase1 != string.Empty && phase2 != string.Empty && phase3 != string.Empty)
        {
            if (phase1 == "Select")
            {
                retVal = "-1";
                validationMessage("Please fill Phase 1 Start time");

            }
            else
            {
                cmdText = "select Duration from ContestSettings where ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and Phase=1";
                //cmdText = string.Format("select Duration from ContestSettings where {0}='" + ddYear.SelectedValue + "' and {1}=" + productGroupId + " and {2}=" + productId + " and {3}=1", "ContestYear", "ProductGroupID", "ProductID", "Phase");

                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["Duration"] != null && ds.Tables[0].Rows[0]["Duration"].ToString() != string.Empty)
                        {
                            phase1Duration = ds.Tables[0].Rows[0]["Duration"].ToString();
                        }
                        else
                        {
                            phase1Duration = "0";
                        }
                    }
                    else
                    {
                        phase1Duration = "0";
                    }
                }


                //  cmdText = string.Format("select Duration from ContestSettings where {0}='" + ddYear.SelectedValue + "' and {1}=" + productGroupId + " and {2}=" + productId + " and {3}=2","ContestYear", "ProductGroupID", "ProductID", "Phase");
                cmdText = "select Duration from ContestSettings where ContestYear='" + ddYear.SelectedValue + "' and ProductGroupID=" + productGroupId + " and ProductID=" + productId + " and Phase=2";
                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["Duration"] != null && ds.Tables[0].Rows[0]["Duration"].ToString() != string.Empty)
                        {
                            phase2Duration = ds.Tables[0].Rows[0]["Duration"].ToString();
                        }
                        else
                        {
                            phase2Duration = "0";
                        }
                    }
                    else
                    {
                        phase2Duration = "0";
                    }
                }

                string phase1Time = phase1.Replace(":", string.Empty);
                int ph1Time = Convert.ToInt32(phase1Time);
                int durationTime = Convert.ToInt32(phase1Duration);
                if (durationTime == 60)
                {
                    durationTime = 100;
                }
                string phase2Time = string.Empty;
                string phase3Time = string.Empty;
                if (phase2 != "Select")
                {
                    phase2Time = phase2.Replace(":", string.Empty);
                }
                else
                {
                    phase2Time = "0";
                }

                int ph2Time = Convert.ToInt32(phase2Time);
                int resultTime = ph1Time + durationTime;
                if (phase3 != "Select")
                {
                    phase3Time = phase3.Replace(":", string.Empty);
                }
                else
                {
                    phase3Time = "0";
                }

                int ph3Time = Convert.ToInt32(phase3Time);
                int durationTime2 = Convert.ToInt32(phase2Duration);
                if (durationTime2 == 60)
                {
                    durationTime2 = 100;
                }
                int resultTime2 = ph2Time + durationTime2;
                if (phase2 != "Select")
                {
                    if (ProductGroup == "Math-MB" || ProductGroup == "Science-SC")
                    {
                        if (ph2Time < ph1Time)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time");

                        }
                    }
                    else
                    {
                        if (ph2Time <= resultTime)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time + Duration of Phase 1");

                        }
                    }
                }
                if (phase3 != "Select")
                {
                    if (ph3Time <= resultTime2)
                    {
                        retVal = "-1";
                        validationMessage("Phase 3 Start time sholud be greater than Phase 2 Start time + Duration of Phase 2");

                    }
                }
                if (phase2 != "Select" && phase3 != "Select")
                {
                    if (ProductGroup == "Math-MB" || ProductGroup == "Science-SC")
                    {
                        if (ph2Time < ph1Time)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time");

                        }
                        else if (ph3Time <= resultTime2)
                        {
                            retVal = "-1";
                            validationMessage("Phase 3 Start time sholud be greater than Phase 2 Start time + Duration of Phase 2");

                        }
                    }
                    else
                    {
                        if (ph2Time <= resultTime)
                        {
                            retVal = "-1";
                            validationMessage("Phase 2 Start time sholud be greater than Phase 1 Start time + Duration of Phase 1");

                        }
                        else if (ph3Time <= resultTime2)
                        {
                            retVal = "-1";
                            validationMessage("Phase 3 Start time sholud be greater than Phase 2 Start time + Duration of Phase 2");

                        }
                    }



                }
            }
        }
        return retVal;

    }

    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadContest();
        grdContestTiming.EditIndex = -1;
        loadContest();

        loadContestTimingDay2();
        grdContestTimingsDay2.EditIndex = -1;
        loadContestTimingDay2();

        loadContestTimingDay3();
        grdContestTimingsDay3.EditIndex = -1;
        loadContestTimingDay3();
    }

    private TimeSpan GetTimeFromString1(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }

    public void validationMessage(string Message)
    {
        lblErrMsg.Text = Message;
        lblErrMsg.ForeColor = Color.Red;
    }
}