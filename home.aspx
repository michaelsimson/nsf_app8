<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/home.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Welcome to North South Foundation</title>
    <!--#include file="incfiles.aspx"-->
</head>

<body onload="MM_preloadImages('/public/images/more.jpg','/public/images/donate_tab.png')">
    <!--#include file="header.aspx"-->

    <tr>
        <td align="center">
            <table width="912px" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="1px"></td>
                </tr>
                <tr>
                    <td>
                        <iframe hspace="0" vspace="0" marginheight="0" marginwidth="0" src="/public/main/slideshow.aspx" width="912" height="141" align="middle" frameborder="0" scrolling="No"></iframe>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="11%" align="center" background="/public/images/img_12.jpg" style="background-repeat: no-repeat" bgcolor="#84b702">
                                    <!--<img src="/public/images/img_12.jpg" alt="" width="104" height="31" />-->
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <!-- <td width="2%" background="/public/images/img_13.jpg">&nbsp;</td> -->
                                <td width="89%" align="left" background="/public/images/img_13.jpg">
                                    <marquee scrolldelay="100" scrollamount="4" height="20" width="50%">
								<span class="con">
                                    <!-- New redesigned web site launched for North South Foundation on 10th January, 2011.&nbsp;&nbsp;&nbsp; -->
									<!-- &nbsp;&nbsp;<a target="_blank" href="https://attendee.gotowebinar.com/recording/8512742651239186956" class="con">Click Here for a Recording of Parent Orientation Webinar.</a> -->
									&nbsp;&nbsp;<a href="/public/news/newsandevents.aspx#2017NationalFinals">Registrations are Open for 2017 National Finals. </a> 
								</span>
                                </marquee>
                                </td>
                                <td width="0%" align="right">
                                    <img src="/public/images/img_14.jpg" alt="" width="8" height="31" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <!-- InstanceBeginEditable name="EditRegion1" -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="26%" valign="top">
                        <table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="5"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="/public/images/donate_banner.gif" width="222" border="0" usemap="#Map2Map" />
                                    <map name="Map2Map" id="Map2Map">
                                        <area shape="rect" coords="116,52,204,76" href="https://www.northsouth.org/app9/Don_athonDonorDetails.aspx?id=1" alt="Donate Now" />
                                    </map>
                                </td>
                            </tr>
                            <tr>
                                <td height="15"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="zoomin image">


                                        <a href="/public/USContests/Finals/NSF2017.aspx">
                                            <img alt="nsf25" src="/public/images/bgNSF.png" border="0" /></a>


                                    </div>
                                    <center>
                                        <img src="/public/images/donate_finals.gif" width="222" border="0" usemap="#Map3Map" /></center>
                                    <map name="Map3Map" id="Map1">
                                        <area shape="rect" coords="50,40,175,70" href="https://www.northsouth.org/app9/Donation_Finals.aspx" alt="Donate Now" />
                                    </map>
                                </td>
                            </tr>
                            <tr>
                                <td height="15"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <img src="/public/images/img_38_1.jpg" alt="Video" width="217" height="43" /></td>
                                        </tr>
                                        <tr>
                                            <td background="/public/images/img_44.jpg">
                                                <table width="96%" border="0" align="center" cellpadding="1" cellspacing="0">

                                                    <tr>
                                                        <td align="center">
                                                            <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                        <td class="btn_03" align="left"><a target="_blank" href="https://www.youtube.com/watch?v=wrLK58kKXoU&feature=youtu.be">North South Foundation</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                        <td class="btn_03" align="left"><a target="_blank" href="http://www.youtube.com/watch?v=uFQl5ybNVck">North South Foundation, India Part I</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                        <td class="btn_03" align="left"><a target="_blank" href="http://www.youtube.com/watch?v=Uv77Yjy17xQ&feature=channel">North South Foundation, India Part II</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="/public/images/img_47.jpg" width="217" height="13" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="15"></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>

                        </table>
                    </td>
                    <td width="74%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top">
                                    <table width="690" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="66%" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="68%" valign="top">
                                                                        <%-- <table width="85%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td><a target="_blank" href="https://www.northsouth.org/App9/FreeEvent/FreeEventReg.aspx" style="text-decoration: none; font-size: 20px; color: blue;">McDonald's Chicago Education Expo 2017</a></td>
                                                                            </tr>
                                                                        </table>--%>
                                                                        <div style="clear: both; margin-bottom: 8px;"></div>
                                                                        <table width="85%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <img src="/public/images/img_23.jpg" alt="Welcome" width="451" height="43" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td background="/public/images/img_30.jpg">
                                                                                    <table width="94%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="txt">
                                                                                                <div align="justify">
                                                                                                    North South Foundation (NSF) is a volunteer driven non-profit organization established in 1989.<br />


                                                                                                    <%--<a class="btn_02" href="https://www.northsouth.org/app9/Don_athonDonorDetails.aspx?id=5"> Donate for 2017 Finals in Houston</a>--%>

                                                                                                    <p>
                                                                                                        <span class="txt01_strong">Our Mission						
                                                                                                    </p>
                                                                                                    <ul>
                                                                                                        <li>Develop human resources by giving scholarships to brilliant but needy students in India entering colleges, regardless of religion, gender, caste or creed</li>
                                                                                                        <li>Promote excellence in human endeavor by organizing Educational Contests for the kids in USA</li>
                                                                                                        <li>Help people achieve success regardless of religion, gender, caste, geographic origin by giving hope to those who may have none</li>
                                                                                                    </ul>
                                                                                                    <p>
                                                                                                        <span class="txt01_strong">The NSF Scholarship program in India</span>
                                                                                                        is designed to encourage excellence among the poor children who excel academically but
																need help to attend college. Each scholarship is $250 per student per year.
																More Information is available at <a class="btn_02" href="/public/india/scholarships.aspx">India Scholarships</a>.
                                                                                                    </p>
                                                                                                    <p>
                                                                                                        <span class="txt01_strong">Educational Contests</span>
                                                                                                        organized by NSF in the US are designed to encourage academic excellence among children, K-12.
																We conduct spelling, vocabulary, math, essay writing, public speaking, brain bee and geography bees.
																Some of the NSF children have achieved top ranks in the Scripps National Spelling Bee, MATHCOUNTS
																and National Geographic Bee.
																More Information can be found at <a class="btn_02" href="/public/USContests/uscontests.aspx">US Contests</a>.
                                                                                                    </p>
                                                                                                    <p>
                                                                                                        <span class="txt01_strong">Role Model Award:</span>
                                                                                                        The &#8216;Vishwa Jyothi&#8217; Role Model is awarded to showcase human values and academic excellence to
																the children of Indian American community.
                                                                                                    </p>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td width="78%">
                                                                                                <img src="/public/images/img_31.jpg" width="351" height="26" /></td>
                                                                                            <td width="22%"><a href="/public/aboutus/aboutus.aspx" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image51','','/public/images/more.jpg',1)">
                                                                                                <img src="/public/images/img_32.jpg" alt="Read More" name="Image51" width="100" height="26" border="0" id="Image51" /></a></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="16"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="34%" valign="top">
                                                            <table width="86%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center"><a href="/public/uscontests/mentorship.aspx">
                                                                        <img src="/public/images/mentor.jpg"
                                                                            border="0" /></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="15"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="/public/images/img_25.jpg" alt="News &amp; Events" width="217" height="29" /></td>
                                                                </tr>
                                                                <td valign="top" background="/public/images/img_28.jpg">
                                                                    <table width="96%" border="0" align="center" cellpadding="1" cellspacing="0">
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_02">
                                                                                <a href="/public/news/newsandevents.aspx#2017NationalFinals">2017 National Finals - Registrations are Open.</a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_03">
                                                                                <a href="/public/news/newsandevents.aspx#2017RegionalContests">2017 Regional Contests Completed Successfully. </a>
                                                                            </td>
                                                                        </tr>
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/newsandevents.aspx#MathusAcademy2017">Free Common Core based Math Practice Assessments by Mathus Academy - March 12th - 18th, 2017. </a>
                                                        </td>
														</tr>
														-->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/newsandevents.aspx#Sankargunturu">A Tribute to an outstanding NSF volunteer - Mr. Sankar Gunturu. </a>
                                                        </td>
														</tr>
														-->
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_03">
                                                                                <a href="/public/news/newsandevents.aspx#2016CoachingSessions">2016 Online Coaching Sessions. </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_03">
                                                                                <a href="/public/news/newsandevents.aspx#2016NationalFinals">2016 National Finals held successfully on Aug 20 and 21 at Univ. of South Florida, Tampa, FL. </a>
                                                                            </td>
                                                                        </tr>
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/newsandevents.aspx#2016RegionalContests">2016 Regional Contests Completed Successfully. </a>
                                                        </td>
														</tr>
                                                        -->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_02">
														<a target="_blank" class="btn_02" href="/public/news/docs/MathusAcademy2016.pdf">Free Common Core based Math Practice Assessments by Mathus Academy - March 14th - 20th, 2016. </a>
                                                        </td>
														</tr>
                                                        -->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a target="_blank" href="https://attendee.gotowebinar.com/recording/8512742651239186956">Recording of Parent Orientation Webinar held on Feb 13, 2016 now available. </a>
                                                        </td>
														</tr>
                                                        -->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/newsandevents.aspx#2015CoachingSessions">Registrations are now closed for 2015 Online Coaching Sessions. </a>
                                                        </td>
														</tr>
														-->
                                                                        <!--
                                                        <tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" />
														</td>
														<td width="91%" class="btn_03">
															<a href="/public/news/newsandevents.aspx#2015NationalFinals ">2015 National Finals held successfully in Columbus, OH on August 29 and 30. </a>
														</td>
														</tr>
                                                        -->
                                                                        <!--
														<tr>
                                                        <td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" />
                                                        </td>
                                                        <td width="91%" class="btn_03">
                                                                <a href="/public/news/newsandevents.aspx#June2014PressRelease">Children from North South Foundation Win Four Academic Super Bowls In 2014!</a>
                                                        </td>
                                                        </tr>
                                                        -->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/newsandevents.aspx#2014CoachingSessions">Registrations are closed for Mathcounts, SAT, Geography and Universal Values coaching. Classes will begin mid-October.</font></link></td>
														</tr>
														-->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/news/docs/NSF_2012NationalFinals_PressRelease.pdf">2012 National Finals - Press Release.</a></td>
														</tr>
														-->
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_03">
														<a href="/public/India/2012_13_scholarshipsannouncement.aspx">NSF announces 2012-13 scholarships.</a></td>
														</tr>
														-->
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_03">
                                                                                <a href="excelathon.aspx">Excelathon is now open for children. please register.</a></td>
                                                                        </tr>
                                                                        <!--
														<tr>
														<td width="9%" style="margin-top:10px;padding-top:10px;" valign="top" align="center" ><img src="/public/images/bullets.jpg" width="7" height="5" /></td>
														<td width="91%" class="btn_02">
														<a href="/public/India/collegescholarshipsflier.aspx">Travelling to India this summer? Help spread info about NSF college scholarships</a></td>
														</tr>
														-->
                                                                        <tr>
                                                                            <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                            <td width="91%" class="btn_03">
                                                                                <a href="/public/news/newsandevents.aspx#NewChapterNotif">Notification for starting New NSF Chapters for unrepresented locations.</a></td>
                                                                        </tr>

                                                                    </table>
                                                                </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="100%" background="/public/images/img_31.jpg"></td>
                                                                    <td width="100"><a href="/public/news/newsandevents.aspx" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image52','','/public/images/more.jpg',1)">
                                                                        <img src="/public/images/img_32.jpg" alt="Read More" name="Image52" width="100" height="26" border="0" id="Img52" /></a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%--<tr>
												<td>&nbsp;</td>
											</tr>--%>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <img src="/public/images/img_38_3.jpg" alt="Video" width="217" height="43" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="/public/images/img_44.jpg">
                                                                        <table width="96%" border="0" align="center" cellpadding="1" cellspacing="0">
                                                                            <tr>
                                                                                <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                    <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                                <td width="91%" align="left" class="btn_03"><a target="_blank" href="http://www.youtube.com/watch?v=6ToiUzjhWdU">Why should children participate in NSF Contests?</a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="9%" style="margin-top: 10px; padding-top: 10px;" valign="top" align="center">
                                                                                    <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                                <td width="91%" align="left" class="btn_03"><a target="_blank" href="http://www.youtube.com/watch?v=3Ct5zXvOwFQ&context=C43fde7dADvjVQa1PpcFM53ONp0oo2hH8pGbB47NHx-dsZ3SbrXRQ=">How to create a new parent login</a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                                <td class="btn_03" align="left"><a target="_blank" href="http://www.youtube.com/watch?v=6uyRfBFI7Cs&context=C4e4dd4eADvjVQa1PpcFM53ONp0oo2hGYEaHngCm3twUgr_6hDX9o=">How to register your child for a NSF contest</a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                                <td class="btn_03" align="left"><a target="_blank" href="http://www.youtube.com/watch?v=MH2-veyNMJw&context=C4454279ADvjVQa1PpcFM53ONp0oo2hE5F3v83d2x50dbaL5mzscs=">How to download practice words</a></td>
                                                                            </tr>
                                                                            <%--<tr>
																<td>&nbsp;</td>
															</tr>--%>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <img src="/public/images/bullets.jpg" width="7" height="5" /></td>
                                                                                <td class="btn_03" align="left"><a target="_blank" href="http://www.youtube.com/watch?v=RL2IapPvIEk&context=C403ba93ADvjVQa1PpcFM53ONp0oo2hLGwnxvAsIzgOqbY58vafWc=">Volunteer Login Information</a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="/public/images/img_47.jpg" width="217" height="13" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--#include file="footer.aspx"-->

</body>
