﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VolunteerSignUpnew.aspx.cs" Inherits="VolunteerSignUpnew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <input type="text" id="txt" runat="server" style="display: none;" />

    <script language="javascript" type="text/javascript">
        function confirmtoAddTeams() {
            try {

                if (confirm("Do you want to Add Teams and then continue?")) {
                    document.getElementById('<%=hfIsRedirect.ClientID%>').value = 'True';
                    document.getElementById('<%= btnAdd.ClientID %>').click();

                }
                else {
                    document.getElementById('<%= hiddenbtn.ClientID %>').click();
                }
            } catch (ex) { }
        }

        function PopupPicker() {
            try {
                var myVar = document.getElementById("<%= ddEvent.ClientID %>").value;
                settings = 'width=970,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no, resizable=no,dependent=no';
                PopupWindow = window.open('VolunteerSignupGuide.aspx?value=' + myVar, 'VolunteerSignupGuide Help', settings);
                document.getElementById("_ctl0_Content_main_txt").value = PopupWindow;
                PopupWindow.focus();
            } catch (E) { }
        }

        function PopupPickerByCond() {
            try {
                PopupWindow = document.getElementById("_ctl0_Content_main_txt").value;
                if (!(PopupWindow.Closed) && PopupWindow != null && PopupWindow != "") {
                    var myVar = document.getElementById("<%= ddEvent.ClientID %>").value;
                    settings = 'width=970,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no, resizable=no,dependent=no';
                    PopupWindow = window.open('VolunteerSignupGuide.aspx?value=' + myVar, 'VolunteerSignupGuide Help', settings);
                    document.getElementById("_ctl0_Content_main_txt").value = PopupWindow;
                }
            } catch (E) { }
        }
    </script>

    <div style="border: 0px solid #000; display: inline-block; width: 920px">
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" runat="server" OnClick="lbtnVolunteerFunctions_Click"></asp:LinkButton>
        |  <b>
            <asp:Label ID="lblUserName" runat="server"></asp:Label>
            <asp:Label ID="lblChapter" runat="server" Style="float: right"></asp:Label></b>
    </div>


    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <strong>Volunteer Signup

        </strong>
        <br />

    </div>

<div id="divVolSignUp" runat="server" visible="false">
    
    <div align="center">
        <strong>To signup, select year, event and chapter.</strong>
        <%--  <span style="font-size:15px;color:red">This is new this year.  Please signup again; otherwise, press continue </span>--%>
    </div>

    <br />
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Your request in the signup table will not delete the roles you have been currently assigned, please confirm that you still want to delete?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <script type="text/javascript">
        function checkBoxList1OnCheck(listControlRef) {
            var inputItemArray = listControlRef.getElementsByTagName('input');

            for (var i = 0; i < inputItemArray.length; i++) {
                var inputItem = inputItemArray[i];

                if (inputItem.checked) {
                    if (inputItem.disabled == false) {

                        inputItem.parentElement.style.color = 'blue';

                    }

                }
                else {
                    inputItem.parentElement.style.color = 'Black';
                }
            }
        }
    </script>
    <table>
        <asp:Button ID="hiddenbtn" runat="server" Text="Button" OnClick="hiddenbtn_Click" Style="display: none;" />
        <asp:HiddenField ID="hfIsRedirect" Value="False" runat="server" />
        <tr>

            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

            <td align="left" nowrap="nowrap">
                <asp:DropDownList ID="ddYear" runat="server" Width="100px"
                    OnSelectedIndexChanged="ddYear_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>

            </td>

            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp<b>Chapter</b>
            </td>

            <td align="left" nowrap="nowrap" style="width: 125px">
                <asp:DropDownList ID="ddchapter" runat="server" Width="155px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hours you can spend</b>
            </td>
            <td align="left">
                <asp:DropDownList ID="ddhrs" runat="server" Width="105px" AutoPostBack="True" OnSelectedIndexChanged="ddhrs_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btncont" runat="server" Text="Submit" OnClick="btncont_Click" /></td>







        </tr>

    </table>

    <table></table>
    <table>
        <tr>
            <td style="width: 500px">
                <table style="width: 500px" id="TblTeams" runat="server">
                    <tr>
                        <td id="Chk" align="left" runat="server" visible="false">
                            <%--        <div id="Chk"  align="left"  runat="server" visible="false">--%>

                            <strong>Teams

   

                            </strong>

                            <br />
                            <div runat="server" style="width: 500px">
                                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" onclick="checkBoxList1OnCheck(this);"></asp:CheckBoxList>
                                <asp:Label ID="lbNoteams" runat="server" ForeColor="Red"></asp:Label>
                            </div>
                        </td>


                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>


            </td>
            <td>
                <table>
                    <tr>
                        <td>
                            <div style="background: #EFEFEF; width: 96%; height: auto; overflow: hidden; padding: 10px; border: solid 1px #767474; border-radius: 5px; margin-top: 20px;">
                                <asp:Label ID="Label4" runat="server" Text="For more assistance, please click" Style="font-weight: bold; font-size: 13px; color: #4c4c4c;"></asp:Label>
                                <asp:LinkButton ID="lnkHelpGuide" runat="server" Text="Help Guide" Style="font-size: 13px; font-weight: bold;" OnClick="lnkHelpGuide1_Click"></asp:LinkButton>
                                <%--  <a href="javascript:PopupPicker()" id="lnkHelpGuide" runat="server" style="font-size: 13px;font-weight: bold;" disabled="false" >Help Guide</a> --%>
                            </div>
                        </td>
                    </tr>
                </table>

            </td>



        </tr>
    </table>

    <br />
    

    <div id="Div4" align="center" runat="server">
        <asp:Label ID="lblTeam" runat="server" Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblErr" runat="server" ForeColor="Red" Visible="true" Font-Bold="true"></asp:Label>
        <asp:Label ID="lbReqfield" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblErr1" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />

        <asp:Label ID="lbEmail" runat="server" ForeColor="Blue" Font-Bold="true"></asp:Label>
        <br />
        <b>
            <asp:Label ID="GridValText" runat="server"></asp:Label>
        </b>
        <br />
        <asp:Label ID="ProductError" runat="server" ForeColor="Red"></asp:Label>
        <asp:GridView ID="Gridval" runat="server" Visible="false" EnableViewState="true"
            AutoGenerateColumns="false" OnRowDataBound="Gridval_RowDataBound">
            <Columns>
                <asp:BoundField DataField="TeamId" HeaderText="TeamId" />
                <asp:BoundField DataField="Team" HeaderText="Team" />

                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:HiddenField ID="HdnProduct" Value='<%# Bind("ProductGroup")%>' runat="server" />
                        <asp:DropDownList ID="ddlproductGroup" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddday1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:HiddenField ID="HdnProductval" Value='<%# Bind("Product")%>' runat="server" />
                        <asp:DropDownList ID="ddlproduct1" runat="server" Width="120px" AutoPostBack="True">
                        </asp:DropDownList>
                    </ItemTemplate>

                </asp:TemplateField>
            </Columns>



        </asp:GridView>

    </div>

    <br />

    <div id="Div1" align="center" runat="server">
        <asp:Button ID="btnAdd" runat="server" Text="Add to Teams"
            OnClick="btnAdd_Click" Visible="false" Height="38px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      <asp:Button ID="BtnContinue" runat="server" Text="Continue"
          OnClick="BtnContinue_Click" Height="36px" Width="118px" /><br />

        <span style="font-size: 15px; color: red" id="spnVolunteerSkip" runat="server" visible="false" >You can skip volunteer signup by pressing Continue</span>
    </div>
    <br />

   
        <div id="Div2" align="center" runat="server">
      <b>       <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>
    </b>
    <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
    <asp:GridView ID="GvVolsignup" runat="server"  DataKeyNames="VolsignupId"
        AutoGenerateColumns="false" OnRowCancelingEdit="GvVolsignup_RowCancelingEdit"
        OnRowEditing="GvVolsignup_RowEditing" OnRowUpdating="GvVolsignup_RowUpdating"
        OnRowDeleting="GvVolsignup_RowDeleting" OnPageIndexChanged="GvVolsignup_PageIndexChanged" OnPageIndexChanging="GvVolsignup_PageIndexChanging" AllowPaging="true">

        <Columns>
            <asp:TemplateField>

                <ItemTemplate>
                    <asp:Button ID="btnEdit" Text="Modify" runat="server" CommandName="Edit" />
                    <asp:Button ID="BtnDelete" Text="Delete" runat="server" CommandName="Delete" OnClientClick="Confirm()" OnClick="OnConfirm" />
                </ItemTemplate>

                <EditItemTemplate>
                    <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" />

                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                </EditItemTemplate>


            </asp:TemplateField>
            <asp:TemplateField HeaderText="VolsignupId">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblvolsignupID" Text='<%#Eval("VolsignupId") %>' />
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="TeamId">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                </ItemTemplate>

            </asp:TemplateField>



            <asp:TemplateField HeaderText="TeamName">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblTeamName" Text='<%#Eval("TeamName") %>' />
                </ItemTemplate>

            </asp:TemplateField>



            <asp:TemplateField HeaderText="Year">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventName">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblEventName" Text='<%#Eval("Eventname") %>' />
                </ItemTemplate>

            </asp:TemplateField>


            <asp:TemplateField HeaderText="ProductGroup">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HdnProductgroup" Value='<%# Bind("ProductGroupId")%>' runat="server" />
                    <asp:DropDownList ID="productGroup" runat="server" Width="120px" AutoPostBack="True" OnPreRender="DDproductGroupEdit" OnSelectedIndexChanged="ddday_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:TextBox runat="server" Visible="false" ID="TxtprodGroup" Text='<%#Eval("ProductGroup") %>' />
                </EditItemTemplate>
            </asp:TemplateField>

            <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


            <asp:TemplateField HeaderText="Product">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HdnProduct" Value='<%# Bind("ProductId")%>' runat="server" />
                    <asp:DropDownList ID="product" runat="server" Width="120px" AutoPostBack="false" OnPreRender="DDproduct">
                    </asp:DropDownList>
                    <asp:TextBox runat="server" Visible="false" ID="TxtProduct" Text='<%#Eval("product") %>' />
                </EditItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="AvailHours">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HdnAvailhrs" Value='<%# Bind("AvailHours")%>' runat="server" />
                    <asp:DropDownList ID="ddavailhrs" runat="server" Width="60px" AutoPostBack="false" OnPreRender="DDAvailhrs">
                    </asp:DropDownList>
                    <asp:TextBox runat="server" Visible="false" ID="Txthrs" Text='<%#Eval("AvailHours") %>' />
                </EditItemTemplate>
            </asp:TemplateField>




            <asp:TemplateField HeaderText="Chapter">
                <ItemTemplate>

                    <asp:Label runat="server" ID="Label3" Text='<%#Eval("Chapter") %>' />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HdnChapId" Value='<%# Bind("ChapterID")%>' runat="server" />
                    <asp:DropDownList ID="ddChaptergv" runat="server" Width="120px" AutoPostBack="false" OnPreRender="ChapterEdit">
                    </asp:DropDownList>
                    <asp:TextBox runat="server" Visible="false" ID="TxtChapter" Text='<%#Eval("Chapter") %>' />
                </EditItemTemplate>
            </asp:TemplateField>



        </Columns>


        <PagerStyle HorizontalAlign="Center" />


    </asp:GridView>

    <br />
    <asp:Label ID="lbnorec1" runat="server" ForeColor="Red"></asp:Label>
    </div>
  <br />
  
        <div id="Div3" align="center" runat="server">
         <b>     <asp:Label ID="lblRoles" Font-Bold="true" runat="server" Text=""></asp:Label>
            <asp:Label ID="lbRolesName" Font-Bold="true" runat="server" Text=""></asp:Label>
    </b>
    <asp:GridView ID="GridView1" runat="server"></asp:GridView>

    <br />
    <asp:Label ID="lbnorec" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
   
    <asp:Label ID="lbevent" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbPrd" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbEventId" runat="server" Visible="false"></asp:Label>

    <asp:Label ID="lbchaptr" runat="server" Visible="false"></asp:Label>

    <input type="hidden" id="hdnProductGroupText" value="" runat="server" />
    <input type="hidden" id="hdnProductText" value="" runat="server" />
    <input type="hidden" id="hdnTeamText" value="" runat="server" />

    </div>

    <div id="divOption" runat="server" align="center" >

        <table style="text-align:left;">
            <tr>
                <td style="text-align:center"><span id="spErr" runat="server" style="color:red"> </span></td>
            </tr>
            <tr>
                <td>NSF is completely run by volunteers. So NSF would be grateful if you can help. 
                    <br />
                    In your family, who wants to volunteer?  (Please select one option)</td>
                </tr>
            <tr>
                <td>
                    <div  style="margin-left:60px" >
                    <asp:RadioButtonList ID="rbtnOption" runat="server">
                        <asp:ListItem Value="1"></asp:ListItem>
                        <asp:ListItem Value="2"></asp:ListItem>
                        <asp:ListItem Value="3">None right now</asp:ListItem>
                    </asp:RadioButtonList></div>
                    <div  style="text-align:center">
                    <asp:Button ID="btnOptionContinue" runat="server" Text="Continue" OnClick="btnOptionContinue_Click" /></div>
                </td>
            </tr>
        </table>
    </div>


</asp:Content>

