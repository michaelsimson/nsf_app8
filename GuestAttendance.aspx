﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GuestAttendance.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="GuestAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to remove this Guest Attendee from session?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }



            function JoinMeeting() {

                var sessionkey = document.getElementById("<%=hdnSessionKey.ClientID%>").value;
                var url = "https://northsouth.zoom.us/j/" + sessionkey

                window.open(url, '_blank');

            }
            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }

        </script>
    </div>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Guest Attendance
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblErrMsg" ForeColor="Red" runat="server"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div runat="server" style="margin: auto; width: 1080px;">
        <div style="margin-top: 10px; margin-left: 20px;">

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left; width: 60px;">
                        <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddYear" runat="server" Width="85px" OnSelectedIndexChanged="ddYear_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 60px;">
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlSemester" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 60px;">
                        <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Coach"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="DDlCoach" runat="server" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="DDlCoach_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 100px;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroup" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>

                <div style="clear: both; margin-bottom: 10px;">
                </div>
                <div style="float: left;">
                    <div style="float: left; width: 60px;">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="DDlProduct" runat="server" Width="85px" OnSelectedIndexChanged="DDlProduct_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <div style="float: left; width: 60px;">
                            <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddlLevel" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" runat="server" Width="75px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <div style="float: left; width: 60px;">
                            <asp:Label ID="Label6" runat="server" Font-Bold="true" Text="Session#"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="DDlSessionNo" OnSelectedIndexChanged="DDlSessionNo_SelectedIndexChanged" runat="server" Width="110px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <div style="float: left; width: 100px;">
                            <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Day"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="DDLDay" runat="server" Width="75px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 10px;">
                </div>
                <div style="float: left;">
                    <div style="float: left;">
                        <div style="float: left; width: 60px;">
                            <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Time"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="DDLTime" runat="server" Width="85px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <div style="float: left; width: 150px;">
                            <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Guest Attendee (Coach)"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="DDlGuestAttendee" runat="server" Width="186px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <div style="float: left; width: 150px;">
                            <asp:Label ID="Label10" runat="server" Font-Bold="true" Text="Guest Attendee(Student)"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="DDLGuestStudent" runat="server" Width="100px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Button ID="BtnGenerateReport" runat="server" Text="Register Guest Attendee" OnClick="BtnGenerateReport_Click" />
                    </div>
                    <div style="float: left;">
                        <asp:Button ID="BtnReset" runat="server" Text="Reset" OnClick="BtnReset_Click" />
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div id="dvSearchGuestAttendance" runat="server" style="margin: auto; width: 1000px;">
        <div style="margin-top: 10px; margin-left: 20px;">
            <center>
                <h2>Search Guest Session</h2>
            </center>

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label11" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlEventyearFilter" runat="server" Width="100">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label12" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlPhaseFilter" runat="server" OnSelectedIndexChanged="ddlPhaseFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Fall">Fall</asp:ListItem>
                            <asp:ListItem Value="Spring">Spring</asp:ListItem>
                            <asp:ListItem Value="Summer">Summer</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroupFilter" OnSelectedIndexChanged="ddlProductGroupFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label14" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductFilter" OnSelectedIndexChanged="ddlProductFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label15" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 36px;">
                        <asp:DropDownList ID="ddlLevelFilter" runat="server" OnSelectedIndexChanged="ddlLevelFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left;">
                    <div style="float: left; margin-left: 60px;">
                        <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Session#"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 13px;">
                        <asp:DropDownList ID="ddlSessionFilter" runat="server" OnSelectedIndexChanged="ddlSessionFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 62px;">
                    <div style="float: left;">
                        <asp:Label ID="Label17" runat="server" Font-Bold="true" Text="Coach"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 54px;">
                        <asp:DropDownList ID="ddlCoachFilter" runat="server" Width="100px" OnSelectedIndexChanged="ddlCoachFilter_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 60px;">
                    <div style="float: left;">
                        <asp:Label ID="Label18" runat="server" Font-Bold="true">Day </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 34px;">
                        <asp:DropDownList ID="ddlDayFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDayFilter_SelectedIndexChanged1" Width="100">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                            <asp:ListItem Value="Monday">Monday</asp:ListItem>
                            <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                            <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                            <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                            <asp:ListItem Value="Friday">Friday</asp:ListItem>
                            <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;">

                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnClearFilter" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="width: 1100px;">

        <table id="tblCoachReg" runat="server" style="margin-left: auto; display: none; margin-right: auto; width: 75%; padding: 10px;">
            <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
                <td align="left">Event year <span style="color: red; font-size: 11px;">*</span></td>

                <td align="left">Semester <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Coach <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Product Group <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">product <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Level <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Session No <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Day<span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Time<span style="color: red; font-size: 11px;">*</span></td>
            </tr>

            <tr class="ContentSubTitle" align="center">
                <td align="left"></td>

                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
            </tr>

            <tr>
                <td align="center" colspan="9">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />

                </td>
            </tr>
        </table>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="margin-left: auto; margin-right: auto; padding: 10px; width: 95%; display: none; border: 1px solid #c6cccd;">
            <center>
                <h2>Create Guest Attendance</h2>
            </center>

            <table id="tblAddNewMeeting" runat="server" visible="true" align="center" style="width: 60%;">

                <tr class="ContentSubTitle" align="center">
                    <td style="" align="left" nowrap="nowrap">Guest Attendee (Coach)</td>

                    <td align="left" nowrap="nowrap" style=""></td>
                    <td style="" align="left" nowrap="nowrap">Guest Attendee(Student)</td>

                    <td align="left" nowrap="nowrap" style=""></td>
                    <td align="left"></td>
                </tr>
            </table>
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <span style="font-weight: bold;">Table 1: Guest Attendance</span>
        </div>
        <div style="clear: both;"></div>
        <center><span id="spntable" runat="server" style="font-weight: bold; color: red;"></span></center>
        <div align="center">
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdGuestAttendee" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 100%; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdGuestAttendee_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                        <ItemTemplate>
                            <div style="display: none;">

                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblGuestAttendID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"GuestAttendID") %>'>'></asp:Label>
                                <asp:Label ID="lblGuestStudentId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                <asp:Label ID="lblGuestCoach" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"memberID") %>'>'></asp:Label>

                                <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="LblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                                <asp:Label ID="LblMeetingKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'>'></asp:Label>
                                <asp:Label ID="LblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="LblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                                <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'>'></asp:Label>
                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WebExEmail") %>'>'></asp:Label>
                                <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'>'></asp:Label>
                                <asp:Label ID="LblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>
                                <asp:Label ID="lblType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Type") %>'>'></asp:Label>

                            </div>
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CommandName="DeleteAttendee" />
                            <asp:Button ID="BtnModify" runat="server" Text="Modify" CommandName="ModifyAttendee" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Guest Coach"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Guest Coach Email"></asp:BoundField>
                    <asp:BoundField DataField="ChildName" HeaderText="Guest Student"></asp:BoundField>
                    <asp:BoundField DataField="Childemail" HeaderText="Guest Student Email"></asp:BoundField>
                    <asp:BoundField DataField="CoachName" HeaderText="Coach"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="Time" HeaderText="Time"></asp:BoundField>
                    <asp:BoundField DataField="Type" HeaderText="Type"></asp:BoundField>

                    <asp:BoundField DataField="RegisteredID" Visible="false" HeaderText="Registered ID"></asp:BoundField>
                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("MeetingURL").ToString()%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingURL").ToString()%>'></asp:LinkButton>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("StartTime").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblPractiseDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
        <div style="clear: both;"></div>
    </div>
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />

    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />

    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnChildID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
    <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />

    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="HdnChildEmail" value="0" runat="server" />
    <input type="hidden" id="HdnLevel" value="0" runat="server" />
    <input type="hidden" id="hdnCoachname" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
    <input type="hidden" id="HdnCoachRegID" value="0" runat="server" />
    <input type="hidden" id="hdnZoomURL" value="0" runat="server" />
    <input type="hidden" id="hdnGuestAttendanceID" value="0" runat="server" />
    <input type="hidden" id="hdnGuestCoach" value="0" runat="server" />
    <input type="hidden" id="hdnGuestStudent" value="0" runat="server" />
    <input type="hidden" id="hdnRegType" value="0" runat="server" />
</asp:Content>
