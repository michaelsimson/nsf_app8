﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClassStatus.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="ClassStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>

        <style type="text/css">
            .grid {
                width: 500px;
            }

                .grid th {
                    background-color: Green;
                    color: #ffffff;
                }

                .grid tr:nth-child(even) {
                    background-color: #ffffff;
                }

                .grid tr:nth-child(odd) {
                    background-color: #cccccc;
                }

                .grid td {
                    padding-left: 10px;
                }
        </style>
    </div>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Class Status
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <div style="width: 1000px; margin-right: auto; margin-left: auto;">
        <div id="dvFrstRow">
            <div style="float: left; width: 210px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Event Year</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddYear" runat="server" Width="75px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                    </asp:DropDownList>

                </div>
            </div>
            <div style="float: left; width: 210px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Semester</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddlSemester" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                        <%--<asp:ListItem Value="13">Coaching</asp:ListItem>--%>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; width: 250px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Coach</span>
                </div>
                <div style="float: left; width: 150px;">
                    <asp:DropDownList ID="ddlCoach" runat="server" Width="140px" AutoPostBack="True" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; width: 250px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Product group</span>
                </div>
                <div style="float: left; width: 150px;">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>


        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvSecondRow">
            <div style="float: left; width: 210px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Product</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>

                </div>
            </div>
            <div style="float: left; width: 210px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Level</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddlLevel" runat="server" Width="100px" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>

                </div>
            </div>
            <div style="float: left; width: 251px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Session#</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="75px" OnSelectedIndexChanged="ddlSession_SelectedIndexChanged">
                    </asp:DropDownList>

                </div>
            </div>
            <div style="float: left; width: 210px;">
                <div style="float: left; width: 90px;">
                    <span style="font-weight: bold;">Week#</span>
                </div>
                <div style="float: left; width: 110px;">
                    <asp:DropDownList ID="ddlWeekNo" runat="server" Width="75px" OnSelectedIndexChanged="ddlWeekNo_SelectedIndexChanged">
                      <%--  <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>

                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                        <asp:ListItem Value="17">17</asp:ListItem>
                        <asp:ListItem Value="18">18</asp:ListItem>
                        <asp:ListItem Value="19">19</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="21">21</asp:ListItem>
                        <asp:ListItem Value="22">22</asp:ListItem>
                        <asp:ListItem Value="23">23</asp:ListItem>
                        <asp:ListItem Value="24">24</asp:ListItem>
                        <asp:ListItem Value="25">25</asp:ListItem>
                        <asp:ListItem Value="26">26</asp:ListItem>
                        <asp:ListItem Value="27">27</asp:ListItem>
                        <asp:ListItem Value="28">28</asp:ListItem>
                        <asp:ListItem Value="29">29</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>--%>
                    </asp:DropDownList>

                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick="BtnClear_Click" />
            </div>
        </div>
    </div>

    <table id="Table1" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; padding: 10px; display: none;">
        <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
            <td align="left">Event year</td>
            <td align="left">Event</td>

            <td align="left">Semester</td>
            <td align="left">Coach</td>
            <td align="left">Product Group</td>
            <td align="left">product</td>
            <td align="left">Level</td>
            <td align="left">Session No</td>
            <td align="left">Week#</td>
        </tr>

        <tr class="ContentSubTitle" align="center">
            <td align="left"></td>
            <td align="left">
                <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                    <%--<asp:ListItem Value="13">Coaching</asp:ListItem>--%>
                </asp:DropDownList></td>

            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
        </tr>

        <tr>
            <td align="center" colspan="9">
                <asp:Button ID="btnSubmit" runat="server" Visible="true" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />


            </td>
        </tr>
    </table>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 1: Class Status</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnExort" runat="server" Visible="true" Text="Export to Excel" OnClick="btnExort_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:Label ID="lblCoachClassStatus" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdCoachClassStatus" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdCoachClassStatus_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="Product" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="SessionNo" HeaderText="SessionNo"></asp:BoundField>
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="ClassType" HeaderText="Class Type"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                <asp:BoundField DataField="WeekNo" HeaderText="WeekNo"></asp:BoundField>




            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 2: Coaches who did not use Coach Class Calendar</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnExportCoachList" runat="server" Visible="true" Text="Export to Excel" OnClick="btnExportCoachList_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>

        <asp:Label ID="lblTable2Status" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdCoachList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdCoachList_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupcODE" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="SessionNo" HeaderText="SessionNo"></asp:BoundField>





            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>


    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 3: Class Status "Cancelled/ Makeup/ Substitute/ Makeup Cancelled/ Substitute Cancelled"</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnExportCancelledClass" runat="server" Visible="true" Text="Export to Excel" OnClick="btnExportCancelledClass_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>

        <asp:Label ID="lblTable3Status" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdCancelledClass" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdCancelledClass_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="Product" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="SessionNo" HeaderText="SessionNo"></asp:BoundField>
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="ClassType" HeaderText="Class Type"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                <asp:BoundField DataField="WeekNo" HeaderText="WeekNo"></asp:BoundField>

            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 4: Coaches who set calendar ON for the next 7 days</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnExportNext7DaysReport" runat="server" Visible="true" Text="Export to Excel" OnClick="btnExportNext7DaysReport_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:Label ID="lbltable4Status" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdCoachNext7DaysReport" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdCoachNext7DaysReport_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="Product" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="SessionNo" HeaderText="SessionNo"></asp:BoundField>
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="ClassType" HeaderText="Class Type"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                <asp:BoundField DataField="WeekNo" HeaderText="WeekNo"></asp:BoundField>




            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 5: Coaches who have not set up the calendar for the next 7 days</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnExportNotNext7DaysReport" runat="server" Visible="true" Text="Export to Excel" OnClick="btnExportNotNext7DaysReport_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:Label ID="lbltable5Status" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdCoachListNotNext7Days" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdCoachListNotNext7Days_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupcODE" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="SessionNo" HeaderText="SessionNo"></asp:BoundField>

                <asp:BoundField DataField="LastDAteOn" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Last Date On"></asp:BoundField>



            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>



    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1200px; overflow: scroll;">
        <center><b>Table 6: Practise and Guest Sessions</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; margin-left: 16px;">
            <asp:Button ID="btnPractiseAndGuest" runat="server" Visible="true" Text="Export to Excel" OnClick="btnPractiseAndGuest_Click" Style="height: 26px" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:Label ID="lbltable6Status" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdPractiseAndGuest" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" CssClass="grid" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdPractiseAndGuest_PageIndexChanging">
            <Columns>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>
                <asp:BoundField DataField="WPhone" HeaderText="WPhone"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupcODE" HeaderText="ProductGroup"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>


                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>

                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>

                <asp:BoundField DataField="Session" HeaderText="SessionNo"></asp:BoundField>

                <asp:BoundField DataField="SessionType" HeaderText="SessionNoType"></asp:BoundField>



            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>

</asp:Content>
