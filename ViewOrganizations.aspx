<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewOrganizations.aspx.vb" Inherits="ViewOrganizations" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
          <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
				<tr bgcolor="#FFFFFF">
					<td class="Heading" align="center" colspan="2">Organization Information</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td class="ContentHeading" align="right" colspan="2">
					<asp:linkbutton id="lnksearch" runat="server" Font-Bold="True" CssClass="btn_02" PostBackUrl="~/dbsearch.aspx">Search Records</asp:linkbutton>&nbsp;&nbsp;&nbsp;
					    <asp:linkbutton id="lnkAdd" runat="server" Font-Bold="True" CssClass="btn_02" PostBackUrl="~/AddOrganization.aspx">Add Organization</asp:linkbutton>&nbsp;&nbsp;&nbsp;
					    <asp:linkbutton id="lnkMainMenu" runat="server" Font-Bold="True" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:linkbutton>&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td colspan="2">
					    <asp:datagrid id="dgOrgList" runat="server" Width="100%" CssClass="GridStyle" AllowSorting="True"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="memberid" AllowPaging="True">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Member ID">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Automemberid") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Organization Name" SortExpression="Organization_Name">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Organization_Name") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address1">
									<ItemTemplate>
										<asp:Label id="lblTrnsNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address1") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address2">
									<ItemTemplate>
										<asp:Label id="lblDonationDt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address2") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="City">
									<ItemTemplate>
										<asp:Label id="lblMethod" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="State" SortExpression="State">
									<ItemTemplate>
										<asp:Label id="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Zip">
									<ItemTemplate>
										<asp:Label id="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Country">
									<ItemTemplate>
										<asp:Label id="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Country")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Email">
									<ItemTemplate>
										<asp:Label id="lblEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Created Date">
									<ItemTemplate>
										<asp:Label id="lblCreatedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreateDate")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update">
									<ItemTemplate>
										<asp:LinkButton  id="hlUpdateOrganization" runat="server" Text='Update' Font-Bold="true"></asp:LinkButton>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>								
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>&nbsp;</td>
				</tr>
				
				<tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center" colspan="2" style="height: 21px"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red"></asp:label></td>
				</tr>
				
 				<tr bgcolor="#FFFFFF">
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label></td>
				</tr> 
				<tr bgcolor="#FFFFFF">
					<td class="Heading" colspan="2" height="15"></td>
				</tr>
	            </table>
</asp:Content>



 
 
 