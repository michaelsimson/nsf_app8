﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="CalendarSignupCount.aspx.cs" Inherits="CalendarSignupCount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 1200px;
            top: 55%;
            left: 50%;
            margin-left: -600px;
            margin-top: -250px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script>
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }
        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvMemberDetails").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvMemberDetails").hide();
        }

    </script>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Calendar Signup Count
             <br />
        <br />
    </div>
    <br />

    <table align="center">
        <tr>

            <td align="left" nowrap="nowrap">From Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 100px;">
                <asp:DropDownList ID="ddlFromYear" runat="server" Width="100px">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap">To Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 100px;">
                <asp:DropDownList ID="ddlToYear" runat="server" Width="100px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" /></td>
            <td align="center">
                <asp:Button ID="BtnExpo"
                    runat="server" Visible="false"
                    Text="Export to Excel" OnClick="BtnExpo_Click" /></td>


        </tr>

    </table>

    <br />

    <table align="center">
        <tr>

            <td>
                <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

            </td>
        </tr>
    </table>

    <div style="clear: both;"></div>
    <div id="dvMemberInfo" runat="server" visible="false">
        <center>
            <span id="spnMemberTitle" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table3 : Calendar Signups for Acceptance</span>
        </center>
        <center>

            <asp:GridView ID="GrdCoachSignUp" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-right: auto; margin-top: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdCoachSignUp_RowCommand" OnRowEditing="GrdCoachSignUp_RowEditing" OnRowDataBound="GrdCoachSignUp_RowDataBound" OnRowCancelingEdit="GrdCoachSignUp_RowCancelingEdit" OnRowUpdating="GrdCoachSignUp_RowUpdating">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" Visible="false"></asp:LinkButton>
                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" Visible="false"></asp:LinkButton>
                            <div style="display: none;">
                                <asp:Label runat="server" ID="lblHdnDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPhase" Text='<%#DataBinder.Eval(Container.DataItem,"PhaseEx") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPreference" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnUserID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblhdnAutoMemberID" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SignUp ID" SortExpression="SignUpID">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSignUpID" Text='<%#DataBinder.Eval(Container.DataItem,"SignUpID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Volunteer Name" SortExpression="Name">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVolunteerName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVolunteerList" runat="server" Style="width: 100px;">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Year" SortExpression="EventYear">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlEventYear" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Code" SortExpression="EventCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventCode" Text='<%#DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Semester" SortExpression="Semester">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Semester") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ProductGroup" SortExpression="ProductGroupCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="ProductCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlProduct" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level" SortExpression="Level">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlLevel" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Session" SortExpression="SessionNo">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlSessionNo" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day" SortExpression="Day">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlDay" runat="server">
                                <asp:ListItem Value="-1">Select Day</asp:ListItem>
                                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time" SortExpression="Time">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlTime" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted" SortExpression="Accepted">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlAccepted" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="Y">Y</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Preference" SortExpression="Preference">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPreferences" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlPreference" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="2">3</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Cap" SortExpression="MaxCapacity">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlMaxCap" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VRoom" SortExpression="VRoom">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVRoom" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UserID" SortExpression="UserID" Visible="false">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblUID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUserID" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Password" SortExpression="PWD" Visible="false">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPWD" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Years" SortExpression="Years">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblYears" Text='<%#DataBinder.Eval(Container.DataItem,"Years") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sessions" SortExpression="Sessions">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSessions" Text='<%#DataBinder.Eval(Container.DataItem,"Sessions") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>


        </center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <asp:Button ID="btnCloseTable3" runat="server"
                Text="Close Table3" OnClick="btnCloseTable3_Click" />
        </center>
    </div>
    <div style="clear: both;"></div>
    <div id="dvCoachList" runat="server" visible="false">
        <center>
            <span id="Span1" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table2 : Coach List</span>
        </center>
        <center>
            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridCoachList"
                runat="server" AllowPaging="true" Width="950" PageSize="10" PageIndex="1" HeaderStyle-BackColor="#ffffcc" Style="margin-top: 10px;" OnPageIndexChanging="GridCoachList_PageIndexChanging" OnRowCommand="GridCoachList_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                        <ItemTemplate>

                            <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                            <div style="display: none;">
                                <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                            </div>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <asp:Button ID="btnColseTable2" runat="server"
                Text="Close Table2" OnClick="btnColseTable2_Click" />

        </center>
    </div>
    <div style="clear: both;"></div>
    <div runat="server" align="center">
        <center>
            <span id="Span2" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table1: Calendar Signup Count</span>
        </center>
        <table>
            <tr>
                <td>
                    <asp:GridView ID="GVcount" runat="server" EnableModelValidation="True" OnRowDataBound="GVcount_RowDataBound" OnRowCommand="GVcount_RowCommand" AutoGenerateColumns="false" HeaderStyle-BackColor="#ffffcc">
                        <%-- <Columns>
                            <asp:BoundField DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="Product"></asp:BoundField>
                            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                            <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                            <asp:BoundField DataField="2010" HeaderText="2010"></asp:BoundField>
                            <asp:BoundField DataField="2011" HeaderText="2011"></asp:BoundField>
                            <asp:BoundField DataField="2012" HeaderText="2012"></asp:BoundField>
                            <asp:BoundField DataField="2013" HeaderText="2013"></asp:BoundField>
                            <asp:BoundField DataField="2014" HeaderText="2014"></asp:BoundField>
                            <asp:BoundField DataField="2015" HeaderText="2015"></asp:BoundField>
                            <asp:BoundField DataField="2016" HeaderText="2016"></asp:BoundField>
                            <asp:TemplateField HeaderText="Accepted" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblAccepted" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>' CommandName="SelectAccepted"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Not Accepted" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblNotAccepted" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Not Accepted") %>' CommandName="SelectNotAccepted"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>--%>
                        <%-- <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>--%>
                    </asp:GridView>


                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <span id="Span3" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table2: Calendar Signup - Accepted Count</span>
                    </center>
                    <center>
                        <asp:GridView ID="gvAccepted" runat="server" EnableModelValidation="True" AutoGenerateColumns="true" HeaderStyle-BackColor="#ffffcc" OnRowDataBound="gvAccepted_RowDataBound">
                        </asp:GridView>
                    </center>
                </td>
            </tr>
        </table>
    </div>
    <div style="clear: both;">
    </div>
    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dvMemberDetails" class="web_dialog">

        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <input type="button" id="btnClose" onclick="HideDialog()" value="Close" />
        </center>
    </div>
    <input type="hidden" id="hdnMemberID" value="0" runat="server" />
    <input type="hidden" id="hdnProductGroupCode" value="0" runat="server" />
    <input type="hidden" id="hdnProductCode" value="0" runat="server" />

    <input type="hidden" id="hdnLevel" value="0" runat="server" />
    <input type="hidden" id="hdnPhase" value="0" runat="server" />
    <input type="hidden" id="hdnStatus" value="0" runat="server" />

    <input type="hidden" id="hdnCoachingYear" value="0" runat="server" />


</asp:Content>

