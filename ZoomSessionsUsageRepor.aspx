﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZoomSessionsUsageRepor.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="ZoomSessionsUsageRepor" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var pickerOpts = {
                dateFormat: $.datepicker.regional['fr']
            };
            $("#<%=txtFrom.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtTo.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });

        });
    </script>

    <table id="tblLogin" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none;" runat="server" align="center" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>
        </tr>
        <tr>

            <td>
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Zoom Sessions Usage Report</strong>
                </div>
            </td>
        </tr>

    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <div id="dvManipulateZoom" style="width: 1050px; margin: auto;">
        <div id="dvFrstRow" style="padding: 10px;">
            <div style="width: 200px; float: left;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Year</span>
                </div>
                <div style="width: 100px; float: left;">
                    <asp:DropDownList ID="ddlYear" Style="width: 100px;" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="2017">2017</asp:ListItem>
                        <asp:ListItem Value="2018" Selected="True">2018</asp:ListItem>
                        <asp:ListItem Value="2019">2019</asp:ListItem>
                        <asp:ListItem Value="2020">2020</asp:ListItem>
                        <asp:ListItem Value="2021">2021</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 250px; float: left;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Volunteer</span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlVolunteer" Style="width: 140px;" AutoPostBack="true" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div style="width: 200px; float: left;">
                <div style="width: 80px; float: left;">
                    <span style="font-weight: bold;">From</span>
                </div>
                <div style="width: 100px; float: left;">
                    <asp:TextBox ID="txtFrom" runat="server" Style="width: 90px;"></asp:TextBox>
                </div>
            </div>


            <div style="width: 200px; float: left;">
                <div style="width: 60px; float: left;">
                    <span style="font-weight: bold;">To</span>
                </div>
                <div style="width: 100px; float: left;">
                    <asp:TextBox ID="txtTo" runat="server" Style="width: 90px;"></asp:TextBox>
                </div>
            </div>

            <div style="width: 150px; float: left;">
                <div style="width: 70px; float: left;">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </div>
                  <div style="width: 70px; float: left;">
                    <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                </div>

            </div>


        </div>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div style="float: left;">
        <asp:Button ID="BtnExport" runat="server" Text="Export to Excel" OnClick="BtnExport_Click" />
    </div>
    <div style="clear: both;"></div>
    <center>
        <asp:Label ID="lblTitle" runat="server" Font-Bold="true">Table 1: Zoom Sessions</asp:Label>
    </center>
    <div style="width: 1100px; margin: auto;">

        <center>
            <asp:Label ID="lblNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
        </center>

        <asp:GridView ID="gvOnlineWkShopZoom" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" HeaderStyle-BackColor="#FFFFCC">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Host URL" Visible="false">
                    <ItemTemplate>
                        <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteSession" />
                        <div style="display: none;">
                            <asp:Label ID="lblWebConfId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ConfId") %>'></asp:Label>
                            <asp:Label ID="lblHostId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Year" HeaderText="Year" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="RoleCode" HeaderText="Role" />
                <asp:BoundField DataField="teamName" HeaderText="team" />
                <asp:BoundField DataField="EventCode" HeaderText="Event" />
                <asp:BoundField DataField="Date" HeaderText="Meeting Date" DataFormatString="{0:MM-dd-yyyy}" />
                <asp:BoundField DataField="Time" HeaderText="Time" />
                <asp:BoundField DataField="Duration" HeaderText="Duration" />
                <asp:BoundField DataField="SessionType" HeaderText="SessionType" />
                <asp:BoundField DataField="Vroom" HeaderText="Vroom" />
                <asp:BoundField DataField="SessionKey" HeaderText="SessionKey" />
                <%--  <asp:BoundField DataField="WebinarLink" HeaderText="WebinarLink" />--%>
                <asp:TemplateField HeaderText="Host URL" Visible="false">
                    <ItemTemplate>

                        <div style="display: none;">

                            <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                            <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </div>
                        <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />


                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Attendees" Visible="false">
                    <ItemTemplate>

                        <div style="display: none;">

                            <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>

                        </div>
                        <input type="button" class="btnInviteAttendee" value="invite Attendees" attr-joinurl='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>' />
                        <%--  <asp:Button ID="" runat="server" attr-JoinURl=CssClass="BtnInviteAttendee" Text="invite Attendees" CommandName="invite" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <%-- <asp:BoundField DataField="JoinURL" HeaderText="JoinURL" />--%>
            </Columns>

        </asp:GridView>
    </div>
</asp:Content>
