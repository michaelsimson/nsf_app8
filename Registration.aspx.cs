﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Registration : System.Web.UI.Page
{
    BeeDatabaseDataContext dc = new BeeDatabaseDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if(Session["Page"] ==null)
            Session["Page"]="NewScholars";

        if (Session["Page"].ToString() == "NewScholars")
        {
            back.Visible = false;
        }
        if (Session["Page"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        txtFirstName.Focus();
        Page.Form.DefaultButton = Button1.UniqueID;
        if (!this.IsPostBack)
        {
            Session["NSFCaptchaText"] = GenerateRandomCode();
            DataTable dtcrs = DAL.GetDataTable("GetCourse");
            if (dtcrs.Rows.Count > 0)
            {
                ddlCourseDesired.DataSource = dtcrs;
                ddlCourseDesired.DataValueField = "CourseID";
                ddlCourseDesired.DataTextField = "Name";
                ddlCourseDesired.DataBind();
                ddlCourseDesired.Items.Insert(0, "--Select--");

            }
            Session["VID"] = "";

            lblMessage.Visible = false;
            int MaxYear = DateTime.Now.Year;
            for (int i = MaxYear; i >= (DateTime.Now.Year - 10); i--)
            {
                ddlYearofJoining.Items.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString().Substring(2, 2), i.ToString()));
                ddlyearofapply.Items.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString().Substring(2, 2), i.ToString())); ;
            }
           
            
        }
        if (Session["Page"].ToString() == "Scholar")
        {
            DataTable dt1 = BLPersonDetails.GetRegPersonDetails(Convert.ToInt32(Session["newid"]));

            if (dt1.Rows.Count > 0)
            {
                lblApplyingyear1.Visible = true;
                lblApplyingyear2.Visible = false;
                lblTitle.Text = "NSF India Scholarship Application - Existing Applicant Registration";
                lblCurrentYear.Visible = false;
                lblCurrentYearDisplay.Visible = false;
                lblYearofJoining.Visible = false;
                ddlyearofapply.Visible = true;
                trmain.Visible = false;
                Button1.OnClientClick = "";
                txtFirstName.Text = dt1.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dt1.Rows[0]["LastName"].ToString();
                txtPassword.Text = dt1.Rows[0]["Password"].ToString();
                txtConfirmPass.Text = dt1.Rows[0]["Password"].ToString();
                ddlFrequency.SelectedValue = dt1.Rows[0]["EmailUSage"].ToString();
                txtIncome.Text = dt1.Rows[0]["FamilyIncome"].ToString();
                ddlyearofapply.SelectedValue = dt1.Rows[0]["ApplyingYear"].ToString();
                ddlNSF.SelectedValue = dt1.Rows[0]["AboutNSF"].ToString();
                ddlCourseDesired.SelectedValue = dt1.Rows[0]["Course"].ToString();
                ddlmarks.SelectedValue = dt1.Rows[0]["EntranceScore"].ToString();
                ddlYearofJoining.SelectedValue = dt1.Rows[0]["YearJoined"].ToString();
                txtbrief.Text = dt1.Rows[0]["AboutNSFBrief"].ToString();
                txtEmail.Text = dt1.Rows[0]["Email"].ToString();
                back.Visible = false;

            }
        }
        if (Session["Page"].ToString() == "ExistingScholars")
        {
            lblTitle.Text = "NSF India Scholarship Application - NSF Student First Registration";
            lblApplyingyear1.Visible = false;
            lblApplyingyear2.Visible = true;
            lblApplyingyear2.Text = "First Time of NSF Scholarship Year";
            lblCurrentYear.Visible = false;
            lblCurrentYearDisplay.Visible = false;
            lblYearofJoining.Visible = false;
            ddlyearofapply.Visible = true;
            back.Visible = false;
            yearofjoiningDefaultValue();
        }
        else if (Session["Page"].ToString() != "Scholar")
        {
            lblYearofJoining.Visible = true;
            lblApplyingyear1.Visible = false;
            lblCurrentYear.Text = DateTime.Now.Year.ToString();
            DateTime startDate = new DateTime(DateTime.Now.Year, 6, 1);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 10);
            DateTime now = DateTime.Now;

            //DateTime now = new DateTime(DateTime.Now.Year, 5, 1); // testing for Jan 1 st to May 31
            if (now >= startDate && now <= endDate)
            {
                lblCurrentYearDisplay.Text = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Year + 1).ToString().Substring(2, 2);
                lblCurrentYear.Text = DateTime.Now.Year.ToString();
            }
            else
            {
                lblCurrentYearDisplay.Text = (DateTime.Now.Year-1).ToString() + "-" + (DateTime.Now.Year).ToString().Substring(2, 2);
                lblCurrentYear.Text = (DateTime.Now.Year-1).ToString();
            }

            
        }
        if (Session["IsApplicant"] == "false")
        {

            DateTime stDT = new DateTime(DateTime.Now.Year, 6, 1);
            DateTime endDT = new DateTime(DateTime.Now.Year, 12, 10); // Fresh Online Application Submisson Date Extended upto 10th December 2017
            if (DateTime.Now < stDT || DateTime.Now > endDT)
            {
               // Session["IsApplicant"] = "false";
                lblMessage.Visible = true;
                lblMessage.Text = "Fresh Application form online submission date has been Closed";
                return;
            }
        }
        
    }
    protected void yearofjoiningDefaultValue()
    {
        DateTime startDate = new DateTime(DateTime.Now.Year, 6, 1);
        DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31);
        DateTime now = DateTime.Now;
        int MaxYear = DateTime.Now.Year;
        if (now >= startDate && now <= endDate)
        {
            ddlYearofJoining.Items.Add(new ListItem(MaxYear.ToString() + "-" + (MaxYear + 1).ToString().Substring(2, 2), MaxYear.ToString()));

        }
        else
        {

            ddlYearofJoining.Items.Add(new ListItem((MaxYear - 1).ToString() + "-" + (MaxYear).ToString().Substring(2, 2), (MaxYear - 1).ToString()));
        }
        ddlYearofJoining.Enabled = false;
    }
    private void RefreshCaptcha()
    {
        this.Session["NSFCaptchaText"] = GenerateRandomCode();
        imgCaptcha.ImageUrl = "NSFCaptcha.aspx";
        lblCaptchaErr.Text = "";
    }
    int PreAppId;
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        { 
            PreAppId=0;
            if (!(txtReCaptcha.Text.ToString().Equals(Session["NSFCaptchaText"].ToString())))
            {
                RefreshCaptcha();
                lblCaptchaErr.Text = "You have entered the wrong verification code.";
                return;
            }
            
            if (Session["Page"].ToString() != "Scholar")
            {
                DataTable dtuserId = BLUser.GetUserID(txtEmail.Text.Trim());
                int bIsDupEmail = 0;
                int bIsDupName = 0;
                lblPswd.Text = txtPassword.Text;
                if (dtuserId.Rows.Count > 0)
                {
                    bIsDupEmail = 1;
                    //lblMessage.Visible = true;
                    //lblMessage.Text = "Email ID Already Exists.Try With Another";
                } 
                DataTable dt = BLUser.GetUserDetailsByName(txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim(),"Applicant");
                if (dt.Rows.Count > 0)
                {
                    bIsDupName = 1;
                    //lblMessage.Visible = true;
                    //lblMessage.Text = "The first name and last name already exists.Try With Another";
                    //return;
                }
                if (bIsDupEmail == 0 && bIsDupName == 0 )
                {
                    btnCreateNewAccount_Click(sender, new EventArgs());
                }
                else{
                    string strCmd = "select AppId from Applicant_Header where ";// user_email='"+ txtEmail.Text.Trim() +"' and user_name='"+ txtFirstName.Text.Trim() + " "+ txtLastName.Text.Trim() +"' ";
                    if(bIsDupEmail == 1 && bIsDupName == 0)
                    {
                        strCmd = strCmd+ " Email='"+ txtEmail.Text.Trim() +"'";
                    }
                    if(bIsDupEmail == 1 && bIsDupName == 1)
                    {
                        strCmd = strCmd+ " Email='" + txtEmail.Text.Trim() + "' and FirstName='" + txtFirstName.Text.Trim() + "' and LastName='" + txtLastName.Text.Trim() + "' ";
                    }
                    if (bIsDupEmail == 0 && bIsDupName == 1)
                    {
                        strCmd = strCmd + " FirstName='" + txtFirstName.Text.Trim() + "' and LastName='" + txtLastName.Text.Trim() + "'";
                    }
 strCmd = strCmd +  " and AppStatusId=7 and CourseId=6";
                    string con= ConfigurationManager.ConnectionStrings["NSFDBCONNECTION"].ToString();
                     
                    DataTable dtPrevAppId=DAL.GetDataTableBySql(strCmd);
                    if(dtPrevAppId.Rows.Count==1)
                    {
                        PreAppId= Convert.ToInt32(dtPrevAppId.Rows[0][0]);                      
                     }
                    else if (dtPrevAppId.Rows.Count > 1)
                    {
                        PreAppId = -1;
                    }
                    lblPreAppId.Text = PreAppId.ToString();
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:DuplicateConfirmation('" + bIsDupName.ToString() + "','" + bIsDupEmail.ToString() + "','"+ PreAppId +"');", true);
                }
                
                //if(bIsDupEmail==false && bIsDupName==false)
                //{  
                //    lblMessage.Visible = false;
                //    Login_Master newuser = new Login_Master
                //    {
                //        user_email = txtEmail.Text,
                //        user_pwd = txtPassword.Text,
                //        user_role = "Applicant",
                //        date_added = Convert.ToDateTime(DateTime.Now.ToString()),
                //        user_name = txtFirstName.Text + ' ' + txtLastName.Text,
                //        CreateDate = DateTime.Now.ToString(), 
                //    };
                //    dc.Login_Masters.InsertOnSubmit(newuser);
                //    dc.SubmitChanges();

                //    int maxId1 = newuser.login_id;

                //    Login_Master ind = dc.Login_Masters.Single(p => p.login_id == maxId1);
                //    {
                //        ind.FirstTimeLogon = Convert.ToInt32(maxId1);
                //    }
                //    dc.SubmitChanges();

                //    Applicant_Header newreg = new Applicant_Header();
                //    {
                //        newreg.FirstName = txtFirstName.Text;
                //        newreg.LastName = txtLastName.Text;
                //        newreg.EmailUsage = ddlFrequency.SelectedItem.Text;
                //        newreg.FamilyIncome = decimal.Parse(txtIncome.Text);
                //        if (Session["Page"].ToString() == "ExistingScholars")
                //        {
                //            newreg.ScholarshipEntryYear = Convert.ToInt32(ddlyearofapply.SelectedValue);
                //        }
                //        else
                //        {
                //            newreg.ScholarshipEntryYear = int.Parse(lblCurrentYear.Text);
                //        }
                //        newreg.AboutNSF = ddlNSF.SelectedItem.Text;
                //        newreg.AboutNSFBrief = txtbrief.Text;
                //        newreg.User_ID = txtEmail.Text;
                //        newreg.Email = txtEmail.Text;
                //        newreg.AppStatusID = 1;
                //        newreg.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                //        newreg.CreatedBy = maxId1;

                //    };
                //    dc.Applicant_Headers.InsertOnSubmit(newreg);
                //    dc.SubmitChanges();
                //    Current_Education_Detail newEdu = new Current_Education_Detail();
                //    {

                //        newEdu.AppID = newreg.AppID;
                //        newEdu.EntranceScoreOverall = int.Parse(ddlmarks.SelectedValue);
                //        newEdu.YearJoined = int.Parse(ddlYearofJoining.SelectedValue);
                //        newEdu.CourseID = int.Parse(ddlCourseDesired.SelectedValue);
                //        if (Session["Page"].ToString() == "ExistingScholars")
                //        {
                //            newEdu.ScholarshipEntryYear = Convert.ToInt32(ddlyearofapply.SelectedValue);
                //        }
                //        else
                //        {
                //            newEdu.ScholarshipEntryYear = int.Parse(lblCurrentYear.Text);
                //        }
                //        newEdu.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                //        newEdu.CreatedBy = maxId1;

                //    };
                //    dc.Current_Education_Details.InsertOnSubmit(newEdu);
                //    dc.SubmitChanges();
                //    Educational_Institution EdInst = new Educational_Institution
                //    {
                //        AppID = newreg.AppID,
                //        CreatedDate = Convert.ToDateTime(DateTime.Now.ToString()),
                //        CreatedBy = maxId1
                //    };
                //    dc.Educational_Institutions.InsertOnSubmit(EdInst);
                //    dc.SubmitChanges();
                    
                //    Applicant_Address newreg1 = new Applicant_Address();
                //    {
                //        newreg1.AppID = newreg.AppID;
                //        newreg1.AddressType = "P";
                //        newreg1.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                //        newreg1.CreatedBy = maxId1;
                //    }
                //    dc.Applicant_Addresses.InsertOnSubmit(newreg1);
                //    dc.SubmitChanges();
                //    Applicant_Address newreg2 = new Applicant_Address();
                //    {
                //        newreg2.AppID = newreg.AppID;
                //        newreg2.AddressType = "C";
                //        newreg2.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                //        newreg2.CreatedBy = maxId1;
                //    }
                //    dc.Applicant_Addresses.InsertOnSubmit(newreg2);
                //    dc.SubmitChanges();
                     
                //    int maxId = newreg.AppID;
                //    Session["newid"] = maxId.ToString();
                //    maxId1 = newuser.login_id;
                //    Session["SR_NO"] = newuser.login_id;
                //    Session["SRNO"] = newuser.login_id;
                //    Session["EmailID"] = newuser.user_email;
                //    Session["eMail"] = newuser.user_email;
                //    Session["UserID"] = newuser.user_email;
                //    HttpContext.Current.Session["USER_TYPE"] = "Applicant";
                //    HttpContext.Current.Session["USERTYPE"] = "Applicant";
                //    Session["Role"] = "Applicant";
                //    if (Session["Page"].ToString() == "ExistingScholars")
                //    {
                //        Session["Page"] = "ExistingScholars";
                //    }
                //    else
                //    {
                //        Session["Page"] = "Registration";
                //    }
                //    Response.Redirect("Student_Details.aspx");
                //}
            }
            else
            {
                HttpContext.Current.Session["newid"] = Session["newid"].ToString();
                HttpContext.Current.Session["eMail"] = Session["eMail"].ToString();
                HttpContext.Current.Session["EmailID"] = Session["EmailID"].ToString();
                HttpContext.Current.Session["UserID"] = Session["UserID"].ToString();
                HttpContext.Current.Session["SR_NO"] = Session["SR_NO"].ToString();
                HttpContext.Current.Session["SRNO"] = Session["SRNO"].ToString();
                HttpContext.Current.Session["USER_TYPE"] = Session["USER_TYPE"].ToString();
                HttpContext.Current.Session["USERTYPE"] = Session["USERTYPE"].ToString();
                HttpContext.Current.Session["Role"] = "Applicant";
                HttpContext.Current.Session["PageIndex"] = "";
                Session["Page"] = "ScholarLogin";
                Response.Redirect("Student_Details.aspx");
            }
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error Message:" + ex.Message;
        }
    }
    protected void back_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    private System.Random rand = new System.Random();
    private string GenerateRandomCode()
    {

        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        string s = "";
        for (int i = 0; i <= 5; i++)
        {
            s = s + chars.Substring(this.rand.Next(36), 1);
            //s = s + chars.Substring(this.rand.Next(36), 1);
        }
        return s;
    }
    protected void lBGenerateCaptcha_Click(object sender, EventArgs e)
    {
        lblCaptchaErr.Text = "";
        this.Session["NSFCaptchaText"] = GenerateRandomCode();
    }

    protected void btnCreateNewAccount_Click(object sender, EventArgs e)
    {        

            lblMessage.Visible = false;
            Login_Master newuser = new Login_Master
            {
                user_email = txtEmail.Text,
                user_pwd = lblPswd.Text,
                user_role = "Applicant",
                date_added = Convert.ToDateTime(DateTime.Now.ToString()),
                user_name = txtFirstName.Text + ' ' + txtLastName.Text,
                CreateDate = DateTime.Now.ToString(),

            };
            dc.Login_Masters.InsertOnSubmit(newuser);
            dc.SubmitChanges();

            int maxId1 = newuser.login_id;

            Login_Master ind = dc.Login_Masters.Single(p => p.login_id == maxId1);
            {
                ind.FirstTimeLogon = Convert.ToInt32(maxId1);
            }
            dc.SubmitChanges();

            Applicant_Header newreg = new Applicant_Header();
            {
                newreg.FirstName = txtFirstName.Text;
                newreg.LastName = txtLastName.Text;
                newreg.EmailUsage = ddlFrequency.SelectedItem.Text;
                newreg.FamilyIncome = decimal.Parse(txtIncome.Text);
                if (Session["Page"].ToString() == "ExistingScholars")
                {
                    newreg.ScholarshipEntryYear = Convert.ToInt32(ddlyearofapply.SelectedValue);
                }
                else
                {
                    newreg.ScholarshipEntryYear = int.Parse(lblCurrentYear.Text);
                }
                newreg.AboutNSF = ddlNSF.SelectedItem.Text;
                newreg.AboutNSFBrief = txtbrief.Text;
                newreg.User_ID = txtEmail.Text;
                newreg.Email = txtEmail.Text;
                newreg.AppStatusID = 1;
                newreg.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                newreg.CreatedBy = maxId1;
                if (lblPreAppId.Text == "-1")
                {
                    newreg.PrevAppId = 0;
                }
                else
                {
                    newreg.PrevAppId = Convert.ToInt32(lblPreAppId.Text);
                }

            };
            dc.Applicant_Headers.InsertOnSubmit(newreg);
            dc.SubmitChanges();
            Current_Education_Detail newEdu = new Current_Education_Detail();
            {

                newEdu.AppID = newreg.AppID;
                newEdu.EntranceScoreOverall = int.Parse(ddlmarks.SelectedValue);
                newEdu.YearJoined = int.Parse(ddlYearofJoining.SelectedValue);
                newEdu.CourseID = int.Parse(ddlCourseDesired.SelectedValue);
                if (Session["Page"].ToString() == "ExistingScholars")
                {
                    newEdu.ScholarshipEntryYear = Convert.ToInt32(ddlyearofapply.SelectedValue);
                }
                else
                {
                    newEdu.ScholarshipEntryYear = int.Parse(lblCurrentYear.Text);
                }
                newEdu.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                newEdu.CreatedBy = maxId1;

            };
            dc.Current_Education_Details.InsertOnSubmit(newEdu);
            dc.SubmitChanges();
            Educational_Institution EdInst = new Educational_Institution
            {
                AppID = newreg.AppID,
                CreatedDate = Convert.ToDateTime(DateTime.Now.ToString()),
                CreatedBy = maxId1
            };
            dc.Educational_Institutions.InsertOnSubmit(EdInst);
            dc.SubmitChanges();

            Applicant_Address newreg1 = new Applicant_Address();
            {
                newreg1.AppID = newreg.AppID;
                newreg1.AddressType = "P";
                newreg1.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                newreg1.CreatedBy = maxId1;
            }
            dc.Applicant_Addresses.InsertOnSubmit(newreg1);
            dc.SubmitChanges();
            Applicant_Address newreg2 = new Applicant_Address();
            {
                newreg2.AppID = newreg.AppID;
                newreg2.AddressType = "C";
                newreg2.CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
                newreg2.CreatedBy = maxId1;
            }
            dc.Applicant_Addresses.InsertOnSubmit(newreg2);
            dc.SubmitChanges();

            int maxId = newreg.AppID;
            Session["newid"] = maxId.ToString();
            maxId1 = newuser.login_id;
            Session["SR_NO"] = newuser.login_id;
            Session["SRNO"] = newuser.login_id;
            Session["EmailID"] = newuser.user_email;
            Session["eMail"] = newuser.user_email;
            Session["UserID"] = newuser.user_email;
            HttpContext.Current.Session["USER_TYPE"] = "Applicant";
            HttpContext.Current.Session["USERTYPE"] = "Applicant";
            Session["Role"] = "Applicant";
            if (Session["Page"].ToString() == "ExistingScholars")
            {
                Session["Page"] = "ExistingScholars";
            }
            else
            {
                Session["Page"] = "Registration";
            }
            Response.Redirect("Student_Details.aspx");
      
    }
    protected void DeleteRecord(object sender, EventArgs e)
    {
       // ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Record Deleted.')", true);
    }
}
