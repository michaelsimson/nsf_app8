﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class QRAttendance
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'If LCase(Session("LoggedIn")) <> "true" Then
            '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            'End If
            Dim ContestYear, ChildNumber As Integer
            Dim StrSQL As String = ""
            ContestYear = 2011 'Now.Year() 'Testing
            Try
                ChildNumber = Request.QueryString("ChildNumber")

                StrSQL = "Update Contestant Set CheckinFlag='Y' From Contestant cn Inner Join Contest c On c.ContestID= cn.ContestID and c.Contest_Year= cn.ContestYear "
                StrSQL = StrSQL & "Where cn.ChildNumber = " & ChildNumber & "  And cn.ContestYear = " & ContestYear & " and c.EventId=1 and Convert(Date, c.ContestDate) = Convert(Date,'2011-09-03 00:00:00') and cn.BadgeNumber is not null" ' Convert(Date, getdate())"  Testing 

                If ChildNumber <> 0 Then
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL) > 0 Then
                        LblBarCode.Text = "Attendance Flag Updated Successfully"
                    Else
                        LblBarCode.Text = "Unable to Update Attendance Flag"
                    End If
                Else
                    LblBarCode.Text = "ChildNumber Not Valid"
                End If

            Catch ex As Exception
                LblBarCode.Text = "Input(ChildNumber) Not Valid"
            End Try
        End If
    End Sub
End Class
