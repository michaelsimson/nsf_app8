

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContactList.aspx.vb" Inherits="ContactList" title="Volunteer Contact List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <asp:SqlDataSource ID="ChapInZonesDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
      SelectCommand="usp_GetChapterWithinZone" SelectCommandType="StoredProcedure">
      <SelectParameters>
         <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
            Type="Int32" />
      </SelectParameters>
   </asp:SqlDataSource>
   <asp:SqlDataSource ID="ChaWithinClustersDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
      SelectCommand="usp_GetChapterWithinCluster" SelectCommandType="StoredProcedure">
      <SelectParameters>
         <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
            Type="Int32" />
      </SelectParameters>
   </asp:SqlDataSource>
   <asp:SqlDataSource ID="ChapersDSet" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
      SelectCommand="usp_GetChapterAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
   <table border=0  width=100%>
      <tr>
         <td>
            <b>
               <asp:hyperlink id="hlinkParentRegistration" runat="server" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Label  ForeColor=green ID="lblLoginName" runat=server></asp:Label>
            </b>
            &nbsp;&nbsp;&nbsp;&nbsp; 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>
               <asp:Label forecolor=green ID="lblLoginRole" runat=server></asp:Label>
            </b>
         </td>
      </tr>
      <tr>
         <td>
            <asp:Label ID="lblSessionTimeout" Visible=false ForeColor=red runat=server></asp:Label>
         </td>
      </tr>
      <tr>
         <td>
            <table  border=1 runat="server" id="tblAssignRoles" bgcolor=silver width =30%>
               <TBODY>
                  <tr>
                     <td  colspan="2" bgcolor=white  >
                        <b>1. Select Role to get Contact List</b>						
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px; height: 25px;">
                        &nbsp;Role Category:
                     </td>
                     <td style="width: 500px; height: 25px;">
                        <asp:dropdownlist id="ddlRoleCatSch" tabIndex="7" runat="server" CssClass="SmallFont" autopostback=true OnSelectedIndexChanged="ddlRoleCat_selectedIndexChanged" Width="160px">
                           <asp:ListItem Text="Select a Category" Value="0" Selected=True></asp:ListItem>
                        </asp:dropdownlist>
                        <asp:label ID="lblRoleCat" runat=server Visible=false></asp:label>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">
                        Role:
                     </td>
                     <td style="width: 347px">
                        <asp:ListBox id="ddlRoleSch" SelectionMode="Multiple" tabIndex="7"   DataTextField="selection" DataValueField="roleid" runat="server" CssClass="SmallFont" Width="160px">
                        </asp:ListBox>
                        <br />(Hold Ctrl key to select muplitple roles)
                     </td>
                  </tr>
                  <tr runat = "server" id="tr1">
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px; height: 25px;">
                        &nbsp;Week of :
                     </td>
                     <td style="width: 500x; height: 25px;">
                        <asp:dropdownlist id="ddWeekOf" tabIndex="7" runat="server" CssClass="SmallFont" autopostback=true Enabled=false  Width="160px">
                           <asp:ListItem Text="Select a Date" Value="0" Selected=True></asp:ListItem>
                        </asp:dropdownlist>
                        <asp:label ID="lblWeekOf" runat=server Visible=false></asp:label>
                     </td>
                  </tr>
                  <tr  runat = "server" id="tr2">
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">
                        Zone:
                     </td>
                     <td style="width: 347px">
                        <asp:SqlDataSource ID="ZoneDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                           SelectCommand="usp_Getzones" SelectCommandType="StoredProcedure">
                        </asp:SqlDataSource>
                        <asp:DropDownList ID="DdlZonalCoordinator" runat="server" Width="160px" AppendDataBoundItems="True" OnSelectedIndexChanged="DdlZonalCoordinator_SelectedIndexChanged" AutoPostBack="True">
                           <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr  runat = "server" id="tr3">
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">
                        Cluster:
                     </td>
                     <td style="width: 347px">
                        <asp:DropDownList ID="DdlCluster" runat="server"  Width="162px" OnSelectedIndexChanged="Cluster_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True">
                           <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="ClusterDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                           SelectCommand="usp_Getclusters" SelectCommandType="StoredProcedure">
                        </asp:SqlDataSource>
                     </td>
                  </tr>
                  <tr  runat = "server" id="tr4">
                     <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 103px">
                        Chapter:
                     </td>
                     <td style="width: 347px">
                        <asp:DropDownList ID="ddlChapter" runat="server" AppendDataBoundItems="True">
                           <asp:ListItem>Select Chapter</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ChapterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                           SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                        </asp:ObjectDataSource>
                     </td>
                  </tr>
                  <tr runat ="server" id="TrPrdGrp" visible ="false" >
                     <td class="ItemLabel" vAlign="top" noWrap align="right" >
                        Product Group
                     </td>
                     <td align="left">
                        <asp:DropDownList ID="ddlProductGroup" Width="170px"   DataTextField="Name" DataValueField="ProductGroupID"  OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                     </td>
                  </tr>
                  <tr runat ="server" id="TrPrd" visible ="false">
                     <td class="ItemLabel" vAlign="top" noWrap align="right">
                        Product 
                     </td>
                     <td align="left">
                        <asp:DropDownList ID="ddlProduct" Enabled="false" DataTextField="Name"        DataValueField="ProductID"   Width="170px" runat="server"></asp:DropDownList>
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                     </td>
                  </tr>
                  <tr>
                     <td style="height: 4px; width: 103px;">
                        <asp:Label ID="lbllRoleIds" runat="server" Visible = "false" ></asp:Label>
                     </td>
                     <td style="width: 347px; height: 4px;">
                        <asp:Button ID="Find" runat="server" Text="Find" />
                        <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label>
                     </td>
                  </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td align="center">
            <table cellpadding="3" border ="0" cellspacing ="0">
               <tr id="trviewChildren" runat ="server" visible ="false"  >
                  <td class="ItemLabel" style="vertical-align : middle " noWrap align="right">
                     Select Coach
                  </td>
                  <td>
                     <asp:DropDownList ID="ddlCoach" AutoPostBack="true" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged"  DataTextField="Name" 
                        DataValueField="ID"  runat="server" Height="22px" Width="150px"></asp:DropDownList>
                     &nbsp;
                  </td>
                  <td>
                     Year :
                     <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlCyear_SelectedIndexChanged" ID="ddlCYear" runat="server">
                     </asp:DropDownList>
                  </td>
                  <td>Phase :</td>
                  <td>
                     <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlCyear_SelectedIndexChanged" ID="ddlPhase" runat="server">
                        <asp:ListItem Value="1">One</asp:ListItem>
                        <asp:ListItem Value="2">Two</asp:ListItem>
                     </asp:DropDownList>
                  </td>
                  <td>Session :</td>
                  <td>
                     <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlSessionNo_SelectedIndexChanged" ID="ddlSessionNo" runat="server">
                        <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                     </asp:DropDownList>
                     &nbsp;&nbsp; 
                     <asp:Button ID="btnChildrenDetail" runat="server" Visible="false" OnClick ="btnExport_Click"   Text="Export Grids" />
                      &nbsp; 
                     <asp:Button ID="btnChildrenDetWebex" runat="server" Visible="false" OnClick ="btnExportWebex_Click"   Text="Export Webex Format" />
                 
                  </td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td>
            <table id="TblSearchString" visible=false runat="server" width=100%>
               <tr>
                  <td >
                     <asp:label id="txtSearchCriteria"  runat="server" Visible="False" ForeColor="blue">
                     </asp:label>
                  </td>
               </tr>
            </table>
            <table id="tblMessage"  visible=false runat="server" width=65%>
               <tr>
                  <td>
                     <asp:Label id="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                     <asp:Label id="lblMsg" runat="server" Visible="False" ForeColor="blue"></asp:Label>
                     <asp:Label ID="lblCoachMsg" ForeColor="Red"  runat="server" Visible = "false"></asp:Label>
                     <br />
                     <p></p>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td align="center">
            <table id="tblviewchldgrid" border="1" runat = "server" visible = "false" >
            <tr><td><asp:Label ID="lblCoaching" runat="server" Text="Contact List" ForeColor="Green"></asp:Label></td></tr>
               <tr>
                  <td>
                     <asp:GridView ID="GVCoaching" runat="server" AutoGenerateColumns="False" 
                       CellPadding="4" ForeColor="#333333" GridLines="None" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                           BorderColor="#336666" >
                      
                       
                        
                        <AlternatingRowStyle backcolor="WhiteSmoke" font-size="small"></AlternatingRowStyle>
                           <RowStyle backcolor="white" Wrap="False" Font-Size="Small" ></RowStyle>
                           <HeaderStyle  BorderStyle="Solid" Font-Bold="True" backcolor="White" ForeColor="Black"></HeaderStyle>
                           <FooterStyle BackColor="Gainsboro" ></FooterStyle>
                       
                     <%--  
                        <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#FFFBD6" ForeColor="Black" Font-Bold="True"  ></HeaderStyle>
                       <AlternatingRowStyle BackColor="White" />
                       <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />--%>
                         <Columns>
                           <asp:BoundField HeaderText="UUID" ItemStyle-HorizontalAlign ="center" DataField="UUID" HeaderStyle-Font-Bold="true" />
                           <asp:BoundField HeaderText="SNo" ItemStyle-HorizontalAlign ="center" Visible="false" DataField="SNo" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="ChildName" HeaderStyle-Font-Bold="true"  DataField="ChildName" />
                           <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="Email" DataField="Email" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="Parent_Name" DataField="FatherName" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="JobTitle" DataField="JobTitle" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Grade" DataField="Grade" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Approved" DataField="approved" ItemStyle-HorizontalAlign ="center"  HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="HomePhone" DataField="HPhone" HeaderStyle-Font-Bold="true"   />
                           <asp:BoundField HeaderText="CPhone" DataField="CPhone" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Address1" DataField="Address1" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="City" DataField="City" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="State" DataField="State" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Zip" DataField="zip" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Product_Name" DataField="ProductName" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="CoachName" DataField="CoachName" Visible="false"  />
                           <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Session" DataField="SessionNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="MaxCapacity" DataField="MaxCapacity"  Visible="false"  />
                           <asp:BoundField HeaderText="CoachDay" DataField="Day"  />
                           <asp:BoundField HeaderText="Time" DataField="Time"  />
                           <asp:BoundField HeaderText="PaymentReference" DataField="PaymentReference" Visible ="false"  />
                           <asp:BoundField HeaderText="PaymentDate" DataField="PaymentDate" Visible ="true" DataFormatString="{0:d}"   HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Status" DataField="Status"  Visible="false"  />
                           <asp:BoundField HeaderText="ChildNumber" DataField="ChildNumber" Visible="false"  />
                           <asp:BoundField HeaderText="CoachRegID" DataField="CoachRegID"  Visible="false"  />
                           <asp:BoundField HeaderText="SignUpID" DataField="SignUpID"  Visible="false" />
                           <asp:BoundField HeaderText="UserId" DataField="childEmail" Visible ="true"   HeaderStyle-Font-Bold="true"  />
                            <asp:BoundField HeaderText="Pwd" DataField="pwd" Visible ="true"   HeaderStyle-Font-Bold="true"  />
                        </Columns>
                         <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <PagerStyle Wrap="False"></PagerStyle>
                     </asp:GridView>
                  </td>
               </tr>
               <tr><td><asp:Label ID="lblDistList" runat="server" Text="Distribution List" ForeColor="Green"></asp:Label></td></tr>
               <tr>
               <td>
               <asp:GridView ID="GVDistributionList" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" >
                            <AlternatingRowStyle backcolor="WhiteSmoke" font-size="small"></AlternatingRowStyle>
                           <RowStyle backcolor="white" Wrap="False" Font-Size="Small" ></RowStyle>
                           <HeaderStyle  BorderStyle="Solid" Font-Bold="True" backcolor="White" ForeColor="Black"></HeaderStyle>
                           <FooterStyle BackColor="Gainsboro" ></FooterStyle>
                    <Columns>
                           <asp:BoundField HeaderText="DUID" ItemStyle-HorizontalAlign ="center" DataField="DUID" HeaderStyle-Font-Bold="true"  />
                          <%-- <asp:BoundField HeaderText="SNo" ItemStyle-HorizontalAlign ="center" DataField="SNo" HeaderStyle-Font-Bold="true"  Visible="false"  />--%> 
                          <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="Distribution list name"   DataField="ListName" HeaderStyle-Font-Bold="true" />
                           <asp:BoundField HeaderText="Distribution list Description" DataField="ListDescription" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Members" DataField="Members" HeaderStyle-Font-Bold="true"  />
                           
                         </Columns>
               </asp:GridView>
               </td>
               </tr>
            </table>
            <table id="tblVolRoleResults" visible=false runat="server" width="100%">
               <tr>
                  <td align="left" >
                     <strong>
                        <asp:Label ID="lblGrdVolResultsHdg" Visible=true runat=server></asp:Label>
                        <br />
                        <asp:Label id="lblUpdateError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                     </strong>
                  </td>
               </tr>
               <tr>
                  <td align="right">
                      <asp:button id="btn_ShowUniqueRecords"  Width="180px" runat="server" Text="Show Unique Records" Visible="false"></asp:button>
                      <asp:button id="btn_ExportUniqueList"  Width="180px" runat="server" Text="Export Unique Contact List" Visible="false"></asp:button>
                      <asp:button id="btnExport2" Visible="false"   Width="180px" runat="server" Text="Export  Addresses to Excel"></asp:button>
                     <asp:button id="btnExport1"  Width="180px" runat="server" Text="Export Contacts to Excel"></asp:button>
                     <asp:button id="btnEmailExport" runat="server" Text="Export Emails"></asp:button>
                  </td>
               </tr>
               
               <tr>
               <td>
               <asp:Panel ID="Panel_Unique" runat="server">
                        <asp:DataGrid ID="DG_Unique" runat="server" CssClass="announcement_text" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="MemberID"
                           Height="14px" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                           BorderColor="#336666" AllowPaging="true" PageSize="50"  PagerStyle-Mode ="NumericPages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small">
                           <AlternatingItemStyle backcolor="WhiteSmoke" font-size="small"></AlternatingItemStyle>
                           <ItemStyle backcolor="white" Wrap="False" Font-Size="Small" ></ItemStyle>
                           <HeaderStyle  BorderStyle="Solid" Font-Size="Small" Font-Bold="True" backcolor="Gainsboro"></HeaderStyle>
                           <FooterStyle BackColor="Gainsboro" ></FooterStyle>
                           <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                           <Columns>
                              <asp:BoundColumn DataField="memberID" HeaderText="Member Id" readonly=true  Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="name"  headerText="Member Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <%--<asp:TemplateColumn HeaderText="RoleCode" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Selection")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRoleId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RoleId")%>' />
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                              <asp:BoundColumn DataField="Email" headerText="Email" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn  ItemStyle-Wrap=false DataField="HPhone"  headerText="Home Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn ItemStyle-Wrap=false DataField="CPhone" headerText="Cell Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                             <%-- <asp:TemplateColumn HeaderText="TL" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblTeamLead" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeamLead")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                              <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <%--<asp:TemplateColumn HeaderText="Year" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                              <%--<asp:TemplateColumn HeaderText="Product Group" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductGroupId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label>
                                    <asp:Label ID="lblProductGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>'></asp:Label>
                                    <asp:Label ID="lblProduct" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                              <asp:TemplateColumn HeaderText="State" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Chapter" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="YahooGroup" Visible="false" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:BoundColumn ItemStyle-Wrap="false" DataField="WPhone" headerText="Work Phone" ReadOnly="true" Visible="true" ></asp:BoundColumn>
                              <%--<asp:BoundColumn DataField="national"  HeaderText="National" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="finals" HeaderText="finals" readonly="true"  Visible="false" ></asp:BoundColumn>--%>
                              <asp:BoundColumn DataField="zoneId" headerText="zoneId" ReadOnly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="clusterId"  HeaderText="clusterId" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="chapterId" HeaderText="Chapter Id" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="eventId" headerText="eventId" ReadOnly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="indiaChapter" headerText="India Chapter" ReadOnly="true" Visible="false" ></asp:BoundColumn  >
                           </Columns>
                        </asp:DataGrid>
                        &nbsp;
                     </asp:Panel>
               </td>
               </tr>
               <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
                  <td colspan="2" class="ContentSubTitle" align="center" style="height: 103px" >
                     <asp:Panel ID="Panel1" runat=server>
                        <asp:DataGrid ID="grdVolResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="volunteerid"
                           Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                           BorderColor="#336666" AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
                           <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
                           <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>
                           <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
                           <FooterStyle BackColor="Gainsboro" ></FooterStyle>
                           <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                           <Columns>
                              <asp:BoundColumn DataField="memberID" HeaderText="Member Id" readonly=true  Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="name"  headerText="Member Name" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:TemplateColumn HeaderText="RoleCode" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Selection")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRoleId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RoleId")%>' />
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:BoundColumn DataField="Email" headerText="Email" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn  ItemStyle-Wrap=false DataField="HPhone"  headerText="Home Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn ItemStyle-Wrap=false DataField="CPhone" headerText="Cell Phone" ReadOnly=true Visible="true" ></asp:BoundColumn>
                              <asp:TemplateColumn HeaderText="TL" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblTeamLead" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeamLead")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Year" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Product Group" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductGroupId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label>
                                    <asp:Label ID="lblProductGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>'></asp:Label>
                                    <asp:Label ID="lblProduct" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="State" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Chapter" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="YahooGroup" Visible="false" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:BoundColumn ItemStyle-Wrap="false" DataField="WPhone" headerText="Work Phone" ReadOnly="true" Visible="true" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="national"  HeaderText="National" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="finals" HeaderText="finals" readonly="true"  Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="zoneId" headerText="zoneId" ReadOnly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="clusterId"  HeaderText="clusterId" readonly="true" Visible="false" ></asp:BoundColumn>
                              
                              <asp:BoundColumn DataField="chapterId" HeaderText="Chapter Id" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="eventId" headerText="eventId" ReadOnly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="indiaChapter" headerText="India Chapter" ReadOnly="true" Visible="false" ></asp:BoundColumn  >
                           </Columns>
                        </asp:DataGrid>
                        &nbsp;
                     </asp:Panel>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
</asp:Content>

