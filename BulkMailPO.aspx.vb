﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class BulkMailPO
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Not Session("RoleId").ToString() = "1" Then
                Response.Redirect("~/VolunteerFunctions.aspx")
            End If
        End If
    End Sub
    '******************************************************************************************************************************************************************************************************************************************
    '******************************************************************************************************************************************************************************************************************************************
    'Select T1.AutomemberID as AutomemberID, T1.PubName, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,Convert(int,SUM(T1.DonationAmount)) as DonationAmount, 'D' as StatusType
    '      FROM (( Select I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(N.Fee * 0.6667) as DonationAmount from IndSpouse I,NFG_Transactions N 
    '      WHERE N.[Payment Date] <=isnull(@Dates ,GetDate()) AND I.AutoMemberID = N.MemberId AND N.Fee>0 Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email) Union All Select 
    '      I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(D.Amount) as DonationAmount from IndSpouse I,donationsinfo D WHERE D.[DonationDate] <=isnull(@Dates ,GetDate()) 
    '      AND I.AutoMemberID = Case WHEN D.DonorType = 'IND' THEN D.MemberId ELSE dbo.ufn_getIndMemberid(D.MemberID) End Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, 
    '      I.CPhone, I.Email)T1 Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone,
    '      T1.CPhone, T1.Email Having SUM(T1.DonationAmount) >= @DonationAmount
    '
    '      Union --Volunteer
    '      Select I.AutomemberID as AutomemberID, I.PubName, I.FirstName, I.LastName,I2.FirstName as SpFirstName,I2.LastName as SpLastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount , 'V' as StatusType
    '      from IndSpouse I1 Inner Join Volunteer V ON V.MemberId = I1.AutoMemberID INNER JOIN IndSpouse I ON I.AutoMemberID = Case WHEN I1.DonorType = 'IND' THEN  I1.AutoMemberId ELSE  I1.Relationship  End  
    '      LEFT JOIN IndSpouse I2 ON I.AutoMemberID = I2.Relationship 
    '      Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName,I2.FirstName,I2.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email
    '
    '       Union - Welwisher
    '       Select I.AutomemberID as AutomemberID, I.PubName, I.FirstName, I.LastName,I2.FirstName as SpFirstName,I2.LastName as SpLastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount, 'W' as StatusType 
    '      from IndSpouse I LEFT JOIN IndSpouse I2 ON I.AutoMemberID = I2.Relationship  WHERE  I.DonorType = 'IND' AND I.Mailinglabel like '%Y%'
    '******************************************************************************************************************************************************************************************************************************************
    '******************************************************************************************************************************************************************************************************************************************
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Datecntn As String
        'If txtFrmDate.Text.Length > 0 And txtToDate.Text.Length > 0 Then
        '    Datecntn = "Between '" & txtFrmDate.Text & "' and '" & txtToDate.Text & "'"
        'ElseIf txtFrmDate.Text.Length > 0 Then
        'Datecntn = " >='" & txtFrmDate.Text & "'"
        'Else
        If txtToDate.Text.Length > 0 Then
            Datecntn = " <='" & txtToDate.Text & "'"
        Else
            Datecntn = " <=GetDate()"
        End If

        Dim strSQL As String
        Dim ds As DataSet
        
        Try
            'For Donors
            lblVErr.Text = String.Empty
            strSQL = "Select Top 500 T1.AutomemberID, Case when T1.PubName IS null then Case when T1.Gender = 'Male' Then Case when I1.FirstName is not null then I1.FirstName + ' & '+T1.FirstName + ' ' + T1.LastName  Else T1.FirstName + ' ' + T1.LastName End Else Case when I1.FirstName is not null then T1.FirstName + ' & '+I1.FirstName + ' ' + I1.LastName  Else T1.FirstName + ' ' + T1.LastName End End Else T1.PubName End as Pubname, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'D' as StatusType "
            strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
            strSQL = strSQL & " FROM (("
            strSQL = strSQL & " Select I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(N.Fee * 0.6667) as DonationAmount from  IndSpouse I,NFG_Transactions N WHERE N.[Payment Date] " & Datecntn & " AND I.AutoMemberID = N.MemberId AND N.Fee>0 "
            strSQL = strSQL & " Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,I.Gender)"
            strSQL = strSQL & " Union All Select I.AutomemberID,I.Gender , I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(D.Amount) as DonationAmount from  IndSpouse I,donationsinfo D WHERE D.[DonationDate] " & Datecntn & " AND D.donortype <> 'OWN' AND  I.AutoMemberID = Case WHEN D.DonorType = 'IND' THEN  D.MemberId ELSE  dbo.ufn_getIndMemberid(D.MemberID) End"
            strSQL = strSQL & " Group By I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email, I.Gender"
            strSQL = strSQL & ")T1  Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = T1.AutoMemberID  Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I1.AutoMemberID  Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,T1.Gender,V1.Roleid,V.RoleID" ',T1.DonationAmount"
            strSQL = strSQL & " Having SUM(T1.DonationAmount) >= " & txtAmt.Text
            strSQL = strSQL & " Order By T1.AutomemberID" 'sum(T1.DonationAmount) Desc"
            'Response.Write(strSQL & "<br>")
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                GridMemberDt.DataSource = ds
                GridMemberDt.DataBind()
                GridMemberDt.Visible = True
                lblVErr.Text = ""
                btnExport.Enabled = True
                btnExportAll.Enabled = True
            Else
                lblVErr.Text = lblVErr.Text & "<br> Sorry No Donor Data to Display"
                GridMemberDt.Visible = False
                btnExport.Enabled = False
                btnExportAll.Enabled = False
            End If

            'Volunteer *' ***Top 500

            strSQL = "Select Distinct T1.AutomemberID,Case when T1.PubName IS null then Case when T1.Gender = 'Male' Then Case when I1.FirstName is not null then I1.FirstName + ' & '+T1.FirstName + ' ' + T1.LastName  Else T1.FirstName + ' ' + T1.LastName End Else Case when I1.FirstName is not null then T1.FirstName + ' & '+I1.FirstName + ' ' + I1.LastName  Else T1.FirstName + ' ' + T1.LastName End End Else T1.PubName End as Pubname, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'V' as StatusType "
            strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
            strSQL = strSQL & " FROM ("
            strSQL = strSQL & " Select I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount "
            strSQL = strSQL & " from IndSpouse I,IndSpouse I1 ,Volunteer V WHERE V.MemberId = I1.AutoMemberID AND  I.AutoMemberID = Case WHEN I1.DonorType = 'IND' THEN  I1.AutoMemberId ELSE  I1.Relationship  End "
            strSQL = strSQL & " Group By I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email"
            strSQL = strSQL & ")T1  Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = T1.AutoMemberID  Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I1.AutoMemberID Where (T1.Zip is not null Or T1.Zip <>0) Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,T1.Gender,V1.Roleid,V.RoleID"
            strSQL = strSQL & " Order By T1.AutomemberID "
            'Response.Write(strSQL & "<br>")
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                GridMemberDt1.DataSource = ds
                GridMemberDt1.DataBind()
                GridMemberDt1.Visible = True
                Label1.Visible = True
                lblVErr.Text = ""
                btnExport1.Enabled = True
                btnExportAll.Enabled = True
            Else
                lblVErr.Text = lblVErr.Text & "<br> Sorry No Vlunteer  Data to Display"
                GridMemberDt1.Visible = False
                Label1.Visible = False
                btnExport1.Enabled = False
                btnExportAll.Enabled = False
            End If

            'WelWishers

            strSQL = "Select Top 500 I.AutomemberID as AutomemberID, Case when I.PubName IS null then Case when I.Gender = 'Male' Then Case when I2.FirstName is not null then I2.FirstName + ' & '+I.FirstName + ' ' + I.LastName  Else I.FirstName + ' ' + I.LastName End Else Case when I2.FirstName is not null then I.FirstName + ' & '+I2.FirstName + ' ' + I2.LastName  Else I.FirstName + ' ' + I.LastName End End Else I.PubName End as Pubname, I.FirstName, I.LastName,I2.FirstName as SpFirstName,I2.LastName as SpLastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount, 'W' as StatusType "
            strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
            strSQL = strSQL & " from IndSpouse I Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = I.AutoMemberID LEFT JOIN IndSpouse I2 ON I.AutoMemberID = I2.Relationship Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I2.AutoMemberID  WHERE  I.DonorType = 'IND' AND I.Mailinglabel like '%Y%' "
            'Response.Write(strSQL & "<br>")
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                GridMemberDt2.DataSource = ds
                GridMemberDt2.DataBind()
                GridMemberDt2.Visible = True
                Label2.Visible = True
                lblVErr.Text = ""
                btnExport2.Enabled = True
            Else
                lblVErr.Text = lblVErr.Text & "<br> Sorry NO Well Wisher Data to Display"
                GridMemberDt2.Visible = False
                Label2.Visible = False
                btnExport2.Enabled = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
            lblVErr.Text = ex.ToString() ' "Please Enter Amount"
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSQL, Datecntn As String
        Dim ds As DataSet
        Dim Gv As New GridView
        If txtToDate.Text.Length > 0 Then
            Datecntn = " <='" & txtToDate.Text & "'"
        Else
            Datecntn = " <=GetDate()"
        End If
        'strSQL = "Select  T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'D' as StatusType FROM (("
        'strSQL = strSQL & " Select I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(N.Fee * 0.6667) as DonationAmount from  IndSpouse I,NFG_Transactions N WHERE N.[Payment Date] " & Datecntn & " AND I.AutoMemberID = N.MemberId AND N.Fee>0 "
        'strSQL = strSQL & " Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email)"
        'strSQL = strSQL & " Union All Select I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(D.Amount) as DonationAmount from  IndSpouse I,donationsinfo D WHERE D.[DonationDate] " & Datecntn & " AND D.donortype <> 'OWN'  AND  I.AutoMemberID = Case WHEN D.DonorType = 'IND' THEN  D.MemberId ELSE  dbo.ufn_getIndMemberid(D.MemberID) End"
        'strSQL = strSQL & " Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email"
        'strSQL = strSQL & ")T1 Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID  Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email"
        'strSQL = strSQL & " Having SUM(T1.DonationAmount) >= " & txtAmt.Text
        'strSQL = strSQL & " Order By DonationAmount Desc"
        strSQL = "Select T1.AutomemberID, Case when T1.PubName IS null then Case when T1.Gender = 'Male' Then Case when I1.FirstName is not null then I1.FirstName + ' & '+T1.FirstName + ' ' + T1.LastName  Else T1.FirstName + ' ' + T1.LastName End Else Case when I1.FirstName is not null then T1.FirstName + ' & '+I1.FirstName + ' ' + I1.LastName  Else T1.FirstName + ' ' + T1.LastName End End Else T1.PubName End as Pubname, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'D' as StatusType "
        strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
        strSQL = strSQL & " FROM (("
        strSQL = strSQL & " Select I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(N.Fee * 0.6667) as DonationAmount from  IndSpouse I,NFG_Transactions N WHERE N.[Payment Date] " & Datecntn & " AND I.AutoMemberID = N.MemberId AND N.Fee>0 "
        strSQL = strSQL & " Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,I.Gender)"
        strSQL = strSQL & " Union All Select I.AutomemberID,I.Gender , I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,SUM(D.Amount) as DonationAmount from  IndSpouse I,donationsinfo D WHERE D.[DonationDate] " & Datecntn & " AND D.donortype <> 'OWN' AND  I.AutoMemberID = Case WHEN D.DonorType = 'IND' THEN  D.MemberId ELSE  dbo.ufn_getIndMemberid(D.MemberID) End"
        strSQL = strSQL & " Group By I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email, I.Gender"
        strSQL = strSQL & ")T1  Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = T1.AutoMemberID  Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I1.AutoMemberID  Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,T1.Gender,V1.Roleid,V.RoleID"
        strSQL = strSQL & " Having SUM(T1.DonationAmount) >= " & txtAmt.Text
        strSQL = strSQL & " Order By T1.AutomemberID" 'T1.DonationAmount Desc"

        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                Gv.DataSource = ds
                Gv.DataBind()
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=BulkMailPODonor.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                Gv.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Else
                lblVErr.Text = "Sorry No Data to Display"
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(strSQL)
        End Try

       
    End Sub

    Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSQL As String
        Dim ds As DataSet
        Dim Gv As New GridView
        'strSQL = "Select T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'V' as StatusType FROM ("
        'strSQL = strSQL & " Select I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount "
        'strSQL = strSQL & " from IndSpouse I,IndSpouse I1 ,Volunteer V WHERE V.MemberId = I1.AutoMemberID AND  I.AutoMemberID = Case WHEN I1.DonorType = 'IND' THEN  I1.AutoMemberId ELSE  I1.Relationship  End "
        'strSQL = strSQL & " Group By I.AutomemberID, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email"
        'strSQL = strSQL & ")T1    Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID  Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email"
        'strSQL = strSQL & " Order By T1.AutomemberID "
        strSQL = "Select T1.AutomemberID,Case when T1.PubName IS null then Case when T1.Gender = 'Male' Then Case when I1.FirstName is not null then I1.FirstName + ' & '+T1.FirstName + ' ' + T1.LastName  Else T1.FirstName + ' ' + T1.LastName End Else Case when I1.FirstName is not null then T1.FirstName + ' & '+I1.FirstName + ' ' + I1.LastName  Else T1.FirstName + ' ' + T1.LastName End End Else T1.PubName End as Pubname, T1.FirstName, T1.LastName, I1.FirstName as SpFirstName,I1.LastName as SpLastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,SUM(T1.DonationAmount) as DonationAmount,'V' as StatusType "
        strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
        strSQL = strSQL & " FROM ("
        strSQL = strSQL & " Select I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount "
        strSQL = strSQL & " from IndSpouse I,IndSpouse I1 ,Volunteer V WHERE V.MemberId = I1.AutoMemberID AND  I.AutoMemberID = Case WHEN I1.DonorType = 'IND' THEN  I1.AutoMemberId ELSE  I1.Relationship  End "
        strSQL = strSQL & " Group By I.AutomemberID,I.Gender, I.PubName, I.FirstName, I.LastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email"
        strSQL = strSQL & ")T1  Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = T1.AutoMemberID  Left JOIN IndSpouse I1 ON I1.Relationship = T1.AutoMemberID Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I1.AutoMemberID Group By T1.AutomemberID, T1.PubName, T1.FirstName, T1.LastName,I1.FirstName,I1.LastName, T1.Address1, T1.City, T1.State, T1.zip, T1.HPhone, T1.CPhone, T1.Email,T1.Gender,V1.Roleid,V.RoleID"
        strSQL = strSQL & " Order By T1.AutomemberID "
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                Gv.DataSource = ds
                Gv.DataBind()
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=BulkMailPOVolunteer.xls")
                Response.Charset = ""
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                Gv.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Else
                lblVErr.Text = "Sorry No Data to Display"
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(strSQL)
        End Try
    End Sub

    Protected Sub btnExport2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSQL As String
        Dim ds As DataSet
        Dim Gv As New GridView
        'strSQL = "Select Top 100 I.AutomemberID as AutomemberID, I.PubName, I.FirstName, I.LastName,I2.FirstName as SpFirstName,I2.LastName as SpLastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount, 'W' as StatusType "
        'strSQL = strSQL & " from IndSpouse I LEFT JOIN IndSpouse I2 ON I.AutoMemberID = I2.Relationship  WHERE  I.DonorType = 'IND' AND I.Mailinglabel like '%Y%' "
        strSQL = "Select I.AutomemberID as AutomemberID, Case when I.PubName IS null then Case when I.Gender = 'Male' Then Case when I2.FirstName is not null then I2.FirstName + ' & '+I.FirstName + ' ' + I.LastName  Else I.FirstName + ' ' + I.LastName End Else Case when I2.FirstName is not null then I.FirstName + ' & '+I2.FirstName + ' ' + I2.LastName  Else I.FirstName + ' ' + I.LastName End End Else I.PubName End as Pubname, I.FirstName, I.LastName,I2.FirstName as SpFirstName,I2.LastName as SpLastName, I.Address1, I.City, I.State, I.zip, I.HPhone, I.CPhone, I.Email,0 as DonationAmount, 'W' as StatusType "
        strSQL = strSQL & ", CASE WHEN V1.Roleid IS NULL THEN V.RoleID ELSE CASE WHEN V.RoleID IS NULL THEN V1.Roleid ELSE CASE WHEN V.RoleID < V1.Roleid THEN V.RoleID ELSE V1.Roleid END END END as RoleID "
        strSQL = strSQL & " from IndSpouse I Left Join  (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V1 ON V1.MemberId = I.AutoMemberID LEFT JOIN IndSpouse I2 ON I.AutoMemberID = I2.Relationship Left Join (select MIN(Roleid) as RoleID,Memberid from Volunteer Group by MemberID ) V ON V.MemberId = I2.AutoMemberID  WHERE  I.DonorType = 'IND' AND I.Mailinglabel like '%Y%' "

        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                Gv.DataSource = ds
                Gv.DataBind()
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=BulkMailPOWellWisher.xls")
                Response.Charset = ""
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                Gv.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Else
                lblVErr.Text = "Sorry No Data to Display"
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(strSQL)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnExportAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=BulkMailPO.xls")
        Response.Charset = ""
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim Gv As New GridView
        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter("@DonationAmount", txtAmt.Text)
        prmArray(1) = New SqlParameter("@Dates", txtToDate.Text)
        Dim ds As DataSet
        If txtToDate.Text.Length > 0 Then
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetBULKMAIL", prmArray)
        Else
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetBULKMAIL", New SqlParameter("@DonationAmount", txtAmt.Text))
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            Gv.DataSource = ds
            Gv.DataBind()
        Else
            lblVErr.Text = "Sorry No Data to Display"
            Exit Sub
        End If
        Gv.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
End Class
