﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ChildAssessmentForm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            Loadchild(Session("CustIndID"))
            'pnl.visible = false
        End If
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = " select Ch.First_name+' '+Ch.Last_name as Name, Ch.ChildNumber from  Child Ch, AssessExam AE  WHERE"
        strSql = strSql & " ch.Grade BETWEEN AE.GradeFrom AND AE.GradeTo and  DAtediff(d,AE.StartDate,GetDate())>= 0 AND  DAtediff(d,GetDate(),AE.EndDate)>=0  AND Ch.MEMBERID = " & ParentID
        strSql = strSql & " UNION select Ch.First_name+' '+Ch.Last_name as Name, Ch.ChildNumber from  Child Ch, AssessExamResp AR  WHERE AR.ChildNumber = Ch.ChildNumber   AND Ch.MEMBERID = " & ParentID
        strSql = strSql & " GROUP BY Ch.First_name+' '+Ch.Last_name , Ch.ChildNumber"
        Dim drChild As SqlDataReader
        drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        ddlChild.Enabled = True
        If ddlChild.Items.Count > 0 Then
            LoadProduct()
        Else
            ddlChild.Enabled = False
            divid1.Visible = False
            lblError.Text = "No assessment exam is available at this time. Assessment exam is by invitation only."
        End If

        'ddlChild.Items(0).Selected = True
    End Sub

    Private Sub LoadProduct()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select P.Name as ProductName,P.ProductID from  Child Ch, AssessExam AE"
        SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = AE.ProductID"
        SQLStr = SQLStr & " WHERE ch.Grade BETWEEN AE.GradeFrom AND AE.GradeTo and  DATEDIFF(d,AE.StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),AE.EndDate)>=0  AND Ch.ChildNumber = " & ddlChild.SelectedValue & " Group by P.ProductId,P.Name "
        Dim drProduct As SqlDataReader
        Try
            drProduct = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            ddlProduct.DataSource = drProduct
            ddlProduct.DataBind()
            If ddlProduct.Items.Count = 1 Then
                ddlProduct.Enabled = False
                lblerr.Text = ""
            ElseIf ddlProduct.Items.Count > 0 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
                lblerr.Text = ""
            Else
                lblerr.ForeColor = Color.Red
                lblerr.Text = "No Eligible Exam Available now. For Results please click on Answer Key"
            End If
        Catch ex As Exception
            lblerr.Text = SQLStr
        End Try
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DivID3.Visible = False
        LoadProduct()
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        DivID3.Visible = False
        If ddlProduct.Items.Count > 0 Then
            If CheckData() Then
                ddlChild.Enabled = False
                btnContinue.Enabled = False
                DivID2.Visible = True
            Else
                lblerr.Text = "You have already finished your Assessment Exam"
            End If
        Else
            lblerr.Text = "Sorry, No Eligible Exam available for you"
        End If
    End Sub

    Function CheckData() As Boolean
        cleardiv2()
        Dim flag As Boolean = True
        Try
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(AR.AssessExRespID) from AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID where AR.ChildNumber = " & ddlChild.SelectedValue & " AND AR.ProductID = " & ddlProduct.SelectedValue & " AND DATEDIFF(d,AE.StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),AE.EndDate)>=0 ") > 0 Then
                flag = False
                Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select AR.* from AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID where AR.ChildNumber = " & ddlChild.SelectedValue & " AND AR.ProductID = " & ddlProduct.SelectedValue & " AND DATEDIFF(d,AE.StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),AE.EndDate)>=0 AND DATEDIFF(hh,AR.CreateDate ,GetDate()) < 25")
                While Readr.Read()
                    txtQ1.Text = Readr("Q1")
                    txtQ2.Text = Readr("Q2")
                    txtQ3.Text = Readr("Q3")
                    txtQ4.Text = Readr("Q4")
                    txtQ5.Text = Readr("Q5")
                    txtQ6.Text = Readr("Q6")
                    txtQ7.Text = Readr("Q7")
                    txtQ8.Text = Readr("Q8")
                    txtQ9.Text = Readr("Q9")
                    txtQ10.Text = Readr("Q10")
                    lblAssessExRespID.Text = Readr("AssessExRespID")
                    btnSubmit.Text = "Modifiy"
                    flag = True
                End While
            Else
                flag = True
            End If
        Catch ex As Exception
            lblerr.Text = ex.ToString()
        End Try
        Return flag
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtQ1.Text = "" Then
            lblerr.Text = "Please Enter Q1"
            Exit Sub
        ElseIf txtQ2.Text = "" Then
            lblerr.Text = "Please Enter Q2"
            Exit Sub
        ElseIf txtQ3.Text = "" Then
            lblerr.Text = "Please Enter Q3"
            Exit Sub
        ElseIf txtQ4.Text = "" Then
            lblerr.Text = "Please Enter Q4"
            Exit Sub
        ElseIf txtQ5.Text = "" Then
            lblerr.Text = "Please Enter Q5"
            Exit Sub
        ElseIf txtQ6.Text = "" Then
            lblerr.Text = "Please Enter Q6"
            Exit Sub
        ElseIf txtQ7.Text = "" Then
            lblerr.Text = "Please Enter Q7"
            Exit Sub
        ElseIf txtQ8.Text = "" Then
            lblerr.Text = "Please Enter Q8"
            Exit Sub
        ElseIf txtQ9.Text = "" Then
            lblerr.Text = "Please Enter Q9"
            Exit Sub
        ElseIf txtQ10.Text = "" Then
            lblerr.Text = "Please Enter Q10"
            Exit Sub
        End If
        Dim StrSQl As String
        Try

            If btnSubmit.Text = "Save" Then
                Dim Grade, AssessExamID As Integer
                Dim ProductCode As String
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select AE.AssessExamID,AE.ProductCode,ch.Grade from  Child Ch, AssessExam AE WHERE ch.Grade BETWEEN AE.GradeFrom AND AE.GradeTo and  DATEDIFF(d,AE.StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),AE.EndDate)>=0  AND Ch.ChildNumber = " & ddlChild.SelectedValue & " AND AE.ProductId=" & ddlProduct.SelectedValue & "")
                While readr.Read()
                    Grade = readr("Grade")
                    AssessExamID = readr("AssessExamID")
                    ProductCode = readr("ProductCode")
                End While
                readr.Close()
                StrSQl = "INSERT INTO AssessExamResp(AssessExamID, ChapterID, MemberID, ChildNumber, Grade, ProductID, ProductCode, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10,CreateDate, CreatedBy) VALUES ("
                StrSQl = StrSQl & AssessExamID & "," & Session("CustIndChapterID") & "," & Session("CustIndID") & "," & ddlChild.SelectedValue & "," & Grade & "," & ddlProduct.SelectedValue & ",'" & ProductCode & "','" & txtQ1.Text & "','" & txtQ2.Text & "','" & txtQ3.Text & "','" & txtQ4.Text & "','" & txtQ5.Text & "','" & txtQ6.Text & "','" & txtQ7.Text & "','" & txtQ8.Text & "','" & txtQ9.Text & "','" & txtQ10.Text & "', Getdate()," & Session("LoginID") & ")"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                cleardiv2()
                lblerr.Text = "Inserted Successfully"
            Else
                StrSQl = "UPDATE AssessExamResp SET Q1='" & txtQ1.Text & "', Q2='" & txtQ2.Text & "', Q3='" & txtQ3.Text & "', Q4='" & txtQ4.Text & "', Q5='" & txtQ5.Text & "', Q6='" & txtQ6.Text & "', Q7='" & txtQ7.Text & "', Q8='" & txtQ8.Text & "', Q9='" & txtQ9.Text & "', Q10='" & txtQ10.Text & "',ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE AssessExRespID=" & lblAssessExRespID.Text
                StrSQl = StrSQl & ""
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                cleardiv2()
                lblerr.Text = "Updated Successfully"
            End If
        Catch ex As Exception
            lblerr.Text = ex.ToString()
        End Try
        ddlChild.Enabled = True
        btnContinue.Enabled = True
        DivID2.Visible = False
    End Sub

    Private Sub cleardiv2()
        txtQ1.Text = String.Empty
        txtQ2.Text = String.Empty
        txtQ3.Text = String.Empty
        txtQ4.Text = String.Empty
        txtQ5.Text = String.Empty
        txtQ6.Text = String.Empty
        txtQ7.Text = String.Empty
        txtQ8.Text = String.Empty
        txtQ9.Text = String.Empty
        txtQ10.Text = String.Empty
        lblAssessExRespID.Text = ""
        lblerr.Text = ""
        btnSubmit.Text = "Save"
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cleardiv2()
        ddlChild.Enabled = True
        btnContinue.Enabled = True
        DivID2.Visible = False
    End Sub

    Protected Sub btnAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cleardiv2()
        ddlChild.Enabled = True
        btnContinue.Enabled = True
        DivID2.Visible = False
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(AR.AssessExRespID)  from AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID  WHERE DATEDIFF(d,AE.EndDate,GETDATE())>0 AND ChildNumber = " & ddlChild.SelectedValue & "") < 1 Then
            lblerr.Text = "Results cannot be shared, since the deadline has not yet passed."
        Else
            Dim dt As New DataTable
            Dim dr As DataRow
            dt.Columns.Add("Heading", Type.GetType("System.String"))
            dt.Columns.Add("ProductCode", Type.GetType("System.String"))
            dt.Columns.Add("Q1", Type.GetType("System.String"))
            dt.Columns.Add("Q2", Type.GetType("System.String"))
            dt.Columns.Add("Q3", Type.GetType("System.String"))
            dt.Columns.Add("Q4", Type.GetType("System.String"))
            dt.Columns.Add("Q5", Type.GetType("System.String"))
            dt.Columns.Add("Q6", Type.GetType("System.String"))
            dt.Columns.Add("Q7", Type.GetType("System.String"))
            dt.Columns.Add("Q8", Type.GetType("System.String"))
            dt.Columns.Add("Q9", Type.GetType("System.String"))
            dt.Columns.Add("Q10", Type.GetType("System.String"))
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 3 AR.ProductCode,AR.AssessExamID, AR.Q1, AR.Q2, AR.Q3, AR.Q4, AR.Q5, AR.Q6, AR.Q7, AR.Q8, AR.Q9, AR.Q10  from AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID  WHERE DATEDIFF(d,AE.EndDate,GETDATE())>0 AND ChildNumber = " & ddlChild.SelectedValue & " ORDER BY AE.AssessExamID DESC ")
            While reader.Read()
                dr = dt.NewRow
                dr(0) = "Child Answer"
                dr(1) = reader("ProductCode")
                dr(2) = reader("Q1")
                dr(3) = reader("Q2")
                dr(4) = reader("Q3")
                dr(5) = reader("Q4")
                dr(6) = reader("Q5")
                dr(7) = reader("Q6")
                dr(8) = reader("Q7")
                dr(9) = reader("Q8")
                dr(10) = reader("Q9")
                dr(11) = reader("Q10")
                dt.Rows.Add(dr)
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from AssessExam where AssessExamID=" & reader("AssessExamID") & "")
                While readr.Read()
                    dr = dt.NewRow
                    dr(0) = "Answer Key"
                    dr(1) = readr("ProductCode")
                    dr(2) = readr("Q1")
                    dr(3) = readr("Q2")
                    dr(4) = readr("Q3")
                    dr(5) = readr("Q4")
                    dr(6) = readr("Q5")
                    dr(7) = readr("Q6")
                    dr(8) = readr("Q7")
                    dr(9) = readr("Q8")
                    dr(10) = readr("Q9")
                    dr(11) = readr("Q10")
                    dt.Rows.Add(dr)
                End While
                readr.Close()
            End While
            reader.Close()
            GridResult.DataSource = dt
            GridResult.DataBind()
            If GridResult.Rows.Count > 0 Then
                DivID3.Visible = True
            End If

        End If

    End Sub
End Class
