﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;


public partial class DonateADollar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["PreviousPage"] = "DonateADollar";
        lblValidateMsg.Text = "";
    }

    public string login()
    {

        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        string RetVal = "0";

        try
        {
            if (txtUserId.Text.Trim() != "" && txtPassword.Text.Trim() != "")
            {
                cmdText = "select IP.AutoMemberID,Relationship,DonorType from IndSpouse IP inner join Login_Master LM on (IP.Email=LM.User_Email) where LM.User_Email='" + txtUserId.Text.Trim() + "' and LM.user_pwd='" + txtPassword.Text.Trim() + "' ";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        tblErrorLogin.Visible = false;
                        RetVal = ds.Tables[0].Rows[0]["AutoMemberID"].ToString();
                        Session["LoginID"] = ds.Tables[0].Rows[0]["AutoMemberID"].ToString();
                        Session["LoggedIn"] = "True";
                        if (ds.Tables[0].Rows[0]["DonorType"].ToString() == "IND")
                        {
                            Session["CustIndID"] = ds.Tables[0].Rows[0]["AutoMemberID"].ToString();
                        }
                        else if (ds.Tables[0].Rows[0]["DonorType"].ToString() == "SPOUSE")
                        {
                            Session["CustIndID"] = ds.Tables[0].Rows[0]["Relationship"].ToString();
                        }

                    }
                    else
                    {
                        tblErrorLogin.Visible = true;
                    }
                }
                else
                {
                    tblErrorLogin.Visible = true;
                }
            }

        }
        catch
        {
            tblErrorLogin.Visible = true;
        }
        return RetVal;
    }
    public bool Validate()
    {

        try
        {
            string Response = Request["g-recaptcha-response"];

            //Getting Response String Append to Post Method
            bool Valid = false;
            //Request to Google Server
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(" https://www.google.com/recaptcha/api/siteverify?secret=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ&response=" + Response);

            //Google recaptcha Response
            using (WebResponse wResponse = req.GetResponse())
            {

                using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                {
                    string jsonResponse = readStream.ReadToEnd();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    MyObject data = js.Deserialize<MyObject>(jsonResponse);
                    // Deserialize Json
                    Valid = Convert.ToBoolean(data.success);
                }
            }

            return Valid;
        }
        catch (WebException ex)
        {
            //Response.Write("Error :" & ex.ToString())
            return false;
        }

    }

    public class MyObject
    {
        private string _success;
        public string success
        {
            get { return _success; }
            set { _success = value; }
        }


    }

    protected void btnSaveProceed_Click(object sender, EventArgs e)
    {
        spCaptcha.Visible = false;
        if (validateDonation() == false)
        {
            return;
        }
        if (Validate()==false)
        {
            spCaptcha.Visible = true;
            return;
        }
            string count = login();
            int LoginCount = Convert.ToInt32(count);
            if (LoginCount > 0)
            {                
                    SaveDonation();                
            }
       
    }

    public bool  validateDonation()
    {
        lblValidateMsg.Text = "";
       
        bool isDollorSelected = true;
        if (RbtnOneDollor.Checked == false && RbtnTwoDollor.Checked == false && RbtnFiveDollor.Checked == false && RbtnTenDollor.Checked == false && RbtnTwentyFive.Checked == false && RbtnOther.Checked == false)
        {
            isDollorSelected = false;
        }
        if (txtGreetings.Text.Trim().Length == 0)
        {
            if (isDollorSelected == false)
            {
                if (RbtnNoDonate.Checked == false)
                {
                    lblValidateMsg.Text = "Please provide greetings or select a donation amount";return false;
                }
                else if (RbtnNoDonate.Checked == true)
                {
                    lblValidateMsg.Text = "Please provide greetings"; return false;
                }
            }
        }
        else
        {
            if (RbtnNoDonate.Checked == false && isDollorSelected == false)
            {
                lblValidateMsg.Text = "Please make a selection in the donation section"; return false;
            }
        }
        if (RbtnOther.Checked == true)
        {
            if (TxtOther.Text == "")
            {
                lblValidateMsg.Text = "Please enter amount.";return false;
            }
            else if (Convert.ToInt32(TxtOther.Text) <= 0)
            {
                lblValidateMsg.Text = "Please enter a positive amount.";return false;
            }
            else
            {
                Session["Donation"] = TxtOther.Text;
            }
        }
        return true;
         
        //if (RbtnOneDollor.Checked == false && RbtnTwoDollor.Checked == false && RbtnFiveDollor.Checked == false && RbtnTenDollor.Checked == false && RbtnTwentyFive.Checked == false && RbtnOther.Checked == false)
        //{            
        //    lblValidateMsg.Text = "Please provide greetings or select a donation amount.";return false;
        //}
        //else if (RbtnNoDonate.Checked == true)
        //{
        //    if (txtGreetings.Text == ""){lblValidateMsg.Text = "Please enter your greetings for the 25th Anniversary Celebration.";return false;}
        //}
        //else if (RbtnOther.Checked == true)
        //{
        //    if (TxtOther.Text == "")
        //    {
        //        lblValidateMsg.Text = "Please enter amount.";return false;
        //    }
        //    else if (Convert.ToInt32(TxtOther.Text) <= 0)
        //    {               
        //        lblValidateMsg.Text = "Please enter a positive amount.";return false;
        //    }
        //    else
        //    {
        //        Session["Donation"] = TxtOther.Text;
        //    }
        //}
        //return true;
    }

    protected void RbtnOneDollor_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = "1";
    }
    protected void RbtnTwoDollor_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = "2";
    }
    protected void RbtnFiveDollor_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = "5";
    }
    protected void RbtnTenDollor_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = "10";
    }
    protected void RbtnTwentyFive_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = "25";
    }
    protected void RbtnOther_CheckedChanged(object sender, EventArgs e)
    {
        Session["Donation"] = TxtOther.Text;
    }

    public void SaveDonation()
    {
        Session["entryToken"] = "Parent";

        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        try
        {
            CmdText = "select FR.EventID,FR.EventYear,FR.FundRCalID,FR.ChapterID,FR.EventDate,FR.VenueID,E.Name from FundRaisingCal FR inner join Event E on (FR.EventID=E.EventID) where FR.EventYear='" + DateTime.Now.Year + "' and FR.EventDate=(select max(EventDate) from FundRaisingCal)";
            string EventID = string.Empty;
            string EventCode = string.Empty;
            string EventYear = string.Empty;
            string FundRCalID = string.Empty;
            string ChapterID = string.Empty;
            string EventDate = string.Empty;
            string VenueID = string.Empty;
            string MemberID = string.Empty;
            string Greeting = string.Empty;
            string Amount = string.Empty;

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EventID = ds.Tables[0].Rows[0]["EventID"].ToString();
                    EventYear = ds.Tables[0].Rows[0]["EventYear"].ToString();
                    FundRCalID = ds.Tables[0].Rows[0]["FundRCalID"].ToString();
                    ChapterID = ds.Tables[0].Rows[0]["ChapterID"].ToString();
                    EventDate = ds.Tables[0].Rows[0]["EventDate"].ToString();
                    VenueID = ds.Tables[0].Rows[0]["VenueID"].ToString();
                    MemberID = Session["LoginID"].ToString();
                    Greeting = txtGreetings.Text;
                    if (RbtnNoDonate.Checked == true)
                    {
                        Amount = "null";
                        Greeting = "'" + Greeting + "'";
                    }
                    else
                    {
                        if (Greeting == "")
                        {
                            Greeting = "null";
                        }
                        else
                        {
                            Greeting = "'" + Greeting + "'";
                        }
                        Amount = Session["Donation"].ToString();
                    }
                    EventCode = ds.Tables[0].Rows[0]["Name"].ToString();

                    Session["@EID"] = EventID;
                    Session["@EVE"] = EventCode;
                    Session["@Anonymous1"] = "No";
                    Session["@Access1"] = "";
                    Session["@Matching"] = "No";
                    Session["@inmemory"] = "";
                    Session["DONATIONFOR"] = "";

                    Session["EventId"] = EventID;

                    string cmdGreetings = "insert into Greetings (EventYear, EventID, FundRCalID, ChapterID, EventDate, VenueID, MemberID, Greeting, Amount, Createddate, CreatedBy) values(" + EventYear + "," + EventID + "," + FundRCalID + "," + ChapterID + ",'" + EventDate + "'," + VenueID + "," + MemberID + "," + Greeting + "," + Amount + ",GetDate()," + MemberID + ")";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdGreetings);
                    if (RbtnNoDonate.Checked == false)
                    {
                        Session["DoPay"] = "DonateADollar";
                        Response.Redirect("Donor_Pay.aspx");
                    }
                    else
                    {
                        lblSuccess.Text = "Greetings message saved successfully.";
                    }

                }
                else
                {

                }
            }
            else
            {

            }
        }
        catch
        {

        }
    }

    public void SaveContestCharity()
    {

    }

   
}
