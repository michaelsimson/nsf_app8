Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports RKLib.ExportData
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections

Imports System.Net

Imports System.Xml

Partial Class CoachClassContactList
    Inherits System.Web.UI.Page

    Protected isProductGroupSelectionChanged As Boolean
    Dim isDebug As Boolean = False
    Dim conn As SqlConnection

    Dim volRoleDataReader As SqlDataReader
    Dim isInEditMode As Boolean = False
    Dim editIndex As Integer = -1
    Dim loginId As Integer = 0
    Dim coachflag As Integer
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1"
        'Session("LoginID") = "4240"
        lblStatus.Text = ""
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=p")
        End If
        Dim loginChapterId As Integer = 0
        Dim loginZoneId As Integer = 0
        Dim loginClusterId As Integer = 0
        Dim loginVolunteerId As Integer = 0
        Dim loginRoleId As Integer = 0
        Dim loginName As String = ""
        Dim teamLead As Char = ""

        conn = New SqlConnection(Application("ConnectionString"))

        If (Not Page.IsPostBack) Then

            Dim nsfMaster As VRegistration.NSFMasterPage = Me.Master
            nsfMaster.addBackMenuItem("volunteerfunctions.aspx")

            loginId = CType(Session("LoginId"), Decimal)
            loginRoleId = CType(Session("RoleId"), Decimal)

            Dim sb As StringBuilder = New StringBuilder

            sb.Append("select firstname,lastname,v.volunteerid ,isnull(v.chapterId,0) as chapterid ,isnull(v.clusterid,0) as clusterid ,isnull(v.zoneid,0) as zoneid, isnull(TeamLead,'N') as TeamLead ")
            sb.Append(" from volunteer v ,indspouse i where v.memberid = i.automemberid and v.memberid= ")
            sb.Append(CType(loginId, String))
            sb.Append(" and roleid =")
            sb.Append(CType(loginRoleId, String))

            Dim dr As SqlDataReader
            Try
                dr = SqlHelper.ExecuteReader(Application("connectionString"), CommandType.Text, sb.ToString)
                ' Iterate through DataReader, should only be one 
                While (dr.Read())
                    If Not dr.IsDBNull(0) Then
                        loginName = CStr(dr("firstName")) + " " + CStr(dr("lastname"))
                        loginVolunteerId = CInt(dr("volunteerid"))
                        loginChapterId = CInt(dr("chapterid"))
                        loginZoneId = CInt(dr("ZoneId"))
                        loginClusterId = CInt(dr("ClusterId"))
                        teamLead = CChar(dr("TeamLead"))
                    End If
                End While
                If Not dr Is Nothing Then dr.Close()
            Finally
                dr = Nothing
            End Try
            If ((loginRoleId = Nothing) Or (loginRoleId = 0)) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is no role assigned to this login id."
                Return
            End If
            If (loginRoleId < 1 Or loginRoleId > 5) And loginRoleId <> 89 And loginRoleId <> 96 And loginRoleId <> 88 And loginRoleId <> 30 Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is a mismatch in the assignment of roles.  Please contact the National Coordinator"
                Return
            End If
            '*************************************
            '** Setting flag fo coach
            '*************************************

            If Not Request.QueryString("coachflag") = Nothing Then
                If Request.QueryString("coachflag") = 1 Then
                    coachflag = 1
                ElseIf Request.QueryString("coachflag") = 2 Then
                    coachflag = 2
                End If
            End If


            If coachflag = 1 Then
                '**** Make all for Coach
                ddlRoleCatSch.Items.Clear()
                ddlRoleCatSch.Items.Add("National")
                ddlRoleCatSch.Enabled = False
                Dim strSql As String
                If loginRoleId = 88 Then
                    strSql = "Select * from Role where roleID in (88)"
                Else
                    strSql = "Select * from Role where roleID in (88,89)"
                End If
                Dim ds As DataSet
                Try
                    ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                Catch se As SqlException
                    displayErrorMessage(strSql)
                    Return
                End Try
                If (ds.Tables.Count > 0) Then
                    ddlRoleSch.DataSource = ds.Tables(0)
                    ddlRoleSch.DataBind()
                    ddlRoleSch.SelectedIndex = ddlRoleSch.Items.IndexOf(ddlRoleSch.Items.FindByText("Coach"))
                End If
                tr1.Visible = False
                tr2.Visible = False
                tr3.Visible = False
                tr4.Visible = False
                loadyear()
                TrPrd.Visible = True
                TrPrdGrp.Visible = True
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "30") Then
                    LoadProductGroup()

                ElseIf Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then

                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null") > 1 Then
                        'more than one 
                        'Response.Write("select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null")
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        LoadProductGroup()

                    Else
                        'only one
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                        Else
                            lblerr.Text = "Sorry, No product assinged to the Volunteer"
                            Exit Sub

                        End If
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        LoadProductGroup()

                    End If
                End If
            ElseIf coachflag = 2 Then

                loadyear()
                FillProductGroup()
                FillProductCode()
                tblAssignRoles.Visible = False
                trviewChildren.Visible = True
                LoadCoach()
                If loginRoleId = 88 Then
                    ddlCoach.SelectedIndex = ddlCoach.Items.IndexOf(ddlCoach.Items.FindByValue(Session("loginID")))
                    If teamLead.ToString.ToUpper = "Y" Then
                        ddlCoach.Enabled = True
                    Else
                        ddlCoach.Enabled = False
                    End If

                    loadChildren()
                End If
            Else
                Dim errMsg As String = Nothing
                If (loginRoleId = 3 And loginZoneId = 0) Then
                    errMsg = "The login Zonal Coordinator's zone information missing"
                ElseIf (loginRoleId = 4 And loginClusterId = 0) Then
                    errMsg = "The login Cluster coordinator's cluster information missing"
                ElseIf (loginRoleId = 5 And loginChapterId = 0) Then
                    errMsg = "The login Chapter Coordinator's chapter information missing"
                End If
                If (Not errMsg Is Nothing) Then
                    tblAssignRoles.Visible = False
                    tblMessage.Visible = True
                    lblError.Visible = True
                    lblError.Text = errMsg
                    Return
                End If

                Session("loginChapterId") = loginChapterId
                Session("loginRoleId") = loginRoleId
                Session("loginZoneId") = loginZoneId
                Session("loginClusterId") = loginClusterId
                Session("loginVolunteerId") = loginVolunteerId
                Session("loginName") = loginName

                DdlCluster.DataSource = ClusterDS
                DdlCluster.DataTextField = "ClusterCode"
                DdlCluster.DataValueField = "ClusterID"
                DdlCluster.DataBind()

                ddlChapter.DataSource = ChapersDSet
                ddlChapter.DataTextField = "ChapterCode"
                ddlChapter.DataValueField = "ChapterID"
                ddlChapter.DataBind()

                DdlZonalCoordinator.DataSource = ZoneDS
                DdlZonalCoordinator.DataTextField = "ZoneCode"
                DdlZonalCoordinator.DataValueField = "ZoneID"
                DdlZonalCoordinator.DataBind()

                loadSearchControls(loginVolunteerId, loginRoleId, loginZoneId, loginClusterId, loginChapterId)
            End If
            Page.MaintainScrollPositionOnPostBack = True
            clearMessages()

        End If
    End Sub

    Private Sub LoadProductGroup()
        Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P  where P.EventId=13 " & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID" ' P.ProductGroupId IN (Select Distinct ProductGroupId  from Product  where  EventID=13 ) AND
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

        ddlProductGroup.DataSource = drproductgroup
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count < 1 Then
            lblerr.Text = "No Product is opened. Please Contact admin and Get Product Opened in EventFees table"
        ElseIf ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Items.Insert(0, New ListItem("All", 0)) '"Select Product Group")
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        Else
            ddlProductGroup.Enabled = False
            LoadProductID()
        End If
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try
                strSql = "Select P.ProductID, P.Name from Product P  where  P.EventID=13 " & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " order by P.ProductID"
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "All") '"Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    'loadGrid(True)
                End If
            Catch ex As Exception
                lblerr.Text = strSql & ex.ToString
            End Try
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadProductID()

    End Sub

    Protected Sub ddlCyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        FillProductGroup()
        FillProductCode()
        loadChildren()
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 2 Step -1
            ddlCYear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        If Now.Month <= 3 Then
            ddlCYear.Items(1).Selected = True
        Else
            ddlCYear.Items(0).Selected = True
        End If

    End Sub

    Private Sub LoadCoach()
        Dim strSql As String = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as ID,I.Firstname from IndSpouse I INNER JOIN Volunteer V ON v.MemberID = I.AutoMemberID INNER JOIN CalSignUp C ON v.MemberID=C.MemberID where V.RoleId in (88)  "
        Dim Prdgrp As String = String.Empty
        If Session("RoleId").ToString() = "89" Then
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null") > 0 Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null ")
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
            End If
            If Prdgrp.Length > 0 Then strSql = strSql & " AND C.ProductGroupID IN (" & Prdgrp & ")"
        End If
        strSql = strSql & " order by I.FirstName "
        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlCoach.DataSource = drcoach
        ddlCoach.DataBind()
        If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "96" Then
            ddlCoach.Items.Insert(0, "[ALL]")
        End If
        ddlCoach.Items.Insert(0, "Select Coach Name")
        ddlCoach.Items(0).Selected = True
    End Sub
    Private Sub showTimeOutMsg()
        'lblSessionTimeout.Visible = True
        'lblSessionTimeout.Text = "<blink>Session has expired. Please try logging in again.</blink>"
        Response.Redirect("timeoutError.aspx")
    End Sub
    Private Function isSessionValid() As Boolean
        If (Session("LoginID") Is Nothing Or Session("loginRoleId") Is Nothing) Then
            Return True
        Else : Return False
        End If
    End Function
    Private Function getLoginTypeId(ByVal type As String) As Integer
        Dim id As Integer = 0
        If (type = "Zone") Then
            If (Not Session("loginZoneId") Is Nothing) Then
                id = CInt(Session("loginZoneId").ToString)
            End If
        ElseIf (type = "Chapter") Then
            If (Not Session("loginChapterId") Is Nothing) Then
                id = CInt(Session("loginChapterId").ToString)
            End If
        ElseIf (type = "Cluster") Then
            If (Not Session("loginClusterId") Is Nothing) Then
                id = CInt(Session("loginClusterId").ToString)
            End If
        End If
        Return id
    End Function

    Protected Function getLoginRoleId() As Integer
        Dim loginRoleId As Integer = 0
        Try
            If (Not Session("loginRoleId") Is Nothing) Then
                loginRoleId = CInt(Session("loginRoleId").ToString())
            End If
        Catch

        End Try
        Return loginRoleId
    End Function
    Protected Function getRoleSearchCategory() As String
        If (Not Session("searchRoleCategory") Is Nothing) Then
            Return Session("searchRoleCategory").ToString
        Else
            showTimeOutMsg()
            Return Nothing
        End If
    End Function
    Private Sub LoadSelectSessions()

        If ((Not Session("selChapterID") Is Nothing) And Session("selChapterID") <> "" And Session("selChapterID") <> "0" And Session("selChapterID") <> "Select Chapter") Then
            ddlChapter.SelectedIndex = -1
            If (Not ddlChapter.Items.FindByValue(Session("selChapterID")) Is Nothing) Then
                ddlChapter.Items.FindByValue(Session("selChapterID")).Selected = True
                ddlChapter.Enabled = False
            End If
        ElseIf ((Not Session("selClusterID") Is Nothing) And Session("selClusterID") <> "0") Then
            DdlCluster.SelectedIndex = -1
            If (Not DdlCluster.Items.FindByValue(Session("selClusterID")) Is Nothing) Then
                DdlCluster.Items.FindByValue(Session("selClusterID")).Selected = True
                DdlCluster.Enabled = False
            End If
        ElseIf ((Not Session("selZoneID") Is Nothing) And Session("selZoneID") <> "0") Then
            DdlZonalCoordinator.SelectedIndex = -1
            If (Not DdlZonalCoordinator.Items.FindByValue(Session("selZoneID")) Is Nothing) Then
                DdlZonalCoordinator.Items.FindByValue(Session("selZoneID")).Selected = True
                DdlZonalCoordinator.Enabled = False
            End If
        End If
    End Sub
    Public Sub loadSearchControls(ByVal volId As Integer, ByVal roleId As Integer, ByVal zoneId As Integer, ByVal clusterId As Integer, ByVal chapterId As Integer)
        If (Not roleId = Nothing) Then

            lblLoginName.Text = Session("loginName")
            loadRoleCatDropDown(roleId)
            loadWeekOfDown()


            Select Case roleId
                Case Is < 3 'admin, NationalC
                    If (roleId = 1) Then
                        lblLoginRole.Text = " You are logged in as 'Admin'"
                    ElseIf (roleId = 2) Then
                        lblLoginRole.Text = "You are logged in as 'National Coordinator'."
                    End If
                    LoadSelectSessions()
                    DdlZonalCoordinator.Enabled = False
                    DdlCluster.Enabled = False
                    ddlChapter.Enabled = False
                Case 3 'ZonalC
                    lblLoginRole.Text = "You are logged in as 'Zonal Coordinator'"
                    If (zoneId <> 0) Then
                        DdlZonalCoordinator.SelectedIndex = -1
                        DdlZonalCoordinator.Items.FindByValue(zoneId).Selected = True
                        LoadZonalChapters()
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.Enabled = False
                        ddlChapter.Enabled = True
                    End If

                    LoadSelectSessions()
                Case 4 'ClusterC
                    lblLoginRole.Text = "You are logged in as 'Cluster Coordinator'"
                    If (clusterId <> 0) Then
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.SelectedIndex = -1
                        DdlCluster.Items.FindByValue(clusterId).Selected = True
                        LoadClusterChapters()
                        DdlCluster.Enabled = False
                        ddlChapter.Enabled = True
                    End If
                    LoadSelectSessions()
                Case 5 'ChapterC
                    lblLoginRole.Text = "You are logged in as 'Chapter Coordinator'"
                    If (chapterId <> 0) Then
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.Enabled = False
                        ddlChapter.SelectedIndex = -1
                        ddlChapter.Items.FindByValue(chapterId).Selected = True
                        ddlChapter.Enabled = False
                    End If
            End Select
        End If
    End Sub
    Private Sub loadRoleCatDropDown(ByVal loginRoleId As Integer)
        If (getSessionSelectionID("zone") > 0) Then
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Zonal"
            loadRolesDropDown("Zonal")
            loadList(loginRoleId, "Zonal")
        ElseIf (getSessionSelectionID("cluster") > 0) Then
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Cluster"
            loadRolesDropDown("Cluster")
            loadList(loginRoleId, "Cluster")
        ElseIf (getSessionSelectionID("chapter") > 0) Then
            ddlRoleCatSch.Visible = False
            lblRoleCat.Visible = True
            lblRoleCat.Text = "Chapter"
            loadRolesDropDown("Chapter")
            'loadList( loginRoleId,"Chapter")
        Else
            If (loginRoleId > 0 And loginRoleId < 3) Then
                ddlRoleCatSch.Items.Add("National")
                ddlRoleCatSch.Items.Add("Zonal")
                ddlRoleCatSch.Items.Add("Cluster")
                ddlRoleCatSch.Items.Add("Finals")
                ddlRoleCatSch.Items.Add("Chapter")
                ddlRoleCatSch.Items.Add("IndiaChapter")
            ElseIf (loginRoleId = 3) Then 'ZonalC
                ddlRoleCatSch.Items.Add("Zonal")
                ddlRoleCatSch.Items.Add("Chapter")
            ElseIf (loginRoleId = 4) Then  'clusterC
                ddlRoleCatSch.Items.Add("Cluster")
                ddlRoleCatSch.Items.Add("Chapter")
            ElseIf (loginRoleId = 5) Then 'ChapterC
                ddlRoleCatSch.Visible = False
                lblRoleCat.Visible = True
                lblRoleCat.Text = "Chapter"
                loadRolesDropDown("Chapter")
                loginRoleId = getLoginRoleId() 'CInt(Session("loginRoleId").ToString
                loadList(loginRoleId, "Chapter")
            End If
        End If
    End Sub
    Private Sub loadWeekOfDown()
        loadDropDownBox(ddWeekOf, "usp_GetWeekOfContactList", "WeekOfChapters1", "SatOrSunDay", "SatOrSunDay", "[Select a Week]", getSessionSelectionID("WeekOfChapters1"))
    End Sub

    Private Sub loadDropDownBox(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal storedProc As String, ByVal name As String, ByVal textCol As String, ByVal valueCol As String, ByVal nullItemText As String, ByVal selectedValue As Integer)

        Dim ds As New DataSet
        Dim liEvent As New ListItem(nullItemText, "0")

        ds = Cache.Get(name)

        If (ds Is Nothing) Then
            Try
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, storedProc)
            Catch se As SqlException
                lblError.Visible = True
                lblError.Text = se.Message
                ' tblMessage.Visible = True
                Return
            End Try
            Cache.Insert(name, ds)
        End If

        If ds.Tables.Count > 0 Then
            ddl.DataSource = ds.Tables(0)
            ddl.DataTextField = ds.Tables(0).Columns(textCol).ToString
            ddl.DataValueField = ds.Tables(0).Columns(valueCol).ToString
            ddl.DataBind()
            ddl.Items.Insert(0, liEvent)
            If (selectedValue > 0) Then
                ddl.Items.FindByValue(selectedValue).Selected = True
                ddl.Enabled = False
            Else
                ddl.Items.FindByValue("0").Selected = True
            End If
        End If
    End Sub
    Private Sub loadChapterDropDown(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal zoneId As Integer, ByVal clusterid As Integer, ByVal selectedValue As Integer)
        Dim dsEvents As New DataSet
        Dim liNull As New ListItem("Select A Chapter", "0")
        Dim sb As New StringBuilder
        sb.Append(" select chapterId, Name, chapterCode from chapter ")
        If (zoneId > 0 And clusterid > 0) Then
            sb.Append(" where zoneid=")
            sb.Append(CStr(zoneId))
            sb.Append(" and clusterid= ")
            sb.Append(CStr(clusterid))
        ElseIf (zoneId > 0) Then
            sb.Append(" where zoneid = ")
            sb.Append(CStr(zoneId))
        ElseIf (clusterid > 0) Then
            sb.Append(" where clusterid = ")
            sb.Append(CStr(clusterid))
        End If
        sb.Append(" order by state,chapterCode ")
        Try
            dsEvents = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString)
        Catch se As SqlException
            displayErrorMessage(se.Message)
            Return
        End Try

        If dsEvents.Tables.Count > 0 Then
            ddl.DataSource = dsEvents.Tables(0)
            ddl.DataTextField = dsEvents.Tables(0).Columns("chapterCode").ToString
            ddl.DataValueField = dsEvents.Tables(0).Columns("ChapterId").ToString
            ddl.DataBind()
            ddl.Items.Insert(0, liNull)
            If (selectedValue > 0) Then
                ddl.Items.FindByValue(selectedValue).Selected = True
                ddl.Enabled = False
            Else
                ddl.Items.FindByValue("0").Selected = True
            End If
        Else


        End If

    End Sub
    '----------------------------------------------------------------------------------------
    ' Procedure   : AddEdi4010Remittance
    ' Created     : 04/07/2007
    ' Created by  : Srini Eerpina
    ' Language    : VB.Net
    ' Arguments   : , 
    ' Purpose     :  overloaded method to implement chapters dropdown based on the week select
    ' Revised     : 
    '--------------------------------------------------------------------------------------
    '
    Private Sub loadChapterDropDown(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal zoneId As Integer, ByVal clusterid As Integer, ByVal selectedValue As Integer, ByVal weekday As String)
        Dim dsEvents As New DataSet
        Dim liNull As New ListItem("[Select a Chapter]", "0")
        Dim liNullNoChapters As New ListItem("[No Chapters found]", "0")
        Dim sb As New StringBuilder

        Dim strWeek() As String
        Dim strSat As String = ""
        Dim strSun As String = ""

        If ddWeekOf.SelectedIndex <> 0 Then
            strWeek = ddWeekOf.SelectedValue.Split("-")
            strSat = LTrim(RTrim(strWeek(0)))
            strSun = LTrim(RTrim(strWeek(1)))
        End If
        sb.Append(" select chapterId, Name, chapterCode from chapter ")
        If (zoneId > 0 And clusterid > 0) Then
            sb.Append(" where zoneid=")
            sb.Append(CStr(zoneId))
            sb.Append(" and clusterid= ")
            sb.Append(CStr(clusterid))
        ElseIf (zoneId > 0) Then
            sb.Append(" where zoneid = ")
            sb.Append(CStr(zoneId))
        ElseIf (clusterid > 0) Then
            sb.Append(" where clusterid = ")
            sb.Append(CStr(clusterid))
        End If
        If strSat <> "" AndAlso strSun <> "" Then
            If (zoneId > 0 Or clusterid > 0) Then
                sb.Append(" and ChapterID IN  ")
            Else
                sb.Append(" where ChapterID IN  ")
            End If

            sb.Append(" ( ")
            sb.Append("Select  DISTINCT(nsfchapterid) ")
            sb.Append(" FROM contest  ")
            sb.Append(" WHERE ContestDate IN ( ' " & strSat & "','" & strSun & "') ")
            sb.Append(" ) ")
        End If
        sb.Append(" order by state,chapterCode ")
        Try
            dsEvents = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString)
        Catch se As SqlException
            displayErrorMessage(se.Message)
            Return
        End Try
        If dsEvents.Tables.Count > 0 Then
            ddl.DataSource = dsEvents.Tables(0)
            ddl.DataTextField = dsEvents.Tables(0).Columns("chapterCode").ToString
            ddl.DataValueField = dsEvents.Tables(0).Columns("ChapterId").ToString
            ddl.DataBind()
            ddl.Items.Insert(0, liNull)
            If (selectedValue > 0) Then
                ddl.Items.FindByValue(selectedValue).Selected = True
                ddl.Enabled = False
            Else
                ddl.Items.FindByValue("0").Selected = True
            End If
        Else
            ddl.Items.Clear()
            ddl.Items.Insert(0, liNullNoChapters)
        End If
    End Sub
    Public Sub loadRolesDropDown(ByVal category As String)

        Dim strSql As String = "Select roleid, selection from Role where "
        Dim loginRoleId As Integer = getLoginRoleId()

        If (category <> "0") Then
            strSql = strSql + getCategorySql(category)
            If (loginRoleId > 1 And loginRoleId <= 5) Then  'not to let Chapter Coor to select ChapterC
                strSql = strSql + " and roleid >" + CStr(loginRoleId)
            ElseIf loginRoleId = 88 Then
                strSql = strSql + " and roleid  in (88)"
            ElseIf loginRoleId = 89 Then
                strSql = strSql + " and roleid in (88,89)"
            End If
            strSql = strSql + " order by selection "

            Dim ds As DataSet
            Try
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            Catch se As SqlException
                displayErrorMessage(strSql)
                Return
            End Try
            If (ds.Tables.Count > 0) Then
                ddlRoleSch.DataSource = ds.Tables(0)
                ddlRoleSch.DataBind()
                Dim lstItem As ListItem
                lstItem = New ListItem()
                lstItem.Text = "Select a Role"
                lstItem.Value = 0
                ddlRoleSch.Items.Insert(0, lstItem)
                ddlRoleSch.Items.FindByText("Select a Role").Selected = True
                ddlRoleSch.Items.FindByText("Select a Role").Value = 0
            End If
        Else
            ddlRoleSch.Items.Clear()
            ddlRoleSch.Items.Add("Select a Role")
            ddlRoleSch.Items.FindByText("Select a Role").Selected = True
            ddlRoleSch.Items.FindByText("Select a Role").Value = 0
        End If

    End Sub
    Private Function getCategorySql(ByVal category As String) As String
        Dim strSql As String = ""

        If (category <> Nothing) Then
            Select Case (category)
                Case "Zonal"
                    strSql = " [zonal]='Y'"
                Case "Cluster"
                    strSql = " [Cluster]='Y'"
                Case "Chapter"
                    strSql = " [chapter]='Y'"
                Case "Finals"
                    strSql = " [Finals]='Y'"
                Case "IndiaChapter"
                    strSql = " [IndiaChapter]='Y'"
                Case "National"
                    strSql = " [National]='Y'"
            End Select
        End If
        Return strSql
    End Function

    Private Sub displayErrorMessage(ByVal msg As String)
        lblError.Visible = True
        'lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = msg
        tblMessage.Visible = True
        TblSearchString.Visible = True
        'txtSearchCriteria.Text = ""
    End Sub
    Private Sub clearMessages()
        lblError.Visible = False
        lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = ""
        lblSessionTimeout.Text = ""
        lblSessionTimeout.Visible = False
        lblUpdateError.Text = ""
    End Sub
    Private Sub clearAllPanels()
        tblVolRoleResults.Visible = False
        grdVolResults.Visible = False
        DG_Unique.Visible = False
    End Sub
    Private Function getSessionSelectionID(ByVal type As String) As Integer
        Dim selChapterId As Integer = 0  'sel*ID are values selected at VolunteerFunctions.aspx page
        Dim selClusterId As Integer = 0
        Dim selZoneId As Integer = 0
        Dim retId As Integer = 0

        If (Not Session("selZoneID") Is Nothing) Then
            selZoneId = CInt(Session("selZoneID"))
        End If
        If (Not Session("selClusterID") Is Nothing) Then
            selClusterId = CInt(Session("selClusterID"))
        End If
        If (Not Session("selChapterID") Is Nothing) Then
            selChapterId = CInt(Session("selChapterID"))
        End If
        Select Case type.ToLower()
            Case "zone"
                retId = selZoneId
            Case "cluster"
                retId = selClusterId
            Case "chapter"
                retId = selChapterId
        End Select
        Return retId
    End Function
    Public Sub ddlRoleCat_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoleCatSch.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        Dim selectText As String = ddl.SelectedValue
        Dim loginRoleId As Integer = 0

        clearAllPanels()
        Session("volRoleSearchCriteria") = ""
        Session("volRoleSearchSql") = ""
        hideSearchCriteria()

        'ddlList.Visible = False
        'ddlList.SelectedIndex = -1

        loadRolesDropDown(selectText)
        loginRoleId = getLoginRoleId() 'CInt(Session("loginRoleId").ToString
        loadList(loginRoleId, selectText)
        'enable/disable zone, cluster and chapter drop downs
        If (selectText = "Chapter") Then
            If ((Not Session("selChapterID") Is Nothing) And Session("selChapterID") <> "" And Session("selChapterID") <> "0" And Session("selChapterID") <> "Select Chapter") Then
                DdlZonalCoordinator.Enabled = False
                DdlCluster.Enabled = False
                ddlChapter.Enabled = False
            ElseIf ((Not Session("selClusterID") Is Nothing) And Session("selClusterID") <> "0") _
                    Or ((Not Session("loginClusterID") Is Nothing) And Session("loginClusterID") <> "0") Then
                DdlCluster.Enabled = False
            ElseIf ((Not Session("selZoneID") Is Nothing) And Session("selZoneID") <> "0") _
                    Or ((Not Session("loginZoneId") Is Nothing) And Session("loginZoneId") <> "0") Then
                DdlZonalCoordinator.Enabled = False
            Else
                DdlZonalCoordinator.Enabled = True
                DdlCluster.Enabled = True
                ddlChapter.Enabled = True
            End If
        ElseIf (selectText = "Zonal") Then
            If ((Not Session("selZoneID") Is Nothing) And Session("selZoneID") <> "0") _
                Or ((Not Session("loginZoneId") Is Nothing) And Session("loginZoneId") <> "0") Then
                DdlZonalCoordinator.Enabled = False
            Else
                DdlZonalCoordinator.Enabled = True
                DdlCluster.Enabled = False
                ddlChapter.Enabled = True
            End If
        ElseIf (selectText = "Cluster") Then
            If ((Not Session("selClusterID") Is Nothing) And Session("selClusterID") <> "0") _
                Or ((Not Session("loginClusterID") Is Nothing) And Session("loginClusterID") <> "0") Then
                DdlCluster.Enabled = False
            Else
                DdlZonalCoordinator.Enabled = False
                DdlCluster.Enabled = True
                ddlChapter.Enabled = True
            End If
        Else
            DdlZonalCoordinator.Enabled = False
            DdlCluster.Enabled = False
            ddlChapter.Enabled = False
        End If


        If ddlRoleCatSch.SelectedValue = "Chapter" Then
            ddWeekOf.Enabled = True
        Else
            ddWeekOf.Enabled = False
        End If

    End Sub
    Private Sub loadList(ByVal loginRoleId As Integer, ByVal selectText As String)

    End Sub

    Protected Sub Find_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Find.Click

        ' strSql.Append("select i.memberid,v.volunteerid,FirstName+' '+LastName as Name,v.RoleId,v.RoleCode,v.TeamLead,v.EventYear,v.EventId,v.EventCode,v.Chapterid,v.ChapterCode,[National],Zoneid,Finals,ClusterId,ProductGroupId From volunteer v,Indspouse i where v.memberid = i.automemberId ")
        Dim sb As StringBuilder = New StringBuilder()
        Dim sbSch As StringBuilder = New StringBuilder()
        Dim bCheck As Boolean = False
        Dim bFirst As Boolean = False
        Dim txtRole As String = "0" '= ddlRoleSch.SelectedValue
        Dim txtRoleCat As String = ddlRoleCatSch.SelectedValue
        Dim txtRoleCat2 As String = lblRoleCat.Text
        Dim txtList As String '= ddlList.SelectedValue
        Dim blnFirst As Boolean = True
        Dim loginRoleId As Integer = getLoginRoleId()
        Dim bError As Boolean = False
        Dim strErrMsg As String = ""
        Dim strRoleText As String = ""
        grdVolResults.Visible = False
        grdVolResults.Controls.Clear()
        Session("volRoleSearchCriteria") = ""
        Session("volRoleSearchSql") = ""
        hideSearchCriteria()

        DG_Unique.Visible = False
        btn_ShowUniqueRecords.Text = "Show Unique Records"
        If txtList = "" Then txtList = Session("loginChapterId")

        Dim intIndex
        For intIndex = 0 To ddlRoleSch.Items.Count - 1
            If ddlRoleSch.Items(intIndex).Selected Then
                txtRole = txtRole + "," + ddlRoleSch.Items(intIndex).Value.ToString()
                strRoleText = strRoleText + "|" + ddlRoleSch.Items(intIndex).Text
            End If
        Next
        txtRole = CStr(txtRole)
        Dim intLength As String = txtRole.Length
        If intLength >= 2 Then
            txtRole = txtRole.Substring(2, intLength - 2)
        End If
        If (txtRoleCat <> "0" Or txtRoleCat2 <> Nothing) Then
            Dim roleCategory As String

            If (txtRoleCat <> Nothing And txtRoleCat <> "0") Then
                roleCategory = txtRoleCat
            Else
                roleCategory = txtRoleCat2
            End If

            If (loginRoleId < 3) Then
                If (roleCategory = "Zonal" And DdlZonalCoordinator.SelectedValue = "0") Then 'txtList = "0"
                    bError = True
                    strErrMsg = "Please Select a Zone"
                ElseIf (roleCategory = "Cluster" And DdlCluster.SelectedValue = "0") Then
                    bError = True
                    strErrMsg = "Please select a Cluster"
                End If
            End If

            If (bError = False) Then

                sbSch.Append("<font color=green> Role Category = </font>" + roleCategory + "<br>")
                Session("searchRoleCategory") = roleCategory

                If (txtRole <> "0" And txtRole <> "" And txtRole.Length() > 0) Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.RoleId in (" + txtRole + ")")
                    lbllRoleIds.Text = txtRole
                    sbSch.Append("<font color=green>Role = </font>" + strRoleText + "<br>")
                    Session("searchRoleId") = CStr(txtRole)
                    Session("searchRoleCode") = strRoleText
                Else
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.RoleId in ( select roleid from role where " + getCategorySql(roleCategory) + ")")
                    sbSch.Append("<font color=green>Role =  </font>All Roles <br>")
                    Session("searchRoleCode") = "All Roles"
                    Session("searchRoleId") = 0
                End If

                If (roleCategory = "Zonal") Or (roleCategory = "Cluster") Or (roleCategory = "Chapter") Then
                    If (ddlChapter.SelectedValue <> "" And ddlChapter.SelectedValue <> "Select Chapter") Then
                        txtList = ddlChapter.SelectedValue
                        Dim chId As Integer
                        If (txtList <> "0" And txtList <> "") Then
                            chId = CInt(txtList)
                        Else
                            ' chId = getLoginTypeId("Chapter")
                            chId = getSessionSelectionID("chapter")
                        End If
                        If chId <> "0" Then
                            If (blnFirst = True) Then
                                blnFirst = False
                            Else
                                sb.Append(" and ")
                            End If
                            sb.Append(" V.chapterId=" + CStr(chId))
                            sbSch.Append("<font color=green>Chapter = </font>" + getChapterName(chId) + "<br>")
                            Session("searchChapterId") = chId
                        Else
                            sbSch.Append("<font color=green>Chapter = </font> All Chapters <br>")
                        End If
                    ElseIf (DdlZonalCoordinator.SelectedValue <> "0") Then
                        txtList = DdlZonalCoordinator.SelectedValue
                        Dim zId As Integer
                        If (txtList <> "0" And txtList <> "") Then
                            zId = CInt(txtList)
                        Else
                            ' zId = getLoginTypeId("Zone")
                            zId = getSessionSelectionID("zone")
                        End If
                        If (blnFirst = True) Then
                            blnFirst = False
                        Else
                            sb.Append(" and ")
                        End If
                        'sb.Append(" zoneId=" + CStr(zId))
                        sb.Append(" V.ChapterID in (select ChapterID from Chapter where ZoneId=" + CStr(zId) + ")")
                        sbSch.Append("<font color=green>Zone = </font>" + getZoneName(zId) + "<br>")
                        Session("searchZoneId") = zId
                    ElseIf (DdlCluster.SelectedValue <> "0") Then
                        txtList = DdlCluster.SelectedValue
                        Dim clId As Integer
                        If (txtList <> "0" And txtList <> "") Then
                            clId = CInt(txtList)
                        Else
                            ' clId = getLoginTypeId("Cluster")
                            clId = getSessionSelectionID("cluster")
                        End If
                        If (blnFirst = True) Then
                            blnFirst = False
                        Else
                            sb.Append(" and ")
                        End If
                        ' sb.Append(" clusterId=" + CStr(clId))
                        sb.Append(" V.ChapterID in (select ChapterID from Chapter where clusterId=" + CStr(clId) + ")")
                        sbSch.Append("<font color=green>Cluster = </font>" + getClusterName(clId) + "<br>")
                        Session("searchClusterId") = clId
                    End If
                ElseIf (roleCategory = "National") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.[national]='Y'")
                    sbSch.Append("<font color=green>National = </font> Yes" + "<br>")
                ElseIf (roleCategory = "Finals") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.[finals]='Y'")
                    sbSch.Append("<font color=green>Finals = </font> Yes" + "<br>")
                ElseIf (roleCategory = "IndiaChapter") Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.[indiachapter]='Y'")
                    sbSch.Append("<font color=green>India Chapter = </font> Yes" + "<br>")
                End If

                If (loginRoleId <= 5 And loginRoleId > 1) Then
                    If (blnFirst = True) Then
                        blnFirst = False
                    Else
                        sb.Append(" and ")
                    End If
                    sb.Append(" V.roleid > " + CStr(loginRoleId)) ' Do not show records above the given level.
                End If

                Dim whereStr As String = ""

                If (sb.Length() > 0) Then
                    whereStr = sb.ToString()
                End If
                Session("volRoleSearchSql") = whereStr
                Session("volRoleSearchCriteria") = sbSch.ToString
                'displaySearchCriteria()
                clearGrid()
                loadGrid(True)
            End If
        Else
            bError = True
            strErrMsg = "Please select an option"
        End If

        If (bError = True) Then
            displayErrorMessage(strErrMsg)
        End If
    End Sub
    Public Function getChapterName(ByVal chapterId As Int16) As String
        Dim chapName As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from chapter where chapterid = " + CStr(chapterId))
        Return chapName
    End Function
    Public Function getClusterName(ByVal clusterId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from cluster where clusterid = " + CStr(clusterId))
        Return name
    End Function
    Public Function getZoneName(ByVal zoneId As Int16) As String
        Dim name As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select name from zone where zoneid = " + CStr(zoneId))
        Return name
    End Function
    Public Sub displaySearchCriteria()
        Dim str As String = Session("volRoleSearchCriteria").ToString
        'Dim str As String = Session("volRoleSearchSql").ToString ''for debugging
        TblSearchString.Visible = True
        txtSearchCriteria.Visible = True
        txtSearchCriteria.Text = str

    End Sub
    Public Sub hideSearchCriteria()
        TblSearchString.Visible = False
        txtSearchCriteria.Visible = False
        txtSearchCriteria.Text = False
    End Sub
    Public Sub clearGrid()
        btnExport1.Enabled = False
        btnEmailExport.Enabled = False
        grdVolResults.EditItemIndex = -1
        grdVolResults.DataSource = Nothing
        grdVolResults.DataBind()
    End Sub

    Public Sub loadGrid(ByVal blnReload As Boolean)
        lblerr.Text = ""
        btnExport1.Enabled = True
        btnEmailExport.Enabled = True
        Dim ProductIDs As String = ""
        Dim ProductGroupIDs As String = ""

        Dim WeekOf As String = ""

        Dim ds As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        cmd.CommandText = " " + Session("volRoleSearchSql").ToString

        If (blnReload = True) Then
            'Response.Write(cmd.CommandText)
            Session("volDataSet") = Nothing
            Try
                If ddlRoleCatSch.SelectedValue = "Chapter" Then
                    If ddWeekOf.SelectedIndex = 0 Then
                        WeekOf = ""
                    Else
                        WeekOf = ddWeekOf.SelectedValue
                    End If
                Else
                    WeekOf = ""
                End If
                Dim str As String
                If Request.QueryString("coachflag") = 1 And Session("RoleId").ToString() = "89" Then
                    'If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                    '    lblerr.Text = "Please Select Product Group"
                    '    Exit Sub
                    'End If
                    '*******MODIFIED ON 11-02-2014 **********************************************'
                    If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                        For i As Integer = 1 To ddlProductGroup.Items.Count - 1
                            ProductGroupIDs = ProductGroupIDs + ddlProductGroup.Items(i).Value + ","
                        Next
                        ProductGroupIDs = ProductGroupIDs.ToString.TrimEnd(",")
                    Else
                        ProductGroupIDs = ddlProductGroup.SelectedValue
                        If ddlProduct.Items.Count > 0 Then
                            If ddlProduct.SelectedIndex = 0 Then
                                For i As Integer = 1 To ddlProduct.Items.Count - 1
                                    ProductIDs = ProductIDs + ddlProduct.Items(i).Value + ","
                                Next
                                ProductIDs = ProductIDs.ToString.TrimEnd(",")
                            Else
                                ProductIDs = ddlProduct.SelectedValue
                            End If
                        End If
                    End If
                    'If ddlProduct.SelectedItem.Text = "Select Product" Or ddlProduct.Items.Count = 0 Then
                    '    lblerr.Text = "Sorry, No product Selected to load"
                    '    Exit Sub
                    'End If
                    '*******MODIFIED ON 11-02-2014 to EXPORT STATE,CHAPTER,ZONE AND CLUSTER DETAILS**********************'
                    str = " SELECT Distinct dbo.ufn_getMemberNameByID(v.memberid) as Name,v.[VolunteerId] ,v.[MemberId],v.[RoleId],"
                    str = str & " dbo.ufn_getRoleCode(v.[RoleId]) as Selection , case v.TeamLead when null then 'N' else v.TeamLead end as TeamLead , v.[EventYear] ,"
                    str = str & " v.[EventId] , v.[EventCode] ,I.State, I.[ChapterId] ,I.Chapter as [ChapterCode] , v.[National] , Ch.[ZoneId] , Ch.[ZoneCode] , Ch.[ClusterId] , "
                    str = str & " Ch.[ClusterCode] , v.[Finals] , v.[IndiaChapter] , c.[ProductGroupID] , dbo.ufn_getProductGroupName(c.[ProductGroupId]) as productGroupCode , "
                    str = str & " c.[ProductId] , dbo.ufn_getProductName(c.[ProductId]) as productCode ,isnull(v.AgentFlag,'N') as AgentFlag ,"
                    str = str & " isnull(v.WriteAccess,'N') as WriteAccess , "
                    str = str & " isnull(v.[Authorization],'N') as 'Authorization' , v.[Yahoogroup] , v.[IndiaChapterName] , v.[CreateDate] , "
                    str = str & " case len(v.createDate) when 0 then '' else dbo.ufn_getMemberNameByID(v.[CreatedBy]) end as CreatedBy , v.[ModifyDate] ,"
                    str = str & " case len(v.ModifyDate) when 0 then '' else dbo.ufn_getMemberNameByID(v.[ModifiedBy]) end as ModifiedBy,I.Email,I.CPhone,I. HPhone, I.WPhone , "
                    str = str & "  dbo.ufn_GetMemberFirstNameByID( v.memberid) as FistName, dbo.ufn_GetMemberLastNameByID( v.memberid) as LastName, I.Address1 as Street,I.City,I.State,I.Zip as ZipCode, I.ChapterID as VChapterID,  I.Chapter as VChapterCode FROM [Volunteer] v "
                    str = str & " Inner Join CalSignUp C on V.MemberId = C.MemberID and V.RoleId in (" & lbllRoleIds.Text & ")  and C.Accepted='Y'"
                    str = str & " Inner Join IndSpouse I on v.MemberId =I.AutoMemberID"
                    str = str & " Inner Join Chapter Ch on Ch.ChapterID =I.ChapterID "
                    str = str & " where  C.ProductGroupId in(" & ProductGroupIDs & ") " & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in (" & ProductIDs & ")", "") '& ddlProduct.SelectedValue & ")" 'and V.RoleId = 88 
                    str = str & " order by v.[EventYear] Desc,V.roleid, dbo.ufn_getMemberLastNameByID(V.memberid) "

                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                ElseIf Request.QueryString("coachflag") = 1 Then
                    'If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                    '    lblerr.Text = "Please Select Product Group"
                    '    Exit Sub
                    'End If
                    '*******MODIFIED ON 11-02-2014 **********************************************'
                    If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                        For i As Integer = 1 To ddlProductGroup.Items.Count - 1
                            ProductGroupIDs = ProductGroupIDs + ddlProductGroup.Items(i).Value + ","
                        Next
                        ProductGroupIDs = ProductGroupIDs.ToString.TrimEnd(",")
                    Else
                        ProductGroupIDs = ddlProductGroup.SelectedValue

                        If ddlProduct.Items.Count > 0 Then
                            If ddlProduct.SelectedIndex = 0 Then
                                For i As Integer = 1 To ddlProduct.Items.Count - 1
                                    ProductIDs = ProductIDs + ddlProduct.Items(i).Value + ","
                                Next
                                ProductIDs = ProductIDs.ToString.TrimEnd(",")
                            Else
                                ProductIDs = ddlProduct.SelectedValue
                            End If
                        End If
                    End If
                    '*******MODIFIED ON 11-02-2014 to EXPORT STATE,CHAPTER,ZONE AND CLUSTER DETAILS**********************'

                    str = "SELECT Distinct dbo.ufn_getMemberNameByID(v.memberid) as Name, v.[VolunteerId] , v.[MemberId] , v.[RoleId] , "
                    str = str & " dbo.ufn_getRoleCode(v.[RoleId]) as Selection,case v.TeamLead when null then 'N' else v.TeamLead end as TeamLead,v.[EventYear],v.[EventId],"
                    str = str & " v.[EventCode] , I.State,I.[ChapterId] ,  I.Chapter as [ChapterCode] , v.[National] , Ch.[ZoneId] ,Ch.[ZoneCode] , "
                    str = str & " Ch.[ClusterId] , Ch.[ClusterCode] , v.[Finals] , v.[IndiaChapter] , c.[ProductGroupID] , "
                    str = str & " dbo.ufn_getProductGroupName(c.[ProductGroupId]) as productGroupCode , c.[ProductId] , "
                    str = str & " dbo.ufn_getProductName(c.[ProductId]) as productCode , isnull(v.AgentFlag,'N') as AgentFlag , isnull(v.WriteAccess,'N') as WriteAccess ,"
                    str = str & " isnull(v.[Authorization],'N') as 'Authorization' , v.[Yahoogroup] , v.[IndiaChapterName] , v.[CreateDate] , "
                    str = str & " case len(v.createDate) when 0 then '' else dbo.ufn_getMemberNameByID(v.[CreatedBy]) end as CreatedBy , v.[ModifyDate] , "
                    str = str & " case len(v.ModifyDate) when 0 then '' else dbo.ufn_getMemberNameByID(v.[ModifiedBy]) end as ModifiedBy, "
                    str = str & " I.Email, I.CPhone,I.HPhone, I.WPhone, dbo.ufn_GetMemberFirstNameByID( v.memberid) as FirstName, "
                    str = str & " dbo.ufn_GetMemberLastNameByID( v.memberid) as LastName,I.Address1 as Street,I.City,I.State,I.Zip as ZipCode, I.ChapterID as VChapterID,  I.Chapter as VChapterCode FROM [Volunteer] v "
                    str = str & " Left Join CalSignUp C on V.MemberId = C.MemberID and V.RoleId in(" & lbllRoleIds.Text & " ) and C.Accepted='Y'"
                    str = str & " and C.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in  (" & ProductIDs & ")", "")
                    str = str & " Inner Join IndSpouse I on I.AutoMemberID =V.MemberId "
                    str = str & " Inner Join Chapter Ch on Ch.ChapterID =I.ChapterID "
                    str = str & " where V.RoleId in  (" & lbllRoleIds.Text & ") and ((V.RoleID=89 and V.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and V.ProductID in  (" & ProductIDs & ")", "") & ") or  (V.RoleID=88 and C.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in  (" & ProductIDs & ")", "") & "))" 'and V.RoleId = 88  " '(" & ddlProduct.SelectedValue & ")
                    str = str & " order by v.[EventYear] Desc,V.roleid, dbo.ufn_getMemberLastNameByID(V.memberid) "

                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)

                Else
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.usp_SelectVolunteerList", New SqlParameter("@wherecondition", Session("volRoleSearchSql").ToString()), New SqlParameter("@ChapterWeek", WeekOf))

                End If

                btnExport2.Visible = True
                Session("volDataSet") = ds
                Session("Volun_DataSet") = ds
                grdVolResults.CurrentPageIndex = 0
            Catch se As SqlException
                displayErrorMessage(cmd.CommandText + " " + se.ToString())
                'displayErrorMessage("There is an error while trying to execute the query.")
                Return
            End Try
        Else
            If (Not Session("volDataSet") Is Nothing) Then
                ds = CType(Session("volDataSet"), DataSet)
            End If
        End If

        If (ds Is Nothing Or ds.Tables.Count < 1) Then
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            btn_ShowUniqueRecords.Visible = False
            btn_ExportUniqueList.Visible = False
            DG_Unique.Visible = False
            lblGrdVolResultsHdg.Text = ""
        ElseIf (ds.Tables(0).Rows.Count < 1) Then
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            btn_ShowUniqueRecords.Visible = False
            btn_ExportUniqueList.Visible = False
            DG_Unique.Visible = False
            lblGrdVolResultsHdg.Text = ""
        Else
            displayErrorMessage("")
            Dim i As Integer = ds.Tables(0).Rows.Count
            grdVolResults.Visible = True
            grdVolResults.DataSource = ds.Tables(0)
            grdVolResults.DataBind()
            ' lblError.Visible = False
            'tblMessage.Visible = True
            tblVolRoleResults.Visible = True
            btn_ShowUniqueRecords.Visible = True
            lblGrdVolResultsHdg.Text = "2. List of <u>'" + Session("searchRoleCode") + "'</u>"
        End If
        'hLinkAddNewVolunteer.Visible = True
        displaySearchCriteria()

    End Sub
    Protected Function getEventId(ByVal strEventCode As String) As Integer
        Dim eventId As Integer

        If (strEventCode = "Chapter") Then
            eventId = 2
        ElseIf (strEventCode = "Finals") Then
            eventId = 1
        ElseIf (strEventCode = "WkShop") Then
            eventId = 3
        End If
        Return eventId
    End Function
    Protected Sub grdVolResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdVolResults.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim volunteerId As Integer
            Dim automemberid As Integer
            volunteerId = CInt(e.Item.Cells(2).Text)
            automemberid = CInt(e.Item.Cells(3).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "delete from volunteer where volunteerid=" + CStr(volunteerId))
            Catch se As SqlException
                displayErrorMessage("Error: while deleting the record")
                Return
            End Try
            loadGrid(True)
        End If
    End Sub
    Private Sub grdVolResults_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdVolResults.ItemCreated
        'If (isDebug = True) Then
        '    Response.Write("<br>itemcreated")
        'End If
        Dim loginRoleId As Integer
        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        If (roleCat Is Nothing) Then
            Return
        End If

        loginRoleId = getLoginRoleId()

        If (loginRoleId > 0) Then
            If (loginRoleId > 2) Then
                'grdVolResults.Columns(6).Visible = False 'TeamLead
                grdVolResults.Columns(14).Visible = False 'AgentFlag
                grdVolResults.Columns(15).Visible = False 'WriteAccess
                grdVolResults.Columns(16).Visible = False 'Authorization
                grdVolResults.Columns(17).Visible = False 'IndiaChapterName
            End If

            grdVolResults.Columns(8).Visible = True 'EventYear
            grdVolResults.Columns(7).Visible = False
            Select Case roleCat
                Case "Finals"
                    grdVolResults.Columns(7).Visible = True 'eventcode                   
                Case "Zonal"
                    grdVolResults.Columns(7).Visible = False

                Case "Cluster"
                    grdVolResults.Columns(7).Visible = False

                Case "Chapter"
                    grdVolResults.Columns(7).Visible = True 'eventcode

                Case "IndiaChapter"
                    grdVolResults.Columns(7).Visible = False 'eventcode
                Case Nothing
                    showTimeOutMsg()
                    Return
            End Select
        End If
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim btn As LinkButton = Nothing
        End If
    End Sub
    Protected Sub grdVolResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdVolResults.PageIndexChanged
        grdVolResults.CurrentPageIndex = e.NewPageIndex
        grdVolResults.EditItemIndex = -1
        loadGrid(False)
    End Sub
    Private Sub LoadRoleDropDownList(ByRef ddl As DropDownList)
        Dim dsTarget As New DataSet
        Dim whereStr As String = ""
        Dim roleCat As String = getRoleSearchCategory() 'Session("searchRoleCategory").ToString
        Dim loginRoleId As Integer = getLoginRoleId()

        If (Not roleCat Is Nothing) Then
            Select Case roleCat
                Case "Admin"
                    whereStr = " where [National]='Y'"
                Case "National"
                    whereStr = " where [National]='Y' "
                Case "Zonal"
                    whereStr = " where Zonal='Y'"
                Case "Cluster"
                    whereStr = " where Cluster='Y'"
                Case "Chapter"
                    whereStr = " where Chapter='Y'"
            End Select

            If (loginRoleId <= 5) Then
                whereStr = whereStr + " and roleid >" + CStr(loginRoleId)
            End If

            dsTarget = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_getRolesWhere", New SqlParameter("@whereCondition", whereStr))
            If Not ddl Is Nothing Then
                ddl.DataValueField = "RoleId"
                ddl.DataTextField = "Selection"
                ddl.DataSource = dsTarget.Tables(0)
                ddl.DataBind()
            End If
        Else : Return

        End If
    End Sub
    Private Sub LoadProductGroupDropDownList(ByRef ddl As DropDownList, ByVal eventId As Integer, ByVal productGroupId As Integer)
        If (Not Session("volRoleId") Is Nothing) Then
            Dim loginRole As Integer = CInt(Session("volRoleId").ToString)
            If (loginRole < 0) Then
                ddl.Enabled = False
            End If
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_getProductGroupByEvent", New SqlParameter("@eventId", eventId))
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "ProductGroupId"
            ddl.DataTextField = "Name"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()

            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Insert(0, blankListItem)

            If (productGroupId > 0) Then
                ddl.SelectedValue = productGroupId
            Else
                ddl.SelectedIndex = -1
            End If

        End If
    End Sub

    Private Sub LoadProductDropDownList(ByRef ddl As DropDownList, ByVal productGroupId As Integer, ByVal eventId As Integer, ByVal productId As Integer)
        If (Not Session("volRoleId") Is Nothing) Then
            Dim loginRole As Integer = CInt(Session("volRoleId").ToString)
            If (loginRole < 0) Then
                ddl.Enabled = False
            End If
        End If
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ProductGroupId", productGroupId)
        params(1) = New SqlParameter("@EventId", eventId)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_getProductByGroupAndEvent", params)
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "ProductId"
            ddl.DataTextField = "Name"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()
            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Insert(0, blankListItem)
            If (productId > 0) Then
                ddl.SelectedItem.Value = productId
            Else
                ddl.SelectedValue = -1
            End If
        End If
    End Sub
    Private Sub LoadEventCodeDropDownList(ByRef ddl As DropDownList, ByVal eventId As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select eventid,eventcode from event")
        If (ds.Tables.Count > 0 And (Not ddl Is Nothing)) Then
            ddl.DataValueField = "eventId"
            ddl.DataTextField = "eventCode"
            ddl.DataSource = ds.Tables(0)
            ddl.DataBind()
            Dim blankListItem As New ListItem
            blankListItem.Value = -1
            blankListItem.Text = ""
            ddl.Items.Add(blankListItem)
            If (eventId > 0) Then
                ddl.SelectedItem.Value = eventId
            Else
                ddl.SelectedValue = -1
            End If
        End If
    End Sub
    Public Sub EventCode_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
        'If (isDebug = True) Then
        '    Response.Write("<br>event_code_oninit")
        'End If
        Dim searchRoleCategory As String
        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            Dim rowEventId As Integer
            Dim rowEventCode As String = ""
            Dim dr As DataRow = CType(Session("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("EventId") Is DBNull.Value) Then
                    rowEventId = dr.Item("EventId")
                End If
            End If

            If (searchRoleCategory = "Chapter") Then
                LoadEventCodeDropDownList(sender, rowEventId)
            Else
                Dim ddl As DropDownList = CType(sender, DropDownList)
                Dim item As ListItem = New ListItem
                item.Value = rowEventId
                item.Text = rowEventCode
                ddl.Enabled = False
                ddl.Items.Insert(0, item)
            End If
        Else : Return
        End If

    End Sub
    Public Sub Role_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadRoleDropDownList(sender)
    End Sub
    Public Sub Role_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        'If (isDebug = True) Then
        '    Response.Write("<br>role_prerender")
        'End If
        Dim rowRoleCode As String = ""
        Dim rowRoleId As Integer
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            'If (Not dr.Item("Selection") Is DBNull.Value) Then
            '    rowRoleCode = dr.Item("Selection")
            'End If
            If (Not dr.Item("roleId") Is DBNull.Value) Then
                rowRoleCode = dr.Item("Selection")
                rowRoleId = dr.Item("roleId")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowRoleCode")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRoleId))
            'mRowRoleId = ddlTemp.SelectedValue
            'Session("rowRoleId") = ddlTemp.SelectedValue
        Else
            showTimeOutMsg()
            Return
        End If
    End Sub
    Public Sub Role_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList

        ddlTemp = sender
        'Session("rowRoleId") = ddlTemp.SelectedValue
        'Session("rowRoleCode") = ddlTemp.SelectedItem.Text
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("roleId") = ddlTemp.SelectedValue
            dr.Item("selection") = ddlTemp.SelectedItem.Text
            Session("editRow") = dr
        Else
            Return
        End If

    End Sub
    Public Sub lblEventCode2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim searchRoleCategory As String
        Dim lbl As Label

        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            lbl = CType(sender, Label)
            If (searchRoleCategory = "Chapter") Then
                lbl.Visible = False
            Else
                lbl.Visible = True
            End If
        Else : Return
        End If

    End Sub
    Public Sub EventCode_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim searchRoleCategory As String
        Dim rowEventId As Integer = 0
        Dim rowEventCode As String = ""

        If (Not Session("searchRoleCategory") Is Nothing) Then
            searchRoleCategory = Session("searchRoleCategory").ToString
            Dim dr As DataRow = CType(Session("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("EventId") Is DBNull.Value) Then
                    rowEventId = dr.Item("EventId")
                End If
            End If
            If (searchRoleCategory = "Chapter") Then
                LoadEventCodeDropDownList(sender, rowEventId)
            Else
                Dim ddl As DropDownList = CType(sender, DropDownList)
                ddl.Visible = False
            End If
        Else : Return
        End If
    End Sub
    Public Sub EventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        'If (isDebug = True) Then
        '    Response.Write("<br>event_year prerender")
        'End If
        Dim rowEventYear As Integer = 0
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventYear") Is DBNull.Value) Then
                rowEventYear = dr.Item("EventYear")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowEventYear")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowEventYear))
        Else
            Return
        End If
    End Sub
    Public Sub ProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>PG prerender")
        End If
        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0

        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("RoleId") Is DBNull.Value) Then
            rowRoleId = dr.Item("RoleId")
        End If
        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (ddlTemp.Items.Count = 0) Then
            LoadProductGroupDropDownList(ddlTemp, rowEventId, 0)  'if empty, load
        End If
        ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowProductGroup")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowProdGroupCode))
        'mProductGroupId = ddlTemp.SelectedValue
        'Session("rowProductGroup") = ddlTemp.SelectedValue

        'If (Session("rowRoleId") < 8) Then
        If rowRoleId < 8 Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
    End Sub
    Public Sub ProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>PG selectedIndexChanged")
        End If
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""

        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductGroupCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductGroupId") = ddlTemp.SelectedItem.Value
        Session("editRow") = dr

        isProductGroupSelectionChanged = True

    End Sub
    Public Sub Product_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>product prerender")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0
        Dim rowProductCode As String = ""

        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("RoleId") Is DBNull.Value) Then
            rowRoleId = dr.Item("RoleId")
        End If
        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If
        If (Not dr.Item("ProductCode") Is DBNull.Value) Then
            rowProductCode = dr.Item("productCode")
        End If

        If (rowProdGroupId > 0) Then
            LoadProductDropDownList(ddlTemp, rowProdGroupId, rowEventId, 0)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowProductCode))
        Else
            ddlTemp.Items.Clear()
        End If

        If (rowRoleId < 8) Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If

    End Sub
    Public Sub IndiaChapter_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>IC Prerender")
        End If
        Dim indiaChapterName As String = ""
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("IndiaChapterName") Is DBNull.Value) Then
            indiaChapterName = dr.Item("IndiaChapterName")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowIndiaChapter")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(indiaChapterName))
    End Sub

    Public Sub AgentFlag_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>AgentFlag Prerender")
        End If
        Dim rowAgentFlag As String = ""
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("AgentFlag") Is DBNull.Value) Then
            rowAgentFlag = dr.Item("AgentFlag")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowAgentFlag")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowAgentFlag))
    End Sub
    Public Sub WriteAccess_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>WriteAccess Prerender")
        End If
        Dim rowWriteAccess As String = ""
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("WriteAccess") Is DBNull.Value) Then
            rowWriteAccess = dr.Item("WriteAccess")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowWriteAccess")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowWriteAccess))
    End Sub
    Public Sub Authorization_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>Authorization Prerender")
        End If
        Dim rowAuthorization As String = ""
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("authorization") Is DBNull.Value) Then
            rowAuthorization = dr.Item("Authorization")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowAuthorization")))
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowAuthorization))
    End Sub
    Public Sub TeamLead_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        If (isDebug = True) Then
            Response.Write("<br>Team Lead Prerender")
        End If
        Dim rowTeamLead As String = "N"
        Dim dr As DataRow = CType(Session("editRow"), DataRow)
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("TeamLead") Is DBNull.Value) Then
            rowTeamLead = dr.Item("TeamLead")
        End If

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("rowTeamLead")))
        If (rowTeamLead.Trim = "") Then
            rowTeamLead = "N"
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowTeamLead))
    End Sub
    Sub PagerButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim arg As String = sender.CommandArgument
        Select Case arg
            Case "Next"
                If DG_Unique.Visible = True Then
                    If (DG_Unique.CurrentPageIndex < (DG_Unique.PageCount - 1)) Then
                        DG_Unique.CurrentPageIndex += 1
                    End If
                Else
                    If (grdVolResults.CurrentPageIndex < (grdVolResults.PageCount - 1)) Then
                        grdVolResults.CurrentPageIndex += 1
                    End If
                End If
            Case "Prev"
                If DG_Unique.Visible = True Then
                    If (DG_Unique.CurrentPageIndex > 0) Then
                        DG_Unique.CurrentPageIndex -= 1
                    End If
                Else
                    If (grdVolResults.CurrentPageIndex > 0) Then
                        grdVolResults.CurrentPageIndex -= 1
                    End If
                End If
            Case "Last"
                If DG_Unique.Visible = True Then
                    DG_Unique.CurrentPageIndex = (grdVolResults.PageCount - 1)
                Else
                    grdVolResults.CurrentPageIndex = (grdVolResults.PageCount - 1)
                End If
            Case Else
                If DG_Unique.Visible = True Then
                    DG_Unique.CurrentPageIndex = Convert.ToInt32(arg)
                Else
                    grdVolResults.CurrentPageIndex = Convert.ToInt32(arg)
                End If
        End Select
        If DG_Unique.Visible = True Then
            LoadUniqueGrid(DG_Unique, False)
        Else
            loadGrid(False)
        End If


    End Sub

    Protected Sub ddWeekOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddWeekOf.SelectedIndexChanged

        clearGrid()
        Dim loginZoneId As Integer = 0
        If DdlZonalCoordinator.SelectedValue = 0 Then
            loginZoneId = getSessionSelectionID("Zone")
        Else
            loginZoneId = DdlZonalCoordinator.SelectedValue
        End If

        ddlChapter.SelectedIndex = -1

        If ddWeekOf.SelectedIndex <> 0 Then
            loadChapterDropDown(ddlChapter, loginZoneId, 0, getSessionSelectionID("chapter"), ddWeekOf.SelectedValue)
        End If
    End Sub

    'Protected Sub ddlList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlList.SelectedIndexChanged
    '    clearGrid()
    'End Sub

    Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport1.Click
        ' Export all the details
        Try
            ' Get the datatable to export			

            Dim dtEmployee As DataTable = (CType(Session("volDataSet"), DataSet)).Tables(0).Copy()

            ' Specify the column list and headers to export
            Dim iColumns() As Integer = New Integer() {36, 37, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 32, 33, 34, 35}
            Dim sHeaders() As String = New String() {"FistName", "LastName", "VolunteerId", "MemberId", "RoleId", "Selection", "TeamLead", "EventYear", "EventId", "EventCode", "State", "ChapterId", "ChapterCode", "National", "ZoneId", "ZoneCode", "ClusterId", "ClusterCode", "Finals", "IndiaChapter", "ProductGroupID", "ProductGroupCode", "ProductId", "ProductCode", "Email", "CPhone", "HPhone", "WPhone"}

            ' Export all the details to CSV
            Dim objExport As New RKLib.ExportData.Export("Web")
            objExport.ExportDetails(dtEmployee, iColumns, sHeaders, Export.ExportFormat.CSV, "ContactList_" & Session("searchRoleCode") & "_" & Now.Date & ".csv")
        Catch Ex As Exception
            lblError.Text = Ex.Message
        End Try

    End Sub

    Protected Sub btnEmailExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailExport.Click
        Try

            Dim appType As String = "web"
            Dim response_ As System.Web.HttpResponse
            response_ = System.Web.HttpContext.Current.Response()
            'Appending Headers
            response_.Clear()
            response_.Buffer = True

            response_.ContentType = "text"
            response_.AppendHeader("content-disposition", "attachment; filename=ContestEmail.txt")

            Dim Stream As MemoryStream = New MemoryStream()
            Dim writer As System.Xml.XmlTextWriter = New System.Xml.XmlTextWriter(Stream, Encoding.UTF8)

            writer.Flush()
            Stream.Seek(0, SeekOrigin.Begin)
            ' Get the datatable to export			
            Dim dtEmployee As DataTable = (CType(Session("volDataSet"), DataSet)).Tables(0).Copy()

            Dim sw As System.IO.StringWriter = New System.IO.StringWriter()
            ' Specify the column list to export
            Dim iColumns() As Integer = New Integer() {30}
            Dim i As Integer
            Dim strEmail As String
            For i = 0 To dtEmployee.Rows.Count
                Try
                    strEmail = dtEmployee.Rows(i)("Email")
                    If strEmail = "" Or strEmail = Nothing Then
                        strEmail = ""
                    End If
                Catch
                    strEmail = ""
                End Try
                If strEmail <> "" Then
                    sw.Write(strEmail)
                    sw.Write(",")
                End If
            Next

            'Writeout the Content				
            response_.Write(sw.ToString())
            sw.Close()
            writer.Close()
            Stream.Close()
            response_.End()


            ' Export the details of specified columns to Excel
            ' Dim objExport As New RKLib.ExportData.Export("Web")
            'objExport.ExportDetails(dtEmployee, iColumns, Export.ExportFormat.Excel, "EmployeesInfo2.xls")
        Catch Ex As Exception
            lblError.Text = Ex.Message
        End Try
    End Sub
    Private Sub LoadClusterChapters()
        If (DdlCluster.SelectedIndex <> 0) Then
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChaWithinClustersDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlZonalCoordinator.SelectedIndex = -1
            DdlZonalCoordinator.Items.FindByText("[Select Zone]").Selected = True
        Else
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChapInZonesDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlCluster.SelectedIndex = -1
            DdlCluster.Items.FindByText("[Select Cluster]").Selected = True
        End If
    End Sub
    Protected Sub Cluster_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DdlCluster.SelectedIndexChanged
        LoadClusterChapters()
    End Sub
    Private Sub LoadZonalChapters()
        If (DdlZonalCoordinator.SelectedIndex <> 0) Then
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChapInZonesDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlCluster.SelectedIndex = -1
            DdlCluster.Items.FindByText("[Select Cluster]").Selected = True
        End If
    End Sub
    Protected Sub DdlZonalCoordinator_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DdlZonalCoordinator.SelectedIndexChanged

        LoadZonalChapters()
    End Sub

    Protected Sub ddlCoach_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadLevel(DDLLevel, DDLProductGroupCode.SelectedItem.Text)
        ddlCyear_SelectedIndexChanged(ddlCYear, e)
        FillProductGroup()
        FillProductCode()

        'loadChildren()
    End Sub
    Private Sub loadChildren()
        If Not ddlCoach.SelectedItem.Text = "Select Coach Name" Then

            Try
                Dim SQLStr As String = "select ROW_NUMBER() Over (ORDER BY Co.LastName,Co.FirstName,P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, I1.Email as Email ,I1.Career as JobTitle,Ch.Email as ChildEmail,CH.OnlineClassEmail,CR.Approved,CR.SessionNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,Ch.FIRST_NAME, Ch.LAST_NAME ,CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status, CR.AttendeeJoinURL, C.MeetingKey, C.UserID, C.Pwd,C.[Begin],C.Day "
                ' If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "96" Then
                SQLStr = SQLStr & ", Co.FirstName + ' ' + Co.LastName CoacherName "
                'Else
                'SQLStr = SQLStr & ", CoacherName "
                'End If

                SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Left Join IndSpouse Co on Co.AutomemberId=CR.CMemberId INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON " 'I1.Email';' as Email,
                SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlCYear.Text & " where " ' CR.CMemberid=" & ddlCoach.SelectedValue & " " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
                If ddlCoach.SelectedItem.Text = "[ALL]" Then
                    SQLStr = SQLStr & " CR.CMemberid is not null "
                    GVCoaching.Columns(1).Visible = True
                Else
                    SQLStr = SQLStr & " CR.CMemberid=" & ddlCoach.SelectedValue
                    GVCoaching.Columns(1).Visible = False
                End If
                If ddlSessionNo.SelectedValue > 0 Then
                    SQLStr = SQLStr & " AND CR.SessionNo= " & ddlSessionNo.SelectedValue
                End If
                If DDLLevel.SelectedValue <> "Select" And DDLLevel.SelectedValue <> "" And DDLLevel.SelectedValue <> "0" Then
                    SQLStr = SQLStr & " AND CR.Level= '" & DDLLevel.SelectedValue & "'"
                End If
                If DDLProductGroupCode.SelectedValue = "All" And DDLProductCode.SelectedValue = "All" Then

                ElseIf DDLProductGroupCode.SelectedValue <> "All" And DDLProductCode.SelectedValue = "All" Then
                    SQLStr = SQLStr & " AND CR.ProductGroupID= " & DDLProductGroupCode.SelectedValue
                ElseIf DDLProductGroupCode.SelectedValue <> "All" And DDLProductCode.SelectedValue <> "All" Then
                    SQLStr = SQLStr & "  AND CR.ProductGroupID= " & DDLProductGroupCode.SelectedValue & " AND CR.ProductID= " & DDLProductCode.SelectedValue & ""
                End If
                SQLStr = SQLStr & " AND CR.Phase=" & ddlPhase.SelectedValue & " AND CR.Approved='Y' and C.Accepted='Y' ORDER BY  "

                SQLStr = SQLStr & " C.Level,C.Day,Co.LastName,Co.FirstName,P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"


                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)

                If ds.Tables(0).Rows.Count > 0 Then
                    'DDLLevel.Items.Clear()
                    tblviewchldgrid.Visible = True
                    GVCoaching.DataSource = ds
                    GVCoaching.DataBind()
                    lblCoachMsg.Text = ""
                    lblCoachMsg.Visible = False
                    tblMessage.Visible = False
                    btnChildrenDetail.Visible = True
                    btnChildrenDetWebex.Visible = True
                    Dim Level As String = String.Empty
                    Dim count As Integer = 0
                    'DDLLevel.Items.Insert(count, "Select")
                    'For Each dr In ds.Tables(0).Rows


                    '    If (Level <> dr("Level").ToString()) Then
                    '        count = count + 1
                    '        DDLLevel.Items.Insert(count, dr("Level").ToString())

                    '    End If
                    '    Level = dr("Level").ToString()
                    'Next
                Else
                    btnChildrenDetail.Visible = False
                    btnChildrenDetWebex.Visible = False
                    tblviewchldgrid.Visible = False
                    lblCoachMsg.Text = "Sorry No Child found"
                    lblCoachMsg.Visible = True
                    tblMessage.Visible = True
                    ' DDLLevel.Items.Clear()
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        Else
            lblCoachMsg.Text = "Please select Valid Coach"
            lblCoachMsg.Visible = True
            tblMessage.Visible = True
        End If
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildrenDetail.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CoachingChildrenDetails.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Response.Write("<br>Contacts<br>")
        GVCoaching.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())

        Response.[End]()
    End Sub


    Protected Sub btnExport2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport2.Click
        Try
            ' Get the datatable to export			

            Dim dtEmployee As DataTable = (CType(Session("volDataSet"), DataSet)).Tables(0).Copy()
            'First Name , LastName , Chapter Code,  Street (Address1) , City, State, Zip Code, Home Phone, Cell Phone, Work Phone, MemberID, ChapterID
            ' Specify the column list and headers to export

            Dim iColumns() As Integer = New Integer() {36, 37, 10, 11, 38, 39, 40, 41, 32, 33, 34, 35, 2, 42, 43}
            Dim sHeaders() As String = New String() {"FistName", "LastName", "Assigned_ChapterID", "Assigned_ChapterCode", "Street", "City", "State", "ZipCode", "Email", "CPhone", "HPhone", "WPhone", "MemberId", "ChapterId", "ChapterCode"}

            ' Export all the details to CSV
            Dim objExport As New RKLib.ExportData.Export("Web")
            objExport.ExportDetails(dtEmployee, iColumns, sHeaders, Export.ExportFormat.CSV, "nsfChaptersInfo1.csv")
        Catch Ex As Exception
            lblError.Text = Ex.Message
        End Try
    End Sub

    Protected Sub ddlSessionNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadChildren()
    End Sub
    Protected Sub DDLLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadChildren()
    End Sub

    Protected Sub btn_ShowUniqueRecords_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ShowUniqueRecords.Click
        If btn_ShowUniqueRecords.Text = "Hide Unique Records" Then
            DG_Unique.Visible = False
            btn_ExportUniqueList.Visible = False
            btn_ShowUniqueRecords.Text = "Show Unique Records"
            lblGrdVolResultsHdg.Text = "2. List of <u>'" + Session("searchRoleCode") + " '</u>"
            btnExport1.Enabled = True
            'btnExport2.Visible = True
            btnExport2.Enabled = True
            loadGrid(True)
            grdVolResults.Visible = True
        Else
            btnExport1.Enabled = False
            btnExport2.Enabled = False
            grdVolResults.Visible = False
            LoadUniqueGrid(DG_Unique, True)
        End If
    End Sub
    Private Sub LoadUniqueGrid(ByVal DG_Unique As DataGrid, ByVal blnReload As Boolean)
        Try
            Dim ProductIDs As String = ""
            Dim ProductGroupIDs As String = ""
            Dim Str As String = ""
            Dim ds As DataSet
            Dim WeekOf As String = ""
            If ddlRoleCatSch.SelectedValue = "Chapter" Then
                If ddWeekOf.SelectedIndex = 0 Then
                    WeekOf = ""
                Else
                    WeekOf = ddWeekOf.SelectedValue
                End If

            Else
                WeekOf = ""
            End If

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (blnReload = True) Then
                'Response.Write(cmd.CommandText)
                Session("volDataSet") = Nothing
                If Request.QueryString("coachflag") = 1 And Session("RoleId").ToString() = "89" Then
                    If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                        For i As Integer = 1 To ddlProductGroup.Items.Count - 1
                            ProductGroupIDs = ProductGroupIDs + ddlProductGroup.Items(i).Value + ","
                        Next
                        ProductGroupIDs = ProductGroupIDs.ToString.TrimEnd(",")
                    Else
                        ProductGroupIDs = ddlProductGroup.SelectedValue
                        If ddlProduct.Items.Count > 0 Then
                            If ddlProduct.SelectedIndex = 0 Then
                                For i As Integer = 1 To ddlProduct.Items.Count - 1
                                    ProductIDs = ProductIDs + ddlProduct.Items(i).Value + ","
                                Next
                                ProductIDs = ProductIDs.ToString.TrimEnd(",")
                            Else
                                ProductIDs = ddlProduct.SelectedValue
                            End If
                        End If
                    End If

                    '*******MODIFIED ON 11-02-2014 to EXPORT STATE,CHAPTER,ZONE AND CLUSTER DETAILS**********************'
                    Str = " SELECT Distinct dbo.ufn_getMemberNameByID(v.memberid) as Name,v.[MemberId]," 'v.[RoleId], dbo.ufn_getRoleCode(v.[RoleId]) as Selection ,"
                    'Str = Str & " case v.TeamLead when null then 'N' else v.TeamLead end as TeamLead , " 'v.[EventYear] ,"
                    Str = Str & " v.[EventId] , v.[EventCode] ,I.State, I.[ChapterId] ,I.Chapter as [ChapterCode] ,  Ch.[ZoneId] , Ch.[ZoneCode] , Ch.[ClusterId] , "
                    Str = Str & " Ch.[ClusterCode] , v.[IndiaChapter] ,isnull(v.AgentFlag,'N') as AgentFlag ," ',v.[National]  , v.[Finals]
                    Str = Str & " isnull(v.WriteAccess,'N') as WriteAccess , "
                    Str = Str & " isnull(v.[Authorization],'N') as 'Authorization' , v.[Yahoogroup] , v.[IndiaChapterName] , "
                    Str = Str & " I.Email,I.CPhone,I. HPhone, I.WPhone , "
                    Str = Str & " dbo.ufn_GetMemberFirstNameByID( v.memberid) as FistName, dbo.ufn_GetMemberLastNameByID( v.memberid) as LastName,I.Address1 as Street,I.City,I.State,I.Zip as ZipCode, I.ChapterID as VChapterID,  I.Chapter as VChapterCode FROM [Volunteer] v "
                    Str = Str & " Inner Join CalSignUp C on V.MemberId = C.MemberID and V.RoleId in (" & lbllRoleIds.Text & ") and C.Accepted='Y'"
                    Str = Str & " Inner join CoachReg CR on CR.CMemberId= V.MemberID and CR.EventYear = C.EventYear and CR.Approved='Y' "
                    Str = Str & " Inner Join IndSpouse I on v.MemberId =I.AutoMemberID"
                    Str = Str & " Inner Join Chapter Ch on Ch.ChapterID =I.ChapterID "
                    Str = Str & " where  C.ProductGroupId in(" & ProductGroupIDs & ") " & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in (" & ProductIDs & ")", "") '& ddlProduct.SelectedValue & ")" 'and V.RoleId = 88 
                    'Str = Str & " and ((Case when C.EventYear> =2012 Then C.Accepted End)='Y' Or (Case When C.EventYear <2012 then C.Accepted End) is null)"
                    Str = Str & " order by dbo.ufn_getMemberLastNameByID(V.memberid)" 'v.[EventYear] Desc,roleid,  "
                    'Response.Write(Str)
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Str)

                ElseIf Request.QueryString("coachflag") = 1 Then
                    'If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                    '    lblerr.Text = "Please Select Product Group"
                    '    Exit Sub
                    'End If
                    '*******MODIFIED ON 11-02-2014 **********************************************'
                    If ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0 Then
                        For i As Integer = 1 To ddlProductGroup.Items.Count - 1
                            ProductGroupIDs = ProductGroupIDs + ddlProductGroup.Items(i).Value + ","
                        Next
                        ProductGroupIDs = ProductGroupIDs.ToString.TrimEnd(",")
                    Else
                        ProductGroupIDs = ddlProductGroup.SelectedValue

                        'If ddlProduct.Items.Count = 0 Or ddlProduct.SelectedItem.Text = "Select Product" Then
                        '    lblerr.Text = "Sorry, No product Selected to load"
                        '    Exit Sub
                        'End If
                        If ddlProduct.Items.Count > 0 Then
                            If ddlProduct.SelectedIndex = 0 Then
                                For i As Integer = 1 To ddlProduct.Items.Count - 1
                                    ProductIDs = ProductIDs + ddlProduct.Items(i).Value + ","
                                Next
                                ProductIDs = ProductIDs.ToString.TrimEnd(",")
                            Else
                                ProductIDs = ddlProduct.SelectedValue
                            End If
                        End If
                    End If
                    '*******MODIFIED ON 11-02-2014 to EXPORT STATE,CHAPTER,ZONE AND CLUSTER DETAILS**********************'

                    Str = " SELECT Distinct dbo.ufn_getMemberNameByID(v.memberid) as Name, v.[MemberId] , " 'v.[RoleId] , dbo.ufn_getRoleCode(v.[RoleId]) as Selection,"
                    'Str = Str & " case v.TeamLead when null then 'N' else v.TeamLead end as TeamLead," 'v.[EventYear],
                    Str = Str & " v.[EventId],v.[EventCode] , I.State,I.[ChapterId] ,  I.Chapter as [ChapterCode] ,  Ch.[ZoneId] ,Ch.[ZoneCode] , "
                    Str = Str & " Ch.[ClusterId] , Ch.[ClusterCode] , v.[IndiaChapter] , isnull(v.AgentFlag,'N') as AgentFlag , isnull(v.WriteAccess,'N') as WriteAccess ," ',v.[National] , v.[Finals]
                    Str = Str & " isnull(v.[Authorization],'N') as 'Authorization' , v.[Yahoogroup] , v.[IndiaChapterName] , "
                    Str = Str & " I.Email, I.CPhone,I.HPhone, I.WPhone, dbo.ufn_GetMemberFirstNameByID( v.memberid) as FistName, "
                    Str = Str & " dbo.ufn_GetMemberLastNameByID( v.memberid) as LastName, I.Address1 as Street,I.City,I.State,I.Zip as ZipCode, I.ChapterID as VChapterID,  I.Chapter as VChapterCode FROM [Volunteer] v "
                    Str = Str & " Left Join CalSignUp C on V.MemberId = C.MemberID and V.RoleId in(" & lbllRoleIds.Text & " )  and C.Accepted='Y' "
                    Str = Str & " and C.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in  (" & ProductIDs & ")", "")
                    Str = Str & " Inner join CoachReg CR on CR.CMemberId= V.MemberID and CR.EventYear = C.EventYear and CR.Approved='Y'"
                    Str = Str & " Inner Join IndSpouse I on I.AutoMemberID =V.MemberId "
                    Str = Str & " Inner Join Chapter Ch on Ch.ChapterID =I.ChapterID "
                    Str = Str & " where V.RoleId in  (" & lbllRoleIds.Text & ") and ((V.RoleID=89 and V.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and V.ProductID in  (" & ProductIDs & ")", "") & ") or  (V.RoleID=88 and C.ProductGroupId in(" & ProductGroupIDs & ")" & IIf(ddlProductGroup.SelectedValue > 0, " and C.ProductID in  (" & ProductIDs & ")", "") & "))" 'and V.RoleId = 88  " '(" & ddlProduct.SelectedValue & ")
                    'Str = Str & " and ((Case when C.EventYear> =2012 Then C.Accepted End)='Y' Or (Case When C.EventYear <2012 then C.Accepted End) is null)"
                    Str = Str & " order by dbo.ufn_getMemberLastNameByID(V.memberid)" 'v.[EventYear] Desc,roleid,  "

                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Str)

                Else
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.usp_SelectVolunteerList_Unique", New SqlParameter("@wherecondition", Session("volRoleSearchSql").ToString()), New SqlParameter("@ChapterWeek", WeekOf))
                End If
                Session("volDataSet") = ds
                DG_Unique.CurrentPageIndex = 0
                'Response.Write(Str)
            Else
                If (Not Session("volDataSet") Is Nothing) Then
                    ds = CType(Session("volDataSet"), DataSet)
                End If
            End If
            'If ds.Tables(0).Rows.Count > 0 Then
            '    DG_Unique.DataSource = ds
            '    DG_Unique.DataBind()
            'End If

            If ds.Tables(0).Rows.Count < 1 Then
                displayErrorMessage("No records matching your criteria ")
                DG_Unique.Visible = False
                btn_ExportUniqueList.Visible = False
            Else
                displayErrorMessage("")
                btn_ExportUniqueList.Visible = True
                btn_ShowUniqueRecords.Text = "Hide Unique Records"
                Dim i As Integer = ds.Tables(0).Rows.Count
                DG_Unique.Visible = True
                DG_Unique.DataSource = ds.Tables(0)
                DG_Unique.DataBind()
                lblGrdVolResultsHdg.Text = "2. List of <u>' Unique " + Session("searchRoleCode") + " '</u>"
            End If
            'hLinkAddNewVolunteer.Visible = True
            displaySearchCriteria()
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub DG_Unique_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DG_Unique.PageIndexChanged
        DG_Unique.CurrentPageIndex = e.NewPageIndex
        DG_Unique.EditItemIndex = -1
        LoadUniqueGrid(DG_Unique, False)

    End Sub
    Protected Sub btn_ExportUniqueList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ExportUniqueList.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CoachContactList_Unique.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim DG_Unique As New DataGrid
        LoadUniqueGrid(DG_Unique, True)
        DG_Unique.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub btnExportWebex_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildrenDetWebex.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=CoachingChildrenDetails_Zoom.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Response.Write("<br>Contacts<br>")

        Dim SQLStr As String = ""
        'SQLStr = SQLStr & "select '' as UUID ,  ,'' as JobTitle,Ch.Email as ChildEmail,ch.pwd,CR.Approved,CR.SessionNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME, Ch.LAST_NAME ,CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status "
        SQLStr = SQLStr & "Select Distinct '' as UUID,Ch.LAST_NAME, Ch.FIRST_NAME,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS Name,I1.Email as Email,'' as Company,'' as JobTitle,'' as URL,1 as OffCntry,'' as OffLocal,'' as CellCntry,'' as CellLocal,'' as	FaxCntry,'' as	FaxLocal,'' as	Address1,'' as	Address2,'' as	City,'' as	State_Or_Province,'' as Zip_Or_Postal,'' as Country,'' as	TimeZone,'' as	Language,'' as Locale,'' as UserName,'' as	Notes"
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON " 'I1.Email';' as Email,
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlCYear.Text & " where  CR.CMemberid=" & ddlCoach.SelectedValue & " " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        If ddlSessionNo.SelectedValue > 0 Then
            SQLStr = SQLStr & " AND CR.SessionNo= " & ddlSessionNo.SelectedValue
        End If

        If DDLProductGroupCode.SelectedValue = "All" And DDLProductCode.SelectedValue = "All" Then

        ElseIf DDLProductGroupCode.SelectedValue <> "All" And DDLProductCode.SelectedValue = "All" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & DDLProductGroupCode.SelectedValue
        ElseIf DDLProductGroupCode.SelectedValue <> "All" And DDLProductCode.SelectedValue <> "All" Then
            SQLStr = SQLStr & "  AND CR.ProductGroupID= " & DDLProductGroupCode.SelectedValue & " AND CR.ProductID= " & DDLProductCode.SelectedValue & ""
        End If

        SQLStr = SQLStr & " AND CR.Phase=" & ddlPhase.SelectedValue & " AND CR.Approved='Y' and C.Accepted='Y'" ' ORDER BY P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"
        SQLStr = SQLStr & " UNION ALL "
        'SQLStr = SQLStr & " select '' as UUID ,ROW_NUMBER() Over (ORDER BY P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo,Ch.Email  as Email,I1.Career as JobTitle,Ch.Email as ChildEmail,ch.pwd,CR.Approved,CR.SessionNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,Ch.FIRST_NAME, Ch.LAST_NAME, CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName, I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status "
        SQLStr = SQLStr & "select  Distinct '' as UUID,Ch.LAST_NAME, Ch.FIRST_NAME,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS Name,Ch.Email as Email,'' as Company,'' as JobTitle,'' as URL,1 as OffCntry,'' as OffLocal,'' as CellCntry,'' as CellLocal,'' as	FaxCntry,'' as	FaxLocal,'' as	Address1,'' as	Address2,'' as	City,'' as	State_Or_Province,'' as Zip_Or_Postal,'' as Country,'' as	TimeZone,'' as	Language,'' as Locale,'' as UserName,'' as	Notes"
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON " 'I1.Email+';' as Email,
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlCYear.Text & " where  CR.CMemberid=" & ddlCoach.SelectedValue & " " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        If ddlSessionNo.SelectedValue > 0 Then
            SQLStr = SQLStr & " AND CR.SessionNo= " & ddlSessionNo.SelectedValue
        End If
        SQLStr = SQLStr & " AND CR.Phase=" & ddlPhase.SelectedValue & " AND CR.Approved='Y' and C.Accepted='Y' and Ch.Email is not null ORDER BY LAST_NAME, FIRST_NAME" '" ' " ' P.,CR.,Ch.LAST_NAME,Ch.FIRST_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)
        Dim GVCoachingExp As GridView = New GridView()
        ds.Tables(0).Columns.RemoveAt(2)
        ds.Tables(0).Columns.RemoveAt(1) 'First Name,Last Name are removed in Export.
        'If ds.Tables(0).Rows.Count > 0 Then

        GVCoachingExp.DataSource = ds
        GVCoachingExp.DataBind()

        Dim SQLStrList As String = ""
        SQLStrList = SQLStrList & " select '' as DUID ,ROW_NUMBER() Over (ORDER BY CR.ProductCode, Ch.LAST_NAME,Ch.FIRST_NAME,Ch.ChildNumber) as SNo,I1.Email  as Email, ISNull(Ch.Email,'') as ChildEmail,Ch.FIRST_NAME + ' ' + Ch.Last_Name as ChildName,CR.ProductCode,Ch.LAST_NAME,ch.FIRST_NAME,Ch.ChildNumber,I.FirstName +' ' + (Select top 1 Name from Product where ProductID =CR.ProductID)+ ' ' + CR.Level + ' ' + 'List'  as ListName,I.FirstName +' ' + (Select top 1 Name from Product where ProductID =CR.ProductID)+ ' ' + CR.Level+ ' ' + 'List'   as ListDescription,'' as Members   "
        SQLStrList = SQLStrList & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON " 'I1.Email+';' as Email,
        SQLStrList = SQLStrList & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlCYear.Text & " where  CR.CMemberid=" & ddlCoach.SelectedValue & " " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        If ddlSessionNo.SelectedValue > 0 Then
            SQLStrList = SQLStrList & " AND CR.SessionNo= " & ddlSessionNo.SelectedValue
        End If
        SQLStrList = SQLStrList & " AND CR.Phase=" & ddlPhase.SelectedValue & " AND CR.Approved='Y' and C.Accepted='Y'  ORDER BY ProductCode, LAST_NAME,FIRST_NAME,ChildNumber " ' P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"

        Dim dsList As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStrList)
        Dim StrList As String = ""
        Dim ListName As String = ""
        Dim ListDescription As String = ""

        If dsList.Tables(0).Rows.Count > 0 Then
            ListName = dsList.Tables(0).Rows(0)("ListName")
            ListDescription = dsList.Tables(0).Rows(0)("ListDescription")
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                StrList = StrList & dsList.Tables(0).Rows(i)("ChildName") & "(" & dsList.Tables(0).Rows(i)("Email") & ");"
                If dsList.Tables(0).Rows(i)("ChildEmail") <> "" Then
                    StrList = StrList & dsList.Tables(0).Rows(i)("ChildName") & "(" & dsList.Tables(0).Rows(i)("ChildEmail") & ");"
                End If
            Next
        End If

        dsList.Tables(0).Rows(0)("ListName") = ListName
        dsList.Tables(0).Rows(0)("ListDescription") = ListDescription
        dsList.Tables(0).Rows(0)("Members") = StrList
        For k As Integer = 1 To 8
            dsList.Tables(0).Columns.RemoveAt(1)
        Next

        Dim GVDistributionList As GridView = New GridView()
        GVDistributionList.DataSource = dsList
        GVDistributionList.DataBind()
        For j As Integer = 1 To GVDistributionList.Rows.Count - 1
            GVDistributionList.Rows(j).Enabled = False
            GVDistributionList.Rows(j).Visible = False
        Next

        GVCoachingExp.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.Write("<br>Distribution List<br>")
        Dim stringWrite1 As New System.IO.StringWriter()
        Dim htmlWrite1 As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite1)
        GVDistributionList.RenderControl(htmlWrite1)
        Response.Write(stringWrite1.ToString())
        Response.[End]()
    End Sub

    Public Sub FillProductGroup()
        Dim CmdText As String = String.Empty
        Dim ds As DataSet = New DataSet()
        Dim memberID As Integer
        If ddlCoach.SelectedValue = "" Then
            memberID = 0
        Else
            memberID = ddlCoach.SelectedValue
        End If
        CmdText = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" & ddlCYear.SelectedValue & ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" & ddlCYear.SelectedValue & "' and MemberID=" & memberID & " and Accepted='Y')"
        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
        If ds.Tables.Count > 0 Then
            DDLProductGroupCode.DataTextField = "ProductGroupCode"
            DDLProductGroupCode.DataValueField = "ProductGroupId"
            DDLProductGroupCode.DataSource = ds
            DDLProductGroupCode.DataBind()
            DDLProductGroupCode.Items.Insert(0, "All")
            DDLProductGroupCode.Items(0).Selected = True
            LoadLevel(DDLLevel, DDLProductGroupCode.SelectedItem.Text)
        End If
    End Sub
    Public Sub FillProductCode()
        Dim CmdText As String = String.Empty
        Dim memberID As Integer
        If ddlCoach.SelectedValue = "" Then
            memberID = 0
        Else
            memberID = ddlCoach.SelectedValue
        End If
        Dim ds As DataSet = New DataSet()
        If DDLProductGroupCode.SelectedValue = "All" Then
            DDLProductCode.Items.Clear()
            DDLProductCode.Items.Insert(0, "All")
            DDLProductCode.Items(0).Selected = True
            DDLProductCode.Enabled = False
        Else
            DDLProductCode.Items.Clear()
            CmdText = "select ProductId,ProductCode from Product where ProductGroupId=" + DDLProductGroupCode.SelectedValue + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" & DDLProductGroupCode.SelectedValue & " and EventID=13 and EventYear=" & ddlCYear.SelectedValue & ") and ProductID in(select ProductID from CalSignUp where EventYear='" & ddlCYear.SelectedValue & "' and MemberID=" & memberID & " and Accepted='Y')"
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            If ds.Tables.Count > 0 Then
                DDLProductCode.DataTextField = "ProductCode"
                DDLProductCode.DataValueField = "ProductId"
                DDLProductCode.DataSource = ds
                DDLProductCode.DataBind()
                DDLProductCode.Items.Insert(0, "All")
                DDLProductCode.Items(0).Selected = True
                DDLProductCode.Enabled = True
            End If
        End If

    End Sub

    Protected Sub DDLProductGroupCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillProductCode()
        If ddlCoach.SelectedValue <> "Select Coach Name" Then
            loadChildren()

        End If
        LoadLevel(DDLLevel, DDLProductGroupCode.SelectedItem.Text)
    End Sub
    Protected Sub DDLProductCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If ddlCoach.SelectedValue <> "Select Coach Name" And DDLProductGroupCode.SelectedValue <> "All" Then
            loadChildren()

        End If
    End Sub
    Protected Sub GVCoaching_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If e.CommandName = "SelectMeetingURL" Then
            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

            Dim RowIndex As Integer = gvRow.RowIndex

            Dim MeetingURL As String = DirectCast(gvRow.FindControl("LblMeetingURL"), Label).Text
            hdnChildMeetingURL.Value = MeetingURL
            Dim OnlineClassEmail As String = DirectCast(gvRow.FindControl("LblOnlineClassEmail"), Label).Text

            Dim WebExID As String = DirectCast(gvRow.FindControl("lblWebExID"), Label).Text
            Dim WebExPwd As String = DirectCast(gvRow.FindControl("lblWebExPwd"), Label).Text
            Dim SessionKey As String = DirectCast(gvRow.FindControl("lblSessionKey"), Label).Text

            Dim ProductCode As String = DirectCast(gvRow.FindControl("lblPrdCode"), Label).Text
            Dim Coachname As String = DirectCast(gvRow.FindControl("lblCoachname"), Label).Text

            Dim ds As DataSet = New DataSet()
            Dim CmdText As String = String.Empty
            Dim beginTime As String = TryCast(DirectCast(gvRow.FindControl("lblBegTime"), Label), Label).Text
            Dim day As String = TryCast(DirectCast(gvRow.FindControl("lblMeetDay"), Label), Label).Text
            CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then

                        Dim dtFromS As New DateTime()
                        Dim dtEnds As DateTime = DateTime.Now
                        Dim mins As Double = 40.0
                        If DateTime.TryParse(beginTime, dtFromS) Then
                            Dim TS As TimeSpan = dtFromS - dtEnds

                            mins = TS.TotalMinutes
                        End If
                        Dim today As String = DateTime.Now.DayOfWeek.ToString()
                        If mins <= 30 AndAlso day = today Then
                          CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + SessionKey + ""

                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdText)

                            Dim meetingLink As String = TryCast(DirectCast(gvRow.FindControl("MyHyperLinkControl"), LinkButton), LinkButton).Text



                            hdnChildMeetingURL.Value = meetingLink
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                        Else
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                        End If

                    Else
                        lblStatus.Text = "Only authorized child can join into the training session."
                    End If

                End If
            End If

        End If
    End Sub

    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String)
        ddlObject.Items.Clear()
        ddlObject.Enabled = True
        If ProductGroup.Contains("SAT") Then ' ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
            ddlObject.Items.Insert(0, New ListItem("All", 0))
            ddlObject.Items.Insert(1, New ListItem("Junior", "Junior"))
            ddlObject.Items.Insert(2, New ListItem("Senior", "Senior"))
        ElseIf ProductGroup.Contains("UV") Then
            ''ddlObject.Enabled = False
            ddlObject.Items.Insert(0, New ListItem("All", 0))
            ddlObject.Items.Insert(1, New ListItem("Junior", "Junior"))
            ddlObject.Items.Insert(2, New ListItem("Intermediate", "Intermediate"))
            ddlObject.Items.Insert(3, New ListItem("Senior", "Senior"))
        ElseIf ProductGroup.Contains("SC") Then
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("One level only", "One level only"))
        ElseIf ProductGroup.Contains("All") And ddlCoach.SelectedValue <> "Select Coach Name" And ddlCoach.SelectedValue <> "[ALL]" And ddlCoach.SelectedValue <> "" Then
            Dim cmdText As String = String.Empty
            cmdText = "select distinct Level from CalSignup where MemberID=" & ddlCoach.SelectedValue & " and EventYear=" & ddlCYear.SelectedValue & ""
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            DDLLevel.DataValueField = "Level"
            DDLLevel.DataTextField = "Level"
            DDLLevel.DataSource = ds

            DDLLevel.DataBind()
            ddlObject.Items.Insert(0, New ListItem("All", 0))
        ElseIf ProductGroup.Contains("All") And ddlCoach.SelectedValue <> "Select Coach Name" And ddlCoach.SelectedValue = "[ALL]" And ddlCoach.SelectedValue <> "" Then
            Dim cmdText As String = String.Empty
            cmdText = "select distinct Level from CalSignup where EventYear=" & ddlCYear.SelectedValue & ""
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            DDLLevel.DataValueField = "Level"
            DDLLevel.DataTextField = "Level"
            DDLLevel.DataSource = ds
            DDLLevel.DataBind()
            ddlObject.Items.Insert(0, New ListItem("All", 0))
        Else
            ddlObject.Items.Insert(0, New ListItem("All", 0))
            ddlObject.Items.Insert(1, New ListItem("Beginner", "Beginner"))
            ddlObject.Items.Insert(2, New ListItem("Intermediate", "Intermediate"))
            ddlObject.Items.Insert(3, New ListItem("Advanced", "Advanced"))
        End If

    End Sub

    Public Function GetTrainingSessions(WebExID As String, Pwd As String, SessionKey As String) As String
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.GetTrainingSession"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & SessionKey & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = ProcessTrainingStatusTestResponse(xmlReply)
        'lblMsg3.Text = result;
        Return result

    End Function
    Private Function ProcessTrainingStatusTestResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim MeetingStatus As String = String.Empty
        Dim startTime As String = String.Empty
        Dim Day As String = String.Empty
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)

            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession")
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText
                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText
                Day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText
                startTime = startTime.Substring(10, 9)

                hdnSessionStartTime.Value = startTime
                Dim Mins As Double = 0.0
                Dim dFrom As DateTime = DateTime.Now
                Dim dTo As DateTime = DateTime.Now
                Dim sDateFrom As String = startTime

                Dim today As String = DateTime.Today.DayOfWeek.ToString()
                If today.ToLower() = Day.ToLower() Then
                    Dim sDateTo As String = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                    'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    Dim easternTime As DateTime = DateTime.Now
                    Dim strEasternTime As String = easternTime.ToShortDateString()
                    strEasternTime = Convert.ToString(strEasternTime & Convert.ToString(" ")) & sDateFrom
                    Dim dtEasternTime As DateTime = Convert.ToDateTime(strEasternTime)
                    Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                    Dim TS As TimeSpan = dtEasternTime - easternTimeNow


                    Mins = TS.TotalMinutes
                Else
                    For i As Integer = 1 To 7
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString()
                        If today.ToLower() = Day.ToLower() Then
                            dTo = DateTime.UtcNow.AddDays(i)
                            'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            Dim easternTime As DateTime = DateTime.Now.AddDays(i)
                            Dim sDateTo As String = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            Dim targetDateTime As String = Convert.ToString(easternTime.ToShortDateString() + " ") & sDateFrom
                            'DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                            Dim dtTargetTime As DateTime = Convert.ToDateTime(targetDateTime)
                            Dim TS As TimeSpan = dtTargetTime - easternTimeNow


                            Mins = TS.TotalMinutes
                        End If
                    Next
                End If
                Dim DueMins As String = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."))
                hdnSessionStartTime.Value = startTime
                hdnStartMins.Value = Convert.ToString(DueMins)
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return MeetingStatus
    End Function

End Class


