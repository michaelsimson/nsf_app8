Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddUpdCluster
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            GetZone()
            GetRecords()
        End If
    End Sub
    Private Sub GetZone()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Zone order by Zonecode Desc")
        While dsRecords.Read
            ddlZone.Items.Insert(++i, New ListItem(dsRecords("ZoneCode"), dsRecords("ZoneId")))
        End While
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Cluster")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("ClusterID") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from Cluster where ClusterId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtClusterCode.Text = dsRecords.Tables(0).Rows(0)("ClusterCode").ToString()
        txtClusterName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        txtDescription.Text = dsRecords.Tables(0).Rows(0)("Description").ToString()
        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ZoneID").ToString()))
        txtClusterCode.Enabled = False
        Dim i As Integer
        ddlStatus.ClearSelection()
        For i = 0 To ddlStatus.Items.Count - 1
            If (ddlStatus.Items(i).Value = dsRecords.Tables(0).Rows(0)("Status").ToString()) Then
                ddlStatus.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        If txtClusterCode.Text.Length < 1 Then
            Pnl3Msg.Text = "Please Enter Cluster Code"
            Exit Sub
        ElseIf txtClusterName.Text.Length < 1 Then
            Pnl3Msg.Text = "Please Enter Cluster Name"
            Exit Sub
        ElseIf ddlZone.SelectedValue = "0" Then
            Pnl3Msg.Text = "Please select Zone"
            Exit Sub
        ElseIf txtDescription.Text.Length < 1 Then
            Pnl3Msg.Text = "Please Enter Description"
            Exit Sub
        ElseIf ddlStatus.Text = "Select Status" Then
            Pnl3Msg.Text = "Please Select Valid Status"
            Exit Sub
        End If
        Dim cnt As Integer = 0
        If BtnAdd.Text = "Add" Then
            Strsql = "Select count(ClusterCode) from Cluster where ClusterCode='" & txtClusterCode.Text & "'"
            cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
            If cnt = 0 Then
                Strsql = "Insert Into Cluster(ClusterCode, Name, Description, Status,ZoneId,ZoneCode, CreateDate, CreatedBy) Values ('"
                Strsql = Strsql & txtClusterCode.Text & "','" & txtClusterName.Text & "','" & txtDescription.Text & "','"
                Strsql = Strsql & ddlStatus.Text & "'," & ddlZone.SelectedValue & ",'" & ddlZone.SelectedItem.Text & "',getDate(),'" & Session("LoginID") & "')"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                GetRecords()
                clear()
                Pnl3Msg.Text = "Record Added Successfully"
            Else
                Pnl3Msg.Text = "The Code already Exist"
            End If
        ElseIf BtnAdd.Text = "Update" Then
            Strsql = "Select count(ClusterCode) from Cluster where ClusterCode='" & txtClusterCode.Text & "' AND ClusterID <> " & Session("ClusterID")
            cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
            If cnt = 0 Then
                Strsql = "Update Cluster Set Description='" & txtDescription.Text & "', Status='" & ddlStatus.SelectedItem.Value & "',Zoneid=" & ddlZone.SelectedValue & ", ZoneCode='" & ddlZone.SelectedItem.Text & "', ModifyDate=GetDate(), ModifiedBy='" & Session("LoginID") & "', ClusterCode='" & txtClusterCode.Text & "',Name = '" & txtClusterName.Text & "'  Where ClusterID=" & Session("ClusterID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                GetRecords()
                clear()
                Pnl3Msg.Text = "Record Updated Successfully"
                BtnAdd.Text = "Add"
            Else
                Pnl3Msg.Text = "The Code already Exist"
            End If
        End If
    End Sub
    Private Sub clear()
        txtClusterCode.Text = ""
        txtClusterName.Text = ""
        txtDescription.Text = ""
        BtnAdd.Text = "Add"
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select Status"))
        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue("0"))
        txtClusterCode.Enabled = True
    End Sub


    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
    End Sub

End Class
