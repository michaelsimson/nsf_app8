﻿using System;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;

public partial class CoachingRegCount : System.Web.UI.Page
{
    double sumVal;
    int StartYear;
    int EndYear;
    bool IsFlag = false;
    int MaxYear = DateTime.Now.Year;
    DataTable dtnew;
    DataTable dtCount = new DataTable();
    DataTable DtSATE = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            Session["CalendarSignupCount"] = null;
            lblerr.Text = string.Empty;
            YearConditions();
            PivotTable();
            PopulateYear(ddlFromYear);
            PopulateYear(ddlToYear);
        }
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {

            ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= 2010; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));

            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }
    protected void btnsubmit_Click(object sender, System.EventArgs e)
    {
        try
        {
            dropdownCondition();
        }
        catch (Exception err)
        {
        }
    }
    protected void PivotTable()
    {
        try
        {
            BtnExpo.Visible = false;
            string WCNT = string.Empty;
            string am = string.Empty;
            string Tot = string.Empty;
            lblerr.Text = string.Empty;
            for (int i = StartYear; i <= EndYear; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                    Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                    am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                }
            }
            string Qrycondition = "with tbl as(select EventYear as Year, productgroupcode as ProductGroup,ProductCode as Product, Level, Semester,"
            + "COUNT(*)Signups,'' as Accepted,'' as Ratio from CoachReg where  Approved='Y'   Group by EventYear, productgroupcode, ProductCode, Level, Semester )"
           + " select ProductGroup,Product,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'"
            + "when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' when Level='One Level'then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level, Semester " + am + " ,'Accepted'=Accepted,'Ratio'=Ratio"
            + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by ProductGroup,product,level ";
            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            dtCount = dsCount.Tables[0];

            string QryApproved = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' when Level='One Level'then '1.One Level'  when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + MaxYear + "  and Accepted='Y'  Group by EventYear, productgroupcode,ProductCode,Accepted, Level, Semester  order by  productgroupcode,ProductCode,level ";
            DataSet dsCountProdAccep = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, QryApproved);
            DataTable dtCountProdAcce = dsCountProdAccep.Tables[0];
            for (int i = 0; i < dtCountProdAcce.Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {
                    if ((dtCountProdAcce.Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dtCountProdAcce.Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dtCountProdAcce.Rows[i]["Level"].ToString() == dtCount.Rows[j]["Level"].ToString()) && (dtCountProdAcce.Rows[i]["Semester"].ToString() == dtCount.Rows[j]["Semester"].ToString()))
                    {

                        dtCount.Rows[j]["Accepted"] = dtCountProdAcce.Rows[i]["Signup"];
                    }

                }
            }

            string Qry = " select distinct productgroupid,productid,productcode,productgroupcode from CoachReg order by  productgroupid,productid ";
            DataSet dsCountProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtCountProduct = dsCountProduct.Tables[0];
            // Ratio();
            dtnew = dtCount.Clone();
            DtSATE = dtCount.Clone();

            for (int i = 0; i <= dtCountProduct.Rows.Count - 1; i++)
            {
                DataRow dr = null;
                DataRow dr1 = null;
                string StrProdut2 = dtCountProduct.Rows[i]["productcode"].ToString();
                DataRow[] drs = dtCount.Select("product='" + StrProdut2 + "'");

                if (drs.Length > 0)
                {
                    dr = dtnew.NewRow();
                    dr[0] = "";
                    dr1 = DtSATE.NewRow();
                    dr1[0] = "";
                    foreach (DataRow drnew in drs)
                    {
                        dtnew.ImportRow(drnew);
                    }
                }
                else
                {
                    continue;
                }

                for (int j = 4; j < dtCount.Columns.Count; j++)
                {
                    double sum = 0;
                    foreach (DataRow drnew in drs)
                    {
                        if (!DBNull.Value.Equals(drnew[j]))
                        {
                            string st = drnew[j].ToString();
                            if (st != "")
                            {
                                sum += Convert.ToDouble(drnew[j]);
                            }

                        }
                    }
                    dr[j] = sum;
                    dr1[j] = sum;
                }
                dtnew.Rows.Add(dr);

                if (StrProdut2 == "SATE")
                {
                    dr1[0] = "SATTotal";
                    DtSATE.Rows.Add(dr1);
                    // if (EndYear == Convert.ToInt16(DateTime.Now.Year))
                    // {
                    //     double RatioVal = (Convert.ToDouble(DtSATE.Rows[0][Convert.ToString(DateTime.Now.Year).ToString()]) / Convert.ToDouble(DtSATE.Rows[0]["Accepted"]));
                    //     DtSATE.Rows[0]["Ratio"] = Math.Round(RatioVal, 1);
                    //}
                }
            }


            // dtCount = dtnew.Copy();
            DataRow drcont = dtnew.NewRow();
            DataRow drcontSatE = dtnew.NewRow();
            for (int i = 4; i <= dtCount.Columns.Count - 1; i++)
            {
                drcont[i] = 0;
                double sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtCount.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {

                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }
                    }
                }
                drcont[i] = sum;
            }
            drcont[0] = "Total";
            dtnew.Rows.Add(drcont);




            int val = dtnew.Rows.Count - 1;


            for (int i = 4; i <= dtnew.Columns.Count - 1; i++)
            {
                drcontSatE[i] = 0;
                double Subtract = 0;
                double TotVal = Convert.ToDouble(dtnew.Rows[val][i].ToString());
                double SatTot = Convert.ToDouble(DtSATE.Rows[0][i].ToString());
                Subtract = TotVal - SatTot;
                drcontSatE[i] = Subtract;
            }
            drcontSatE[0] = "Total less SAT English";
            dtnew.Rows.Add(drcontSatE);
            if (EndYear == Convert.ToInt16(DateTime.Now.Year))
            {

                for (int i = 0; i <= dtnew.Rows.Count - 1; i++)
                {
                    string YearCol = Convert.ToString(DateTime.Now.Year);

                    if ((dtnew.Rows[i]["Accepted"].ToString() != "") && (dtnew.Rows[i][Convert.ToString(DateTime.Now.Year)].ToString() != ""))
                    {
                        double currentYear = Convert.ToDouble(dtnew.Rows[i][Convert.ToString(DateTime.Now.Year).ToString()]);
                        double Accepted = Convert.ToDouble(dtnew.Rows[i]["Accepted"]);
                        if ((currentYear != 0) && (Accepted != 0))
                        {
                            decimal RatioVal = (Convert.ToDecimal(dtnew.Rows[i][Convert.ToString(DateTime.Now.Year).ToString()]) / Convert.ToDecimal(dtnew.Rows[i]["Accepted"]));
                            dtnew.Rows[i]["Ratio"] = Math.Round(RatioVal, 1, MidpointRounding.AwayFromZero);

                        }
                    }


                }

            }
            if (dtCount.Rows.Count > 0)
            {
                Div1.Visible = true;
                Div2.Visible = false;
                BtnExpo.Visible = true;
                Session["CalendarSignupCount"] = dtnew;
                GVcount.Visible = true;
                GVcount.DataSource = dtnew;
                GVcount.DataBind();
                lblPageTitle.Text = "Coaching Registration Count";
            }
            else
            {
                Div1.Visible = true;
                Div2.Visible = false;
                BtnExpo.Visible = false; ;
                lblerr.Text = "No records found";
                lblPageTitle.Text = "Coaching Registration Count";
            }
        }
        catch (Exception err)
        {
        }
    }



    protected void YearConditions()
    {
        string SqlStr = "select Distinct min(eventyear) as MInYear,MAX(eventyear) as MaxYear from CoachReg";
        DataSet dsRecordsYear = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlStr);
        DataTable dt = dsRecordsYear.Tables[0];
        StartYear = Convert.ToInt16(dt.Rows[0]["MInYear"].ToString());
        EndYear = Convert.ToInt16(dt.Rows[0]["MaxYear"].ToString());
    }
    protected void dropdownCondition()
    {
        string ReportType = DDLReportType.SelectedValue;
        if (((ddlFromYear.SelectedItem.Text == "Select Year") && (ddlToYear.SelectedItem.Text == "Select Year")) || ((ddlFromYear.SelectedItem.Text == "All") && (ddlToYear.SelectedItem.Text == "All")))
        {
            YearConditions();
            if (ReportType == "1")
            {
                PivotTable();
            }
            else if (ReportType == "2")
            {
                ChildrenByChapterPivotTable();
            }
            else if (ReportType == "3")
            {
                CoachesByChapterPivotTable();
            }
        }
        else
        {
            if (((ddlFromYear.SelectedItem.Text == "Select Year") || (ddlToYear.SelectedItem.Text == "Select Year")) || ((ddlFromYear.SelectedItem.Text == "All") || (ddlToYear.SelectedItem.Text == "All")))
            {
                if ((ddlFromYear.SelectedItem.Text == "All") || (ddlToYear.SelectedItem.Text == "All"))
                {
                    ddlFromYear.SelectedValue = "0";
                    ddlToYear.SelectedValue = "0";
                    YearConditions();
                    if (ReportType == "1")
                    {
                        PivotTable();
                    }
                    else if (ReportType == "2")
                    {
                        ChildrenByChapterPivotTable();
                    }
                    else if (ReportType == "3")
                    {
                        CoachesByChapterPivotTable();
                    }

                }
                else
                {
                    lblerr.Text = "Please Select both years";
                    GVcount.Visible = false;
                    BtnExpo.Visible = false;
                }


            }
            else
            {
                lblerr.Text = string.Empty;
                if (Convert.ToInt16(ddlFromYear.SelectedValue) > Convert.ToInt16(ddlToYear.SelectedValue))
                {
                    lblerr.Text = "Please select less From year";
                    GVcount.Visible = false;
                    BtnExpo.Visible = false;
                }
                else
                {
                    lblerr.Text = "";
                    StartYear = Convert.ToInt16(ddlFromYear.SelectedValue);
                    EndYear = Convert.ToInt16(ddlToYear.SelectedValue);
                    if (ReportType == "1")
                    {
                        PivotTable();
                    }
                    else if (ReportType == "2")
                    {
                        ChildrenByChapterPivotTable();
                    }
                    else if (ReportType == "3")
                    {
                        CoachesByChapterPivotTable();
                    }
                    IsFlag = true;

                }
            }
        }

    }
    protected void BtnExpo_Click(object sender, System.EventArgs e)
    {
        GeneralExport((DataTable)Session["CalendarSignupCount"], "CalendarSignupCount.xls");
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        string ReportType = DDLReportType.SelectedValue;
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;
        if (ReportType == "1")
        {
            fname = "CalendarSignupCount_" + monthDay + "_" + year + ".xls";
        }
        else if (ReportType == "2")
        {
            fname = "CoachingChildrenByChapter_" + monthDay + "_" + year + ".xls";
        }
        else if (ReportType == "3")
        {
            fname = "CoachesByChapter_" + monthDay + "_" + year + ".xls";
        }
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
                //sep = ";";
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {

                    Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                }
                Response.Write("\n");
            }

            // Response.Write(sw.ToString());
            //response.End()                           //       Thread was being aborted.
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void GVcount_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (drv["productgroup"].ToString().Equals(""))
                {
                    e.Row.BackColor = System.Drawing.Color.FromArgb(194, 240, 191);
                }
                if (drv["productgroup"].ToString().Equals("Total"))
                {
                    //  e.Row.BackColor = System.Drawing.Color.FromArgb(235, 235, 235);
                }

            }
            for (int i = 3; i < e.Row.Cells.Count; i++)
            {
                if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
                {
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;

                }
            }
        }
        catch
        {

        }
    }

    protected void DDLReportType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ChildrenByChapterPivotTable()
    {
        try
        {
            BtnExpo.Visible = false;
            string WCNT = string.Empty;
            string am = string.Empty;
            string Tot = string.Empty;
            lblerr.Text = string.Empty;
            for (int i = StartYear; i <= EndYear; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                    Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                    am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                }
            }
            string Qrycondition = "with tbl as(select EventYear as Year, ChapterCode as Chapter,"
            + "COUNT(distinct(ChildNumber))Signups from CoachReg CR inner join Indspouse IP on Ip.AutoMemberId=CR.PMemberid inner join Chapter CH on CH.ChapterCode=IP.Chapter where  Approved='Y'   Group by EventYear, ChapterCode )"
           + " select Chapter " + am + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by Chapter ";
            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            dtCount = dsCount.Tables[0];



            if (dtCount.Rows.Count > 0)
            {

                DataRow drcont = dtCount.NewRow();
               
                for (int i = 1; i <= dtCount.Columns.Count - 1; i++)
                {
                    drcont[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtCount.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {

                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }
                        }
                    }
                    drcont[i] = sum;
                }
                drcont[0] = "Total";
                dtCount.Rows.Add(drcont);

                Div1.Visible = false;
                Div2.Visible = true;
                BtnExpo.Visible = true;
                Session["CalendarSignupCount"] = dtCount;
                GrdReportByChapter.Visible = true;
                GrdReportByChapter.DataSource = dtCount;
                GrdReportByChapter.DataBind();
                lblPageTitle.Text = "Coaching Children by Chapter";
                for (int i = 0; i < GrdReportByChapter.Rows.Count; i++)
                {
                    if (i % 2 != 0)
                    {
                        GrdReportByChapter.Rows[i].BackColor = System.Drawing.Color.FromArgb(194, 240, 191);
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                Div1.Visible = false;
                Div2.Visible = true;
                BtnExpo.Visible = false; ;
                lblerr.Text = "No records found";
                lblPageTitle.Text = "Coaching Children by Chapter";
            }
        }
        catch (Exception err)
        {
        }
    }

    protected void CoachesByChapterPivotTable()
    {
        try
        {
            BtnExpo.Visible = false;
            string WCNT = string.Empty;
            string am = string.Empty;
            string Tot = string.Empty;
            lblerr.Text = string.Empty;
            for (int i = StartYear; i <= EndYear; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                    Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                    am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                }
            }
            string Qrycondition = "with tbl as(select CS.EventYear as Year, ChapterCode as Chapter,"
            + "COUNT(distinct(CS.MemberId))Signups from CalSignup CS inner join Indspouse IP on Ip.AutoMemberId=CS.Memberid inner join Chapter CH on CH.ChapterId=IP.ChapterID  where  Accepted='Y' and CS.MemberId in (select distinct CMemberId from CoachReg where EventYear=cs.EventYear and Approved='Y')   Group by CS.EventYear, ChapterCode )"
           + " select Chapter " + am + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by Chapter ";
            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            dtCount = dsCount.Tables[0];



            if (dtCount.Rows.Count > 0)
            {
                DataRow drcont = dtCount.NewRow();

                for (int i = 1; i <= dtCount.Columns.Count - 1; i++)
                {
                    drcont[i] = 0;
                    double sum = 0;
                    string suma = "";
                    foreach (DataRow drnew in dtCount.Rows)
                    {
                        if (!DBNull.Value.Equals(drnew[i]))
                        {

                            suma = drnew[i].ToString();
                            if (suma != "")
                            {
                                sum += Convert.ToDouble(drnew[i]);
                            }
                        }
                    }
                    drcont[i] = sum;
                }
                drcont[0] = "Total";
                dtCount.Rows.Add(drcont);

                Div1.Visible = false;
                Div2.Visible = true;
                BtnExpo.Visible = true;
                Session["CalendarSignupCount"] = dtCount;

                GrdReportByChapter.Visible = true;
                GrdReportByChapter.DataSource = dtCount;
                GrdReportByChapter.DataBind();
                lblPageTitle.Text = "Coaches by Chapter";
                
                for (int i = 0; i < GrdReportByChapter.Rows.Count; i++)
                {
                    
                    if (i % 2 != 0)
                    {
                        GrdReportByChapter.Rows[i].BackColor = System.Drawing.Color.FromArgb(194, 240, 191);
                    }
                    else
                    {

                    }
                }
               
            }
            else
            {
                Div1.Visible = false;
                Div2.Visible = true;
                BtnExpo.Visible = false; ;
                lblerr.Text = "No records found";
                lblPageTitle.Text = "Coaches by Chapter";
            }
        }
        catch (Exception err)
        {
        }
    }

}