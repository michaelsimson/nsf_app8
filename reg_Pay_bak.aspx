<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.reg_Pay" Trace="false" CodeFile="reg_Pay.aspx.vb" CodeFileBaseClass="VRegistration.LinkPointAPI_cs.LinkPointTxn_Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">	
	         <table  width=75%>
	         <tr><td align=center><table>
			    <tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center" colspan="2">
						<H1 align="center"><FONT face="Arial" color="#0000ff" size="3"><B>Your Payment</B></FONT>&nbsp;<BR>
						</H1>
					</td>
				</tr>
			
					<TR>
						<td class="largewordingbold">This is a Secure Page. Security by 
								GeoTrust<br /> <IMG height="50" src="images/gt-logo.gif" Onclick="window.open('https://smarticon.geotrust.com/smarticonprofile?Referer=http://www.northsouth.org')" style="cursor:pointer" ><a href=" target="new"></a></td>
						<td vAlign="middle">
							<table id="Table3" cellSpacing="1" cellPadding="1" width="300" border="0">
								<tr>
									<td class="largewordingbold" align ="left" style="Font-Size:12pt;">
									<asp:Label  ID="lblRgfee" Text="Registration Fee :"  runat="server"/>
									</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblRegFee" runat="server" Width="46px"></asp:label></td>
								</tr>
								<tr runat="server" id="trDonation" >
									<td class="largewordingbold" align ="Left" style="Font-Size:12pt;">Donation :</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblDonation" runat="server" Width="46px"></asp:label></td>
								</tr>
								<tr runat="server" id="trFundR" visible = "false">
								    <td class="largewordingbold" align ="left" style="Font-Size:12pt;"><asp:Label  ID="Label1" Text="Tickets and Sponsorships :"  runat ="server"/></td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblFundRAmt" runat="server" Width="46px"></asp:label></td>
								</tr>
								<tr runat="server" id="trsale" visible = "false" ><td class="largewordingbold" align ="left" style="Font-Size:12pt;">
								<asp:Label  ID="lblSale" Text="Purchase Amount :"  runat ="server"/>
									</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblSaleAmount" runat="server" Width="46px"></asp:label></td>
								</tr>
								<tr>
									<%--<TD class="largewordingbold">Meals :</TD>--%>
									<td class="largewordingbold" align ="left" style="Font-Size:12pt;">
									<asp:Label  ID="lblMeals" Text="Meals :"  runat ="server"/>
									</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblMealsAmount" runat="server" Width="46px"></asp:label></td>
								</tr>
								<tr>
									<%--<TD class="largewordingbold">Late Fee :</TD>--%>
									<td class="largewordingbold" align ="Left" style="Font-Size:12pt;">
									<asp:Label  ID="lblLatefeetext" Text="Late Fee :"  runat ="server"/>
									</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;">
									<asp:label id="lblLateFee"  runat="server" Width="46px"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="largewordingbold" align ="left" style="Font-Size:12pt;"><span style="color:#0033FF; font-weight:bold" > Total Due</span> :</td>
									<td class="largewordingbold" align ="right" style="Font-Size:12pt;"><asp:label id="lblTotalAmount" runat="server" style="color:#0033FF; font-weight:bold" Width="46px"></asp:label></td>
								</tr>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD class="largewordingbold" colSpan="2">
						<table id="tblError"  runat=server visible=false border=1 width=100% ><tr><td>
						<asp:label id="lblMessage" runat="server" Font-Bold="True" Font-Size=X-Small ForeColor="Red"></asp:label>
						</td></tr></table>
						
						</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<table width="100%" border="1">						
								<tr>
									<td class="largewordingbold" width="100%" colSpan="4" ><b>Billing Address</b></td>
								</tr>
								<tr>
								    <td class="largewordingbold" align="right" style="width: 162px"><b>Name</b></td>									
								    <td class="largewordingbold" align="left" width="75%" colspan="3"><asp:label id="nameLabel" runat="server" Width="46px"></asp:label></td>									
								</tr>
								
								<tr>
									<td class="largewordingbold" align="right" style="width: 162px"><b>Street Address</b></td>
									<td class="largewordingbold" align="left" width="75%" colSpan="3">&nbsp;<asp:textbox id="txtAddress1" runat="server" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Street Address Required"></asp:requiredfieldvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" align="right" style="width: 162px"><b>Apt#</b></td>
									<td class="largewordingbold" align="left" width="75%" colSpan="3">&nbsp;<asp:textbox id="txtAddress2" runat="server" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="largewordingbold" vAlign="top" align="right" style="width: 162px"><b>City</b></td>
									<td class="largewordingbold" vAlign="top" align="left" width="25%">&nbsp;<asp:textbox id="txtCity" runat="server" MaxLength="50"></asp:textbox>&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" ControlToValidate="txtCity" ErrorMessage="City Required"></asp:requiredfieldvalidator></td>
									<td class="largewordingbold" vAlign="top" align="right" width="25%"><b>State/Province</b></td>
									<td class="largewordingbold" vAlign="top" align="left" width="25%">&nbsp;<asp:dropdownlist id="ddlState" runat="server"></asp:dropdownlist><asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="ddlState" ErrorMessage="Please select a State"></asp:requiredfieldvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" vAlign="top" align="right" style="width: 162px"><b>Country</b></td>
									<td class="largewordingbold" vAlign="top" align="left" width="25%">&nbsp;
										<asp:dropdownlist id="ddlCountry" runat="server"></asp:dropdownlist><asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" ControlToValidate="ddlCountry" ErrorMessage="Please select a Country "></asp:requiredfieldvalidator></td>
									<td class="largewordingbold" vAlign="top" align="right" width="25%"><b>Zip/Postal Code</b></td>
									<td class="largewordingbold" vAlign="top" align="left" width="25%">&nbsp;<asp:textbox id="txtZip" runat="server" Width="84px" MaxLength="20"></asp:textbox><asp:requiredfieldvalidator id="rfv_zip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip/Postal Code required"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="rev_zip" runat="server" ControlToValidate="txtZip" ErrorMessage="Invalid Zip/Postal code"
											ValidationExpression="^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z]\d[A-Z]\d$"></asp:regularexpressionvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" width="100%" colSpan="4">Please ensure credit card 
										information entered is accurate. Credit Card holder name,
                                        Card number and Card expiration date should be correct. If payment fails for some reason, you will need 
										to start all over again.</td>
								</tr>
								<TR>
									<TD class="largewordingbold" align="right" style="width: 162px" nowrap="noWrap"><b>Credit Card Information</b></TD>
									<TD class="largewordingbold" align="left" width="75%" colSpan="3"><IMG height="27" alt="We Accept American Express, Discover,Mastercard and Visa cards"
											src="images/validCreditcards.gif" width="125"></TD>
								</TR>
								<tr>
									<td class="largewordingbold" align="right" style="width: 162px"><b>Card Holder Name</b></td>
									<td class="largewordingbold" align="left" width="75%" colSpan="3">&nbsp;<asp:textbox id="txtCardHolderName" runat="server" MaxLength="50"></asp:textbox>(as
                                        it exactly appears on your card)<br />
                                        <asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" ControlToValidate="txtCardHolderName"
											ErrorMessage="Card Holder &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;Name Required"></asp:requiredfieldvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" align="right" style="width: 162px"><b>Card Number</b></td>
									<td class="largewordingbold" align="left" width="75%" colSpan="3">&nbsp;<asp:textbox id="txtCardNumber" runat="server" MaxLength="20"></asp:textbox><asp:requiredfieldvalidator id="Requiredfieldvalidator8" runat="server" ControlToValidate="txtCardNumber" ErrorMessage="CardNumber &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;Required"></asp:requiredfieldvalidator><asp:label id="lblCardError" runat="server" ForeColor="Red"></asp:label><asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" ControlToValidate="txtCardNumber"
											ErrorMessage="Invalid card number - please enter only numbers - no spaces or dashes" ValidationExpression="\d{15,16}"></asp:regularexpressionvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" align="right" style="width: 162px"><b>Card Expiration</b></td>
									<td class="largewordingbold" width="75%" colSpan="3"><b>Month</b>&nbsp;
										<asp:dropdownlist id="ddlMonth" runat="server" Width="42px">
											<asp:ListItem Selected="True"></asp:ListItem>
											<asp:ListItem Value="01">01</asp:ListItem>
											<asp:ListItem Value="02">02</asp:ListItem>
											<asp:ListItem Value="03">03</asp:ListItem>
											<asp:ListItem Value="04">04</asp:ListItem>
											<asp:ListItem Value="05">05</asp:ListItem>
											<asp:ListItem Value="06">06</asp:ListItem>
											<asp:ListItem Value="07">07</asp:ListItem>
											<asp:ListItem Value="08">08</asp:ListItem>
											<asp:ListItem Value="09">09</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
											<asp:ListItem Value="11">11</asp:ListItem>
											<asp:ListItem Value="12">12</asp:ListItem>
										</asp:dropdownlist><b>Year</b>&nbsp;
										<asp:dropdownlist id="ddlYear" runat="server" Width="66px"></asp:dropdownlist><asp:requiredfieldvalidator id="Requiredfieldvalidator9" runat="server" ControlToValidate="ddlMonth" ErrorMessage="Please select a Month"></asp:requiredfieldvalidator><asp:requiredfieldvalidator id="Requiredfieldvalidator10" runat="server" ControlToValidate="ddlYear" ErrorMessage="Please select a Year"></asp:requiredfieldvalidator></td>
								</tr>
								<tr>
									<td class="largewordingbold" align="left" width="100%" colSpan="4"></td>
								</tr>
							</table>
						</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2" style="height: 177px">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="largewordingbold" colSpan="2" nowrap="noWrap">*** Please read the following notes before you 
										proceed with payment:</TD>
								</TR>
								<TR>
									<TD class="largewordingbold" align ="left" >
										<FONT color="#ff0000">1. After clicking on "Submit for Payment," please be patient and wait for the payment confirmation screen. This can take 10 seconds or up to 3 minutes depending on Internet traffic and the speed of your connection. </FONT>
										</td>
								</tr>
								<tr>
										<td class="largewordingbold" align ="left">
										<FONT class="ErrorFont">2. Please do not use "Back Button" or "Refresh button" on your Browser to avoid any duplicate charges to your Credit Card. </FONT>
										</td>
								</tr>
								<tr>
								    <td class="largewordingbold" align ="left"><FONT color="#ff0000">3. We had instances of people pressing "Submit for Payment" multiple times due to impatience, which resulted in multiple charges. </FONT>
										</td>
								</tr>
								<tr>
										<td class="largewordingbold" align ="left"><FONT color="#ff0000">4. If you do not want to pay at this time
												<asp:linkbutton id="lbRegistration" runat="server" CausesValidation=False>Click Here </asp:linkbutton>to 
												go back to Registration Page. </FONT>
										</td>
								</tr></TABLE>
										
							</TD>
					</TR>
					<TR>
						<TD vAlign="middle" colspan ="2" align="center">
							<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="largewordingbold" align="left" style="height: 34px">
                                        Clicking Submit for Payment will enable 
										credit card payment. Your registration will
                                        be completed, after your credit card payment is processed and a confirmation is
                                        received.</TD>
								</TR>
								<TR>
									<TD align="center" style="height: 55px">
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                        <asp:linkbutton id="lbContinue" runat="server" Font-Bold="True" Font-Size="Medium">Submit for Payment</asp:linkbutton>&nbsp;&nbsp;
                                        (Once paid, your payment is non-refundable.)

                                        </TD>
								</TR>
							</TABLE>
						</TD>
					</TR>			
        </TABLE>
			   </td></table>
    <asp:Label ID="lbltest" runat="server"></asp:Label>
</asp:Content>
 

 
 
 