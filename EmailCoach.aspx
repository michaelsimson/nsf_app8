<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="EmailCoach.aspx.cs" Inherits="EmailCoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: center; width: 100%">
        <div style="text-align: left">
            &nbsp;&nbsp;
   
        <asp:LinkButton CssClass="btn_02" ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
        </div>
        <div align="center">

            <asp:RadioButton ID="rdParents" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" AutoPostBack="True" Checked="True" Font-Bold="True" Text="Parents" />&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:RadioButton ID="rdParentandstudent" runat="server" AutoPostBack="True"
                  Font-Bold="True" Text="Parents and Students"
                  OnCheckedChanged="rdParentandstudent_CheckedChanged" />
            <asp:RadioButton ID="rdCoaches" runat="server" AutoPostBack="True" Font-Bold="True" OnCheckedChanged="rdCoaches_CheckedChanged" Text="Coaches � Scheduled" />
            <asp:RadioButton ID="rdcalsignup" runat="server" AutoPostBack="True"
                Font-Bold="True" Text="Coaches - All Calendar Sign Ups"
                OnCheckedChanged="rdcalsignup_CheckedChanged" />
            <asp:RadioButton ID="RadioButton1" runat="server" AutoPostBack="True"
                Font-Bold="True" Text="All Coaches � Scheduled & Unscheduled"
                OnCheckedChanged="RadioButton1_CheckedChanged1" />


        </div>
        <div style="clear: both;"></div>
        <div align="left" style="margin-left: 45px;">
            <asp:RadioButton ID="RbtnVolunteerSignUp" runat="server" AutoPostBack="True"
                Font-Bold="True" Text="Volunteer Signups"
                OnCheckedChanged="RbtnVolunteerSignUp_CheckedChanged" />
        </div>
        <br />
        <div>
            <asp:Panel ID="Panel1" runat="server">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="tbl1"><span class="title03">Send Email to Parents</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">Event Year</td>
                                    <td align="left">
                                        <asp:ListBox ID="lstYear" SelectionMode="Multiple" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="lstYear_SelectedIndexChanged"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Semester</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSemester" Width="150px" Height="24" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                                            <%-- <asp:ListItem Selected="True" Value="1">One</asp:ListItem>
                                            <asp:ListItem Value="2">Two</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Group Name </td>
                                    <td align="left"><%-- <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>--%>
                                        <asp:DropDownList ID="lstProductGroup" runat="server" Width="150px" DataTextField="Name" DataValueField="ProductGroupId"
                                            OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Name </td>
                                    <td align="left">
                                        <asp:DropDownList ID="lstProduct" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="lstProduct_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">Level </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLevelparent" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">Session #</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSessionNo" Width="150px" Height="24" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlSessionNo_SelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Coach Name</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCoach" runat="server" Height="24px" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="height: 27px">Type of Registration</td>
                                    <td align="left" style="height: 27px">
                                        <asp:DropDownList ID="ddlRegistype" runat="server" Height="24px" Width="150px">
                                            <asp:ListItem Selected="True" Value="1">Approved</asp:ListItem>
                                            <asp:ListItem Value="2">Pending</asp:ListItem>
                                            <asp:ListItem Value="3">Paid</asp:ListItem>
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSendEMail" runat="server" Text="Send EMail"
                                            OnClick="btnSendEMail_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPrd" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblPrdGrp" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" Visible="False">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="Td2"><span class="title03">Send Email to Parents and Students</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">Event Year</td>
                                    <td align="left">
                                        <asp:ListBox ID="lstYearparentstudents" SelectionMode="Multiple" runat="server"
                                            Width="150px" AutoPostBack="true"
                                            OnSelectedIndexChanged="lstYearparentstudents_SelectedIndexChanged"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Semester</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSemester1" Width="150px" Height="24" runat="server"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSemester1_SelectedIndexChanged">
                                            <%-- <asp:ListItem Selected="True" Value="1">One</asp:ListItem>
                                            <asp:ListItem Value="2">Two</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Group Name </td>
                                    <td align="left"><%-- <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>--%>
                                        <asp:DropDownList ID="DDproductGroup" runat="server" Width="150px"
                                            DataTextField="Name" DataValueField="ProductGroupId"
                                            AutoPostBack="true" OnSelectedIndexChanged="DDproductGroup_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Name </td>
                                    <td align="left">
                                        <asp:DropDownList ID="DDproduct" SelectionMode="Multiple" runat="server"
                                            AutoPostBack="true" Width="150px"
                                            OnSelectedIndexChanged="DDproduct_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Level </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLevelParentStudent" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlLevelParentStudent_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Session #</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSessionNo1" Width="150px" Height="24"
                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSessionNo1_SelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Coach Name</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DDcoachparntstudent" runat="server" Height="24px" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Type of Registration</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRegistype1" runat="server" Height="24px" Width="150px">
                                            <asp:ListItem Selected="True" Value="1">Approved</asp:ListItem>
                                            <asp:ListItem Value="2">Pending</asp:ListItem>
                                            <asp:ListItem Value="3">Paid</asp:ListItem>
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="ParentStudEmail" runat="server" Text="Send EMail" OnClick="ParentStudEmail_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblerr1" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="Label5" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="Panel2" runat="server" Visible="False">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="Td1"><span class="title03">Send Email to Coaches � Scheduled</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">Event Year</td>
                                    <td align="left">
                                        <asp:ListBox ID="lstYearCoach" SelectionMode="Multiple" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="lstYearCoach_SelectedIndexChanged"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Semester</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSemesterCoach" Width="150px" Height="24" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlSemesterCoach_SelectedIndexChanged">
                                            <%--  <asp:ListItem Selected="True" Value="1">One</asp:ListItem>
                                            <asp:ListItem Value="2">Two</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Group Name </td>
                                    <td align="left"><%-- <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>--%>
                                        <asp:DropDownList ID="ddlpgCoach" runat="server" Width="150px" DataTextField="Name" DataValueField="ProductGroupId"
                                            OnSelectedIndexChanged="ddlpgCoach_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Name </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlprCoach" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlprCoach_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Level </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLevelCoach" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlLevelCoach_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">Session #</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlsessionCoach" Width="150px" Height="24" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlSessionNo_SelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:CheckBox ID="Checkboxcoach" runat="server" /><b>Exclude Current Year Calendar Signups</b>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSendEmailCoaches" runat="server" Text="Send EMail"
                                            OnClick="btnSendEmailCoaches_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblerrcoach" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>






            <asp:Panel ID="PnlSheduled" runat="server" Visible="False">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="600px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="Td3"><span class="title03">Send Email to All Coaches � Scheduled & Unscheduled</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckEmail" runat="server" /><b>Exclude All Calendar Signups so far this year</b></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="Button1" runat="server" Text="Send EMail" OnClick="Button1_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lbLvol" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


            <asp:Panel ID="pnlVolunteerSignUp" runat="server" Visible="False">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="600px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="Td5"><span class="title03">Send Email to New Volunteers</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">


                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSendEmailToNewVolunteer" runat="server" Text="Send EMail" OnClick="btnSendEmailToNewVolunteer_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


            <asp:Panel ID="Pnlcoach" runat="server" Visible="False">
                <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
                    <tr>
                        <td align="center" bgcolor="#99CC33" id="Td4"><span class="title03">Coaches - All Calendar Sign Ups</span></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">Event Year</td>
                                    <td align="left">
                                        <asp:ListBox ID="lstYearCoach1" SelectionMode="Multiple" runat="server"
                                            Width="150px" AutoPostBack="true"
                                            OnSelectedIndexChanged="lstYearCoach1_SelectedIndexChanged"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Semester</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DropDownList3" Width="150px" Height="24" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                            <%--<asp:ListItem Selected="True" Value="1">One</asp:ListItem>
                                            <asp:ListItem Value="2">Two</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Product Group Name </td>
                                    <td align="left"><%-- <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>--%>
                                        <asp:DropDownList ID="ddlpgCoach1" runat="server" Width="150px"
                                            DataTextField="Name" DataValueField="ProductGroupId"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlpgCoach1_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">Product Name </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlprCoach1" SelectionMode="Multiple" runat="server"
                                            AutoPostBack="true" Width="150px"
                                            OnSelectedIndexChanged="ddlprCoach1_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Level </td>
                                    <td align="left">
                                        <asp:DropDownList ID="DDlSignupCoacheslevel" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="DDlSignupCoacheslevel_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">Session #</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlsessionCoach1" Width="150px" Height="24" runat="server" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="Button2" runat="server" Text="Send EMail" OnClick="Button2_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblerrcoach1" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPrd1" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblPrdGrp1" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
