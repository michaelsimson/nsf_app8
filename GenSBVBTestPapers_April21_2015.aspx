﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="GenSBVBTestPapers_April21_2015.aspx.cs" Inherits="GenSBVBTestPapers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     
    <script language="Javascript" type="text/javascript">
         function confirmtooverwrite() {
             try {

                 if (confirm("Data already exists, do you want to override?")) {
                     document.getElementById('<%= hiddenbtn.ClientID %>').click();

                }
                else {

                }
            } catch (ex) { }
         }
         function notallowedtoOverwrite() {
             try {

                 if (alert("Don’t allow override, since test paper was already uploaded")) {
                     

                 }
                 else {

                 }
             } catch (ex) { }
         }
         </script>


    <table align="center">
         <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>

        </tr>
        <tr>

            <td>
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Generate Spelling / Vocab Papers Sets</strong> </div>
            </td>
        </tr>
      <tr>

            <td align="right">
                
            </td>
        </tr>
       <tr>
           <td align="center">
               

                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
                <table width="50%" visible="true" >
                      <asp:Button ID="hiddenbtn" runat="server" Text="Button" OnClick="hiddenbtn_Click" Style="display: none;" />
                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server">
                            </asp:DropDownList>
                        </td>
                         <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"  >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Finals</asp:ListItem>
                          <asp:ListItem Value="2">Chapter</asp:ListItem>
                        
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" >
                                <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="SB" >Spelling</asp:ListItem>
                          <asp:ListItem Value="VB">Vocabulary</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        
                        <td>Product</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProduct" runat="server" >
                               <%-- <asp:ListItem Value="0">Select one</asp:ListItem>
                          <asp:ListItem Value="JVB">Junior Vocabulary</asp:ListItem>
                          <asp:ListItem Value="IVB">Intermediate Vocabulary</asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                         <td>Set</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSet" runat="server">
                                <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >1</asp:ListItem>
                          <asp:ListItem Value="2">2</asp:ListItem>
                          <asp:ListItem Value="3" >3</asp:ListItem>
                          <asp:ListItem Value="4">4</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>Output</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlOutput" runat="server"  >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Generate Random Words</asp:ListItem>
                          <asp:ListItem Value="2">Generate TP Words</asp:ListItem>
                          <asp:ListItem Value="3">Generate Test Papers</asp:ListItem>
                            </asp:DropDownList>
                       
                        </td>
                          <td align="center">
                        
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>
                     </td>  
                             </tr>
                       <tr><td colspan="11" align="center"><asp:Label ID="lblErr" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"></asp:Label>
                         
              <asp:LinkButton Text ="Click Here" ID="lnkClickHere1" runat="server" Visible="false" ForeColor="Blue" OnClick="lnkClickHere1_Click" ></asp:LinkButton>
          

                           <asp:Label ID="lblErr1" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"></asp:Label>
                         <asp:LinkButton Text ="Click Here" ID="lnkClickHere2" runat="server" Visible="false" ForeColor="Blue" OnClick="lnkClickHere2_Click"></asp:LinkButton>

                           </td>
                           <td>
                               <asp:Button ID="btnSaveWords" runat="server" Text="Save Words" Enabled="False" OnClick="btnSaveWords_Click"  />

                           </td>
                           <td>
                               <asp:Button ID="btnExportExcel" runat="server" Text="Export to Excel" Enabled="False" OnClick="btnExportExcel_Click" />

                           </td>
                       </tr>

                  
                  
                </table>
     
        </td>
   
         
         

     </tr>
     <tr align="center">

         <td><div style=" overflow:auto;">
             
             
             <asp:GridView ID="gvPubUnpubList" runat="server">
             </asp:GridView>
             <asp:HiddenField ID="hfIncDec" runat="server" />
            <div id="divTestPapers" runat="server">
                <asp:Button ID="btnexporttopdf" style="float:left;" runat="server" Text="Export to PDF" align="left" OnClick="btnexporttopdf_Click" />
                <center>
                <asp:Label ID="lblTestPaperPhase" runat="server" Text=""></asp:Label></center>

      <input id="txtHidden" style="width: 28px" type="hidden" value="0"
		runat="server" /> 
		
		<div style="clear:both;"><asp:LinkButton ID="lnkBtnPrev" runat="server" style="float:left;" Font-Underline="False" OnClick="lnkBtnPrev_Click" Font-Bold="True"><< Prev </asp:LinkButton>
		&nbsp;&nbsp;
		<asp:LinkButton ID="lnkBtnNext" runat="server" style="float:right;" Font-Underline="False" OnClick="lnkBtnNext_Click" Font-Bold="True">Next >></asp:LinkButton></div>
               
             <table border="1" cellpadding="0" cellspacing="0" width="100%">
                 <tr style="font-size:medium">
                     <th width="5%"><b>S.No</b></th>
                     <th width="5%"><b>Level</b></th>
                     <th width="30%"><b>Word</b></th>
                     <th width="5%"><b>Ans</b></th>
                     <th width="55%"><b>Meaning</b></th>
                  </tr></table>
                 <asp:Repeater ID="rpTestPaper" runat="server" OnItemDataBound="rpTestPaper_ItemDataBound">
                     <ItemTemplate>
                     
                     <table border="1" cellpadding="0" cellspacing="0" width="100%">
                         <tr style="page-break-before:always;">
                     <td align="center" width="5%">
                        <b><asp:Label runat="server" ID="lblSNo" Text=""  Font-Size="10" ></asp:Label></b>
				     </td>
				     <td width="5%">
                        <%# DataBinder.Eval(Container.DataItem, "Level")%>
				     </td>
                     <td width="30%">
                        <b><%# DataBinder.Eval(Container.DataItem, "Word")%></b>
				     </td>
                     <td width="5%">
                        <%# DataBinder.Eval(Container.DataItem, "Answer")%>
				     </td>
                     <td width="55%">

                        <%# DataBinder.Eval(Container.DataItem, "Meaning")%>
				     </td>
                 </tr>
                         <tr>
                             <td></td>
                             <td></td>
                             <td>
                                 A. <%# DataBinder.Eval(Container.DataItem, "A")%>
                                 <br/>
                                 B. <%# DataBinder.Eval(Container.DataItem, "B")%>
                                 <br/>
                                 C. <%# DataBinder.Eval(Container.DataItem, "C")%>
                                 <br/>
                                 D. <%# DataBinder.Eval(Container.DataItem, "D")%>
                                 <br/>
                                 E. <%# DataBinder.Eval(Container.DataItem, "E")%>
                              </td>
                             <td></td>
                             <td></td>

                       
				            
                         </tr></table></ItemTemplate>
                         </asp:Repeater>


                      <div id="divTemp" runat="server">
                          
                          
                          <table border="0" cellpadding="1" cellspacing="1" width="100%">
                               <%--<tr><td>

                                      </td>
                                      <td align="right">
                                           <asp:Label runat="server" Text="Office Use Only"></asp:Label>
                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      </td>
                                  </tr>--%>
                              <tr>
                                  <td colspan="2">
                                       <asp:Panel ID="pTableHeader" runat="server">
             <table border="1" cellpadding="1" cellspacing="0" width="100%" style="height:100%">
                 <tr style="page-break-before:always;font-size:10px;" >
                     <th width="7%"><b>S.No</b></th>
                     <th width="7%"><b>Level</b></th>
                     <th width="22%"><b>Word</b></th>
                     <th width="5%"><b>Ans</b></th>
                     <th width="59%"><b>Meaning</b></th>
                  </tr></table></asp:Panel>
                                      <asp:Repeater ID="rpToPDF" Visible="true" runat="server" OnItemDataBound="rpToPDF_ItemDataBound" >
                     <ItemTemplate>
                     
                     <table border="1" cellpadding="1" cellspacing="0" width="100%"  style="height:100%">
                         <tr style="font-size:8px;">
                     <td align="center" width="7%">
                        <b><asp:Label runat="server" ID="lblSNoTemp" Text=""  Font-Size="10" ></asp:Label></b>
				     </td>
				     <td align="center" width="7%">
                        <%# DataBinder.Eval(Container.DataItem, "Level")%>
				     </td>
                     <td width="22%"  bgcolor="#F2F2F2">
                        <b><%# DataBinder.Eval(Container.DataItem, "Word")%></b>
				     </td>
                     <td align="center" width="5%">
                        <%# DataBinder.Eval(Container.DataItem, "Answer")%>
				     </td>
                     <td width="59%" style="font-size:8px;">

                        <%# DataBinder.Eval(Container.DataItem, "Meaning")%>
				     </td>
                 </tr>
                         <tr style="font-size:8px;">
                             <td width="7%"></td>
                             <td width="7%"></td>
                             <td width="22%">
                                 A. <%# DataBinder.Eval(Container.DataItem, "A")%>
                                 <br/>
                                 B. <%# DataBinder.Eval(Container.DataItem, "B")%>
                                 <br/>
                                 C. <%# DataBinder.Eval(Container.DataItem, "C")%>
                                 <br/>
                                 D. <%# DataBinder.Eval(Container.DataItem, "D")%>
                                 <br/>
                                 E. <%# DataBinder.Eval(Container.DataItem, "E")%>
                              </td>
                             <td width="5%"></td>
                             <td width="59%"></td>

                       
				            
                         </tr></table></ItemTemplate>
                         </asp:Repeater>


                      <%--  <asp:Repeater ID="rpStuPhase2" Visible="true" runat="server" OnItemDataBound="rpStuPhase2_ItemDataBound" >
                     <ItemTemplate>
                     
                     <table border="1" cellpadding="2" cellspacing="0" width= "33%">
                      <tr style="font-size:10px;">
                     <td align="center" width="10%">
                        <b><asp:Label runat="server" ID="SNoStuPhase2" Text=""  Font-Size="10" ></asp:Label></b>
				     </td>
				     <td align="center" width="15%">
                        <%# DataBinder.Eval(Container.DataItem, "Level")%>
				     </td>
                     <td width="75%">
                        <b><%# DataBinder.Eval(Container.DataItem, "Word")%></b>
                                 <br/>
                                 A. <%# DataBinder.Eval(Container.DataItem, "A")%>
                                 <br/>
                                 B. <%# DataBinder.Eval(Container.DataItem, "B")%>
                                 <br/>
                                 C. <%# DataBinder.Eval(Container.DataItem, "C")%>
                                 <br/>
                                 D. <%# DataBinder.Eval(Container.DataItem, "D")%>
                                 <br/>
                                 E. <%# DataBinder.Eval(Container.DataItem, "E")%>    
				     </td>
                     
                    </tr>
                         
                     </table></ItemTemplate>
                         </asp:Repeater>--%>

                                      <asp:DataList ID="rpStuPhase2" runat="server" RepeatDirection="Vertical"
            RepeatColumns="3" Width="300px">
            <ItemTemplate>

                <table border="1">
                <tr>
                    <td><%#Container.DataItem %></td>
                </tr>
                <tr>
                    <td>--------------</td>
                </tr>
             </table>
           <%-- <table border="1" cellpadding="2" cellspacing="0" width="30%">
                      <tr style="font-size:10px;">
                     <td align="center" width="10%">
                        <b><asp:Label runat="server" ID="SNoStuPhase2" Text=""  Font-Size="10" ></asp:Label></b>
				     </td>
				     <td align="center" width="15%">
                        <%# DataBinder.Eval(Container.DataItem, "Level")%>
				     </td>
                     <td width="75%">
                        <b><%# DataBinder.Eval(Container.DataItem, "Word")%></b>
                                 <br/>
                                 A. <%# DataBinder.Eval(Container.DataItem, "A")%>
                                 <br/>
                                 B. <%# DataBinder.Eval(Container.DataItem, "B")%>
                                 <br/>
                                 C. <%# DataBinder.Eval(Container.DataItem, "C")%>
                                 <br/>
                                 D. <%# DataBinder.Eval(Container.DataItem, "D")%>
                                 <br/>
                                 E. <%# DataBinder.Eval(Container.DataItem, "E")%>    
				     </td>
                     
                    </tr>
                         
                     </table>--%>
            </ItemTemplate>
         

        </asp:DataList>



                                  </td>
                                 
                              </tr>
                              </table>
                             <table border="1" cellpadding="10" width="50%" align="right">
                                 <tr>
                                     <td rowspan="2" align="left" ><asp:Label style="font-size:8px;text-align:start" runat="server" Text="Phase I Score(1-25):"></asp:Label></td>
                                     <td align="left" ><asp:Label style="font-size:8px;text-align:start" runat="server" Text="Grade I:"></asp:Label></td>
                                 </tr>
                                 <tr>
                                     <td  align="left"><asp:Label style="font-size:8px;text-align:start" runat="server" Text="Grade II:"></asp:Label></td>
                                 </tr>
                             </table>
                          
                          
                              <%--<asp:Label ID="lblAnswerKey" runat="server" Text=""></asp:Label>
                               <br />--%>

                          <asp:panel id="pAnsKey" runat="server">
                              <table border="1" cellpadding="10" cellspacing="0" width="60%">
                                  <b><tr bgcolor="#F2F2F2">
                                      <td>Question No</td>
                                      <td>Answer Key</td>
                                  </tr></b>
                                  <tr>
                                      <td colspan="2">
                                          <asp:Repeater ID="rpAnswerKey" Visible="true" runat="server" OnItemDataBound="rpAnswerKey_ItemDataBound" >
                     <ItemTemplate>
                     
                     <table border="1" cellpadding="5" cellspacing="0" width="100%">
                         <tr style="page-break-before:always; ">
                     <td align="left" >
                        <asp:Label runat="server" ID="lblSNoAnsKey" Text=""  Font-Size="10" ></asp:Label>
				     </td>
                        <td align="left">
                        <%# DataBinder.Eval(Container.DataItem, "Answer")%>
				        </td>
                    
                         </tr>
                         </table></ItemTemplate>
                         </asp:Repeater>
                                      </td>
                                  </tr>
                              </table>
                          </asp:panel>
                     


                      </div>
      
                 
  </div>
             
             <br />
             
              
         </div></td>

     </tr>

<tr align="center">

    <td>
        </td>

               

       </tr>

  
       
                </table>
                
       </asp:Content>
    