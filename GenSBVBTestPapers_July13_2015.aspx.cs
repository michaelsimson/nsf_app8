﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Configuration;
using System.IO;
using System.Threading;




public partial class GenSBVBTestPapers : System.Web.UI.Page
{
    #region <<Variable Declaration>>
    int totalCount;
    int ic;
    int icTemp;
    int icAnsKey;
    int icAnsKey1;
    int icStuPh2=25;
    int icStuCopy = 0;
    int icStuCopyOffUse1 = 0;
    int icStuCopyOffUse2 = 0;
    int icStuCopy1 = 15;
    int icStuCopyOffUse11 = 15;
    int icStuCopyOffUse21 = 15;
    string roleIdColumnName = "RoleId";
    string cString = "ConnectionString";
    #endregion
    #region <<Pre-defined Functions>>
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            
            ic = 0;
            icTemp = 0;
            icAnsKey = 0;
            icAnsKey1 = 15;
            ViewState["increValue"] = ic;
            ViewState["increValueTemp"] = icTemp;
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            //{
            //    Response.Redirect("~/login.aspx?entry=p");
            //}
            //if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            //if (Array.IndexOf(new int[] { 1, 30 }, Convert.ToInt32(Session[roleIdColumnName])) < 0)
            //{
            //    Response.Redirect("~/VolunteerFunctions.aspx");
            //}
            divTemp.Visible = false;
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();
         
            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));

            divTestPapers.Visible = false;
            if (Convert.ToInt32(Session["RoleId"]) == 30)
            {
                int memberid = Convert.ToInt32(Session["LoginID"]);
                DataSet dsProductgroup = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select ProductGroupCode from Volunteer where memberid =" + memberid + " and productGroupCode in('SB','VB')");
                if (dsProductgroup.Tables[0].Rows.Count==1)
                {
                    ddlProductGroup.SelectedValue = dsProductgroup.Tables[0].Rows[0]["ProductGroupCode"].ToString();
                    ddlProductGroup.Enabled = false;
                }
                else
                {
                    ddlProductGroup.Enabled = true;
                }
            }

            if (Request.QueryString["Param"] != null)
            {
                string filename = "";


                if (Request.QueryString["Param"].ToString() == "1")
                {
                    if (Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        
                        report_JudgeCopy_VB();
                        
                    }
                    else
                    {
                        report_JudgeCopy_SB();
                    }



                }
                else if (Request.QueryString["Param"].ToString() == "2")
                {
                    if (Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                     report_AnswerKey();
                    }
                    else
                    {
                      report_AnswerKey_SB();
                    }
                }
                else if (Request.QueryString["Param"].ToString() == "3")
                {

                    if (Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                        report_StudentCopy();
                    }
                    else
                    {
                        report_StudentCopy_SB();
                    }
                }
                else if (Request.QueryString["Param"].ToString() == "4")
                {
                    excelReport_SB();
                }
                else if (Request.QueryString["Param"].ToString() == "5")
                {
                    generateProjectionSlidePhase2();
                }
            }
        }
   
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }
    protected void selectqry_excelReportSB()
    {

        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' Order By year, eventID, productID, phase, Level, [sublevel] ";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and sp.PubUnp='P'";
                TextLevel = TextLevel + "/P";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and sp.PubUnp='U'";
                TextLevel = TextLevel + "/U";
            }
            

            //strquery = strquery + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";
            string RoundNumberRange;
            if (dr["RoundFrom"].ToString() != "" && dr["RoundTo"].ToString() != "")
            {
                if (dr["RoundFrom"].ToString().Equals(dr["RoundTo"].ToString()))
                {
                    RoundNumberRange = "'"+dr["RoundFrom"]+"'";
                }
                else
                {
                    RoundNumberRange = "'" + dr["RoundFrom"] + " to " + dr["RoundTo"] + "'";
                }
            }
            else
            {
            RoundNumberRange="NULL";
            }

            if (Session["ddlProductGroup"].ToString().Equals("VB"))
            {
                strquery = strquery + " (select PubUnp,sp.year, eventID,ProductGroupCode,ProductCode,setid,sp.PubUnp,RandSeqID," + RoundNumberRange + " as Round,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " ) union";

            }
            else if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                if ((dr["PubUnpub"].ToString()).Equals("P"))
                {

                    strquery = strquery + " (select PubUnp,year, eventID,ProductGroupCode,ProductCode,setid,sp.PubUnp,RandSeqID, productID," + RoundNumberRange + " as Round, Phase,sp.Level, sp.[sublevel],'" + TextLevel + "' as 'Level/Sublevel',sp.Word,vn.[Parts of Speech] as POS,vn.Pronounciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master_New vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + ") union";
                }
                else if ((dr["PubUnpub"].ToString()).Equals("U"))
                {

                    strquery = strquery + " (select PubUnp,year, eventID,ProductGroupCode,ProductCode,setid,sp.PubUnp,RandSeqID, productID," + RoundNumberRange + " as Round, Phase,sp.Level, sp.[sublevel],'" + TextLevel + "' as 'Level/Sublevel',sp.Word,vn.[Parts of Speech] as POS,vn.Pronunciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + ") union";
                }
            }

            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }

        strquery = strquery.Substring(0, strquery.Length - 6);

        strquery = strquery + "   Order By year, eventID,ProductGroupCode, productCode,setid, phase,sp.PubUnp, Level, [sublevel], RandSeqID";

        Session["selectqry_excelReportSB"] = strquery;
    }
    protected void excelReport_SB()
    {
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        selectqry_excelReportSB();
        DataSet ds1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, Session["selectqry_excelReportSB"].ToString());
        //adding serial number to dataTable
        DataColumn column = ds1.Tables[0].Columns.Add("Ser#", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int iy = 1;
        foreach (DataRow dr in ds1.Tables[0].Rows)
        {
            dr["Ser#"] = iy;
            iy++;

        }
        DataTable dt1 = (DataTable)ds1.Tables[0];
        dt1.Columns.Remove("year");
        dt1.Columns.Remove("eventID");
        //dt1.Columns.Remove("productID");
        dt1.Columns.Remove("Level");
        dt1.Columns.Remove("sublevel");
        dt1.Columns.Remove("PubUnp");
        

        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+".xls");

        Response.Charset = "";
        Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dt1.Columns.Count) + " style='color:red;font-weight:bold;text-align:center;vertical-align:middle;'> Phase I: " + Session["PageTitleProductName"] + " Bee Words </td></tr>");
        Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
        foreach (DataColumn dc in dt1.Columns)
        {


            Response.Write("<th>" + dc.ColumnName + "</th>");

        }
        Response.Write("</tr>");
        Boolean phase2=true;
        Boolean phase3 = true;
        Response.Write(System.Environment.NewLine);
        foreach (DataRow dr in dt1.Rows)
        {
            if (dr["Phase"].ToString().Equals("2") && phase2==true )
            {
                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dt1.Columns.Count) + " style='color:red;font-weight:bold;text-align:center;vertical-align:middle;'> Phase II: " + Session["PageTitleProductName"] + " Bee Words </td></tr>");
                phase2 = false;
            }
            if (dr["Phase"].ToString().Equals("3") && phase3 == true)
            {
                Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dt1.Columns.Count) + " style='color:red;font-weight:bold;text-align:center;vertical-align:middle;'> Phase III: " + Session["PageTitleProductName"] + " Bee Words </td></tr>");
                phase3 = false;
            }
            Response.Write("<tr>");
            for (int i = 0; i < dt1.Columns.Count; i++)
            {
                            Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
            }
            Response.Write("</tr>");
        }

        Response.Write("</table></center>");
        Response.Flush();
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.SuppressContent = true;
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = 0;
        ViewState["increValueTemp"] = 0;
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlSet.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Set";
            lblErr.Visible = true;
        }
        else if (ddlOutput.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Output";
            lblErr.Visible = true;
        }
        else
        {
            Session["ddlYear"] = ddlYear.SelectedValue;
            Session["ddlEvent"] = ddlEvent.SelectedValue;
            Session["ddlProductGroup"] = ddlProductGroup.SelectedValue;
            Session["ddlProduct"] = ddlProduct.SelectedValue;
            Session["ddlProductText"] = ddlProduct.SelectedItem.Text;
            Session["ddlSet"] = ddlSet.SelectedValue;

            if(ddlEvent.SelectedItem.Text.Equals("Chapter"))
            {
                Session["ddlEventText"] = "Regionals";
            }else
            {
                Session["ddlEventText"] = "Finals";
            }
            

            String strcheckExistsqry;
            DataSet dscheckexists;
            if (ddlOutput.SelectedIndex == 1)
            {
                lblErr.Text = "";
                strcheckExistsqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new random words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    generatingRandomWords();
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
                
                
                
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                   lblErr.Text = "";
                strcheckExistsqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new TP words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    generateTPWords_Vocab();
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
            }
            else if (ddlOutput.SelectedIndex == 3)
            {
                lblErr.Text = "";
                lblErr1.Text = "";
                lnkClickHere1.Visible = false;
                lnkClickHere2.Visible = false;
                btnSaveWords.Enabled = false;
                btnExportExcel.Enabled = false;
                generateTestPapers();
                
            }
        }

       
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillSet();
        fillProduct();

    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
     
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e);
    }
    protected void btnSaveWords_Click(object sender, EventArgs e)
    {
        string strqry = "";
        if (ddlOutput.SelectedIndex == 1)
        {
            strqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            strqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }

        DataSet ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DateTime dt = DateTime.Now;
            // Checking Test Paper Approved or not
            string strApprovedTP = "select * from TestPapers where ContestYear=" + ddlYear.SelectedValue + " and EventId=" + ddlEvent.SelectedValue + " and SetNum=" + ddlSet.SelectedValue + " and ProductGroupCode ='" + ddlProductGroup.SelectedValue + "' and ProductCode='"+Session["ddlProduct"]+"' and DocType = 'TestP' ";
            DataSet dsApprovedStatus = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strApprovedTP);

            // Checking current date >= Contest date (based on the setID) minus 14 days; 
            string strContestDate = "select SatDay1 from WeekCalendar where WeekID=" + ddlSet.SelectedValue + "";
            DataSet dsCheckDate = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strContestDate);
            if (dsCheckDate.Tables[0].Rows.Count > 0)
            {
                dt = (DateTime)dsCheckDate.Tables[0].Rows[0]["SatDay1"];
                dt = dt.AddDays(-14).Date;
            }



            if (dsApprovedStatus.Tables[0].Rows.Count > 0)
            {
                //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "notallowedtoOverwrite();", true);
                lblErr.Text = "Cannot override, since test paper was already uploaded";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else if (DateTime.Now.Date.CompareTo(Convert.ToDateTime(dt)) > 0)
            {
                lblErr.Text = "Cannot override, since current date is Greater than Contest date ";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtooverwrite();", true);
            }

        }
        else
        {
            insertintoTables();
        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {

        if (ddlOutput.SelectedIndex == 1)
        {
            DataTable dtRandWords = (DataTable)Session["GeneratedRandomWords"];

            Response.Clear();
            string filenameProductCode = Session["ddlProduct"].ToString();
            Response.AppendHeader("content-disposition", "attachment;filename=GenerateRandomWords_"+filenameProductCode+".xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtRandWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated Random Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtRandWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtRandWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtRandWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            DataTable dtGenTPWords = (DataTable)Session["GenTPWords"];

            Response.Clear();
            string filenameProductCode = Session["ddlProduct"].ToString();
            Response.AppendHeader("content-disposition", "attachment;filename=GenerateTPWords_"+filenameProductCode+".xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtGenTPWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated TP Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtGenTPWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtGenTPWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtGenTPWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
          
            string todelete = "";

            if (ddlOutput.SelectedIndex == 1)
            {

                todelete = "delete from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                todelete = "delete from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void btnexporttopdf_Click(object sender, EventArgs e)
    {
        
        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
        {
            Session["PageTitleProductName"] = " Junior Vocab";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
        {
            Session["PageTitleProductName"] = " Intermediate Vocab";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("JSB"))
        {
            Session["PageTitleProductName"] = " Junior Spelling";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("SSB"))
        {
            Session["PageTitleProductName"] = " Senior Spelling";
        }
        string param;
        if (ddlProductGroup.SelectedValue.Equals("SB"))
        {
            if (ddlReportType.SelectedValue.Equals("1"))
            {
                report_JudgeCopy_SB();
            }
            else if (ddlReportType.SelectedValue.Equals("2"))
            {
                report_StudentCopy_SB();
            }
            else if (ddlReportType.SelectedValue.Equals("3"))
            {
                report_AnswerKey_SB();
            }
            else if (ddlReportType.SelectedValue.Equals("4"))
            {
                excelReport_SB();
            }
            else if (ddlReportType.SelectedValue.Equals("6"))
            {

                Ionic.Zip.ZipFile multipleFilesAsZipFile = new Ionic.Zip.ZipFile();

                Response.AddHeader("Content-Disposition", "attachment; filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP.zip");
                Response.ContentType = "application/zip";


                DirectoryInfo directory = new DirectoryInfo(Server.MapPath("~/zipfolder"));
                FileInfo[] filesInFolder = directory.GetFiles("*.*", SearchOption.AllDirectories);
                foreach (FileInfo fileInfo in filesInFolder)
                {
                    //checkBoxList.Items.Add(fileInfo.Name);
                    string filePath = Server.MapPath("~/zipfolder/" + fileInfo.Name);
                    multipleFilesAsZipFile.AddFile(filePath, string.Empty);
                }
                
                  
                multipleFilesAsZipFile.Save(Response.OutputStream); 

            //param = "1";
            //ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
            //param = "2";
            //ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
            //param = "3";
            //ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
            //param = "4";
            //ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
            }
            
           
        }
        else
        {
            
            if (ddlReportType.SelectedValue.Equals("1"))
            {
                pTableHeader.Visible = true;
                report_JudgeCopy_VB();
              
            }
            else if (ddlReportType.SelectedValue.Equals("2"))
            {
                report_StudentCopy();
              
            }
            else if (ddlReportType.SelectedValue.Equals("3"))
            {
                report_AnswerKey();

            }
            else if (ddlReportType.SelectedValue.Equals("4"))
            {
                excelReport_SB();
            }
            else if (ddlReportType.SelectedValue.Equals("5"))
            {
                generateProjectionSlidePhase2();
            }
            else if (ddlReportType.SelectedValue.Equals("6"))
            {
                param = "1";
                ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
                param = "2";
                ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
                param = "3";
                ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
                param = "4";
                ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
                param = "5";
                ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
            }
  
        }


    }
    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {

        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 5);
        if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //Reg_JVB & Reg_IVB
            generateTestPapers();
        }
        else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //generateTestPapers_Finals_Vocab();
        }
        else
        {
            generateTestPapers();
        }
    }
    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = Convert.ToInt32(ViewState["increValue"].ToString()) - 10;
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 5);
        if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //Reg_JVB & Reg_IVB
            generateTestPapers();
        }
        else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //generateTestPapers_Finals_Vocab();
        }
        else
        {
            generateTestPapers();
        }
    }
    protected void rpTestPaper_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        ic = Convert.ToInt32(ViewState["increValue"].ToString()) + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNo");
        lblSNo.Text = Convert.ToString(ic);

        ViewState["increValue"] = ic;

    }
    protected void lnkClickHere1_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        DataSet ds;
        if (ddlOutput.SelectedIndex == 1)
        {
            ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "");
        }
        else
        {
            ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,Phase,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[SubLevel],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '" + Session["ddlProduct"] + "' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + " Order By year, eventID,ProductGroupCode, productCode,setid, phase,PubUnp, Level, [sublevel], RandSeqID ");
        }
        
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            if (ddlOutput.SelectedIndex == 1)
            {
                Session["GeneratedRandomWords"] = ds.Tables[0];
            }
            else
            {
                Session["GenTPWords"] = ds.Tables[0];
            }

            btnSaveWords.Enabled = true;
            btnExportExcel.Enabled = true;
            divTestPapers.Visible = false;
        }
    }
    protected void lnkClickHere2_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        if (ddlOutput.SelectedIndex == 1)
        {
            generatingRandomWords();

        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            
                generateTPWords_Vocab();
           
        }
        btnSaveWords.Enabled = true;
        btnExportExcel.Enabled = true;
        divTestPapers.Visible = false;
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //Required to verify that the control is rendered properly on page
    }
    protected void rpToPDF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        
        icTemp = icTemp + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoTemp");
        lblSNo.Text = Convert.ToString(icTemp);
        
    }
    protected void rpStuPhase2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        
    }
    protected void Item_Bound(Object sender, DataListItemEventArgs e)
    {

        icStuPh2 = icStuPh2 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("SNoStuPhase2");
        lblSNo.Text = Convert.ToString(icStuPh2);

    }
    protected void Item_Bound_AnsKey(Object sender, DataListItemEventArgs e)
    {

        icAnsKey = icAnsKey + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoAnsKey");
        lblSNo.Text = Convert.ToString(icAnsKey) + ".";

    }
    protected void Item_Bound_AnsKey1(Object sender, DataListItemEventArgs e)
    {

        icAnsKey1 = icAnsKey1 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoAnsKey1");
        lblSNo.Text = Convert.ToString(icAnsKey1) + ".";

    }
    protected void Item_Bound_StuCopy(Object sender, DataListItemEventArgs e)
    {

        icStuCopy = icStuCopy + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNo");
        lblSNo.Text = Convert.ToString(icStuCopy) + ".";
        
    }
    protected void Item_Bound_StuCopyOffUse1(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse1 = icStuCopyOffUse1 + 1;
        if (Session["ddlProductGroup"].ToString().Equals("SB"))
        {
        }
        else
        {
            Label lblSNo = default(Label);
            lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse1");
            lblSNo.Text = Convert.ToString(icStuCopyOffUse1) + ".";
        }



        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange1");
        if (icStuCopyOffUse1 == 10)
        {
            if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                lblScoreRange.Text = "1-10";
            }
            else {
                lblScoreRange.Text = "Score 1-10";
            }
            
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 15)
        {
            if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                lblScoreRange.Text = "11-15";
            }
            else
            {
                lblScoreRange.Text = "Score 11-15";
            }
            //lblScoreRange.Text = "";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 20)
        {
            if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                lblScoreRange.Text = "16-20";
            }
            else
            {
                lblScoreRange.Text = "Score 16-20";
            }
           // lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 25)
        {
            if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                lblScoreRange.Text = "21-25";
            }
            else
            {
                lblScoreRange.Text = "Score 21-25";
            }
            //lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 30)
        {
            if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                lblScoreRange.Text = "26-30";
            }
            else
            {
                lblScoreRange.Text = "Score 26-30";
            }
            //lblScoreRange.Text = "Score 26-30";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }


    }
    protected void Item_Bound_StuCopyOffUse2(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse2 = icStuCopyOffUse2 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse2");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse2) + ".";

        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange2");
        if (icStuCopyOffUse2 == 10)
        {
            lblScoreRange.Text = "Score 1-10";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 15)
        {
            lblScoreRange.Text = "Score 11-15";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 30)
        {
            lblScoreRange.Text = "Score 26-30";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }

    }
    protected void Item_Bound_StuCopy1(Object sender, DataListItemEventArgs e)
    {

        icStuCopy1 = icStuCopy1 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNo1");
        lblSNo.Text = Convert.ToString(icStuCopy1) + ".";
        
    }
    protected void Item_Bound_StuCopyOffUse11(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse11 = icStuCopyOffUse11 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse11");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse11) + ".";



        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange11");

        if (icStuCopyOffUse11 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse11 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }


    }
    protected void Item_Bound_StuCopyOffUse21(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse21 = icStuCopyOffUse21 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse21");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse21) + ".";

        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange21");
        if (icStuCopyOffUse21 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse21 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }

    }
    protected void gvPubUnpubList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPubUnpubList.PageIndex = e.NewPageIndex;

        gvPubUnpubList.DataBind();
        if (ddlOutput.SelectedIndex == 1)
        {
            generatingRandomWords();
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            //generateTPWords_Vocab();

           
                generateTPWords_Vocab();
           
        }
    }
       //Generate TestPapers for Reg_JVB & Reg_IVB
#endregion
    #region <<User-defined Functions>>

   
    protected void generateTestPapers() {

        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }
            string RoundNumberRange;
            if (dr["RoundFrom"].ToString() != "" && dr["RoundTo"].ToString() != "")
            {
                if (dr["RoundFrom"].ToString().Equals(dr["RoundTo"].ToString()))
                {
                    RoundNumberRange = "'" + dr["RoundFrom"] + "'";
                }
                else
                {
                    RoundNumberRange = "'" + dr["RoundFrom"] + " to " + dr["RoundTo"] + "'";
                }
            }
            else
            {
                RoundNumberRange = "NULL";
            }
           
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";

            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and sp.PubUnp='P'";
                TextLevel = TextLevel + "/P";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and sp.PubUnp='U'";
                TextLevel = TextLevel + "/U";
            }
            if (Session["ddlProductGroup"].ToString().Equals("VB"))
            {
                strquery = strquery + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1 ) union";
            }
            else if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                if ((dr["PubUnpub"].ToString()).Equals("P"))
                {

                    strquery = strquery + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID," + RoundNumberRange + " as Round,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.[Parts of Speech] as POS,vn.Pronounciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master_New vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1 ) union";
                }
                else if ((dr["PubUnpub"].ToString()).Equals("U"))
                {

                    strquery = strquery + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID," + RoundNumberRange + " as Round,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.[Parts of Speech] as POS,vn.Pronunciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1 ) union";
                }
            }
            
           

            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);

        strquery = strquery + " Order By year, eventID,ProductGroupCode, productCode,setid, sp.phase,PubUnp, Level, [sublevel], RandSeqID ";

       
        
        divFrontEndView.Visible = true;
        totalCount = 0;
        lblTestPaperPhase.Text = string.Format("<b>Phase: I {0}- Judge Copy(Published)</b>", Session["ddlProductText"]);
  
        
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        Session["dsGenerateTestPaper"]=dsJVB;
       //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i=1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;
            
        }
          Session["GenerateTestPapers"] = dsJVB.Tables[0];
          totalCount = dsJVB.Tables[0].Rows.Count;
       //rpTestPaper.DataBind();
          if (totalCount > 0)
          {
              if (ddlProductGroup.SelectedValue.Equals("SB"))
              {
                  //rpTestPaper.DataSource = dsJVB;
                  //rpTestPaper.DataBind();
                  pTableHeader_SB.Visible = true;
                  rpSB_JudgeCopy.Visible = true;
                  rpSB_JudgeCopy.DataSource = dsJVB;
                  rpSB_JudgeCopy.DataBind();
                  divvb.Visible = false;
                  divsb.Visible = true;
              }
              else
              {
                 // bindData();
                  pTableHeader_SB.Visible = false;
                  rpSB_JudgeCopy.Visible = false;
                  rpTestPaper.DataSource = dsJVB;
                  rpTestPaper.DataBind();
                  divvb.Visible = true;
                  divsb.Visible = false;
              }
             
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = true;
              pTableHeader.Visible = false;
          }
          else
          {
              lblErr.Text = "No Record Exists!";
              lblErr.Visible = true;
              lblErr.ForeColor = Color.Red;
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = false;
          }
         
    }
   
   
   
    protected void generateTPWords_Vocab()
    {
        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' Order By year, eventID, productID, phase, Level, [sublevel]";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition="";
        string WCcolumn = "";
        for (int i = 0; i < dsSelMatrix.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[i];
            if (dr["Eventid"].ToString().Equals("2") && dr["Phase"].ToString().Equals("2"))
            { 
            WCcolumn="WordCount4";
            }
            else
            { 
            WCcolumn="WordCount1";
            }

            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()])+1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr[WCcolumn])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp='P'";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='U'";
            }

            strquery = strquery + "(select Year,EventID,SetID," + dr["Phase"] + " as Phase,'" + dr["RoundType"] + "' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sublevel],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + Session["ddlYear"] + " and Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and Level=" + dr["Level"] + " and Sublevel=" + dr["Sublevel"] + " and SetID=" + Session["ddlSet"] + " ) ";
            if (i < dsSelMatrix.Tables[0].Rows.Count - 1)
            {
                strquery = strquery + " union ";
            }
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }
        strquery = strquery + " Order By year, eventID,ProductGroupCode, productCode,setid, phase,PubUnp, Level, [sublevel], RandSeqID ";
        
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        if (dsJVB.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = dsJVB.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            Session["GenTPWords"] = dsJVB.Tables[0];
        }
        else
        {
            lblErr.Text = "No Record Exists!";
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            gvPubUnpubList.Visible = false;
        }

       

    }
  

    //Generating RandomWords
    protected void generatingRandomWords()
    {
        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' Order By year, eventID, productID, phase, Level, [sublevel]";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
       
        string PubUnpubCondition = "";
        string pubUnpub = "";
        DataTable dtnew;
        string updateqry = "";
        int k;
        string strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempSBVBRandWords')) begin drop table TempSBVBRandWords end create table TempSBVBRandWords(Year int,EventID int,SetID int,ProductGroupID int,ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20),PubUnp nvarchar(20),Level int, SubLevel int,OrigSeqID int identity(1,1) primary key,RandomNumber float, RandSeqID int,Word nvarchar(50),  CreatedDate date, CreatedBy int, ModifiedDate date, ModifiedBy int)";
        SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
        for (int i = 0; i < dsSelMatrix.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[i];
            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {


            }
            else
            {

                if ((dr["PubUnpub"].ToString()).Equals("P"))
                {
                    
                    if (ddlEvent.SelectedValue.Equals("1"))
                    {
                        PubUnpubCondition = " and  [National] = 'Y'";
                    }
                    else
                    {
                        PubUnpubCondition = "and Regional ='Y'";
                    }
                    pubUnpub = " and PubUnp='P'";
                }
                else if ((dr["PubUnpub"].ToString()).Equals("U"))
                {
                    pubUnpub = " and PubUnp='U'";
                    if (ddlEvent.SelectedValue.Equals("1"))
                    {
                        PubUnpubCondition = " and National_Reserved = 'Y'";
                    }
                    else
                    {
                        PubUnpubCondition = " and Reg_Reserved ='Y'";
                    }
                }
                string EventFlag="";
                if (Session["ddlEvent"].ToString().Equals("1"))
                {
                    EventFlag = " Nat_Flag='Y'";

                }
                else if (Session["ddlEvent"].ToString().Equals("2"))
                {
                    EventFlag = " Regional_Flag='Y'";
                }
                if (Session["ddlProductGroup"].ToString().Equals("VB"))
                {
                    strquery = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,'" + ddlProductGroup.SelectedValue + "' as ProductGroupCode, (select ProductID from Product where ProductCode='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, '" + ddlProduct.SelectedValue + "' as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then CASE WHEN National_Reserved IS NULL THEN 'P' ELSE 'U' END when 2 then CASE WHEN Reg_Reserved IS NULL THEN 'P' ELSE 'U' END end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Level=" + dr["Level"] + " and [Sub-level]=" + dr["SubLevel"] + " " + PubUnpubCondition + "))";                    
                }
                else if (Session["ddlProductGroup"].ToString().Equals("SB"))
                {
                    if ((dr["PubUnpub"].ToString()).Equals("P"))
                    {
                        strquery = "insert into TempSBVBRandWords (Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,'" + ddlProductGroup.SelectedValue + "' as ProductGroupCode, (select ProductID from Product where ProductCode='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, '" + ddlProduct.SelectedValue + "' as ProductCode,'P' as PubUnp,Level_NSF, [Sub-Level_NSF],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Word_Master_New where Level_NSF=" + dr["Level"] + " and [Sub-Level_NSF]=" + dr["SubLevel"] + " and "+EventFlag+"))";
                    }
                    else if ((dr["PubUnpub"].ToString()).Equals("U"))
                    {
                        strquery = "insert into TempSBVBRandWords (Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,'" + ddlProductGroup.SelectedValue + "' as ProductGroupCode, (select ProductID from Product where ProductCode='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, '" + ddlProduct.SelectedValue + "' as ProductCode,'U' as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Word_Master where Level=" + dr["Level"] + " and [Sub-level]=" + dr["SubLevel"] + " and Word_Master.word not in (select word from word_master_new where " + EventFlag + ")))";
                    }
                }
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strquery);
                if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
                {
                    ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
                }
                else
                {
                    ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
                }

                //Randomizing the set of records in TempTable and feeded into same table
                DataSet dsTemp = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where Level=" + dr["Level"] + " and [Sublevel]=" + dr["SubLevel"] + " " + pubUnpub + "");
                DataTable dtTemp = dsTemp.Tables[0];
                dtnew = (DataTable)dtTemp.Clone();

                updateqry = "";

                if (dtTemp.Rows.Count > 0)
                {
                    for (k = 0; k < dtTemp.Rows.Count; k++)
                    {
                        dtnew.ImportRow(dtTemp.Rows[k]);
                        dtnew.Rows[k]["RandSeqID"] = k + 1;

                        updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                              + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and Level=" + dr["Level"] + " and [Sublevel]=" + dr["SubLevel"] + " " + pubUnpub + "  ";
                    }
                }
                if (!updateqry.Equals(""))
                {
                    SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
                }

                dsTemp = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where Level=" + dr["Level"] + " and [Sublevel]=" + dr["SubLevel"] + " " + pubUnpub + " Order By RandomNumber");
                dtTemp = dsTemp.Tables[0];
                dtnew = (DataTable)dtTemp.Clone();

                updateqry = "";
                if (dtTemp.Rows.Count > 0)
                {
                    for (k = 0; k < dtTemp.Rows.Count; k++)
                    {
                        dtnew.ImportRow(dtTemp.Rows[k]);
                        dtnew.Rows[k]["RandSeqID"] = k + 1;

                        updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                              + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and Level=" + dr["Level"] + " and [Sublevel]=" + dr["SubLevel"] + " " + pubUnpub + "  ";
                    }
                }
                if (!updateqry.Equals(""))
                {
                    SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
                }
            }
        }
        DataSet ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from TempSBVBRandWords Order By year, eventID,ProductGroupCode, productCode,setid,PubUnp, Level, [sublevel], RandSeqID ");
        //DataTable dtEasy = dsEasy.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            Session["GeneratedRandomWords"] = ds.Tables[0];
        }

    }
    
    protected void fillSet()
    {

        ddlSet.Items.Clear();
        if (ddlEvent.SelectedIndex == 1)
        {
           
            ddlSet.Items.Add(new ListItem("Select Set"));


            ddlSet.Items.Add(new ListItem("4"));

            ddlSet.SelectedIndex = 1;
            ddlSet.Enabled = false;
        }
        else
        {
          
            ddlSet.Items.Add(new ListItem("Select Set"));

            for (int i = 1; i <= 3; i++)
            {
                ddlSet.Items.Add(new ListItem(i.ToString()));
            }
            ddlSet.SelectedIndex = 0;
            ddlSet.Enabled = true;
        }
    }
    protected void fillProduct()
    {

        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, ddlproductqry);
        ddlProduct.DataSource = dsstate;
        //ddlProductGroup.DataTextField = "ProductCode";
        ddlProduct.DataValueField = "ProductCode";
        ddlProduct.DataTextField = "Name";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
    }
    protected void insertintoTables()
    {
        string sql = "";
        if (ddlOutput.SelectedIndex == 1)
        {
            DataTable dt = (DataTable)Session["GeneratedRandomWords"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Year"] = "null";
                }
                if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["EventID"] = "null";
                }
                if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SetId"] = "null";
                }
                if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupID"] = "null";
                }
                if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupCode"] = "null";
                }
                if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductID"] = "null";
                }
                if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductCode"] = "null";
                }
                if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["PubUnp"] = "null";
                }
                if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Level"] = "null";
                }
                if (dt.Rows[i]["SubLevel"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SubLevel"] = "null";
                }
                if (dt.Rows[i]["RandSeqID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["RandSeqID"] = 0;
                }
                if (dt.Rows[i]["Word"].ToString().Contains("'"))
                {
                    dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                }
                sql = sql + "insert into SBVBRandWords (Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                      + dt.Rows[i]["Year"].ToString().Trim() + ","
                      + dt.Rows[i]["EventID"].ToString().Trim() + ","
                      + dt.Rows[i]["SetId"].ToString().Trim() + ","
                      + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductCode"].ToString().Trim() + "','"
                       + dt.Rows[i]["PubUnp"].ToString().Trim() + "',"
                      + dt.Rows[i]["Level"].ToString().Trim() + ","
                      + dt.Rows[i]["SubLevel"].ToString().Trim() + ","
                      + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                      + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                      + Session["LoginID"] + ")";
                }

        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            DataTable dt = (DataTable)Session["GenTPWords"];


            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Year"] = "null";
                }
                if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["EventID"] = "null";
                }
                if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SetId"] = "null";
                }
                if (dt.Rows[i]["Phase"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Phase"] = "null";
                }
                if (dt.Rows[i]["RoundType"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["RoundType"] = "null";
                }
                if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupID"] = "null";
                }
                if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupCode"] = "null";
                }
                if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductID"] = "null";
                }
                if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductCode"] = "null";
                }
                if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["PubUnp"] = "null";
                }
                if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Level"] = "null";
                }
                if (dt.Rows[i]["SubLevel"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SubLevel"] = "null";
                }
                if (dt.Rows[i]["Word"].ToString().Contains("'"))
                {
                    dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                }
                sql = sql + "insert into SpVocabTPWords (Year,EventID,SetID,Phase,RoundType,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[SubLevel],OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                      + dt.Rows[i]["Year"].ToString().Trim() + ","
                      + dt.Rows[i]["EventID"].ToString().Trim() + ","
                      + dt.Rows[i]["SetId"].ToString().Trim() + ","
                      + dt.Rows[i]["Phase"].ToString().Trim() + ",'"
                      + dt.Rows[i]["RoundType"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductCode"].ToString().Trim() + "','"
                       + dt.Rows[i]["PubUnp"].ToString().Trim() + "',"
                      + dt.Rows[i]["Level"].ToString().Trim() + ","
                      + dt.Rows[i]["SubLevel"].ToString().Trim() + ","
                      + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                      + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                      + Session["LoginID"] + ")";

            }
        }


        int j = SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, sql);

        if (j > 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Blue;
            lblErr.Text = "Saved Successfully";
        }
    }
    protected void generateProjectionSlidePhase2()
    {
              
        string strquery = "";
        selectqry_excelReportSB();
        strquery = Session["selectqry_excelReportSB"].ToString();
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;

        }
        DataTable dataTable = GetFilteredTable(dsJVB.Tables[0], "Phase = '2'");
        Session["Phase2SlideWords"] = dataTable;
       // app8
        string m_OutputFile = HttpContext.Current.Server.MapPath("~/Reports/Phase2SlideProjection.htm");
       // string m_OutputFile = HttpContext.Current.Server.MapPath("~/Phase2SlideProjection.htm");
      //  local
        //string m_OutputFile = HttpContext.Current.Server.MapPath("~/Phase2SlideProjection.htm");
        StreamWriter sw = new StreamWriter(m_OutputFile, false);

        Server.Execute("TestSlidesPh2Projection.aspx?", sw);

        sw.Flush();
        sw.Close();

        //Server.Execute("~/TestSlidesPh2Projection.aspx?");
        //ClientScript.RegisterStartupScript(Page.GetType(), "<script language='javascript'>fnOpen();</script>");
        //HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>window.open('Phase2SlideProjection.htm', '_new');</SCRIPT>");
        Response.AppendHeader("content-disposition", "attachment; filename=Phase2SlideProjection_"+Session["ddlProduct"]+".html");
        Response.TransmitFile(Server.MapPath("~/Reports/Phase2SlideProjection.htm"));
        Response.End();
    }
   
    protected void report_StudentCopy_SB() {
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        string strquery="";
        selectqry_excelReportSB();
        strquery=Session["selectqry_excelReportSB"].ToString();
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);

        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;

        }
        DataTable dataTable = GetFilteredTable(dsJVB.Tables[0], "Phase = '1'");
        //DataView DV = dsJVB.Tables[0].AsDataView();
        //DV.RowFilter = "Phase ='1'";
        //DataTable newDataTable = DV.ToTable();

        rpAnswerKey_SB.DataSource = dataTable;
        rpAnswerKey_SB.DataBind();

        dlStudentCopy_SB.DataSource=dataTable;
        dlStudentCopy_SB.DataBind();

        dlStudentCopyOffUse_SB.DataSource = dataTable;
        dlStudentCopyOffUse_SB.DataBind();

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_StudentCopy_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
       // iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg0;
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        iTextSharp.text.Paragraph pg3;
        iTextSharp.text.Paragraph pg4;
        pg0 = new iTextSharp.text.Paragraph("NORTH SOUTH FOUNDATION");
        pg1 = new iTextSharp.text.Paragraph("" + Session["ddlYear"] + " " + Session["ddlEventText"] + " " + Session["PageTitleProductName"].ToString() + " Bee - PHASE I WRITTEN TEST");
        pg2 = new iTextSharp.text.Paragraph("BADGE# ____________________ NAME:  _______________________________  Official Use Only");
       // pg3 = new iTextSharp.text.Paragraph("Please circle correct letter choice and meaning below:");
        pg4 = new iTextSharp.text.Paragraph(" ");
        pg0.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        pg1.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        //pg2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
       // pg3.Alignment = iTextSharp.text.Element.ALIGN_CENTER;

        pdfDoc.Open();

        //int ii = 0;
        int stupno = 1;

        
           
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            lblYrRegional.Text = Session["ddlYear"] + " " + Session["ddlEventText"] + "";
            lblProductName.Text = Session["ddlProductText"] + " Bee - Phase I";
            li1.Visible = true;
           
           // lblWordAnswer.visible = false;
            //FrontPage student copy
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuCopyFront.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);

            pdfDoc.NewPage();
            pdfDoc.Add(pg0);
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
           // pdfDoc.Add(pg4);

            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pStudentCopy_SB.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

      
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_StudentCopy()
    {
        string strquery = "";
        string strquery1 = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1 Order by WordCount1 desc";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];
            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp='P'";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='U'";
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }

            //if (l < 2)
            //{
            strquery = strquery + "(select  PubUnp,sp.year, eventID,ProductGroupCode,ProductCode,setid,sp.PubUnp,RandSeqID,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + ") union";
            //}
            //else
            //{
            //    strquery1 = strquery1 + "(select  '" + TextLevel + "' as Level,sp.Phase,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + ") union";
            //}
          
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
        //strquery1 = strquery1.Substring(0, strquery1.Length - 6);
        strquery = strquery + " Order By sp.year, sp.eventID,ProductGroupCode, productCode,setid, phase,sp.PubUnp, Level, sp.[sublevel], RandSeqID";
        //strquery1 = strquery1 + " Order by sp.Phase,sp.[Sublevel]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);

        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;

        }
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        dlStudentCopy.DataSource = dsJVB;
        dlStudentCopy.DataBind();
        dlStudentCopyOffUse1.DataSource = dsJVB;
        dlStudentCopyOffUse1.DataBind();
        dlStudentCopyOffUse2.DataSource = dsJVB;
        dlStudentCopyOffUse2.DataBind();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_StudentCopy_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        iTextSharp.text.Paragraph pg3;
        iTextSharp.text.Paragraph pg4;
        pg1 = new iTextSharp.text.Paragraph("" + Session["ddlYear"] + " North South Foundation Regionals " + Session["PageTitleProductName"].ToString() + " Bee Phase I ");
        pg2 = new iTextSharp.text.Paragraph("Badge# ____________________ Student Name:  __________________________");
        pg3 = new iTextSharp.text.Paragraph("Please circle correct letter choice and meaning below:");
        pg4 = new iTextSharp.text.Paragraph(" ");

        pdfDoc.Open();

        int ii = 0;
        int stupno = 1;

        if (dlStudentCopy.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            lblYrRegional.Text = Session["ddlYear"] + " Regionals";
            lblProductName.Text = Session["ddlProductText"] + " Bee - Phase I";
            //FrontPage student copy
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuCopyFront.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);
            pdfDoc.NewPage();
            // from page no:2

            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            pdfDoc.Add(pg3);
            pdfDoc.Add(pg4);
            stupno++;



            pStudentCopyPhase1.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }
        //second Page:

        //DataSet dsJVB1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery1);

        ////adding serial number to dataTable
        //DataColumn column1 = dsJVB1.Tables[0].Columns.Add("SNo", typeof(Int32));
        //column1.SetOrdinal(0);// to put the column in position 0;
        //int i1 = 16;
        //foreach (DataRow dr in dsJVB1.Tables[0].Rows)
        //{
        //    dr["SNo"] = i1;
        //    i1++;

        //}

        //dlStudentCopy1.DataSource = dsJVB1;
        //dlStudentCopy1.DataBind();
        //dlStudentCopyOffUse11.DataSource = dsJVB1;
        //dlStudentCopyOffUse11.DataBind();
        //dlStudentCopyOffUse21.DataSource = dsJVB1;
        //dlStudentCopyOffUse21.DataBind();

        //pdfDoc.Add(pg1);
        //pdfDoc.Add(pg2);
        //pdfDoc.Add(pg3);
        //pdfDoc.Add(pg4);

        //////Office user 1
        //StringWriter swHeaderOffUse1 = new StringWriter();
        //HtmlTextWriter hwHeaderOffUse1 = new HtmlTextWriter(swHeaderOffUse1);
        //pStudentCopyPhase11.RenderControl(hwHeaderOffUse1);
        //StringReader srHeaderOffUse1 = new StringReader(swHeaderOffUse1.ToString());
        //htmlparser.Parse(srHeaderOffUse1);
        // score sheet;

        StringWriter swHeader = new StringWriter();
        HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
        pScoreSheet.RenderControl(hwHeader);
        StringReader srHeader = new StringReader(swHeader.ToString());
        htmlparser.Parse(srHeader);


        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void selectqryJudgecopy()
    {
        string strquery = "";
        string strqueryUnpub = "";
        string strqryStuPh2 = "";

        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' Order By year, eventID, productID, phase, Level, [sublevel]";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

           
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and sp.PubUnp='P'";
                TextLevel = TextLevel + "/P";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and sp.PubUnp='U'";
                TextLevel = TextLevel + "/U";
            }
            //strquery = strquery + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";

            if (Session["ddlProductGroup"].ToString().Equals("VB"))
            {
                strquery = strquery + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as Level,sp.Phase,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase in (1,2) ) union";
                
            }
            else if (Session["ddlProductGroup"].ToString().Equals("SB"))
            {
                if ((dr["PubUnpub"].ToString()).Equals("P"))
                {

                    strquery = strquery + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as Level,sp.Word,vn.[Parts of Speech] as POS,vn.Pronounciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master_New vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1 ) union";
                }
                else if ((dr["PubUnpub"].ToString()).Equals("U"))
                {

                    strqueryUnpub = strqueryUnpub + " (select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as Level,sp.Word,vn.[Parts of Speech] as POS,vn.Pronunciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1 ) union";
                }
            }

            if (dr["Phase"].ToString().Equals("2"))
            {
               // strqryStuPh2 = strqryStuPh2 + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";
                if (Session["ddlProductGroup"].ToString().Equals("VB"))
                {
                    strqryStuPh2 = strqryStuPh2 + " (select '" + TextLevel + "' as Level,sp.Phase,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";

                }
                else if (Session["ddlProductGroup"].ToString().Equals("SB"))
                {
                    if ((dr["PubUnpub"].ToString()).Equals("P"))
                    {

                        strqryStuPh2 = strqryStuPh2 + " (select '" + TextLevel + "' as Level,sp.Word,vn.[Parts of Speech] as POS,vn.Pronounciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master_New vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";
                    }
                    else if ((dr["PubUnpub"].ToString()).Equals("U"))
                    {

                        strqryStuPh2 = strqryStuPh2 + " (select '" + TextLevel + "' as Level,sp.Word,vn.[Parts of Speech] as POS,vn.Pronunciation as Pronunciation,vn.[Language Origin] as Root,convert(nvarchar(max),vn.Definitions) as Definitions,convert(nvarchar(max),vn.Sentence) as Sentence from SpVocabTPWords sp inner join Word_Master vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";
                    }
                }
            }

            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }

        if (Session["ddlProductGroup"].ToString().Equals("VB"))
        {

            strquery = strquery.Substring(0, strquery.Length - 6);
            strqryStuPh2 = strqryStuPh2.Substring(0, strqryStuPh2.Length - 6);
        }
        else if (Session["ddlProductGroup"].ToString().Equals("SB"))
        {
            strquery = strquery.Substring(0, strquery.Length - 6);
            strqueryUnpub = strqueryUnpub.Substring(0, strqueryUnpub.Length - 6);
            strqryStuPh2 = strqryStuPh2.Substring(0, strqryStuPh2.Length - 6);
        }


         strquery = strquery + " Order By year, eventID,ProductGroupCode, productCode,setid, sp.phase,PubUnp, Level, [sublevel], RandSeqID ";
       // strqryStuPh2 = strqryStuPh2 + " Order by sp.Phase,sp.[Sub-level]";

        Session["selqry1"] = strquery;
        Session["selqry1Unpub"] = strqueryUnpub;
        Session["selqry2"] = strqryStuPh2;
    }
    protected void report_JudgeCopy_VB()
    {
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        string strquery = "";
        // string strqryStuPh2 = "";
        selectqry_excelReportSB();
        strquery = Session["selectqry_excelReportSB"].ToString();
        //strqryStuPh2 = Session["selqry2"].ToString();


        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;

        }

        rpToPDF.DataSource = dsJVB;
        rpToPDF.DataBind();

        int numberOfPh1records = 0;
        DataRow[] rows1;
        rows1 = dsJVB.Tables[0].Select("Phase = '1'");
        numberOfPh1records = rows1.Length;

        int numberOfPh2records = 0;
        DataRow[] rows2;
        rows2 = dsJVB.Tables[0].Select("Phase = '2'");
        numberOfPh2records = rows2.Length;

        int numberOfPh3records = 0;
        DataRow[] rows3;
        rows3 = dsJVB.Tables[0].Select("Phase = '3'");
        numberOfPh3records = rows3.Length;
       
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_JudgesCopy_Ph1_Ph2.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        // iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();

        int i = 0;
        int itemp = 0;
        int pno = 1;
        int pnoPh2 = 1;
        string pageTitle = "";
        Boolean pflag = true;
        Boolean pflag1 = true;

        string Phasedisplay = "";
        string totalPages = "";
        int noofRecordperpage = 0;
        for (i = 0; i <= rpToPDF.Items.Count - 1; i++)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            Label chk = (Label)rpToPDF.Items[i].FindControl("lblPhaseVB");
            //string PhaseValue = chk.Text;

            if (chk.Text.Equals("1"))
            {
                Phasedisplay = "Phase I: ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh1records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (chk.Text.Equals("2"))
            {
                Phasedisplay = "Phase II: ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh2records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else
            {
                Phasedisplay = "Phase III: ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh3records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }

            Label l1 = (Label)rpToPDF.Items[i].FindControl("lblPubUnpVB");
            //string PubUnpValue = l1.Text;
            string PubUnpDisplay = "";
            if (l1.Text.Equals("P"))
            {
                PubUnpDisplay = "Published";
            }
            else
            {
                PubUnpDisplay = "Unpublished";
            }
            //if()


            if (i == 0)
            {
                pno = 1;
                pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                pg1 = new iTextSharp.text.Paragraph(pageTitle);
                pg2 = new iTextSharp.text.Paragraph(" ");

                pdfDoc.Add(pg1);
                pdfDoc.Add(pg2);


                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);
            }

            if (chk.Text.Equals("2") && pflag == true)
            {
                itemp = 0;
                pno = 1;
                pdfDoc.NewPage();
                pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                pg1 = new iTextSharp.text.Paragraph(pageTitle);
                pg2 = new iTextSharp.text.Paragraph(" ");

                pdfDoc.Add(pg1);
                pdfDoc.Add(pg2);


                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);
                pflag = false;
            }
            else if (chk.Text.Equals("3") && pflag1 == true)
            {
                itemp = 0;
                pno = 1;
                pdfDoc.NewPage();
                pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                pg1 = new iTextSharp.text.Paragraph(pageTitle);
                pg2 = new iTextSharp.text.Paragraph(" ");

                pdfDoc.Add(pg1);
                pdfDoc.Add(pg2);


                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);
                pflag1 = false;
            }

            if (itemp == noofRecordperpage)
            {
                itemp = 0;
                pdfDoc.NewPage();
                pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                pg1 = new iTextSharp.text.Paragraph(pageTitle);
                pg2 = new iTextSharp.text.Paragraph(" ");

                pdfDoc.Add(pg1);
                pdfDoc.Add(pg2);


                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);
            }
            itemp++;
            rpToPDF.Items[i].RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_JudgeCopy_SB() {
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        string strquery = "";
       // string strqryStuPh2 = "";
        selectqry_excelReportSB();
        strquery = Session["selectqry_excelReportSB"].ToString();
        //strqryStuPh2 = Session["selqry2"].ToString();


        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;

        }

        rpSB_JudgeCopy.DataSource = dsJVB;
        rpSB_JudgeCopy.DataBind();

        int numberOfPh1records = 0;
        DataRow[] rows1;
        rows1 = dsJVB.Tables[0].Select("Phase = '1'");
        numberOfPh1records = rows1.Length;

        int numberOfPh2records = 0;
        DataRow[] rows2;
        rows2 = dsJVB.Tables[0].Select("Phase = '2'");
        numberOfPh2records = rows2.Length;

        int numberOfPh3records = 0;
        DataRow[] rows3;
        rows3 = dsJVB.Tables[0].Select("Phase = '3'");
        numberOfPh3records = rows3.Length;

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_JudgesCopy_Ph1_Ph2.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
       // iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();

        int i = 0;
        int itemp = 0;
        int pno = 1;
        int pnoPh2 = 1;
        string pageTitle = "";
        Boolean pflag = true;
        Boolean pflag1 = true;

        string Phasedisplay = "";
        string totalPages = "";
        int noofRecordperpage = 0;
        for (i = 0; i <= rpSB_JudgeCopy.Items.Count - 1; i++)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            Label chk = (Label)rpSB_JudgeCopy.Items[i].FindControl("lblPhase");
            //string PhaseValue = chk.Text;
           
            if (chk.Text.Equals("1"))
            {
                Phasedisplay = "Phase I: ";
                noofRecordperpage=10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh1records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (chk.Text.Equals("2"))
            {
                Phasedisplay = "Phase II: ";
                noofRecordperpage=15;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh2records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else
            {
                Phasedisplay = "Phase III: ";
                noofRecordperpage = 15;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh3records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }

            Label l1 = (Label)rpSB_JudgeCopy.Items[i].FindControl("lblPubUnp");
            //string PubUnpValue = l1.Text;
            string PubUnpDisplay = "";
            if (l1.Text.Equals("P"))
            {
                PubUnpDisplay="Published";
            }
            else
            {
                PubUnpDisplay = "Unpublished";
            }
            //if()


            if (i == 0)
            {
                pno = 1;
                pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                pg1 = new iTextSharp.text.Paragraph(pageTitle);
                pg2 = new iTextSharp.text.Paragraph(" ");

                pdfDoc.Add(pg1);
                pdfDoc.Add(pg2);


                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader_SB.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);
            }

                    if (chk.Text.Equals("2") && pflag == true)
                    {
                        itemp = 0;
                        pno = 1;
                        pdfDoc.NewPage();
                        pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                        pg1 = new iTextSharp.text.Paragraph(pageTitle);
                        pg2 = new iTextSharp.text.Paragraph(" ");

                        pdfDoc.Add(pg1);
                        pdfDoc.Add(pg2);


                        pno++;
                        StringWriter swHeader = new StringWriter();
                        HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                        pTableHeader_SB.RenderControl(hwHeader);
                        StringReader srHeader = new StringReader(swHeader.ToString());
                        htmlparser.Parse(srHeader);
                        pflag = false;
                    }
                    else if (chk.Text.Equals("3") && pflag1 == true)
                    {
                        itemp = 0;
                        pno = 1;
                        pdfDoc.NewPage();
                        pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of " + totalPages + " ";

                        pg1 = new iTextSharp.text.Paragraph(pageTitle);
                        pg2 = new iTextSharp.text.Paragraph(" ");

                        pdfDoc.Add(pg1);
                        pdfDoc.Add(pg2);


                        pno++;
                        StringWriter swHeader = new StringWriter();
                        HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                        pTableHeader_SB.RenderControl(hwHeader);
                        StringReader srHeader = new StringReader(swHeader.ToString());
                        htmlparser.Parse(srHeader);
                        pflag1 = false;
                    }

            if (itemp==noofRecordperpage )
                {
                    itemp = 0;
                    pdfDoc.NewPage();
                    pageTitle = "" + Phasedisplay + " " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy " + PubUnpDisplay + " - Pg " + pno + " of "+totalPages+" ";

                    pg1 = new iTextSharp.text.Paragraph(pageTitle);
                    pg2 = new iTextSharp.text.Paragraph(" ");
                   
                    pdfDoc.Add(pg1);
                    pdfDoc.Add(pg2);
                    
                    
                    pno++;
                    StringWriter swHeader = new StringWriter();
                    HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                    pTableHeader_SB.RenderControl(hwHeader);
                    StringReader srHeader = new StringReader(swHeader.ToString());
                    htmlparser.Parse(srHeader);
                }
            itemp++;
                rpSB_JudgeCopy.Items[i].RenderControl(hw1);
                string repeaterTable = sw1.ToString();
                StringReader sread = new StringReader(repeaterTable);

                htmlparser.Parse(sread);
            
        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
   
    
    
    public static DataTable GetFilteredTable(DataTable sourceTable, string selectFilter)
    {
        DataTable filteredTable = sourceTable.Clone();
        DataRow[] rows = sourceTable.Select(selectFilter);
        foreach (DataRow row in rows)
        {
            filteredTable.ImportRow(row);
        }
        return filteredTable;
    }
    protected void report_AnswerKey_SB()
    {
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        string strquery = "";
        selectqry_excelReportSB();
        strquery = Session["selectqry_excelReportSB"].ToString();
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Select("Phase = '1'"))
        {
            dr["SNo"] = k;
            k++;
        }

        DataTable dataTable = GetFilteredTable(dsJVB.Tables[0], "Phase = '1'");

        //DataView DV = dsJVB.Tables[0].AsDataView();
        //DV.RowFilter = "Phase ='1'";
        //DataTable newDataTable = DV.ToTable();

        rpAnswerKey_SB.DataSource = dataTable;
        rpAnswerKey_SB.DataBind();

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_AnswerKey_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);   
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
      //  iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg0;
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        iTextSharp.text.Paragraph pg3;
        string pageTitleStuPh2 = "";
        pageTitleStuPh2 = "" + Session["ddlYear"] + " " + Session["ddlEventText"] + " " + Session["PageTitleProductName"].ToString() + " Bee - PHASE I WRITTEN TEST";
        pg0 = new iTextSharp.text.Paragraph("NORTH SOUTH FOUNDATION");
        pg0.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
       // pg0.Font.SetStyle(System.Drawing.Font.BOLD);
        //pg0.Alignment=iTextSharp.text.Element.
        pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
        pg2 = new iTextSharp.text.Paragraph("ANSWER KEY");
        pg1.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        pg2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        pg3 = new iTextSharp.text.Paragraph(" ");
        pdfDoc.Open();
       // int ii = 0;
        int stupno = 1;

        if (rpAnswerKey_SB.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            pdfDoc.Add(pg0);
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            pdfDoc.Add(pg3);
            stupno++;
            rpAnswerKey_SB.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }

        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();

    }
    protected void report_AnswerKey()
    {
        string strquery = "";
        string strquery1 = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1 Order by WordCount1 desc";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int i = 0; i < dsSelMatrix.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[i];
            
            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

           
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp='P'";
                TextLevel = TextLevel + "/P";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='U'";
                TextLevel = TextLevel + "/U";
            }
            //if (i < 2)
            //{
            strquery = strquery + "(select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as 'Level/Sublevel',sp.Phase,sp.Level,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + "   )  union ";
            //}
            //else
            //{
            //    strquery1 = strquery1 + "(select sp.year,eventid,productgroupcode,productcode,setid,sp.phase,PubUnp,RandSeqID,'" + TextLevel + "' as Level,sp.Phase,sp.[Sublevel],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sublevel]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + "   ) union ";
            //}
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
            else
            {
                ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()] = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
       // strquery1 = strquery1.Substring(0, strquery1.Length - 6);
        strquery = strquery + " Order By year, eventID,ProductGroupCode, productCode,setid, sp.phase,PubUnp, Level, [sublevel], RandSeqID";
        //strquery1 = strquery1 + " Order By year, eventID,ProductGroupCode, productCode,setid, sp.phase,PubUnp, Level, [sublevel], RandSeqID";

             
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;
        }

        rpAnswerKey.DataSource = dsJVB;
        rpAnswerKey.DataBind();
        string TagName = "";
        if (Session["ddlEvent"].ToString().Equals("1"))
        {
            TagName = "25";
        }
        else
        {
            TagName = "05";
        }
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_"+TagName+"_AnswerKey_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
        pdfDoc.SetMargins(12f, 12f, 12f, 12f);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        string pageTitleStuPh2 = "";
        pageTitleStuPh2 = "" + Session["ddlYear"] + " North South Foundation Regionals " + Session["PageTitleProductName"].ToString() + " Bee Phase I (Answer Key)";
        pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
        pg2 = new iTextSharp.text.Paragraph(" ");
        pdfDoc.Open();
        int ii = 0;
        int stupno = 1;

        if (rpAnswerKey.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER);
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            stupno++;
            pAnswerKey.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }

        // second Page
      
        // Judge Copy Phase1 & Phase2
        
        //DataSet dsJVB1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery1);
        ////adding serial number to dataTable
        //DataColumn column1 = dsJVB1.Tables[0].Columns.Add("SNo", typeof(Int32));
        //column1.SetOrdinal(0);// to put the column in position 0;
        //int k1 = 16;
        //foreach (DataRow dr in dsJVB1.Tables[0].Rows)
        //{
        //    dr["SNo"] = k1;
        //    k1++;
        //}
        //rpAnswerKey1.DataSource = dsJVB1;
        //rpAnswerKey1.DataBind();
        //pdfDoc.NewPage();
        //pdfDoc.Add(pg1);
        //pdfDoc.Add(pg2);
        //StringWriter sw11 = new StringWriter();
        //HtmlTextWriter hw11 = new HtmlTextWriter(sw11);
        //pAnswerKey1.RenderControl(hw11);
        //StringReader sr11 = new StringReader(sw11.ToString());
        //htmlparser.Parse(sr11);

        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    #endregion
}