
Imports System
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports NorthSouth.BAL
Imports System.Net
Imports System.Net.Mail
Imports System.Collections.Specialized
Imports LinkPointTransaction
Imports System.Text.RegularExpressions
Imports nsf.Entities
Imports GDE4
Imports GDE4.Service
Imports GDE4.Transaction
Namespace VRegistration

    Partial Class Refund
        Inherits LinkPointAPI_cs.LinkPointTxn_Page
        Dim isTestMode As Boolean = False
        '*********************************
        Dim Cust_id As String = String.Empty
        Private nRefundAmt As Decimal = 0
        Dim txnStatus As String = String.Empty
        Dim txtAuth As String = String.Empty
        Dim custIndId As Integer = 0
        Dim e4_Auth As String = String.Empty
        Dim e4_Tag As String = String.Empty
        Dim e4_BankMessage As String = String.Empty
        Dim e4_result As String = String.Empty
        Dim e4_Id As String = String.Empty
        Dim e4_Pass As String = String.Empty
        Dim e4_Platform As String = String.Empty
        Dim Re_Donation As Decimal = 0
        Dim Re_Regfee As Decimal = 0
        Dim Re_Meals As Decimal = 0
        Dim Re_Latefee As Decimal = 0
        Dim Re_Amount As Decimal = 0
        '**************************************


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If

            'Put user code to initialize the page here
            If Page.IsPostBack = False Then
                Session("whereChapter") = " "
                Session("StrContestIDs") = Nothing
                Session("StrRegisIDs") = Nothing
                Session("StrMealChargeIDs") = Nothing
                Session("Pending") = False
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    txtUserId.Text = Trim(Session("LoginEmail"))
                    txtUserId.Enabled = False
                    tblSelection.Visible = False
                    hlnkMainMenu.NavigateUrl = "UserFunctions.aspx"
                    'GetNFGRecords()
                    'GetRefundRecords()
                    trContinue.Visible = True
                    GridNFG.Columns(1).Visible = False
                    GridRefund.Columns(0).Visible = False
                    GridRefund.Columns(1).Visible = False
                    GridRefund.Columns(23).Visible = False
                    GridRefund.Columns(24).Visible = False
                ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    trNaStatus.Visible = True
                    If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                        trContinue.Visible = True
                        'tblSelection.Visible = True
                        'trNewUpdate.Visible = False
                        If (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Then
                            Session("whereChapter") = " AND i.ChapterID=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select ChapterID from Volunteer where roleID=" & Session("RoleId").ToString() & " AND  memberid=" & Session("LoginID") & "")
                        End If
                    End If
                End If
            End If
        End Sub

        Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
            If DdlNewUpdate.SelectedValue > 0 Then
                If DdlNewUpdate.SelectedValue = 1 Then
                    GridRefund.Columns(0).Visible = False
                    GridRefund.Columns(1).Visible = False
                    GridNFG.Columns(1).Visible = True
                ElseIf DdlNewUpdate.SelectedValue = 2 Then
                    If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                        GridNFG.Columns(1).Visible = False
                    ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                        GridNFG.Columns(1).Visible = True
                    Else
                        GridNFG.Columns(1).Visible = False
                    End If
                    GridRefund.Columns(0).Visible = True
                    GridRefund.Columns(1).Visible = True
                End If
                If txtUserId.Text.Length < 3 And DdlNewUpdate.SelectedValue = 1 Then
                    lblError.Text = "Please Enter Email"
                Else
                    tblSelection.Visible = True
                    If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                            If DdlNewUpdate.SelectedValue = 1 Then
                                GetNFGRecords()
                            End If
                            GetRefundRecords()
                        End If
                    End If
                    trContinue.Visible = False
                    lblError.Visible = False
                    DdlNewUpdate.Enabled = False
                End If
            Else
                lblError.Visible = True
                lblError.Text = "Please select Option New/Update"
            End If

        End Sub
        Protected Sub btnSel1Continue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSel1Continue.Click
            Panel3.Visible = False
            If DdlReason.SelectedValue > 0 And DdlEventID.SelectedValue > 0 And DdlNewUpdate.SelectedValue > 0 Then
                ClearTxtBox()
                If DdlReason.SelectedValue = 1 Then
                    lblError.Visible = False
                    DdlEventID.Enabled = False
                    DdlReason.Enabled = False
                    If DdlNewUpdate.SelectedValue = 1 Then
                        GetNFGRecords()
                    End If
                    GetRefundRecords()
                    showdatas()
                ElseIf DdlReason.SelectedValue = 2 And DdlNewUpdate.SelectedValue = 1 Then
                    If txtUserId.Text.Length < 3 Then
                        lblError.Visible = True
                        lblError.Text = "E-Mail must not be blank"
                    Else
                        lblError.Text = ""
                        DdlEventID.Enabled = False

                        txtRefundAmt.Enabled = False
                        txtNaRefundAmt.Enabled = False
                        txtChRefundAmt.Enabled = False
                        txtRefundAmt.Text = "Not Needed"
                        TxtONo.Text = "Not Needed"
                        TxtPDate.Text = "Not Needed"
                        txtNaRefundAmt.Text = "Not Needed"
                        txtChRefundAmt.Text = "Not Needed"

                        If DdlNewUpdate.SelectedValue = 1 Then
                            GetNFGRecords()
                        End If
                        GetRefundRecords()
                        GridNFG.Visible = False
                        'pnl1Msg2.Text = ""
                        showdatas()
                        DdlReason.Enabled = False
                        DdlEventID.Enabled = False
                        btnSel1Continue.Enabled = False
                        'to show details in Panel3
                        Dim memid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                        showPendingdetails(memid, 0)
                    End If
                ElseIf DdlReason.SelectedValue = 2 And DdlNewUpdate.SelectedValue = 2 Then
                    lblError.Text = ""
                    If DdlNewUpdate.SelectedValue = 1 Then
                        GetNFGRecords()
                    End If
                    GetRefundRecords()
                    DdlEventID.Enabled = False

                    txtRefundAmt.Enabled = False
                    txtNaRefundAmt.Enabled = False
                    txtChRefundAmt.Enabled = False
                    txtRefundAmt.Text = "Not Needed"
                    txtNaRefundAmt.Text = "Not Needed"
                    txtChRefundAmt.Text = "Not Needed"

                    DdlReason.Enabled = False
                    DdlEventID.Enabled = False
                    btnSel1Continue.Enabled = False
                    GridNFG.Visible = False
                    showdatas()
                ElseIf DdlReason.SelectedValue = 3 Then
                    lblError.Visible = False
                    DdlReason.Enabled = False
                    DdlEventID.Enabled = False
                    txtCardFNo.Enabled = False
                    txtCardLNo.Enabled = False
                    txtAmtCharged.Enabled = False
                    txtDOCharged.Enabled = False
                    '** Added 25/5/2010
                    TxtNaPNotes.Enabled = True

                    txtCardFNo.Text = "Not Needed"
                    txtCardLNo.Text = "Not Needed"
                    txtAmtCharged.Text = "Not Needed"
                    txtDOCharged.Text = "Not Needed"
                    '**25/5/2010
                    'TxtNaPNotes.Text = "Not Needed"
                    If DdlNewUpdate.SelectedValue = 1 Then
                        GetNFGRecords()
                    End If
                    GetRefundRecords()
                    showdatas()

                Else
                    'If other The Reason 1,2,3 is selected means
                    lblError.Visible = False
                    txtCardFNo.Enabled = False
                    txtCardLNo.Enabled = False
                    txtCardFNo.Text = "Not Needed"
                    txtCardLNo.Text = "Not Needed"
                    DdlReason.Enabled = False
                    DdlEventID.Enabled = False
                    If DdlNewUpdate.SelectedValue = 1 Then
                        GetNFGRecords()
                    End If
                    GetRefundRecords()
                    showdatas()
                End If
            Else
                lblError.Visible = True
                lblError.Text = "Please Select all Dropdown options"
            End If

        End Sub
        Sub showdatas()
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                tblData.Visible = True
                pnlChapter.Visible = False
                PnlNational.Visible = False
                trParentButton.Visible = True
            ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                    tblData.Visible = True
                    pnlChapter.Visible = True
                    PnlNational.Visible = True

                    'Hide parent & Chapters Buttons
                    TrChButton.Visible = False
                    trNaButton.Visible = True
                    trParentButton.Visible = False
                ElseIf (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Then
                    tblData.Visible = True
                    pnlChapter.Visible = True
                    PnlNational.Visible = False

                    'Hide parent  Buttons
                    trParentButton.Visible = False
                    TrChButton.Visible = True

                End If

                'For Volunteers to add refund request when dropdown button has new option
                'If DdlNewUpdate.SelectedValue = 1 Then
                trParentButton.Visible = True
                'If
            End If
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Try
                Dim flag As Integer = 0

                'For Parents

                If DdlReason.SelectedValue = 1 Then
                    If DdlEventID.SelectedValue < 1 Then
                        lblDataErr.Text = "Please Select Event"
                    ElseIf txtRefundAmt.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Refund Amount"
                    ElseIf txtAmtCharged.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Amount Charged on your bill"
                    ElseIf txtDOCharged.Text.Length < 6 Then
                        lblDataErr.Text = "Please Enter date as MM/DD/YYYY"
                    ElseIf txtExplanation.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Detailed Explanation"
                    ElseIf TxtONo.Text.Length < 1 Then
                        lblDataErr.Text = "Please select the Applicable Payments"
                    ElseIf txtCardFNo.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter First four digits of Credit Card"
                    ElseIf txtCardLNo.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Last four digits of Credit Card"
                    Else

                        ' Do All operation related to Duplicate 
                        Dim StrSql As String

                        'Now for Update or New
                        If btnSubmit.Text = "Submit" Then

                            'Coding to Insert new
                            Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                            StrSql = "Insert into Refund (MemberID, ChapterID, EventID, Pa_ReqRefund, Pa_Reason,"
                            StrSql = StrSql & "Pa_ReqDate, pa_AmountCharged, Pa_ChargedDate, Pa_Comments,Pa_OrderNumber, Pa_Paymentdate,Alpha, Beta, CreatedDate, CreatedBy,Ch_Status,Na_Status) Values("
                            StrSql = StrSql & memberID & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", memberID))
                            StrSql = StrSql & "," & DdlEventID.SelectedValue & "," & Val(txtRefundAmt.Text) & ",'" & DdlReason.SelectedItem.Text & "',GetDate()," & Val(txtAmtCharged.Text) & ",'" & txtDOCharged.Text
                            StrSql = StrSql & "','" & txtExplanation.Text.Replace("'", "''") & "','" & TxtONo.Text & "','" & TxtPDate.Text & "'," & Val(txtCardFNo.Text) & "," & Val(txtCardLNo.Text) & ",GetDate()," & Session("LoginID") & ",'Pending','Pending')"
                            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "'")
                            If flag < 1 Then
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                If flag > 0 Then
                                    SendEMail(txtUserId.Text, 0, 0)
                                    clear()
                                    'clearNewUpdate()
                                    GetRefundRecords()
                                Else
                                    lblDataErr.Text = "Error In Records"
                                End If
                            Else
                                lblDataErr.Text = "Order Number Is already in Refund Request"
                            End If
                        Else
                            btnSubmit.Text = "Submit"
                            'coding to update the parent values.
                            StrSql = "UPDATE Refund SET Pa_ReqRefund=" & Val(txtRefundAmt.Text) & ", pa_AmountCharged=" & Val(txtAmtCharged.Text) & ", Pa_ChargedDate='" & txtDOCharged.Text & "', Pa_Comments='" & txtExplanation.Text.Replace("'", "''") & "', Pa_OrderNumber='" & TxtONo.Text & "', Pa_Paymentdate='" & TxtPDate.Text & "', Alpha=" & Val(txtCardFNo.Text) & ", Beta=" & Val(txtCardLNo.Text) & ", ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & " where refundid=" & Session("RefundID")
                            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "' AND refundid <>" & Session("RefundID") & "")
                            If flag < 1 Then
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                If flag > 0 Then
                                    clear()

                                    'clearNewUpdate()
                                    GetRefundRecords()
                                Else
                                    lblDataErr.Text = "Error In Records"
                                End If
                            Else
                                lblDataErr.Text = "Order Number is already in Refund Request"
                            End If
                        End If
                    End If

                ElseIf DdlReason.SelectedValue = 2 Then

                    'For pending but charged
                    If DdlEventID.SelectedValue < 1 Then
                        lblDataErr.Text = "Please Select Event"
                    ElseIf txtAmtCharged.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Amount Charged on your bill"
                    ElseIf txtDOCharged.Text.Length < 6 Then
                        lblDataErr.Text = "Please Enter date as MM/DD/YYYY"
                    ElseIf txtExplanation.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Detailed Explanation"
                    ElseIf txtCardFNo.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter First four digits of Credit Card"
                    ElseIf txtCardLNo.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Last four digits of Credit Card"
                    Else

                        ' Do All operation related to Pending, but was Charged 
                        Dim StrSql As String

                        'Now for Update or New
                        If btnSubmit.Text = "Submit" Then
                            ' UpdateTable()
                            If Session("Pending") = True Then
                                'Coding to Insert new
                                Session("Pending") = False
                                Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                                flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM  Refund WHERE CONVERT(VARCHAR(10), TempLink, 101) = CONVERT(VARCHAR(10), GETDATE(), 101) AND MemberID=" & memberID & "")
                                If flag < 1 Then
                                    StrSql = "Insert into Refund (MemberID, ChapterID, EventID, Pa_Reason,"
                                    StrSql = StrSql & "Pa_ReqDate, pa_AmountCharged, Pa_ChargedDate, Pa_Comments,Alpha, Beta, CreatedDate, CreatedBy,Ch_Status,Na_Status,TempLink) Values("
                                    StrSql = StrSql & memberID & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", memberID))
                                    StrSql = StrSql & "," & DdlEventID.SelectedValue & ",'" & DdlReason.SelectedItem.Text & "',GetDate()," & Val(txtAmtCharged.Text) & ",'" & txtDOCharged.Text
                                    StrSql = StrSql & "','" & txtExplanation.Text.Replace("'", "''") & "'," & Val(txtCardFNo.Text) & "," & Val(txtCardLNo.Text) & ",GetDate()," & Session("LoginID") & ",'Pending','Pending',GetDate())"
                                    flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                    If flag > 0 Then
                                        Try
                                            If Session("StrContestIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Getdate() where  contestant_id IN (" & Session("StrContestIDs") & ")")
                                                Session("StrContestIDs") = Nothing
                                            End If
                                            If Session("StrRegisIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Getdate() where  regid IN (" & Session("StrRegisIDs") & ")")
                                                Session("StrRegisIDs") = Nothing
                                            End If
                                            If Session("StrMealChargeIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Getdate() where  MealChargeId IN (" & Session("StrMealChargeIDs") & ")")
                                                Session("StrMealChargeIDs") = Nothing
                                            End If
                                            If Session("StrCoachingIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                                                Session("StrCoachingIDs") = Nothing
                                            End If
                                            SendEMail(txtUserId.Text, 0, 0)
                                            GetRefundRecords()
                                        Catch ex As Exception
                                            'MsgBox(ex.ToString())
                                        End Try
                                        clear()
                                        'clearNewUpdate()
                                    Else
                                        lblDataErr.Text = "Error In Records"
                                    End If
                                Else
                                    'If he have submitted one Pending but charged today
                                    lblDataErr.Text = "You Can Request for Pending only once in a day"
                                End If
                            Else
                                lblDataErr.Text = "You must have atleast one item selected in Panel3"
                            End If

                        Else
                            'coding to update the parent values for  Pending, but was Charged 
                            StrSql = "UPDATE Refund SET pa_AmountCharged=" & Val(txtAmtCharged.Text) & ", Pa_ChargedDate='" & txtDOCharged.Text & "', Pa_Comments='" & txtExplanation.Text.Replace("'", "''") & "', Alpha=" & Val(txtCardFNo.Text) & ", Beta=" & Val(txtCardLNo.Text) & ", ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & " where refundid=" & Session("RefundID")
                            flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                            If flag > 0 Then
                                btnSubmit.Text = "Submit"
                                If Session("Pending") = True Then
                                    Dim i As Integer = 0
                                    If Session("StrContestIDs") <> Nothing Then
                                        i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Getdate() where  contestant_id IN (" & Session("StrContestIDs") & ")")
                                        Session("StrContestIDs") = Nothing
                                    End If
                                    If Session("StrRegisIDs") <> Nothing Then
                                        i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Getdate() where  regid IN (" & Session("StrRegisIDs") & ")")
                                        Session("StrRegisIDs") = Nothing
                                    End If
                                    If Session("StrMealChargeIDs") <> Nothing Then
                                        i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Getdate() where  MealChargeId IN (" & Session("StrMealChargeIDs") & ")")
                                        Session("StrMealChargeIDs") = Nothing
                                    End If
                                    If Session("StrCoachingIDs") <> Nothing Then
                                        i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                                        Session("StrCoachingIDs") = Nothing
                                    End If
                                    Session("Pending") = False

                                    'Need MemberID here

                                    If i > 0 Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Refund set TempLink=Getdate() where  RefundID=" & Session("refundID") & "")
                                        Dim Mins As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "SELECT DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') FROM Refund  where refundid=" & Session("refundID") & "")
                                        Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                                        If (Mins > 2) Or (Mins < 2) Then
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND ParentID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND MemberID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND AutoMemberID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND PMemberID = " & memberID & "")
                                        Else
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0)  AND ParentID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0)  AND MemberID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0) AND AutoMemberID = " & memberID & "")
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0) AND PMemberID = " & memberID & "")
                                        End If
                                    End If
                                End If
                                clear()
                                'clearNewUpdate()
                                GetRefundRecords()
                            Else
                                lblDataErr.Text = "Error In Records"
                            End If
                        End If
                    End If
                ElseIf DdlReason.SelectedValue < 1 Then
                    lblDataErr.Text = "Please Select Reason"
                ElseIf DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Then
                    '**   Selected is Event Cancelled && Hardship *****************
                    If DdlEventID.SelectedValue < 1 Then
                        lblDataErr.Text = "Please Select Event"
                    ElseIf txtRefundAmt.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Refund Amount"
                    ElseIf txtExplanation.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Detailed Explanation"
                    ElseIf TxtONo.Text.Length < 1 Then
                        lblDataErr.Text = "Please select the Applicable Payments"
                    ElseIf Session("Pending") = True And lblTotalFeeMsg.Text <> txtRefundAmt.Text Then
                        lblDataErr.Text = "Mismatch between Refund amount and Selected Amount"
                    Else
                        Dim StrSql As String
                        'Now for Update or New
                        If btnSubmit.Text = "Submit" Then
                            'Coding to Insert new
                            If Session("Pending") = True Then
                                'Coding to Insert new
                                Session("Pending") = False
                                Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                                StrSql = "Insert into Refund (MemberID, ChapterID, EventID, Pa_ReqRefund, Pa_Reason,"
                                StrSql = StrSql & "Pa_ReqDate, Pa_Comments,Pa_OrderNumber, Pa_Paymentdate, CreatedDate, CreatedBy,Ch_Status,Na_Status,Na_OrderNumber,Na_PaymentDate,TempLink,Na_PaymentNotes) Values("
                                StrSql = StrSql & memberID & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", memberID))
                                StrSql = StrSql & "," & DdlEventID.SelectedValue & "," & Val(txtRefundAmt.Text) & ",'" & DdlReason.SelectedItem.Text & "',GetDate(),'"
                                StrSql = StrSql & txtExplanation.Text.Replace("'", "''") & "','" & TxtONo.Text & "','" & TxtPDate.Text & "',GetDate()," & Session("LoginID") & ",'Pending','Pending','" & TxtNaONo.Text & "','" & TxtPDate.Text & "',GetDate(),'" & TxtNaPNotes.Text & "')"
                                flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "'")
                                ''** Added on Jan 18 th to Have more Record for same Order Number
                                Dim RFlag As Boolean = False
                                If flag > 0 Then
                                    RFlag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN SUM(R.na_amount) < SUM(N.TotalPayment) THEN 'True' Else 'False' END    from NFG_Transactions N Inner Join Refund R ON R.Na_OrderNumber = N.asp_session_id where N.TotalPayment > 0 and N.asp_session_id='" & TxtONo.Text & "'")
                                Else
                                    RFlag = True
                                End If
                                If RFlag = True Then
                                    flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                    If flag > 0 Then
                                        Try
                                            If Session("StrContestIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Getdate() where  contestant_id IN (" & Session("StrContestIDs") & ")")
                                                Session("StrContestIDs") = Nothing
                                            End If
                                            If Session("StrRegisIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Getdate() where  regid IN (" & Session("StrRegisIDs") & ")")
                                                Session("StrRegisIDs") = Nothing
                                            End If
                                            If Session("StrMealChargeIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Getdate() where  MealChargeId IN (" & Session("StrMealChargeIDs") & ")")
                                                Session("StrMealChargeIDs") = Nothing
                                            End If
                                            If Session("StrCoachingIDs") <> Nothing Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                                                Session("StrCoachingIDs") = Nothing
                                            End If
                                            SendEMail(txtUserId.Text, 0, 0)
                                            GetRefundRecords()
                                        Catch ex As Exception
                                            'MsgBox(ex.ToString())
                                        End Try

                                        clear()
                                        'clearNewUpdate()
                                        GetRefundRecords()
                                    Else
                                        lblDataErr.Text = "Error In Records"
                                    End If
                                Else
                                    lblDataErr.Text = "Order Number Is already in Refund Request"
                                End If
                            Else
                                lblDataErr.Text = "Please select the Cancelled Event in Panel3"
                            End If

                        Else

                            'coding to update the for Event Cancelled
                            StrSql = "UPDATE Refund SET Pa_ReqRefund=" & Val(txtRefundAmt.Text) & ", Pa_Comments='" & txtExplanation.Text.Replace("'", "''") & "', Pa_OrderNumber='" & TxtONo.Text & "', Pa_Paymentdate='" & TxtPDate.Text & "', ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & ",Na_OrderNumber='" & TxtNaONo.Text & "',Na_PaymentDate='" & TxtPDate.Text & "',Na_PaymentNotes='" & TxtNaPNotes.Text & "' where refundid=" & Session("RefundID")
                            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "' AND refundid <>" & Session("RefundID") & "")
                            If flag < 1 Then
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                If flag > 0 Then
                                    btnSubmit.Text = "Submit"
                                    If Session("Pending") = True Then
                                        Dim i As Integer = 0
                                        If Session("StrContestIDs") <> Nothing Then
                                            i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Getdate() where  contestant_id IN (" & Session("StrContestIDs") & ")")
                                            Session("StrContestIDs") = Nothing
                                        End If
                                        If Session("StrRegisIDs") <> Nothing Then
                                            i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Getdate() where  regid IN (" & Session("StrRegisIDs") & ")")
                                            Session("StrRegisIDs") = Nothing
                                        End If
                                        If Session("StrMealChargeIDs") <> Nothing Then
                                            i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Getdate() where  MealChargeId IN (" & Session("StrMealChargeIDs") & ")")
                                            Session("StrMealChargeIDs") = Nothing
                                        End If
                                        If Session("StrCoachingIDs") <> Nothing Then
                                            i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                                            Session("StrCoachingIDs") = Nothing
                                        End If
                                        Session("Pending") = False
                                        If i > 0 Then
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Refund set TempLink=Getdate() where  RefundID=" & Session("refundID") & "")
                                            Dim Mins As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "SELECT DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') FROM Refund  where refundid=" & Session("refundID") & "")
                                            Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                                            If (Mins > 2) Or (Mins < 2) Then
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1)  AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND PaymentReference='" & TxtONo.Text & "'")
                                            Else
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0)   AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Registration set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0)   AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0)  AND PaymentReference='" & TxtONo.Text & "'")
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0) AND PaymentReference='" & TxtONo.Text & "'")
                                            End If
                                        End If

                                    End If
                                    clear()
                                    'clearNewUpdate()
                                    GetRefundRecords()
                                Else
                                    lblDataErr.Text = "Error In Records"
                                End If
                            Else
                                lblDataErr.Text = "Order Number is already in Refund Request"
                            End If
                        End If
                    End If
                    '******** Ferdine Silvaa 09/09/2010 ***************
                    ''ElseIf DdlReason.SelectedValue = 4 And DdlEventID.SelectedValue = 13 Then
                    ''    '**   Selected is Refund Donation For Coaching *****************
                    ''    If DdlEventID.SelectedValue < 1 Then
                    ''        lblDataErr.Text = "Please Select Event"
                    ''    ElseIf txtRefundAmt.Text.Length < 1 Then
                    ''        lblDataErr.Text = "Please Enter Refund Amount"
                    ''    ElseIf txtExplanation.Text.Length < 1 Then
                    ''        lblDataErr.Text = "Please Enter Detailed Explanation"
                    ''    ElseIf TxtONo.Text.Length < 1 Then
                    ''        lblDataErr.Text = "Please select the Applicable Payments"
                    ''    ElseIf Session("Pending") = True And lblTotalFeeMsg.Text <> txtRefundAmt.Text Then
                    ''        lblDataErr.Text = "Mismatch between Refund amount and Selected Amount"
                    ''    Else
                    ''        Dim StrSql As String
                    ''        'Now for Update or New
                    ''        If btnSubmit.Text = "Submit" Then
                    ''            'Coding to Insert new
                    ''            If Session("Pending") = True Then
                    ''                'Coding to Insert new
                    ''                Session("Pending") = False
                    ''                Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                    ''                StrSql = "Insert into Refund (MemberID, ChapterID, EventID, Pa_ReqRefund, Pa_Reason,"
                    ''                StrSql = StrSql & "Pa_ReqDate, Pa_Comments,Pa_OrderNumber, Pa_Paymentdate, CreatedDate, CreatedBy,Ch_Status,Na_Status,Na_OrderNumber,Na_PaymentDate,TempLink,Na_PaymentNotes) Values("
                    ''                StrSql = StrSql & memberID & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", memberID))
                    ''                StrSql = StrSql & "," & DdlEventID.SelectedValue & "," & Val(txtRefundAmt.Text) & ",'" & DdlReason.SelectedItem.Text & "',GetDate(),'"
                    ''                StrSql = StrSql & txtExplanation.Text.Replace("'", "''")& "','" & TxtONo.Text & "','" & TxtPDate.Text & "',GetDate()," & Session("LoginID") & ",'Pending','Pending','" & TxtNaONo.Text & "','" & TxtPDate.Text & "',GetDate(),'" & TxtNaPNotes.Text & "')"
                    ''                flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "'")
                    ''                If flag < 1 Then
                    ''                    flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                    ''                    If flag > 0 Then
                    ''                        Try
                    ''                            If Session("StrCoachingIDs") <> Nothing Then
                    ''                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                    ''                                Session("StrCoachingIDs") = Nothing
                    ''                            End If
                    ''                            SendEMail(txtUserId.Text, 0, 0)
                    ''                            GetRefundRecords()
                    ''                        Catch ex As Exception
                    ''                            'MsgBox(ex.ToString())
                    ''                        End Try

                    ''                        clear()
                    ''                        'clearNewUpdate()
                    ''                        GetRefundRecords()
                    ''                    Else
                    ''                        lblDataErr.Text = "Error In Records"
                    ''                    End If
                    ''                Else
                    ''                    lblDataErr.Text = "Order Number Is already in Refund Request"
                    ''                End If
                    ''            Else
                    ''                lblDataErr.Text = "Please select the Coaching in Panel3"
                    ''            End If

                    ''        Else

                    ''            'coding to Refund Donation For Coaching
                    ''            StrSql = "UPDATE Refund SET Pa_ReqRefund=" & Val(txtRefundAmt.Text) & ", Pa_Comments='" & txtExplanation.Text.Replace("'", "''")& "', Pa_OrderNumber='" & TxtONo.Text & "', Pa_Paymentdate='" & TxtPDate.Text & "', ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & ",Na_OrderNumber='" & TxtNaONo.Text & "',Na_PaymentDate='" & TxtPDate.Text & "',Na_PaymentNotes='" & TxtNaPNotes.Text & "' where refundid=" & Session("RefundID")
                    ''            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "' AND refundid <>" & Session("RefundID") & "")
                    ''            If flag < 1 Then
                    ''                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                    ''                If flag > 0 Then
                    ''                    btnSubmit.Text = "Submit"
                    ''                    If Session("Pending") = True Then
                    ''                        Dim i As Integer = 0

                    ''                        If Session("StrCoachingIDs") <> Nothing Then
                    ''                            i += SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Getdate() where  CoachRegID IN (" & Session("StrCoachingIDs") & ")")
                    ''                            Session("StrCoachingIDs") = Nothing
                    ''                        End If
                    ''                        Session("Pending") = False
                    ''                        If i > 0 Then
                    ''                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Refund set TempLink=Getdate() where  RefundID=" & Session("refundID") & "")
                    ''                            Dim Mins As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "SELECT DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') FROM Refund  where refundid=" & Session("refundID") & "")
                    ''                            Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                    ''                            If (Mins > 2) Or (Mins < 2) Then
                    ''                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 1) AND PaymentReference='" & TxtONo.Text & "'")
                    ''                            Else
                    ''                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CoachReg set TempLink=Null where  (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') >= - 1) AND (DATEDIFF(minute, TempLink, '" & lblHidden.Text & "') <= 0) AND PaymentReference='" & TxtONo.Text & "'")
                    ''                            End If
                    ''                        End If

                    ''                    End If
                    ''                    clear()
                    ''                    'clearNewUpdate()
                    ''                    GetRefundRecords()
                    ''                Else
                    ''                    lblDataErr.Text = "Error In Records"
                    ''                End If
                    ''            Else
                    ''                lblDataErr.Text = "Order Number is already in Refund Request"
                    ''            End If
                    ''        End If
                    ''    End If
                Else
                    'coding for insert & update for  Refund Donation ''Remove Hardship 
                    '** Ferdine Silvaa 28/05/2010
                    If DdlEventID.SelectedValue < 1 Then
                        lblDataErr.Text = "Please Select Event"
                    ElseIf txtRefundAmt.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Refund Amount"
                    ElseIf txtExplanation.Text.Length < 1 Then
                        lblDataErr.Text = "Please Enter Detailed Explanation"
                    ElseIf TxtONo.Text.Length < 1 Then
                        lblDataErr.Text = "Please select the Applicable Payments"
                    Else
                        Dim StrSql As String

                        'Now for Update or New
                        If btnSubmit.Text = "Submit" Then
                            'Coding to Insert new
                            Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                            StrSql = "Insert into Refund (MemberID, ChapterID, EventID, Pa_ReqRefund, Pa_Reason,"
                            StrSql = StrSql & "Pa_ReqDate, pa_AmountCharged, Pa_ChargedDate, Pa_Comments,Pa_OrderNumber, Pa_Paymentdate, CreatedDate, CreatedBy,Ch_Status,Na_Status,Na_PaymentNotes) Values("
                            StrSql = StrSql & memberID & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", memberID))
                            StrSql = StrSql & "," & DdlEventID.SelectedValue & "," & Val(txtRefundAmt.Text) & ",'" & DdlReason.SelectedItem.Text & "',GetDate()," & Val(txtAmtCharged.Text) & ",'" & txtDOCharged.Text
                            StrSql = StrSql & "','" & txtExplanation.Text.Replace("'", "''") & "','" & TxtONo.Text & "','" & TxtPDate.Text & "',GetDate()," & Session("LoginID") & ",'Pending','Pending','" & TxtNaPNotes.Text & "')"
                            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "'")
                            If flag < 1 Then
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                If flag > 0 Then
                                    clear()
                                    'clearNewUpdate()
                                    GetRefundRecords()
                                Else
                                    lblDataErr.Text = "Error In Records"
                                End If
                            Else
                                lblDataErr.Text = "Order Number Is already in Refund Request"
                            End If
                        Else
                            btnSubmit.Text = "Submit"
                            'coding to update the hardship & donation refund
                            StrSql = "UPDATE Refund SET Pa_ReqRefund=" & Val(txtRefundAmt.Text) & ", pa_AmountCharged=" & Val(txtAmtCharged.Text) & ", Pa_ChargedDate='" & txtDOCharged.Text & "', Pa_Comments='" & txtExplanation.Text.Replace("'", "''") & "', Pa_OrderNumber='" & TxtONo.Text & "', Pa_Paymentdate='" & TxtPDate.Text & "',Na_PaymentNotes='" & TxtNaPNotes.Text & "', ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & " where refundid=" & Session("RefundID")
                            flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from refund where Pa_OrderNumber='" & TxtONo.Text & "' AND refundid <>" & Session("RefundID") & "")
                            If flag < 1 Then
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql)
                                If flag > 0 Then
                                    clear()
                                    'clearNewUpdate()
                                    GetRefundRecords()
                                Else
                                    lblDataErr.Text = "Error In Records"
                                End If
                            Else
                                lblDataErr.Text = "Order Number is already in Refund Request"
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        End Sub

        Private Sub GetNFGRecords()
            Dim dsRecords As New DataSet
            Dim wherecondition As String = " "
            Dim RefundMembers As String = " "
            If txtUserId.Text.Length > 2 Then
                wherecondition = " AND I.AutomemberID = (SELECT TOP 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END FROM INDSPOUSE WHERE Email='" & txtUserId.Text.Trim() & "' ORDER BY DonorType)"
            ElseIf DdlNewUpdate.SelectedValue = 2 Then
                RefundMembers = "INNER JOIN Refund AS R ON N.MemberId = R.MemberID"
                If ddlNa_Status.SelectedItem.Text <> "Select" Then
                    wherecondition = " AND R.Na_Status='" & ddlNa_Status.SelectedItem.Text & "'"
                End If
            End If
            If (Session("EntryToken").ToString.ToUpper() = "PARENT" Or DdlNewUpdate.SelectedValue = 1) And DdlEventID.SelectedValue > 0 Then
                wherecondition = wherecondition & " AND N.EventID=" & DdlEventID.SelectedValue
            End If
            Dim sqlquerystring As String = "SELECT DISTINCT N.TotalPayment,N.unique_id, N.[Payment Date], N.[First Name]+ ' ' +N.[Last Name] AS Parent_Name, N.asp_session_id AS Order_No, CONVERT(decimal(10, 0), N.Fee) as Fee, N.MealsAmount,  N.[Contribution Amount] AS Donation, N.ChapterId, N.EventId, N.MemberId,N.PaymentNotes, I.Chapter, I.HPhone, I.CPhone, I.Email, I.Address1, I.City, I.State, I.Zip FROM  NFG_Transactions AS N INNER JOIN  IndSpouse AS I ON N.MemberId =  CASE when I.DonorType ='IND' THEN I.automemberid ELSE I.Relationship END  " & RefundMembers & " WHERE     (DATEDIFF(m, N.[Payment Date], GETDATE()) < 13) " & wherecondition & Session("whereChapter") & " order by  N.[Payment Date] desc "
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, sqlquerystring)
            'Response.Write(sqlquerystring)
            'dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT DISTINCT N.TotalPayment,N.unique_id, N.[Payment Date], N.[First Name]+ ' ' +N.[Last Name] AS Parent_Name, N.asp_session_id AS Order_No, CONVERT(decimal(10, 0), N.Fee) as Fee, N.MealsAmount,  N.[Contribution Amount] AS Donation, N.ChapterId, N.EventId, N.MemberId,N.PaymentNotes, I.Chapter, I.HPhone, I.CPhone, I.Email, I.Address1, I.City, I.State, I.Zip FROM  NFG_Transactions AS N INNER JOIN  IndSpouse AS I ON N.MemberId =  CASE when I.DonorType ='IND' THEN I.automemberid ELSE I.Relationship END  " & RefundMembers & " WHERE     (DATEDIFF(m, N.[Payment Date], GETDATE()) < 13) " & wherecondition & Session("whereChapter") & " order by  N.[Payment Date] desc ")

            Dim dt As DataTable = dsRecords.Tables(0)
            GridNFG.Columns(18).Visible = True
            GridNFG.DataSource = dt
            GridNFG.DataBind()
            GridNFG.Columns(18).Visible = False
            pnl1Msg2.Text = ""
            pnlNFG.Visible = True
            If (dt.Rows.Count = 0) Then
                pnl1Msg2.Text = "There is no record of Payments"
                If DdlEventID.SelectedValue <> 2 Or DdlNewUpdate.SelectedValue <> 1 Then
                    DdlEventID.Enabled = True
                    DdlReason.Enabled = True
                    btnSel1Continue.Enabled = True
                End If
            Else
                If DdlEventID.SelectedValue > 0 And DdlReason.SelectedValue > 0 Then
                    DdlEventID.Enabled = False
                    DdlReason.Enabled = False
                    showdatas()
                End If
            End If
        End Sub
        Private Sub GetNFGRecords(ByVal refundId As Integer)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT DISTINCT N.TotalPayment,N.unique_id, N.[Payment Date], N.[First Name]+ ' ' +N.[Last Name] AS Parent_Name, N.asp_session_id AS Order_No, CONVERT(decimal(10, 0), N.Fee) as Fee, N.MealsAmount,  N.[Contribution Amount] AS Donation, N.ChapterId, N.EventId, N.MemberId,N.PaymentNotes, I.Chapter, I.HPhone, I.CPhone, I.Email, I.Address1, I.City, I.State, I.Zip FROM  NFG_Transactions AS N INNER JOIN  IndSpouse AS I ON N.MemberId = I.AutoMemberID INNER JOIN Refund AS R ON N.asp_session_id = R.Pa_OrderNumber WHERE     (DATEDIFF(m, N.[Payment Date], GETDATE()) < 13) AND  R.RefundID = " & refundId & " order by  N.[Payment Date] desc ")
            Dim dt As DataTable = dsRecords.Tables(0)
            GridNFG.Columns(18).Visible = True
            GridNFG.DataSource = dt
            GridNFG.DataBind()
            GridNFG.Columns(18).Visible = False
            pnl1Msg2.Text = ""
            pnlNFG.Visible = True
            If (dt.Rows.Count = 0) Then
                pnl1Msg2.Text = "There is no record of Payments"
                If DdlEventID.SelectedValue <> 2 Or DdlNewUpdate.SelectedValue <> 1 Then
                    DdlEventID.Enabled = True
                    DdlReason.Enabled = True
                    btnSel1Continue.Enabled = True
                End If
            Else
                If DdlEventID.SelectedValue > 0 And DdlReason.SelectedValue > 0 Then
                    DdlEventID.Enabled = False
                    DdlReason.Enabled = False
                    showdatas()
                End If
            End If
        End Sub
        Protected Sub GridNFG_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridNFG.PageIndexChanging
            GetNFGRecords()
            GridNFG.PageIndex = e.NewPageIndex
            GridNFG.DataBind()
        End Sub
        Private Sub GetRefundRecords()
            Dim dsRecords As New DataSet
            Dim wherecondition As String = " "
            If txtUserId.Text.Length > 2 Then
                Dim memberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
                wherecondition = " AND R.MemberID=" & memberID
            ElseIf (ddlNa_Status.SelectedValue > 0) Then
                wherecondition = " AND R.Na_Status='" & ddlNa_Status.SelectedItem.Text & "'"
            End If
            Dim SqlString1 As String = "select R.RefundID,C.chaptercode,R.Pa_Paymentdate,I.FirstName+ ' ' +I.LastName AS Parent_Name,R.Pa_OrderNumber,CONVERT(decimal(10, 2), R.Pa_ReqRefund) as Pa_ReqRefund,E.EventCode,R.Pa_Reason,R.Pa_ReqDate, CONVERT(decimal(10, 2), R.pa_AmountCharged) as pa_AmountCharged,R.Pa_ChargedDate,R.Ch_Status,CONVERT(decimal(10, 2), R.Ch_Amount) AS Ch_Amount,R.Ch_DateApproved,R.Ch_ApprovedBy,R.Na_Status,CONVERT(decimal(10, 2), R.Na_Amount) AS Na_Amount,R.Na_DateApproved,R.Na_ApprovedBy,R.Alpha,R.Beta, R.Pa_Comments,R.Ch_Comments,R.Na_Comments,R.TempLink from refund R,chapter C, Indspouse I,Event E where R.MemberId=I.AutoMemberID AND R.ChapterID=C.ChapterID AND (DATEDIFF(m, R.Pa_ReqDate, GETDATE()) < 13) AND E.EventID = R.EventID" & wherecondition & Session("whereChapter") & " order by  R.CreatedDate desc "
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SqlString1)
            'dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select R.RefundID,C.chaptercode,R.Pa_Paymentdate,I.FirstName+ ' ' +I.LastName AS Parent_Name,R.Pa_OrderNumber,CONVERT(decimal(10, 2), R.Pa_ReqRefund) as Pa_ReqRefund,E.EventCode,R.Pa_Reason,R.Pa_ReqDate, CONVERT(decimal(10, 2), R.pa_AmountCharged) as pa_AmountCharged,R.Pa_ChargedDate,R.Ch_Status,CONVERT(decimal(10, 2), R.Ch_Amount) AS Ch_Amount,R.Ch_DateApproved,R.Ch_ApprovedBy,R.Na_Status,CONVERT(decimal(10, 2), R.Na_Amount) AS Na_Amount,R.Na_DateApproved,R.Na_ApprovedBy,R.Alpha,R.Beta, R.Pa_Comments,R.Ch_Comments,R.Na_Comments,R.TempLink from refund R,chapter C, Indspouse I,Event E where R.MemberId=I.AutoMemberID AND R.ChapterID=C.ChapterID AND (DATEDIFF(m, R.Pa_ReqDate, GETDATE()) < 13) AND E.EventID = R.EventID" & wherecondition & Session("whereChapter") & " order by  R.CreatedDate desc ")
            'Response.Write("#############" & SqlString1)
            Dim dt As DataTable = dsRecords.Tables(0)
            GridRefund.DataSource = dt
            GridRefund.DataBind()
            pnlNFG.Visible = True
            Pnl1Msg.Text = ""
            If (dt.Rows.Count = 0) Then
                Pnl1Msg.Text = "There is no record Refund Request"
                If DdlEventID.SelectedValue <> 2 Or DdlNewUpdate.SelectedValue = 2 Then
                    DdlEventID.Enabled = True
                    DdlReason.Enabled = True
                    btnSel1Continue.Enabled = True
                End If
            End If
        End Sub
        Protected Sub GridRefund_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridRefund.PageIndexChanging
            GetRefundRecords()
            GridRefund.PageIndex = e.NewPageIndex
            GridRefund.DataBind()
        End Sub
        Private Sub GetDetails1(ByVal OrdNo As String)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND  C.PaymentReference='" & OrdNo & "'")
            Dim dt As DataTable = dsRecords.Tables(0)
            GridDetails1.DataSource = dt
            GridDetails1.DataBind()
            If (dt.Rows.Count = 0) Then
                ' Invisible MSg
                Details1.Visible = False
            Else
                Details1.Visible = True
            End If
        End Sub
        Private Sub GetDetails2(ByVal OrdNo As String)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT  M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND  M.PaymentReference='" & OrdNo & "'")
            Dim dt As DataTable = dsRecords.Tables(0)
            GridDetails2.DataSource = dt
            GridDetails2.DataBind()
            If (dt.Rows.Count = 0) Then
                Details2.Visible = False
            Else
                Details2.Visible = True
            End If
        End Sub
        Private Sub GetDetails3(ByVal OrdNo As String)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT  R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.ChildNumber = C.ChildNumber AND  R.PaymentReference='" & OrdNo & "'")
            Dim dt As DataTable = dsRecords.Tables(0)
            GridDetails3.DataSource = dt
            GridDetails3.DataBind()
            If (dt.Rows.Count = 0) Then
                Details3.Visible = False
            Else
                Details3.Visible = True
            End If
        End Sub
        Private Sub GetDetails4(ByVal OrdNo As String)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT  C.ChapterCode,D.EventYear, I.FirstName + ' ' + I.LastName AS Parent_Name,D.DonorType, CONVERT(decimal(10, 2),D.AMOUNT) AS AMOUNT, D.TRANSACTION_NUMBER, D.DonationDate, D.PURPOSE, D.EVENT,D.STATUS, D.CreateDate,D.TaxDeduction,D.DonationID From DonationsInfo D, IndSpouse I,Chapter C WHERE D.MEMBERID= I.AutoMemberID AND D.ChapterID=C.ChapterID AND D.TRANSACTION_NUMBER='" & OrdNo & "'")
            Dim dt As DataTable = dsRecords.Tables(0)
            GridDetails4.DataSource = dt
            GridDetails4.DataBind()
            If (dt.Rows.Count = 0) Then
                ' Invisible MSg
                Details4.Visible = False
            Else
                Details4.Visible = True
            End If
        End Sub
        Private Sub GetDetails5(ByVal OrdNo As String)
            Dim dsRecords As New DataSet
            Dim SQLStr As String = "select CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes "
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] and CR.EventYear=C.EventYear and CR.Phase = C.Phase and C.SessionNo=CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PaymentReference='" & OrdNo & "'"
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLStr)
            Dim dt As DataTable = dsRecords.Tables(0)
            GridDetails5.DataSource = dt
            GridDetails5.DataBind()
            If (dt.Rows.Count = 0) Then
                ' Invisible MSg
                Details5.Visible = False
            Else
                Details5.Visible = True
            End If
        End Sub
        Sub showdetails(ByVal OrderNo As String)
            ' enter coding to view datas from Child,Inspouse,Contestant, registration, Donation Info & Indspouse In Panel2
            PanelDetails.Visible = True
            GetDetails1(OrderNo)
            GetDetails2(OrderNo)
            GetDetails3(OrderNo)
            GetDetails4(OrderNo)
            GetDetails5(OrderNo)
            If Details1.Visible = False And Details2.Visible = False And Details3.Visible = False And Details4.Visible = False And Details5.Visible = False Then
                lblDetailsErr.Visible = True
            Else
                lblDetailsErr.Visible = False
            End If

        End Sub

        Protected Sub GridNFG_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            If e.CommandName = "Select" Then

                ''** Added on Jan 18 th to Have more Record for same Order Number
                Dim flag As Integer = 0
                Dim Rsum, Psum As Double

                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select count(*) as Cnt,CASE WHEN count(*)>0 THEN SUM(Pa_ReqRefund) ELSE 0 End as Amt from refund where Pa_OrderNumber='" & GridNFG.Rows(index).Cells(4).Text & "'")
                flag = ds.Tables(0).Rows(0)(0)
                Rsum = ds.Tables(0).Rows(0)(1)
                Psum = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select TotalPayment from NFG_Transactions where TotalPayment > 0 and asp_session_id='" & GridNFG.Rows(index).Cells(4).Text & "'")
                Dim tsTimeSpan As TimeSpan = Now.Subtract(Convert.ToDateTime(GridNFG.Rows(index).Cells(2).Text))
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Or DdlNewUpdate.SelectedValue = 1 Then
                    ' For Parent Select Option  
                    If flag > 0 And Rsum >= Psum Then
                        pnl1Msg2.Text = "* Sorry Order Number already Exist"
                    ElseIf ((tsTimeSpan.Days) / 30) > 13 Then
                        pnl1Msg2.Text = "* Sorry the time limit to apply(13 months) has crossed"
                    ElseIf Val(GridNFG.Rows(index).Cells(17).Text) < 0 Then
                        pnl1Msg2.Text = "* This Returned Payment You cannot use this"
                    Else
                        TxtONo.Text = GridNFG.Rows(index).Cells(4).Text
                        TxtPDate.Text = GridNFG.Rows(index).Cells(2).Text
                        txtUserId.Text = GridNFG.Rows(index).Cells(12).Text
                        If DdlReason.SelectedValue = 4 Then
                            If DdlEventID.SelectedValue = 11 Then
                                TxtNaPNotes.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select 'Donation:-'+ Convert(varchar,[Contribution Amount]) +' Total:-'+ Convert(varchar,[Contribution Amount]) from Nfg_transactions where PaymentNotes is not null  and asp_session_id='" & TxtONo.Text.Trim & "'")
                            Else
                                TxtNaPNotes.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select 'RegFee:0 Mealsamount:0 LateFee:0 Donation:-'+ Convert(varchar,[Contribution Amount]) +' Total:-'+ Convert(varchar,[Contribution Amount]) from Nfg_transactions where PaymentNotes is not null  and asp_session_id='" & TxtONo.Text.Trim & "'")
                            End If
                        End If
                        If DdlReason.SelectedValue > 2 Then
                            TxtNaONo.Text = GridNFG.Rows(index).Cells(4).Text
                            TxtNaPDate.Text = GridNFG.Rows(index).Cells(2).Text
                            If DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Then
                                showPendingdetails(0, 3)
                            End If
                        End If
                        pnl1Msg2.Text = ""
                        Dim i As Integer
                        For i = 0 To DdlEventID.Items.Count - 1
                            If (DdlEventID.Items(i).Value = GridNFG.Rows(index).Cells(9).Text) Then
                                DdlEventID.SelectedIndex = i
                                DdlEventID.Enabled = False
                                Exit For
                            End If
                        Next
                    End If
                Else
                    'For Volunteer Select Option
                    TxtNaONo.Text = GridNFG.Rows(index).Cells(4).Text
                    TxtNaPDate.Text = GridNFG.Rows(index).Cells(2).Text
                    If DdlReason.SelectedValue = 4 Then
                        TxtNaPNotes.Text = GridNFG.Rows(index).Cells(18).Text
                    End If
                End If
            ElseIf e.CommandName = "Details" Then
                showdetails(GridNFG.Rows(index).Cells(4).Text)
            End If
        End Sub

        Protected Sub GridRefund_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
            'while edit button is clicked
            ' MsgBox(e.CommandName)
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridRefund.DataKeys(index).Value)
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select  * from refund where refundid=" & TransID & "")
            Dim dt As DataTable = dsRecords.Tables(0)
            If e.CommandName = "Modify" Then
                'enable_reason()
                showdatas()
                GetNFGRecords(TransID)
                tblSelection.Visible = True
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    DdlNewUpdate.Enabled = False
                    If (dt.Rows(0).Item("Na_Status") = "Pending" And dt.Rows(0).Item("Ch_Status") = "Pending") Then
                        loadvalues(dt)
                        Session("RefundId") = TransID
                        Pnl1Msg.Text = ""
                    Else
                        Pnl1Msg.Text = "Your request is Under Process/Approved. You can't edit"
                    End If
                ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                        If dt.Rows(0).Item("Na_Status") <> "Approved" Then
                            DdlNewUpdate.Enabled = False
                            loadvalues(dt)
                            Session("RefundId") = TransID
                            Pnl1Msg.Text = ""
                        Else
                            Pnl1Msg.Text = "This Refund request was Approved."
                        End If
                    ElseIf (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Then
                        If dt.Rows(0).Item("Na_Status") = "Pending" Then
                            DdlNewUpdate.Enabled = False
                            loadvalues(dt)
                            Session("RefundId") = TransID
                            Pnl1Msg.Text = ""
                        Else
                            Pnl1Msg.Text = "This Refund Request is Under Process/Approved By National Coordinator"
                        End If
                    End If
                End If
            ElseIf e.CommandName = "Delete1" Then
                'delete Refund
                If (dt.Rows(0).Item("Na_Status") = "Pending" And dt.Rows(0).Item("Ch_Status") = "Pending") Then
                    'Delete TempLink and refund
                    If DelRefund(TransID) = True Then
                        GetRefundRecords()
                        Pnl1Msg.Text = "Deleted Successfully"
                    Else
                        Pnl1Msg.Text = "Delete Falied"
                    End If
                Else
                    Pnl1Msg.Text = "Your request is Under Process/Approved. You can't delete"
                End If
            End If
        End Sub

        Protected Sub GridRefund_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridRefund.RowDataBound
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btn As LinkButton = Nothing
                btn = CType(e.Row.Cells(1).Controls(0), LinkButton)
                If DataBinder.Eval(e.Row.DataItem, "Ch_Status").ToString().Trim = "Pending" And DataBinder.Eval(e.Row.DataItem, "Na_Status").ToString().Trim = "Pending" Then
                    btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
                Else
                    btn.Visible = False
                End If
            End If
        End Sub

        Function DelRefund(ByVal RefundId As Integer) As Boolean
            'Duplicate Charges
            'Pending, but was Charged - We use templink
            'Event Cancelled by NSF
            'Refund Donation
            'Hardship
            Dim StrSQl As String = ""
            Dim flag As Boolean = False
            Dim SQL As String = "select  RefundID,Pa_Reason,MemberID,CONVERT(VARCHAR(10), templink, 101) as templink,Pa_OrderNumber,EventId from refund where Ch_Status ='Pending' and Na_Status = 'Pending' AND RefundID = " & RefundId
            Dim MemberID, Templink, Ordernumber, Reason As String
            Dim EventId As Integer
            Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQL)
            While Readr.Read()
                MemberID = Readr("MemberID")
                If Not Readr("templink") Is DBNull.Value Then
                    Templink = Readr("templink")
                End If
                If Not Readr("Pa_OrderNumber") Is DBNull.Value Then
                    Ordernumber = Readr("Pa_OrderNumber")
                End If
                EventId = Readr("EventId")
                Reason = Readr("Pa_Reason")
                flag = True
            End While
            If flag = False Then
                Return flag
            End If
            If Reason.Trim() = "Duplicate Charges" Then
                StrSQl = "Delete from Refund where RefundID=" & RefundId
            Else
                Dim whrCondition As String
                If Reason.Trim() = "Pending, but was Charged" Then
                    whrCondition = " and TempLink ='" & Readr("templink") & "'"
                Else
                    whrCondition = " and PaymentReference ='" & Ordernumber & "' and datediff(d,TempLink,'" & Templink & "') = 0 "
                End If
                If EventId = 1 Then
                    StrSQl = StrSQl & " Update  Contestant Set templink=Null where parentID = " & MemberID & whrCondition & ";"
                    StrSQl = StrSQl & " Update  MealCharge Set templink=Null  where AutoMemberID = " & MemberID & whrCondition & ";"
                ElseIf EventId = 2 Then
                    StrSQl = StrSQl & " Update  Contestant Set templink=Null where parentID = " & MemberID & whrCondition & ";"
                ElseIf EventId = 3 Then
                    StrSQl = StrSQl & " Update  Registration Set templink=Null  where MemberID = " & MemberID & whrCondition & ";"
                ElseIf EventId = 13 Then
                    StrSQl = StrSQl & " Update  CoachReg Set templink=Null  where pMemberID = " & MemberID & whrCondition & ";"
                End If
                StrSQl = StrSQl & " Delete from Refund where RefundID=" & RefundId
            End If
            'Response.Write(StrSQl)
            'Execute query
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
            Return True
        End Function

        Sub loadvalues(ByVal dt As DataTable)
            'load values from database to boxes
            ClearTxtBox()
            If (dt.Rows(0).Item("Pa_ReqRefund") Is System.DBNull.Value) Then
            Else
                txtRefundAmt.Text = Convert.ToInt32(dt.Rows(0).Item("Pa_ReqRefund"))
            End If

            If (dt.Rows(0).Item("pa_AmountCharged") Is System.DBNull.Value) Then
            Else
                txtAmtCharged.Text = Convert.ToInt32(dt.Rows(0).Item("pa_AmountCharged"))
            End If

            If (dt.Rows(0).Item("Pa_ChargedDate") Is System.DBNull.Value) Then
            Else
                txtDOCharged.Text = dt.Rows(0).Item("Pa_ChargedDate")
            End If

            If (dt.Rows(0).Item("Pa_Comments") Is System.DBNull.Value) Then
            Else
                txtExplanation.Text = dt.Rows(0).Item("Pa_Comments")
            End If

            If (dt.Rows(0).Item("Alpha") Is System.DBNull.Value) Then
            Else
                txtCardFNo.Text = dt.Rows(0).Item("Alpha")
            End If

            If (dt.Rows(0).Item("Beta") Is System.DBNull.Value) Then
            Else
                txtCardLNo.Text = dt.Rows(0).Item("Beta")
            End If


            If (dt.Rows(0).Item("Pa_OrderNumber") Is System.DBNull.Value) Then
            Else
                TxtONo.Text = dt.Rows(0).Item("Pa_OrderNumber")
            End If

            If (dt.Rows(0).Item("Pa_Paymentdate") Is System.DBNull.Value) Then
            Else
                TxtPDate.Text = dt.Rows(0).Item("Pa_Paymentdate")
            End If


            'Chapter codinator
            If (dt.Rows(0).Item("Ch_Amount") Is System.DBNull.Value) Then
            Else
                txtChRefundAmt.Text = Val(dt.Rows(0).Item("Ch_Amount"))
            End If
            If (dt.Rows(0).Item("Ch_Comments") Is System.DBNull.Value) Then
            Else
                txtChExplanation.Text = dt.Rows(0).Item("Ch_Comments")
            End If


            'national Cordinator
            If (dt.Rows(0).Item("Na_Amount") Is System.DBNull.Value) Then
            Else
                txtNaRefundAmt.Text = dt.Rows(0).Item("Na_Amount")
            End If
            If (dt.Rows(0).Item("Na_AmountCharged") Is System.DBNull.Value) Then
            Else
                txtNaAmtCharged.Text = dt.Rows(0).Item("Na_AmountCharged")
            End If
            If (dt.Rows(0).Item("Na_Comments") Is System.DBNull.Value) Then
            Else
                txtNaExplanation.Text = dt.Rows(0).Item("Na_Comments")
            End If
            If (dt.Rows(0).Item("Na_OrderNumber") Is System.DBNull.Value) Then
            Else
                TxtNaONo.Text = dt.Rows(0).Item("Na_OrderNumber")
            End If

            If (dt.Rows(0).Item("Na_PaymentDate") Is System.DBNull.Value) Then
            Else
                TxtNaPDate.Text = dt.Rows(0).Item("Na_PaymentDate")
            End If
            If (dt.Rows(0).Item("Na_PaymentNotes") Is System.DBNull.Value) Then
            Else
                TxtNaPNotes.Text = dt.Rows(0).Item("Na_PaymentNotes")
            End If

            Dim i As Integer
            For i = 0 To DdlReason.Items.Count - 1
                If (DdlReason.Items(i).Text = dt.Rows(0).Item("Pa_Reason")) Then
                    DdlReason.SelectedIndex = i
                    DdlReason.Enabled = False
                    Exit For
                End If
            Next

            If DdlReason.SelectedItem.Text = "Refund Donation" Then
                TxtNaONo.Text = TxtONo.Text
                TxtNaPDate.Text = TxtPDate.Text
                If TxtNaPNotes.Text.Length < 3 And TxtNaONo.Text.Length > 3 Then
                    TxtNaPNotes.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select 'Donation:-'+ Convert(varchar,[Contribution Amount]) +' Total:-'+ Convert(varchar,[Contribution Amount]) from Nfg_transactions where PaymentNotes is not null  and asp_session_id='" & TxtONo.Text.Trim & "'")
                End If
            End If
            For i = 0 To DdlEventID.Items.Count - 1
                If (DdlEventID.Items(i).Value = dt.Rows(0).Item("EventID")) Then
                    DdlEventID.SelectedIndex = i
                    DdlEventID.Enabled = False
                    Exit For
                End If
            Next
            If DdlReason.SelectedValue = 2 Then
                txtRefundAmt.Enabled = False
                txtNaRefundAmt.Enabled = False
                txtChRefundAmt.Enabled = False
                GridNFG.Visible = False
                trAmtCharged.Visible = True
                txtRefundAmt.Text = "Not Needed"
                TxtONo.Text = "Not Needed"
                TxtPDate.Text = "Not Needed"
                txtNaRefundAmt.Text = "Not Needed"
                txtChRefundAmt.Text = "Not Needed"
                lblHiddenMemberID.Text = dt.Rows(0).Item("MemberID")
                lblHidden.Text = dt.Rows(0).Item("TempLink")
                showPendingdetails(dt.Rows(0).Item("MemberID"), 1)
                'to show details in Panel3

            ElseIf DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Then
                hdnPaidAmount.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select totalpayment from Nfg_transactions where PaymentNotes is not null  and asp_session_id='" & TxtONo.Text.Trim & "'")
                txtCardFNo.Enabled = False
                txtCardLNo.Enabled = False
                txtAmtCharged.Enabled = False
                txtDOCharged.Enabled = False
                ' Modified 25/05/2010
                TxtNaPNotes.Enabled = True
                txtCardFNo.Text = "Not Needed"
                txtCardLNo.Text = "Not Needed"
                txtAmtCharged.Text = "Not Needed"
                txtDOCharged.Text = "Not Needed"
                '**25/5/2010
                'TxtNaPNotes.Text = "Not Needed"
                If (dt.Rows(0).Item("TempLink") Is System.DBNull.Value) Then
                Else
                    'to show details in Panel3
                    lblHidden.Text = dt.Rows(0).Item("TempLink")
                    showPendingdetails(0, 4)
                End If
            ElseIf DdlReason.SelectedValue = 4 And DdlEventID.SelectedValue = 13 Then
                txtCardFNo.Enabled = False
                txtCardLNo.Enabled = False
                txtAmtCharged.Enabled = False
                'txtDOCharged.Enabled = False
                TxtNaPNotes.Enabled = True
                txtCardFNo.Text = "Not Needed"
                txtCardLNo.Text = "Not Needed"
                txtAmtCharged.Text = "Not Needed"
                'txtDOCharged.Text = "Not Needed"
                '**25/5/2010
                'TxtNaPNotes.Text = "Not Needed"
                If (dt.Rows(0).Item("TempLink") Is System.DBNull.Value) Then
                Else
                    'to show details in Panel3
                    lblHidden.Text = dt.Rows(0).Item("TempLink")
                    showPendingdetails(0, 4)
                End If

            ElseIf DdlReason.SelectedValue > 3 Then
                txtCardFNo.Enabled = False
                txtCardLNo.Enabled = False
                txtCardFNo.Text = "Not Needed"
                txtCardLNo.Text = "Not Needed"
                'txtAmtCharged.Enabled = False
                'txtDOCharged.Enabled = False
                'TxtNaPNotes.Enabled = False
            End If
            For i = 0 To DdlChStatus.Items.Count - 1
                If (DdlChStatus.Items(i).Text = dt.Rows(0).Item("Ch_Status")) Then
                    DdlChStatus.SelectedIndex = i
                    Exit For
                End If
            Next
            For i = 0 To DdlNaStatus.Items.Count - 1
                If (DdlNaStatus.Items(i).Text = dt.Rows(0).Item("Na_Status")) Then
                    DdlNaStatus.SelectedIndex = i
                    Exit For
                End If
            Next
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                btnSubmit.Text = "Update"
            ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                    btnSubmit.Text = "Update"
                    btnChSubmit.Enabled = True
                    btnNaSubmit.Enabled = True
                ElseIf (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "37") Or (Session("RoleId").ToString() = "38") Then
                    btnSubmit.Text = "Update"
                    btnChSubmit.Enabled = True
                End If
            End If
            Pnl1Msg.Text = ""

        End Sub
        Sub clear()
            tblData.Visible = False

            GridNFG.Columns(1).Visible = False
            GridRefund.Columns(0).Visible = False
            GridRefund.Columns(1).Visible = False
            ClearTxtBox()


            btnSel1Continue.Enabled = True
            GridNFG.Visible = True
            If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                DdlNewUpdate.Enabled = True
            End If
            DdlEventID.Enabled = True
            btnSubmit.Text = "Submit"
            btnChSubmit.Enabled = False
            btnNaSubmit.Enabled = False

            ' hidding tables
            trContinue.Visible = True
            tblSelection.Visible = False

            If txtUserId.Enabled = True Then
                txtUserId.Text = ""
            End If

            'Assign Default values to DropDownLists 
            Dim i As Integer
            For i = 0 To DdlChStatus.Items.Count - 1
                If (DdlChStatus.Items(i).Text = "Pending") Then
                    DdlChStatus.SelectedIndex = i
                    Exit For
                End If
            Next
            For i = 0 To DdlNaStatus.Items.Count - 1
                If (DdlNaStatus.Items(i).Text = "Pending") Then
                    DdlNaStatus.SelectedIndex = i
                    Exit For
                End If
            Next
            For i = 0 To DdlReason.Items.Count - 1
                If (DdlReason.Items(i).Value = -1) Then
                    DdlReason.SelectedIndex = i
                    DdlReason.Enabled = True
                    Exit For
                End If
            Next
            For i = 0 To DdlEventID.Items.Count - 1
                If (DdlEventID.Items(i).Value = -1) Then
                    DdlEventID.SelectedIndex = i
                    DdlEventID.Enabled = True
                    Exit For
                End If
            Next
        End Sub
        Sub ClearTxtBox()
            trAmtCharged.Visible = False
            hdnPaidAmount.Text = String.Empty
            Session("RefundId") = Nothing
            lblDataErr.Text = ""
            Session("StrContestIDs") = Nothing
            Session("StrRegisIDs") = Nothing
            Session("StrMealChargeIDs") = Nothing
            Session("StrCoachingIDs") = Nothing
            TrTotalFee.Visible = False
            Panel3.Visible = False
            PendDetails4.Visible = False
            PendDetails3.Visible = False
            PendDetails2.Visible = False
            PendDetails1.Visible = False
            txtNaAmtCharged.Text = ""
            lblHiddenMemberID.Text = ""
            lblHidden.Text = ""
            txtRefundAmt.Text = ""
            txtAmtCharged.Text = ""
            txtDOCharged.Text = ""
            txtExplanation.Text = ""
            txtCardFNo.Text = ""
            txtCardLNo.Text = ""
            TxtONo.Text = ""
            TxtPDate.Text = ""
            'Chapter Cordinators 
            txtChRefundAmt.Text = ""
            txtChExplanation.Text = ""
            'National Cordinators
            txtNaRefundAmt.Text = ""
            txtNaExplanation.Text = ""
            TxtNaONo.Text = ""
            TxtNaPDate.Text = ""
            TxtNaPNotes.Text = ""

            'DdlReason.Items.FindByText("Event Cancelled by NSF").Enabled = False
            'DdlReason.Items.FindByText("Refund Donation").Enabled = False
            'DdlReason.Items.FindByText("Hardship").Enabled = False
            'Disabled For Pending But Paid

            txtRefundAmt.Enabled = True
            txtNaRefundAmt.Enabled = True
            txtChRefundAmt.Enabled = True
            txtCardFNo.Enabled = True
            txtCardLNo.Enabled = True
            txtAmtCharged.Enabled = True
            txtDOCharged.Enabled = True
            TxtNaPNotes.Enabled = True
        End Sub
        Sub clearNewUpdate()
            Dim i As Integer
            For i = 0 To DdlNewUpdate.Items.Count - 1
                If (DdlNewUpdate.Items(i).Value = -1) Then
                    DdlNewUpdate.SelectedIndex = i
                    Exit For
                End If
            Next
            DdlNewUpdate.Enabled = True
        End Sub

        Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
            clear()
            clearNewUpdate()
        End Sub

        Protected Sub DdlNewUpdate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlNewUpdate.SelectedIndexChanged
            If DdlNewUpdate.SelectedValue = 1 Then
                'clear()
                GridRefund.Columns(0).Visible = False
                GridRefund.Columns(1).Visible = False
                GridNFG.Columns(1).Visible = True
            ElseIf DdlNewUpdate.SelectedValue = 2 Then
                'clear()
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    GridNFG.Columns(1).Visible = False
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Then
                    GridNFG.Columns(1).Visible = True
                Else
                    GridNFG.Columns(1).Visible = False
                End If
                GridRefund.Columns(0).Visible = True
                GridRefund.Columns(1).Visible = True
            Else
                GridNFG.Columns(1).Visible = False
                GridRefund.Columns(0).Visible = False
                GridRefund.Columns(1).Visible = False
            End If
        End Sub

        Protected Sub btnChSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChSubmit.Click
            ' update function for chapter co-ordinator
            Dim strsql As String
            Dim flag As Integer = 0
            strsql = "UPDATE Refund SET  Ch_Status='" & DdlChStatus.SelectedItem.Text & "'"
            If DdlReason.SelectedValue <> 2 Then
                strsql = strsql & ", Ch_Amount=" & Val(txtChRefundAmt.Text)
            End If
            strsql = strsql & ", Ch_DateApproved = GetDate(), Ch_ApprovedBy=" & Session("LoginID") & ", Ch_Comments='" & txtChExplanation.Text.Replace("'", "''") & "', ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & " where refundID=" & Session("RefundId")
            flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strsql)
            If flag > 0 Then
                clear()
                GetRefundRecords()
            End If
        End Sub

        Protected Sub btnNaSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNaSubmit.Click
            ' update function for national co-ordinator
            'ferdine add some validations here for the national cordinator to update
            Try
                Dim paynotes As String = ""
                If DdlNaStatus.SelectedItem.Text <> "Pending" Then
                    Dim strsql As String
                    Dim flag As Integer = 0
                    strsql = "UPDATE Refund SET  Na_Status='" & DdlNaStatus.SelectedItem.Text & "'"
                    If DdlReason.SelectedValue = 2 Then
                        strsql = strsql & ", Na_AmountCharged=" & Val(txtNaAmtCharged.Text)
                    End If
                    If DdlReason.SelectedValue <> 2 Then
                        strsql = strsql & ", Na_Amount=" & Val(txtNaRefundAmt.Text)
                    End If
                    '' ** Ferdine Silva 28/05/2010

                    ''If DdlReason.SelectedValue = 3 Then
                    ''    If paynotes = "" Then
                    ''        paynotes = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select PaymentNotes from MealCharge where PaymentReference='" & TxtNaONo.Text & "'")
                    ''    End If
                    ''    If paynotes = "" Then
                    ''        paynotes = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select PaymentNotes from contestant where PaymentReference='" & TxtNaONo.Text & "'")
                    ''    End If
                    ''    If paynotes = "" Then
                    ''        paynotes = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select PaymentNotes from registration where PaymentReference='" & TxtNaONo.Text & "'")
                    ''    End If
                    ''    '** 25/5/2010
                    ''    'TxtNaPNotes.Text = paynotes
                    ''    strsql = strsql & ", Na_PaymentNotes='" & paynotes & "'"
                    ''Else
                    strsql = strsql & ", Na_PaymentNotes='" & TxtNaPNotes.Text & "'"
                    ''End If
                    strsql = strsql & ", Na_DateApproved = GetDate(), Na_ApprovedBy=" & Session("LoginID") & ", Na_Comments='" & txtNaExplanation.Text.Replace("'", "''") & "', Na_OrderNumber='" & TxtNaONo.Text & "', Na_PaymentDate='" & TxtNaPDate.Text & "', ModifiedDate=GetDate(), Modifiedby=" & Session("LoginID") & " where refundID=" & Session("RefundId")
                    If DdlNaStatus.SelectedItem.Text = "Approved" Then
                        If DdlReason.SelectedValue = 2 Then
                            If (TxtNaPNotes.Text.Length < 2 Or TxtNaONo.Text.Length < 3 Or txtNaExplanation.Text.Length < 3 Or TxtNaPDate.Text.Length < 3 Or TxtNaONo.Text.Length < 1) Then
                                lblNaErr.Text = "Payment Notes/Order Number/Explanation/Amount Charged Missing. Please Fill All"
                            Else
                                flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strsql)
                                If flag > 0 Then
                                    ' Write code to update the contestant,MealCharge,Registration and CoachReg table with Paymentreference,PaymentNotes and PaymentDate.
                                    If DdlEventID.SelectedValue = 1 Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE MealCharge SET PaymentDate='" & TxtNaPDate.Text & "', PaymentMode='Credit Card', PaymentReference='" & TxtNaONo.Text & "', PaymentNotes='" & TxtNaPNotes.Text & "',ModifiedDate=GetDate(), ModifiedUserID=" & Session("LoginID") & " WHERE datediff(d,TempLink,'" & lblHidden.Text & "') = 0  AND  AutoMemberID=" & lblHiddenMemberID.Text & "")
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE Contestant SET PaymentDate='" & TxtNaPDate.Text & "', PaymentMode='Credit Card', PaymentReference='" & TxtNaONo.Text & "', PaymentNotes='" & TxtNaPNotes.Text & "',ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE datediff(d,TempLink,'" & lblHidden.Text & "') = 0  AND  ParentID=" & lblHiddenMemberID.Text & "")
                                    ElseIf DdlEventID.SelectedValue = 2 Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE Contestant SET PaymentDate='" & TxtNaPDate.Text & "', PaymentMode='Credit Card', PaymentReference='" & TxtNaONo.Text & "', PaymentNotes='" & TxtNaPNotes.Text & "',ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE datediff(d,TempLink,'" & lblHidden.Text & "') = 0  AND  ParentID=" & lblHiddenMemberID.Text & "")
                                    ElseIf DdlEventID.SelectedValue = 3 Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE Registration SET PaymentDate='" & TxtNaPDate.Text & "', PaymentMode='Credit Card', PaymentReference='" & TxtNaONo.Text & "', PaymentNotes='" & TxtNaPNotes.Text & "',ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE datediff(d,TempLink,'" & lblHidden.Text & "') = 0  AND  MemberID=" & lblHiddenMemberID.Text & "")
                                    ElseIf DdlEventID.SelectedValue = 13 Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE CoachReg SET PaymentDate='" & TxtNaPDate.Text & "', PaymentMode='Credit Card', PaymentReference='" & TxtNaONo.Text & "', PaymentNotes='" & TxtNaPNotes.Text & "',ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE datediff(d,TempLink,'" & lblHidden.Text & "') = 0  AND  PMemberID=" & lblHiddenMemberID.Text & "")
                                    End If
                                    '** Amount must calculated donation amount
                                    Dim EventYear As Integer
                                    EventYear = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 EventYear from NFG_Transactions where asp_session_id ='" & TxtNaONo.Text & "' and TotalPayment > 0")
                                    Dim chapterId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select ChapterID from indspouse where automemberid=" & lblHiddenMemberID.Text & "")
                                    Dim Donation As Integer = 0
                                    Dim totalfee As Integer = 0
                                    Dim fee As Integer = 0
                                    Dim mealsamount As Integer = 0

                                    If DdlEventID.SelectedValue = 1 Then
                                        mealsamount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select sum(amount) from  mealcharge where paymentreference ='" & TxtNaONo.Text & "'")
                                        fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee) from contestant where paymentreference ='" & TxtNaONo.Text & "'")
                                        totalfee = mealsamount + fee
                                    ElseIf DdlEventID.SelectedValue = 2 Then
                                        fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee) from contestant where paymentreference ='" & TxtNaONo.Text & "'")
                                        totalfee = fee
                                    ElseIf DdlEventID.SelectedValue = 3 Then
                                        fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee)  from registration where paymentreference ='" & TxtNaONo.Text & "'")
                                        totalfee = fee
                                    ElseIf DdlEventID.SelectedValue = 13 Then
                                        fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Isnull(SUM(fee),0) from CoachReg where PaymentReference='" & TxtNaONo.Text & "'")
                                        totalfee = fee
                                    End If
                                    Donation = Val(txtNaAmtCharged.Text) - totalfee
                                    ' MsgBox(Val(txtNaAmtCharged.Text) & " :  Doanation " & Donation & " :  total Fee " & totalfee)
                                    If Donation > 0 Then
                                        ' MsgBox("donation INserted")
                                        Dim iMaxDonationNumber As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT ISNULL(MAX(DonationNumber),0) AS Expr1 FROM DonationsInfo WHERE MemberID=" & lblHiddenMemberID.Text & ""))
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO  DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate, EventId, ChapterId, CreatedBy, EventYear, DepositDate) Values(" & lblHiddenMemberID.Text & ", " & iMaxDonationNumber + 1 & " , 'IND', " & Donation & ",'" & TxtNaONo.Text & "', '" & TxtNaPDate.Text & "', 'Credit Card', 'Donation', 'Completed', GETDATE(), " & DdlEventID.SelectedValue & "," & chapterId & ", " & Session("LoginID") & ", " & EventYear & ", '" & TxtNaPDate.Text & "')")
                                    Else
                                        Donation = 0
                                    End If
                                    Dim eventname As String = ""

                                    Select Case DdlEventID.SelectedValue
                                        Case 1
                                            eventname = "Finals"
                                        Case 2
                                            eventname = "Chapter"
                                        Case 3
                                            eventname = "WkShop"
                                        Case 13
                                            eventname = "Coaching"
                                    End Select
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions ([Last Name], [First Name], Address, City, State, Zip, [Contribution Date], [Contribution Amount], [Payment Date], Status, [Email (ok to contact)], approval_status,event_for, asp_session_id,MealsAmount, LateFee, EventId, MemberId,ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT   LastName, FirstName, Address1, City, State, Zip, '" & TxtNaPDate.Text & "', " & Donation & ", '" & TxtNaPDate.Text & "', 'Complete', email, 'APPROVED','" & eventname & "','" & TxtNaONo.Text & "'," & mealsamount & ", 0, " & DdlEventID.SelectedValue & ", autoMemberId," & chapterId & "," & fee & ", " & txtNaAmtCharged.Text & ", " & EventYear & ",'NSF','" & TxtNaPNotes.Text & "'  FROM IndSpouse where  AutoMemberID=" & lblHiddenMemberID.Text & "")
                                    SendEMail(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select I.Email from Indspouse I,Refund R where R.MemberID=I.AutomemberID AND R.RefundID = " & Session("RefundId")), 3, 0)
                                    clear()
                                End If
                            End If
                        ElseIf DdlReason.SelectedValue = 3 Then
                            '**  3 - EvenT cancelled
                            'Add Process to carry out the Creditcard
                            If (TxtNaPNotes.Text.Length < 2 Or txtNaRefundAmt.Text.Length < 1 Or TxtNaONo.Text.Length < 3 Or txtNaExplanation.Text.Length < 3 Or TxtNaPDate.Text.Length < 3) Then
                                lblNaErr.Text = "PaymentNotes/Refund Amount/Order Number/Explanation Missing. Please Fill All"
                            Else
                                'Get the amount in it and Check
                                If Not CheckPaymentNotes() Then
                                    Exit Sub
                                Else
                                    lblNaErr.Text = ""
                                End If
                                '**************************************************************************************************************
                                ''GetConfigParams()
                                configGDE4()
                                'oid = TxtNaONo.Text
                                'total = txtNaRefundAmt.Text
                                Session("strsql") = strsql
                                ProcessOrder()
                            End If
                        ElseIf DdlReason.SelectedValue = 4 Then
                            '** 4 - Refund Donation, 
                            'Add Process to carry out the Creditcard
                            If (TxtNaPNotes.Text.Length < 2 Or txtNaRefundAmt.Text.Length < 1 Or TxtNaONo.Text.Length < 3 Or txtNaExplanation.Text.Length < 3 Or TxtNaPDate.Text.Length < 3) Then
                                lblNaErr.Text = "PaymentNotes/Refund Amount/Order Number/Explanation Missing. Please Fill All"
                            Else
                                'Get the amount in it and Check
                                If Not CheckPaymentNotes() Then
                                    Exit Sub
                                Else
                                    lblNaErr.Text = ""
                                End If
                                ''GetConfigParams()
                                configGDE4()

                                'oid = TxtNaONo.Text
                                'total = txtNaRefundAmt.Text
                                Session("strsql") = strsql
                                ProcessOrder()
                            End If
                        ElseIf DdlReason.SelectedValue = 1 Then
                            'Duplicate Charges
                            If (TxtNaPNotes.Text.Length < 2 Or txtNaRefundAmt.Text.Length < 1 Or TxtNaONo.Text.Length < 3 Or txtNaExplanation.Text.Length < 3 Or TxtNaPDate.Text.Length < 3) Then
                                lblNaErr.Text = "Payment Notes/Refund Amount/Order Number/Explanation Missing. Please Fill All"
                            ElseIf TxtNaONo.Text = TxtONo.Text Then
                                lblNaErr.Text = "The Order Number Must Be different"
                            Else
                                ''GetConfigParams()
                                configGDE4()
                                'oid = TxtNaONo.Text
                                'total = txtNaRefundAmt.Text
                                Session("strsql") = strsql
                                ProcessOrder()
                            End If
                        ElseIf DdlReason.SelectedValue = 5 Then
                            'Hardship
                            If (TxtNaPNotes.Text.Length < 2 Or txtNaRefundAmt.Text.Length < 1 Or TxtNaONo.Text.Length < 3 Or txtNaExplanation.Text.Length < 3 Or TxtNaPDate.Text.Length < 3) Then
                                lblNaErr.Text = "Payment Notes/Refund Amount/Order Number/Explanation Missing. Please Fill All"
                            ElseIf Val(txtNaRefundAmt.Text) > Val(hdnPaidAmount.Text) Then
                                lblNaErr.Text = "Refund cannot be more than the total amount charged"
                            Else

                                If Not CheckPaymentNotes() Then
                                    Exit Sub
                                Else
                                    lblNaErr.Text = ""
                                End If
                                ''GetConfigParams()
                                configGDE4()
                                'oid = TxtNaONo.Text
                                'total = txtNaRefundAmt.Text
                                Session("strsql") = strsql
                                ProcessOrder()
                            End If
                        End If

                    ElseIf DdlNaStatus.SelectedItem.Text = "Declined" Then
                        flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strsql)
                        If flag > 0 Then
                            SendEMail(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select I.Email from Indspouse I,Refund R where R.MemberID=I.AutomemberID AND R.RefundID = " & Session("RefundId")), 1, 0)
                            GetRefundRecords()
                            lblNaErr.Text = "Updated Successfully"
                            clear()
                        End If
                    End If
                Else
                    lblNaErr.Text = "Please Select Valid Status"
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        End Sub
        Function CheckPaymentNotes() As Boolean
            'Get the amount in it and Check
            Dim match As Match
            Dim capture As Capture
            Dim Amt As Decimal = 0
            Dim Totamt As Decimal = 0
            Dim Mealamt As Decimal = 0
            Dim Donamt As Decimal = 0
            Dim RFee As Decimal = 0
            Dim LFee As Decimal = 0

            'individual Registrations
            Dim pattern As String = "[(][-]*[0-9]+[.][0-9]+[)]"
            ' LateFee:0 Donation:0 
            Dim Matches As MatchCollection = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    Amt = Amt + Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
                Next
            Next
            ''RegFee
            pattern = "[R][e][g][F][e][e][:][-]*[0-9]+"
            Matches = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    RFee = Convert.ToDecimal(capture.Value.Replace("RegFee:", ""))
                Next
            Next
            ''LateFee
            pattern = "[L][a][t][e][F][e][e][:][-]*[0-9]+"
            Matches = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    LFee = Convert.ToDecimal(capture.Value.Replace("LateFee:", ""))
                Next
            Next

            ''Mealsamount:0
            pattern = "[M][e][a][l][s][a][m][o][u][n][t][:][-]*[0-9]+"
            Matches = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    Mealamt = Convert.ToDecimal(capture.Value.Replace("Mealsamount:", ""))
                Next
            Next

            'Donation:0 
            pattern = "[D][o][n][a][t][i][o][n][:][-]*[0-9]+"
            Matches = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    Donamt = Convert.ToDecimal(capture.Value.Replace("Donation:", ""))
                Next
            Next

            'Total:
            pattern = "[T][o][t][a][l][:][-]*[0-9]+"
            Matches = Regex.Matches(TxtNaPNotes.Text, pattern)
            For Each match In Matches
                For Each capture In match.Captures
                    Totamt = Convert.ToDecimal(capture.Value.Replace("Total:", ""))
                Next
            Next

            If Val(txtNaRefundAmt.Text) <> (-1 * Totamt) Then
                lblNaErr.Text = "The refunded amount is not equal to the total in payment Notes."
                Return False
            ElseIf (Amt + Mealamt + Donamt) <> Totamt Then
                lblNaErr.Text = "Components do not add to the total."
                Return False
            End If
            Re_Amount = Totamt
            Re_Donation = Donamt
            Re_Regfee = RFee
            Re_Latefee = LFee
            Re_Meals = Mealamt
            Return True
        End Function

        Private Sub ProcessOrder()
            Try
                Dim flag As Integer = 0
                '' '' create order
                ' ''Dim op As LinkPointTransaction.LPOrderPart
                ' ''Dim order As LinkPointTransaction.LPOrderPart

                ' ''isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)

                ' ''order = LinkPointTransaction.LPOrderFactory.createOrderPart("order")
                '' '' create a part we will use to build the order
                ' ''op = LinkPointTransaction.LPOrderFactory.createOrderPart()

                '' '' Build 'orderoptions'
                ' ''op.put("ordertype", "CREDIT")
                '' '' add 'orderoptions to order
                ' ''order.addPart("orderoptions", op)


                '' '' Build 'merchantinfo'
                ' ''op.clear()
                ' ''op.put("configfile", configfile)
                '' '' add 'merchantinfo to order
                ' ''order.addPart("merchantinfo", op)


                '' '' Build 'creditcard'
                '' ''op.clear()
                '' ''op.put("cardnumber", cardnumber)
                '' ''op.put("cardexpmonth", expmonth)
                '' ''op.put("cardexpyear", expyear)
                ' '' '' add 'creditcard to order
                '' ''order.addPart("creditcard", op)

                '' ''**************  Ferdine Silva Needed ***************

                '' '' Build 'payment' 
                ' ''op.clear()
                ' ''op.put("chargetotal", total)
                '' '' add 'payment to order
                ' ''order.addPart("payment", op)

                '' '' Add oid
                ' ''op.clear()
                ' ''op.put("oid", oid)
                '' '' add 'transactiondetails to order
                ' ''order.addPart("transactiondetails", op)

                '' '' create transaction object	
                ' ''LPTxn = New LinkPointTransaction.LinkPointTxn()
                ' ''Dim resp As String
                '' '' get outgoing XML from 'order' object
                ' ''Dim outXml As String = order.toXML()
                ' ''Try

                ' ''    ' Call LPTxn
                ' ''    resp = LPTxn.send(keyfile, host, port, outXml)

                ' ''    'Store transaction data on Session and redirect
                ' ''    'Session("outXml") = outXml
                ' ''    'Session("resp") = resp
                ' ''    'Server.Transfer("afs_status.aspx")
                ' ''Catch
                ' ''    Response.Write("Error processing payment information")
                ' ''End Try

                ''Try
                ''    'Session(outXml) = outXml.ToString   'Not sure what this is for (Meena)
                ''    Session("outXml") = outXml.ToString
                ''    ParseResponse(resp)
                ''Catch
                ''    Response.Write("There was an error in parsing the response back from LP<BR>")
                ''    Response.Write(outXml.ToString)
                ''    If (isTestMode = True) Then
                ''        Response.Write("<BR>Debug:Resp back from LP :" + resp)
                ''        Response.Write("<br>")
                ''    End If

                ''End Try
                PreUpdate()
                Dim ws As New GDE4.Service()
                Dim txn As New GDE4.Transaction()
                Try

                    nRefundAmt = CDec(txtNaRefundAmt.Text)
                    txn.Transaction_Type = "34" 'Tagged Refund
                    txn.ExactID = e4_Id
                    txn.Password = e4_Pass
                    txn.Authorization_Num = GetTagAuth("A", TxtNaONo)
                    txn.Transaction_Tag = GetTagAuth("T", TxtNaONo)
                    txn.DollarAmount = nRefundAmt.ToString("f2")

                    Dim result As GDE4.TransactionResult = ws.SendAndCommit(txn)
                    e4_BankMessage = result.Bank_Message.ToString()
                    e4_Auth = result.Authorization_Num.ToString()
                    e4_Tag = result.Transaction_Tag.ToString()
                    e4_result = result.CTR

                Catch ex As Exception
                    Response.Write(ex.ToString)
                End Try
                PostUpdate()
                ''lblMessage.Text = ""
                Dim PaymentNotes As String = ""
                Dim StrSSql As String = ""
                'If (R_Approved = "APPROVED") Then
                If (e4_BankMessage = "Approved") Then
                    Dim Donation As Integer = 0
                    '****CCSubmitLog Updation ****
                    flag = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Session("strsql"))
                    If flag > 0 Then
                        Session("StrSql") = Nothing
                        If DdlReason.SelectedValue = 1 Then
                            'Duplicate to insert Missing Values
                            ''** Checking for Existing of the Order Number Jan 31, 2011
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) FROM NFG_Transactions where asp_session_id='" & TxtNaONo.Text & "'") = 0 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status, event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT     Id, [Last Name], [First Name], Address, City, State, Zip,'" & TxtNaPDate.Text & "' , [Contribution Amount] ,'" & TxtNaPDate.Text & "', Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee," & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & TxtNaPNotes.Text & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            End If
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, AMOUNT, '" & TxtNaONo.Text & "','" & TxtNaPDate.Text & "', METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            Select Case DdlEventID.SelectedValue
                                Case 1
                                    StrSSql = "select  R.ChapterID,R.ChildNumber,P.ProductCode,EF.NationalFinalsFee as RegFee from Contestant  R Inner Join ContestCategory EF ON R.ContestCategoryID =EF.ContestCategoryID  and R.ContestYear  = EF.ContestYear   and R.ProductCode  = Ef.ContestCode  Inner Join Product P ON P.ProductId = R.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' and R.EventId = 1"
                                Case 2
                                    StrSSql = "select  R.ChapterID,R.ChildNumber,P.ProductCode,EF.RegionalFee as RegFee from Contestant  R Inner Join ContestCategory EF ON R.ContestCategoryID =EF.ContestCategoryID  and R.ContestYear  = EF.ContestYear   and R.ProductCode  = Ef.ContestCode  Inner Join Product P ON P.ProductId = R.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' and R.EventId = 2"
                                Case 3
                                    StrSSql = "select  R.ChapterID,R.ChildNumber,P.ProductCode,EF.RegFee from Registration  R Inner Join EventFees EF ON R.EventID=EF.EventID and R.EventYear = EF.EventYear  and R.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = R.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' "
                                Case 13
                                    'StrSSql = "select  CR.ChapterID,CR.ChildNumber,P.ProductCode,EF.RegFee from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.EventYear  and CR.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = CR.ProductID where CR.PaymentReference = '" & TxtNaONo.Text & "' "
                                    StrSSql = "select  CR.ChapterID,CR.ChildNumber,P.ProductCode,CASE WHEN EF.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE EF.RegFee END as RegFee from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.EventYear  and CR.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = CR.ProductID where CR.PaymentReference = '" & TxtNaONo.Text & "' "
                            End Select
                            Dim DS As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSSql)
                            Dim i As Integer
                            For i = 0 To DS.Tables(0).Rows.Count - 1
                                PaymentNotes = PaymentNotes + DS.Tables(0).Rows(i)("ProductCode") & "(" & DS.Tables(0).Rows(i)("ChildNumber").ToString() & ")(" & DS.Tables(0).Rows(i)("ChapterID").ToString() & ")(-" & DS.Tables(0).Rows(i)("RegFee").ToString() & ")"
                            Next
                            'Duplicate to insert reverse values
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status, event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT     Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),  -1 * [Contribution Amount] ,  GetDate(), Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', -MealsAmount, - LateFee, EventId, MemberId, ChapterId, - Fee, - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & PaymentNotes & " RegFee:-' + Convert(varchar,fee)+' Mealsamount:-' + Convert(varchar,MealsAmount)+' LateFee:-' + Convert(varchar,LateFee)+' Donation:-'+ Convert(varchar,[Contribution Amount]) +' Total:-'+ Convert(varchar,TotalPayment) FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - AMOUNT, '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            TxtONo.Text = ""
                            TxtNaONo.Text = ""
                        ElseIf DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Then
                            'EvenT cancelled

                            Dim totalfee As Integer = 0
                            Dim fee As Integer = 0
                            Dim mealsamount As Integer = 0
                            If DdlEventID.SelectedValue = 1 Then
                                Try
                                    mealsamount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select sum(amount) from  mealcharge where paymentreference ='" & TxtONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                Catch ex As Exception
                                    mealsamount = 0
                                End Try
                                Try
                                    fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee) from contestant where paymentreference ='" & TxtONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                Catch ex As Exception
                                    fee = 0
                                End Try
                                totalfee = mealsamount + fee
                            ElseIf DdlEventID.SelectedValue = 2 Then
                                Try
                                    fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee) from contestant where paymentreference ='" & TxtONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                Catch ex As Exception
                                    fee = 0
                                End Try
                                totalfee = fee
                            ElseIf DdlEventID.SelectedValue = 3 Then
                                Try
                                    fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(fee)  from registration where paymentreference ='" & TxtONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                Catch ex As Exception
                                    fee = 0
                                End Try
                                totalfee = fee
                            ElseIf DdlEventID.SelectedValue = 13 Then
                                fee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Isnull(SUM(fee),0) from CoachReg where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                totalfee = fee
                            End If
                            Donation = Val(txtNaAmtCharged.Text) - totalfee
                            If Donation > 0 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - " & Donation & ", '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            Else
                                Donation = 0
                            End If
                            'Refund Donation is Claimed here
                            ''If DdlEventID.SelectedValue = 13 Then
                            ''    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - " & txtNaRefundAmt.Text & ", '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            ''    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status, event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT  Top 1   Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),  - " & txtNaRefundAmt.Text & " ,  GetDate(), Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', -MealsAmount, - LateFee, EventId, MemberId, ChapterId,- " & txtNaRefundAmt.Text & ", - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & TxtNaPNotes.Text & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            ''Else
                            ''If DdlReason.SelectedValue = 3 Or Val(txtNaRefundAmt.Text) = Val(hdnPaidAmount.Text) Then
                            ''    Select Case DdlEventID.SelectedValue
                            ''        Case 1, 2
                            ''            StrSSql = "select  R.ChapterID,R.ChildNumber,P.ProductCode,EF.NationalFinalsFee as RegFee  from Contestant  R Inner Join ContestCategory EF ON R.ContestCategoryID =EF.ContestCategoryID  and R.ContestYear  = EF.ContestYear   and R.ProductCode  = Ef.ContestCode  Inner Join Product P ON P.ProductId = R.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0 and R.EventId = " & DdlEventID.SelectedValue
                            ''        Case 3
                            ''            StrSSql = "select  R.ChapterID,R.ChildNumber,P.ProductCode,EF.RegFee from Registration  R Inner Join EventFees EF ON R.EventID=EF.EventID and R.EventYear = EF.EventYear  and R.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = R.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0"
                            ''        Case 13
                            ''            StrSSql = "select  CR.ChapterID,CR.ChildNumber,P.ProductCode,EF.RegFee from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.EventYear  and CR.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = CR.ProductID where R.PaymentReference = '" & TxtNaONo.Text & "' AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0"
                            ''    End Select

                            ''    Dim DS As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSSql)
                            ''    Dim i As Integer
                            ''    For i = 0 To DS.Tables(0).Rows.Count - 1
                            ''        PaymentNotes = PaymentNotes + DS.Tables(0).Rows(i)("ProductCode") + "(" + DS.Tables(0).Rows(i)("ChildNumber").ToString() & ")(" & DS.Tables(0).Rows(i)("ChapterID").ToString() & ")(-" & Math.Round(Convert.ToDecimal(DS.Tables(0).Rows(i)("RegFee").ToString()), 2) & ")"
                            ''    Next
                            ''    PaymentNotes = PaymentNotes + "RegFee: -" & fee & " Mealsamount:-" & mealsamount & " LateFee:0 Donation:-" & Donation & " Total:-" & txtNaRefundAmt.Text
                            ''Else
                            PaymentNotes = TxtNaPNotes.Text
                            ''End If

                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status, event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT   Top 1   Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),  - " & Donation & " ,  GetDate(), Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', -" & mealsamount & ", - LateFee, EventId, MemberId, ChapterId, " & Re_Regfee & ", - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & PaymentNotes & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            ''End If
                            If DdlEventID.SelectedValue = 1 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update mealcharge set OrderNo=PaymentReference,PaymentReference = Null,ModifiedUserID='" & Session("LoginID") & "',ModifiedDate=GetDate() where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update contestant set OrderNo=PaymentReference,PaymentReference = Null,ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate() where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                            ElseIf DdlEventID.SelectedValue = 2 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update contestant set OrderNo=PaymentReference,PaymentReference = Null,ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate() where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                            ElseIf DdlEventID.SelectedValue = 3 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update Registration set OrderNo=PaymentReference,PaymentReference = Null,ModifiedBy=" & Session("LoginID") & ",ModifyDate=GetDate() where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                            ElseIf DdlEventID.SelectedValue = 13 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CoachReg set OrderNo=PaymentReference,PaymentReference = Null,ModifiedBy=" & Session("LoginID") & ",ModifyDate=GetDate(),Approved='N' where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                            End If
                            TxtONo.Text = ""
                            TxtNaONo.Text = ""
                            ''ElseIf DdlReason.SelectedValue = 4 And DdlEventID.SelectedValue = 13 Then
                            ''    ''Refund Donation for Coaching
                            ''    Dim Donation As Integer = 0
                            ''    Dim totalfee As Integer = 0
                            ''    Dim fee As Integer = 0
                            ''    Dim mealsamount As Integer = 0
                            ''    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - " & txtNaRefundAmt.Text & ", '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            ''    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status, event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT  Top 1   Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),  - " & txtNaRefundAmt.Text & " ,  GetDate(), Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', -MealsAmount, - LateFee, EventId, MemberId, ChapterId,- " & txtNaRefundAmt.Text & ", - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & TxtNaPNotes.Text & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            ''    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CoachReg set PaymentNotes = Null,PaymentReference = Null,PaymentMode = null, PaymentDate = null,ModifiedBy=" & Session("LoginID") & ",ModifyDate=GetDate(),Approved='N' where PaymentReference='" & TxtNaONo.Text & "' AND datediff(d,TempLink,'" & lblHidden.Text & "') = 0")
                            ''    TxtONo.Text = ""
                            ''    TxtNaONo.Text = ""
                        ElseIf DdlReason.SelectedValue = 4 Then
                            'Refund Donation
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status,  event_for,  asp_session_id,  EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT  Top 1   Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),   - " & txtNaRefundAmt.Text & " ,  GetDate(), Status,   [Email (ok to contact)], approval_status,  event_for,  '" & TxtNaONo.Text & "', EventId, MemberId, ChapterId, 0, - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & TxtNaPNotes.Text & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - AMOUNT, '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            TxtONo.Text = ""
                            TxtNaONo.Text = ""
                            '' Ferdine Silvaa 28/05/2010
                            ''ElseIf DdlReason.SelectedValue = 5 Then
                            ''    'Hardship
                            ''    'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO NFG_Transactions (Id, [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],  [Contribution Amount],  [Payment Date], Status,   [Email (ok to contact)], approval_status,  event_for,  asp_session_id, MealsAmount, LateFee, EventId, MemberId, ChapterId, Fee, TotalPayment, EventYear,[Source Website],PaymentNotes) SELECT     Id, [Last Name], [First Name], Address, City, State, Zip, GetDate(),  0 ,  GetDate(), Status,   [Email (ok to contact)], approval_status, event_for,  '" & TxtNaONo.Text & "', 0, 0, EventId, MemberId, ChapterId, 0, - " & txtNaRefundAmt.Text & ",  EventYear,'NSF','" & TxtNaPNotes.Text & "' FROM NFG_Transactions where asp_session_id='" & TxtONo.Text & "'")
                            ''    'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DonationsInfo (MEMBERID, DonationNumber, DonorType, AMOUNT, TRANSACTION_NUMBER, DonationDate, METHOD, EVENT, STATUS, CreateDate,  EventId, ChapterId,  CreatedBy,  EventYear,   DepositDate) SELECT MEMBERID, DonationNumber, DonorType, - AMOUNT, '" & TxtNaONo.Text & "', GetDate(), METHOD, EVENT, STATUS, GetDate(),  EventId, ChapterId,  " & Session("LoginID") & ", EventYear,  DepositDate FROM DonationsInfo WHERE TRANSACTION_NUMBER='" & TxtONo.Text & "'")
                            ''    TxtONo.Text = ""
                            ''    TxtNaONo.Text = ""
                        End If
                        SendEMail(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select I.Email from Indspouse I,Refund R where R.MemberID=I.AutomemberID AND R.RefundID = " & Session("RefundId")), 2, Val(txtNaRefundAmt.Text))
                        GetRefundRecords()
                        lblError.Text = "Updated Successfully"
                        clear()

                    End If
                Else

                    lblMessage.Text = "Refund Cannot be processed"
                    ' lblMessage.Text = (lblMessage.Text + ("<BR>Card Status : " + R_Approved))
                    lblMessage.Text = (lblMessage.Text + ("<BR>Card Status : " + e4_BankMessage))
                    lblMessage.Text = (lblMessage.Text + ("<BR>Error : " + e4_result))
                    '' ''lblMessage.Text = (lblMessage.Text + ("<BR>Message:" + R_Message))
                    '' ''lblMessage.Text = (lblMessage.Text + ("<BR>FraudCode:" + R_FraudCode))
                    lblMessage.Text = (lblMessage.Text + "<BR>Please check Refund Amount and try again")
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        End Sub

        Protected Sub btnNaCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNaCancel.Click
            clear()
            clearNewUpdate()
        End Sub
        Protected Sub BtnChCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnChCancel.Click
            clear()
            clearNewUpdate()
        End Sub

        Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Close.Click
            PanelDetails.Visible = False
        End Sub

        Protected Sub Close3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Close3.Click
            Panel3.Visible = False
        End Sub

        '******************  FERDINE SILVA 09/09/2010  **************************************
        '******************  Status ID's used below    **************************************
        '****************** 0 - For  selecting Pending Records with MemberID  ***************
        '****************** 1 - Veiw selected Pending Records with MemberID   ***************
        '****************** 2 - View selected & unselected Pending records for reselection **
        '****************** 3 - For  selecting Paid Records with Order Number ***************
        '****************** 4 - View selected Paid Records with Order Number  ***************
        '************************************************************************************

        Private Sub GetPendingDetails1(ByVal MemberID As Integer, ByVal Status As Integer)
            'To get From Meal Charge Table
            Dim dsRecords As New DataSet
            DataGridMealCharge.Columns(0).Visible = True
            Dim strSQl As String
            If Status = 0 Then
                strSQl = "SELECT M.TempLink,M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND M.PaymentReference is Null AND M.TempLink is Null AND  M.AutoMemberID=" & MemberID
            ElseIf Status = 1 Then
                strSQl = "SELECT M.TempLink,M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND M.PaymentReference is Null AND datediff(d,M.TempLink,'" & lblHidden.Text & "') = 0  AND  M.AutoMemberID=" & MemberID
                DataGridMealCharge.Columns(0).Visible = False
            ElseIf Status = 2 Then
                strSQl = "SELECT M.TempLink,M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND M.PaymentReference is Null AND (M.TempLink is Null OR datediff(d,M.TempLink,'" & lblHidden.Text & "') = 0) AND  M.AutoMemberID=" & MemberID
            ElseIf Status = 3 Then
                strSQl = "SELECT M.TempLink,M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND M.PaymentReference='" & TxtONo.Text & "'"
            ElseIf Status = 4 Then
                strSQl = "SELECT M.TempLink,M.MealChargeID,'Home' AS Chapter,M.ContestYear,I.FirstName + ' ' + I.LastName AS Parent_Name , M.Name, M.RelationType, M.ContestDate, M.ChildFlag, M.ChaperoneFlag,M.EventName, CONVERT(decimal(10, 2), M.Amount)  AS Amount, M.PaymentMode, M.PaymentDate, M.PaymentReference, M.PaymentNotes, M.ModifiedDate, M.ModifiedUserID  FROM MealCharge  M,IndSpouse I where M.AutoMemberID=I.AutoMemberID AND M.PaymentReference='" & TxtONo.Text & "' AND datediff(d,M.TempLink,'" & lblHidden.Text & "') = 0"
                DataGridMealCharge.Columns(0).Visible = False
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strSQl)
            Dim dt As DataTable = dsRecords.Tables(0)
            DataGridMealCharge.DataSource = dt
            DataGridMealCharge.DataBind()
            If (dt.Rows.Count = 0) Then
                PendDetails2.Visible = False
            Else
                PendDetails2.Visible = True
            End If
        End Sub

        Private Sub GetPendingDetails2(ByVal MemberID As Integer, ByVal Status As Integer)
            'To get From Contestant Table
            Dim dsRecords As New DataSet
            Dim strSQl As String
            DataGridContests.Columns(0).Visible = True
            If Status = 0 Then
                strSQl = "SELECT     C.TempLink,C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND C.PaymentReference is Null AND C.TempLink is Null AND  C.ParentID=" & MemberID & " AND C.eventid=" & DdlEventID.SelectedValue
            ElseIf Status = 1 Then
                DataGridContests.Columns(0).Visible = False
                strSQl = "SELECT     C.TempLink,C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND C.PaymentReference is Null AND datediff(d,C.TempLink,'" & lblHidden.Text & "') = 0  AND  C.ParentID=" & MemberID
            ElseIf Status = 2 Then
                strSQl = "SELECT     C.TempLink,C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND C.PaymentReference is Null AND (C.TempLink is Null OR datediff(d,C.TempLink,'" & lblHidden.Text & "') = 0 ) AND  C.ParentID=" & MemberID & " AND C.eventid=" & DdlEventID.SelectedValue
            ElseIf Status = 3 Then
                strSQl = "SELECT     C.TempLink,C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND C.PaymentReference='" & TxtONo.Text & "'"
            ElseIf Status = 4 Then
                strSQl = "SELECT     C.TempLink,C.contestant_id, Ch.ChapterCode as Chapter, C.ContestYear, I.FirstName + ' ' + I.LastName AS Parent_Name, Chld.FIRST_NAME + ' ' + Chld.LAST_NAME AS Child_Name, C.BadgeNumber, C.Score1, C.Score2, C.Score3, C.Rank, CONVERT(decimal(10, 2), C.Fee)  AS Fee, C.PaymentDate,  C.PaymentReference, C.PaymentNotes, C.CreateDate,C.ProductCode,C.EventCode, (SELECT     IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = C.CreatedBy)AS Created FROM Contestant AS C INNER JOIN IndSpouse AS I ON C.ParentID = I.AutoMemberID INNER JOIN Chapter AS Ch ON C.ChapterID = Ch.ChapterID INNER JOIN Child AS Chld ON C.ChildNumber = Chld.ChildNumber AND C.PaymentReference='" & TxtONo.Text & "' AND datediff(d,C.TempLink,'" & lblHidden.Text & "') = 0"
                DataGridContests.Columns(0).Visible = False
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strSQl)
            Dim dt As DataTable = dsRecords.Tables(0)
            DataGridContests.DataSource = dt
            DataGridContests.DataBind()
            If (dt.Rows.Count = 0) Then
                ' Invisible MSg
                PendDetails1.Visible = False
            Else
                PendDetails1.Visible = True
            End If
        End Sub

        Private Sub GetPendingDetails3(ByVal MemberID As Integer, ByVal Status As Integer)
            'To get From Registration Table
            Dim dsRecords As New DataSet
            DataGridWrkshp.Columns(0).Visible = True
            Dim strSQl As String
            If Status = 0 Then
                strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND R.TempLink is Null AND  R.MemberID=" & MemberID
            ElseIf Status = 1 Then
                strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0  AND  R.MemberID=" & MemberID
                DataGridWrkshp.Columns(0).Visible = False
            ElseIf Status = 2 Then
                strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND (R.TempLink is Null or datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0) AND  R.MemberID=" & MemberID
            ElseIf Status = 3 Then
                strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.ChildNumber = C.ChildNumber AND R.PaymentReference='" & TxtONo.Text & "'"
            ElseIf Status = 4 Then
                strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.ChildNumber = C.ChildNumber AND R.PaymentReference='" & TxtONo.Text & "' AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0"
                DataGridWrkshp.Columns(0).Visible = False
                'ElseIf Status = 5 Then
                '    strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference='" & TxtONo.Text & "'"
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strSQl)
            Dim dt As DataTable = dsRecords.Tables(0)
            DataGridWrkshp.DataSource = dt
            DataGridWrkshp.DataBind()
            If (dt.Rows.Count = 0) Then
                PendDetails3.Visible = False
            Else
                PendDetails3.Visible = True
            End If
        End Sub

        Private Sub GetPendingDetails4(ByVal MemberID As Integer, ByVal Status As Integer)
            'To get From CoachingReg Table
            Dim dsRecords As New DataSet
            DataGridCoaching.Columns(0).Visible = True
            Dim strSQl As String
            If Status = 0 Then
                strSQl = "select CR.TempLink,Ef.Fee,CR.EventYear,CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
                strSQl = strSQl & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase = CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as Fee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID  where  CR.PaymentReference is Null AND CR.TempLink is Null AND  CR.PMemberID=" & MemberID
                'strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND R.TempLink is Null AND  R.MemberID=" & MemberID
            ElseIf Status = 1 Then
                strSQl = "select CR.TempLink,Ef.Fee,CR.EventYear,CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
                strSQl = strSQl & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as Fee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID  where  CR.PaymentReference is Null AND datediff(d,CR.TempLink,'" & lblHidden.Text & "') = 0  AND  CR.PMemberID=" & MemberID
                'strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0  AND  R.MemberID=" & MemberID
                DataGridCoaching.Columns(0).Visible = False
            ElseIf Status = 2 Then
                strSQl = "select CR.TempLink,Ef.Fee,CR.EventYear,CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
                strSQl = strSQl & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase = CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as Fee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where  CR.PaymentReference is Null AND (CR.TempLink is Null or datediff(d,CR.TempLink,'" & lblHidden.Text & "') = 0) AND  CR.PMemberID=" & MemberID
                'strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference is Null AND  R.ChildNumber = C.ChildNumber AND (R.TempLink is Null or datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0) AND  R.MemberID=" & MemberID
            ElseIf Status = 3 Then
                strSQl = "select CR.TempLink,Ef.Fee,CR.EventYear,CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
                strSQl = strSQl & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase = CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as Fee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where  CR.PaymentReference ='" & TxtONo.Text & "'"
                'strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference='" & TxtONo.Text & "'"
            ElseIf Status = 4 Then
                strSQl = "select CR.TempLink,Ef.Fee,CR.EventYear,CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.Maxcapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,CR.Approved,CR.PaymentReference,CR.CreateDate,CR.PaymentDate,CR.PaymentNotes from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalsignUp C ON "
                strSQl = strSQl & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase = CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as Fee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where  CR.PaymentReference='" & TxtONo.Text & "' AND datediff(d,CR.TempLink,'" & lblHidden.Text & "') = 0"
                'strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference='" & TxtONo.Text & "' AND datediff(d,R.TempLink,'" & lblHidden.Text & "') = 0"
                DataGridCoaching.Columns(0).Visible = False
                'ElseIf Status = 5 Then
                '    strSQl = "SELECT  R.TempLink,R.RegId,R.EventYear, R.ChapterCode, I.FirstName + ' ' + I.LastName AS Parent_Name,C.FIRST_NAME + ' ' + C.LAST_NAME AS Child_Name, CONVERT(decimal(10, 2), R.Fee)  AS Fee, R.PaymentDate, R.PaymentMode, R.PaymentReference, R.PaymentNotes, R.CreateDate,  R.EventCode, R.ProductCode,  (SELECT IND.FirstName + ' ' + IND.LastName  FROM IndSpouse IND WHERE IND.AutoMemberID = R.CreatedBy)AS CreatedBy  FROM Registration R, IndSpouse I,Child C where R.MemberID=I.AutoMemberID AND R.PaymentReference='" & TxtONo.Text & "'"
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strSQl)
            Dim dt As DataTable = dsRecords.Tables(0)
            DataGridCoaching.DataSource = dt
            DataGridCoaching.DataBind()
            If (dt.Rows.Count = 0) Then
                PendDetails4.Visible = False
            Else
                PendDetails4.Visible = True
            End If
        End Sub

        Sub showPendingdetails(ByVal MemberID As Integer, ByVal Status As Integer)
            ' enter coding to view datas from Child,Inspouse,Contestant, registration, Donation Info & Indspouse In Panel2
            Panel3.Visible = True
            If Status = 0 Then
                If DdlEventID.SelectedValue = 1 Then
                    GetPendingDetails1(MemberID, 0)
                    GetPendingDetails2(MemberID, 0)
                ElseIf DdlEventID.SelectedValue = 2 Then
                    GetPendingDetails2(MemberID, 0)
                ElseIf DdlEventID.SelectedValue = 3 Then
                    GetPendingDetails3(MemberID, 0)
                ElseIf DdlEventID.SelectedValue = 13 Then
                    GetPendingDetails4(MemberID, 0)
                End If
            ElseIf Status = 1 Then
                If DdlEventID.SelectedValue = 1 Then
                    GetPendingDetails1(MemberID, 1)
                    GetPendingDetails2(MemberID, 1)
                ElseIf DdlEventID.SelectedValue = 2 Then
                    GetPendingDetails2(MemberID, 1)
                ElseIf DdlEventID.SelectedValue = 3 Then
                    GetPendingDetails3(MemberID, 1)
                ElseIf DdlEventID.SelectedValue = 13 Then
                    GetPendingDetails4(MemberID, 1)
                End If
            ElseIf Status = 2 Then
                If DdlEventID.SelectedValue = 1 Then
                    GetPendingDetails1(MemberID, 2)
                    GetPendingDetails2(MemberID, 2)
                ElseIf DdlEventID.SelectedValue = 2 Then
                    GetPendingDetails2(MemberID, 2)
                ElseIf DdlEventID.SelectedValue = 3 Then
                    GetPendingDetails3(MemberID, 2)
                ElseIf DdlEventID.SelectedValue = 13 Then
                    GetPendingDetails4(MemberID, 2)
                End If
            ElseIf Status = 3 Then

                If DdlEventID.SelectedValue = 1 Then
                    GetPendingDetails1(MemberID, 3)
                    GetPendingDetails2(MemberID, 3)
                ElseIf DdlEventID.SelectedValue = 2 Then
                    GetPendingDetails2(MemberID, 3)
                ElseIf DdlEventID.SelectedValue = 3 Then
                    GetPendingDetails3(MemberID, 3)
                ElseIf DdlEventID.SelectedValue = 13 Then
                    GetPendingDetails4(MemberID, 3)
                End If
            ElseIf Status = 4 Then
                If DdlEventID.SelectedValue = 1 Then
                    GetPendingDetails1(MemberID, 4)
                    GetPendingDetails2(MemberID, 4)
                ElseIf DdlEventID.SelectedValue = 2 Then
                    GetPendingDetails2(MemberID, 4)
                ElseIf DdlEventID.SelectedValue = 3 Then
                    GetPendingDetails3(MemberID, 4)
                ElseIf DdlEventID.SelectedValue = 13 Then
                    GetPendingDetails4(MemberID, 4)
                End If
                'ElseIf Status = 5 Then
                '    If DdlEventID.SelectedValue = 1 Then
                '        lblDataErr.Text = "Please Select The Cancelled Contest"
                '        GetPendingDetails1(MemberID, 5)
                '        GetPendingDetails2(MemberID, 5)
                '    ElseIf DdlEventID.SelectedValue = 2 Then
                '        lblDataErr.Text = "Please Select The Cancelled Contest"
                '        GetPendingDetails2(MemberID, 5)
                '    ElseIf DdlEventID.SelectedValue = 3 Then
                '        lblDataErr.Text = "Please Select The Cancelled Event"
                '        GetPendingDetails3(MemberID, 5)
                '    End If
            End If
            If PendDetails1.Visible = False And PendDetails2.Visible = False And PendDetails3.Visible = False And PendDetails4.Visible = False Then
                lblPendDetailsErr.Visible = True
                DdlReason.Enabled = True
                DdlEventID.Enabled = True
                btnSel1Continue.Enabled = True
                BtnPnl3Cancel.Visible = False
            Else
                BtnPnl3Cancel.Visible = True
                BtnSelect.Visible = True
                If (Status = 1) Or (Status = 4) Then
                    BtnSelect.Visible = False
                    BtnReSelect.Visible = True
                End If
                lblPendDetailsErr.Visible = False
            End If
        End Sub

        Private Sub SendEMail(ByVal user_email As String, ByVal Status As Integer, ByVal RefundAmount As Integer)
            'Email Sending Status 0 - To NSF Refunds, 1 - Rejected, 2 - Amount Refunded, 3 - Status Change For Pending

            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfrefund@gmail.com"
            'sMailTo = "fsilva@redegginfoexpert.com"
            If Status = 0 Then
                sMailTo = "nsfrefund@gmail.com"
                'sMailTo = "fsilva@redegginfoexpert.com"
                sSubject = "Refund Request filed by Parent"
            ElseIf Status = 1 Then
                sMailTo = user_email
                sSubject = "Refund Request"
            ElseIf Status = 2 Then
                sMailTo = user_email
                sSubject = "Refund Request"
            ElseIf Status = 3 Then
                sMailTo = user_email
                sSubject = "Status Change from Pending to Paid"
            End If
            'sMailTo = "fsilva@redegginfoexpert.com"
            Dim mm As New MailMessage(SMailFrom, sMailTo)
            mm.Subject = sSubject
            sBody = Server.HtmlEncode("")
            sBody = sBody & "Dear Sir/Madam, <br>"
            If Status = 0 Then
                sBody = sBody & "A parent filed a request for " & DdlReason.SelectedItem.Text & ".  His/Her email address is " & user_email & ". The scenario is " & DdlReason.SelectedItem.Text & ". "
            ElseIf Status = 1 Then
                sBody = sBody & "You request for refund was denied."
            ElseIf Status = 2 Then
                sBody = sBody & "A refund of $" & RefundAmount & " was processed and will show up on your next bill.  Thank you for your continued support."
            ElseIf Status = 3 Then
                sBody = sBody & "Your pending transactions corresponding to the amount you were charged have been updated to the Paid status.  Please verify by going to View Status.  We are sorry for the inconvenience you have experienced on this matter.  Thank you for your continued support."
            End If

            mm.Body = sBody
            mm.IsBodyHtml = True

            '(3) Create the SmtpClient object
            Dim client As New SmtpClient()

            '(4) Send the MailMessage (will use the Web.config settings)
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

            'client.Host = host
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            'client.Timeout = 20000
            Try
                client.Send(mm)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End Sub

        Protected Sub BtnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSelect.Click
            'Update the Table
            Dim dgItem As DataGridItem
            Dim selectRadioButton As CheckBox
            Dim intKeyValue As Integer
            Dim bolSelected As Boolean
            Dim totalFee As Double
            Dim mealfee As Double
            Dim lblFee As Label
            totalFee = 0.0
            mealfee = 0.0
            Dim i As Integer = 0
            Try
                bolSelected = False
                For Each dgItem In DataGridContests.Items
                    selectRadioButton = dgItem.FindControl("chkContests")
                    If selectRadioButton.Checked = True Then
                        bolSelected = True
                        Session("Pending") = True
                        lblFee = dgItem.FindControl("lblContestsFee")
                        If lblFee.Text.Trim().Length > 0 Then
                            totalFee = totalFee + Double.Parse(lblFee.Text)
                        End If
                        intKeyValue = DataGridContests.DataKeys(dgItem.ItemIndex)
                        'next codings 
                        If i = 0 Then
                            i += 1
                            Session("StrContestIDs") = Convert.ToString(intKeyValue)
                        Else
                            Session("StrContestIDs") = Session("StrContestIDs") & "," & Convert.ToString(intKeyValue)

                        End If
                    End If
                Next
                i = 0
                For Each dgItem In DataGridWrkshp.Items
                    selectRadioButton = dgItem.FindControl("chkWrkshp")
                    If selectRadioButton.Checked = True Then
                        bolSelected = True
                        Session("Pending") = True
                        lblFee = dgItem.FindControl("lblWrkshpFee")
                        If lblFee.Text.Trim().Length > 0 Then
                            totalFee = totalFee + Double.Parse(lblFee.Text)
                        End If
                        intKeyValue = DataGridWrkshp.DataKeys(dgItem.ItemIndex)
                        'next codings
                        If i = 0 Then
                            i += 1
                            Session("StrRegisIDs") = intKeyValue.ToString()
                        Else
                            Session("StrRegisIDs") = Session("StrRegisIDs") & "," & intKeyValue.ToString()
                        End If
                    End If
                Next
                i = 0
                For Each dgItem In DataGridMealCharge.Items
                    selectRadioButton = dgItem.FindControl("chkMealCharge")
                    If selectRadioButton.Checked = True Then
                        bolSelected = True
                        Session("Pending") = True
                        lblFee = dgItem.FindControl("lblMealChargeFee")
                        If lblFee.Text.Trim().Length > 0 Then
                            mealfee = mealfee + Double.Parse(lblFee.Text)
                        End If
                        intKeyValue = DataGridMealCharge.DataKeys(dgItem.ItemIndex)
                        'next codings 
                        If i = 0 Then
                            i += 1
                            Session("StrMealChargeIDs") = intKeyValue.ToString()
                        Else
                            Session("StrMealChargeIDs") = Session("StrMealChargeIDs") & "," & intKeyValue.ToString()
                        End If
                    End If
                Next
                i = 0
                For Each dgItem In DataGridCoaching.Items
                    selectRadioButton = dgItem.FindControl("chkCoaching")
                    If selectRadioButton.Checked = True Then
                        bolSelected = True
                        Session("Pending") = True

                        'totalFee = totalFee + 150.0
                        lblFee = dgItem.FindControl("lblCoachingFee")
                        If lblFee.Text.Trim().Length > 0 Then
                            totalFee = totalFee + Double.Parse(lblFee.Text)
                        End If
                        intKeyValue = DataGridCoaching.DataKeys(dgItem.ItemIndex)
                        'next codings
                        If i = 0 Then
                            i += 1
                            Session("StrCoachingIDs") = intKeyValue.ToString()
                        Else
                            Session("StrCoachingIDs") = Session("StrCoachingIDs") & "," & intKeyValue.ToString()
                        End If
                    End If
                Next
            Catch ex As Exception
                'MsgBox(ex.ToString())
            End Try
            If Session("Pending") = True Then
                Dim Comments As String = ""
                Dim StrSQl As String

                If Session("StrContestIDs") <> Nothing Or Session("StrRegisIDs") <> Nothing Or Session("StrCoachingIDs") <> Nothing Then
                    If Session("StrContestIDs") <> Nothing Then
                        StrSQl = "select ProductCode+'('+Convert(varchar(6),ChildNumber)+')('+Convert(varchar(6),ChapterID)+')(-'+Convert(varchar(6),Fee)+') ' as Comments from Contestant Where Contestant_ID in (" & Session("StrContestIDs") & ")"
                    ElseIf Session("StrCoachingIDs") <> Nothing Then
                        StrSQl = "select CR.ProductCode+'('+Convert(varchar(6),CR.ChildNumber)+')('+Convert(varchar(6),CR.ChapterID)+')(-'+Convert(varchar(6),EF.RegFee)+') ' as Comments from CoachReg CR INNER JOIN (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as RegFee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where CR.CoachRegID in (" & Session("StrCoachingIDs") & ")"
                    Else
                        StrSQl = "select ProductCode+'('+Convert(varchar(6),ChildNumber)+')('+Convert(varchar(6),ChapterID)+')(-'+Convert(varchar(6),Fee)+') ' as Comments from Registration Where regid in (" & Session("StrRegisIDs") & ")"
                    End If
                    Try
                        Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, StrSQl)
                        While Reader.Read()
                            Comments = Comments & Reader("Comments")
                        End While
                        Reader.Close()
                    Catch ex As Exception
                    End Try
                End If
                'If Session("StrCoachingIDs") <> Nothing Then
                '    Comments = "Donation:" & totalFee & " Total:" & totalFee
                '    'TxtNaPNotes.Text = Comments.ToString()
                '    lblTotalFeeMsg.Text = totalFee.ToString
                'Else
                Comments = Comments & "RegFee:-" & totalFee & " Mealsamount:" & -1 * mealfee & " LateFee:0 Donation:0 Total:-" & (totalFee + mealfee).ToString
                TxtNaPNotes.Text = Comments.ToString()
                lblTotalFeeMsg.Text = (totalFee + mealfee).ToString
                'End If
                TrTotalFee.Visible = True
                Panel3.Visible = False
                lblDataErr.Text = ""
            Else
                TrTotalFee.Visible = False
                lblDataErr.Text = "You have not selected any option in panel3"
            End If

        End Sub

        Protected Sub BtnReSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReSelect.Click
            Dim flag As Integer = 0
            If DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Or (DdlReason.SelectedValue = 4 And DdlEventID.SelectedValue = 13) Then
                flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM  Refund WHERE CONVERT(VARCHAR(10), TempLink, 101) = CONVERT(VARCHAR(10), GETDATE(), 101) AND Pa_OrderNumber='" & TxtONo.Text & "' AND RefundId <>" & Session("RefundID") & "")
            Else
                flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM  Refund WHERE CONVERT(VARCHAR(10), TempLink, 101) = CONVERT(VARCHAR(10), GETDATE(), 101) AND MemberID=" & Convert.ToInt32(lblHiddenMemberID.Text) & " AND RefundId <>" & Session("RefundID") & "")
            End If
            If flag < 1 Then
                If DdlReason.SelectedValue = 3 Or DdlReason.SelectedValue = 5 Then
                    showPendingdetails(0, 3)
                ElseIf DdlReason.SelectedValue = 4 And DdlEventID.SelectedValue = 13 Then
                    showPendingdetails(0, 3)
                Else
                    showPendingdetails(Convert.ToInt32(lblHiddenMemberID.Text), 2)
                End If
                BtnReSelect.Visible = False
            Else
                lblDataErr.Text = "Only one Edit/Submit Permitted in Panel3 Per day"
                BtnReSelect.Visible = False
                BtnPnl3Cancel.Visible = False
            End If
        End Sub

        Protected Sub BtnPnl3Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPnl3Cancel.Click
            Panel3.Visible = False
        End Sub

        Sub configGDE4()
            Dim G4 As NameValueCollection = CType(ConfigurationManager.GetSection("GDE4/crd"), NameValueCollection)
            e4_Id = G4("E4UserName")
            e4_Pass = G4("E4Password")
            e4_Platform = G4("Platform")
            'lblStatus.Text = "Config File : " + d1 _
            '+ "<br>" + "Key File : " + d2 + "<br>" _
            '+ "Host : " + d3 + "<br>" + "Port : " + d4 _
            '+ "<br>" + "Result : " + d5
        End Sub

        Function GetTagAuth(ByVal type As String, ByRef inputBox As TextBox) As String
            Dim index As Integer = -1
            Dim RCode As String = String.Empty
            If type = "A" Then
                index = inputBox.Text.IndexOf("-")
                If index > 0 Then
                    RCode = inputBox.Text.Trim().Substring(0, index)
                End If
            ElseIf type = "T" Then
                index = inputBox.Text.Trim().LastIndexOf("-")
                If (index > 0) Then
                    index += 1
                    RCode = inputBox.Text.Trim().Substring(index)
                End If
                End If
                Return RCode
        End Function

        Private Sub PreUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim eventId As Integer = 0
            Dim pName As String = System.IO.Path.GetFileNameWithoutExtension(Request.Path)
            Dim sqlparams(2) As SqlClient.SqlParameter
            sqlparams(0) = New SqlParameter("@Reference", SqlDbType.VarChar)
            sqlparams(0).Value = TxtNaONo.Text
            sqlparams(1) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(1).Direction = ParameterDirection.Output
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertCCSubmitLog_Refund", sqlparams)
                Cust_id = sqlparams(1).Value.ToString()
            Catch ex As Exception
                lblNaErr.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support."
                lblNaErr.Text = (lblNaErr.Text + ex.Message)
                lblNaErr.Visible = True
            End Try
        End Sub
        Private Sub PostUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim userId As String = String.Empty
            If (Not Session("LoginID") Is Nothing) Then
                userId = Session("LoginID").ToString
            End If

            Try
                '' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE CCSubmitLog SET RefundReference ='" & e4_Auth & "-" & e4_Tag & "',StatusReturn ='" & e4_BankMessage & "',ModifiedBy='" & Session("LoginID").ToString & "',Modifydate = GETDATE(),[Donation] = - " & Donation & ",[Fee] = -[Fee],[Amount] = -" & txtNaRefundAmt.Text & ",[PaymentNotes] = '" & TxtNaPNotes.Text & "' WHERE CCSubmitLogID = " & Cust_id)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE CCSubmitLog SET RefundReference ='" & e4_Auth & "-" & e4_Tag & "',StatusReturn ='" & e4_BankMessage & "',ModifiedBy='" & Session("LoginID").ToString & "',Modifydate = GETDATE(),[Donation] = " & Re_Donation & ",[Fee] = " & Re_Regfee & ",[LateFee] = " & Re_Latefee & ",[Meal] = " & Re_Meals & ",[Amount] = " & Re_Amount & ",[PaymentNotes] = '" & TxtNaPNotes.Text & "' WHERE CCSubmitLogID = " & Cust_id)

            Catch ex As SqlException
                lblNaErr.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support."
                lblNaErr.Text = (lblNaErr.Text + ex.Message)
            End Try
        End Sub

    End Class
End Namespace

